﻿using System;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Runtime.Remoting.Lifetime;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.ServerApplication.Helpers.Tables;

namespace FORIS.TSS.ServerApplication.Helpers
{
	/* Есть одно маленькое но. 
	 * Когда на клиентской стороне меняются параметры контейнера данных, 
	 * то это приводит к разрушению контейнера, загрузке данных и 
	 * построению контейнера снова. 
	 * На стороне сервера параметры у контейнера данных меняться не могут.
	 */

	/// <summary>
	/// Абстрактный класс - контейнер данных. 
	/// </summary>
	/// <remarks>
	/// Данный класс более приближен к предметной области, инкапсулирует 
	/// кастование типов для полей и многое другое.
	/// При конструировании контейнера данных он рождается 
	/// с пустым инициализированным объектом m_DataSet.
	/// И с инициализированными объектами таблиц - пустыми, конечно же.
	/// И все это что бы контролы с присоединенными контейнерами данных
	/// не вызывали исключения при конструировании в дизайнере.
	/// </remarks>
	public class Data : Component
	{
		#region LifeTime

		/// <summary>
		/// Влияет на лицензию, выдаваемую контейнерам данных
		/// </summary>
		public static readonly TimeSpan InitialLifeTime = TimeSpan.FromHours( 2 );

		public override object InitializeLifetimeService()
		{
			ILease Lease = (ILease)base.InitializeLifetimeService();

			Lease.InitialLeaseTime = Data.InitialLifeTime;
			Lease.RenewOnCallTime = Data.InitialLifeTime;

			return Lease;
		}

		#endregion // LifeTime

		#region Properties

		public DataSet DataSet
		{
			get { return this.dsDataSet; }
		}


		public virtual OperatorTable Operator { get { return this.tblOperator; } }

		#endregion Properties

		#region Controls & Components

		private System.ComponentModel.IContainer components;

		protected DataSet dsDataSet;

		protected FORIS.TSS.ServerApplication.Helpers.DataDispatcher ddTables;
		protected FORIS.TSS.ServerApplication.Helpers.DataDispatcher ddMain;


		private OperatorTable tblOperator;

		#endregion Controls & Components

		#region Properties

		private Guid m_GUID = Guid.NewGuid();
		public Guid GUID
		{
			get { return m_GUID; }
		}


		#endregion Properties

		#region Data

		protected virtual void OnBeforeSetData()
		{

		}
		protected virtual void OnAfterSetData()
		{

		}


		#endregion // Data

		#region View

		protected virtual void OnDestroyView()
		{

		}
		protected virtual void OnBuildView()
		{

		}
		protected virtual void OnUpdateView()
		{

		}


		#endregion // View

		#region Constructor & Dispose

		public Data()
		{
			InitializeComponent();
		}
		public Data( IContainer container )
		{
			container.Add( this );

			InitializeComponent();
		}



		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}

				Debug.WriteLine("Data.Dispose() " + this.GetType().FullName);
			}

			base.Dispose(disposing);
		}


		#endregion Constructor & Dipsose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.ddTables = new FORIS.TSS.ServerApplication.Helpers.DataDispatcher(this.components);


			this.ddMain = new FORIS.TSS.ServerApplication.Helpers.DataDispatcher(this.components);
			this.dsDataSet = new System.Data.DataSet();
			((System.ComponentModel.ISupportInitialize)(this.dsDataSet)).BeginInit();
			// 
			// ddTables
			// 

			this.ddMain.AfterSetData += new System.EventHandler(this.ddMain_AfterSetData);
			this.ddMain.UpdateView += new System.EventHandler(this.ddMain_UpdateView);
			this.ddMain.BeforeSetData += new System.EventHandler(this.ddMain_BeforeSetData);
			this.ddMain.BuildView += new System.EventHandler(this.ddMain_BuildView);
			this.ddMain.DestroyView += new System.EventHandler(this.ddMain_DestroyView);
			this.dsDataSet.DataSetName = "Data";
			this.dsDataSet.Locale = new System.Globalization.CultureInfo("ru-RU");
			((System.ComponentModel.ISupportInitialize)(this.dsDataSet)).EndInit();

		}


		#endregion Component Designer generated code

		#region ddMain events

		private void ddMain_BeforeSetData(object sender, System.EventArgs e)
		{
			this.OnBeforeSetData();
		}
		private void ddMain_AfterSetData(object sender, System.EventArgs e)
		{
			this.OnAfterSetData();
		}


		private void ddMain_DestroyView(object sender, System.EventArgs e)
		{
			this.OnDestroyView();
		}
		private void ddMain_BuildView(object sender, System.EventArgs e)
		{
			this.OnBuildView();
		}
		private void ddMain_UpdateView(object sender, System.EventArgs e)
		{
			this.OnUpdateView();
		}


		#endregion ddMain events

		public virtual void AcceptChanges(IPersonalServer session)
		{
		}
		public virtual void RejectChanges()
		{
		}
	}
}