using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Interfaces.Data.Department;
using FORIS.TSS.Helpers;
using FORIS.TSS.ServerApplication.Helpers;
using DataParams=FORIS.TSS.BusinessLogic.Interfaces.Data.DataParams;

namespace FORIS.TSS.ServerApplication.Helpers
{
	/// <summary>
	/// Summary description for DepartmentData.
	/// </summary>
	public class DepartmentData: 
		DataBase<DataParams>, 
		IDepartmentDataServerSupplier
	{
		#region Controls & Components

		private System.ComponentModel.IContainer components;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public DepartmentData()
			: base( DataParams.Empty )
		{
			InitializeComponent();
		}

		public DepartmentData( IContainer container ):this()
		{
			container.Add( this );
		}

		protected override void Dispose(bool disposing)
		{
			if( disposing )
			{
				if( components != null )
				{
					components.Dispose();
				}
			}

			base.Dispose (disposing);
		}

		#endregion Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			((System.ComponentModel.ISupportInitialize)(this.DataSet)).BeginInit();
			// 
			// DataSet
			// 
			this.DataSet.DataSetName = "Data";
			this.DataSet.Locale = new System.Globalization.CultureInfo("ru-RU");

			((System.ComponentModel.ISupportInitialize)(this.DataSet)).EndInit();
		}
		
		#endregion // Component Designer generated code

		#region Schema

		protected override void OnMakeSchema( DataSet dataSet )
		{
			List<string> TableNames = new List<string>();

			foreach( string tableName in FORIS.TSS.BusinessLogic.Interfaces.Data.Tables.GetDataForDepartmentNew )
			{
				TableNames.Add( tableName );
			}


			DataSet SeedDataSet = new DataSet();

			foreach( string tableName in TableNames )
			{
				SeedDataSet.Tables.Add( tableName );
			}

			DatasetHelper.Merge( dataSet, SeedDataSet );
		}

		#endregion // Schema

		protected override DataSet OnLoadData( IDatabaseDataSupplier databaseDataSupplier )
		{
			DataSet DataSetDepartment =
				databaseDataSupplier.GetDataFromDB(
					new ParamValue[] { },
					"dbo.GetDataForDepartmentNew",
					FORIS.TSS.BusinessLogic.Interfaces.Data.Tables.GetDataForDepartmentNew
					);

			return DataSetDepartment;
		}
	}
}
