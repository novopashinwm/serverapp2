using FORIS.TSS.BusinessLogic.Interfaces.Data;

namespace FORIS.TSS.ServerApplication.Helpers
{
	public interface IDataItem<TData, TParams>
		where TData: DataBase<TParams>
		where TParams: DataParams
	{
		TData Data { get; set; }
	}

	public interface IDataItem
	{
		Data Data { get; set; }
	}
}
