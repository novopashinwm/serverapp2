using FORIS.TSS.Common;

namespace FORIS.TSS.ServerApplication.Helpers.Common
{
	public sealed class ChildRowCollection<TRow> : SortedCollectionWithEvents<TRow>
		where TRow: Row
	{
		public TRow First { get { return this[0]; } }
		public TRow Last { get { return this[this.Count-1]; } }
	}
}