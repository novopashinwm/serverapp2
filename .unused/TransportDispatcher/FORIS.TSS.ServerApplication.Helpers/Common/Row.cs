using System;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using FORIS.TSS.Common.Collections;

namespace FORIS.TSS.ServerApplication.Helpers.Common
{
	public abstract class Row: IDisposable, IRow
	{
		#region Implement IDataItem 

		private Data data;
		[ Browsable(false) ]
		[DefaultValue(null)]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden) ]
		public virtual Data Data
		{
			get { return this.data; }
			set
			{
				if( this.data != null )
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.data = value;

				if( this.data != null )
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		#endregion // Implement IDataItem 

		#region ISortable members

		object ISortable.Key
		{
			get { return this; }
		}
		
		#endregion // ISortable members

		#region IDisposable members

		public void Dispose()
		{
			this.Dispose( true );
		}
			

		#endregion // IDisposable members

		#region Fields
        
		public int ID
		{
			get { return GetID(); }
		}

		protected abstract int GetID();
		
		#endregion Fields

		#region Properties

		private Guid guid;
		public Guid GUID
		{
			get { return this.guid; }
		}
		
//		private ITable m_Table;
//		public ITable Table { get { return m_Table; } }

		private ITable table;

		public ITable Table
		{
			get { return this.table; }
			set { this.table = value; }
		}

		private DataRow dataRow;
		public virtual DataRow DataRow
		{
			get { return dataRow; }
		}

		public DataRowState RowState { get { return this.DataRow.RowState; } }
		
		public bool Free { get { return this.Table == null; } }

		#endregion // Properties

		#region IRow members

		int IRow.ID
		{
			get { return this.ID; }
		}
		DataRow IRow.DataRow
		{
			get { return this.DataRow; }
		}
		
		
		bool IRow.Free
		{
			get { return this.Free; }
		}

//		Table IRow.Table
//		{
//			get { return m_Table; }
//			set { m_Table = value; }
//		}
		
		Guid IRow.GUID
		{
			set { guid = value; }
		}

		void IRow.Delete()
		{
			this.Delete();
		}

		void IRow.BeginEdit()
		{
			this.OnBeforeChange();

			#region View

			Trace.Assert( this.Data != null );

			this.OnDestroyView();

			#endregion // View
		}
		void IRow.CancelEdit()
		{
			#region View

			Trace.Assert( this.Data != null );

			this.OnBuildView();
			this.OnUpdateView();

			#endregion // View

			this.OnAfterChange();
		}
		void IRow.EndEdit()
		{
			if( this.Free )
			{
				guid = this.Data.GUID;
			}

			#region View

			this.OnBuildView();
			this.OnUpdateView();

			#endregion // View

			this.OnAfterChange();
		}
		
		#endregion // IRow Members

		#region IComparable members

		public int CompareTo( object obj )
		{
			return this.CompareTo( (Row)obj );
		}

		protected virtual int CompareTo( Row Row )
		{
			return this.ID.CompareTo( Row.ID );
		}

		
		#endregion IComparable members

		#region Data

		protected virtual void OnBeforeSetData()
		{
			
		}
		protected virtual void OnAfterSetData()
		{
			
		}

		#endregion Data

		#region View

		protected virtual void OnDestroyView()
		{
			
		}
		protected virtual void OnBuildView()
		{
			if( this.DataRow != null )
			{
//				if( this.DataRow.RowState == DataRowState.Added )
//				{
//					this.OnChanging( new RowEventArgs( this ) );
//				}

				this.OnBuildFields();
			}
		}
		protected virtual void OnUpdateView()
		{
		}

		
		#endregion // View

		#region Consructor & Dispose

		protected Row( DataRow dataRow )
		{
			this.dataRow = dataRow;
		}

		protected virtual void Dispose( bool disposing )
		{
			if( disposing )
			{
				dlgtBeforeChange = null;
				dlgtAfterChange = null;
			}
		}


		#endregion // Constructor & Dispose

		#region Manage
	
		/// <summary>
		/// ������ ��������� �� ���������
		/// </summary>
		
		public void Delete()
		{
			Debug.Assert( 
				this.DataRow.RowState == DataRowState.Added || 
				this.DataRow.RowState == DataRowState.Modified || 
				this.DataRow.RowState == DataRowState.Unchanged 
				);

			this.DataRow.Delete();
		}

		public void SetData( Row Row )
		{
			if( this.Data != null )
			{
				this.OnDestroyView();
			}

			this.DataRow.ItemArray = (object[])Row.DataRow.ItemArray.Clone();

			if( this.Data != null )
			{
				this.OnBuildView();
				this.OnUpdateView();
			}
		}


		#endregion // Manage

		#region Edit Begin/Cancel/End
		
		public void BeginEdit()
		{
//			( (IRow)this ).BeginEdit();

			this.DataRow.BeginEdit();
		}

		public void CancelEdit()
		{
			this.DataRow.CancelEdit();

//			( (IRow)this ).CancelEdit();
		}

		public void EndEdit()
		{
			this.DataRow.EndEdit();

//			( (IRow)this ).EndEdit();            
		}

		
		#endregion Edit Begin/Cancel/End

		#region Build fields

		protected abstract void OnBuildFields();

		#endregion Build fields
		
		#region Events

		private EventHandler dlgtBeforeChange;
		private EventHandler dlgtAfterChange;


		/// <summary>
		/// ������� ����� ���������� ������ ������ (��� ������ BeginEdit() )
		/// </summary>
		public event EventHandler BeforeChange
		{
			add { this.dlgtBeforeChange += value; }
			remove { this.dlgtBeforeChange -= value; }
		}
		/// <summary>
		/// ������� ��������� ������ ������ (��� ������ EndEdit() )
		/// </summary>
		public event EventHandler AfterChange
		{
			add { this.dlgtAfterChange += value; }
			remove { this.dlgtAfterChange -= value; }
		}

		
		protected virtual void OnBeforeChange()
		{
			if( this.dlgtBeforeChange != null /* && !this.Table.Data.UnderConstruction */ )
			{
				this.dlgtBeforeChange( this, EventArgs.Empty );
			}
		}
		
		protected virtual void OnAfterChange()
		{
			if( this.dlgtAfterChange != null /* && !this.Table.Data.UnderConstruction */ )
			{
				this.dlgtAfterChange( this, EventArgs.Empty );
			}
		}

//		protected virtual void OnChanging( RowEventArgs e )
//		{
//			
//		}


		#endregion Events
	}
	internal interface IRow: IDataItem, ISortable, IComparable
	{
		int ID { get; }
		DataRow DataRow { get; }

		
		bool Free { get; }

		Guid GUID { set; }

		void Delete();

		void BeginEdit();
		void CancelEdit();
		void EndEdit();
		
//		event EventHandler BeforeChange;
//		event EventHandler AfterChange;
	}

	public delegate void RowEventHandler<TRow>( object sender, RowEventArgs<TRow> e ) where TRow: Row;

	public class RowEventArgs<TRow>: EventArgs
		where TRow : Row
	{
		#region Properties

		private TRow row;
		
		public TRow Row
		{
			get { return this.row; }
		}

		#endregion // Properties

		#region Constructor

		public RowEventArgs( TRow row )
		{
			this.row = row;
		}

		#endregion // Constructor
	}
}