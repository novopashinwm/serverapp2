﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using FORIS.TSS.Common;

namespace FORIS.TSS.ServerApplication.Helpers.Common
{
		
	[DesignTimeVisible(false)]
	public abstract class Table<TRow>: Component, ITable, IDataItem
		where TRow: Row
	{
		#region Implement IDataItem 

		[ Browsable(false) ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden) ]
		public virtual Data Data
		{
			get { return this.ddMain.Data; }
			set
			{
				this.ddMain.Data = value;
			}
		}


		#endregion // Implement IDataItem 

		#region Properties

		public abstract string Name { get; }


		#endregion // Properties

		#region Rows

		protected internal InternalRowCollection<TRow> rows;
		private InternalRowCollection<TRow> changingRows;
		
		public IRowCollection<TRow> Rows
		{
			get { return this.rows; }
		}

		#endregion Rows

		#region Data
		
		private DataTable dataTable;
		[ Browsable(false) ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden) ]
		public DataTable DataTable
		{
			get { return dataTable; }
		}

		
		protected virtual void OnBeforeSetData()
		{
//			#region Rows supervision
//
//			lock( this.Rows )
//			{
//				foreach( Row row in this.Rows )
//				{
//					row.BeforeChange -= m_EventHandlerRowBeforeChange;
//					row.AfterChange -= m_EventHandlerRowAfterChange;
//				}
//			}
//
//			#endregion // Rows supervision

			#region DataTable supervision

			if( this.DataTable != null )
			{
				this.DataTable.RowDeleting -= new DataRowChangeEventHandler( DataTable_RowDeleting );
				this.DataTable.RowChanging -= new DataRowChangeEventHandler( DataTable_RowChanging );
				this.DataTable.RowChanged -= new DataRowChangeEventHandler( DataTable_RowChanged );
			}

			#endregion DataTable supervision
		}
		protected virtual void OnAfterSetData()
		{
			#region DataTable supervision

			if( this.DataTable != null )
			{
				this.DataTable.RowDeleting += new DataRowChangeEventHandler( DataTable_RowDeleting );				
				this.DataTable.RowChanging += new DataRowChangeEventHandler( DataTable_RowChanging );
				this.DataTable.RowChanged += new DataRowChangeEventHandler( DataTable_RowChanged );
			}

			#endregion DataTable supervision
		
//			#region Rows supervision
//
//			lock( this.Rows )
//			{
//				foreach( Row row in this.Rows )
//				{
//					row.BeforeChange += m_EventHandlerRowBeforeChange;
//					row.AfterChange += m_EventHandlerRowAfterChange;
//				}
//			}
//
//			#endregion // Rows supervison
		}


		#endregion // Data

		#region Data events

		private void DataTable_RowDeleting( object sender, DataRowChangeEventArgs e )
		{
			Debug.Assert( this.DataTable != null );

			switch( e.Action )
			{
				#region Delete

				case DataRowAction.Delete:

					int IdRow = (int)e.Row[ e.Row.Table.PrimaryKey[0] ];

					TRow Row = this.rows[ IdRow ];

					Debug.Assert( Row != null, "Попытка удаления строки без обертки" );

					try
					{
						/* Форсируем удаление строк по дочерним связям,
						 * чтоб их удаление происходило ДО того как строка 
						 * исчезнет из коллекции Rows 
						 */

						foreach( DataRelation rel in this.DataTable.ChildRelations )
						{
							foreach( DataRow row in Row.DataRow.GetChildRows(rel) )
								row.Delete();
						}
						
						this.rows.Remove( Row );
						
						Debug.WriteLine( "Удалена запись ID=" + IdRow.ToString() + " из таблицы " + this.Name );
					}
					finally
					{
						Row.Table = null;						
					}

				break;

				#endregion Delete
			}
		}
		private void DataTable_RowChanging( object sender, DataRowChangeEventArgs e )
		{
			Trace.Assert( this.DataTable != null );

			switch( e.Action )
			{
				case DataRowAction.Change:
					{
						/* Возникает такая хитрая фишка.
						 * Если разработчик вызвал BeginEdit() для обертки заранее, 
						 * то строки нет в this.rows, она уже в this.changingRows.
						 * 
						 * А событие это возникает лишь при вызове EndEdit() для строки данных.
						 * 
						 */
						IRow Row = this.rows[e.Row];

						Row.BeginEdit();
					}
					break;
			}
		}
		private void DataTable_RowChanged( object sender, DataRowChangeEventArgs e )
		{
			Trace.Assert( this.DataTable != null );

			switch( e.Action )
			{
				case DataRowAction.Change:
					{
						IRow Row = this.changingRows[e.Row];

						Row.EndEdit();
					}
					break;

				case DataRowAction.Rollback:
					{
						IRow Row = this.changingRows[e.Row];

						Row.CancelEdit();
					}
					break;

					#region Add

				case DataRowAction.Add:
					{
						TRow Row;

						if( this.hashQueue.ContainsKey( e.Row ) )
						{
							Row = this.hashQueue[e.Row];

							this.hashQueue.Remove( e.Row );
						}
						else
						{
							Trace.WriteLine( "RowCollection.DataTable_RowChanged() row added beside queue" );

							Row = this.OnCreateRow( e.Row );
						}

						/* В этом месте так как обертка в таблицу еще не добавлена,
						 * значения полей (и ключа) не посчитаны и все по нулям.
						 * 
						 * А следовательно Row.ID всегда 0 и this.rows 
						 * такого ключа не содержит. Глупая неуместная проверка.
						 */

						Debug.Assert( !this.rows.ContainsID( Row.ID ) );

						Row.Table = this;

						try
						{
							this.rows.Add( Row );
						}
						catch( Exception Ex )
						{
							Row.DataRow.RowError = Ex.Message;
						}
					}
					break;

					#endregion // Add
			}
		}

		
		#endregion // Data events

		#region View

		protected virtual void OnDestroyView()
		{
			try
			{
				lock( this.rows )
				{
					this.rows.Clear();
				}

				dataTable = null;
			}
			catch( Exception Ex )
			{
				Debug.WriteLine( "Table.OnDestroyView() Exception: " + Ex.Message );
			}
		}
		protected virtual void OnBuildView()
		{
			try
			{
				Debug.Assert( this.Name != null && this.Name != String.Empty );

				this.dataTable = this.Data.DataSet.Tables[ this.Name ];
			}
			catch( Exception Ex )
			{
				Debug.WriteLine( "Table.OnBuildView() Exception: " + Ex.Message );
			}
		}
		protected virtual void OnUpdateView()
		{
			try
			{
				if( this.DataTable != null )
				{
					lock( this.rows )
					{
						foreach( DataRow rowData in this.DataTable.Rows )
							if( rowData.RowState != DataRowState.Detached )
							{
								TRow Row = this.OnCreateRow( rowData );
								
								Row.Table = this;
								
								this.rows.Add( Row );
							}
					}
				}
			}
			catch( Exception Ex )
			{
				Debug.WriteLine( "Table.OnUpdateView() Exception: " + Ex.Message );
			}
		}

		
		#endregion // View

		#region Members

		private Dictionary<DataRow, TRow> hashQueue;

		private EventHandler rowBeforeChangeHandler;
		private EventHandler rowAfterChangeHandler;

		#endregion // Members

		#region Components

		private System.ComponentModel.IContainer components;
		protected FORIS.TSS.ServerApplication.Helpers.DataDispatcher ddMain;
		protected FORIS.TSS.ServerApplication.Helpers.DataDispatcher ddRows;
		private FORIS.TSS.ServerApplication.Helpers.DataDispatcher ddChangingRows;
		protected FORIS.TSS.ServerApplication.Helpers.DataDispatcher ddClones;
		private FORIS.TSS.ServerApplication.Helpers.DataAmbassador daClonesDataDispatcher;
		protected FORIS.TSS.ServerApplication.Helpers.DataAmbassador daRowsDataDispatcher;
		private FORIS.TSS.ServerApplication.Helpers.DataAmbassador daChangingRowsDataDispatcher;

		#endregion // Components
		
		#region Constructor & Dispose

		/// <summary>
		/// Default constructor. For designer only.
		/// </summary>
		public Table()
		{
			InitializeComponent();

			Initialize();
		}
		public Table( IContainer Container )
		{
			Container.Add( this );

			InitializeComponent();

			Initialize();
		}


		protected override void Dispose(bool disposing)
		{
			if( disposing )
			{
				this.rows.Clear();
				this.changingRows.Clear();

				if( components != null )
				{
					components.Dispose();
				}
			}

			base.Dispose (disposing);
		}
		
		
		#endregion Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.ddMain = new FORIS.TSS.ServerApplication.Helpers.DataDispatcher(this.components);
			this.daRowsDataDispatcher = new FORIS.TSS.ServerApplication.Helpers.DataAmbassador();
			this.daChangingRowsDataDispatcher = new FORIS.TSS.ServerApplication.Helpers.DataAmbassador();
			this.ddRows = new FORIS.TSS.ServerApplication.Helpers.DataDispatcher(this.components);
			this.ddChangingRows = new FORIS.TSS.ServerApplication.Helpers.DataDispatcher( this.components );
			this.ddClones = new FORIS.TSS.ServerApplication.Helpers.DataDispatcher(this.components);
			this.daClonesDataDispatcher = new FORIS.TSS.ServerApplication.Helpers.DataAmbassador();
			// 
			// ddMain
			// 
			this.ddMain.Ambassadors.Add(this.daRowsDataDispatcher);
			this.ddMain.Ambassadors.Add( this.daChangingRowsDataDispatcher );
			this.ddMain.Ambassadors.Add(this.daClonesDataDispatcher);
			this.ddMain.AfterSetData += new System.EventHandler(this.ddMain_AfterSetData);
			this.ddMain.UpdateView += new System.EventHandler(this.ddMain_UpdateView);
			this.ddMain.BeforeSetData += new System.EventHandler(this.ddMain_BeforeSetData);
			this.ddMain.BuildView += new System.EventHandler(this.ddMain_BuildView);
			this.ddMain.DestroyView += new System.EventHandler(this.ddMain_DestroyView);
			// 
			// daRowsDataDispatcher
			// 
			this.daRowsDataDispatcher.Item = this.ddRows;
			//
			// daChangingRowsDataDispatcher
			//
			this.daChangingRowsDataDispatcher.Item = this.ddChangingRows;
			// 
			// daClonesDataDispatcher
			// 
			this.daClonesDataDispatcher.Item = this.ddClones;

		}

		
		#endregion // Component Designer generated code

		#region Initialize

		private void Initialize()
		{
			this.rowBeforeChangeHandler = new EventHandler( Row_BeforeChange );
			this.rowAfterChangeHandler = new EventHandler( Row_AfterChange);

			this.rows = new InternalRowCollection<TRow>();
			this.rows.AddRow += new RowEventHandler<TRow>( rows_AddRow );
			this.rows.Inserting += new CollectionChangeEventHandler<TRow>( rows_Inserting );
			this.rows.Inserted += new CollectionChangeEventHandler<TRow>( rows_Inserted );
			this.rows.Removing += new CollectionChangeEventHandler<TRow>( rows_Removing );
			this.rows.Removed += new CollectionChangeEventHandler<TRow>( rows_Removed );
			this.rows.Clearing += new CollectionEventHandler<TRow>( rows_Clearing );
			this.rows.Cleared += new CollectionEventHandler<TRow>( rows_Cleared );

			this.changingRows = new InternalRowCollection<TRow>();
			this.changingRows.Inserting += new CollectionChangeEventHandler<TRow>( changingRows_Inserting );
			this.changingRows.Inserted += new CollectionChangeEventHandler<TRow>( changingRows_Inserted );
			this.changingRows.Removing += new CollectionChangeEventHandler<TRow>( changingRows_Removing );
			this.changingRows.Removed += new CollectionChangeEventHandler<TRow>( changingRows_Removed );
			this.changingRows.Clearing += new CollectionEventHandler<TRow>( changingRows_Clearing );
			this.changingRows.Cleared += new CollectionEventHandler<TRow>( changingRows_Cleared );

			this.hashQueue = new Dictionary<DataRow, TRow>();
		}

		
		#endregion // Initialize

		#region Actions

		public TRow FindRow( int id )
		{
			if( this.rows.ContainsID( id ) )
			{
				return this.rows[id];
			}

			return null;
		}

		public TRow CloneRow( TRow Row )
		{
			Trace.Assert( Row.Table == this );

			TRow Result = this.NewRow();

			Result.DataRow.ItemArray = (object[])Row.DataRow.ItemArray.Clone();

			this.ddClones.Ambassadors.Add( Result );

			return Result;
		}

		#endregion // Actions

		#region ddMain events

		private void ddMain_BeforeSetData( object sender, EventArgs e )
		{
			Debug.Assert( this.Data != null );

			this.OnBeforeSetData();
		}
		private void ddMain_AfterSetData( object sender, EventArgs e )
		{
			Debug.Assert( this.Data != null );

			this.OnAfterSetData();
		}


		private void ddMain_DestroyView( object sender, EventArgs e )
		{
			this.OnDestroyView();
		}
		private void ddMain_BuildView( object sender, EventArgs e )
		{
			this.OnBuildView();
		}
		private void ddMain_UpdateView( object sender, EventArgs e )
		{
			this.OnUpdateView();			
		}


		#endregion // ddMain events

		#region rows events

		private void rows_AddRow( object sender, RowEventArgs<TRow> e )
		{
			Debug.Assert( e.Row.Free, "Строка принадлежит другой коллекции строк (таблице)" );

			Debug.Assert( this.DataTable != null, "Нет обертки "+this.GetType().FullName );

			this.hashQueue.Add( e.Row.DataRow, e.Row );

			this.DataTable.Rows.Add( e.Row.DataRow );

			if( e.Row.DataRow.HasErrors )
			{
				Trace.WriteLine( "Row {"+ e.Row.ID+"} was added in " + this.DataTable.TableName+ " abortively with error: " + e.Row.DataRow.RowError );

				this.DataTable.Rows.Find( e.Row.ID ).Delete();

				throw new ApplicationException( e.Row.DataRow.RowError );
			}
		}


		private void rows_Inserting( object sender, CollectionChangeEventArgs<TRow> e )
		{
			TRow Row = e.Item;

			this.ddRows.Ambassadors.Add( Row );
		}
		private void rows_Inserted( object sender, CollectionChangeEventArgs<TRow> e )
		{
			TRow Row = e.Item;

			Row.BeforeChange += this.rowBeforeChangeHandler;
			Row.AfterChange +=this.rowAfterChangeHandler;

			this.OnRowActionShow( Row );
		}

		private void rows_Removing( object sender, CollectionChangeEventArgs<TRow> e )
		{
			TRow Row = e.Item;

			this.OnRowActionHide( Row );

			Row.BeforeChange -= this.rowBeforeChangeHandler;
			Row.AfterChange -= this.rowAfterChangeHandler;
		}
		private void rows_Removed( object sender, CollectionChangeEventArgs<TRow> e )
		{
			TRow Row = e.Item;

			this.ddRows.Ambassadors.Remove( Row );
		}
		private void rows_Clearing( object sender, CollectionEventArgs<TRow> e )
		{
			foreach( TRow row in this.rows )
			{
				this.OnRowActionHide( row );

				row.BeforeChange -= this.rowBeforeChangeHandler;
				row.AfterChange -= this.rowAfterChangeHandler;
			}
		}
		private void rows_Cleared( object sender, CollectionEventArgs<TRow> e )
		{
			this.ddRows.Ambassadors.Clear();
		}

		
		#endregion // rows events

		#region Handle changingRows events

		private void changingRows_Inserting( object sender, CollectionChangeEventArgs<TRow> e )
		{
			TRow Row = e.Item;

			this.ddChangingRows.Ambassadors.Add( Row );
		}
		private void changingRows_Inserted( object sender, CollectionChangeEventArgs<TRow> e )
		{
			TRow Row = e.Item;

			Row.BeforeChange += this.rowBeforeChangeHandler;
			Row.AfterChange += this.rowAfterChangeHandler;
		}
		private void changingRows_Removing( object sender, CollectionChangeEventArgs<TRow> e )
		{
			TRow Row = e.Item;

			Row.BeforeChange -= this.rowBeforeChangeHandler;
			Row.AfterChange -= this.rowAfterChangeHandler;
		}
		private void changingRows_Removed( object sender, CollectionChangeEventArgs<TRow> e )
		{
			TRow Row = e.Item;

			this.ddChangingRows.Ambassadors.Remove( Row );
		}
		private void changingRows_Clearing( object sender, CollectionEventArgs<TRow> e )
		{
			foreach( TRow row in this.changingRows )
			{
				row.BeforeChange -= this.rowBeforeChangeHandler;
				row.AfterChange -= this.rowAfterChangeHandler;
			}
		}
		private void changingRows_Cleared( object sender, CollectionEventArgs<TRow> e )
		{
			this.ddChangingRows.Ambassadors.Clear();
		}

		#endregion // Handle changingRows events

		#region Row level

		public TRow NewRow()
		{
			if( this.DataTable != null )
			{
				DataRow DataRow = this.DataTable.NewRow();

				TRow Row = this.OnCreateRow( DataRow );

				return Row;
			}
			else
				throw new InvalidOperationException( "Table must be builded before call NewRow()" );
		}

		
		#endregion Row level

		#region Row events

		/* Как же быть?
		 * Мы не можем просто удалить обертку строки из коллекции 
		 * оберток строк (this.rows) обертки таблицы при изменении строки, 
		 * так как при этом будут оторваны местные обработчики событий 
		 * изменения строки и мы потеряем эту строку навсегда. (вместе с оберткой ;-)
		 *
		 * Но мы можем не просто удалить обертку из коллекции this.rows
		 * а поместить ее при этом в специальную коллекцию изменяемых строк.
		 */
		
		private void Row_BeforeChange( object sender, EventArgs e )
		{
			TRow Row = (TRow)sender;

			/* Вызов this.OnRowActionHide() не нужен здесь.
			 * this.rows.Remove() -> this.rows_Removing() -> this.OnRowActionHide()
			 */
//			this.OnRowActionHide( Row );

			this.rows.Remove( Row );
			this.changingRows.Add( Row );

		}
		private void Row_AfterChange( object sender, EventArgs e )
		{
			TRow Row = (TRow)sender;

			this.changingRows.Remove( Row );
			this.rows.Add( Row );

			/* Вызов this.OnRowActionShow() не нужен здесь.
			 * this.rows.Add() -> this.rows_Inserted() -> this.OnRowActionShow()
			 */
//			this.OnRowActionShow( (TRow)sender );
		}

//		MethodInfo rowCreator = typeof( TRow ).GetMethod( "Create" );

		protected abstract TRow OnCreateRow( DataRow dataRow );
//		{
//			return (TRow)this.rowCreator.Invoke( null, new object[] { dataRow } );			
//		}

		protected virtual void OnRowActionHide( TRow Row )
		{

		}
		protected virtual void OnRowActionShow( TRow Row )
		{

		}

		
		#endregion Row events

		#region Events

		private EventHandler m_BeforeBuild;
		private EventHandler m_AfterBuild;

		public event EventHandler BeforeBuild
		{
			add { m_BeforeBuild += value; }
			remove { m_BeforeBuild -= value; }
		}
		public event EventHandler AfterChange
		{
			add{ m_AfterBuild += value; }
			remove { m_AfterBuild -= value; }
		}


		protected virtual void OnBeforeBuild()
		{
			if( m_BeforeBuild != null )
				m_BeforeBuild( this, EventArgs.Empty );
		}
		protected virtual void OnAfterBuild()
		{
			if( m_AfterBuild != null )
				m_AfterBuild( this, EventArgs.Empty );
		}
		
		
	#endregion Events 
	}
	
	public class DataRowEventArgs: EventArgs
	{
		private System.Data.DataRow m_DataRow;
		public System.Data.DataRow DataRow { get { return m_DataRow; } }

		public DataRowEventArgs( System.Data.DataRow DataRow )
		{
			m_DataRow = DataRow;
		}
	}

	public interface IRowCollection<TRow>: ICollectionWithEvents<TRow>
		where TRow: Row
	{
		new TRow this[ int id ] { get;}

		int Count { get; }

		void Add( TRow Row );
	}


	public interface ITable
	{

	}
}