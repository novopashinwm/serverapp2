using System.Collections.Generic;
using System.Data;
using FORIS.TSS.Common;

namespace FORIS.TSS.ServerApplication.Helpers.Common
{
	public class InternalRowCollection<TRow>: CollectionWithEvents<TRow>, IRowCollection<TRow>
		where TRow: Row
	{
		#region IRowCollection members

		TRow IRowCollection<TRow>.this[ int ID ]
		{
			get { return this.hashByID[ ID ]; }
		}

		void IRowCollection<TRow>.Add( TRow Row )
		{
			this.OnAddRow( new RowEventArgs<TRow>( Row ) );
		}

		#endregion // IRowCollection members

		#region Indexer

		public new TRow this[ int id ]
		{
			get { return this.hashByID[id]; }
		}

		public TRow this[ DataRow dataRow ]
		{
			get { return this.hashByDataRows[dataRow]; }
		}

		#endregion // Indexer

		#region Components

		private readonly Dictionary<int, TRow> hashByID;
		private readonly Dictionary<DataRow, TRow> hashByDataRows;

		#endregion // Component

		#region Constructor 

		public InternalRowCollection()
		{
			this.hashByID = new Dictionary<int, TRow>();
			this.hashByDataRows = new Dictionary<DataRow, TRow>();
		}
		public InternalRowCollection( int capacity )
		{
			this.hashByID = new Dictionary<int, TRow>( capacity );
			this.hashByDataRows = new Dictionary<DataRow, TRow>();
		}

		#endregion // Constructor

		#region Manipulation methods

		public bool ContainsID( int ID )
		{
			return this.hashByID.ContainsKey( ID );
		}
		public bool ContainsDataRow( DataRow dataRow )
		{
			return this.hashByDataRows.ContainsKey( dataRow );
		}
		
		#endregion // Manipulation methods

		#region Maintain hash

		/* ��������, ��� DataRow ������ ����������� 
		 * ����������� � ��������� �������.
		 * 
		 * ������ ���������� �������� ����� (� �����) � �������
		 * ����������� ����� ������ ����� ���������� �������
		 * � ��������� ������ ����� ������� �������.
		 * ���������� ��� �� ����� ��������� ������� Inserting 
		 * ���� ���������. ������������ ������� base.OnInserting()
		 * 
		 * �������� ����� (� �����) � ������� ��������� ���� �� ���� ���
		 * ��� ����� ������� �� ���������� ������ ����� ������� �������.
		 * ���������� ��� �� ����� ��������� ������� Removed
		 * ���� ���������. ������������ ������� base.OnRemoved()
		 */

		protected override void OnInserting( CollectionChangeEventArgs<TRow> e )
		{
			base.OnInserting( e );

			TRow Row = e.Item;

			this.hashByID.Add( Row.ID, Row );
			this.hashByDataRows.Add( Row.DataRow, Row );
		}
		protected override void OnRemoved( CollectionChangeEventArgs<TRow> e ) 
		{
			TRow Row = e.Item;

			this.hashByID.Remove( Row.ID );
			this.hashByDataRows.Remove( Row.DataRow );

			base.OnRemoved( e );
		}
		protected override void OnCleared( CollectionEventArgs<TRow> e )
		{
			this.hashByID.Clear();
			this.hashByDataRows.Clear();

			base.OnCleared( e );
		}

		#endregion // Maintain hash

		#region Events

		private RowEventHandler<TRow> dlgtAddRow;

		public event RowEventHandler<TRow> AddRow
		{
			add { this.dlgtAddRow += value; }
			remove { this.dlgtAddRow -= value; }
		}


		protected virtual void OnAddRow( RowEventArgs<TRow> e )
		{
			if( this.dlgtAddRow != null )
			{
				this.dlgtAddRow( this, e );
			}
		}


		#endregion // Events
	}
}