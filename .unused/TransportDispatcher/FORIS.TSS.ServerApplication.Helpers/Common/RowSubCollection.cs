using FORIS.TSS.Common;

namespace FORIS.TSS.ServerApplication.Helpers.Common
{
	public interface IRowSubCollection<TRow>: ICollectionWithEvents<TRow>
		where TRow: Row
	{

	}

	public class RowSubCollection<TRow>: CollectionWithEvents<TRow>, IRowSubCollection<TRow>
		where TRow: Row
	{

	}
}