﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using FORIS.TSS.BusinessLogic.Interfaces.Data;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers;

namespace FORIS.TSS.ServerApplication.Helpers
{
	/// <summary>
	/// 
	/// </summary>
	/// <typeparam name="TParams"></typeparam>
	public abstract class DataBase<TParams> : Data, IDataSupplier<TParams>
		where TParams : DataParams
	{
		#region Params

		private readonly TParams @params;
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public TParams Params
		{
			get { return this.@params; }
		}


		#endregion // Params

		#region ReadOnly

		private readonly bool readOnly = false;
		[Browsable(false)]
		[DefaultValue(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool ReadOnly
		{
			get { return this.readOnly; }
		}


		#endregion // ReadOnly

		#region Constructor & Dispose

		/// <summary>
		/// Конструктор по умолчанию
		/// </summary>
		/// <returns></returns>
		/// <remarks>
		/// Так как в стороне клиента контейнеры данных являются компонентами форм,
		/// то определим только один конструктор по умолчанию, 
		/// и никаких других конструкторов использовать не будем
		/// </remarks>
		public DataBase(TParams @params)
		{
			this.@params = @params;

			this.MakeSchema();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.dlgtReadOnlyChanged = null;
				this.dlgtDataChanged = null;
			}

			base.Dispose(disposing);
		}

		#endregion // Constructor & Dispose

		#region IDataSupplier<TParams> Members

		TParams IDataSupplier<TParams>.Params
		{
			get { return this.Params; }
			set
			{
				throw new NotSupportedException();
			}
		}

		#region Event ParamsChanged

		event EventHandler IDataSupplier<TParams>.ParamsChanged
		{
			add { throw new NotSupportedException(); }
			remove { throw new NotSupportedException(); }
		}

		#endregion // Event ParamsChanged

		#endregion // IDataSupplier<TParams> Members

		#region Implement IDataSupplier

		DataSet IDataSupplier.GetData()
		{
			return this.dsDataSet;
		}

		void IDataSupplier.Tick()
		{
			Trace.WriteLine(
				String.Format("{0}.Tick()", this.GetType().FullName)
				);
		}

		#region DataChanged

		private AnonymousEventHandler<DataChangedEventArgs> dlgtDataChanged;

		event AnonymousEventHandler<DataChangedEventArgs> IDataSupplier.DataChanged
		{
			add { this.dlgtDataChanged += value; }
			remove { this.dlgtDataChanged -= value; }
		}

		protected virtual void OnDataChanged(DataChangedEventArgs e)
		{
			if (this.dlgtDataChanged != null)
			{
				Delegate[] Handlers = this.dlgtDataChanged.GetInvocationList();

				foreach (AnonymousEventHandler<DataChangedEventArgs> handler in Handlers)
				{
					handler.BeginInvoke(e, new AsyncCallback(DataChanged_Callback), handler);
				}
			}
		}

		private void DataChanged_Callback(IAsyncResult asyncResult)
		{
			AnonymousEventHandler<DataChangedEventArgs> Handler =
				(AnonymousEventHandler<DataChangedEventArgs>)asyncResult.AsyncState;

			try
			{
				Handler.EndInvoke(asyncResult);
			}
			catch (Exception)
			{
				this.dlgtDataChanged -= Handler;
			}
		}

		#endregion // DataChanged

		#region ReadOnlyChanged

		bool IDataSupplier<TParams>.IsReadOnly()
		{
			return this.readOnly;
		}

		private AnonymousEventHandler<EventArgs> dlgtReadOnlyChanged;

		event AnonymousEventHandler<EventArgs> IDataSupplier<TParams>.ReadOnlyChanged
		{
			add { this.dlgtReadOnlyChanged += value; }
			remove { this.dlgtReadOnlyChanged -= value; }
		}

		protected virtual void OnReadOnlyChanged()
		{
			if (this.dlgtReadOnlyChanged != null)
			{
				Delegate[] Handlers = this.dlgtReadOnlyChanged.GetInvocationList();

				foreach (AnonymousEventHandler<EventArgs> handler in Handlers)
				{
					handler.BeginInvoke(EventArgs.Empty, new AsyncCallback(ReadOnlyChanged_Callback), handler);
				}
			}
		}

		private void ReadOnlyChanged_Callback(IAsyncResult asyncResult)
		{
			AnonymousEventHandler<EventArgs> Handler =
				(AnonymousEventHandler<EventArgs>)asyncResult.AsyncState;

			try
			{
				Handler.EndInvoke(asyncResult);
			}
			catch (Exception)
			{
				this.dlgtReadOnlyChanged -= Handler;
			}
		}

		#endregion // ReadOnlyChanged

		#endregion // Implement IDataSupplier

		private void MakeSchema()
		{
			this.OnMakeSchema(this.dsDataSet);
		}

		protected virtual void OnMakeSchema(DataSet dataSet)
		{
			throw new NotImplementedException(
				"Override to build schema before load data. Don't call base"
				);
		}

		private readonly object loadDataLockObject = new object();

		/// <summary>
		/// Загружает данные.
		/// Не переводит выполнение в другой поток.
		/// </summary>
		public void LoadData(IDatabaseDataSupplier databaseDataSupplier)
		{
			lock (this.loadDataLockObject)
			{
				this.ddMain.Data = null;

				DataSet LoadedData = this.OnLoadData(databaseDataSupplier);

				this.dsDataSet.Clear();
				this.dsDataSet.Merge(LoadedData, true, MissingSchemaAction.Error);

				this.ddMain.Data = this;
			}
		}

		protected virtual DataSet OnLoadData(IDatabaseDataSupplier databaseDataSupplier)
		{
			throw new NotImplementedException();
		}

		private DataSet deleted = null;
		private DataSet added = null;
		private DataSet modified = null;

		public override void AcceptChanges(IPersonalServer session)
		{
			Trace.Assert(this.deleted == null && this.added == null && this.modified == null);

			DataSet Changes = this.dsDataSet.GetChanges();

			if (Changes != null)
			{
				#region Cut relations & foreign key constraints

				/* Целесообразно снести схему с набора данных изменений сразу.
				 * И после этого производить отдельную упаковку 
				 * удаленных, измененных, и добавленных записей.
				 */

				/* DataTable.Clone() 
				 * Clones the structure of the DataTable, 
				 * including all DataTable schemas and constraints. 
				 */

				Changes.Relations.Clear();

				foreach (DataTable table in Changes.Tables)
				{
					List<ForeignKeyConstraint> ForeignKeys =
						new List<ForeignKeyConstraint>(table.Constraints.Count);

					foreach (Constraint constraint in table.Constraints)
					{
						if (constraint is ForeignKeyConstraint)
						{
							ForeignKeys.Add((ForeignKeyConstraint)constraint);
						}
					}

					foreach (ForeignKeyConstraint constraint in ForeignKeys)
					{
						table.Constraints.Remove(constraint);
					}
				}

				#endregion // Cut relations & foreign key constraints

				#region Build deleted

				/* В базе данных могут быть записи в других таблицах, 
				 * связанные с удаляемой записью внешним ключом.
				 * Если эти записи тоже находятся в этом же наборе и 
				 * удалены, то не возникает проблем с удалением данных,
				 * в противном случае будет ошибка сохранения данных в БД.
				 * 
				 * Более того в этом же наборе данных могут быть такие связанные по 
				 * внешнему ключу записи, которые необходимо изменить (чтоб перенаправить 
				 * связь внешнего ключа на другую запись таблицы, может быть даже 
				 * добавляемую запись) прежде чем проводить удаление данных.
				 * То есть в наборе данных все согласовано, однако при "кусочном"
				 * удалить-вставить-изменить могут наблюдаться косяки. 
				 * При внесении изменений в пределах одной таблицы такой порядок вполне
				 * оправдан, чтоб не было конфликтов при вставке записей с существующими
				 * значениями ключа некоторого уникального индекса.
				 * 
				 * По пунктам/случаям. Берем первую таблицу. Сначала получаем записи, 
				 * которые необходимо удалить. Смотрим на запись. Если запись имеет 
				 * дочерние в оригинальной версии (которые должны быть изменены
				 */

				/* Прямо сразу можно собрать идентификаторы удаляемых записей
				 * Довольно сложно колдовать с удаленными записями.
				 * Датасет содержит удаленные записи, те которые реально удалены, 
				 * а также, те которые связаны с удаленными отношениями. 
				 * По идее передавать связанные записи по сети - избыточно и не нужно,
				 * однако нет пути преобразовать датасет так, чтоб он содержал только
				 * удаленные записи. Можно однако создать новый датасет 
				 * с хитрой схемой данных в таблицах которой создать только ключи.
				 * И насоздавать там записей с ключами удаленных записей. Это
				 * будет самый экономичный вариант.
				 */

				this.deleted = new DataSet();

				DataSet DeletedChanges = Changes.GetChanges(DataRowState.Deleted);

				if (DeletedChanges != null)
				{
					foreach (DataTable table in DeletedChanges.Tables)
					{
						DataTable Table = new DataTable(table.TableName);

						#region Create columns for PrimaryKey only

						foreach (DataColumn column in table.PrimaryKey)
						{
							Table.Columns.Add(
								new DataColumn(
									column.ColumnName,
									column.DataType
									)
								);
						}

						#endregion // Create columns for PrimaryKey only

						#region Define primary key

						DataColumn[] Key = new DataColumn[Table.Columns.Count];

						Table.Columns.CopyTo(Key, 0);

						Table.PrimaryKey = Key;

						#endregion // Define primary key

						#region Copy rows (key only)

						foreach (DataRow row in table.Rows)
							if (row.RowState == DataRowState.Deleted)
							{
								DataRow Row = Table.NewRow();

								foreach (DataColumn column in table.PrimaryKey)
								{
									Row[column.ColumnName] = row[column, DataRowVersion.Original];
								}

								Table.Rows.Add(Row);
							}

						#endregion // Copy rows (key only)

						if (Table.Rows.Count > 0) this.deleted.Tables.Add(Table);
					}

					this.deleted.AcceptChanges();
				}

				/* Вот эту штуку уже вполне целесообразно передавать по сетям
					 */

				#endregion // Deleted

				#region Build added

				/* При синхронизации добавленных записей нет необходимости передавать
				 * на сторону клиента отрицательные значения новых ключей добавленных 
				 * записей. Так как в клиентском наборе данных таких значений ключей 
				 * нет вовсе.
				 */

				this.added = new DataSet();

				DataSet AddedChanges = Changes.GetChanges(DataRowState.Added);

				if (AddedChanges != null)
				{
					foreach (DataTable table in AddedChanges.Tables)
					{
						DataTable Table = table.Clone();

						#region Copy rows

						foreach (DataRow row in table.Rows)
							if (row.RowState == DataRowState.Added)
							{
								DataRow Row = Table.NewRow();

								Row.ItemArray = (object[])row.ItemArray.Clone();

								Table.Rows.Add(Row);
							}

						#endregion // Copy rows

						if (Table.Rows.Count > 0) this.added.Tables.Add(Table);
					}

					this.added.AcceptChanges();
				}

				#endregion // Build added

				#region Modified

				/* Модифицированные записи нужно передавать целиком, так как 
				 * мы совсем не знаем, что именно в этих записях изменено.
				 * Ну и конечно же нет никакой необходимости передавать 
				 * связанные записи. 
				 */

				this.modified = new DataSet();

				DataSet ModifiedChanges = Changes.GetChanges(DataRowState.Modified);

				if (ModifiedChanges != null)
				{
					foreach (DataTable table in ModifiedChanges.Tables)
					{
						DataTable Table = table.Clone();

						#region Copy rows

						foreach (DataRow row in table.Rows)
							if (row.RowState == DataRowState.Modified)
							{
								DataRow Row = Table.NewRow();

								Row.ItemArray = (object[])row.ItemArray.Clone();

								Table.Rows.Add(Row);
							}

						#endregion // Copy rows

						if (Table.Rows.Count > 0) this.modified.Tables.Add(Table);
					}

					this.modified.AcceptChanges();
				}

				#endregion // Modified

				/* Вообще можно собрать и отрицательные идентификаторы 
				 * добавляемых записей, ну если только для того, чтоб потом 
				 * проконтроллировать, что все-все добавляемые записи
				 * имеют реальные, полученные из БД идентификаторы.
				 */

				foreach (DataTable table in Changes.Tables)
				{
					table.RowChanging += new DataRowChangeEventHandler(table_RowChanging);
				}

				/* При вызове этого метода все новые значения ключей будут получены. 
				 * Однако и Added и Modified записи меняют состояние на Unmodified.
				 * И как же узнать после этого как их синхронизировать?
				 * А никак не узнавать. Если запись есть то изменить ее на новые 
				 * значения полей, а если нет, то добавить новую с сответствующими
				 * значениями полей. А вот удаленные записи 
				 */

				session.Update(Changes);

				foreach (DataTable table in Changes.Tables)
				{
					table.RowChanging -= new DataRowChangeEventHandler(table_RowChanging);
				}

				Changes.AcceptChanges();
			}

			this.dsDataSet.AcceptChanges();

			this.OnDataChanged(new DataChangedEventArgs(this.deleted, this.added, this.modified));

			this.deleted = null;
			this.added = null;
			this.modified = null;
		}

		private void table_RowChanging(object sender, DataRowChangeEventArgs e)
		{
			Trace.Assert(this.added != null);

			if (e.Action == DataRowAction.Change &&
				e.Row.RowState == DataRowState.Added
				)
			{
				#region key

				object[] key = new object[e.Row.Table.PrimaryKey.Length];

				for (int i = 0; i < e.Row.Table.PrimaryKey.Length; i++)
				{
					key[i] = e.Row[e.Row.Table.PrimaryKey[i], DataRowVersion.Current];
				}

				#endregion // key

				DataRow RowAdded = this.added.Tables[e.Row.Table.TableName].Rows.Find(key);

				RowAdded.ItemArray = (object[])e.Row.ItemArray.Clone();

				/* Устанавливаем новые значения ключей (полученные из БД)
				 * для добавленных строк. И должны обновиться значения
				 * в полях внешних ключей других таблиц.
				 * 
				 * Так же необходимо вызвать BeginEdit() и EndEdit() для обертки строки
				 * иначе изменения значений полей (ключа) не приводят к изменению положения
				 * строки в контейнере данных, он все еще находится под отрицательным ключем
				 * 
				 * Также необходимо отследить как меняются значения внешних ключей
				 * в дочерних записях.
				 * 
				 * А вообще конечно имеет смысл сделать правильную реакцию
				 * на события DataTable об изменении строки в Table
				 */



				DataRow RowData = this.dsDataSet.Tables[e.Row.Table.TableName].Rows.Find(key);

				RowData.ItemArray = (object[])e.Row.ItemArray.Clone();
			}
		}

		public override void RejectChanges()
		{
			this.dsDataSet.RejectChanges();
		}
	}
}