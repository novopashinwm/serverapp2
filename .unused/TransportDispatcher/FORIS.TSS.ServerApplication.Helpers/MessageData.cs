using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Data;
using FORIS.TSS.Helpers;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Interfaces.Data;
using FORIS.TSS.ServerApplication.Helpers;
using DataParams = FORIS.TSS.BusinessLogic.Interfaces.Data.DataParams;
using FORIS.TSS.BusinessLogic.Interfaces.Data.Message;


namespace FORIS.TSS.ServerApplication.Helpers
{
	/// <summary>
	/// Summary description for RuleData.
	/// </summary>
    public class MessageData : DataBase<DataParams>, IMessageDataServerSupplier
	{
		#region Properties

		private DateTime dateCurrent = System.Windows.Forms.DateTimePicker.MinDateTime;
		[ Browsable(false) ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden) ]
		public DateTime DateCurrent
		{
            get { return dateCurrent; }
			set
			{
                if (dateCurrent != value)
				{
					this.OnDateCurrentBeforeChange();

                    dateCurrent = value;

					this.OnDateCurrentAfterChange();
				}
			}
		}

		#endregion Properties


		#region Controls & Components

		private System.ComponentModel.IContainer components;

		#endregion Controls & Components


		#region Constructor & Dispose

		public MessageData() : base( DataParams.Empty )
		{
			InitializeComponent();
		}

        public MessageData(IContainer container) : this()
		{
			container.Add( this );
		}

		protected override void Dispose(bool disposing)
		{
			if( disposing )
			{
				if( components != null )
				{
					components.Dispose();
				}
			}

			base.Dispose (disposing);
		}

		#endregion Constructor & Dispose


		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			((System.ComponentModel.ISupportInitialize)(this.DataSet)).BeginInit();
			// 
			// DataSet
			// 
			this.DataSet.DataSetName = "Data";
			this.DataSet.Locale = new System.Globalization.CultureInfo("ru-RU");

			((System.ComponentModel.ISupportInitialize)(this.DataSet)).EndInit();
		}
		
		#endregion Component Designer generated code


		#region IRuleDataServerSupplier Members
		#endregion IRuleDataServerSupplier Members


		#region Schema

        protected override DataSet OnLoadData(IDatabaseDataSupplier databaseDataSupplier)
        {
            // this.vehicleData.Supplier = Server.Instance().GetDepartmentDataSupplier();

            throw new NotImplementedException();

            DataSet ds =
                databaseDataSupplier.GetDataFromDB(
                    new ParamValue[] { },
                    "dbo.GetDataForMessage",
                    FORIS.TSS.BusinessLogic.Interfaces.Data.Tables.GetTableNameForMessage
                    );

            return ds;
        }

		protected override void OnMakeSchema( DataSet dataSet )
		{
			List<string> TableNames = new List<string>();

			foreach( string tableName in FORIS.TSS.BusinessLogic.Interfaces.Data.Tables.GetDataForRuleNew )
			{
				TableNames.Add( tableName );
			}

			DataSet SeedDataSet = new DataSet();

			foreach( string tableName in TableNames )
			{
				SeedDataSet.Tables.Add( tableName );
			}

			DatasetHelper.Merge( dataSet, SeedDataSet );
		}

		#endregion Schema


		#region Events

		private EventHandler m_DateCurrentBeforeChange;
		private EventHandler m_DateCurrentAfterChange;

		public event EventHandler DateCurrentBeforeChange
		{
			add { m_DateCurrentBeforeChange += value; }
			remove { m_DateCurrentBeforeChange -= value; }
		}
		public event EventHandler DateCurrentAfterChange
		{
			add { m_DateCurrentAfterChange += value; }
			remove { m_DateCurrentAfterChange -= value; }
		}

		protected virtual void OnDateCurrentBeforeChange()
		{
			if( m_DateCurrentBeforeChange != null )
				m_DateCurrentBeforeChange( this, EventArgs.Empty );
		}
		protected virtual void OnDateCurrentAfterChange()
		{
			if( m_DateCurrentAfterChange != null )
				m_DateCurrentAfterChange( this, EventArgs.Empty );
		}

		#endregion Events
    }
}
