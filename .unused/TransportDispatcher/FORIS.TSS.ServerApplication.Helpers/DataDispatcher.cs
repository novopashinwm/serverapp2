using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using FORIS.TSS.ServerApplication.Helpers;

using FORIS.TSS.Common;

namespace FORIS.TSS.ServerApplication.Helpers
{
	/// <summary>
	/// Summary description for DataDispatcher.
	/// </summary>
	[ ToolboxItem(true) ]
	[ ToolboxBitmap( typeof(DataDispatcher) ) ]
	public class DataDispatcher: Dispatcher<IDataItem>, IDataItem
	{
		#region Public properties

		private Data data = null;
		[ Browsable(false) ]
		[ DefaultValue(null)]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden) ]
		public Data Data
		{
			get { return this.data; }
			set
			{
				if( this.data != value )
				{
					this.OnBeforeChange();

					if( value != null )
					{
						this.data = value;
						
						for( int i = 0; i < this.Ambassadors.Count; i++ )
						{
							Ambassador<IDataItem> ambassador = this.GetAmbassadorCollection()[ i ];
                            
							if( ambassador.Item != null && ambassador.Enabled )
								ambassador.Item.Data = value;
						}
					}
					else
					{
						for( int i = this.Ambassadors.Count-1; i >= 0; i-- )
						{
							Ambassador<IDataItem> ambassador = this.GetAmbassadorCollection()[ i ];

							if( ambassador.Item != null && ambassador.Enabled )
								ambassador.Item.Data = value;
						}

						this.data = value;
					}
				
					this.OnAfterChange();

					/* ������ ��������, ��� ��������� Ambassadors 
					 * ��������� ����� ��������� OnAfterChange()
					 */
				}
			}
		}

		#endregion Public properties

		#region Ambassadors

		private readonly DataAmbassadorCollection ambassadors = new DataAmbassadorCollection();
		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public DataAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<IDataItem> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

		#endregion // Ambassadors

		#region Constructor & Dispose

		public DataDispatcher():base()
		{

		}
		public DataDispatcher( IContainer container ): base()
		{
			container.Add( this );
		}
		protected override void Dispose(bool disposing)
		{
			if( disposing )
			{
				this.Data = null;

				this.delegateBeforeSetData = null;
				this.delegateAfterSetData = null;

				this.delegateDestroyView = null;
				this.delegateBuildView = null;
				this.delegateUpdateView = null;
			}

			base.Dispose (disposing);
		}


		#endregion // Constructor & Dispose

		protected override void OnBeforeSetItem( Ambassador<IDataItem> Ambassador )
		{
			if( Ambassador.Item != null )
			{
				Ambassador.Item.Data = null;
			}
		}
		protected override void OnAfterSetItem( Ambassador<IDataItem> Ambassador )
		{
			if( Ambassador.Item != null )
			{
				Ambassador.Item.Data = this.data;
			}
		}
	
		#region Events

		private EventHandler delegateBeforeSetData;
		private EventHandler delegateAfterSetData;

		private EventHandler delegateDestroyView;
		private EventHandler delegateBuildView;
		private EventHandler delegateUpdateView;

		public event EventHandler BeforeSetData
		{
			add { delegateBeforeSetData += value; }
			remove { delegateBeforeSetData -= value; }
		}
		public event EventHandler AfterSetData
		{
			add { delegateAfterSetData += value; }
			remove { delegateAfterSetData -= value; }
		}
		

		public event EventHandler DestroyView
		{
			add { delegateDestroyView += value; }
			remove { delegateDestroyView -= value; }
		}
		public event EventHandler BuildView
		{
			add { delegateBuildView += value; }
			remove { delegateBuildView -= value; }
		}
		public event EventHandler UpdateView
		{
			add { delegateUpdateView += value; }
			remove { delegateUpdateView -= value; }
		}

		
		private EventHandler m_BeforeAction;
		private EventHandler m_AfterAction;

		[ Category("Actions") ]
		public event EventHandler BeforeAction
		{
			add { m_BeforeAction += value; }
			remove { m_BeforeAction -= value; }
		}
		[ Category("Actions") ]
		public event EventHandler AfterAction
		{
			add { m_AfterAction += value; }
			remove { m_AfterAction -= value; }
		}

		
		protected virtual void OnBeforeAction( EventArgs e )
		{
			if( m_BeforeAction != null )
				m_BeforeAction( this, e );

//			foreach( DataAmbassador ambassador in this.Ambassadors )
//				if( ambassador.Item != null )
//				{
//					ambassador.Item.BeforeAction();
//				}
		}
		protected virtual void OnAfterAction( EventArgs e )
		{
//			foreach( DataAmbassador ambassador in this.Ambassadors )
//				if( ambassador.Item != null )
//				{
//					ambassador.Item.AfterAction();
//				}

			if( m_AfterAction != null )
				m_AfterAction( this, e );
		}


		protected override void OnBeforeChange()
		{
			if( this.data != null )
			{
				try
				{
					if( delegateBeforeSetData != null )
						delegateBeforeSetData( this, EventArgs.Empty );

					if( delegateDestroyView != null )
						delegateDestroyView( this, EventArgs.Empty );
				}
				catch( Exception Ex )
				{
					Debug.WriteLine( "DataDispatcher.OnBeforeChange() Exception: " + Ex.Message );
					Debug.WriteLine( Ex.StackTrace );
				}
			}

			base.OnBeforeChange ();
		}
		protected override void OnAfterChange()
		{
			base.OnAfterChange ();

			if( this.data != null )
			{
				try
				{
					if( delegateBuildView != null )
						delegateBuildView( this, EventArgs.Empty );

					if( delegateUpdateView != null )
						delegateUpdateView( this, EventArgs.Empty );

					if( delegateAfterSetData != null )
						delegateAfterSetData( this, EventArgs.Empty );
				}
				catch( Exception Ex )
				{
					Debug.WriteLine( "DataDispatcher.OnAfterChange() Exception: " + Ex.Message );
					Debug.WriteLine( Ex.StackTrace );
				}
			}
		}


		#endregion Events
	}

	public class DataAmbassadorCollection: AmbassadorCollection<IDataItem>
	{
		public new DataAmbassador this[ int Index ]
		{
			get { return (DataAmbassador)base[ Index ]; }
		}
	}

	[DesignTimeVisible( false )]
	public class DataAmbassador: Ambassador<IDataItem>
	{

	}
}