using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Interfaces.Data.Calendar;
using FORIS.TSS.Helpers;
using FORIS.TSS.ServerApplication.Helpers;
using DataParams=FORIS.TSS.BusinessLogic.Interfaces.Data.DataParams;

namespace FORIS.TSS.ServerApplication.Helpers
{
	/// <summary>
	/// Summary description for CalendarData.
	/// </summary>
	public class CalendarData: DataBase<DataParams>, ICalendarDataServerSupplier
	{
		#region Properties

		private DateTime m_DateCurrent = DateTimePicker.MinDateTime;
		[ Browsable(false) ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden) ]
		public DateTime DateCurrent
		{
			get { return m_DateCurrent; }
			set
			{
				if( m_DateCurrent != value )
				{
					this.OnDateCurrentBeforeChange();

					m_DateCurrent = value;

					this.OnDateCurrentAfterChange();
				}
			}
		}

		#endregion // Properties

		#region Controls & Components

		private System.ComponentModel.IContainer components;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public CalendarData( IContainer container )
			: base( DataParams.Empty )
		{
			container.Add( this );

			InitializeComponent();
		}

		public CalendarData()
			: base( DataParams.Empty )
		{
			InitializeComponent();
		}

		protected override void Dispose(bool disposing)
		{
			if( disposing )
			{
				if( components != null )
				{
					components.Dispose();
				}
			}
			base.Dispose (disposing);
		}

		#endregion Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			((System.ComponentModel.ISupportInitialize)(this.DataSet)).BeginInit();
			// 
			// DataSet
			// 
			this.DataSet.DataSetName = "Data";
			this.DataSet.Locale = new System.Globalization.CultureInfo("ru-RU");

			((System.ComponentModel.ISupportInitialize)(this.DataSet)).EndInit();
		}
		
		#endregion // Component Designer generated code

		#region Schema

		protected override void OnMakeSchema( DataSet dataSet )
		{
			List<string> TableNames = new List<string>();

			foreach( string tableName in FORIS.TSS.BusinessLogic.Interfaces.Data.Tables.GetDataForCalendar )
			{
				TableNames.Add( tableName );
			}


			DataSet SeedDataSet = new DataSet();

			foreach( string tableName in TableNames )
			{
				SeedDataSet.Tables.Add( tableName );
			}

			DatasetHelper.Merge( dataSet, SeedDataSet );
		}

		#endregion // Schema

		protected override DataSet OnLoadData( IDatabaseDataSupplier databaseDataSupplier )
		{
			DataSet DataSetCalendar =
				databaseDataSupplier.GetDataFromDB(
					new ParamValue[] { },
					"dbo.GetDataForCalendar",
					FORIS.TSS.BusinessLogic.Interfaces.Data.Tables.GetDataForCalendar
					);

			return DataSetCalendar;
		}

		#region Events

		private EventHandler m_DateCurrentBeforeChange;
		private EventHandler m_DateCurrentAfterChange;

		public event EventHandler DateCurrentBeforeChange
		{
			add { m_DateCurrentBeforeChange += value; }
			remove { m_DateCurrentBeforeChange -= value; }
		}
		public event EventHandler DateCurrentAfterChange
		{
			add { m_DateCurrentAfterChange += value; }
			remove { m_DateCurrentAfterChange -= value; }
		}

		protected virtual void OnDateCurrentBeforeChange()
		{
			if( m_DateCurrentBeforeChange != null )
				m_DateCurrentBeforeChange( this, EventArgs.Empty );
		}
		protected virtual void OnDateCurrentAfterChange()
		{
			if( m_DateCurrentAfterChange != null )
				m_DateCurrentAfterChange( this, EventArgs.Empty );
		}

		#endregion Events
	}
}
