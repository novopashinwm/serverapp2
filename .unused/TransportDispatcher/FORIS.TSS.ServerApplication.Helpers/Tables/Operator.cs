using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using FORIS.TSS.ServerApplication.Helpers.Common;



namespace FORIS.TSS.ServerApplication.Helpers.Tables
{
	#region public class OperatorTable : Table
    public class OperatorTable : Table<OperatorRow>
	{
		#region Properties

		public override string Name { get { return "OPERATOR"; } }

		private OperatorRow rowDefault;
		public OperatorRow RowDefault
		{
			get { return this.rowDefault; }
		}

		#endregion Properties


		#region View

        protected override void OnBuildView()
        {
            base.OnBuildView();

            if (this.DataTable != null)
            {
                this.rowDefault = (OperatorRow)this.NewRow();
            }
        }
        
        protected override void OnDestroyView()
		{
			this.rowDefault = null;

			base.OnDestroyView ();
		}

		#endregion View


		#region Constructor & Dispose

		public OperatorTable(): base() {}

		public OperatorTable( IContainer Container)//: base( Container as DataContainer )
		{
			Container.Add( this );
		}

		#endregion Constructor & Dispose

			
		#region Row level

        protected override OperatorRow OnCreateRow(DataRow dataRow)
        {
            return new OperatorRow(dataRow);
        } 

        public OperatorRow FindRow(int IdOperator)
        {
            return this.Rows[IdOperator] as OperatorRow;
        }

		#endregion Row level
	}
	#endregion public class OperatorTable : Table


	#region public class OperatorRow : Row

	public class OperatorRow : Row
	{

        #region properties

        private string m_Name;
        public string Name
		{
			get
			{
				return this.m_Name;
			}
			set
			{
				this.DataRow["NAME"] = value;
			}
        }

        private string m_Login;
        public string Login
        {
            get
            {
                return this.m_Login;
            }
            set
            {
                this.DataRow["LOGIN"] = value;
            }
        }

        private string m_Password;
        public string Password
        {
            get
            {
                return this.m_Password;
            }
            set
            {
                this.DataRow["PASSWORD"] = value;
            }
        }

        private string m_Phone;
        public string Phone
        {
            get
            {
                return this.m_Phone;
            }
            set
            {
                this.DataRow["PHONE"] = value;
            }
        }

        private string m_Email;
        public string Email
        {
            get
            {
                return this.m_Email;
            }
            set
            {
                this.DataRow["EMAIL"] = value;
            }
        }

		#endregion properties


        #region ParentRows
        #endregion ParentRows


        #region ChildRows
        #endregion ChildRows


        #region ID

        private int m_id;
        protected override int GetID()
        {
            return m_id;
        }

		#endregion ID


		#region Constructor

		public OperatorRow (DataRow DataRow) : base(DataRow)
		{
		}

		#endregion Constructor
			

		#region Build

		protected override void OnBuildFields()
		{
            this.m_id = (int)this.DataRow["OPERATOR_ID"];
            this.m_Name = this.DataRow["NAME"].ToString();
            this.m_Login = this.DataRow["LOGIN"].ToString();
            this.m_Password = this.DataRow["PASSWORD"].ToString();

            if (this.DataRow["PHONE"] != DBNull.Value)
                this.m_Phone = this.DataRow["PHONE"].ToString();
            else
                this.m_Phone = "";

            if (this.DataRow["EMAIL"] != DBNull.Value)
                this.m_Email = this.DataRow["EMAIL"].ToString();
            else
                this.m_Email = "";
        }

		#endregion Build


        #region Compare

        protected override int CompareTo(Row Row)
        {
            Debug.Assert(Row is OperatorRow);
            OperatorRow RowOperator = (OperatorRow)Row;

            int Result = this.m_Name.CompareTo(RowOperator.m_Name);

            if (Result != 0)
                return Result;

            return base.CompareTo(Row);
        }

        #endregion Compare


        public override string ToString()
        {
            return "{ ID=" + this.ID + " Text=" + this.m_Name + " }";
        }

	}

	#endregion public class OperatorRow : Row

}
