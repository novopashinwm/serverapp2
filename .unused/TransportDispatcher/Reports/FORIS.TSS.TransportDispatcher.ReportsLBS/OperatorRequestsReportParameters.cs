﻿using System;
using System.ComponentModel;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Attributes;
using DisplayNameAttribute = FORIS.TSS.BusinessLogic.Attributes.DisplayNameAttribute;

namespace FORIS.TSS.TransportDispatcher.Reports
{
    [Serializable]
    public class OperatorRequestsReportParameters : ReportParameters
    {
        [Browsable(false)]
        public int OperatorID { get; set; }

        [DisplayName("DateTimeInterval"), Type(typeof(DateTimeInterval))]
        [ControlType("DateTimeFromToPicker", "ReportPeriod"), Options("Accuracy:0"), Order(255)]
        public override DateTimeInterval DateTimeInterval
        {
            get
            {
                return base.DateTimeInterval;
            }
            set
            {
                base.DateTimeInterval = value;
            }
        }

    }
}
