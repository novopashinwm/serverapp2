﻿using System.Globalization;
using System.Runtime.InteropServices;
using FORIS.TSS.BusinessLogic.Interfaces.Data;
using FORIS.TSS.TransportDispatcher.Reports;

//TODO: устранить copy-paste совместно с OperatorRequestsTssReport.cs
//TODO: возможно, вынести тип отчета в параметр

namespace FORIS.TSS.TransportDispatcher.ReportsLBS.CommunityRequests
{
    /// <summary> Отчет "Запросы в сообществе" </summary>
    [Guid("D2299B07-03BA-4c17-99BB-7C51F0CDEC3B")]
    public class CommunityRequestsTssReport : MlpRequestsReportBase
    {
        public override int Order
        {
            get { return 2; }
        }

        public override string ReportNameGet(CultureInfo culture)
        {
            var @string = GetStrings(culture);
            return @string["CommunityRequests"];
        }

        public override string GetDescription(CultureInfo culture)
        {
            return GetStrings(culture)["CommunityRequestsDescription"];
        }

        protected override OperatorRequestsReportResultTuple[] GetReportData(OperatorRequestsReportParameters reportParameters)
        {
            return mclsInstance.GetCommunityMlpRequests(
                reportParameters.OperatorID,
                reportParameters.DepartmentId,
                reportParameters.DateTimeInterval.DateFrom,
                reportParameters.DateTimeInterval.DateTo,
                reportParameters.MapGuid,
                reportParameters.Culture, 
                reportParameters.TimeZoneInfo);
        }
    }
}