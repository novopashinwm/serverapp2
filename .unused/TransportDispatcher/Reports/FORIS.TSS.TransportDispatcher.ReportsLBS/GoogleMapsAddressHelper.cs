﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Web;
using System.Xml;
using System.IO;
using System.Globalization;
using FORIS.TSS.Config;

namespace FORIS.TSS.ServerApplication.Geo.Address
{
    internal class GoogleMapsAddressHelper
    {
        internal static WebProxy myProxy;
        internal static string googleGetAddressRequest= string.Empty;

        static GoogleMapsAddressHelper()
        {
            myProxy= WebProxy.GetDefaultProxy();
            myProxy.UseDefaultCredentials=true;

            string key = Config.Globals.AppSettings["GoogleMapsKey"]; // = "client=gme-sitronics"; //"key=ABQIAAAAz_EEX3nR0d4QcXaAfCMaYhRoEYH6FNtghCrteCbMYfrsKZdZyRS3Rxc2LEFz6j0b5OBXwKCivxjPQQ";
            if (key != null)
            {
                googleGetAddressRequest = @"http://maps.google.com/maps/geo?ll={0},{1}&gl=ru&sensor=false&output=xml&" + key;
            }
        }

        public static bool IsConfigured
        {
            get
            {
                return !string.IsNullOrEmpty(googleGetAddressRequest);
            }
        }

        public static string GetAddressByPoint(double lat, double lng)
        {
            //T
            string res = "";

            string sUrl = string.Format(googleGetAddressRequest, lat.ToString(CultureInfo.InvariantCulture), lng.ToString(CultureInfo.InvariantCulture));

            try
            {
                XmlDocument doc = new XmlDocument();

                HttpWebRequest loHttp = (HttpWebRequest)WebRequest.CreateDefault(new Uri(sUrl));
                loHttp.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)";
                loHttp.Timeout = 20000;     // 20 secs
                //loHttp.Referer = "http://tss.sitels.ru";
                loHttp.Headers.Add("Accept-Language", "ru-ru");
                loHttp.UseDefaultCredentials = true;
                loHttp.Method = "GET";
                loHttp.KeepAlive = false;
                loHttp.Proxy = myProxy;
                HttpWebResponse loWebResponse = (HttpWebResponse)loHttp.GetResponse(); //GetProxySettings(loHttp); //

                Encoding enc = Encoding.UTF8;
                StreamReader loResponseStream = new StreamReader(loWebResponse.GetResponseStream(), enc);

                doc.Load(loResponseStream);

                loWebResponse.Close();
                loResponseStream.Close();
                XmlNamespaceManager nsmng = new XmlNamespaceManager(doc.NameTable);
                nsmng.AddNamespace("x", @"http://earth.google.com/kml/2.0");
                nsmng.AddNamespace("a", @"urn:oasis:names:tc:ciq:xsdschema:xAL:2.0");
                XmlNodeList list = doc.SelectNodes("//x:address", nsmng);
                if (list.Count > 0)
                {
                    res = list[0].InnerText;
                }
                list = doc.SelectNodes("//a:CountryName", nsmng);
                if (list.Count > 0)
                {
                    res = res.Replace(list[0].InnerText + ", ", "");
                }
                list = doc.SelectNodes("//a:PostalCodeNumber", nsmng);
                if (list.Count > 0)
                {
                    res = res.Replace(list[0].InnerText + ", ", "");
                }

                list = doc.SelectNodes("//a:DependentLocalityName", nsmng);
                if (list.Count > 0)
                {
                    res += ", " + list[0].InnerText;
                }

                list = doc.SelectNodes("//a:AddressLine", nsmng);
                if (list.Count > 0)
                {
                    string addAddr = list[0].InnerText;

                    list = doc.SelectNodes("//a:SubAdministrativeAreaName", nsmng);
                    if (list.Count > 0)
                    {
                        addAddr += ", " + list[0].InnerText;
                    }
                    res = res + " (" + addAddr + ")";
                }

                res = res.Replace("город ", "");
                res = res.Replace("муниципальный ", "");
                res = res.Replace("городское поселение", @"г/п");
                res = res.Replace("сельское поселение", @"с/п");


                return res;
            }
            catch (WebException w_ex)
            {
                // If the resource is not returned within the time-out period, the request throws a WebException with the Status property set to WebExceptionStatus..::.Timeout.
                if (w_ex.Status != WebExceptionStatus.Timeout)
                    throw;
                else
                    return "-";
            }
        }
    }
}
