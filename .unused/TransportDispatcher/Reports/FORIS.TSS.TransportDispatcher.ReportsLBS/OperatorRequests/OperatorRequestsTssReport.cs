﻿using System.Globalization;
using System.Runtime.InteropServices;
using FORIS.TSS.BusinessLogic.Interfaces.Data;
using FORIS.TSS.TransportDispatcher.Reports;

namespace FORIS.TSS.TransportDispatcher.ReportsLBS.CommunityRequests
{
    /// <summary>
    /// Отчет "Запросы, инициированные оператором"
    /// </summary>
    [Guid("B939736A-7B95-4d0b-B59C-6A126D310E70")]
    public class OperatorRequestsTssReport : MlpRequestsReportBase
    {
        public override int Order
        {
            get { return 0; }
        }

        public override string ReportNameGet(CultureInfo culture)
        {
            var @string = GetStrings(culture);
            return @string["OperatorRequestsTssReport"];
        }

        public override string GetDescription(CultureInfo culture)
        {
            return GetStrings(culture)["OperatorRequestsTssReportDescription"];
        }

        protected override OperatorRequestsReportResultTuple[] GetReportData(OperatorRequestsReportParameters reportParameters)
        {
            return mclsInstance.GetMlpRequestsByOperator(
                reportParameters.OperatorID,
                reportParameters.DepartmentId,
                reportParameters.DateTimeInterval.DateFrom,
                reportParameters.DateTimeInterval.DateTo,
                reportParameters.MapGuid,
                reportParameters.Culture, 
                reportParameters.TimeZoneInfo);
        }
    }
}
