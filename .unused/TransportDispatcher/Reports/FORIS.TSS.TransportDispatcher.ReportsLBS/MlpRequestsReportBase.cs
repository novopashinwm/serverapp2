﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Interfaces.Data;
using FORIS.TSS.Common;
using FORIS.TSS.Resources;
using FORIS.TSS.TransportDispatcher.Reports;
using System.Linq;

namespace FORIS.TSS.TransportDispatcher.ReportsLBS.CommunityRequests
{
    public abstract class MlpRequestsReportBase : TssReportBase
    {
        public override ReportParameters ReportParametersInstanceGet(TssReportParametersSettings settings)
        {
            return new OperatorRequestsReportParameters { OperatorID = settings.OperatorId };
        }

        public override ReportParametersInstanceCheckResult ReportParametersInstanceCheck(ReportParameters iReportParameters)
        {
            var parameters = iReportParameters as OperatorRequestsReportParameters;
            if (parameters == null)
                return false;

            return base.ReportParametersInstanceCheck(iReportParameters);
        }

        public override IReportClass Create(ReportParameters iReportParameters)
        {

            ReportClassWrapper reportClass = null;
            OperatorRequestDataSet dataSet = null;
            var processed = false;
            try
            {
                reportClass = new OperatorRequestsReport();
                dataSet = GetDataSet(iReportParameters);
                reportClass.SetDataSource(dataSet);
                TranslateReport<MlpRequestsReportBase>(reportClass, iReportParameters.Culture);
                processed = true;

                return reportClass;
            }
            finally
            {
                if (!processed && reportClass != null)
                    reportClass.Dispose();

                if (dataSet != null)
                    dataSet.Dispose();
            }
        }

        public OperatorRequestDataSet GetDataSet(ReportParameters iReportParameters)
        {
            var resultTuples = ((IEnumerable<OperatorRequestsReportResultTuple>)GetReportDataOnly(iReportParameters)).ToArray();

            var ds = new OperatorRequestDataSet();

            ConstantsListDataset dsEstablishmentAttributes = ReportEstablishmentAttributesFill(mclsInstance);
            //Добавление в датасет данных атрибутов предприятия.
            ds.Merge(dsEstablishmentAttributes);

            var headerRow = ds.Header.NewHeaderRow();
            headerRow.From = iReportParameters.FromUtc(iReportParameters.DateFrom);
            headerRow.To = iReportParameters.FromUtc(iReportParameters.DateTo);
            headerRow.ReportName = ReportNameGet(iReportParameters.Culture);
            ds.Header.AddHeaderRow(headerRow);

            foreach (var tuple in resultTuples)
            {
                var row = ds.OperatorRequest.NewOperatorRequestRow();

                row.Asker = tuple.Asker;
                row.Position = tuple.Position;
                row.PositionTime = tuple.PositionTime;
                row.RequestDate = tuple.RequestDate;
                row.RequestSource = tuple.RequestSource;
                row.ResponseDate = tuple.ResponseDate;
                row.Result = tuple.Result;
                row.Target = tuple.Target;

                ds.OperatorRequest.Rows.Add(row);
            }

            ds.AcceptChanges();
            return ds;
        }

        public object GetReportDataOnly(ReportParameters reportParameters)
        {
            var result = GetReportData((OperatorRequestsReportParameters)reportParameters);

            var r = ResourceContainers.Get(reportParameters.Culture);

            foreach (var tuple in result)
            {
                tuple.Result = r[tuple.ResultEnum];
                tuple.RequestDate = reportParameters.FromUtc(tuple.RequestDate);
                tuple.ResponseDate = reportParameters.FromUtc(tuple.ResponseDate);
                tuple.PositionTime = reportParameters.FromUtc(tuple.PositionTime);
            }

            Array.Sort(result, (x, y) => x.RequestDate.CompareTo(y.RequestDate));

            return result;
        }

        public override object GetReportData(ReportParameters reportParameters)
        {
            var result = (IEnumerable<OperatorRequestsReportResultTuple>)GetReportDataOnly(reportParameters);
            return GetReportHtml(result, reportParameters);
        }

        private string GetReportHtml(IEnumerable<OperatorRequestsReportResultTuple> data, ReportParameters parameters)
        {
            var stringBuilder = new StringBuilder();
            var @string = new ResourceStringContainer(parameters.Culture, "FORIS.TSS.TransportDispatcher.Reports.Strings", GetType().Assembly);
            using (var stringWriter = new StringWriter(stringBuilder))
            using (var xw = new XmlTextWriter(stringWriter))
            {
                xw.WriteStartElement("table", "http://www.w3.org/1999/xhtml");
                xw.WriteAttributeString("class", "reportTable withBorders");

                xw.WriteStartElement("thead");
                xw.WriteStartElement("tr");

                xw.WriteElementString("td", @string["HeaderAsker"]);
                xw.WriteElementString("td", @string["HeaderTarget"]);
                xw.WriteElementString("td", @string["HeaderRequestDate"]);
                xw.WriteElementString("td", @string["HeaderResponseDate"]);
                xw.WriteElementString("td", @string["HeaderResult"]);
                xw.WriteElementString("td", @string["HeaderPositionTime"]);
                xw.WriteElementString("td", @string["HeaderPosition"]);

                //tr
                xw.WriteEndElement();
                //thead
                xw.WriteEndElement();

                xw.WriteStartElement("tbody");

                foreach (var dr in data)
                {
                    xw.WriteStartElement("tr");
                    
                    xw.WriteElementString("td", dr.Asker);
                    xw.WriteElementString("td", dr.Target);
                    xw.WriteElementString("td", dr.RequestDate == DateTime.MinValue
                                                    ? string.Empty
                                                    : dr.RequestDate.ToString(TimeHelper.DefaultTimeFormat));
                    xw.WriteElementString("td", dr.ResponseDate == DateTime.MinValue
                                                    ? string.Empty
                                                    : dr.ResponseDate.ToString(TimeHelper.DefaultTimeFormat));
                    xw.WriteElementString("td", dr.Result);
                    xw.WriteElementString("td", dr.PositionTime == DateTime.MinValue
                                                    ? string.Empty
                                                    : dr.PositionTime.ToString(TimeHelper.DefaultTimeFormat));
                    var path =
                        string.Format(
                            "https://{0}/objects.aspx?id={1}&a=journal&dateFrom={2}&dateTo={3}&interval=300"
                            , parameters.ApplicationPath
                            , dr.TargetID
                            , dr.PositionTime.ToString("dd.MM.yyyy+HH:mm:ss")
                            , dr.PositionTime.ToString("dd.MM.yyyy+HH:mm:ss")
                            );
                    xw.WriteStartElement("td");
                    xw.WriteStartElement("a");
                    xw.WriteAttributeString("href", path.Replace(' ', '+'));
                    xw.WriteString(dr.Position);
                    xw.WriteEndElement();
                    xw.WriteEndElement();

                    //tr
                    xw.WriteEndElement();
                }
                
                //tbody
                xw.WriteEndElement();
                
                //table
                xw.WriteEndElement();
            }

            return stringBuilder.ToString();
        }

        protected abstract OperatorRequestsReportResultTuple[] GetReportData(OperatorRequestsReportParameters reportParameters);

        protected static ResourceStringContainer GetStrings(CultureInfo culture)
        {
            return new ResourceStringContainer(
                culture, "FORIS.TSS.TransportDispatcher.Reports.Strings", typeof(OperatorRequestsTssReport).Assembly);
        }
    }
}
