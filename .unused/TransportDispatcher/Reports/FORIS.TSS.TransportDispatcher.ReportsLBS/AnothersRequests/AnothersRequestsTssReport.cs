﻿using System.Globalization;
using System.Runtime.InteropServices;
using FORIS.TSS.BusinessLogic.Interfaces.Data;
using FORIS.TSS.TransportDispatcher.Reports;

//TODO: устранить copy-paste совместно с OperatorRequestsTssReport.cs

namespace FORIS.TSS.TransportDispatcher.ReportsLBS.CommunityRequests
{
    /// <summary>
    /// Отчет "Запросы, других операторов"
    /// </summary>
    [Guid("873109F4-D52A-40c5-B095-6CE759A17469")]
    public class AnothersRequestsTssReport : MlpRequestsReportBase
    {
        public override int Order
        {
            get { return 1; }
        }

        public override string ReportNameGet(CultureInfo culture)
        {
            var @string = GetStrings(culture);
            return @string["AnothersRequestsTssReport"];
        }

        public override string GetDescription(CultureInfo culture)
        {
            return GetStrings(culture)["AnothersRequestsTssReportDescription"];
        }

        protected override OperatorRequestsReportResultTuple[] GetReportData(OperatorRequestsReportParameters reportParameters)
        {
            return mclsInstance.GetMlpRequestsToOperator(
                reportParameters.OperatorID,
                reportParameters.DepartmentId,
                reportParameters.DateTimeInterval.DateFrom,
                reportParameters.DateTimeInterval.DateTo,
                reportParameters.MapGuid,
                reportParameters.Culture, 
                reportParameters.TimeZoneInfo);
        }
    }
}