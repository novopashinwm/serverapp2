﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using System.Data;
using FORIS.TSS.TransportDispatcher.Reports;

namespace TestReport
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var report = new GroupFuelReport())
            {
                var ds = new FuelCostDataSet();
                ds.ReadXml(@"\TEMP\dsFuelCostDataSet.xml");
                report.SetDataSource(ds);
                //TssReportBase.TranslateReport(report, System.Globalization.CultureInfo.CurrentCulture);
                report.PrintOptions.PaperOrientation =
                    CrystalDecisions.Shared.PaperOrientation.Landscape;

                foreach (FuelCostDataSet.ReportHeaderRow row in ds.ReportHeader.Rows)
                    row.ReportHeader_ID = 1;

                foreach (FuelCostDataSet.ConstantsListRow row in ds.ConstantsList.Rows)
                    row.ReportHeader_ID = 1;

                foreach (FuelCostDataSet.TotalRow row in ds.Total.Rows)
                    row.ReportHeader_ID = 1;

                report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"\temp\" + Guid.NewGuid() + ".pdf");
            }
        }
    }
}
