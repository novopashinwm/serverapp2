﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using FORIS.TSS.BusinessLogic.Map;
using MTMap.MathTypes;
using DPoint = FORIS.TSS.BusinessLogic.Map.DPoint;

/*
using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.Map;
using TerminalService.Interfaces;
using DPoint = FORIS.TSS.BusinessLogic.Map.DPoint;
using System.Data;
*/

namespace FORIS.TSS.TransportDispatcher.Reports
{
    static class GeoZoneCheckerContain
    {
        //public static bool Contains(DataRow[] drsZonePoints, int zoneTypeID, double x, double y)
        //{
        //    bool result = false;

        //    ZoneTypeEnum zoneType = (ZoneTypeEnum)zoneTypeID;

        //    switch (zoneType)
        //    {
        //        case ZoneTypeEnum.Round:
        //            result = ContainsRound(drsZonePoints, x, y);
        //            break;

        //        case ZoneTypeEnum.Rounds:
        //            result = ContainsRounds(drsZonePoints, x, y);
        //            break;

        //        case ZoneTypeEnum.Poligon:
        //            result = ContainsPoligon(drsZonePoints, x, y);
        //            break;

        //        case ZoneTypeEnum.HoldingAlley:
        //            result = ContainsHoldingAlley(drsZonePoints, x, y);
        //            break;
        //    }

        //    return result;
        //}

        //private static bool ContainsRound(DataRow[] drsZonePoints, double x, double y)
        //{
        //    if (drsZonePoints.Length > 0)
        //    {
        //        double distance =
        //            PositionHelper.SLClose(
        //                (float)drsZonePoints[0]["X"],
        //                (float)drsZonePoints[0]["Y"],
        //                x,
        //                y
        //                );
        //        if (distance < (float)drsZonePoints[0]["Radius"]) return true;
        //    }

        //    return false;
        //}

        //private static bool ContainsRounds(DataRow[] drsZonePoints, double x, double y)
        //{
        //    foreach (DataRow zonePoint in drsZonePoints)
        //    {
        //        double distance =
        //            PositionHelper.SLClose(
        //                (float)zonePoint["X"],
        //                (float)zonePoint["Y"],
        //                x,
        //                y
        //                );
        //        if (distance < (float)zonePoint["Radius"]) return true;
        //    }

        //    return false;
        //}

        public static bool ContainsPoligon(DataRow[] drsZonePoints, double latitude, double longitude)
        {
            #region Preconditions

            if (drsZonePoints.Length == 0) return false;

            #endregion  // Preconditions

            DPoint checkPoint = GeoPoint.CreateGeo(latitude, longitude).Planar;
            List<DPoint> points = new List<DPoint>(drsZonePoints.Length);

            foreach (DataRow zonePoint in drsZonePoints)
            {
                points.Add(GeoPoint.CreateGeo((double)zonePoint["X"], (double)zonePoint["Y"]).Planar);
            }

            bool result = false;
            for (int i = 0, j = points.Count - 1; i < points.Count; j = i++)
            {
                if (
                    ((points[i].y <= checkPoint.y && checkPoint.y < points[j].y)
                        || (points[j].y <= checkPoint.y && checkPoint.y < points[i].y))
                    && (checkPoint.x > (points[j].x - points[i].x) * (checkPoint.y - points[i].y) / (points[j].y - points[i].y) + points[i].x)
                    )
                {
                    result = !result;
                }
            }

            return result;
        }

        //private static bool ContainsHoldingAlley(DataRow[] drsZonePoints, double x, double y)
        //{
        //    #region Preconditions

        //    if (drsZonePoints.Length == 0) return false;

        //    #endregion  // Preconditions

        //    DPoint checkPoint = GeoPoint.CreateGeo(x, y).Planar;
        //    List<DPoint> points = new List<DPoint>(drsZonePoints.Length);
        //    List<float> radiuses = new List<float>(drsZonePoints.Length);

        //    foreach (DataRow zonePoint in drsZonePoints)
        //    {
        //        DPoint point = GeoPoint.CreateGeo((float)zonePoint["X"], (float)zonePoint["Y"]).Planar;
        //        DPoint point1 =
        //            PositionHelper.ShiftGeo(
        //                (float)zonePoint["X"],
        //                (float)zonePoint["Y"],
        //                (float)zonePoint["Radius"],
        //                0
        //            ).Planar;

        //        points.Add(point);
        //        radiuses.Add(point.distanceTo(point1));
        //    }

        //    if (checkPoint.distanceTo(points[0]) < radiuses[0]) return true;

        //    for (int i = 1; i < points.Count; i++)
        //    {
        //        DPoint point1 = points[i - 1];
        //        DPoint point2 = points[i];

        //        if (checkPoint.distanceTo(point2) < radiuses[i]) return true;

        //        double l = point1.distanceTo(point2);
        //        if (l > Math.Abs(radiuses[i - 1] - radiuses[i]))
        //        {
        //            double cos = (point2.x - point1.x) / l;
        //            double sin = (point1.y - point2.y) / l;

        //            double s = Math.Sign(radiuses[i - 1] - radiuses[i]);
        //            double BC =
        //                Math.Sqrt(
        //                    l * l -
        //                    (radiuses[i - 1] - radiuses[i]) * (radiuses[i - 1] - radiuses[i])
        //                    );
        //            double CE = radiuses[i] * BC / l;
        //            double DE = Math.Sqrt(radiuses[i] * radiuses[i] - CE * CE);

        //            double x1 = s * DE * cos + CE * sin + point2.x;
        //            double y1 = -s * DE * sin + CE * cos + point2.y;
        //            double x2 = s * DE * cos - CE * sin + point2.x;
        //            double y2 = -s * DE * sin - CE * cos + point2.y;

        //            CE = radiuses[i - 1] * BC / l;
        //            DE = Math.Sqrt(radiuses[i - 1] * radiuses[i - 1] - CE * CE);

        //            double x3 = s * DE * cos - CE * sin + point1.x;
        //            double y3 = -s * DE * sin - CE * cos + point1.y;
        //            double x4 = s * DE * cos + CE * sin + point1.x;
        //            double y4 = -s * DE * sin + CE * cos + point1.y;

        //            int checker = 0;
        //            double A1 = y2 - y1;
        //            double B1 = x1 - x2;
        //            double C1 = y1 * (x2 - x1) - x1 * (y2 - y1);
        //            checker += Math.Sign(A1 * checkPoint.x + B1 * checkPoint.y + C1);
        //            double A2 = y3 - y2;
        //            double B2 = x2 - x3;
        //            double C2 = y2 * (x3 - x2) - x2 * (y3 - y2);
        //            checker += Math.Sign(A2 * checkPoint.x + B2 * checkPoint.y + C2);
        //            double A3 = y4 - y3;
        //            double B3 = x3 - x4;
        //            double C3 = y3 * (x4 - x3) - x3 * (y4 - y3);
        //            checker += Math.Sign(A3 * checkPoint.x + B3 * checkPoint.y + C3);
        //            double A4 = y1 - y4;
        //            double B4 = x4 - x1;
        //            double C4 = y4 * (x1 - x4) - x4 * (y1 - y4);
        //            checker += Math.Sign(A4 * checkPoint.x + B4 * checkPoint.y + C4);

        //            if (Math.Abs(checker) == 4) return true;
        //        }
        //    }

        //    return false;
        //}

        public static bool ContainsPointInPoligon(DataRow[] drsZonePoints, double latitude, double longitude)
        {
            if (drsZonePoints.Length == 0)
                return false;

            DPoint pointPlanar = GeoPoint.CreateGeo(latitude, longitude).Planar;

            MTMap.MathTypes.DPoint point = new MTMap.MathTypes.DPoint(pointPlanar.x, pointPlanar.y);

            DPointList poligon = new DPointList();
            poligon.setSize(drsZonePoints.Length);
            for (int i = 0; i < drsZonePoints.Length; i++)
            {
                DPoint pointPolygonPlanar = GeoPoint.CreateGeo((double)drsZonePoints[i]["X"], (double)drsZonePoints[i]["Y"]).Planar;
                MTMap.MathTypes.DPoint pointPolygon = new MTMap.MathTypes.DPoint(pointPolygonPlanar.x, pointPolygonPlanar.y);
                poligon.points[i] = pointPolygon;
            }

            if (MTMap.MathTypes.Math2D.isPointInСontour(point, poligon) == Math2D.PointInPolygonState.ppsInside)
                return true;

            return false;
        }

    }


    //public enum ZoneTypeEnum
    //{
    //    Undefined = 0,
    //    Round = 1,
    //    Rounds = 2,
    //    Poligon = 3,
    //    HoldingAlley = 4
    //}
}
