using System;
using System.Collections.Generic;
using System.Diagnostics;
using FORIS.TSS.Terminal;

namespace FORIS.TSS.Performance
{
	public interface IUnitInfoCounters
	{
		PerformanceCounter this[UnitInfoPurpose purpose] { get; }
	}

	class UnitInfoPurposeCounterCollection : IUnitInfoCounters
	{
		private string prefix;

		private UnitInfoPurpose[] purposes;

		private Dictionary<UnitInfoPurpose, PerformanceCounter> counters;

		public PerformanceCounter this[UnitInfoPurpose purpose]
		{
			get { return this.counters[purpose]; }
		}

		private CounterCreationData[] creationData;

		public CounterCreationData[] CreationData
		{
			get { return this.creationData; }
		}

		public UnitInfoPurposeCounterCollection(string prefix)
		{
			this.prefix = prefix;

			this.purposes = (UnitInfoPurpose[])Enum.GetValues(typeof(UnitInfoPurpose));

			this.creationData = new CounterCreationData[this.purposes.Length];

			for (int i = 0; i < this.purposes.Length; i++)
			{
				UnitInfoPurpose Purpose = this.purposes[i];

				this.creationData[i] = new CounterCreationData();
				this.creationData[i].CounterName =
					String.Format("{0}: {1}", this.prefix, Purpose.ToString());
				this.creationData[i].CounterType = PerformanceCounterType.NumberOfItems32;
			}
		}

		public void RegisterCounters(string categoryName)
		{
			this.counters = new Dictionary<UnitInfoPurpose, PerformanceCounter>();

			for (int i = 0; i < this.purposes.Length; i++)
			{
				UnitInfoPurpose Purpose = this.purposes[i];

				this.counters[Purpose] =
					new PerformanceCounter(
						categoryName,
						String.Format("{0}: {1}", this.prefix, Purpose.ToString()),
						false
						);
			}
		}
	}
}