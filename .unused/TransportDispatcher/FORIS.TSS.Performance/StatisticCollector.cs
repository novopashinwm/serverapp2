﻿using System;
using System.Diagnostics;

namespace FORIS.TSS.Performance
{
	public class StatisticCollector
	{
		public readonly PerformanceCounter EventTranslatorServerSinkRegistryCounter;
		public readonly PerformanceCounter EventTranslatorServerSinkEventStubCounter;
		public readonly PerformanceCounter EventTranslatorServerSinkEventMessageCounter;
		private readonly PerformanceCounter _eventTranslatorWebclientCreatePersonalServerCounter;

		#region Instance (static)

		private static readonly StatisticCollector SInstance;

		public static StatisticCollector Instance
		{
			get { return SInstance; }
		}

		static StatisticCollector()
		{
			try
			{
				SInstance = new StatisticCollector();
			}
			catch (Exception ex)
			{
				Debug.WriteLine("Не сконструировался StatisticCollector ");
				Debug.WriteLine("Exception: " + ex);

				if (ex.InnerException != null)
				{
					Debug.WriteLine("InnerException: " + ex.InnerException);
				}
			}
		}

		#endregion // Instance (static)

		private const string CategoryName = "Nika";

		private readonly UnitInfoPurposeCounterCollection _unitInfoAliveCounters;

		public IUnitInfoCounters UnitInfoAliveCounters
		{
			get { return _unitInfoAliveCounters; }
		}

		private readonly UnitInfoPurposeCounterCollection _unitInfoTotalCounters;

		public IUnitInfoCounters UnitInfoTotalCounters
		{
			get { return _unitInfoTotalCounters; }
		}


		private StatisticCollector()
		{
			try
			{
				_unitInfoAliveCounters = new UnitInfoPurposeCounterCollection("UnitInfo Alive");
				_unitInfoTotalCounters = new UnitInfoPurposeCounterCollection("UnitInfo Total");

				if (PerformanceCounterCategory.Exists(CategoryName))
				{
					try
					{
						PerformanceCounterCategory.Delete(CategoryName);
					}
					catch (System.ComponentModel.Win32Exception ex)
					{
						Trace.WriteLine(ex.ToString());
					}

				}

				if (PerformanceCounterCategory.Exists(CategoryName))
					Trace.Write("Категория " + CategoryName + " не удалилась");

				if (!PerformanceCounterCategory.Exists(CategoryName))
				{
					#region EventTranslator server sink counters

					var eventTranslatorServerSinkRegistryCount = new CounterCreationData
					{
						CounterName = "ET: registries count",
						CounterType = PerformanceCounterType.NumberOfItems32
					};

					var eventTranslatorServerSinkEventStubCount = new CounterCreationData
					{
						CounterName = "ET: EventStub count",
						CounterType = PerformanceCounterType.NumberOfItems32
					};

					var eventTranslatorServerSinkEventMessageCount = new CounterCreationData
					{
						CounterName = "ET: EventMessage queue length",
						CounterType = PerformanceCounterType.NumberOfItems32
					};

					var eventTranslatorWebclientCreatePersonalServerCount = new CounterCreationData
					{
						CounterName = "ET: IWebPS instance count",
						CounterType = PerformanceCounterType.NumberOfItems32
					};

					var eventTranslatorCounters =
						new[]
						{
							eventTranslatorServerSinkRegistryCount,
							eventTranslatorServerSinkEventStubCount,
							eventTranslatorServerSinkEventMessageCount,
							eventTranslatorWebclientCreatePersonalServerCount
						};
#if DEBUG
					Trace.WriteLine("EventTranslator server sink counters:");
					foreach (CounterCreationData ccd in eventTranslatorCounters)
					{
						Trace.TraceInformation("\t" + ccd.CounterName);
					}
#endif

					#endregion // EventTranslator server sink counters

					var counters = new CounterCreationDataCollection();

					counters.AddRange(eventTranslatorCounters);
					counters.AddRange(_unitInfoAliveCounters.CreationData);
					counters.AddRange(_unitInfoTotalCounters.CreationData);

					const string performanceCounterCategory = "TSS Server statistics";
					try
					{
						PerformanceCounterCategory.Create(
							CategoryName,
							performanceCounterCategory,
							PerformanceCounterCategoryType.SingleInstance,
							counters
							);
					}
					catch (UnauthorizedAccessException)
					{
						Trace.TraceWarning("Unable to create performance counter category {0}", performanceCounterCategory);
						return;
					}

					Trace.TraceInformation("Категория для счетчиков создана {0}", performanceCounterCategory);
				}

				_unitInfoAliveCounters.RegisterCounters(CategoryName);
				_unitInfoTotalCounters.RegisterCounters(CategoryName);

				EventTranslatorServerSinkRegistryCounter =
					new PerformanceCounter(
						CategoryName,
						"ET: registries count",
						false
						);

				EventTranslatorServerSinkEventStubCounter =
					new PerformanceCounter(
						CategoryName,
						"ET: EventStub count",
						false
						);

				EventTranslatorServerSinkEventMessageCounter =
					new PerformanceCounter(
						CategoryName,
						"ET: EventMessage queue length",
						false
						);

				_eventTranslatorWebclientCreatePersonalServerCounter = new PerformanceCounter(
						CategoryName,
						"ET: IWebPS instance count",
						false
						);

				Trace.TraceInformation("Счетчики созданы");
			}
			catch (Exception e)
			{
				Trace.TraceError("StatisticCollector(): {0}", e);
			}
		}

		public void Initialize()
		{

		}

		public void IncrementIWebPSCounter()
		{
			if (_eventTranslatorWebclientCreatePersonalServerCounter == null)
				return;
			_eventTranslatorWebclientCreatePersonalServerCounter.Increment();
		}
	}
}