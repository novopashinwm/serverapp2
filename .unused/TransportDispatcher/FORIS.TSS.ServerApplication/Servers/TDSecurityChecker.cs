﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Server;

namespace FORIS.TSS.ServerApplication.Servers
{
	public class TDSecurityChecker
	{
		private readonly IPersonalServer _ps;

		/// <summary> БД с правами оператора </summary>
		private /*readonly*/ DataSet _dsRights;
		private readonly object _dsRightsLock = new object();
		public DataSet Rights
		{
			get
			{
				if (_dsRights != null)
					return _dsRights;

				lock (_dsRightsLock)
				{
					if (_dsRights != null)
						return _dsRights;
					_dsRights = _ps.GetSecurityDataset();
					DatabaseSchema.MakeSchema(_dsRights);
				}

				return _dsRights;
			}
		}
		public TDSecurityChecker(IPersonalServer ps)
		{
			_ps = ps;
		}
		internal DataSet Check(IPersonalServer server, DataSet dataset)
		{
			if (dataset.Tables.Contains("VEHICLE"))
				DatabaseSchema.MakeSchema(dataset);

			if (SystemRightsHelper.GetEffectiveOperatorRight(Rights, server.SessionInfo.OperatorInfo.OperatorId, 1))
				return dataset;

			return CallCheckMethod(server, dataset);
		}
		private DataSet CallCheckMethod(IPersonalServer server, DataSet dataset)
		{
			// check security on vehicles
			if (dataset.Tables.Contains("VEHICLE"))
			{
				foreach (DataRow row in dataset.Tables["VEHICLE"].Rows)
				{
					int object_id = (int)row["VEHICLE_ID"];
					bool allowed = VehicleRightsHelper.GetEffectiveOperatorVehicleRights(Rights, server.SessionInfo.OperatorInfo.OperatorId, object_id, (int)SystemRight.VehicleAccess);
					if (allowed == false)
					{
						DatasetHelper.CascadeRemove_Vehicle(dataset, object_id);
					}
				}
			}

			// заплатка! astfanov
			// надо добавтиь право на объект Группа ТС
			// check security on vehicles group
			if (dataset.Tables.Contains("VEHICLEGROUP"))
			{
				foreach (DataRow row in dataset.Tables["VEHICLEGROUP"].Rows)
				{
					int object_id = (int)row["VEHICLEGROUP_ID"];
					int allowed = VehicleRightsHelper.GetEffectiveOperatorGroupRights(Rights, server.SessionInfo.OperatorInfo.OperatorId, object_id, (int)SystemRight.VehicleAccess);
					if (allowed == 0 || allowed == 2)
					{
						DatasetHelper.CascadeRemove_Vehiclegroup(dataset, object_id);
					}
				}
			}

			if (dataset.Tables.Contains("DEPARTMENT"))
			{
				foreach (DataRow row in dataset.Tables["DEPARTMENT"].Rows)
				{
					int object_id = (int)row["DEPARTMENT_ID"];
					bool allowed = DepartmentRightsHelper.GetEffectiveOperatorDepartmentRights(Rights, server.SessionInfo.OperatorInfo.OperatorId, object_id, (int)SystemRight.DepartmentsAccess);
					if (allowed == false)
					{
						DatasetHelper.CascadeRemove_Department(dataset, object_id);
					}
				}
			}

			if (dataset.Tables.Contains("GEO_ZONE"))
			{
				foreach (DataRow row in dataset.Tables["GEO_ZONE"].Rows)
				{
					int object_id = (int)row["ZONE_ID"];
					bool allowed = ZoneRightsHelper.GetEffectiveOperatorZoneRights(Rights, server.SessionInfo.OperatorInfo.OperatorId, object_id, (int)SystemRight.ZoneAccess);
					if (allowed == false)
					{
						DatasetHelper.CascadeRemove_GeoZone(dataset, object_id);
					}
				}
			}

			dataset.AcceptChanges();
			return dataset;
		}
		public void CheckAll(IPersonalServer ps, DataSet ds)
		{
			bool bHasFullAccess = SystemRightsHelper.GetEffectiveOperatorRight(Rights,
				ps.SessionInfo.OperatorInfo.OperatorId,
				(int)SystemRight.ServerAdministration);

			if (bHasFullAccess)
				return;

			CallCheckAllMethods(ps, ds);
		}
		private void CallCheckAllMethods(IPersonalServer ps, DataSet ds)
		{
			CheckDepartments(ps, ds);
			CheckDriverGroup(ps, ds);
			CheckDrivers(ps, ds);
			CheckRouteGroup(ps, ds);
			CheckRoutes(ps, ds);
			CheckWayouts(ps, ds);
			CheckRouteTrip(ps, ds);
			CheckVehiclesGroup(ps, ds);
			CheckVehicles(ps, ds);
			CheckForWaybills(ps, ds);
			CheckForJournal(ps, ds);
			CheckWorkTime(ps, ds);
		}
		public void CheckDepartments(IPersonalServer ps, DataSet ds)
		{
			if (ds.Tables.Contains("DEPARTMENT"))
			{
				DataTable DepartmentTable = ds.Tables["DEPARTMENT"];

				List<DataRow> NoAccessRows = new List<DataRow>(DepartmentTable.Rows.Count);

				foreach (DataRow departmentRow in DepartmentTable.Rows)
				{
					bool HasAccess =
						DepartmentRightsHelper.GetEffectiveOperatorDepartmentRights(
							Rights,
							ps.SessionInfo.OperatorInfo.OperatorId,
							(int)departmentRow["DEPARTMENT_ID"],
							(int)SystemRight.DepartmentsAccess
							);

					if (!HasAccess && !NoAccessRows.Contains(departmentRow))
					{
						NoAccessRows.Add(departmentRow);
					}
				}

				foreach (DataRow departmentRow in NoAccessRows)
				{
					DatasetHelper.CascadeRemove_Department(ds, (int)departmentRow["DEPARTMENT_ID"]);
				}

				ds.AcceptChanges();
			}
			else
			{
				#region Check Rights for VEHICLE & DRIVER & ORDER if dataset does not contain DEPARTMENT

				if (ds.Tables.Contains("DRIVER"))
				{
					DataTable DriverTable = ds.Tables["DRIVER"];

					List<DataRow> NoAccessRows = new List<DataRow>(DriverTable.Rows.Count);

					foreach (DataRow driverRow in DriverTable.Rows)
					{
						/* Если водитель не принадлежит ни одной автоколонне,
						 * то увидеть его может только пользователь с полными 
						 * правами, для которого не выполняется проверка прав
						 */

						bool HasAccess =
							driverRow["DEPARTMENT"] != DBNull.Value
								?
							DepartmentRightsHelper.GetEffectiveOperatorDepartmentRights(
								Rights,
								ps.SessionInfo.OperatorInfo.OperatorId,
								(int)driverRow["DEPARTMENT"],
								(int)SystemRight.DepartmentsAccess
								)
								:
							true;


						if (!HasAccess && !NoAccessRows.Contains(driverRow))
						{
							NoAccessRows.Add(driverRow);
						}
					}

					foreach (DataRow driverRow in NoAccessRows)
					{
						DatasetHelper.CascadeRemove_Driver(ds, (int)driverRow["DRIVER_ID"]);
					}

					ds.AcceptChanges();
				}

				if (ds.Tables.Contains("VEHICLE") && ds.Tables["VEHICLE"].Columns.Contains("DEPARTMENT"))
				{
					DataTable VehicleTable = ds.Tables["VEHICLE"];

					List<DataRow> NoAccessRows = new List<DataRow>(VehicleTable.Rows.Count);

					foreach (DataRow vehicleRow in VehicleTable.Rows)
					{
						/* Если ТС не принадлежит ни одной автоколонне,
						 * то увидеть его может только пользователь с полными 
						 * правами, для которого не выполняется проверка прав
						 */

						bool HasAccess =
							vehicleRow["DEPARTMENT"] != DBNull.Value
								?
							DepartmentRightsHelper.GetEffectiveOperatorDepartmentRights(
								Rights,
								ps.SessionInfo.OperatorInfo.OperatorId,
								(int)vehicleRow["DEPARTMENT"],
								(int)SystemRight.DepartmentsAccess
								)
								:
							true;

						if (!HasAccess && !NoAccessRows.Contains(vehicleRow))
						{
							NoAccessRows.Add(vehicleRow);
						}
					}

					foreach (DataRow vehicleRow in NoAccessRows)
					{
						DatasetHelper.CascadeRemove_Vehicle(ds, (int)vehicleRow["VEHICLE_ID"]);
					}

					ds.AcceptChanges();
				}

				if (ds.Tables.Contains("ORDER"))
				{
					DataTable OrderTable = ds.Tables["ORDER"];

					List<DataRow> NoAccessRows = new List<DataRow>(OrderTable.Rows.Count);

					foreach (DataRow orderRow in OrderTable.Rows)
					{
						/* Если заказ не принадлежит ни одной автоколонне,
						 * то увидеть его может только пользователь с полными 
						 * правами, для которого не выполняется проверка прав
						 */

						bool HasAccess =
							orderRow["DEPARTMENT"] != DBNull.Value
								?
							DepartmentRightsHelper.GetEffectiveOperatorDepartmentRights(
								Rights,
								ps.SessionInfo.OperatorInfo.OperatorId,
								(int)orderRow["DEPARTMENT"],
								(int)SystemRight.DepartmentsAccess
								)
								:
							false;

						if (!HasAccess && !NoAccessRows.Contains(orderRow))
						{
							NoAccessRows.Add(orderRow);
						}
					}

					foreach (DataRow orderRow in NoAccessRows)
					{
						DatasetHelper.CascadeRemove_Order(ds, (int)orderRow["ORDER_ID"]);
					}

					ds.AcceptChanges();
				}

				#endregion // Check Rights for VEHICLE & DRIVER & ORDER if dataset does not contain DEPARTMENT
			}
		}
		public void CheckDriverGroup(IPersonalServer ps, DataSet ds)
		{
			if (!ds.Tables.Contains("DRIVERGROUP"))
				return;

			ArrayList al = new ArrayList();

			foreach (DataRow dr in ds.Tables["DRIVERGROUP"].Rows)
			{
				int bHasAccessDriver = DriverRightsHelper.GetEffectiveOperatorGroupRights(
					Rights,
					ps.SessionInfo.OperatorInfo.OperatorId,
					(int)dr["DRIVERGROUP_ID"],
					(int)SystemRight.DriverAccess);

				if ((bHasAccessDriver == 0 || bHasAccessDriver == 2)
					&& !al.Contains((int)dr["DRIVERGROUP_ID"]))
				{
					al.Add((int)dr["DRIVERGROUP_ID"]);
				}
			}

			foreach (int id in al)
				DatasetHelper.CascadeRemove_Drivergroup(ds, id);

			ds.AcceptChanges();
		}
		public void CheckDrivers(IPersonalServer ps, DataSet ds)
		{
			if (ds.Tables.Contains("DRIVER"))
			{
				ArrayList alDriverIds = new ArrayList();

				foreach (DataRow drDriver in ds.Tables["DRIVER"].Rows)
				{
					bool bHasAccessDriver = DriverRightsHelper.GetEffectiveOperatorDriverRights(
						Rights,
						ps.SessionInfo.OperatorInfo.OperatorId,
						(int)drDriver["DRIVER_ID"],
						(int)SystemRight.DriverAccess);

					if (!bHasAccessDriver && !alDriverIds.Contains((int)drDriver["DRIVER_ID"]))
						alDriverIds.Add((int)drDriver["DRIVER_ID"]);
				}

				foreach (int iDriverId in alDriverIds)
					DatasetHelper.CascadeRemove_Driver(ds, iDriverId);

				ds.AcceptChanges();
			}

			if (ds.Tables.Contains("DRIVER_VEHICLE"))
			{
				DataTable driverVehicleTable = ds.Tables["DRIVER_VEHICLE"];

				List<DataRow> NoAccessRows = new List<DataRow>(driverVehicleTable.Rows.Count);

				foreach (DataRow driverVehicleRow in driverVehicleTable.Rows)
				{
					bool HasAccess =
						DriverRightsHelper.GetEffectiveOperatorDriverRights(
							Rights,
							ps.SessionInfo.OperatorInfo.OperatorId,
							(int)driverVehicleRow["DRIVER_ID"],
							(int)SystemRight.DriverAccess
							);


					if (!HasAccess && !NoAccessRows.Contains(driverVehicleRow))
					{
						NoAccessRows.Add(driverVehicleRow);
					}
				}

				foreach (DataRow driverVehicleRow in NoAccessRows)
				{
					DatasetHelper.CascadeRemove_DriverVehicle(ds, (int)driverVehicleRow["DRIVER_ID"]);
				}

				ds.AcceptChanges();
			}
		}
		public void CheckRouteGroup(IPersonalServer ps, DataSet ds)
		{
			if (!ds.Tables.Contains("ROUTEGROUP"))
				return;

			ArrayList alDel = new ArrayList();

			foreach (DataRow dr in ds.Tables["ROUTEGROUP"].Rows)
			{
				int bHasAccessRoute = RouteRightsHelper.GetEffectiveOperatorGroupRights(
					Rights,
					ps.SessionInfo.OperatorInfo.OperatorId,
					(int)dr["ROUTEGROUP_ID"],
					(int)SystemRight.RouteAccess);

				if ((bHasAccessRoute == 0 || bHasAccessRoute == 2)
					&& !alDel.Contains((int)dr["ROUTEGROUP_ID"]))
				{
					alDel.Add((int)dr["ROUTEGROUP_ID"]);
				}
			}

			foreach (int iRouteId in alDel)
				DatasetHelper.CascadeRemove_Routegroup(ds, iRouteId);

			ds.AcceptChanges();
		}
		public void CheckRoutes(IPersonalServer ps, DataSet ds)
		{
			if (!ds.Tables.Contains("ROUTE"))
				return;

			ArrayList alRouteIds = new ArrayList();

			foreach (DataRow drRoute in ds.Tables["ROUTE"].Rows)
			{
				bool bHasAccessRoute = RouteRightsHelper.GetEffectiveOperatorRouteRights(Rights,
					ps.SessionInfo.OperatorInfo.OperatorId,
					(int)drRoute["ROUTE_ID"],
					(int)SystemRight.RouteAccess);

				if (!bHasAccessRoute && !alRouteIds.Contains((int)drRoute["ROUTE_ID"]))
					alRouteIds.Add((int)drRoute["ROUTE_ID"]);
			}

			foreach (int iRouteId in alRouteIds)
				DatasetHelper.CascadeRemove_Route(ds, iRouteId);

			ds.AcceptChanges();
		}
		public void CheckWayouts(IPersonalServer ps, DataSet ds)
		{
			if (ds.Tables.Contains("WAYOUT") && !ds.Tables.Contains("ROUTE"))
			{
				DataTable WayoutTable = ds.Tables["WAYOUT"];

				List<DataRow> NoAccessRows = new List<DataRow>(WayoutTable.Rows.Count);

				foreach (DataRow wayoutRow in WayoutTable.Rows)
				{
					bool HasAccess =
						RouteRightsHelper.GetEffectiveOperatorRouteRights(
							Rights,
							ps.SessionInfo.OperatorInfo.OperatorId,
							(int)wayoutRow["ROUTE"],
							(int)SystemRight.RouteAccess
							);

					if (!HasAccess && !NoAccessRows.Contains(wayoutRow))
					{
						NoAccessRows.Add(wayoutRow);
					}
				}

				foreach (DataRow wayoutRow in NoAccessRows)
				{
					DatasetHelper.CascadeRemove_Wayout(ds, (int)wayoutRow["WAYOUT_ID"]);
				}

				ds.AcceptChanges();
			}
		}
		public void CheckRouteTrip(IPersonalServer ps, DataSet ds)
		{
			if (ds.Tables.Contains("ROUTE_TRIP") && !ds.Tables.Contains("ROUTE"))
			{
				DataTable RouteTripTable = ds.Tables["ROUTE_TRIP"];

				List<DataRow> NoAccessRows = new List<DataRow>(RouteTripTable.Rows.Count);

				foreach (DataRow routeTripRow in RouteTripTable.Rows)
				{
					bool HasAccess =
						RouteRightsHelper.GetEffectiveOperatorRouteRights(
							Rights,
							ps.SessionInfo.OperatorInfo.OperatorId,
							(int)routeTripRow["ROUTE_ID"],
							(int)SystemRight.RouteAccess
							);

					if (!HasAccess && !NoAccessRows.Contains(routeTripRow))
					{
						NoAccessRows.Add(routeTripRow);
					}
				}

				foreach (DataRow routeTripRow in NoAccessRows)
				{
					DatasetHelper.CascadeRemove_RouteTrip(ds, (int)routeTripRow["ROUTE_TRIP_ID"]);
				}

				ds.AcceptChanges();
			}
		}
		public void CheckVehiclesGroup(IPersonalServer ps, DataSet ds)
		{
			if (!ds.Tables.Contains("VEHICLEGROUP"))
				return;

			ArrayList alDel = new ArrayList();

			foreach (DataRow dr in ds.Tables["VEHICLEGROUP"].Rows)
			{
				int bHasAccessVehicle = VehicleRightsHelper.GetEffectiveOperatorGroupRights(
					Rights,
					ps.SessionInfo.OperatorInfo.OperatorId,
					(int)dr["VEHICLEGROUP_ID"],
					(int)SystemRight.VehicleAccess);

				if ((bHasAccessVehicle == 0 || bHasAccessVehicle == 2)
					&& !alDel.Contains((int)dr["VEHICLEGROUP_ID"]))
				{
					alDel.Add((int)dr["VEHICLEGROUP_ID"]);
				}
			}

			foreach (int id in alDel)
				DatasetHelper.CascadeRemove_Vehiclegroup(ds, id);

			ds.AcceptChanges();
		}
		public void CheckVehicles(IPersonalServer ps, DataSet ds)
		{
			if (ds.Tables.Contains("VEHICLE"))
			{
				ArrayList alVehicleIds = new ArrayList();

				foreach (DataRow drVehicle in ds.Tables["VEHICLE"].Rows)
				{
					bool bHasAccessVehicle =
						VehicleRightsHelper.GetEffectiveOperatorVehicleRights(
							Rights,
							ps.SessionInfo.OperatorInfo.OperatorId,
							(int)drVehicle["VEHICLE_ID"],
							(int)SystemRight.VehicleAccess
							);

					if (!bHasAccessVehicle && !alVehicleIds.Contains((int)drVehicle["VEHICLE_ID"]))
					{
						alVehicleIds.Add((int)drVehicle["VEHICLE_ID"]);
					}
				}

				foreach (int iVehicleId in alVehicleIds)
					DatasetHelper.CascadeRemove_Vehicle(ds, iVehicleId);

				ds.AcceptChanges();
			}

			if (ds.Tables.Contains("DRIVER_VEHICLE"))
			{
				DataTable driverVehicleTable = ds.Tables["DRIVER_VEHICLE"];

				List<DataRow> NoAccessRows = new List<DataRow>(driverVehicleTable.Rows.Count);

				foreach (DataRow driverVehicleRow in driverVehicleTable.Rows)
				{
					bool HasAccess =
						VehicleRightsHelper.GetEffectiveOperatorVehicleRights(
							Rights,
							ps.SessionInfo.OperatorInfo.OperatorId,
							(int)driverVehicleRow["VEHICLE"],
							(int)SystemRight.VehicleAccess
							);

					if (!HasAccess && !NoAccessRows.Contains(driverVehicleRow))
					{
						NoAccessRows.Add(driverVehicleRow);
					}
				}

				foreach (DataRow driverVehicleRow in NoAccessRows)
				{
					DatasetHelper.CascadeRemove_DriverVehicle(ds, (int)driverVehicleRow["DRIVER_ID"]);
				}

				ds.AcceptChanges();
			}
		}
		public void CheckForWaybills(IPersonalServer ps, DataSet ds)
		{
			//			DataSet dsRights = ps.GetSecurity();
			//			DatabaseSchema.MakeSchema(dsRights);

			#region WaybillHeader
			ArrayList alWaybillHeaderIdsDelete = new ArrayList();

			if (ds.Tables.Contains("WAYBILL_HEADER") && !(ds.Tables.Contains("ROUTE") && ds.Tables.Contains("VEHICLE")))
				foreach (DataRow drWaybillHeader in ds.Tables["WAYBILL_HEADER"].Rows)
				{
					if (drWaybillHeader["SCHEDULE_ID"] != DBNull.Value)
					{
						DataRow drSchedule = Rights.Tables["SCHEDULE"].Rows.Find((int)drWaybillHeader["SCHEDULE_ID"]);

						bool bHasAccessRoute = RouteRightsHelper.GetEffectiveOperatorRouteRights(Rights,
							ps.SessionInfo.OperatorInfo.OperatorId,
							(int)drSchedule["ROUTE_ID"],
							(int)SystemRight.RouteAccess);

						if (!bHasAccessRoute && !alWaybillHeaderIdsDelete.Contains((int)drWaybillHeader["WAYBILL_HEADER_ID"]))
							alWaybillHeaderIdsDelete.Add((int)drWaybillHeader["WAYBILL_HEADER_ID"]);
					}

					bool bHasAccessVehicle = VehicleRightsHelper.GetEffectiveOperatorVehicleRights(Rights,
						ps.SessionInfo.OperatorInfo.OperatorId,
						(int)drWaybillHeader["VEHICLE_ID"],
						(int)SystemRight.VehicleAccess);

					if (!bHasAccessVehicle && !alWaybillHeaderIdsDelete.Contains((int)drWaybillHeader["WAYBILL_HEADER_ID"]))
						alWaybillHeaderIdsDelete.Add((int)drWaybillHeader["WAYBILL_HEADER_ID"]);
				}
			#endregion // WaybillHeader

			#region Waybill
			ArrayList alWaybillIdsDelete = new ArrayList();

			if (ds.Tables.Contains("WAYBILL") && !ds.Tables.Contains("DRIVER"))
				foreach (DataRow drWaybill in ds.Tables["WAYBILL"].Rows)
				{
					bool bHasAccessDriver = DriverRightsHelper.GetEffectiveOperatorDriverRights(Rights,
						ps.SessionInfo.OperatorInfo.OperatorId,
						(int)drWaybill["DRIVER_ID"],
						(int)SystemRight.DriverAccess);

					if (!bHasAccessDriver && !alWaybillIdsDelete.Contains((int)drWaybill["WAYBILL_ID"]))
						alWaybillIdsDelete.Add((int)drWaybill["WAYBILL_ID"]);
				}
			#endregion // Waybill

			#region Period
			ArrayList alPeriodIdsDelete = new ArrayList();

			if (ds.Tables.Contains("PERIOD") && !ds.Tables.Contains("ROUTE"))
				foreach (DataRow drPeriod in ds.Tables["PERIOD"].Rows)
					if (drPeriod["ROUTE"] != DBNull.Value)
					{
						bool bHasAccessRoute = RouteRightsHelper.GetEffectiveOperatorRouteRights(Rights,
							ps.SessionInfo.OperatorInfo.OperatorId,
							(int)drPeriod["ROUTE"],
							(int)SystemRight.RouteAccess);

						if (!bHasAccessRoute && !alPeriodIdsDelete.Contains((int)drPeriod["PERIOD_ID"]))
							alPeriodIdsDelete.Add((int)drPeriod["PERIOD_ID"]);
					}
			#endregion // Period

			#region Schedule
			ArrayList alScheduleIdsDelete = new ArrayList();

			if (ds.Tables.Contains("SCHEDULE") && !ds.Tables.Contains("ROUTE"))
				foreach (DataRow drSchedule in ds.Tables["SCHEDULE"].Rows)
				{
					bool bHasAccessRoute = RouteRightsHelper.GetEffectiveOperatorRouteRights(Rights,
						ps.SessionInfo.OperatorInfo.OperatorId,
						(int)drSchedule["ROUTE_ID"],
						(int)SystemRight.RouteAccess);

					if (!bHasAccessRoute && !alScheduleIdsDelete.Contains((int)drSchedule["SCHEDULE_ID"]))
						alScheduleIdsDelete.Add((int)drSchedule["SCHEDULE_ID"]);
				}
			#endregion // Schedule

			#region  Fill hash WaybillHeadersMayBeDelete
			Hashtable hWaybillHeadersMayBeDelete = new Hashtable();

			for (int i = 0; i < alWaybillIdsDelete.Count; i++)
			{
				DataRow[] drWaybillArray = ds.Tables["WAYBILL"].Select("WAYBILL_ID = " + ((int)alWaybillIdsDelete[i]));

				if (drWaybillArray.Length > 0)
				{
					DataRow[] drWbTripArray = ds.Tables["WB_TRIP"].Select("WAYBILL_ID = " + ((int)drWaybillArray[0]["WAYBILL_ID"]));

					foreach (DataRow drWbTrip in drWbTripArray)
					{
						if (drWbTrip["WAYBILL_HEADER_ID"] != DBNull.Value)
						{
							DataRow[] drWaybillHeaderArray = ds.Tables["WAYBILL_HEADER"].Select("WAYBILL_HEADER_ID = " + ((int)drWbTrip["WAYBILL_HEADER_ID"]));

							if (drWaybillHeaderArray.Length > 0)
								hWaybillHeadersMayBeDelete[(int)drWaybillHeaderArray[0]["WAYBILL_HEADER_ID"]] = drWaybillHeaderArray[0];
						}
					}
				}
			}
			#endregion  // Fill hash WaybillHeadersMayBeDelete

			#region Fill hash WaybillsMayBeDelete
			Hashtable hWaybillsMayBeDelete = new Hashtable();

			for (int i = 0; i < alWaybillHeaderIdsDelete.Count; i++)
			{
				DataRow[] drWaybillHeaderArray = ds.Tables["WAYBILL_HEADER"].Select("WAYBILL_HEADER_ID = " + ((int)alWaybillHeaderIdsDelete[i]));

				if (drWaybillHeaderArray.Length > 0)
				{
					DataRow[] drWbTripArray = ds.Tables["WB_TRIP"].Select("WAYBILL_HEADER_ID = " + ((int)drWaybillHeaderArray[0]["WAYBILL_HEADER_ID"]));

					foreach (DataRow drWbTrip in drWbTripArray)
					{
						if (drWbTrip["WAYBILL_ID"] != DBNull.Value)
						{
							DataRow[] drWaybillArray = ds.Tables["WAYBILL"].Select("WAYBILL_ID = " + ((int)drWbTrip["WAYBILL_ID"]));

							if (drWaybillArray.Length > 0)
								hWaybillsMayBeDelete[(int)drWaybillArray[0]["WAYBILL_ID"]] = drWaybillArray[0];
						}
					}
				}
			}
			#endregion // Fill hash  WaybillsMayBeDelete

			#region Delete
			foreach (int iWaybillHeaderId in alWaybillHeaderIdsDelete)
			{
				DataRow[] drWaybillHeaderArray = ds.Tables["WAYBILL_HEADER"].Select("WAYBILL_HEADER_ID = " + iWaybillHeaderId);

				if (drWaybillHeaderArray.Length > 0)
				{
					DataRow[] drWbTripArray = ds.Tables["WB_TRIP"].Select("WAYBILL_HEADER_ID = " + ((int)drWaybillHeaderArray[0]["WAYBILL_HEADER_ID"]));

					for (int i = 0; i < drWbTripArray.Length; i++)
						drWbTripArray[i].Delete();

					DataRow[] drWaybillheaderWaybillmarkArray = ds.Tables["WAYBILLHEADER_WAYBILLMARK"].Select("WAYBILLHEADER_ID = " + ((int)drWaybillHeaderArray[0]["WAYBILL_HEADER_ID"]));

					for (int i = 0; i < drWaybillheaderWaybillmarkArray.Length; i++)
						drWaybillheaderWaybillmarkArray[i].Delete();

					drWaybillHeaderArray[0].Delete();
				}
			}

			foreach (int iWaybillId in alWaybillIdsDelete)
			{
				DataRow[] drWaybillArray = ds.Tables["WAYBILL"].Select("WAYBILL_ID = " + iWaybillId);

				if (drWaybillArray.Length > 0)
				{
					DataRow[] drWbTripArray = ds.Tables["WB_TRIP"].Select("WAYBILL_ID = " + ((int)drWaybillArray[0]["WAYBILL_ID"]));

					for (int i = 0; i < drWbTripArray.Length; i++)
						drWbTripArray[i].Delete();

					drWaybillArray[0].Delete();
				}
			}

			foreach (DataRow drWaybillHeader in hWaybillHeadersMayBeDelete.Values)
				if (drWaybillHeader.RowState != DataRowState.Deleted && drWaybillHeader.RowState != DataRowState.Detached)
				{
					DataRow[] drWbTripArray = ds.Tables["WB_TRIP"].Select("WAYBILL_HEADER_ID = " + ((int)drWaybillHeader["WAYBILL_HEADER_ID"]));

					if (drWbTripArray.Length == 0)
					{
						DataRow[] drWaybillheaderWaybillmarkArray = ds.Tables["WAYBILLHEADER_WAYBILLMARK"].Select("WAYBILLHEADER_ID = " + ((int)drWaybillHeader["WAYBILL_HEADER_ID"]));

						for (int i = 0; i < drWaybillheaderWaybillmarkArray.Length; i++)
							drWaybillheaderWaybillmarkArray[i].Delete();

						drWaybillHeader.Delete();
					}
				}

			foreach (DataRow drWaybill in hWaybillsMayBeDelete.Values)
				if (drWaybill.RowState != DataRowState.Deleted && drWaybill.RowState != DataRowState.Detached)
				{
					DataRow[] drWbTripArray = ds.Tables["WB_TRIP"].Select("WAYBILL_ID = " + ((int)drWaybill["WAYBILL_ID"]));

					if (drWbTripArray.Length == 0)
						drWaybill.Delete();
				}

			foreach (int iPeriodId in alPeriodIdsDelete)
			{
				DataRow drPeriod = ds.Tables["PERIOD"].Rows.Find(iPeriodId);

				if (drPeriod != null)
					drPeriod.Delete();
			}

			foreach (int iScheduleId in alScheduleIdsDelete)
				DatasetHelper.CascadeRemove_Schedule(ds, iScheduleId);
			#endregion // Delete

			ds.AcceptChanges();
		}
		public void CheckForJournal(IPersonalServer ps, DataSet ds)
		{
			if (ds.Tables.Contains("JOURNAL"))
			{
				//				DataSet dsRights = ps.GetSecurity();
				//				DatabaseSchema.MakeSchema(dsRights);

				ArrayList RowsJournalForDelete = new ArrayList();

				foreach (DataRow drJournal in ds.Tables["JOURNAL"].Rows)
				{
					bool HasAccessRoute = true;
					bool HasAccessVehicle = true;
					bool HasAccessDriver = true;

					#region Wayout

					if (drJournal["WAYOUT"] != DBNull.Value)
					{
						DataRow drWayout = Rights.Tables["WAYOUT"].Rows.Find((int)drJournal["WAYOUT"]);

						HasAccessRoute = RouteRightsHelper.GetEffectiveOperatorRouteRights(
							Rights,
							ps.SessionInfo.OperatorInfo.OperatorId,
							(int)drWayout["ROUTE"],
							(int)SystemRight.RouteAccess
							);
					}

					#endregion // Wayout

					#region Vehicle

					if (drJournal["VEHICLE"] != DBNull.Value)
					{
						HasAccessVehicle = VehicleRightsHelper.GetEffectiveOperatorVehicleRights(
							Rights,
							ps.SessionInfo.OperatorInfo.OperatorId,
							(int)drJournal["VEHICLE"],
							(int)SystemRight.VehicleAccess
							);
					}

					#endregion // Vehicle

					#region Driver

					if (drJournal["DRIVER"] != DBNull.Value)
					{
						HasAccessDriver = DriverRightsHelper.GetEffectiveOperatorDriverRights(
							Rights,
							ps.SessionInfo.OperatorInfo.OperatorId,
							(int)drJournal["DRIVER"],
							(int)SystemRight.DriverAccess
							);
					}

					#endregion // Driver

					if (!HasAccessRoute || !HasAccessVehicle || !HasAccessDriver)
					{
						RowsJournalForDelete.Add(drJournal);
					}
					else
					{
						Debug.WriteLine("JOURNAL row OK");
					}

				}

				#region JournalVehicle
				ArrayList alJournalVehicleIdsDelete = new ArrayList();

				if (ds.Tables.Contains("JOURNAL_VEHICLE") && !ds.Tables.Contains("VEHICLE"))
					foreach (DataRow drJournalVehicle in ds.Tables["JOURNAL_VEHICLE"].Rows)
					{
						bool bHasAccessVehicle = VehicleRightsHelper.GetEffectiveOperatorVehicleRights(Rights,
							ps.SessionInfo.OperatorInfo.OperatorId,
							(int)drJournalVehicle["VEHICLE"],
							(int)SystemRight.VehicleAccess);

						if (!bHasAccessVehicle && !alJournalVehicleIdsDelete.Contains((int)drJournalVehicle["JOURNAL_VEHICLE_ID"]))
							alJournalVehicleIdsDelete.Add((int)drJournalVehicle["JOURNAL_VEHICLE_ID"]);
					}
				#endregion // JournalVehicle

				#region JournalDriver
				ArrayList alJournalDriverIdsDelete = new ArrayList();

				if (ds.Tables.Contains("JOURNAL_DRIVER") && !ds.Tables.Contains("DRIVER"))
					foreach (DataRow drJournalDriver in ds.Tables["JOURNAL_DRIVER"].Rows)
					{
						bool bHasAccessDriver = DriverRightsHelper.GetEffectiveOperatorDriverRights(Rights,
							ps.SessionInfo.OperatorInfo.OperatorId,
							(int)drJournalDriver["DRIVER"],
							(int)SystemRight.DriverAccess);

						if (!bHasAccessDriver && !alJournalDriverIdsDelete.Contains((int)drJournalDriver["JOURNAL_DRIVER_ID"]))
							alJournalDriverIdsDelete.Add((int)drJournalDriver["JOURNAL_DRIVER_ID"]);
					}
				#endregion // JournalDriver

				foreach (DataRow dr in RowsJournalForDelete)
				{
					dr.Delete();
				}

				foreach (int iJournalVehicleId in alJournalVehicleIdsDelete)
				{
					DataRow drJournalVehicle = ds.Tables["JOURNAL_VEHICLE"].Rows.Find(iJournalVehicleId);

					if (drJournalVehicle != null)
						drJournalVehicle.Delete();
				}

				foreach (int iJournalDriverId in alJournalDriverIdsDelete)
				{
					DataRow drJournalDriver = ds.Tables["JOURNAL_DRIVER"].Rows.Find(iJournalDriverId);

					if (drJournalDriver != null)
						drJournalDriver.Delete();
				}
			}

			ds.AcceptChanges();
		}
		public void CheckForDepartments(IPersonalServer ps, DataSet ds)
		{
			if (SystemRightsHelper.GetEffectiveOperatorRight(Rights,
				ps.SessionInfo.OperatorInfo.OperatorId,
				(int)SystemRight.ServerAdministration))
				return;

			if (!ds.Tables.Contains("DEPARTMENT"))
				return;

			ArrayList alDepartmentIds = new ArrayList();

			foreach (DataRow drDepartment in ds.Tables["DEPARTMENT"].Rows)
			{
				bool bHasAccessDepartment = DepartmentRightsHelper.GetEffectiveOperatorDepartmentRights(Rights,
					ps.SessionInfo.OperatorInfo.OperatorId,
					(int)drDepartment["DEPARTMENT_ID"],
					(int)SystemRight.DepartmentsAccess);

				if (!bHasAccessDepartment && !alDepartmentIds.Contains((int)drDepartment["DEPARTMENT_ID"]))
					alDepartmentIds.Add((int)drDepartment["DEPARTMENT_ID"]);
			}

			foreach (int iDepartment in alDepartmentIds)
				DatasetHelper.CascadeRemove_Department(ds, iDepartment);

			ds.AcceptChanges();

			#region BRIGADE

			if (ds.Tables.Contains("BRIGADE"))
				foreach (DataRow drBrigade in ds.Tables["BRIGADE"].Rows)
				{
					if (drBrigade["WAYOUT"] != DBNull.Value)
					{
						if (ds.Tables["WAYOUT"].Rows.Find((int)drBrigade["WAYOUT"]) == null)
						{
							drBrigade.Delete();
						}
					}
				}


			#endregion // BRIGADE

			#region SCHEDULE_DETAIL_INFO

			if (ds.Tables.Contains("SCHEDULE_DETAIL_INFO"))
				foreach (DataRow drSDI in ds.Tables["SCHEDULE_DETAIL_INFO"].Rows)
				{
					if (ds.Tables["SCHEDULE_DETAIL"].Rows.Find((int)drSDI["SCHEDULE_DETAIL_ID"]) == null)
					{
						drSDI.Delete();
					}
				}


			#endregion // SCHEDULE_DETAIL_INFO

			ds.AcceptChanges();
		}
		public void CheckWorkTime(IPersonalServer ps, DataSet ds)
		{
			if (ds.Tables.Contains("WORK_TIME") && !ds.Tables.Contains("DRIVER"))
			{
				DataTable WorkTimeTable = ds.Tables["WORK_TIME"];

				List<DataRow> NoAccessRows = new List<DataRow>(WorkTimeTable.Rows.Count);

				foreach (DataRow workTimeRow in WorkTimeTable.Rows)
				{
					bool HasAccess =
						DriverRightsHelper.GetEffectiveOperatorDriverRights(
							Rights,
							ps.SessionInfo.OperatorInfo.OperatorId,
							(int)workTimeRow["DRIVER_ID"],
							(int)SystemRight.DriverAccess
							);

					if (!HasAccess && !NoAccessRows.Contains(workTimeRow))
					{
						NoAccessRows.Add(workTimeRow);
					}
				}

				foreach (DataRow workTimeRow in NoAccessRows)
				{
					DatasetHelper.CascadeRemove_WorkTime(ds, (int)workTimeRow["DRIVER_ID"]);
				}

				ds.AcceptChanges();
			}
		}
	}
}