using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Interfaces.Data;

namespace FORIS.TSS.ServerApplication.Servers
{
	internal class SessionListAdmin: Component, ISessionListAdmin
	{
		ISessionListInternal sessionList;

		#region Constructor & Dispose

		internal SessionListAdmin( ISessionListInternal sessionList )
		{
			this.sessionList = sessionList;
		}
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				this.sessionList = null;

				System.Runtime.Remoting.RemotingServices.Disconnect( this );
			}
		}

		#endregion // Constructor & Dispose

		#region Implement ISessionListAdmin

		public SessionListDataSet GetSessionList()
		{
			return this.sessionList.GetSessionList();
		}

		public ContainerListDataSet GetContainerList()
		{
			return this.sessionList.GetContainerList();
		}

		public void DeleteSession( int sessionID )
		{
			this.sessionList.DeleteSession( sessionID );
		}

		public void CollectGarbage()
		{
			GC.Collect();
			GC.WaitForPendingFinalizers();
			GC.Collect();
		}

		#endregion // Implement ISessionListAdmin
	}
}

