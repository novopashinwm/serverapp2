﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Lifetime;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Bus;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;
using FORIS.TSS.IO;
using FORIS.TSS.ServerApplication.Mail;
using FORIS.TSS.ServerApplication.Servers;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using FORIS.TSS.TransportDispatcher.Reports;
using FORIS.TSS.TransportDispatcher.Reports.ReportCollection;
using FORIS.TSS.TransportDispatcher.RuleManager;
using IPersonalServer = FORIS.TSS.BusinessLogic.IPersonalServer;

namespace FORIS.TSS.ServerApplication
{
	/// <summary> Session object </summary>
	public class PersonalServer : PersonalServerBase, IPersonalServer
	{
		#region **************************** События *******************************

		/// <summary>
		/// Событие наличия новых сообщений
		/// </summary>
		public event AnonymousEventHandler<ExecRuleServerEventArgs> ReceiveMessageEvent;

		#region Guard

		private AnonymousEventHandler<GuardEventArgs> dlgtGuard;

		public event AnonymousEventHandler<GuardEventArgs> Guard
		{
			add { this.dlgtGuard += value; } 
			remove { this.dlgtGuard -= value; }
		}

		private readonly AsyncCallback guardCallbackHandler;

		protected virtual void OnGuard( GuardEventArgs e )
		{
			if( this.dlgtGuard != null )
			{
				Delegate[] Handlers = this.dlgtGuard.GetInvocationList();

				foreach( AnonymousEventHandler<GuardEventArgs> handler in Handlers )
				{
					handler.BeginInvoke( e, this.guardCallbackHandler, handler );
				}
			}
		}

		private void Guard_Callback( IAsyncResult asyncResult )
		{
			AnonymousEventHandler<GuardEventArgs> handler =
				(AnonymousEventHandler<GuardEventArgs>)asyncResult.AsyncState;

			try
			{
				handler.EndInvoke( asyncResult );
			}
			catch( Exception )
			{
				this.dlgtGuard -= handler;
			}
		}

		#endregion // Guard

		#region Receive

		private AnonymousEventHandler<ReceiveEventArgs> dlgtReceive;

		public event AnonymousEventHandler<ReceiveEventArgs> Receive
		{
			add { this.dlgtReceive += value; }
			remove { this.dlgtReceive -= value; }
		}

		private readonly AsyncCallback receiveCallbackHandler;
		
		protected virtual void OnReceive( ReceiveEventArgs e )
		{
			if( this.dlgtReceive != null )
			{
				Delegate[] Handlers = this.dlgtReceive.GetInvocationList();

				foreach( AnonymousEventHandler<ReceiveEventArgs> handler in Handlers )
				{
					handler.BeginInvoke( e, this.receiveCallbackHandler, handler );
				}
			}
		}

		private void Receive_Callback( IAsyncResult asyncResult )
		{
			AnonymousEventHandler<ReceiveEventArgs> handler =
				(AnonymousEventHandler<ReceiveEventArgs>)asyncResult.AsyncState;

			try
			{
				handler.EndInvoke( asyncResult );
			}
			catch( Exception )
			{
				this.dlgtReceive -= handler;
			}
		}

		#endregion // Receive

		#region Notify

		private AnonymousEventHandler<NotifyEventArgs> dlgtNotify;

		public event AnonymousEventHandler<NotifyEventArgs> Notify
		{
			add { this.dlgtNotify += value; }
			remove { this.dlgtNotify -= value; }
		}

		private readonly AsyncCallback notifyCallbackHandler;

		protected virtual void OnNotify( NotifyEventArgs e )
		{
			if( this.dlgtNotify != null )
			{
				Delegate[] Handlers = this.dlgtNotify.GetInvocationList();

				foreach( AnonymousEventHandler<NotifyEventArgs> handler in Handlers )
				{
					handler.BeginInvoke( e, this.notifyCallbackHandler, handler );
				}
			}
		}

		private void Notify_Callback( IAsyncResult asyncResult )
		{
			AnonymousEventHandler<NotifyEventArgs> handler =
				(AnonymousEventHandler<NotifyEventArgs>)asyncResult.AsyncState;

			try
			{
				handler.EndInvoke( asyncResult );
			}
			catch( Exception )
			{
				this.dlgtNotify -= handler;
			}
		}

		#endregion // Notify

		#region ReceiveLog

		private AnonymousEventHandler<ReceiveLogEventArgs> dlgtReceiveLog;

		public event AnonymousEventHandler<ReceiveLogEventArgs> ReceiveLog
		{
			add { this.dlgtReceiveLog += value; }
			remove { this.dlgtReceiveLog -= value; }
		}

		private readonly AsyncCallback receiveLogCallbackHandler;

		protected virtual void OnReceiveLog( ReceiveLogEventArgs e )
		{
			if( this.dlgtReceiveLog != null )
			{
				Delegate[] Handlers = this.dlgtReceiveLog.GetInvocationList();

				foreach( AnonymousEventHandler<ReceiveLogEventArgs> handler in Handlers )
				{
					handler.BeginInvoke( e, this.receiveLogCallbackHandler, handler );
				}
			}
		}

		private void ReceiveLog_Callback( IAsyncResult asyncResult )
		{
			AnonymousEventHandler<ReceiveLogEventArgs> handler =
				(AnonymousEventHandler<ReceiveLogEventArgs>)asyncResult.AsyncState;

			try
			{
				handler.EndInvoke( asyncResult );
			}
			catch( Exception )
			{
				this.dlgtReceiveLog -= handler;
			}
		}

		#endregion // ReceiveLog

		/// <summary>
		/// делегат события наличия данных у терминала сервера
		/// </summary>
		ReceiveEventHandler receiveEventHandler = null;


		#endregion // **************************** События *******************************

		#region **************************** Переменные *******************************

		/// <summary>
		/// текущая команда сервера
		/// </summary>
		protected ICommand currentCommand = null;

		/// <summary>
		/// название сервера
		/// </summary>
		protected string name = String.Empty;

		/// <summary>
		/// хэш-таблица отслеживаемых мобильных объектов с последней информацией
		/// о их состоянии
		/// </summary>
		//protected Hashtable mobilUnits = Hashtable.Synchronized(new Hashtable(200));
		private readonly DictionaryWithEvents<int, IMobilUnit> mobilUnits = 
			new DictionaryWithEvents<int, IMobilUnit>();

		/// <summary>
		/// Список для получения истории
		/// </summary>
		private readonly SortedList historyList = SortedList.Synchronized(new SortedList(200));
		
		/// <summary>
		/// Очередь данных 
		/// </summary>

		/// <summary>
		/// таймер для отсылки очереди сообщений клиенту
		/// </summary>
		protected System.Threading.Timer timer;

		/// <summary>
		/// максимальный размер очереди сообщений
		/// </summary>
		protected int queueSize = 10;

		/// <summary>
		/// интервал времени, по истечении которого происходит принудительная очистка очереди сообщений
		/// </summary>
		protected int timerInterval = 5000;

		#endregion // **************************** Переменные *******************************

		#region **************************** Свойства *******************************

		/// <summary>
		/// Название сервера
		/// </summary>
		public string Name
		{
			get { return name; }
		}

		/// <summary>
		/// Текущая команда сервера
		/// </summary>
		public virtual ICommand CurrentCommand
		{
			get { return currentCommand; }
		}

		#endregion // **************************** Свойства *******************************

		#region Controls & Components

		private new System.ComponentModel.IContainer components;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public PersonalServer()
		{
			this.guardCallbackHandler = new AsyncCallback( this.Guard_Callback );
			this.receiveCallbackHandler = new AsyncCallback( this.Receive_Callback );
			this.notifyCallbackHandler = new AsyncCallback( this.Notify_Callback );
			this.receiveLogCallbackHandler = new AsyncCallback( this.ReceiveLog_Callback );

			InitializeComponent();

			//if( !this.DesignMode )
			{
				// пустой экземпляр последней команды
				currentCommand = new FORIS.TSS.Terminal.Command();
				//Server.Instance().ReceiveEvent += new ReceiveEventHandler(OnReceive);
				Server.Instance().NotifyEvent += new NotifyEventHandler( Terminal_NotifyEvent );
				Server.Instance().GuardEvent += new GuardEventHandler( PersonalServer_GuardEvent );
			}
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( this.components != null )
				{
					this.components.Dispose();
				}

				this.dlgtGuard = null;
				this.dlgtReceive = null;
				this.dlgtNotify = null;
				this.dlgtReceiveLog = null;

				this.Close();

				//if( !this.DesignMode )
				{
					Server.Instance().NotifyEvent -= new NotifyEventHandler( Terminal_NotifyEvent );
					Server.Instance().GuardEvent -= new GuardEventHandler( PersonalServer_GuardEvent );
				}

				RemotingServices.Disconnect( this );

				this.dlgtClosing = null;
				this.dlgtClosed = null;
			}

			base.Dispose( disposing );
		}
		
		#endregion // Constructor & Dispose

		public void Init( SessionList sessionList, ISessionInfo sessionInfo)
		{
			this.state = PersonalServerState.Initialized;


			base.Init( server, sessionInfo );
		}

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
		}

		#endregion // Component Designer generated code

		public bool DisconnectedByClient = false;

		private PersonalServerState state = PersonalServerState.Undefined;
		[Browsable(false)]
		public PersonalServerState State
		{
			get { return this.state; }
		}


		public string ApplicationName
		{
			get { throw new NotImplementedException(); }
		}

		public override string GetVehicleName(int? operatorId, int vehicleId)
		{
			throw new NotImplementedException();
		}

		public override string GetAsidName(int asidId)
		{
			throw new NotImplementedException();
		}

		/// <summary> Закрывает сессию </summary>
		public void Close()
		{
			if( this.state != PersonalServerState.Closed )
			{
				this.OnClosing();

				this.state = PersonalServerState.Closed;

				this.OnClosed();

				/* Нет возможности генерировать события 
				 * после вызова Dispose()
				 */

				this.Dispose();
			}
		}

		public void ExtremeDisconnect()
		{
			// allow next login
			server.ExtremeDeleteSession(this);
		}
		
		#region IPersonalServer Members

		/// <summary>
		/// Передача команд от клиентов соответствующим терминалам
		/// </summary>
		/// <param name="type">type id</param>
		/// <param name="ids">list of target id</param>
		public void SendCommand(CmdType type, params int[] ids)
		{
			SendCommand(type, null, ids);
		}

		/// <summary>
		/// Передача команд от клиентов соответствующим терминалам
		/// </summary>
		/// <param name="type">type id</param>
		/// <param name="param">named params</param>
		/// <param name="ids">list of target id</param>
		public void SendCommand(CmdType type, PARAMS param, params int[] ids)
		{
			if(ids.Length == 0) throw new ArgumentException("Список пуст", "ids");

			// если заказано получение истории, то надо очистить список исторических позиций
			if(type == CmdType.GetLog)
				lock( this.historyList )
				{
					historyList.Clear();
				}

			switch(type)
			{
					#region Получение истории через CmdType.GetLogFromDB надо убрать

			case CmdType.GetLogFromDB: // получаем историю из базы данных
				throw new NotSupportedException("Команда CmdType.GetLogFromDB больше не поддерживается." + 
					"Пользуйтесь новым способом получения истории из БД.\n" + 
					"Обратитесь к разработчику");
//				Debug.WriteLine(" получаем историю из базы данных ");
//
//				int from = (int)param["fromtime"];
//				int to = (int)param["totime"];
//				int interval = (int)param["interval"];
//				int count = (int)param["count"];
//				
//				if (to >= from) 
//					GetLogFromDB(from, to, ids[0], interval, count);
//
//				break;

					#endregion // Получение истории через CmdType.GetLogFromDB надо убрать

				// std case
			default:
				Logger.WriteEntry("PersonalServer.SendCommand. Creating command with type " + 
					type, LogType.Information, LogCategory.General, LogPriority.Low, 0);

				// standard command for specified command type
				IStdCommand cmd = CommandCreator.CreateCommand(type) as IStdCommand;
				Debug.Assert(cmd != null, "Unable to create command");

				// fill cmd fields
				cmd.Params = param;
				cmd.Sender = Operator;
				cmd.Context.Init(GetSecurityDataset());

				// send command to recipients
				for(int i = 0, cnt = ids.Length; i < cnt; ++i)
				{
					Logger.WriteEntry(
						"PersonalServer.SendCommand. Sending command to terminal", 
						LogType.Information, LogCategory.General, LogPriority.Low, 0);

					cmd.Target.Unique = ids[i];
					Server.Instance().DbManager.FillUnitInfo(cmd.Target);

					// get suitable terminal
					//ITerminal term = Server.TerminalManager.CreateTerminal(cmd);
					//Debug.Assert(term != null, "Unable to create terminal");

					//Logger.WriteEntry("PersonalServer.SendCommand. Terminal created with ID = "
					//    + term.ID + ", Name = " + term.Name, LogType.Information, 
					//    LogCategory.General, LogPriority.Low, 0);

					// listen events
					//Server.Instance().SubscribeTo(term);

					//term.SendCommand((ICommand)((IStdCommand)cmd.Requery()).Clone());
					var clonedCommand = (IStdCommand)((IStdCommand)cmd.Requery()).Clone();

					if (Server.Instance().TerminalManager != null)
						Server.Instance().TerminalManager.SendCommand(clonedCommand);
					else
						Trace.WriteLine("Cann't send command: " + clonedCommand.Text);
				}
				break;
			}
			
			// пытаемся убрать за собой
			GC.Collect();
		}

		public void SendCommand(CmdType type, Media mediaType, PARAMS param, params int[] ids)
		{
			if (ids.Length == 0) throw new ArgumentException("Список пуст", "ids");

			// если заказано получение истории, то надо очистить список исторических позиций
			if (type == CmdType.GetLog)
				lock (this.historyList)
				{
					historyList.Clear();
				}

			switch (type)
			{
				#region Получение истории через CmdType.GetLogFromDB надо убрать

				case CmdType.GetLogFromDB: // получаем историю из базы данных
					throw new NotSupportedException("Команда CmdType.GetLogFromDB больше не поддерживается." +
						"Пользуйтесь новым способом получения истории из БД.\n" +
						"Обратитесь к разработчику");
				//				Debug.WriteLine(" получаем историю из базы данных ");
				//
				//				int from = (int)param["fromtime"];
				//				int to = (int)param["totime"];
				//				int interval = (int)param["interval"];
				//				int count = (int)param["count"];
				//				
				//				if (to >= from) 
				//					GetLogFromDB(from, to, ids[0], interval, count);
				//
				//				break;

				#endregion // Получение истории через CmdType.GetLogFromDB надо убрать

				// std case
				default:
					Logger.WriteEntry("PersonalServer.SendCommand. Creating command with type " +
						type, LogType.Information, LogCategory.General, LogPriority.Low, 0);

					// standard command for specified command type
					IStdCommand cmd = CommandCreator.CreateCommand(type) as IStdCommand;
					Debug.Assert(cmd != null, "Unable to create command");

					// fill cmd fields
					cmd.Params = param;
					cmd.Sender = Operator;
					cmd.MediaType = mediaType;
					cmd.Context.Init(GetSecurity());

					// send command to recipients
					for (int i = 0, cnt = ids.Length; i < cnt; ++i)
					{
						Logger.WriteEntry(
							"PersonalServer.SendCommand. Sending command to terminal",
							LogType.Information, LogCategory.General, LogPriority.Low, 0);

						cmd.Target.Unique = ids[i];
						Server.Instance().DbManager.FillUnitInfo(cmd.Target);

						var clonedCommand = (IStdCommand)((IStdCommand)cmd.Requery()).Clone();

						if (Server.Instance().TerminalManager != null)
							Server.Instance().TerminalManager.SendCommand(clonedCommand);
						else
							Trace.WriteLine("Cann't send command: " + clonedCommand.Text);
					}
					break;
			}

			// пытаемся убрать за собой
			GC.Collect();
		}

		public TimeSpan LeaseRenewOnCallTime
		{
			get
			{
				#region Preconditions

				if( this.lease == null )
				{
					throw new InvalidOperationException( 
						"Call property only through remoting infrastructure"
						);
				}

				#endregion // Preconditions

				return this.lease.RenewOnCallTime;
			}
		}

		public bool IsAlive()
		{
			return true;
		}

		/// <summary>
		/// 
		/// </summary>
		public ISecurity Security
		{
			get { return null; }
		}

		/// <summary>
		/// Проверяет совпадает ли указанный 
		/// пароль с паролем оператора сессии
		/// </summary>
		/// <param name="password"></param>
		/// <returns></returns>
		public bool CheckPassword(string password)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Изменяет/задает собственный пароль оператора
		/// </summary>
		/// <param name="password"></param>
		/// <remarks>
		/// Может возникнуть исключение в случае 
		/// если пользователю не разрешено менять
		/// собственный пароль
		/// </remarks>
		public SetNewPasswordResult SetPassword(string password)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Изменяет/задает пароль оператора
		/// </summary>
		/// <param name="idOperator"></param>
		/// <param name="password"></param>
		/// <remarks>
		/// Может возникнуть исключение в случае, 
		/// если пользователю не разрешено менять 
		/// пароль указанного пользователя
		/// </remarks>
		public void SetPassword(string password, int idOperator)
		{
			throw new NotImplementedException();
		}

		public SetNewLoginResult SetNewLogin(string newLogin)
		{
			throw new NotImplementedException();
		}

		public IList<TssPrincipalInfo> GetPrincipals()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Получение списков контроля доступа объекта
		/// </summary>
		/// <param name="info"></param>
		/// <returns></returns>
		public ISecurityModel GetACL(SecureObjectInfo info)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Применение списка контрола доступа объекта
		/// </summary>
		/// <param name="info"></param>
		/// <param name="security"></param>
		public void SetACL(SecureObjectInfo info, ISecurityModel security)
		{
			throw new NotImplementedException();
		}

		public void ServerGC()
		{
			GC.Collect();
			GC.Collect();
		}

		public TssPrincipalInfo GetPrincipal()
		{
			return
				new TssPrincipalInfo(
					this.Operator.Login,
					"OPERATOR",
					this.OperatorID,
					TssPrincipalType.Operator
					);
		}

		BusinessLogic.Server.IPersonalServer BusinessLogic.Server.IPersonalServer.GetAdmin()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Returns tables for calculating access from Operators to Vehicles and Routes
		/// </summary>
		/// <returns></returns>
		public DataSet GetSecurityDataset()
		{
			return this.GetSecurity();
		}

		#endregion // IPersonalServer Members

		#region GetLogFromDB
		/// <summary>
		/// Возвращает отсортированный по времени список последних позиций ТС из БД за указанный период времени
		/// </summary>
		/// <param name="from">начало периода</param>
		/// <param name="to">конец периода</param>
		/// <param name="id">инедтификатор машины</param>
		/// <param name="interval">интервал между позициями в секундах</param>
		/// <param name="count">количество запрашиваемых позиций</param>
		/// <returns>отсортированный по времени список последних позиций ТС</returns>
		public ReceiveLogEventArgs GetLogFromDB(int from, int to, int id, int interval, int count)
		{
			int iLogLength;
			string garageNumber = String.Empty;
			
			SortedList sl = Server.Instance().DbManager.GetLogNew(from, to, id, interval, count, out iLogLength, out garageNumber);

			ReceiveLogEventArgs args = new ReceiveLogEventArgs(sl, id, iLogLength, count, garageNumber);

			#region Группировка для WEB-клиента

			//if(true) 
			//{
			//    // "константы"					
			//    int stopTimeSpan = 300; // секунды определяющие максимальное время стоянки
			//    int slowSpeed = 5; // минимальная скорость движения

			//    // текущие параметры
			//    IMobilUnit curMU = null;
			//    IMobilUnit lastMU = null;
			//    GroupingMobileUnit curGrouping = null;
			//    int curStopDelay = 0; // время стоянки

			//    // массив для хранения параметров группировки
			//    IEnumerator enumerator = sl.GetEnumerator();
			//    args.Grouping = new List<GroupingMobileUnit>(1);

			//    while (enumerator.MoveNext())
			//    {
			//        DictionaryEntry entry = (DictionaryEntry)enumerator.Current;

			//        curMU = (IMobilUnit)entry.Value;

			//        // определяем к какому типу группировки надо причислить текущую позицию
			//        GroupingType groupType;
			//        if (curMU.Speed >= slowSpeed)
			//            groupType = GroupingType.Motion;
			//        else if (curMU.Speed > 0 && curMU.Speed < slowSpeed)
			//            groupType = GroupingType.Slow;
			//        else
			//            groupType = GroupingType.Stop;

			//        // формирование текущей группировка
			//        if(curGrouping == null || curGrouping.GroupingType != groupType)
			//        {
			//            curGrouping = new GroupingMobileUnit(groupType);
			//            args.Grouping.Add(curGrouping);
			//        }

			//        // формирование узла
			//        GroupingMobileUnitNode node = new GroupingMobileUnitNode(curMU.Time, null, curMU);

			//        // добавление узла в текущую группировку
			//        curGrouping.Add(node);

			//        lastMU = curMU;
			//    }
			//}

			#endregion // Группировка для WEB-клиента

			// пытаемся убрать за собой
			GC.Collect();
			
			return args;
		}

		#region NotifyReceiveLogEvents(ReceiveLogEventArgs args)
		/// <summary>
		/// Уведомление клиентов о событии наличия принятых сервером данных о истории движения
		/// </summary>
		/// <param name="args">принятые данные</param>
		private void NotifyReceiveLogEvents(ReceiveLogEventArgs args)
		{
			this.OnReceiveLog( args );
		}


		#endregion // NotifyReceiveLogEvents(ReceiveLogEventArgs args)

		#endregion // GetLogFromDB

		#region GetVehiclesForRoute(int route)
		public List<int> GetVehiclesForRoute(int route)
		{
			return Server.Instance().GetVehiclesForRoute(route);
		}
		#endregion // GetVehiclesForRoute(int route)

		#region SetMonitoreeGroup(int [] group)

		// TODO: Безобразный код - необходимо определить способ обновления списка отслеживания 
		/// <summary>
		/// Установка группы мобильных объектов для отслеживания в реальном времени
		/// </summary>
		/// <param name="group">массив идентификаторов мобильных объектов</param>
		/// <param name="bIsRefresh">clear old cache</param>
		/// мобильных объектов
		/// <param name="sendLastPositions"></param>
		public IDictionary<int,IMobilUnit> SetMonitoreeGroup( int[] group, bool bIsRefresh, bool sendLastPositions )
		{
			#region Log trace

			Trace.WriteLine(
				String.Format(
					"User {0}: SetMonitoreeGroup -> group.Length = {1}",
					this.Login,
					group.Length
					)
				);

			#endregion // Log trace 

			IDictionary<int, IMobilUnit> LastPositions = new Dictionary<int, IMobilUnit>();

			if( group.Length == 0 )
			{
				if( this.receiveEventHandler != null )
				{
					#region Unsubscribe server Receive events

					Server.Instance().ReceiveEvent -= this.receiveEventHandler;

					this.receiveEventHandler = null;

					lock( this.mobilUnits )
					{
						this.mobilUnits.Clear();
					}

					return null;

					#endregion // Unsubscribe server Receive event
				}
			}
			else
			{
				#region group -> units

				//List<int> units = new List<int>(group.Length);
				
				/* Хэши последних позиций по unique (иденитификатору ТС)
				 * 
				// обработка запроса оператора на фильтрацию информации принятой сервером
				// по id мобильного объекта
				lock( group )
				{
					units.AddRange(
						Server.Instance().DbManager.GetUnitNumberByID( group )
						);
				}
				 */

				#endregion // group -> units


				#region Sync this.mobilUnits with group

				lock( this.mobilUnits )
				{
					// надо обновить хэш
					if (bIsRefresh)
						mobilUnits.Clear();

					List<int> deleteUniques = new List<int>( this.mobilUnits.Count );

					#region Build deleteUniques

					{
						List<int> uniques = new List<int>( group );

						foreach( int unique in this.mobilUnits.Keys )
							if( !uniques.Contains( unique ) )
							{
								deleteUniques.Add( unique );
							}
					}

					#endregion // Build deleteUniques

					#region Sync deleteUniques -> this.mobilUnits

					foreach( int unique in deleteUniques )
					{
						this.mobilUnits.Remove( unique );
					}

					#endregion // Sync deleteUniques -> this.mobilUnits

					List<int> newUniques = new List<int>( group.Length - this.mobilUnits.Count );

					#region Build newUnits & put to this.mobilUnits

					foreach( int unique in group )
					{
						if( !this.mobilUnits.ContainsKey( unique ) )
						{
							newUniques.Add( unique );

							this.mobilUnits.Add( unique, null );
						}
					}

					#endregion // Build newUnits & put to this.mobilUnits

					LastPositions = Server.Instance().GetLastPositions( newUniques.ToArray() );

					#region Apply LastPositions to this.mobilUnits

					foreach( int unique in LastPositions.Keys )
					{
						Trace.Assert( this.mobilUnits.ContainsKey( unique ) );

						this.mobilUnits[ unique ] = LastPositions[ unique ];

						/* Вероятно тут следует сгенерировать событие 
						 * получения позиций для новых наблюдаемых.
						 * Сейчас это делается как-то через ...клиента.
						 */
					}

					#endregion // Apply LastPositions to this.mobilUnits
				}

				#endregion //  Sync this.mobilUnits with group

				#region Subscribe server Receive events

				if( this.receiveEventHandler == null )
				{
					this.receiveEventHandler = new ReceiveEventHandler( Server_ReceiveEvent );
					
					//if( Server.Instance().Terminal != null )
					{
						Server.Instance().ReceiveEvent += this.receiveEventHandler;
					}
				}

				#endregion // Subscribe server Receive event
			}

			return LastPositions;
		}
		
		#endregion // SetMonitoreeGroup(int [] group)

		#region GetLastPositions(int[] iArrayGroup)
		
		public IDictionary<int, IMobilUnit> GetLastPositions(int[] iArrayGroup)
		{
			return Server.Instance().GetLastPositions(iArrayGroup);
		}
			
		#endregion // GetLastPositions(int[] iArrayGroup)

		private void MessageHandler(ExecRuleServerEventArgs args)
		{
			if (ReceiveMessageEvent == null)
				return;

			Delegate[] invkList = ReceiveMessageEvent.GetInvocationList();
			IEnumerator ie = invkList.GetEnumerator();
			//
			while (ie.MoveNext())
			{
				AnonymousEventHandler<ExecRuleServerEventArgs> handler = (AnonymousEventHandler<ExecRuleServerEventArgs>)ie.Current;

				handler.BeginInvoke(args, new AsyncCallback(CallBackReceiveMessageEvent), handler);  //null, null);
			}
		}

		private void CallBackReceiveMessageEvent(IAsyncResult ar)
		{
			AnonymousEventHandler<ExecRuleServerEventArgs> handler =
				(AnonymousEventHandler<ExecRuleServerEventArgs>)ar.AsyncState;

			try
			{
				handler.EndInvoke(ar);
			}
			catch (Exception e)
			{
				Trace.WriteLine(e.ToString());
			}
		}

		#region Handle server Receive event

#if DEBUG
		/// <summary>
		/// Статистика количества пакетов по ТС
		/// </summary>
		readonly Dictionary<int, int> stat = new Dictionary<int,int>();
#endif
		
		/// <summary>
		/// Событие наличия принятых терминалом данных 
		/// </summary>
		/// <param name="args">принятые терминалом данные</param>
		private void Server_ReceiveEvent( ReceiveEventArgs args )
		{
			// TODO: Логику обработки информации приходящей от сервера
			try
			{
				lock( this.mobilUnits )
				{
					#region Statistic
#if DEBUG
					foreach( IMobilUnit mobilUnit in args.MobilUnits )
					{
						if( !this.stat.ContainsKey( mobilUnit.Unique ) )
						{
							this.stat.Add( mobilUnit.Unique, 1 );
						}
						else
						{
							this.stat[ mobilUnit.Unique ]++;
						}
					}
#endif
					#endregion // Statistic

					/*astefanov
					заменил использование args.MobilUnits[0]
					на IMobilUnit ardsMU в цикле по массиву аргумента события
					
					надо доделать фильтр по времени, чтобы в mobilUnits попадали только последние данные!
					*/
					List<IMobilUnit> list = new List<IMobilUnit>(0);

					foreach( IMobilUnit mobilUnit in args.MobilUnits )
						if( this.mobilUnits.ContainsKey( mobilUnit.Unique ) )
						{
							var prevUnit = this.mobilUnits[mobilUnit.Unique];

							//Если пришла позиция без координат, 
							//то кладем ее в коллекцию только если предыдущая позиция с координатами 
							//была более 15 минут назад
							if (mobilUnit.ValidPosition || !prevUnit.ValidPosition || (mobilUnit.Time - prevUnit.Time) > 15*60)
							{
								// добавление последней информации в хэш мобильных устройств
								this.mobilUnits[mobilUnit.Unique] = mobilUnit;
							}

							switch(mobilUnit.cmdType)
							{
								case CmdType.GetLog:
									lock (historyList.SyncRoot)
									{
										if (!historyList.ContainsKey(mobilUnit.Time))
											historyList.Add(mobilUnit.Time, mobilUnit);
									} 
									break;
								default:
									list.Add(mobilUnit);
									break;
							}
						}

					// Отправка новых позиций клиенту
					if(list.Count > 0)
						this.OnReceive(new ReceiveEventArgs(list.ToArray()));

				}
			}
			catch(Exception e)
			{
				Trace.WriteLine("Ошибка вызова call-back функции клиента при обработке информации принятой сервером." + e.ToString());
			}
		}
		
		#endregion // Handle server Receive event

		#region NotifyReceiveEvents(ReceiveEventArgs args)
		/// <summary>
		/// Уведомление клиентов о событии наличия принятых сервером данных
		/// </summary>
		/// <param name="args">принятые данные</param>
		private void NotifyReceiveEvents(ReceiveEventArgs args)
		{
			this.OnReceive( args );
		}



		#endregion // NotifyReceiveEvents(ReceiveEventArgs args)

		#region Вызов функций сервера с клиента
//		/// <summary>
//		/// Даставка последних позиций
//		/// </summary>
//		public ArrayList GetMonitoreeGroup(int [] group, bool bIsRefresh)
//		{
//			ArrayList lastPositions = null;
//			try
//			{
//				if(group.Length > 0)
//					lastPositions = Server.Instance().GetLastPositions(group);
//				else
//					lastPositions = new ArrayList(0);
//			}
//			catch(Exception e)
//			{
//				Trace.WriteLine("error: GetMonitoreeGroup -> " + e.Message);
//			}
//			return lastPositions;
//		}

				/// <summary>
		/// Даставка новых позиций
		/// </summary>
		public ArrayList GetNewMonitoreeGroup()
		{
			ArrayList Result = new ArrayList(this.mobilUnits.Count);

			try
			{
				lock (this.mobilUnits)
				{
					foreach (IMobilUnit mu in mobilUnits.Values)
					{
						if (mu != null)
							Result.Add(mu);
					}

					foreach (int unique in new List<int>(mobilUnits.Keys))
						mobilUnits[unique] = null;
				}
			}
			catch (Exception e)
			{
				Trace.WriteLine("error: GetNewMonitoreeGroup -> " + e.Message);
			}

			return Result;
		}

		#endregion // Вызов функций сервера с клиента
	
		//#endregion // SetMonitoreeGroup - задаёт группу для отслеживания в реальном времени

		#region Terminal_NotifyEvent(NotifyEventArgs args)

		/// <summary>
		/// terminal notification receiver
		/// </summary>
		/// <param name="args">notification args</param>
		public void Terminal_NotifyEvent(NotifyEventArgs args)
		{
			try
			{
				if (args.Operator == null || args.Operator.Equals(Operator))
					NotifyNotifyEvents(args);
			}
			catch(Exception e)
			{
				Logger.WriteEntry(e.Message, LogType.Error, LogCategory.General, LogPriority.High, 0);
			}
		}
		
		#endregion // Terminal_NotifyEvent(NotifyEventArgs args)

		#region NotifyNotifyEvents(NotifyEventArgs args)
		/// <summary>
		/// Уведомление клиентов о событии наличия принятых сервером данных
		/// modified. added
		/// </summary>
		/// <param name="args">принятые данные</param>
		private void NotifyNotifyEvents(NotifyEventArgs args)
		{
			this.OnNotify( args );
		}


		#endregion // NotifyNotifyEvents(NotifyEventArgs args)

		// *****************************************************
		// ***************************************************** статический вызов		
		// *****************************************************

		#region ISponsor Members

		/// <summary>
		/// Requests a sponsoring client to renew the lease for the specified object
		/// </summary>
		/// <param name="lease">
		/// The lifetime lease of the object that requires lease renewal
		/// </param>
		/// <returns>
		/// The additional lease time for the specified object
		/// </returns>
		TimeSpan ISponsor.Renewal(ILease lease)
		{
			return lease.InitialLeaseTime;
		}

		#endregion // ISponsor Members

		private readonly MailService mailService = new MailService();

		public DataSet GetEvents()
		{
			return this.mailService.GetEvents();
		}

		public DataSet GetQueue()
		{
			return mailService.GetQueue();
		}

		public DataSet GetSubscribtions()
		{
			return mailService.GetSubscribtions();
		}

		public object GetDefaultPluginSettings(int schedulerevent_id)
		{
			return this.mailService.GetDefaultPluginSettings(schedulerevent_id);
		}

		public SortedList GetHistory()
		{
			SortedList tmpSL = new SortedList(historyList.Count);
			for( int i = 0; i < historyList.Count; i++)
				tmpSL.Add(historyList.GetKey(i), historyList.GetByIndex(i));

			historyList.Clear();

			/* Нехорошо пытаться убирать за собой, так как уборка будет 
			 * за всеми и приведет к лишней нагрузке на ЦП.
			 * GC сам решит лучше когда нужно убираться.
			 */
			// пытаемся убрать за собой
			// GC.Collect();
			
			return tmpSL;
		}

		
		#region Events

		private EventHandler dlgtClosing;
		private EventHandler dlgtClosed;

		public event EventHandler Closing
		{
			add { this.dlgtClosing += value; }
			remove { this.dlgtClosing -= value; }
		}
		public event EventHandler Closed
		{
			add { this.dlgtClosed += value; }
			remove { this.dlgtClosed -= value; }
		}

		protected virtual void OnClosing()
		{
			if( this.dlgtClosing != null )
				this.dlgtClosing( this, EventArgs.Empty );
		}
		protected virtual void OnClosed()
		{
			if( this.dlgtClosed != null )
				this.dlgtClosed( this, EventArgs.Empty );
		}

		#endregion // Events

		#region События охранных функций
		/// <summary>
		/// Обработчик событий охранных функций
		/// </summary>
		/// <param name="args">аргументы событий охранных функций</param>
		private void PersonalServer_GuardEvent(GuardEventArgs args)
		{
			this.OnGuard( args );
		}



		#endregion // События охранных функций //

		#region Admin

		public bool IsAdmin
		{
			get
			{
				SystemRights Rights = this.LogicForUI.GetSystemRights();

				return Rights.Contains( SystemRight.ServerAdministration );
			}
		}

		public ISessionListAdmin GetAdmin()
		{
			if( !this.IsAdmin )
			{
				throw new ApplicationException( "There is not enough rights to obtain Admin object" );
			}

			return new SessionListAdmin( this.server );
		}

		#endregion // Admin

		public enum PersonalServerState
		{
			Undefined = 0,
			Initialized = 1,
			Closed = 2
		}

#region Constants

		public new IBusConstants Constants
		{
			get { return Server.Instance().Constants; }
		}

		#endregion // Constants

		public ISchedulePassageService GetSchedulePassageService()
		{
			return Server.Instance().GetSchedulePassageService();
		}

		/// <summary>
		/// Фабрика отчетов для web
		/// </summary>
		private ReportsFactoryWeb reportsFactoryWeb = null;

		/// <summary>
		/// Формирует отчет на диск
		/// </summary>
		/// <param name="guid">GUID отчёта</param>
		/// <param name="reportParams">список параметров отчёта</param>
		/// <param name="formatType">формат файла отчёта</param>
		/// <returns>полное имя файл отчёта</returns>
		/// <remarks>Отчёты создаются в папке "PathWebGis"</remarks>
		public string ReportMake(Guid guid, PARAMS reportParams, BusinessLogic.Enums.ReportTypeEnum formatType)
		{
			// фабрика для построения web отчётов
			if (reportsFactoryWeb == null)
				reportsFactoryWeb = new ReportsFactoryWeb(Server.Instance(), Repository.Instance, OperatorID);

			string url = reportsFactoryWeb.ReportMake(guid, reportParams, formatType);

			return url;
		}


	/// <summary>
	/// Снятие статуса МО со стороны клиента
	/// </summary>
	/// <param name="id">ТС, для которого сбросить статус</param>
	/// <param name="status">Статус, который необходимо снять</param>
		public void UnSetStatus(int id, Status status)
		{
		}


		/// <summary>
		/// Посылка SMS сообщения
		/// </summary>
		/// <param name="phone">телефон</param>
		/// <param name="text">текст SMS</param>
		public void SendSMS(string phone, string text)
		{
			if (Server.Instance().TerminalManager != null)
				Server.Instance().TerminalManager.SendSMS(phone, text);
			else
				Trace.WriteLine("Cann't send command: " + text);
		}

		#region Functions for Web Points

		public DataSet GetPointsWebForOperator(Guid mapGuid)
		{
			ArrayList arParams = new ArrayList();
			arParams.Add(new ParamValue("@map_guid", mapGuid));
			arParams.Add(new ParamValue("@operator_id", this.OperatorID));

			DataSet ds = Server.Instance().GetDataFromDB(arParams, "GetPointsWeb", new string[] { "PointsWeb" });

			return ds;
		}

		public void AddPointWebForOperator(Guid mapGuid, PointF point, string pointName, string description, int pointType)
		{
			ArrayList arParams = new ArrayList();
			arParams.Add(new ParamValue("@map_guid", mapGuid));
			arParams.Add(new ParamValue("@x", point.X));
			arParams.Add(new ParamValue("@y", point.Y));
			arParams.Add(new ParamValue("@name", pointName));
			arParams.Add(new ParamValue("@description", description));
			arParams.Add(new ParamValue("@type_id", pointType));
			arParams.Add(new ParamValue("@operator_id", this.OperatorID));

			Server.Instance().GetDataFromDB(arParams, "AddPointsWeb", null);
		}

		public void EditPointWebForOperator(/*Guid mapGuid,*/ int pointID, PointF point, string pointName, string description, int pointType)
		{
			ArrayList arParams = new ArrayList();
			//arParams.Add(new ParamValue("@map_guid", mapGuid)); 
			arParams.Add(new ParamValue("@point_id", pointID));
			arParams.Add(new ParamValue("@x", point.X));
			arParams.Add(new ParamValue("@y", point.Y));
			arParams.Add(new ParamValue("@name", pointName));
			arParams.Add(new ParamValue("@description", description));
			arParams.Add(new ParamValue("@type_id", pointType));

			Server.Instance().GetDataFromDB(arParams, "EditPointsWeb", null);
		}

		public void DelPointWebForOperator(/*Guid mapGuid,*/ int pointID)
		{
			ArrayList arParams = new ArrayList();
			//arParams.Add(new ParamValue("@map_guid", mapGuid));
			arParams.Add(new ParamValue("@point_id", pointID));

			Server.Instance().GetDataFromDB(arParams, "DelPointsWeb", null);
		}

		#endregion Functions for Web Points
		 

		#region Functions for Web Lines

		/// <summary>
		/// 
		/// </summary>
		/// <param name="mapGuid"></param>
		/// <returns></returns>
		public DataSet GetLinesWebForOperator(Guid mapGuid) 
		{
			ArrayList arParams = new ArrayList();
			arParams.Add(new ParamValue("@map_guid", mapGuid));
			arParams.Add(new ParamValue("@operator_id", this.OperatorID));

			DataSet ds = Server.Instance().GetDataFromDB(arParams, "GetLinesWeb", new string[] { "LinesWeb" });

			return ds;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="mapGuid"></param>
		/// <param name="points"></param>
		/// <param name="lineName"></param>
		/// <param name="description"></param>
		public void AddLineWebForOperator(Guid mapGuid, PointF[] points, string lineName, string description)
		{
			StringBuilder vertex = new StringBuilder();
			foreach(PointF p in points)
			{
				vertex.Append(p.X.ToString());
				vertex.Append(":");
				vertex.Append(p.Y.ToString());
				vertex.Append(";");
			}
			ArrayList arParams = new ArrayList();
			arParams.Add(new ParamValue("@map_guid", mapGuid));
			arParams.Add(new ParamValue("@vertex", vertex.ToString()));
			arParams.Add(new ParamValue("@name", lineName));
			arParams.Add(new ParamValue("@description", description));
			arParams.Add(new ParamValue("@operator_id", this.OperatorID));

			Server.Instance().GetDataFromDB(arParams, "AddLinesWeb", null);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="lineID"></param>
		/// <param name="points"></param>
		/// <param name="lineName"></param>
		/// <param name="description"></param>
		public void EditLineWebForOperator(int lineID, PointF[] points, string lineName, string description)
		{
			StringBuilder vertex = new StringBuilder();
			foreach (PointF p in points)
			{
				vertex.Append(p.X.ToString());
				vertex.Append(":");
				vertex.Append(p.Y.ToString());
				vertex.Append(";");
			}
			ArrayList arParams = new ArrayList();
			arParams.Add(new ParamValue("@line_id", lineID));
			arParams.Add(new ParamValue("@vertex", vertex.ToString()));
			arParams.Add(new ParamValue("@name", lineName));
			arParams.Add(new ParamValue("@description", description));

			Server.Instance().GetDataFromDB(arParams, "EditLinesWeb", null);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="lineID"></param>
		public void DelLineWebForOperator(int lineID)
		{
			ArrayList arParams = new ArrayList();
			//arParams.Add(new ParamValue("@map_guid", mapGuid));
			arParams.Add(new ParamValue("@point_id", lineID));

			Server.Instance().GetDataFromDB(arParams, "DelPointsWeb", null);
		}

		#endregion Functions for Web Lines

	}
}