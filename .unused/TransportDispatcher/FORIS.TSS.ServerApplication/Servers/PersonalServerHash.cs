using System;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Lifetime;
using FORIS.TSS.Common.Collections;

namespace FORIS.TSS.ServerApplication.Servers
{
	/// <summary>
	/// Summary description for PersonalServerHash.
	/// </summary>
	public class PersonalServerHash: HashWithEvents
	{
		#region Indexer

		public new PersonalServer this[ object Key ]
		{
			get
			{
				this.Check( Key );

				PersonalServerReference Reference = (PersonalServerReference)base[Key];

				if( Reference != null )
				{
					return Reference.Target;
				}

				return null;
			}
		}

		#endregion // Indexer

		#region Manipulation methods

		public void Add( object Key, PersonalServer Server )
		{
			base.Add( Key, new PersonalServerReference(Server) );
		}
		public new void Remove( object Key )
		{
			base.Remove( Key );
		}


		public new bool Contains( object Key )
		{
			this.Check( Key );

			return base.Contains( Key );
		}


		#endregion // Manipulation methods

		#region Check

		private void Check( object Key )
		{
			PersonalServerReference Reference = (PersonalServerReference)base[Key];

			if( Reference != null )
			{
				if( Reference.IsAlive )
				{
					ILease Lease = (ILease)RemotingServices.GetLifetimeService( Reference.Target );

					if( Lease == null || Lease.CurrentState == LeaseState.Expired )
					{
						// TODO: ���� �� �������� Dispose? 
						
						/* ���� Dispose �� ��������, �� ��������� ������ �� 
						 * PersonalServer ��������� ����� �������
						 */

//						Reference.Target.Dispose();
 
						base.Remove( Key );
					}
				}
				else
				{
					base.Remove( Key );
				}
			}
		}

		#endregion // Check
	}

	public class PersonalServerReference
	{
		private WeakReference reference;

		public bool IsAlive
		{
			get { return this.reference.IsAlive; }
		}

		public PersonalServer Target
		{
			get { return (PersonalServer)this.reference.Target; }
		}

		public PersonalServerReference( PersonalServer Target )
		{
			this.reference = new WeakReference( Target );
		}
	}
}
