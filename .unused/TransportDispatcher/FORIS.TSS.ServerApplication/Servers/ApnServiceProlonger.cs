﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.EntityModel;
using FORIS.TSS.EntityModel.History;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.ServerApplication
{
	public class ApnServiceProlonger
	{
		private bool IsMobilUnitFromTrustedSource(IMobilUnit mu)
		{
			//TODO: вынести часть IP в настройку
			return mu.IP != null &&
				   mu.IP.Address != null &&
				   mu.IP.Address.ToString().StartsWith("172.21.");
		}

		public void Process(IMobilUnit[] items)
		{
			var prolongingVehicleIds = items
				.Where(
					mu =>
					{
						if (!IsMobilUnitFromTrustedSource(mu)) return false;
						DateTime lastTime;
						if (!_lastProlongedTime.TryGetValue(mu.Unique, out lastTime))
							return true;
						return lastTime.AddMinutes(5) < DateTime.UtcNow;
					})
				.Select(mu => mu.Unique)
				.Distinct()
				.ToArray();

			if (prolongingVehicleIds.Length == 0)
				return;

			using (var entities = new Entities())
			{
				var bsType = entities.Billing_Service_Type.First(
					bst => bst.Service_Type == Billing_Service_Type.ApnNikaMsk);

				foreach (var vehicleId in prolongingVehicleIds)
				{
					_lastProlongedTime[vehicleId] = DateTime.UtcNow;

					ProlongueApnService(entities, vehicleId, bsType);
				}
			}
		}

		public static void ProlongueApnService(Entities entities, int vehicleId, Billing_Service_Type bsType)
		{
			var vehicle =
				entities.VEHICLE.Include("CONTROLLER")
						.Include("CONTROLLER.MLP_Controller")
						.Include("CONTROLLER.MLP_Controller.Asid")
						.Include("DEPARTMENT")
						.FirstOrDefault(v => v.VEHICLE_ID == vehicleId);

			if (vehicle == null || vehicle.CONTROLLER == null)
				return;

			if (vehicle.CONTROLLER.MLP_Controller == null)
			{
				vehicle.CONTROLLER.MLP_Controller = new MLP_Controller {Asid = new Asid {DEPARTMENT = vehicle.DEPARTMENT}};
				entities.MLP_Controller.AddObject(vehicle.CONTROLLER.MLP_Controller);
				entities.Asid.AddObject(vehicle.CONTROLLER.MLP_Controller.Asid);

				entities.SaveChangesByOperator(null);
			}

			var asid = vehicle.CONTROLLER.MLP_Controller.Asid;

			var billingService = entities.Billing_Service.FirstOrDefault(
				bs => bs.Asid.ID == asid.ID &&
					  bs.Type.ID == bsType.ID);

			if (billingService == null)
			{
				var startDate = DateTime.UtcNow;
				using (var history = new HistoricalEntities())
				{
					if (!history.H_Billing_Service.Any(
						h => h.Asid_ID == asid.ID && h.Billing_Service_Type_ID == bsType.ID))
					{
						var firstLogTime = entities.Log_Time
							.Where(lt => lt.Vehicle_ID == vehicleId && lt.InsertTime != null)
							.OrderBy(lt => lt.InsertTime)
							.FirstOrDefault();
						if (firstLogTime != null)
							startDate = firstLogTime.InsertTime ?? DateTime.UtcNow;
					}
				}

				billingService = entities.CreateBillingService(asid, bsType, null);
				billingService.StartDate = startDate;
			}

			billingService.EndDate = DateTime.UtcNow.AddDays(1);

			entities.SaveChangesByOperator(null);
		}

		private readonly Dictionary<int, DateTime> _lastProlongedTime = new Dictionary<int, DateTime>();
	}
}