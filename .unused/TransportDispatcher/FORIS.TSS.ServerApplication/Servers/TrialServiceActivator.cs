﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Helpers;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.ServerApplication
{
	public class TrialServiceActivator
	{
		private readonly HashSet<int> _ignoredVehicleIds = new HashSet<int>();
		private DateTime _lastUpdateTime = DateTime.MinValue;

		private void UpdateIgnoredVehicleIds()
		{
			if (DateTime.UtcNow - _lastUpdateTime < TimeSpan.FromMinutes(1))
				return;

			_ignoredVehicleIds.Clear();
			using (var entities = new Entities())
			{
				foreach (
					var vehicleId in
						entities.VEHICLE
								.Where(
									v => !v.CONTROLLER.MLP_Controller.Asid.Billing_Service.Any(
										bs => bs.Type.PeriodDays != null && !bs.Activated))
								.Select(v => v.VEHICLE_ID))
				{
					_ignoredVehicleIds.Add(vehicleId);
				}
			}

			_lastUpdateTime = DateTime.UtcNow;
		}

		public void Process(IMobilUnit[] mobilUnits)
		{
			UpdateIgnoredVehicleIds();

			var vehicleIds = mobilUnits
				.Where(mu => mu != null)
				.Select(mu => mu.Unique)
				.Where(id => !_ignoredVehicleIds.Contains(id))
				.Distinct()
				.ToArray();

			if (vehicleIds.Length == 0)
				return;

			ActivateTrialServices(vehicleIds);
		}

		private void ActivateTrialServices(int[] vehicleIds)
		{
			if (vehicleIds.Length == 0)
				return;

			foreach (var vehicleIdsPart in vehicleIds.By(100))
			{
				using (var entities = new Entities())
				{
					var services = entities.Billing_Service
						.Include(Billing_Service.TypeReferencePath)
						.Where(v => vehicleIdsPart.Contains(v.Asid.MLP_Controller.CONTROLLER.VEHICLE.VEHICLE_ID))
						.Where(bs => bs.Type.PeriodDays != null && !bs.Activated)
						.ToList();

					foreach (var service in services)
					{
						//NOTE: также см. хранимую процедуру ActivateService
						service.Activated = true;
						if (service.Type.PeriodDays != null)
							service.EndDate = DateTime.UtcNow.AddDays(service.Type.PeriodDays.Value);

					}

					entities.SaveChangesByOperator(null);
				}
			}
		}
	}
}