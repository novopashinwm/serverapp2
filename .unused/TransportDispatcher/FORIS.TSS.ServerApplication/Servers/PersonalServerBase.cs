using System;
using System.Diagnostics;
using System.Data;
using System.Runtime.Remoting.Lifetime;
using FORIS.DataAccess;
using FORIS.TSS.BusinessLogic.Bus;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Server.LogicForUI;

namespace FORIS.TSS.ServerApplication
{
	/// <summary>
	/// Summary description for PersonalServerBase.
	/// </summary>
	public abstract class PersonalServerBase : LogicForUI
	{
		protected SessionListBase server = null; // this variable holds server object in memory
		IDbDatasetAdapter dataAdapter = null;

		#region Constructor & Dispose

		public PersonalServerBase()
		{

		}
		
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				//if( Server.Instance() != null )
				//    Server.Instance().Notifications.DetachEvents( dataAdapter );
			}
		}

	    #endregion // Constructor & Dispose

		#region Initialize

		protected ILease lease;

		public override object InitializeLifetimeService()
		{
			#region Preconditions

			if( this.lease != null )
			{
				throw new InvalidOperationException(
					"Lifetime service has been initialized for object already"
					);
			}

			#endregion // Preconditions

			this.lease = (ILease)base.InitializeLifetimeService();

			/* �������� � ������� �����. 
			 * � ���������, �� ��� ��������
			 */

			return this.lease;
		}

		public void Init( SessionListBase server, ISessionInfo sessionInfo )
		{
			this.server = server;
			this.sessionInfo = sessionInfo;

			this.dataAdapter = HistoryFactory.CreateDatasetAdapter();
			this.dataAdapter.SessionID = this.SessionInfo.SessionID;
			//Server.Instance().Notifications.AttachEvents( this.dataAdapter );


			Init(); //(this as IPersonalServer);

			Trace.WriteLine(
				String.Format(
					"User {0}: create session",
					this.sessionInfo.OperatorInfo.Login
					)
				);
		}

		#endregion // Initialize

		private ISessionInfo sessionInfo = null;
		public ISessionInfo SessionInfo
		{
			get { return this.sessionInfo; }
		}

		public string Login
		{
			get { return this.sessionInfo.OperatorInfo.Login; }
		}
		
		protected int operatorID;
		public int OperatorID
		{
			get { return this.sessionInfo.OperatorInfo.OperatorID; }
		}
		
		/// <summary>
		/// full operator related info
		/// </summary>
		public IOperatorInfo Operator
		{
			get { return this.sessionInfo.OperatorInfo; }
		}

		/// <summary>
		/// �������� ������ ������ � �������� ???
		/// </summary>
		public ILogicForUI LogicForUI
		{
			get { return this; }
		}

		/// <summary>
		/// Identifier of the session in the database
		/// </summary>
		public int SessionID
		{
			get
			{
				return this.sessionInfo.SessionID;
			}
		}
		/// <summary>
		/// ��������� ����� ������ � ��
		/// </summary>
		/// <param name="dataset">������ �� ����� ������, ������� ��������� �������� � ��</param>
		public void Update(DataSet dataset)
		{
			//���������� � �������
			// Server.Instance().Notifications.CheckDatasetProperties(dataset);
			
			DataBaseManager.Update(dataAdapter, dataset, new FORIS.TSS.BusinessLogic.DatabaseSchema() );
			
            this.sc.Update(this.GetSecurity());
			// this.FireEvents(Server.Instance().Notifications);
		}

		protected override IBusConstants Constants
		{
			get { return Server.Instance().Constants; }
		}
	}
}