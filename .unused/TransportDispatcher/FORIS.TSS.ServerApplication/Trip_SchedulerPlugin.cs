﻿using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using FORIS.DataAccess;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.Terminal;

namespace FORIS.TSS.ServerApplication
{
	/// <summary>
	/// plug-in for sending schedules data to controllers
	/// </summary>
	public class Trip_SchedulerPlugin : ISchedulerPlugin
	{
		/// <summary>
		/// message to send
		/// </summary>
		protected struct Message
		{
			/// <summary>
			/// VEHCILE_ID
			/// </summary>
			public readonly int vid;
			/// <summary>
			/// text to send
			/// </summary>
			public string text;
			/// <summary>
			/// controll time
			/// </summary>
			public DateTime time;

			/// <summary>
			/// ctor
			/// </summary>
			/// <param name="_vid">VEHICLE_ID</param>
			/// <param name="_name">point name</param>
			/// <param name="_tin">plan time in</param>
			/// <param name="_tout">plan time out</param>
			/// <param name="_ltin">fact time in</param>
			/// <param name="_ltout">fact time out</param>
			public Message(int _vid, string _name, DateTime _tin, DateTime _tout, 
				DateTime _ltin, DateTime _ltout)
			{
				vid = _vid;
				text = _name + ": ";
				if(_tin != TimeHelper.NULL) text += string.Format("{0} ({1})", 
					_tin.ToString("t"), _ltin != TimeHelper.NULL? 
					((int)(_ltin - _tin).TotalMinutes).ToString(): "");
				else text += "--:-- ()";
				text += " - ";
				if(_tout != TimeHelper.NULL) text += string.Format("{0} ({1})", 
					_tout.ToString("t"), _ltout != TimeHelper.NULL? 
					((int)(_ltout - _tout).TotalMinutes).ToString(): "");
				else text += "--:-- ()";
				time = _tout != TimeHelper.NULL? _tout: _tin;
			}
		}
		
		/// <summary>
		/// notification list
		/// </summary>
		protected static ArrayList alNotify = new ArrayList(20);
		protected static DataTable tblNotify;

		protected TraceSwitch tsSchedule = new TraceSwitch("SCHEDULER", "scheduler info");

		private static bool debugMode = false;

		static Trip_SchedulerPlugin()
		{
			tblNotify = new DataTable("Messages");
			tblNotify.PrimaryKey = new DataColumn[]{tblNotify.Columns.Add("VID", typeof(int))};
			tblNotify.Columns.Add("Message", typeof(Message));
			tblNotify.Columns.Add("SortTime", typeof(DateTime));

			string debugModeString = Globals.AppSettings["CheckScheduleDebug"];
			if (debugModeString != null && debugModeString.ToUpper() == "ON")
				debugMode = true;
		}

		#region ISchedulerPlugin Members

		public string GetStaticConfig()
		{
			// TODO:  Add Trip_SchedulerPlugin.GetStaticConfig implementation
			return null;
		}

		public void SetStaticConfig(int id, string configurationData)
		{
			// TODO:  Add Trip_SchedulerPlugin.SetStaticConfig implementation
		}

		public object GetParameters(string strConfigXML)
		{
			// TODO:  Add Trip_SchedulerPlugin.GetParameters implementation
			return null;
		}

		public string GetDisplayName(CultureInfo locale)
		{
			return "Рассылка расписаний";
		}

		public void Execute()
		{
			if(tsSchedule.TraceVerbose)
			{
				Trace.WriteLine(DateTime.Now);
				Trace.WriteLine("Executing Trip_SchedulerPlugin", "Scheduler");
			}
			DataSet ds = null;
			try
			{
				using(StoredProcedure sp = 
					Globals.TssDatabaseManager.DefaultDataBase.Procedure(
						"dbo.GetPassedPoints"))
				{
					ds = sp.ExecuteDataSet();
				}
				DataView dv = new DataView(ds.Tables[0], "", "VEHICLE_ID, SORT_TIME desc", 
					DataViewRowState.CurrentRows);
				// for each vehicle get only last message. other discarded
				int vid = -1;
				foreach(DataRowView row in dv)
				{
					if(vid == (int)row["VEHICLE_ID"]) continue;

					vid = (int)row["VEHICLE_ID"];
					// !!! WARN !!!
					// using ToLocalTime() at server
					// assume that target in the same time zone as server
					Message newmsg = new Message(vid, row["NAME"].ToString(), 
						row["TIME_IN"] != DBNull.Value? 
							((DateTime)row["TIME_IN"]).ToLocalTime(): TimeHelper.NULL, 
						row["TIME_OUT"] != DBNull.Value? 
							((DateTime)row["TIME_OUT"]).ToLocalTime(): TimeHelper.NULL, 
						row["LOG_TIME_IN"] != DBNull.Value? 
							((DateTime)row["LOG_TIME_IN"]).ToLocalTime(): TimeHelper.NULL, 
						row["LOG_TIME_OUT"] != DBNull.Value? 
							((DateTime)row["LOG_TIME_OUT"]).ToLocalTime(): TimeHelper.NULL);
					DataRow rowmsg = tblNotify.Rows.Find(vid);
					if(rowmsg == null) 
						tblNotify.Rows.Add(new object[]{vid, newmsg, newmsg.time});
					else
					{
						Message msg = (Message)rowmsg["Message"];
						Debug.Assert(msg.time < newmsg.time);

						rowmsg["Message"] = newmsg;
						rowmsg["SortTime"] = newmsg.time;
					}
				}
				ProcessList(tblNotify);
			}
			catch(Exception exc)
			{
				Trace.TraceError("{0}", exc);
				Trace.WriteLine(exc.StackTrace);
			}
			finally
			{
				if(ds != null)
					ds.Dispose();
			}
		}

		public BusinessLogic.DTO.Results.SchedulerExecutionResult Execute(string xml, string commentXML, int queueId)
		{
			if (tsSchedule.TraceVerbose)
			{
				Trace.WriteLine(DateTime.Now);
				Trace.WriteLine("Executing Trip_SchedulerPlugin", "Scheduler");
			}
			DataSet ds = null;
			try
			{
				using (StoredProcedure sp =
					Globals.TssDatabaseManager.DefaultDataBase.Procedure(
						"dbo.GetPassedPoints"))
				{
					ds = sp.ExecuteDataSet();
				}
				DataView dv = new DataView(ds.Tables[0], "", "VEHICLE_ID, SORT_TIME desc",
					DataViewRowState.CurrentRows);
				// for each vehicle get only last message. other discarded
				int vid = -1;
				foreach (DataRowView row in dv)
				{
					if (vid == (int)row["VEHICLE_ID"]) continue;

					vid = (int)row["VEHICLE_ID"];
					// !!! WARN !!!
					// using ToLocalTime() at server
					// assume that target in the same time zone as server
					Message newmsg = new Message(vid, row["NAME"].ToString(),
						row["TIME_IN"] != DBNull.Value ?
							((DateTime)row["TIME_IN"]).ToLocalTime() : TimeHelper.NULL,
						row["TIME_OUT"] != DBNull.Value ?
							((DateTime)row["TIME_OUT"]).ToLocalTime() : TimeHelper.NULL,
						row["LOG_TIME_IN"] != DBNull.Value ?
							((DateTime)row["LOG_TIME_IN"]).ToLocalTime() : TimeHelper.NULL,
						row["LOG_TIME_OUT"] != DBNull.Value ?
							((DateTime)row["LOG_TIME_OUT"]).ToLocalTime() : TimeHelper.NULL);
					DataRow rowmsg = tblNotify.Rows.Find(vid);
					if (rowmsg == null)
						tblNotify.Rows.Add(new object[] { vid, newmsg, newmsg.time });
					else
					{
						Message msg = (Message)rowmsg["Message"];
						Debug.Assert(msg.time < newmsg.time);

						rowmsg["Message"] = newmsg;
						rowmsg["SortTime"] = newmsg.time;
					}
				}
				ProcessList(tblNotify);
			}
			catch (Exception exc)
			{
				Trace.TraceError("{0}", exc);
				Trace.WriteLine(exc.StackTrace);
			}
			finally
			{
				if (ds != null)
					ds.Dispose();
			}
			return SchedulerExecutionResult.Success;
		}

		public object GetDefaultSettings()
		{
			// TODO:  Add Trip_SchedulerPlugin.GetDefaultSettings implementation
			return null;
		}

		public bool IsActualTask(DateTime nearestTime)
		{
			return true;
		}

		#endregion

		/// <summary>
		/// process list of messages
		/// </summary>
		/// <param name="tbl">messages</param>
		protected virtual void ProcessList(DataTable tbl)
		{
			IStdCommand cmd = 
				CommandCreator.CreateCommand(CmdType.SendText) as IStdCommand;
			cmd.Sender = new OperatorInfo( (int)ObjectID.Server, null, "Trip_SchedulerPlugin" );
			//ITerminal term = Server.TerminalManager.CreateTerminal(CmdType.SendText);
			DataView dv = new DataView(tbl);

			if (debugMode)
			{
				const string logsCheckscheduleLogFileName = @"c:\Nika\Logs\checkSchedule.log";
				try
				{
					using (StreamWriter sw = new StreamWriter(new FileStream(logsCheckscheduleLogFileName, FileMode.Append)))
					{
						sw.WriteLine(DateTime.Now.ToShortTimeString());
						foreach (DataRowView row in dv)
						{
							try
							{
								Message msg = (Message) row["Message"];
								cmd.Params["Text"] = msg.text;
								cmd.Target.Unique = msg.vid;
								Server.Instance().DbManager.FillUnitInfo(cmd.Target);

								if (IPAddress.Any.Equals(cmd.Target.IP.Address))
								{
									Trace.WriteLineIf(tsSchedule.TraceWarning,
													  "End point unknown. unit - " + cmd.Target.ToString());
									continue;
								}

								sw.WriteLine(msg.text);
								row.Delete();
							}
							catch (Exception ex)
							{
								if (ex.InnerException is ArgumentException) row.Delete();
								Trace.WriteLineIf(
									tsSchedule.TraceError, ex.Message, "Trip_SchedulerPlugin");
							}
						}
					}
				}
				catch(Exception exc)
				{
					Trace.WriteLine("Can't open file " + logsCheckscheduleLogFileName);
					Trace.TraceError("{0}: Can't open file {1}: {2}", this, logsCheckscheduleLogFileName, exc);
				}
			}
			else
			{
				foreach (DataRowView row in dv)
				{
					try
					{
						Message msg = (Message)row["Message"];
						cmd.Params["Text"] = msg.text;
						cmd.Target.Unique = msg.vid;
						Server.Instance().DbManager.FillUnitInfo(cmd.Target);

						if (IPAddress.Any.Equals(cmd.Target.IP.Address))
						{
							Trace.WriteLineIf(tsSchedule.TraceWarning,
											  "Trip_Schedule - end point unknown. unit - " + cmd.Target.ToString());
							continue;
						}


						var clonedCommand = (ICommand)cmd.Clone() as IStdCommand;

						if (Server.Instance().TerminalManager != null && clonedCommand != null)
							Server.Instance().TerminalManager.SendCommand(clonedCommand);
						else
							Trace.WriteLine("Cann't send command: " + (clonedCommand != null ? clonedCommand.Text : string.Empty));
						
						row.Delete();
					}
					catch (Exception ex)
					{
						if (ex.InnerException is ArgumentException) row.Delete();
						Trace.WriteLineIf(
							tsSchedule.TraceError, ex.Message, "Trip_SchedulerPlugin");
					}
				}
			}

			tbl.AcceptChanges();
		}
	}
}