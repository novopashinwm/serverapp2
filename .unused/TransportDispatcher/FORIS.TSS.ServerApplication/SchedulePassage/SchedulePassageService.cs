﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using FORIS.TSS.BusinessLogic.Interfaces;

namespace FORIS.TSS.ServerApplication.SchedulePassage
{
	class SchedulePassageService : Component, ISchedulePassageService
	{
		public SchedulePassageService()
		{

		}
		public SchedulePassageService(IContainer container)
		{
			container.Add(this);
		}

		public override object InitializeLifetimeService()
		{
			/* Класс ходит ссылкой по ремотингу и чтоб за
			 * ним не следил менеджер лицензий вернем null
			 */

			return null;
		}


		public ISchedulePointPassageInfo GetOriginPassageInfo(int idWbTrip)
		{
			return Server.Instance().DbManager.GetOriginPassageInfo(idWbTrip);
		}

		public ISchedulePointPassageInfo GetDestinationPassageInfo(int idWbTrip)
		{
			return Server.Instance().DbManager.GetDestinationPassageInfo(idWbTrip);
		}

		public ISchedulePointPassageInfo[] GetPassageInfo(int idWbTrip)
		{
			throw new NotImplementedException();
		}

		public IDictionary<int, ISchedulePointPassageInfo[]> GetPassageInfo(int[] wbTrips)
		{
			throw new NotImplementedException();
		}
	}
}