﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Common;
using FORIS.TSS.EntityModel;
using FORIS.TSS.ServerApplication.Exceptions;
using FORIS.TSS.ServerApplication.SmsService;
using FORIS.TSS.ServerApplication.SmsService.Configuration;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	/// <summary>
	/// Разбирает очередь запросов на определение местоположения с помощью MPX
	/// </summary>
	sealed class MpxLbsSenderProcessor : MessageSender
	{
		private readonly Server _server;
		private readonly HashSet<int> _mlpTerminalIDs;
		private readonly ConcurrentQueue<NotifyEventArgs> _notifications = new ConcurrentQueue<NotifyEventArgs>();

		private static ContentProviderSectionHandler Config
		{
			get { return (ContentProviderSectionHandler) ConfigurationManager.GetSection("ContentProvider"); }
		}

		public MpxLbsSenderProcessor()
			: base(
				MessageDestinationType.MpxLbs, Config.MaxTPS,
				string.Equals(ConfigurationManager.AppSettings["MpxLbsMessageSender.Enabled"], "true",
							  StringComparison.OrdinalIgnoreCase))
		{
			_server = Server.Instance();
			_mlpTerminalIDs = GetMLPTerminalIDs();
			BeforeStart += delegate
				{
					SubscribeToServerEvents();
				};

			AfterStop += delegate { UnsubscribeFromServerEvents(); };
		}

		private static readonly object InstanceLock = new object();
		private static MpxLbsSenderProcessor _instance;

		/// <summary>Синглтон обработчика очереди</summary>
		public static MpxLbsSenderProcessor Instance
		{
			get
			{
				if (_instance != null)
					return _instance;

				lock (InstanceLock)
					return _instance ?? (_instance = new MpxLbsSenderProcessor());
			}
		}

		private static HashSet<int> GetMLPTerminalIDs()
		{
			using (var entities = new Entities())
			{
				return
					new HashSet<int>(
						entities.MEDIA.Where(m => m.NAME == "MLP")
								.SelectMany(m => m.MEDIA_ACCEPTORS)
								.Select(ma => ma.MA_ID));
			}
		}

		private void SubscribeToServerEvents()
		{
			_server.NotifyEvent += OnNotified;
		}

		private void OnNotified(NotifyEventArgs args)
		{
			if (args.TermID == null || !_mlpTerminalIDs.Contains(args.TermID.Value))
				return;
			_notifications.Enqueue(args);
		}

		private void UnsubscribeFromServerEvents()
		{
			_server.NotifyEvent -= OnNotified;
		}

		public override void Initialize()
		{
		}

		protected override MessageProcessingResult SendMessage(Entities context, MessageTuple messageTuple)
		{
			var commandId = context.GetIntegerFieldValue(messageTuple.Message.MESSAGE_ID, MESSAGE_TEMPLATE_FIELD.CommandId);
			if (commandId == null)
				return MessageProcessingResult.Error;
			var vehicleID = context.GetIntegerFieldValue(messageTuple.Message.MESSAGE_ID, MESSAGE_TEMPLATE_FIELD.VehicleID);
			if (vehicleID == null)
				return MessageProcessingResult.Error;

			var @params = new PARAMS();
			@params.Add(CommandParameter.CommandID, commandId);
			var senderAsid = messageTuple.Message
				.MESSAGE_FIELD
				.Where(mf => mf.MESSAGE_TEMPLATE_FIELD.NAME == MESSAGE_TEMPLATE_FIELD.SenderAsid)
				.Select(mf => mf.CONTENT)
				.FirstOrDefault();
			if (senderAsid != null)
				@params.Add(CommandParameter.SenderAsid, senderAsid);

			const CmdType cmdType = CmdType.AskPosition;
			var cmd = CommandCreator.CreateCommand(cmdType);
			if (cmd == null)
				throw new InvalidOperationException("Unable to create command by type: " + cmdType);

			cmd.Params = @params;

			if (messageTuple.Message.Owner_Operator_ID != null)
			{
				var ownerOperator =
					context.OPERATOR.Where(o => o.OPERATOR_ID == messageTuple.Message.Owner_Operator_ID.Value)
						   .Select(o => new {
											o.OPERATOR_ID,
											o.LOGIN,
											o.NAME})
						   .FirstOrDefault();
				if (ownerOperator != null)
					cmd.Sender = new OperatorInfo(ownerOperator.OPERATOR_ID, ownerOperator.LOGIN, ownerOperator.NAME);
			}

			cmd.MediaType = Media.LBS;
			cmd.Target.Unique = vehicleID.Value;
			try
			{
				_server.SendCommand(cmd);
			}
			catch (TerminalServerConnectionException)
			{
				return MessageProcessingResult.NoConnectionWithGateway;
			}
			catch (Exception ex)
			{
				Trace.TraceError("{0}", ex);
				return MessageProcessingResult.Error;
			}

			return MessageProcessingResult.Sent;
		}

		public override bool CanSendTo(Contact contact)
		{
			return false;
		}

		protected override bool Do()
		{
			var somethingWasDone = ProcessEvents();

			if (base.Do())
				somethingWasDone = true;

			return somethingWasDone;
		}

		private bool ProcessEvents()
		{
			if (_notifications.IsEmpty)
				return false;

			var stopWatch = new Stopwatch();
			stopWatch.Start();

			try
			{
				NotifyEventArgs notifyEventArgs;
				while (_notifications.TryDequeue(out notifyEventArgs))
				{
					ProcessNotifyEvent(notifyEventArgs);
					CheckEventProcessingTime(stopWatch.Elapsed);

				}
			}
			finally
			{
				stopWatch.Stop();
				if (CriticalEventProcessingTime < stopWatch.Elapsed)
					Trace.TraceWarning("{0}.ProcessEvents was too long: {1}", GetType(), stopWatch.Elapsed);
			}

			return true;
		}

		private void ProcessNotifyEvent(NotifyEventArgs args)
		{
			var commandTags = args.Tag as object[];
			if (commandTags == null)
				return;

			if (commandTags.Length != 1)
				return;
			
			var commandResult = commandTags[0] as CommandResult;
			if (commandResult == null)
				return;

			if (commandResult.CmdResult == CmdResult.CapacityLimitReached)
			{
				Throttled(Config.RetryThrottlingTime);
			}
		}

		private TimeSpan _maxEventProcessingTime = TimeSpan.Zero;
		private static readonly TimeSpan CriticalEventProcessingTime = TimeSpan.FromMinutes(1);

		private void CheckEventProcessingTime(TimeSpan elapsed)
		{
			//Время выполнения увеличилось более чем вдвое по сравнению с предыдущим разом
			if (_maxEventProcessingTime < elapsed - _maxEventProcessingTime)
			{
				Trace.TraceWarning("{0}.Peak event processing time is {1}", GetType(), elapsed);
				_maxEventProcessingTime = elapsed;
			}
		}
	}
}
