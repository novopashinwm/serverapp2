﻿using System;
using System.Configuration;
using System.Data.Objects;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.EntityModel;
using FORIS.TSS.ServerApplication.SmsService.Configuration;

namespace FORIS.TSS.ServerApplication.BackgroundProcessors
{
	class CallDetailRecordSender : BackgroundProcessorSingleton<CallDetailRecordSender>
	{
		private const string SuccessfulResponse = 
@"<?xml version=""1.0"" encoding=""utf-8""?>
<!DOCTYPE svc_result PUBLIC ""-//OMA//DTD SVC_RESULT 3.2//EN"" ""SVC_RESULT.DTD"">
<svc_result ver=""3.2.0"">
	<slia ver=""3.0.0"">
		<pos>
			<msid type=""ASID"">p4primc93c9eec8132d7da8204d9226626db37</msid>
			<poserr>
				<result resid=""509"">LOCATION SUPPRESSED</result>
				<time utc_off=""+0000"">20150430073723</time>
			</poserr>
		</pos>
	</slia>
	<ma:slia-extension ver=""3.0.0"">
		<ma:network-parameters msid-ref=""p4primc93c9eec8132d7da8204d9226626db37"">
			<ma:mnp-info plmn=""on-net""/>
		</ma:network-parameters>
	</ma:slia-extension>
</svc_result>";

		private const string BarredResponse =
			@"<?xml version=""1.0"" encoding=""utf-8""?>
<!DOCTYPE svc_result PUBLIC ""-//OMA//DTD SVC_RESULT 3.2//EN"" ""SVC_RESULT.DTD"">
<svc_result ver=""3.2.0"">
	<slia ver=""3.0.0"">
		<result resid=""504"">REMOTE FACILITY BARRED</result>
		<add_info>OBST=3</add_info>
	</slia>
</svc_result>
";

		private const string ErrorResponse = "blablabla";

		private int _stubResponsesIndex;
		private readonly string[] _stubResponses =
		{
			SuccessfulResponse,
			ErrorResponse,
			BarredResponse,
		};

		private readonly int _timeoutMilliseconds;
		private readonly string _login;
		private readonly string _password;
		private readonly Uri _uri;
		private readonly Func<Entities, IQueryable<Tuple>> _compiledQuery;

		public CallDetailRecordSender() : base(TimeSpan.FromSeconds(1))
		{
			var configuration = (ContentProviderSectionHandler)ConfigurationManager.GetSection("ContentProvider");
			if (configuration == null)
				return;
			_timeoutMilliseconds = 5000;
			_login = configuration.SendSmsLogin;
			_password = configuration.SendSmsPassword;
			_uri = new Uri(configuration.SendCdrUri);
			_compiledQuery = CompiledQuery.Compile<Entities, IQueryable<Tuple>>(
				entities => entities.RenderedServiceItem
									.Where(x => x.Status != (int) RenderedServiceItemStatus.Payed &&
												x.Billing_Service_ID == null &&
												x.Asid.Billing_Blocking == null)
									.OrderBy(x => x.ErrorCount)
									.ThenBy(x => x.StatusDate)
									.Select(x => new Tuple
										{
											Cdr = x,
											Asid = x.Asid.Contact.Value,
											AsidAllowsCdrPayment = x.Asid.AllowsCdrPayment,
											Type = x.Rendered_Service_Type.Name,
											TargetPhone = x.MESSAGE.Contacts
														   .Where(c => c.Type == (int) MessageContactType.Destination &&
																	   c.Contact.Type == (int) ContactType.Phone)
														   .Select(c => c.Contact.Value)
														   .FirstOrDefault()
										})
									.Take(1));
		}

		private class Tuple
		{
			public RenderedServiceItem Cdr;
			public string Asid;
			public bool AsidAllowsCdrPayment;
			public string Type;
			public string TargetPhone;
		}

		protected override bool Do()
		{
			using (var entities = new Entities())
			{
				var item = _compiledQuery(entities).FirstOrDefault();

				if (item == null)
				{
					return false;
				}

				var renderedServiceItemStatus = Send(item.Asid, item.Type, item.TargetPhone);

				var newAllowCdrPayment = renderedServiceItemStatus == RenderedServiceItemStatus.Payed;
				if (newAllowCdrPayment != item.AsidAllowsCdrPayment)
				{
					var asid = entities.Asid.FirstOrDefault(a => a.ID == item.Cdr.Asid_ID);
					if (asid != null)
						asid.AllowsCdrPayment = newAllowCdrPayment;
				}

				var newStatus = (int)renderedServiceItemStatus;
				if (item.Cdr.Status == newStatus)
					++item.Cdr.ErrorCount;
				else
					item.Cdr.Status = newStatus;

				item.Cdr.StatusDate = DateTime.UtcNow;

				entities.SaveChanges();

				return true;
			}
		}

		private string GetSlir(string asid, string chargeString)
		{
			const string requestXmlFormatString = @"<?xml version='1.0' ?>
<svc_init ver='3.2.0'>
  <hdr ver='3.0.0'>
	<client>
	  <id>{0}</id>
	  <pwd>{1}</pwd>
	</client>
  </hdr>
  <slir ver='3.2.0' res_type='SYNC'>
	<msids>
	  <msid type='ASID'>{2}</msid>
	</msids>
	<eqop>
	  <max_loc_age>1</max_loc_age>
	</eqop>
	<loc_type type='MA:SUPPRESS_LOCATION'/>
  </slir>

	<ma:slir-extension>
		<ma:apply-charge value=""YES""/>
		<ma:add-charge-data>
			<ma:msid2 type=""ASID"">{2}</ma:msid2>
			<ma:charged-party value=""MSID2""/>
			<ma:transparent-charge-string>{3}</ma:transparent-charge-string>
		</ma:add-charge-data>
		<ma:barring-check value=""YES""/>
	</ma:slir-extension>

</svc_init>";
			return string.Format(requestXmlFormatString,
								 _login,
								 _password,
								 asid,
								 chargeString);
		}

		private RenderedServiceItemStatus Send(string asid, string type, string targetPhone)
		{
			var chargeString = GetChargeString(type, targetPhone);

			var slir = GetSlir(asid, chargeString);

			var response = _uri.IsFile ? Stub(slir) : RequestViaHttpPost(_uri, slir);

			var responseValue = ParseResponse(response);

			if (responseValue == RenderedServiceItemStatus.Error)
			{
				Trace.TraceWarning(
					"{0}: Sending CDR (asid={1}, type={2}, targetPhone={3} failed, response was {4}",
					this, asid, type, targetPhone, response);
			}

			return responseValue;
		}

		private string Stub(string slir)
		{
			File.AppendAllText(_uri.AbsolutePath, DateTime.UtcNow + @": " + slir + Environment.NewLine);

			_stubResponsesIndex++;
			if (_stubResponsesIndex == _stubResponses.Length)
				_stubResponsesIndex = 0;

			return _stubResponses[_stubResponsesIndex];
		}

		private RenderedServiceItemStatus ParseResponse(string response)
		{
			if (string.IsNullOrWhiteSpace(response))
				return RenderedServiceItemStatus.Error;

			XmlDocument xdoc = new XmlDocument();
			try
			{
				using (var xmlReader = new XmlTextReader(new StringReader(response)) { Namespaces = false, XmlResolver = null })
					xdoc.Load(xmlReader);
			}
			catch (XmlException e)
			{
				Trace.TraceWarning("{0}", e);
				return RenderedServiceItemStatus.Error;
			}

			var resultNode = xdoc.SelectSingleNode("svc_result/slia/pos/poserr/result");
			if (resultNode == null || resultNode.Attributes == null)
			{
				resultNode = xdoc.SelectSingleNode("/svc_result/slia/result");
				if (resultNode == null || resultNode.Attributes == null)
					return RenderedServiceItemStatus.Error;
			}
			var residAttribute = resultNode.Attributes["resid"];
			if (residAttribute == null)
				return RenderedServiceItemStatus.Error;

			switch (residAttribute.Value)
			{
				case "509":
					return RenderedServiceItemStatus.Payed;
				case "504":
					return RenderedServiceItemStatus.Barred;
				default:
					return RenderedServiceItemStatus.Error;
			}
		}

		private string GetChargeString(string type, string targetPhone)
		{
			switch (type)
			{
				case "LBS":
					return "FRNIKAMPXWhere";
				case "SMS":
					return "FRNIKASMS_" + (targetPhone != null ? targetPhone.TrimStart('+') : string.Empty);
				default:
					throw new ArgumentOutOfRangeException("type", type, @"Value is not supported");
			}
		}

		private string RequestViaHttpPost(Uri uri, string content)
		{
			try
			{

				var httpRequest = (HttpWebRequest) WebRequest.CreateDefault(uri);
				httpRequest.UserAgent =
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)";
				httpRequest.Timeout = _timeoutMilliseconds;
				httpRequest.Method = "POST";
				httpRequest.KeepAlive = false;
				httpRequest.ContentType = "application/xml";
				var requestStream = httpRequest.GetRequestStream();
				byte[] buffer = Encoding.ASCII.GetBytes(content);
				requestStream.Write(buffer, 0, buffer.Length);
				requestStream.Close();
				var loWebResponse = (HttpWebResponse) httpRequest.GetResponse();

				var responseStream = loWebResponse.GetResponseStream();
				if (responseStream == null)
					return null;
				using (var stringReader = new StreamReader(responseStream))
					return stringReader.ReadToEnd();
			}
			catch (Exception ex)
			{
				Trace.TraceError("{0}", ex);
				return null;
			}
		}
	}
}