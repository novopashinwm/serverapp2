using System;
using System.Diagnostics;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Services;
using System.Windows.Forms;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces.Data;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.EntityModel;
using FORIS.TSS.IO;
using Interfaces.Web;
using FORIS.TSS.BusinessLogic.ResultCodes;
using Interfaces.Web.Enums;

namespace FORIS.TSS.ServerApplication
{
	public class SessionList : 
		SessionListBase, 
		ITrackingHandler,
		IWebLoginProvider
	{
		private System.ComponentModel.IContainer components;
	
		public SessionList()
		{
			InitializeComponent();

			AppDomain.CurrentDomain.DomainUnload += new EventHandler( CurrentDomain_DomainUnload );

			//TrackingServices.RegisterTrackingHandler(this);
		}


		
		public override System.Object InitializeLifetimeService()
		{
			return null;
		}

		public void UnregisterTcpChannel()
		{
			Trace.WriteLine("Channels list:");
			IChannel[] channels = ChannelServices.RegisteredChannels;
			foreach (IChannel channel in channels)
			{
				Trace.WriteLine(channel.ChannelName);
				Trace.Flush();
				ChannelServices.UnregisterChannel(channel);
			}
			Trace.Flush();
		}

		/// <summary>
		/// ��������� ������ �������
		/// </summary>
		private void Stop()
		{
			Trace.WriteLine( "Session List - OnStop", "OnStop" );
			RemotingServices.Disconnect( this );
			if( Server.Instance().IsActive )
			{
				Server.Instance().Stop();
			}
			Trace.WriteLine( "Session List - before unregistring channels", "OnStop" );
			UnregisterTcpChannel();
			// Stop message loop
			if( Application.MessageLoop == true )
			{
				Trace.WriteLine( "Session List - before Application.Exit();", "OnStop" );
				Application.Exit();
			}
			// ���
			Logger.WriteEntry( "������ TSS ����������.", LogType.Information, LogCategory.General, LogPriority.Normal, 0 );
			Trace.WriteLine( "Session List - OnStop completed", "OnStop" );
		}

		#region ITrackingHandler members

		void ITrackingHandler.DisconnectedObject(object obj)
		{
			Debug.WriteLine("Disconnected object: " + obj.ToString());
			if(obj is PersonalServer)
			{
				PersonalServer ps = (PersonalServer) obj;
				Debug.WriteLine("DisconnectedByClient? " + ps.DisconnectedByClient);
				
				// ��������� ���������� ������ ������������� �������
				if(!ps.DisconnectedByClient)
				{
					ps.ExtremeDisconnect();
				}
			}
		}

		void ITrackingHandler.UnmarshaledObject(object obj, ObjRef or)
		{
			Debug.WriteLine("Unmarshalled object: " + or.ToString());
		}

		void ITrackingHandler.MarshaledObject(object obj, ObjRef or)
		{
			Debug.WriteLine("Marshalled object: " + or.ToString());
		}


		#endregion // ITrackingHandler members

		#region Handle current AppDomain events

		private void CurrentDomain_DomainUnload( object sender, EventArgs e )
		{
			Debug.WriteLine( "SessionList.CurrentDomain_DomainUnload()" );

			this.Stop();
		}

		#endregion // Handle current AppDomain events

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();

		}

	    /// <summary>
        /// ������� ������ ��������� � ������ ����������� � ����� ����������
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <param name="nickname">��� ���������</param>
        /// <param name="garage_number">�������� �������</param>
        /// <param name="publicNumber">���. �����</param>
        /// <param name="vehicleKind">��� ����������</param>
        /// <param name="rights">������ ���� ��������� �� ���� ������</param>
        /// <param name="controllerTypeName">�������� ���� �����������. ���� �� ������ - �� �� ����� ����������� ����������</param>
        /// <param name="asid">��� MLP ������������</param>
        /// <param name="deviceID">ID ���������� ��� ������ ������</param>
        /// <param name="deviceNumber">������ ����� ����������</param>
        /// <param name="phone">�������</param>
        /// <param name="newOperator">��������� ��������</param>
        /// <param name="vehicle">��������� ������ �����������</param>
        /// <returns></returns>
        public CreateOperatorResult CreateNewOperatorAndVehicle(string login, string password,
                                             string nickname, string garage_number, string publicNumber,
                                             VehicleKind vehicleKind, SystemRight[] rights,
                                            string controllerTypeName, Asid asid, string deviceID,
                                            int deviceNumber, string phone,
                                             out OPERATOR newOperator, out VEHICLE vehicle)
        {
            throw new NotSupportedException("CreateNewOperatorAndVehicle method is not supported by SessionList class");
        }

        void IWebLoginProvider.SendConfirmationEmail(string emailAddressTo, string subjectFormat, string messageFormatString, bool encodeAsUri)
        {
            throw new NotSupportedException("SendConfirmationEmail method is not supported by SessionList class");
        }

	    public void SendConfirmationEmail(string emailAddressTo, string subjectFormat, string messageFormatString, bool encodeAsUri, int operatorId)
	    {
	        throw new NotImplementedException();
	    }

        string IWebLoginProvider.GetEmailByConfirmationKey(string confirmationKey)
        {
            throw new NotSupportedException("GetEmailByConfirmationKey method is not supported by SessionList class");
        }

        int? IWebLoginProvider.GetOperatorIDByConfirmationKey(string confirmationKey)
        {
            throw new NotSupportedException("GetLoginByConfirmationKey method is not supported by SessionList class");
        }

        string IWebLoginProvider.GetLoginByEmail(string email)
        {
            throw new NotSupportedException("GetLoginByEmail method is not supported by SessionList class");
        }

        bool IWebLoginProvider.CheckCaptcha(string remoteIP, string challenge, string response)
        {
            throw new NotSupportedException("CheckCaptcha method is not supported by SessionList class");
        }

        void IWebLoginProvider.SendEmail(string email, string subject, string body)
        {
            throw new NotSupportedException("SendEmail method is not supported by SessionList class");
        }

	    public void SendQuestionToSupport(string clientName, string trackerNumber, string clientContacts, string question)
	    {
	        throw new NotImplementedException();
	    }

	    public TrackerRegistrationTuple RegisterNikaTrackerForSmartphone()
	    {
            throw new NotSupportedException("RegisterNikaTrackerForSmartphone method is not supported by SessionList class");
	    }

	    public int? GetOperatorIDByAsid(string asid)
	    {
	        throw new NotImplementedException();
	    }

	    public int? GetOperatorIDBySimId(string simId, string name)
	    {
	        throw new NotImplementedException();
	    }

	    public ActivateNikaTrackerTrialResult ActivateNikaTrackerTrial(string simId)
	    {
	        throw new NotImplementedException();
	    }

	    public void SendCalculationRequest(CalculationRequest customerForm)
	    {
	        throw new NotImplementedException();
	    }

	    public bool SimIdExists(string simId)
	    {
	        throw new NotImplementedException();
	    }

	    public string CreateNewUser(string name)
	    {
	        throw new NotImplementedException();
	    }

	    public SetNewContactResult UpdatePhone(string phoneNumber, string keycode, int operatorid)
	    {
            throw new NotSupportedException("UpdatePhone method is not supported by SessionList class");
	    }

	    public void SendConfirmationEmail(string emailAddressTo, string subjectFormat, string messageFormatString, bool encodeAsUri, int operatorId, out int emailId)
	    {
	        throw new NotImplementedException();
	    }
	}

	internal interface ISessionListInternal
	{
		SessionListDataSet GetSessionList();

		ContainerListDataSet GetContainerList();

		void DeleteSession( int sessionID );
	}
}
