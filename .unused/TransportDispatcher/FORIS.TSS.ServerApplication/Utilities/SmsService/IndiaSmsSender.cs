﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using FORIS.TSS.ServerApplication.SmsService.Configuration;

namespace FORIS.TSS.ServerApplication.SmsService
{
	public class IndiaSmsSender : AbstractGenericSingleton<IndiaSmsSender>, ISmsSender
	{
		private readonly string _login;
		private readonly string _password;
		private readonly string _host;
		private readonly string _sender;
		private readonly bool _singleSmsNeedsReport;
		private const string SmsSingleSendUrlTemplate = @"{0}?usr={1}&pwd={2}&sndr={3}&ph={4}&text={5}&rpt={6}";
		private readonly bool _enabled;

		public IndiaSmsSender()
		{
			var configuration = (SmsProviderIndiaSectionHandler)ConfigurationManager.GetSection("SmsProviderIndia");
			_login                = configuration.Login;
			_password             = configuration.Password;
			_host                 = configuration.SendSmsUrl;
			_sender               = configuration.Sender;
			_enabled              = configuration.Enabled;
			_singleSmsNeedsReport = true;
		}

		public SendSmsResult SendSms(MessageTuple smsMessage)
		{
			if (!_enabled)
				return SendSmsResult.ServiceTurnedOff;
			var queryUrl = string.Format(SmsSingleSendUrlTemplate,
				_host,
				_login,
				_password,
				_sender, // Sender_IN ??
				smsMessage.Destination,
				smsMessage.Body,
				_singleSmsNeedsReport ? 1 : 0);

			var httpRequest = (HttpWebRequest)WebRequest.Create(queryUrl);
			httpRequest.Method = "GET";
			httpRequest.Timeout = 3000;
			string xResultString;

			try
			{
				var response = (HttpWebResponse)httpRequest.GetResponse();

				using (var stringReader = new StreamReader(response.GetResponseStream()))
					xResultString = stringReader.ReadToEnd();

				if (String.IsNullOrWhiteSpace(xResultString))
					return SendSmsResult.UnspecifiedError; // TODO: Unknown sms message send error.
				
				if (xResultString.IndexOf("Message Sent") < 0)
					return SendSmsResult.UnspecifiedError; // TODO: Add no sms message send status.

				var smsGuidString = xResultString.Substring(xResultString.IndexOf("Message Id:") + 11);
				if (!string.IsNullOrWhiteSpace(smsGuidString))
				{
					Guid smsGuid;
					if (!Guid.TryParse(smsGuidString, out smsGuid))
						return SendSmsResult.UnspecifiedError; // TODO: Add no report sms message id status.

					smsMessage.Message.SystemId = smsGuid.ToString();
				}

				Trace.WriteLine(xResultString);
			}
			catch (TimeoutException)
			{
				return SendSmsResult.Timeout;
			}
			catch (Exception ex)
			{
				Trace.WriteLine(ex);
				return SendSmsResult.HttpError;
			}

			return SendSmsResult.Success;
		}

		public SendSmsResult SendSms(MessageTuple smsMessage, ref int sendMask)
		{
			throw new NotImplementedException();
		}
	}
}