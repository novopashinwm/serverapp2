﻿using System;
using System.Runtime.Serialization;

namespace FORIS.TSS.ServerApplication.SmsService.Exceptions
{
	[Serializable]
	public class MPXException : Exception
	{
		//
		// For guidelines regarding the creation of new exception types, see
		//    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
		// and
		//    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
		//

		public MPXException()
		{
		}

		public MPXException(string message) : base(message)
		{
		}

		public MPXException(string message, Exception inner) : base(message, inner)
		{
		}

		protected MPXException(
			SerializationInfo info,
			StreamingContext context) : base(info, context)
		{
		}
	}
}