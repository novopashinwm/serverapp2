﻿using System;
using System.Configuration;
using System.Diagnostics;
using FORIS.TSS.ServerApplication.SmsService.Configuration;
using EasySMPP;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.ServerApplication.SmsService
{
	public class SmppIndiaSmsSender : AbstractGenericSingleton<SmppIndiaSmsSender>, ISmsSender
	{
		private readonly string _description;
		private readonly string _host;
		private readonly int _port;

		private readonly string _systemId;
		private readonly string _password;

		private readonly string _systemType;
		private readonly byte _addrTon;
		private readonly byte _addrNpi;
		private readonly string _addressRange;
		private readonly int _sequenceNumber;

		private readonly string _sender;
		private readonly bool _testModeOn;

		private SMPPClient _client;
		private readonly bool _enabled;


		public SendSmsResult SendSms(MessageTuple smsMessage)
		{
			if (!_enabled)
				return SendSmsResult.ServiceTurnedOff;
			if (_client == null)
				throw new Exception("Connection Not initialized. Use internal Initialize() method.");

			if (!_client.CanSend)
				return SendSmsResult.Timeout;

			return SendSmsResult.UnspecifiedError;
		}

		public SendSmsResult SendSms(MessageTuple smsMessage, ref int sendMask)
		{
			return SendSmsResult.ThrottlingError;
			//throw new NotImplementedException();
		}

		public SmppIndiaSmsSender()
		{
			var configuration = (SmsProviderSmppSectionHandler)ConfigurationManager.GetSection("SmsProviderSmppIndia");
			_description = configuration.Description;
			_host = configuration.Host;
			_port = configuration.Port;

			_systemId = configuration.SystemId;
			_password = configuration.Password;

			_systemType = configuration.SystemType;
			_addrTon = configuration.AddrTon;
			_addrNpi = configuration.AddrNpi;
			_addressRange = configuration.AddressRange;
			_sequenceNumber = configuration.SequenceNumber;

			_sender = Server.Instance().GetConstant(Constant.ServiceInternationalPhoneNumber);
			_testModeOn = configuration.TestModeOn;

			// TODO: Initialize due to config settings.
			_enabled = configuration.Enabled;

			Initialize();
		}

		internal void Initialize()
		{
			_client = new SMPPClient();

			_client.EnquireLinkTimeout = 59000;
			_client.ReconnectTimeout = 91000;

			_client.LogLevel = LogLevels.LogAll;

			_client.OnLog += client_OnLog;
			_client.OnSubmitSmResp += _client_OnSubmitSmResp;

			_client.AddSMSC(new SMSC(
					_description,
					_host,
					_port,
					_systemId,
					_password,
					_systemType,
					_addrTon,
					_addrNpi,
					_addressRange,
					_sequenceNumber,
					_testModeOn));

			if (_enabled)
				_client.Connect();
		}

		private static void _client_OnSubmitSmResp(SubmitSmRespEventArgs e)
		{
			string result;
			switch (e.Status)
			{
				case StatusCodes.ESME_ROK:
					result = "OK";
					break;
				case StatusCodes.ESME_RTHROTTLED:
					result = "THROTTLED";
					break;
				case StatusCodes.ESME_RMSGQFUL:
					result = "MQF";
					break;
				default:
					result = "UNHANDLED";
					break;
			}
			Trace.WriteLine(string.Format("*** messageid: {0}, status: {1}, sequence: {2}", e.MessageId, result, e.Sequence));
		}

		private static void client_OnLog(LogEventArgs e)
		{
			Trace.WriteLine(e.Message);
		}
	}
}