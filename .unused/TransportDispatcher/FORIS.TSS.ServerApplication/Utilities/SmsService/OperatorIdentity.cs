﻿using System;
using System.Security.Principal;
using System.Threading;

namespace FORIS.TSS.Service.SmsService
{
    internal class OperatorIdentity : IDisposable
    {
        private readonly IPrincipal _oldPrincipal;

        public OperatorIdentity(string operatorName)
        {
            var gi = new GenericIdentity(operatorName);
            _oldPrincipal = Thread.CurrentPrincipal;
            Thread.CurrentPrincipal = new GenericPrincipal(gi, null);
        }

        public void Dispose()
        {
            Thread.CurrentPrincipal = _oldPrincipal;
        }
    }
}
