﻿using System;
using System.Configuration;

namespace FORIS.TSS.ServerApplication.SmsService.Configuration
{
	public class ContentProviderSectionHandler : ConfigurationSection
	{
		private const string ShortNumberKey = "ShortNumber";

		[ConfigurationProperty(ShortNumberKey, IsRequired = true)]
		public string ShortNumber
		{
			get { return (string) this[ShortNumberKey]; }
			set { this[ShortNumberKey] = value; }
		}

		private const string SendSmsPasswordKey = "SendSmsPassword";

		[ConfigurationProperty(SendSmsPasswordKey, IsRequired = true)]
		public string SendSmsPassword
		{
			get { return (string) this[SendSmsPasswordKey]; }
			set { this[SendSmsPasswordKey] = value; }
		}

		private const string SendSmsLoginKey = "SendSmsLogin";

		[ConfigurationProperty(SendSmsLoginKey, IsRequired = true)]
		public string SendSmsLogin
		{
			get { return (string) this[SendSmsLoginKey]; }
			set { this[SendSmsLoginKey] = value; }
		}

		private const string SendSmsUriKey = "SendSmsUri";

		[ConfigurationProperty(SendSmsUriKey, IsRequired = true)]
		public string SendSmsUri
		{
			get { return (string) this[SendSmsUriKey]; }
			set { this[SendSmsUriKey] = value; }
		}

		private const string PasswordLengthKey = "PasswordLength";

		[ConfigurationProperty(PasswordLengthKey, IsRequired = false, DefaultValue = 8)]
		public int PasswordLength
		{
			get { return (int) this[PasswordLengthKey]; }
			set { this[PasswordLengthKey] = value; }
		}

		private const string DefaultOperatorAliasKey = "DefaultOperatorAlias";

		[ConfigurationProperty(DefaultOperatorAliasKey, IsRequired = false, DefaultValue = "")]
		public string DefaultOperatorAlias
		{
			get { return (string) this[DefaultOperatorAliasKey]; }
			set { this[DefaultOperatorAliasKey] = value; }
		}

		private const string DefaultMegafonOperatorAliasKey = "DefaultMegafonOperatorAlias";

		[ConfigurationProperty(DefaultMegafonOperatorAliasKey, IsRequired = false, DefaultValue = "Megafon")]
		public string DefaultMegafonOperatorAlias
		{
			get { return (string) this[DefaultMegafonOperatorAliasKey]; }
			set { this[DefaultMegafonOperatorAliasKey] = value; }
		}

		private const string DefaultLongitudeKey = "DefaultLongitude";

		[ConfigurationProperty(DefaultLongitudeKey, IsRequired = false, DefaultValue = 37.5)]
		public double DefaultLongitude
		{
			get { return (double)this[DefaultLongitudeKey]; }
			set { this[DefaultLongitudeKey] = value; }
		}

		private const string DefaultLatitudeKey = "DefaultLatitude";

		[ConfigurationProperty(DefaultLatitudeKey, IsRequired = false, DefaultValue = 55.75)]
		public double DefaultLatitude
		{
			get { return (double)this[DefaultLatitudeKey]; }
			set { this[DefaultLatitudeKey] = value; }
		}

		/// <summary>
		/// Максимальная пропускная способность MPX-сервера: количество транзакций в секунду (Transaction Per Second)
		/// </summary>
		private const string MaxTPSKey = "MaxTPS";
		[ConfigurationProperty(MaxTPSKey, IsRequired = false, DefaultValue = 4)]
		public int MaxTPS
		{
			get { return (int) this[MaxTPSKey]; }
			set { this[MaxTPSKey] = value; }
		}

		private const string MaxSmsPartsCountKey = "MaxSmsPartsCount";

		/// <summary> Максимальное количество частей в sms сообщении, которое способен принять MPX send sms interface для одного пользователя </summary>
		/// <remarks> Значение физически ограничивается пропускной способностью MPX-сервера и SMSC </remarks>
		[ConfigurationProperty(MaxSmsPartsCountKey, IsRequired = false, DefaultValue = 3)]
		public int MaxSmsPartsCount
		{
			get { return (int) this[MaxSmsPartsCountKey]; }
			set { this[MaxSmsPartsCountKey] = value; }
		}

		private const string MaxSmsPartSizeKey = "MaxSmsPartSize";

		[ConfigurationProperty(MaxSmsPartSizeKey, IsRequired = false, DefaultValue = 63)]
		public int MaxSmsPartSize
		{
			get { return (int) this[MaxSmsPartSizeKey]; }
			set { this[MaxSmsPartSizeKey] = value; }
		}

		private const string RetryWaitingTimeKey = "RetryWaitingTime";

		[ConfigurationProperty(RetryWaitingTimeKey, IsRequired = true, DefaultValue = "60")]
		public TimeSpan RetryWaitingTime
		{
			get { return (TimeSpan) this[RetryWaitingTimeKey]; }
			set { this[RetryWaitingTimeKey] = value; }
		}

		private const string RetryThrottlingTimeKey = "RetryThrottlingTime";

		[ConfigurationProperty(RetryThrottlingTimeKey, IsRequired = false, DefaultValue = "14400")]
		public TimeSpan RetryThrottlingTime
		{
			get { return (TimeSpan)this[RetryThrottlingTimeKey]; }
			set { this[RetryThrottlingTimeKey] = value; }
		}

		private const string EnabledKey = "Enabled";

		[ConfigurationProperty(EnabledKey, IsRequired = false, DefaultValue = true)]
		public bool Enabled
		{
			get { return (bool) this[EnabledKey]; }
			set { this[EnabledKey] = value; }
		}

		private const string DebugModeKey = "DebugMode";

		[ConfigurationProperty(DebugModeKey, IsRequired = false, DefaultValue = false)]
		public bool DebugMode
		{
			get { return (bool)this[DebugModeKey]; }
			set { this[DebugModeKey] = value; }
		}

		private const string PeriodBetweenSuccessfulPositionAskingKey = "PeriodBetweenSuccessfulPositionAsking";

		[ConfigurationProperty(PeriodBetweenSuccessfulPositionAskingKey, IsRequired = true)]
		public TimeSpan PeriodBetweenSuccessfulPositionAsking
		{
			get { return (TimeSpan)this[PeriodBetweenSuccessfulPositionAskingKey]; }
			set { this[PeriodBetweenSuccessfulPositionAskingKey] = value; }
		}

		private const string MaxCommandAgeKey = "MaxCommandAge";
		[ConfigurationProperty(MaxCommandAgeKey, IsRequired = false, DefaultValue = "86400" /*1 сутки по умолчанию*/)]
		public TimeSpan MaxCommandAge
		{
			get { return (TimeSpan)this[MaxCommandAgeKey]; }
			set { this[MaxCommandAgeKey] = value; }
		}

		private const string SendCdrUriKey = "SendCdrUri";

		[ConfigurationProperty(SendCdrUriKey, IsRequired = true)]
		public string SendCdrUri
		{
			get { return (string)this[SendCdrUriKey]; }
			set { this[SendCdrUriKey] = value; }
		}
	}
}