﻿using System;
using System.Configuration;

namespace FORIS.TSS.ServerApplication.SmsService.Configuration
{
	public class SmsProviderIndiaSectionHandler : ConfigurationSection
	{
		private const string LoginKey = "Login";

		[ConfigurationProperty(LoginKey, IsRequired = true, DefaultValue = "Login")]
		public string Login
		{
			get { return (string)this[LoginKey]; }
			set { this[LoginKey] = value; }
		}

		private const string PasswordKey = "Password";

		[ConfigurationProperty(PasswordKey, IsRequired = true, DefaultValue = "Password")]
		public string Password
		{
			get { return (string)this[PasswordKey]; }
			set { this[PasswordKey] = value; }
		}

		private const string SendSmsUrlKey = "SendSmsUrl";

		[ConfigurationProperty(SendSmsUrlKey, IsRequired = true, DefaultValue = "http://site.com")]
		public string SendSmsUrl
		{
			get { return (string)this[SendSmsUrlKey]; }
			set { this[SendSmsUrlKey] = value; }
		}

		private const string SenderKey = "Sender";

		[ConfigurationProperty(SenderKey, IsRequired = true, DefaultValue = "test")]
		public string Sender
		{
			get { return (string)this[SenderKey]; }
			set { this[SenderKey] = value; }
		}

		private const string EnabledKey = "Enabled";

		[ConfigurationProperty(EnabledKey, IsRequired = false, DefaultValue = "true")]
		public bool Enabled
		{
			get { return (bool)this[EnabledKey]; }
			set { this[EnabledKey] = value; }
		}
	}
}