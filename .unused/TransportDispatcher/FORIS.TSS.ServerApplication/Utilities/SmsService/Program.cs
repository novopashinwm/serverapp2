﻿using System;
using System.Configuration;
using System.Runtime.Remoting;
using FORIS.TSS.Service.SmsService.Configuration;

namespace FORIS.TSS.Service.SmsService
{
    class Program
    {
        public static void Main(string[] args)
        {
            RemotingConfiguration.Configure(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile, false);

            if (args.Length == 0)
                return;
            
            var requestor = args[0];
            var text = args.Length > 1 ? args[1] : string.Empty;
            var incomingSms = new IncomingSms
                                  {
                                      Requestor = requestor,
                                      Text = text,
                                      TimeReceived = DateTime.Now
                                  };
            using (var smsProcessor = new SmsDispatcher(
                (ContentProviderSectionHandler)ConfigurationManager.GetSection("ContentProvider")))
            {
                smsProcessor.ProcessSms(incomingSms);
            }
        }
    }
}