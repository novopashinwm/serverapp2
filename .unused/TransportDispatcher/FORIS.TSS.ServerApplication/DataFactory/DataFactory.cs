﻿using System.Collections;
using System.ComponentModel;
using System.Collections.Generic;

using FORIS.TSS.BusinessLogic.Interfaces.Data;
using FORIS.TSS.Common;
using FORIS.TSS.ServerApplication.Helpers;

namespace FORIS.TSS.ServerApplication.DataFactory
{
	public abstract class DataFactory<TKey, TData> : Component, IEnumerable//<KeyValuePair<TKey,WeakReference<TData>>>
		where TKey : DataParams
		where TData : DataBase<TKey>
	{
		public TData this[TKey key]
		{
			get
			{
				/* В случае, если в это время идет построение контейнера данных
				 * с некоторыми параметрами для помещения в хэш, то будет довольно 
				 * длительное ожидание, однако имеет смысл подождать, если строится 
				 * контейнер именно с теми параметрами, которые нам нужны сечас
				 * Так как мы не знаем с какими именно параметрами строится контейнер, 
				 * из-за которого происходит ожидание, то будем ждать в любом случае.
				 */

				lock (this.hash)
				{

					if (this.hash.ContainsKey(key))
					{
						TData DataHolder = this.hash[key].Target;

						if (DataHolder != null)
						{
							return DataHolder;
						}
					}

					TData Data = this.CreateData(key);

					if (this.hash.ContainsKey(key)) this.hash.Remove(key);

					this.hash.Add(key, new WeakReference<TData>(Data));

					return Data;
				}
			}
		}

		private readonly Dictionary<TKey, WeakReference<TData>> hash;

		#region Constructor & Dispose

		public DataFactory()
		{
			this.hash = new Dictionary<TKey, WeakReference<TData>>();

		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				foreach (WeakReference<TData> reference in this.hash.Values)
				{
					TData DataHolder = reference.Target;

					if (DataHolder != null)
					{
						DataHolder.Dispose();
					}
				}
			}
			base.Dispose(disposing);
		}

		#endregion // Constructor & Dispose

		protected abstract TData CreateData(TKey @params);

		///<summary>
		///Returns an enumerator that iterates through a collection.
		///</summary>
		///
		///<returns>
		///An <see cref="T:System.Collections.IEnumerator"></see> object that can be used to iterate through the collection.
		///</returns>
		///<filterpriority>2</filterpriority>
		public IEnumerator GetEnumerator()
		{
			return this.hash.GetEnumerator();
		}
	}
}