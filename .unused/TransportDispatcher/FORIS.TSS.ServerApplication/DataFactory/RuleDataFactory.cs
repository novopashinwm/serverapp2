using System.ComponentModel;

using FORIS.TSS.BusinessLogic.Interfaces.Data;
using FORIS.TSS.Helpers;
using FORIS.TSS.ServerApplication.Helpers;

namespace FORIS.TSS.ServerApplication.DataFactory.Rule
{
	public class RuleDataFactory: DataFactory<DataParams, RuleData>
	{
		#region Constructor & Dispose

		public RuleDataFactory( IContainer container )
		{
			container.Add( this );
		}
		public RuleDataFactory()
		{

		}

		#endregion // Constructor & Dispose

		protected override RuleData CreateData( DataParams @params )
		{
			RuleData Data = new RuleData();

			using( IDatabaseDataSupplier DatabaseDataSupplier = new DataBaseManager() )
			{
				Data.LoadData( DatabaseDataSupplier );
			}

			return Data;
		}
	}
}

