﻿using System.ComponentModel;

using FORIS.TSS.BusinessLogic.Interfaces.Data;
using FORIS.TSS.Helpers;
using FORIS.TSS.ServerApplication.Helpers;

namespace FORIS.TSS.ServerApplication.DataFactory.Calendar
{
	public class CalendarDataFactory : DataFactory<DataParams, CalendarData>
	{
		#region Constructor & Dispose

		public CalendarDataFactory(IContainer container)
		{
			container.Add(this);
		}
		public CalendarDataFactory()
		{

		}

		#endregion // Constructor & Dispose

		protected override CalendarData CreateData(DataParams @params)
		{
			CalendarData Data = new CalendarData();

			using (IDatabaseDataSupplier DatabaseDataSupplier = new DataBaseManager())
			{
				Data.LoadData(DatabaseDataSupplier);
			}

			return Data;
		}
	}
}