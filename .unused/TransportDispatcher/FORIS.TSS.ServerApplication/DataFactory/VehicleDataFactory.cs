using System.ComponentModel;

using FORIS.TSS.BusinessLogic.Interfaces.Data;
using FORIS.TSS.Helpers;
using FORIS.TSS.ServerApplication.Helpers;

namespace FORIS.TSS.ServerApplication.DataFactory.Vehicle
{
	public class VehicleDataFactory: DataFactory<DataParams, VehicleData>
	{
		#region Constructor & Dispose

		public VehicleDataFactory( IContainer container )
		{
			container.Add( this );
		}
		public VehicleDataFactory()
		{

		}

		#endregion // Constructor & Dispose

		protected override VehicleData CreateData( DataParams @params )
		{
			VehicleData Data = new VehicleData();

			using( IDatabaseDataSupplier DatabaseDataSupplier = new DataBaseManager() )
			{
				Data.LoadData( DatabaseDataSupplier );
			}

			return Data;
		}
	}
}

