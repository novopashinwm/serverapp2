using System;
using System.Data;
using System.Diagnostics;
using FORIS.DataAccess;
using FORIS.TSS.IO;
using FORIS.TSS.Terminal;

namespace FORIS.TSS.ServerApplication
{
	/// <summary>
	/// This class contains helper functions for raising alarms
	/// </summary>
	public class DataBaseManagerB
	{
		/// <summary>
		/// Raised when there is no signal for fixed period for some vehicle
		/// </summary>
		/// <param name="vehicle_id">id of vehicle, which doesn't send signals</param>
		/// <param name="interval">length of the interval when there was no signal</param>
		public static void AlarmNoSignal(int vehicle_id, TimeSpan interval)
		{
			AlarmNoSignal(vehicle_id, (int)interval.TotalSeconds);
		}
		/// <summary>
		/// Raised when there is no signal for fixed period for some vehicle
		/// </summary>
		/// <param name="vehicle_id">id of vehicle, which doesn't send signals</param>
		/// <param name="seconds">length of the interval when there was no signal</param>
		public static void AlarmNoSignal(int vehicle_id, int seconds)
		{
			try
			{
				using(StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.AlarmNoSignal"))
				{
					sp["@vehicle_id"].Value = vehicle_id;
					sp["@seconds"].Value = seconds;
					sp.ExecuteNonQuery();
				}
			}
			catch(Exception exception)
			{
				Logger.WriteEntry(exception.Message, LogType.Error, LogCategory.DataBase, LogPriority.Urgent, 0);
				throw exception;
			}
		}
		/// <summary>
		/// Raised 
		/// </summary>
		/// <param name="route_id">identifier of route</param>
		/// <param name="vehicle1_id">first vehicle</param>
		/// <param name="vehicle2_id">second vehicle</param>
		/// <param name="distance">distance between vehicles</param>
		public static void AlarmVehiclesTooClose(int route_id, int vehicle1_id, int vehicle2_id, double distance)
		{
			try
			{
				using(StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.AlarmVehiclesTooClose"))
				{
					sp["@route_id"].Value = route_id;
					sp["@vehicle1_id"].Value = vehicle1_id;
					sp["@vehicle2_id"].Value = vehicle2_id;
					sp["@distance"].Value = distance;
					sp.ExecuteNonQuery();
				}
			}
			catch(Exception exception)
			{
				Logger.WriteEntry(exception.Message, LogType.Error, LogCategory.DataBase, LogPriority.Urgent, 0);
				throw exception;
			}
		}
		/// <summary>
		/// Raised when there is no waybills for some route
		/// </summary>
		/// <param name="schedule_id">schedule, which was analysed</param>
		/// <param name="date">date, for which waybills was not entered</param>
		public static void AlarmNoWaybills(int schedule_id, DateTime date)
		{
			try
			{
				using(StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.AlarmNoWaybills"))
				{
					sp["@schedule_id"].Value = schedule_id;
					sp["@date"].Value = date;
					sp.ExecuteNonQuery();
				}
			}
			catch(Exception exception)
			{
				Logger.WriteEntry(exception.Message, LogType.Error, LogCategory.DataBase, LogPriority.Urgent, 0);
				throw exception;
			}
		}
		/// <summary>
		/// Raised when vehicle doesn't arrive at the spesified time in the specified place
		/// </summary>
		/// <param name="route_id">identifier of route</param>
		/// <param name="vehicle_id">identifier of vehicle</param>
		/// <param name="schedule_point_id">identifier of schedule point</param>
		/// <param name="interval">length of the interval when there was no vehicle in the point</param>
		public static void AlarmMissedSchedulePoint(int route_id, int vehicle_id, int schedule_point_id, TimeSpan interval)
		{
			AlarmMissedSchedulePoint(route_id, vehicle_id, schedule_point_id, interval);
		}
		/// <summary>
		/// Raised when vehicle doesn't arrive at the spesified time in the specified place
		/// </summary>
		/// <param name="route_id">identifier of route</param>
		/// <param name="vehicle_id">identifier of vehicle</param>
		/// <param name="schedule_point_id">identifier of schedule point</param>
		/// <param name="delta">length of the interval when there was no vehicle in the point</param>
		public static void AlarmMissedSchedulePoint(int route_id, int vehicle_id, int schedule_point_id, int delta)
		{
			try
			{
				using(StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.AlarmMissedSchedulePoint"))
				{
					sp["@route_id"].Value = route_id;
					sp["@vehicle_id"].Value = vehicle_id;
					sp["@schedule_point_id"].Value = schedule_point_id;
					sp["@delta"].Value = delta;
					sp.ExecuteNonQuery();
				}
			}
			catch(Exception exception)
			{
				Logger.WriteEntry(exception.Message, LogType.Error, LogCategory.DataBase, LogPriority.Urgent, 0);
				throw exception;
			}
		}
		/// <summary>
		/// called when vehicle sends alarm
		/// </summary>
		/// <param name="type">1 - on, 0 - off</param>
		/// <param name="text">alarm text</param>
		/// <param name="ui">unit</param>
		/// <param name="dt">when, DateTime.MinValue means unknown</param>
		public static void Alarm(int type, string text, IUnitInfo ui, DateTime dt)
		{
			try
			{
				using(StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.Alarm"))
				{
					sp["@type"].Value = type;
					sp["@text"].Value = text != null? text.Trim(): "";
					sp["@uniqueID"].Value = ui.Unique;
					sp["@id"].Value = ui.ID;
					sp["@phone"].Value = ui.Telephone;
					sp["@log_time"].Value = dt;
					using(new ConnectionOpener(sp.Command.Connection))
					{
						using(IDataReader dr = sp.ExecuteReader())
						{
							if(dr == null || dr.IsClosed || !dr.Read()) return;

							ui.Unique = (int) dr["Unique"];
							ui.ID = (int) dr["ID"];
							ui.Telephone = (string) dr["Phone"];
						}
					}
				}
			}
			catch(Exception exception)
			{
				Logger.WriteEntry(exception.Message, LogType.Error, LogCategory.DataBase, LogPriority.Urgent, 0);
				throw exception;
			}
		}

        public static int NewMessage(string sSubject, string sBody, int iSourceId, out DateTime dtTime)
        {
			dtTime = DateTime.Now;
			
			try
            {
                using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.NewMessage"))
                {
                    sp["@subject"].Value = sSubject;
                    sp["@body"].Value = sBody;
					sp["@time"].Value = dtTime.ToUniversalTime();
                    sp["@source_id"].Value = iSourceId;

					object oReturn = sp.ExecuteScalar();

                    if (oReturn != null)
                        return (int)oReturn;
                }
            }
            catch (Exception exception)
            {
				Trace.WriteLine("Can't save new rule manager message in DB!");
				Trace.TraceError("{0}", exception);
				Trace.WriteLine(exception.StackTrace);
            }

            return 0;
        }
    }
}