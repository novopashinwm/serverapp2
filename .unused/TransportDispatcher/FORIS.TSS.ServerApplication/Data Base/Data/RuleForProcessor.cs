﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO.Rules;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.ServerApplication.Data_Base.Data
{
    [Serializable]
    public class RuleForProcessor : Rule
    {
        /// <summary>
        /// Словарь: ключ - VehicleID, значение - список индексов новых позиций для данного правила
        /// </summary>
        public Dictionary<int, List<int>> NewGeoLogIndices = 
            new Dictionary<int, List<int>>();

        /// <summary>
        /// Словарь: ключ - VehicleID, значение - список индексов новых записей ControllerSensorLog для данного правила
        /// </summary>
        public Dictionary<int, List<KeyValuePair<SensorLegend, int>>> NewSensorLogIndices = 
            new Dictionary<int, List<KeyValuePair<SensorLegend, int>>>();
    }
}
