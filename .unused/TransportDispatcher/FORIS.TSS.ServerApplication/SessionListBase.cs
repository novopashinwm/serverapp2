using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Collections.Generic;
using System.Threading;
using System.Security.Principal;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Lifetime;
using System.Net;

using FORIS.DataAccess;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.BusinessLogic.Interfaces.Data;
using FORIS.TSS.BusinessLogic.Interfaces.Data.Roster;
using FORIS.TSS.Common;
using FORIS.TSS.EntityModel;
using FORIS.TSS.EntityModel.History;
using FORIS.TSS.Infrastructure.Authentication;
using FORIS.TSS.Infrastructure.UserName;
using FORIS.TSS.ServerApplication.Helpers;
using FORIS.TSS.Interfaces.Diagnostic;
using Interfaces.Web;

namespace FORIS.TSS.ServerApplication
{
	/// <summary>
	/// �����, ����������� ������� ������ ��� ������ � ��������
	/// </summary>
    public class SessionListBase : Component, ISessionList, ISessionListInternal, IDiagnosticHelper
	{
		protected Dictionary<Guid, Common.WeakReference<PersonalServer>> InnerHashtable =
            new Dictionary<Guid, Common.WeakReference<PersonalServer>>();

		/// <summary>
		/// �����, ����������� ������� ������ ��� ������ � ��������
		/// </summary>
		public SessionListBase()
		{
			this.sessionKeeperTimer =
				new System.Threading.Timer(
					new TimerCallback(sessionKeeperTimer_TimerCallback),
					null,
					LifetimeServices.LeaseTime,
					LifetimeServices.LeaseManagerPollTime
					);
		}
        public CheckPasswordResult CheckPasswordForWeb(string login, string password)
        {
            return Server.Instance().CheckPasswordForWeb(login, password);
        }




		protected override void Dispose( bool disposing )
		{
			if( disposing)
			{
				this.sessionKeeperTimer.Dispose();
			}

			base.Dispose( disposing );
		}

		public override System.Object InitializeLifetimeService()
		{
			return null;
		}

		/// <summary>
		/// �������� ����� ������
		/// </summary>
		/// <returns>������ �� ����� ������</returns>
		public IPersonalServer CreateSession(Guid clientID, string clientVersion, string login, string password)
		{
			GenericIdentity gi = new GenericIdentity(login);
			Thread.CurrentPrincipal = new GenericPrincipal(gi, null);

			return CreateSession(clientID, clientVersion);
		}


		/// <summary>
		/// �������� ����� ������
		/// </summary>
		/// <returns>������ �� ����� ������</returns>
		public IPersonalServer CreateSession( Guid clientID, string clientVersion )
		{
			Trace.WriteLine("CreateSession: " + clientID.ToString() + " " + clientVersion);

			/* �����:
			 * ������ �������� ������� � ����� �������, ��� �����
			 * ������� ����� ����� ���� ���� � ��� ������,
			 * ���� ClientAuthentication � SSPIAuthentication ���������.
			 * � ������ ��������� ������������ ������ ����� �� �� ����� �����.
			 * � ���� �������� �� ����� ����������� ����������� ������������ 
			 * ��������� � ����� �� ������������ � ����������.
			 */

			if( Thread.CurrentPrincipal == null )
			{
				Trace.WriteLine("CreateSession: Thread.CurrentPrincipal == null to be returned!");
				// client should provide some login information
				return null;
			}

			#region Create OperatorInfo structure on user Identity

			/* ���� ������� ������� ���������, ������ � ������������� 
			 * ��� � �������, ������ ����������� ��� ������ (������� ���� � ������).
			 */

			IIdentity Identity = Thread.CurrentPrincipal.Identity;

			IOperatorInfo OperatorInfo = 
				OperatorInfoHelper.Create( Identity.Name, Server.Instance().DbManager );

			#endregion // Create OperatorInfo structure on user Identity

			#region Get current connection user IP address 

			IPAddress Address;

			IPrincipal Principal = Thread.CurrentPrincipal;

			if( Principal is CustomPrincipal )
			{
				/* ������ ��� ������������ �������� � ���������.
				 * �������������� �� ����� ���� �� ���������, ������ �� ��������.
				 */

				Address = ( (CustomPrincipal)Principal ).IPAddress;
			}
			else
			{
				Address = IPAddress.Loopback;
			}

			#endregion // Get current connection user IP address

			lock( this )
			{
				if( OperatorInfo == null )
				{
					throw new ApplicationException( "Does not exist OperatorInfo structure" );
				}

				#region Only one session for login at one time

                foreach (Common.WeakReference<PersonalServer> sessionReference in this.InnerHashtable.Values)
				{
					PersonalServer SessionHolder = sessionReference.Target;

					if( SessionHolder != null &&
						SessionHolder.SessionInfo.OperatorInfo.Equals( OperatorInfo )
						)
					{
						throw new SessionExistException();
					}
				}

				#endregion Only one session for login at one time

				#region Try find existed PersonalServer

				PersonalServer Result = this.GetPersonalServer( clientID );
				
				if( Result != null )
				{
					if(
						Result.SessionInfo.IP.Equals( Address ) &&
						Result.SessionInfo.OperatorInfo.Equals( OperatorInfo )
						)
					{
						Debug.WriteLine(
							string.Format(
								"Existed session for '{0}' was returned",
								Result.SessionInfo.OperatorInfo.Login
								)
							);

						return Result;
					}

					throw new ApplicationException( "���������� �������������� �������" );
				}

				#endregion // Try find existed PersonalServer

				ISessionInfo SessionInfo = 
					CreateNewSessionInDatabase( 
						OperatorInfo, 
						DateTime.UtcNow,
						clientID,
						clientVersion 
						);

				Result = new PersonalServer();

				Result.Init( this, SessionInfo );

				Result.Closed += new EventHandler( Session_Closed );

				#region Put weak reference of PersonalServer object into InnerHashtable

				/* ��� ��� ��� ������ ������� �� ������-������ ����������� �����,
				 * �� � �����-�� ��� ������ �������� ������� ������ ��� �������.
				 */

                this.InnerHashtable.Add(clientID, new Common.WeakReference<PersonalServer>(Result));

				Debug.WriteLine(
					string.Format(
						"New session for '{0}' was added",
						SessionInfo.OperatorInfo.Login
						)
					);

				#endregion // Put weak reference of PersonalServer object into InnerHashtable

				Trace.WriteLine("CreateSession: completed!");

				return Result;
			}
		}

		#region Implement ISessionListInternal

		/// <summary>
		/// �������� ������ ������
		/// </summary>
		/// <returns></returns>
		SessionListDataSet ISessionListInternal.GetSessionList()
		{
			SessionListDataSet Result = new SessionListDataSet();

			lock( this )
			{
                foreach (Common.WeakReference<PersonalServer> reference in this.InnerHashtable.Values)
				{
					PersonalServer Session = reference.Target;

					if( Session != null )
					{
						SessionListDataSet.SessionListRow Row = Result.SessionList.NewSessionListRow();

						Row.SessionID = Session.SessionID;
						Row.OperatorID = Session.OperatorID;
						Row.IP = Session.SessionInfo.IP.ToString();
						Row.HostName = Session.SessionInfo.HostName;
						Row.StartOn = Session.SessionInfo.StartOn;

						#region StopOn

						if( Session.SessionInfo.StopOn != null )
							Row.StopOn = (DateTime)Session.SessionInfo.StopOn;
						else
							Row.SetStopOnNull();

						#endregion

						Row.Login = Session.Login;
						Row.Name = Session.Operator.Name;

						#region Lease

						ILease Lease = (ILease)RemotingServices.GetLifetimeService( Session );

						if( Lease != null )
						{
							Row.Lease =
								String.Format(
									"{0} - {1}",
									Lease.CurrentLeaseTime,
									Lease.CurrentState
									);
						}
						else
						{
							Row.Lease = "��� ��������";
						}

						#endregion // Lease

						Row.State = Session.State.ToString();

						Result.SessionList.Rows.Add( Row );
					}
				}
			}

			return Result;
		}

		ContainerListDataSet ISessionListInternal.GetContainerList()
		{
			ContainerListDataSet Result = new ContainerListDataSet();
			return Result;
		}

		/// <summary>
		/// ������� ������
		/// </summary>
		/// <param name="sessionID">������������� ������</param>
		void ISessionListInternal.DeleteSession( int sessionID )
		{
			lock( this )
			{
				PersonalServer Session = null;

                foreach (Common.WeakReference<PersonalServer> reference in this.InnerHashtable.Values)
				{
					PersonalServer SessionHolder = reference.Target;

					if( SessionHolder != null && SessionHolder.SessionInfo.SessionID == sessionID )
					{
						Session = reference.Target;
					}
				}

				if( Session != null )
				{
					Session.Close();
				}
			}
		}

		#endregion // Implement ISessionListInternal

		/// <summary>
		/// ������� ������
		/// </summary>
		/// <param name="ps">������ �� ������</param>
		public void ExtremeDeleteSession(IPersonalServer ps)
		{
			lock (this)
			{
                foreach (KeyValuePair<Guid, Common.WeakReference<PersonalServer>> entry in this.InnerHashtable)
				{
					PersonalServer SessionHolder = entry.Value.Target;

					if( SessionHolder != null && SessionHolder == ps )
					{
						string Login = SessionHolder.Login;

						this.InnerHashtable.Remove( entry.Key );
						
						SessionHolder.Close();

						Debug.WriteLine( "Session for '" + Login + "' was removed EXRTREMALY!!! ~~~~~~~~~~~~~~~~~~~~~~~~~" );

						break;
					}
				}
			}
		}

		protected bool IsAdmin( IPersonalServer ps )
		{
			if( ps == null )
			{
				return false; // wrong session
			}
			if( ps != GetPersonalServer( ps.SessionInfo.ClientID ) )
			{
				return false; // strange foreign session
			}
			SystemRights rights = ps.LogicForUI.GetSystemRights();
			if( rights.Contains( SystemRight.ServerAdministration ) == false )
			{
				return false; // no access
			}
			return true;
		}

		protected PersonalServer GetPersonalServer( Guid clientID )
		{
			if( this.InnerHashtable.ContainsKey( clientID ) )
			{
				PersonalServer SessionHolder = this.InnerHashtable[clientID].Target;

				if( SessionHolder == null )
				{
					this.InnerHashtable.Remove( clientID );

					/* � ��� ��� ���� ������ ��� ����� ������� ������
					 */
					Debug.WriteLine( "Session '" + clientID.ToString() + "' was removed'" );
				}

				return SessionHolder;
			}

			return null;
		}
		
		public bool IsActive()
		{
			return Server.Instance().IsActive;
		}

		protected ISessionInfo CreateNewSessionInDatabase( IOperatorInfo operatorInfo, DateTime sessionStart, Guid clientID, string clientVersion )
		{
		    IPAddress IP;
		    IPrincipal principal = Thread.CurrentPrincipal;
		    
            if (principal is CustomPrincipal)
		    {
		        CustomPrincipal customPrincipal = (CustomPrincipal) principal;
		        IP = customPrincipal.IPAddress;
		    }
		    else
		    {
		        IP = IPAddress.Loopback;
		    }

		    string hostname = IP.ToString();

		    using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.InsertSession"))
		    {
		        sp["@OPERATOR_ID"].Value = operatorInfo.OperatorID;
		        sp["@HOST_IP"].Value = IP.ToString();
		        sp["@HOST_NAME"].Value = hostname;
		        sp["@SESSION_START"].Value = sessionStart;
		        sp["@SESSION_END"].Value = DBNull.Value;
		        sp["@CLIENT_VERSION"].Value = clientVersion;
		        var session_id = (int) sp.ExecuteScalar();

		        return new SessionInfo(
		            session_id,
		            operatorInfo,
		            clientID,
		            clientVersion,
		            hostname,
		            IP,
		            sessionStart
		            );
		    }
		}

	    protected void CloseSessionInDatabase(int session_id, DateTime endTime)
		{
			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.CloseSession");
			sp["@SESSION_ID"].Value = session_id;
			sp["@SESSION_END"].Value = endTime.ToUniversalTime();
			sp.ExecuteNonQuery();
		}

		private void Session_Closed( object sender, EventArgs e )
		{
			PersonalServer Session = (PersonalServer)sender;

			this.CloseSessionInDatabase( Session.SessionID, DateTime.UtcNow );

			this.InnerHashtable.Remove( Session.SessionInfo.ClientID );
		}

		private readonly System.Threading.Timer sessionKeeperTimer;

		private void sessionKeeperTimer_TimerCallback( object state )
		{
			List<Guid> Dead = new List<Guid>( this.InnerHashtable.Count );
			List<Guid> Expired = new List<Guid>( this.InnerHashtable.Count );

			lock( this.InnerHashtable )
			{
				#region Find dead references and Session object with expired Lease

                foreach (KeyValuePair<Guid, Common.WeakReference<PersonalServer>> entry in this.InnerHashtable)
				{
					PersonalServer Session = entry.Value.Target;

					if( Session != null )
					{
						ILease Lease = (ILease)RemotingServices.GetLifetimeService( Session );

						if( Lease != null && Lease.CurrentState == LeaseState.Expired )
						{
							Expired.Add( entry.Key );
						}
					}
					else
					{
						Dead.Add( entry.Key );
					}
				}

				#endregion // Find dead references and Session objects with expired Lease

				#region Remove dead references

				foreach( Guid key in Dead )
				{
					this.InnerHashtable.Remove( key );
				}

				#endregion // Remove dead references

				#region Call Dispose for Session objects with expired Lease

				foreach( Guid key in Expired )
				{
					PersonalServer Session = this.InnerHashtable[ key ].Target;

					if( Session != null )
					{
						Session.Dispose();
					}
				}

				#endregion // Call Dispose for Session objects with expired Lease
			}
		}

        #region IDiagnosticHelper Members

        public IObservedServer ObservedServer
        {
            get 
            {
                return Server.ObservedServer;
            }
        }

        #endregion
    }
}
