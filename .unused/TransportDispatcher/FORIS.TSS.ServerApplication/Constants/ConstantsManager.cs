﻿using System.ComponentModel;
using FORIS.TSS.BusinessLogic.Server;

namespace FORIS.TSS.ServerApplication.Constants
{
	/// <summary>
	/// 
	/// </summary>
	public class ConstantsManager : ConstantsManagerBase, IConstants
	{
		#region Controls & Components

		private System.ComponentModel.IContainer components;

		private IntegerConstant version;
		private StringConstant  applicationName;

		private StringConstant establishmentName;
		private StringConstant establishmentAddress;
		private StringConstant establishmentPhone;

		private StringConstant reportCreatorSignature;

		#endregion // Controls & Components

		#region IConstantInfo members

		public IConstant<int>    Version
		{
			get { return version; }
		}
		public IConstant<string> ApplicationName
		{
			get { return applicationName; }
		}
		public IConstant<string> EstablishmentName
		{
			get { return establishmentName; }
		}
		public IConstant<string> EstablishmentAddress
		{
			get { return establishmentAddress; }
		}
		public IConstant<string> EstablishmentPhone
		{
			get { return establishmentPhone; }
		}
		public IConstant<string> ReportCreatorSignature
		{
			get { return reportCreatorSignature; }
		}

		#endregion // IConstantInfo members

		#region Constructor & Dispose

		public ConstantsManager()
		{
			InitializeComponent();
		}
		public ConstantsManager(IContainer container)
			: this()
		{
			container.Add(this);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				/*
				this.constants.Inserted += new CollectionChangeEventHandler<IConstant>( constants_Inserted );
				this.constants.Removing += new CollectionChangeEventHandler<IConstant>(constants_Removing);
				this.constants.Clearing += new CollectionEventHandler<IConstant>( constants_Clearing);
				 */

				if (components != null)
				{
					components.Dispose();
				}
			}

			base.Dispose(disposing);
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			version = new IntegerConstant(components);
			applicationName = new StringConstant(components);
			establishmentName = new StringConstant(components);
			establishmentAddress = new StringConstant(components);
			establishmentPhone = new StringConstant(components);
			reportCreatorSignature = new StringConstant(components);
			// 
			// constantsPound
			// 
			constantsPound.Constants.Add(version);
			constantsPound.Constants.Add(applicationName);
			constantsPound.Constants.Add(establishmentName);
			constantsPound.Constants.Add(establishmentAddress);
			constantsPound.Constants.Add(establishmentPhone);
			constantsPound.Constants.Add(reportCreatorSignature);
			// 
			// version
			// 
			version.Name = "version";
			// 
			// applicationName
			// 
			applicationName.Name = "application_name";
			// 
			// establishmentName
			// 
			establishmentName.Name = "ESTABLISHMENT_NAME";
			// 
			// establishmentAddress
			// 
			establishmentAddress.Name = "ESTABLISHMENT_ADDRESS";
			// 
			// establishmentPhone
			// 
			establishmentPhone.Name = "ESTABLISHMENT_PHONE";
			// 
			// reportCreatorSignature
			// 
			reportCreatorSignature.Name = "REPORT_CREATOR_SIGNATURE";
		}

		#endregion // Component Designer generated code
	}
}