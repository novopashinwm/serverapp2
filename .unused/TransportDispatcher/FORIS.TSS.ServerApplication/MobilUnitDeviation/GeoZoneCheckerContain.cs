﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using FORIS.TSS.BusinessLogic.Map;


namespace FORIS.TSS.ServerApplication.MobilUnitDeviation
{
    static class GeoZoneCheckerContain
    {

        public static bool ContainsPoligon(DataRow[] drsZonePoints, double latitude, double longitude)
        {
            #region Preconditions

            if (drsZonePoints.Length == 0) return false;

            #endregion  // Preconditions

            DPoint checkPoint = FORIS.TSS.BusinessLogic.Map.GeoPoint.CreateGeo(latitude, longitude).Planar;
            List<DPoint> points = new List<DPoint>(drsZonePoints.Length);

            foreach (DataRow zonePoint in drsZonePoints)
            {
                points.Add(FORIS.TSS.BusinessLogic.Map.GeoPoint.CreateGeo((double)zonePoint["X"], (double)zonePoint["Y"]).Planar);
            }

            bool result = false;
            for (int i = 0, j = points.Count - 1; i < points.Count; j = i++)
            {
                if (
                    ((points[i].y <= checkPoint.y && checkPoint.y < points[j].y) 
                        || (points[j].y <= checkPoint.y && checkPoint.y < points[i].y))
                    && (checkPoint.x > (points[j].x - points[i].x) * (checkPoint.y - points[i].y) / (points[j].y - points[i].y) + points[i].x)
                    )
                {
                    result = !result;
                }
            }

            return result;
        }
    }

}
