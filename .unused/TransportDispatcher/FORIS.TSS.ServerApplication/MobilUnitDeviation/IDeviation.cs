using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.ServerApplication
{
	/// <summary> ��������� ��� ��������, ����������� ������ �������� ���������� </summary>
	public interface IDeviation
	{
		/// <summary> �������� ���������� </summary>
		/// <param name="mobilUnit">������ ���������� ��� ��������</param>
		/// <returns>���������� true, ���� ���������� ������ �����������</returns>
		bool CheckDeviation(IMobilUnit mobilUnit);
	}
}