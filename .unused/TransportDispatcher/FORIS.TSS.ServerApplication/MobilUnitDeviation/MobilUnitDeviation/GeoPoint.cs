using System;

namespace FORIS.TSS.ServerApplication.MobilUnitDeviation
{
    /// <summary>
    /// ���������� ��������� ���������
    /// </summary>
    class GeoConstant
    {
        public const double kx = 63166;
        public const double ky = 111411;
        public const double origCenterX = 38.000;
        public const double origCenterY = 55.665;
    }

    /// <summary>
    /// ���-�����
    /// </summary>
    struct GeoPoint
    {
        /// <summary>
        /// ���������� ����������
        /// </summary>
        private double X;
        /// <summary>
        /// ���������� ����������
        /// </summary>
        public double XG
        {
            get { return this.X; }
        }

        /// <summary>
        /// ���������� ����������
        /// </summary>
        private double Y;
        /// <summary>
        /// ���������� ����������
        /// </summary>
        public double YG
        {
            get { return this.Y; }
        }


        /// <summary>
        /// ��������� ����������
        /// </summary>
        private double x;
        /// <summary>
        /// ��������� ����������
        /// </summary>
        public double XL
        {
            get { return this.x; }
        }
        /// <summary>
        /// ��������� ����������
        /// </summary>
        private double y;
        /// <summary>
        /// ��������� ����������
        /// </summary>
        public double YL
        {
            get { return this.y; }
        }

		/// <summary>
        /// �������������� ����� � ��������� ������������ ���������� ������� (true) ��� ��������� (false).
        /// </summary>
        public GeoPoint(double x, double y, bool glob)
        {
            // ���������� ���������� 
            this.X = 0;
            this.Y = 0;
            // ��������� ���������� 
            this.x = 0;
            this.y = 0;

            // ���� ������� ��������� � ���������� �������
            if (glob)
            {
				// ���������� ���������� 
				this.X = x;
				this.Y = y;
				// ��������� ���������� 
				this.x = (x - GeoConstant.origCenterX) * GeoConstant.kx;
				this.y = (y - GeoConstant.origCenterY) * GeoConstant.ky;
			}
            // ���� � ��������� 
            else
            {
				// ��������� ���������� 
				this.x = x;
				this.y = y;
				// ���������� ���������� (������������� �� �����������)
				this.X = x / GeoConstant.kx + GeoConstant.origCenterX;
				this.Y = y / GeoConstant.ky + GeoConstant.origCenterY;
			}
        }

        /// <summary>
        /// ���������� �� ������ ����� �� ���������
        /// </summary>
        public double DistanceToPoint(double x, double y)
        {
            double x_ = x - this.x;
            double y_ = y - this.y;
            double distance = Math.Sqrt(x_ * x_ + y_ * y_);
            // ���������� ����� �� ������
            return Math.Abs(distance);
        }
    }
}
