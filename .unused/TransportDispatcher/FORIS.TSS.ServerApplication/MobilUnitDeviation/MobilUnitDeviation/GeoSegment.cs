using System;
using System.Diagnostics;


namespace FORIS.TSS.ServerApplication.MobilUnitDeviation
{
    
    /// <summary>
    /// ���-�������
    /// <summary>
    struct GeoSegment
    {
        /// <summary>
        /// ��������� ����� �������
        /// </summary>
        private GeoPoint p1;
        /// <summary>
        /// ��������� ����� �������
        /// </summary>
        public GeoPoint P1
        {
            get { return this.p1; }
        }

        /// <summary>
        /// �������� ����� �������
        /// </summary>
        private GeoPoint p2;
        /// <summary>
        /// �������� ����� �������
        /// </summary>
        public GeoPoint P2
        {
            get { return this.p2; }
        }

        // �����
        private double l;
        /// <summary>
        /// ����� ��� ��������� �������� 
        /// </summary>
        public double L
        {
            get { return this.l; }
        }

        // ���������� ��� ��������� �������� 
        private double a;
        /// <summary>
        /// ���������� ��� ��������� �������� 
        /// </summary>
        public double A
        {
            get { return this.a; }
        }

        private double b;
        /// <summary>
        /// ���������� ��� ��������� �������� 
        /// </summary>
        public double B
        {
            get { return this.b; }
        }

        private double c;
        /// <summary>
        /// ���������� ��� ��������� �������� 
        /// </summary>
        public double C
        {
            get { return this.c; }
        }

        /// <summary>
        /// 
        /// </summary>
        private Vector vector; 

		///// <summary>
		///// �������������� ����� �������
		///// </summary>
		//public GeoSegment()
		//{
		//    this.p1 = new GeoPoint();
		//    this.p2 = new GeoPoint();

		//    this.CalcABCL();

		//    this.vector = new Vector(this.p1, this.p2);
		//}

        /// <summary>
        /// �������������� ����� ������� �� �������� ����������� 
        /// </summary>
        public GeoSegment(GeoPoint vertex1, GeoPoint vertex2)
        {
            this.p1 = vertex1;
            this.p2 = vertex2;

			this.a = this.p1.YL - this.p2.YL;
			this.b = this.p2.XL - this.p1.XL;
			this.c = this.p1.XL * this.p2.YL - this.p2.XL * this.p1.YL;
			this.l = this.p1.DistanceToPoint(this.p2.XL, this.p2.YL);

            this.vector = new Vector(this.p1, this.p2);
        }

		///// <summary>
		///// ������ ������������� ������ ��� ��������� ��������
		///// </summary>
		//private void CalcABCL()
		//{
		//    this.a = this.p1.YL - this.p2.YL;
		//    this.b = this.p2.XL - this.p1.XL;
		//    this.c = this.p1.XL * this.p2.YL - this.p2.XL * this.p1.YL;
		//    this.l = this.p1.DistanceToPoint(this.p2.XL, this.p2.YL);
		//}

        /// <summary>
        /// ����� �������������� �� �������� ����� � ������� ��� ��������� �������� 
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public double PerpendicularLengthFromPoint(GeoPoint position)
        {
            //double distance =
            //    ((this.p1.y - this.p2.y) * position.x + (this.p2.x - this.p1.x) * position.y + (this.p1.x * this.p2.y - this.p2.x * this.p1.y))
            //    / Math.Sqrt((this.p2.x - this.p1.x) * (this.p2.x - this.p1.x) + (this.p2.y - this.p1.y) * (this.p2.y - this.p1.y));
            double distance = (this.A * position.XL + this.B * position.YL + this.C) / this.L;
            return Math.Abs(distance);
        }

        /// <summary>
        /// ���������� �� ������� �� ����� 
        /// ����� ������� �� ��������� ������������ ��������. ��������� ��� ����: +, - ��� 0. 
        /// ��������� ���������, ��� ������ ����������: ��� �� ����� �� ����� ������� P1 ��� P2, ��� ��� ���������� �� ������ L 
        /// <summary>
        public double DistanceToPoint(GeoPoint position)
        {
            // ����������
            double distance;

            // ��������� ������� �������� �� ������ � ��� �� ��������� ������� �� ����� �� ������ ������� �������� �� ������ � ��������� ��������� ������������ ���� ��������
            Vector vector_p1_p2 = this.vector; //new Vector(this.p1, this.p2);
            Vector vector_position_p1 = new Vector(position, p1);

            double scalarVxVp = vector_p1_p2.VxV(vector_position_p1);
            // ���� ����������� ��� ������, ���������� ���������� �� ����� �� ������ ����� ������� ��������
            if (scalarVxVp >= 0)
                distance = position.DistanceToPoint(this.p1.XL, this.p1.YL);
            // ���� ���� ����� ��������� ������ ���� �� ����� ������� �������� 
            else //if(scalarVxVp < 0)
            {
                Vector vector_p2_position = new Vector(p2, position);
                scalarVxVp = vector_p1_p2.VxV(vector_p2_position);
                // �� ��������, ���� ���� ����������� ��� ������, ���������� ���������� �� ����� �� ������ ����� ������� ��������
                if (scalarVxVp >= 0)
                    distance = position.DistanceToPoint(this.p2.XL, this.p2.YL);
                // ���� �����, �� ���������� ����� �������������� 
                else //if (scalarVxVp < 0)
                    distance = this.PerpendicularLengthFromPoint(position);
            }
//#if DEBUG
//            Trace.WriteLine(
//                    "�������: " + position.XG.ToString() + " , " + position.YG.ToString() + " (" + position.XL.ToString() + " , " + position.YL.ToString() + ")"
//                    + "scalarVxVp = " + scalarVxVp.ToString()
//                    + " ���������� = " + distance.ToString()
//                    + " �������: p1 = " + this.p1.XG.ToString() + " , " + this.p1.YG.ToString() + " (" + this.p1.XL.ToString() + " , " + this.p1.YL.ToString() + ")"
//                    + " p2 = " + this.p2.XG.ToString() + " , " + this.p2.YG.ToString() + " (" + this.p2.XL.ToString() + " , " + this.p2.YL.ToString() + ")"
//                    );
//#endif

            return distance;
        }
    }


    /// <summary>
    /// ������
    /// <summary>
    class Vector
    {
        /// <summary>
        /// ���������� �������
        /// </summary>
        private double x;
        /// <summary>
        /// ���������� �������
        /// </summary>
        public double X
        {
            get { return this.x; }
        }

        /// <summary>
        /// ���������� �������
        /// </summary>
        private double y;
        /// <summary>
        /// ���������� �������
        /// </summary>
        public double Y
        {
            get { return this.y; }
        }

        /// <summary>
        /// �������������� ������ � ��������� ������������.
        /// </summary>
        public Vector(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        /// <summary>
        /// �������������� ������ � ��������� ������������ �� ���� ������.
        /// </summary>
        public Vector(GeoPoint p1, GeoPoint p2)
        {
            this.x = p2.XL - p1.XL;
            this.y = p2.YL - p1.YL;
        }

        /// <summary>
        /// ��������� ������������
        /// </summary>
        public double VxV(Vector v)
        {
            return (this.x * v.x + this.y * v.y);
        }

        /// <summary>
        /// ��������� ������� �� �����
        /// </summary>
        public Vector VxN(double n)
        {
            return new Vector(this.x * n, this.y * n);
        }
    }

    
	///// <summary>
	///// 
	///// </summary>
	//class RouteSegment : GeoSegment
	//{

	//}
}
