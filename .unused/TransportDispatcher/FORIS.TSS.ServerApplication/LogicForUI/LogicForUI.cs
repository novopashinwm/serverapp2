﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using FORIS.DataAccess;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Interfaces.Data;
using FORIS.TSS.Protection;
using FORIS.TSS.ServerApplication;
using Globals = FORIS.DataAccess.Globals;
using IPersonalServer = FORIS.TSS.BusinessLogic.IPersonalServer;

namespace FORIS.TSS.Server.LogicForUI
{
	/// <summary>
	/// Реализует функции для считывания/записи данных для работы UI
	/// </summary>
	public abstract class LogicForUI : BasicFunctionSetImpl, ILogicForUI
	{
		DataSet ILogicForUI.GetVehiclesForMonitoring()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetVehiclesForMonitoring"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetVehiclesForMonitoring);
				ds = sc.Check(PersonalServer, ds);
				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		DataSet ILogicForUI.GetVehiclesForMonitoring(bool checkAdminRight)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetVehiclesForMonitoring"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetVehiclesForMonitoring);
				ds = sc.Check(PersonalServer, ds, checkAdminRight);
				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		DataSet ILogicForUI.GetSchedule(int dkid, int from, int to, int route, int rgid,
			DateTime whdate)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetSchedule"))
			{
				sp["@dkid"].Value = dkid;
				sp["@from"].Value = from;
				sp["@to"].Value = to;
				if (route > 0) sp["@route"].Value = route;
				if (rgid > 0) sp["@rgid"].Value = rgid;
				sp["@whdate"].Value = whdate;

				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetSchedule);
				DatasetHelperBase.Dump(dataSet);
				DatabaseSchema.MakeSchema(dataSet);
				return dataSet;
			}
		}

		DataSet ILogicForUI.GetAllStation()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetAllStation"))
			{
				sp["@operatorID"].Value = this.PersonalServer.OperatorID;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetAllStation);
				return dataSet;
			}
		}

		DataSet ILogicForUI.GetDataForGeoTrip()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForGeoTrip"))
			{
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDataForGeoTrip);
				return dataSet;
			}
		}

		DataSet ILogicForUI.GetTripInRoute(int Route_ID)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetTripInRoute"))
			{
				sp["@Route_ID"].Value = Route_ID;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetTripInRoute);
				return dataSet;
			}
		}

		/// <summary>
		/// Возвращает имя приложения
		/// </summary>
		/// <returns>Имя приложения (заголовок главного окна, зависит от места установки приложения)</returns>
		string ILogicForUI.GetApplicationName()
		{
			string applicationName = String.Empty;

			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetApplicationName"))
			{
				applicationName = (string)sp.ExecuteScalar();
			}

			return applicationName;
		}

		/// <summary>
		/// Возвращает изображения ТС из БД 
		/// </summary>
		/// <param name="vehicle_id">идентификатор ТС</param>
		/// <returns>изображения ТС, хранящееся в БД (массив байт)</returns>
		byte[] ILogicForUI.GetVehiclePicture(int vehicle_id)
		{
			byte[] bytes = null;
			try
			{
				using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetVehiclePicture"))
				{
					sp["@vehicle_id"].Value = vehicle_id;
					using (new ConnectionOpener(sp.Command.Connection))
					{
						IDataReader dr = sp.ExecuteReader();
						try
						{
							bool nextRecord = dr.Read();

							if (nextRecord)
							{
								// Получаем размер данных поля типа image;
								// в качестве параметра - массива байтов передаем null
								long bytesize = dr.GetBytes(0, 0, null, 0, 0);

								// Выделяем память под массив байтов, предназначенный для хранения
								// данных поля
								bytes = new byte[bytesize];
								long bytesread = 0;
								int curpos = 0;
								int chunkSize = 256;
								while (bytesread < bytesize)
								{
									// chunkSize - произвольное значение, определяемое приложением
									bytesread += dr.GetBytes(0, curpos, bytes, curpos, chunkSize);
									curpos += chunkSize;
								}
							}
						}
						finally
						{
							dr.Close();
						}
					}
				}
			}
			catch (Exception exception)
			{
				Trace.TraceError("{0}", exception);
				Trace.WriteLine(exception.StackTrace);
				return bytes;
			}
			return bytes;
		}

		/// <summary>
		/// Запись информации о действии оператора
		/// </summary>
		/// <param name="vehicle_id">идентификатор ТС, с которым работает оператор</param>
		/// <param name="operator_id">идентификатор оператора</param>
		/// <param name="event_id">идентификатор события</param>
		/// <param name="time">время возникновения события</param>
		void ILogicForUI.LogOperatorEvent(int operator_id, int event_id, int vehicle_id, DateTime time)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.LogOperatorEvent"))
			{
				sp["@operator_id"].Value = operator_id;
				sp["@event_id"].Value = event_id;
				sp["@vehicle_id"].Value = vehicle_id;
				sp["@event_time"].Value = time;
				sp.ExecuteScalar();
			}
		}

		/// <summary>
		/// Возвращает набор данных, в котором содержится вся информация о владельцах и совладельцах
		/// </summary>
		/// <param name="vehicle_id">идентификатор ТС в БД [VEHICLE.VEHICLE_ID]</param>
		/// <returns>набор данных, в котором содержится вся информация о владельцах и совладельцах</returns>
		DataSet ILogicForUI.GetOwners(int vehicle_id)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetOwners"))
			{
				sp["@vehicle_id"].Value = vehicle_id;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetOwners);
				DatabaseSchema.MakeSchema(dataSet);
				dataSet = sc.Check(PersonalServer, dataSet);
				return dataSet;
			}
		}

		void ILogicForUI.MarkMessage(int iMessageId, int iOperatorId)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.MarkMessage"))
			{
				sp["@msg_id"].Value = iMessageId;
				sp["@op_id"].Value = iOperatorId;
				sp.ExecuteScalar();
			}
		}

		void ILogicForUI.DeleteMessage(int iMessageId, int iOperatorId)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.DeleteMessage"))
			{
				sp["@msg_id"].Value = iMessageId;
				sp["@op_id"].Value = iOperatorId;
				sp.ExecuteScalar();
			}
		}

		/// <summary>
		/// Получение данных истории движения по расписанию дял указанного ПЛ в ЛВ
		/// </summary>
		/// <param name="waybill_header_id">ID заголовка ПЛ</param>
		/// <param name="schedule_id">ID расписания</param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet ILogicForUI.GetTimeDeviations_LR_WH_S(int waybill_header_id, int schedule_id)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GET_TIME_DEVIATIONS_LR_WH_S"))
			{
				sp["@WH_ID"].Value = waybill_header_id;
				sp["@S_ID"].Value = schedule_id;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GET_TIME_DEVIATIONS);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		/// <summary>
		/// Получение данных истории движения для указанной смены по всем выходам и ПЛ в ЛВ
		/// </summary>
		/// <param name="waybill_id"></param>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet ILogicForUI.GetTimeDeviations_LR_W(int waybill_id, DateTime waybill_date_utc)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GET_TIME_DEVIATIONS_LR_W"))
			{
				sp["@w_id"].Value = waybill_id;
				sp["@waybilldate"].Value = waybill_date_utc;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GET_TIME_DEVIATIONS);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		/// <summary>
		/// Получение списка сообщений послнанных на/с ТС 
		/// </summary>
		/// <param name="OperatorID">ID оператора</param>
		/// <param name="VehicleID">ID ТС</param>
		/// <param name="dtFrom">Время "С"</param>
		/// <param name="dtTo">Время "По"</param>
		/// <returns>Датасет с таблицей "MESSAGE"</returns>
		DataSet ILogicForUI.GetMessageList(int OperatorID, int VehicleID, DateTime dtFrom, DateTime dtTo)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GETMESSAGELIST"))
			{
				sp["@Op_id"].Value = OperatorID;
				sp["@Vehicle_id"].Value = VehicleID;
				sp["@TimeFrom"].Value = dtFrom;
				sp["@TimeTo"].Value = dtTo;

				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetMessageList);
				DatasetHelperBase.Dump(dataSet);

				return dataSet;
			}
		}

		/// <summary>
		/// Получение шаблонов сообщений водителям
		/// </summary>
		/// <returns></returns>
		DataSet ILogicForUI.GetDriverMsgTemplate()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GET_DRIVER_MSG_TEMPLATE"))
			{

				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDriverMsgTemplate);
				DatasetHelperBase.Dump(dataSet);

				return dataSet;
			}
		}

		DataSet ILogicForUI.GetTaskLog(DateTime from, DateTime to)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetTaskLog"))
			{
				sp["@from"].Value = from;
				sp["@to"].Value = to;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetTaskLog);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		/// <summary>
		/// register operator calls
		/// </summary>
		/// <param name="op">OPERATOR_ID</param>
		/// <param name="phone">phone number</param>
		/// <param name="from">tm of begin</param>
		/// <param name="to">tm of end</param>
		/// <param name="voice">voice or data</param>
		/// <param name="incall">out or in</param>
		void ILogicForUI.RegisterCall(int op, string phone, DateTime from, DateTime to, bool voice, bool incall)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.RegisterCall"))
			{
				try
				{
					sp["@op"].Value = op;
					sp["@phone"].Value = phone;
					sp["@from"].Value = from;
					sp["@to"].Value = to;
					sp["@type"].Value = voice ? 1 : 0;
					sp["@dir"].Value = incall ? 1 : 0;
					sp.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					Trace.TraceError("{0}", ex);

				}
			}
		}

		private new DataSet GetVehicles()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetVehicles"))
			{
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetVehicles);
				DatabaseSchema.MakeSchema(dataSet);
				this.sc.CheckAll(PersonalServer, dataSet);
				return dataSet;
			}
		}

		DataSet ILogicForUI.GetVehiclesByRights()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetVehicles"))
			{
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetVehicles);
				DatabaseSchema.MakeSchema(dataSet);
				this.sc.CheckAll(PersonalServer, dataSet);
				return dataSet;
			}
		}

		public new int CheckPasswordForWeb(string login, string password)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.CheckPasswordForWeb"))
			{
				sp["@login"].Value = login;
				sp["@password"].Value = password;
				int num = (int)sp.ExecuteScalar();
				return num;
			}
		}

		#region Перенос методов из LogicForUIBase

		protected IPersonalServer PersonalServer //= null;
		{
			get
			{
				return this as IPersonalServer;
			}
		}

		protected SecurityChecker sc = null;

		public void Init() //(IPersonalServer PersonalServer)
		{
			//this.PersonalServer = PersonalServer;
			sc = new SecurityChecker(this as IPersonalServer); //this.PersonalServer);
		}

		/// <summary>
		/// Returns tables for calculating access from Operators to Vehicles and Routes
		/// </summary>
		/// <returns></returns>
		internal DataSet GetSecurity(IDbTransaction transaction)
		{
			IDbConnection connection;
			if (transaction == null)
			{
				connection = null;
			}
			else
			{
				connection = transaction.Connection;
			}

			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GET_SECURITY", connection, transaction))
			{
				sp["@oid"].Value = this.PersonalServer.OperatorID;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GET_SECURITY);
				DatasetHelperBase.Dump(dataSet);
				DatabaseSchema.MakeSchema(dataSet);
				return dataSet;
			}
		}

		public DataSet GetSecurity()
		{
			return GetSecurity(null);
		}

		/// <summary>
		/// выполняет процедуру dbo.GetRules
		/// </summary>
		/// <returns>набор данных с результатом выполнения процедуры</returns>
		DataSet ILogicForUI.GetRules()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetRules"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetRules);
				DatabaseSchema.MakeSchema(ds);
				return ds;
			}
		}

		/// <summary>
		/// выполняет процедуру dbo.GetWaybillsPartPermanent
		/// </summary>
		/// <returns>набор данных с результатом выполнения процедуры</returns>
		DataSet ILogicForUI.GetWaybillsPartPermanent()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetWaybillsPartPermanent"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetWaybillsPartPermanent);
				DatabaseSchema.MakeSchema(ds);
				return ds;
			}
		}

		/// <summary>
		/// выполняет процедуру dbo.GetWaybillsPartWaybillList
		/// </summary>
		/// <param name="dt">аргумент процедуры</param>
		/// <returns>набор данных с результатом выполнения процедуры</returns>
		DataSet ILogicForUI.GetWaybillsPartWaybillList(DateTime dt, int iDeltaSeconds)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetWaybillsPartWaybillList"))
			{
				sp["@whd"].Value = dt;
				sp["@delta_seconds"].Value = iDeltaSeconds;
				sp["@operator_id"].Value = PersonalServer.OperatorID;
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetWaybillsPartWaybillList);
				this.sc.CheckAll(PersonalServer, ds);
				ds.Tables.Add("RightStatusAndPenalty");
				ds.Tables["RightStatusAndPenalty"].Columns.Add("Enabled", typeof(bool));
				DataRow dr = ds.Tables["RightStatusAndPenalty"].NewRow();
				dr["Enabled"] = (SystemRightsHelper.GetEffectiveOperatorRight(this.sc.Rights, this.PersonalServer.OperatorID, (int)SystemRight.EditStatusAndPenalty) || SystemRightsHelper.GetEffectiveOperatorRight(this.sc.Rights, this.PersonalServer.OperatorID, (int)SystemRight.ServerAdministration));
				ds.Tables["RightStatusAndPenalty"].Rows.Add(dr);
				return ds;
			}
		}

		/// <summary>
		/// выполняет процедуру dbo.GetWaybillsPartClosed
		/// </summary>
		/// <param name="dt">аргумент процедуры</param>
		/// <returns>набор данных с результатом выполнения процедуры</returns>
		DataSet ILogicForUI.GetWaybillsPartClosed(DateTime dt, int iDeltaSeconds)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetWaybillsPartClosed"))
			{
				sp["@whd"].Value = dt;
				sp["@delta_seconds"].Value = iDeltaSeconds;
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetWaybillsPartClosed);
				DatabaseSchema.MakeSchema(ds);
				this.sc.CheckAll(PersonalServer, ds);
				return ds;
			}
		}


		/// <summary>
		/// выполняет процедуру dbo.GetWaybillsPartUnclosed
		/// </summary>
		/// <param name="dt">аргумент процедуры</param>
		/// <returns>набор данных с результатом выполнения процедуры</returns>
		DataSet ILogicForUI.GetWaybillsPartUnclosed(DateTime dt)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetWaybillsPartUnclosed"))
			{
				sp["@whd"].Value = dt;
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetWaybillsPartUnclosed);
				DatabaseSchema.MakeSchema(ds);
				this.sc.CheckAll(PersonalServer, ds);
				return ds;
			}
		}

		/// <summary>
		/// Возвращает список идентификаторов системных прав пользователя
		/// </summary>
		/// <returns>список идентификаторов системных прав пользователя</returns>
		SystemRights ILogicForUI.GetSystemRights()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GET_SYSTEM_RIGHTS"))
			{
				sp["@login"].Value = this.PersonalServer.Login;
				DataSet dataSet = sp.ExecuteDataSet();
				if (dataSet.Tables.Count == Tables.GET_SYSTEM_RIGHTS.Length)
				{
					DatasetHelperBase.RenameTables(dataSet, Tables.GET_SYSTEM_RIGHTS);
				}
				else
				{
					DatasetHelperBase.RenameTables(dataSet, new string[] { "OPERATOR", "OPERATORGROUP_OPERATOR", "RIGHT_OPERATOR", "OPERATORGROUP", "RIGHT_OPERATORGROUP" });
				}
				DatasetHelperBase.Dump(dataSet);
				DatabaseSchema.MakeSchema(dataSet);

				DataTable tableOperator = dataSet.Tables["OPERATOR"];
				DataRow row = tableOperator.Rows[0];
				if (row == null)
				{
					throw new ApplicationException("There is no operator with login '" + this.PersonalServer.Login + "'");
				}

				int operator_id = (int)row["OPERATOR_ID"];

				DataTable tableRight = dataSet.Tables["RIGHT"];
				if (tableRight == null)
				{
					throw new ApplicationException("Old database");
				}

				SystemRights res = new SystemRights();
				foreach (DataRow rowRight in tableRight.Rows)
				{
					int right_id = (int)rowRight["RIGHT_ID"];
					if (SystemRightsHelper.GetEffectiveOperatorRight(dataSet, operator_id, right_id))
					{
						res.Add((SystemRight)right_id);
					}
				}
				return res;
			}
		}

		/// <summary>
		/// Возвращает данные дял списка и свойств контроллеров
		/// </summary>
		/// <returns></returns>
		DataSet ILogicForUI.GetDataForController()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForController"))
			{
				sp["@controllerId"].Value = 0;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDataForController);
				DatabaseSchema.MakeSchema(dataSet);
				return dataSet;
			}
		}

		DataSet ILogicForUI.GetDataForController(int controllerId)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForController"))
			{
				sp["@controllerId"].Value = controllerId;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDataForControllerWithId);
				DatabaseSchema.MakeSchema(dataSet);
				return dataSet;
			}
		}

		DataSet ILogicForUI.GetWaybillHeaderStatusHistory(int waybill_header_id)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GET_WAYBILL_HEADER_STATUS_HISTORY"))
			{
				sp["@waybill_header_id"].Value = waybill_header_id;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GET_WAYBILL_HEADER_STATUS_HISTORY);
				DatabaseSchema.MakeSchema(dataSet);
				return dataSet;
			}
		}

		DataSet ILogicForUI.GetHistoryForWaybill(int waybill_header_id)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetHistoryForWaybill"))
			{
				sp["@wh_id"].Value = waybill_header_id;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetHistoryForWaybill);
				DatasetHelperBase.Dump(dataSet);
				DatabaseSchema.MakeSchema(dataSet);
				dataSet = sc.Check(PersonalServer, dataSet);
				return dataSet;
			}
		}

		DataSet ILogicForUI.GetOperator(int operator_id)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetOperator"))
			{
				sp["@OPERATOR_ID"].Value = operator_id;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetOperator);
				DatasetHelperBase.Dump(dataSet);
				DatabaseSchema.MakeSchema(dataSet);
				dataSet = sc.Check(PersonalServer, dataSet);
				return dataSet;
			}
		}

		DataSet ILogicForUI.GetDataForSchedulePropertiesDialog(int schedule_id)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForSchedulePropertiesDialog"))
			{
				sp["@schedule_id"].Value = schedule_id;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDataForSchedulePropertiesDialog);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		void ILogicForUI.DeleteSchedule(int schedule_id)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.DeleteSchedule"))
			{
				sp["@schedule_id"].Value = schedule_id;
				sp.ExecuteNonQuery();
			}
		}

		/// <summary>
		/// Возвращает набор данных для формы "свойства точки"
		/// </summary>
		/// <param name="point_id">индентификатор точки</param>
		/// <returns>набор данных для формы "свойства точки"</returns>
		DataSet ILogicForUI.GetDataForPointPropertiesDialog(int point_id)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForPointPropertiesDialog"))
			{
				sp["@point_id"].Value = point_id;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDataForPointPropertiesDialog);
				dataSet = sc.Check(PersonalServer, dataSet);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		/// <summary>
		/// Возвращает набор данных с информацией о маршрутах и группах маршрутов
		/// </summary>
		/// <returns>набор данных с информацией о маршрутах и группах маршрутов</returns>
		DataSet ILogicForUI.GetRouteGroupsAndRoutes()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetRouteGroupsAndRoutes"))
			{
				sp["@operator_id"].Value = PersonalServer.OperatorID;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetRouteGroupsAndRoutes);
				//				dataSet = sc.Check(PersonalServer, dataSet);

				//checking rights
				DatabaseSchema.MakeSchema(dataSet);
				this.sc.CheckAll(PersonalServer, dataSet);

				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		DataSet ILogicForUI.GetOperatorGroupsAndOperators()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetOperatorGroupsAndOperators"))
			{
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetOperatorGroupsAndOperators);
				dataSet = sc.Check(PersonalServer, dataSet);
				DatasetHelperBase.Dump(dataSet);
				#region HASP
				if (dataSet.Tables["OPERATOR"].Rows.Count > ProtectionKey.ValidUsersCount)
					throw new Exception("Protection: Max count of operators exceed");
				#endregion
				return dataSet;
			}
		}

		public DataSet GetVehicleGroupsAndVehicles()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetVehicleGroupsAndVehicles"))
			{
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetVehicleGroupsAndVehicles);

				//checking rights
				DatabaseSchema.MakeSchema(dataSet);
				this.sc.CheckAll(PersonalServer, dataSet);

				DatasetHelperBase.Dump(dataSet);
				#region HASP
				if (dataSet.Tables["VEHICLE"].Rows.Count > ProtectionKey.ValidVehiclesCount)
					throw new Exception("Protection: Max count of vehicles exceed");
				#endregion
				return dataSet;
			}
		}

		DataSet ILogicForUI.GetDriverGroupsAndDrivers()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDriverGroupsAndDrivers"))
			{
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDriverGroupsAndDrivers);
				//				dataSet = sc.Check(PersonalServer, dataSet);

				//checking rights
				DatabaseSchema.MakeSchema(dataSet);
				this.sc.CheckAll(PersonalServer, dataSet);

				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		/// <summary>
		/// Возвращает набор данных для создания нового ТС
		/// </summary>
		/// <returns>набор данных для создания нового ТС</returns>
		DataSet ILogicForUI.GetDataForVehiclePropertiesNew()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForVehiclePropertiesNew"))
			{
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDataForVehiclePropertiesNew);
				dataSet = sc.Check(PersonalServer, dataSet);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		/// <summary>
		/// Возвращает набор данных для отображения маршрутов на карте ГИС
		/// </summary>
		/// <returns>набор данных для отображения маршрутов на карте ГИС</returns>
		DataSet ILogicForUI.GetDataForRoutesAndPoints()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForRoutesAndPoints"))
			{
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDataForRoutesAndPoints);
				dataSet = sc.Check(PersonalServer, dataSet);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		/// <summary>
		/// Возвращает набор данных для создания нового маршрута
		/// </summary>
		/// <returns>набор данных для создания нового маршрута</returns>
		DataSet ILogicForUI.GetDataForRoutePropertiesCreate()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForRoutePropertiesCreate"))
			{
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDataForRoutePropertiesCreate);
				dataSet = sc.Check(PersonalServer, dataSet);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		DataSet ILogicForUI.GetDataForRoutes()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForRoutes"))
			{
				sp["@operatorID"].Value = this.PersonalServer.OperatorID;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDataForRoutes);
				dataSet = sc.Check(PersonalServer, dataSet);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		DataSet ILogicForUI.GetDataForStatusBar()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForStatusBar"))
			{
				sp["@login"].Value = this.PersonalServer.Login;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDataForStatusBar);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		DataSet ILogicForUI.GetOperatorProfile(int operator_id)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetOperatorProfile"))
			{
				sp["@operator_id"].Value = operator_id;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetOperatorProfile);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		/// <summary>
		/// Возвращает набор данных для редактирования существующего ТС
		/// </summary>
		/// <param name="vehicle_id">идентификатор существующего ТС</param>
		/// <returns>набор данных для редактирования существующего ТС</returns>
		DataSet ILogicForUI.GetDataForVehiclePropertiesEdit(int vehicle_id)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForVehiclePropertiesEdit"))
			{
				sp["@vehicle_id"].Value = vehicle_id;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDataForVehiclePropertiesEdit);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		DataSet ILogicForUI.GetDataForOperatorPropertiesForm()
		{
			//DataSet dataSet = GetSecurity();
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForOperatorPropertiesForm"))
			{
				sp["@operatorID"].Value = this.PersonalServer.OperatorID;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDataForOperatorPropertiesForm);
				DatasetHelperBase.Dump(dataSet);
				DatabaseSchema.MakeSchema(dataSet);
				return dataSet;
			}
		}

		/// <summary>
		/// Возвращает набор данных для редактирования существующего маршрута
		/// </summary>
		/// <param name="routeID">идентификатор существующего маршрута</param>
		/// <returns>набор данных для редактирования существующего маршрута</returns>
		DataSet ILogicForUI.GetDataForRoutePropertiesEdit(int routeID)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForRoutePropertiesEdit"))
			{
				sp["@route_id"].Value = routeID;
				sp["@operatorID"].Value = this.PersonalServer.OperatorID;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDataForRoutePropertiesEdit);
				dataSet = sc.Check(PersonalServer, dataSet);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		/// <summary>
		/// Возвращает набор данных по топливу
		/// </summary>
		/// <param name="begin">Время начала</param>
		/// <param name="end">Время конца</param>
		/// <returns>DataSet с данными</returns>
		DataSet ILogicForUI.GetDataForControllerFuel(int controllerId, DateTime begin, DateTime end, int maxValue)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForControllerFuel"))
			{
				sp["@id"].Value = controllerId;
				sp["@dBegin"].Value = begin;
				sp["@dEnd"].Value = end;
				sp["@maxValue"].Value = maxValue;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDataForControllerFuel);
				//				dataSet = sc.Check(PersonalServer, dataSet);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		/// <summary>
		/// Возвращает набор данных для редактирования сезонных коэф.
		/// </summary>
		/// <returns></returns>
		DataSet ILogicForUI.GetDataForFactors()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForFactors"))
			{
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDataForFactors);
				//				dataSet = sc.Check(PersonalServer, dataSet);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		DataSet ILogicForUI.GetDayKind(DateTime dt)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDayKind"))
			{
				if (dt != DateTime.MinValue) sp["@date"].Value = dt;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetDayKind);
				dataSet = sc.Check(PersonalServer, dataSet);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		void ILogicForUI.SetDayKind(DateTime dt, int dkid)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.SetDayKind"))
			{
				sp["@date"].Value = dt;
				sp["@dkid"].Value = dkid;
				sp.ExecuteNonQuery();
			}
		}

		DataSet ILogicForUI.GetSchedulePassage(DateTime from, DateTime to, int route, int sch_id)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetSchedulePassage"))
			{
				sp["@from"].Value = from;
				if (to != TimeHelper.NULL) sp["@to"].Value = to;
				if (route > 0) sp["@route"].Value = route;
				if (sch_id > 0) sp["@sch_id"].Value = sch_id;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetSchedulePassage);
				dataSet = sc.Check(PersonalServer, dataSet);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		DataSet ILogicForUI.GetFreeSchedules(int dkid, DateTime whdate)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetFreeSchedules"))
			{
				sp["@dkid"].Value = dkid;
				sp["@whdate"].Value = whdate;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetFreeSchedules);
				dataSet = sc.Check(PersonalServer, dataSet);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		DataSet ILogicForUI.GetUnclosed(int iDayKindId, DateTime dtWaybillHeader)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetUnclosed"))
			{
				sp["@dkid"].Value = iDayKindId;
				sp["@whdate"].Value = dtWaybillHeader;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetUnclosed);
				DatabaseSchema.MakeSchema(dataSet);
				this.sc.CheckAll(PersonalServer, dataSet);

				/*
				foreach (DataRow drSchedule in dataSet.Tables["SCHEDULE"].Rows)
				{
					DataRow[] drWaybillHeaderArray = drSchedule.GetChildRows("WaybillHeader_Schedule");
					string s = "";

					foreach (DataRow drWaybillHeader in drWaybillHeaderArray)
						s += ((int)drWaybillHeader["WAYBILL_HEADER_ID"]).ToString()+", ";

					s = s.TrimEnd(new char[] {' ', ','});
					DataRow[] drScheduleDetailArray = drSchedule.GetChildRows("ScheduleDetail_Schedule");
					bool bClosed = true;

					foreach (DataRow drScheduleDetail in drScheduleDetailArray)
					{
						dataSet.Tables["WAYBILL"].Select("WAYBILL_HEADER_ID in ("+s+") and SHIFT_ID = "+((int)drScheduleDetail["SHIFT_ID"]).ToString());
					}
				}
				*/

				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		int ILogicForUI.GetMaxWayBillExtNumber()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetMaxWayBillExtNumber"))
			{
				return (int)sp.ExecuteScalar();
			}
		}


		#region справочник ТС

		#region GetListsForVehicles()

		DataSet ILogicForUI.GetListsForVehicles()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetListsForVehicles"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetListsForVehicles);

				DatabaseSchema.MakeSchema(ds);

				this.sc.CheckAll(PersonalServer, ds);

				DatasetHelperBase.Dump(ds);

				return ds;
			}
		}

		DataSet ILogicForUI.GetListsForVehicles(bool checkAdminRight)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetListsForVehicles"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetListsForVehicles);

				DatabaseSchema.MakeSchema(ds);

				this.sc.CheckAll(PersonalServer, ds, checkAdminRight);

				DatasetHelperBase.Dump(ds);

				return ds;
			}
		}

		#endregion // GetListsForVehicles()

		DataSet ILogicForUI.GetDataForVehicles()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForVehicles"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetDataForVehicles);

				DatabaseSchema.MakeSchema(ds);

				this.sc.CheckAll(PersonalServer, ds);

				DatasetHelperBase.Dump(ds);

				return ds;
			}
		}

		#endregion // справочник ТС

		#region справочник Водителей

		DataSet ILogicForUI.GetListsForDrivers()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetListsForDrivers"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetListsForDrivers);

				DatabaseSchema.MakeSchema(ds);

				this.sc.CheckAll(PersonalServer, ds);

				DatasetHelperBase.Dump(ds);

				return ds;
			}
		}


		DataSet ILogicForUI.GetDataForDrivers()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForDrivers"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetDataForDrivers);

				DatabaseSchema.MakeSchema(ds);

				this.sc.CheckAll(PersonalServer, ds);

				DatasetHelperBase.Dump(ds);

				return ds;
			}
		}

		#endregion // справочник Водителей

		#region справочник Контроллеров

		DataSet ILogicForUI.GetListsForControllers()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetListsForControllers"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetListsForControllers);

				DatabaseSchema.MakeSchema(ds);

				this.sc.CheckAll(PersonalServer, ds);

				DatasetHelperBase.Dump(ds);

				return ds;
			}
		}


		DataSet ILogicForUI.GetDataForControllers()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForControllers"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetDataForControllers);

				DatabaseSchema.MakeSchema(ds);

				this.sc.CheckAll(PersonalServer, ds);

				DatasetHelperBase.Dump(ds);

				return ds;
			}
		}

		#endregion // справочник Контроллеров

		#region Журнал

		DataSet ILogicForUI.GetListsForOldJournal()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetListsForOldJournal"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetListsForOldJournal);

				DatabaseSchema.MakeSchema(ds);
				this.sc.CheckAll(PersonalServer, ds);

				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		DataSet ILogicForUI.GetDataForOldJournal()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForOldJournal"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetDataForOldJournal);

				DatabaseSchema.MakeSchema(ds);
				this.sc.CheckAll(PersonalServer, ds);

				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		#endregion Журнал

		#region Новый Ж

		DataSet ILogicForUI.GetDepartment()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDepartment"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetDepartment);

				DatabaseSchema.MakeSchema(ds);

				this.sc.CheckAll(PersonalServer, ds);
				this.sc.CheckForDepartments(PersonalServer, ds);
				//this.sc.CheckAll(PersonalServer,  ds );

				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		DataSet ILogicForUI.GetDataForSchedulesInfo()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("GetDataForSchedulesInfo"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetDataForSchedulesInfo);

				DatabaseSchema.MakeSchema(ds);
				this.sc.CheckAll(PersonalServer, ds);

				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		DataSet ILogicForUI.GetDataForJournals()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("GetDataForJournals"))
			{
				sp["@operator_id"].Value = PersonalServer.OperatorID;
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetDataForJournals);

				DatabaseSchema.MakeSchema(ds);
				this.sc.CheckAll(PersonalServer, ds);

				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		public DataSet GetDataForDepartments()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("GetDataForDepartments"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetDataForDepartments);

				DatabaseSchema.MakeSchema(ds);
				this.sc.CheckAll(PersonalServer, ds);

				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		DataSet ILogicForUI.GetDataForDepartment(int IdDepartment)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("GetDataForDepartment"))
			{
				sp["@DEPARTMENT"].Value = IdDepartment;
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetDataForDepartment);

				DatabaseSchema.MakeSchema(ds);

				this.sc.CheckAll(PersonalServer, ds);
				this.sc.CheckForDepartments(PersonalServer, ds);
				//this.sc.CheckAll(PersonalServer,  ds );

				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		public DataSet GetWorkTimeForMonth(int idDepartment, int year, int month)
		{
			using (
				StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("GetWorkTimeForMonth")
				)
			{
				sp["@DEPARTMENT"].Value = idDepartment;
				sp["@YEAR"].Value = year;
				sp["@MONTH"].Value = month;
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetWorkTimeForMonth);

				DatabaseSchema.MakeSchema(ds);

				this.sc.CheckAll(PersonalServer, ds);
				this.sc.CheckForDepartments(PersonalServer, ds);
				//this.sc.CheckAll(PersonalServer,  ds );

				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		public DataSet GetDataForDecade(int idDecade, int idDecadePrev)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForDecade"))
			{
				sp["@DECADE"].Value = idDecade;
				sp["@DECADE_PREV"].Value = idDecadePrev;

				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetDataForDecade);

				DatabaseSchema.MakeSchema(ds);
				this.sc.CheckAll(PersonalServer, ds);

				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		public int CreateDecade(int idDepartment, DateTime DateBegin, DateTime DateEnd, int idDecadeSource)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.CreateDecade"))
			{
				sp["@DEPARTMENT"].Value = idDepartment;
				sp["@DATE_BEGIN"].Value = DateBegin;
				sp["@DATE_END"].Value = DateEnd;
				sp["@DECADE_SOURCE"].Value = idDecadeSource;

				return (int)sp.ExecuteScalar();
			}
		}

		#endregion // Новый Ж

		#region Заказы

		DataSet ILogicForUI.GetListsForOrders()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetListsForOrders"))
			{
				sp["@operator_id"].Value = this.PersonalServer.OperatorID;
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetListsForOrders);

				DatabaseSchema.MakeSchema(ds);
				this.sc.CheckAll(PersonalServer, ds);

				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		DataSet ILogicForUI.GetDataForOrders()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForOrders"))
			{
				sp["@operator_id"].Value = this.PersonalServer.OperatorID;
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetDataForOrders);

				DatabaseSchema.MakeSchema(ds);
				this.sc.CheckAll(PersonalServer, ds);

				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		#endregion // Заказы

		#region Линейная ведомость

		/// <summary>
		/// Набор данных для ЛВ
		/// </summary>
		/// <returns></returns>
		DataSet ILogicForUI.GetListsForLineRoll()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetListsForLineRoll"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetListsForLineRoll);

				DatabaseSchema.MakeSchema(ds);
				this.sc.CheckAll(PersonalServer, ds);

				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		DataSet ILogicForUI.GetDataForLineRoll(DateTime Date)
		{
			try
			{
				using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForLineRoll"))
				{
					sp["@DATE"].Value = Date.Date;
					DataSet ds = sp.ExecuteDataSet();
					DatasetHelperBase.RenameTables(ds, Tables.GetDataForLineRoll);

					DatabaseSchema.MakeSchema(ds);
					this.sc.CheckAll(PersonalServer, ds);

					DatasetHelperBase.Dump(ds);
					return ds;
				}
			}
			catch (Exception ex)
			{
				Trace.WriteLine(ex.ToString());
				throw;
			}
		}

		#endregion //Линейная ведомость

		#region Электронный наряд

		/// <summary>
		/// Набор данных для ЭН, на день и на завтра
		/// </summary>
		/// <param name="Date"></param>
		/// <returns></returns>
		DataSet ILogicForUI.GetDataForRoster(DateTime Date)
		{
			try
			{
				using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForRoster"))
				{
					sp["@DATE"].Value = Date.Date;
					sp["@operator_id"].Value = this.PersonalServer.OperatorID;
					DataSet ds = sp.ExecuteDataSet();
					DatasetHelperBase.RenameTables(ds, Tables.GetDataForRoster);

					DatabaseSchema.MakeSchema(ds);
					this.sc.CheckAll(PersonalServer, ds);

					DatasetHelperBase.Dump(ds);
					return ds;
				}
			}
			catch (Exception ex)
			{
				Trace.WriteLine(ex.ToString());
				throw;
			}
		}

		DataSet ILogicForUI.GetListsForRoster()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetListsForRoster"))
			{
				sp["@operator_id"].Value = this.PersonalServer.OperatorID;
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetListsForRoster);

				DatabaseSchema.MakeSchema(ds);
				this.sc.CheckAll(PersonalServer, ds);

				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}
		/// <summary>
		/// Набор данных расписания на день
		/// </summary>
		/// <param name="Date"></param>
		/// <returns></returns>
		DataSet ILogicForUI.GetScheduleByDate(DateTime Date)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetScheduleByDate"))
			{
				sp["@DATE"].Value = Date.Date;
				sp["@operator_id"].Value = this.PersonalServer.OperatorID;
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetScheduleByDate);

				DatabaseSchema.MakeSchema(ds);
				this.sc.CheckAll(PersonalServer, ds);

				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}


		#endregion Электронный наряд

		#region Календарь

		DataSet ILogicForUI.GetDataForCalendar()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForCalendar"))
			{
				sp["@operator_id"].Value = this.PersonalServer.OperatorID;
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetDataForCalendar);
				DatabaseSchema.MakeSchema(ds);
				this.sc.CheckAll(PersonalServer, ds);
				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		int ILogicForUI.CreateDayKind(string Name, int DkSetId, byte Weekdays)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.CreateDayKind"))
			{
				sp["@NAME"].Value = Name;
				sp["@DK_SET_ID"].Value = DkSetId;
				sp["@WEEKDAYS"].Value = Weekdays;

				return Convert.ToInt32(sp.ExecuteScalar());
			}
		}

		int ILogicForUI.CreateDkSet(string Name, int departmentID)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.CreateDkSet"))
			{
				sp["@NAME"].Value = Name;
				sp["@departmentID"].Value = departmentID;

				return Convert.ToInt32(sp.ExecuteScalar());
			}
		}

		void ILogicForUI.CopyScheduleToDayKind(int ScheduleId, int DayKindId)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.CopyScheduleToDayKind"))
			{
				sp["@SCHEDULE_ID"].Value = ScheduleId;
				sp["@DAY_KIND_ID"].Value = DayKindId;
				sp.ExecuteNonQuery();
			}
		}

		DataSet ILogicForUI.GetScheduleByDayKind(int DayKindId)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetScheduleByDayKind"))
			{
				sp["@DAY_KIND_ID"].Value = DayKindId;
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.Schedule);
				ds = sc.Check(PersonalServer, ds);
				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		DataSet ILogicForUI.GetScheduleByDkSet(int DkSetId)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetScheduleByDkSet"))
			{
				sp["@DK_SET_ID"].Value = DkSetId;
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.Schedule);
				ds = sc.Check(PersonalServer, ds);
				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}
		//#################################################################################
		#endregion Календарь 

		#region Графики

		DataSet ILogicForUI.GetDataForGraphic()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForGraphic"))
			{
				sp["@operator_id"].Value = this.PersonalServer.OperatorID;
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetDataForGraphic);
				ds = sc.Check(PersonalServer, ds);
				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}



		#endregion Графики

		DataSet ILogicForUI.GetScheduleInfo(int sch_id, DateTime whdate)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetScheduleInfo"))
			{
				sp["@sch_id"].Value = sch_id;
				sp["@whdate"].Value = whdate;
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetScheduleInfo);
				ds = sc.Check(PersonalServer, ds);
				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		void ILogicForUI.UpdateWBTrip(int wbtrip, int status, DateTime begin, DateTime end)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.UpdateWBTrip"))
			{
				sp["@wbtrip"].Value = wbtrip;
				sp["@status"].Value = status;
				sp["@begin"].Value = begin;
				sp["@end"].Value = end;
				sp.ExecuteNonQuery();
			}
		}

		DataSet ILogicForUI.GetReservedWaybills(DateTime whdate)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetReservedWaybills"))
			{
				sp["@whdate"].Value = whdate;
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetReservedWaybills);
				ds = sc.Check(PersonalServer, ds);
				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		DataSet ILogicForUI.GetScheduleTrips(int sch_id, DateTime whdate)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetScheduleTrips"))
			{
				sp["@sch_id"].Value = sch_id;
				sp["@whdate"].Value = whdate;
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetScheduleTrips);
				ds = sc.Check(PersonalServer, ds);
				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		void ILogicForUI.BindSchedule(int wb_id, int sch_id, int tripFrom, int tripTo)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.BindSchedule"))
			{
				sp["@wb_id"].Value = wb_id;
				sp["@sch_id"].Value = sch_id;
				sp["@tripfrom"].Value = tripFrom;
				sp["@tripto"].Value = tripTo;
				sp.ExecuteNonQuery();
			}
		}

		DataSet ILogicForUI.GetWaybillStatus()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetWaybillStatus"))
			{
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetWaybillStatus);
				ds = sc.Check(PersonalServer, ds);
				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		void ILogicForUI.UnbindSchedule(int wh_id, int status, int sch_id)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.UnbindSchedule"))
			{
				sp["@wh_id"].Value = wh_id;
				if (status != -1) sp["@status"].Value = status;
				sp["@sch_id"].Value = sch_id;
				sp.ExecuteNonQuery();
			}
		}

		DataSet ILogicForUI.GetCounts(DateTime dtFrom, DateTime dtTo)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetCounts"))
			{
				sp["@time_from"].Value = dtFrom;
				sp["@time_to"].Value = dtTo;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetCounts);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		//Получение водителей истории движения
		DataSet ILogicForUI.GetMoveHistoryDrivers(int count, int interval, DateTime time_from, DateTime time_to, string vehicle)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetMoveHistoryDrivers"))
			{
				sp["@count"].Value = count;
				sp["@interval"].Value = interval;
				sp["@time_from"].Value = time_from;
				sp["@time_to"].Value = time_to;
				sp["@vehicle"].Value = vehicle;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetMoveHistoryDrivers);
				return dataSet;
			}
		}

		//Получение инфы по маршруту с указанием типа расписания
		DataSet ILogicForUI.GetRouteInfo(int route_id)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetRouteInfo"))
			{
				sp["@route_id"].Value = route_id;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetRouteInfo);
				return dataSet;
			}
		}

		//Получение инфы по маршруту
		DataSet ILogicForUI.GetRoute(int route_id)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetRoute"))
			{
				sp["@route_id"].Value = route_id;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetRoute);
				return dataSet;
			}
		}
		//добавление остановки
		int ILogicForUI.PointAdd(int POINT_KIND_ID, string NAME, float X, float Y, string ADDRESS, decimal RADIUS, string COMMENT)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.PointAdd"))
			{
				sp["@POINT_KIND_ID"].Value = POINT_KIND_ID;
				sp["@NAME"].Value = NAME;
				sp["@X"].Value = X;
				sp["@Y"].Value = Y;
				sp["@ADDRESS"].Value = ADDRESS;
				sp["@RADIUS"].Value = RADIUS;
				sp["@COMMENT"].Value = COMMENT;
				return (int)(Decimal)sp.ExecuteScalar();
			}
		}

		bool ILogicForUI.CanReprint()
		{
			return sc.CanReprint(PersonalServer);
		}

		DataSet ILogicForUI.GetWaybillsByDate(DateTime dtBegin, DateTime dtEnd)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetWaybillsByDate"))
			{
				sp["@date_begin"].Value = dtBegin;
				sp["@date_end"].Value = dtEnd;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetWaybillsByDate);
				return dataSet;
			}
		}


		DataSet ILogicForUI.GetFuelStat(bool full, DateTime from, DateTime to, int vid, int iUBound)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetFuelStat"))
			{
				if (full)
				{
					sp["@full"].Value = 1;
					sp["@vid"].Value = vid;
				}
				if (from != TimeHelper.NULL) sp["@from"].Value = from;
				if (to != TimeHelper.NULL) sp["@to"].Value = to;
				sp["@ubound"].Value = iUBound;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetFuelStat);
				DatasetHelperBase.Dump(dataSet);
				return dataSet;
			}
		}

		DataSet ILogicForUI.GetOperatorsSessionByDate(int operator_id, DateTime from, DateTime to)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetOperatorsSessionByDate"))
			{
				sp["@from"].Value = from;
				sp["@to"].Value = to;
				sp["@OperatorID"].Value = operator_id;

				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetOperatorsSessionByDate);
				DatasetHelperBase.Dump(dataSet);

				return dataSet;
			}
		}

		/// <summary>
		/// Возвращает сисок действий оператора за определенный период времени для указанной таблицы
		/// </summary>
		/// <param name="HTableName">Название H-таблицы</param>
		/// <param name="operator_id">ID оператора</param>
		/// <param name="from">Нижняя граница интервала</param>
		/// <param name="to">Верхняя граница интервала</param>
		/// <returns>DataSet</returns>
		/// <remarks>Добавлено: Гапченко</remarks>
		DataSet ILogicForUI.GetOpearatorActions(string HTableName, int operator_id, DateTime from, DateTime to)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetOpearatorActions"))
			{
				sp["@from"].Value = from;
				sp["@to"].Value = to;
				sp["@OperatorID"].Value = operator_id;
				sp["@HTableName"].Value = HTableName;

				DataSet dataSet = sp.ExecuteDataSet();
				if (dataSet.Tables.Count != 0)
					DatasetHelperBase.RenameTables(dataSet, Tables.GetOpearatorActions);

				return dataSet;
			}
		}

		#region Логирование действий оператора	
		/// <summary>
		/// Получение списка H-таблиц. Нужно для логирования действий оператора
		/// </summary>
		/// <returns>DataSet со списком таблиц</returns>
		/// <remarks>Добавлено: Гапченко</remarks>
		public static DataSet GetHTables()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetHTables"))
			{
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetHTables);
				DatasetHelperBase.Dump(dataSet);

				return dataSet;
			}
		}
		/// <summary>
		/// Загружает из базы список Н-таблиц
		/// </summary>
		private static string[] LoadHTablesList()
		{
			string[] HTablesList;
			using (DataSet ds = GetHTables())
			{
				HTablesList = new string[ds.Tables["H_TABLES"].Rows.Count];
				int i = 0;
				foreach (DataRow row in ds.Tables["H_TABLES"].Rows)
				{
					HTablesList[i++] = row["NAME"].ToString();
				}
			}
			return HTablesList;
		}
		#endregion

		Hashtable ILogicForUI.GetOpearatorActions(int operator_id, DateTime from, DateTime to)
		{
			Hashtable HT = new Hashtable();
			foreach (string HTableName in LoadHTablesList())
			{
				DataSet ds = ((ILogicForUI)this).GetOpearatorActions(HTableName, operator_id, from, to);
				if (ds.Tables.Count != 0)
					foreach (DataRow row in ds.Tables["ACTIONS"].Rows)
					{
						HT.Add(row["TRAIL_TIME"], row["ACTION"] + " in " + HTableName);
					}
			}
			return HT;
		}

		string[] ILogicForUI.GetHTablesList()
		{
			return LoadHTablesList();
		}

		void ILogicForUI.SetFuelInfo(int cid, int lval, int uval, bool force, bool bDirectSensor)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.SetFuelInfo"))
			{
				sp["@ctlr"].Value = cid;
				if (lval > -1) sp["@lval"].Value = lval;
				if (uval > -1) sp["@uval"].Value = uval;
				sp["@force"].Value = force ? 1 : 0;
				sp["@sensor_direction"].Value = bDirectSensor ? 1 : 0;
				sp.ExecuteNonQuery();
			}
		}

		void ILogicForUI.DelControllerStat(int cid, DateTime from, DateTime to)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.DelControllerStat"))
			{
				sp["@ctlr"].Value = cid;
				if (from != TimeHelper.NULL) sp["@from"].Value = from;
				if (to != TimeHelper.NULL) sp["@to"].Value = from;
				sp.ExecuteNonQuery();
			}
		}
		#endregion

		/// <summary>
		/// Удаляет точки истории
		/// </summary>
		/// <param name="monitoreeId">ID of monitoree</param>
		/// <param name="pointTimes">time point to delete</param>
		/// <returns>success</returns>
		bool ILogicForUI.DeleteMonitoreePoints(int monitoreeId, int[] pointTimes)
		{
			bool ret = false;

			int operatorId = 0;

			operatorId = this.PersonalServer.OperatorID;

			SqlConnection sqlConnection1 = new SqlConnection();
			sqlConnection1.ConnectionString = Globals.TssDatabaseManager.DefaultDataBase.ConnectionString;

			try
			{
				sqlConnection1.Open();

				string tableName = "";

				SqlCommand cmd = new SqlCommand();
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Connection = sqlConnection1;
				cmd.CommandText = "dbo.TempTableCreate";
				cmd.Parameters.AddWithValue("@params", "op_id; id_vehicle; time;");

				SqlParameter param = new SqlParameter();
				param.ParameterName = "@ret_table_name";
				param.SqlDbType = SqlDbType.NVarChar;
				param.Size = 50;
				param.DbType = DbType.String;
				param.Value = tableName;
				param.Direction = ParameterDirection.Output;
				cmd.Parameters.Add(param);

				cmd.ExecuteScalar();
				object oString = cmd.Parameters["@ret_table_name"].Value;
				tableName = oString.ToString().Trim();

				//=========================================================================
				//=========================================================================
				//=========================================================================
				string insText = "";
				SqlCommand cmdInsert = new SqlCommand(insText, sqlConnection1);
				cmdInsert.CommandType = CommandType.Text;

				foreach (int tm in pointTimes)
				{
					int id_op = operatorId;
					int id_vehicle = monitoreeId;
					int time = tm;
					insText = string.Format("insert into [{0}] ([op_id], [id_vehicle], [time]) " +
						"values ({1}, {2}, {3})", tableName, id_op, id_vehicle, time);
					cmdInsert.CommandText = insText;
					cmdInsert.ExecuteNonQuery();
				}

				//=========================================================================
				//=========================================================================
				//=========================================================================
				SqlCommand cmdReadProc = new SqlCommand();
				cmdReadProc.CommandType = CommandType.StoredProcedure;
				cmdReadProc.Connection = sqlConnection1;
				cmdReadProc.CommandText = "dbo.DeleteMonitoreePoints";
				cmdReadProc.Parameters.AddWithValue("@ids_table_name", tableName);

				cmd.ExecuteScalar();

				DataSet ds = new DataSet();
				SqlDataAdapter da = new SqlDataAdapter(cmdReadProc);
				da.Fill(ds);

				if (ds.Tables.Count > 0)
					if (ds.Tables[0].Rows.Count > 0)
						ret = true;
			}
			finally
			{
				sqlConnection1.Close();
			}

			return ret;
		}


		/// <summary>
		/// Проверяет можно ли удалять расписание
		/// </summary>
		/// <param name="schedule_id"></param>
		/// <returns>True - если можно удалять расписание</returns>
		bool ILogicForUI.CanDeleteScheduleCheck(int schedule_id)
		{
			bool ret = false;

			int ret_value = 0;

			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.CanDeleteScheduleCheck"))
			{
				sp["@schedule_id"].Value = schedule_id;

				object o = sp.ExecuteScalar();
				if (o != DBNull.Value)
					ret_value = (int)o;
				else
					ret_value = 0;
			}
			if (ret_value == 1)
				ret = true;
			else
				ret = false;

			return ret;
		}

		#region Генератор расписаний

		DataSet ILogicForUI.GetListsForScheduleGen(int routeID)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetListsForScheduleGen"))
			{
				sp["@routeID"].Value = routeID;
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetListsForScheduleGen);
				this.sc.CheckAll(PersonalServer, ds);
				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		DataSet ILogicForUI.GetDataForScheduleGen(int variantID)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForScheduleGen"))
			{
				sp["@variantID"].Value = variantID;
				DataSet ds = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(ds, Tables.GetDataForScheduleGen);
				this.sc.CheckAll(PersonalServer, ds);
				DatasetHelperBase.Dump(ds);
				return ds;
			}
		}

		#endregion // Генератор расписаний


		/// <summary>
		/// Список рабочих станций и телефонов для оператора
		/// </summary>
		/// <returns></returns>
		DataSet ILogicForUI.GetWorkstationsList()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetWorkstationsList"))
			{
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetWorkstationsList);
				return dataSet;
			}
		}

		#region HASP

		int ILogicForUI.GetMaxOperatorsCount()
		{
			return ProtectionKey.ValidUsersCount;
		}

		int ILogicForUI.GetMaxVehicleCount()
		{
			return ProtectionKey.ValidVehiclesCount;
		}

		int ILogicForUI.GetOperatorsCount()
		{
			DataSet ds = ((ILogicForUI)this).GetOperatorGroupsAndOperators();
			return ds.Tables["OPERATOR"].Rows.Count;
		}

		int ILogicForUI.GetVehicleCount()
		{
			DataSet ds = GetVehicles();
			return ds.Tables["VEHICLE"].Rows.Count;
		}

		int ILogicForUI.GetMaxControllerCount()
		{
			return ProtectionKey.ValidControllersCount;
		}

		int ILogicForUI.GetControllerCount()
		{
			DataSet ds = ((ILogicForUI)this).GetDataForControllers();
			return ds.Tables["CONTROLLER"].Rows.Count;
		}
		#endregion //HASP

		/// <summary>
		/// Устанавливает новый пароль оператора
		/// </summary>
		/// <param name="newPassword">новый пароль</param>
		public void SetNewPassword(string newPassword)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.SetNewPassword"))
			{
				sp["@operator_id"].Value = this.PersonalServer.OperatorID;
				sp["@new_password"].Value = newPassword;
				sp.ExecuteNonQuery();
			}
		}

		///// <summary>
		///// Возвращает набор данных, в котором содержится вся информация о зонах для выбранного ТС
		///// </summary>
		///// <param name="vehicle_id">идентификатор ТС в БД [VEHICLE.VEHICLE_ID]</param>
		///// <returns>набор данных, в котором содержится вся информация о зонах для выбранного ТС</returns>
		//DataSet IBasicFunctionSet.GetZones(int vehicle_id)
		//{
		//    using (StoredProcedure sp = FORIS.DataAccess.Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetZones"))
		//    {
		//        sp["@vehicle_id"].Value = vehicle_id;
		//        DataSet dataSet = sp.ExecuteDataSet();
		//        DatasetHelperBase.RenameTables(dataSet, Tables.GetZones);
		//        DatabaseSchema.MakeSchema(dataSet);
		//        dataSet = sc.CheckZones(PersonalServer, dataSet);
		//        return dataSet;
		//    }
		//}

		DataSet ILogicForUI.GetZonesForMap()
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForZone"))
			{
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetZonesForMap);
				DatabaseSchema.MakeSchema(dataSet);
				dataSet = sc.CheckZones(PersonalServer, dataSet);
				return dataSet;
			}
		}


		/// <summary>
		/// Возвращает набор данных, в котором содержится вся информация о зонах для выбранного ТС
		/// </summary>
		/// <param name="vehicle_id">идентификатор ТС в БД [VEHICLE.VEHICLE_ID]</param>
		/// <returns>набор данных, в котором содержится вся информация о зонах для выбранного ТС</returns>
		public override DataSet GetZones(int vehicle_id)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetZones"))
			{
				sp["@vehicle_id"].Value = vehicle_id;
				DataSet dataSet = sp.ExecuteDataSet();
				DatasetHelperBase.RenameTables(dataSet, Tables.GetZones);
				DatabaseSchema.MakeSchema(dataSet);

				dataSet = sc.CheckZones(PersonalServer, dataSet);
				return dataSet;
			}
		}
	}
}