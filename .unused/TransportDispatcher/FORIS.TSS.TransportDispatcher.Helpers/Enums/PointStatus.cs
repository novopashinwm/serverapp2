using System;

namespace FORIS.TSS.TransportDispatcher.Enums
{
	/// <summary>
	/// ����� ����������� �����
	/// </summary>
	[Flags]
	public enum PointStatus : int
	{
		/// <summary>
		/// �������� ��������� A (������)
		/// </summary>
		/// <remarks>����� ������ ��������, ����� �� ���� ������ 1 ������</remarks>
		DispatcherA = 0x01,
		/// <summary>
		/// �������� ��������� B (��������)
		/// </summary>
		/// <remarks>����� ������ ��������, ����� �� ���� ������ 2 ��������</remarks>
		DispatcherB = 0x04,
		/// <summary>
		/// �������� �����
		/// </summary>
		EndPoint = 0x08,
		/// <summary>
		/// ���������� ���������
		/// </summary>
		BusStation = 0x10,
		/// <summary>
		/// ����������� �����
		/// </summary>
		ControlPoint = 0x20
	}
}
