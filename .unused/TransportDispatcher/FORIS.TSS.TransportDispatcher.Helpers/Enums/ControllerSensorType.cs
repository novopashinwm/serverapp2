using System;
using System.Collections.Generic;
using System.Text;

namespace FORIS.TSS.TransportDispatcher.Helpers.Enums
{
	public enum ControllerSensorTypeEnum : int
	{
		Analog = 1,
		Digital = 2
	}
}
