using System;
using System.Collections.Generic;
using System.Text;

namespace FORIS.TSS.TransportDispatcher.Helpers.Enums
{
	public enum ControllerSensorEnum : int
	{
		MJ2216_ANALOG_0 = 1,
		MJ2216_ANALOG_1,
		MJ2216_ANALOG_2,
		MJ2216_ANALOG_3,
		MJ2216_ANALOG_4,
		SGGB_ANALOG_1,
		SGGB_ANALOG_2,
		SGGB_ANALOG_3,
		SGGB_DIGITAL_1,
		SGGB_DIGITAL_2,
		SGGB_DIGITAL_3,
		SGGB_DIGITAL_4,
		SGGB_DIGITAL_5,
		SGGB_DIGITAL_6,
		SGGB_DIGITAL_7,
		SGGB_DIGITAL_8,
		SGGB_DIGITAL_9,
		SGGB_DIGITAL_10,
		SGGB_DIGITAL_11,
		SGGB_DIGITAL_12
	}
}
