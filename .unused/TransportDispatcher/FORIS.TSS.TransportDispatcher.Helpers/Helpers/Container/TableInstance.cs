using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using FORIS.TSS.Common.Collections;

namespace FORIS.TSS.TransportDispatcher.Helpers.Container
{
	public class TableInstance: Component, ITableInstance
	{
		#region Properties

		private string name;
		[ Browsable(true) ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public string Name
		{
			get { return this.name; }
			set { this.name = value; }
		}


		[ Browsable(false) ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden) ]
		public DataTable Table
		{
			get { return this.table; }
		}

		
		#endregion // Properties

		#region Interface

		internal ITableInstance Interface
		{
			get { return this; }
		}

		#endregion // Interface

		#region ITableInstance members

		string ITableInstance.Name
		{
			get { return this.name; }
		}


		RowInstanceCollection ITableInstance.Rows
		{
			get { return this.rows; }
		}
//		ContainerData IContainerItem.Data
//		{
//            get { return this.cddMain.Data; }		 
//			set { this.cddMain.Data = value; }
//		}

		DataTable ITableInstance.Table
		{
			get { return this.table; }
			set
			{
				if( this.table != null )
				{
					this.Interface.Rows.Clear();
				}

				this.table = value;

				if( this.table != null )
				{
					Debug.Assert( this.table.PrimaryKey.Length == 1 );

                    foreach( DataRow row in this.table.Rows )
                    {
						Debug.Assert( row[ this.table.PrimaryKey[0] ] is int );
						int Id = (int)row[ this.table.PrimaryKey[0] ];

						RowInstance InstanceRow = new RowInstance( Id, row );

                    	this.Interface.Rows.Add( InstanceRow );
                    }
				}
			}
		}


		#endregion // ITableInstance members

		#region Components

		private RowInstanceCollection rows;
		private DataTable table;

		#endregion // Components

		#region Constructor & Dispose

		public TableInstance()
		{
			Initialize();
		}
		public TableInstance( IContainer Container )
		{
			Container.Add( this );

			Initialize();
		}

		
		protected override void Dispose(bool disposing)
		{
			if( disposing )
			{
				
			}
			base.Dispose (disposing);
		}

		#endregion // Constructor

		#region Initialize

		private void Initialize()
		{
			this.rows = new RowInstanceCollection();
		}


		#endregion // Initialize
	}
	internal interface ITableInstance//: IContainerItem
	{
		string Name { get; }

		RowInstanceCollection Rows { get; }

		DataTable Table { get; set; }
	}

	public class TableInstanceCollection: CollectionWithEvents
	{
		#region Indexer

		public TableInstance this [ int Index ]
		{
			get { return (TableInstance)base.List[ Index ]; }
		}


		#endregion // Indexer

		#region Manipulation

		public void Add( TableInstance Instance )
		{
			base.List.Add( Instance );
		}
		public void Remove( TableInstance Instance )
		{
			base.List.Remove( Instance );
		}


		#endregion // Manipulation
	}
}