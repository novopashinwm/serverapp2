using System.Collections;
using System.Data;
using System.Diagnostics;
using FORIS.TSS.Common.Collections;

namespace FORIS.TSS.TransportDispatcher.Helpers.Container
{
	public class RowInstance: IRowInstance
	{
		#region IRowInstance.Members

		int IRowInstance.Id
		{
			get { return this.id; }
		}
		DataRow IRowInstance.DataRow
		{
			get { return this.dataRow; }
		}

		ITableInstance IRowInstance.Table
		{
			get { return this.table; }
		}

		

		#endregion // IRowInstance members

		#region Interface

		internal IRowInstance Interface
		{
			get { return this; }
		}

		#endregion // Interface
		
		#region Members

		private int id;
		private DataRow dataRow;
		private TableInstance table;

		#endregion // Members

		#region Constructor

		public RowInstance( int Id, DataRow Row )
		{
			this.id = Id;

			this.dataRow = Row;
		}


		#endregion // Constructor
	}

	internal interface IRowInstance
	{
		int Id { get; }

		DataRow DataRow { get; }

		ITableInstance Table { get; }
	}

	internal class RowInstanceCollection: CollectionWithEvents
	{
		#region Indexer

		internal IRowInstance this [ int ID ]
		{
			get { return (IRowInstance)this.hash[ ID ]; }
		}


		#endregion // Indexer

		#region Components

		private Hashtable hash;

		#endregion // Components

		#region Constructor

		public RowInstanceCollection()
		{
			this.hash = new Hashtable();			
		}


		#endregion // Constructor

		#region Manipulation

		public void Add( IRowInstance Row  )
		{
			base.List.Add( Row );
		}
		public void Remove( IRowInstance Row )
		{
			base.List.Remove( Row );
		}

		#endregion // Manipulation

		#region Maintain hash

		protected override void OnInsert( int Index, object Value )
		{
			Debug.Assert( Value is IRowInstance );
			IRowInstance Instance = (IRowInstance)Value;

			this.hash.Add( Instance.Id, Instance );
			
			base.OnInsert( Index, Value );
		}
		protected override void OnRemoveComplete( int Index, object Value )
		{
			Debug.Assert( Value is IRowInstance );
			IRowInstance Instance = (IRowInstance)Value;

			base.OnRemoveComplete( Index, Value );

			this.hash.Remove( Instance.Id );
		}
		protected override void OnClearComplete()
		{
			base.OnClearComplete ();

			this.hash.Clear();
		}


		#endregion // Maintain hash
	}
}