using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;

// FORIS
using FORIS.TSS.CommonTypes;

namespace FORIS.TSS.TransportDispatcher
{
	#region class Route
	/// <summary>
	/// ����� �������� ��������
	/// </summary>
	[Serializable]
	public class Route
	{
		public class Trip
		{
			public int ID;
			public string Name;
			
			public Trip(int ID, string Name)
			{
				this.ID=ID;
				this.Name=Name;
			}
		}

		/// <summary>
		/// ������ ������ � ��������.
		/// </summary>
		public ArrayList trip=new ArrayList(); 
		/// <summary>
		/// ������������� �������� � ��
		/// </summary>
		protected int routeID;

		/// <summary>
		/// ������������� �������� � ��
		/// </summary>
		[Browsable(false)]
		public int RouteID
		{
			get { return routeID; }
		}

		/// <summary>
		/// �������� (�����) ��������
		/// </summary>
		protected string name;
		
		/// <summary>
		/// �������� (�����) ��������
		/// </summary>
		/// <remarks>� ������ 1.3 </remarks>
		[DisplayName("�������� ��������"), Category("�������� ��������"), Description("��� ��������, ��������� �� ������ � �������������� �������.")]
		public string Name
		{
			get { return name; }
			set 
			{
				//���������, ����� ��������� ����� �������� ���� ������, ��� �, �, �, �
				string lastCh = value.Substring(value.Length-1);
				int num = -1;
				try
				{
					num = int.Parse(lastCh);
				}
				catch
				{
				}
				if (num==-1)
				{
					lastCh = lastCh.ToUpper();
					if (lastCh=="�" || lastCh=="�" || lastCh=="�" || lastCh=="�")
					{
						name = value;
					}
					else
					{
						System.Windows.Forms.MessageBox.Show("��������� ����� � �������� �������� ����� ���� ������ �, �, � ��� �");
						return;
					}
				}
				else
				{
					/**
					 * ���������� ������
					 * 
					 * ����� ����� ���������� ���� ��� ����� ��2
					 */
					name = value;
				}
			}
		}

		protected int color;
		[Browsable(false)]
		public int ColorARGB
		{
			get { return color; }
		}

		protected float length;
		[DisplayName("������������"), Category("�������� ��������"), Description("������������ �������� � ����������.")]
		public float Length
 		{
 			get { return length; }
			set { length = value; }
 		}

		[DisplayName("����"), Category("�������� ��������"), Description("����, ������� ������������ ����������� ����� �������� �� �����.")]
		public Color Color
		{
			get { return Color.FromArgb(color); }
			set { color = value.ToArgb(); }
		}


		protected string prefix = "";
		[DisplayName("�������"), Category("�������� �������"), Description("��������� ���������� �������� ��������")]
		public string Prefix
		{
			get{return prefix;}
			set{prefix = value;}
		}

		protected int digitCount = 0;
		[DisplayName("���������� ���� � �������� ������"), Category("�������� �������"), Description("���������� ���� � �������� ������")]
		public int DigitCount
		{
			get {return digitCount;}
			set
			{
				if (value==0 || value==1 || value==2)
					digitCount = value;
				else
					System.Windows.Forms.MessageBox.Show("����� ���� 0, ���� ��� ��� �����");
			}
		}


		private int _TimeDeviation = 600;
		[DisplayName("���������� ��-���������"), Category("�������� �������"), Description("����������, ������� ����� ��������� ����� ����� ����������")]
		public int TimeDeviation
		{
			get{return _TimeDeviation;}
			set{_TimeDeviation = value;}
		}

		private int routeDeviation = 0;
		[DisplayName("���������� �� ���������� ��������"), Category("�������� ��������"), Description("���������� �� ���������� ��������. �� ��������� = 0 - �� ������")]
		public int RouteDeviation
		{
			get { return routeDeviation; }
			set { routeDeviation = value; }
		}

		#region .ctor
		public Route(int routeID, string name, int color)
		{
			this.routeID = routeID;
			this.name = name;
			this.color = color;
			this.length = 0;
		}
		public Route(int routeID, string name, int color, float length)
		{
			this.routeID = routeID;
			this.name = name;
			this.color = color;
			this.length = length;
		}
		#endregion // .ctor

		public override string ToString()
		{
			return "� " + name.Trim(); // + " (" + this.RouteID.ToString() + ")";
		}
		public override	bool Equals(object obj)
		{
			Route r = obj as Route;
			if (r == null) return false;
			return this.RouteID == r.RouteID;
		}
		public override	int GetHashCode()
		{
			return this.RouteID;
		}
	}
	#endregion // class Route

	#region class Address
	/// <summary>
	/// ����� ������ (����� � �.�.)
	/// </summary>
	/// <remarks>������������ ��� ����������� ���������.</remarks>
	[Serializable]
	public class Address
	{
		protected string street;
		protected DPoint point;

		/// <summary>
		/// �������� ������
		/// </summary>
		public string Street
		{
			get { return street; }
			set { street = value as String; }
		}

		/// <summary>
		/// ����� �������� � �����
		/// </summary>
		public DPoint Point
		{
			get { return point; }
			set { point = value; }
		}
	}
	#endregion // class Address

	#region class RoutePoint
	/// <summary>
	/// ����� �������� ��������
	/// </summary>
	[Serializable]
	public class RoutePoint
	{
		#region ������� ��������� � ���������� ������� ����� ��������
		public event EventHandler RoutesCountChange;
		#endregion // ������� ��������� � ���������� ������� ����� ��������

		protected int pointID;
		public int PointID
		{
			get { return pointID; }
		}

		protected string name;
		public string Name
		{
			get { return name; }
		}
		
		protected Address address;
		public Address Address
		{
			get { return address; }
		}
		
		// ������ ���������, ������� ����������� ������ �����
		protected Hashtable routes = new Hashtable();
		/// <summary>
		/// ������ ���������
		/// </summary>
		public Route[] Routes
		{
			get 
			{ 
				Route[] r = new Route[routes.Count]; 
				routes.Values.CopyTo(r, 0);
				return r;
			}
		}

		/// <summary>
		/// ���������� ���������, ������� ����������� ������ �����
		/// </summary>
		public int RoutesCount
		{
			get { return routes.Count; }
		}

		public RoutePoint(int pointID, string name, Route route, Address address)
		{
			this.name = name;
			this.pointID = pointID;
			this.address = address;
			AddRoute(route);
		}

		public override string ToString()
		{
			return name.Trim();// + " (" + this.pointID.ToString() + ")";
		}

		public override	bool Equals(object obj)
		{
			RoutePoint p = obj as RoutePoint;
			if(p == null) return false;
			return this.pointID == p.pointID;
		}
		
		public override	int GetHashCode()
		{
			return this.pointID;
		}

		#region AddRoute(Route route)
		/// <summary>
		/// ���������� ������ �� �������
		/// </summary>
		/// <param name="route">�������, ������� ���� �������� � ������ ���������</param>
		public void AddRoute(Route route)
		{
			if(route == null)
				return;

			lock(routes)
			{
				if(!routes.ContainsKey(route.RouteID))
				{
					routes.Add(route.RouteID, route);
					OnRoutesCountChange();
				}
			}
		}
		#endregion AddRoute(Route route)

		#region RemoveRoute(Route route)
		/// <summary>
		/// �������� ������ �� �������
		/// </summary>
		public void RemoveRoute(Route route)
		{
			if(route == null)
				return;

			lock(routes)
			{
				if(routes.ContainsKey(route.RouteID))
				{
					routes.Remove(route.RouteID);
					OnRoutesCountChange();
				}
			}
		}
		/// <summary>
		/// �������� ������ �� �������
		/// </summary>
		public void RemoveRoute(int routeID)
		{
			lock(routes)
			{
				if(routes.ContainsKey(routeID))
				{
					routes.Remove(routeID);
					OnRoutesCountChange();
				}
			}
		}
		/// <summary>
		/// �������� ������ �� �������
		/// </summary>
		public void RemoveRoute(string routeName)
		{
			lock(routes)
			{
				int routeID = 0;
				foreach(Route route in routes)
					if(route.Name == routeName)
					{
						routeID = route.RouteID;
						break;
					}
				if(routeID != 0)
				{
					routes.Remove(routeID);
					OnRoutesCountChange();
				}
			}
		}
		#endregion // RemoveRoute(Route route)

		#region ContainsRoute(int routeID)
		/// <summary>
		/// ���������� ������� ���������� �������� � ������ ��������� �����
		/// </summary>
		public bool ContainsRoute(int routeID)
		{
			bool exist_on;
			lock(routes)
			{
				exist_on = routes.ContainsKey(routeID);
			}
			return exist_on;
		}
		/// <summary>
		/// ���������� ������� ���������� �������� � ������ ��������� �����
		/// </summary>
		public bool ContainsRoute(string routeName)
		{
			bool exist_on = false;
			lock(routes)
			{
				foreach(Route route in routes)
					if(route.Name == routeName)
					{
						exist_on = true;
						break;
					}
			}
			return exist_on;
		}
		#endregion // ContainsRoute(int routeID)

		#region OnRoutesCountChange
		protected void OnRoutesCountChange()
		{
			if(RoutesCountChange != null)
				RoutesCountChange(this, new EventArgs());
		}
		#endregion // OnRoutesCountChange

	public class Driver
	{
		public string Name;
		public int DriverId;
		public override string ToString()
		{
			return Name + "(" + DriverId + ")";
		}
		public override bool Equals(object obj)
		{
			Driver d = obj as Driver;
			if (d == null) return false;
			return this.DriverId == d.DriverId;
		}
		public override	int GetHashCode()
		{
			return this.DriverId;
		}
	}
	}
	#endregion // class RoutePoint

	public class Schedule
	{
		public string ExtNumber;
		int Id;

		public int ScheduleId
		{
			get {return Id;}
			set {Id = value;}
		}

		public override string ToString()
		{
			return ExtNumber;
		}
		public override bool Equals(object obj)
		{
			Schedule s = obj as Schedule;
			if (s == null) return false;
			return this.Id == s.ScheduleId;
		}
		public override	int GetHashCode()
		{
			return this.ScheduleId;
		}
	}
	public class Driver
	{
		public string Name;
		public string ShortName;
		public string ExtNumber;
		public int DriverId;
		public override string ToString()
		{
			return ExtNumber + " " + ShortName + " " + Name/* + " (" + DriverId + ")"*/;
		}
		public override bool Equals(object obj)
		{
			Driver d = obj as Driver;
			if (d == null) return false;
			return this.DriverId == d.DriverId;
		}
		public override	int GetHashCode()
		{
			return this.DriverId;
		}
	}
	public class DayKind
	{
		public string Name;
		public int DayKindId;
		public override string ToString()
		{
			return Name/* + " (" + DayKindId + ")"*/;
		}
		public override bool Equals(object obj)
		{
			DayKind d = obj as DayKind;
			if (d == null) return false;
			return this.DayKindId == d.DayKindId;
		}
		public override	int GetHashCode()
		{
			return this.DayKindId;
		}
	}

	public class DKSet
	{
		public string Name;
		public int DKSetId;

		public override string ToString()
		{
			return Name;
		}

		public override bool Equals(object o)
		{
			DKSet dks = o as DKSet;

			if (dks == null)
				return false;

			return (this.DKSetId == dks.DKSetId);
		}

		public override	int GetHashCode()
		{
			return this.DKSetId;
		}
	}
}
