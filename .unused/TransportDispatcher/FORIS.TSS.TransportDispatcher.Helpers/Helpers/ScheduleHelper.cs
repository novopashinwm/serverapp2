using System;

namespace FORIS.TSS.TransportDispatcher.Helpers
{
	/// <summary>
	/// ����� ���������� ������� ��� ������ � ���������� ���������� ����������
	/// </summary>
	public class ScheduleHelper
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		public static TimeSpan TimeParse(string s)
		{
			if(s == "") return TimeSpan.FromSeconds(-1);

			s = s.Trim();
			
			string[] parts = s.Split(":".ToCharArray());
			
			if(parts.Length != 2 || s.IndexOfAny("-.eE".ToCharArray()) != -1) 
				throw new ApplicationException("������������ ������ �������");

			int h = Int32.Parse(parts[0]), m = Int32.Parse(parts[1]);
			
			if(h > 47 || m > 59) 
				throw new ApplicationException("������������ ������ �������");

			TimeSpan ts = new TimeSpan();

			if(h > 23) 
				ts = ts.Add(TimeSpan.FromDays(1));
			
			return ts.Add(TimeSpan.FromHours(h % 24)).Add(TimeSpan.FromMinutes(m));
		}


	}
}
