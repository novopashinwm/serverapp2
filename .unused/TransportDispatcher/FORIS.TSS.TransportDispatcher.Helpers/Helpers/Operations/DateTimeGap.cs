using System;
using System.Collections;
using FORIS.TSS.Common;

namespace FORIS.TSS.TransportDispatcher.Helpers.Operations
{
	public class DateTimeGap
	{
		DateTime dtBegin;
		public DateTime Begin
		{
			get { return this.dtBegin; } 
			set { this.dtBegin = value; }
		}

		
		DateTime dtEnd;
		public DateTime End
		{
			get { return this.dtEnd; } 
			set { this.dtEnd = value; }
		}

		
		public DateTimeGap( DateTime Begin, DateTime End )
		{
			this.dtBegin = Begin;
			this.dtEnd = End;
		}
	}

	public class DateTimeGapCollection: CollectionWithEvents<DateTimeGap>
	{
		#region Manipulation methods

		public DateTimeGapCollection Clone()
		{
			DateTimeGapCollection Collection = new DateTimeGapCollection();

			foreach( DateTimeGap gap in this )
			{
				Collection.Add( gap );
			}

			return Collection;
		}

		
		#endregion // Manipulation methods

		#region Actions

		public void Fuse()
		{
            ArrayList ListFusedGaps = new ArrayList();

			foreach( DateTimeGap gap in this )
			{
				bool Fused = false;

				foreach( DateTimeGap gapFused in ListFusedGaps )
				{
					if( gap.Begin == gapFused.End )
					{
						Fused = true;

						gapFused.End = gap.End;
					}
					else if( gap.End == gapFused.Begin )
					{
						Fused = true;

						gapFused.Begin = gap.Begin;
					}
				}

				if( !Fused )
				{
					ListFusedGaps.Add( gap );
				}
			}

			this.Clear();

			foreach( DateTimeGap gapFused in ListFusedGaps )
			{
				this.Add( gapFused );
			}
		}


		#endregion // Actions

		#region Operations

		public static DateTimeGapCollection operator - ( DateTimeGapCollection Gaps, DateTimeGap Gap )
		{
            DateTimeGapCollection Result = Gaps.Clone();
			
			foreach( DateTimeGap gap in Result.Clone() )
			{
				if( gap.Begin < Gap.Begin && Gap.End < gap.End )
				{
					DateTimeGap GapNew = new DateTimeGap( Gap.End, gap.End );

					gap.End = Gap.Begin;

					Result.Add( GapNew );
				}
				else if( gap.Begin < Gap.Begin && Gap.Begin < gap.End )
				{
					gap.End = Gap.Begin;
				}
				else if( gap.Begin < Gap.End && Gap.End < gap.End )
				{
					gap.Begin = Gap.End;
				}
				else if( Gap.Begin <= gap.Begin && gap.End <= Gap.End )
				{
					Result.Remove( gap );
				}
			}

			return Result;
		}

		#endregion Operations
	}
		
	public class DateTimeGapCollectionHash<TKey>: DictionaryWithEvents<TKey, DateTimeGapCollection>
	{
		public new DateTimeGapCollection this[ TKey key ]
		{
			get
			{
				lock( this )
				{
					if( !this.ContainsKey( key ) )
					{
						this.Add( key, new DateTimeGapCollection() );
					}

					return base[ key ];
				}
			}
		}

		#region Actions

		public void Fuse()
		{
			foreach( DateTimeGapCollection collection in this.Values )
			{
				collection.Fuse();
			}
		}


		#endregion // Actions
	}
}
