using System;
using System.Diagnostics;
using Microsoft.Win32;

namespace FORIS.TSS.TransportDispatcher.Helpers
{
	/// <summary>
	/// ����� �������������� ������� � ������� Windows
	/// </summary>
	/// <remarks>��������� ���������� ���������� (�������� �.�.)</remarks>
	public class RegistryHelper
	{
		/// <summary>
		/// �������� ��������, ������� ������������ � ������� ����������
		/// </summary>
		public static String CompanyName
		{
			get
			{
				return FORIS.TSS.Resources.Strings.CompanyName;
			}
		}

		/// <summary>
		/// �������� ��������, ������� ������������ � ������� ����������
		/// </summary>
		public static String ProductName
		{
			get
			{
				return FORIS.TSS.Resources.Strings.ProductName;
			}
		}

		/// <summary>
		/// �������� ����������� ����������, ������� ������������ � ������� ����������
		/// </summary>
		public static String ClientApplicationName
		{
			get
			{
				return FORIS.TSS.Resources.Strings.ClientApplicationName;
			}
		}

		/// <summary>
		/// ���������� ���� ������� ����������� ���������� ��� �������� ������������
		/// </summary>
		public static RegistryKey CurrentUserRegistry
		{
			get
			{
				string subKey = String.Format(@"Software\{0}\{1}\{2}", RegistryHelper.CompanyName, RegistryHelper.ProductName, RegistryHelper.ClientApplicationName);

				RegistryKey currentUserRegistry = Registry.CurrentUser.OpenSubKey(subKey, true);	
				if(currentUserRegistry == null)
					currentUserRegistry = Registry.CurrentUser.CreateSubKey(subKey);	

				return currentUserRegistry;
			}
		}

		/// <summary>
		/// ���������� ���� ������� ����������� ���������� ��� �������� ������������
		/// </summary>
		public static RegistryKey LocalMachineRegistry
		{
			get
			{
				string subKey = String.Format(@"Software\{0}\{1}\{2}", RegistryHelper.CompanyName, RegistryHelper.ProductName, RegistryHelper.ClientApplicationName);

				RegistryKey localMachineRegistry = Registry.LocalMachine.OpenSubKey(subKey, true);	
				if(localMachineRegistry == null)
					localMachineRegistry = Registry.LocalMachine.CreateSubKey(subKey);	

				return localMachineRegistry;
			}
		}

//		/// <summary>
//		/// ������������ �������� ���������� ��������� � ������� ����������� ���������� ��� �������� ������������
//		/// </summary>
//		/// <param name="parameterName">��� ���������</param>
//		/// <param name="parameterValue">�������� ���������</param>
//		/// <remarks>�������� ������ � ������� � ������ ��������� <a ref="http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpref/html/frlrfmicrosoftwin32registrykeyclasssetvaluetopic.asp"><i>�����</i>.</remarks>
//		public static void SetValue(string parameterName, object parameterValue)
//		{
//			try
//			{
//				RegistryKey userAppDataRegistry = CurrentUserRegistry;
//				if(userAppDataRegistry != null)
//					userAppDataRegistry.SetValue(parameterName, parameterValue);
//			}
//			catch(Exception exc)
//			{
//				Trace.TraceError("{0}", exc);
//			}
//		}
//
//		/// <summary>
//		/// ���������� �������� ���������� ��������� �� ������� ����������� ���������� ��� �������� ������������
//		/// </summary>
//		/// <param name="parameterName">��� ���������</param>
//		/// <returns>�������� ���������</returns>
//		/// <remarks>�������� ������ � ������� � ������ ��������� <a ref="http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpref/html/frlrfmicrosoftwin32registrykeyclassgetvaluetopic.asp"><i>�����</i>.</remarks>
//		public static object GetValue(string parameterName)
//		{
//			object parameterValue = null;
//
//			try
//			{
//				RegistryKey userAppDataRegistry = CurrentUserRegistry;
//				if(userAppDataRegistry != null)
//					parameterValue = userAppDataRegistry.GetValue(parameterName);
//			}
//			catch(Exception exc)
//			{
//				Trace.TraceError("{0}", exc);
//			}
//
//			return parameterValue;
//		}
//
//		/// <summary>
//		/// ���������� �������� ���������� ��������� �� ������� ����������� ���������� ��� �������� ������������. � ������
//		/// ���� ������ ��������� �� ����������, ������������ �������� ��-���������
//		/// </summary>
//		/// <param name="parameterName">��� ���������</param>
//		/// <param name="defaultValue">�������� ��������� ��-���������</param>
//		/// <returns>�������� ���������</returns>
//		/// <remarks>�������� ������ � ������� � ������ ��������� <a ref="http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpref/html/frlrfmicrosoftwin32registrykeyclassgetvaluetopic.asp"><i>�����</i>.</remarks>
//		public static object GetValue(string parameterName, object defaultValue)
//		{
//			object parameterValue = null;
//
//			try
//			{
//				RegistryKey userAppDataRegistry = CurrentUserRegistry;
//				if(userAppDataRegistry != null)
//					parameterValue = userAppDataRegistry.GetValue(parameterName);
//			}
//			catch(Exception exc)
//			{
//				Trace.TraceError("{0}", exc);
//			}
//
//			return parameterValue != null ? parameterValue : defaultValue;
//		}
	}
}
