using System;
using System.Diagnostics;

using FORIS.TSS.Common.Collections;
using FORIS.TSS.Helpers;

namespace FORIS.TSS.TransportDispatcher.Helpers
{
	public class IntegerList: CollectionWithEvents
	{
		#region Indexer

		[ Obsolete("", false) ]
		public int this[ int Index ]
		{
			get { return (int)base.List[ Index ]; }
		}

		#endregion Indexer

		#region Manipulation methods

		public int Add( int Value )
		{
			return base.List.Add( Value );
		}	   
        public void Remove( int Value )
        {
			base.List.Remove( Value );
        }

		public bool Contains( int Value )
		{
			return base.List.Contains( Value );
		}
		
		
		#endregion Manipulation methods

		#region Events

		#region Insert & Delete

		private IntegerEventHandler delegateInsert;
		private IntegerEventHandler delegateDelete;

		/// <summary>
		/// ������� ����������� �������
		/// </summary>
		public event IntegerEventHandler Insert
		{
			add { this.delegateInsert += value; }
			remove { this.delegateInsert -= value; }
		}
		/// <summary>
		/// ������� ������������ ��������
		/// </summary>
		public event IntegerEventHandler Delete
		{
			add { this.delegateDelete += value; }
			remove { this.delegateDelete -= value; }
		}

		#endregion // Insert & Delete

		protected override void OnInsertComplete( int index, object value )
		{
			Debug.Assert( value is int );

			base.OnInsertComplete (index, value);

			if( this.delegateInsert != null )
				this.delegateInsert( this, new IntegerEventArgs((int)value) );
		}
		protected override void OnRemoveComplete(int index, object value)
		{
			Debug.Assert( value is int );

			base.OnRemoveComplete (index, value);

			if( this.delegateDelete != null )
				this.delegateDelete( this, new IntegerEventArgs((int)value) );
		}


		#endregion Events
	}
}
