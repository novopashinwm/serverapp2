using System;
using System.Collections.Generic;
using System.Diagnostics;
using FORIS.TSS.Common;


namespace FORIS.TSS.TransportDispatcher.Helpers
{
	public abstract class CollectionHash<TKey,TCollection, TItem>: DictionaryWithEvents<TKey, TCollection>
		where TCollection : CollectionWithEvents<TItem>
	{
		#region Indexer

		public new TCollection this[ TKey Key ]
		{
			get
			{
				if( !base.ContainsKey(Key)  )
				{
					return this.Add( Key );
				}

				return base[Key];
			}
		}


		#endregion // Indexer

		#region Properties

		public bool Empty
		{
			get { return this.Count == 0; }
		}

		#endregion // Properties

		#region Manipulation methods

		protected TCollection Add( TKey Key )
		{
			TCollection Hash = this.NewHash();

			base.Add( Key, Hash );

			return Hash;
		}
		
		#endregion Manipulation methods

		protected abstract TCollection NewHash();

		#region Collections' events

		private void Periods_Removed( object sender, CollectionChangeEventArgs<TItem> e )
		{
			Debug.Assert( sender is CollectionWithEvents<TItem> );
			CollectionWithEvents<TItem> Collection = (CollectionWithEvents<TItem>)sender;
			
			TKey Key = this.hashKeys[ Collection ];

			this.OnChange( new CollectionHashChangeEventArgs<TKey,TItem>(Key, Collection) );
		}
		private void Periods_Inserted( object sender, CollectionChangeEventArgs<TItem> e )
		{
			Debug.Assert( sender is CollectionWithEvents<TItem> );
			CollectionWithEvents<TItem> Collection = (CollectionWithEvents<TItem>)sender;
			
			TKey Key = this.hashKeys[ Collection ];

			this.OnChange( new CollectionHashChangeEventArgs<TKey,TItem>(Key, Collection) );
		}
		
		#endregion Collections' events

		#region

		#region Events

		private Dictionary<CollectionWithEvents<TItem>, TKey> hashKeys =
			new Dictionary<CollectionWithEvents<TItem>, TKey>();


		protected override void OnRemoving( TKey key, TCollection value )
		{
			TCollection Collection = value;
			
			this.hashKeys.Remove( Collection );
			
			base.OnRemoving( key, value );
			
			Collection.Inserted -= new CollectionChangeEventHandler<TItem>( Periods_Inserted );
			Collection.Removed -= new CollectionChangeEventHandler<TItem>( Periods_Removed );
		}

		protected override void OnInserted(TKey key, TCollection value )
		{
			TCollection Collection = value;
			
			this.hashKeys.Add( Collection, key );

			base.OnInserted( key, value );

			Collection.Inserted += new CollectionChangeEventHandler<TItem>( Periods_Inserted );
			Collection.Removed += new CollectionChangeEventHandler<TItem>( Periods_Removed );
		}

		protected override void OnClearing()
		{
			this.hashKeys.Clear();

			base.OnClearing();
		}



		#endregion Events

		private CollectionHashChangeEventHandler<TKey,TItem> delegateChange;

		public event CollectionHashChangeEventHandler<TKey,TItem> Change
		{
			add { this.delegateChange += value; }
			remove { this.delegateChange -= value; }
		}

		
		protected virtual void OnChange( CollectionHashChangeEventArgs<TKey,TItem> e )
		{
			if( this.delegateChange != null )//&& !this.IsSuspended )
			{
				this.delegateChange( this, e );
			}
		}

		
		#endregion Events
	}

	public delegate void CollectionHashChangeEventHandler<TKey,TItem>( object sender, CollectionHashChangeEventArgs<TKey, TItem> e );

	public class CollectionHashChangeEventArgs<TKey, TItem>: EventArgs
	{
		#region Properties

		private TKey key;
		public TKey Key
		{
			get { return this.key; }
		}
		
		private CollectionWithEvents<TItem> collection;
		public CollectionWithEvents<TItem> Collection
		{
			get { return this.collection; }
		}
		
		#endregion // Properties

		#region Constructor

		public CollectionHashChangeEventArgs( TKey key, CollectionWithEvents<TItem> collection )
		{
            this.key = key;
			this.collection = collection;
		}


		#endregion // Constructor
	}
}