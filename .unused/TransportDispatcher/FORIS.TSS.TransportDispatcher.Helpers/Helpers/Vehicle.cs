using System;
using System.Collections;
using System.Data;

namespace FORIS.TSS.TransportDispatcher.Helpers
{
	/// <summary>
	/// class representing vehicle
	/// </summary>
	/// <remarks>use and extend it freely with respect to written code</remarks>
	public class Vehicle : IComparable
	{
		protected int iID;
		protected string sGarageNo;
		protected int iFuelTank;

		public virtual int ID
		{
			get
			{
				return iID;
			}
			set
			{
				iID = value;
			}
		}

		public virtual string GarageNo
		{
			get
			{
				return sGarageNo;
			}
			set
			{
				sGarageNo = value == null? "": value.Trim();
			}
		}

		public virtual int FuelTank
		{
			get
			{
				return iFuelTank;
			}
			set
			{
				iFuelTank = value;
			}
		}

		/// <summary>
		/// create vehicles from data table
		/// </summary>
		/// <param name="tbl">table with vehicle info</param>
		/// <returns>array</returns>
		public static Vehicle[] GetVehicle(DataTable tbl)
		{
			DataRowCollection rows = tbl.Rows;
			Vehicle[] ar = new Vehicle[rows.Count];
			bool fuel = tbl.Columns.Contains("FUEL_TANK");
			for(int i = 0, cnt = ar.Length; i < cnt; ++i)
			{
				DataRow row = rows[i];
				Vehicle v = new Vehicle();
				v.ID = (int)row["VEHICLE_ID"];
				v.GarageNo = row["GARAGE_NUMBER"].ToString();
				if(fuel && row["FUEL_TANK"] != DBNull.Value) 
					v.FuelTank = Convert.ToInt32(row["FUEL_TANK"]);
				ar[i] = v;
			}
			return ar;
		}

		public class GarageNoComparer : IComparer
		{
			#region IComparer Members

			public int Compare(object x, object y)
			{
				Vehicle v = x as Vehicle;
				Vehicle v1 = y as Vehicle;
				if(v == null || v1 == null) throw new ArgumentException();

				return v.GarageNo.CompareTo(v1.GarageNo);
			}

			#endregion
		}

		#region IComparable Members

		public int CompareTo(object obj)
		{
			int i = -1;
			if(obj is Vehicle) i = ((Vehicle)obj).ID;
			if(obj is int) i = (int)obj;
			if(i == -1) throw new ArgumentException();

			return ID.CompareTo(i);
		}

		#endregion
	}
}