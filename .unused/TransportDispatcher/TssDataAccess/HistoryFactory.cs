﻿using System;

namespace FORIS.DataAccess
{
	public class HistoryFactory
	{
		public static IDbDatasetAdapter CreateDatasetAdapter()
		{
			var typeOfProvider = Globals.TssDatabaseManager.DefaultDataBase.DbProvider.GetType();
			if (typeOfProvider.Name.IndexOf("TssSqlProvider") >= 0)
				return new SqlDatasetAdapter();
			throw new NotImplementedException();
		}
	}
}