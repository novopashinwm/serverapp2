using System;
using System.Diagnostics;
using System.Reflection;
using System.Collections;
using System.Runtime.Remoting;
using System.Text;
using System.Data;
using FORIS.DataAccess;
using FORIS.TSS.BusinessLogic;

namespace FORIS.DataAccess
{
    /// <summary>
    /// <P>Implements queues for notifications</P>
    /// </summary>
	public class Notifications : IDatasetAdapterAttacheable
	{
		/// <summary>
		/// Maps table name to it's queue of events
		/// (string -> System.Collections.Queue)
		/// </summary>
		/// <remarks>
		/// Why we need to divide queues by table, why not to create single table?
		/// Why we need a strong types?
		/// </remarks>
		public Hashtable queues = new Hashtable();

		/// <summary>
		/// Maps rows in dataset to events in queues
		/// </summary>
		/// <remarks>
		/// Event contains record primary key. 
		/// Primary key of inserted record is unknown until data is not saved to server.
		/// So when insert event is created it contains uninitialised data.
		/// Later, when processing row update we obtain primary key and initialize event data.
		/// </remarks>
		protected Hashtable inserts = new Hashtable();
        
		public Notifications()
		{
		}
        
		public virtual void AttachEvents(IDbDatasetAdapter a)
		{
		}
        
		public virtual void DetachEvents(IDbDatasetAdapter a)
		{
		}
        
		public virtual void CheckDatasetProperties(System.Data.DataSet dataset)
		{
			for (int i = 0; i < dataset.Tables.Count; ++i) // for is faster then foreach
			{
				DataTable table = dataset.Tables[i];
				string tableName = table.TableName.ToUpper();
				if (DatabaseSchema.ContainsTable(tableName))
				{
					CheckProperties(tableName, table);
				}
			}
		}

		protected void CheckProperties(string tableName, DataTable table)
		{
			string pascalTableName = NameHelper.ToPascal(tableName);
			for (int i = 0; i < table.Rows.Count; ++i)
			{
				DataRow row = table.Rows[i];
				switch (row.RowState)
				{
					case DataRowState.Added:
					{
						object args = CreateInsertEventArgs(tableName);
						inserts.Add(row, args);
						Queue q = GetOrAddQueueIfNecessary("On" + pascalTableName + "Added");
						q.Enqueue(args);
						break;
					}
					case DataRowState.Deleted:
					{
						object args = CreateDeleteEventArgs(tableName);
						CopyPrimaryKeyToEvent(args, row);
						Queue q = GetOrAddQueueIfNecessary("On" + pascalTableName + "Removed");
						q.Enqueue(args);
						break;
					}
					case DataRowState.Modified:
					{
						object args = CreateUpdateEventArgs(tableName);
						CopyPrimaryKeyToEvent(args, row);
						Queue q = GetOrAddQueueIfNecessary("On" + pascalTableName + "Updated");
						q.Enqueue(args);
						break;
					}
					default:
					{
						// do nothing
						break;
					}
				}
			}
		}

		protected object CreateInsertEventArgs(string tableName)
		{
			string className = NameHelper.ToPascal(tableName) + "AddedEventArgs";
			ObjectHandle oh = Activator.CreateInstance("FORIS.TSS.Interfaces", "FORIS.TSS.BusinessLogic." + className);
			return oh.Unwrap();
		}

		protected object CreateDeleteEventArgs(string tableName)
		{
			string className = NameHelper.ToPascal(tableName) + "RemovedEventArgs";
			ObjectHandle oh = Activator.CreateInstance("FORIS.TSS.Interfaces", "FORIS.TSS.BusinessLogic." + className);
			return oh.Unwrap();
		}

		protected object CreateUpdateEventArgs(string tableName)
		{
			string className = NameHelper.ToPascal(tableName) + "UpdatedEventArgs";
			ObjectHandle oh = Activator.CreateInstance("FORIS.TSS.Interfaces", "FORIS.TSS.BusinessLogic." + className);
			return oh.Unwrap();
		}

		protected Queue GetOrAddQueueIfNecessary(string name)
		{
			if (this.queues.ContainsKey(name))
			{
				return (Queue)this.queues[name];
			}
			Queue q = new Queue();
			this.queues.Add(name, q);
			return q;
		}

		protected void CopyPrimaryKeyToEvent(object evtArgs, DataRow row)
		{
			string tableName = row.Table.TableName;
			string pascalTableName = NameHelper.ToPascal(tableName);

			FieldInfo pk = evtArgs.GetType().GetField("primaryKey");
			FieldInfo ti = evtArgs.GetType().GetField("TableRow");
			if(ti != null)
			{
				DataTable dt = new DataTable(tableName);
				foreach(DataColumn col in row.Table.Columns)
				{
					dt.Columns.Add(col.ColumnName, col.DataType);
				}
				dt.ImportRow(row);
				ti.SetValue(evtArgs, dt);
			}
			object pkField = pk.GetValue(evtArgs);

			Type keyType = pkField.GetType();
			
			for (int i = 0; i < row.Table.Columns.Count; ++i)
			{
				string columnName = row.Table.Columns[i].ColumnName;
				string memberName = NameHelper.ToPascal(columnName);

				FieldInfo fi = keyType.GetField(memberName);

				if (fi != null)
				{
					if (row.RowState == DataRowState.Deleted)
					{
						fi.SetValue(pkField, row[i, DataRowVersion.Original]); // copy value
					}
					else
					{
						fi.SetValue(pkField, row[i]); // copy value
					}
					pk.SetValue(evtArgs, pkField);				}
			}
		}
	}
}
