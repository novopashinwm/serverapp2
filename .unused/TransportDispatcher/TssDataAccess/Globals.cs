﻿using System.Configuration;
using System.Diagnostics;
using FORIS.DataAccess;
using FORIS.TSS.BusinessLogic;

namespace FORIS.TSS.ServerApplication
{
	public class Globals : GlobalsBase
	{
		/// <summary> строка соединения с БД </summary>
		private static string connectionString          = null;
		/// <summary> флаг инициализации строки соединения </summary>
		private static bool connectionStringInitialized = false;
		/// <summary> Строка соединения с БД </summary>
		/// <remarks> Данное свойство возвращает ссылку на глобальный объект строки соединения к БД </remarks>
		public static string ConnectionString
		{
			get
			{
				// double checking to optimize performance
				if (!connectionStringInitialized)
				{
					lock (GlobalsLock)
					{
						if (!connectionStringInitialized)
						{
							connectionStringInitialized = true;
							connectionString            = TssDatabaseManager.DefaultDataBase.ConnectionString;
						}
					}
				}
				return connectionString;
			}
		}
		/// <summary> Менеджер доступа к БД </summary>
		protected static TssDatabaseManager databaseManager = null;
		/// <summary> Менеджер доступа к БД </summary>
		/// <remarks><p>Данное свойство возвращает ссылку на глобальный объект менеджера доступа к БД (патерн Singleton).</p></remarks>
		public static TssDatabaseManager TssDatabaseManager
		{
			get
			{
				if (databaseManager == null) // double checking for efficiency
				{
					lock (GlobalsLock)
					{
						if (databaseManager == null)
						{
							databaseManager = (TssDatabaseManager)ConfigurationManager.GetSection("databases");
							string database = AppSettings["Database"];
							databaseManager.SetDefaultDatabase(database);

							Trace.TraceInformation("Database Manager initialization: database alias is '{0}'", database);
							string connectionstring = databaseManager.DefaultDataBase.ConnectionString;
							Trace.TraceInformation("Database Manager initialization: connection string is '{0}'", connectionstring);
						}
					}
				}
				return databaseManager;
			}
		}
	}
}