﻿using System;

namespace FORIS.TSS.ServerLauncher.Application.TD
{
	static class Program
	{
		[STAThread]
		static void Main()
		{
			/// Приложение сервера конфигурируется при старте домена приложения сервера
			TrayIconApplicationContext.RunInteractive("Business Server", true, true, true);
		}
	}
}