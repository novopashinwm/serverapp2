using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;

using FORIS.TSS.Common;


namespace FORIS.TSS.TransportDispatcher
{
	/// <summary>
	/// Summary description for UpdateUpdater.
	/// </summary>
	public class UpdateUpdaterHelpers : Component
	{
		public static BooleanSwitch swUpdater = new BooleanSwitch("UPDATER", "Diagnostic for domain creation during updater update");

		/// <summary>
		/// Updates updater application
		/// </summary>
		/// <remarks>
		/// Called from main function through timer.
		/// Parameter 'obj' is not used (it is required in order to match delegate)
		/// </remarks>
		/// <param name="obj">is not used</param>
		public static void UpdateUpdater(object obj)
		{
			try
			{
				
				// Create new domain to setup configuration file
				string nameOfNewDomain = "TSSUpdateClient";
				string nameOfConfigFile = "AppUpdater.config";
				string nameOfAssemblyFile = "FORIS.TSS.Update.ClientCore";
				string nameOfType = "FORIS.TSS.Update.ClientEntryPoint";
				
				DirectoryInfo root = new DirectoryInfo(AppDomain.CurrentDomain.SetupInformation.ApplicationBase);
				DirectoryHelper.AdjustForDebug(ref root, "..\\..\\..\\..\\");
				Trace.WriteLineIf(swUpdater.Enabled, "Root directory after adjusting : '" + root.FullName + "'");
#if DEBUG
				CheckFiles(root);
#endif

				AppDomain appDomain;
				IEntryPoint ep = (IEntryPoint)DomainHelper.CreateDomainAndGetEntryPoint(
					nameOfNewDomain, nameOfConfigFile, nameOfAssemblyFile, nameOfType, 
					out appDomain,
					root.FullName, 
					"FORIS.TSS.Update\\ClientCore\\",
					"FORIS.TSS.Update\\ClientCore\\bin\\debug\\",
					"FORIS.TSS.TransportDispatcher\\Workplace\\",
					"FORIS.TSS.TransportDispatcher\\Workplace\\bin\\Debug\\"
					);
				ep.Start(false, false, true );
			}
			catch (System.Exception ex)
			{
				Trace.WriteLine(ex.ToString());
			}
		}

		public static void CheckFiles(DirectoryInfo directoryInfo)
		{
			CheckFile(directoryInfo, "FORIS.TSS.Infrastructure.Y_Router_Client");
			CheckFile(directoryInfo, "FORIS.TSS.Infrastructure.MachineName");
		}

		public static void CheckFile(DirectoryInfo directoryInfo, string filename)
		{
			FileInfo[] files = directoryInfo.GetFiles(filename + ".dll");
			if (files.Length == 0)
			{
				Trace.WriteLineIf(swUpdater.Enabled, "File " + filename + " was not found in directory");
			}
			else
			{
				Trace.WriteLineIf(swUpdater.Enabled, "File " + filename + " was founded");
			}
		}
	}
}
