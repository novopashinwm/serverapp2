using System;
using System.Collections.Generic;
using System.Text;
using FORIS.TSS.Common;
using System.ComponentModel;

namespace FORIS.TSS.WorkplaceShadow
{
	public class SecurityCheckerStub:
		Component,
		ISecurityChecker
	{
		public bool Check(string key)
		{
			return true;
		}
	}
}
