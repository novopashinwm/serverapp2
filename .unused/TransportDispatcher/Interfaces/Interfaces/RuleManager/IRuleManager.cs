using System;
using System.Collections.Generic;
using System.ComponentModel;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.TransportDispatcher.RuleManager
{
	public delegate void ExecRuleServerHandler(ExecRuleServerEventArgs args);

	public interface IRuleManager
	{
		void LoadData();

		void SetCheckedVehicles(List<int> array);

		Status GetStatus( int vehicleID );
		bool SetStatus( int vehicleID, Status status );
		bool UnSetStatus( int vehicleID, Status status );
		IRule GetRule(int ruleNumber, int vehicleID);

		event ExecRuleHandler ExecRule;
	}

	public delegate void ExecRuleHandler( ExecRuleEventArgs args );

	[Serializable]
	public class ExecRuleEventArgs: EventArgs
	{
		private IRule ir;

		#region public IRule Rule
		public IRule Rule
		{
			get
			{
				return this.ir;
			}
		}
		#endregion public IRule Rule

		public ExecRuleEventArgs( IRule ir )
		{
			this.ir = ir;
		}
	}

	public abstract class RuleManagerBase : Component
	{

	}

	#region class ExecRuleServerEventArgs
	/// <summary>
	/// ������������ �������� ������� ������������ �������
	/// </summary>
	[Serializable]
	public class ExecRuleServerEventArgs : EventArgs
	{
		#region properties

		/// <summary>
		/// ��������
		/// </summary>
		private int iSourceId;
		public int SourceId
		{
			get { return this.iSourceId; }
		}

		/// <summary>
		/// ������ ���������
		/// </summary>
		public int templateID;

		/// <summary>
		/// ���� 
		/// </summary>
		private string sSubject;

		/// <summary>
		/// ���� 
		/// </summary>
		public string Subject
		{
			get { return this.sSubject; }
		}

		/// <summary>
		/// ���������
		/// </summary>
		private string sBody;
		public string Body
		{
			get { return this.sBody; }
		}

		/// <summary>
		/// �����
		/// </summary>
		private DateTime dtTime;
		public DateTime Time
		{
			get { return this.dtTime; }
			set { this.dtTime = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		private int iMessageId;
		public int MessageId
		{
			get { return this.iMessageId; }
			set { this.iMessageId = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		private int iStatusId;
		public int StatusId
		{
			get { return this.iStatusId; }
		}

		/// <summary>
		/// ������ �� �������
		/// </summary>
		public IRule rule;

		/// <summary>
		/// 
		/// </summary>
		private bool ruleMustExecute;
		public bool RuleMustExecute
		{
			get { return this.ruleMustExecute; }
		}


		/// <summary>
		/// �������� ����� ��� ��
		/// </summary>
		private string sVehicleNumber;
		public string VehicleNumber
		{
			get { return this.sVehicleNumber; }
			set { this.sVehicleNumber = value; }
		}

		#endregion properties

		#region public ExecRuleServerEventArgs

		public ExecRuleServerEventArgs(string sSubject, string sBody, DateTime dtTime, int iSourceId, int iStatusId, string sVehicleNumber)
		{
			this.sSubject = sSubject;
			this.sBody = sBody;
			this.dtTime = dtTime;
			this.iSourceId = iSourceId;
			this.iStatusId = iStatusId;
			this.sVehicleNumber = sVehicleNumber;
			this.iMessageId = 0;
		}

		public ExecRuleServerEventArgs(int iSourceId, int templateID, DateTime dtTime, bool ruleMustExecute)
		{
			// ��
			this.iSourceId = iSourceId;
			this.dtTime = dtTime;
			this.templateID = templateID;
			this.ruleMustExecute = ruleMustExecute;
			this.sSubject = "";
			this.sBody = "";
			this.iStatusId = 0;
			this.sVehicleNumber = "";
			this.iMessageId = 0;
		}

		#endregion public ExecRuleServerEventArgs
	}

	#endregion class ExecRuleServerEventArgs

}