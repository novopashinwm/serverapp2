using System.Collections;
using System.Drawing;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.MobilUnit;
using FORIS.TSS.TerminalService.Interfaces;
using FORIS.TSS.TransportDispatcher.Message;

//using FORIS.TSS.TransportDispatcher.Helpers.Tables;

namespace FORIS.TSS.TransportDispatcher.RuleManager
{
	/// <summary>
	/// Summary description for IRule.
	/// </summary>
	public interface IRule
	{
        /// <summary>
        /// �������� ��� �������� ������������ �������
        /// </summary>
        int Value
        {
            get;
            set;
        }
        
        /// <summary>
		/// ������������� �� ��� �������� ������ �������
		/// </summary>
        int VehicleId
        {
            get; 
        }

		/// <summary>
		/// ������������� ������� (�����)
		/// </summary>
        RuleNumber Number
        {
            get;
        }

		/// <summary>
		/// ����, ����������� ��� ������� ��������� (���� ��������� ���������???)
		/// </summary>
        bool Checked
        {
            get;
        }

		/// <summary>
		/// ���� - ������� ������ ���� ���������� ��� ����� 
		/// </summary>
		bool MustExecute
        {
            get;
        }

		/// <summary>
		/// ���-���
		/// </summary>
		int Hashcode
		{
			get;
		}

		/// <summary>
		/// ��������� ����� ������� ��� ��
		/// </summary>
		/// <param name="mobilUnit">������ �� ������� ��</param>
        void AddPosition(IMobilUnit mobilUnit);
		
		/// <summary>
		/// �������� ��������� ������� - ��������� ��� ������ ���� �����
		/// </summary>
		/// <returns></returns>
		bool CheckRule();

        /// <summary>
        /// �������� ���������
        /// </summary>
        /// <returns></returns>
        void SendRule(IMessageManager imm, bool mustExecute);

        /// <summary>
		/// ���������� ��������� ��������� �������
		/// </summary>
		void Reset();

		/// <summary>
		/// ������ - ��������� ����������� 16�16
		/// </summary>
		Image Icon
		{
			get; 
			set;
		}

		/// <summary>
		/// �������� - ������� ����������� 20�20
		/// </summary>
		Image Image
		{ 
			get;
			set;
		}

	}
}
