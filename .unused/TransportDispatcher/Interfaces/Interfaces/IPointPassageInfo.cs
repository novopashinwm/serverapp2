﻿using System;
using System.Collections.Generic;

namespace FORIS.TSS.BusinessLogic.Interfaces
{
	public interface ISchedulePassageService
	{
		/// <summary> Возвращает информацию об отправлении водителя в рейс </summary>
		/// <param name="idWbTrip"> р.рейс по рейсу расписания </param>
		/// <returns></returns>
		ISchedulePointPassageInfo GetOriginPassageInfo(int idWbTrip);
		/// <summary> Возвращает информацию о прибытии водителя из рейса </summary>
		/// <param name="idWbTrip"> р.рейс по рейсу расписания </param>
		/// <returns></returns>
		ISchedulePointPassageInfo GetDestinationPassageInfo(int idWbTrip);
		/// <summary> Возвращает информацию о прохождении всех контрольных точек водителем в рейсе </summary>
		/// <param name="idWbTrip"> р.рейс по рейсу расписания </param>
		/// <returns></returns>
		ISchedulePointPassageInfo[] GetPassageInfo(int idWbTrip);
		/// <summary> Возвращает информацию о прохождении всех контрольных точек за указанные рейсы </summary>
		/// <param name="wbTrips"> р.рейсы по рейсам расписания </param>
		/// <returns></returns>
		IDictionary<int, ISchedulePointPassageInfo[]> GetPassageInfo(int[] wbTrips);
	}
	public interface ISchedulePointPassageInfo
	{
		int      IdPoint         { get; }
		int      IdSchedulePoint { get; }
		DateTime TimeIn          { get; }
		DateTime TimeOut         { get; }
		DateTime? LogTimeIn      { get; }
		DateTime? LogTimeOut     { get; }
	}
}