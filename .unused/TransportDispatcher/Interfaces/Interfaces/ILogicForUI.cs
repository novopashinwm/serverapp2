using System;
using System.Collections;
using System.Data;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary>
	/// ���� ��������� �������� ����������� �������, 
	/// ������� ���������� � ������� ������� ���
	/// ����������� � ��������� ������
	/// </summary>
	/// <remarks>
	/// <P>����� ���� �� ��������� ��� ������� �� �����
	/// ���������� � ��������� <see cref="IPersonalServer"/>.
	/// ��� �� ���� ������� ����������, ��� ����, �����
	/// ����� ���� ��������� �� VSS ��� ������� ���������� ��
	/// ����� IPersonalServer.</P>
	/// <P>�����, ����������� ������� ����� ����������, 
	/// ��������� � ������ logic_for_ui. � ������ logic_for_ui
	/// ����� ���������� ����� references � ������ ServerApplication.</P>
	/// </remarks>
	public interface ILogicForUI
	{
		/// <summary>
		/// ���������� ������ ��������������� ��������� ���� ������������
		/// </summary>
		/// <returns>������ ��������������� ��������� ���� ������������</returns>
		SystemRights GetSystemRights();

		/// <summary>
		/// ���������� ������ ��� ������ � �������������
		/// </summary>
		/// <returns></returns>
		DataSet GetDataForController();

		/// <summary>
		/// ���������� ������ ��� ������ � �������������
		/// </summary>
		/// <returns></returns>
		DataSet GetDataForController(int controllerId);

		/// <summary>
		/// ���������� ������ ����� ��������� � ���������� � ���������
		/// </summary>
		/// <returns>������� � ��������� [DRIVER_GROUP] � [DRIVER]</returns>
		DataSet GetDriverGroupsAndDrivers();

		/// <summary>
		/// ���������� ����� ������ ��� �������� ������ ��
		/// </summary>
		/// <returns>����� ������ ��� �������� ������ ��</returns>
		DataSet GetDataForVehiclePropertiesNew();

		/// <summary>
		/// ���������� ����� ������ ��� �������������� ������������� ��
		/// </summary>
		/// <param name="vehicle_id">������������� ������������� ��</param>
		/// <returns>����� ������ ��� �������������� ������������� ��</returns>
		DataSet GetDataForVehiclePropertiesEdit(int vehicle_id);

		/// <summary>
		/// ���������� ����� ������ ��� �������� ������ ��������
		/// </summary>
		/// <returns>����� ������ ��� �������� ������ ��������</returns>
		DataSet GetDataForRoutePropertiesCreate();

		/// <summary>
		/// ���������� ����� ������ ��� �������������� ������������� ��������
		/// </summary>
		/// <param name="routeID">������������� ������������� ��������</param>
		/// <returns>����� ������ ��� �������������� ������������� ��������</returns>
		DataSet GetDataForRoutePropertiesEdit(int routeID);

		/// <summary>
		/// ���������� ����� ������ �� �������
		/// </summary>
		/// <param name="begin">����� ������</param>
		/// <param name="end">����� �����</param>
		/// <returns>DataSet � �������</returns>
		DataSet GetDataForControllerFuel(int controllerId, DateTime begin, DateTime end, int maxValue);

		/// <summary>
		/// ���������� ����� ������ ��� �������������� �������� ����.
		/// </summary>
		/// <returns></returns>
		DataSet GetDataForFactors();

		/// <summary>
		/// ���������� ����� ������ ��� ����������� ��������� �� ����� ���
		/// </summary>
		/// <returns>����� ������ ��� ����������� ��������� �� ����� ���</returns>
		DataSet GetDataForRoutesAndPoints();

		/// <summary>
		/// ���������� ������ ����� ������������ ������� � ���������� � ������������ ���������
		/// </summary>
		/// <returns>������� � ��������� [VEHICLE_GROUP] � [VEHICLE]</returns>
		DataSet GetVehicleGroupsAndVehicles();

		/// <summary>
		/// ���������� �������� ��������� ��� ������� "�������� ���������"
		/// </summary>
		/// <remarks>
		/// ������ ���������� �� ��������� �� �������
		/// </remarks>
		/// <returns>������� � �������� [OPERATOR].</returns>
		DataSet GetOperator(int object_id);

		/// <summary>
		/// ���������� ������ ����� ���������� � ���������� �� ����������
		/// </summary>
		/// <returns>������� � ��������� [OPERATOR_GROUP] � [OPERATOR]</returns>
		DataSet GetOperatorGroupsAndOperators();

		/// <summary>
		/// ���������� ����� ������ � ����������� � ��������� � ������� ���������
		/// </summary>
		/// <returns>����� ������ � ����������� � ��������� � ������� ���������</returns>
		DataSet GetRouteGroupsAndRoutes();

		/// <summary>
		/// ���������� ����� ������ ��� ����� "�������� �����"
		/// </summary>
		/// <param name="point_id">�������������� �����</param>
		/// <returns>����� ������ ��� ����� "�������� �����"</returns>
		DataSet GetDataForPointPropertiesDialog(int point_id);
		
		/// <summary>
		/// ������� ��� ����� "�������� ����������"
		/// </summary>
		/// <param name="objectid"></param>
		/// <returns></returns>
		DataSet GetDataForSchedulePropertiesDialog(int objectid);

		DataSet GetRules();

        DataSet GetOwners(int vehicle_id);

		DataSet GetWaybillsPartPermanent();
		DataSet GetWaybillsPartWaybillList(DateTime dt, int iDeltaSeconds);
		DataSet GetWaybillsPartClosed(DateTime dt, int iDeltaSeconds);
		DataSet GetWaybillsPartUnclosed(DateTime dt);

		DataSet GetHistoryForWaybill(int waybill_id);

		DataSet GetVehiclesByRights();

		DataSet GetDataForOperatorPropertiesForm();
		DataSet GetDataForStatusBar();
		DataSet GetVehiclesForMonitoring();
		DataSet GetDataForRoutes();
		
		// ��� ������� ���������� � ����������, ������� ���������, ��������� ����� �������������� ��� ���
		DataSet GetVehiclesForMonitoring(bool checkAdminRight);
		DataSet GetListsForVehicles(bool checkAdminRight);
		
		DataSet GetWaybillHeaderStatusHistory(int waybill_header_id);

//		DataSet GetScheduleForWaybill(int waybill_header_id);
//		DataSet SpecialTripsScheduleGet(int waybill_header_id, int trip_kind_id);

//		DataSet GetAnalytic(int route, DateTime from, DateTime to, string rtnum);

		DataSet GetDayKind(DateTime dt);
		void SetDayKind(DateTime dt, int dkid);

        void MarkMessage(int iMessageId, int iOperatorId);
        void DeleteMessage(int iMessageId, int iOperatorId);

		/// <summary>
		/// get routes and schedules.
		/// </summary>
		/// <param name="dkid">day kind id, not used</param>
		/// <param name="from">time from in schedule. in seconds from schedule start</param>
		/// <param name="to">time to in schedule. in seconds from schedule start</param>
		/// <param name="route">route id, not positive means all</param>
		/// <param name="rgid">routegroup id, not positive means all</param>
		/// <param name="whdate">waybill date</param>
		/// <returns>route and schedule carcasses</returns>
		DataSet GetSchedule(int dkid, int from, int to, int route, int rgid, DateTime whdate);
//
//		/// <summary>
//		/// ��������� ������ �� �� ��� ���������� ��������� �������� �������� �����.
//		/// </summary>
//		/// <param name="iWaybillHeaderID">����� ��</param>
//		/// <param name="iShift">����� �����</param>
//		/// <returns>����������� ������� � ������� ���������� ��� ��</returns>
//		/// <remarks>���������: ������� �. ������</remarks>
//		DataSet WaybillTimesinGet(int iWaybillHeaderID, long iShift);
		

		/// <summary>
		/// get real movement in time through the schedules of 1 or more routes. 
		/// route and sch_id should be used separately. 
		/// in case of using sch_id from - waybill date, to - not used
		/// </summary>
		/// <param name="from">start time or waybill date in UTC</param>
		/// <param name="to">end time</param>
		/// <param name="route">route id, -1 means all</param>
		/// <param name="sch_id">schedule id, -1 means all</param>
		/// <returns>dataset</returns>
		DataSet GetSchedulePassage(DateTime from, DateTime to, int route, int sch_id);

		/// <summary>
		/// get schedules without waybills
		/// </summary>
		/// <param name="dkid">day kind id, not used</param>
		/// <param name="whdate">waybill date</param>
		/// <returns>dataset</returns>
		DataSet GetFreeSchedules(int dkid, DateTime whdate);

		/// <summary>
		/// get schedules and vehicles without waybills
		/// </summary>
		/// <param name="iDayKindId">day kind id</param>
		/// <param name="dtWaybillHeader">waybill date in utc</param>
		/// <returns>dataset</returns>
		DataSet GetUnclosed(int iDayKindId, DateTime dtWaybillHeader);

		/// <summary>
		/// Get next waybill number
		/// </summary>
		/// <returns>next waybill number</returns>
		int GetMaxWayBillExtNumber();

		#region ���������� ��

		DataSet GetListsForVehicles();

		DataSet GetDataForVehicles();

		#endregion // ���������� ��

		#region ���������� ���������

		DataSet GetListsForDrivers();

		DataSet GetDataForDrivers();

		#endregion // ���������� ���������

		#region ���������� ������������

		DataSet GetListsForControllers();

		DataSet GetDataForControllers();

		#endregion // ���������� ������������
		
		#region ������

		DataSet GetDepartment();

		DataSet GetListsForOldJournal();
		DataSet GetDataForOldJournal();
		int CreateDayKind(string Name, int DkSetId, byte Weekdays );
        int CreateDkSet(string Name, int departmentID);
		DataSet GetScheduleByDayKind( int DayKindId );
		DataSet GetScheduleByDkSet( int DkSetId );
		void CopyScheduleToDayKind( int ScheduleId, int DayKindId );

		#endregion ������

		#region ��

		DataSet GetDataForSchedulesInfo();

		DataSet GetDataForJournals();

		DataSet GetDataForDepartment( int IdDepartment );

		DataSet GetWorkTimeForMonth( int idDepartment, int year, int month );

		DataSet GetDataForDecade( int idDecade, int idDecadePrev );

		int CreateDecade( int idDepartment, DateTime DateBegin, DateTime DateEnd, int idDecadeSource );

		#endregion // ��

		#region ������

		DataSet GetListsForOrders();
		DataSet GetDataForOrders();

		#endregion // ������

		#region ���������
		
        DataSet GetDataForCalendar();

		#endregion ���������

		#region ������

		//DataSet GetListsForGraphic();
		DataSet GetDataForGraphic();

		#endregion ������

		#region ����������� �����

		DataSet GetListsForRoster();
		DataSet GetDataForRoster( DateTime Date );
		DataSet GetScheduleByDate( DateTime Date );

		#endregion ����������� �����

		#region �������� ���������

		DataSet GetListsForLineRoll();
		DataSet GetDataForLineRoll( DateTime Date );

		#endregion //�������� ���������

		#region ��������� ����������

		DataSet GetListsForScheduleGen(int routeID);
		DataSet GetDataForScheduleGen(int variantID);
		void DeleteSchedule(int scheduleID);
		
		#endregion // ��������� ����������
		
		/// <summary>
		/// get schedule and its current waybill 
		/// </summary>
		/// <param name="sch_id">schedule id</param>
		/// <param name="whdate">waybill date</param>
		/// <returns>full schedule info and currnet waybill for date</returns>
		DataSet GetScheduleInfo(int sch_id, DateTime whdate);

		/// <summary>
		/// change wbtrip
		/// </summary>
		/// <param name="wbtrip">wbtrip id</param>
		/// <param name="status">new status</param>
		/// <param name="begin">new begin time in utc</param>
		/// <param name="end">new end time in utc</param>
		void UpdateWBTrip(int wbtrip, int status, DateTime begin, DateTime end);

		/// <summary>
		/// get reserved waybills for date
		/// </summary>
		/// <param name="whdate">waybill date</param>
		/// <returns></returns>
		DataSet GetReservedWaybills(DateTime whdate);
		
		/// <summary>
		/// get schedule with trips not having real wbtrips
		/// </summary>
		/// <param name="sch_id">schedule_id</param>
		/// <param name="whdate">waybill_date to look for</param>
		/// <returns></returns>
		DataSet GetScheduleTrips(int sch_id, DateTime whdate);

		/// <summary>
		/// bind schedule with waybill_header and create wbtrips for specified shift
		/// </summary>
		/// <param name="wb_id">waybill_id</param>
		/// <param name="sch_id">schedule_id</param>
		/// <param name="tripFrom">bind from tripFrom</param>
		/// <param name="tripTo">to tripTo</param>
		void BindSchedule(int wb_id, int sch_id, int tripFrom, int tripTo);

		/// <summary>
		/// get list of possible status
		/// </summary>
		/// <returns>all statuses</returns>
		DataSet GetWaybillStatus();

		/// <summary>
		/// unbind schedule and waybill header and drop unclosed trips
		/// </summary>
		/// <param name="wh_id">waybill_header_id</param>
		/// <param name="status">waybill_header_status_id, -1 - default</param>
		/// <param name="sch_id">schedule_id</param>
		void UnbindSchedule(int wh_id, int status, int sch_id);
		DataSet GetCounts(DateTime dtFrom, DateTime dtTo);
		/// <summary>
		///���������� �� ��������
		/// </summary>
		/// <remarks>���������: �������</remarks>
		DataSet GetRouteInfo(int route_id);
		/// <summary>
		///�������
		/// </summary>
		/// <remarks>���������: �������</remarks>
		DataSet GetRoute(int route_id);
		/// <summary>
		///�������� �� ������� ��������
		/// </summary>
		/// <remarks>���������: �������</remarks>
		DataSet GetMoveHistoryDrivers(int count, int interval, DateTime time_from, DateTime time_to, string vehicle);
		/// <summary>
		///��� ���������
		/// </summary>
		/// <remarks>���������: �������</remarks>
		DataSet GetAllStation();
		/// <summary>
		///����� � ��������
		/// </summary>
		/// <remarks>���������: �������</remarks>
		DataSet GetDataForGeoTrip();
		/// <summary>
		///����� ����� � ��������
		/// </summary>
		/// <remarks>���������: �������</remarks>
		DataSet GetTripInRoute(int Route_ID);
		/// <summary>
		///�������� ��������� � �������� �� �������������
		/// </summary>
		/// <remarks>���������: �������</remarks>
		int PointAdd(int POINT_KIND_ID, string NAME, float @X, float @Y, string ADDRESS, decimal RADIUS, string COMMENT);
		/// <summary>
		/// �������� ������� ���������
		/// </summary>
		/// <param name="Operator_ID"></param>
		/// <remarks>���������: �������</remarks>
		DataSet GetOperatorProfile(int Operator_ID);

		bool CanReprint();
		DataSet GetWaybillsByDate(DateTime dtBegin, DateTime dtEnd);

		/// <summary>
		/// ���������� ��� ����������
		/// </summary>
		/// <returns>��� ���������� (��������� �������� ����, ������� �� ����� ��������� ����������)</returns>
		string GetApplicationName();

		/// <summary>
		/// ���������� ����������� �� �� �� 
		/// </summary>
		/// <param name="vehicle_id">������������� ��</param>
		/// <returns>����������� ��, ���������� � �� (������ ����)</returns>
		byte[] GetVehiclePicture(int vehicle_id);

		/// <summary>
		/// ������ ���������� � �������� ���������
		/// </summary>
		/// <param name="vehicle_id">������������� ��, � ������� �������� ��������</param>
		/// <param name="operator_id">������������� ���������</param>
		/// <param name="event_id">������������� �������</param>
		/// <param name="time">����� ������������� �������</param>
		void LogOperatorEvent(int operator_id, int event_id, int vehicle_id, DateTime time);

		/// <summary>
		/// get stat about fuel sensors
		/// </summary>
		/// <param name="full">true - all stat, false - only extremums. default false.
		/// if(!full) next 3 pars not obligatory</param>
		/// <param name="from">begin of time interval. UTC. default - MinValue</param>
		/// <param name="to">end of time interval. UTC. default - MinValue</param>
		/// <param name="vid">VEHICLE_ID</param>
		/// <param name="iUBound">upper bound for statistics data</param>
		/// <returns>dataset with stat</returns>
		DataSet GetFuelStat(bool full, DateTime from, DateTime to, int vid, int iUBound);

		/// <summary>
		/// set fuel sensor bounds
		/// </summary>
		/// <param name="cid">CONTROLLER_ID</param>
		/// <param name="lval">lower bound for sensor, -1 - default</param>
		/// <param name="uval">upper bound for sensor, -1 - default</param>
		/// <param name="force">true - set vals, false - if wider than current</param>
        /// <param name="bDirectSensor">������/�������� ����� �������</param>
		/// <remarks>if vals default current not modified</remarks>
        void SetFuelInfo(int cid, int lval, int uval, bool force, bool bDirectSensor);

		/// <summary>
		/// delete controller stat records
		/// </summary>
		/// <param name="cid">CONTROLLER_ID</param>
		/// <param name="from">from time. MinValue - default means all</param>
		/// <param name="to">to time. MinValue - default means same as from</param>
		void DelControllerStat(int cid, DateTime from, DateTime to);

		/// <summary>
		/// ��������� ������ ������ ���������, � ��������� ���������� �������
		/// </summary>
		/// <param name="operator_id">ID ���������</param>
		/// <param name="from">������ ������� ���������</param>
		/// <param name="to">������� ������� ���������</param>
		/// <returns>DataSet �� ������� ������</returns>
		/// <remarks>���������: ��������</remarks>
		DataSet GetOperatorsSessionByDate(int operator_id, DateTime from, DateTime to);

		/// <summary>
		/// ���������� ����� �������� ��������� �� ������������ ������ ������� ��� ��������� �������
		/// </summary>
		/// <param name="HTableName">�������� H-�������</param>
        /// <param name="operator_id">ID ���������</param>
		/// <param name="from">������ ������� ���������</param>
		/// <param name="to">������� ������� ���������</param>
		/// <returns>DataSet</returns>
		/// <remarks>���������: ��������</remarks>
		DataSet GetOpearatorActions(string HTableName, int operator_id, DateTime from, DateTime to);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="operator_id"></param>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <returns></returns>
		Hashtable GetOpearatorActions(int operator_id, DateTime from, DateTime to);
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		string[] GetHTablesList();

		/// <summary>
		/// ������� ����� �������
		/// </summary>
		/// <returns></returns>
		bool DeleteMonitoreePoints(int monitoreeId, int[] pointTimes);

		/// <summary>
		/// ��������� ����� �� ������� ����������
		/// </summary>
		/// <param name="schedule_id"></param>
		/// <returns></returns>
		bool CanDeleteScheduleCheck(int schedule_id);

		/// <summary>
		/// ������ ������� ������� � ��������� ��� ���������
		/// </summary>
		/// <returns></returns>
		DataSet GetWorkstationsList();
		
		/// <summary>
		/// ��������� ������ ������� �������� �� ���������� ��� ���������� �� � ��
		/// </summary>
		/// <param name="waybill_header_id">ID ��������� ��</param>
		/// <param name="schedule_id">ID ����������</param>
		/// <returns>���������� �� �� ������, ������������ �������� ����������</returns>
		DataSet GetTimeDeviations_LR_WH_S(int waybill_header_id, int schedule_id);
		
		/// <summary>
		/// ��������� ������ ������� �������� ��� ��������� ����� �� ���� ������� � �� � ��
		/// </summary>
		/// <param name="waybill_id"></param>
		/// <param name="waybill_date_utc"></param>
		/// <returns>���������� �� �� ������, ������������ �������� ����������</returns>
		DataSet GetTimeDeviations_LR_W(int waybill_id, DateTime waybill_date_utc);
		
		/// <summary>
		/// register operator calls
		/// </summary>
		/// <param name="op">OPERATOR_ID</param>
		/// <param name="phone">phone number</param>
		/// <param name="from">tm of begin</param>
		/// <param name="to">tm of end</param>
		/// <param name="voice">voice or data</param>
		/// <param name="outcall">out or in</param>
		void RegisterCall(int op, string phone, DateTime from, DateTime to, bool voice, bool outcall);

        /// <summary>
        /// ��������� ������ ��������� ���������� ��/� �� 
        /// </summary>
        /// <param name="OperatorID">ID ���������</param>
        /// <param name="VehicleID">ID ��</param>
        /// <param name="dtFrom">����� "�"</param>
        /// <param name="dtTo">����� "��"</param>
        /// <returns>������� � �������� "MESSAGE"</returns>
        DataSet GetMessageList(int OperatorID, int VehicleID, DateTime dtFrom, DateTime dtTo);

        /// <summary>
        /// ��������� �������� ��������� ���������
        /// </summary>
        /// <returns></returns>
        DataSet GetDriverMsgTemplate();

        /// <summary>
        /// ��������� ������� �����������
        /// </summary>
        /// <param name="from">����� "�"</param>
        /// <param name="to">����� "��"</param>
        /// <returns></returns>
	    DataSet GetTaskLog(DateTime from, DateTime to);
		
		#region HASP
		/// <summary>
		/// ���������� ���������� ���������� ���������� � ����� HASP
		/// </summary>
		/// <returns></returns>
		int GetMaxOperatorsCount();
		/// <summary>
		/// ���������� ���������� ����� ���������� � ����� HASP
		/// </summary>
		/// <returns></returns>
		int GetMaxVehicleCount();
		/// <summary>
		/// ���������� ���������� ���������� � ����
		/// </summary>
		/// <returns></returns>
		int GetOperatorsCount();
		/// <summary>
		/// ���������� ���������� ����� � ����
		/// </summary>
		/// <returns></returns>
		int GetVehicleCount();

        /// <summary>
        /// ���������� ���������� ������������ ���������� � ����� HASP
        /// </summary>
        /// <returns></returns>
        int GetMaxControllerCount();

        /// <summary>
        /// ���������� ���������� ������������ � ����
        /// </summary>
        /// <returns></returns>
        int GetControllerCount();
		#endregion

        DataSet GetZonesForMap();
    }
}