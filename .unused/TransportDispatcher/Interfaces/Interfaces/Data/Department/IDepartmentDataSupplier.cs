
namespace FORIS.TSS.BusinessLogic.Interfaces.Data.Department
{
	public interface IDepartmentDataSupplier:
		IDataSupplier<DataParams>
	{

	}

	public interface IDepartmentDataServerSupplier:
		IDepartmentDataSupplier,
		IDepartmentOperationsProvider
	{
		
	}

	public interface IDepartmentOperationsProvider
	{
		
	}
}
