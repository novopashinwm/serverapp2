
namespace FORIS.TSS.BusinessLogic.Interfaces.Data.Message
{
    public interface IMessageDataSupplier : IDataSupplier<DataParams>
    {

    }

    public interface IMessageDataServerSupplier :
        IMessageDataSupplier,
        IMessageOperationsProvider
    {

    }

    public interface IMessageOperationsProvider
    {

    }
}
