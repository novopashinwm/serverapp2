﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.Interfaces.Data
{
	/// <summary> Кортеж данных результата отчета "Запросы оператора" </summary>
	[Serializable]
	public class OperatorRequestsReportResultTuple
	{
		public int       ID;
		public int       AskerID;
		public string    Asker;
		public int       TargetID;
		public string    Target;
		public DateTime  RequestDate;
		public DateTime  ResponseDate;
		public DateTime  PositionTime;
		public string    Position;
		public string    Result;
		public string    RequestSource;
		public double    Latitude;
		public double    Longitude;
		public int       Radius;
		public CmdResult ResultEnum;
	}
}