
namespace FORIS.TSS.BusinessLogic.Interfaces.Data.Calendar
{
	public interface ICalendarDataSupplier: IDataSupplier<DataParams>
	{

	}
	public interface ICalendarDataServerSupplier: ICalendarDataSupplier, ICalendarOperationsProvider
	{
		
	}
}
