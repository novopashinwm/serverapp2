using FORIS.TSS.BusinessLogic.Interfaces.Data.Vehicle;

namespace FORIS.TSS.BusinessLogic.Interfaces.Data.Rule
{
	public interface IRuleDataSupplier:
		IDataSupplier<DataParams>
	{
		IVehicleDataSupplier GetVehicleDataSupplier();
	}

	public interface IRuleDataServerSupplier:
		IRuleDataSupplier,
		IRuleOperationsProvider
	{
		
	}

	public interface IRuleOperationsProvider
	{
		
	}
}
