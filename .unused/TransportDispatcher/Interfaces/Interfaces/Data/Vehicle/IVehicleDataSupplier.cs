
namespace FORIS.TSS.BusinessLogic.Interfaces.Data.Vehicle
{
	public interface IVehicleDataSupplier:
		IDataSupplier<DataParams>
	{

	}

	public interface IVehicleDataServerSupplier:
		IVehicleDataSupplier,
		IVehicleOperationsProvider
	{
		
	}

	public interface IVehicleOperationsProvider
	{
		
	}
}
