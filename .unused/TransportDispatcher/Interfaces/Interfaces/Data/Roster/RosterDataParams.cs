using System;

namespace FORIS.TSS.BusinessLogic.Interfaces.Data.Roster
{
	[Serializable()]
	public class RosterDataParams: DataParams
	{
		private Date date;

		public Date Date
		{
			get { return this.date; }
		}

		public RosterDataParams( Date date )
		{
			this.date = date;
		}

		public override int GetHashCode()
		{
			return this.date.GetHashCode();
		}
		public override bool Equals( object obj )
		{
			return this.date.Equals( ((RosterDataParams)obj).date );
		}

		public override string ToString()
		{
			return this.Date.ToString();
		}
	}
}
