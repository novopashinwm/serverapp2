using System;
using System.Data;
using System.Runtime.Remoting.Messaging;
using FORIS.TSS.Common;

namespace FORIS.TSS.BusinessLogic.Interfaces.Data
{
	public interface IDataSupplier<TParams>: 
		IDisposable,
		IDataSupplier
		where TParams: DataParams
	{
		TParams Params { get; set; }
		
		event EventHandler ParamsChanged;
		
		bool IsReadOnly();
		
		event AnonymousEventHandler<EventArgs> ReadOnlyChanged;
	}

	public interface IDataSupplier
	{
		DataSet GetData();
		
		[ OneWay ]
		void Tick();

		event AnonymousEventHandler<DataChangedEventArgs> DataChanged;
	}

	[Serializable]
	public class DataChangedEventArgs: EventArgs
	{
		private readonly DataSet deleted;
		public DataSet Deleted
		{
			get { return this.deleted; }
		}

		private readonly DataSet added;
		public DataSet Added
		{
			get { return this.added; }
		}

		private readonly DataSet modified;
		public DataSet Modified
		{
			get { return this.modified; }
		}

		public DataChangedEventArgs( DataSet deleted, DataSet added, DataSet modified )
		{
			this.deleted = deleted;
			this.added = added;
			this.modified = modified;
		}
	}
}
