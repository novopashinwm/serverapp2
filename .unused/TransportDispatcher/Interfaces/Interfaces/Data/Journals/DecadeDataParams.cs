using System;

namespace FORIS.TSS.BusinessLogic.Interfaces.Data.Journals
{
	[Serializable]
	public class DecadeDataParams: DataParams
	{
		private int idDecade;

		public int IdDecade
		{
			get { return this.idDecade; }
		}

		private int idDecadePrev;

		public int IdDecadePrev
		{
			get { return this.idDecadePrev; }
		}

		public DecadeDataParams( int idDecade, int idDecadePrev )
		{
			this.idDecade = idDecade;
			this.idDecadePrev = idDecadePrev;
		}

		public override bool Equals( object obj )
		{
			DecadeDataParams @params = obj as DecadeDataParams;

			if( @params != null )
			{
				return
					this.idDecade == @params.idDecade &&
					this.idDecadePrev == @params.idDecadePrev;
			}

			return false;
		}
		public override int GetHashCode()
		{
			return ( this.idDecade + this.idDecadePrev ).GetHashCode();
		}

	}
}
