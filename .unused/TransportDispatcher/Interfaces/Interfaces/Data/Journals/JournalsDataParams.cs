
namespace FORIS.TSS.BusinessLogic.Interfaces.Data.Journals
{
	public class JournalsDataParams: DataParams
	{
		private readonly int department;

		public int Department
		{
			get { return this.department; }
		}

		public JournalsDataParams( int department )
		{
			this.department = department;
		}

		public override int GetHashCode()
		{
			return this.department.GetHashCode();
		}
		public override bool Equals( object obj )
		{
			return this.department.Equals( ( (JournalsDataParams)obj ).department );
		}

		public override string ToString()
		{
			return this.department.ToString();
		}
	}
}
