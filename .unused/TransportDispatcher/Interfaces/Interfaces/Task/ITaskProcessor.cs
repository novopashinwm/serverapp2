using System;
using System.Collections.Generic;
using System.Text;
using FORIS.TSS.Common;
using System.Collections;
using System.Data;
using FORIS.TSS.Terminal;

namespace FORIS.TSS.BusinessLogic
{
    public interface ITaskProcessor
    {
        /// <summary>
        /// ������� ��������� ������� �����
        /// </summary>
        event AnonymousEventHandler<EventArgs> QueueChange;

        event AnonymousEventHandler<GetSettingEventArgs> SettingsSatusChange;
        /// <summary>
        /// ������ ����������� �����
        /// </summary>
        /// <returns>������ ����������� �����</returns>
        SortedList<int, ITaskDesk> GetTaskList();

        /// <summary>
        /// ������ ������ � ���������
        /// </summary>
        /// <returns>���������� dataset �� ������� ������ ��������</returns>
        DataSet GetFWFileList();

        void AddFWTask(int VehicleID, int ControllerNumber, string FWpath);

        void RemoveFWTask(int ControllerNumber);

        void GetSettings(int VehicleID, int ControllerNumber);
    }

    [Serializable]
    public class GetSettingEventArgs : EventArgs
    {
        public GetSettingEventArgs(IUnitInfo ui, SettingsState state)
        {
            this.unitInfo = ui;
            this.state = state;
        }

        IUnitInfo unitInfo;
        SettingsState state;
        public IUnitInfo UnitInfo
        {
            get { return this.unitInfo; }
        }

        public SettingsState State
        {
            get { return this.state; }
        }

        public enum SettingsState
        {
            CantSendCommand, //������ ��������� ������� ��� ��������� ��������
            CommandSent, // ������� ����������
            SettingsReceived // �������� ���������
        }
    }

    /// <summary>
    /// ��������� ��� ���������� ����� ����������� ��������
    /// </summary>
    public interface IAutoRequestTaskProcessor
    {
        void AddTask(int VehicleID, int ControllerNumber, DateTime From, DateTime To);
        void AddTask(int? TaskID, int VehicleID, int ControllerNumber, DateTime From, DateTime To);
    }

    public interface ITaskDesk
    {
        string Desc { get;}

        string Name { get;}
    }

	public interface ITaskProcessorSettings
	{
		/// <summary>
		/// ���� � ���������� ��������
		/// </summary>
		string FirmwareDescPath
		{
			get; set;
		}

		/// <summary>
		/// ��������� ��������� ������������ �������
		/// </summary>
		bool FirmwareTaskProcessor
		{
			get;
			set;
		}
         
		/// <summary>
		/// ������������ ������ ����������� "�����", ���
		/// </summary>
		int TaskMaxSkeepSize
		{
			get;
			set;
		}

		/// <summary>
		/// ��������� ���������� ����������� � ��
		/// </summary>
		bool TaskSaveToDB
		{
			get;
			set;
		}

		/// <summary>
		/// 
		/// </summary>
		bool TaskLogBadOnly
		{
			get;
			set;
		}

		/// <summary>
		/// ���������� ������� �����������
		/// </summary>
		int TaskRetryCount
		{
			get;
			set;
		}

		/// <summary>
		/// ����� ����� ������, �� ��������� �������� ��� ����� �������, ���
		/// </summary>
		int TaskTotalLifeTime
		{
			get;
			set;
		}

		/// <summary>
		/// ������������ ��������� �������� ������������� � ���������� �������, ���
		/// </summary>
		int TaskMaxQueryTime
		{
			get;
			set;
		}

		/// <summary>
		/// ����� ����� ������ ����� ���������� ������, ���
		/// </summary>
		int TaskLifeTime
		{
			get;
			set;
		}

		/// <summary>
		/// ������� ������ �� ������, ���
		/// </summary>
		int TaskTimeOut
		{
			get;
			set;
		}

		/// <summary>
		/// max tolerable absence interval
		/// </summary>
		int PosInterval
		{
			get;
			set;
		}

		/// <summary>
		/// time length to analyze
		/// </summary>
		int PosAnalPeriod
		{
			get;
			set;
		}

		/// <summary>
		/// time between analysis. find position absence. secs
		/// </summary>
		int PosPoolInterval
		{
			get;
			set;
		}

		/// <summary>
		/// ����� ������ ������� ������������ �������� �������, ���
		/// </summary>
		int TasksStartTimeShift
		{
			get;
			set;
		}

		/// <summary>
		/// ���� ��������� TaskProcessor
		/// </summary>
		bool SwitchTaskProcessor
		{
			get;
			set;
		}

        /// <summary>
        /// ������������ ���������� ��������� ����� �� ���� ������
        /// </summary>
        int TaskMaxKilling
        {
            get;
            set;
        }


	}

    [Serializable]
    public class TaskDesc : ITaskDesk
    {
        #region ITaskDesk Members
        string desc;
        public string Desc
        {
            get { return desc; }
            set { desc = value; }
        }

        string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        #endregion
    }
}
