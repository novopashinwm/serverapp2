using System;
using System.Collections;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary>
	/// Summary description for SystemRights.
	/// </summary>
	[Serializable]
	public class SystemRights : CollectionBase
	{
		public void Add(SystemRight right)
		{
			this.InnerList.Add(right);
		}
		public SystemRight this[int index]
		{
			get
			{
				return (SystemRight)this.InnerList[index];
			}
			set
			{
				this.InnerList[index] = value;
			}
		}
		public bool Contains(SystemRight right)
		{
			return this.InnerList.IndexOf(right) >= 0;
		}
		
		/// <summary>
		/// check for right. stop on first true. left to right
		/// </summary>
		/// <param name="rights">rights to check</param>
		/// <returns>first true right or 0 if no one</returns>
		public SystemRight CheckOne(params SystemRight[] rights)
		{
			foreach(SystemRight right in rights)
				if(Contains(right)) return right;
			
			return 0;
		}
	}
}
