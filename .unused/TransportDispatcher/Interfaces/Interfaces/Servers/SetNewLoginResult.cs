﻿namespace FORIS.TSS.BusinessLogic.Interfaces.Servers
{
    /// <summary>	Результат установки нового логина	</summary>
    public enum SetNewLoginResult
    {
        /// <summary>
        /// Логин успешно изменен
        /// </summary>
        Success,
        /// <summary>
        /// Некорректное значение логина
        /// </summary>
        InvalidLogin,
        /// <summary>
        /// Такой логин уже существует
        /// </summary>
        LoginAlreadyExists
    }
}
