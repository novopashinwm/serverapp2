﻿using System;
using FORIS.TSS.BusinessLogic.Interfaces.Data;
using FORIS.TSS.Interfaces.Diagnostic;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary> Базовый интерфейс для всех типов серверов </summary>
	public interface ISessionList
	{
		IPersonalServer CreateSession(Guid clientID, string clientVersion);
		IPersonalServer CreateSession(Guid clientID, string clientVersion, string login, string password);
		/// <summary> Проверяет логин и пароль пользователя </summary>
		/// <param name="login">Логин</param>
		/// <param name="password">Пароль</param>
		CheckPasswordResult CheckPasswordForWeb(string login, string password);
	}
	public interface ISessionListAdmin : IDisposable
	{
		SessionListDataSet GetSessionList();
		ContainerListDataSet GetContainerList();
		void DeleteSession(int clientID);
		void CollectGarbage();
	}
	public interface IDiagnosticHelper
	{
		IObservedServer ObservedServer { get; }
	}
}