﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Common;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using FORIS.TSS.TransportDispatcher.RuleManager;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary> Базовый интерфейс для всех типов персональных серверов </summary>
	public interface IPersonalServer :
		ILogicForUI, IMailService, IBasicFunctionSet,
		Server.IPersonalServer // Интерфейс сессии пользователя (3.0)
	{
		/// <summary> События охранных функций </summary>
		event AnonymousEventHandler<GuardEventArgs>          GuardEvent;
		/// <summary> Событие наличия данных у терминала сервера </summary>
		event AnonymousEventHandler<ReceiveEventArgs>        ReceiveEvent;
		/// <summary> Событие наличия сообщений у терминала/ </summary>
		event AnonymousEventHandler<NotifyEventArgs>         NotifyEvent;
		/// <summary> Событие наличия данных у терминала сервера о истории движения </summary>
		event AnonymousEventHandler<ReceiveLogEventArgs>     ReceiveLogEvent;
		/// <summary> Событие срабатывания правила </summary>
		event AnonymousEventHandler<ExecRuleServerEventArgs> ReceiveMessageEvent; 
		/// <summary> Текущая команда сервера </summary>
		/// <remarks> Или, по-другому, последняя отосланная команда </remarks>
		ICommand CurrentCommand { get; }
		/// <summary> Класс, реализующий логику работы с клиентом ??? </summary>
		ILogicForUI LogicForUI { get; }
		/// <summary> Передача команды от персонального сервера менеджеру терминалов </summary>
		/// <param name="type"> type id </param>
		/// <param name="ids"> list of target id </param>
		void SendCommand(CmdType type, params int[] ids);
		/// <summary> Передача команды от персонального сервера менеджеру терминалов </summary>
		/// <param name="type"> type id </param>
		/// <param name="param"> named params </param>
		/// <param name="ids"> list of target id </param>
		void SendCommand(CmdType type, PARAMS param, params int[] ids);
		/// <summary> Передача команды от персонального сервера менеджеру терминалов </summary>
		/// <param name="type"> type id </param>
		/// <param name="mediaType"> Тип канала по которому будет послана команда </param>
		/// <param name="param"> named params </param>
		/// <param name="ids"> list of target id </param>
		void SendCommand(CmdType type, Media mediaType, PARAMS param, params int[] ids); 
		/// <summary> Установка группы мобильных объектов для отслеживания в реальном времени </summary>
		/// <param name="group"> массив идентификаторов мобильных объектов </param>
		/// <param name="bIsRefresh"> clear old cache </param>
		/// <param name="sendLastPositions"> устаревший параметр </param>
		/// <returns> список последних позиций для выбранной группы ТС </returns>
		IDictionary<int, IMobilUnit> SetMonitoreeGroup( int[] group, bool bIsRefresh, bool sendLastPositions );
		IDictionary<int, IMobilUnit> GetLastPositions(int[] iArrayGroup);
		/// <summary> Доставка новых позиций </summary>
		/// <remarks>
		/// // TODO: Комментарий. Какие данные должен возвращать этот метод
		/// Предположительно координаты всех ТС, за которыми установлено наблюдение в сессии.
		/// </remarks>
		ArrayList GetNewMonitoreeGroup();
		/// <summary> Возвращает идентификаторы ТС для указанного маршрута на текущий день </summary>
		/// <param name="route"> идентификатор маршрута </param>
		/// <returns> идентификаторы ТС для указанного маршрута на текущий день </returns>
		List<int> GetVehiclesForRoute(int route);
		/// <summary> Возвращает отсортированный по времени список последних позиций ТС из БД за указанный период времени </summary>
		/// <param name="from"> начало периода </param>
		/// <param name="to"> конец периода </param>
		/// <param name="id"> идентификатор машины </param>
		/// <param name="interval"> интервал между позициями в секундах </param>
		/// <param name="count"> количество запрашиваемых позиций </param>
		/// <returns> отсортированный по времени список последних позиций ТС </returns>
		ReceiveLogEventArgs GetLogFromDB(int from, int to, int id, int interval, int count);
		/// <summary> Получение истории движения </summary>
		SortedList GetHistory();
		/// <summary> Returns tables for calculating access from Operators to Vehicles and Routes </summary>
		DataSet GetSecurity();
		int SessionID { get; }
		int OperatorID { get; }
		string Login { get; }
		new ISessionListAdmin GetAdmin();
		ISchedulePassageService GetSchedulePassageService();
		/// <summary> Формирует отчет на диск </summary>
		/// <param name="guid"> GUID отчёта </param>
		/// <param name="reportParams"> список параметров отчёта </param>
		/// <param name="formatType"> формат файла отчёта </param>
		/// <returns> полное имя файл отчёта </returns>
		/// <remarks> Отчёты создаются в папке "FilesetUsersReaports" </remarks>
		string ReportMake(Guid guid, PARAMS reportParams, ReportTypeEnum formatType);
		/// <summary> Снятие статуса МО со стороны клиента </summary>
		/// <param name="id"> ТС, для которого сбросить статус </param>
		/// <param name="status"> Статус, который необходимо снять </param>
		void UnSetStatus(int id, Status status);
		/// <summary> Посылка SMS сообщения </summary>
		/// <param name="phone"> телефон </param>
		/// <param name="text"> текст SMS </param>
		void SendSMS(string phone, string text);
		/// <summary> Устанавливает новый пароль оператора </summary>
		/// <param name="newPassword"> новый пароль </param>
		void SetNewPassword(string newPassword);
		#region Functions for Web Points
		DataSet GetPointsWebForOperator(Guid mapGuid);
		void AddPointWebForOperator(Guid mapGuid, PointF point, string pointName, string description, int pointType);
		void EdtPointWebForOperator(/*Guid mapGuid,*/ int pointID, PointF point, string pointName, string description, int pointType);
		void DelPointWebForOperator(/*Guid mapGuid,*/ int pointID); 
		#endregion Functions for Web Points 
		#region Functions for Web Lines 
		DataSet GetLinesWebForOperator(Guid mapGuid);
		void AddLineWebForOperator(Guid mapGuid, PointF[] points, string lineName, string description);
		void EdtLineWebForOperator(/*Guid mapGuid,*/ int lineID, PointF[] points, string lineName, string description);
		void DelLineWebForOperator(/*Guid mapGuid,*/ int lineID);
		#endregion Functions for Web Lines
	}
}