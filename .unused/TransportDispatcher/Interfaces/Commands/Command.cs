using System;

namespace FORIS.TSS.Terminal
{/*
	/// <summary>
	/// ��� �������
	/// </summary>
	public enum CommandType : int
	{
		Monitor = 1,
		Trace,
		AskPosition,
		GetLog
	}
*/
	#region class Command
	/// <summary>
	/// ������� ����� ������� ��� ���� ����� ������ ��������� ��������
	/// </summary>
	/// <remarks>������ ������������ ��� �������� ������ ������� �� ������� 
	/// ���������, ���������� ��������.</remarks>
	[Serializable]
	public class Command : ICommand
	{
		/// <summary>
		/// �������������� ���������� � �������, ��������� �� ���������
		/// </summary>
		protected object tag;

		/// <summary>
		/// ����� �������
		/// </summary>
		protected string text;

		/// <summary>
		/// �������������� ���������� � �������, ��������� �� ���������
		/// </summary>
		public virtual object Tag
		{
			get { return tag; }
			set { tag = value; }
		}

		/// <summary>
		/// ����� �������
		/// </summary>
		public virtual string Text
		{
			get { return text; }
			set { text = value; }
		}

		#region .ctor
		/// <summary>
		/// �������
		/// </summary>
		public Command()
		{
			this.text = String.Empty;
			this.tag = -1;
		}

		/// <summary>
		/// �������
		/// </summary>
		public Command(string text, object tag)
		{
			this.text = text;
			this.tag = tag;
		}
		#endregion // .ctor
	}
	#endregion //class Command 
}
