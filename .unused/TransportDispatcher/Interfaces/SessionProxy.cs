﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Activation;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Lifetime;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Proxies;

namespace FORIS.TSS.BusinessLogic
{
	public class ProxyExceptionEvent
	{
		public Exception ex;
	}

	public delegate void ProxyExceptionEventHandler(object sender, ProxyExceptionEvent e);

	/// <summary>
	/// Summary description for SessionProxy.
	/// </summary>
	public class SessionProxy : RealProxy, IRemotingTypeInfo
	{
		SessionObjRef objref = null;
		IMessageSink target = null;
		string _uri = null;

		public event ProxyExceptionEventHandler ProxyExceptionEvent;
		/// <summary>
		/// </summary>
		/// <remarks>
		/// The key to extending a RealProxy is specifying the type of object 
		/// that the resulting TransparentProxy will represent. 
		/// A RealProxy-derived class accomplishes this type specification 
		/// through the protected constructor of the RealProxy class.
		/// </remarks>
		public SessionProxy(SessionObjRef objref) : base(GetInterfaceType(objref))
		{
			Trace.WriteLine("SessionProxy Constructor Called ...", "MBHOOK");
			this.objref = objref;
			target = GetTarget(objref);
		}

		/// <summary>
		/// </summary>
		/// <remarks>
		/// To participate in marshaling, override RealProxy. CreateObjRef and 
		/// provide a custom ObjRef that extends ObjRef. If you want to add 
		/// custom data to the custom ObjRef, override GetObjectData.
		/// </remarks>
		/// <param name="requestedType"></param>
		/// <returns></returns>
		public override ObjRef CreateObjRef(Type requestedType)
		{
			Trace.WriteLine("SessionProxy - create ObjRef for " + requestedType.Name, "MBHOOK");
			return this.objref;
		}

		protected IMessageSink GetTarget(ObjRef objref)
		{
			IChannel[] channels = ChannelServices.RegisteredChannels;
			foreach (object remoteChannelData in objref.ChannelInfo.ChannelData)
			{
				if (remoteChannelData == null)
				{
					continue;
				}
				foreach (IChannel chnl in channels)
				{
					if (chnl is IChannelSender)
					{
						IChannelSender chnlSender = (IChannelSender)chnl;
						IMessageSink res = chnlSender.CreateMessageSink(null, remoteChannelData, out _uri);
						if (res != null)
						{
							return res;
						}
					}
				}
			}
			return null;
		}

		public override IMessage Invoke(IMessage myMessage)
		{
			Debug.WriteLine("SessionProxy 'Invoke method' Called...", "MBHOOK");
			Debug.WriteLine("In domain " + AppDomain.CurrentDomain.FriendlyName, "MBHOOK");

			if (myMessage is IMethodCallMessage)
			{
				string methodName = (string)myMessage.Properties["__MethodName"];
				Debug.WriteLine("IMethodCallMessage '" + methodName + "'", "MBHOOK");
				if (methodName == "InitializeLifetimeService"
					|| methodName == "GetType")
				{
					Trace.WriteLine("shouldn't be catched here", "MBHOOK");
				}
			}
			if (myMessage is IMethodReturnMessage)
			{
				Debug.WriteLine("IMethodReturnMessage");
			}
			if (myMessage is IConstructionCallMessage)
			{
				// Initialize a new instance of remote object
				IConstructionReturnMessage myIConstructionReturnMessage =
					this.InitializeServerObject((IConstructionCallMessage)myMessage);
				ConstructionResponse constructionResponse = new
					ConstructionResponse(null, (IMethodCallMessage)myMessage);
				return constructionResponse;
			}
			IDictionary myIDictionary = myMessage.Properties;
			myIDictionary["__Uri"] = objref.URI;

			// Synchronously dispatch messages to server.

			try
			{
#if DEBUG
				//FORIS.TSS.Infrastructure.Diagnostics.RemotingUtilities.DumpMessageObj(myMessage);
#endif
				IMessage returnMessage = null;
				if (target != null)
				{
					returnMessage = target.SyncProcessMessage(myMessage);
				}
				else
				{
					throw new RemotingException("Target is null");
				}
#if DEBUG
				//FORIS.TSS.Infrastructure.Diagnostics.RemotingUtilities.DumpMessageObj(returnMessage);
#endif
				return returnMessage;
			}
			catch (Exception ex)
			{
				if (ProxyExceptionEvent != null)
				{
					ProxyExceptionEvent evt = new ProxyExceptionEvent();
					evt.ex = ex;
					ProxyExceptionEvent(this, evt);
				}
				// rethrow an exception (any other ideas?)
				throw;
			}
		}

		public static Type GetInterfaceType(ObjRef objref)
		{
			string tt = objref.TypeInfo.TypeName;
			if (tt.StartsWith("FORIS.TSS.ServerApplication.PersonalServer"))
			{
				return typeof(IPersonalServer);
			}
			if (tt.StartsWith("FORIS.TSS.ServerApplication.LogicForUI"))
			{
				return typeof(ILogicForUI);
			}
			return null;
		}

		public bool CanCastTo(Type fromType, object o)
		{
			Trace.WriteLine("CanCastTo " + fromType.Name, "MBHOOK");
			if (fromType == typeof(ContextBoundObject))
			{
				return false;
			}
			if (fromType == typeof(ISponsor))
			{
				return true;
			}
			string tt = this.objref.TypeInfo.TypeName;
			if (tt.StartsWith("FORIS.TSS.ServerApplication.PersonalServer"))
			{
				return true;
			}
			if (tt.StartsWith("FORIS.TSS.ServerApplication.LogicForUI"))
			{
				return true;
			}
			// TODO:  Add SessionProxy.CanCastTo implementation
			return false;
		}

		public string TypeName
		{
			get
			{
				return this.objref.TypeInfo.TypeName;
			}
			set
			{
				this.objref.TypeInfo.TypeName = value;
			}
		}
	}
}