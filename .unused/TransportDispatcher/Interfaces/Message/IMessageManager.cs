using System;
using System.Collections.Generic;
using System.Text;

namespace FORIS.TSS.TransportDispatcher.Message
{
    /// <summary>
    /// ��������� ��� ��������� ��������� (�������� ������, ��� � ��.)
    /// </summary>
    public interface IMessageManager
    {
        /// <summary>
        /// ��������� ��������� ��� �������� ������
        /// </summary>
        /// <param name="args"></param>
        void SendRule(FORIS.TSS.TransportDispatcher.RuleManager.ExecRuleServerEventArgs args);

        
        #region ��������� ������� "������ ��"

        /// <summary>
        /// ��� ������������ ��������� - �������
        /// </summary>
        /// <param name="vehicle_id"></param>
        void MessageAlarm(int vehicle_id, bool MustExecute);

        /// <summary>
        /// ��� ������������ ��������� - ��������
        /// </summary>
        /// <param name="vehicle_id"></param>
        void MessageParking(int vehicle_id, bool MustExecute);

        /// <summary>
        /// ��� ������������ ��������� - �������� ��������������� ����
        /// </summary>
        /// <param name="vehicle_id"></param>
        void MessageParkingMoving(int vehicle_id, bool MustExecute);

        #endregion ��������� ������� "������ ��"

       
        /// <summary>
        /// ��� ������������ ��������� - ���������
        /// </summary>
        /// <param name="vehicle_id"></param>
        void MessageIgnition(int vehicle_id, bool MustExecute);
         
        /// <summary>
        /// ��� ������������ ��������� - NoGps
        /// </summary>
        /// <param name="vehicle_id"></param>
        void MessageNoGps(int vehicle_id, bool MustExecute);

        /// <summary>
        /// ��� ������������ ��������� - NoGsm
        /// </summary>
        /// <param name="vehicle_id"></param>
        void MessageNoGsm(int vehicle_id, bool MustExecute);

        /// <summary>
        /// ��� ������������ ��������� - ���������� �� ��������
        /// </summary>
        /// <param name="vehicle_id"></param>
        void MessageRouteDeviation(int vehicle_id, bool MustExecute, int routeDeviation);

        /// <summary>
        /// ��� ������������ ��������� - ������� ������ ����������� �������
        /// </summary>
        /// <param name="vehicle_id"></param>
        void MessageStop(int vehicle_id, bool MustExecute);

        /// <summary>
        /// ��� ������������ ��������� - ����� �� ����
        /// </summary>
        /// <param name="vehicle_id"></param>
        void MessageZoneDeviation(int vehicle_id, bool MustExecute, int zoneID, string zoneName, int zoneDeviation);

        /// <summary>
        /// ��� ������������ ��������� - ���� �������
        /// </summary>
        /// <param name="vehicle_id"></param>
        void MessageFuelDeviation(int vehicle_id, bool MustExecute);
    }


    /// <summary>
    /// ID �������� ��� ���������
    /// </summary>
    public enum MessageTamplate
    {
        Ignition,
        NoGps,
        NoGsm,
        RouteDeviation,
        Alarm,
        Parking,
        ParkingMoving,
        Stop,
        Zone,
        Fuel
    }


 
} 
