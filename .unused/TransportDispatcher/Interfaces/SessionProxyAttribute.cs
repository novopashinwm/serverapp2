﻿using System;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Contexts;
using System.Runtime.Remoting.Proxies;
using System.Security.Permissions;

namespace FORIS.TSS.BusinessLogic
{
	[AttributeUsage(AttributeTargets.Class)]
	[SecurityPermissionAttribute(SecurityAction.Demand, Flags = SecurityPermissionFlag.Infrastructure)]
	public class SessionProxyAttribute : ProxyAttribute
	{
		public SessionProxyAttribute()
		{
		}
		public override MarshalByRefObject CreateInstance(Type serverType)
		{
			return base.CreateInstance(serverType);
		}
		public override RealProxy CreateProxy(ObjRef objRef1,
			Type serverType,
			object serverObject,
			Context serverContext
			)
		{
			SessionObjRef or = (SessionObjRef)objRef1;
			SessionProxy myCustomProxy = new SessionProxy(or);
			if (serverContext != null)
			{
				RealProxy.SetStubData(myCustomProxy, serverContext);
			}
#if DEBUG
			if ((serverType.IsMarshalByRef == false) && (serverContext == null))
			{
				throw new RemotingException("Bad Type for CreateProxy");
			}
#endif
			return myCustomProxy;
		}
	}
}