﻿using System;
using System.Diagnostics;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary>
	/// a custom ObjRef class that creates custom proxy
	/// </summary>

	public class SessionObjRef : ObjRef
	{
		/// <summary>
		/// </summary>
		/// <remarks>
		/// Only instantiate via marshaling or deserialization
		/// </remarks>
		private SessionObjRef()
		{
		}

		public SessionObjRef(MarshalByRefObject o, Type t) : base(o, t) 
		{
			Trace.WriteLine("Created SessionObjRef.", "MBHOOK");
			ORDump();
		}

		public SessionObjRef(SerializationInfo i, StreamingContext c) : base(i, c) 
		{
			Trace.WriteLine("Deserialized SessionObjRef.", "MBHOOK");
		}

		/// <summary>
		/// </summary>
		/// <remarks>
		/// After calling the base method, change the type from ObjRef to SessionObjRef
		/// </remarks>
		/// <param name="s"></param>
		/// <param name="c"></param>
		public override void GetObjectData(SerializationInfo s, StreamingContext c) 
		{
			base.GetObjectData(s, c);
			s.SetType(GetType());
			Trace.WriteLine("Serialized SessionObjRef.", "MBHOOK");
		}

		public override Object GetRealObject(StreamingContext context) 
		{

			if (IsFromThisAppDomain() || IsFromThisProcess())  
			{
				Trace.WriteLine("Returning actual object referenced by SessionObjRef.", "MBHOOK");
				return base.GetRealObject(context);
			}
			else 
			{
				Trace.WriteLine("Returning proxy to remote object.", "MBHOOK");
				//object tp = base.GetRealObject(context);
				//object tp = RemotingServices.Unmarshal(this);
				//return tp;

				// Create additional proxy instead
				SessionProxy prx = new SessionProxy(this);
				object tp2 = prx.GetTransparentProxy();
				return tp2;
			}
		}

		public void ORDump() 
		{
			Trace.WriteLine(" --- Reporting SessionObjRef Info --- ", "MBHOOK");
			Trace.WriteLine("Reference to " + TypeInfo.TypeName, "MBHOOK");
			Trace.WriteLine("URI is " + URI, "MBHOOK");

			Trace.WriteLine("Writing EnvoyInfo: ", "MBHOOK");
	  
			if ( EnvoyInfo != null) 
			{

				IMessageSink EISinks = EnvoyInfo.EnvoySinks;

				while (EISinks != null) 
				{
					Trace.WriteLine("\tSink: " + EISinks.ToString(), "MBHOOK");  
					EISinks = EISinks.NextSink;
				}
			}
			else
				Trace.WriteLine("\t {no sinks}", "MBHOOK");

			Trace.WriteLine("Writing ChannelInfo: ", "MBHOOK");
			for (int i = 0; i < ChannelInfo.ChannelData.Length; i++)
				Trace.WriteLine ("\tChannel: " + ChannelInfo.ChannelData[i].ToString(), "MBHOOK");
	  
			Trace.WriteLine(" ----------------------------- ", "MBHOOK");
		}
	}
}