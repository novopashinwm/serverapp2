﻿using System.Data;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary> Interface for mail subsystem </summary>
	public interface IMailService
	{
		/// <summary> Returns the list of all mailing events/subscribtions/newsgroups </summary>
		/// <returns></returns>
		DataSet GetEvents();
		/// <summary> Returns queue </summary>
		/// <returns></returns>
		DataSet GetQueue();
		/// <summary> Returns default xml for given plugin type </summary>
		/// <param name="schedulerevent_id"></param>
		/// <returns></returns>
		object GetDefaultPluginSettings(int schedulerevent_id);
		/// <summary> Returns subscribtions </summary>
		/// <returns>Dataset with tables EMAIL, SCHEDULEREVENT and SCHEDULEREVENT_EMAIL</returns>
		DataSet GetSubscribtions();
	}
}