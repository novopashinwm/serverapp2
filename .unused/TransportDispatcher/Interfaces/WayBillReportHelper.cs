using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using FORIS.TSS.BusinessLogic;

namespace FORIS.TSS.TransportDispatcher
{
	/// <summary>
	/// ��������� ��� �������� ������ ������� ��
	/// </summary>
	public class WayBillReportHelper
	{
		/// <summary>
		/// ��������� ��� �������� ������ ������� ��
		/// </summary>
		public WayBillReportHelper()
		{
		}

		/// <summary>
		/// TODO: ��� � ���������� ������ BuildPagedDataset
		/// </summary>
		/// <param name="dsWaybillCalculated">???</param>
		/// <param name="dateBase">???</param>
		/// <param name="ds">???</param>
		/// <returns>����� ������ ???</returns>
		public static DataSet BuildPagedDataset(DataSet dsWaybillCalculated, DateTime dateBase, DataSet ds)
		{
			DriverBonus dsDriverBonus = new DriverBonus();
			DataSet res = new DataSet();
			DataTable t = new DataTable("RAW_DATASET");
			res.Tables.Add(t);
			for (int i = 0; i <= 9; ++i)
			{
				string pad = i.ToString();
				while (pad.Length < 2) pad = "0" + pad;
				t.Columns.Add(new DataColumn("COL_" + pad, typeof(string)));
			}
			t.Columns.Add(new DataColumn("LINE_ID", typeof(int)));
			t.Columns.Add(new DataColumn("LINE_GROUP_ID", typeof(int)));
			t.Columns.Add(new DataColumn("WAYBILL_ID", typeof(int)));

			// Build list of ORD and TRIP
			Hashtable waybills = new Hashtable();
			foreach (DataRow row in dsWaybillCalculated.Tables["WAYBILL_CALCULATED"].Rows)
			{
				if (row["ORD"] == DBNull.Value) continue;
				if (row["TRIP"] == DBNull.Value) continue;
				int waybill_id = (int)row["WAYBILL_ID"];
				if (waybills.ContainsKey(waybill_id) == false)
				{
					Waybill wb = new Waybill(waybill_id, dsWaybillCalculated, ds);
					waybills.Add(waybill_id, wb);
					wb.AppendRows(t, dsWaybillCalculated, dateBase);
					wb.BuildBonusTable(dsDriverBonus, dsWaybillCalculated, dateBase);
				}
			}
			res.Merge(dsDriverBonus);
			return res;
		}

		/// <summary>
		/// TODO: ��� � ���������� ������ BuildLocalTimes
		/// </summary>
		/// <param name="dsWaybillCalculated">???</param>
		/// <param name="baseDate">???</param>
		public static void BuildLocalTimes(DataSet dsWaybillCalculated, DateTime baseDate)
		{
			foreach (DataRow row in dsWaybillCalculated.Tables["WAYBILL_CALCULATED"].Rows)
			{
				int time_in = row["TIME_IN"] != DBNull.Value? (int)row["TIME_IN"]: 0;
				int time_out = row["TIME_OUT"] != DBNull.Value? (int)row["TIME_OUT"]: 0;
				int log_time_in = (int)row["LOG_TIME_IN"];
				int log_time_out = (int)row["LOG_TIME_OUT"];
				DateTime local_time_in = (baseDate + new TimeSpan(0, 0, time_in));
				DateTime local_time_out = (baseDate + new TimeSpan(0, 0, time_out));
				//				DateTime local_actual_in = (baseDate + new TimeSpan(0, 0, log_time_in));
				//				DateTime local_actual_out = (baseDate + new TimeSpan(0, 0, log_time_out));
				string tin = "--:--";
				if (time_in != 0)
				{
					tin = local_time_in.ToString("HH:mm");
				}
				string tout = "--:--";
				if (time_out != 0)
				{
					tout = local_time_out.ToString("HH:mm");
				}
				string delta1 = " ";
				if (log_time_in != 0)
				{
					delta1 = ((int)Math.Round((log_time_in - time_in) / 60.0)).ToString();
					if (log_time_in - time_in > 0)
					{
						delta1 = "+" + delta1;
					}
				}
				string delta2 = " ";
				if (log_time_out != 0)
				{
					delta2 = ((int)Math.Round((log_time_out - time_out) / 60.0)).ToString();
					if (log_time_out - time_out > 0)
					{
						delta2 = "+" + delta2;
					}
				}
				row["VALUE"] = string.Format("{0} ({1}) {2} ({3})", tin, delta1, tout, delta2);
			}
		}
	}

	/// <summary>
	/// TODO: �������� ��������� Cell
	/// </summary>
	public struct Cell
	{
		public int ord;
		public int trip;
		public DataRow rowSource;
		public Cell(int trip, int ord, DataRow rowSource)
		{
			this.ord = ord;
			this.trip = trip;
			this.rowSource = rowSource;
		}
		public int WaybillID
		{
			get
			{
				return (int)rowSource["WAYBILL_ID"];
			}
		}
	}

	/// <summary>
	/// TODO: �������� ������ TripStats
	/// </summary>
	public class TripStats
	{
		public int Index;
		public string Name;
		/// <summary>
		/// maximal negative deviation
		/// </summary>
		public int max_minus; 
		/// <summary>
		/// maximal positive deviation
		/// </summary>
		public int max_plus;
		/// <summary>
		/// time of trip in seconds
		/// </summary>
		public int time; 
		/// <summary>
		/// bonus in percents
		/// </summary>
		public int bonus;
		/// <summary>
		/// first point of trip
		/// </summary>
		public int min_time;
		/// <summary>
		/// last point of trip
		/// </summary>
		public int max_time;
		/// <summary>
		/// time of first point of next trip, if trip exists
		/// </summary>
		public int next_trip_start;
	}

	/// <summary>
	/// TODO: �������� ��������� Waybill1
	/// </summary>
	public struct Waybill
	{
		public int Waybill_ID;
		public SortedList listOrd;
		public SortedList listTrip;
		public DataSet ds;
		public Waybill(int waybill_id, DataSet dsWaybillCalculated, DataSet ds)
		{
			listOrd = new SortedList();
			listTrip = new SortedList();
			this.ds = ds;
			this.Waybill_ID = waybill_id;
			// Populate table from dataset
			foreach (DataRow row in dsWaybillCalculated.Tables["WAYBILL_CALCULATED"].Rows)
			{
				// only rows for this waybill
				if (waybill_id != (int)row["WAYBILL_ID"]) continue;
				// skip invalid data
				if (row["ORD"] == DBNull.Value) continue;
				if (row["TRIP"] == DBNull.Value) continue;
				// get numbers
				int ord = (int)row["ORD"];
				int trip = (int)(Int16)row["TRIP"];
				if (listOrd.ContainsKey(ord) == false)
				{
					listOrd.Add(ord, ord);
				}
				if (listTrip.ContainsKey(trip) == false)
				{
					TripStats ts = new TripStats();
					ts.Name = row["TRIP_NAME"].ToString();
					listTrip.Add(trip, ts);
				}
			}
		}
		public void AppendRows(DataTable t, DataSet dsWaybillCalculated, DateTime dateBase)
		{
			int nRowsPerPoint = 3;
			int start = t.Rows.Count;
			int nCountBand = ((listTrip.Count + 8) / 9);
			int nCountOrds = listOrd.Count;
			int nCount = nCountBand * (2 + nCountOrds * nRowsPerPoint);
			// Build table for ORD/TRIP
			for (int ord = 0; ord < nCount; ++ord)
			{
				DataRow row = t.NewRow();
				row["WAYBILL_ID"] = this.Waybill_ID;
				t.Rows.Add(row);
			}
			foreach (DataRow row in dsWaybillCalculated.Tables["WAYBILL_CALCULATED"].Rows)
			{
				// only rows for this waybill
				if (Waybill_ID != (int)row["WAYBILL_ID"]) continue;
				// skip invalid data
				if (row["ORD"] == DBNull.Value) continue;
				if (row["TRIP"] == DBNull.Value) continue;
				// get numbers
				int ord = (int)row["ORD"]; 
				ord = listOrd.IndexOfKey(ord);
				int trip = (int)(Int16)row["TRIP"];
				trip = listTrip.IndexOfKey(trip);

				int xpos = 1 + trip % 9;
				int ypos = start + (2 + nRowsPerPoint * nCountOrds) * (trip / 9);
				t.Rows[ypos][xpos] = ((TripStats)listTrip.GetByIndex(trip)).Name.ToString();
				t.Rows[ypos + 0][0] = "� �����";
				t.Rows[ypos + nRowsPerPoint * ord + 1][0] = row["POINT_NAME"].ToString();
				//				string time_in = (TimeHelper.GetDateTimeUTCFromBase(dateBase, (int)row["TIME_IN"])).ToString("HH:mm");
				//				string time_out = (TimeHelper.GetDateTimeUTCFromBase(dateBase, (int)row["TIME_OUT"])).ToString("HH:mm");
				string time_in = row["TIME_IN"] != DBNull.Value?
					TimeHelper.GetDateTimeUTCFromBase(
					dateBase, (int)row["TIME_IN"]).ToString("HH:mm"): "--:--";
				string time_out = row["TIME_OUT"] != DBNull.Value? 
					TimeHelper.GetDateTimeUTCFromBase(
					dateBase, (int)row["TIME_OUT"]).ToString("HH:mm"): "--:--";
				t.Rows[ypos + nRowsPerPoint * ord + 2][xpos] = time_in + " " + time_out;
				t.Rows[ypos + nRowsPerPoint * ord + 2][0] = "����� �� ����.";

				if ((int)row["LOG_TIME_IN"] == 0 && (int)row["LOG_TIME_OUT"] == 0)
				{
					t.Rows[ypos + nRowsPerPoint * ord + 3][xpos] = "��� ������";
				}
				else
				{
					//int delta1 = (int)row["LOG_TIME_IN"]- (int)row["TIME_IN"];
					//string log_time_in = ((int)Math.Round((delta1) / 60.0)).ToString();
					int delta1 = (int)row["time_deviation_in"];
					string log_time_in = delta1.ToString();
					if (delta1 > 0) log_time_in = "+" + log_time_in;
					if (delta1 == 0) log_time_in = " " + log_time_in;
					
					//					int delta2 = (int)row["LOG_TIME_OUT"] - (int)row["TIME_OUT"];
					//					string log_time_out = ((int)Math.Round((delta2) / 60.0)).ToString();
					int delta2 = (int)row["time_deviation_out"];
					string log_time_out = delta2.ToString();
					if (delta2 > 0) log_time_out = "+" + log_time_out;
					if (delta2 == 0) log_time_out = " " + log_time_out;

					t.Rows[ypos + nRowsPerPoint * ord + 3][xpos] = log_time_in + "    " + log_time_out;
				}
				t.Rows[ypos + nRowsPerPoint * ord + 3][0] = "����������";
			}
		}


		
		public void BuildBonusTable(DriverBonus dsDriverBonus, DataSet dsWaybillCalculated, DateTime baseDate)
		{
			for (int trip_index = 0; trip_index < this.listTrip.Count; ++trip_index)
			{
				TripStats ts = (TripStats)this.listTrip.GetByIndex(trip_index);
				ts.Index = trip_index;
				foreach (DataRow row in dsWaybillCalculated.Tables["WAYBILL_CALCULATED"].Rows)
				{
					// only rows for this waybill
					if (Waybill_ID != (int)row["WAYBILL_ID"]) continue;
					// skip invalid data
					if (row["ORD"] == DBNull.Value) continue;
					if (row["TRIP"] == DBNull.Value) continue;
					// get numbers
					int trip = (int)(Int16)row["TRIP"];
					trip = listTrip.IndexOfKey(trip);
					if (trip != trip_index) continue; // skip cycle
					int ord = (int)row["ORD"]; 
					ord = listOrd.IndexOfKey(ord);

					if (row["TIME_IN"] != DBNull.Value && 
						((int)row["TIME_IN"] < ts.min_time || ts.min_time == 0))
					{
						ts.min_time = (int)row["TIME_IN"];
					}
					if (row["TIME_OUT"] != DBNull.Value && 
						((int)row["TIME_OUT"] < ts.min_time || ts.min_time == 0))
					{
						ts.min_time = (int)row["TIME_OUT"];
					}

					if (row["TIME_IN"] != DBNull.Value && (int)row["TIME_IN"] > ts.max_time)
					{
						ts.max_time = (int)row["TIME_IN"];
					}
					if (row["TIME_OUT"] != DBNull.Value && (int)row["TIME_OUT"] > ts.max_time)
					{
						ts.max_time = (int)row["TIME_OUT"];
					}
					
					if (trip_index > 0)
					{
						TripStats ts_prev = (TripStats)this.listTrip.GetByIndex(trip_index - 1);
						if (row["TIME_IN"] != DBNull.Value && 
							((int)row["TIME_IN"] < ts_prev.next_trip_start || 
							ts_prev.next_trip_start == 0))
						{
							ts_prev.next_trip_start = (int)row["TIME_IN"];
						}
						if (row["TIME_OUT"] != DBNull.Value && 
							((int)row["TIME_OUT"] < ts_prev.next_trip_start || 
							ts_prev.next_trip_start == 0))
						{
							ts_prev.next_trip_start = (int)row["TIME_OUT"];
						}
					}

					if ((int)row["LOG_TIME_IN"] == 0 || (int)row["LOG_TIME_OUT"] == 0)
					{
					}
					else
					{
						// int delta1 = (int)row["LOG_TIME_IN"]- (int)row["TIME_IN"];
						// string log_time_in = ((int)Math.Round((delta1) / 60.0)).ToString();
						int delta1 = (int)row["time_deviation_in"];
						string log_time_in = delta1.ToString();
						if (delta1 > 0) log_time_in = "+" + log_time_in;
						if (delta1 == 0) log_time_in = " " + log_time_in;
					
						// int delta2 = (int)row["LOG_TIME_OUT"] - (int)row["TIME_OUT"];
						// string log_time_out = ((int)Math.Round((delta2) / 60.0)).ToString();
						int delta2 = (int)row["time_deviation_out"];
						string log_time_out = delta2.ToString();
						if (delta2 > 0) log_time_out = "+" + log_time_out;
						if (delta2 == 0) log_time_out = " " + log_time_out;

						if (delta1 < ts.max_minus)
						{
							ts.max_minus = delta1;
						}
						if (delta1 > ts.max_plus)
						{
							ts.max_plus = delta1;
						}
						if (delta2 < ts.max_minus)
						{
							ts.max_minus = delta2;
						}
						if (delta2 > ts.max_plus)
						{
							ts.max_plus = delta2;
						}
					}
				}
				Debug.WriteLine("range " + ts.max_minus + " " + ts.max_plus);
				ts.max_minus *= 60;
				ts.max_plus *= 60;
				DataView dv = new DataView(this.ds.Tables["DRIVER_BONUS"], "", "time_delta desc", DataViewRowState.CurrentRows);
				int max = Math.Max(Math.Abs(ts.max_minus), Math.Abs(ts.max_plus));
				foreach (DataRowView view in dv)
				{
					DataRow rowPercent = view.Row;
					int td = (int)rowPercent["time_delta"];
					Debug.WriteLine(td);
					if (max <= Math.Abs(td))
					{
						ts.bonus = (int)rowPercent["bonus_percentage"];
						Debug.WriteLine("bonus " + ts.bonus);
					}
					if (max <= Math.Abs(td))
					{
						ts.bonus = (int)rowPercent["bonus_percentage"];
						Debug.WriteLine("bonus " + ts.bonus);
					}
				}
			}
			int total_time = 0;
			double percent = 0;
			for (int trip_index = 0; trip_index < listTrip.Count; ++trip_index)
			{
				TripStats ts = (TripStats)listTrip.GetByIndex(trip_index);
				int trip_time = ts.max_time - ts.min_time;
				if (trip_index + 1 != this.listTrip.Count)
				{
					trip_time += ts.next_trip_start - ts.max_time;
				}
				ts.time = trip_time;
				total_time += trip_time;
				percent += ts.bonus * ts.time;
				dsDriverBonus.DRIVER_TRIP.Rows.Add(new object[]
					{
						this.Waybill_ID, 
						ts.Index.ToString(),
						ts.Name,
						(TimeHelper.Round(ts.max_minus)).ToString(),
						TimeHelper.Round(ts.max_plus).ToString(),
						ts.bonus,
						TimeHelper.Round(ts.time)
					});
			}
			string percentString = String.Format("{0:##.##}", percent / total_time);

			dsDriverBonus.DRIVER_SUMMARY.Rows.Add(new object[]{this.Waybill_ID, TimeHelper.Round(total_time), percentString});
		}
	}
}
