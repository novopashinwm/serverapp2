using System;
using System.Diagnostics;
using System.Collections;
using System.Data;

namespace FORIS.TSS.BusinessLogic
{
    ///<summary>
    /// Class for creating table schema on client
    ///</summary>
    public class DatasetHelper : DatasetHelperBase
    {
        ///<summary>
        /// [BLADING]
        ///</summary>
        public static void CascadeRemove_Blading(DataSet dataset, System.Int32 BladingId)
        {
            Debug.Assert(dataset.Tables.Contains("BLADING"));
            DataRow row = dataset.Tables["BLADING"].Rows.Find(BladingId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [BLADING]
        ///</summary>
        public static void Compact_Blading(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("BLADING"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["BLADING"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [BLADING]
        ///</summary>
        public static void CompactFreshRecords_Blading(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("BLADING"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["BLADING"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["BLADING"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [BLADING_TYPE]
        ///</summary>
        public static void CascadeRemove_BladingType(DataSet dataset, System.Int32 BladingTypeId)
        {
            Debug.Assert(dataset.Tables.Contains("BLADING_TYPE"));
            DataRow row = dataset.Tables["BLADING_TYPE"].Rows.Find(BladingTypeId);
            if( row != null){
                DataRow[] rowsFK_BLADING_BLADING_TYPE = row.GetChildRows("FK_BLADING_BLADING_TYPE");
                if( rowsFK_BLADING_BLADING_TYPE != null && rowsFK_BLADING_BLADING_TYPE.Length > 0 ){
                    foreach (DataRow rowFK_BLADING_BLADING_TYPE in rowsFK_BLADING_BLADING_TYPE)
                    {
                        System.Int32 childBladingId = (System.Int32)rowFK_BLADING_BLADING_TYPE["BLADING_ID"];
                        CascadeRemove_Blading(dataset, childBladingId);
                    }
                }
                DataRow[] rowsFK_LOGISTIC_ORDER_BLADING_TYPE = row.GetChildRows("FK_LOGISTIC_ORDER_BLADING_TYPE");
                if( rowsFK_LOGISTIC_ORDER_BLADING_TYPE != null && rowsFK_LOGISTIC_ORDER_BLADING_TYPE.Length > 0 ){
                    foreach (DataRow rowFK_LOGISTIC_ORDER_BLADING_TYPE in rowsFK_LOGISTIC_ORDER_BLADING_TYPE)
                    {
                        System.Int32 childLogisticOrderId = (System.Int32)rowFK_LOGISTIC_ORDER_BLADING_TYPE["LOGISTIC_ORDER_ID"];
                        CascadeRemove_LogisticOrder(dataset, childLogisticOrderId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [BLADING_TYPE]
        ///</summary>
        public static void Compact_BladingType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("BLADING_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["BLADING_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [BLADING_TYPE]
        ///</summary>
        public static void CompactFreshRecords_BladingType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("BLADING_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["BLADING_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["BLADING_TYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [BRIGADE]
        ///</summary>
        public static void CascadeRemove_Brigade(DataSet dataset, System.Int32 BrigadeId)
        {
            Debug.Assert(dataset.Tables.Contains("BRIGADE"));
            DataRow row = dataset.Tables["BRIGADE"].Rows.Find(BrigadeId);
            if( row != null){
                DataRow[] rowsFK_BRIGADE_DRIVER_BRIGADE = row.GetChildRows("FK_BRIGADE_DRIVER_BRIGADE");
                if( rowsFK_BRIGADE_DRIVER_BRIGADE != null && rowsFK_BRIGADE_DRIVER_BRIGADE.Length > 0 ){
                    foreach (DataRow rowFK_BRIGADE_DRIVER_BRIGADE in rowsFK_BRIGADE_DRIVER_BRIGADE)
                    {
                        System.Int32 childBrigadeDriverId = (System.Int32)rowFK_BRIGADE_DRIVER_BRIGADE["BRIGADE_DRIVER_ID"];
                        CascadeRemove_BrigadeDriver(dataset, childBrigadeDriverId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [BRIGADE]
        ///</summary>
        public static void Compact_Brigade(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("BRIGADE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["BRIGADE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [BRIGADE]
        ///</summary>
        public static void CompactFreshRecords_Brigade(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("BRIGADE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["BRIGADE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["BRIGADE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [BRIGADE_DRIVER]
        ///</summary>
        public static void CascadeRemove_BrigadeDriver(DataSet dataset, System.Int32 BrigadeDriverId)
        {
            Debug.Assert(dataset.Tables.Contains("BRIGADE_DRIVER"));
            DataRow row = dataset.Tables["BRIGADE_DRIVER"].Rows.Find(BrigadeDriverId);
            if( row != null){
                DataRow[] rowsFK_JOURNAL_DAY_BRIGADE_DRIVER = row.GetChildRows("FK_JOURNAL_DAY_BRIGADE_DRIVER");
                if( rowsFK_JOURNAL_DAY_BRIGADE_DRIVER != null && rowsFK_JOURNAL_DAY_BRIGADE_DRIVER.Length > 0 ){
                    foreach (DataRow rowFK_JOURNAL_DAY_BRIGADE_DRIVER in rowsFK_JOURNAL_DAY_BRIGADE_DRIVER)
                    {
                        System.Int32 childJournalDayId = (System.Int32)rowFK_JOURNAL_DAY_BRIGADE_DRIVER["JOURNAL_DAY_ID"];
                        CascadeRemove_JournalDay(dataset, childJournalDayId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [BRIGADE_DRIVER]
        ///</summary>
        public static void Compact_BrigadeDriver(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("BRIGADE_DRIVER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["BRIGADE_DRIVER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [BRIGADE_DRIVER]
        ///</summary>
        public static void CompactFreshRecords_BrigadeDriver(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("BRIGADE_DRIVER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["BRIGADE_DRIVER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["BRIGADE_DRIVER"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [BUSSTOP]
        ///</summary>
        public static void CascadeRemove_Busstop(DataSet dataset, System.Int32 BusstopId)
        {
            Debug.Assert(dataset.Tables.Contains("BUSSTOP"));
            DataRow row = dataset.Tables["BUSSTOP"].Rows.Find(BusstopId);
            if( row != null){
                DataRow[] rowsPoint_Busstop = row.GetChildRows("Point_Busstop");
                if( rowsPoint_Busstop != null && rowsPoint_Busstop.Length > 0 ){
                    foreach (DataRow rowPoint_Busstop in rowsPoint_Busstop)
                    {
                        System.Int32 childPointId = (System.Int32)rowPoint_Busstop["POINT_ID"];
                        CascadeRemove_Point(dataset, childPointId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [BUSSTOP]
        ///</summary>
        public static void Compact_Busstop(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("BUSSTOP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["BUSSTOP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [BUSSTOP]
        ///</summary>
        public static void CompactFreshRecords_Busstop(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("BUSSTOP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["BUSSTOP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["BUSSTOP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CAL]
        ///</summary>
        public static void CascadeRemove_Cal(DataSet dataset, System.DateTime Date)
        {
            Debug.Assert(dataset.Tables.Contains("CAL"));
            DataRow row = dataset.Tables["CAL"].Rows.Find(Date);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CAL]
        ///</summary>
        public static void Compact_Cal(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CAL"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CAL"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CAL]
        ///</summary>
        public static void CompactFreshRecords_Cal(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CAL"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CAL"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CAL"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CALENDAR_DAY]
        ///</summary>
        public static void CascadeRemove_CalendarDay(DataSet dataset, System.Int32 CalendarDayId)
        {
            Debug.Assert(dataset.Tables.Contains("CALENDAR_DAY"));
            DataRow row = dataset.Tables["CALENDAR_DAY"].Rows.Find(CalendarDayId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CALENDAR_DAY]
        ///</summary>
        public static void Compact_CalendarDay(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CALENDAR_DAY"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CALENDAR_DAY"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CALENDAR_DAY]
        ///</summary>
        public static void CompactFreshRecords_CalendarDay(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CALENDAR_DAY"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CALENDAR_DAY"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CALENDAR_DAY"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CONSTANTS]
        ///</summary>
        public static void CascadeRemove_Constants(DataSet dataset, System.String Name)
        {
            Debug.Assert(dataset.Tables.Contains("CONSTANTS"));
            DataRow row = dataset.Tables["CONSTANTS"].Rows.Find(Name);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CONSTANTS]
        ///</summary>
        public static void Compact_Constants(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONSTANTS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONSTANTS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CONSTANTS]
        ///</summary>
        public static void CompactFreshRecords_Constants(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONSTANTS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONSTANTS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CONSTANTS"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CONTRACTOR]
        ///</summary>
        public static void CascadeRemove_Contractor(DataSet dataset, System.Int32 ContractorId)
        {
            Debug.Assert(dataset.Tables.Contains("CONTRACTOR"));
            DataRow row = dataset.Tables["CONTRACTOR"].Rows.Find(ContractorId);
            if( row != null){
                DataRow[] rowsFK_BLADING_CONTRACTOR = row.GetChildRows("FK_BLADING_CONTRACTOR");
                if( rowsFK_BLADING_CONTRACTOR != null && rowsFK_BLADING_CONTRACTOR.Length > 0 ){
                    foreach (DataRow rowFK_BLADING_CONTRACTOR in rowsFK_BLADING_CONTRACTOR)
                    {
                        System.Int32 childBladingId = (System.Int32)rowFK_BLADING_CONTRACTOR["BLADING_ID"];
                        CascadeRemove_Blading(dataset, childBladingId);
                    }
                }
                DataRow[] rowsFK_LOGISTIC_ADDRESS_CONTRACTOR = row.GetChildRows("FK_LOGISTIC_ADDRESS_CONTRACTOR");
                if( rowsFK_LOGISTIC_ADDRESS_CONTRACTOR != null && rowsFK_LOGISTIC_ADDRESS_CONTRACTOR.Length > 0 ){
                    foreach (DataRow rowFK_LOGISTIC_ADDRESS_CONTRACTOR in rowsFK_LOGISTIC_ADDRESS_CONTRACTOR)
                    {
                        System.Int32 childLogisticAddressId = (System.Int32)rowFK_LOGISTIC_ADDRESS_CONTRACTOR["LOGISTIC_ADDRESS_ID"];
                        CascadeRemove_LogisticAddress(dataset, childLogisticAddressId);
                    }
                }
                DataRow[] rowsFK_LOGISTIC_ORDER_CONTRACTOR = row.GetChildRows("FK_LOGISTIC_ORDER_CONTRACTOR");
                if( rowsFK_LOGISTIC_ORDER_CONTRACTOR != null && rowsFK_LOGISTIC_ORDER_CONTRACTOR.Length > 0 ){
                    foreach (DataRow rowFK_LOGISTIC_ORDER_CONTRACTOR in rowsFK_LOGISTIC_ORDER_CONTRACTOR)
                    {
                        System.Int32 childLogisticOrderId = (System.Int32)rowFK_LOGISTIC_ORDER_CONTRACTOR["LOGISTIC_ORDER_ID"];
                        CascadeRemove_LogisticOrder(dataset, childLogisticOrderId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTRACTOR]
        ///</summary>
        public static void Compact_Contractor(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTRACTOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTRACTOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTRACTOR]
        ///</summary>
        public static void CompactFreshRecords_Contractor(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTRACTOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTRACTOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CONTRACTOR"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CONTRACTOR_CALENDAR]
        ///</summary>
        public static void CascadeRemove_ContractorCalendar(DataSet dataset, System.Int32 ContractorCalendarId)
        {
            Debug.Assert(dataset.Tables.Contains("CONTRACTOR_CALENDAR"));
            DataRow row = dataset.Tables["CONTRACTOR_CALENDAR"].Rows.Find(ContractorCalendarId);
            if( row != null){
                DataRow[] rowsFK_LOGISTIC_ADDRESS_CONTRACTOR_CALENDAR = row.GetChildRows("FK_LOGISTIC_ADDRESS_CONTRACTOR_CALENDAR");
                if( rowsFK_LOGISTIC_ADDRESS_CONTRACTOR_CALENDAR != null && rowsFK_LOGISTIC_ADDRESS_CONTRACTOR_CALENDAR.Length > 0 ){
                    foreach (DataRow rowFK_LOGISTIC_ADDRESS_CONTRACTOR_CALENDAR in rowsFK_LOGISTIC_ADDRESS_CONTRACTOR_CALENDAR)
                    {
                        System.Int32 childLogisticAddressId = (System.Int32)rowFK_LOGISTIC_ADDRESS_CONTRACTOR_CALENDAR["LOGISTIC_ADDRESS_ID"];
                        CascadeRemove_LogisticAddress(dataset, childLogisticAddressId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTRACTOR_CALENDAR]
        ///</summary>
        public static void Compact_ContractorCalendar(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTRACTOR_CALENDAR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTRACTOR_CALENDAR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTRACTOR_CALENDAR]
        ///</summary>
        public static void CompactFreshRecords_ContractorCalendar(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTRACTOR_CALENDAR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTRACTOR_CALENDAR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CONTRACTOR_CALENDAR"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CONTROLLER]
        ///</summary>
        public static void CascadeRemove_Controller(DataSet dataset, System.Int32 ControllerId)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER"));
            DataRow row = dataset.Tables["CONTROLLER"].Rows.Find(ControllerId);
            if( row != null){
                DataRow[] rowsControllerTime_Controller = row.GetChildRows("ControllerTime_Controller");
                if( rowsControllerTime_Controller != null && rowsControllerTime_Controller.Length > 0 ){
                    foreach (DataRow rowControllerTime_Controller in rowsControllerTime_Controller)
                    {
                        System.Int32 childControllerTimeId = (System.Int32)rowControllerTime_Controller["CONTROLLER_TIME_ID"];
                        CascadeRemove_ControllerTime(dataset, childControllerTimeId);
                    }
                }
                DataRow[] rowsFK_CONTROLLER_INFO_CONTROLLER = row.GetChildRows("FK_CONTROLLER_INFO_CONTROLLER");
                if( rowsFK_CONTROLLER_INFO_CONTROLLER != null && rowsFK_CONTROLLER_INFO_CONTROLLER.Length > 0 ){
                    foreach (DataRow rowFK_CONTROLLER_INFO_CONTROLLER in rowsFK_CONTROLLER_INFO_CONTROLLER)
                    {
                        System.Int32 childControllerId = (System.Int32)rowFK_CONTROLLER_INFO_CONTROLLER["CONTROLLER_ID"];
                        CascadeRemove_ControllerInfo(dataset, childControllerId);
                    }
                }
                DataRow[] rowsFK_ControllerSensorMap_Controller = row.GetChildRows("FK_ControllerSensorMap_Controller");
                if( rowsFK_ControllerSensorMap_Controller != null && rowsFK_ControllerSensorMap_Controller.Length > 0 ){
                    foreach (DataRow rowFK_ControllerSensorMap_Controller in rowsFK_ControllerSensorMap_Controller)
                    {
                        System.Int32 childControllerSensorMapId = (System.Int32)rowFK_ControllerSensorMap_Controller["CONTROLLER_SENSOR_MAP_ID"];
                        CascadeRemove_ControllerSensorMap(dataset, childControllerSensorMapId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER]
        ///</summary>
        public static void Compact_Controller(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER]
        ///</summary>
        public static void CompactFreshRecords_Controller(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CONTROLLER"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CONTROLLER_INFO]
        ///</summary>
        public static void CascadeRemove_ControllerInfo(DataSet dataset, System.Int32 ControllerId)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_INFO"));
            DataRow row = dataset.Tables["CONTROLLER_INFO"].Rows.Find(ControllerId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_INFO]
        ///</summary>
        public static void Compact_ControllerInfo(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_INFO"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_INFO"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_INFO]
        ///</summary>
        public static void CompactFreshRecords_ControllerInfo(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_INFO"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_INFO"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CONTROLLER_INFO"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR]
        ///</summary>
        public static void CascadeRemove_ControllerSensor(DataSet dataset, System.Int32 ControllerSensorId)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR"));
            DataRow row = dataset.Tables["CONTROLLER_SENSOR"].Rows.Find(ControllerSensorId);
            if( row != null){
                DataRow[] rowsFK_ControllerSensorMap_ControllerSensor = row.GetChildRows("FK_ControllerSensorMap_ControllerSensor");
                if( rowsFK_ControllerSensorMap_ControllerSensor != null && rowsFK_ControllerSensorMap_ControllerSensor.Length > 0 ){
                    foreach (DataRow rowFK_ControllerSensorMap_ControllerSensor in rowsFK_ControllerSensorMap_ControllerSensor)
                    {
                        System.Int32 childControllerSensorMapId = (System.Int32)rowFK_ControllerSensorMap_ControllerSensor["CONTROLLER_SENSOR_MAP_ID"];
                        CascadeRemove_ControllerSensorMap(dataset, childControllerSensorMapId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR]
        ///</summary>
        public static void Compact_ControllerSensor(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_SENSOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR]
        ///</summary>
        public static void CompactFreshRecords_ControllerSensor(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_SENSOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CONTROLLER_SENSOR"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_LEGEND]
        ///</summary>
        public static void CascadeRemove_ControllerSensorLegend(DataSet dataset, System.Int32 ControllerSensorLegendId)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR_LEGEND"));
            DataRow row = dataset.Tables["CONTROLLER_SENSOR_LEGEND"].Rows.Find(ControllerSensorLegendId);
            if( row != null){
                DataRow[] rowsFK_ControllerSensorMap_ControllerSensorLegend = row.GetChildRows("FK_ControllerSensorMap_ControllerSensorLegend");
                if( rowsFK_ControllerSensorMap_ControllerSensorLegend != null && rowsFK_ControllerSensorMap_ControllerSensorLegend.Length > 0 ){
                    foreach (DataRow rowFK_ControllerSensorMap_ControllerSensorLegend in rowsFK_ControllerSensorMap_ControllerSensorLegend)
                    {
                        System.Int32 childControllerSensorMapId = (System.Int32)rowFK_ControllerSensorMap_ControllerSensorLegend["CONTROLLER_SENSOR_MAP_ID"];
                        CascadeRemove_ControllerSensorMap(dataset, childControllerSensorMapId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_LEGEND]
        ///</summary>
        public static void Compact_ControllerSensorLegend(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR_LEGEND"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_SENSOR_LEGEND"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_LEGEND]
        ///</summary>
        public static void CompactFreshRecords_ControllerSensorLegend(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR_LEGEND"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_SENSOR_LEGEND"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CONTROLLER_SENSOR_LEGEND"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_MAP]
        ///</summary>
        public static void CascadeRemove_ControllerSensorMap(DataSet dataset, System.Int32 ControllerSensorMapId)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR_MAP"));
            DataRow row = dataset.Tables["CONTROLLER_SENSOR_MAP"].Rows.Find(ControllerSensorMapId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_MAP]
        ///</summary>
        public static void Compact_ControllerSensorMap(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR_MAP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_SENSOR_MAP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_MAP]
        ///</summary>
        public static void CompactFreshRecords_ControllerSensorMap(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR_MAP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_SENSOR_MAP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CONTROLLER_SENSOR_MAP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_TYPE]
        ///</summary>
        public static void CascadeRemove_ControllerSensorType(DataSet dataset, System.Int32 ControllerSensorTypeId)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR_TYPE"));
            DataRow row = dataset.Tables["CONTROLLER_SENSOR_TYPE"].Rows.Find(ControllerSensorTypeId);
            if( row != null){
                DataRow[] rowsFK_ControllerSensor_ControllerSensorType = row.GetChildRows("FK_ControllerSensor_ControllerSensorType");
                if( rowsFK_ControllerSensor_ControllerSensorType != null && rowsFK_ControllerSensor_ControllerSensorType.Length > 0 ){
                    foreach (DataRow rowFK_ControllerSensor_ControllerSensorType in rowsFK_ControllerSensor_ControllerSensorType)
                    {
                        System.Int32 childControllerSensorId = (System.Int32)rowFK_ControllerSensor_ControllerSensorType["CONTROLLER_SENSOR_ID"];
                        CascadeRemove_ControllerSensor(dataset, childControllerSensorId);
                    }
                }
                DataRow[] rowsFK_ControllerSensorLegend_ControllerSensorType = row.GetChildRows("FK_ControllerSensorLegend_ControllerSensorType");
                if( rowsFK_ControllerSensorLegend_ControllerSensorType != null && rowsFK_ControllerSensorLegend_ControllerSensorType.Length > 0 ){
                    foreach (DataRow rowFK_ControllerSensorLegend_ControllerSensorType in rowsFK_ControllerSensorLegend_ControllerSensorType)
                    {
                        System.Int32 childControllerSensorLegendId = (System.Int32)rowFK_ControllerSensorLegend_ControllerSensorType["CONTROLLER_SENSOR_LEGEND_ID"];
                        CascadeRemove_ControllerSensorLegend(dataset, childControllerSensorLegendId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_TYPE]
        ///</summary>
        public static void Compact_ControllerSensorType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_SENSOR_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_SENSOR_TYPE]
        ///</summary>
        public static void CompactFreshRecords_ControllerSensorType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_SENSOR_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_SENSOR_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CONTROLLER_SENSOR_TYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CONTROLLER_TIME]
        ///</summary>
        public static void CascadeRemove_ControllerTime(DataSet dataset, System.Int32 ControllerTimeId)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_TIME"));
            DataRow row = dataset.Tables["CONTROLLER_TIME"].Rows.Find(ControllerTimeId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_TIME]
        ///</summary>
        public static void Compact_ControllerTime(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_TIME"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_TIME"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_TIME]
        ///</summary>
        public static void CompactFreshRecords_ControllerTime(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_TIME"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_TIME"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CONTROLLER_TIME"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CONTROLLER_TYPE]
        ///</summary>
        public static void CascadeRemove_ControllerType(DataSet dataset, System.Int32 ControllerTypeId)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_TYPE"));
            DataRow row = dataset.Tables["CONTROLLER_TYPE"].Rows.Find(ControllerTypeId);
            if( row != null){
                DataRow[] rowsController_Controller_Type = row.GetChildRows("Controller_Controller_Type");
                if( rowsController_Controller_Type != null && rowsController_Controller_Type.Length > 0 ){
                    foreach (DataRow rowController_Controller_Type in rowsController_Controller_Type)
                    {
                        System.Int32 childControllerId = (System.Int32)rowController_Controller_Type["CONTROLLER_ID"];
                        CascadeRemove_Controller(dataset, childControllerId);
                    }
                }
                DataRow[] rowsFK_ControllerSensor_ControllerType = row.GetChildRows("FK_ControllerSensor_ControllerType");
                if( rowsFK_ControllerSensor_ControllerType != null && rowsFK_ControllerSensor_ControllerType.Length > 0 ){
                    foreach (DataRow rowFK_ControllerSensor_ControllerType in rowsFK_ControllerSensor_ControllerType)
                    {
                        System.Int32 childControllerSensorId = (System.Int32)rowFK_ControllerSensor_ControllerType["CONTROLLER_SENSOR_ID"];
                        CascadeRemove_ControllerSensor(dataset, childControllerSensorId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_TYPE]
        ///</summary>
        public static void Compact_ControllerType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CONTROLLER_TYPE]
        ///</summary>
        public static void CompactFreshRecords_ControllerType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CONTROLLER_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CONTROLLER_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CONTROLLER_TYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [COUNT_GPRS]
        ///</summary>
        public static void CascadeRemove_CountGprs(DataSet dataset, System.DateTime TimeBegin, System.Int32 Duration)
        {
            Debug.Assert(dataset.Tables.Contains("COUNT_GPRS"));
            DataRow row = dataset.Tables["COUNT_GPRS"].Rows.Find(new object[]{TimeBegin, Duration});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [COUNT_GPRS]
        ///</summary>
        public static void Compact_CountGprs(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("COUNT_GPRS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["COUNT_GPRS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [COUNT_GPRS]
        ///</summary>
        public static void CompactFreshRecords_CountGprs(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("COUNT_GPRS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["COUNT_GPRS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["COUNT_GPRS"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [COUNT_SMS]
        ///</summary>
        public static void CascadeRemove_CountSms(DataSet dataset, System.DateTime TimeBegin, System.Int32 Duration)
        {
            Debug.Assert(dataset.Tables.Contains("COUNT_SMS"));
            DataRow row = dataset.Tables["COUNT_SMS"].Rows.Find(new object[]{TimeBegin, Duration});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [COUNT_SMS]
        ///</summary>
        public static void Compact_CountSms(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("COUNT_SMS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["COUNT_SMS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [COUNT_SMS]
        ///</summary>
        public static void CompactFreshRecords_CountSms(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("COUNT_SMS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["COUNT_SMS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["COUNT_SMS"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [CUSTOMER]
        ///</summary>
        public static void CascadeRemove_Customer(DataSet dataset, System.Int32 CustomerId)
        {
            Debug.Assert(dataset.Tables.Contains("CUSTOMER"));
            DataRow row = dataset.Tables["CUSTOMER"].Rows.Find(CustomerId);
            if( row != null){
                DataRow[] rowsOrder_Customer = row.GetChildRows("Order_Customer");
                if( rowsOrder_Customer != null && rowsOrder_Customer.Length > 0 ){
                    foreach (DataRow rowOrder_Customer in rowsOrder_Customer)
                    {
                        System.Int32 childOrderId = (System.Int32)rowOrder_Customer["ORDER_ID"];
                        CascadeRemove_Order(dataset, childOrderId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [CUSTOMER]
        ///</summary>
        public static void Compact_Customer(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CUSTOMER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CUSTOMER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [CUSTOMER]
        ///</summary>
        public static void CompactFreshRecords_Customer(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("CUSTOMER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["CUSTOMER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["CUSTOMER"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [DAY]
        ///</summary>
        public static void CascadeRemove_Day(DataSet dataset, System.Int32 DayId)
        {
            Debug.Assert(dataset.Tables.Contains("DAY"));
            DataRow row = dataset.Tables["DAY"].Rows.Find(DayId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [DAY]
        ///</summary>
        public static void Compact_Day(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DAY"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DAY"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [DAY]
        ///</summary>
        public static void CompactFreshRecords_Day(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DAY"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DAY"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["DAY"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [DAY_KIND]
        ///</summary>
        public static void CascadeRemove_DayKind(DataSet dataset, System.Int32 DayKindId)
        {
            Debug.Assert(dataset.Tables.Contains("DAY_KIND"));
            DataRow row = dataset.Tables["DAY_KIND"].Rows.Find(DayKindId);
            if( row != null){
                DataRow[] rowsCal_DayKind = row.GetChildRows("Cal_DayKind");
                if( rowsCal_DayKind != null && rowsCal_DayKind.Length > 0 ){
                    foreach (DataRow rowCal_DayKind in rowsCal_DayKind)
                    {
                        System.DateTime childDate = (System.DateTime)rowCal_DayKind["DATE"];
                        CascadeRemove_Cal(dataset, childDate);
                    }
                }
                DataRow[] rowsPeriod_DayKind = row.GetChildRows("Period_DayKind");
                if( rowsPeriod_DayKind != null && rowsPeriod_DayKind.Length > 0 ){
                    foreach (DataRow rowPeriod_DayKind in rowsPeriod_DayKind)
                    {
                        System.Int32 childPeriodId = (System.Int32)rowPeriod_DayKind["PERIOD_ID"];
                        CascadeRemove_Period(dataset, childPeriodId);
                    }
                }
                DataRow[] rowsSchedule_DayKind = row.GetChildRows("Schedule_DayKind");
                if( rowsSchedule_DayKind != null && rowsSchedule_DayKind.Length > 0 ){
                    foreach (DataRow rowSchedule_DayKind in rowsSchedule_DayKind)
                    {
                        System.Int32 childScheduleId = (System.Int32)rowSchedule_DayKind["SCHEDULE_ID"];
                        CascadeRemove_Schedule(dataset, childScheduleId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [DAY_KIND]
        ///</summary>
        public static void Compact_DayKind(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DAY_KIND"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DAY_KIND"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [DAY_KIND]
        ///</summary>
        public static void CompactFreshRecords_DayKind(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DAY_KIND"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DAY_KIND"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["DAY_KIND"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [DAY_TIME]
        ///</summary>
        public static void CascadeRemove_DayTime(DataSet dataset, System.Int32 DayTimeId)
        {
            Debug.Assert(dataset.Tables.Contains("DAY_TIME"));
            DataRow row = dataset.Tables["DAY_TIME"].Rows.Find(DayTimeId);
            if( row != null){
                DataRow[] rowsFK_GEO_SEGMENT_RUNTIME_DAY_TIME = row.GetChildRows("FK_GEO_SEGMENT_RUNTIME_DAY_TIME");
                if( rowsFK_GEO_SEGMENT_RUNTIME_DAY_TIME != null && rowsFK_GEO_SEGMENT_RUNTIME_DAY_TIME.Length > 0 ){
                    foreach (DataRow rowFK_GEO_SEGMENT_RUNTIME_DAY_TIME in rowsFK_GEO_SEGMENT_RUNTIME_DAY_TIME)
                    {
                        System.Int32 childGeoSegmentRuntimeId = (System.Int32)rowFK_GEO_SEGMENT_RUNTIME_DAY_TIME["GEO_SEGMENT_RUNTIME_ID"];
                        CascadeRemove_GeoSegmentRuntime(dataset, childGeoSegmentRuntimeId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [DAY_TIME]
        ///</summary>
        public static void Compact_DayTime(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DAY_TIME"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DAY_TIME"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [DAY_TIME]
        ///</summary>
        public static void CompactFreshRecords_DayTime(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DAY_TIME"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DAY_TIME"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["DAY_TIME"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [DAY_TYPE]
        ///</summary>
        public static void CascadeRemove_DayType(DataSet dataset, System.Int32 DayTypeId)
        {
            Debug.Assert(dataset.Tables.Contains("DAY_TYPE"));
            DataRow row = dataset.Tables["DAY_TYPE"].Rows.Find(DayTypeId);
            if( row != null){
                DataRow[] rowsFK_DAY_TIME_DAY_TYPE = row.GetChildRows("FK_DAY_TIME_DAY_TYPE");
                if( rowsFK_DAY_TIME_DAY_TYPE != null && rowsFK_DAY_TIME_DAY_TYPE.Length > 0 ){
                    foreach (DataRow rowFK_DAY_TIME_DAY_TYPE in rowsFK_DAY_TIME_DAY_TYPE)
                    {
                        System.Int32 childDayTimeId = (System.Int32)rowFK_DAY_TIME_DAY_TYPE["DAY_TIME_ID"];
                        CascadeRemove_DayTime(dataset, childDayTimeId);
                    }
                }
                DataRow[] rowsFK_RS_DAY_TYPE = row.GetChildRows("FK_RS_DAY_TYPE");
                if( rowsFK_RS_DAY_TYPE != null && rowsFK_RS_DAY_TYPE.Length > 0 ){
                    foreach (DataRow rowFK_RS_DAY_TYPE in rowsFK_RS_DAY_TYPE)
                    {
                        System.Int32 childRsId = (System.Int32)rowFK_RS_DAY_TYPE["RS_ID"];
                        CascadeRemove_Rs(dataset, childRsId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [DAY_TYPE]
        ///</summary>
        public static void Compact_DayType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DAY_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DAY_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [DAY_TYPE]
        ///</summary>
        public static void CompactFreshRecords_DayType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DAY_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DAY_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["DAY_TYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [DECADE]
        ///</summary>
        public static void CascadeRemove_Decade(DataSet dataset, System.Int32 DecadeId)
        {
            Debug.Assert(dataset.Tables.Contains("DECADE"));
            DataRow row = dataset.Tables["DECADE"].Rows.Find(DecadeId);
            if( row != null){
                DataRow[] rowsFK_BRIGADE_DECADE = row.GetChildRows("FK_BRIGADE_DECADE");
                if( rowsFK_BRIGADE_DECADE != null && rowsFK_BRIGADE_DECADE.Length > 0 ){
                    foreach (DataRow rowFK_BRIGADE_DECADE in rowsFK_BRIGADE_DECADE)
                    {
                        System.Int32 childBrigadeId = (System.Int32)rowFK_BRIGADE_DECADE["BRIGADE_ID"];
                        CascadeRemove_Brigade(dataset, childBrigadeId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [DECADE]
        ///</summary>
        public static void Compact_Decade(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DECADE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DECADE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [DECADE]
        ///</summary>
        public static void CompactFreshRecords_Decade(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DECADE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DECADE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["DECADE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [DEPARTMENT]
        ///</summary>
        public static void CascadeRemove_Department(DataSet dataset, System.Int32 DepartmentId)
        {
            Debug.Assert(dataset.Tables.Contains("DEPARTMENT"));
            DataRow row = dataset.Tables["DEPARTMENT"].Rows.Find(DepartmentId);
            if( row != null){
                DataRow[] rowsFK_DECADE_DEPARTMENT = row.GetChildRows("FK_DECADE_DEPARTMENT");
                if( rowsFK_DECADE_DEPARTMENT != null && rowsFK_DECADE_DEPARTMENT.Length > 0 ){
                    foreach (DataRow rowFK_DECADE_DEPARTMENT in rowsFK_DECADE_DEPARTMENT)
                    {
                        System.Int32 childDecadeId = (System.Int32)rowFK_DECADE_DEPARTMENT["DECADE_ID"];
                        CascadeRemove_Decade(dataset, childDecadeId);
                    }
                }
                DataRow[] rowsFK_DRIVER_DEPARTMENT = row.GetChildRows("FK_DRIVER_DEPARTMENT");
                if( rowsFK_DRIVER_DEPARTMENT != null && rowsFK_DRIVER_DEPARTMENT.Length > 0 ){
                    foreach (DataRow rowFK_DRIVER_DEPARTMENT in rowsFK_DRIVER_DEPARTMENT)
                    {
                        System.Int32 childDriverId = (System.Int32)rowFK_DRIVER_DEPARTMENT["DRIVER_ID"];
                        CascadeRemove_Driver(dataset, childDriverId);
                    }
                }
                DataRow[] rowsFK_VEHICLE_DEPARTMENT = row.GetChildRows("FK_VEHICLE_DEPARTMENT");
                if( rowsFK_VEHICLE_DEPARTMENT != null && rowsFK_VEHICLE_DEPARTMENT.Length > 0 ){
                    foreach (DataRow rowFK_VEHICLE_DEPARTMENT in rowsFK_VEHICLE_DEPARTMENT)
                    {
                        System.Int32 childVehicleId = (System.Int32)rowFK_VEHICLE_DEPARTMENT["VEHICLE_ID"];
                        CascadeRemove_Vehicle(dataset, childVehicleId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [DEPARTMENT]
        ///</summary>
        public static void Compact_Department(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DEPARTMENT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DEPARTMENT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [DEPARTMENT]
        ///</summary>
        public static void CompactFreshRecords_Department(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DEPARTMENT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DEPARTMENT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["DEPARTMENT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [DK_SET]
        ///</summary>
        public static void CascadeRemove_DkSet(DataSet dataset, System.Int32 DkSetId)
        {
            Debug.Assert(dataset.Tables.Contains("DK_SET"));
            DataRow row = dataset.Tables["DK_SET"].Rows.Find(DkSetId);
            if( row != null){
                DataRow[] rowsDayKind_DkSet = row.GetChildRows("DayKind_DkSet");
                if( rowsDayKind_DkSet != null && rowsDayKind_DkSet.Length > 0 ){
                    foreach (DataRow rowDayKind_DkSet in rowsDayKind_DkSet)
                    {
                        System.Int32 childDayKindId = (System.Int32)rowDayKind_DkSet["DAY_KIND_ID"];
                        CascadeRemove_DayKind(dataset, childDayKindId);
                    }
                }
                DataRow[] rowsPeriod_DkSet = row.GetChildRows("Period_DkSet");
                if( rowsPeriod_DkSet != null && rowsPeriod_DkSet.Length > 0 ){
                    foreach (DataRow rowPeriod_DkSet in rowsPeriod_DkSet)
                    {
                        System.Int32 childPeriodId = (System.Int32)rowPeriod_DkSet["PERIOD_ID"];
                        CascadeRemove_Period(dataset, childPeriodId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [DK_SET]
        ///</summary>
        public static void Compact_DkSet(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DK_SET"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DK_SET"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [DK_SET]
        ///</summary>
        public static void CompactFreshRecords_DkSet(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DK_SET"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DK_SET"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["DK_SET"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [DOTNET_TYPE]
        ///</summary>
        public static void CascadeRemove_DotnetType(DataSet dataset, System.Int32 DotnetTypeId)
        {
            Debug.Assert(dataset.Tables.Contains("DOTNET_TYPE"));
            DataRow row = dataset.Tables["DOTNET_TYPE"].Rows.Find(DotnetTypeId);
            if( row != null){
                DataRow[] rowsMessageTemplateField_DotnetType = row.GetChildRows("MessageTemplateField_DotnetType");
                if( rowsMessageTemplateField_DotnetType != null && rowsMessageTemplateField_DotnetType.Length > 0 ){
                    foreach (DataRow rowMessageTemplateField_DotnetType in rowsMessageTemplateField_DotnetType)
                    {
                        System.Int32 childMessageTemplateFieldId = (System.Int32)rowMessageTemplateField_DotnetType["MESSAGE_TEMPLATE_FIELD_ID"];
                        CascadeRemove_MessageTemplateField(dataset, childMessageTemplateFieldId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [DOTNET_TYPE]
        ///</summary>
        public static void Compact_DotnetType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DOTNET_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DOTNET_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [DOTNET_TYPE]
        ///</summary>
        public static void CompactFreshRecords_DotnetType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DOTNET_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DOTNET_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["DOTNET_TYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [DRIVER]
        ///</summary>
        public static void CascadeRemove_Driver(DataSet dataset, System.Int32 DriverId)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVER"));
            DataRow row = dataset.Tables["DRIVER"].Rows.Find(DriverId);
            if( row != null){
                DataRow[] rowsDriverGroupDriver_Driver = row.GetChildRows("DriverGroupDriver_Driver");
                if( rowsDriverGroupDriver_Driver != null && rowsDriverGroupDriver_Driver.Length > 0 ){
                    foreach (DataRow rowDriverGroupDriver_Driver in rowsDriverGroupDriver_Driver)
                    {
                        System.Int32 childDrivergroupId = (System.Int32)rowDriverGroupDriver_Driver["DRIVERGROUP_ID"];
                        System.Int32 childDriverId = (System.Int32)rowDriverGroupDriver_Driver["DRIVER_ID"];
                        CascadeRemove_DrivergroupDriver(dataset, childDrivergroupId, childDriverId);
                    }
                }
                DataRow[] rowsDriverVehicle_Driver = row.GetChildRows("DriverVehicle_Driver");
                if( rowsDriverVehicle_Driver != null && rowsDriverVehicle_Driver.Length > 0 ){
                    foreach (DataRow rowDriverVehicle_Driver in rowsDriverVehicle_Driver)
                    {
                        System.Int32 childDriverId = (System.Int32)rowDriverVehicle_Driver["DRIVER_ID"];
                        CascadeRemove_DriverVehicle(dataset, childDriverId);
                    }
                }
                DataRow[] rowsFK_BRIGADE_DRIVER_DRIVER = row.GetChildRows("FK_BRIGADE_DRIVER_DRIVER");
                if( rowsFK_BRIGADE_DRIVER_DRIVER != null && rowsFK_BRIGADE_DRIVER_DRIVER.Length > 0 ){
                    foreach (DataRow rowFK_BRIGADE_DRIVER_DRIVER in rowsFK_BRIGADE_DRIVER_DRIVER)
                    {
                        System.Int32 childBrigadeDriverId = (System.Int32)rowFK_BRIGADE_DRIVER_DRIVER["BRIGADE_DRIVER_ID"];
                        CascadeRemove_BrigadeDriver(dataset, childBrigadeDriverId);
                    }
                }
                DataRow[] rowsFK_JOURNAL_DRIVER = row.GetChildRows("FK_JOURNAL_DRIVER");
                if( rowsFK_JOURNAL_DRIVER != null && rowsFK_JOURNAL_DRIVER.Length > 0 ){
                    foreach (DataRow rowFK_JOURNAL_DRIVER in rowsFK_JOURNAL_DRIVER)
                    {
                        System.Int32 childJournalId = (System.Int32)rowFK_JOURNAL_DRIVER["JOURNAL_ID"];
                        CascadeRemove_Journal(dataset, childJournalId);
                    }
                }
                DataRow[] rowsFK_JOURNAL_DRIVER_DRIVER = row.GetChildRows("FK_JOURNAL_DRIVER_DRIVER");
                if( rowsFK_JOURNAL_DRIVER_DRIVER != null && rowsFK_JOURNAL_DRIVER_DRIVER.Length > 0 ){
                    foreach (DataRow rowFK_JOURNAL_DRIVER_DRIVER in rowsFK_JOURNAL_DRIVER_DRIVER)
                    {
                        System.Int32 childJournalDriverId = (System.Int32)rowFK_JOURNAL_DRIVER_DRIVER["JOURNAL_DRIVER_ID"];
                        CascadeRemove_JournalDriver(dataset, childJournalDriverId);
                    }
                }
                DataRow[] rowsFK_WORK_TIME_DRIVER = row.GetChildRows("FK_WORK_TIME_DRIVER");
                if( rowsFK_WORK_TIME_DRIVER != null && rowsFK_WORK_TIME_DRIVER.Length > 0 ){
                    foreach (DataRow rowFK_WORK_TIME_DRIVER in rowsFK_WORK_TIME_DRIVER)
                    {
                        System.Int32 childDriverId = (System.Int32)rowFK_WORK_TIME_DRIVER["DRIVER_ID"];
                        CascadeRemove_WorkTime(dataset, childDriverId);
                    }
                }
                DataRow[] rowsJournal_Driver1 = row.GetChildRows("Journal_Driver1");
                if( rowsJournal_Driver1 != null && rowsJournal_Driver1.Length > 0 ){
                    foreach (DataRow rowJournal_Driver1 in rowsJournal_Driver1)
                    {
                        System.Int32 childJournalId = (System.Int32)rowJournal_Driver1["JOURNAL_ID"];
                        CascadeRemove_Journal(dataset, childJournalId);
                    }
                }
                DataRow[] rowsOperatorDriver_Driver = row.GetChildRows("OperatorDriver_Driver");
                if( rowsOperatorDriver_Driver != null && rowsOperatorDriver_Driver.Length > 0 ){
                    foreach (DataRow rowOperatorDriver_Driver in rowsOperatorDriver_Driver)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorDriver_Driver["OPERATOR_ID"];
                        System.Int32 childDriverId = (System.Int32)rowOperatorDriver_Driver["DRIVER_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorDriver_Driver["RIGHT_ID"];
                        CascadeRemove_OperatorDriver(dataset, childOperatorId, childDriverId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupDriver_Driver = row.GetChildRows("OperatorGroupDriver_Driver");
                if( rowsOperatorGroupDriver_Driver != null && rowsOperatorGroupDriver_Driver.Length > 0 ){
                    foreach (DataRow rowOperatorGroupDriver_Driver in rowsOperatorGroupDriver_Driver)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupDriver_Driver["OPERATORGROUP_ID"];
                        System.Int32 childDriverId = (System.Int32)rowOperatorGroupDriver_Driver["DRIVER_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupDriver_Driver["RIGHT_ID"];
                        CascadeRemove_OperatorgroupDriver(dataset, childOperatorgroupId, childDriverId, childRightId);
                    }
                }
                DataRow[] rowsWaybill_Driver = row.GetChildRows("Waybill_Driver");
                if( rowsWaybill_Driver != null && rowsWaybill_Driver.Length > 0 ){
                    foreach (DataRow rowWaybill_Driver in rowsWaybill_Driver)
                    {
                        System.Int32 childWaybillId = (System.Int32)rowWaybill_Driver["WAYBILL_ID"];
                        CascadeRemove_Waybill(dataset, childWaybillId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [DRIVER]
        ///</summary>
        public static void Compact_Driver(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DRIVER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [DRIVER]
        ///</summary>
        public static void CompactFreshRecords_Driver(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DRIVER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["DRIVER"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [DRIVER_BONUS]
        ///</summary>
        public static void CascadeRemove_DriverBonus(DataSet dataset, System.Int32 DriverBonusId)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVER_BONUS"));
            DataRow row = dataset.Tables["DRIVER_BONUS"].Rows.Find(DriverBonusId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [DRIVER_BONUS]
        ///</summary>
        public static void Compact_DriverBonus(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVER_BONUS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DRIVER_BONUS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [DRIVER_BONUS]
        ///</summary>
        public static void CompactFreshRecords_DriverBonus(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVER_BONUS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DRIVER_BONUS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["DRIVER_BONUS"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [DRIVER_MSG_TEMPLATE]
        ///</summary>
        public static void CascadeRemove_DriverMsgTemplate(DataSet dataset, System.Int32 DriverMsgTemplateId)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVER_MSG_TEMPLATE"));
            DataRow row = dataset.Tables["DRIVER_MSG_TEMPLATE"].Rows.Find(DriverMsgTemplateId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [DRIVER_MSG_TEMPLATE]
        ///</summary>
        public static void Compact_DriverMsgTemplate(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVER_MSG_TEMPLATE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DRIVER_MSG_TEMPLATE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [DRIVER_MSG_TEMPLATE]
        ///</summary>
        public static void CompactFreshRecords_DriverMsgTemplate(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVER_MSG_TEMPLATE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DRIVER_MSG_TEMPLATE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["DRIVER_MSG_TEMPLATE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [DRIVER_STATUS]
        ///</summary>
        public static void CascadeRemove_DriverStatus(DataSet dataset, System.Int32 DriverStatusId)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVER_STATUS"));
            DataRow row = dataset.Tables["DRIVER_STATUS"].Rows.Find(DriverStatusId);
            if( row != null){
                DataRow[] rowsDriver_DriverStatus = row.GetChildRows("Driver_DriverStatus");
                if( rowsDriver_DriverStatus != null && rowsDriver_DriverStatus.Length > 0 ){
                    foreach (DataRow rowDriver_DriverStatus in rowsDriver_DriverStatus)
                    {
                        System.Int32 childDriverId = (System.Int32)rowDriver_DriverStatus["DRIVER_ID"];
                        CascadeRemove_Driver(dataset, childDriverId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [DRIVER_STATUS]
        ///</summary>
        public static void Compact_DriverStatus(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVER_STATUS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DRIVER_STATUS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [DRIVER_STATUS]
        ///</summary>
        public static void CompactFreshRecords_DriverStatus(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVER_STATUS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DRIVER_STATUS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["DRIVER_STATUS"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [DRIVER_VEHICLE]
        ///</summary>
        public static void CascadeRemove_DriverVehicle(DataSet dataset, System.Int32 DriverId)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVER_VEHICLE"));
            DataRow row = dataset.Tables["DRIVER_VEHICLE"].Rows.Find(DriverId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [DRIVER_VEHICLE]
        ///</summary>
        public static void Compact_DriverVehicle(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVER_VEHICLE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DRIVER_VEHICLE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [DRIVER_VEHICLE]
        ///</summary>
        public static void CompactFreshRecords_DriverVehicle(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVER_VEHICLE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DRIVER_VEHICLE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["DRIVER_VEHICLE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [DRIVERGROUP]
        ///</summary>
        public static void CascadeRemove_Drivergroup(DataSet dataset, System.Int32 DrivergroupId)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVERGROUP"));
            DataRow row = dataset.Tables["DRIVERGROUP"].Rows.Find(DrivergroupId);
            if( row != null){
                DataRow[] rowsDriverGroupDriver_DriverGroup = row.GetChildRows("DriverGroupDriver_DriverGroup");
                if( rowsDriverGroupDriver_DriverGroup != null && rowsDriverGroupDriver_DriverGroup.Length > 0 ){
                    foreach (DataRow rowDriverGroupDriver_DriverGroup in rowsDriverGroupDriver_DriverGroup)
                    {
                        System.Int32 childDrivergroupId = (System.Int32)rowDriverGroupDriver_DriverGroup["DRIVERGROUP_ID"];
                        System.Int32 childDriverId = (System.Int32)rowDriverGroupDriver_DriverGroup["DRIVER_ID"];
                        CascadeRemove_DrivergroupDriver(dataset, childDrivergroupId, childDriverId);
                    }
                }
                DataRow[] rowsOoperatorDriverGroup_DriverGroup = row.GetChildRows("OoperatorDriverGroup_DriverGroup");
                if( rowsOoperatorDriverGroup_DriverGroup != null && rowsOoperatorDriverGroup_DriverGroup.Length > 0 ){
                    foreach (DataRow rowOoperatorDriverGroup_DriverGroup in rowsOoperatorDriverGroup_DriverGroup)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOoperatorDriverGroup_DriverGroup["OPERATOR_ID"];
                        System.Int32 childDrivergroupId = (System.Int32)rowOoperatorDriverGroup_DriverGroup["DRIVERGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOoperatorDriverGroup_DriverGroup["RIGHT_ID"];
                        CascadeRemove_OperatorDrivergroup(dataset, childOperatorId, childDrivergroupId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupDriverGroup_DriverGroup = row.GetChildRows("OperatorGroupDriverGroup_DriverGroup");
                if( rowsOperatorGroupDriverGroup_DriverGroup != null && rowsOperatorGroupDriverGroup_DriverGroup.Length > 0 ){
                    foreach (DataRow rowOperatorGroupDriverGroup_DriverGroup in rowsOperatorGroupDriverGroup_DriverGroup)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupDriverGroup_DriverGroup["OPERATORGROUP_ID"];
                        System.Int32 childDrivergroupId = (System.Int32)rowOperatorGroupDriverGroup_DriverGroup["DRIVERGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupDriverGroup_DriverGroup["RIGHT_ID"];
                        CascadeRemove_OperatorgroupDrivergroup(dataset, childOperatorgroupId, childDrivergroupId, childRightId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [DRIVERGROUP]
        ///</summary>
        public static void Compact_Drivergroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVERGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DRIVERGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [DRIVERGROUP]
        ///</summary>
        public static void CompactFreshRecords_Drivergroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVERGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DRIVERGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["DRIVERGROUP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [DRIVERGROUP_DRIVER]
        ///</summary>
        public static void CascadeRemove_DrivergroupDriver(DataSet dataset, System.Int32 DrivergroupId, System.Int32 DriverId)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVERGROUP_DRIVER"));
            DataRow row = dataset.Tables["DRIVERGROUP_DRIVER"].Rows.Find(new object[]{DrivergroupId, DriverId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [DRIVERGROUP_DRIVER]
        ///</summary>
        public static void Compact_DrivergroupDriver(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVERGROUP_DRIVER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DRIVERGROUP_DRIVER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [DRIVERGROUP_DRIVER]
        ///</summary>
        public static void CompactFreshRecords_DrivergroupDriver(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("DRIVERGROUP_DRIVER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["DRIVERGROUP_DRIVER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["DRIVERGROUP_DRIVER"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [EMAIL]
        ///</summary>
        public static void CascadeRemove_Email(DataSet dataset, System.Int32 EmailId)
        {
            Debug.Assert(dataset.Tables.Contains("EMAIL"));
            DataRow row = dataset.Tables["EMAIL"].Rows.Find(EmailId);
            if( row != null){
                DataRow[] rowsFK_EMAIL_SCHEDULEREVENT_EMAIL = row.GetChildRows("FK_EMAIL_SCHEDULEREVENT_EMAIL");
                if( rowsFK_EMAIL_SCHEDULEREVENT_EMAIL != null && rowsFK_EMAIL_SCHEDULEREVENT_EMAIL.Length > 0 ){
                    foreach (DataRow rowFK_EMAIL_SCHEDULEREVENT_EMAIL in rowsFK_EMAIL_SCHEDULEREVENT_EMAIL)
                    {
                        System.Int32 childSchedulereventId = (System.Int32)rowFK_EMAIL_SCHEDULEREVENT_EMAIL["SCHEDULEREVENT_ID"];
                        System.Int32 childEmailId = (System.Int32)rowFK_EMAIL_SCHEDULEREVENT_EMAIL["EMAIL_ID"];
                        CascadeRemove_EmailSchedulerevent(dataset, childSchedulereventId, childEmailId);
                    }
                }
                DataRow[] rowsFK_EMAIL_SCHEDULERQUEUE_EMAIL = row.GetChildRows("FK_EMAIL_SCHEDULERQUEUE_EMAIL");
                if( rowsFK_EMAIL_SCHEDULERQUEUE_EMAIL != null && rowsFK_EMAIL_SCHEDULERQUEUE_EMAIL.Length > 0 ){
                    foreach (DataRow rowFK_EMAIL_SCHEDULERQUEUE_EMAIL in rowsFK_EMAIL_SCHEDULERQUEUE_EMAIL)
                    {
                        System.Int32 childSchedulerqueueId = (System.Int32)rowFK_EMAIL_SCHEDULERQUEUE_EMAIL["SCHEDULERQUEUE_ID"];
                        System.Int32 childEmailId = (System.Int32)rowFK_EMAIL_SCHEDULERQUEUE_EMAIL["EMAIL_ID"];
                        CascadeRemove_EmailSchedulerqueue(dataset, childSchedulerqueueId, childEmailId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [EMAIL]
        ///</summary>
        public static void Compact_Email(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("EMAIL"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["EMAIL"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [EMAIL]
        ///</summary>
        public static void CompactFreshRecords_Email(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("EMAIL"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["EMAIL"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["EMAIL"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [EMAIL_SCHEDULEREVENT]
        ///</summary>
        public static void CascadeRemove_EmailSchedulerevent(DataSet dataset, System.Int32 SchedulereventId, System.Int32 EmailId)
        {
            Debug.Assert(dataset.Tables.Contains("EMAIL_SCHEDULEREVENT"));
            DataRow row = dataset.Tables["EMAIL_SCHEDULEREVENT"].Rows.Find(new object[]{SchedulereventId, EmailId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [EMAIL_SCHEDULEREVENT]
        ///</summary>
        public static void Compact_EmailSchedulerevent(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("EMAIL_SCHEDULEREVENT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["EMAIL_SCHEDULEREVENT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [EMAIL_SCHEDULEREVENT]
        ///</summary>
        public static void CompactFreshRecords_EmailSchedulerevent(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("EMAIL_SCHEDULEREVENT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["EMAIL_SCHEDULEREVENT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["EMAIL_SCHEDULEREVENT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [EMAIL_SCHEDULERQUEUE]
        ///</summary>
        public static void CascadeRemove_EmailSchedulerqueue(DataSet dataset, System.Int32 SchedulerqueueId, System.Int32 EmailId)
        {
            Debug.Assert(dataset.Tables.Contains("EMAIL_SCHEDULERQUEUE"));
            DataRow row = dataset.Tables["EMAIL_SCHEDULERQUEUE"].Rows.Find(new object[]{SchedulerqueueId, EmailId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [EMAIL_SCHEDULERQUEUE]
        ///</summary>
        public static void Compact_EmailSchedulerqueue(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("EMAIL_SCHEDULERQUEUE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["EMAIL_SCHEDULERQUEUE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [EMAIL_SCHEDULERQUEUE]
        ///</summary>
        public static void CompactFreshRecords_EmailSchedulerqueue(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("EMAIL_SCHEDULERQUEUE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["EMAIL_SCHEDULERQUEUE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["EMAIL_SCHEDULERQUEUE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [FACTOR_VALUES]
        ///</summary>
        public static void CascadeRemove_FactorValues(DataSet dataset, System.Int32 FactorValuesId)
        {
            Debug.Assert(dataset.Tables.Contains("FACTOR_VALUES"));
            DataRow row = dataset.Tables["FACTOR_VALUES"].Rows.Find(FactorValuesId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [FACTOR_VALUES]
        ///</summary>
        public static void Compact_FactorValues(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("FACTOR_VALUES"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["FACTOR_VALUES"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [FACTOR_VALUES]
        ///</summary>
        public static void CompactFreshRecords_FactorValues(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("FACTOR_VALUES"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["FACTOR_VALUES"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["FACTOR_VALUES"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [FACTORS]
        ///</summary>
        public static void CascadeRemove_Factors(DataSet dataset, System.Int32 FactorId)
        {
            Debug.Assert(dataset.Tables.Contains("FACTORS"));
            DataRow row = dataset.Tables["FACTORS"].Rows.Find(FactorId);
            if( row != null){
                DataRow[] rowsFactorValues_Factors = row.GetChildRows("FactorValues_Factors");
                if( rowsFactorValues_Factors != null && rowsFactorValues_Factors.Length > 0 ){
                    foreach (DataRow rowFactorValues_Factors in rowsFactorValues_Factors)
                    {
                        System.Int32 childFactorValuesId = (System.Int32)rowFactorValues_Factors["FACTOR_VALUES_ID"];
                        CascadeRemove_FactorValues(dataset, childFactorValuesId);
                    }
                }
                DataRow[] rowsGeoSegment_Factors = row.GetChildRows("GeoSegment_Factors");
                if( rowsGeoSegment_Factors != null && rowsGeoSegment_Factors.Length > 0 ){
                    foreach (DataRow rowGeoSegment_Factors in rowsGeoSegment_Factors)
                    {
                        System.Int32 childGeoSegmentId = (System.Int32)rowGeoSegment_Factors["GEO_SEGMENT_ID"];
                        CascadeRemove_GeoSegment(dataset, childGeoSegmentId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [FACTORS]
        ///</summary>
        public static void Compact_Factors(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("FACTORS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["FACTORS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [FACTORS]
        ///</summary>
        public static void CompactFreshRecords_Factors(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("FACTORS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["FACTORS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["FACTORS"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [GEO_SEGMENT]
        ///</summary>
        public static void CascadeRemove_GeoSegment(DataSet dataset, System.Int32 GeoSegmentId)
        {
            Debug.Assert(dataset.Tables.Contains("GEO_SEGMENT"));
            DataRow row = dataset.Tables["GEO_SEGMENT"].Rows.Find(GeoSegmentId);
            if( row != null){
                DataRow[] rowsFK_GEO_SEGMENT_RUNTIME_GEO_SEGMENT = row.GetChildRows("FK_GEO_SEGMENT_RUNTIME_GEO_SEGMENT");
                if( rowsFK_GEO_SEGMENT_RUNTIME_GEO_SEGMENT != null && rowsFK_GEO_SEGMENT_RUNTIME_GEO_SEGMENT.Length > 0 ){
                    foreach (DataRow rowFK_GEO_SEGMENT_RUNTIME_GEO_SEGMENT in rowsFK_GEO_SEGMENT_RUNTIME_GEO_SEGMENT)
                    {
                        System.Int32 childGeoSegmentRuntimeId = (System.Int32)rowFK_GEO_SEGMENT_RUNTIME_GEO_SEGMENT["GEO_SEGMENT_RUNTIME_ID"];
                        CascadeRemove_GeoSegmentRuntime(dataset, childGeoSegmentRuntimeId);
                    }
                }
                DataRow[] rowsGeoSegmentVeretx_GeoSegment = row.GetChildRows("GeoSegmentVeretx_GeoSegment");
                if( rowsGeoSegmentVeretx_GeoSegment != null && rowsGeoSegmentVeretx_GeoSegment.Length > 0 ){
                    foreach (DataRow rowGeoSegmentVeretx_GeoSegment in rowsGeoSegmentVeretx_GeoSegment)
                    {
                        System.Int32 childGeoSegmentVertexId = (System.Int32)rowGeoSegmentVeretx_GeoSegment["GEO_SEGMENT_VERTEX_ID"];
                        CascadeRemove_GeoSegmentVertex(dataset, childGeoSegmentVertexId);
                    }
                }
                DataRow[] rowsSchedulePoint_GeoSegment = row.GetChildRows("SchedulePoint_GeoSegment");
                if( rowsSchedulePoint_GeoSegment != null && rowsSchedulePoint_GeoSegment.Length > 0 ){
                    foreach (DataRow rowSchedulePoint_GeoSegment in rowsSchedulePoint_GeoSegment)
                    {
                        System.Int32 childSchedulePointId = (System.Int32)rowSchedulePoint_GeoSegment["SCHEDULE_POINT_ID"];
                        CascadeRemove_SchedulePoint(dataset, childSchedulePointId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [GEO_SEGMENT]
        ///</summary>
        public static void Compact_GeoSegment(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GEO_SEGMENT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GEO_SEGMENT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [GEO_SEGMENT]
        ///</summary>
        public static void CompactFreshRecords_GeoSegment(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GEO_SEGMENT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GEO_SEGMENT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["GEO_SEGMENT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [GEO_SEGMENT_RUNTIME]
        ///</summary>
        public static void CascadeRemove_GeoSegmentRuntime(DataSet dataset, System.Int32 GeoSegmentRuntimeId)
        {
            Debug.Assert(dataset.Tables.Contains("GEO_SEGMENT_RUNTIME"));
            DataRow row = dataset.Tables["GEO_SEGMENT_RUNTIME"].Rows.Find(GeoSegmentRuntimeId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [GEO_SEGMENT_RUNTIME]
        ///</summary>
        public static void Compact_GeoSegmentRuntime(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GEO_SEGMENT_RUNTIME"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GEO_SEGMENT_RUNTIME"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [GEO_SEGMENT_RUNTIME]
        ///</summary>
        public static void CompactFreshRecords_GeoSegmentRuntime(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GEO_SEGMENT_RUNTIME"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GEO_SEGMENT_RUNTIME"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["GEO_SEGMENT_RUNTIME"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [GEO_SEGMENT_VERTEX]
        ///</summary>
        public static void CascadeRemove_GeoSegmentVertex(DataSet dataset, System.Int32 GeoSegmentVertexId)
        {
            Debug.Assert(dataset.Tables.Contains("GEO_SEGMENT_VERTEX"));
            DataRow row = dataset.Tables["GEO_SEGMENT_VERTEX"].Rows.Find(GeoSegmentVertexId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [GEO_SEGMENT_VERTEX]
        ///</summary>
        public static void Compact_GeoSegmentVertex(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GEO_SEGMENT_VERTEX"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GEO_SEGMENT_VERTEX"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [GEO_SEGMENT_VERTEX]
        ///</summary>
        public static void CompactFreshRecords_GeoSegmentVertex(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GEO_SEGMENT_VERTEX"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GEO_SEGMENT_VERTEX"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["GEO_SEGMENT_VERTEX"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [GEO_TRIP]
        ///</summary>
        public static void CascadeRemove_GeoTrip(DataSet dataset, System.Int32 GeoTripId)
        {
            Debug.Assert(dataset.Tables.Contains("GEO_TRIP"));
            DataRow row = dataset.Tables["GEO_TRIP"].Rows.Find(GeoTripId);
            if( row != null){
                DataRow[] rowsFK_Wb_Trip_Geo_Trip = row.GetChildRows("FK_Wb_Trip_Geo_Trip");
                if( rowsFK_Wb_Trip_Geo_Trip != null && rowsFK_Wb_Trip_Geo_Trip.Length > 0 ){
                    foreach (DataRow rowFK_Wb_Trip_Geo_Trip in rowsFK_Wb_Trip_Geo_Trip)
                    {
                        System.Int32 childWbTripId = (System.Int32)rowFK_Wb_Trip_Geo_Trip["WB_TRIP_ID"];
                        CascadeRemove_WbTrip(dataset, childWbTripId);
                    }
                }
                DataRow[] rowsGeoSegment_GeoTrip = row.GetChildRows("GeoSegment_GeoTrip");
                if( rowsGeoSegment_GeoTrip != null && rowsGeoSegment_GeoTrip.Length > 0 ){
                    foreach (DataRow rowGeoSegment_GeoTrip in rowsGeoSegment_GeoTrip)
                    {
                        System.Int32 childGeoSegmentId = (System.Int32)rowGeoSegment_GeoTrip["GEO_SEGMENT_ID"];
                        CascadeRemove_GeoSegment(dataset, childGeoSegmentId);
                    }
                }
                DataRow[] rowsRouteTrip_GeoTrip = row.GetChildRows("RouteTrip_GeoTrip");
                if( rowsRouteTrip_GeoTrip != null && rowsRouteTrip_GeoTrip.Length > 0 ){
                    foreach (DataRow rowRouteTrip_GeoTrip in rowsRouteTrip_GeoTrip)
                    {
                        System.Int32 childRouteTripId = (System.Int32)rowRouteTrip_GeoTrip["ROUTE_TRIP_ID"];
                        CascadeRemove_RouteTrip(dataset, childRouteTripId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [GEO_TRIP]
        ///</summary>
        public static void Compact_GeoTrip(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GEO_TRIP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GEO_TRIP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [GEO_TRIP]
        ///</summary>
        public static void CompactFreshRecords_GeoTrip(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GEO_TRIP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GEO_TRIP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["GEO_TRIP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [GEO_ZONE]
        ///</summary>
        public static void CascadeRemove_GeoZone(DataSet dataset, System.Int32 ZoneId)
        {
            Debug.Assert(dataset.Tables.Contains("GEO_ZONE"));
            DataRow row = dataset.Tables["GEO_ZONE"].Rows.Find(ZoneId);
            if( row != null){
                DataRow[] rowsFK_LOGISTIC_ADDRESS_GEO_ZONE = row.GetChildRows("FK_LOGISTIC_ADDRESS_GEO_ZONE");
                if( rowsFK_LOGISTIC_ADDRESS_GEO_ZONE != null && rowsFK_LOGISTIC_ADDRESS_GEO_ZONE.Length > 0 ){
                    foreach (DataRow rowFK_LOGISTIC_ADDRESS_GEO_ZONE in rowsFK_LOGISTIC_ADDRESS_GEO_ZONE)
                    {
                        System.Int32 childLogisticAddressId = (System.Int32)rowFK_LOGISTIC_ADDRESS_GEO_ZONE["LOGISTIC_ADDRESS_ID"];
                        CascadeRemove_LogisticAddress(dataset, childLogisticAddressId);
                    }
                }
                DataRow[] rowsFK_ROUTE_GEOZONE_GEO_ZONE = row.GetChildRows("FK_ROUTE_GEOZONE_GEO_ZONE");
                if( rowsFK_ROUTE_GEOZONE_GEO_ZONE != null && rowsFK_ROUTE_GEOZONE_GEO_ZONE.Length > 0 ){
                    foreach (DataRow rowFK_ROUTE_GEOZONE_GEO_ZONE in rowsFK_ROUTE_GEOZONE_GEO_ZONE)
                    {
                        System.Int32 childRouteGeozoneId = (System.Int32)rowFK_ROUTE_GEOZONE_GEO_ZONE["ROUTE_GEOZONE_ID"];
                        CascadeRemove_RouteGeozone(dataset, childRouteGeozoneId);
                    }
                }
                DataRow[] rowsGeoZonePrimitive_GeoZone = row.GetChildRows("GeoZonePrimitive_GeoZone");
                if( rowsGeoZonePrimitive_GeoZone != null && rowsGeoZonePrimitive_GeoZone.Length > 0 ){
                    foreach (DataRow rowGeoZonePrimitive_GeoZone in rowsGeoZonePrimitive_GeoZone)
                    {
                        System.Int32 childGeoZonePrimitiveId = (System.Int32)rowGeoZonePrimitive_GeoZone["GEO_ZONE_PRIMITIVE_ID"];
                        CascadeRemove_GeoZonePrimitive(dataset, childGeoZonePrimitiveId);
                    }
                }
                DataRow[] rowsOperatorGroupZone_Zone = row.GetChildRows("OperatorGroupZone_Zone");
                if( rowsOperatorGroupZone_Zone != null && rowsOperatorGroupZone_Zone.Length > 0 ){
                    foreach (DataRow rowOperatorGroupZone_Zone in rowsOperatorGroupZone_Zone)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupZone_Zone["OPERATORGROUP_ID"];
                        System.Int32 childZoneId = (System.Int32)rowOperatorGroupZone_Zone["ZONE_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupZone_Zone["RIGHT_ID"];
                        CascadeRemove_OperatorgroupZone(dataset, childOperatorgroupId, childZoneId, childRightId);
                    }
                }
                DataRow[] rowsOperatorZone_Zone = row.GetChildRows("OperatorZone_Zone");
                if( rowsOperatorZone_Zone != null && rowsOperatorZone_Zone.Length > 0 ){
                    foreach (DataRow rowOperatorZone_Zone in rowsOperatorZone_Zone)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorZone_Zone["OPERATOR_ID"];
                        System.Int32 childZoneId = (System.Int32)rowOperatorZone_Zone["ZONE_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorZone_Zone["RIGHT_ID"];
                        CascadeRemove_OperatorZone(dataset, childOperatorId, childZoneId, childRightId);
                    }
                }
                DataRow[] rowsZoneGroupZone_Zone = row.GetChildRows("ZoneGroupZone_Zone");
                if( rowsZoneGroupZone_Zone != null && rowsZoneGroupZone_Zone.Length > 0 ){
                    foreach (DataRow rowZoneGroupZone_Zone in rowsZoneGroupZone_Zone)
                    {
                        System.Int32 childZonegroupZoneId = (System.Int32)rowZoneGroupZone_Zone["ZONEGROUP_ZONE_ID"];
                        CascadeRemove_ZonegroupZone(dataset, childZonegroupZoneId);
                    }
                }
                DataRow[] rowsZoneVehicle_Zone = row.GetChildRows("ZoneVehicle_Zone");
                if( rowsZoneVehicle_Zone != null && rowsZoneVehicle_Zone.Length > 0 ){
                    foreach (DataRow rowZoneVehicle_Zone in rowsZoneVehicle_Zone)
                    {
                        System.Int32 childZoneVehicleId = (System.Int32)rowZoneVehicle_Zone["ZONE_VEHICLE_ID"];
                        CascadeRemove_ZoneVehicle(dataset, childZoneVehicleId);
                    }
                }
                DataRow[] rowsZoneVehicleGroup_Zone = row.GetChildRows("ZoneVehicleGroup_Zone");
                if( rowsZoneVehicleGroup_Zone != null && rowsZoneVehicleGroup_Zone.Length > 0 ){
                    foreach (DataRow rowZoneVehicleGroup_Zone in rowsZoneVehicleGroup_Zone)
                    {
                        System.Int32 childZoneVehiclegroupId = (System.Int32)rowZoneVehicleGroup_Zone["ZONE_VEHICLEGROUP_ID"];
                        CascadeRemove_ZoneVehiclegroup(dataset, childZoneVehiclegroupId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [GEO_ZONE]
        ///</summary>
        public static void Compact_GeoZone(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GEO_ZONE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GEO_ZONE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [GEO_ZONE]
        ///</summary>
        public static void CompactFreshRecords_GeoZone(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GEO_ZONE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GEO_ZONE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["GEO_ZONE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [GEO_ZONE_PRIMITIVE]
        ///</summary>
        public static void CascadeRemove_GeoZonePrimitive(DataSet dataset, System.Int32 GeoZonePrimitiveId)
        {
            Debug.Assert(dataset.Tables.Contains("GEO_ZONE_PRIMITIVE"));
            DataRow row = dataset.Tables["GEO_ZONE_PRIMITIVE"].Rows.Find(GeoZonePrimitiveId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [GEO_ZONE_PRIMITIVE]
        ///</summary>
        public static void Compact_GeoZonePrimitive(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GEO_ZONE_PRIMITIVE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GEO_ZONE_PRIMITIVE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [GEO_ZONE_PRIMITIVE]
        ///</summary>
        public static void CompactFreshRecords_GeoZonePrimitive(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GEO_ZONE_PRIMITIVE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GEO_ZONE_PRIMITIVE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["GEO_ZONE_PRIMITIVE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [GOODS]
        ///</summary>
        public static void CascadeRemove_Goods(DataSet dataset, System.Int32 GoodsId)
        {
            Debug.Assert(dataset.Tables.Contains("GOODS"));
            DataRow row = dataset.Tables["GOODS"].Rows.Find(GoodsId);
            if( row != null){
                DataRow[] rowsFK_GOODS_LOGISTIC_ORDER_GOODS = row.GetChildRows("FK_GOODS_LOGISTIC_ORDER_GOODS");
                if( rowsFK_GOODS_LOGISTIC_ORDER_GOODS != null && rowsFK_GOODS_LOGISTIC_ORDER_GOODS.Length > 0 ){
                    foreach (DataRow rowFK_GOODS_LOGISTIC_ORDER_GOODS in rowsFK_GOODS_LOGISTIC_ORDER_GOODS)
                    {
                        System.Int32 childGoodsLogisticOrderId = (System.Int32)rowFK_GOODS_LOGISTIC_ORDER_GOODS["GOODS_LOGISTIC_ORDER_ID"];
                        CascadeRemove_GoodsLogisticOrder(dataset, childGoodsLogisticOrderId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [GOODS]
        ///</summary>
        public static void Compact_Goods(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GOODS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GOODS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [GOODS]
        ///</summary>
        public static void CompactFreshRecords_Goods(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GOODS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GOODS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["GOODS"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [GOODS_LOGISTIC_ORDER]
        ///</summary>
        public static void CascadeRemove_GoodsLogisticOrder(DataSet dataset, System.Int32 GoodsLogisticOrderId)
        {
            Debug.Assert(dataset.Tables.Contains("GOODS_LOGISTIC_ORDER"));
            DataRow row = dataset.Tables["GOODS_LOGISTIC_ORDER"].Rows.Find(GoodsLogisticOrderId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [GOODS_LOGISTIC_ORDER]
        ///</summary>
        public static void Compact_GoodsLogisticOrder(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GOODS_LOGISTIC_ORDER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GOODS_LOGISTIC_ORDER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [GOODS_LOGISTIC_ORDER]
        ///</summary>
        public static void CompactFreshRecords_GoodsLogisticOrder(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GOODS_LOGISTIC_ORDER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GOODS_LOGISTIC_ORDER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["GOODS_LOGISTIC_ORDER"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [GOODS_TYPE]
        ///</summary>
        public static void CascadeRemove_GoodsType(DataSet dataset, System.Int32 GoodsTypeId)
        {
            Debug.Assert(dataset.Tables.Contains("GOODS_TYPE"));
            DataRow row = dataset.Tables["GOODS_TYPE"].Rows.Find(GoodsTypeId);
            if( row != null){
                DataRow[] rowsFK_GOODS_GOODS_TYPE = row.GetChildRows("FK_GOODS_GOODS_TYPE");
                if( rowsFK_GOODS_GOODS_TYPE != null && rowsFK_GOODS_GOODS_TYPE.Length > 0 ){
                    foreach (DataRow rowFK_GOODS_GOODS_TYPE in rowsFK_GOODS_GOODS_TYPE)
                    {
                        System.Int32 childGoodsId = (System.Int32)rowFK_GOODS_GOODS_TYPE["GOODS_ID"];
                        CascadeRemove_Goods(dataset, childGoodsId);
                    }
                }
                DataRow[] rowsFK_GOODS_TYPE_VEHICLE_GOODS_TYPE = row.GetChildRows("FK_GOODS_TYPE_VEHICLE_GOODS_TYPE");
                if( rowsFK_GOODS_TYPE_VEHICLE_GOODS_TYPE != null && rowsFK_GOODS_TYPE_VEHICLE_GOODS_TYPE.Length > 0 ){
                    foreach (DataRow rowFK_GOODS_TYPE_VEHICLE_GOODS_TYPE in rowsFK_GOODS_TYPE_VEHICLE_GOODS_TYPE)
                    {
                        System.Int32 childGoodsTypeVehicleId = (System.Int32)rowFK_GOODS_TYPE_VEHICLE_GOODS_TYPE["GOODS_TYPE_VEHICLE_id"];
                        CascadeRemove_GoodsTypeVehicle(dataset, childGoodsTypeVehicleId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [GOODS_TYPE]
        ///</summary>
        public static void Compact_GoodsType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GOODS_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GOODS_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [GOODS_TYPE]
        ///</summary>
        public static void CompactFreshRecords_GoodsType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GOODS_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GOODS_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["GOODS_TYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [GOODS_TYPE_VEHICLE]
        ///</summary>
        public static void CascadeRemove_GoodsTypeVehicle(DataSet dataset, System.Int32 GoodsTypeVehicleId)
        {
            Debug.Assert(dataset.Tables.Contains("GOODS_TYPE_VEHICLE"));
            DataRow row = dataset.Tables["GOODS_TYPE_VEHICLE"].Rows.Find(GoodsTypeVehicleId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [GOODS_TYPE_VEHICLE]
        ///</summary>
        public static void Compact_GoodsTypeVehicle(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GOODS_TYPE_VEHICLE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GOODS_TYPE_VEHICLE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [GOODS_TYPE_VEHICLE]
        ///</summary>
        public static void CompactFreshRecords_GoodsTypeVehicle(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GOODS_TYPE_VEHICLE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GOODS_TYPE_VEHICLE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["GOODS_TYPE_VEHICLE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [GRAPHIC]
        ///</summary>
        public static void CascadeRemove_Graphic(DataSet dataset, System.Int32 GraphicId)
        {
            Debug.Assert(dataset.Tables.Contains("GRAPHIC"));
            DataRow row = dataset.Tables["GRAPHIC"].Rows.Find(GraphicId);
            if( row != null){
                DataRow[] rowsFK_BRIGADE_GRAPHIC = row.GetChildRows("FK_BRIGADE_GRAPHIC");
                if( rowsFK_BRIGADE_GRAPHIC != null && rowsFK_BRIGADE_GRAPHIC.Length > 0 ){
                    foreach (DataRow rowFK_BRIGADE_GRAPHIC in rowsFK_BRIGADE_GRAPHIC)
                    {
                        System.Int32 childBrigadeId = (System.Int32)rowFK_BRIGADE_GRAPHIC["BRIGADE_ID"];
                        CascadeRemove_Brigade(dataset, childBrigadeId);
                    }
                }
                DataRow[] rowsFK_RS_WAYOUT_GRAPHIC = row.GetChildRows("FK_RS_WAYOUT_GRAPHIC");
                if( rowsFK_RS_WAYOUT_GRAPHIC != null && rowsFK_RS_WAYOUT_GRAPHIC.Length > 0 ){
                    foreach (DataRow rowFK_RS_WAYOUT_GRAPHIC in rowsFK_RS_WAYOUT_GRAPHIC)
                    {
                        System.Int32 childRsWayoutId = (System.Int32)rowFK_RS_WAYOUT_GRAPHIC["RS_WAYOUT_ID"];
                        CascadeRemove_RsWayout(dataset, childRsWayoutId);
                    }
                }
                DataRow[] rowsGraphicShift_Graphic = row.GetChildRows("GraphicShift_Graphic");
                if( rowsGraphicShift_Graphic != null && rowsGraphicShift_Graphic.Length > 0 ){
                    foreach (DataRow rowGraphicShift_Graphic in rowsGraphicShift_Graphic)
                    {
                        System.Int32 childId = (System.Int32)rowGraphicShift_Graphic["ID"];
                        CascadeRemove_GraphicShift(dataset, childId);
                    }
                }
                DataRow[] rowsVehicle_Graphic = row.GetChildRows("Vehicle_Graphic");
                if( rowsVehicle_Graphic != null && rowsVehicle_Graphic.Length > 0 ){
                    foreach (DataRow rowVehicle_Graphic in rowsVehicle_Graphic)
                    {
                        System.Int32 childVehicleId = (System.Int32)rowVehicle_Graphic["VEHICLE_ID"];
                        CascadeRemove_Vehicle(dataset, childVehicleId);
                    }
                }
                DataRow[] rowsWayout_Graphic = row.GetChildRows("Wayout_Graphic");
                if( rowsWayout_Graphic != null && rowsWayout_Graphic.Length > 0 ){
                    foreach (DataRow rowWayout_Graphic in rowsWayout_Graphic)
                    {
                        System.Int32 childWayoutId = (System.Int32)rowWayout_Graphic["WAYOUT_ID"];
                        CascadeRemove_Wayout(dataset, childWayoutId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [GRAPHIC]
        ///</summary>
        public static void Compact_Graphic(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GRAPHIC"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GRAPHIC"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [GRAPHIC]
        ///</summary>
        public static void CompactFreshRecords_Graphic(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GRAPHIC"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GRAPHIC"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["GRAPHIC"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [GRAPHIC_SHIFT]
        ///</summary>
        public static void CascadeRemove_GraphicShift(DataSet dataset, System.Int32 Id)
        {
            Debug.Assert(dataset.Tables.Contains("GRAPHIC_SHIFT"));
            DataRow row = dataset.Tables["GRAPHIC_SHIFT"].Rows.Find(Id);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [GRAPHIC_SHIFT]
        ///</summary>
        public static void Compact_GraphicShift(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GRAPHIC_SHIFT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GRAPHIC_SHIFT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [GRAPHIC_SHIFT]
        ///</summary>
        public static void CompactFreshRecords_GraphicShift(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GRAPHIC_SHIFT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GRAPHIC_SHIFT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["GRAPHIC_SHIFT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [GUARD_EVENTS]
        ///</summary>
        public static void CascadeRemove_GuardEvents(DataSet dataset, System.Int32 GuardEventId)
        {
            Debug.Assert(dataset.Tables.Contains("GUARD_EVENTS"));
            DataRow row = dataset.Tables["GUARD_EVENTS"].Rows.Find(GuardEventId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [GUARD_EVENTS]
        ///</summary>
        public static void Compact_GuardEvents(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GUARD_EVENTS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GUARD_EVENTS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [GUARD_EVENTS]
        ///</summary>
        public static void CompactFreshRecords_GuardEvents(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("GUARD_EVENTS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["GUARD_EVENTS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["GUARD_EVENTS"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [Job_DelBadXY]
        ///</summary>
        public static void CascadeRemove_JobDelbadxy(DataSet dataset, System.Int32 JobDelbadxyId)
        {
            Debug.Assert(dataset.Tables.Contains("Job_DelBadXY"));
            DataRow row = dataset.Tables["Job_DelBadXY"].Rows.Find(JobDelbadxyId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [Job_DelBadXY]
        ///</summary>
        public static void Compact_JobDelbadxy(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("Job_DelBadXY"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["Job_DelBadXY"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [Job_DelBadXY]
        ///</summary>
        public static void CompactFreshRecords_JobDelbadxy(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("Job_DelBadXY"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["Job_DelBadXY"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["Job_DelBadXY"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [JOURNAL]
        ///</summary>
        public static void CascadeRemove_Journal(DataSet dataset, System.Int32 JournalId)
        {
            Debug.Assert(dataset.Tables.Contains("JOURNAL"));
            DataRow row = dataset.Tables["JOURNAL"].Rows.Find(JournalId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [JOURNAL]
        ///</summary>
        public static void Compact_Journal(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("JOURNAL"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["JOURNAL"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [JOURNAL]
        ///</summary>
        public static void CompactFreshRecords_Journal(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("JOURNAL"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["JOURNAL"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["JOURNAL"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [JOURNAL_DAY]
        ///</summary>
        public static void CascadeRemove_JournalDay(DataSet dataset, System.Int32 JournalDayId)
        {
            Debug.Assert(dataset.Tables.Contains("JOURNAL_DAY"));
            DataRow row = dataset.Tables["JOURNAL_DAY"].Rows.Find(JournalDayId);
            if( row != null){
                DataRow[] rowsFK_ORDER_TRIP_JOURNAL_DAY = row.GetChildRows("FK_ORDER_TRIP_JOURNAL_DAY");
                if( rowsFK_ORDER_TRIP_JOURNAL_DAY != null && rowsFK_ORDER_TRIP_JOURNAL_DAY.Length > 0 ){
                    foreach (DataRow rowFK_ORDER_TRIP_JOURNAL_DAY in rowsFK_ORDER_TRIP_JOURNAL_DAY)
                    {
                        System.Int32 childOrderTripId = (System.Int32)rowFK_ORDER_TRIP_JOURNAL_DAY["ORDER_TRIP_ID"];
                        CascadeRemove_OrderTrip(dataset, childOrderTripId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [JOURNAL_DAY]
        ///</summary>
        public static void Compact_JournalDay(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("JOURNAL_DAY"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["JOURNAL_DAY"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [JOURNAL_DAY]
        ///</summary>
        public static void CompactFreshRecords_JournalDay(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("JOURNAL_DAY"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["JOURNAL_DAY"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["JOURNAL_DAY"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [JOURNAL_DRIVER]
        ///</summary>
        public static void CascadeRemove_JournalDriver(DataSet dataset, System.Int32 JournalDriverId)
        {
            Debug.Assert(dataset.Tables.Contains("JOURNAL_DRIVER"));
            DataRow row = dataset.Tables["JOURNAL_DRIVER"].Rows.Find(JournalDriverId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [JOURNAL_DRIVER]
        ///</summary>
        public static void Compact_JournalDriver(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("JOURNAL_DRIVER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["JOURNAL_DRIVER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [JOURNAL_DRIVER]
        ///</summary>
        public static void CompactFreshRecords_JournalDriver(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("JOURNAL_DRIVER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["JOURNAL_DRIVER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["JOURNAL_DRIVER"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [JOURNAL_VEHICLE]
        ///</summary>
        public static void CascadeRemove_JournalVehicle(DataSet dataset, System.Int32 JournalVehicleId)
        {
            Debug.Assert(dataset.Tables.Contains("JOURNAL_VEHICLE"));
            DataRow row = dataset.Tables["JOURNAL_VEHICLE"].Rows.Find(JournalVehicleId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [JOURNAL_VEHICLE]
        ///</summary>
        public static void Compact_JournalVehicle(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("JOURNAL_VEHICLE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["JOURNAL_VEHICLE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [JOURNAL_VEHICLE]
        ///</summary>
        public static void CompactFreshRecords_JournalVehicle(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("JOURNAL_VEHICLE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["JOURNAL_VEHICLE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["JOURNAL_VEHICLE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [LOG_OPS_EVENTS]
        ///</summary>
        public static void CascadeRemove_LogOpsEvents(DataSet dataset, System.Int32 RecordId)
        {
            Debug.Assert(dataset.Tables.Contains("LOG_OPS_EVENTS"));
            DataRow row = dataset.Tables["LOG_OPS_EVENTS"].Rows.Find(RecordId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [LOG_OPS_EVENTS]
        ///</summary>
        public static void Compact_LogOpsEvents(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("LOG_OPS_EVENTS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["LOG_OPS_EVENTS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [LOG_OPS_EVENTS]
        ///</summary>
        public static void CompactFreshRecords_LogOpsEvents(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("LOG_OPS_EVENTS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["LOG_OPS_EVENTS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["LOG_OPS_EVENTS"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [LOG_VEHICLE_STATUS]
        ///</summary>
        public static void CascadeRemove_LogVehicleStatus(DataSet dataset, System.Int32 LogVehicleStatusId)
        {
            Debug.Assert(dataset.Tables.Contains("LOG_VEHICLE_STATUS"));
            DataRow row = dataset.Tables["LOG_VEHICLE_STATUS"].Rows.Find(LogVehicleStatusId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [LOG_VEHICLE_STATUS]
        ///</summary>
        public static void Compact_LogVehicleStatus(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("LOG_VEHICLE_STATUS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["LOG_VEHICLE_STATUS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [LOG_VEHICLE_STATUS]
        ///</summary>
        public static void CompactFreshRecords_LogVehicleStatus(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("LOG_VEHICLE_STATUS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["LOG_VEHICLE_STATUS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["LOG_VEHICLE_STATUS"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [LOG_WAYBILL_HEADER_STATUS]
        ///</summary>
        public static void CascadeRemove_LogWaybillHeaderStatus(DataSet dataset, System.Int32 LogWaybillHeaderStatusId)
        {
            Debug.Assert(dataset.Tables.Contains("LOG_WAYBILL_HEADER_STATUS"));
            DataRow row = dataset.Tables["LOG_WAYBILL_HEADER_STATUS"].Rows.Find(LogWaybillHeaderStatusId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [LOG_WAYBILL_HEADER_STATUS]
        ///</summary>
        public static void Compact_LogWaybillHeaderStatus(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("LOG_WAYBILL_HEADER_STATUS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["LOG_WAYBILL_HEADER_STATUS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [LOG_WAYBILL_HEADER_STATUS]
        ///</summary>
        public static void CompactFreshRecords_LogWaybillHeaderStatus(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("LOG_WAYBILL_HEADER_STATUS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["LOG_WAYBILL_HEADER_STATUS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["LOG_WAYBILL_HEADER_STATUS"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [LOGISTIC_ADDRESS]
        ///</summary>
        public static void CascadeRemove_LogisticAddress(DataSet dataset, System.Int32 LogisticAddressId)
        {
            Debug.Assert(dataset.Tables.Contains("LOGISTIC_ADDRESS"));
            DataRow row = dataset.Tables["LOGISTIC_ADDRESS"].Rows.Find(LogisticAddressId);
            if( row != null){
                DataRow[] rowsFK_LOGISTIC_ORDER_LOGISTIC_ADDRESS = row.GetChildRows("FK_LOGISTIC_ORDER_LOGISTIC_ADDRESS");
                if( rowsFK_LOGISTIC_ORDER_LOGISTIC_ADDRESS != null && rowsFK_LOGISTIC_ORDER_LOGISTIC_ADDRESS.Length > 0 ){
                    foreach (DataRow rowFK_LOGISTIC_ORDER_LOGISTIC_ADDRESS in rowsFK_LOGISTIC_ORDER_LOGISTIC_ADDRESS)
                    {
                        System.Int32 childLogisticOrderId = (System.Int32)rowFK_LOGISTIC_ORDER_LOGISTIC_ADDRESS["LOGISTIC_ORDER_ID"];
                        CascadeRemove_LogisticOrder(dataset, childLogisticOrderId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [LOGISTIC_ADDRESS]
        ///</summary>
        public static void Compact_LogisticAddress(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("LOGISTIC_ADDRESS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["LOGISTIC_ADDRESS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [LOGISTIC_ADDRESS]
        ///</summary>
        public static void CompactFreshRecords_LogisticAddress(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("LOGISTIC_ADDRESS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["LOGISTIC_ADDRESS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["LOGISTIC_ADDRESS"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [LOGISTIC_ORDER]
        ///</summary>
        public static void CascadeRemove_LogisticOrder(DataSet dataset, System.Int32 LogisticOrderId)
        {
            Debug.Assert(dataset.Tables.Contains("LOGISTIC_ORDER"));
            DataRow row = dataset.Tables["LOGISTIC_ORDER"].Rows.Find(LogisticOrderId);
            if( row != null){
                DataRow[] rowsFK_GOODS_LOGISTIC_ORDER_LOGISTIC_ORDER = row.GetChildRows("FK_GOODS_LOGISTIC_ORDER_LOGISTIC_ORDER");
                if( rowsFK_GOODS_LOGISTIC_ORDER_LOGISTIC_ORDER != null && rowsFK_GOODS_LOGISTIC_ORDER_LOGISTIC_ORDER.Length > 0 ){
                    foreach (DataRow rowFK_GOODS_LOGISTIC_ORDER_LOGISTIC_ORDER in rowsFK_GOODS_LOGISTIC_ORDER_LOGISTIC_ORDER)
                    {
                        System.Int32 childGoodsLogisticOrderId = (System.Int32)rowFK_GOODS_LOGISTIC_ORDER_LOGISTIC_ORDER["GOODS_LOGISTIC_ORDER_ID"];
                        CascadeRemove_GoodsLogisticOrder(dataset, childGoodsLogisticOrderId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [LOGISTIC_ORDER]
        ///</summary>
        public static void Compact_LogisticOrder(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("LOGISTIC_ORDER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["LOGISTIC_ORDER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [LOGISTIC_ORDER]
        ///</summary>
        public static void CompactFreshRecords_LogisticOrder(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("LOGISTIC_ORDER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["LOGISTIC_ORDER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["LOGISTIC_ORDER"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [MAP_VERTEX]
        ///</summary>
        public static void CascadeRemove_MapVertex(DataSet dataset, System.Int32 VertexId)
        {
            Debug.Assert(dataset.Tables.Contains("MAP_VERTEX"));
            DataRow row = dataset.Tables["MAP_VERTEX"].Rows.Find(VertexId);
            if( row != null){
                DataRow[] rowsGeoSegmentVeretx_MapVeretx = row.GetChildRows("GeoSegmentVeretx_MapVeretx");
                if( rowsGeoSegmentVeretx_MapVeretx != null && rowsGeoSegmentVeretx_MapVeretx.Length > 0 ){
                    foreach (DataRow rowGeoSegmentVeretx_MapVeretx in rowsGeoSegmentVeretx_MapVeretx)
                    {
                        System.Int32 childGeoSegmentVertexId = (System.Int32)rowGeoSegmentVeretx_MapVeretx["GEO_SEGMENT_VERTEX_ID"];
                        CascadeRemove_GeoSegmentVertex(dataset, childGeoSegmentVertexId);
                    }
                }
                DataRow[] rowsPoint_MapVeretx = row.GetChildRows("Point_MapVeretx");
                if( rowsPoint_MapVeretx != null && rowsPoint_MapVeretx.Length > 0 ){
                    foreach (DataRow rowPoint_MapVeretx in rowsPoint_MapVeretx)
                    {
                        System.Int32 childPointId = (System.Int32)rowPoint_MapVeretx["POINT_ID"];
                        CascadeRemove_Point(dataset, childPointId);
                    }
                }
                DataRow[] rowsWebLineVertex_Vertex = row.GetChildRows("WebLineVertex_Vertex");
                if( rowsWebLineVertex_Vertex != null && rowsWebLineVertex_Vertex.Length > 0 ){
                    foreach (DataRow rowWebLineVertex_Vertex in rowsWebLineVertex_Vertex)
                    {
                        System.Int32 childLineVertexId = (System.Int32)rowWebLineVertex_Vertex["LINE_VERTEX_ID"];
                        CascadeRemove_WebLineVertex(dataset, childLineVertexId);
                    }
                }
                DataRow[] rowsWebPoint_Vertex = row.GetChildRows("WebPoint_Vertex");
                if( rowsWebPoint_Vertex != null && rowsWebPoint_Vertex.Length > 0 ){
                    foreach (DataRow rowWebPoint_Vertex in rowsWebPoint_Vertex)
                    {
                        System.Int32 childPointId = (System.Int32)rowWebPoint_Vertex["POINT_ID"];
                        CascadeRemove_WebPoint(dataset, childPointId);
                    }
                }
                DataRow[] rowsZonePrimitiveVertex_MapVeretx = row.GetChildRows("ZonePrimitiveVertex_MapVeretx");
                if( rowsZonePrimitiveVertex_MapVeretx != null && rowsZonePrimitiveVertex_MapVeretx.Length > 0 ){
                    foreach (DataRow rowZonePrimitiveVertex_MapVeretx in rowsZonePrimitiveVertex_MapVeretx)
                    {
                        System.Int32 childPrimitiveVertexId = (System.Int32)rowZonePrimitiveVertex_MapVeretx["PRIMITIVE_VERTEX_ID"];
                        CascadeRemove_ZonePrimitiveVertex(dataset, childPrimitiveVertexId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [MAP_VERTEX]
        ///</summary>
        public static void Compact_MapVertex(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MAP_VERTEX"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MAP_VERTEX"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [MAP_VERTEX]
        ///</summary>
        public static void CompactFreshRecords_MapVertex(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MAP_VERTEX"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MAP_VERTEX"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["MAP_VERTEX"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [MAPS]
        ///</summary>
        public static void CascadeRemove_Maps(DataSet dataset, System.Int32 MapId)
        {
            Debug.Assert(dataset.Tables.Contains("MAPS"));
            DataRow row = dataset.Tables["MAPS"].Rows.Find(MapId);
            if( row != null){
                DataRow[] rowsMapVeretx_Maps = row.GetChildRows("MapVeretx_Maps");
                if( rowsMapVeretx_Maps != null && rowsMapVeretx_Maps.Length > 0 ){
                    foreach (DataRow rowMapVeretx_Maps in rowsMapVeretx_Maps)
                    {
                        System.Int32 childVertexId = (System.Int32)rowMapVeretx_Maps["VERTEX_ID"];
                        CascadeRemove_MapVertex(dataset, childVertexId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [MAPS]
        ///</summary>
        public static void Compact_Maps(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MAPS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MAPS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [MAPS]
        ///</summary>
        public static void CompactFreshRecords_Maps(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MAPS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MAPS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["MAPS"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [MEDIA]
        ///</summary>
        public static void CascadeRemove_Media(DataSet dataset, System.Int32 MediaId)
        {
            Debug.Assert(dataset.Tables.Contains("MEDIA"));
            DataRow row = dataset.Tables["MEDIA"].Rows.Find(MediaId);
            if( row != null){
                DataRow[] rowsMediaAcceptors_Media = row.GetChildRows("MediaAcceptors_Media");
                if( rowsMediaAcceptors_Media != null && rowsMediaAcceptors_Media.Length > 0 ){
                    foreach (DataRow rowMediaAcceptors_Media in rowsMediaAcceptors_Media)
                    {
                        System.Int32 childMaId = (System.Int32)rowMediaAcceptors_Media["MA_ID"];
                        CascadeRemove_MediaAcceptors(dataset, childMaId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [MEDIA]
        ///</summary>
        public static void Compact_Media(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MEDIA"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MEDIA"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [MEDIA]
        ///</summary>
        public static void CompactFreshRecords_Media(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MEDIA"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MEDIA"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["MEDIA"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [MEDIA_ACCEPTORS]
        ///</summary>
        public static void CascadeRemove_MediaAcceptors(DataSet dataset, System.Int32 MaId)
        {
            Debug.Assert(dataset.Tables.Contains("MEDIA_ACCEPTORS"));
            DataRow row = dataset.Tables["MEDIA_ACCEPTORS"].Rows.Find(MaId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [MEDIA_ACCEPTORS]
        ///</summary>
        public static void Compact_MediaAcceptors(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MEDIA_ACCEPTORS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MEDIA_ACCEPTORS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [MEDIA_ACCEPTORS]
        ///</summary>
        public static void CompactFreshRecords_MediaAcceptors(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MEDIA_ACCEPTORS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MEDIA_ACCEPTORS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["MEDIA_ACCEPTORS"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [MEDIA_TYPE]
        ///</summary>
        public static void CascadeRemove_MediaType(DataSet dataset, System.Byte Id)
        {
            Debug.Assert(dataset.Tables.Contains("MEDIA_TYPE"));
            DataRow row = dataset.Tables["MEDIA_TYPE"].Rows.Find(Id);
            if( row != null){
                DataRow[] rowsMedia_MediaType = row.GetChildRows("Media_MediaType");
                if( rowsMedia_MediaType != null && rowsMedia_MediaType.Length > 0 ){
                    foreach (DataRow rowMedia_MediaType in rowsMedia_MediaType)
                    {
                        System.Int32 childMediaId = (System.Int32)rowMedia_MediaType["MEDIA_ID"];
                        CascadeRemove_Media(dataset, childMediaId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [MEDIA_TYPE]
        ///</summary>
        public static void Compact_MediaType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MEDIA_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MEDIA_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [MEDIA_TYPE]
        ///</summary>
        public static void CompactFreshRecords_MediaType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MEDIA_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MEDIA_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["MEDIA_TYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [MESSAGE]
        ///</summary>
        public static void CascadeRemove_Message(DataSet dataset, System.Int32 MessageId)
        {
            Debug.Assert(dataset.Tables.Contains("MESSAGE"));
            DataRow row = dataset.Tables["MESSAGE"].Rows.Find(MessageId);
            if( row != null){
                DataRow[] rowsMessageField_Message = row.GetChildRows("MessageField_Message");
                if( rowsMessageField_Message != null && rowsMessageField_Message.Length > 0 ){
                    foreach (DataRow rowMessageField_Message in rowsMessageField_Message)
                    {
                        System.Int32 childMessageFieldId = (System.Int32)rowMessageField_Message["MESSAGE_FIELD_ID"];
                        CascadeRemove_MessageField(dataset, childMessageFieldId);
                    }
                }
                DataRow[] rowsMessageOperator_Message = row.GetChildRows("MessageOperator_Message");
                if( rowsMessageOperator_Message != null && rowsMessageOperator_Message.Length > 0 ){
                    foreach (DataRow rowMessageOperator_Message in rowsMessageOperator_Message)
                    {
                        System.Int32 childMessageOperatorId = (System.Int32)rowMessageOperator_Message["MESSAGE_OPERATOR_ID"];
                        CascadeRemove_MessageOperator(dataset, childMessageOperatorId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [MESSAGE]
        ///</summary>
        public static void Compact_Message(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MESSAGE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MESSAGE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [MESSAGE]
        ///</summary>
        public static void CompactFreshRecords_Message(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MESSAGE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MESSAGE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["MESSAGE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [MESSAGE_BOARD]
        ///</summary>
        public static void CascadeRemove_MessageBoard(DataSet dataset, System.Int32 Id)
        {
            Debug.Assert(dataset.Tables.Contains("MESSAGE_BOARD"));
            DataRow row = dataset.Tables["MESSAGE_BOARD"].Rows.Find(Id);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [MESSAGE_BOARD]
        ///</summary>
        public static void Compact_MessageBoard(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MESSAGE_BOARD"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MESSAGE_BOARD"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [MESSAGE_BOARD]
        ///</summary>
        public static void CompactFreshRecords_MessageBoard(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MESSAGE_BOARD"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MESSAGE_BOARD"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["MESSAGE_BOARD"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [MESSAGE_FIELD]
        ///</summary>
        public static void CascadeRemove_MessageField(DataSet dataset, System.Int32 MessageFieldId)
        {
            Debug.Assert(dataset.Tables.Contains("MESSAGE_FIELD"));
            DataRow row = dataset.Tables["MESSAGE_FIELD"].Rows.Find(MessageFieldId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [MESSAGE_FIELD]
        ///</summary>
        public static void Compact_MessageField(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MESSAGE_FIELD"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MESSAGE_FIELD"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [MESSAGE_FIELD]
        ///</summary>
        public static void CompactFreshRecords_MessageField(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MESSAGE_FIELD"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MESSAGE_FIELD"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["MESSAGE_FIELD"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [MESSAGE_OPERATOR]
        ///</summary>
        public static void CascadeRemove_MessageOperator(DataSet dataset, System.Int32 MessageOperatorId)
        {
            Debug.Assert(dataset.Tables.Contains("MESSAGE_OPERATOR"));
            DataRow row = dataset.Tables["MESSAGE_OPERATOR"].Rows.Find(MessageOperatorId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [MESSAGE_OPERATOR]
        ///</summary>
        public static void Compact_MessageOperator(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MESSAGE_OPERATOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MESSAGE_OPERATOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [MESSAGE_OPERATOR]
        ///</summary>
        public static void CompactFreshRecords_MessageOperator(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MESSAGE_OPERATOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MESSAGE_OPERATOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["MESSAGE_OPERATOR"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [MESSAGE_TEMPLATE]
        ///</summary>
        public static void CascadeRemove_MessageTemplate(DataSet dataset, System.Int32 MessageTemplateId)
        {
            Debug.Assert(dataset.Tables.Contains("MESSAGE_TEMPLATE"));
            DataRow row = dataset.Tables["MESSAGE_TEMPLATE"].Rows.Find(MessageTemplateId);
            if( row != null){
                DataRow[] rowsMessage_MessageTemplate = row.GetChildRows("Message_MessageTemplate");
                if( rowsMessage_MessageTemplate != null && rowsMessage_MessageTemplate.Length > 0 ){
                    foreach (DataRow rowMessage_MessageTemplate in rowsMessage_MessageTemplate)
                    {
                        System.Int32 childMessageId = (System.Int32)rowMessage_MessageTemplate["MESSAGE_ID"];
                        CascadeRemove_Message(dataset, childMessageId);
                    }
                }
                DataRow[] rowsMessageTemplateField_MessageTemplate = row.GetChildRows("MessageTemplateField_MessageTemplate");
                if( rowsMessageTemplateField_MessageTemplate != null && rowsMessageTemplateField_MessageTemplate.Length > 0 ){
                    foreach (DataRow rowMessageTemplateField_MessageTemplate in rowsMessageTemplateField_MessageTemplate)
                    {
                        System.Int32 childMessageTemplateFieldId = (System.Int32)rowMessageTemplateField_MessageTemplate["MESSAGE_TEMPLATE_FIELD_ID"];
                        CascadeRemove_MessageTemplateField(dataset, childMessageTemplateFieldId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [MESSAGE_TEMPLATE]
        ///</summary>
        public static void Compact_MessageTemplate(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MESSAGE_TEMPLATE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MESSAGE_TEMPLATE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [MESSAGE_TEMPLATE]
        ///</summary>
        public static void CompactFreshRecords_MessageTemplate(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MESSAGE_TEMPLATE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MESSAGE_TEMPLATE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["MESSAGE_TEMPLATE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [MESSAGE_TEMPLATE_FIELD]
        ///</summary>
        public static void CascadeRemove_MessageTemplateField(DataSet dataset, System.Int32 MessageTemplateFieldId)
        {
            Debug.Assert(dataset.Tables.Contains("MESSAGE_TEMPLATE_FIELD"));
            DataRow row = dataset.Tables["MESSAGE_TEMPLATE_FIELD"].Rows.Find(MessageTemplateFieldId);
            if( row != null){
                DataRow[] rowsMessageField_MessageTemplateField = row.GetChildRows("MessageField_MessageTemplateField");
                if( rowsMessageField_MessageTemplateField != null && rowsMessageField_MessageTemplateField.Length > 0 ){
                    foreach (DataRow rowMessageField_MessageTemplateField in rowsMessageField_MessageTemplateField)
                    {
                        System.Int32 childMessageFieldId = (System.Int32)rowMessageField_MessageTemplateField["MESSAGE_FIELD_ID"];
                        CascadeRemove_MessageField(dataset, childMessageFieldId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [MESSAGE_TEMPLATE_FIELD]
        ///</summary>
        public static void Compact_MessageTemplateField(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MESSAGE_TEMPLATE_FIELD"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MESSAGE_TEMPLATE_FIELD"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [MESSAGE_TEMPLATE_FIELD]
        ///</summary>
        public static void CompactFreshRecords_MessageTemplateField(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("MESSAGE_TEMPLATE_FIELD"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["MESSAGE_TEMPLATE_FIELD"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["MESSAGE_TEMPLATE_FIELD"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATOR]
        ///</summary>
        public static void CascadeRemove_Operator(DataSet dataset, System.Int32 OperatorId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR"));
            DataRow row = dataset.Tables["OPERATOR"].Rows.Find(OperatorId);
            if( row != null){
                DataRow[] rowsFK_OPERATOR_PROFILE_OPERATOR = row.GetChildRows("FK_OPERATOR_PROFILE_OPERATOR");
                if( rowsFK_OPERATOR_PROFILE_OPERATOR != null && rowsFK_OPERATOR_PROFILE_OPERATOR.Length > 0 ){
                    foreach (DataRow rowFK_OPERATOR_PROFILE_OPERATOR in rowsFK_OPERATOR_PROFILE_OPERATOR)
                    {
                        System.Int32 childOperatorProfileId = (System.Int32)rowFK_OPERATOR_PROFILE_OPERATOR["OPERATOR_PROFILE_ID"];
                        CascadeRemove_OperatorProfile(dataset, childOperatorProfileId);
                    }
                }
                DataRow[] rowsLogOpsEvents_Operator = row.GetChildRows("LogOpsEvents_Operator");
                if( rowsLogOpsEvents_Operator != null && rowsLogOpsEvents_Operator.Length > 0 ){
                    foreach (DataRow rowLogOpsEvents_Operator in rowsLogOpsEvents_Operator)
                    {
                        System.Int32 childRecordId = (System.Int32)rowLogOpsEvents_Operator["RECORD_ID"];
                        CascadeRemove_LogOpsEvents(dataset, childRecordId);
                    }
                }
                DataRow[] rowsLogVehicleStatus_Operator = row.GetChildRows("LogVehicleStatus_Operator");
                if( rowsLogVehicleStatus_Operator != null && rowsLogVehicleStatus_Operator.Length > 0 ){
                    foreach (DataRow rowLogVehicleStatus_Operator in rowsLogVehicleStatus_Operator)
                    {
                        System.Int32 childLogVehicleStatusId = (System.Int32)rowLogVehicleStatus_Operator["LOG_VEHICLE_STATUS_ID"];
                        CascadeRemove_LogVehicleStatus(dataset, childLogVehicleStatusId);
                    }
                }
                DataRow[] rowsLogWaybillHeaderStatus_Operator = row.GetChildRows("LogWaybillHeaderStatus_Operator");
                if( rowsLogWaybillHeaderStatus_Operator != null && rowsLogWaybillHeaderStatus_Operator.Length > 0 ){
                    foreach (DataRow rowLogWaybillHeaderStatus_Operator in rowsLogWaybillHeaderStatus_Operator)
                    {
                        System.Int32 childLogWaybillHeaderStatusId = (System.Int32)rowLogWaybillHeaderStatus_Operator["LOG_WAYBILL_HEADER_STATUS_ID"];
                        CascadeRemove_LogWaybillHeaderStatus(dataset, childLogWaybillHeaderStatusId);
                    }
                }
                DataRow[] rowsMessageOperator_Operator = row.GetChildRows("MessageOperator_Operator");
                if( rowsMessageOperator_Operator != null && rowsMessageOperator_Operator.Length > 0 ){
                    foreach (DataRow rowMessageOperator_Operator in rowsMessageOperator_Operator)
                    {
                        System.Int32 childMessageOperatorId = (System.Int32)rowMessageOperator_Operator["MESSAGE_OPERATOR_ID"];
                        CascadeRemove_MessageOperator(dataset, childMessageOperatorId);
                    }
                }
                DataRow[] rowsOperatorDepartment_Operator = row.GetChildRows("OperatorDepartment_Operator");
                if( rowsOperatorDepartment_Operator != null && rowsOperatorDepartment_Operator.Length > 0 ){
                    foreach (DataRow rowOperatorDepartment_Operator in rowsOperatorDepartment_Operator)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorDepartment_Operator["OPERATOR_ID"];
                        System.Int32 childDepartmentId = (System.Int32)rowOperatorDepartment_Operator["DEPARTMENT_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorDepartment_Operator["RIGHT_ID"];
                        CascadeRemove_OperatorDepartment(dataset, childOperatorId, childDepartmentId, childRightId);
                    }
                }
                DataRow[] rowsOperatorDriver_Operator = row.GetChildRows("OperatorDriver_Operator");
                if( rowsOperatorDriver_Operator != null && rowsOperatorDriver_Operator.Length > 0 ){
                    foreach (DataRow rowOperatorDriver_Operator in rowsOperatorDriver_Operator)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorDriver_Operator["OPERATOR_ID"];
                        System.Int32 childDriverId = (System.Int32)rowOperatorDriver_Operator["DRIVER_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorDriver_Operator["RIGHT_ID"];
                        CascadeRemove_OperatorDriver(dataset, childOperatorId, childDriverId, childRightId);
                    }
                }
                DataRow[] rowsOperatorDriverGroup_Operator = row.GetChildRows("OperatorDriverGroup_Operator");
                if( rowsOperatorDriverGroup_Operator != null && rowsOperatorDriverGroup_Operator.Length > 0 ){
                    foreach (DataRow rowOperatorDriverGroup_Operator in rowsOperatorDriverGroup_Operator)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorDriverGroup_Operator["OPERATOR_ID"];
                        System.Int32 childDrivergroupId = (System.Int32)rowOperatorDriverGroup_Operator["DRIVERGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorDriverGroup_Operator["RIGHT_ID"];
                        CascadeRemove_OperatorDrivergroup(dataset, childOperatorId, childDrivergroupId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupOperator_Operator = row.GetChildRows("OperatorGroupOperator_Operator");
                if( rowsOperatorGroupOperator_Operator != null && rowsOperatorGroupOperator_Operator.Length > 0 ){
                    foreach (DataRow rowOperatorGroupOperator_Operator in rowsOperatorGroupOperator_Operator)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupOperator_Operator["OPERATORGROUP_ID"];
                        System.Int32 childOperatorId = (System.Int32)rowOperatorGroupOperator_Operator["OPERATOR_ID"];
                        CascadeRemove_OperatorgroupOperator(dataset, childOperatorgroupId, childOperatorId);
                    }
                }
                DataRow[] rowsOperatorReport_Operator = row.GetChildRows("OperatorReport_Operator");
                if( rowsOperatorReport_Operator != null && rowsOperatorReport_Operator.Length > 0 ){
                    foreach (DataRow rowOperatorReport_Operator in rowsOperatorReport_Operator)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorReport_Operator["OPERATOR_ID"];
                        System.Int32 childReportId = (System.Int32)rowOperatorReport_Operator["REPORT_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorReport_Operator["RIGHT_ID"];
                        CascadeRemove_OperatorReport(dataset, childOperatorId, childReportId, childRightId);
                    }
                }
                DataRow[] rowsOperatorRoute_Operator = row.GetChildRows("OperatorRoute_Operator");
                if( rowsOperatorRoute_Operator != null && rowsOperatorRoute_Operator.Length > 0 ){
                    foreach (DataRow rowOperatorRoute_Operator in rowsOperatorRoute_Operator)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorRoute_Operator["OPERATOR_ID"];
                        System.Int32 childRouteId = (System.Int32)rowOperatorRoute_Operator["ROUTE_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorRoute_Operator["RIGHT_ID"];
                        CascadeRemove_OperatorRoute(dataset, childOperatorId, childRouteId, childRightId);
                    }
                }
                DataRow[] rowsOperatorRouteGroup_Operator = row.GetChildRows("OperatorRouteGroup_Operator");
                if( rowsOperatorRouteGroup_Operator != null && rowsOperatorRouteGroup_Operator.Length > 0 ){
                    foreach (DataRow rowOperatorRouteGroup_Operator in rowsOperatorRouteGroup_Operator)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorRouteGroup_Operator["OPERATOR_ID"];
                        System.Int32 childRoutegroupId = (System.Int32)rowOperatorRouteGroup_Operator["ROUTEGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorRouteGroup_Operator["RIGHT_ID"];
                        CascadeRemove_OperatorRoutegroup(dataset, childOperatorId, childRoutegroupId, childRightId);
                    }
                }
                DataRow[] rowsOperatorVehicle_Operator = row.GetChildRows("OperatorVehicle_Operator");
                if( rowsOperatorVehicle_Operator != null && rowsOperatorVehicle_Operator.Length > 0 ){
                    foreach (DataRow rowOperatorVehicle_Operator in rowsOperatorVehicle_Operator)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorVehicle_Operator["OPERATOR_ID"];
                        System.Int32 childVehicleId = (System.Int32)rowOperatorVehicle_Operator["VEHICLE_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorVehicle_Operator["RIGHT_ID"];
                        CascadeRemove_OperatorVehicle(dataset, childOperatorId, childVehicleId, childRightId);
                    }
                }
                DataRow[] rowsOperatorVehicleGroup_Operator = row.GetChildRows("OperatorVehicleGroup_Operator");
                if( rowsOperatorVehicleGroup_Operator != null && rowsOperatorVehicleGroup_Operator.Length > 0 ){
                    foreach (DataRow rowOperatorVehicleGroup_Operator in rowsOperatorVehicleGroup_Operator)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorVehicleGroup_Operator["OPERATOR_ID"];
                        System.Int32 childVehiclegroupId = (System.Int32)rowOperatorVehicleGroup_Operator["VEHICLEGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorVehicleGroup_Operator["RIGHT_ID"];
                        CascadeRemove_OperatorVehiclegroup(dataset, childOperatorId, childVehiclegroupId, childRightId);
                    }
                }
                DataRow[] rowsOperatorZone_Operator = row.GetChildRows("OperatorZone_Operator");
                if( rowsOperatorZone_Operator != null && rowsOperatorZone_Operator.Length > 0 ){
                    foreach (DataRow rowOperatorZone_Operator in rowsOperatorZone_Operator)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorZone_Operator["OPERATOR_ID"];
                        System.Int32 childZoneId = (System.Int32)rowOperatorZone_Operator["ZONE_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorZone_Operator["RIGHT_ID"];
                        CascadeRemove_OperatorZone(dataset, childOperatorId, childZoneId, childRightId);
                    }
                }
                DataRow[] rowsOperatorZoneGroup_Operator = row.GetChildRows("OperatorZoneGroup_Operator");
                if( rowsOperatorZoneGroup_Operator != null && rowsOperatorZoneGroup_Operator.Length > 0 ){
                    foreach (DataRow rowOperatorZoneGroup_Operator in rowsOperatorZoneGroup_Operator)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorZoneGroup_Operator["OPERATOR_ID"];
                        System.Int32 childZonegroupId = (System.Int32)rowOperatorZoneGroup_Operator["ZONEGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorZoneGroup_Operator["RIGHT_ID"];
                        CascadeRemove_OperatorZonegroup(dataset, childOperatorId, childZonegroupId, childRightId);
                    }
                }
                DataRow[] rowsRightOperator_Operator = row.GetChildRows("RightOperator_Operator");
                if( rowsRightOperator_Operator != null && rowsRightOperator_Operator.Length > 0 ){
                    foreach (DataRow rowRightOperator_Operator in rowsRightOperator_Operator)
                    {
                        System.Int32 childRightId = (System.Int32)rowRightOperator_Operator["RIGHT_ID"];
                        System.Int32 childOperatorId = (System.Int32)rowRightOperator_Operator["OPERATOR_ID"];
                        CascadeRemove_RightOperator(dataset, childRightId, childOperatorId);
                    }
                }
                DataRow[] rowsWebLine_Operator = row.GetChildRows("WebLine_Operator");
                if( rowsWebLine_Operator != null && rowsWebLine_Operator.Length > 0 ){
                    foreach (DataRow rowWebLine_Operator in rowsWebLine_Operator)
                    {
                        System.Int32 childLineId = (System.Int32)rowWebLine_Operator["LINE_ID"];
                        CascadeRemove_WebLine(dataset, childLineId);
                    }
                }
                DataRow[] rowsWebPoint_Operator = row.GetChildRows("WebPoint_Operator");
                if( rowsWebPoint_Operator != null && rowsWebPoint_Operator.Length > 0 ){
                    foreach (DataRow rowWebPoint_Operator in rowsWebPoint_Operator)
                    {
                        System.Int32 childPointId = (System.Int32)rowWebPoint_Operator["POINT_ID"];
                        CascadeRemove_WebPoint(dataset, childPointId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR]
        ///</summary>
        public static void Compact_Operator(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR]
        ///</summary>
        public static void CompactFreshRecords_Operator(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATOR"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATOR_DEPARTMENT]
        ///</summary>
        public static void CascadeRemove_OperatorDepartment(DataSet dataset, System.Int32 OperatorId, System.Int32 DepartmentId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_DEPARTMENT"));
            DataRow row = dataset.Tables["OPERATOR_DEPARTMENT"].Rows.Find(new object[]{OperatorId, DepartmentId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_DEPARTMENT]
        ///</summary>
        public static void Compact_OperatorDepartment(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_DEPARTMENT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_DEPARTMENT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_DEPARTMENT]
        ///</summary>
        public static void CompactFreshRecords_OperatorDepartment(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_DEPARTMENT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_DEPARTMENT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATOR_DEPARTMENT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATOR_DRIVER]
        ///</summary>
        public static void CascadeRemove_OperatorDriver(DataSet dataset, System.Int32 OperatorId, System.Int32 DriverId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_DRIVER"));
            DataRow row = dataset.Tables["OPERATOR_DRIVER"].Rows.Find(new object[]{OperatorId, DriverId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_DRIVER]
        ///</summary>
        public static void Compact_OperatorDriver(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_DRIVER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_DRIVER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_DRIVER]
        ///</summary>
        public static void CompactFreshRecords_OperatorDriver(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_DRIVER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_DRIVER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATOR_DRIVER"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATOR_DRIVERGROUP]
        ///</summary>
        public static void CascadeRemove_OperatorDrivergroup(DataSet dataset, System.Int32 OperatorId, System.Int32 DrivergroupId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_DRIVERGROUP"));
            DataRow row = dataset.Tables["OPERATOR_DRIVERGROUP"].Rows.Find(new object[]{OperatorId, DrivergroupId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_DRIVERGROUP]
        ///</summary>
        public static void Compact_OperatorDrivergroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_DRIVERGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_DRIVERGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_DRIVERGROUP]
        ///</summary>
        public static void CompactFreshRecords_OperatorDrivergroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_DRIVERGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_DRIVERGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATOR_DRIVERGROUP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATOR_PROFILE]
        ///</summary>
        public static void CascadeRemove_OperatorProfile(DataSet dataset, System.Int32 OperatorProfileId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_PROFILE"));
            DataRow row = dataset.Tables["OPERATOR_PROFILE"].Rows.Find(OperatorProfileId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_PROFILE]
        ///</summary>
        public static void Compact_OperatorProfile(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_PROFILE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_PROFILE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_PROFILE]
        ///</summary>
        public static void CompactFreshRecords_OperatorProfile(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_PROFILE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_PROFILE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATOR_PROFILE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATOR_REPORT]
        ///</summary>
        public static void CascadeRemove_OperatorReport(DataSet dataset, System.Int32 OperatorId, System.Int32 ReportId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_REPORT"));
            DataRow row = dataset.Tables["OPERATOR_REPORT"].Rows.Find(new object[]{OperatorId, ReportId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_REPORT]
        ///</summary>
        public static void Compact_OperatorReport(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_REPORT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_REPORT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_REPORT]
        ///</summary>
        public static void CompactFreshRecords_OperatorReport(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_REPORT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_REPORT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATOR_REPORT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATOR_ROUTE]
        ///</summary>
        public static void CascadeRemove_OperatorRoute(DataSet dataset, System.Int32 OperatorId, System.Int32 RouteId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_ROUTE"));
            DataRow row = dataset.Tables["OPERATOR_ROUTE"].Rows.Find(new object[]{OperatorId, RouteId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_ROUTE]
        ///</summary>
        public static void Compact_OperatorRoute(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_ROUTE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_ROUTE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_ROUTE]
        ///</summary>
        public static void CompactFreshRecords_OperatorRoute(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_ROUTE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_ROUTE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATOR_ROUTE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATOR_ROUTEGROUP]
        ///</summary>
        public static void CascadeRemove_OperatorRoutegroup(DataSet dataset, System.Int32 OperatorId, System.Int32 RoutegroupId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_ROUTEGROUP"));
            DataRow row = dataset.Tables["OPERATOR_ROUTEGROUP"].Rows.Find(new object[]{OperatorId, RoutegroupId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_ROUTEGROUP]
        ///</summary>
        public static void Compact_OperatorRoutegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_ROUTEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_ROUTEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_ROUTEGROUP]
        ///</summary>
        public static void CompactFreshRecords_OperatorRoutegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_ROUTEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_ROUTEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATOR_ROUTEGROUP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATOR_VEHICLE]
        ///</summary>
        public static void CascadeRemove_OperatorVehicle(DataSet dataset, System.Int32 OperatorId, System.Int32 VehicleId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_VEHICLE"));
            DataRow row = dataset.Tables["OPERATOR_VEHICLE"].Rows.Find(new object[]{OperatorId, VehicleId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_VEHICLE]
        ///</summary>
        public static void Compact_OperatorVehicle(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_VEHICLE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_VEHICLE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_VEHICLE]
        ///</summary>
        public static void CompactFreshRecords_OperatorVehicle(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_VEHICLE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_VEHICLE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATOR_VEHICLE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATOR_VEHICLEGROUP]
        ///</summary>
        public static void CascadeRemove_OperatorVehiclegroup(DataSet dataset, System.Int32 OperatorId, System.Int32 VehiclegroupId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_VEHICLEGROUP"));
            DataRow row = dataset.Tables["OPERATOR_VEHICLEGROUP"].Rows.Find(new object[]{OperatorId, VehiclegroupId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_VEHICLEGROUP]
        ///</summary>
        public static void Compact_OperatorVehiclegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_VEHICLEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_VEHICLEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_VEHICLEGROUP]
        ///</summary>
        public static void CompactFreshRecords_OperatorVehiclegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_VEHICLEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_VEHICLEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATOR_VEHICLEGROUP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATOR_ZONE]
        ///</summary>
        public static void CascadeRemove_OperatorZone(DataSet dataset, System.Int32 OperatorId, System.Int32 ZoneId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_ZONE"));
            DataRow row = dataset.Tables["OPERATOR_ZONE"].Rows.Find(new object[]{OperatorId, ZoneId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_ZONE]
        ///</summary>
        public static void Compact_OperatorZone(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_ZONE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_ZONE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_ZONE]
        ///</summary>
        public static void CompactFreshRecords_OperatorZone(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_ZONE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_ZONE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATOR_ZONE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATOR_ZONEGROUP]
        ///</summary>
        public static void CascadeRemove_OperatorZonegroup(DataSet dataset, System.Int32 OperatorId, System.Int32 ZonegroupId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_ZONEGROUP"));
            DataRow row = dataset.Tables["OPERATOR_ZONEGROUP"].Rows.Find(new object[]{OperatorId, ZonegroupId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_ZONEGROUP]
        ///</summary>
        public static void Compact_OperatorZonegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_ZONEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_ZONEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATOR_ZONEGROUP]
        ///</summary>
        public static void CompactFreshRecords_OperatorZonegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATOR_ZONEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATOR_ZONEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATOR_ZONEGROUP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATORGROUP]
        ///</summary>
        public static void CascadeRemove_Operatorgroup(DataSet dataset, System.Int32 OperatorgroupId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP"));
            DataRow row = dataset.Tables["OPERATORGROUP"].Rows.Find(OperatorgroupId);
            if( row != null){
                DataRow[] rowsOperatorGroupDepartment_OperatorGroup = row.GetChildRows("OperatorGroupDepartment_OperatorGroup");
                if( rowsOperatorGroupDepartment_OperatorGroup != null && rowsOperatorGroupDepartment_OperatorGroup.Length > 0 ){
                    foreach (DataRow rowOperatorGroupDepartment_OperatorGroup in rowsOperatorGroupDepartment_OperatorGroup)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupDepartment_OperatorGroup["OPERATORGROUP_ID"];
                        System.Int32 childDepartmentId = (System.Int32)rowOperatorGroupDepartment_OperatorGroup["DEPARTMENT_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupDepartment_OperatorGroup["RIGHT_ID"];
                        CascadeRemove_OperatorgroupDepartment(dataset, childOperatorgroupId, childDepartmentId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupDriver_OperatorGroup = row.GetChildRows("OperatorGroupDriver_OperatorGroup");
                if( rowsOperatorGroupDriver_OperatorGroup != null && rowsOperatorGroupDriver_OperatorGroup.Length > 0 ){
                    foreach (DataRow rowOperatorGroupDriver_OperatorGroup in rowsOperatorGroupDriver_OperatorGroup)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupDriver_OperatorGroup["OPERATORGROUP_ID"];
                        System.Int32 childDriverId = (System.Int32)rowOperatorGroupDriver_OperatorGroup["DRIVER_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupDriver_OperatorGroup["RIGHT_ID"];
                        CascadeRemove_OperatorgroupDriver(dataset, childOperatorgroupId, childDriverId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupDriverGroup_OperatorGroup = row.GetChildRows("OperatorGroupDriverGroup_OperatorGroup");
                if( rowsOperatorGroupDriverGroup_OperatorGroup != null && rowsOperatorGroupDriverGroup_OperatorGroup.Length > 0 ){
                    foreach (DataRow rowOperatorGroupDriverGroup_OperatorGroup in rowsOperatorGroupDriverGroup_OperatorGroup)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupDriverGroup_OperatorGroup["OPERATORGROUP_ID"];
                        System.Int32 childDrivergroupId = (System.Int32)rowOperatorGroupDriverGroup_OperatorGroup["DRIVERGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupDriverGroup_OperatorGroup["RIGHT_ID"];
                        CascadeRemove_OperatorgroupDrivergroup(dataset, childOperatorgroupId, childDrivergroupId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupOperator_OperatorGroup = row.GetChildRows("OperatorGroupOperator_OperatorGroup");
                if( rowsOperatorGroupOperator_OperatorGroup != null && rowsOperatorGroupOperator_OperatorGroup.Length > 0 ){
                    foreach (DataRow rowOperatorGroupOperator_OperatorGroup in rowsOperatorGroupOperator_OperatorGroup)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupOperator_OperatorGroup["OPERATORGROUP_ID"];
                        System.Int32 childOperatorId = (System.Int32)rowOperatorGroupOperator_OperatorGroup["OPERATOR_ID"];
                        CascadeRemove_OperatorgroupOperator(dataset, childOperatorgroupId, childOperatorId);
                    }
                }
                DataRow[] rowsOperatorgroupReport_Operator = row.GetChildRows("OperatorgroupReport_Operator");
                if( rowsOperatorgroupReport_Operator != null && rowsOperatorgroupReport_Operator.Length > 0 ){
                    foreach (DataRow rowOperatorgroupReport_Operator in rowsOperatorgroupReport_Operator)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorgroupReport_Operator["OPERATORGROUP_ID"];
                        System.Int32 childReportId = (System.Int32)rowOperatorgroupReport_Operator["REPORT_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorgroupReport_Operator["RIGHT_ID"];
                        CascadeRemove_OperatorgroupReport(dataset, childOperatorgroupId, childReportId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupRoute_OperatorGroup = row.GetChildRows("OperatorGroupRoute_OperatorGroup");
                if( rowsOperatorGroupRoute_OperatorGroup != null && rowsOperatorGroupRoute_OperatorGroup.Length > 0 ){
                    foreach (DataRow rowOperatorGroupRoute_OperatorGroup in rowsOperatorGroupRoute_OperatorGroup)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupRoute_OperatorGroup["OPERATORGROUP_ID"];
                        System.Int32 childRouteId = (System.Int32)rowOperatorGroupRoute_OperatorGroup["ROUTE_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupRoute_OperatorGroup["RIGHT_ID"];
                        CascadeRemove_OperatorgroupRoute(dataset, childOperatorgroupId, childRouteId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupRouteGroup_OperatorGroup = row.GetChildRows("OperatorGroupRouteGroup_OperatorGroup");
                if( rowsOperatorGroupRouteGroup_OperatorGroup != null && rowsOperatorGroupRouteGroup_OperatorGroup.Length > 0 ){
                    foreach (DataRow rowOperatorGroupRouteGroup_OperatorGroup in rowsOperatorGroupRouteGroup_OperatorGroup)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupRouteGroup_OperatorGroup["OPERATORGROUP_ID"];
                        System.Int32 childRoutegroupId = (System.Int32)rowOperatorGroupRouteGroup_OperatorGroup["ROUTEGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupRouteGroup_OperatorGroup["RIGHT_ID"];
                        CascadeRemove_OperatorgroupRoutegroup(dataset, childOperatorgroupId, childRoutegroupId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupVehicle_OperatorGroup = row.GetChildRows("OperatorGroupVehicle_OperatorGroup");
                if( rowsOperatorGroupVehicle_OperatorGroup != null && rowsOperatorGroupVehicle_OperatorGroup.Length > 0 ){
                    foreach (DataRow rowOperatorGroupVehicle_OperatorGroup in rowsOperatorGroupVehicle_OperatorGroup)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupVehicle_OperatorGroup["OPERATORGROUP_ID"];
                        System.Int32 childVehicleId = (System.Int32)rowOperatorGroupVehicle_OperatorGroup["VEHICLE_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupVehicle_OperatorGroup["RIGHT_ID"];
                        CascadeRemove_OperatorgroupVehicle(dataset, childOperatorgroupId, childVehicleId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupVehicleGroup_OperatorGroup = row.GetChildRows("OperatorGroupVehicleGroup_OperatorGroup");
                if( rowsOperatorGroupVehicleGroup_OperatorGroup != null && rowsOperatorGroupVehicleGroup_OperatorGroup.Length > 0 ){
                    foreach (DataRow rowOperatorGroupVehicleGroup_OperatorGroup in rowsOperatorGroupVehicleGroup_OperatorGroup)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupVehicleGroup_OperatorGroup["OPERATORGROUP_ID"];
                        System.Int32 childVehiclegroupId = (System.Int32)rowOperatorGroupVehicleGroup_OperatorGroup["VEHICLEGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupVehicleGroup_OperatorGroup["RIGHT_ID"];
                        CascadeRemove_OperatorgroupVehiclegroup(dataset, childOperatorgroupId, childVehiclegroupId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupZone_OperatorGroup = row.GetChildRows("OperatorGroupZone_OperatorGroup");
                if( rowsOperatorGroupZone_OperatorGroup != null && rowsOperatorGroupZone_OperatorGroup.Length > 0 ){
                    foreach (DataRow rowOperatorGroupZone_OperatorGroup in rowsOperatorGroupZone_OperatorGroup)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupZone_OperatorGroup["OPERATORGROUP_ID"];
                        System.Int32 childZoneId = (System.Int32)rowOperatorGroupZone_OperatorGroup["ZONE_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupZone_OperatorGroup["RIGHT_ID"];
                        CascadeRemove_OperatorgroupZone(dataset, childOperatorgroupId, childZoneId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupZoneGroup_OperatorGroup = row.GetChildRows("OperatorGroupZoneGroup_OperatorGroup");
                if( rowsOperatorGroupZoneGroup_OperatorGroup != null && rowsOperatorGroupZoneGroup_OperatorGroup.Length > 0 ){
                    foreach (DataRow rowOperatorGroupZoneGroup_OperatorGroup in rowsOperatorGroupZoneGroup_OperatorGroup)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupZoneGroup_OperatorGroup["OPERATORGROUP_ID"];
                        System.Int32 childZonegroupId = (System.Int32)rowOperatorGroupZoneGroup_OperatorGroup["ZONEGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupZoneGroup_OperatorGroup["RIGHT_ID"];
                        CascadeRemove_OperatorgroupZonegroup(dataset, childOperatorgroupId, childZonegroupId, childRightId);
                    }
                }
                DataRow[] rowsRightOperatorGroup_OperatorGroup = row.GetChildRows("RightOperatorGroup_OperatorGroup");
                if( rowsRightOperatorGroup_OperatorGroup != null && rowsRightOperatorGroup_OperatorGroup.Length > 0 ){
                    foreach (DataRow rowRightOperatorGroup_OperatorGroup in rowsRightOperatorGroup_OperatorGroup)
                    {
                        System.Int32 childRightId = (System.Int32)rowRightOperatorGroup_OperatorGroup["RIGHT_ID"];
                        System.Int32 childOperatorgroupId = (System.Int32)rowRightOperatorGroup_OperatorGroup["OPERATORGROUP_ID"];
                        CascadeRemove_RightOperatorgroup(dataset, childRightId, childOperatorgroupId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP]
        ///</summary>
        public static void Compact_Operatorgroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP]
        ///</summary>
        public static void CompactFreshRecords_Operatorgroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATORGROUP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATORGROUP_DEPARTMENT]
        ///</summary>
        public static void CascadeRemove_OperatorgroupDepartment(DataSet dataset, System.Int32 OperatorgroupId, System.Int32 DepartmentId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_DEPARTMENT"));
            DataRow row = dataset.Tables["OPERATORGROUP_DEPARTMENT"].Rows.Find(new object[]{OperatorgroupId, DepartmentId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_DEPARTMENT]
        ///</summary>
        public static void Compact_OperatorgroupDepartment(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_DEPARTMENT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_DEPARTMENT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_DEPARTMENT]
        ///</summary>
        public static void CompactFreshRecords_OperatorgroupDepartment(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_DEPARTMENT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_DEPARTMENT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATORGROUP_DEPARTMENT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATORGROUP_DRIVER]
        ///</summary>
        public static void CascadeRemove_OperatorgroupDriver(DataSet dataset, System.Int32 OperatorgroupId, System.Int32 DriverId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_DRIVER"));
            DataRow row = dataset.Tables["OPERATORGROUP_DRIVER"].Rows.Find(new object[]{OperatorgroupId, DriverId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_DRIVER]
        ///</summary>
        public static void Compact_OperatorgroupDriver(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_DRIVER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_DRIVER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_DRIVER]
        ///</summary>
        public static void CompactFreshRecords_OperatorgroupDriver(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_DRIVER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_DRIVER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATORGROUP_DRIVER"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATORGROUP_DRIVERGROUP]
        ///</summary>
        public static void CascadeRemove_OperatorgroupDrivergroup(DataSet dataset, System.Int32 OperatorgroupId, System.Int32 DrivergroupId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_DRIVERGROUP"));
            DataRow row = dataset.Tables["OPERATORGROUP_DRIVERGROUP"].Rows.Find(new object[]{OperatorgroupId, DrivergroupId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_DRIVERGROUP]
        ///</summary>
        public static void Compact_OperatorgroupDrivergroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_DRIVERGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_DRIVERGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_DRIVERGROUP]
        ///</summary>
        public static void CompactFreshRecords_OperatorgroupDrivergroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_DRIVERGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_DRIVERGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATORGROUP_DRIVERGROUP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATORGROUP_OPERATOR]
        ///</summary>
        public static void CascadeRemove_OperatorgroupOperator(DataSet dataset, System.Int32 OperatorgroupId, System.Int32 OperatorId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_OPERATOR"));
            DataRow row = dataset.Tables["OPERATORGROUP_OPERATOR"].Rows.Find(new object[]{OperatorgroupId, OperatorId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_OPERATOR]
        ///</summary>
        public static void Compact_OperatorgroupOperator(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_OPERATOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_OPERATOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_OPERATOR]
        ///</summary>
        public static void CompactFreshRecords_OperatorgroupOperator(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_OPERATOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_OPERATOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATORGROUP_OPERATOR"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATORGROUP_REPORT]
        ///</summary>
        public static void CascadeRemove_OperatorgroupReport(DataSet dataset, System.Int32 OperatorgroupId, System.Int32 ReportId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_REPORT"));
            DataRow row = dataset.Tables["OPERATORGROUP_REPORT"].Rows.Find(new object[]{OperatorgroupId, ReportId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_REPORT]
        ///</summary>
        public static void Compact_OperatorgroupReport(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_REPORT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_REPORT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_REPORT]
        ///</summary>
        public static void CompactFreshRecords_OperatorgroupReport(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_REPORT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_REPORT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATORGROUP_REPORT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATORGROUP_ROUTE]
        ///</summary>
        public static void CascadeRemove_OperatorgroupRoute(DataSet dataset, System.Int32 OperatorgroupId, System.Int32 RouteId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_ROUTE"));
            DataRow row = dataset.Tables["OPERATORGROUP_ROUTE"].Rows.Find(new object[]{OperatorgroupId, RouteId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_ROUTE]
        ///</summary>
        public static void Compact_OperatorgroupRoute(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_ROUTE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_ROUTE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_ROUTE]
        ///</summary>
        public static void CompactFreshRecords_OperatorgroupRoute(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_ROUTE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_ROUTE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATORGROUP_ROUTE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATORGROUP_ROUTEGROUP]
        ///</summary>
        public static void CascadeRemove_OperatorgroupRoutegroup(DataSet dataset, System.Int32 OperatorgroupId, System.Int32 RoutegroupId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_ROUTEGROUP"));
            DataRow row = dataset.Tables["OPERATORGROUP_ROUTEGROUP"].Rows.Find(new object[]{OperatorgroupId, RoutegroupId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_ROUTEGROUP]
        ///</summary>
        public static void Compact_OperatorgroupRoutegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_ROUTEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_ROUTEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_ROUTEGROUP]
        ///</summary>
        public static void CompactFreshRecords_OperatorgroupRoutegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_ROUTEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_ROUTEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATORGROUP_ROUTEGROUP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATORGROUP_VEHICLE]
        ///</summary>
        public static void CascadeRemove_OperatorgroupVehicle(DataSet dataset, System.Int32 OperatorgroupId, System.Int32 VehicleId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_VEHICLE"));
            DataRow row = dataset.Tables["OPERATORGROUP_VEHICLE"].Rows.Find(new object[]{OperatorgroupId, VehicleId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_VEHICLE]
        ///</summary>
        public static void Compact_OperatorgroupVehicle(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_VEHICLE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_VEHICLE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_VEHICLE]
        ///</summary>
        public static void CompactFreshRecords_OperatorgroupVehicle(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_VEHICLE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_VEHICLE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATORGROUP_VEHICLE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATORGROUP_VEHICLEGROUP]
        ///</summary>
        public static void CascadeRemove_OperatorgroupVehiclegroup(DataSet dataset, System.Int32 OperatorgroupId, System.Int32 VehiclegroupId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_VEHICLEGROUP"));
            DataRow row = dataset.Tables["OPERATORGROUP_VEHICLEGROUP"].Rows.Find(new object[]{OperatorgroupId, VehiclegroupId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_VEHICLEGROUP]
        ///</summary>
        public static void Compact_OperatorgroupVehiclegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_VEHICLEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_VEHICLEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_VEHICLEGROUP]
        ///</summary>
        public static void CompactFreshRecords_OperatorgroupVehiclegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_VEHICLEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_VEHICLEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATORGROUP_VEHICLEGROUP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATORGROUP_ZONE]
        ///</summary>
        public static void CascadeRemove_OperatorgroupZone(DataSet dataset, System.Int32 OperatorgroupId, System.Int32 ZoneId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_ZONE"));
            DataRow row = dataset.Tables["OPERATORGROUP_ZONE"].Rows.Find(new object[]{OperatorgroupId, ZoneId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_ZONE]
        ///</summary>
        public static void Compact_OperatorgroupZone(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_ZONE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_ZONE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_ZONE]
        ///</summary>
        public static void CompactFreshRecords_OperatorgroupZone(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_ZONE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_ZONE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATORGROUP_ZONE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPERATORGROUP_ZONEGROUP]
        ///</summary>
        public static void CascadeRemove_OperatorgroupZonegroup(DataSet dataset, System.Int32 OperatorgroupId, System.Int32 ZonegroupId, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_ZONEGROUP"));
            DataRow row = dataset.Tables["OPERATORGROUP_ZONEGROUP"].Rows.Find(new object[]{OperatorgroupId, ZonegroupId, RightId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_ZONEGROUP]
        ///</summary>
        public static void Compact_OperatorgroupZonegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_ZONEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_ZONEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPERATORGROUP_ZONEGROUP]
        ///</summary>
        public static void CompactFreshRecords_OperatorgroupZonegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPERATORGROUP_ZONEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPERATORGROUP_ZONEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPERATORGROUP_ZONEGROUP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OPS_EVENT]
        ///</summary>
        public static void CascadeRemove_OpsEvent(DataSet dataset, System.Int32 OpsEventId)
        {
            Debug.Assert(dataset.Tables.Contains("OPS_EVENT"));
            DataRow row = dataset.Tables["OPS_EVENT"].Rows.Find(OpsEventId);
            if( row != null){
                DataRow[] rowsLogOpsEvents_OpsEvent = row.GetChildRows("LogOpsEvents_OpsEvent");
                if( rowsLogOpsEvents_OpsEvent != null && rowsLogOpsEvents_OpsEvent.Length > 0 ){
                    foreach (DataRow rowLogOpsEvents_OpsEvent in rowsLogOpsEvents_OpsEvent)
                    {
                        System.Int32 childRecordId = (System.Int32)rowLogOpsEvents_OpsEvent["RECORD_ID"];
                        CascadeRemove_LogOpsEvents(dataset, childRecordId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [OPS_EVENT]
        ///</summary>
        public static void Compact_OpsEvent(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPS_EVENT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPS_EVENT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OPS_EVENT]
        ///</summary>
        public static void CompactFreshRecords_OpsEvent(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OPS_EVENT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OPS_EVENT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OPS_EVENT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [ORDER]
        ///</summary>
        public static void CascadeRemove_Order(DataSet dataset, System.Int32 OrderId)
        {
            Debug.Assert(dataset.Tables.Contains("ORDER"));
            DataRow row = dataset.Tables["ORDER"].Rows.Find(OrderId);
            if( row != null){
                DataRow[] rowsFK_ORDER_TRIP_ORDER = row.GetChildRows("FK_ORDER_TRIP_ORDER");
                if( rowsFK_ORDER_TRIP_ORDER != null && rowsFK_ORDER_TRIP_ORDER.Length > 0 ){
                    foreach (DataRow rowFK_ORDER_TRIP_ORDER in rowsFK_ORDER_TRIP_ORDER)
                    {
                        System.Int32 childOrderTripId = (System.Int32)rowFK_ORDER_TRIP_ORDER["ORDER_TRIP_ID"];
                        CascadeRemove_OrderTrip(dataset, childOrderTripId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [ORDER]
        ///</summary>
        public static void Compact_Order(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ORDER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ORDER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [ORDER]
        ///</summary>
        public static void CompactFreshRecords_Order(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ORDER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ORDER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["ORDER"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [ORDER_TRIP]
        ///</summary>
        public static void CascadeRemove_OrderTrip(DataSet dataset, System.Int32 OrderTripId)
        {
            Debug.Assert(dataset.Tables.Contains("ORDER_TRIP"));
            DataRow row = dataset.Tables["ORDER_TRIP"].Rows.Find(OrderTripId);
            if( row != null){
                DataRow[] rowsFK_WB_TRIP_ORDER_TRIP = row.GetChildRows("FK_WB_TRIP_ORDER_TRIP");
                if( rowsFK_WB_TRIP_ORDER_TRIP != null && rowsFK_WB_TRIP_ORDER_TRIP.Length > 0 ){
                    foreach (DataRow rowFK_WB_TRIP_ORDER_TRIP in rowsFK_WB_TRIP_ORDER_TRIP)
                    {
                        System.Int32 childWbTripId = (System.Int32)rowFK_WB_TRIP_ORDER_TRIP["WB_TRIP_ID"];
                        CascadeRemove_WbTrip(dataset, childWbTripId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [ORDER_TRIP]
        ///</summary>
        public static void Compact_OrderTrip(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ORDER_TRIP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ORDER_TRIP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [ORDER_TRIP]
        ///</summary>
        public static void CompactFreshRecords_OrderTrip(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ORDER_TRIP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ORDER_TRIP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["ORDER_TRIP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [ORDER_TYPE]
        ///</summary>
        public static void CascadeRemove_OrderType(DataSet dataset, System.Int32 OrderTypeId)
        {
            Debug.Assert(dataset.Tables.Contains("ORDER_TYPE"));
            DataRow row = dataset.Tables["ORDER_TYPE"].Rows.Find(OrderTypeId);
            if( row != null){
                DataRow[] rowsOrder_OrderType = row.GetChildRows("Order_OrderType");
                if( rowsOrder_OrderType != null && rowsOrder_OrderType.Length > 0 ){
                    foreach (DataRow rowOrder_OrderType in rowsOrder_OrderType)
                    {
                        System.Int32 childOrderId = (System.Int32)rowOrder_OrderType["ORDER_ID"];
                        CascadeRemove_Order(dataset, childOrderId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [ORDER_TYPE]
        ///</summary>
        public static void Compact_OrderType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ORDER_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ORDER_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [ORDER_TYPE]
        ///</summary>
        public static void CompactFreshRecords_OrderType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ORDER_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ORDER_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["ORDER_TYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [OWNER]
        ///</summary>
        public static void CascadeRemove_Owner(DataSet dataset, System.Int32 OwnerId)
        {
            Debug.Assert(dataset.Tables.Contains("OWNER"));
            DataRow row = dataset.Tables["OWNER"].Rows.Find(OwnerId);
            if( row != null){
                DataRow[] rowsFK_VEHICLE_OWNER_OWNER = row.GetChildRows("FK_VEHICLE_OWNER_OWNER");
                if( rowsFK_VEHICLE_OWNER_OWNER != null && rowsFK_VEHICLE_OWNER_OWNER.Length > 0 ){
                    foreach (DataRow rowFK_VEHICLE_OWNER_OWNER in rowsFK_VEHICLE_OWNER_OWNER)
                    {
                        System.Int32 childVehicleOwnerId = (System.Int32)rowFK_VEHICLE_OWNER_OWNER["VEHICLE_OWNER_ID"];
                        CascadeRemove_VehicleOwner(dataset, childVehicleOwnerId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [OWNER]
        ///</summary>
        public static void Compact_Owner(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OWNER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OWNER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [OWNER]
        ///</summary>
        public static void CompactFreshRecords_Owner(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("OWNER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["OWNER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["OWNER"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [PERIOD]
        ///</summary>
        public static void CascadeRemove_Period(DataSet dataset, System.Int32 PeriodId)
        {
            Debug.Assert(dataset.Tables.Contains("PERIOD"));
            DataRow row = dataset.Tables["PERIOD"].Rows.Find(PeriodId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [PERIOD]
        ///</summary>
        public static void Compact_Period(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("PERIOD"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["PERIOD"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [PERIOD]
        ///</summary>
        public static void CompactFreshRecords_Period(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("PERIOD"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["PERIOD"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["PERIOD"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [POINT]
        ///</summary>
        public static void CascadeRemove_Point(DataSet dataset, System.Int32 PointId)
        {
            Debug.Assert(dataset.Tables.Contains("POINT"));
            DataRow row = dataset.Tables["POINT"].Rows.Find(PointId);
            if( row != null){
                DataRow[] rowsGeoSegment_Point = row.GetChildRows("GeoSegment_Point");
                if( rowsGeoSegment_Point != null && rowsGeoSegment_Point.Length > 0 ){
                    foreach (DataRow rowGeoSegment_Point in rowsGeoSegment_Point)
                    {
                        System.Int32 childGeoSegmentId = (System.Int32)rowGeoSegment_Point["GEO_SEGMENT_ID"];
                        CascadeRemove_GeoSegment(dataset, childGeoSegmentId);
                    }
                }
                DataRow[] rowsGeoSegment_PointTo = row.GetChildRows("GeoSegment_PointTo");
                if( rowsGeoSegment_PointTo != null && rowsGeoSegment_PointTo.Length > 0 ){
                    foreach (DataRow rowGeoSegment_PointTo in rowsGeoSegment_PointTo)
                    {
                        System.Int32 childGeoSegmentId = (System.Int32)rowGeoSegment_PointTo["GEO_SEGMENT_ID"];
                        CascadeRemove_GeoSegment(dataset, childGeoSegmentId);
                    }
                }
                DataRow[] rowsGeoTrip_PointA = row.GetChildRows("GeoTrip_PointA");
                if( rowsGeoTrip_PointA != null && rowsGeoTrip_PointA.Length > 0 ){
                    foreach (DataRow rowGeoTrip_PointA in rowsGeoTrip_PointA)
                    {
                        System.Int32 childGeoTripId = (System.Int32)rowGeoTrip_PointA["GEO_TRIP_ID"];
                        CascadeRemove_GeoTrip(dataset, childGeoTripId);
                    }
                }
                DataRow[] rowsGeoTrip_PointB = row.GetChildRows("GeoTrip_PointB");
                if( rowsGeoTrip_PointB != null && rowsGeoTrip_PointB.Length > 0 ){
                    foreach (DataRow rowGeoTrip_PointB in rowsGeoTrip_PointB)
                    {
                        System.Int32 childGeoTripId = (System.Int32)rowGeoTrip_PointB["GEO_TRIP_ID"];
                        CascadeRemove_GeoTrip(dataset, childGeoTripId);
                    }
                }
                DataRow[] rowsRoutePoint_Point = row.GetChildRows("RoutePoint_Point");
                if( rowsRoutePoint_Point != null && rowsRoutePoint_Point.Length > 0 ){
                    foreach (DataRow rowRoutePoint_Point in rowsRoutePoint_Point)
                    {
                        System.Int32 childRoutePointId = (System.Int32)rowRoutePoint_Point["ROUTE_POINT_ID"];
                        CascadeRemove_RoutePoint(dataset, childRoutePointId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [POINT]
        ///</summary>
        public static void Compact_Point(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("POINT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["POINT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [POINT]
        ///</summary>
        public static void CompactFreshRecords_Point(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("POINT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["POINT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["POINT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [POINT_KIND]
        ///</summary>
        public static void CascadeRemove_PointKind(DataSet dataset, System.Int32 PointKindId)
        {
            Debug.Assert(dataset.Tables.Contains("POINT_KIND"));
            DataRow row = dataset.Tables["POINT_KIND"].Rows.Find(PointKindId);
            if( row != null){
                DataRow[] rowsPoint_PointKind = row.GetChildRows("Point_PointKind");
                if( rowsPoint_PointKind != null && rowsPoint_PointKind.Length > 0 ){
                    foreach (DataRow rowPoint_PointKind in rowsPoint_PointKind)
                    {
                        System.Int32 childPointId = (System.Int32)rowPoint_PointKind["POINT_ID"];
                        CascadeRemove_Point(dataset, childPointId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [POINT_KIND]
        ///</summary>
        public static void Compact_PointKind(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("POINT_KIND"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["POINT_KIND"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [POINT_KIND]
        ///</summary>
        public static void CompactFreshRecords_PointKind(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("POINT_KIND"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["POINT_KIND"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["POINT_KIND"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [REPORT]
        ///</summary>
        public static void CascadeRemove_Report(DataSet dataset, System.Int32 ReportId)
        {
            Debug.Assert(dataset.Tables.Contains("REPORT"));
            DataRow row = dataset.Tables["REPORT"].Rows.Find(ReportId);
            if( row != null){
                DataRow[] rowsOperatorgroupReport_Report = row.GetChildRows("OperatorgroupReport_Report");
                if( rowsOperatorgroupReport_Report != null && rowsOperatorgroupReport_Report.Length > 0 ){
                    foreach (DataRow rowOperatorgroupReport_Report in rowsOperatorgroupReport_Report)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorgroupReport_Report["OPERATORGROUP_ID"];
                        System.Int32 childReportId = (System.Int32)rowOperatorgroupReport_Report["REPORT_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorgroupReport_Report["RIGHT_ID"];
                        CascadeRemove_OperatorgroupReport(dataset, childOperatorgroupId, childReportId, childRightId);
                    }
                }
                DataRow[] rowsOperatorReport_Report = row.GetChildRows("OperatorReport_Report");
                if( rowsOperatorReport_Report != null && rowsOperatorReport_Report.Length > 0 ){
                    foreach (DataRow rowOperatorReport_Report in rowsOperatorReport_Report)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorReport_Report["OPERATOR_ID"];
                        System.Int32 childReportId = (System.Int32)rowOperatorReport_Report["REPORT_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorReport_Report["RIGHT_ID"];
                        CascadeRemove_OperatorReport(dataset, childOperatorId, childReportId, childRightId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [REPORT]
        ///</summary>
        public static void Compact_Report(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("REPORT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["REPORT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [REPORT]
        ///</summary>
        public static void CompactFreshRecords_Report(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("REPORT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["REPORT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["REPORT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [REPRINT_REASON]
        ///</summary>
        public static void CascadeRemove_ReprintReason(DataSet dataset, System.Int32 ReprintReasonId)
        {
            Debug.Assert(dataset.Tables.Contains("REPRINT_REASON"));
            DataRow row = dataset.Tables["REPRINT_REASON"].Rows.Find(ReprintReasonId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [REPRINT_REASON]
        ///</summary>
        public static void Compact_ReprintReason(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("REPRINT_REASON"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["REPRINT_REASON"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [REPRINT_REASON]
        ///</summary>
        public static void CompactFreshRecords_ReprintReason(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("REPRINT_REASON"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["REPRINT_REASON"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["REPRINT_REASON"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RIGHT]
        ///</summary>
        public static void CascadeRemove_Right(DataSet dataset, System.Int32 RightId)
        {
            Debug.Assert(dataset.Tables.Contains("RIGHT"));
            DataRow row = dataset.Tables["RIGHT"].Rows.Find(RightId);
            if( row != null){
                DataRow[] rowsOperatorDepartment_Right = row.GetChildRows("OperatorDepartment_Right");
                if( rowsOperatorDepartment_Right != null && rowsOperatorDepartment_Right.Length > 0 ){
                    foreach (DataRow rowOperatorDepartment_Right in rowsOperatorDepartment_Right)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorDepartment_Right["OPERATOR_ID"];
                        System.Int32 childDepartmentId = (System.Int32)rowOperatorDepartment_Right["DEPARTMENT_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorDepartment_Right["RIGHT_ID"];
                        CascadeRemove_OperatorDepartment(dataset, childOperatorId, childDepartmentId, childRightId);
                    }
                }
                DataRow[] rowsOperatorDriver_Right = row.GetChildRows("OperatorDriver_Right");
                if( rowsOperatorDriver_Right != null && rowsOperatorDriver_Right.Length > 0 ){
                    foreach (DataRow rowOperatorDriver_Right in rowsOperatorDriver_Right)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorDriver_Right["OPERATOR_ID"];
                        System.Int32 childDriverId = (System.Int32)rowOperatorDriver_Right["DRIVER_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorDriver_Right["RIGHT_ID"];
                        CascadeRemove_OperatorDriver(dataset, childOperatorId, childDriverId, childRightId);
                    }
                }
                DataRow[] rowsOperatorDriverGroup_Right = row.GetChildRows("OperatorDriverGroup_Right");
                if( rowsOperatorDriverGroup_Right != null && rowsOperatorDriverGroup_Right.Length > 0 ){
                    foreach (DataRow rowOperatorDriverGroup_Right in rowsOperatorDriverGroup_Right)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorDriverGroup_Right["OPERATOR_ID"];
                        System.Int32 childDrivergroupId = (System.Int32)rowOperatorDriverGroup_Right["DRIVERGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorDriverGroup_Right["RIGHT_ID"];
                        CascadeRemove_OperatorDrivergroup(dataset, childOperatorId, childDrivergroupId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupDepartment_Right = row.GetChildRows("OperatorGroupDepartment_Right");
                if( rowsOperatorGroupDepartment_Right != null && rowsOperatorGroupDepartment_Right.Length > 0 ){
                    foreach (DataRow rowOperatorGroupDepartment_Right in rowsOperatorGroupDepartment_Right)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupDepartment_Right["OPERATORGROUP_ID"];
                        System.Int32 childDepartmentId = (System.Int32)rowOperatorGroupDepartment_Right["DEPARTMENT_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupDepartment_Right["RIGHT_ID"];
                        CascadeRemove_OperatorgroupDepartment(dataset, childOperatorgroupId, childDepartmentId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupDriver_Right = row.GetChildRows("OperatorGroupDriver_Right");
                if( rowsOperatorGroupDriver_Right != null && rowsOperatorGroupDriver_Right.Length > 0 ){
                    foreach (DataRow rowOperatorGroupDriver_Right in rowsOperatorGroupDriver_Right)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupDriver_Right["OPERATORGROUP_ID"];
                        System.Int32 childDriverId = (System.Int32)rowOperatorGroupDriver_Right["DRIVER_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupDriver_Right["RIGHT_ID"];
                        CascadeRemove_OperatorgroupDriver(dataset, childOperatorgroupId, childDriverId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupDriverGroup_Right = row.GetChildRows("OperatorGroupDriverGroup_Right");
                if( rowsOperatorGroupDriverGroup_Right != null && rowsOperatorGroupDriverGroup_Right.Length > 0 ){
                    foreach (DataRow rowOperatorGroupDriverGroup_Right in rowsOperatorGroupDriverGroup_Right)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupDriverGroup_Right["OPERATORGROUP_ID"];
                        System.Int32 childDrivergroupId = (System.Int32)rowOperatorGroupDriverGroup_Right["DRIVERGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupDriverGroup_Right["RIGHT_ID"];
                        CascadeRemove_OperatorgroupDrivergroup(dataset, childOperatorgroupId, childDrivergroupId, childRightId);
                    }
                }
                DataRow[] rowsOperatorgroupReport_Right = row.GetChildRows("OperatorgroupReport_Right");
                if( rowsOperatorgroupReport_Right != null && rowsOperatorgroupReport_Right.Length > 0 ){
                    foreach (DataRow rowOperatorgroupReport_Right in rowsOperatorgroupReport_Right)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorgroupReport_Right["OPERATORGROUP_ID"];
                        System.Int32 childReportId = (System.Int32)rowOperatorgroupReport_Right["REPORT_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorgroupReport_Right["RIGHT_ID"];
                        CascadeRemove_OperatorgroupReport(dataset, childOperatorgroupId, childReportId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupRoute_Right = row.GetChildRows("OperatorGroupRoute_Right");
                if( rowsOperatorGroupRoute_Right != null && rowsOperatorGroupRoute_Right.Length > 0 ){
                    foreach (DataRow rowOperatorGroupRoute_Right in rowsOperatorGroupRoute_Right)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupRoute_Right["OPERATORGROUP_ID"];
                        System.Int32 childRouteId = (System.Int32)rowOperatorGroupRoute_Right["ROUTE_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupRoute_Right["RIGHT_ID"];
                        CascadeRemove_OperatorgroupRoute(dataset, childOperatorgroupId, childRouteId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupRouteGroup_Right = row.GetChildRows("OperatorGroupRouteGroup_Right");
                if( rowsOperatorGroupRouteGroup_Right != null && rowsOperatorGroupRouteGroup_Right.Length > 0 ){
                    foreach (DataRow rowOperatorGroupRouteGroup_Right in rowsOperatorGroupRouteGroup_Right)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupRouteGroup_Right["OPERATORGROUP_ID"];
                        System.Int32 childRoutegroupId = (System.Int32)rowOperatorGroupRouteGroup_Right["ROUTEGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupRouteGroup_Right["RIGHT_ID"];
                        CascadeRemove_OperatorgroupRoutegroup(dataset, childOperatorgroupId, childRoutegroupId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupVehicle_Right = row.GetChildRows("OperatorGroupVehicle_Right");
                if( rowsOperatorGroupVehicle_Right != null && rowsOperatorGroupVehicle_Right.Length > 0 ){
                    foreach (DataRow rowOperatorGroupVehicle_Right in rowsOperatorGroupVehicle_Right)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupVehicle_Right["OPERATORGROUP_ID"];
                        System.Int32 childVehicleId = (System.Int32)rowOperatorGroupVehicle_Right["VEHICLE_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupVehicle_Right["RIGHT_ID"];
                        CascadeRemove_OperatorgroupVehicle(dataset, childOperatorgroupId, childVehicleId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupVehicleGroup_Right = row.GetChildRows("OperatorGroupVehicleGroup_Right");
                if( rowsOperatorGroupVehicleGroup_Right != null && rowsOperatorGroupVehicleGroup_Right.Length > 0 ){
                    foreach (DataRow rowOperatorGroupVehicleGroup_Right in rowsOperatorGroupVehicleGroup_Right)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupVehicleGroup_Right["OPERATORGROUP_ID"];
                        System.Int32 childVehiclegroupId = (System.Int32)rowOperatorGroupVehicleGroup_Right["VEHICLEGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupVehicleGroup_Right["RIGHT_ID"];
                        CascadeRemove_OperatorgroupVehiclegroup(dataset, childOperatorgroupId, childVehiclegroupId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupZone_Right = row.GetChildRows("OperatorGroupZone_Right");
                if( rowsOperatorGroupZone_Right != null && rowsOperatorGroupZone_Right.Length > 0 ){
                    foreach (DataRow rowOperatorGroupZone_Right in rowsOperatorGroupZone_Right)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupZone_Right["OPERATORGROUP_ID"];
                        System.Int32 childZoneId = (System.Int32)rowOperatorGroupZone_Right["ZONE_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupZone_Right["RIGHT_ID"];
                        CascadeRemove_OperatorgroupZone(dataset, childOperatorgroupId, childZoneId, childRightId);
                    }
                }
                DataRow[] rowsOperatorGroupZoneGroup_Right = row.GetChildRows("OperatorGroupZoneGroup_Right");
                if( rowsOperatorGroupZoneGroup_Right != null && rowsOperatorGroupZoneGroup_Right.Length > 0 ){
                    foreach (DataRow rowOperatorGroupZoneGroup_Right in rowsOperatorGroupZoneGroup_Right)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupZoneGroup_Right["OPERATORGROUP_ID"];
                        System.Int32 childZonegroupId = (System.Int32)rowOperatorGroupZoneGroup_Right["ZONEGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupZoneGroup_Right["RIGHT_ID"];
                        CascadeRemove_OperatorgroupZonegroup(dataset, childOperatorgroupId, childZonegroupId, childRightId);
                    }
                }
                DataRow[] rowsOperatorReport_Right = row.GetChildRows("OperatorReport_Right");
                if( rowsOperatorReport_Right != null && rowsOperatorReport_Right.Length > 0 ){
                    foreach (DataRow rowOperatorReport_Right in rowsOperatorReport_Right)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorReport_Right["OPERATOR_ID"];
                        System.Int32 childReportId = (System.Int32)rowOperatorReport_Right["REPORT_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorReport_Right["RIGHT_ID"];
                        CascadeRemove_OperatorReport(dataset, childOperatorId, childReportId, childRightId);
                    }
                }
                DataRow[] rowsOperatorRoute_Right = row.GetChildRows("OperatorRoute_Right");
                if( rowsOperatorRoute_Right != null && rowsOperatorRoute_Right.Length > 0 ){
                    foreach (DataRow rowOperatorRoute_Right in rowsOperatorRoute_Right)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorRoute_Right["OPERATOR_ID"];
                        System.Int32 childRouteId = (System.Int32)rowOperatorRoute_Right["ROUTE_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorRoute_Right["RIGHT_ID"];
                        CascadeRemove_OperatorRoute(dataset, childOperatorId, childRouteId, childRightId);
                    }
                }
                DataRow[] rowsOperatorRouteGroup_Right = row.GetChildRows("OperatorRouteGroup_Right");
                if( rowsOperatorRouteGroup_Right != null && rowsOperatorRouteGroup_Right.Length > 0 ){
                    foreach (DataRow rowOperatorRouteGroup_Right in rowsOperatorRouteGroup_Right)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorRouteGroup_Right["OPERATOR_ID"];
                        System.Int32 childRoutegroupId = (System.Int32)rowOperatorRouteGroup_Right["ROUTEGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorRouteGroup_Right["RIGHT_ID"];
                        CascadeRemove_OperatorRoutegroup(dataset, childOperatorId, childRoutegroupId, childRightId);
                    }
                }
                DataRow[] rowsOperatorVehicle_Right = row.GetChildRows("OperatorVehicle_Right");
                if( rowsOperatorVehicle_Right != null && rowsOperatorVehicle_Right.Length > 0 ){
                    foreach (DataRow rowOperatorVehicle_Right in rowsOperatorVehicle_Right)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorVehicle_Right["OPERATOR_ID"];
                        System.Int32 childVehicleId = (System.Int32)rowOperatorVehicle_Right["VEHICLE_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorVehicle_Right["RIGHT_ID"];
                        CascadeRemove_OperatorVehicle(dataset, childOperatorId, childVehicleId, childRightId);
                    }
                }
                DataRow[] rowsOperatorVehicleGroup_Right = row.GetChildRows("OperatorVehicleGroup_Right");
                if( rowsOperatorVehicleGroup_Right != null && rowsOperatorVehicleGroup_Right.Length > 0 ){
                    foreach (DataRow rowOperatorVehicleGroup_Right in rowsOperatorVehicleGroup_Right)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorVehicleGroup_Right["OPERATOR_ID"];
                        System.Int32 childVehiclegroupId = (System.Int32)rowOperatorVehicleGroup_Right["VEHICLEGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorVehicleGroup_Right["RIGHT_ID"];
                        CascadeRemove_OperatorVehiclegroup(dataset, childOperatorId, childVehiclegroupId, childRightId);
                    }
                }
                DataRow[] rowsOperatorZone_Right = row.GetChildRows("OperatorZone_Right");
                if( rowsOperatorZone_Right != null && rowsOperatorZone_Right.Length > 0 ){
                    foreach (DataRow rowOperatorZone_Right in rowsOperatorZone_Right)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorZone_Right["OPERATOR_ID"];
                        System.Int32 childZoneId = (System.Int32)rowOperatorZone_Right["ZONE_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorZone_Right["RIGHT_ID"];
                        CascadeRemove_OperatorZone(dataset, childOperatorId, childZoneId, childRightId);
                    }
                }
                DataRow[] rowsOperatorZoneGroup_Right = row.GetChildRows("OperatorZoneGroup_Right");
                if( rowsOperatorZoneGroup_Right != null && rowsOperatorZoneGroup_Right.Length > 0 ){
                    foreach (DataRow rowOperatorZoneGroup_Right in rowsOperatorZoneGroup_Right)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorZoneGroup_Right["OPERATOR_ID"];
                        System.Int32 childZonegroupId = (System.Int32)rowOperatorZoneGroup_Right["ZONEGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorZoneGroup_Right["RIGHT_ID"];
                        CascadeRemove_OperatorZonegroup(dataset, childOperatorId, childZonegroupId, childRightId);
                    }
                }
                DataRow[] rowsRightOperator_Right = row.GetChildRows("RightOperator_Right");
                if( rowsRightOperator_Right != null && rowsRightOperator_Right.Length > 0 ){
                    foreach (DataRow rowRightOperator_Right in rowsRightOperator_Right)
                    {
                        System.Int32 childRightId = (System.Int32)rowRightOperator_Right["RIGHT_ID"];
                        System.Int32 childOperatorId = (System.Int32)rowRightOperator_Right["OPERATOR_ID"];
                        CascadeRemove_RightOperator(dataset, childRightId, childOperatorId);
                    }
                }
                DataRow[] rowsRightOperatorGroup_Right = row.GetChildRows("RightOperatorGroup_Right");
                if( rowsRightOperatorGroup_Right != null && rowsRightOperatorGroup_Right.Length > 0 ){
                    foreach (DataRow rowRightOperatorGroup_Right in rowsRightOperatorGroup_Right)
                    {
                        System.Int32 childRightId = (System.Int32)rowRightOperatorGroup_Right["RIGHT_ID"];
                        System.Int32 childOperatorgroupId = (System.Int32)rowRightOperatorGroup_Right["OPERATORGROUP_ID"];
                        CascadeRemove_RightOperatorgroup(dataset, childRightId, childOperatorgroupId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [RIGHT]
        ///</summary>
        public static void Compact_Right(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RIGHT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RIGHT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RIGHT]
        ///</summary>
        public static void CompactFreshRecords_Right(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RIGHT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RIGHT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RIGHT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RIGHT_OPERATOR]
        ///</summary>
        public static void CascadeRemove_RightOperator(DataSet dataset, System.Int32 RightId, System.Int32 OperatorId)
        {
            Debug.Assert(dataset.Tables.Contains("RIGHT_OPERATOR"));
            DataRow row = dataset.Tables["RIGHT_OPERATOR"].Rows.Find(new object[]{RightId, OperatorId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [RIGHT_OPERATOR]
        ///</summary>
        public static void Compact_RightOperator(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RIGHT_OPERATOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RIGHT_OPERATOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RIGHT_OPERATOR]
        ///</summary>
        public static void CompactFreshRecords_RightOperator(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RIGHT_OPERATOR"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RIGHT_OPERATOR"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RIGHT_OPERATOR"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RIGHT_OPERATORGROUP]
        ///</summary>
        public static void CascadeRemove_RightOperatorgroup(DataSet dataset, System.Int32 RightId, System.Int32 OperatorgroupId)
        {
            Debug.Assert(dataset.Tables.Contains("RIGHT_OPERATORGROUP"));
            DataRow row = dataset.Tables["RIGHT_OPERATORGROUP"].Rows.Find(new object[]{RightId, OperatorgroupId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [RIGHT_OPERATORGROUP]
        ///</summary>
        public static void Compact_RightOperatorgroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RIGHT_OPERATORGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RIGHT_OPERATORGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RIGHT_OPERATORGROUP]
        ///</summary>
        public static void CompactFreshRecords_RightOperatorgroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RIGHT_OPERATORGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RIGHT_OPERATORGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RIGHT_OPERATORGROUP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [ROUTE]
        ///</summary>
        public static void CascadeRemove_Route(DataSet dataset, System.Int32 RouteId)
        {
            Debug.Assert(dataset.Tables.Contains("ROUTE"));
            DataRow row = dataset.Tables["ROUTE"].Rows.Find(RouteId);
            if( row != null){
                DataRow[] rowsFK_ROUTE_GEOZONE_ROUTE = row.GetChildRows("FK_ROUTE_GEOZONE_ROUTE");
                if( rowsFK_ROUTE_GEOZONE_ROUTE != null && rowsFK_ROUTE_GEOZONE_ROUTE.Length > 0 ){
                    foreach (DataRow rowFK_ROUTE_GEOZONE_ROUTE in rowsFK_ROUTE_GEOZONE_ROUTE)
                    {
                        System.Int32 childRouteGeozoneId = (System.Int32)rowFK_ROUTE_GEOZONE_ROUTE["ROUTE_GEOZONE_ID"];
                        CascadeRemove_RouteGeozone(dataset, childRouteGeozoneId);
                    }
                }
                DataRow[] rowsFK_RS_ROUTE = row.GetChildRows("FK_RS_ROUTE");
                if( rowsFK_RS_ROUTE != null && rowsFK_RS_ROUTE.Length > 0 ){
                    foreach (DataRow rowFK_RS_ROUTE in rowsFK_RS_ROUTE)
                    {
                        System.Int32 childRsId = (System.Int32)rowFK_RS_ROUTE["RS_ID"];
                        CascadeRemove_Rs(dataset, childRsId);
                    }
                }
                DataRow[] rowsGeoTrip_Route = row.GetChildRows("GeoTrip_Route");
                if( rowsGeoTrip_Route != null && rowsGeoTrip_Route.Length > 0 ){
                    foreach (DataRow rowGeoTrip_Route in rowsGeoTrip_Route)
                    {
                        System.Int32 childGeoTripId = (System.Int32)rowGeoTrip_Route["GEO_TRIP_ID"];
                        CascadeRemove_GeoTrip(dataset, childGeoTripId);
                    }
                }
                DataRow[] rowsOperatorGroupRoute_Route = row.GetChildRows("OperatorGroupRoute_Route");
                if( rowsOperatorGroupRoute_Route != null && rowsOperatorGroupRoute_Route.Length > 0 ){
                    foreach (DataRow rowOperatorGroupRoute_Route in rowsOperatorGroupRoute_Route)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupRoute_Route["OPERATORGROUP_ID"];
                        System.Int32 childRouteId = (System.Int32)rowOperatorGroupRoute_Route["ROUTE_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupRoute_Route["RIGHT_ID"];
                        CascadeRemove_OperatorgroupRoute(dataset, childOperatorgroupId, childRouteId, childRightId);
                    }
                }
                DataRow[] rowsOperatorRoute_Route = row.GetChildRows("OperatorRoute_Route");
                if( rowsOperatorRoute_Route != null && rowsOperatorRoute_Route.Length > 0 ){
                    foreach (DataRow rowOperatorRoute_Route in rowsOperatorRoute_Route)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorRoute_Route["OPERATOR_ID"];
                        System.Int32 childRouteId = (System.Int32)rowOperatorRoute_Route["ROUTE_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorRoute_Route["RIGHT_ID"];
                        CascadeRemove_OperatorRoute(dataset, childOperatorId, childRouteId, childRightId);
                    }
                }
                DataRow[] rowsPeriod_Route = row.GetChildRows("Period_Route");
                if( rowsPeriod_Route != null && rowsPeriod_Route.Length > 0 ){
                    foreach (DataRow rowPeriod_Route in rowsPeriod_Route)
                    {
                        System.Int32 childPeriodId = (System.Int32)rowPeriod_Route["PERIOD_ID"];
                        CascadeRemove_Period(dataset, childPeriodId);
                    }
                }
                DataRow[] rowsRouteGroupRoute_Route = row.GetChildRows("RouteGroupRoute_Route");
                if( rowsRouteGroupRoute_Route != null && rowsRouteGroupRoute_Route.Length > 0 ){
                    foreach (DataRow rowRouteGroupRoute_Route in rowsRouteGroupRoute_Route)
                    {
                        System.Int32 childRoutegroupId = (System.Int32)rowRouteGroupRoute_Route["ROUTEGROUP_ID"];
                        System.Int32 childRouteId = (System.Int32)rowRouteGroupRoute_Route["ROUTE_ID"];
                        CascadeRemove_RoutegroupRoute(dataset, childRoutegroupId, childRouteId);
                    }
                }
                DataRow[] rowsRoutePoint_Route = row.GetChildRows("RoutePoint_Route");
                if( rowsRoutePoint_Route != null && rowsRoutePoint_Route.Length > 0 ){
                    foreach (DataRow rowRoutePoint_Route in rowsRoutePoint_Route)
                    {
                        System.Int32 childRoutePointId = (System.Int32)rowRoutePoint_Route["ROUTE_POINT_ID"];
                        CascadeRemove_RoutePoint(dataset, childRoutePointId);
                    }
                }
                DataRow[] rowsRouteTrip_Route = row.GetChildRows("RouteTrip_Route");
                if( rowsRouteTrip_Route != null && rowsRouteTrip_Route.Length > 0 ){
                    foreach (DataRow rowRouteTrip_Route in rowsRouteTrip_Route)
                    {
                        System.Int32 childRouteTripId = (System.Int32)rowRouteTrip_Route["ROUTE_TRIP_ID"];
                        CascadeRemove_RouteTrip(dataset, childRouteTripId);
                    }
                }
                DataRow[] rowsSchedule_Route = row.GetChildRows("Schedule_Route");
                if( rowsSchedule_Route != null && rowsSchedule_Route.Length > 0 ){
                    foreach (DataRow rowSchedule_Route in rowsSchedule_Route)
                    {
                        System.Int32 childScheduleId = (System.Int32)rowSchedule_Route["SCHEDULE_ID"];
                        CascadeRemove_Schedule(dataset, childScheduleId);
                    }
                }
                DataRow[] rowsWayout_Route = row.GetChildRows("Wayout_Route");
                if( rowsWayout_Route != null && rowsWayout_Route.Length > 0 ){
                    foreach (DataRow rowWayout_Route in rowsWayout_Route)
                    {
                        System.Int32 childWayoutId = (System.Int32)rowWayout_Route["WAYOUT_ID"];
                        CascadeRemove_Wayout(dataset, childWayoutId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [ROUTE]
        ///</summary>
        public static void Compact_Route(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ROUTE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ROUTE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [ROUTE]
        ///</summary>
        public static void CompactFreshRecords_Route(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ROUTE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ROUTE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["ROUTE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [ROUTE_GEOZONE]
        ///</summary>
        public static void CascadeRemove_RouteGeozone(DataSet dataset, System.Int32 RouteGeozoneId)
        {
            Debug.Assert(dataset.Tables.Contains("ROUTE_GEOZONE"));
            DataRow row = dataset.Tables["ROUTE_GEOZONE"].Rows.Find(RouteGeozoneId);
            if( row != null){
                DataRow[] rowsFK_SCHEDULE_GEOZONE_ROUTE_GEOZONE = row.GetChildRows("FK_SCHEDULE_GEOZONE_ROUTE_GEOZONE");
                if( rowsFK_SCHEDULE_GEOZONE_ROUTE_GEOZONE != null && rowsFK_SCHEDULE_GEOZONE_ROUTE_GEOZONE.Length > 0 ){
                    foreach (DataRow rowFK_SCHEDULE_GEOZONE_ROUTE_GEOZONE in rowsFK_SCHEDULE_GEOZONE_ROUTE_GEOZONE)
                    {
                        System.Int32 childScheduleGeozoneId = (System.Int32)rowFK_SCHEDULE_GEOZONE_ROUTE_GEOZONE["SCHEDULE_GEOZONE_ID"];
                        CascadeRemove_ScheduleGeozone(dataset, childScheduleGeozoneId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [ROUTE_GEOZONE]
        ///</summary>
        public static void Compact_RouteGeozone(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ROUTE_GEOZONE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ROUTE_GEOZONE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [ROUTE_GEOZONE]
        ///</summary>
        public static void CompactFreshRecords_RouteGeozone(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ROUTE_GEOZONE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ROUTE_GEOZONE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["ROUTE_GEOZONE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [ROUTE_POINT]
        ///</summary>
        public static void CascadeRemove_RoutePoint(DataSet dataset, System.Int32 RoutePointId)
        {
            Debug.Assert(dataset.Tables.Contains("ROUTE_POINT"));
            DataRow row = dataset.Tables["ROUTE_POINT"].Rows.Find(RoutePointId);
            if( row != null){
                DataRow[] rowsSchedulePoint_RoutePoint = row.GetChildRows("SchedulePoint_RoutePoint");
                if( rowsSchedulePoint_RoutePoint != null && rowsSchedulePoint_RoutePoint.Length > 0 ){
                    foreach (DataRow rowSchedulePoint_RoutePoint in rowsSchedulePoint_RoutePoint)
                    {
                        System.Int32 childSchedulePointId = (System.Int32)rowSchedulePoint_RoutePoint["SCHEDULE_POINT_ID"];
                        CascadeRemove_SchedulePoint(dataset, childSchedulePointId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [ROUTE_POINT]
        ///</summary>
        public static void Compact_RoutePoint(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ROUTE_POINT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ROUTE_POINT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [ROUTE_POINT]
        ///</summary>
        public static void CompactFreshRecords_RoutePoint(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ROUTE_POINT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ROUTE_POINT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["ROUTE_POINT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [ROUTE_TRIP]
        ///</summary>
        public static void CascadeRemove_RouteTrip(DataSet dataset, System.Int32 RouteTripId)
        {
            Debug.Assert(dataset.Tables.Contains("ROUTE_TRIP"));
            DataRow row = dataset.Tables["ROUTE_TRIP"].Rows.Find(RouteTripId);
            if( row != null){
                DataRow[] rowsFK_RS_TRIP0_ROUTE_TRIP = row.GetChildRows("FK_RS_TRIP0_ROUTE_TRIP");
                if( rowsFK_RS_TRIP0_ROUTE_TRIP != null && rowsFK_RS_TRIP0_ROUTE_TRIP.Length > 0 ){
                    foreach (DataRow rowFK_RS_TRIP0_ROUTE_TRIP in rowsFK_RS_TRIP0_ROUTE_TRIP)
                    {
                        System.Int32 childRsTripId = (System.Int32)rowFK_RS_TRIP0_ROUTE_TRIP["RS_TRIP_ID"];
                        CascadeRemove_RsTrip(dataset, childRsTripId);
                    }
                }
                DataRow[] rowsTrip_RouteTrip = row.GetChildRows("Trip_RouteTrip");
                if( rowsTrip_RouteTrip != null && rowsTrip_RouteTrip.Length > 0 ){
                    foreach (DataRow rowTrip_RouteTrip in rowsTrip_RouteTrip)
                    {
                        System.Int32 childTripId = (System.Int32)rowTrip_RouteTrip["TRIP_ID"];
                        CascadeRemove_Trip(dataset, childTripId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [ROUTE_TRIP]
        ///</summary>
        public static void Compact_RouteTrip(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ROUTE_TRIP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ROUTE_TRIP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [ROUTE_TRIP]
        ///</summary>
        public static void CompactFreshRecords_RouteTrip(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ROUTE_TRIP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ROUTE_TRIP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["ROUTE_TRIP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [ROUTEGROUP]
        ///</summary>
        public static void CascadeRemove_Routegroup(DataSet dataset, System.Int32 RoutegroupId)
        {
            Debug.Assert(dataset.Tables.Contains("ROUTEGROUP"));
            DataRow row = dataset.Tables["ROUTEGROUP"].Rows.Find(RoutegroupId);
            if( row != null){
                DataRow[] rowsOperatorGroupRouteGroup_RouteGroup = row.GetChildRows("OperatorGroupRouteGroup_RouteGroup");
                if( rowsOperatorGroupRouteGroup_RouteGroup != null && rowsOperatorGroupRouteGroup_RouteGroup.Length > 0 ){
                    foreach (DataRow rowOperatorGroupRouteGroup_RouteGroup in rowsOperatorGroupRouteGroup_RouteGroup)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupRouteGroup_RouteGroup["OPERATORGROUP_ID"];
                        System.Int32 childRoutegroupId = (System.Int32)rowOperatorGroupRouteGroup_RouteGroup["ROUTEGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupRouteGroup_RouteGroup["RIGHT_ID"];
                        CascadeRemove_OperatorgroupRoutegroup(dataset, childOperatorgroupId, childRoutegroupId, childRightId);
                    }
                }
                DataRow[] rowsOperatorRouteGroup_RouteGroup = row.GetChildRows("OperatorRouteGroup_RouteGroup");
                if( rowsOperatorRouteGroup_RouteGroup != null && rowsOperatorRouteGroup_RouteGroup.Length > 0 ){
                    foreach (DataRow rowOperatorRouteGroup_RouteGroup in rowsOperatorRouteGroup_RouteGroup)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorRouteGroup_RouteGroup["OPERATOR_ID"];
                        System.Int32 childRoutegroupId = (System.Int32)rowOperatorRouteGroup_RouteGroup["ROUTEGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorRouteGroup_RouteGroup["RIGHT_ID"];
                        CascadeRemove_OperatorRoutegroup(dataset, childOperatorId, childRoutegroupId, childRightId);
                    }
                }
                DataRow[] rowsRouteGroupRoute_RouteGroup = row.GetChildRows("RouteGroupRoute_RouteGroup");
                if( rowsRouteGroupRoute_RouteGroup != null && rowsRouteGroupRoute_RouteGroup.Length > 0 ){
                    foreach (DataRow rowRouteGroupRoute_RouteGroup in rowsRouteGroupRoute_RouteGroup)
                    {
                        System.Int32 childRoutegroupId = (System.Int32)rowRouteGroupRoute_RouteGroup["ROUTEGROUP_ID"];
                        System.Int32 childRouteId = (System.Int32)rowRouteGroupRoute_RouteGroup["ROUTE_ID"];
                        CascadeRemove_RoutegroupRoute(dataset, childRoutegroupId, childRouteId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [ROUTEGROUP]
        ///</summary>
        public static void Compact_Routegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ROUTEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ROUTEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [ROUTEGROUP]
        ///</summary>
        public static void CompactFreshRecords_Routegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ROUTEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ROUTEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["ROUTEGROUP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [ROUTEGROUP_ROUTE]
        ///</summary>
        public static void CascadeRemove_RoutegroupRoute(DataSet dataset, System.Int32 RoutegroupId, System.Int32 RouteId)
        {
            Debug.Assert(dataset.Tables.Contains("ROUTEGROUP_ROUTE"));
            DataRow row = dataset.Tables["ROUTEGROUP_ROUTE"].Rows.Find(new object[]{RoutegroupId, RouteId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [ROUTEGROUP_ROUTE]
        ///</summary>
        public static void Compact_RoutegroupRoute(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ROUTEGROUP_ROUTE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ROUTEGROUP_ROUTE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [ROUTEGROUP_ROUTE]
        ///</summary>
        public static void CompactFreshRecords_RoutegroupRoute(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ROUTEGROUP_ROUTE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ROUTEGROUP_ROUTE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["ROUTEGROUP_ROUTE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RS]
        ///</summary>
        public static void CascadeRemove_Rs(DataSet dataset, System.Int32 RsId)
        {
            Debug.Assert(dataset.Tables.Contains("RS"));
            DataRow row = dataset.Tables["RS"].Rows.Find(RsId);
            if( row != null){
                DataRow[] rowsFK_RS_VARIANT_RS = row.GetChildRows("FK_RS_VARIANT_RS");
                if( rowsFK_RS_VARIANT_RS != null && rowsFK_RS_VARIANT_RS.Length > 0 ){
                    foreach (DataRow rowFK_RS_VARIANT_RS in rowsFK_RS_VARIANT_RS)
                    {
                        System.Int32 childRsVariantId = (System.Int32)rowFK_RS_VARIANT_RS["RS_VARIANT_ID"];
                        CascadeRemove_RsVariant(dataset, childRsVariantId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [RS]
        ///</summary>
        public static void Compact_Rs(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RS]
        ///</summary>
        public static void CompactFreshRecords_Rs(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RS"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RS_NUMBER]
        ///</summary>
        public static void CascadeRemove_RsNumber(DataSet dataset, System.Int32 RsNumberId)
        {
            Debug.Assert(dataset.Tables.Contains("RS_NUMBER"));
            DataRow row = dataset.Tables["RS_NUMBER"].Rows.Find(RsNumberId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_NUMBER]
        ///</summary>
        public static void Compact_RsNumber(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_NUMBER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_NUMBER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_NUMBER]
        ///</summary>
        public static void CompactFreshRecords_RsNumber(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_NUMBER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_NUMBER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RS_NUMBER"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RS_OPERATIONSTYPE]
        ///</summary>
        public static void CascadeRemove_RsOperationstype(DataSet dataset, System.Int32 RsOperationstypeId)
        {
            Debug.Assert(dataset.Tables.Contains("RS_OPERATIONSTYPE"));
            DataRow row = dataset.Tables["RS_OPERATIONSTYPE"].Rows.Find(RsOperationstypeId);
            if( row != null){
                DataRow[] rowsFK_RS_OPERATIONTYPE_RS_OPERATIONSTYPE = row.GetChildRows("FK_RS_OPERATIONTYPE_RS_OPERATIONSTYPE");
                if( rowsFK_RS_OPERATIONTYPE_RS_OPERATIONSTYPE != null && rowsFK_RS_OPERATIONTYPE_RS_OPERATIONSTYPE.Length > 0 ){
                    foreach (DataRow rowFK_RS_OPERATIONTYPE_RS_OPERATIONSTYPE in rowsFK_RS_OPERATIONTYPE_RS_OPERATIONSTYPE)
                    {
                        System.Int32 childRsOperationtypeId = (System.Int32)rowFK_RS_OPERATIONTYPE_RS_OPERATIONSTYPE["RS_OPERATIONTYPE_ID"];
                        CascadeRemove_RsOperationtype(dataset, childRsOperationtypeId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_OPERATIONSTYPE]
        ///</summary>
        public static void Compact_RsOperationstype(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_OPERATIONSTYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_OPERATIONSTYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_OPERATIONSTYPE]
        ///</summary>
        public static void CompactFreshRecords_RsOperationstype(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_OPERATIONSTYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_OPERATIONSTYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RS_OPERATIONSTYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RS_OPERATIONTYPE]
        ///</summary>
        public static void CascadeRemove_RsOperationtype(DataSet dataset, System.Int32 RsOperationtypeId)
        {
            Debug.Assert(dataset.Tables.Contains("RS_OPERATIONTYPE"));
            DataRow row = dataset.Tables["RS_OPERATIONTYPE"].Rows.Find(RsOperationtypeId);
            if( row != null){
                DataRow[] rowsFK_RS_POINT_RS_OPERATIONTYPE = row.GetChildRows("FK_RS_POINT_RS_OPERATIONTYPE");
                if( rowsFK_RS_POINT_RS_OPERATIONTYPE != null && rowsFK_RS_POINT_RS_OPERATIONTYPE.Length > 0 ){
                    foreach (DataRow rowFK_RS_POINT_RS_OPERATIONTYPE in rowsFK_RS_POINT_RS_OPERATIONTYPE)
                    {
                        System.Int32 childRsPointId = (System.Int32)rowFK_RS_POINT_RS_OPERATIONTYPE["RS_POINT_ID"];
                        CascadeRemove_RsPoint(dataset, childRsPointId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_OPERATIONTYPE]
        ///</summary>
        public static void Compact_RsOperationtype(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_OPERATIONTYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_OPERATIONTYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_OPERATIONTYPE]
        ///</summary>
        public static void CompactFreshRecords_RsOperationtype(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_OPERATIONTYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_OPERATIONTYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RS_OPERATIONTYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RS_PERIOD]
        ///</summary>
        public static void CascadeRemove_RsPeriod(DataSet dataset, System.Int32 PeriodId)
        {
            Debug.Assert(dataset.Tables.Contains("RS_PERIOD"));
            DataRow row = dataset.Tables["RS_PERIOD"].Rows.Find(PeriodId);
            if( row != null){
                DataRow[] rowsFK_RS_RUNTIME_RS_PERIOD = row.GetChildRows("FK_RS_RUNTIME_RS_PERIOD");
                if( rowsFK_RS_RUNTIME_RS_PERIOD != null && rowsFK_RS_RUNTIME_RS_PERIOD.Length > 0 ){
                    foreach (DataRow rowFK_RS_RUNTIME_RS_PERIOD in rowsFK_RS_RUNTIME_RS_PERIOD)
                    {
                        System.Int32 childRsRuntimeId = (System.Int32)rowFK_RS_RUNTIME_RS_PERIOD["RS_RUNTIME_ID"];
                        CascadeRemove_RsRuntime(dataset, childRsRuntimeId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_PERIOD]
        ///</summary>
        public static void Compact_RsPeriod(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_PERIOD"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_PERIOD"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_PERIOD]
        ///</summary>
        public static void CompactFreshRecords_RsPeriod(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_PERIOD"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_PERIOD"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RS_PERIOD"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RS_POINT]
        ///</summary>
        public static void CascadeRemove_RsPoint(DataSet dataset, System.Int32 RsPointId)
        {
            Debug.Assert(dataset.Tables.Contains("RS_POINT"));
            DataRow row = dataset.Tables["RS_POINT"].Rows.Find(RsPointId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_POINT]
        ///</summary>
        public static void Compact_RsPoint(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_POINT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_POINT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_POINT]
        ///</summary>
        public static void CompactFreshRecords_RsPoint(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_POINT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_POINT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RS_POINT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RS_ROUND_TRIP]
        ///</summary>
        public static void CascadeRemove_RsRoundTrip(DataSet dataset, System.Int32 RsRoundTripId)
        {
            Debug.Assert(dataset.Tables.Contains("RS_ROUND_TRIP"));
            DataRow row = dataset.Tables["RS_ROUND_TRIP"].Rows.Find(RsRoundTripId);
            if( row != null){
                DataRow[] rowsFK_RS_POINT_RS_TRIP = row.GetChildRows("FK_RS_POINT_RS_TRIP");
                if( rowsFK_RS_POINT_RS_TRIP != null && rowsFK_RS_POINT_RS_TRIP.Length > 0 ){
                    foreach (DataRow rowFK_RS_POINT_RS_TRIP in rowsFK_RS_POINT_RS_TRIP)
                    {
                        System.Int32 childRsPointId = (System.Int32)rowFK_RS_POINT_RS_TRIP["RS_POINT_ID"];
                        CascadeRemove_RsPoint(dataset, childRsPointId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_ROUND_TRIP]
        ///</summary>
        public static void Compact_RsRoundTrip(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_ROUND_TRIP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_ROUND_TRIP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_ROUND_TRIP]
        ///</summary>
        public static void CompactFreshRecords_RsRoundTrip(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_ROUND_TRIP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_ROUND_TRIP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RS_ROUND_TRIP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RS_RUNTIME]
        ///</summary>
        public static void CascadeRemove_RsRuntime(DataSet dataset, System.Int32 RsRuntimeId)
        {
            Debug.Assert(dataset.Tables.Contains("RS_RUNTIME"));
            DataRow row = dataset.Tables["RS_RUNTIME"].Rows.Find(RsRuntimeId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_RUNTIME]
        ///</summary>
        public static void Compact_RsRuntime(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_RUNTIME"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_RUNTIME"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_RUNTIME]
        ///</summary>
        public static void CompactFreshRecords_RsRuntime(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_RUNTIME"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_RUNTIME"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RS_RUNTIME"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RS_SHIFT]
        ///</summary>
        public static void CascadeRemove_RsShift(DataSet dataset, System.Int32 RsShiftId)
        {
            Debug.Assert(dataset.Tables.Contains("RS_SHIFT"));
            DataRow row = dataset.Tables["RS_SHIFT"].Rows.Find(RsShiftId);
            if( row != null){
                DataRow[] rowsFK_RS_POINT_RS_SHIFT = row.GetChildRows("FK_RS_POINT_RS_SHIFT");
                if( rowsFK_RS_POINT_RS_SHIFT != null && rowsFK_RS_POINT_RS_SHIFT.Length > 0 ){
                    foreach (DataRow rowFK_RS_POINT_RS_SHIFT in rowsFK_RS_POINT_RS_SHIFT)
                    {
                        System.Int32 childRsPointId = (System.Int32)rowFK_RS_POINT_RS_SHIFT["RS_POINT_ID"];
                        CascadeRemove_RsPoint(dataset, childRsPointId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_SHIFT]
        ///</summary>
        public static void Compact_RsShift(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_SHIFT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_SHIFT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_SHIFT]
        ///</summary>
        public static void CompactFreshRecords_RsShift(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_SHIFT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_SHIFT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RS_SHIFT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RS_STEP]
        ///</summary>
        public static void CascadeRemove_RsStep(DataSet dataset, System.Int32 RsStepId)
        {
            Debug.Assert(dataset.Tables.Contains("RS_STEP"));
            DataRow row = dataset.Tables["RS_STEP"].Rows.Find(RsStepId);
            if( row != null){
                DataRow[] rowsFK_RS_NUMBER_RS_STEP = row.GetChildRows("FK_RS_NUMBER_RS_STEP");
                if( rowsFK_RS_NUMBER_RS_STEP != null && rowsFK_RS_NUMBER_RS_STEP.Length > 0 ){
                    foreach (DataRow rowFK_RS_NUMBER_RS_STEP in rowsFK_RS_NUMBER_RS_STEP)
                    {
                        System.Int32 childRsNumberId = (System.Int32)rowFK_RS_NUMBER_RS_STEP["RS_NUMBER_ID"];
                        CascadeRemove_RsNumber(dataset, childRsNumberId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_STEP]
        ///</summary>
        public static void Compact_RsStep(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_STEP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_STEP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_STEP]
        ///</summary>
        public static void CompactFreshRecords_RsStep(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_STEP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_STEP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RS_STEP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RS_TRIP]
        ///</summary>
        public static void CascadeRemove_RsTrip(DataSet dataset, System.Int32 RsTripId)
        {
            Debug.Assert(dataset.Tables.Contains("RS_TRIP"));
            DataRow row = dataset.Tables["RS_TRIP"].Rows.Find(RsTripId);
            if( row != null){
                DataRow[] rowsFK_RS_RUNTIME_RS_TRIP = row.GetChildRows("FK_RS_RUNTIME_RS_TRIP");
                if( rowsFK_RS_RUNTIME_RS_TRIP != null && rowsFK_RS_RUNTIME_RS_TRIP.Length > 0 ){
                    foreach (DataRow rowFK_RS_RUNTIME_RS_TRIP in rowsFK_RS_RUNTIME_RS_TRIP)
                    {
                        System.Int32 childRsRuntimeId = (System.Int32)rowFK_RS_RUNTIME_RS_TRIP["RS_RUNTIME_ID"];
                        CascadeRemove_RsRuntime(dataset, childRsRuntimeId);
                    }
                }
                DataRow[] rowsFK_RSOT_TRIPIN_RS_TRIP = row.GetChildRows("FK_RSOT_TRIPIN_RS_TRIP");
                if( rowsFK_RSOT_TRIPIN_RS_TRIP != null && rowsFK_RSOT_TRIPIN_RS_TRIP.Length > 0 ){
                    foreach (DataRow rowFK_RSOT_TRIPIN_RS_TRIP in rowsFK_RSOT_TRIPIN_RS_TRIP)
                    {
                        System.Int32 childRsOperationtypeId = (System.Int32)rowFK_RSOT_TRIPIN_RS_TRIP["RS_OPERATIONTYPE_ID"];
                        CascadeRemove_RsOperationtype(dataset, childRsOperationtypeId);
                    }
                }
                DataRow[] rowsFK_RSOT_TRIPIN0_RS_TRIP = row.GetChildRows("FK_RSOT_TRIPIN0_RS_TRIP");
                if( rowsFK_RSOT_TRIPIN0_RS_TRIP != null && rowsFK_RSOT_TRIPIN0_RS_TRIP.Length > 0 ){
                    foreach (DataRow rowFK_RSOT_TRIPIN0_RS_TRIP in rowsFK_RSOT_TRIPIN0_RS_TRIP)
                    {
                        System.Int32 childRsOperationtypeId = (System.Int32)rowFK_RSOT_TRIPIN0_RS_TRIP["RS_OPERATIONTYPE_ID"];
                        CascadeRemove_RsOperationtype(dataset, childRsOperationtypeId);
                    }
                }
                DataRow[] rowsFK_RSOT_TRIPOUT_RS_TRIP = row.GetChildRows("FK_RSOT_TRIPOUT_RS_TRIP");
                if( rowsFK_RSOT_TRIPOUT_RS_TRIP != null && rowsFK_RSOT_TRIPOUT_RS_TRIP.Length > 0 ){
                    foreach (DataRow rowFK_RSOT_TRIPOUT_RS_TRIP in rowsFK_RSOT_TRIPOUT_RS_TRIP)
                    {
                        System.Int32 childRsOperationtypeId = (System.Int32)rowFK_RSOT_TRIPOUT_RS_TRIP["RS_OPERATIONTYPE_ID"];
                        CascadeRemove_RsOperationtype(dataset, childRsOperationtypeId);
                    }
                }
                DataRow[] rowsFK_RSOT_TRIPOUT0_RS_TRIP = row.GetChildRows("FK_RSOT_TRIPOUT0_RS_TRIP");
                if( rowsFK_RSOT_TRIPOUT0_RS_TRIP != null && rowsFK_RSOT_TRIPOUT0_RS_TRIP.Length > 0 ){
                    foreach (DataRow rowFK_RSOT_TRIPOUT0_RS_TRIP in rowsFK_RSOT_TRIPOUT0_RS_TRIP)
                    {
                        System.Int32 childRsOperationtypeId = (System.Int32)rowFK_RSOT_TRIPOUT0_RS_TRIP["RS_OPERATIONTYPE_ID"];
                        CascadeRemove_RsOperationtype(dataset, childRsOperationtypeId);
                    }
                }
                DataRow[] rowsFK_RSSHIFT_TRIPIN_RS_TRIP = row.GetChildRows("FK_RSSHIFT_TRIPIN_RS_TRIP");
                if( rowsFK_RSSHIFT_TRIPIN_RS_TRIP != null && rowsFK_RSSHIFT_TRIPIN_RS_TRIP.Length > 0 ){
                    foreach (DataRow rowFK_RSSHIFT_TRIPIN_RS_TRIP in rowsFK_RSSHIFT_TRIPIN_RS_TRIP)
                    {
                        System.Int32 childRsShiftId = (System.Int32)rowFK_RSSHIFT_TRIPIN_RS_TRIP["RS_SHIFT_ID"];
                        CascadeRemove_RsShift(dataset, childRsShiftId);
                    }
                }
                DataRow[] rowsFK_RSSHIFT_TRIPIN0_RS_TRIP = row.GetChildRows("FK_RSSHIFT_TRIPIN0_RS_TRIP");
                if( rowsFK_RSSHIFT_TRIPIN0_RS_TRIP != null && rowsFK_RSSHIFT_TRIPIN0_RS_TRIP.Length > 0 ){
                    foreach (DataRow rowFK_RSSHIFT_TRIPIN0_RS_TRIP in rowsFK_RSSHIFT_TRIPIN0_RS_TRIP)
                    {
                        System.Int32 childRsShiftId = (System.Int32)rowFK_RSSHIFT_TRIPIN0_RS_TRIP["RS_SHIFT_ID"];
                        CascadeRemove_RsShift(dataset, childRsShiftId);
                    }
                }
                DataRow[] rowsFK_RSSHIFT_TRIPOUT_RS_TRIP = row.GetChildRows("FK_RSSHIFT_TRIPOUT_RS_TRIP");
                if( rowsFK_RSSHIFT_TRIPOUT_RS_TRIP != null && rowsFK_RSSHIFT_TRIPOUT_RS_TRIP.Length > 0 ){
                    foreach (DataRow rowFK_RSSHIFT_TRIPOUT_RS_TRIP in rowsFK_RSSHIFT_TRIPOUT_RS_TRIP)
                    {
                        System.Int32 childRsShiftId = (System.Int32)rowFK_RSSHIFT_TRIPOUT_RS_TRIP["RS_SHIFT_ID"];
                        CascadeRemove_RsShift(dataset, childRsShiftId);
                    }
                }
                DataRow[] rowsFK_RSSHIFT_TRIPOUT0_RS_TRIP = row.GetChildRows("FK_RSSHIFT_TRIPOUT0_RS_TRIP");
                if( rowsFK_RSSHIFT_TRIPOUT0_RS_TRIP != null && rowsFK_RSSHIFT_TRIPOUT0_RS_TRIP.Length > 0 ){
                    foreach (DataRow rowFK_RSSHIFT_TRIPOUT0_RS_TRIP in rowsFK_RSSHIFT_TRIPOUT0_RS_TRIP)
                    {
                        System.Int32 childRsShiftId = (System.Int32)rowFK_RSSHIFT_TRIPOUT0_RS_TRIP["RS_SHIFT_ID"];
                        CascadeRemove_RsShift(dataset, childRsShiftId);
                    }
                }
                DataRow[] rowsFK_RSTT_TRIPA_RS_TRIP = row.GetChildRows("FK_RSTT_TRIPA_RS_TRIP");
                if( rowsFK_RSTT_TRIPA_RS_TRIP != null && rowsFK_RSTT_TRIPA_RS_TRIP.Length > 0 ){
                    foreach (DataRow rowFK_RSTT_TRIPA_RS_TRIP in rowsFK_RSTT_TRIPA_RS_TRIP)
                    {
                        System.Int32 childRsTriptypeId = (System.Int32)rowFK_RSTT_TRIPA_RS_TRIP["RS_TRIPTYPE_ID"];
                        CascadeRemove_RsTriptype(dataset, childRsTriptypeId);
                    }
                }
                DataRow[] rowsFK_RSTT_TRIPB_RS_TRIP = row.GetChildRows("FK_RSTT_TRIPB_RS_TRIP");
                if( rowsFK_RSTT_TRIPB_RS_TRIP != null && rowsFK_RSTT_TRIPB_RS_TRIP.Length > 0 ){
                    foreach (DataRow rowFK_RSTT_TRIPB_RS_TRIP in rowsFK_RSTT_TRIPB_RS_TRIP)
                    {
                        System.Int32 childRsTriptypeId = (System.Int32)rowFK_RSTT_TRIPB_RS_TRIP["RS_TRIPTYPE_ID"];
                        CascadeRemove_RsTriptype(dataset, childRsTriptypeId);
                    }
                }
                DataRow[] rowsFK_RSW_TRIPIN_RS_TRIP = row.GetChildRows("FK_RSW_TRIPIN_RS_TRIP");
                if( rowsFK_RSW_TRIPIN_RS_TRIP != null && rowsFK_RSW_TRIPIN_RS_TRIP.Length > 0 ){
                    foreach (DataRow rowFK_RSW_TRIPIN_RS_TRIP in rowsFK_RSW_TRIPIN_RS_TRIP)
                    {
                        System.Int32 childRsWayoutId = (System.Int32)rowFK_RSW_TRIPIN_RS_TRIP["RS_WAYOUT_ID"];
                        CascadeRemove_RsWayout(dataset, childRsWayoutId);
                    }
                }
                DataRow[] rowsFK_RSW_TRIPIN0_RS_TRIP0 = row.GetChildRows("FK_RSW_TRIPIN0_RS_TRIP0");
                if( rowsFK_RSW_TRIPIN0_RS_TRIP0 != null && rowsFK_RSW_TRIPIN0_RS_TRIP0.Length > 0 ){
                    foreach (DataRow rowFK_RSW_TRIPIN0_RS_TRIP0 in rowsFK_RSW_TRIPIN0_RS_TRIP0)
                    {
                        System.Int32 childRsWayoutId = (System.Int32)rowFK_RSW_TRIPIN0_RS_TRIP0["RS_WAYOUT_ID"];
                        CascadeRemove_RsWayout(dataset, childRsWayoutId);
                    }
                }
                DataRow[] rowsFK_RSW_TRIPOUT_RS_TRIP = row.GetChildRows("FK_RSW_TRIPOUT_RS_TRIP");
                if( rowsFK_RSW_TRIPOUT_RS_TRIP != null && rowsFK_RSW_TRIPOUT_RS_TRIP.Length > 0 ){
                    foreach (DataRow rowFK_RSW_TRIPOUT_RS_TRIP in rowsFK_RSW_TRIPOUT_RS_TRIP)
                    {
                        System.Int32 childRsWayoutId = (System.Int32)rowFK_RSW_TRIPOUT_RS_TRIP["RS_WAYOUT_ID"];
                        CascadeRemove_RsWayout(dataset, childRsWayoutId);
                    }
                }
                DataRow[] rowsFK_RSW_TRIPOUT0_RS_TRIP0 = row.GetChildRows("FK_RSW_TRIPOUT0_RS_TRIP0");
                if( rowsFK_RSW_TRIPOUT0_RS_TRIP0 != null && rowsFK_RSW_TRIPOUT0_RS_TRIP0.Length > 0 ){
                    foreach (DataRow rowFK_RSW_TRIPOUT0_RS_TRIP0 in rowsFK_RSW_TRIPOUT0_RS_TRIP0)
                    {
                        System.Int32 childRsWayoutId = (System.Int32)rowFK_RSW_TRIPOUT0_RS_TRIP0["RS_WAYOUT_ID"];
                        CascadeRemove_RsWayout(dataset, childRsWayoutId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_TRIP]
        ///</summary>
        public static void Compact_RsTrip(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_TRIP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_TRIP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_TRIP]
        ///</summary>
        public static void CompactFreshRecords_RsTrip(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_TRIP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_TRIP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RS_TRIP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RS_TRIPSTYPE]
        ///</summary>
        public static void CascadeRemove_RsTripstype(DataSet dataset, System.Int32 RsTripstypeId)
        {
            Debug.Assert(dataset.Tables.Contains("RS_TRIPSTYPE"));
            DataRow row = dataset.Tables["RS_TRIPSTYPE"].Rows.Find(RsTripstypeId);
            if( row != null){
                DataRow[] rowsFK_RS_TRIPTYPE_RS_TRIPSTYPE = row.GetChildRows("FK_RS_TRIPTYPE_RS_TRIPSTYPE");
                if( rowsFK_RS_TRIPTYPE_RS_TRIPSTYPE != null && rowsFK_RS_TRIPTYPE_RS_TRIPSTYPE.Length > 0 ){
                    foreach (DataRow rowFK_RS_TRIPTYPE_RS_TRIPSTYPE in rowsFK_RS_TRIPTYPE_RS_TRIPSTYPE)
                    {
                        System.Int32 childRsTriptypeId = (System.Int32)rowFK_RS_TRIPTYPE_RS_TRIPSTYPE["RS_TRIPTYPE_ID"];
                        CascadeRemove_RsTriptype(dataset, childRsTriptypeId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_TRIPSTYPE]
        ///</summary>
        public static void Compact_RsTripstype(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_TRIPSTYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_TRIPSTYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_TRIPSTYPE]
        ///</summary>
        public static void CompactFreshRecords_RsTripstype(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_TRIPSTYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_TRIPSTYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RS_TRIPSTYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RS_TRIPTYPE]
        ///</summary>
        public static void CascadeRemove_RsTriptype(DataSet dataset, System.Int32 RsTriptypeId)
        {
            Debug.Assert(dataset.Tables.Contains("RS_TRIPTYPE"));
            DataRow row = dataset.Tables["RS_TRIPTYPE"].Rows.Find(RsTriptypeId);
            if( row != null){
                DataRow[] rowsFK_RS_POINT_RS_TRIPTYPE = row.GetChildRows("FK_RS_POINT_RS_TRIPTYPE");
                if( rowsFK_RS_POINT_RS_TRIPTYPE != null && rowsFK_RS_POINT_RS_TRIPTYPE.Length > 0 ){
                    foreach (DataRow rowFK_RS_POINT_RS_TRIPTYPE in rowsFK_RS_POINT_RS_TRIPTYPE)
                    {
                        System.Int32 childRsPointId = (System.Int32)rowFK_RS_POINT_RS_TRIPTYPE["RS_POINT_ID"];
                        CascadeRemove_RsPoint(dataset, childRsPointId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_TRIPTYPE]
        ///</summary>
        public static void Compact_RsTriptype(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_TRIPTYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_TRIPTYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_TRIPTYPE]
        ///</summary>
        public static void CompactFreshRecords_RsTriptype(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_TRIPTYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_TRIPTYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RS_TRIPTYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RS_TYPE]
        ///</summary>
        public static void CascadeRemove_RsType(DataSet dataset, System.Int32 RsTypeId)
        {
            Debug.Assert(dataset.Tables.Contains("RS_TYPE"));
            DataRow row = dataset.Tables["RS_TYPE"].Rows.Find(RsTypeId);
            if( row != null){
                DataRow[] rowsFK_RS_VARIANT_RS_TYPE = row.GetChildRows("FK_RS_VARIANT_RS_TYPE");
                if( rowsFK_RS_VARIANT_RS_TYPE != null && rowsFK_RS_VARIANT_RS_TYPE.Length > 0 ){
                    foreach (DataRow rowFK_RS_VARIANT_RS_TYPE in rowsFK_RS_VARIANT_RS_TYPE)
                    {
                        System.Int32 childRsVariantId = (System.Int32)rowFK_RS_VARIANT_RS_TYPE["RS_VARIANT_ID"];
                        CascadeRemove_RsVariant(dataset, childRsVariantId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_TYPE]
        ///</summary>
        public static void Compact_RsType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_TYPE]
        ///</summary>
        public static void CompactFreshRecords_RsType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RS_TYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RS_VARIANT]
        ///</summary>
        public static void CascadeRemove_RsVariant(DataSet dataset, System.Int32 RsVariantId)
        {
            Debug.Assert(dataset.Tables.Contains("RS_VARIANT"));
            DataRow row = dataset.Tables["RS_VARIANT"].Rows.Find(RsVariantId);
            if( row != null){
                DataRow[] rowsFK_RS_PERIOD_RS_VARIANT = row.GetChildRows("FK_RS_PERIOD_RS_VARIANT");
                if( rowsFK_RS_PERIOD_RS_VARIANT != null && rowsFK_RS_PERIOD_RS_VARIANT.Length > 0 ){
                    foreach (DataRow rowFK_RS_PERIOD_RS_VARIANT in rowsFK_RS_PERIOD_RS_VARIANT)
                    {
                        System.Int32 childPeriodId = (System.Int32)rowFK_RS_PERIOD_RS_VARIANT["PERIOD_ID"];
                        CascadeRemove_RsPeriod(dataset, childPeriodId);
                    }
                }
                DataRow[] rowsFK_RS_ROUND_TRIP_RS_VARIANT = row.GetChildRows("FK_RS_ROUND_TRIP_RS_VARIANT");
                if( rowsFK_RS_ROUND_TRIP_RS_VARIANT != null && rowsFK_RS_ROUND_TRIP_RS_VARIANT.Length > 0 ){
                    foreach (DataRow rowFK_RS_ROUND_TRIP_RS_VARIANT in rowsFK_RS_ROUND_TRIP_RS_VARIANT)
                    {
                        System.Int32 childRsRoundTripId = (System.Int32)rowFK_RS_ROUND_TRIP_RS_VARIANT["RS_ROUND_TRIP_ID"];
                        CascadeRemove_RsRoundTrip(dataset, childRsRoundTripId);
                    }
                }
                DataRow[] rowsFK_RS_STEP_RS_VARIANT = row.GetChildRows("FK_RS_STEP_RS_VARIANT");
                if( rowsFK_RS_STEP_RS_VARIANT != null && rowsFK_RS_STEP_RS_VARIANT.Length > 0 ){
                    foreach (DataRow rowFK_RS_STEP_RS_VARIANT in rowsFK_RS_STEP_RS_VARIANT)
                    {
                        System.Int32 childRsStepId = (System.Int32)rowFK_RS_STEP_RS_VARIANT["RS_STEP_ID"];
                        CascadeRemove_RsStep(dataset, childRsStepId);
                    }
                }
                DataRow[] rowsFK_RS_TRIP_RS_VARIANT = row.GetChildRows("FK_RS_TRIP_RS_VARIANT");
                if( rowsFK_RS_TRIP_RS_VARIANT != null && rowsFK_RS_TRIP_RS_VARIANT.Length > 0 ){
                    foreach (DataRow rowFK_RS_TRIP_RS_VARIANT in rowsFK_RS_TRIP_RS_VARIANT)
                    {
                        System.Int32 childRsTripId = (System.Int32)rowFK_RS_TRIP_RS_VARIANT["RS_TRIP_ID"];
                        CascadeRemove_RsTrip(dataset, childRsTripId);
                    }
                }
                DataRow[] rowsFK_RS_TRIPTYPE_RS_VARIANT = row.GetChildRows("FK_RS_TRIPTYPE_RS_VARIANT");
                if( rowsFK_RS_TRIPTYPE_RS_VARIANT != null && rowsFK_RS_TRIPTYPE_RS_VARIANT.Length > 0 ){
                    foreach (DataRow rowFK_RS_TRIPTYPE_RS_VARIANT in rowsFK_RS_TRIPTYPE_RS_VARIANT)
                    {
                        System.Int32 childRsTriptypeId = (System.Int32)rowFK_RS_TRIPTYPE_RS_VARIANT["RS_TRIPTYPE_ID"];
                        CascadeRemove_RsTriptype(dataset, childRsTriptypeId);
                    }
                }
                DataRow[] rowsFK_RS_WAYOUT_RS_VARIANT = row.GetChildRows("FK_RS_WAYOUT_RS_VARIANT");
                if( rowsFK_RS_WAYOUT_RS_VARIANT != null && rowsFK_RS_WAYOUT_RS_VARIANT.Length > 0 ){
                    foreach (DataRow rowFK_RS_WAYOUT_RS_VARIANT in rowsFK_RS_WAYOUT_RS_VARIANT)
                    {
                        System.Int32 childRsWayoutId = (System.Int32)rowFK_RS_WAYOUT_RS_VARIANT["RS_WAYOUT_ID"];
                        CascadeRemove_RsWayout(dataset, childRsWayoutId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_VARIANT]
        ///</summary>
        public static void Compact_RsVariant(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_VARIANT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_VARIANT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_VARIANT]
        ///</summary>
        public static void CompactFreshRecords_RsVariant(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_VARIANT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_VARIANT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RS_VARIANT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RS_WAYOUT]
        ///</summary>
        public static void CascadeRemove_RsWayout(DataSet dataset, System.Int32 RsWayoutId)
        {
            Debug.Assert(dataset.Tables.Contains("RS_WAYOUT"));
            DataRow row = dataset.Tables["RS_WAYOUT"].Rows.Find(RsWayoutId);
            if( row != null){
                DataRow[] rowsFK_RS_POINT_RS_WAYOUT = row.GetChildRows("FK_RS_POINT_RS_WAYOUT");
                if( rowsFK_RS_POINT_RS_WAYOUT != null && rowsFK_RS_POINT_RS_WAYOUT.Length > 0 ){
                    foreach (DataRow rowFK_RS_POINT_RS_WAYOUT in rowsFK_RS_POINT_RS_WAYOUT)
                    {
                        System.Int32 childRsPointId = (System.Int32)rowFK_RS_POINT_RS_WAYOUT["RS_POINT_ID"];
                        CascadeRemove_RsPoint(dataset, childRsPointId);
                    }
                }
                DataRow[] rowsFK_RS_SHIFT_RS_WAYOUT = row.GetChildRows("FK_RS_SHIFT_RS_WAYOUT");
                if( rowsFK_RS_SHIFT_RS_WAYOUT != null && rowsFK_RS_SHIFT_RS_WAYOUT.Length > 0 ){
                    foreach (DataRow rowFK_RS_SHIFT_RS_WAYOUT in rowsFK_RS_SHIFT_RS_WAYOUT)
                    {
                        System.Int32 childRsShiftId = (System.Int32)rowFK_RS_SHIFT_RS_WAYOUT["RS_SHIFT_ID"];
                        CascadeRemove_RsShift(dataset, childRsShiftId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_WAYOUT]
        ///</summary>
        public static void Compact_RsWayout(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_WAYOUT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_WAYOUT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RS_WAYOUT]
        ///</summary>
        public static void CompactFreshRecords_RsWayout(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RS_WAYOUT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RS_WAYOUT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RS_WAYOUT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [RULE]
        ///</summary>
        public static void CascadeRemove_Rule(DataSet dataset, System.Int32 RuleId)
        {
            Debug.Assert(dataset.Tables.Contains("RULE"));
            DataRow row = dataset.Tables["RULE"].Rows.Find(RuleId);
            if( row != null){
                DataRow[] rowsVehiclegroupRule_Rule = row.GetChildRows("VehiclegroupRule_Rule");
                if( rowsVehiclegroupRule_Rule != null && rowsVehiclegroupRule_Rule.Length > 0 ){
                    foreach (DataRow rowVehiclegroupRule_Rule in rowsVehiclegroupRule_Rule)
                    {
                        System.Int32 childVehiclegroupRuleId = (System.Int32)rowVehiclegroupRule_Rule["VEHICLEGROUP_RULE_ID"];
                        CascadeRemove_VehiclegroupRule(dataset, childVehiclegroupRuleId);
                    }
                }
                DataRow[] rowsVehicleRule_Rule = row.GetChildRows("VehicleRule_Rule");
                if( rowsVehicleRule_Rule != null && rowsVehicleRule_Rule.Length > 0 ){
                    foreach (DataRow rowVehicleRule_Rule in rowsVehicleRule_Rule)
                    {
                        System.Int32 childVehicleRuleId = (System.Int32)rowVehicleRule_Rule["VEHICLE_RULE_ID"];
                        CascadeRemove_VehicleRule(dataset, childVehicleRuleId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [RULE]
        ///</summary>
        public static void Compact_Rule(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RULE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RULE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [RULE]
        ///</summary>
        public static void CompactFreshRecords_Rule(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("RULE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["RULE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["RULE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [SCHEDULE]
        ///</summary>
        public static void CascadeRemove_Schedule(DataSet dataset, System.Int32 ScheduleId)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULE"));
            DataRow row = dataset.Tables["SCHEDULE"].Rows.Find(ScheduleId);
            if( row != null){
                DataRow[] rowsLogWaybillHeaderStatus_Schedule = row.GetChildRows("LogWaybillHeaderStatus_Schedule");
                if( rowsLogWaybillHeaderStatus_Schedule != null && rowsLogWaybillHeaderStatus_Schedule.Length > 0 ){
                    foreach (DataRow rowLogWaybillHeaderStatus_Schedule in rowsLogWaybillHeaderStatus_Schedule)
                    {
                        System.Int32 childLogWaybillHeaderStatusId = (System.Int32)rowLogWaybillHeaderStatus_Schedule["LOG_WAYBILL_HEADER_STATUS_ID"];
                        CascadeRemove_LogWaybillHeaderStatus(dataset, childLogWaybillHeaderStatusId);
                    }
                }
                DataRow[] rowsScheduleDetail_Schedule = row.GetChildRows("ScheduleDetail_Schedule");
                if( rowsScheduleDetail_Schedule != null && rowsScheduleDetail_Schedule.Length > 0 ){
                    foreach (DataRow rowScheduleDetail_Schedule in rowsScheduleDetail_Schedule)
                    {
                        System.Int32 childScheduleDetailId = (System.Int32)rowScheduleDetail_Schedule["SCHEDULE_DETAIL_ID"];
                        CascadeRemove_ScheduleDetail(dataset, childScheduleDetailId);
                    }
                }
                DataRow[] rowsWaybillHeader_Schedule = row.GetChildRows("WaybillHeader_Schedule");
                if( rowsWaybillHeader_Schedule != null && rowsWaybillHeader_Schedule.Length > 0 ){
                    foreach (DataRow rowWaybillHeader_Schedule in rowsWaybillHeader_Schedule)
                    {
                        System.Int32 childWaybillHeaderId = (System.Int32)rowWaybillHeader_Schedule["WAYBILL_HEADER_ID"];
                        CascadeRemove_WaybillHeader(dataset, childWaybillHeaderId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [SCHEDULE]
        ///</summary>
        public static void Compact_Schedule(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SCHEDULE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [SCHEDULE]
        ///</summary>
        public static void CompactFreshRecords_Schedule(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SCHEDULE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["SCHEDULE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [SCHEDULE_DETAIL]
        ///</summary>
        public static void CascadeRemove_ScheduleDetail(DataSet dataset, System.Int32 ScheduleDetailId)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULE_DETAIL"));
            DataRow row = dataset.Tables["SCHEDULE_DETAIL"].Rows.Find(ScheduleDetailId);
            if( row != null){
                DataRow[] rowsFK_SCHEDULE_DETAIL_INFO_SCHEDULE_DETAIL = row.GetChildRows("FK_SCHEDULE_DETAIL_INFO_SCHEDULE_DETAIL");
                if( rowsFK_SCHEDULE_DETAIL_INFO_SCHEDULE_DETAIL != null && rowsFK_SCHEDULE_DETAIL_INFO_SCHEDULE_DETAIL.Length > 0 ){
                    foreach (DataRow rowFK_SCHEDULE_DETAIL_INFO_SCHEDULE_DETAIL in rowsFK_SCHEDULE_DETAIL_INFO_SCHEDULE_DETAIL)
                    {
                        System.Int32 childScheduleDetailId = (System.Int32)rowFK_SCHEDULE_DETAIL_INFO_SCHEDULE_DETAIL["SCHEDULE_DETAIL_ID"];
                        CascadeRemove_ScheduleDetailInfo(dataset, childScheduleDetailId);
                    }
                }
                DataRow[] rowsTrip_ScheduleDetail = row.GetChildRows("Trip_ScheduleDetail");
                if( rowsTrip_ScheduleDetail != null && rowsTrip_ScheduleDetail.Length > 0 ){
                    foreach (DataRow rowTrip_ScheduleDetail in rowsTrip_ScheduleDetail)
                    {
                        System.Int32 childTripId = (System.Int32)rowTrip_ScheduleDetail["TRIP_ID"];
                        CascadeRemove_Trip(dataset, childTripId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [SCHEDULE_DETAIL]
        ///</summary>
        public static void Compact_ScheduleDetail(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULE_DETAIL"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SCHEDULE_DETAIL"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [SCHEDULE_DETAIL]
        ///</summary>
        public static void CompactFreshRecords_ScheduleDetail(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULE_DETAIL"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SCHEDULE_DETAIL"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["SCHEDULE_DETAIL"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [SCHEDULE_DETAIL_INFO]
        ///</summary>
        public static void CascadeRemove_ScheduleDetailInfo(DataSet dataset, System.Int32 ScheduleDetailId)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULE_DETAIL_INFO"));
            DataRow row = dataset.Tables["SCHEDULE_DETAIL_INFO"].Rows.Find(ScheduleDetailId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [SCHEDULE_DETAIL_INFO]
        ///</summary>
        public static void Compact_ScheduleDetailInfo(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULE_DETAIL_INFO"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SCHEDULE_DETAIL_INFO"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [SCHEDULE_DETAIL_INFO]
        ///</summary>
        public static void CompactFreshRecords_ScheduleDetailInfo(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULE_DETAIL_INFO"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SCHEDULE_DETAIL_INFO"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["SCHEDULE_DETAIL_INFO"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [SCHEDULE_GEOZONE]
        ///</summary>
        public static void CascadeRemove_ScheduleGeozone(DataSet dataset, System.Int32 ScheduleGeozoneId)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULE_GEOZONE"));
            DataRow row = dataset.Tables["SCHEDULE_GEOZONE"].Rows.Find(ScheduleGeozoneId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [SCHEDULE_GEOZONE]
        ///</summary>
        public static void Compact_ScheduleGeozone(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULE_GEOZONE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SCHEDULE_GEOZONE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [SCHEDULE_GEOZONE]
        ///</summary>
        public static void CompactFreshRecords_ScheduleGeozone(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULE_GEOZONE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SCHEDULE_GEOZONE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["SCHEDULE_GEOZONE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [SCHEDULE_PASSAGE]
        ///</summary>
        public static void CascadeRemove_SchedulePassage(DataSet dataset, System.Int32 WhId, System.Int32 SpId)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULE_PASSAGE"));
            DataRow row = dataset.Tables["SCHEDULE_PASSAGE"].Rows.Find(new object[]{WhId, SpId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [SCHEDULE_PASSAGE]
        ///</summary>
        public static void Compact_SchedulePassage(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULE_PASSAGE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SCHEDULE_PASSAGE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [SCHEDULE_PASSAGE]
        ///</summary>
        public static void CompactFreshRecords_SchedulePassage(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULE_PASSAGE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SCHEDULE_PASSAGE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["SCHEDULE_PASSAGE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [SCHEDULE_POINT]
        ///</summary>
        public static void CascadeRemove_SchedulePoint(DataSet dataset, System.Int32 SchedulePointId)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULE_POINT"));
            DataRow row = dataset.Tables["SCHEDULE_POINT"].Rows.Find(SchedulePointId);
            if( row != null){
                DataRow[] rowsSchedulePassage_SchedulePoint = row.GetChildRows("SchedulePassage_SchedulePoint");
                if( rowsSchedulePassage_SchedulePoint != null && rowsSchedulePassage_SchedulePoint.Length > 0 ){
                    foreach (DataRow rowSchedulePassage_SchedulePoint in rowsSchedulePassage_SchedulePoint)
                    {
                        System.Int32 childWhId = (System.Int32)rowSchedulePassage_SchedulePoint["WH_ID"];
                        System.Int32 childSpId = (System.Int32)rowSchedulePassage_SchedulePoint["SP_ID"];
                        CascadeRemove_SchedulePassage(dataset, childWhId, childSpId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [SCHEDULE_POINT]
        ///</summary>
        public static void Compact_SchedulePoint(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULE_POINT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SCHEDULE_POINT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [SCHEDULE_POINT]
        ///</summary>
        public static void CompactFreshRecords_SchedulePoint(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULE_POINT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SCHEDULE_POINT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["SCHEDULE_POINT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [SCHEDULEREVENT]
        ///</summary>
        public static void CascadeRemove_Schedulerevent(DataSet dataset, System.Int32 SchedulereventId)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULEREVENT"));
            DataRow row = dataset.Tables["SCHEDULEREVENT"].Rows.Find(SchedulereventId);
            if( row != null){
                DataRow[] rowsFK_EMAIL_SCHEDULEREVENT_SCHEDULEREVENT = row.GetChildRows("FK_EMAIL_SCHEDULEREVENT_SCHEDULEREVENT");
                if( rowsFK_EMAIL_SCHEDULEREVENT_SCHEDULEREVENT != null && rowsFK_EMAIL_SCHEDULEREVENT_SCHEDULEREVENT.Length > 0 ){
                    foreach (DataRow rowFK_EMAIL_SCHEDULEREVENT_SCHEDULEREVENT in rowsFK_EMAIL_SCHEDULEREVENT_SCHEDULEREVENT)
                    {
                        System.Int32 childSchedulereventId = (System.Int32)rowFK_EMAIL_SCHEDULEREVENT_SCHEDULEREVENT["SCHEDULEREVENT_ID"];
                        System.Int32 childEmailId = (System.Int32)rowFK_EMAIL_SCHEDULEREVENT_SCHEDULEREVENT["EMAIL_ID"];
                        CascadeRemove_EmailSchedulerevent(dataset, childSchedulereventId, childEmailId);
                    }
                }
                DataRow[] rowsSchedulerQueue_SchedulerEvent = row.GetChildRows("SchedulerQueue_SchedulerEvent");
                if( rowsSchedulerQueue_SchedulerEvent != null && rowsSchedulerQueue_SchedulerEvent.Length > 0 ){
                    foreach (DataRow rowSchedulerQueue_SchedulerEvent in rowsSchedulerQueue_SchedulerEvent)
                    {
                        System.Int32 childSchedulerqueueId = (System.Int32)rowSchedulerQueue_SchedulerEvent["SCHEDULERQUEUE_ID"];
                        CascadeRemove_Schedulerqueue(dataset, childSchedulerqueueId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [SCHEDULEREVENT]
        ///</summary>
        public static void Compact_Schedulerevent(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULEREVENT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SCHEDULEREVENT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [SCHEDULEREVENT]
        ///</summary>
        public static void CompactFreshRecords_Schedulerevent(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULEREVENT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SCHEDULEREVENT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["SCHEDULEREVENT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [SCHEDULERQUEUE]
        ///</summary>
        public static void CascadeRemove_Schedulerqueue(DataSet dataset, System.Int32 SchedulerqueueId)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULERQUEUE"));
            DataRow row = dataset.Tables["SCHEDULERQUEUE"].Rows.Find(SchedulerqueueId);
            if( row != null){
                DataRow[] rowsFK_EMAIL_SCHEDULERQUEUE_SCHEDULERQUEUE = row.GetChildRows("FK_EMAIL_SCHEDULERQUEUE_SCHEDULERQUEUE");
                if( rowsFK_EMAIL_SCHEDULERQUEUE_SCHEDULERQUEUE != null && rowsFK_EMAIL_SCHEDULERQUEUE_SCHEDULERQUEUE.Length > 0 ){
                    foreach (DataRow rowFK_EMAIL_SCHEDULERQUEUE_SCHEDULERQUEUE in rowsFK_EMAIL_SCHEDULERQUEUE_SCHEDULERQUEUE)
                    {
                        System.Int32 childSchedulerqueueId = (System.Int32)rowFK_EMAIL_SCHEDULERQUEUE_SCHEDULERQUEUE["SCHEDULERQUEUE_ID"];
                        System.Int32 childEmailId = (System.Int32)rowFK_EMAIL_SCHEDULERQUEUE_SCHEDULERQUEUE["EMAIL_ID"];
                        CascadeRemove_EmailSchedulerqueue(dataset, childSchedulerqueueId, childEmailId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [SCHEDULERQUEUE]
        ///</summary>
        public static void Compact_Schedulerqueue(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULERQUEUE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SCHEDULERQUEUE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [SCHEDULERQUEUE]
        ///</summary>
        public static void CompactFreshRecords_Schedulerqueue(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SCHEDULERQUEUE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SCHEDULERQUEUE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["SCHEDULERQUEUE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [SEASON]
        ///</summary>
        public static void CascadeRemove_Season(DataSet dataset, System.Int32 SeasonId)
        {
            Debug.Assert(dataset.Tables.Contains("SEASON"));
            DataRow row = dataset.Tables["SEASON"].Rows.Find(SeasonId);
            if( row != null){
                DataRow[] rowsFactorValues_Season = row.GetChildRows("FactorValues_Season");
                if( rowsFactorValues_Season != null && rowsFactorValues_Season.Length > 0 ){
                    foreach (DataRow rowFactorValues_Season in rowsFactorValues_Season)
                    {
                        System.Int32 childFactorValuesId = (System.Int32)rowFactorValues_Season["FACTOR_VALUES_ID"];
                        CascadeRemove_FactorValues(dataset, childFactorValuesId);
                    }
                }
                DataRow[] rowsPeriod_Season = row.GetChildRows("Period_Season");
                if( rowsPeriod_Season != null && rowsPeriod_Season.Length > 0 ){
                    foreach (DataRow rowPeriod_Season in rowsPeriod_Season)
                    {
                        System.Int32 childPeriodId = (System.Int32)rowPeriod_Season["PERIOD_ID"];
                        CascadeRemove_Period(dataset, childPeriodId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [SEASON]
        ///</summary>
        public static void Compact_Season(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SEASON"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SEASON"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [SEASON]
        ///</summary>
        public static void CompactFreshRecords_Season(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SEASON"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SEASON"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["SEASON"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [SEATTYPE]
        ///</summary>
        public static void CascadeRemove_Seattype(DataSet dataset, System.Int32 SeattypeId)
        {
            Debug.Assert(dataset.Tables.Contains("SEATTYPE"));
            DataRow row = dataset.Tables["SEATTYPE"].Rows.Find(SeattypeId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [SEATTYPE]
        ///</summary>
        public static void Compact_Seattype(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SEATTYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SEATTYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [SEATTYPE]
        ///</summary>
        public static void CompactFreshRecords_Seattype(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SEATTYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SEATTYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["SEATTYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [SESSION]
        ///</summary>
        public static void CascadeRemove_Session(DataSet dataset, System.Int32 SessionId)
        {
            Debug.Assert(dataset.Tables.Contains("SESSION"));
            DataRow row = dataset.Tables["SESSION"].Rows.Find(SessionId);
            if( row != null){
                DataRow[] rowsTrail_Session = row.GetChildRows("Trail_Session");
                if( rowsTrail_Session != null && rowsTrail_Session.Length > 0 ){
                    foreach (DataRow rowTrail_Session in rowsTrail_Session)
                    {
                        System.Int32 childTrailId = (System.Int32)rowTrail_Session["TRAIL_ID"];
                        CascadeRemove_Trail(dataset, childTrailId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [SESSION]
        ///</summary>
        public static void Compact_Session(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SESSION"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SESSION"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [SESSION]
        ///</summary>
        public static void CompactFreshRecords_Session(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SESSION"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SESSION"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["SESSION"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [SMS_TYPE]
        ///</summary>
        public static void CascadeRemove_SmsType(DataSet dataset, System.Int32 SmsTypeId)
        {
            Debug.Assert(dataset.Tables.Contains("SMS_TYPE"));
            DataRow row = dataset.Tables["SMS_TYPE"].Rows.Find(SmsTypeId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [SMS_TYPE]
        ///</summary>
        public static void Compact_SmsType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SMS_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SMS_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [SMS_TYPE]
        ///</summary>
        public static void CompactFreshRecords_SmsType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("SMS_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["SMS_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["SMS_TYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [sysdiagrams]
        ///</summary>
        public static void CascadeRemove_Sysdiagrams(DataSet dataset, System.Int32 DiagramId)
        {
            Debug.Assert(dataset.Tables.Contains("sysdiagrams"));
            DataRow row = dataset.Tables["sysdiagrams"].Rows.Find(DiagramId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [sysdiagrams]
        ///</summary>
        public static void Compact_Sysdiagrams(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("sysdiagrams"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["sysdiagrams"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [sysdiagrams]
        ///</summary>
        public static void CompactFreshRecords_Sysdiagrams(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("sysdiagrams"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["sysdiagrams"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["sysdiagrams"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [TASK_PROCESSOR_LOG]
        ///</summary>
        public static void CascadeRemove_TaskProcessorLog(DataSet dataset, System.Int32 TaskProcessorLogId)
        {
            Debug.Assert(dataset.Tables.Contains("TASK_PROCESSOR_LOG"));
            DataRow row = dataset.Tables["TASK_PROCESSOR_LOG"].Rows.Find(TaskProcessorLogId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [TASK_PROCESSOR_LOG]
        ///</summary>
        public static void Compact_TaskProcessorLog(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("TASK_PROCESSOR_LOG"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["TASK_PROCESSOR_LOG"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [TASK_PROCESSOR_LOG]
        ///</summary>
        public static void CompactFreshRecords_TaskProcessorLog(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("TASK_PROCESSOR_LOG"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["TASK_PROCESSOR_LOG"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["TASK_PROCESSOR_LOG"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [TRAIL]
        ///</summary>
        public static void CascadeRemove_Trail(DataSet dataset, System.Int32 TrailId)
        {
            Debug.Assert(dataset.Tables.Contains("TRAIL"));
            DataRow row = dataset.Tables["TRAIL"].Rows.Find(TrailId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [TRAIL]
        ///</summary>
        public static void Compact_Trail(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("TRAIL"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["TRAIL"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [TRAIL]
        ///</summary>
        public static void CompactFreshRecords_Trail(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("TRAIL"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["TRAIL"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["TRAIL"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [TRIP]
        ///</summary>
        public static void CascadeRemove_Trip(DataSet dataset, System.Int32 TripId)
        {
            Debug.Assert(dataset.Tables.Contains("TRIP"));
            DataRow row = dataset.Tables["TRIP"].Rows.Find(TripId);
            if( row != null){
                DataRow[] rowsFK_SCHEDULE_GEOZONE_TRIP = row.GetChildRows("FK_SCHEDULE_GEOZONE_TRIP");
                if( rowsFK_SCHEDULE_GEOZONE_TRIP != null && rowsFK_SCHEDULE_GEOZONE_TRIP.Length > 0 ){
                    foreach (DataRow rowFK_SCHEDULE_GEOZONE_TRIP in rowsFK_SCHEDULE_GEOZONE_TRIP)
                    {
                        System.Int32 childScheduleGeozoneId = (System.Int32)rowFK_SCHEDULE_GEOZONE_TRIP["SCHEDULE_GEOZONE_ID"];
                        CascadeRemove_ScheduleGeozone(dataset, childScheduleGeozoneId);
                    }
                }
                DataRow[] rowsSchedulePoint_Trip = row.GetChildRows("SchedulePoint_Trip");
                if( rowsSchedulePoint_Trip != null && rowsSchedulePoint_Trip.Length > 0 ){
                    foreach (DataRow rowSchedulePoint_Trip in rowsSchedulePoint_Trip)
                    {
                        System.Int32 childSchedulePointId = (System.Int32)rowSchedulePoint_Trip["SCHEDULE_POINT_ID"];
                        CascadeRemove_SchedulePoint(dataset, childSchedulePointId);
                    }
                }
                DataRow[] rowsWbTrip_Trip = row.GetChildRows("WbTrip_Trip");
                if( rowsWbTrip_Trip != null && rowsWbTrip_Trip.Length > 0 ){
                    foreach (DataRow rowWbTrip_Trip in rowsWbTrip_Trip)
                    {
                        System.Int32 childWbTripId = (System.Int32)rowWbTrip_Trip["WB_TRIP_ID"];
                        CascadeRemove_WbTrip(dataset, childWbTripId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [TRIP]
        ///</summary>
        public static void Compact_Trip(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("TRIP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["TRIP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [TRIP]
        ///</summary>
        public static void CompactFreshRecords_Trip(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("TRIP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["TRIP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["TRIP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [TRIP_KIND]
        ///</summary>
        public static void CascadeRemove_TripKind(DataSet dataset, System.Int32 TripKindId)
        {
            Debug.Assert(dataset.Tables.Contains("TRIP_KIND"));
            DataRow row = dataset.Tables["TRIP_KIND"].Rows.Find(TripKindId);
            if( row != null){
                DataRow[] rowsTrip_TripKind = row.GetChildRows("Trip_TripKind");
                if( rowsTrip_TripKind != null && rowsTrip_TripKind.Length > 0 ){
                    foreach (DataRow rowTrip_TripKind in rowsTrip_TripKind)
                    {
                        System.Int32 childTripId = (System.Int32)rowTrip_TripKind["TRIP_ID"];
                        CascadeRemove_Trip(dataset, childTripId);
                    }
                }
                DataRow[] rowsWaybillHeader_TripKind = row.GetChildRows("WaybillHeader_TripKind");
                if( rowsWaybillHeader_TripKind != null && rowsWaybillHeader_TripKind.Length > 0 ){
                    foreach (DataRow rowWaybillHeader_TripKind in rowsWaybillHeader_TripKind)
                    {
                        System.Int32 childWaybillHeaderId = (System.Int32)rowWaybillHeader_TripKind["WAYBILL_HEADER_ID"];
                        CascadeRemove_WaybillHeader(dataset, childWaybillHeaderId);
                    }
                }
                DataRow[] rowsWbTrip_TripKind = row.GetChildRows("WbTrip_TripKind");
                if( rowsWbTrip_TripKind != null && rowsWbTrip_TripKind.Length > 0 ){
                    foreach (DataRow rowWbTrip_TripKind in rowsWbTrip_TripKind)
                    {
                        System.Int32 childWbTripId = (System.Int32)rowWbTrip_TripKind["WB_TRIP_ID"];
                        CascadeRemove_WbTrip(dataset, childWbTripId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [TRIP_KIND]
        ///</summary>
        public static void Compact_TripKind(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("TRIP_KIND"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["TRIP_KIND"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [TRIP_KIND]
        ///</summary>
        public static void CompactFreshRecords_TripKind(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("TRIP_KIND"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["TRIP_KIND"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["TRIP_KIND"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [VEHICLE]
        ///</summary>
        public static void CascadeRemove_Vehicle(DataSet dataset, System.Int32 VehicleId)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLE"));
            DataRow row = dataset.Tables["VEHICLE"].Rows.Find(VehicleId);
            if( row != null){
                DataRow[] rowsController_Vehicle = row.GetChildRows("Controller_Vehicle");
                if( rowsController_Vehicle != null && rowsController_Vehicle.Length > 0 ){
                    foreach (DataRow rowController_Vehicle in rowsController_Vehicle)
                    {
                        System.Int32 childControllerId = (System.Int32)rowController_Vehicle["CONTROLLER_ID"];
                        CascadeRemove_Controller(dataset, childControllerId);
                    }
                }
                DataRow[] rowsDriverVehicle_Vehicle = row.GetChildRows("DriverVehicle_Vehicle");
                if( rowsDriverVehicle_Vehicle != null && rowsDriverVehicle_Vehicle.Length > 0 ){
                    foreach (DataRow rowDriverVehicle_Vehicle in rowsDriverVehicle_Vehicle)
                    {
                        System.Int32 childDriverId = (System.Int32)rowDriverVehicle_Vehicle["DRIVER_ID"];
                        CascadeRemove_DriverVehicle(dataset, childDriverId);
                    }
                }
                DataRow[] rowsFK_BRIGADE_VEHICLE = row.GetChildRows("FK_BRIGADE_VEHICLE");
                if( rowsFK_BRIGADE_VEHICLE != null && rowsFK_BRIGADE_VEHICLE.Length > 0 ){
                    foreach (DataRow rowFK_BRIGADE_VEHICLE in rowsFK_BRIGADE_VEHICLE)
                    {
                        System.Int32 childBrigadeId = (System.Int32)rowFK_BRIGADE_VEHICLE["BRIGADE_ID"];
                        CascadeRemove_Brigade(dataset, childBrigadeId);
                    }
                }
                DataRow[] rowsFK_GOODS_TYPE_VEHICLE_VEHICLE = row.GetChildRows("FK_GOODS_TYPE_VEHICLE_VEHICLE");
                if( rowsFK_GOODS_TYPE_VEHICLE_VEHICLE != null && rowsFK_GOODS_TYPE_VEHICLE_VEHICLE.Length > 0 ){
                    foreach (DataRow rowFK_GOODS_TYPE_VEHICLE_VEHICLE in rowsFK_GOODS_TYPE_VEHICLE_VEHICLE)
                    {
                        System.Int32 childGoodsTypeVehicleId = (System.Int32)rowFK_GOODS_TYPE_VEHICLE_VEHICLE["GOODS_TYPE_VEHICLE_id"];
                        CascadeRemove_GoodsTypeVehicle(dataset, childGoodsTypeVehicleId);
                    }
                }
                DataRow[] rowsFK_JOURNAL_DAY_VEHICLE = row.GetChildRows("FK_JOURNAL_DAY_VEHICLE");
                if( rowsFK_JOURNAL_DAY_VEHICLE != null && rowsFK_JOURNAL_DAY_VEHICLE.Length > 0 ){
                    foreach (DataRow rowFK_JOURNAL_DAY_VEHICLE in rowsFK_JOURNAL_DAY_VEHICLE)
                    {
                        System.Int32 childJournalDayId = (System.Int32)rowFK_JOURNAL_DAY_VEHICLE["JOURNAL_DAY_ID"];
                        CascadeRemove_JournalDay(dataset, childJournalDayId);
                    }
                }
                DataRow[] rowsFK_JOURNAL_VEHICLE = row.GetChildRows("FK_JOURNAL_VEHICLE");
                if( rowsFK_JOURNAL_VEHICLE != null && rowsFK_JOURNAL_VEHICLE.Length > 0 ){
                    foreach (DataRow rowFK_JOURNAL_VEHICLE in rowsFK_JOURNAL_VEHICLE)
                    {
                        System.Int32 childJournalId = (System.Int32)rowFK_JOURNAL_VEHICLE["JOURNAL_ID"];
                        CascadeRemove_Journal(dataset, childJournalId);
                    }
                }
                DataRow[] rowsFK_JOURNAL_VEHICLE_VEHICLE = row.GetChildRows("FK_JOURNAL_VEHICLE_VEHICLE");
                if( rowsFK_JOURNAL_VEHICLE_VEHICLE != null && rowsFK_JOURNAL_VEHICLE_VEHICLE.Length > 0 ){
                    foreach (DataRow rowFK_JOURNAL_VEHICLE_VEHICLE in rowsFK_JOURNAL_VEHICLE_VEHICLE)
                    {
                        System.Int32 childJournalVehicleId = (System.Int32)rowFK_JOURNAL_VEHICLE_VEHICLE["JOURNAL_VEHICLE_ID"];
                        CascadeRemove_JournalVehicle(dataset, childJournalVehicleId);
                    }
                }
                DataRow[] rowsFK_VEHICLE_OWNER_VEHICLE = row.GetChildRows("FK_VEHICLE_OWNER_VEHICLE");
                if( rowsFK_VEHICLE_OWNER_VEHICLE != null && rowsFK_VEHICLE_OWNER_VEHICLE.Length > 0 ){
                    foreach (DataRow rowFK_VEHICLE_OWNER_VEHICLE in rowsFK_VEHICLE_OWNER_VEHICLE)
                    {
                        System.Int32 childVehicleOwnerId = (System.Int32)rowFK_VEHICLE_OWNER_VEHICLE["VEHICLE_OWNER_ID"];
                        CascadeRemove_VehicleOwner(dataset, childVehicleOwnerId);
                    }
                }
                DataRow[] rowsFK_VEHICLE_PICTURE_VEHICLE = row.GetChildRows("FK_VEHICLE_PICTURE_VEHICLE");
                if( rowsFK_VEHICLE_PICTURE_VEHICLE != null && rowsFK_VEHICLE_PICTURE_VEHICLE.Length > 0 ){
                    foreach (DataRow rowFK_VEHICLE_PICTURE_VEHICLE in rowsFK_VEHICLE_PICTURE_VEHICLE)
                    {
                        System.Int32 childVehiclePictureId = (System.Int32)rowFK_VEHICLE_PICTURE_VEHICLE["VEHICLE_PICTURE_ID"];
                        CascadeRemove_VehiclePicture(dataset, childVehiclePictureId);
                    }
                }
                DataRow[] rowsLogVehicleStatus_Vehicle = row.GetChildRows("LogVehicleStatus_Vehicle");
                if( rowsLogVehicleStatus_Vehicle != null && rowsLogVehicleStatus_Vehicle.Length > 0 ){
                    foreach (DataRow rowLogVehicleStatus_Vehicle in rowsLogVehicleStatus_Vehicle)
                    {
                        System.Int32 childLogVehicleStatusId = (System.Int32)rowLogVehicleStatus_Vehicle["LOG_VEHICLE_STATUS_ID"];
                        CascadeRemove_LogVehicleStatus(dataset, childLogVehicleStatusId);
                    }
                }
                DataRow[] rowsOperatorGroupVehicle_Vehicle = row.GetChildRows("OperatorGroupVehicle_Vehicle");
                if( rowsOperatorGroupVehicle_Vehicle != null && rowsOperatorGroupVehicle_Vehicle.Length > 0 ){
                    foreach (DataRow rowOperatorGroupVehicle_Vehicle in rowsOperatorGroupVehicle_Vehicle)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupVehicle_Vehicle["OPERATORGROUP_ID"];
                        System.Int32 childVehicleId = (System.Int32)rowOperatorGroupVehicle_Vehicle["VEHICLE_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupVehicle_Vehicle["RIGHT_ID"];
                        CascadeRemove_OperatorgroupVehicle(dataset, childOperatorgroupId, childVehicleId, childRightId);
                    }
                }
                DataRow[] rowsOperatorVehicle_Vehicle = row.GetChildRows("OperatorVehicle_Vehicle");
                if( rowsOperatorVehicle_Vehicle != null && rowsOperatorVehicle_Vehicle.Length > 0 ){
                    foreach (DataRow rowOperatorVehicle_Vehicle in rowsOperatorVehicle_Vehicle)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorVehicle_Vehicle["OPERATOR_ID"];
                        System.Int32 childVehicleId = (System.Int32)rowOperatorVehicle_Vehicle["VEHICLE_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorVehicle_Vehicle["RIGHT_ID"];
                        CascadeRemove_OperatorVehicle(dataset, childOperatorId, childVehicleId, childRightId);
                    }
                }
                DataRow[] rowsVehicleGroupVehicle_Vehicle = row.GetChildRows("VehicleGroupVehicle_Vehicle");
                if( rowsVehicleGroupVehicle_Vehicle != null && rowsVehicleGroupVehicle_Vehicle.Length > 0 ){
                    foreach (DataRow rowVehicleGroupVehicle_Vehicle in rowsVehicleGroupVehicle_Vehicle)
                    {
                        System.Int32 childVehiclegroupId = (System.Int32)rowVehicleGroupVehicle_Vehicle["VEHICLEGROUP_ID"];
                        System.Int32 childVehicleId = (System.Int32)rowVehicleGroupVehicle_Vehicle["VEHICLE_ID"];
                        CascadeRemove_VehiclegroupVehicle(dataset, childVehiclegroupId, childVehicleId);
                    }
                }
                DataRow[] rowsVehicleRule_Vehicle = row.GetChildRows("VehicleRule_Vehicle");
                if( rowsVehicleRule_Vehicle != null && rowsVehicleRule_Vehicle.Length > 0 ){
                    foreach (DataRow rowVehicleRule_Vehicle in rowsVehicleRule_Vehicle)
                    {
                        System.Int32 childVehicleRuleId = (System.Int32)rowVehicleRule_Vehicle["VEHICLE_RULE_ID"];
                        CascadeRemove_VehicleRule(dataset, childVehicleRuleId);
                    }
                }
                DataRow[] rowsWaybillHeader_Trailor1 = row.GetChildRows("WaybillHeader_Trailor1");
                if( rowsWaybillHeader_Trailor1 != null && rowsWaybillHeader_Trailor1.Length > 0 ){
                    foreach (DataRow rowWaybillHeader_Trailor1 in rowsWaybillHeader_Trailor1)
                    {
                        System.Int32 childWaybillHeaderId = (System.Int32)rowWaybillHeader_Trailor1["WAYBILL_HEADER_ID"];
                        CascadeRemove_WaybillHeader(dataset, childWaybillHeaderId);
                    }
                }
                DataRow[] rowsWaybillHeader_Trailor2 = row.GetChildRows("WaybillHeader_Trailor2");
                if( rowsWaybillHeader_Trailor2 != null && rowsWaybillHeader_Trailor2.Length > 0 ){
                    foreach (DataRow rowWaybillHeader_Trailor2 in rowsWaybillHeader_Trailor2)
                    {
                        System.Int32 childWaybillHeaderId = (System.Int32)rowWaybillHeader_Trailor2["WAYBILL_HEADER_ID"];
                        CascadeRemove_WaybillHeader(dataset, childWaybillHeaderId);
                    }
                }
                DataRow[] rowsWaybillHeader_Vehicle = row.GetChildRows("WaybillHeader_Vehicle");
                if( rowsWaybillHeader_Vehicle != null && rowsWaybillHeader_Vehicle.Length > 0 ){
                    foreach (DataRow rowWaybillHeader_Vehicle in rowsWaybillHeader_Vehicle)
                    {
                        System.Int32 childWaybillHeaderId = (System.Int32)rowWaybillHeader_Vehicle["WAYBILL_HEADER_ID"];
                        CascadeRemove_WaybillHeader(dataset, childWaybillHeaderId);
                    }
                }
                DataRow[] rowsZoneVehicle_Vehicle = row.GetChildRows("ZoneVehicle_Vehicle");
                if( rowsZoneVehicle_Vehicle != null && rowsZoneVehicle_Vehicle.Length > 0 ){
                    foreach (DataRow rowZoneVehicle_Vehicle in rowsZoneVehicle_Vehicle)
                    {
                        System.Int32 childZoneVehicleId = (System.Int32)rowZoneVehicle_Vehicle["ZONE_VEHICLE_ID"];
                        CascadeRemove_ZoneVehicle(dataset, childZoneVehicleId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [VEHICLE]
        ///</summary>
        public static void Compact_Vehicle(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["VEHICLE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [VEHICLE]
        ///</summary>
        public static void CompactFreshRecords_Vehicle(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["VEHICLE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["VEHICLE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [VEHICLE_KIND]
        ///</summary>
        public static void CascadeRemove_VehicleKind(DataSet dataset, System.Int32 VehicleKindId)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLE_KIND"));
            DataRow row = dataset.Tables["VEHICLE_KIND"].Rows.Find(VehicleKindId);
            if( row != null){
                DataRow[] rowsVehicle_VehicleKind = row.GetChildRows("Vehicle_VehicleKind");
                if( rowsVehicle_VehicleKind != null && rowsVehicle_VehicleKind.Length > 0 ){
                    foreach (DataRow rowVehicle_VehicleKind in rowsVehicle_VehicleKind)
                    {
                        System.Int32 childVehicleId = (System.Int32)rowVehicle_VehicleKind["VEHICLE_ID"];
                        CascadeRemove_Vehicle(dataset, childVehicleId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [VEHICLE_KIND]
        ///</summary>
        public static void Compact_VehicleKind(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLE_KIND"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["VEHICLE_KIND"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [VEHICLE_KIND]
        ///</summary>
        public static void CompactFreshRecords_VehicleKind(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLE_KIND"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["VEHICLE_KIND"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["VEHICLE_KIND"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [VEHICLE_OWNER]
        ///</summary>
        public static void CascadeRemove_VehicleOwner(DataSet dataset, System.Int32 VehicleOwnerId)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLE_OWNER"));
            DataRow row = dataset.Tables["VEHICLE_OWNER"].Rows.Find(VehicleOwnerId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [VEHICLE_OWNER]
        ///</summary>
        public static void Compact_VehicleOwner(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLE_OWNER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["VEHICLE_OWNER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [VEHICLE_OWNER]
        ///</summary>
        public static void CompactFreshRecords_VehicleOwner(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLE_OWNER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["VEHICLE_OWNER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["VEHICLE_OWNER"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [VEHICLE_PICTURE]
        ///</summary>
        public static void CascadeRemove_VehiclePicture(DataSet dataset, System.Int32 VehiclePictureId)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLE_PICTURE"));
            DataRow row = dataset.Tables["VEHICLE_PICTURE"].Rows.Find(VehiclePictureId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [VEHICLE_PICTURE]
        ///</summary>
        public static void Compact_VehiclePicture(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLE_PICTURE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["VEHICLE_PICTURE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [VEHICLE_PICTURE]
        ///</summary>
        public static void CompactFreshRecords_VehiclePicture(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLE_PICTURE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["VEHICLE_PICTURE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["VEHICLE_PICTURE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [VEHICLE_RULE]
        ///</summary>
        public static void CascadeRemove_VehicleRule(DataSet dataset, System.Int32 VehicleRuleId)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLE_RULE"));
            DataRow row = dataset.Tables["VEHICLE_RULE"].Rows.Find(VehicleRuleId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [VEHICLE_RULE]
        ///</summary>
        public static void Compact_VehicleRule(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLE_RULE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["VEHICLE_RULE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [VEHICLE_RULE]
        ///</summary>
        public static void CompactFreshRecords_VehicleRule(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLE_RULE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["VEHICLE_RULE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["VEHICLE_RULE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [VEHICLE_STATUS]
        ///</summary>
        public static void CascadeRemove_VehicleStatus(DataSet dataset, System.Int32 VehicleStatusId)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLE_STATUS"));
            DataRow row = dataset.Tables["VEHICLE_STATUS"].Rows.Find(VehicleStatusId);
            if( row != null){
                DataRow[] rowsLogVehicleStatus_VehicleStatus = row.GetChildRows("LogVehicleStatus_VehicleStatus");
                if( rowsLogVehicleStatus_VehicleStatus != null && rowsLogVehicleStatus_VehicleStatus.Length > 0 ){
                    foreach (DataRow rowLogVehicleStatus_VehicleStatus in rowsLogVehicleStatus_VehicleStatus)
                    {
                        System.Int32 childLogVehicleStatusId = (System.Int32)rowLogVehicleStatus_VehicleStatus["LOG_VEHICLE_STATUS_ID"];
                        CascadeRemove_LogVehicleStatus(dataset, childLogVehicleStatusId);
                    }
                }
                DataRow[] rowsVehicle_VehicleStatus = row.GetChildRows("Vehicle_VehicleStatus");
                if( rowsVehicle_VehicleStatus != null && rowsVehicle_VehicleStatus.Length > 0 ){
                    foreach (DataRow rowVehicle_VehicleStatus in rowsVehicle_VehicleStatus)
                    {
                        System.Int32 childVehicleId = (System.Int32)rowVehicle_VehicleStatus["VEHICLE_ID"];
                        CascadeRemove_Vehicle(dataset, childVehicleId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [VEHICLE_STATUS]
        ///</summary>
        public static void Compact_VehicleStatus(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLE_STATUS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["VEHICLE_STATUS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [VEHICLE_STATUS]
        ///</summary>
        public static void CompactFreshRecords_VehicleStatus(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLE_STATUS"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["VEHICLE_STATUS"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["VEHICLE_STATUS"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [VEHICLEGROUP]
        ///</summary>
        public static void CascadeRemove_Vehiclegroup(DataSet dataset, System.Int32 VehiclegroupId)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLEGROUP"));
            DataRow row = dataset.Tables["VEHICLEGROUP"].Rows.Find(VehiclegroupId);
            if( row != null){
                DataRow[] rowsOperatorGroupVehicleGroup_VehicleGroup = row.GetChildRows("OperatorGroupVehicleGroup_VehicleGroup");
                if( rowsOperatorGroupVehicleGroup_VehicleGroup != null && rowsOperatorGroupVehicleGroup_VehicleGroup.Length > 0 ){
                    foreach (DataRow rowOperatorGroupVehicleGroup_VehicleGroup in rowsOperatorGroupVehicleGroup_VehicleGroup)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupVehicleGroup_VehicleGroup["OPERATORGROUP_ID"];
                        System.Int32 childVehiclegroupId = (System.Int32)rowOperatorGroupVehicleGroup_VehicleGroup["VEHICLEGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupVehicleGroup_VehicleGroup["RIGHT_ID"];
                        CascadeRemove_OperatorgroupVehiclegroup(dataset, childOperatorgroupId, childVehiclegroupId, childRightId);
                    }
                }
                DataRow[] rowsOperatorVehicleGroup_VehicleGroup = row.GetChildRows("OperatorVehicleGroup_VehicleGroup");
                if( rowsOperatorVehicleGroup_VehicleGroup != null && rowsOperatorVehicleGroup_VehicleGroup.Length > 0 ){
                    foreach (DataRow rowOperatorVehicleGroup_VehicleGroup in rowsOperatorVehicleGroup_VehicleGroup)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorVehicleGroup_VehicleGroup["OPERATOR_ID"];
                        System.Int32 childVehiclegroupId = (System.Int32)rowOperatorVehicleGroup_VehicleGroup["VEHICLEGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorVehicleGroup_VehicleGroup["RIGHT_ID"];
                        CascadeRemove_OperatorVehiclegroup(dataset, childOperatorId, childVehiclegroupId, childRightId);
                    }
                }
                DataRow[] rowsVehiclegroupRule_Vehicle = row.GetChildRows("VehiclegroupRule_Vehicle");
                if( rowsVehiclegroupRule_Vehicle != null && rowsVehiclegroupRule_Vehicle.Length > 0 ){
                    foreach (DataRow rowVehiclegroupRule_Vehicle in rowsVehiclegroupRule_Vehicle)
                    {
                        System.Int32 childVehiclegroupRuleId = (System.Int32)rowVehiclegroupRule_Vehicle["VEHICLEGROUP_RULE_ID"];
                        CascadeRemove_VehiclegroupRule(dataset, childVehiclegroupRuleId);
                    }
                }
                DataRow[] rowsVehicleGroupVehicle_VehicleGroup = row.GetChildRows("VehicleGroupVehicle_VehicleGroup");
                if( rowsVehicleGroupVehicle_VehicleGroup != null && rowsVehicleGroupVehicle_VehicleGroup.Length > 0 ){
                    foreach (DataRow rowVehicleGroupVehicle_VehicleGroup in rowsVehicleGroupVehicle_VehicleGroup)
                    {
                        System.Int32 childVehiclegroupId = (System.Int32)rowVehicleGroupVehicle_VehicleGroup["VEHICLEGROUP_ID"];
                        System.Int32 childVehicleId = (System.Int32)rowVehicleGroupVehicle_VehicleGroup["VEHICLE_ID"];
                        CascadeRemove_VehiclegroupVehicle(dataset, childVehiclegroupId, childVehicleId);
                    }
                }
                DataRow[] rowsZoneVehicleGroup_Vehicle = row.GetChildRows("ZoneVehicleGroup_Vehicle");
                if( rowsZoneVehicleGroup_Vehicle != null && rowsZoneVehicleGroup_Vehicle.Length > 0 ){
                    foreach (DataRow rowZoneVehicleGroup_Vehicle in rowsZoneVehicleGroup_Vehicle)
                    {
                        System.Int32 childZoneVehiclegroupId = (System.Int32)rowZoneVehicleGroup_Vehicle["ZONE_VEHICLEGROUP_ID"];
                        CascadeRemove_ZoneVehiclegroup(dataset, childZoneVehiclegroupId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [VEHICLEGROUP]
        ///</summary>
        public static void Compact_Vehiclegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["VEHICLEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [VEHICLEGROUP]
        ///</summary>
        public static void CompactFreshRecords_Vehiclegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["VEHICLEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["VEHICLEGROUP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [VEHICLEGROUP_RULE]
        ///</summary>
        public static void CascadeRemove_VehiclegroupRule(DataSet dataset, System.Int32 VehiclegroupRuleId)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLEGROUP_RULE"));
            DataRow row = dataset.Tables["VEHICLEGROUP_RULE"].Rows.Find(VehiclegroupRuleId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [VEHICLEGROUP_RULE]
        ///</summary>
        public static void Compact_VehiclegroupRule(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLEGROUP_RULE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["VEHICLEGROUP_RULE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [VEHICLEGROUP_RULE]
        ///</summary>
        public static void CompactFreshRecords_VehiclegroupRule(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLEGROUP_RULE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["VEHICLEGROUP_RULE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["VEHICLEGROUP_RULE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [VEHICLEGROUP_VEHICLE]
        ///</summary>
        public static void CascadeRemove_VehiclegroupVehicle(DataSet dataset, System.Int32 VehiclegroupId, System.Int32 VehicleId)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLEGROUP_VEHICLE"));
            DataRow row = dataset.Tables["VEHICLEGROUP_VEHICLE"].Rows.Find(new object[]{VehiclegroupId, VehicleId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [VEHICLEGROUP_VEHICLE]
        ///</summary>
        public static void Compact_VehiclegroupVehicle(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLEGROUP_VEHICLE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["VEHICLEGROUP_VEHICLE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [VEHICLEGROUP_VEHICLE]
        ///</summary>
        public static void CompactFreshRecords_VehiclegroupVehicle(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("VEHICLEGROUP_VEHICLE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["VEHICLEGROUP_VEHICLE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["VEHICLEGROUP_VEHICLE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [WAYBILL]
        ///</summary>
        public static void CascadeRemove_Waybill(DataSet dataset, System.Int32 WaybillId)
        {
            Debug.Assert(dataset.Tables.Contains("WAYBILL"));
            DataRow row = dataset.Tables["WAYBILL"].Rows.Find(WaybillId);
            if( row != null){
                DataRow[] rowsFK_BLADING_WAYBILL = row.GetChildRows("FK_BLADING_WAYBILL");
                if( rowsFK_BLADING_WAYBILL != null && rowsFK_BLADING_WAYBILL.Length > 0 ){
                    foreach (DataRow rowFK_BLADING_WAYBILL in rowsFK_BLADING_WAYBILL)
                    {
                        System.Int32 childBladingId = (System.Int32)rowFK_BLADING_WAYBILL["BLADING_ID"];
                        CascadeRemove_Blading(dataset, childBladingId);
                    }
                }
                DataRow[] rowsWbTrip_Waybill = row.GetChildRows("WbTrip_Waybill");
                if( rowsWbTrip_Waybill != null && rowsWbTrip_Waybill.Length > 0 ){
                    foreach (DataRow rowWbTrip_Waybill in rowsWbTrip_Waybill)
                    {
                        System.Int32 childWbTripId = (System.Int32)rowWbTrip_Waybill["WB_TRIP_ID"];
                        CascadeRemove_WbTrip(dataset, childWbTripId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [WAYBILL]
        ///</summary>
        public static void Compact_Waybill(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WAYBILL"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WAYBILL"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [WAYBILL]
        ///</summary>
        public static void CompactFreshRecords_Waybill(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WAYBILL"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WAYBILL"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["WAYBILL"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [WAYBILL_HEADER]
        ///</summary>
        public static void CascadeRemove_WaybillHeader(DataSet dataset, System.Int32 WaybillHeaderId)
        {
            Debug.Assert(dataset.Tables.Contains("WAYBILL_HEADER"));
            DataRow row = dataset.Tables["WAYBILL_HEADER"].Rows.Find(WaybillHeaderId);
            if( row != null){
                DataRow[] rowsSchedulePassage_WaybillHeader = row.GetChildRows("SchedulePassage_WaybillHeader");
                if( rowsSchedulePassage_WaybillHeader != null && rowsSchedulePassage_WaybillHeader.Length > 0 ){
                    foreach (DataRow rowSchedulePassage_WaybillHeader in rowsSchedulePassage_WaybillHeader)
                    {
                        System.Int32 childWhId = (System.Int32)rowSchedulePassage_WaybillHeader["WH_ID"];
                        System.Int32 childSpId = (System.Int32)rowSchedulePassage_WaybillHeader["SP_ID"];
                        CascadeRemove_SchedulePassage(dataset, childWhId, childSpId);
                    }
                }
                DataRow[] rowsWaybillheaderWaybillmark_WaybillHeader = row.GetChildRows("WaybillheaderWaybillmark_WaybillHeader");
                if( rowsWaybillheaderWaybillmark_WaybillHeader != null && rowsWaybillheaderWaybillmark_WaybillHeader.Length > 0 ){
                    foreach (DataRow rowWaybillheaderWaybillmark_WaybillHeader in rowsWaybillheaderWaybillmark_WaybillHeader)
                    {
                        System.Int32 childWaybillheaderId = (System.Int32)rowWaybillheaderWaybillmark_WaybillHeader["WAYBILLHEADER_ID"];
                        System.Int32 childWaybillmarkId = (System.Int32)rowWaybillheaderWaybillmark_WaybillHeader["WAYBILLMARK_ID"];
                        CascadeRemove_WaybillheaderWaybillmark(dataset, childWaybillheaderId, childWaybillmarkId);
                    }
                }
                DataRow[] rowsWbTrip_WaybillHeader = row.GetChildRows("WbTrip_WaybillHeader");
                if( rowsWbTrip_WaybillHeader != null && rowsWbTrip_WaybillHeader.Length > 0 ){
                    foreach (DataRow rowWbTrip_WaybillHeader in rowsWbTrip_WaybillHeader)
                    {
                        System.Int32 childWbTripId = (System.Int32)rowWbTrip_WaybillHeader["WB_TRIP_ID"];
                        CascadeRemove_WbTrip(dataset, childWbTripId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [WAYBILL_HEADER]
        ///</summary>
        public static void Compact_WaybillHeader(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WAYBILL_HEADER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WAYBILL_HEADER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [WAYBILL_HEADER]
        ///</summary>
        public static void CompactFreshRecords_WaybillHeader(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WAYBILL_HEADER"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WAYBILL_HEADER"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["WAYBILL_HEADER"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [WAYBILLHEADER_WAYBILLMARK]
        ///</summary>
        public static void CascadeRemove_WaybillheaderWaybillmark(DataSet dataset, System.Int32 WaybillheaderId, System.Int32 WaybillmarkId)
        {
            Debug.Assert(dataset.Tables.Contains("WAYBILLHEADER_WAYBILLMARK"));
            DataRow row = dataset.Tables["WAYBILLHEADER_WAYBILLMARK"].Rows.Find(new object[]{WaybillheaderId, WaybillmarkId});
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [WAYBILLHEADER_WAYBILLMARK]
        ///</summary>
        public static void Compact_WaybillheaderWaybillmark(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WAYBILLHEADER_WAYBILLMARK"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WAYBILLHEADER_WAYBILLMARK"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [WAYBILLHEADER_WAYBILLMARK]
        ///</summary>
        public static void CompactFreshRecords_WaybillheaderWaybillmark(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WAYBILLHEADER_WAYBILLMARK"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WAYBILLHEADER_WAYBILLMARK"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["WAYBILLHEADER_WAYBILLMARK"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [WAYBILLMARK]
        ///</summary>
        public static void CascadeRemove_Waybillmark(DataSet dataset, System.Int32 WaybillmarkId)
        {
            Debug.Assert(dataset.Tables.Contains("WAYBILLMARK"));
            DataRow row = dataset.Tables["WAYBILLMARK"].Rows.Find(WaybillmarkId);
            if( row != null){
                DataRow[] rowsWaybillheader_Waybillmark_Waybillmark = row.GetChildRows("Waybillheader_Waybillmark_Waybillmark");
                if( rowsWaybillheader_Waybillmark_Waybillmark != null && rowsWaybillheader_Waybillmark_Waybillmark.Length > 0 ){
                    foreach (DataRow rowWaybillheader_Waybillmark_Waybillmark in rowsWaybillheader_Waybillmark_Waybillmark)
                    {
                        System.Int32 childWaybillheaderId = (System.Int32)rowWaybillheader_Waybillmark_Waybillmark["WAYBILLHEADER_ID"];
                        System.Int32 childWaybillmarkId = (System.Int32)rowWaybillheader_Waybillmark_Waybillmark["WAYBILLMARK_ID"];
                        CascadeRemove_WaybillheaderWaybillmark(dataset, childWaybillheaderId, childWaybillmarkId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [WAYBILLMARK]
        ///</summary>
        public static void Compact_Waybillmark(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WAYBILLMARK"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WAYBILLMARK"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [WAYBILLMARK]
        ///</summary>
        public static void CompactFreshRecords_Waybillmark(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WAYBILLMARK"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WAYBILLMARK"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["WAYBILLMARK"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [WAYOUT]
        ///</summary>
        public static void CascadeRemove_Wayout(DataSet dataset, System.Int32 WayoutId)
        {
            Debug.Assert(dataset.Tables.Contains("WAYOUT"));
            DataRow row = dataset.Tables["WAYOUT"].Rows.Find(WayoutId);
            if( row != null){
                DataRow[] rowsFK_JOURNAL_DAY_WAYOUT = row.GetChildRows("FK_JOURNAL_DAY_WAYOUT");
                if( rowsFK_JOURNAL_DAY_WAYOUT != null && rowsFK_JOURNAL_DAY_WAYOUT.Length > 0 ){
                    foreach (DataRow rowFK_JOURNAL_DAY_WAYOUT in rowsFK_JOURNAL_DAY_WAYOUT)
                    {
                        System.Int32 childJournalDayId = (System.Int32)rowFK_JOURNAL_DAY_WAYOUT["JOURNAL_DAY_ID"];
                        CascadeRemove_JournalDay(dataset, childJournalDayId);
                    }
                }
                DataRow[] rowsFK_RS_WAYOUT_WAYOUT = row.GetChildRows("FK_RS_WAYOUT_WAYOUT");
                if( rowsFK_RS_WAYOUT_WAYOUT != null && rowsFK_RS_WAYOUT_WAYOUT.Length > 0 ){
                    foreach (DataRow rowFK_RS_WAYOUT_WAYOUT in rowsFK_RS_WAYOUT_WAYOUT)
                    {
                        System.Int32 childRsWayoutId = (System.Int32)rowFK_RS_WAYOUT_WAYOUT["RS_WAYOUT_ID"];
                        CascadeRemove_RsWayout(dataset, childRsWayoutId);
                    }
                }
                DataRow[] rowsJournal_Wayout = row.GetChildRows("Journal_Wayout");
                if( rowsJournal_Wayout != null && rowsJournal_Wayout.Length > 0 ){
                    foreach (DataRow rowJournal_Wayout in rowsJournal_Wayout)
                    {
                        System.Int32 childJournalId = (System.Int32)rowJournal_Wayout["JOURNAL_ID"];
                        CascadeRemove_Journal(dataset, childJournalId);
                    }
                }
                DataRow[] rowsSchedule_Wayout = row.GetChildRows("Schedule_Wayout");
                if( rowsSchedule_Wayout != null && rowsSchedule_Wayout.Length > 0 ){
                    foreach (DataRow rowSchedule_Wayout in rowsSchedule_Wayout)
                    {
                        System.Int32 childScheduleId = (System.Int32)rowSchedule_Wayout["SCHEDULE_ID"];
                        CascadeRemove_Schedule(dataset, childScheduleId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [WAYOUT]
        ///</summary>
        public static void Compact_Wayout(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WAYOUT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WAYOUT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [WAYOUT]
        ///</summary>
        public static void CompactFreshRecords_Wayout(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WAYOUT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WAYOUT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["WAYOUT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [WB_TRIP]
        ///</summary>
        public static void CascadeRemove_WbTrip(DataSet dataset, System.Int32 WbTripId)
        {
            Debug.Assert(dataset.Tables.Contains("WB_TRIP"));
            DataRow row = dataset.Tables["WB_TRIP"].Rows.Find(WbTripId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [WB_TRIP]
        ///</summary>
        public static void Compact_WbTrip(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WB_TRIP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WB_TRIP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [WB_TRIP]
        ///</summary>
        public static void CompactFreshRecords_WbTrip(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WB_TRIP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WB_TRIP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["WB_TRIP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [WEB_LINE]
        ///</summary>
        public static void CascadeRemove_WebLine(DataSet dataset, System.Int32 LineId)
        {
            Debug.Assert(dataset.Tables.Contains("WEB_LINE"));
            DataRow row = dataset.Tables["WEB_LINE"].Rows.Find(LineId);
            if( row != null){
                DataRow[] rowsWebLineVertex_WebLine = row.GetChildRows("WebLineVertex_WebLine");
                if( rowsWebLineVertex_WebLine != null && rowsWebLineVertex_WebLine.Length > 0 ){
                    foreach (DataRow rowWebLineVertex_WebLine in rowsWebLineVertex_WebLine)
                    {
                        System.Int32 childLineVertexId = (System.Int32)rowWebLineVertex_WebLine["LINE_VERTEX_ID"];
                        CascadeRemove_WebLineVertex(dataset, childLineVertexId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [WEB_LINE]
        ///</summary>
        public static void Compact_WebLine(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WEB_LINE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WEB_LINE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [WEB_LINE]
        ///</summary>
        public static void CompactFreshRecords_WebLine(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WEB_LINE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WEB_LINE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["WEB_LINE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [WEB_LINE_VERTEX]
        ///</summary>
        public static void CascadeRemove_WebLineVertex(DataSet dataset, System.Int32 LineVertexId)
        {
            Debug.Assert(dataset.Tables.Contains("WEB_LINE_VERTEX"));
            DataRow row = dataset.Tables["WEB_LINE_VERTEX"].Rows.Find(LineVertexId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [WEB_LINE_VERTEX]
        ///</summary>
        public static void Compact_WebLineVertex(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WEB_LINE_VERTEX"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WEB_LINE_VERTEX"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [WEB_LINE_VERTEX]
        ///</summary>
        public static void CompactFreshRecords_WebLineVertex(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WEB_LINE_VERTEX"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WEB_LINE_VERTEX"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["WEB_LINE_VERTEX"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [WEB_POINT]
        ///</summary>
        public static void CascadeRemove_WebPoint(DataSet dataset, System.Int32 PointId)
        {
            Debug.Assert(dataset.Tables.Contains("WEB_POINT"));
            DataRow row = dataset.Tables["WEB_POINT"].Rows.Find(PointId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [WEB_POINT]
        ///</summary>
        public static void Compact_WebPoint(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WEB_POINT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WEB_POINT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [WEB_POINT]
        ///</summary>
        public static void CompactFreshRecords_WebPoint(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WEB_POINT"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WEB_POINT"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["WEB_POINT"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [WEB_POINT_TYPE]
        ///</summary>
        public static void CascadeRemove_WebPointType(DataSet dataset, System.Int32 TypeId)
        {
            Debug.Assert(dataset.Tables.Contains("WEB_POINT_TYPE"));
            DataRow row = dataset.Tables["WEB_POINT_TYPE"].Rows.Find(TypeId);
            if( row != null){
                DataRow[] rowsWebPoint_Type = row.GetChildRows("WebPoint_Type");
                if( rowsWebPoint_Type != null && rowsWebPoint_Type.Length > 0 ){
                    foreach (DataRow rowWebPoint_Type in rowsWebPoint_Type)
                    {
                        System.Int32 childPointId = (System.Int32)rowWebPoint_Type["POINT_ID"];
                        CascadeRemove_WebPoint(dataset, childPointId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [WEB_POINT_TYPE]
        ///</summary>
        public static void Compact_WebPointType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WEB_POINT_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WEB_POINT_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [WEB_POINT_TYPE]
        ///</summary>
        public static void CompactFreshRecords_WebPointType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WEB_POINT_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WEB_POINT_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["WEB_POINT_TYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [WORK_TIME]
        ///</summary>
        public static void CascadeRemove_WorkTime(DataSet dataset, System.Int32 DriverId)
        {
            Debug.Assert(dataset.Tables.Contains("WORK_TIME"));
            DataRow row = dataset.Tables["WORK_TIME"].Rows.Find(DriverId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [WORK_TIME]
        ///</summary>
        public static void Compact_WorkTime(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WORK_TIME"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WORK_TIME"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [WORK_TIME]
        ///</summary>
        public static void CompactFreshRecords_WorkTime(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WORK_TIME"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WORK_TIME"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["WORK_TIME"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [WORKPLACE_CONFIG]
        ///</summary>
        public static void CascadeRemove_WorkplaceConfig(DataSet dataset, System.Int32 Id)
        {
            Debug.Assert(dataset.Tables.Contains("WORKPLACE_CONFIG"));
            DataRow row = dataset.Tables["WORKPLACE_CONFIG"].Rows.Find(Id);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [WORKPLACE_CONFIG]
        ///</summary>
        public static void Compact_WorkplaceConfig(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WORKPLACE_CONFIG"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WORKPLACE_CONFIG"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [WORKPLACE_CONFIG]
        ///</summary>
        public static void CompactFreshRecords_WorkplaceConfig(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WORKPLACE_CONFIG"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WORKPLACE_CONFIG"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["WORKPLACE_CONFIG"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [WORKSTATION]
        ///</summary>
        public static void CascadeRemove_Workstation(DataSet dataset, System.Int32 WorkstationId)
        {
            Debug.Assert(dataset.Tables.Contains("WORKSTATION"));
            DataRow row = dataset.Tables["WORKSTATION"].Rows.Find(WorkstationId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [WORKSTATION]
        ///</summary>
        public static void Compact_Workstation(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WORKSTATION"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WORKSTATION"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [WORKSTATION]
        ///</summary>
        public static void CompactFreshRecords_Workstation(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WORKSTATION"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WORKSTATION"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["WORKSTATION"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [WORKTIME_REASON]
        ///</summary>
        public static void CascadeRemove_WorktimeReason(DataSet dataset, System.Int32 WorktimeReasonId)
        {
            Debug.Assert(dataset.Tables.Contains("WORKTIME_REASON"));
            DataRow row = dataset.Tables["WORKTIME_REASON"].Rows.Find(WorktimeReasonId);
            if( row != null){
                DataRow[] rowsWbTrip_WorktimeReason = row.GetChildRows("WbTrip_WorktimeReason");
                if( rowsWbTrip_WorktimeReason != null && rowsWbTrip_WorktimeReason.Length > 0 ){
                    foreach (DataRow rowWbTrip_WorktimeReason in rowsWbTrip_WorktimeReason)
                    {
                        System.Int32 childWbTripId = (System.Int32)rowWbTrip_WorktimeReason["WB_TRIP_ID"];
                        CascadeRemove_WbTrip(dataset, childWbTripId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [WORKTIME_REASON]
        ///</summary>
        public static void Compact_WorktimeReason(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WORKTIME_REASON"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WORKTIME_REASON"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [WORKTIME_REASON]
        ///</summary>
        public static void CompactFreshRecords_WorktimeReason(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WORKTIME_REASON"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WORKTIME_REASON"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["WORKTIME_REASON"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [WORKTIME_STATUS_TYPE]
        ///</summary>
        public static void CascadeRemove_WorktimeStatusType(DataSet dataset, System.Int32 WorktimeStatusTypeId)
        {
            Debug.Assert(dataset.Tables.Contains("WORKTIME_STATUS_TYPE"));
            DataRow row = dataset.Tables["WORKTIME_STATUS_TYPE"].Rows.Find(WorktimeStatusTypeId);
            if( row != null){
                DataRow[] rowsTripKind_WorktimeStatusType = row.GetChildRows("TripKind_WorktimeStatusType");
                if( rowsTripKind_WorktimeStatusType != null && rowsTripKind_WorktimeStatusType.Length > 0 ){
                    foreach (DataRow rowTripKind_WorktimeStatusType in rowsTripKind_WorktimeStatusType)
                    {
                        System.Int32 childTripKindId = (System.Int32)rowTripKind_WorktimeStatusType["TRIP_KIND_ID"];
                        CascadeRemove_TripKind(dataset, childTripKindId);
                    }
                }
                DataRow[] rowsWorktimeReason_WorktimeStatusType = row.GetChildRows("WorktimeReason_WorktimeStatusType");
                if( rowsWorktimeReason_WorktimeStatusType != null && rowsWorktimeReason_WorktimeStatusType.Length > 0 ){
                    foreach (DataRow rowWorktimeReason_WorktimeStatusType in rowsWorktimeReason_WorktimeStatusType)
                    {
                        System.Int32 childWorktimeReasonId = (System.Int32)rowWorktimeReason_WorktimeStatusType["WORKTIME_REASON_ID"];
                        CascadeRemove_WorktimeReason(dataset, childWorktimeReasonId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [WORKTIME_STATUS_TYPE]
        ///</summary>
        public static void Compact_WorktimeStatusType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WORKTIME_STATUS_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WORKTIME_STATUS_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [WORKTIME_STATUS_TYPE]
        ///</summary>
        public static void CompactFreshRecords_WorktimeStatusType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("WORKTIME_STATUS_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["WORKTIME_STATUS_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["WORKTIME_STATUS_TYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [ZONE_PRIMITIVE]
        ///</summary>
        public static void CascadeRemove_ZonePrimitive(DataSet dataset, System.Int32 PrimitiveId)
        {
            Debug.Assert(dataset.Tables.Contains("ZONE_PRIMITIVE"));
            DataRow row = dataset.Tables["ZONE_PRIMITIVE"].Rows.Find(PrimitiveId);
            if( row != null){
                DataRow[] rowsGeoZonePrimitive_ZonePrimitive = row.GetChildRows("GeoZonePrimitive_ZonePrimitive");
                if( rowsGeoZonePrimitive_ZonePrimitive != null && rowsGeoZonePrimitive_ZonePrimitive.Length > 0 ){
                    foreach (DataRow rowGeoZonePrimitive_ZonePrimitive in rowsGeoZonePrimitive_ZonePrimitive)
                    {
                        System.Int32 childGeoZonePrimitiveId = (System.Int32)rowGeoZonePrimitive_ZonePrimitive["GEO_ZONE_PRIMITIVE_ID"];
                        CascadeRemove_GeoZonePrimitive(dataset, childGeoZonePrimitiveId);
                    }
                }
                DataRow[] rowsZonePrimitiveVertex_ZonePrimitive = row.GetChildRows("ZonePrimitiveVertex_ZonePrimitive");
                if( rowsZonePrimitiveVertex_ZonePrimitive != null && rowsZonePrimitiveVertex_ZonePrimitive.Length > 0 ){
                    foreach (DataRow rowZonePrimitiveVertex_ZonePrimitive in rowsZonePrimitiveVertex_ZonePrimitive)
                    {
                        System.Int32 childPrimitiveVertexId = (System.Int32)rowZonePrimitiveVertex_ZonePrimitive["PRIMITIVE_VERTEX_ID"];
                        CascadeRemove_ZonePrimitiveVertex(dataset, childPrimitiveVertexId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [ZONE_PRIMITIVE]
        ///</summary>
        public static void Compact_ZonePrimitive(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ZONE_PRIMITIVE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ZONE_PRIMITIVE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [ZONE_PRIMITIVE]
        ///</summary>
        public static void CompactFreshRecords_ZonePrimitive(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ZONE_PRIMITIVE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ZONE_PRIMITIVE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["ZONE_PRIMITIVE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [ZONE_PRIMITIVE_VERTEX]
        ///</summary>
        public static void CascadeRemove_ZonePrimitiveVertex(DataSet dataset, System.Int32 PrimitiveVertexId)
        {
            Debug.Assert(dataset.Tables.Contains("ZONE_PRIMITIVE_VERTEX"));
            DataRow row = dataset.Tables["ZONE_PRIMITIVE_VERTEX"].Rows.Find(PrimitiveVertexId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [ZONE_PRIMITIVE_VERTEX]
        ///</summary>
        public static void Compact_ZonePrimitiveVertex(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ZONE_PRIMITIVE_VERTEX"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ZONE_PRIMITIVE_VERTEX"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [ZONE_PRIMITIVE_VERTEX]
        ///</summary>
        public static void CompactFreshRecords_ZonePrimitiveVertex(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ZONE_PRIMITIVE_VERTEX"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ZONE_PRIMITIVE_VERTEX"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["ZONE_PRIMITIVE_VERTEX"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [ZONE_TYPE]
        ///</summary>
        public static void CascadeRemove_ZoneType(DataSet dataset, System.Int32 ZoneTypeId)
        {
            Debug.Assert(dataset.Tables.Contains("ZONE_TYPE"));
            DataRow row = dataset.Tables["ZONE_TYPE"].Rows.Find(ZoneTypeId);
            if( row != null){
                DataRow[] rowsZonePrimitive_ZoneType = row.GetChildRows("ZonePrimitive_ZoneType");
                if( rowsZonePrimitive_ZoneType != null && rowsZonePrimitive_ZoneType.Length > 0 ){
                    foreach (DataRow rowZonePrimitive_ZoneType in rowsZonePrimitive_ZoneType)
                    {
                        System.Int32 childPrimitiveId = (System.Int32)rowZonePrimitive_ZoneType["PRIMITIVE_ID"];
                        CascadeRemove_ZonePrimitive(dataset, childPrimitiveId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [ZONE_TYPE]
        ///</summary>
        public static void Compact_ZoneType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ZONE_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ZONE_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [ZONE_TYPE]
        ///</summary>
        public static void CompactFreshRecords_ZoneType(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ZONE_TYPE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ZONE_TYPE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["ZONE_TYPE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [ZONE_VEHICLE]
        ///</summary>
        public static void CascadeRemove_ZoneVehicle(DataSet dataset, System.Int32 ZoneVehicleId)
        {
            Debug.Assert(dataset.Tables.Contains("ZONE_VEHICLE"));
            DataRow row = dataset.Tables["ZONE_VEHICLE"].Rows.Find(ZoneVehicleId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [ZONE_VEHICLE]
        ///</summary>
        public static void Compact_ZoneVehicle(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ZONE_VEHICLE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ZONE_VEHICLE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [ZONE_VEHICLE]
        ///</summary>
        public static void CompactFreshRecords_ZoneVehicle(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ZONE_VEHICLE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ZONE_VEHICLE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["ZONE_VEHICLE"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [ZONE_VEHICLEGROUP]
        ///</summary>
        public static void CascadeRemove_ZoneVehiclegroup(DataSet dataset, System.Int32 ZoneVehiclegroupId)
        {
            Debug.Assert(dataset.Tables.Contains("ZONE_VEHICLEGROUP"));
            DataRow row = dataset.Tables["ZONE_VEHICLEGROUP"].Rows.Find(ZoneVehiclegroupId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [ZONE_VEHICLEGROUP]
        ///</summary>
        public static void Compact_ZoneVehiclegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ZONE_VEHICLEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ZONE_VEHICLEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [ZONE_VEHICLEGROUP]
        ///</summary>
        public static void CompactFreshRecords_ZoneVehiclegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ZONE_VEHICLEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ZONE_VEHICLEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["ZONE_VEHICLEGROUP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [ZONEGROUP]
        ///</summary>
        public static void CascadeRemove_Zonegroup(DataSet dataset, System.Int32 ZonegroupId)
        {
            Debug.Assert(dataset.Tables.Contains("ZONEGROUP"));
            DataRow row = dataset.Tables["ZONEGROUP"].Rows.Find(ZonegroupId);
            if( row != null){
                DataRow[] rowsOperatorGroupZoneGroup_ZoneGroup = row.GetChildRows("OperatorGroupZoneGroup_ZoneGroup");
                if( rowsOperatorGroupZoneGroup_ZoneGroup != null && rowsOperatorGroupZoneGroup_ZoneGroup.Length > 0 ){
                    foreach (DataRow rowOperatorGroupZoneGroup_ZoneGroup in rowsOperatorGroupZoneGroup_ZoneGroup)
                    {
                        System.Int32 childOperatorgroupId = (System.Int32)rowOperatorGroupZoneGroup_ZoneGroup["OPERATORGROUP_ID"];
                        System.Int32 childZonegroupId = (System.Int32)rowOperatorGroupZoneGroup_ZoneGroup["ZONEGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorGroupZoneGroup_ZoneGroup["RIGHT_ID"];
                        CascadeRemove_OperatorgroupZonegroup(dataset, childOperatorgroupId, childZonegroupId, childRightId);
                    }
                }
                DataRow[] rowsOperatorZoneGroup_ZoneGroup = row.GetChildRows("OperatorZoneGroup_ZoneGroup");
                if( rowsOperatorZoneGroup_ZoneGroup != null && rowsOperatorZoneGroup_ZoneGroup.Length > 0 ){
                    foreach (DataRow rowOperatorZoneGroup_ZoneGroup in rowsOperatorZoneGroup_ZoneGroup)
                    {
                        System.Int32 childOperatorId = (System.Int32)rowOperatorZoneGroup_ZoneGroup["OPERATOR_ID"];
                        System.Int32 childZonegroupId = (System.Int32)rowOperatorZoneGroup_ZoneGroup["ZONEGROUP_ID"];
                        System.Int32 childRightId = (System.Int32)rowOperatorZoneGroup_ZoneGroup["RIGHT_ID"];
                        CascadeRemove_OperatorZonegroup(dataset, childOperatorId, childZonegroupId, childRightId);
                    }
                }
                DataRow[] rowsZoneGroupZone_ZoneGroup = row.GetChildRows("ZoneGroupZone_ZoneGroup");
                if( rowsZoneGroupZone_ZoneGroup != null && rowsZoneGroupZone_ZoneGroup.Length > 0 ){
                    foreach (DataRow rowZoneGroupZone_ZoneGroup in rowsZoneGroupZone_ZoneGroup)
                    {
                        System.Int32 childZonegroupZoneId = (System.Int32)rowZoneGroupZone_ZoneGroup["ZONEGROUP_ZONE_ID"];
                        CascadeRemove_ZonegroupZone(dataset, childZonegroupZoneId);
                    }
                }
                row.Delete();
            }
        }
        ///<summary>
        /// [ZONEGROUP]
        ///</summary>
        public static void Compact_Zonegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ZONEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ZONEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [ZONEGROUP]
        ///</summary>
        public static void CompactFreshRecords_Zonegroup(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ZONEGROUP"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ZONEGROUP"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["ZONEGROUP"].Rows.Remove(row);
            }
        }
        ///<summary>
        /// [ZONEGROUP_ZONE]
        ///</summary>
        public static void CascadeRemove_ZonegroupZone(DataSet dataset, System.Int32 ZonegroupZoneId)
        {
            Debug.Assert(dataset.Tables.Contains("ZONEGROUP_ZONE"));
            DataRow row = dataset.Tables["ZONEGROUP_ZONE"].Rows.Find(ZonegroupZoneId);
            if( row != null){
                row.Delete();
            }
        }
        ///<summary>
        /// [ZONEGROUP_ZONE]
        ///</summary>
        public static void Compact_ZonegroupZone(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ZONEGROUP_ZONE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ZONEGROUP_ZONE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    rowsToDelete.Add(row);
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                row.Delete();
            }
        }
        ///<summary>
        /// [ZONEGROUP_ZONE]
        ///</summary>
        public static void CompactFreshRecords_ZonegroupZone(DataSet dataset)
        {
            Debug.Assert(dataset.Tables.Contains("ZONEGROUP_ZONE"));
            ArrayList rowsToDelete = new ArrayList();
            foreach (DataRow row in dataset.Tables["ZONEGROUP_ZONE"].Rows)
            {
                if (row["DELETED_EVENT_ID"] != DBNull.Value)
                {
                    if (row.HasVersion(DataRowVersion.Original) == false)
                    {
                        rowsToDelete.Add(row);
                    }
                }
            }
            foreach (DataRow row in rowsToDelete)
            {
                dataset.Tables["ZONEGROUP_ZONE"].Rows.Remove(row);
            }
        }
        
        public static void Compact(DataSet dataset)
        {
        }
        
        public static void CompactFreshRecords(DataSet dataset)
        {
        }
        public static void Merge(DataSet cache, DataSet newdata)
        {
            bool oldForce = cache.EnforceConstraints;
            DatabaseSchema.MakeSchema(cache);
            cache.EnforceConstraints = false;
            // foreach table with PK
            bool processBlading = newdata.Tables.Contains("BLADING");
            if (processBlading)
            {
                if (cache.Tables.Contains("BLADING") == false)
                {
                    DatabaseSchema.MakeTable_Blading(cache);
                }
            }
            bool processBladingType = newdata.Tables.Contains("BLADING_TYPE");
            if (processBladingType)
            {
                if (cache.Tables.Contains("BLADING_TYPE") == false)
                {
                    DatabaseSchema.MakeTable_BladingType(cache);
                }
            }
            bool processBrigade = newdata.Tables.Contains("BRIGADE");
            if (processBrigade)
            {
                if (cache.Tables.Contains("BRIGADE") == false)
                {
                    DatabaseSchema.MakeTable_Brigade(cache);
                }
            }
            bool processBrigadeDriver = newdata.Tables.Contains("BRIGADE_DRIVER");
            if (processBrigadeDriver)
            {
                if (cache.Tables.Contains("BRIGADE_DRIVER") == false)
                {
                    DatabaseSchema.MakeTable_BrigadeDriver(cache);
                }
            }
            bool processBusstop = newdata.Tables.Contains("BUSSTOP");
            if (processBusstop)
            {
                if (cache.Tables.Contains("BUSSTOP") == false)
                {
                    DatabaseSchema.MakeTable_Busstop(cache);
                }
            }
            bool processCal = newdata.Tables.Contains("CAL");
            if (processCal)
            {
                if (cache.Tables.Contains("CAL") == false)
                {
                    DatabaseSchema.MakeTable_Cal(cache);
                }
            }
            bool processCalendarDay = newdata.Tables.Contains("CALENDAR_DAY");
            if (processCalendarDay)
            {
                if (cache.Tables.Contains("CALENDAR_DAY") == false)
                {
                    DatabaseSchema.MakeTable_CalendarDay(cache);
                }
            }
            bool processConstants = newdata.Tables.Contains("CONSTANTS");
            if (processConstants)
            {
                if (cache.Tables.Contains("CONSTANTS") == false)
                {
                    DatabaseSchema.MakeTable_Constants(cache);
                }
            }
            bool processContractor = newdata.Tables.Contains("CONTRACTOR");
            if (processContractor)
            {
                if (cache.Tables.Contains("CONTRACTOR") == false)
                {
                    DatabaseSchema.MakeTable_Contractor(cache);
                }
            }
            bool processContractorCalendar = newdata.Tables.Contains("CONTRACTOR_CALENDAR");
            if (processContractorCalendar)
            {
                if (cache.Tables.Contains("CONTRACTOR_CALENDAR") == false)
                {
                    DatabaseSchema.MakeTable_ContractorCalendar(cache);
                }
            }
            bool processController = newdata.Tables.Contains("CONTROLLER");
            if (processController)
            {
                if (cache.Tables.Contains("CONTROLLER") == false)
                {
                    DatabaseSchema.MakeTable_Controller(cache);
                }
            }
            bool processControllerInfo = newdata.Tables.Contains("CONTROLLER_INFO");
            if (processControllerInfo)
            {
                if (cache.Tables.Contains("CONTROLLER_INFO") == false)
                {
                    DatabaseSchema.MakeTable_ControllerInfo(cache);
                }
            }
            bool processControllerSensor = newdata.Tables.Contains("CONTROLLER_SENSOR");
            if (processControllerSensor)
            {
                if (cache.Tables.Contains("CONTROLLER_SENSOR") == false)
                {
                    DatabaseSchema.MakeTable_ControllerSensor(cache);
                }
            }
            bool processControllerSensorLegend = newdata.Tables.Contains("CONTROLLER_SENSOR_LEGEND");
            if (processControllerSensorLegend)
            {
                if (cache.Tables.Contains("CONTROLLER_SENSOR_LEGEND") == false)
                {
                    DatabaseSchema.MakeTable_ControllerSensorLegend(cache);
                }
            }
            bool processControllerSensorMap = newdata.Tables.Contains("CONTROLLER_SENSOR_MAP");
            if (processControllerSensorMap)
            {
                if (cache.Tables.Contains("CONTROLLER_SENSOR_MAP") == false)
                {
                    DatabaseSchema.MakeTable_ControllerSensorMap(cache);
                }
            }
            bool processControllerSensorType = newdata.Tables.Contains("CONTROLLER_SENSOR_TYPE");
            if (processControllerSensorType)
            {
                if (cache.Tables.Contains("CONTROLLER_SENSOR_TYPE") == false)
                {
                    DatabaseSchema.MakeTable_ControllerSensorType(cache);
                }
            }
            bool processControllerTime = newdata.Tables.Contains("CONTROLLER_TIME");
            if (processControllerTime)
            {
                if (cache.Tables.Contains("CONTROLLER_TIME") == false)
                {
                    DatabaseSchema.MakeTable_ControllerTime(cache);
                }
            }
            bool processControllerType = newdata.Tables.Contains("CONTROLLER_TYPE");
            if (processControllerType)
            {
                if (cache.Tables.Contains("CONTROLLER_TYPE") == false)
                {
                    DatabaseSchema.MakeTable_ControllerType(cache);
                }
            }
            bool processCountGprs = newdata.Tables.Contains("COUNT_GPRS");
            if (processCountGprs)
            {
                if (cache.Tables.Contains("COUNT_GPRS") == false)
                {
                    DatabaseSchema.MakeTable_CountGprs(cache);
                }
            }
            bool processCountSms = newdata.Tables.Contains("COUNT_SMS");
            if (processCountSms)
            {
                if (cache.Tables.Contains("COUNT_SMS") == false)
                {
                    DatabaseSchema.MakeTable_CountSms(cache);
                }
            }
            bool processCustomer = newdata.Tables.Contains("CUSTOMER");
            if (processCustomer)
            {
                if (cache.Tables.Contains("CUSTOMER") == false)
                {
                    DatabaseSchema.MakeTable_Customer(cache);
                }
            }
            bool processDay = newdata.Tables.Contains("DAY");
            if (processDay)
            {
                if (cache.Tables.Contains("DAY") == false)
                {
                    DatabaseSchema.MakeTable_Day(cache);
                }
            }
            bool processDayKind = newdata.Tables.Contains("DAY_KIND");
            if (processDayKind)
            {
                if (cache.Tables.Contains("DAY_KIND") == false)
                {
                    DatabaseSchema.MakeTable_DayKind(cache);
                }
            }
            bool processDayTime = newdata.Tables.Contains("DAY_TIME");
            if (processDayTime)
            {
                if (cache.Tables.Contains("DAY_TIME") == false)
                {
                    DatabaseSchema.MakeTable_DayTime(cache);
                }
            }
            bool processDayType = newdata.Tables.Contains("DAY_TYPE");
            if (processDayType)
            {
                if (cache.Tables.Contains("DAY_TYPE") == false)
                {
                    DatabaseSchema.MakeTable_DayType(cache);
                }
            }
            bool processDecade = newdata.Tables.Contains("DECADE");
            if (processDecade)
            {
                if (cache.Tables.Contains("DECADE") == false)
                {
                    DatabaseSchema.MakeTable_Decade(cache);
                }
            }
            bool processDepartment = newdata.Tables.Contains("DEPARTMENT");
            if (processDepartment)
            {
                if (cache.Tables.Contains("DEPARTMENT") == false)
                {
                    DatabaseSchema.MakeTable_Department(cache);
                }
            }
            bool processDkSet = newdata.Tables.Contains("DK_SET");
            if (processDkSet)
            {
                if (cache.Tables.Contains("DK_SET") == false)
                {
                    DatabaseSchema.MakeTable_DkSet(cache);
                }
            }
            bool processDotnetType = newdata.Tables.Contains("DOTNET_TYPE");
            if (processDotnetType)
            {
                if (cache.Tables.Contains("DOTNET_TYPE") == false)
                {
                    DatabaseSchema.MakeTable_DotnetType(cache);
                }
            }
            bool processDriver = newdata.Tables.Contains("DRIVER");
            if (processDriver)
            {
                if (cache.Tables.Contains("DRIVER") == false)
                {
                    DatabaseSchema.MakeTable_Driver(cache);
                }
            }
            bool processDriverBonus = newdata.Tables.Contains("DRIVER_BONUS");
            if (processDriverBonus)
            {
                if (cache.Tables.Contains("DRIVER_BONUS") == false)
                {
                    DatabaseSchema.MakeTable_DriverBonus(cache);
                }
            }
            bool processDriverMsgTemplate = newdata.Tables.Contains("DRIVER_MSG_TEMPLATE");
            if (processDriverMsgTemplate)
            {
                if (cache.Tables.Contains("DRIVER_MSG_TEMPLATE") == false)
                {
                    DatabaseSchema.MakeTable_DriverMsgTemplate(cache);
                }
            }
            bool processDriverStatus = newdata.Tables.Contains("DRIVER_STATUS");
            if (processDriverStatus)
            {
                if (cache.Tables.Contains("DRIVER_STATUS") == false)
                {
                    DatabaseSchema.MakeTable_DriverStatus(cache);
                }
            }
            bool processDriverVehicle = newdata.Tables.Contains("DRIVER_VEHICLE");
            if (processDriverVehicle)
            {
                if (cache.Tables.Contains("DRIVER_VEHICLE") == false)
                {
                    DatabaseSchema.MakeTable_DriverVehicle(cache);
                }
            }
            bool processDrivergroup = newdata.Tables.Contains("DRIVERGROUP");
            if (processDrivergroup)
            {
                if (cache.Tables.Contains("DRIVERGROUP") == false)
                {
                    DatabaseSchema.MakeTable_Drivergroup(cache);
                }
            }
            bool processDrivergroupDriver = newdata.Tables.Contains("DRIVERGROUP_DRIVER");
            if (processDrivergroupDriver)
            {
                if (cache.Tables.Contains("DRIVERGROUP_DRIVER") == false)
                {
                    DatabaseSchema.MakeTable_DrivergroupDriver(cache);
                }
            }
            bool processEmail = newdata.Tables.Contains("EMAIL");
            if (processEmail)
            {
                if (cache.Tables.Contains("EMAIL") == false)
                {
                    DatabaseSchema.MakeTable_Email(cache);
                }
            }
            bool processEmailSchedulerevent = newdata.Tables.Contains("EMAIL_SCHEDULEREVENT");
            if (processEmailSchedulerevent)
            {
                if (cache.Tables.Contains("EMAIL_SCHEDULEREVENT") == false)
                {
                    DatabaseSchema.MakeTable_EmailSchedulerevent(cache);
                }
            }
            bool processEmailSchedulerqueue = newdata.Tables.Contains("EMAIL_SCHEDULERQUEUE");
            if (processEmailSchedulerqueue)
            {
                if (cache.Tables.Contains("EMAIL_SCHEDULERQUEUE") == false)
                {
                    DatabaseSchema.MakeTable_EmailSchedulerqueue(cache);
                }
            }
            bool processFactorValues = newdata.Tables.Contains("FACTOR_VALUES");
            if (processFactorValues)
            {
                if (cache.Tables.Contains("FACTOR_VALUES") == false)
                {
                    DatabaseSchema.MakeTable_FactorValues(cache);
                }
            }
            bool processFactors = newdata.Tables.Contains("FACTORS");
            if (processFactors)
            {
                if (cache.Tables.Contains("FACTORS") == false)
                {
                    DatabaseSchema.MakeTable_Factors(cache);
                }
            }
            bool processGeoSegment = newdata.Tables.Contains("GEO_SEGMENT");
            if (processGeoSegment)
            {
                if (cache.Tables.Contains("GEO_SEGMENT") == false)
                {
                    DatabaseSchema.MakeTable_GeoSegment(cache);
                }
            }
            bool processGeoSegmentRuntime = newdata.Tables.Contains("GEO_SEGMENT_RUNTIME");
            if (processGeoSegmentRuntime)
            {
                if (cache.Tables.Contains("GEO_SEGMENT_RUNTIME") == false)
                {
                    DatabaseSchema.MakeTable_GeoSegmentRuntime(cache);
                }
            }
            bool processGeoSegmentVertex = newdata.Tables.Contains("GEO_SEGMENT_VERTEX");
            if (processGeoSegmentVertex)
            {
                if (cache.Tables.Contains("GEO_SEGMENT_VERTEX") == false)
                {
                    DatabaseSchema.MakeTable_GeoSegmentVertex(cache);
                }
            }
            bool processGeoTrip = newdata.Tables.Contains("GEO_TRIP");
            if (processGeoTrip)
            {
                if (cache.Tables.Contains("GEO_TRIP") == false)
                {
                    DatabaseSchema.MakeTable_GeoTrip(cache);
                }
            }
            bool processGeoZone = newdata.Tables.Contains("GEO_ZONE");
            if (processGeoZone)
            {
                if (cache.Tables.Contains("GEO_ZONE") == false)
                {
                    DatabaseSchema.MakeTable_GeoZone(cache);
                }
            }
            bool processGeoZonePrimitive = newdata.Tables.Contains("GEO_ZONE_PRIMITIVE");
            if (processGeoZonePrimitive)
            {
                if (cache.Tables.Contains("GEO_ZONE_PRIMITIVE") == false)
                {
                    DatabaseSchema.MakeTable_GeoZonePrimitive(cache);
                }
            }
            bool processGoods = newdata.Tables.Contains("GOODS");
            if (processGoods)
            {
                if (cache.Tables.Contains("GOODS") == false)
                {
                    DatabaseSchema.MakeTable_Goods(cache);
                }
            }
            bool processGoodsLogisticOrder = newdata.Tables.Contains("GOODS_LOGISTIC_ORDER");
            if (processGoodsLogisticOrder)
            {
                if (cache.Tables.Contains("GOODS_LOGISTIC_ORDER") == false)
                {
                    DatabaseSchema.MakeTable_GoodsLogisticOrder(cache);
                }
            }
            bool processGoodsType = newdata.Tables.Contains("GOODS_TYPE");
            if (processGoodsType)
            {
                if (cache.Tables.Contains("GOODS_TYPE") == false)
                {
                    DatabaseSchema.MakeTable_GoodsType(cache);
                }
            }
            bool processGoodsTypeVehicle = newdata.Tables.Contains("GOODS_TYPE_VEHICLE");
            if (processGoodsTypeVehicle)
            {
                if (cache.Tables.Contains("GOODS_TYPE_VEHICLE") == false)
                {
                    DatabaseSchema.MakeTable_GoodsTypeVehicle(cache);
                }
            }
            bool processGraphic = newdata.Tables.Contains("GRAPHIC");
            if (processGraphic)
            {
                if (cache.Tables.Contains("GRAPHIC") == false)
                {
                    DatabaseSchema.MakeTable_Graphic(cache);
                }
            }
            bool processGraphicShift = newdata.Tables.Contains("GRAPHIC_SHIFT");
            if (processGraphicShift)
            {
                if (cache.Tables.Contains("GRAPHIC_SHIFT") == false)
                {
                    DatabaseSchema.MakeTable_GraphicShift(cache);
                }
            }
            bool processGuardEvents = newdata.Tables.Contains("GUARD_EVENTS");
            if (processGuardEvents)
            {
                if (cache.Tables.Contains("GUARD_EVENTS") == false)
                {
                    DatabaseSchema.MakeTable_GuardEvents(cache);
                }
            }
            bool processJobDelbadxy = newdata.Tables.Contains("Job_DelBadXY");
            if (processJobDelbadxy)
            {
                if (cache.Tables.Contains("Job_DelBadXY") == false)
                {
                    DatabaseSchema.MakeTable_JobDelbadxy(cache);
                }
            }
            bool processJournal = newdata.Tables.Contains("JOURNAL");
            if (processJournal)
            {
                if (cache.Tables.Contains("JOURNAL") == false)
                {
                    DatabaseSchema.MakeTable_Journal(cache);
                }
            }
            bool processJournalDay = newdata.Tables.Contains("JOURNAL_DAY");
            if (processJournalDay)
            {
                if (cache.Tables.Contains("JOURNAL_DAY") == false)
                {
                    DatabaseSchema.MakeTable_JournalDay(cache);
                }
            }
            bool processJournalDriver = newdata.Tables.Contains("JOURNAL_DRIVER");
            if (processJournalDriver)
            {
                if (cache.Tables.Contains("JOURNAL_DRIVER") == false)
                {
                    DatabaseSchema.MakeTable_JournalDriver(cache);
                }
            }
            bool processJournalVehicle = newdata.Tables.Contains("JOURNAL_VEHICLE");
            if (processJournalVehicle)
            {
                if (cache.Tables.Contains("JOURNAL_VEHICLE") == false)
                {
                    DatabaseSchema.MakeTable_JournalVehicle(cache);
                }
            }
            bool processLogOpsEvents = newdata.Tables.Contains("LOG_OPS_EVENTS");
            if (processLogOpsEvents)
            {
                if (cache.Tables.Contains("LOG_OPS_EVENTS") == false)
                {
                    DatabaseSchema.MakeTable_LogOpsEvents(cache);
                }
            }
            bool processLogVehicleStatus = newdata.Tables.Contains("LOG_VEHICLE_STATUS");
            if (processLogVehicleStatus)
            {
                if (cache.Tables.Contains("LOG_VEHICLE_STATUS") == false)
                {
                    DatabaseSchema.MakeTable_LogVehicleStatus(cache);
                }
            }
            bool processLogWaybillHeaderStatus = newdata.Tables.Contains("LOG_WAYBILL_HEADER_STATUS");
            if (processLogWaybillHeaderStatus)
            {
                if (cache.Tables.Contains("LOG_WAYBILL_HEADER_STATUS") == false)
                {
                    DatabaseSchema.MakeTable_LogWaybillHeaderStatus(cache);
                }
            }
            bool processLogisticAddress = newdata.Tables.Contains("LOGISTIC_ADDRESS");
            if (processLogisticAddress)
            {
                if (cache.Tables.Contains("LOGISTIC_ADDRESS") == false)
                {
                    DatabaseSchema.MakeTable_LogisticAddress(cache);
                }
            }
            bool processLogisticOrder = newdata.Tables.Contains("LOGISTIC_ORDER");
            if (processLogisticOrder)
            {
                if (cache.Tables.Contains("LOGISTIC_ORDER") == false)
                {
                    DatabaseSchema.MakeTable_LogisticOrder(cache);
                }
            }
            bool processMapVertex = newdata.Tables.Contains("MAP_VERTEX");
            if (processMapVertex)
            {
                if (cache.Tables.Contains("MAP_VERTEX") == false)
                {
                    DatabaseSchema.MakeTable_MapVertex(cache);
                }
            }
            bool processMaps = newdata.Tables.Contains("MAPS");
            if (processMaps)
            {
                if (cache.Tables.Contains("MAPS") == false)
                {
                    DatabaseSchema.MakeTable_Maps(cache);
                }
            }
            bool processMedia = newdata.Tables.Contains("MEDIA");
            if (processMedia)
            {
                if (cache.Tables.Contains("MEDIA") == false)
                {
                    DatabaseSchema.MakeTable_Media(cache);
                }
            }
            bool processMediaAcceptors = newdata.Tables.Contains("MEDIA_ACCEPTORS");
            if (processMediaAcceptors)
            {
                if (cache.Tables.Contains("MEDIA_ACCEPTORS") == false)
                {
                    DatabaseSchema.MakeTable_MediaAcceptors(cache);
                }
            }
            bool processMediaType = newdata.Tables.Contains("MEDIA_TYPE");
            if (processMediaType)
            {
                if (cache.Tables.Contains("MEDIA_TYPE") == false)
                {
                    DatabaseSchema.MakeTable_MediaType(cache);
                }
            }
            bool processMessage = newdata.Tables.Contains("MESSAGE");
            if (processMessage)
            {
                if (cache.Tables.Contains("MESSAGE") == false)
                {
                    DatabaseSchema.MakeTable_Message(cache);
                }
            }
            bool processMessageBoard = newdata.Tables.Contains("MESSAGE_BOARD");
            if (processMessageBoard)
            {
                if (cache.Tables.Contains("MESSAGE_BOARD") == false)
                {
                    DatabaseSchema.MakeTable_MessageBoard(cache);
                }
            }
            bool processMessageField = newdata.Tables.Contains("MESSAGE_FIELD");
            if (processMessageField)
            {
                if (cache.Tables.Contains("MESSAGE_FIELD") == false)
                {
                    DatabaseSchema.MakeTable_MessageField(cache);
                }
            }
            bool processMessageOperator = newdata.Tables.Contains("MESSAGE_OPERATOR");
            if (processMessageOperator)
            {
                if (cache.Tables.Contains("MESSAGE_OPERATOR") == false)
                {
                    DatabaseSchema.MakeTable_MessageOperator(cache);
                }
            }
            bool processMessageTemplate = newdata.Tables.Contains("MESSAGE_TEMPLATE");
            if (processMessageTemplate)
            {
                if (cache.Tables.Contains("MESSAGE_TEMPLATE") == false)
                {
                    DatabaseSchema.MakeTable_MessageTemplate(cache);
                }
            }
            bool processMessageTemplateField = newdata.Tables.Contains("MESSAGE_TEMPLATE_FIELD");
            if (processMessageTemplateField)
            {
                if (cache.Tables.Contains("MESSAGE_TEMPLATE_FIELD") == false)
                {
                    DatabaseSchema.MakeTable_MessageTemplateField(cache);
                }
            }
            bool processOperator = newdata.Tables.Contains("OPERATOR");
            if (processOperator)
            {
                if (cache.Tables.Contains("OPERATOR") == false)
                {
                    DatabaseSchema.MakeTable_Operator(cache);
                }
            }
            bool processOperatorDepartment = newdata.Tables.Contains("OPERATOR_DEPARTMENT");
            if (processOperatorDepartment)
            {
                if (cache.Tables.Contains("OPERATOR_DEPARTMENT") == false)
                {
                    DatabaseSchema.MakeTable_OperatorDepartment(cache);
                }
            }
            bool processOperatorDriver = newdata.Tables.Contains("OPERATOR_DRIVER");
            if (processOperatorDriver)
            {
                if (cache.Tables.Contains("OPERATOR_DRIVER") == false)
                {
                    DatabaseSchema.MakeTable_OperatorDriver(cache);
                }
            }
            bool processOperatorDrivergroup = newdata.Tables.Contains("OPERATOR_DRIVERGROUP");
            if (processOperatorDrivergroup)
            {
                if (cache.Tables.Contains("OPERATOR_DRIVERGROUP") == false)
                {
                    DatabaseSchema.MakeTable_OperatorDrivergroup(cache);
                }
            }
            bool processOperatorProfile = newdata.Tables.Contains("OPERATOR_PROFILE");
            if (processOperatorProfile)
            {
                if (cache.Tables.Contains("OPERATOR_PROFILE") == false)
                {
                    DatabaseSchema.MakeTable_OperatorProfile(cache);
                }
            }
            bool processOperatorReport = newdata.Tables.Contains("OPERATOR_REPORT");
            if (processOperatorReport)
            {
                if (cache.Tables.Contains("OPERATOR_REPORT") == false)
                {
                    DatabaseSchema.MakeTable_OperatorReport(cache);
                }
            }
            bool processOperatorRoute = newdata.Tables.Contains("OPERATOR_ROUTE");
            if (processOperatorRoute)
            {
                if (cache.Tables.Contains("OPERATOR_ROUTE") == false)
                {
                    DatabaseSchema.MakeTable_OperatorRoute(cache);
                }
            }
            bool processOperatorRoutegroup = newdata.Tables.Contains("OPERATOR_ROUTEGROUP");
            if (processOperatorRoutegroup)
            {
                if (cache.Tables.Contains("OPERATOR_ROUTEGROUP") == false)
                {
                    DatabaseSchema.MakeTable_OperatorRoutegroup(cache);
                }
            }
            bool processOperatorVehicle = newdata.Tables.Contains("OPERATOR_VEHICLE");
            if (processOperatorVehicle)
            {
                if (cache.Tables.Contains("OPERATOR_VEHICLE") == false)
                {
                    DatabaseSchema.MakeTable_OperatorVehicle(cache);
                }
            }
            bool processOperatorVehiclegroup = newdata.Tables.Contains("OPERATOR_VEHICLEGROUP");
            if (processOperatorVehiclegroup)
            {
                if (cache.Tables.Contains("OPERATOR_VEHICLEGROUP") == false)
                {
                    DatabaseSchema.MakeTable_OperatorVehiclegroup(cache);
                }
            }
            bool processOperatorZone = newdata.Tables.Contains("OPERATOR_ZONE");
            if (processOperatorZone)
            {
                if (cache.Tables.Contains("OPERATOR_ZONE") == false)
                {
                    DatabaseSchema.MakeTable_OperatorZone(cache);
                }
            }
            bool processOperatorZonegroup = newdata.Tables.Contains("OPERATOR_ZONEGROUP");
            if (processOperatorZonegroup)
            {
                if (cache.Tables.Contains("OPERATOR_ZONEGROUP") == false)
                {
                    DatabaseSchema.MakeTable_OperatorZonegroup(cache);
                }
            }
            bool processOperatorgroup = newdata.Tables.Contains("OPERATORGROUP");
            if (processOperatorgroup)
            {
                if (cache.Tables.Contains("OPERATORGROUP") == false)
                {
                    DatabaseSchema.MakeTable_Operatorgroup(cache);
                }
            }
            bool processOperatorgroupDepartment = newdata.Tables.Contains("OPERATORGROUP_DEPARTMENT");
            if (processOperatorgroupDepartment)
            {
                if (cache.Tables.Contains("OPERATORGROUP_DEPARTMENT") == false)
                {
                    DatabaseSchema.MakeTable_OperatorgroupDepartment(cache);
                }
            }
            bool processOperatorgroupDriver = newdata.Tables.Contains("OPERATORGROUP_DRIVER");
            if (processOperatorgroupDriver)
            {
                if (cache.Tables.Contains("OPERATORGROUP_DRIVER") == false)
                {
                    DatabaseSchema.MakeTable_OperatorgroupDriver(cache);
                }
            }
            bool processOperatorgroupDrivergroup = newdata.Tables.Contains("OPERATORGROUP_DRIVERGROUP");
            if (processOperatorgroupDrivergroup)
            {
                if (cache.Tables.Contains("OPERATORGROUP_DRIVERGROUP") == false)
                {
                    DatabaseSchema.MakeTable_OperatorgroupDrivergroup(cache);
                }
            }
            bool processOperatorgroupOperator = newdata.Tables.Contains("OPERATORGROUP_OPERATOR");
            if (processOperatorgroupOperator)
            {
                if (cache.Tables.Contains("OPERATORGROUP_OPERATOR") == false)
                {
                    DatabaseSchema.MakeTable_OperatorgroupOperator(cache);
                }
            }
            bool processOperatorgroupReport = newdata.Tables.Contains("OPERATORGROUP_REPORT");
            if (processOperatorgroupReport)
            {
                if (cache.Tables.Contains("OPERATORGROUP_REPORT") == false)
                {
                    DatabaseSchema.MakeTable_OperatorgroupReport(cache);
                }
            }
            bool processOperatorgroupRoute = newdata.Tables.Contains("OPERATORGROUP_ROUTE");
            if (processOperatorgroupRoute)
            {
                if (cache.Tables.Contains("OPERATORGROUP_ROUTE") == false)
                {
                    DatabaseSchema.MakeTable_OperatorgroupRoute(cache);
                }
            }
            bool processOperatorgroupRoutegroup = newdata.Tables.Contains("OPERATORGROUP_ROUTEGROUP");
            if (processOperatorgroupRoutegroup)
            {
                if (cache.Tables.Contains("OPERATORGROUP_ROUTEGROUP") == false)
                {
                    DatabaseSchema.MakeTable_OperatorgroupRoutegroup(cache);
                }
            }
            bool processOperatorgroupVehicle = newdata.Tables.Contains("OPERATORGROUP_VEHICLE");
            if (processOperatorgroupVehicle)
            {
                if (cache.Tables.Contains("OPERATORGROUP_VEHICLE") == false)
                {
                    DatabaseSchema.MakeTable_OperatorgroupVehicle(cache);
                }
            }
            bool processOperatorgroupVehiclegroup = newdata.Tables.Contains("OPERATORGROUP_VEHICLEGROUP");
            if (processOperatorgroupVehiclegroup)
            {
                if (cache.Tables.Contains("OPERATORGROUP_VEHICLEGROUP") == false)
                {
                    DatabaseSchema.MakeTable_OperatorgroupVehiclegroup(cache);
                }
            }
            bool processOperatorgroupZone = newdata.Tables.Contains("OPERATORGROUP_ZONE");
            if (processOperatorgroupZone)
            {
                if (cache.Tables.Contains("OPERATORGROUP_ZONE") == false)
                {
                    DatabaseSchema.MakeTable_OperatorgroupZone(cache);
                }
            }
            bool processOperatorgroupZonegroup = newdata.Tables.Contains("OPERATORGROUP_ZONEGROUP");
            if (processOperatorgroupZonegroup)
            {
                if (cache.Tables.Contains("OPERATORGROUP_ZONEGROUP") == false)
                {
                    DatabaseSchema.MakeTable_OperatorgroupZonegroup(cache);
                }
            }
            bool processOpsEvent = newdata.Tables.Contains("OPS_EVENT");
            if (processOpsEvent)
            {
                if (cache.Tables.Contains("OPS_EVENT") == false)
                {
                    DatabaseSchema.MakeTable_OpsEvent(cache);
                }
            }
            bool processOrder = newdata.Tables.Contains("ORDER");
            if (processOrder)
            {
                if (cache.Tables.Contains("ORDER") == false)
                {
                    DatabaseSchema.MakeTable_Order(cache);
                }
            }
            bool processOrderTrip = newdata.Tables.Contains("ORDER_TRIP");
            if (processOrderTrip)
            {
                if (cache.Tables.Contains("ORDER_TRIP") == false)
                {
                    DatabaseSchema.MakeTable_OrderTrip(cache);
                }
            }
            bool processOrderType = newdata.Tables.Contains("ORDER_TYPE");
            if (processOrderType)
            {
                if (cache.Tables.Contains("ORDER_TYPE") == false)
                {
                    DatabaseSchema.MakeTable_OrderType(cache);
                }
            }
            bool processOwner = newdata.Tables.Contains("OWNER");
            if (processOwner)
            {
                if (cache.Tables.Contains("OWNER") == false)
                {
                    DatabaseSchema.MakeTable_Owner(cache);
                }
            }
            bool processPeriod = newdata.Tables.Contains("PERIOD");
            if (processPeriod)
            {
                if (cache.Tables.Contains("PERIOD") == false)
                {
                    DatabaseSchema.MakeTable_Period(cache);
                }
            }
            bool processPoint = newdata.Tables.Contains("POINT");
            if (processPoint)
            {
                if (cache.Tables.Contains("POINT") == false)
                {
                    DatabaseSchema.MakeTable_Point(cache);
                }
            }
            bool processPointKind = newdata.Tables.Contains("POINT_KIND");
            if (processPointKind)
            {
                if (cache.Tables.Contains("POINT_KIND") == false)
                {
                    DatabaseSchema.MakeTable_PointKind(cache);
                }
            }
            bool processReport = newdata.Tables.Contains("REPORT");
            if (processReport)
            {
                if (cache.Tables.Contains("REPORT") == false)
                {
                    DatabaseSchema.MakeTable_Report(cache);
                }
            }
            bool processReprintReason = newdata.Tables.Contains("REPRINT_REASON");
            if (processReprintReason)
            {
                if (cache.Tables.Contains("REPRINT_REASON") == false)
                {
                    DatabaseSchema.MakeTable_ReprintReason(cache);
                }
            }
            bool processRight = newdata.Tables.Contains("RIGHT");
            if (processRight)
            {
                if (cache.Tables.Contains("RIGHT") == false)
                {
                    DatabaseSchema.MakeTable_Right(cache);
                }
            }
            bool processRightOperator = newdata.Tables.Contains("RIGHT_OPERATOR");
            if (processRightOperator)
            {
                if (cache.Tables.Contains("RIGHT_OPERATOR") == false)
                {
                    DatabaseSchema.MakeTable_RightOperator(cache);
                }
            }
            bool processRightOperatorgroup = newdata.Tables.Contains("RIGHT_OPERATORGROUP");
            if (processRightOperatorgroup)
            {
                if (cache.Tables.Contains("RIGHT_OPERATORGROUP") == false)
                {
                    DatabaseSchema.MakeTable_RightOperatorgroup(cache);
                }
            }
            bool processRoute = newdata.Tables.Contains("ROUTE");
            if (processRoute)
            {
                if (cache.Tables.Contains("ROUTE") == false)
                {
                    DatabaseSchema.MakeTable_Route(cache);
                }
            }
            bool processRouteGeozone = newdata.Tables.Contains("ROUTE_GEOZONE");
            if (processRouteGeozone)
            {
                if (cache.Tables.Contains("ROUTE_GEOZONE") == false)
                {
                    DatabaseSchema.MakeTable_RouteGeozone(cache);
                }
            }
            bool processRoutePoint = newdata.Tables.Contains("ROUTE_POINT");
            if (processRoutePoint)
            {
                if (cache.Tables.Contains("ROUTE_POINT") == false)
                {
                    DatabaseSchema.MakeTable_RoutePoint(cache);
                }
            }
            bool processRouteTrip = newdata.Tables.Contains("ROUTE_TRIP");
            if (processRouteTrip)
            {
                if (cache.Tables.Contains("ROUTE_TRIP") == false)
                {
                    DatabaseSchema.MakeTable_RouteTrip(cache);
                }
            }
            bool processRoutegroup = newdata.Tables.Contains("ROUTEGROUP");
            if (processRoutegroup)
            {
                if (cache.Tables.Contains("ROUTEGROUP") == false)
                {
                    DatabaseSchema.MakeTable_Routegroup(cache);
                }
            }
            bool processRoutegroupRoute = newdata.Tables.Contains("ROUTEGROUP_ROUTE");
            if (processRoutegroupRoute)
            {
                if (cache.Tables.Contains("ROUTEGROUP_ROUTE") == false)
                {
                    DatabaseSchema.MakeTable_RoutegroupRoute(cache);
                }
            }
            bool processRs = newdata.Tables.Contains("RS");
            if (processRs)
            {
                if (cache.Tables.Contains("RS") == false)
                {
                    DatabaseSchema.MakeTable_Rs(cache);
                }
            }
            bool processRsNumber = newdata.Tables.Contains("RS_NUMBER");
            if (processRsNumber)
            {
                if (cache.Tables.Contains("RS_NUMBER") == false)
                {
                    DatabaseSchema.MakeTable_RsNumber(cache);
                }
            }
            bool processRsOperationstype = newdata.Tables.Contains("RS_OPERATIONSTYPE");
            if (processRsOperationstype)
            {
                if (cache.Tables.Contains("RS_OPERATIONSTYPE") == false)
                {
                    DatabaseSchema.MakeTable_RsOperationstype(cache);
                }
            }
            bool processRsOperationtype = newdata.Tables.Contains("RS_OPERATIONTYPE");
            if (processRsOperationtype)
            {
                if (cache.Tables.Contains("RS_OPERATIONTYPE") == false)
                {
                    DatabaseSchema.MakeTable_RsOperationtype(cache);
                }
            }
            bool processRsPeriod = newdata.Tables.Contains("RS_PERIOD");
            if (processRsPeriod)
            {
                if (cache.Tables.Contains("RS_PERIOD") == false)
                {
                    DatabaseSchema.MakeTable_RsPeriod(cache);
                }
            }
            bool processRsPoint = newdata.Tables.Contains("RS_POINT");
            if (processRsPoint)
            {
                if (cache.Tables.Contains("RS_POINT") == false)
                {
                    DatabaseSchema.MakeTable_RsPoint(cache);
                }
            }
            bool processRsRoundTrip = newdata.Tables.Contains("RS_ROUND_TRIP");
            if (processRsRoundTrip)
            {
                if (cache.Tables.Contains("RS_ROUND_TRIP") == false)
                {
                    DatabaseSchema.MakeTable_RsRoundTrip(cache);
                }
            }
            bool processRsRuntime = newdata.Tables.Contains("RS_RUNTIME");
            if (processRsRuntime)
            {
                if (cache.Tables.Contains("RS_RUNTIME") == false)
                {
                    DatabaseSchema.MakeTable_RsRuntime(cache);
                }
            }
            bool processRsShift = newdata.Tables.Contains("RS_SHIFT");
            if (processRsShift)
            {
                if (cache.Tables.Contains("RS_SHIFT") == false)
                {
                    DatabaseSchema.MakeTable_RsShift(cache);
                }
            }
            bool processRsStep = newdata.Tables.Contains("RS_STEP");
            if (processRsStep)
            {
                if (cache.Tables.Contains("RS_STEP") == false)
                {
                    DatabaseSchema.MakeTable_RsStep(cache);
                }
            }
            bool processRsTrip = newdata.Tables.Contains("RS_TRIP");
            if (processRsTrip)
            {
                if (cache.Tables.Contains("RS_TRIP") == false)
                {
                    DatabaseSchema.MakeTable_RsTrip(cache);
                }
            }
            bool processRsTripstype = newdata.Tables.Contains("RS_TRIPSTYPE");
            if (processRsTripstype)
            {
                if (cache.Tables.Contains("RS_TRIPSTYPE") == false)
                {
                    DatabaseSchema.MakeTable_RsTripstype(cache);
                }
            }
            bool processRsTriptype = newdata.Tables.Contains("RS_TRIPTYPE");
            if (processRsTriptype)
            {
                if (cache.Tables.Contains("RS_TRIPTYPE") == false)
                {
                    DatabaseSchema.MakeTable_RsTriptype(cache);
                }
            }
            bool processRsType = newdata.Tables.Contains("RS_TYPE");
            if (processRsType)
            {
                if (cache.Tables.Contains("RS_TYPE") == false)
                {
                    DatabaseSchema.MakeTable_RsType(cache);
                }
            }
            bool processRsVariant = newdata.Tables.Contains("RS_VARIANT");
            if (processRsVariant)
            {
                if (cache.Tables.Contains("RS_VARIANT") == false)
                {
                    DatabaseSchema.MakeTable_RsVariant(cache);
                }
            }
            bool processRsWayout = newdata.Tables.Contains("RS_WAYOUT");
            if (processRsWayout)
            {
                if (cache.Tables.Contains("RS_WAYOUT") == false)
                {
                    DatabaseSchema.MakeTable_RsWayout(cache);
                }
            }
            bool processRule = newdata.Tables.Contains("RULE");
            if (processRule)
            {
                if (cache.Tables.Contains("RULE") == false)
                {
                    DatabaseSchema.MakeTable_Rule(cache);
                }
            }
            bool processSchedule = newdata.Tables.Contains("SCHEDULE");
            if (processSchedule)
            {
                if (cache.Tables.Contains("SCHEDULE") == false)
                {
                    DatabaseSchema.MakeTable_Schedule(cache);
                }
            }
            bool processScheduleDetail = newdata.Tables.Contains("SCHEDULE_DETAIL");
            if (processScheduleDetail)
            {
                if (cache.Tables.Contains("SCHEDULE_DETAIL") == false)
                {
                    DatabaseSchema.MakeTable_ScheduleDetail(cache);
                }
            }
            bool processScheduleDetailInfo = newdata.Tables.Contains("SCHEDULE_DETAIL_INFO");
            if (processScheduleDetailInfo)
            {
                if (cache.Tables.Contains("SCHEDULE_DETAIL_INFO") == false)
                {
                    DatabaseSchema.MakeTable_ScheduleDetailInfo(cache);
                }
            }
            bool processScheduleGeozone = newdata.Tables.Contains("SCHEDULE_GEOZONE");
            if (processScheduleGeozone)
            {
                if (cache.Tables.Contains("SCHEDULE_GEOZONE") == false)
                {
                    DatabaseSchema.MakeTable_ScheduleGeozone(cache);
                }
            }
            bool processSchedulePassage = newdata.Tables.Contains("SCHEDULE_PASSAGE");
            if (processSchedulePassage)
            {
                if (cache.Tables.Contains("SCHEDULE_PASSAGE") == false)
                {
                    DatabaseSchema.MakeTable_SchedulePassage(cache);
                }
            }
            bool processSchedulePoint = newdata.Tables.Contains("SCHEDULE_POINT");
            if (processSchedulePoint)
            {
                if (cache.Tables.Contains("SCHEDULE_POINT") == false)
                {
                    DatabaseSchema.MakeTable_SchedulePoint(cache);
                }
            }
            bool processSchedulerevent = newdata.Tables.Contains("SCHEDULEREVENT");
            if (processSchedulerevent)
            {
                if (cache.Tables.Contains("SCHEDULEREVENT") == false)
                {
                    DatabaseSchema.MakeTable_Schedulerevent(cache);
                }
            }
            bool processSchedulerqueue = newdata.Tables.Contains("SCHEDULERQUEUE");
            if (processSchedulerqueue)
            {
                if (cache.Tables.Contains("SCHEDULERQUEUE") == false)
                {
                    DatabaseSchema.MakeTable_Schedulerqueue(cache);
                }
            }
            bool processSeason = newdata.Tables.Contains("SEASON");
            if (processSeason)
            {
                if (cache.Tables.Contains("SEASON") == false)
                {
                    DatabaseSchema.MakeTable_Season(cache);
                }
            }
            bool processSeattype = newdata.Tables.Contains("SEATTYPE");
            if (processSeattype)
            {
                if (cache.Tables.Contains("SEATTYPE") == false)
                {
                    DatabaseSchema.MakeTable_Seattype(cache);
                }
            }
            bool processSession = newdata.Tables.Contains("SESSION");
            if (processSession)
            {
                if (cache.Tables.Contains("SESSION") == false)
                {
                    DatabaseSchema.MakeTable_Session(cache);
                }
            }
            bool processSmsType = newdata.Tables.Contains("SMS_TYPE");
            if (processSmsType)
            {
                if (cache.Tables.Contains("SMS_TYPE") == false)
                {
                    DatabaseSchema.MakeTable_SmsType(cache);
                }
            }
            bool processSysdiagrams = newdata.Tables.Contains("sysdiagrams");
            if (processSysdiagrams)
            {
                if (cache.Tables.Contains("sysdiagrams") == false)
                {
                    DatabaseSchema.MakeTable_Sysdiagrams(cache);
                }
            }
            bool processTaskProcessorLog = newdata.Tables.Contains("TASK_PROCESSOR_LOG");
            if (processTaskProcessorLog)
            {
                if (cache.Tables.Contains("TASK_PROCESSOR_LOG") == false)
                {
                    DatabaseSchema.MakeTable_TaskProcessorLog(cache);
                }
            }
            bool processTrail = newdata.Tables.Contains("TRAIL");
            if (processTrail)
            {
                if (cache.Tables.Contains("TRAIL") == false)
                {
                    DatabaseSchema.MakeTable_Trail(cache);
                }
            }
            bool processTrip = newdata.Tables.Contains("TRIP");
            if (processTrip)
            {
                if (cache.Tables.Contains("TRIP") == false)
                {
                    DatabaseSchema.MakeTable_Trip(cache);
                }
            }
            bool processTripKind = newdata.Tables.Contains("TRIP_KIND");
            if (processTripKind)
            {
                if (cache.Tables.Contains("TRIP_KIND") == false)
                {
                    DatabaseSchema.MakeTable_TripKind(cache);
                }
            }
            bool processVehicle = newdata.Tables.Contains("VEHICLE");
            if (processVehicle)
            {
                if (cache.Tables.Contains("VEHICLE") == false)
                {
                    DatabaseSchema.MakeTable_Vehicle(cache);
                }
            }
            bool processVehicleKind = newdata.Tables.Contains("VEHICLE_KIND");
            if (processVehicleKind)
            {
                if (cache.Tables.Contains("VEHICLE_KIND") == false)
                {
                    DatabaseSchema.MakeTable_VehicleKind(cache);
                }
            }
            bool processVehicleOwner = newdata.Tables.Contains("VEHICLE_OWNER");
            if (processVehicleOwner)
            {
                if (cache.Tables.Contains("VEHICLE_OWNER") == false)
                {
                    DatabaseSchema.MakeTable_VehicleOwner(cache);
                }
            }
            bool processVehiclePicture = newdata.Tables.Contains("VEHICLE_PICTURE");
            if (processVehiclePicture)
            {
                if (cache.Tables.Contains("VEHICLE_PICTURE") == false)
                {
                    DatabaseSchema.MakeTable_VehiclePicture(cache);
                }
            }
            bool processVehicleRule = newdata.Tables.Contains("VEHICLE_RULE");
            if (processVehicleRule)
            {
                if (cache.Tables.Contains("VEHICLE_RULE") == false)
                {
                    DatabaseSchema.MakeTable_VehicleRule(cache);
                }
            }
            bool processVehicleStatus = newdata.Tables.Contains("VEHICLE_STATUS");
            if (processVehicleStatus)
            {
                if (cache.Tables.Contains("VEHICLE_STATUS") == false)
                {
                    DatabaseSchema.MakeTable_VehicleStatus(cache);
                }
            }
            bool processVehiclegroup = newdata.Tables.Contains("VEHICLEGROUP");
            if (processVehiclegroup)
            {
                if (cache.Tables.Contains("VEHICLEGROUP") == false)
                {
                    DatabaseSchema.MakeTable_Vehiclegroup(cache);
                }
            }
            bool processVehiclegroupRule = newdata.Tables.Contains("VEHICLEGROUP_RULE");
            if (processVehiclegroupRule)
            {
                if (cache.Tables.Contains("VEHICLEGROUP_RULE") == false)
                {
                    DatabaseSchema.MakeTable_VehiclegroupRule(cache);
                }
            }
            bool processVehiclegroupVehicle = newdata.Tables.Contains("VEHICLEGROUP_VEHICLE");
            if (processVehiclegroupVehicle)
            {
                if (cache.Tables.Contains("VEHICLEGROUP_VEHICLE") == false)
                {
                    DatabaseSchema.MakeTable_VehiclegroupVehicle(cache);
                }
            }
            bool processWaybill = newdata.Tables.Contains("WAYBILL");
            if (processWaybill)
            {
                if (cache.Tables.Contains("WAYBILL") == false)
                {
                    DatabaseSchema.MakeTable_Waybill(cache);
                }
            }
            bool processWaybillHeader = newdata.Tables.Contains("WAYBILL_HEADER");
            if (processWaybillHeader)
            {
                if (cache.Tables.Contains("WAYBILL_HEADER") == false)
                {
                    DatabaseSchema.MakeTable_WaybillHeader(cache);
                }
            }
            bool processWaybillheaderWaybillmark = newdata.Tables.Contains("WAYBILLHEADER_WAYBILLMARK");
            if (processWaybillheaderWaybillmark)
            {
                if (cache.Tables.Contains("WAYBILLHEADER_WAYBILLMARK") == false)
                {
                    DatabaseSchema.MakeTable_WaybillheaderWaybillmark(cache);
                }
            }
            bool processWaybillmark = newdata.Tables.Contains("WAYBILLMARK");
            if (processWaybillmark)
            {
                if (cache.Tables.Contains("WAYBILLMARK") == false)
                {
                    DatabaseSchema.MakeTable_Waybillmark(cache);
                }
            }
            bool processWayout = newdata.Tables.Contains("WAYOUT");
            if (processWayout)
            {
                if (cache.Tables.Contains("WAYOUT") == false)
                {
                    DatabaseSchema.MakeTable_Wayout(cache);
                }
            }
            bool processWbTrip = newdata.Tables.Contains("WB_TRIP");
            if (processWbTrip)
            {
                if (cache.Tables.Contains("WB_TRIP") == false)
                {
                    DatabaseSchema.MakeTable_WbTrip(cache);
                }
            }
            bool processWebLine = newdata.Tables.Contains("WEB_LINE");
            if (processWebLine)
            {
                if (cache.Tables.Contains("WEB_LINE") == false)
                {
                    DatabaseSchema.MakeTable_WebLine(cache);
                }
            }
            bool processWebLineVertex = newdata.Tables.Contains("WEB_LINE_VERTEX");
            if (processWebLineVertex)
            {
                if (cache.Tables.Contains("WEB_LINE_VERTEX") == false)
                {
                    DatabaseSchema.MakeTable_WebLineVertex(cache);
                }
            }
            bool processWebPoint = newdata.Tables.Contains("WEB_POINT");
            if (processWebPoint)
            {
                if (cache.Tables.Contains("WEB_POINT") == false)
                {
                    DatabaseSchema.MakeTable_WebPoint(cache);
                }
            }
            bool processWebPointType = newdata.Tables.Contains("WEB_POINT_TYPE");
            if (processWebPointType)
            {
                if (cache.Tables.Contains("WEB_POINT_TYPE") == false)
                {
                    DatabaseSchema.MakeTable_WebPointType(cache);
                }
            }
            bool processWorkTime = newdata.Tables.Contains("WORK_TIME");
            if (processWorkTime)
            {
                if (cache.Tables.Contains("WORK_TIME") == false)
                {
                    DatabaseSchema.MakeTable_WorkTime(cache);
                }
            }
            bool processWorkplaceConfig = newdata.Tables.Contains("WORKPLACE_CONFIG");
            if (processWorkplaceConfig)
            {
                if (cache.Tables.Contains("WORKPLACE_CONFIG") == false)
                {
                    DatabaseSchema.MakeTable_WorkplaceConfig(cache);
                }
            }
            bool processWorkstation = newdata.Tables.Contains("WORKSTATION");
            if (processWorkstation)
            {
                if (cache.Tables.Contains("WORKSTATION") == false)
                {
                    DatabaseSchema.MakeTable_Workstation(cache);
                }
            }
            bool processWorktimeReason = newdata.Tables.Contains("WORKTIME_REASON");
            if (processWorktimeReason)
            {
                if (cache.Tables.Contains("WORKTIME_REASON") == false)
                {
                    DatabaseSchema.MakeTable_WorktimeReason(cache);
                }
            }
            bool processWorktimeStatusType = newdata.Tables.Contains("WORKTIME_STATUS_TYPE");
            if (processWorktimeStatusType)
            {
                if (cache.Tables.Contains("WORKTIME_STATUS_TYPE") == false)
                {
                    DatabaseSchema.MakeTable_WorktimeStatusType(cache);
                }
            }
            bool processZonePrimitive = newdata.Tables.Contains("ZONE_PRIMITIVE");
            if (processZonePrimitive)
            {
                if (cache.Tables.Contains("ZONE_PRIMITIVE") == false)
                {
                    DatabaseSchema.MakeTable_ZonePrimitive(cache);
                }
            }
            bool processZonePrimitiveVertex = newdata.Tables.Contains("ZONE_PRIMITIVE_VERTEX");
            if (processZonePrimitiveVertex)
            {
                if (cache.Tables.Contains("ZONE_PRIMITIVE_VERTEX") == false)
                {
                    DatabaseSchema.MakeTable_ZonePrimitiveVertex(cache);
                }
            }
            bool processZoneType = newdata.Tables.Contains("ZONE_TYPE");
            if (processZoneType)
            {
                if (cache.Tables.Contains("ZONE_TYPE") == false)
                {
                    DatabaseSchema.MakeTable_ZoneType(cache);
                }
            }
            bool processZoneVehicle = newdata.Tables.Contains("ZONE_VEHICLE");
            if (processZoneVehicle)
            {
                if (cache.Tables.Contains("ZONE_VEHICLE") == false)
                {
                    DatabaseSchema.MakeTable_ZoneVehicle(cache);
                }
            }
            bool processZoneVehiclegroup = newdata.Tables.Contains("ZONE_VEHICLEGROUP");
            if (processZoneVehiclegroup)
            {
                if (cache.Tables.Contains("ZONE_VEHICLEGROUP") == false)
                {
                    DatabaseSchema.MakeTable_ZoneVehiclegroup(cache);
                }
            }
            bool processZonegroup = newdata.Tables.Contains("ZONEGROUP");
            if (processZonegroup)
            {
                if (cache.Tables.Contains("ZONEGROUP") == false)
                {
                    DatabaseSchema.MakeTable_Zonegroup(cache);
                }
            }
            bool processZonegroupZone = newdata.Tables.Contains("ZONEGROUP_ZONE");
            if (processZonegroupZone)
            {
                if (cache.Tables.Contains("ZONEGROUP_ZONE") == false)
                {
                    DatabaseSchema.MakeTable_ZonegroupZone(cache);
                }
            }
            // Make schema once
            DatabaseSchema.MakeSchema(cache);
            // For each table with PK
            if (processBlading)
            {
                DataTable sourceTable = newdata.Tables["BLADING"];
                DataTable targetTable = cache.Tables["BLADING"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    BladingPrimaryKey pk;
                    pk.BladingId = (System.Int32)sourceRow["BLADING_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.BladingId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["BLADING_ID"] = sourceRow["BLADING_ID"];
                    targetRow["BLADING_TYPE_ID"] = sourceRow["BLADING_TYPE_ID"];
                    targetRow["RECEIVE_SUM"] = sourceRow["RECEIVE_SUM"];
                    targetRow["ACTUAL_SUM"] = sourceRow["ACTUAL_SUM"];
                    targetRow["STUFF_NUMBER"] = sourceRow["STUFF_NUMBER"];
                    targetRow["BILL_NUMBER"] = sourceRow["BILL_NUMBER"];
                    targetRow["TIME_IN"] = sourceRow["TIME_IN"];
                    targetRow["TIME_OUT"] = sourceRow["TIME_OUT"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["COMMENT"] = sourceRow["COMMENT"];
                    targetRow["DISPATCHER_COMMENT"] = sourceRow["DISPATCHER_COMMENT"];
                    targetRow["SUM"] = sourceRow["SUM"];
                    targetRow["CONTRACTOR_ID"] = sourceRow["CONTRACTOR_ID"];
                    targetRow["ORDER"] = sourceRow["ORDER"];
                    targetRow["WAYBILL_ID"] = sourceRow["WAYBILL_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processBladingType)
            {
                DataTable sourceTable = newdata.Tables["BLADING_TYPE"];
                DataTable targetTable = cache.Tables["BLADING_TYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    BladingTypePrimaryKey pk;
                    pk.BladingTypeId = (System.Int32)sourceRow["BLADING_TYPE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.BladingTypeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["BLADING_TYPE_ID"] = sourceRow["BLADING_TYPE_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processBrigade)
            {
                DataTable sourceTable = newdata.Tables["BRIGADE"];
                DataTable targetTable = cache.Tables["BRIGADE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    BrigadePrimaryKey pk;
                    pk.BrigadeId = (System.Int32)sourceRow["BRIGADE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.BrigadeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["BRIGADE_ID"] = sourceRow["BRIGADE_ID"];
                    targetRow["DECADE"] = sourceRow["DECADE"];
                    targetRow["VEHICLE"] = sourceRow["VEHICLE"];
                    targetRow["GRAPHIC"] = sourceRow["GRAPHIC"];
                    targetRow["GRAPHIC_BEGIN"] = sourceRow["GRAPHIC_BEGIN"];
                    targetRow["WAYOUT"] = sourceRow["WAYOUT"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processBrigadeDriver)
            {
                DataTable sourceTable = newdata.Tables["BRIGADE_DRIVER"];
                DataTable targetTable = cache.Tables["BRIGADE_DRIVER"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    BrigadeDriverPrimaryKey pk;
                    pk.BrigadeDriverId = (System.Int32)sourceRow["BRIGADE_DRIVER_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.BrigadeDriverId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["BRIGADE_DRIVER_ID"] = sourceRow["BRIGADE_DRIVER_ID"];
                    targetRow["BRIGADE"] = sourceRow["BRIGADE"];
                    targetRow["DRIVER"] = sourceRow["DRIVER"];
                    targetRow["DRIVER_NUMBER"] = sourceRow["DRIVER_NUMBER"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processBusstop)
            {
                DataTable sourceTable = newdata.Tables["BUSSTOP"];
                DataTable targetTable = cache.Tables["BUSSTOP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    BusstopPrimaryKey pk;
                    pk.BusstopId = (System.Int32)sourceRow["BUSSTOP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.BusstopId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["BUSSTOP_ID"] = sourceRow["BUSSTOP_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["SHORTNAME"] = sourceRow["SHORTNAME"];
                    targetRow["ADDRESS"] = sourceRow["ADDRESS"];
                    targetRow["DEPARTMENT_ID"] = sourceRow["DEPARTMENT_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCal)
            {
                DataTable sourceTable = newdata.Tables["CAL"];
                DataTable targetTable = cache.Tables["CAL"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    CalPrimaryKey pk;
                    pk.Date = (System.DateTime)sourceRow["DATE"];
                    DataRow targetRow = targetTable.Rows.Find(pk.Date);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["DATE"] = sourceRow["DATE"];
                    targetRow["DAY_KIND"] = sourceRow["DAY_KIND"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCalendarDay)
            {
                DataTable sourceTable = newdata.Tables["CALENDAR_DAY"];
                DataTable targetTable = cache.Tables["CALENDAR_DAY"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    CalendarDayPrimaryKey pk;
                    pk.CalendarDayId = (System.Int32)sourceRow["CALENDAR_DAY_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.CalendarDayId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["CALENDAR_DAY_ID"] = sourceRow["CALENDAR_DAY_ID"];
                    targetRow["DAY"] = sourceRow["DAY"];
                    targetRow["LOG_FROM"] = sourceRow["LOG_FROM"];
                    targetRow["LOG_TO"] = sourceRow["LOG_TO"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processConstants)
            {
                DataTable sourceTable = newdata.Tables["CONSTANTS"];
                DataTable targetTable = cache.Tables["CONSTANTS"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ConstantsPrimaryKey pk;
                    pk.Name = (System.String)sourceRow["NAME"];
                    DataRow targetRow = targetTable.Rows.Find(pk.Name);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["VALUE"] = sourceRow["VALUE"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processContractor)
            {
                DataTable sourceTable = newdata.Tables["CONTRACTOR"];
                DataTable targetTable = cache.Tables["CONTRACTOR"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ContractorPrimaryKey pk;
                    pk.ContractorId = (System.Int32)sourceRow["CONTRACTOR_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ContractorId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["CONTRACTOR_ID"] = sourceRow["CONTRACTOR_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["CONTACT_INFO"] = sourceRow["CONTACT_INFO"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processContractorCalendar)
            {
                DataTable sourceTable = newdata.Tables["CONTRACTOR_CALENDAR"];
                DataTable targetTable = cache.Tables["CONTRACTOR_CALENDAR"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ContractorCalendarPrimaryKey pk;
                    pk.ContractorCalendarId = (System.Int32)sourceRow["CONTRACTOR_CALENDAR_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ContractorCalendarId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["CONTRACTOR_CALENDAR_ID"] = sourceRow["CONTRACTOR_CALENDAR_ID"];
                    targetRow["GOODS_ACCEPT_TIME_FROM"] = sourceRow["GOODS_ACCEPT_TIME_FROM"];
                    targetRow["GOODS_ACCEPT_TIME_TO"] = sourceRow["GOODS_ACCEPT_TIME_TO"];
                    targetRow["PAYMENT_TIME_FROM"] = sourceRow["PAYMENT_TIME_FROM"];
                    targetRow["PAYMENT_TIME_TO"] = sourceRow["PAYMENT_TIME_TO"];
                    targetRow["CONTRACTOR_CALENDAR_DAY_TYPE"] = sourceRow["CONTRACTOR_CALENDAR_DAY_TYPE"];
                    targetRow["DINNER_BREAK_FROM"] = sourceRow["DINNER_BREAK_FROM"];
                    targetRow["DINNER_BREAK_TO"] = sourceRow["DINNER_BREAK_TO"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processController)
            {
                DataTable sourceTable = newdata.Tables["CONTROLLER"];
                DataTable targetTable = cache.Tables["CONTROLLER"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ControllerPrimaryKey pk;
                    pk.ControllerId = (System.Int32)sourceRow["CONTROLLER_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ControllerId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["CONTROLLER_ID"] = sourceRow["CONTROLLER_ID"];
                    targetRow["CONTROLLER_TYPE_ID"] = sourceRow["CONTROLLER_TYPE_ID"];
                    targetRow["VEHICLE_ID"] = sourceRow["VEHICLE_ID"];
                    targetRow["PHONE"] = sourceRow["PHONE"];
                    targetRow["NUMBER"] = sourceRow["NUMBER"];
                    targetRow["FIRMWARE"] = sourceRow["FIRMWARE"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processControllerInfo)
            {
                DataTable sourceTable = newdata.Tables["CONTROLLER_INFO"];
                DataTable targetTable = cache.Tables["CONTROLLER_INFO"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ControllerInfoPrimaryKey pk;
                    pk.ControllerId = (System.Int32)sourceRow["CONTROLLER_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ControllerId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["CONTROLLER_ID"] = sourceRow["CONTROLLER_ID"];
                    targetRow["TIME"] = sourceRow["TIME"];
                    targetRow["DEVICE_ID"] = sourceRow["DEVICE_ID"];
                    targetRow["FIRMWARE"] = sourceRow["FIRMWARE"];
                    targetRow["FUEL_LVAL"] = sourceRow["FUEL_LVAL"];
                    targetRow["FUEL_UVAL"] = sourceRow["FUEL_UVAL"];
                    targetRow["FUEL_FACTOR"] = sourceRow["FUEL_FACTOR"];
                    targetRow["SERVER_IP"] = sourceRow["SERVER_IP"];
                    targetRow["SERVER_PORT"] = sourceRow["SERVER_PORT"];
                    targetRow["TIME_ZONE"] = sourceRow["TIME_ZONE"];
                    targetRow["VOICE_PHONE"] = sourceRow["VOICE_PHONE"];
                    targetRow["VOICE_PHONE1"] = sourceRow["VOICE_PHONE1"];
                    targetRow["VOICE_PHONE2"] = sourceRow["VOICE_PHONE2"];
                    targetRow["DATA_PHONE"] = sourceRow["DATA_PHONE"];
                    targetRow["DATA_PHONE1"] = sourceRow["DATA_PHONE1"];
                    targetRow["INTERVAL"] = sourceRow["INTERVAL"];
                    targetRow["TCP"] = sourceRow["TCP"];
                    targetRow["INET_ENTRY_POINT"] = sourceRow["INET_ENTRY_POINT"];
                    targetRow["LOGIN"] = sourceRow["LOGIN"];
                    targetRow["PASSWORD"] = sourceRow["PASSWORD"];
                    targetRow["FUEL_SENSOR_DIRECTION"] = sourceRow["FUEL_SENSOR_DIRECTION"];
                    targetRow["EXTRA"] = sourceRow["EXTRA"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processControllerSensor)
            {
                DataTable sourceTable = newdata.Tables["CONTROLLER_SENSOR"];
                DataTable targetTable = cache.Tables["CONTROLLER_SENSOR"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ControllerSensorPrimaryKey pk;
                    pk.ControllerSensorId = (System.Int32)sourceRow["CONTROLLER_SENSOR_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ControllerSensorId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["CONTROLLER_SENSOR_ID"] = sourceRow["CONTROLLER_SENSOR_ID"];
                    targetRow["CONTROLLER_TYPE_ID"] = sourceRow["CONTROLLER_TYPE_ID"];
                    targetRow["CONTROLLER_SENSOR_TYPE_ID"] = sourceRow["CONTROLLER_SENSOR_TYPE_ID"];
                    targetRow["NUMBER"] = sourceRow["NUMBER"];
                    targetRow["MAX_VALUE"] = sourceRow["MAX_VALUE"];
                    targetRow["MIN_VALUE"] = sourceRow["MIN_VALUE"];
                    targetRow["DEFAULT_VALUE"] = sourceRow["DEFAULT_VALUE"];
                    targetRow["BITS"] = sourceRow["BITS"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processControllerSensorLegend)
            {
                DataTable sourceTable = newdata.Tables["CONTROLLER_SENSOR_LEGEND"];
                DataTable targetTable = cache.Tables["CONTROLLER_SENSOR_LEGEND"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ControllerSensorLegendPrimaryKey pk;
                    pk.ControllerSensorLegendId = (System.Int32)sourceRow["CONTROLLER_SENSOR_LEGEND_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ControllerSensorLegendId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["CONTROLLER_SENSOR_LEGEND_ID"] = sourceRow["CONTROLLER_SENSOR_LEGEND_ID"];
                    targetRow["CONTROLLER_SENSOR_TYPE_ID"] = sourceRow["CONTROLLER_SENSOR_TYPE_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processControllerSensorMap)
            {
                DataTable sourceTable = newdata.Tables["CONTROLLER_SENSOR_MAP"];
                DataTable targetTable = cache.Tables["CONTROLLER_SENSOR_MAP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ControllerSensorMapPrimaryKey pk;
                    pk.ControllerSensorMapId = (System.Int32)sourceRow["CONTROLLER_SENSOR_MAP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ControllerSensorMapId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["CONTROLLER_SENSOR_MAP_ID"] = sourceRow["CONTROLLER_SENSOR_MAP_ID"];
                    targetRow["CONTROLLER_ID"] = sourceRow["CONTROLLER_ID"];
                    targetRow["CONTROLLER_SENSOR_ID"] = sourceRow["CONTROLLER_SENSOR_ID"];
                    targetRow["CONTROLLER_SENSOR_LEGEND_ID"] = sourceRow["CONTROLLER_SENSOR_LEGEND_ID"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processControllerSensorType)
            {
                DataTable sourceTable = newdata.Tables["CONTROLLER_SENSOR_TYPE"];
                DataTable targetTable = cache.Tables["CONTROLLER_SENSOR_TYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ControllerSensorTypePrimaryKey pk;
                    pk.ControllerSensorTypeId = (System.Int32)sourceRow["CONTROLLER_SENSOR_TYPE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ControllerSensorTypeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["CONTROLLER_SENSOR_TYPE_ID"] = sourceRow["CONTROLLER_SENSOR_TYPE_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processControllerTime)
            {
                DataTable sourceTable = newdata.Tables["CONTROLLER_TIME"];
                DataTable targetTable = cache.Tables["CONTROLLER_TIME"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ControllerTimePrimaryKey pk;
                    pk.ControllerTimeId = (System.Int32)sourceRow["CONTROLLER_TIME_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ControllerTimeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["CONTROLLER_ID"] = sourceRow["CONTROLLER_ID"];
                    targetRow["TIME_BEGIN"] = sourceRow["TIME_BEGIN"];
                    targetRow["TIME_END"] = sourceRow["TIME_END"];
                    targetRow["CONTROLLER_TIME_ID"] = sourceRow["CONTROLLER_TIME_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processControllerType)
            {
                DataTable sourceTable = newdata.Tables["CONTROLLER_TYPE"];
                DataTable targetTable = cache.Tables["CONTROLLER_TYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ControllerTypePrimaryKey pk;
                    pk.ControllerTypeId = (System.Int32)sourceRow["CONTROLLER_TYPE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ControllerTypeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["CONTROLLER_TYPE_ID"] = sourceRow["CONTROLLER_TYPE_ID"];
                    targetRow["TYPE_NAME"] = sourceRow["TYPE_NAME"];
                    targetRow["PACKET_LENGTH"] = sourceRow["PACKET_LENGTH"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCountGprs)
            {
                DataTable sourceTable = newdata.Tables["COUNT_GPRS"];
                DataTable targetTable = cache.Tables["COUNT_GPRS"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    CountGprsPrimaryKey pk;
                    pk.TimeBegin = (System.DateTime)sourceRow["TIME_BEGIN"];
                    pk.Duration = (System.Int32)sourceRow["DURATION"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.TimeBegin, pk.Duration});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["TIME_BEGIN"] = sourceRow["TIME_BEGIN"];
                    targetRow["DURATION"] = sourceRow["DURATION"];
                    targetRow["BYTES"] = sourceRow["BYTES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCountSms)
            {
                DataTable sourceTable = newdata.Tables["COUNT_SMS"];
                DataTable targetTable = cache.Tables["COUNT_SMS"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    CountSmsPrimaryKey pk;
                    pk.TimeBegin = (System.DateTime)sourceRow["TIME_BEGIN"];
                    pk.Duration = (System.Int32)sourceRow["DURATION"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.TimeBegin, pk.Duration});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["TIME_BEGIN"] = sourceRow["TIME_BEGIN"];
                    targetRow["DURATION"] = sourceRow["DURATION"];
                    targetRow["MESSAGES"] = sourceRow["MESSAGES"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processCustomer)
            {
                DataTable sourceTable = newdata.Tables["CUSTOMER"];
                DataTable targetTable = cache.Tables["CUSTOMER"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    CustomerPrimaryKey pk;
                    pk.CustomerId = (System.Int32)sourceRow["CUSTOMER_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.CustomerId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["CUSTOMER_ID"] = sourceRow["CUSTOMER_ID"];
                    targetRow["EXT_NUMBER"] = sourceRow["EXT_NUMBER"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["SHORT_NAME"] = sourceRow["SHORT_NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processDay)
            {
                DataTable sourceTable = newdata.Tables["DAY"];
                DataTable targetTable = cache.Tables["DAY"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    DayPrimaryKey pk;
                    pk.DayId = (System.Int32)sourceRow["DAY_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.DayId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["DAY_ID"] = sourceRow["DAY_ID"];
                    targetRow["DATE"] = sourceRow["DATE"];
                    targetRow["TEMPERATURE"] = sourceRow["TEMPERATURE"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processDayKind)
            {
                DataTable sourceTable = newdata.Tables["DAY_KIND"];
                DataTable targetTable = cache.Tables["DAY_KIND"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    DayKindPrimaryKey pk;
                    pk.DayKindId = (System.Int32)sourceRow["DAY_KIND_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.DayKindId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["DAY_KIND_ID"] = sourceRow["DAY_KIND_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["DK_SET"] = sourceRow["DK_SET"];
                    targetRow["WEEKDAYS"] = sourceRow["WEEKDAYS"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processDayTime)
            {
                DataTable sourceTable = newdata.Tables["DAY_TIME"];
                DataTable targetTable = cache.Tables["DAY_TIME"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    DayTimePrimaryKey pk;
                    pk.DayTimeId = (System.Int32)sourceRow["DAY_TIME_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.DayTimeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["DAY_TIME_ID"] = sourceRow["DAY_TIME_ID"];
                    targetRow["TIME_BEGIN"] = sourceRow["TIME_BEGIN"];
                    targetRow["TIME_END"] = sourceRow["TIME_END"];
                    targetRow["DAY_TYPE_ID"] = sourceRow["DAY_TYPE_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processDayType)
            {
                DataTable sourceTable = newdata.Tables["DAY_TYPE"];
                DataTable targetTable = cache.Tables["DAY_TYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    DayTypePrimaryKey pk;
                    pk.DayTypeId = (System.Int32)sourceRow["DAY_TYPE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.DayTypeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["DAY_TYPE_ID"] = sourceRow["DAY_TYPE_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["WEEKDAYS"] = sourceRow["WEEKDAYS"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processDecade)
            {
                DataTable sourceTable = newdata.Tables["DECADE"];
                DataTable targetTable = cache.Tables["DECADE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    DecadePrimaryKey pk;
                    pk.DecadeId = (System.Int32)sourceRow["DECADE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.DecadeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["DECADE_ID"] = sourceRow["DECADE_ID"];
                    targetRow["DEPARTMENT"] = sourceRow["DEPARTMENT"];
                    targetRow["BEGIN"] = sourceRow["BEGIN"];
                    targetRow["END"] = sourceRow["END"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processDepartment)
            {
                DataTable sourceTable = newdata.Tables["DEPARTMENT"];
                DataTable targetTable = cache.Tables["DEPARTMENT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    DepartmentPrimaryKey pk;
                    pk.DepartmentId = (System.Int32)sourceRow["DEPARTMENT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.DepartmentId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["DEPARTMENT_ID"] = sourceRow["DEPARTMENT_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processDkSet)
            {
                DataTable sourceTable = newdata.Tables["DK_SET"];
                DataTable targetTable = cache.Tables["DK_SET"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    DkSetPrimaryKey pk;
                    pk.DkSetId = (System.Int32)sourceRow["DK_SET_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.DkSetId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["DK_SET_ID"] = sourceRow["DK_SET_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["COMMENT"] = sourceRow["COMMENT"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processDotnetType)
            {
                DataTable sourceTable = newdata.Tables["DOTNET_TYPE"];
                DataTable targetTable = cache.Tables["DOTNET_TYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    DotnetTypePrimaryKey pk;
                    pk.DotnetTypeId = (System.Int32)sourceRow["DOTNET_TYPE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.DotnetTypeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["DOTNET_TYPE_ID"] = sourceRow["DOTNET_TYPE_ID"];
                    targetRow["TYPE_NAME"] = sourceRow["TYPE_NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processDriver)
            {
                DataTable sourceTable = newdata.Tables["DRIVER"];
                DataTable targetTable = cache.Tables["DRIVER"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    DriverPrimaryKey pk;
                    pk.DriverId = (System.Int32)sourceRow["DRIVER_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.DriverId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["DRIVER_ID"] = sourceRow["DRIVER_ID"];
                    targetRow["EXT_NUMBER"] = sourceRow["EXT_NUMBER"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["SHORT_NAME"] = sourceRow["SHORT_NAME"];
                    targetRow["DATE_OF_BIRTH"] = sourceRow["DATE_OF_BIRTH"];
                    targetRow["COMMENT"] = sourceRow["COMMENT"];
                    targetRow["CITY_ONLY"] = sourceRow["CITY_ONLY"];
                    targetRow["DRIVER_STATUS_ID"] = sourceRow["DRIVER_STATUS_ID"];
                    targetRow["FIRED_DATE"] = sourceRow["FIRED_DATE"];
                    targetRow["DEPARTMENT"] = sourceRow["DEPARTMENT"];
                    targetRow["PHONE"] = sourceRow["PHONE"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processDriverBonus)
            {
                DataTable sourceTable = newdata.Tables["DRIVER_BONUS"];
                DataTable targetTable = cache.Tables["DRIVER_BONUS"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    DriverBonusPrimaryKey pk;
                    pk.DriverBonusId = (System.Int32)sourceRow["driver_bonus_id"];
                    DataRow targetRow = targetTable.Rows.Find(pk.DriverBonusId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["driver_bonus_id"] = sourceRow["driver_bonus_id"];
                    targetRow["time_delta"] = sourceRow["time_delta"];
                    targetRow["bonus_percentage"] = sourceRow["bonus_percentage"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processDriverMsgTemplate)
            {
                DataTable sourceTable = newdata.Tables["DRIVER_MSG_TEMPLATE"];
                DataTable targetTable = cache.Tables["DRIVER_MSG_TEMPLATE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    DriverMsgTemplatePrimaryKey pk;
                    pk.DriverMsgTemplateId = (System.Int32)sourceRow["DRIVER_MSG_TEMPLATE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.DriverMsgTemplateId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["DRIVER_MSG_TEMPLATE_ID"] = sourceRow["DRIVER_MSG_TEMPLATE_ID"];
                    targetRow["MESSAGE"] = sourceRow["MESSAGE"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processDriverStatus)
            {
                DataTable sourceTable = newdata.Tables["DRIVER_STATUS"];
                DataTable targetTable = cache.Tables["DRIVER_STATUS"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    DriverStatusPrimaryKey pk;
                    pk.DriverStatusId = (System.Int32)sourceRow["DRIVER_STATUS_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.DriverStatusId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["DRIVER_STATUS_ID"] = sourceRow["DRIVER_STATUS_ID"];
                    targetRow["DRIVER_STATUS_NAME"] = sourceRow["DRIVER_STATUS_NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processDriverVehicle)
            {
                DataTable sourceTable = newdata.Tables["DRIVER_VEHICLE"];
                DataTable targetTable = cache.Tables["DRIVER_VEHICLE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    DriverVehiclePrimaryKey pk;
                    pk.DriverId = (System.Int32)sourceRow["DRIVER_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.DriverId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["DRIVER_ID"] = sourceRow["DRIVER_ID"];
                    targetRow["VEHICLE"] = sourceRow["VEHICLE"];
                    targetRow["DRIVER_NUMBER"] = sourceRow["DRIVER_NUMBER"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processDrivergroup)
            {
                DataTable sourceTable = newdata.Tables["DRIVERGROUP"];
                DataTable targetTable = cache.Tables["DRIVERGROUP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    DrivergroupPrimaryKey pk;
                    pk.DrivergroupId = (System.Int32)sourceRow["DRIVERGROUP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.DrivergroupId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["DRIVERGROUP_ID"] = sourceRow["DRIVERGROUP_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["COMMENT"] = sourceRow["COMMENT"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processDrivergroupDriver)
            {
                DataTable sourceTable = newdata.Tables["DRIVERGROUP_DRIVER"];
                DataTable targetTable = cache.Tables["DRIVERGROUP_DRIVER"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    DrivergroupDriverPrimaryKey pk;
                    pk.DrivergroupId = (System.Int32)sourceRow["DRIVERGROUP_ID"];
                    pk.DriverId = (System.Int32)sourceRow["DRIVER_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.DrivergroupId, pk.DriverId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["DRIVERGROUP_ID"] = sourceRow["DRIVERGROUP_ID"];
                    targetRow["DRIVER_ID"] = sourceRow["DRIVER_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processEmail)
            {
                DataTable sourceTable = newdata.Tables["EMAIL"];
                DataTable targetTable = cache.Tables["EMAIL"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    EmailPrimaryKey pk;
                    pk.EmailId = (System.Int32)sourceRow["EMAIL_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.EmailId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["EMAIL_ID"] = sourceRow["EMAIL_ID"];
                    targetRow["EMAIL"] = sourceRow["EMAIL"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["COMMENT"] = sourceRow["COMMENT"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processEmailSchedulerevent)
            {
                DataTable sourceTable = newdata.Tables["EMAIL_SCHEDULEREVENT"];
                DataTable targetTable = cache.Tables["EMAIL_SCHEDULEREVENT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    EmailSchedulereventPrimaryKey pk;
                    pk.SchedulereventId = (System.Int32)sourceRow["SCHEDULEREVENT_ID"];
                    pk.EmailId = (System.Int32)sourceRow["EMAIL_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.SchedulereventId, pk.EmailId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["SCHEDULEREVENT_ID"] = sourceRow["SCHEDULEREVENT_ID"];
                    targetRow["EMAIL_ID"] = sourceRow["EMAIL_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processEmailSchedulerqueue)
            {
                DataTable sourceTable = newdata.Tables["EMAIL_SCHEDULERQUEUE"];
                DataTable targetTable = cache.Tables["EMAIL_SCHEDULERQUEUE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    EmailSchedulerqueuePrimaryKey pk;
                    pk.SchedulerqueueId = (System.Int32)sourceRow["SCHEDULERQUEUE_ID"];
                    pk.EmailId = (System.Int32)sourceRow["EMAIL_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.SchedulerqueueId, pk.EmailId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["SCHEDULERQUEUE_ID"] = sourceRow["SCHEDULERQUEUE_ID"];
                    targetRow["EMAIL_ID"] = sourceRow["EMAIL_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processFactorValues)
            {
                DataTable sourceTable = newdata.Tables["FACTOR_VALUES"];
                DataTable targetTable = cache.Tables["FACTOR_VALUES"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    FactorValuesPrimaryKey pk;
                    pk.FactorValuesId = (System.Int32)sourceRow["FACTOR_VALUES_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.FactorValuesId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["FACTOR_VALUES_ID"] = sourceRow["FACTOR_VALUES_ID"];
                    targetRow["FACTOR"] = sourceRow["FACTOR"];
                    targetRow["RUNTIME"] = sourceRow["RUNTIME"];
                    targetRow["SEASON_ID"] = sourceRow["SEASON_ID"];
                    targetRow["DAY_TYPE_ID"] = sourceRow["DAY_TYPE_ID"];
                    targetRow["DAY_TIME_ID"] = sourceRow["DAY_TIME_ID"];
                    targetRow["FACTOR_ID"] = sourceRow["FACTOR_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processFactors)
            {
                DataTable sourceTable = newdata.Tables["FACTORS"];
                DataTable targetTable = cache.Tables["FACTORS"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    FactorsPrimaryKey pk;
                    pk.FactorId = (System.Int32)sourceRow["FACTOR_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.FactorId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["FACTOR_ID"] = sourceRow["FACTOR_ID"];
                    targetRow["COMMENTS"] = sourceRow["COMMENTS"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processGeoSegment)
            {
                DataTable sourceTable = newdata.Tables["GEO_SEGMENT"];
                DataTable targetTable = cache.Tables["GEO_SEGMENT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    GeoSegmentPrimaryKey pk;
                    pk.GeoSegmentId = (System.Int32)sourceRow["GEO_SEGMENT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.GeoSegmentId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["GEO_SEGMENT_ID"] = sourceRow["GEO_SEGMENT_ID"];
                    targetRow["GEO_TRIP_ID"] = sourceRow["GEO_TRIP_ID"];
                    targetRow["LENGTH"] = sourceRow["LENGTH"];
                    targetRow["RUNTIME"] = sourceRow["RUNTIME"];
                    targetRow["STOP_TIME"] = sourceRow["STOP_TIME"];
                    targetRow["ORDER"] = sourceRow["ORDER"];
                    targetRow["POINT"] = sourceRow["POINT"];
                    targetRow["VERTEX"] = sourceRow["VERTEX"];
                    targetRow["FACTOR_ID"] = sourceRow["FACTOR_ID"];
                    targetRow["STATUS"] = sourceRow["STATUS"];
                    targetRow["POINTTO"] = sourceRow["POINTTO"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processGeoSegmentRuntime)
            {
                DataTable sourceTable = newdata.Tables["GEO_SEGMENT_RUNTIME"];
                DataTable targetTable = cache.Tables["GEO_SEGMENT_RUNTIME"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    GeoSegmentRuntimePrimaryKey pk;
                    pk.GeoSegmentRuntimeId = (System.Int32)sourceRow["GEO_SEGMENT_RUNTIME_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.GeoSegmentRuntimeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["GEO_SEGMENT_RUNTIME_ID"] = sourceRow["GEO_SEGMENT_RUNTIME_ID"];
                    targetRow["GEO_SEGMENT_ID"] = sourceRow["GEO_SEGMENT_ID"];
                    targetRow["DAY_TIME_ID"] = sourceRow["DAY_TIME_ID"];
                    targetRow["RUNTIME"] = sourceRow["RUNTIME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processGeoSegmentVertex)
            {
                DataTable sourceTable = newdata.Tables["GEO_SEGMENT_VERTEX"];
                DataTable targetTable = cache.Tables["GEO_SEGMENT_VERTEX"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    GeoSegmentVertexPrimaryKey pk;
                    pk.GeoSegmentVertexId = (System.Int32)sourceRow["GEO_SEGMENT_VERTEX_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.GeoSegmentVertexId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["GEO_SEGMENT_ID"] = sourceRow["GEO_SEGMENT_ID"];
                    targetRow["VERTEX_ID"] = sourceRow["VERTEX_ID"];
                    targetRow["ORDER_GS"] = sourceRow["ORDER_GS"];
                    targetRow["DIRECT"] = sourceRow["DIRECT"];
                    targetRow["GEO_TRIP_ID"] = sourceRow["GEO_TRIP_ID"];
                    targetRow["ORDER_GT"] = sourceRow["ORDER_GT"];
                    targetRow["GEO_SEGMENT_VERTEX_ID"] = sourceRow["GEO_SEGMENT_VERTEX_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processGeoTrip)
            {
                DataTable sourceTable = newdata.Tables["GEO_TRIP"];
                DataTable targetTable = cache.Tables["GEO_TRIP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    GeoTripPrimaryKey pk;
                    pk.GeoTripId = (System.Int32)sourceRow["GEO_TRIP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.GeoTripId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["GEO_TRIP_ID"] = sourceRow["GEO_TRIP_ID"];
                    targetRow["LENGTH"] = sourceRow["LENGTH"];
                    targetRow["POINTA"] = sourceRow["POINTA"];
                    targetRow["POINTB"] = sourceRow["POINTB"];
                    targetRow["COMMENTS"] = sourceRow["COMMENTS"];
                    targetRow["ROUTE_ID"] = sourceRow["ROUTE_ID"];
                    targetRow["RUNTIME"] = sourceRow["RUNTIME"];
                    targetRow["CREATOR_OPERATOR_ID"] = sourceRow["CREATOR_OPERATOR_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processGeoZone)
            {
                DataTable sourceTable = newdata.Tables["GEO_ZONE"];
                DataTable targetTable = cache.Tables["GEO_ZONE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    GeoZonePrimaryKey pk;
                    pk.ZoneId = (System.Int32)sourceRow["ZONE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ZoneId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ZONE_ID"] = sourceRow["ZONE_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["COLOR"] = sourceRow["COLOR"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processGeoZonePrimitive)
            {
                DataTable sourceTable = newdata.Tables["GEO_ZONE_PRIMITIVE"];
                DataTable targetTable = cache.Tables["GEO_ZONE_PRIMITIVE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    GeoZonePrimitivePrimaryKey pk;
                    pk.GeoZonePrimitiveId = (System.Int32)sourceRow["GEO_ZONE_PRIMITIVE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.GeoZonePrimitiveId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["GEO_ZONE_PRIMITIVE_ID"] = sourceRow["GEO_ZONE_PRIMITIVE_ID"];
                    targetRow["ZONE_ID"] = sourceRow["ZONE_ID"];
                    targetRow["PRIMITIVE_ID"] = sourceRow["PRIMITIVE_ID"];
                    targetRow["ORDER"] = sourceRow["ORDER"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processGoods)
            {
                DataTable sourceTable = newdata.Tables["GOODS"];
                DataTable targetTable = cache.Tables["GOODS"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    GoodsPrimaryKey pk;
                    pk.GoodsId = (System.Int32)sourceRow["GOODS_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.GoodsId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["GOODS_ID"] = sourceRow["GOODS_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["PROPERTIES"] = sourceRow["PROPERTIES"];
                    targetRow["COMMENT"] = sourceRow["COMMENT"];
                    targetRow["GOODS_TYPE_ID"] = sourceRow["GOODS_TYPE_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processGoodsLogisticOrder)
            {
                DataTable sourceTable = newdata.Tables["GOODS_LOGISTIC_ORDER"];
                DataTable targetTable = cache.Tables["GOODS_LOGISTIC_ORDER"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    GoodsLogisticOrderPrimaryKey pk;
                    pk.GoodsLogisticOrderId = (System.Int32)sourceRow["GOODS_LOGISTIC_ORDER_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.GoodsLogisticOrderId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["GOODS_LOGISTIC_ORDER_ID"] = sourceRow["GOODS_LOGISTIC_ORDER_ID"];
                    targetRow["LOGISTIC_ORDER_ID"] = sourceRow["LOGISTIC_ORDER_ID"];
                    targetRow["GOODS_ID"] = sourceRow["GOODS_ID"];
                    targetRow["NUMBER"] = sourceRow["NUMBER"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processGoodsType)
            {
                DataTable sourceTable = newdata.Tables["GOODS_TYPE"];
                DataTable targetTable = cache.Tables["GOODS_TYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    GoodsTypePrimaryKey pk;
                    pk.GoodsTypeId = (System.Int32)sourceRow["GOODS_TYPE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.GoodsTypeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["GOODS_TYPE_ID"] = sourceRow["GOODS_TYPE_ID"];
                    targetRow["TYPE_NAME"] = sourceRow["TYPE_NAME"];
                    targetRow["WEIGHT"] = sourceRow["WEIGHT"];
                    targetRow["VOLUME"] = sourceRow["VOLUME"];
                    targetRow["MEASUREMENT_UNIT"] = sourceRow["MEASUREMENT_UNIT"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processGoodsTypeVehicle)
            {
                DataTable sourceTable = newdata.Tables["GOODS_TYPE_VEHICLE"];
                DataTable targetTable = cache.Tables["GOODS_TYPE_VEHICLE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    GoodsTypeVehiclePrimaryKey pk;
                    pk.GoodsTypeVehicleId = (System.Int32)sourceRow["GOODS_TYPE_VEHICLE_id"];
                    DataRow targetRow = targetTable.Rows.Find(pk.GoodsTypeVehicleId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["GOODS_TYPE_VEHICLE_id"] = sourceRow["GOODS_TYPE_VEHICLE_id"];
                    targetRow["GOODS_TYPE_ID"] = sourceRow["GOODS_TYPE_ID"];
                    targetRow["VEHICLE_ID"] = sourceRow["VEHICLE_ID"];
                    targetRow["CAPACITY"] = sourceRow["CAPACITY"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processGraphic)
            {
                DataTable sourceTable = newdata.Tables["GRAPHIC"];
                DataTable targetTable = cache.Tables["GRAPHIC"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    GraphicPrimaryKey pk;
                    pk.GraphicId = (System.Int32)sourceRow["GRAPHIC_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.GraphicId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["GRAPHIC_ID"] = sourceRow["GRAPHIC_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["BEGIN"] = sourceRow["BEGIN"];
                    targetRow["DAY_COUNT"] = sourceRow["DAY_COUNT"];
                    targetRow["DRIVER_COUNT"] = sourceRow["DRIVER_COUNT"];
                    targetRow["SHIFT_COUNT"] = sourceRow["SHIFT_COUNT"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processGraphicShift)
            {
                DataTable sourceTable = newdata.Tables["GRAPHIC_SHIFT"];
                DataTable targetTable = cache.Tables["GRAPHIC_SHIFT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    GraphicShiftPrimaryKey pk;
                    pk.Id = (System.Int32)sourceRow["ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.Id);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ID"] = sourceRow["ID"];
                    targetRow["GRAPHIC"] = sourceRow["GRAPHIC"];
                    targetRow["DAY_NUMBER"] = sourceRow["DAY_NUMBER"];
                    targetRow["DRIVER_NUMBER"] = sourceRow["DRIVER_NUMBER"];
                    targetRow["SHIFT"] = sourceRow["SHIFT"];
                    targetRow["BEGIN_TIME"] = sourceRow["BEGIN_TIME"];
                    targetRow["END_TIME"] = sourceRow["END_TIME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processGuardEvents)
            {
                DataTable sourceTable = newdata.Tables["GUARD_EVENTS"];
                DataTable targetTable = cache.Tables["GUARD_EVENTS"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    GuardEventsPrimaryKey pk;
                    pk.GuardEventId = (System.Int32)sourceRow["GUARD_EVENT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.GuardEventId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["GUARD_EVENT_ID"] = sourceRow["GUARD_EVENT_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processJobDelbadxy)
            {
                DataTable sourceTable = newdata.Tables["Job_DelBadXY"];
                DataTable targetTable = cache.Tables["Job_DelBadXY"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    JobDelbadxyPrimaryKey pk;
                    pk.JobDelbadxyId = (System.Int32)sourceRow["Job_DelBadXY_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.JobDelbadxyId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["Job_DelBadXY_ID"] = sourceRow["Job_DelBadXY_ID"];
                    targetRow["dtFrom"] = sourceRow["dtFrom"];
                    targetRow["dtTo"] = sourceRow["dtTo"];
                    targetRow["NextInHour"] = sourceRow["NextInHour"];
                    targetRow["time_allow"] = sourceRow["time_allow"];
                    targetRow["MaxSpeed"] = sourceRow["MaxSpeed"];
                    targetRow["Del"] = sourceRow["Del"];
                    targetRow["lastDelCount"] = sourceRow["lastDelCount"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processJournal)
            {
                DataTable sourceTable = newdata.Tables["JOURNAL"];
                DataTable targetTable = cache.Tables["JOURNAL"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    JournalPrimaryKey pk;
                    pk.JournalId = (System.Int32)sourceRow["JOURNAL_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.JournalId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["JOURNAL_ID"] = sourceRow["JOURNAL_ID"];
                    targetRow["BEGIN"] = sourceRow["BEGIN"];
                    targetRow["END"] = sourceRow["END"];
                    targetRow["DRIVER"] = sourceRow["DRIVER"];
                    targetRow["VEHICLE"] = sourceRow["VEHICLE"];
                    targetRow["STATUS"] = sourceRow["STATUS"];
                    targetRow["WAYOUT"] = sourceRow["WAYOUT"];
                    targetRow["REPLACED_DRIVER"] = sourceRow["REPLACED_DRIVER"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processJournalDay)
            {
                DataTable sourceTable = newdata.Tables["JOURNAL_DAY"];
                DataTable targetTable = cache.Tables["JOURNAL_DAY"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    JournalDayPrimaryKey pk;
                    pk.JournalDayId = (System.Int32)sourceRow["JOURNAL_DAY_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.JournalDayId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["JOURNAL_DAY_ID"] = sourceRow["JOURNAL_DAY_ID"];
                    targetRow["BRIGADE_DRIVER"] = sourceRow["BRIGADE_DRIVER"];
                    targetRow["DATE_DAY"] = sourceRow["DATE_DAY"];
                    targetRow["VEHICLE"] = sourceRow["VEHICLE"];
                    targetRow["WAYOUT"] = sourceRow["WAYOUT"];
                    targetRow["SHIFT"] = sourceRow["SHIFT"];
                    targetRow["TIME_BEGIN"] = sourceRow["TIME_BEGIN"];
                    targetRow["TIME_END"] = sourceRow["TIME_END"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processJournalDriver)
            {
                DataTable sourceTable = newdata.Tables["JOURNAL_DRIVER"];
                DataTable targetTable = cache.Tables["JOURNAL_DRIVER"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    JournalDriverPrimaryKey pk;
                    pk.JournalDriverId = (System.Int32)sourceRow["JOURNAL_DRIVER_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.JournalDriverId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["JOURNAL_DRIVER_ID"] = sourceRow["JOURNAL_DRIVER_ID"];
                    targetRow["BEGIN"] = sourceRow["BEGIN"];
                    targetRow["END"] = sourceRow["END"];
                    targetRow["DRIVER"] = sourceRow["DRIVER"];
                    targetRow["STATUS"] = sourceRow["STATUS"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processJournalVehicle)
            {
                DataTable sourceTable = newdata.Tables["JOURNAL_VEHICLE"];
                DataTable targetTable = cache.Tables["JOURNAL_VEHICLE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    JournalVehiclePrimaryKey pk;
                    pk.JournalVehicleId = (System.Int32)sourceRow["JOURNAL_VEHICLE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.JournalVehicleId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["JOURNAL_VEHICLE_ID"] = sourceRow["JOURNAL_VEHICLE_ID"];
                    targetRow["BEGIN"] = sourceRow["BEGIN"];
                    targetRow["END"] = sourceRow["END"];
                    targetRow["VEHICLE"] = sourceRow["VEHICLE"];
                    targetRow["STATUS"] = sourceRow["STATUS"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processLogOpsEvents)
            {
                DataTable sourceTable = newdata.Tables["LOG_OPS_EVENTS"];
                DataTable targetTable = cache.Tables["LOG_OPS_EVENTS"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    LogOpsEventsPrimaryKey pk;
                    pk.RecordId = (System.Int32)sourceRow["RECORD_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RecordId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RECORD_ID"] = sourceRow["RECORD_ID"];
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["OPS_EVENT_ID"] = sourceRow["OPS_EVENT_ID"];
                    targetRow["VEHICLE_ID"] = sourceRow["VEHICLE_ID"];
                    targetRow["EVENT_TIME"] = sourceRow["EVENT_TIME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processLogVehicleStatus)
            {
                DataTable sourceTable = newdata.Tables["LOG_VEHICLE_STATUS"];
                DataTable targetTable = cache.Tables["LOG_VEHICLE_STATUS"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    LogVehicleStatusPrimaryKey pk;
                    pk.LogVehicleStatusId = (System.Int32)sourceRow["LOG_VEHICLE_STATUS_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogVehicleStatusId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_VEHICLE_STATUS_ID"] = sourceRow["LOG_VEHICLE_STATUS_ID"];
                    targetRow["VEHICLE_STATUS_ID"] = sourceRow["VEHICLE_STATUS_ID"];
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["EVENTTIME"] = sourceRow["EVENTTIME"];
                    targetRow["COMMENT"] = sourceRow["COMMENT"];
                    targetRow["ENDTIME"] = sourceRow["ENDTIME"];
                    targetRow["VEHICLE_ID"] = sourceRow["VEHICLE_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processLogWaybillHeaderStatus)
            {
                DataTable sourceTable = newdata.Tables["LOG_WAYBILL_HEADER_STATUS"];
                DataTable targetTable = cache.Tables["LOG_WAYBILL_HEADER_STATUS"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    LogWaybillHeaderStatusPrimaryKey pk;
                    pk.LogWaybillHeaderStatusId = (System.Int32)sourceRow["LOG_WAYBILL_HEADER_STATUS_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogWaybillHeaderStatusId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOG_WAYBILL_HEADER_STATUS_ID"] = sourceRow["LOG_WAYBILL_HEADER_STATUS_ID"];
                    targetRow["WAYBILL_HEADER_ID"] = sourceRow["WAYBILL_HEADER_ID"];
                    targetRow["WAYBILL_STATUS_ID"] = sourceRow["WAYBILL_STATUS_ID"];
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["EVENT_TIME"] = sourceRow["EVENT_TIME"];
                    targetRow["COMMENT"] = sourceRow["COMMENT"];
                    targetRow["SCHEDULE_ID"] = sourceRow["SCHEDULE_ID"];
                    targetRow["END_TIME"] = sourceRow["END_TIME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processLogisticAddress)
            {
                DataTable sourceTable = newdata.Tables["LOGISTIC_ADDRESS"];
                DataTable targetTable = cache.Tables["LOGISTIC_ADDRESS"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    LogisticAddressPrimaryKey pk;
                    pk.LogisticAddressId = (System.Int32)sourceRow["LOGISTIC_ADDRESS_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogisticAddressId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOGISTIC_ADDRESS_ID"] = sourceRow["LOGISTIC_ADDRESS_ID"];
                    targetRow["CONTRACTOR_ID"] = sourceRow["CONTRACTOR_ID"];
                    targetRow["ADDRESS"] = sourceRow["ADDRESS"];
                    targetRow["GEOZONE_ID"] = sourceRow["GEOZONE_ID"];
                    targetRow["CONTRACTOR_CALENDAR_ID"] = sourceRow["CONTRACTOR_CALENDAR_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processLogisticOrder)
            {
                DataTable sourceTable = newdata.Tables["LOGISTIC_ORDER"];
                DataTable targetTable = cache.Tables["LOGISTIC_ORDER"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    LogisticOrderPrimaryKey pk;
                    pk.LogisticOrderId = (System.Int32)sourceRow["LOGISTIC_ORDER_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LogisticOrderId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LOGISTIC_ORDER_ID"] = sourceRow["LOGISTIC_ORDER_ID"];
                    targetRow["NUMBER"] = sourceRow["NUMBER"];
                    targetRow["CREATE_DATE"] = sourceRow["CREATE_DATE"];
                    targetRow["LOGISTIC_ADDRESS_ID"] = sourceRow["LOGISTIC_ADDRESS_ID"];
                    targetRow["CONTRACTOR_ID"] = sourceRow["CONTRACTOR_ID"];
                    targetRow["ORDER_TIME_FROM"] = sourceRow["ORDER_TIME_FROM"];
                    targetRow["ORDER_TIME_TO"] = sourceRow["ORDER_TIME_TO"];
                    targetRow["BLADING_TYPE_ID"] = sourceRow["BLADING_TYPE_ID"];
                    targetRow["PLANNING_SHIPMENT_TIME"] = sourceRow["PLANNING_SHIPMENT_TIME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processMapVertex)
            {
                DataTable sourceTable = newdata.Tables["MAP_VERTEX"];
                DataTable targetTable = cache.Tables["MAP_VERTEX"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    MapVertexPrimaryKey pk;
                    pk.VertexId = (System.Int32)sourceRow["VERTEX_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.VertexId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["VERTEX_ID"] = sourceRow["VERTEX_ID"];
                    targetRow["MAP_ID"] = sourceRow["MAP_ID"];
                    targetRow["X"] = sourceRow["X"];
                    targetRow["Y"] = sourceRow["Y"];
                    targetRow["VISIBLE"] = sourceRow["VISIBLE"];
                    targetRow["ENABLE"] = sourceRow["ENABLE"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processMaps)
            {
                DataTable sourceTable = newdata.Tables["MAPS"];
                DataTable targetTable = cache.Tables["MAPS"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    MapsPrimaryKey pk;
                    pk.MapId = (System.Int32)sourceRow["MAP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.MapId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["MAP_ID"] = sourceRow["MAP_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    targetRow["FILE_NAME"] = sourceRow["FILE_NAME"];
                    targetRow["VERSION"] = sourceRow["VERSION"];
                    targetRow["CHECK_SUM"] = sourceRow["CHECK_SUM"];
                    targetRow["GUID"] = sourceRow["GUID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processMedia)
            {
                DataTable sourceTable = newdata.Tables["MEDIA"];
                DataTable targetTable = cache.Tables["MEDIA"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    MediaPrimaryKey pk;
                    pk.MediaId = (System.Int32)sourceRow["MEDIA_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.MediaId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["MEDIA_ID"] = sourceRow["MEDIA_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["TYPE"] = sourceRow["TYPE"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processMediaAcceptors)
            {
                DataTable sourceTable = newdata.Tables["MEDIA_ACCEPTORS"];
                DataTable targetTable = cache.Tables["MEDIA_ACCEPTORS"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    MediaAcceptorsPrimaryKey pk;
                    pk.MaId = (System.Int32)sourceRow["MA_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.MaId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["MA_ID"] = sourceRow["MA_ID"];
                    targetRow["MEDIA_ID"] = sourceRow["MEDIA_ID"];
                    targetRow["INITIALIZATION"] = sourceRow["INITIALIZATION"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processMediaType)
            {
                DataTable sourceTable = newdata.Tables["MEDIA_TYPE"];
                DataTable targetTable = cache.Tables["MEDIA_TYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    MediaTypePrimaryKey pk;
                    pk.Id = (System.Byte)sourceRow["ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.Id);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ID"] = sourceRow["ID"];
                    targetRow["Name"] = sourceRow["Name"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processMessage)
            {
                DataTable sourceTable = newdata.Tables["MESSAGE"];
                DataTable targetTable = cache.Tables["MESSAGE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    MessagePrimaryKey pk;
                    pk.MessageId = (System.Int32)sourceRow["MESSAGE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.MessageId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["MESSAGE_ID"] = sourceRow["MESSAGE_ID"];
                    targetRow["TEMPLATE_ID"] = sourceRow["TEMPLATE_ID"];
                    targetRow["SOURCE_ID"] = sourceRow["SOURCE_ID"];
                    targetRow["SEVERITY_ID"] = sourceRow["SEVERITY_ID"];
                    targetRow["POSITION_ID"] = sourceRow["POSITION_ID"];
                    targetRow["SUBJECT"] = sourceRow["SUBJECT"];
                    targetRow["BODY"] = sourceRow["BODY"];
                    targetRow["STATUS"] = sourceRow["STATUS"];
                    targetRow["TIME"] = sourceRow["TIME"];
                    targetRow["DESTINATION_ID"] = sourceRow["DESTINATION_ID"];
                    targetRow["TYPE"] = sourceRow["TYPE"];
                    targetRow["GROUP"] = sourceRow["GROUP"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processMessageBoard)
            {
                DataTable sourceTable = newdata.Tables["MESSAGE_BOARD"];
                DataTable targetTable = cache.Tables["MESSAGE_BOARD"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    MessageBoardPrimaryKey pk;
                    pk.Id = (System.Int32)sourceRow["ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.Id);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ID"] = sourceRow["ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["DESC"] = sourceRow["DESC"];
                    targetRow["NUM"] = sourceRow["NUM"];
                    targetRow["STATUS"] = sourceRow["STATUS"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processMessageField)
            {
                DataTable sourceTable = newdata.Tables["MESSAGE_FIELD"];
                DataTable targetTable = cache.Tables["MESSAGE_FIELD"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    MessageFieldPrimaryKey pk;
                    pk.MessageFieldId = (System.Int32)sourceRow["MESSAGE_FIELD_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.MessageFieldId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["MESSAGE_FIELD_ID"] = sourceRow["MESSAGE_FIELD_ID"];
                    targetRow["MESSAGE_ID"] = sourceRow["MESSAGE_ID"];
                    targetRow["MESSAGE_TEMPLATE_FIELD_ID"] = sourceRow["MESSAGE_TEMPLATE_FIELD_ID"];
                    targetRow["CONTENT"] = sourceRow["CONTENT"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processMessageOperator)
            {
                DataTable sourceTable = newdata.Tables["MESSAGE_OPERATOR"];
                DataTable targetTable = cache.Tables["MESSAGE_OPERATOR"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    MessageOperatorPrimaryKey pk;
                    pk.MessageOperatorId = (System.Int32)sourceRow["MESSAGE_OPERATOR_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.MessageOperatorId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["MESSAGE_OPERATOR_ID"] = sourceRow["MESSAGE_OPERATOR_ID"];
                    targetRow["MESSAGE_ID"] = sourceRow["MESSAGE_ID"];
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["MESSAGE_STATUS_ID"] = sourceRow["MESSAGE_STATUS_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processMessageTemplate)
            {
                DataTable sourceTable = newdata.Tables["MESSAGE_TEMPLATE"];
                DataTable targetTable = cache.Tables["MESSAGE_TEMPLATE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    MessageTemplatePrimaryKey pk;
                    pk.MessageTemplateId = (System.Int32)sourceRow["MESSAGE_TEMPLATE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.MessageTemplateId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["MESSAGE_TEMPLATE_ID"] = sourceRow["MESSAGE_TEMPLATE_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["HEADER"] = sourceRow["HEADER"];
                    targetRow["BODY"] = sourceRow["BODY"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processMessageTemplateField)
            {
                DataTable sourceTable = newdata.Tables["MESSAGE_TEMPLATE_FIELD"];
                DataTable targetTable = cache.Tables["MESSAGE_TEMPLATE_FIELD"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    MessageTemplateFieldPrimaryKey pk;
                    pk.MessageTemplateFieldId = (System.Int32)sourceRow["MESSAGE_TEMPLATE_FIELD_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.MessageTemplateFieldId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["MESSAGE_TEMPLATE_FIELD_ID"] = sourceRow["MESSAGE_TEMPLATE_FIELD_ID"];
                    targetRow["MESSAGE_TEMPLATE_ID"] = sourceRow["MESSAGE_TEMPLATE_ID"];
                    targetRow["DOTNET_TYPE_ID"] = sourceRow["DOTNET_TYPE_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperator)
            {
                DataTable sourceTable = newdata.Tables["OPERATOR"];
                DataTable targetTable = cache.Tables["OPERATOR"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorPrimaryKey pk;
                    pk.OperatorId = (System.Int32)sourceRow["OPERATOR_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.OperatorId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["LOGIN"] = sourceRow["LOGIN"];
                    targetRow["PASSWORD"] = sourceRow["PASSWORD"];
                    targetRow["PHONE"] = sourceRow["PHONE"];
                    targetRow["EMAIL"] = sourceRow["EMAIL"];
                    targetRow["GUID"] = sourceRow["GUID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorDepartment)
            {
                DataTable sourceTable = newdata.Tables["OPERATOR_DEPARTMENT"];
                DataTable targetTable = cache.Tables["OPERATOR_DEPARTMENT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorDepartmentPrimaryKey pk;
                    pk.OperatorId = (System.Int32)sourceRow["OPERATOR_ID"];
                    pk.DepartmentId = (System.Int32)sourceRow["DEPARTMENT_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorId, pk.DepartmentId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["DEPARTMENT_ID"] = sourceRow["DEPARTMENT_ID"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorDriver)
            {
                DataTable sourceTable = newdata.Tables["OPERATOR_DRIVER"];
                DataTable targetTable = cache.Tables["OPERATOR_DRIVER"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorDriverPrimaryKey pk;
                    pk.OperatorId = (System.Int32)sourceRow["OPERATOR_ID"];
                    pk.DriverId = (System.Int32)sourceRow["DRIVER_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorId, pk.DriverId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["DRIVER_ID"] = sourceRow["DRIVER_ID"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorDrivergroup)
            {
                DataTable sourceTable = newdata.Tables["OPERATOR_DRIVERGROUP"];
                DataTable targetTable = cache.Tables["OPERATOR_DRIVERGROUP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorDrivergroupPrimaryKey pk;
                    pk.OperatorId = (System.Int32)sourceRow["OPERATOR_ID"];
                    pk.DrivergroupId = (System.Int32)sourceRow["DRIVERGROUP_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorId, pk.DrivergroupId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["DRIVERGROUP_ID"] = sourceRow["DRIVERGROUP_ID"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorProfile)
            {
                DataTable sourceTable = newdata.Tables["OPERATOR_PROFILE"];
                DataTable targetTable = cache.Tables["OPERATOR_PROFILE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorProfilePrimaryKey pk;
                    pk.OperatorProfileId = (System.Int32)sourceRow["OPERATOR_PROFILE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.OperatorProfileId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["PROFILE"] = sourceRow["PROFILE"];
                    targetRow["OPERATOR_PROFILE_ID"] = sourceRow["OPERATOR_PROFILE_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorReport)
            {
                DataTable sourceTable = newdata.Tables["OPERATOR_REPORT"];
                DataTable targetTable = cache.Tables["OPERATOR_REPORT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorReportPrimaryKey pk;
                    pk.OperatorId = (System.Int32)sourceRow["OPERATOR_ID"];
                    pk.ReportId = (System.Int32)sourceRow["REPORT_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorId, pk.ReportId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["REPORT_ID"] = sourceRow["REPORT_ID"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorRoute)
            {
                DataTable sourceTable = newdata.Tables["OPERATOR_ROUTE"];
                DataTable targetTable = cache.Tables["OPERATOR_ROUTE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorRoutePrimaryKey pk;
                    pk.OperatorId = (System.Int32)sourceRow["OPERATOR_ID"];
                    pk.RouteId = (System.Int32)sourceRow["ROUTE_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorId, pk.RouteId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["ROUTE_ID"] = sourceRow["ROUTE_ID"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorRoutegroup)
            {
                DataTable sourceTable = newdata.Tables["OPERATOR_ROUTEGROUP"];
                DataTable targetTable = cache.Tables["OPERATOR_ROUTEGROUP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorRoutegroupPrimaryKey pk;
                    pk.OperatorId = (System.Int32)sourceRow["OPERATOR_ID"];
                    pk.RoutegroupId = (System.Int32)sourceRow["ROUTEGROUP_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorId, pk.RoutegroupId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["ROUTEGROUP_ID"] = sourceRow["ROUTEGROUP_ID"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorVehicle)
            {
                DataTable sourceTable = newdata.Tables["OPERATOR_VEHICLE"];
                DataTable targetTable = cache.Tables["OPERATOR_VEHICLE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorVehiclePrimaryKey pk;
                    pk.OperatorId = (System.Int32)sourceRow["OPERATOR_ID"];
                    pk.VehicleId = (System.Int32)sourceRow["VEHICLE_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorId, pk.VehicleId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["VEHICLE_ID"] = sourceRow["VEHICLE_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorVehiclegroup)
            {
                DataTable sourceTable = newdata.Tables["OPERATOR_VEHICLEGROUP"];
                DataTable targetTable = cache.Tables["OPERATOR_VEHICLEGROUP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorVehiclegroupPrimaryKey pk;
                    pk.OperatorId = (System.Int32)sourceRow["OPERATOR_ID"];
                    pk.VehiclegroupId = (System.Int32)sourceRow["VEHICLEGROUP_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorId, pk.VehiclegroupId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["VEHICLEGROUP_ID"] = sourceRow["VEHICLEGROUP_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorZone)
            {
                DataTable sourceTable = newdata.Tables["OPERATOR_ZONE"];
                DataTable targetTable = cache.Tables["OPERATOR_ZONE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorZonePrimaryKey pk;
                    pk.OperatorId = (System.Int32)sourceRow["OPERATOR_ID"];
                    pk.ZoneId = (System.Int32)sourceRow["ZONE_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorId, pk.ZoneId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["ZONE_ID"] = sourceRow["ZONE_ID"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorZonegroup)
            {
                DataTable sourceTable = newdata.Tables["OPERATOR_ZONEGROUP"];
                DataTable targetTable = cache.Tables["OPERATOR_ZONEGROUP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorZonegroupPrimaryKey pk;
                    pk.OperatorId = (System.Int32)sourceRow["OPERATOR_ID"];
                    pk.ZonegroupId = (System.Int32)sourceRow["ZONEGROUP_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorId, pk.ZonegroupId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["ZONEGROUP_ID"] = sourceRow["ZONEGROUP_ID"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorgroup)
            {
                DataTable sourceTable = newdata.Tables["OPERATORGROUP"];
                DataTable targetTable = cache.Tables["OPERATORGROUP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorgroupPrimaryKey pk;
                    pk.OperatorgroupId = (System.Int32)sourceRow["OPERATORGROUP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.OperatorgroupId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATORGROUP_ID"] = sourceRow["OPERATORGROUP_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["COMMENT"] = sourceRow["COMMENT"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorgroupDepartment)
            {
                DataTable sourceTable = newdata.Tables["OPERATORGROUP_DEPARTMENT"];
                DataTable targetTable = cache.Tables["OPERATORGROUP_DEPARTMENT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorgroupDepartmentPrimaryKey pk;
                    pk.OperatorgroupId = (System.Int32)sourceRow["OPERATORGROUP_ID"];
                    pk.DepartmentId = (System.Int32)sourceRow["DEPARTMENT_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorgroupId, pk.DepartmentId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATORGROUP_ID"] = sourceRow["OPERATORGROUP_ID"];
                    targetRow["DEPARTMENT_ID"] = sourceRow["DEPARTMENT_ID"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorgroupDriver)
            {
                DataTable sourceTable = newdata.Tables["OPERATORGROUP_DRIVER"];
                DataTable targetTable = cache.Tables["OPERATORGROUP_DRIVER"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorgroupDriverPrimaryKey pk;
                    pk.OperatorgroupId = (System.Int32)sourceRow["OPERATORGROUP_ID"];
                    pk.DriverId = (System.Int32)sourceRow["DRIVER_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorgroupId, pk.DriverId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATORGROUP_ID"] = sourceRow["OPERATORGROUP_ID"];
                    targetRow["DRIVER_ID"] = sourceRow["DRIVER_ID"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorgroupDrivergroup)
            {
                DataTable sourceTable = newdata.Tables["OPERATORGROUP_DRIVERGROUP"];
                DataTable targetTable = cache.Tables["OPERATORGROUP_DRIVERGROUP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorgroupDrivergroupPrimaryKey pk;
                    pk.OperatorgroupId = (System.Int32)sourceRow["OPERATORGROUP_ID"];
                    pk.DrivergroupId = (System.Int32)sourceRow["DRIVERGROUP_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorgroupId, pk.DrivergroupId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATORGROUP_ID"] = sourceRow["OPERATORGROUP_ID"];
                    targetRow["DRIVERGROUP_ID"] = sourceRow["DRIVERGROUP_ID"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorgroupOperator)
            {
                DataTable sourceTable = newdata.Tables["OPERATORGROUP_OPERATOR"];
                DataTable targetTable = cache.Tables["OPERATORGROUP_OPERATOR"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorgroupOperatorPrimaryKey pk;
                    pk.OperatorgroupId = (System.Int32)sourceRow["OPERATORGROUP_ID"];
                    pk.OperatorId = (System.Int32)sourceRow["OPERATOR_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorgroupId, pk.OperatorId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATORGROUP_ID"] = sourceRow["OPERATORGROUP_ID"];
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorgroupReport)
            {
                DataTable sourceTable = newdata.Tables["OPERATORGROUP_REPORT"];
                DataTable targetTable = cache.Tables["OPERATORGROUP_REPORT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorgroupReportPrimaryKey pk;
                    pk.OperatorgroupId = (System.Int32)sourceRow["OPERATORGROUP_ID"];
                    pk.ReportId = (System.Int32)sourceRow["REPORT_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorgroupId, pk.ReportId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATORGROUP_ID"] = sourceRow["OPERATORGROUP_ID"];
                    targetRow["REPORT_ID"] = sourceRow["REPORT_ID"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorgroupRoute)
            {
                DataTable sourceTable = newdata.Tables["OPERATORGROUP_ROUTE"];
                DataTable targetTable = cache.Tables["OPERATORGROUP_ROUTE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorgroupRoutePrimaryKey pk;
                    pk.OperatorgroupId = (System.Int32)sourceRow["OPERATORGROUP_ID"];
                    pk.RouteId = (System.Int32)sourceRow["ROUTE_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorgroupId, pk.RouteId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATORGROUP_ID"] = sourceRow["OPERATORGROUP_ID"];
                    targetRow["ROUTE_ID"] = sourceRow["ROUTE_ID"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorgroupRoutegroup)
            {
                DataTable sourceTable = newdata.Tables["OPERATORGROUP_ROUTEGROUP"];
                DataTable targetTable = cache.Tables["OPERATORGROUP_ROUTEGROUP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorgroupRoutegroupPrimaryKey pk;
                    pk.OperatorgroupId = (System.Int32)sourceRow["OPERATORGROUP_ID"];
                    pk.RoutegroupId = (System.Int32)sourceRow["ROUTEGROUP_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorgroupId, pk.RoutegroupId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATORGROUP_ID"] = sourceRow["OPERATORGROUP_ID"];
                    targetRow["ROUTEGROUP_ID"] = sourceRow["ROUTEGROUP_ID"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorgroupVehicle)
            {
                DataTable sourceTable = newdata.Tables["OPERATORGROUP_VEHICLE"];
                DataTable targetTable = cache.Tables["OPERATORGROUP_VEHICLE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorgroupVehiclePrimaryKey pk;
                    pk.OperatorgroupId = (System.Int32)sourceRow["OPERATORGROUP_ID"];
                    pk.VehicleId = (System.Int32)sourceRow["VEHICLE_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorgroupId, pk.VehicleId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATORGROUP_ID"] = sourceRow["OPERATORGROUP_ID"];
                    targetRow["VEHICLE_ID"] = sourceRow["VEHICLE_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorgroupVehiclegroup)
            {
                DataTable sourceTable = newdata.Tables["OPERATORGROUP_VEHICLEGROUP"];
                DataTable targetTable = cache.Tables["OPERATORGROUP_VEHICLEGROUP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorgroupVehiclegroupPrimaryKey pk;
                    pk.OperatorgroupId = (System.Int32)sourceRow["OPERATORGROUP_ID"];
                    pk.VehiclegroupId = (System.Int32)sourceRow["VEHICLEGROUP_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorgroupId, pk.VehiclegroupId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATORGROUP_ID"] = sourceRow["OPERATORGROUP_ID"];
                    targetRow["VEHICLEGROUP_ID"] = sourceRow["VEHICLEGROUP_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorgroupZone)
            {
                DataTable sourceTable = newdata.Tables["OPERATORGROUP_ZONE"];
                DataTable targetTable = cache.Tables["OPERATORGROUP_ZONE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorgroupZonePrimaryKey pk;
                    pk.OperatorgroupId = (System.Int32)sourceRow["OPERATORGROUP_ID"];
                    pk.ZoneId = (System.Int32)sourceRow["ZONE_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorgroupId, pk.ZoneId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATORGROUP_ID"] = sourceRow["OPERATORGROUP_ID"];
                    targetRow["ZONE_ID"] = sourceRow["ZONE_ID"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOperatorgroupZonegroup)
            {
                DataTable sourceTable = newdata.Tables["OPERATORGROUP_ZONEGROUP"];
                DataTable targetTable = cache.Tables["OPERATORGROUP_ZONEGROUP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OperatorgroupZonegroupPrimaryKey pk;
                    pk.OperatorgroupId = (System.Int32)sourceRow["OPERATORGROUP_ID"];
                    pk.ZonegroupId = (System.Int32)sourceRow["ZONEGROUP_ID"];
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.OperatorgroupId, pk.ZonegroupId, pk.RightId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPERATORGROUP_ID"] = sourceRow["OPERATORGROUP_ID"];
                    targetRow["ZONEGROUP_ID"] = sourceRow["ZONEGROUP_ID"];
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOpsEvent)
            {
                DataTable sourceTable = newdata.Tables["OPS_EVENT"];
                DataTable targetTable = cache.Tables["OPS_EVENT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OpsEventPrimaryKey pk;
                    pk.OpsEventId = (System.Int32)sourceRow["OPS_EVENT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.OpsEventId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OPS_EVENT_ID"] = sourceRow["OPS_EVENT_ID"];
                    targetRow["OPS_EVENT_NAME"] = sourceRow["OPS_EVENT_NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOrder)
            {
                DataTable sourceTable = newdata.Tables["ORDER"];
                DataTable targetTable = cache.Tables["ORDER"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OrderPrimaryKey pk;
                    pk.OrderId = (System.Int32)sourceRow["ORDER_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.OrderId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ORDER_ID"] = sourceRow["ORDER_ID"];
                    targetRow["DATE"] = sourceRow["DATE"];
                    targetRow["DEPARTMENT"] = sourceRow["DEPARTMENT"];
                    targetRow["ORDER_TYPE"] = sourceRow["ORDER_TYPE"];
                    targetRow["CUSTOMER"] = sourceRow["CUSTOMER"];
                    targetRow["RECIPIENT"] = sourceRow["RECIPIENT"];
                    targetRow["RESPONDENT"] = sourceRow["RESPONDENT"];
                    targetRow["TIME_BEGIN"] = sourceRow["TIME_BEGIN"];
                    targetRow["TIME_END"] = sourceRow["TIME_END"];
                    targetRow["ORIGIN"] = sourceRow["ORIGIN"];
                    targetRow["DESTINATION"] = sourceRow["DESTINATION"];
                    targetRow["LENGTH"] = sourceRow["LENGTH"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOrderTrip)
            {
                DataTable sourceTable = newdata.Tables["ORDER_TRIP"];
                DataTable targetTable = cache.Tables["ORDER_TRIP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OrderTripPrimaryKey pk;
                    pk.OrderTripId = (System.Int32)sourceRow["ORDER_TRIP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.OrderTripId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ORDER_TRIP_ID"] = sourceRow["ORDER_TRIP_ID"];
                    targetRow["ORDER"] = sourceRow["ORDER"];
                    targetRow["JOURNAL_DAY"] = sourceRow["JOURNAL_DAY"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOrderType)
            {
                DataTable sourceTable = newdata.Tables["ORDER_TYPE"];
                DataTable targetTable = cache.Tables["ORDER_TYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OrderTypePrimaryKey pk;
                    pk.OrderTypeId = (System.Int32)sourceRow["ORDER_TYPE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.OrderTypeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ORDER_TYPE_ID"] = sourceRow["ORDER_TYPE_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processOwner)
            {
                DataTable sourceTable = newdata.Tables["OWNER"];
                DataTable targetTable = cache.Tables["OWNER"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    OwnerPrimaryKey pk;
                    pk.OwnerId = (System.Int32)sourceRow["OWNER_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.OwnerId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["OWNER_ID"] = sourceRow["OWNER_ID"];
                    targetRow["FIRST_NAME"] = sourceRow["FIRST_NAME"];
                    targetRow["LAST_NAME"] = sourceRow["LAST_NAME"];
                    targetRow["SECOND_NAME"] = sourceRow["SECOND_NAME"];
                    targetRow["PHONE_NUMBER1"] = sourceRow["PHONE_NUMBER1"];
                    targetRow["PHONE_NUMBER2"] = sourceRow["PHONE_NUMBER2"];
                    targetRow["PHONE_NUMBER3"] = sourceRow["PHONE_NUMBER3"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processPeriod)
            {
                DataTable sourceTable = newdata.Tables["PERIOD"];
                DataTable targetTable = cache.Tables["PERIOD"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    PeriodPrimaryKey pk;
                    pk.PeriodId = (System.Int32)sourceRow["PERIOD_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.PeriodId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["PERIOD_ID"] = sourceRow["PERIOD_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["ROUTE"] = sourceRow["ROUTE"];
                    targetRow["DAY_KIND"] = sourceRow["DAY_KIND"];
                    targetRow["DK_SET"] = sourceRow["DK_SET"];
                    targetRow["BEGIN"] = sourceRow["BEGIN"];
                    targetRow["END"] = sourceRow["END"];
                    targetRow["COLOR"] = sourceRow["COLOR"];
                    targetRow["HOLIDAY"] = sourceRow["HOLIDAY"];
                    targetRow["SEASON_ID"] = sourceRow["SEASON_ID"];
                    targetRow["COMMENT"] = sourceRow["COMMENT"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processPoint)
            {
                DataTable sourceTable = newdata.Tables["POINT"];
                DataTable targetTable = cache.Tables["POINT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    PointPrimaryKey pk;
                    pk.PointId = (System.Int32)sourceRow["POINT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.PointId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["POINT_ID"] = sourceRow["POINT_ID"];
                    targetRow["POINT_KIND_ID"] = sourceRow["POINT_KIND_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["X"] = sourceRow["X"];
                    targetRow["Y"] = sourceRow["Y"];
                    targetRow["RADIUS"] = sourceRow["RADIUS"];
                    targetRow["ADDRESS"] = sourceRow["ADDRESS"];
                    targetRow["COMMENT"] = sourceRow["COMMENT"];
                    targetRow["BUSSTOP_ID"] = sourceRow["BUSSTOP_ID"];
                    targetRow["VERTEX_ID"] = sourceRow["VERTEX_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processPointKind)
            {
                DataTable sourceTable = newdata.Tables["POINT_KIND"];
                DataTable targetTable = cache.Tables["POINT_KIND"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    PointKindPrimaryKey pk;
                    pk.PointKindId = (System.Int32)sourceRow["POINT_KIND_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.PointKindId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["POINT_KIND_ID"] = sourceRow["POINT_KIND_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processReport)
            {
                DataTable sourceTable = newdata.Tables["REPORT"];
                DataTable targetTable = cache.Tables["REPORT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ReportPrimaryKey pk;
                    pk.ReportId = (System.Int32)sourceRow["REPORT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ReportId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["REPORT_ID"] = sourceRow["REPORT_ID"];
                    targetRow["REPORT_GUID"] = sourceRow["REPORT_GUID"];
                    targetRow["REPORT_FILENAME"] = sourceRow["REPORT_FILENAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processReprintReason)
            {
                DataTable sourceTable = newdata.Tables["REPRINT_REASON"];
                DataTable targetTable = cache.Tables["REPRINT_REASON"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ReprintReasonPrimaryKey pk;
                    pk.ReprintReasonId = (System.Int32)sourceRow["REPRINT_REASON_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ReprintReasonId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["REPRINT_REASON_ID"] = sourceRow["REPRINT_REASON_ID"];
                    targetRow["REPRINT_REASON_NAME"] = sourceRow["REPRINT_REASON_NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRight)
            {
                DataTable sourceTable = newdata.Tables["RIGHT"];
                DataTable targetTable = cache.Tables["RIGHT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RightPrimaryKey pk;
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RightId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["SYSTEM"] = sourceRow["SYSTEM"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    targetRow["HELP_URL"] = sourceRow["HELP_URL"];
                    targetRow["URL"] = sourceRow["URL"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRightOperator)
            {
                DataTable sourceTable = newdata.Tables["RIGHT_OPERATOR"];
                DataTable targetTable = cache.Tables["RIGHT_OPERATOR"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RightOperatorPrimaryKey pk;
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    pk.OperatorId = (System.Int32)sourceRow["OPERATOR_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.RightId, pk.OperatorId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRightOperatorgroup)
            {
                DataTable sourceTable = newdata.Tables["RIGHT_OPERATORGROUP"];
                DataTable targetTable = cache.Tables["RIGHT_OPERATORGROUP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RightOperatorgroupPrimaryKey pk;
                    pk.RightId = (System.Int32)sourceRow["RIGHT_ID"];
                    pk.OperatorgroupId = (System.Int32)sourceRow["OPERATORGROUP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.RightId, pk.OperatorgroupId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RIGHT_ID"] = sourceRow["RIGHT_ID"];
                    targetRow["OPERATORGROUP_ID"] = sourceRow["OPERATORGROUP_ID"];
                    targetRow["ALLOWED"] = sourceRow["ALLOWED"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRoute)
            {
                DataTable sourceTable = newdata.Tables["ROUTE"];
                DataTable targetTable = cache.Tables["ROUTE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RoutePrimaryKey pk;
                    pk.RouteId = (System.Int32)sourceRow["ROUTE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RouteId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ROUTE_ID"] = sourceRow["ROUTE_ID"];
                    targetRow["EXT_NUMBER"] = sourceRow["EXT_NUMBER"];
                    targetRow["LENGTH"] = sourceRow["LENGTH"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    targetRow["COLOR"] = sourceRow["COLOR"];
                    targetRow["VERTEX"] = sourceRow["VERTEX"];
                    targetRow["PREFIX"] = sourceRow["PREFIX"];
                    targetRow["DIGIT_COUNT"] = sourceRow["DIGIT_COUNT"];
                    targetRow["TIME_BEGIN"] = sourceRow["TIME_BEGIN"];
                    targetRow["TIME_END"] = sourceRow["TIME_END"];
                    targetRow["TIME_DEVIATION"] = sourceRow["TIME_DEVIATION"];
                    targetRow["DEVIATION"] = sourceRow["DEVIATION"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRouteGeozone)
            {
                DataTable sourceTable = newdata.Tables["ROUTE_GEOZONE"];
                DataTable targetTable = cache.Tables["ROUTE_GEOZONE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RouteGeozonePrimaryKey pk;
                    pk.RouteGeozoneId = (System.Int32)sourceRow["ROUTE_GEOZONE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RouteGeozoneId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ROUTE_GEOZONE_ID"] = sourceRow["ROUTE_GEOZONE_ID"];
                    targetRow["ROUTE_ID"] = sourceRow["ROUTE_ID"];
                    targetRow["GEOZONE_ID"] = sourceRow["GEOZONE_ID"];
                    targetRow["STATUS"] = sourceRow["STATUS"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRoutePoint)
            {
                DataTable sourceTable = newdata.Tables["ROUTE_POINT"];
                DataTable targetTable = cache.Tables["ROUTE_POINT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RoutePointPrimaryKey pk;
                    pk.RoutePointId = (System.Int32)sourceRow["ROUTE_POINT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RoutePointId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ROUTE_POINT_ID"] = sourceRow["ROUTE_POINT_ID"];
                    targetRow["ROUTE_ID"] = sourceRow["ROUTE_ID"];
                    targetRow["POINT_ID"] = sourceRow["POINT_ID"];
                    targetRow["ORD"] = sourceRow["ORD"];
                    targetRow["STATUS"] = sourceRow["STATUS"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRouteTrip)
            {
                DataTable sourceTable = newdata.Tables["ROUTE_TRIP"];
                DataTable targetTable = cache.Tables["ROUTE_TRIP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RouteTripPrimaryKey pk;
                    pk.RouteTripId = (System.Int32)sourceRow["ROUTE_TRIP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RouteTripId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ROUTE_TRIP_ID"] = sourceRow["ROUTE_TRIP_ID"];
                    targetRow["GEO_TRIP_ID"] = sourceRow["GEO_TRIP_ID"];
                    targetRow["ROUTE_ID"] = sourceRow["ROUTE_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRoutegroup)
            {
                DataTable sourceTable = newdata.Tables["ROUTEGROUP"];
                DataTable targetTable = cache.Tables["ROUTEGROUP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RoutegroupPrimaryKey pk;
                    pk.RoutegroupId = (System.Int32)sourceRow["ROUTEGROUP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RoutegroupId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ROUTEGROUP_ID"] = sourceRow["ROUTEGROUP_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["COMMENT"] = sourceRow["COMMENT"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRoutegroupRoute)
            {
                DataTable sourceTable = newdata.Tables["ROUTEGROUP_ROUTE"];
                DataTable targetTable = cache.Tables["ROUTEGROUP_ROUTE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RoutegroupRoutePrimaryKey pk;
                    pk.RoutegroupId = (System.Int32)sourceRow["ROUTEGROUP_ID"];
                    pk.RouteId = (System.Int32)sourceRow["ROUTE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.RoutegroupId, pk.RouteId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ROUTEGROUP_ID"] = sourceRow["ROUTEGROUP_ID"];
                    targetRow["ROUTE_ID"] = sourceRow["ROUTE_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRs)
            {
                DataTable sourceTable = newdata.Tables["RS"];
                DataTable targetTable = cache.Tables["RS"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RsPrimaryKey pk;
                    pk.RsId = (System.Int32)sourceRow["RS_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RsId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RS_ID"] = sourceRow["RS_ID"];
                    targetRow["ROUTE_ID"] = sourceRow["ROUTE_ID"];
                    targetRow["DAY_TYPE_ID"] = sourceRow["DAY_TYPE_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRsNumber)
            {
                DataTable sourceTable = newdata.Tables["RS_NUMBER"];
                DataTable targetTable = cache.Tables["RS_NUMBER"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RsNumberPrimaryKey pk;
                    pk.RsNumberId = (System.Int32)sourceRow["RS_NUMBER_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RsNumberId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RS_NUMBER_ID"] = sourceRow["RS_NUMBER_ID"];
                    targetRow["RS_STEP"] = sourceRow["RS_STEP"];
                    targetRow["NUMBER"] = sourceRow["NUMBER"];
                    targetRow["INTERVAL"] = sourceRow["INTERVAL"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRsOperationstype)
            {
                DataTable sourceTable = newdata.Tables["RS_OPERATIONSTYPE"];
                DataTable targetTable = cache.Tables["RS_OPERATIONSTYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RsOperationstypePrimaryKey pk;
                    pk.RsOperationstypeId = (System.Int32)sourceRow["RS_OPERATIONSTYPE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RsOperationstypeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RS_OPERATIONSTYPE_ID"] = sourceRow["RS_OPERATIONSTYPE_ID"];
                    targetRow["ALIAS"] = sourceRow["ALIAS"];
                    targetRow["LETTER"] = sourceRow["LETTER"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRsOperationtype)
            {
                DataTable sourceTable = newdata.Tables["RS_OPERATIONTYPE"];
                DataTable targetTable = cache.Tables["RS_OPERATIONTYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RsOperationtypePrimaryKey pk;
                    pk.RsOperationtypeId = (System.Int32)sourceRow["RS_OPERATIONTYPE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RsOperationtypeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RS_OPERATIONTYPE_ID"] = sourceRow["RS_OPERATIONTYPE_ID"];
                    targetRow["RS_VARIANT_ID"] = sourceRow["RS_VARIANT_ID"];
                    targetRow["RS_OPERATIONSTYPE_ID"] = sourceRow["RS_OPERATIONSTYPE_ID"];
                    targetRow["TRIP_OUT"] = sourceRow["TRIP_OUT"];
                    targetRow["TRIP_OUT_0"] = sourceRow["TRIP_OUT_0"];
                    targetRow["TRIP_IN_0"] = sourceRow["TRIP_IN_0"];
                    targetRow["TRIP_IN"] = sourceRow["TRIP_IN"];
                    targetRow["CODE"] = sourceRow["CODE"];
                    targetRow["OPERATION_TIME"] = sourceRow["OPERATION_TIME"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRsPeriod)
            {
                DataTable sourceTable = newdata.Tables["RS_PERIOD"];
                DataTable targetTable = cache.Tables["RS_PERIOD"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RsPeriodPrimaryKey pk;
                    pk.PeriodId = (System.Int32)sourceRow["PERIOD_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.PeriodId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["PERIOD_ID"] = sourceRow["PERIOD_ID"];
                    targetRow["BEGIN"] = sourceRow["BEGIN"];
                    targetRow["END"] = sourceRow["END"];
                    targetRow["RS_VARIANT_ID"] = sourceRow["RS_VARIANT_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRsPoint)
            {
                DataTable sourceTable = newdata.Tables["RS_POINT"];
                DataTable targetTable = cache.Tables["RS_POINT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RsPointPrimaryKey pk;
                    pk.RsPointId = (System.Int32)sourceRow["RS_POINT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RsPointId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RS_POINT_ID"] = sourceRow["RS_POINT_ID"];
                    targetRow["RS_ROUND_TRIP_ID"] = sourceRow["RS_ROUND_TRIP_ID"];
                    targetRow["RS_WAYOUT_ID"] = sourceRow["RS_WAYOUT_ID"];
                    targetRow["RS_TRIPTYPE_ID"] = sourceRow["RS_TRIPTYPE_ID"];
                    targetRow["RS_OPERATIONTYPE_ID"] = sourceRow["RS_OPERATIONTYPE_ID"];
                    targetRow["RS_SHIFT_ID"] = sourceRow["RS_SHIFT_ID"];
                    targetRow["TIME_IN"] = sourceRow["TIME_IN"];
                    targetRow["TIME_OUT"] = sourceRow["TIME_OUT"];
                    targetRow["STOP_TIME"] = sourceRow["STOP_TIME"];
                    targetRow["OPERATION_TIME"] = sourceRow["OPERATION_TIME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRsRoundTrip)
            {
                DataTable sourceTable = newdata.Tables["RS_ROUND_TRIP"];
                DataTable targetTable = cache.Tables["RS_ROUND_TRIP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RsRoundTripPrimaryKey pk;
                    pk.RsRoundTripId = (System.Int32)sourceRow["RS_ROUND_TRIP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RsRoundTripId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RS_ROUND_TRIP_ID"] = sourceRow["RS_ROUND_TRIP_ID"];
                    targetRow["RS_VARIANT_ID"] = sourceRow["RS_VARIANT_ID"];
                    targetRow["INDEX"] = sourceRow["INDEX"];
                    targetRow["A_B"] = sourceRow["A_B"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRsRuntime)
            {
                DataTable sourceTable = newdata.Tables["RS_RUNTIME"];
                DataTable targetTable = cache.Tables["RS_RUNTIME"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RsRuntimePrimaryKey pk;
                    pk.RsRuntimeId = (System.Int32)sourceRow["RS_RUNTIME_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RsRuntimeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RS_RUNTIME_ID"] = sourceRow["RS_RUNTIME_ID"];
                    targetRow["RS_TRIP_ID"] = sourceRow["RS_TRIP_ID"];
                    targetRow["PERIOD_ID"] = sourceRow["PERIOD_ID"];
                    targetRow["RUNTIME"] = sourceRow["RUNTIME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRsShift)
            {
                DataTable sourceTable = newdata.Tables["RS_SHIFT"];
                DataTable targetTable = cache.Tables["RS_SHIFT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RsShiftPrimaryKey pk;
                    pk.RsShiftId = (System.Int32)sourceRow["RS_SHIFT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RsShiftId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RS_SHIFT_ID"] = sourceRow["RS_SHIFT_ID"];
                    targetRow["RS_WAYOUT_ID"] = sourceRow["RS_WAYOUT_ID"];
                    targetRow["SHIFT"] = sourceRow["SHIFT"];
                    targetRow["PREPARE_TIME"] = sourceRow["PREPARE_TIME"];
                    targetRow["BEGIN_SHIFT"] = sourceRow["BEGIN_SHIFT"];
                    targetRow["CLOSE_TIME"] = sourceRow["CLOSE_TIME"];
                    targetRow["BEGIN_TIME"] = sourceRow["BEGIN_TIME"];
                    targetRow["END_TIME"] = sourceRow["END_TIME"];
                    targetRow["TRIP_OUT"] = sourceRow["TRIP_OUT"];
                    targetRow["TRIP_OUT_0"] = sourceRow["TRIP_OUT_0"];
                    targetRow["TRIP_IN_0"] = sourceRow["TRIP_IN_0"];
                    targetRow["TRIP_IN"] = sourceRow["TRIP_IN"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRsStep)
            {
                DataTable sourceTable = newdata.Tables["RS_STEP"];
                DataTable targetTable = cache.Tables["RS_STEP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RsStepPrimaryKey pk;
                    pk.RsStepId = (System.Int32)sourceRow["RS_STEP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RsStepId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RS_STEP_ID"] = sourceRow["RS_STEP_ID"];
                    targetRow["BEGIN"] = sourceRow["BEGIN"];
                    targetRow["END"] = sourceRow["END"];
                    targetRow["RS_VARIANT"] = sourceRow["RS_VARIANT"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRsTrip)
            {
                DataTable sourceTable = newdata.Tables["RS_TRIP"];
                DataTable targetTable = cache.Tables["RS_TRIP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RsTripPrimaryKey pk;
                    pk.RsTripId = (System.Int32)sourceRow["RS_TRIP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RsTripId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RS_TRIP_ID"] = sourceRow["RS_TRIP_ID"];
                    targetRow["RS_VARIANT_ID"] = sourceRow["RS_VARIANT_ID"];
                    targetRow["ROUTE_TRIP_ID"] = sourceRow["ROUTE_TRIP_ID"];
                    targetRow["STATUS"] = sourceRow["STATUS"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRsTripstype)
            {
                DataTable sourceTable = newdata.Tables["RS_TRIPSTYPE"];
                DataTable targetTable = cache.Tables["RS_TRIPSTYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RsTripstypePrimaryKey pk;
                    pk.RsTripstypeId = (System.Int32)sourceRow["RS_TRIPSTYPE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RsTripstypeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RS_TRIPSTYPE_ID"] = sourceRow["RS_TRIPSTYPE_ID"];
                    targetRow["ALIAS"] = sourceRow["ALIAS"];
                    targetRow["LETTER"] = sourceRow["LETTER"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRsTriptype)
            {
                DataTable sourceTable = newdata.Tables["RS_TRIPTYPE"];
                DataTable targetTable = cache.Tables["RS_TRIPTYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RsTriptypePrimaryKey pk;
                    pk.RsTriptypeId = (System.Int32)sourceRow["RS_TRIPTYPE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RsTriptypeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RS_TRIPTYPE_ID"] = sourceRow["RS_TRIPTYPE_ID"];
                    targetRow["RS_VARIANT_ID"] = sourceRow["RS_VARIANT_ID"];
                    targetRow["RS_TRIPSTYPE_ID"] = sourceRow["RS_TRIPSTYPE_ID"];
                    targetRow["TRIP_A"] = sourceRow["TRIP_A"];
                    targetRow["TRIP_B"] = sourceRow["TRIP_B"];
                    targetRow["CODE"] = sourceRow["CODE"];
                    targetRow["STOP_TIME"] = sourceRow["STOP_TIME"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRsType)
            {
                DataTable sourceTable = newdata.Tables["RS_TYPE"];
                DataTable targetTable = cache.Tables["RS_TYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RsTypePrimaryKey pk;
                    pk.RsTypeId = (System.Int32)sourceRow["RS_TYPE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RsTypeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RS_TYPE_ID"] = sourceRow["RS_TYPE_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRsVariant)
            {
                DataTable sourceTable = newdata.Tables["RS_VARIANT"];
                DataTable targetTable = cache.Tables["RS_VARIANT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RsVariantPrimaryKey pk;
                    pk.RsVariantId = (System.Int32)sourceRow["RS_VARIANT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RsVariantId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RS_VARIANT_ID"] = sourceRow["RS_VARIANT_ID"];
                    targetRow["RS_ID"] = sourceRow["RS_ID"];
                    targetRow["RS_TYPE_ID"] = sourceRow["RS_TYPE_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["STATUS"] = sourceRow["STATUS"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    targetRow["GUID"] = sourceRow["GUID"];
                    targetRow["BEGIN_TIME"] = sourceRow["BEGIN_TIME"];
                    targetRow["START_POINT"] = sourceRow["START_POINT"];
                    targetRow["STOP_TIME"] = sourceRow["STOP_TIME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRsWayout)
            {
                DataTable sourceTable = newdata.Tables["RS_WAYOUT"];
                DataTable targetTable = cache.Tables["RS_WAYOUT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RsWayoutPrimaryKey pk;
                    pk.RsWayoutId = (System.Int32)sourceRow["RS_WAYOUT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RsWayoutId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RS_WAYOUT_ID"] = sourceRow["RS_WAYOUT_ID"];
                    targetRow["RS_VARIANT_ID"] = sourceRow["RS_VARIANT_ID"];
                    targetRow["GRAPHIC_ID"] = sourceRow["GRAPHIC_ID"];
                    targetRow["EXT_NUMBER"] = sourceRow["EXT_NUMBER"];
                    targetRow["BEGIN_TIME"] = sourceRow["BEGIN_TIME"];
                    targetRow["END_TIME"] = sourceRow["END_TIME"];
                    targetRow["INDEX"] = sourceRow["INDEX"];
                    targetRow["TRIP_OUT_0"] = sourceRow["TRIP_OUT_0"];
                    targetRow["TRIP_OUT"] = sourceRow["TRIP_OUT"];
                    targetRow["TRIP_IN"] = sourceRow["TRIP_IN"];
                    targetRow["TRIP_IN_0"] = sourceRow["TRIP_IN_0"];
                    targetRow["WAYOUT_ID"] = sourceRow["WAYOUT_ID"];
                    targetRow["START_POINT"] = sourceRow["START_POINT"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processRule)
            {
                DataTable sourceTable = newdata.Tables["RULE"];
                DataTable targetTable = cache.Tables["RULE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    RulePrimaryKey pk;
                    pk.RuleId = (System.Int32)sourceRow["RULE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.RuleId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["RULE_ID"] = sourceRow["RULE_ID"];
                    targetRow["NUMBER"] = sourceRow["NUMBER"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    targetRow["CLIENT_SIDE"] = sourceRow["CLIENT_SIDE"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processSchedule)
            {
                DataTable sourceTable = newdata.Tables["SCHEDULE"];
                DataTable targetTable = cache.Tables["SCHEDULE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    SchedulePrimaryKey pk;
                    pk.ScheduleId = (System.Int32)sourceRow["SCHEDULE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ScheduleId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["SCHEDULE_ID"] = sourceRow["SCHEDULE_ID"];
                    targetRow["ROUTE_ID"] = sourceRow["ROUTE_ID"];
                    targetRow["DAY_KIND_ID"] = sourceRow["DAY_KIND_ID"];
                    targetRow["EXT_NUMBER"] = sourceRow["EXT_NUMBER"];
                    targetRow["COMMENT"] = sourceRow["COMMENT"];
                    targetRow["BEGIN_TIME"] = sourceRow["BEGIN_TIME"];
                    targetRow["END_TIME"] = sourceRow["END_TIME"];
                    targetRow["WAYOUT"] = sourceRow["WAYOUT"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processScheduleDetail)
            {
                DataTable sourceTable = newdata.Tables["SCHEDULE_DETAIL"];
                DataTable targetTable = cache.Tables["SCHEDULE_DETAIL"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ScheduleDetailPrimaryKey pk;
                    pk.ScheduleDetailId = (System.Int32)sourceRow["SCHEDULE_DETAIL_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ScheduleDetailId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["SCHEDULE_DETAIL_ID"] = sourceRow["SCHEDULE_DETAIL_ID"];
                    targetRow["SCHEDULE_ID"] = sourceRow["SCHEDULE_ID"];
                    targetRow["SHIFT_ID"] = sourceRow["SHIFT_ID"];
                    targetRow["RUN_FULL"] = sourceRow["RUN_FULL"];
                    targetRow["RUN_EMPTY"] = sourceRow["RUN_EMPTY"];
                    targetRow["BEGIN_TIME"] = sourceRow["BEGIN_TIME"];
                    targetRow["END_TIME"] = sourceRow["END_TIME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processScheduleDetailInfo)
            {
                DataTable sourceTable = newdata.Tables["SCHEDULE_DETAIL_INFO"];
                DataTable targetTable = cache.Tables["SCHEDULE_DETAIL_INFO"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ScheduleDetailInfoPrimaryKey pk;
                    pk.ScheduleDetailId = (System.Int32)sourceRow["SCHEDULE_DETAIL_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ScheduleDetailId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["SCHEDULE_DETAIL_ID"] = sourceRow["SCHEDULE_DETAIL_ID"];
                    targetRow["TIME_DINNER"] = sourceRow["TIME_DINNER"];
                    targetRow["TIME_STOPWORK"] = sourceRow["TIME_STOPWORK"];
                    targetRow["TIME_BEGIN"] = sourceRow["TIME_BEGIN"];
                    targetRow["TIME_END"] = sourceRow["TIME_END"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processScheduleGeozone)
            {
                DataTable sourceTable = newdata.Tables["SCHEDULE_GEOZONE"];
                DataTable targetTable = cache.Tables["SCHEDULE_GEOZONE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ScheduleGeozonePrimaryKey pk;
                    pk.ScheduleGeozoneId = (System.Int32)sourceRow["SCHEDULE_GEOZONE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ScheduleGeozoneId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["SCHEDULE_GEOZONE_ID"] = sourceRow["SCHEDULE_GEOZONE_ID"];
                    targetRow["ROUTE_GEOZONE_ID"] = sourceRow["ROUTE_GEOZONE_ID"];
                    targetRow["TIME_IN"] = sourceRow["TIME_IN"];
                    targetRow["TIME_OUT"] = sourceRow["TIME_OUT"];
                    targetRow["TIME_DEVIATION"] = sourceRow["TIME_DEVIATION"];
                    targetRow["STATUS"] = sourceRow["STATUS"];
                    targetRow["TRIP_ID"] = sourceRow["TRIP_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processSchedulePassage)
            {
                DataTable sourceTable = newdata.Tables["SCHEDULE_PASSAGE"];
                DataTable targetTable = cache.Tables["SCHEDULE_PASSAGE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    SchedulePassagePrimaryKey pk;
                    pk.WhId = (System.Int32)sourceRow["WH_ID"];
                    pk.SpId = (System.Int32)sourceRow["SP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.WhId, pk.SpId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["WH_ID"] = sourceRow["WH_ID"];
                    targetRow["SP_ID"] = sourceRow["SP_ID"];
                    targetRow["LOG_TIME_IN"] = sourceRow["LOG_TIME_IN"];
                    targetRow["LOG_TIME_OUT"] = sourceRow["LOG_TIME_OUT"];
                    targetRow["TIME_IN"] = sourceRow["TIME_IN"];
                    targetRow["TIME_OUT"] = sourceRow["TIME_OUT"];
                    targetRow["STATUS"] = sourceRow["STATUS"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processSchedulePoint)
            {
                DataTable sourceTable = newdata.Tables["SCHEDULE_POINT"];
                DataTable targetTable = cache.Tables["SCHEDULE_POINT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    SchedulePointPrimaryKey pk;
                    pk.SchedulePointId = (System.Int32)sourceRow["SCHEDULE_POINT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.SchedulePointId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["SCHEDULE_POINT_ID"] = sourceRow["SCHEDULE_POINT_ID"];
                    targetRow["ROUTE_POINT_ID"] = sourceRow["ROUTE_POINT_ID"];
                    targetRow["TIME_IN"] = sourceRow["TIME_IN"];
                    targetRow["TIME_OUT"] = sourceRow["TIME_OUT"];
                    targetRow["TIME_DEVIATION"] = sourceRow["TIME_DEVIATION"];
                    targetRow["STATUS"] = sourceRow["STATUS"];
                    targetRow["TRIP_ID"] = sourceRow["TRIP_ID"];
                    targetRow["GEO_SEGMENT_ID"] = sourceRow["GEO_SEGMENT_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processSchedulerevent)
            {
                DataTable sourceTable = newdata.Tables["SCHEDULEREVENT"];
                DataTable targetTable = cache.Tables["SCHEDULEREVENT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    SchedulereventPrimaryKey pk;
                    pk.SchedulereventId = (System.Int32)sourceRow["SCHEDULEREVENT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.SchedulereventId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["SCHEDULEREVENT_ID"] = sourceRow["SCHEDULEREVENT_ID"];
                    targetRow["PLUGIN_NAME"] = sourceRow["PLUGIN_NAME"];
                    targetRow["COMMENT"] = sourceRow["COMMENT"];
                    targetRow["CONFIG_XML"] = sourceRow["CONFIG_XML"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processSchedulerqueue)
            {
                DataTable sourceTable = newdata.Tables["SCHEDULERQUEUE"];
                DataTable targetTable = cache.Tables["SCHEDULERQUEUE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    SchedulerqueuePrimaryKey pk;
                    pk.SchedulerqueueId = (System.Int32)sourceRow["SCHEDULERQUEUE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.SchedulerqueueId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["SCHEDULERQUEUE_ID"] = sourceRow["SCHEDULERQUEUE_ID"];
                    targetRow["SCHEDULEREVENT_ID"] = sourceRow["SCHEDULEREVENT_ID"];
                    targetRow["NEAREST_TIME"] = sourceRow["NEAREST_TIME"];
                    targetRow["CONFIG_XML"] = sourceRow["CONFIG_XML"];
                    targetRow["REPETITION_XML"] = sourceRow["REPETITION_XML"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processSeason)
            {
                DataTable sourceTable = newdata.Tables["SEASON"];
                DataTable targetTable = cache.Tables["SEASON"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    SeasonPrimaryKey pk;
                    pk.SeasonId = (System.Int32)sourceRow["SEASON_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.SeasonId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["SEASON_ID"] = sourceRow["SEASON_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["TIME_BEGIN"] = sourceRow["TIME_BEGIN"];
                    targetRow["TIME_END"] = sourceRow["TIME_END"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processSeattype)
            {
                DataTable sourceTable = newdata.Tables["SEATTYPE"];
                DataTable targetTable = cache.Tables["SEATTYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    SeattypePrimaryKey pk;
                    pk.SeattypeId = (System.Int32)sourceRow["SEATTYPE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.SeattypeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["SEATTYPE_ID"] = sourceRow["SEATTYPE_ID"];
                    targetRow["SEATTYPE_NAME"] = sourceRow["SEATTYPE_NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processSession)
            {
                DataTable sourceTable = newdata.Tables["SESSION"];
                DataTable targetTable = cache.Tables["SESSION"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    SessionPrimaryKey pk;
                    pk.SessionId = (System.Int32)sourceRow["SESSION_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.SessionId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["SESSION_ID"] = sourceRow["SESSION_ID"];
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["HOST_IP"] = sourceRow["HOST_IP"];
                    targetRow["HOST_NAME"] = sourceRow["HOST_NAME"];
                    targetRow["SESSION_START"] = sourceRow["SESSION_START"];
                    targetRow["SESSION_END"] = sourceRow["SESSION_END"];
                    targetRow["CLIENT_VERSION"] = sourceRow["CLIENT_VERSION"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processSmsType)
            {
                DataTable sourceTable = newdata.Tables["SMS_TYPE"];
                DataTable targetTable = cache.Tables["SMS_TYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    SmsTypePrimaryKey pk;
                    pk.SmsTypeId = (System.Int32)sourceRow["SMS_TYPE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.SmsTypeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["SMS_TYPE_ID"] = sourceRow["SMS_TYPE_ID"];
                    targetRow["SMS_DESCRIPTION"] = sourceRow["SMS_DESCRIPTION"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processSysdiagrams)
            {
                DataTable sourceTable = newdata.Tables["sysdiagrams"];
                DataTable targetTable = cache.Tables["sysdiagrams"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    SysdiagramsPrimaryKey pk;
                    pk.DiagramId = (System.Int32)sourceRow["diagram_id"];
                    DataRow targetRow = targetTable.Rows.Find(pk.DiagramId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["name"] = sourceRow["name"];
                    targetRow["principal_id"] = sourceRow["principal_id"];
                    targetRow["diagram_id"] = sourceRow["diagram_id"];
                    targetRow["version"] = sourceRow["version"];
                    targetRow["definition"] = sourceRow["definition"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processTaskProcessorLog)
            {
                DataTable sourceTable = newdata.Tables["TASK_PROCESSOR_LOG"];
                DataTable targetTable = cache.Tables["TASK_PROCESSOR_LOG"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    TaskProcessorLogPrimaryKey pk;
                    pk.TaskProcessorLogId = (System.Int32)sourceRow["TASK_PROCESSOR_LOG_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.TaskProcessorLogId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["TASK_PROCESSOR_LOG_ID"] = sourceRow["TASK_PROCESSOR_LOG_ID"];
                    targetRow["CONTROLLER_NUMBER"] = sourceRow["CONTROLLER_NUMBER"];
                    targetRow["VEHICLE_ID"] = sourceRow["VEHICLE_ID"];
                    targetRow["FROM"] = sourceRow["FROM"];
                    targetRow["TO"] = sourceRow["TO"];
                    targetRow["LAST_POSITION_TIME"] = sourceRow["LAST_POSITION_TIME"];
                    targetRow["START_TIME"] = sourceRow["START_TIME"];
                    targetRow["DIE_TIME"] = sourceRow["DIE_TIME"];
                    targetRow["RESULT"] = sourceRow["RESULT"];
                    targetRow["RETRY_COUNT"] = sourceRow["RETRY_COUNT"];
                    targetRow["POSITIONS_RECEIVED"] = sourceRow["POSITIONS_RECEIVED"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processTrail)
            {
                DataTable sourceTable = newdata.Tables["TRAIL"];
                DataTable targetTable = cache.Tables["TRAIL"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    TrailPrimaryKey pk;
                    pk.TrailId = (System.Int32)sourceRow["TRAIL_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.TrailId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["TRAIL_ID"] = sourceRow["TRAIL_ID"];
                    targetRow["SESSION_ID"] = sourceRow["SESSION_ID"];
                    targetRow["TRAIL_TIME"] = sourceRow["TRAIL_TIME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processTrip)
            {
                DataTable sourceTable = newdata.Tables["TRIP"];
                DataTable targetTable = cache.Tables["TRIP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    TripPrimaryKey pk;
                    pk.TripId = (System.Int32)sourceRow["TRIP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.TripId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["TRIP_ID"] = sourceRow["TRIP_ID"];
                    targetRow["TRIP_KIND_ID"] = sourceRow["TRIP_KIND_ID"];
                    targetRow["BEGIN_TIME"] = sourceRow["BEGIN_TIME"];
                    targetRow["END_TIME"] = sourceRow["END_TIME"];
                    targetRow["SCHEDULE_DETAIL_ID"] = sourceRow["SCHEDULE_DETAIL_ID"];
                    targetRow["TRIP"] = sourceRow["TRIP"];
                    targetRow["ROUTE_TRIP_ID"] = sourceRow["ROUTE_TRIP_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processTripKind)
            {
                DataTable sourceTable = newdata.Tables["TRIP_KIND"];
                DataTable targetTable = cache.Tables["TRIP_KIND"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    TripKindPrimaryKey pk;
                    pk.TripKindId = (System.Int32)sourceRow["TRIP_KIND_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.TripKindId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["TRIP_KIND_ID"] = sourceRow["TRIP_KIND_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["WORKTIME_STATUS_TYPE_ID"] = sourceRow["WORKTIME_STATUS_TYPE_ID"];
                    targetRow["WORK_TIME"] = sourceRow["WORK_TIME"];
                    targetRow["LINE_TIME"] = sourceRow["LINE_TIME"];
                    targetRow["IDLE_TIME"] = sourceRow["IDLE_TIME"];
                    targetRow["RESERVE_TIME"] = sourceRow["RESERVE_TIME"];
                    targetRow["PRODUCTION"] = sourceRow["PRODUCTION"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processVehicle)
            {
                DataTable sourceTable = newdata.Tables["VEHICLE"];
                DataTable targetTable = cache.Tables["VEHICLE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    VehiclePrimaryKey pk;
                    pk.VehicleId = (System.Int32)sourceRow["VEHICLE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.VehicleId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["VEHICLE_ID"] = sourceRow["VEHICLE_ID"];
                    targetRow["PUBLIC_NUMBER"] = sourceRow["PUBLIC_NUMBER"];
                    targetRow["GARAGE_NUMBER"] = sourceRow["GARAGE_NUMBER"];
                    targetRow["VEHICLE_TYPE"] = sourceRow["VEHICLE_TYPE"];
                    targetRow["COMMENT"] = sourceRow["COMMENT"];
                    targetRow["VEHICLE_STATUS_ID"] = sourceRow["VEHICLE_STATUS_ID"];
                    targetRow["OWNER_ID"] = sourceRow["OWNER_ID"];
                    targetRow["VALIDATOR_PRESENT"] = sourceRow["VALIDATOR_PRESENT"];
                    targetRow["SEATTYPE_ID"] = sourceRow["SEATTYPE_ID"];
                    targetRow["SITTING_BERTHS_COUNT"] = sourceRow["SITTING_BERTHS_COUNT"];
                    targetRow["STANDING_BERTHS_COUNT"] = sourceRow["STANDING_BERTHS_COUNT"];
                    targetRow["GUARD"] = sourceRow["GUARD"];
                    targetRow["FUEL_TANK"] = sourceRow["FUEL_TANK"];
                    targetRow["GRAPHIC"] = sourceRow["GRAPHIC"];
                    targetRow["GRAPHIC_BEGIN"] = sourceRow["GRAPHIC_BEGIN"];
                    targetRow["TIME_PREPARATION"] = sourceRow["TIME_PREPARATION"];
                    targetRow["VEHICLE_KIND_ID"] = sourceRow["VEHICLE_KIND_ID"];
                    targetRow["VEHICLE_REMOVED_DATE"] = sourceRow["VEHICLE_REMOVED_DATE"];
                    targetRow["DEPARTMENT"] = sourceRow["DEPARTMENT"];
                    targetRow["VIN"] = sourceRow["VIN"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processVehicleKind)
            {
                DataTable sourceTable = newdata.Tables["VEHICLE_KIND"];
                DataTable targetTable = cache.Tables["VEHICLE_KIND"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    VehicleKindPrimaryKey pk;
                    pk.VehicleKindId = (System.Int32)sourceRow["VEHICLE_KIND_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.VehicleKindId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["VEHICLE_KIND_ID"] = sourceRow["VEHICLE_KIND_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processVehicleOwner)
            {
                DataTable sourceTable = newdata.Tables["VEHICLE_OWNER"];
                DataTable targetTable = cache.Tables["VEHICLE_OWNER"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    VehicleOwnerPrimaryKey pk;
                    pk.VehicleOwnerId = (System.Int32)sourceRow["VEHICLE_OWNER_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.VehicleOwnerId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["VEHICLE_OWNER_ID"] = sourceRow["VEHICLE_OWNER_ID"];
                    targetRow["OWNER_ID"] = sourceRow["OWNER_ID"];
                    targetRow["VEHICLE_ID"] = sourceRow["VEHICLE_ID"];
                    targetRow["OWNER_NUMBER"] = sourceRow["OWNER_NUMBER"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processVehiclePicture)
            {
                DataTable sourceTable = newdata.Tables["VEHICLE_PICTURE"];
                DataTable targetTable = cache.Tables["VEHICLE_PICTURE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    VehiclePicturePrimaryKey pk;
                    pk.VehiclePictureId = (System.Int32)sourceRow["VEHICLE_PICTURE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.VehiclePictureId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["VEHICLE_PICTURE_ID"] = sourceRow["VEHICLE_PICTURE_ID"];
                    targetRow["VEHICLE_ID"] = sourceRow["VEHICLE_ID"];
                    targetRow["PICTURE"] = sourceRow["PICTURE"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processVehicleRule)
            {
                DataTable sourceTable = newdata.Tables["VEHICLE_RULE"];
                DataTable targetTable = cache.Tables["VEHICLE_RULE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    VehicleRulePrimaryKey pk;
                    pk.VehicleRuleId = (System.Int32)sourceRow["VEHICLE_RULE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.VehicleRuleId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["VEHICLE_RULE_ID"] = sourceRow["VEHICLE_RULE_ID"];
                    targetRow["VEHICLE_ID"] = sourceRow["VEHICLE_ID"];
                    targetRow["RULE_ID"] = sourceRow["RULE_ID"];
                    targetRow["VALUE"] = sourceRow["VALUE"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processVehicleStatus)
            {
                DataTable sourceTable = newdata.Tables["VEHICLE_STATUS"];
                DataTable targetTable = cache.Tables["VEHICLE_STATUS"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    VehicleStatusPrimaryKey pk;
                    pk.VehicleStatusId = (System.Int32)sourceRow["VEHICLE_STATUS_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.VehicleStatusId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["VEHICLE_STATUS_ID"] = sourceRow["VEHICLE_STATUS_ID"];
                    targetRow["VEHICLE_STATUS_NAME"] = sourceRow["VEHICLE_STATUS_NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processVehiclegroup)
            {
                DataTable sourceTable = newdata.Tables["VEHICLEGROUP"];
                DataTable targetTable = cache.Tables["VEHICLEGROUP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    VehiclegroupPrimaryKey pk;
                    pk.VehiclegroupId = (System.Int32)sourceRow["VEHICLEGROUP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.VehiclegroupId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["VEHICLEGROUP_ID"] = sourceRow["VEHICLEGROUP_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["COMMENT"] = sourceRow["COMMENT"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processVehiclegroupRule)
            {
                DataTable sourceTable = newdata.Tables["VEHICLEGROUP_RULE"];
                DataTable targetTable = cache.Tables["VEHICLEGROUP_RULE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    VehiclegroupRulePrimaryKey pk;
                    pk.VehiclegroupRuleId = (System.Int32)sourceRow["VEHICLEGROUP_RULE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.VehiclegroupRuleId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["VEHICLEGROUP_RULE_ID"] = sourceRow["VEHICLEGROUP_RULE_ID"];
                    targetRow["VEHICLEGROUP_ID"] = sourceRow["VEHICLEGROUP_ID"];
                    targetRow["RULE_ID"] = sourceRow["RULE_ID"];
                    targetRow["VALUE"] = sourceRow["VALUE"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processVehiclegroupVehicle)
            {
                DataTable sourceTable = newdata.Tables["VEHICLEGROUP_VEHICLE"];
                DataTable targetTable = cache.Tables["VEHICLEGROUP_VEHICLE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    VehiclegroupVehiclePrimaryKey pk;
                    pk.VehiclegroupId = (System.Int32)sourceRow["VEHICLEGROUP_ID"];
                    pk.VehicleId = (System.Int32)sourceRow["VEHICLE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.VehiclegroupId, pk.VehicleId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["VEHICLEGROUP_ID"] = sourceRow["VEHICLEGROUP_ID"];
                    targetRow["VEHICLE_ID"] = sourceRow["VEHICLE_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processWaybill)
            {
                DataTable sourceTable = newdata.Tables["WAYBILL"];
                DataTable targetTable = cache.Tables["WAYBILL"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    WaybillPrimaryKey pk;
                    pk.WaybillId = (System.Int32)sourceRow["WAYBILL_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.WaybillId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["WAYBILL_ID"] = sourceRow["WAYBILL_ID"];
                    targetRow["DRIVER_ID"] = sourceRow["DRIVER_ID"];
                    targetRow["COMMENT"] = sourceRow["COMMENT"];
                    targetRow["BEGIN_TIME"] = sourceRow["BEGIN_TIME"];
                    targetRow["END_TIME"] = sourceRow["END_TIME"];
                    targetRow["SHIFT_ID"] = sourceRow["SHIFT_ID"];
                    targetRow["RELEASED"] = sourceRow["RELEASED"];
                    targetRow["ACTIVE"] = sourceRow["ACTIVE"];
                    targetRow["DESTINATION"] = sourceRow["DESTINATION"];
                    targetRow["RESERVE"] = sourceRow["RESERVE"];
                    targetRow["WAYBILL_DATE"] = sourceRow["WAYBILL_DATE"];
                    targetRow["END_TIME_PLANNED"] = sourceRow["END_TIME_PLANNED"];
                    targetRow["TOTAL_PENALTY"] = sourceRow["TOTAL_PENALTY"];
                    targetRow["TRAINEE"] = sourceRow["TRAINEE"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processWaybillHeader)
            {
                DataTable sourceTable = newdata.Tables["WAYBILL_HEADER"];
                DataTable targetTable = cache.Tables["WAYBILL_HEADER"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    WaybillHeaderPrimaryKey pk;
                    pk.WaybillHeaderId = (System.Int32)sourceRow["WAYBILL_HEADER_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.WaybillHeaderId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["WAYBILL_HEADER_ID"] = sourceRow["WAYBILL_HEADER_ID"];
                    targetRow["SCHEDULE_ID"] = sourceRow["SCHEDULE_ID"];
                    targetRow["WAYBILL_DATE"] = sourceRow["WAYBILL_DATE"];
                    targetRow["VEHICLE_ID"] = sourceRow["VEHICLE_ID"];
                    targetRow["CANCELLED"] = sourceRow["CANCELLED"];
                    targetRow["COMMENT"] = sourceRow["COMMENT"];
                    targetRow["EXT_NUMBER"] = sourceRow["EXT_NUMBER"];
                    targetRow["PRINTED"] = sourceRow["PRINTED"];
                    targetRow["PRINTREASON"] = sourceRow["PRINTREASON"];
                    targetRow["INITIAL_TRIP_KIND"] = sourceRow["INITIAL_TRIP_KIND"];
                    targetRow["TRAILOR1"] = sourceRow["TRAILOR1"];
                    targetRow["TRAILOR2"] = sourceRow["TRAILOR2"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processWaybillheaderWaybillmark)
            {
                DataTable sourceTable = newdata.Tables["WAYBILLHEADER_WAYBILLMARK"];
                DataTable targetTable = cache.Tables["WAYBILLHEADER_WAYBILLMARK"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    WaybillheaderWaybillmarkPrimaryKey pk;
                    pk.WaybillheaderId = (System.Int32)sourceRow["WAYBILLHEADER_ID"];
                    pk.WaybillmarkId = (System.Int32)sourceRow["WAYBILLMARK_ID"];
                    DataRow targetRow = targetTable.Rows.Find(new object[]{pk.WaybillheaderId, pk.WaybillmarkId});
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["WAYBILLHEADER_ID"] = sourceRow["WAYBILLHEADER_ID"];
                    targetRow["MARK_DATE"] = sourceRow["MARK_DATE"];
                    targetRow["WAYBILLMARK_ID"] = sourceRow["WAYBILLMARK_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processWaybillmark)
            {
                DataTable sourceTable = newdata.Tables["WAYBILLMARK"];
                DataTable targetTable = cache.Tables["WAYBILLMARK"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    WaybillmarkPrimaryKey pk;
                    pk.WaybillmarkId = (System.Int32)sourceRow["WAYBILLMARK_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.WaybillmarkId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["WAYBILLMARK_ID"] = sourceRow["WAYBILLMARK_ID"];
                    targetRow["WAYBILLMARK_NAME"] = sourceRow["WAYBILLMARK_NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processWayout)
            {
                DataTable sourceTable = newdata.Tables["WAYOUT"];
                DataTable targetTable = cache.Tables["WAYOUT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    WayoutPrimaryKey pk;
                    pk.WayoutId = (System.Int32)sourceRow["WAYOUT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.WayoutId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ROUTE"] = sourceRow["ROUTE"];
                    targetRow["EXT_NUMBER"] = sourceRow["EXT_NUMBER"];
                    targetRow["WAYOUT_ID"] = sourceRow["WAYOUT_ID"];
                    targetRow["graphic"] = sourceRow["graphic"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processWbTrip)
            {
                DataTable sourceTable = newdata.Tables["WB_TRIP"];
                DataTable targetTable = cache.Tables["WB_TRIP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    WbTripPrimaryKey pk;
                    pk.WbTripId = (System.Int32)sourceRow["WB_TRIP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.WbTripId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["WB_TRIP_ID"] = sourceRow["WB_TRIP_ID"];
                    targetRow["TRIP"] = sourceRow["TRIP"];
                    targetRow["TRIP_ID"] = sourceRow["TRIP_ID"];
                    targetRow["STATUS"] = sourceRow["STATUS"];
                    targetRow["BEGIN_TIME"] = sourceRow["BEGIN_TIME"];
                    targetRow["END_TIME"] = sourceRow["END_TIME"];
                    targetRow["COMMENT"] = sourceRow["COMMENT"];
                    targetRow["WAYBILL_ID"] = sourceRow["WAYBILL_ID"];
                    targetRow["TRIP_KIND_ID"] = sourceRow["TRIP_KIND_ID"];
                    targetRow["WAYBILL_HEADER_ID"] = sourceRow["WAYBILL_HEADER_ID"];
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    targetRow["ACTUAL_TIME"] = sourceRow["ACTUAL_TIME"];
                    targetRow["WORKTIME_REASON_ID"] = sourceRow["WORKTIME_REASON_ID"];
                    targetRow["GEO_TRIP_ID"] = sourceRow["GEO_TRIP_ID"];
                    targetRow["PENALTY"] = sourceRow["PENALTY"];
                    targetRow["ORDER_TRIP_ID"] = sourceRow["ORDER_TRIP_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processWebLine)
            {
                DataTable sourceTable = newdata.Tables["WEB_LINE"];
                DataTable targetTable = cache.Tables["WEB_LINE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    WebLinePrimaryKey pk;
                    pk.LineId = (System.Int32)sourceRow["LINE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LineId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LINE_ID"] = sourceRow["LINE_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processWebLineVertex)
            {
                DataTable sourceTable = newdata.Tables["WEB_LINE_VERTEX"];
                DataTable targetTable = cache.Tables["WEB_LINE_VERTEX"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    WebLineVertexPrimaryKey pk;
                    pk.LineVertexId = (System.Int32)sourceRow["LINE_VERTEX_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.LineVertexId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["LINE_VERTEX_ID"] = sourceRow["LINE_VERTEX_ID"];
                    targetRow["LINE_ID"] = sourceRow["LINE_ID"];
                    targetRow["VERTEX_ID"] = sourceRow["VERTEX_ID"];
                    targetRow["ORDER"] = sourceRow["ORDER"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processWebPoint)
            {
                DataTable sourceTable = newdata.Tables["WEB_POINT"];
                DataTable targetTable = cache.Tables["WEB_POINT"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    WebPointPrimaryKey pk;
                    pk.PointId = (System.Int32)sourceRow["POINT_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.PointId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["POINT_ID"] = sourceRow["POINT_ID"];
                    targetRow["VERTEX_ID"] = sourceRow["VERTEX_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    targetRow["TYPE_ID"] = sourceRow["TYPE_ID"];
                    targetRow["OPERATOR_ID"] = sourceRow["OPERATOR_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processWebPointType)
            {
                DataTable sourceTable = newdata.Tables["WEB_POINT_TYPE"];
                DataTable targetTable = cache.Tables["WEB_POINT_TYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    WebPointTypePrimaryKey pk;
                    pk.TypeId = (System.Int32)sourceRow["TYPE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.TypeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["TYPE_ID"] = sourceRow["TYPE_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processWorkTime)
            {
                DataTable sourceTable = newdata.Tables["WORK_TIME"];
                DataTable targetTable = cache.Tables["WORK_TIME"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    WorkTimePrimaryKey pk;
                    pk.DriverId = (System.Int32)sourceRow["DRIVER_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.DriverId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["DRIVER_ID"] = sourceRow["DRIVER_ID"];
                    targetRow["YEAR"] = sourceRow["YEAR"];
                    targetRow["MONTH"] = sourceRow["MONTH"];
                    targetRow["TIME_WORK"] = sourceRow["TIME_WORK"];
                    targetRow["TIME_LINE"] = sourceRow["TIME_LINE"];
                    targetRow["TIME_IDLE"] = sourceRow["TIME_IDLE"];
                    targetRow["TIME_RESERVE"] = sourceRow["TIME_RESERVE"];
                    targetRow["TIME_NIGHT"] = sourceRow["TIME_NIGHT"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processWorkplaceConfig)
            {
                DataTable sourceTable = newdata.Tables["WORKPLACE_CONFIG"];
                DataTable targetTable = cache.Tables["WORKPLACE_CONFIG"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    WorkplaceConfigPrimaryKey pk;
                    pk.Id = (System.Int32)sourceRow["ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.Id);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ID"] = sourceRow["ID"];
                    targetRow["PARAMETER_NAME"] = sourceRow["PARAMETER_NAME"];
                    targetRow["PARAMETER_VALUE"] = sourceRow["PARAMETER_VALUE"];
                    targetRow["COMMENTS"] = sourceRow["COMMENTS"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processWorkstation)
            {
                DataTable sourceTable = newdata.Tables["WORKSTATION"];
                DataTable targetTable = cache.Tables["WORKSTATION"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    WorkstationPrimaryKey pk;
                    pk.WorkstationId = (System.Int32)sourceRow["WORKSTATION_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.WorkstationId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["WORKSTATION_ID"] = sourceRow["WORKSTATION_ID"];
                    targetRow["PLACE_NUM"] = sourceRow["PLACE_NUM"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["CELLPHONE"] = sourceRow["CELLPHONE"];
                    targetRow["FIO"] = sourceRow["FIO"];
                    targetRow["STATUS"] = sourceRow["STATUS"];
                    targetRow["MACHINE_NAME"] = sourceRow["MACHINE_NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processWorktimeReason)
            {
                DataTable sourceTable = newdata.Tables["WORKTIME_REASON"];
                DataTable targetTable = cache.Tables["WORKTIME_REASON"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    WorktimeReasonPrimaryKey pk;
                    pk.WorktimeReasonId = (System.Int32)sourceRow["WORKTIME_REASON_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.WorktimeReasonId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["WORKTIME_REASON_ID"] = sourceRow["WORKTIME_REASON_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["WORKTIME_STATUS_TYPE_ID"] = sourceRow["WORKTIME_STATUS_TYPE_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processWorktimeStatusType)
            {
                DataTable sourceTable = newdata.Tables["WORKTIME_STATUS_TYPE"];
                DataTable targetTable = cache.Tables["WORKTIME_STATUS_TYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    WorktimeStatusTypePrimaryKey pk;
                    pk.WorktimeStatusTypeId = (System.Int32)sourceRow["WORKTIME_STATUS_TYPE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.WorktimeStatusTypeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["WORKTIME_STATUS_TYPE_ID"] = sourceRow["WORKTIME_STATUS_TYPE_ID"];
                    targetRow["WORKTIME_STATUS_TYPE_NAME"] = sourceRow["WORKTIME_STATUS_TYPE_NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processZonePrimitive)
            {
                DataTable sourceTable = newdata.Tables["ZONE_PRIMITIVE"];
                DataTable targetTable = cache.Tables["ZONE_PRIMITIVE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ZonePrimitivePrimaryKey pk;
                    pk.PrimitiveId = (System.Int32)sourceRow["PRIMITIVE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.PrimitiveId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["PRIMITIVE_ID"] = sourceRow["PRIMITIVE_ID"];
                    targetRow["PRIMITIVE_TYPE"] = sourceRow["PRIMITIVE_TYPE"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    targetRow["RADIUS"] = sourceRow["RADIUS"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processZonePrimitiveVertex)
            {
                DataTable sourceTable = newdata.Tables["ZONE_PRIMITIVE_VERTEX"];
                DataTable targetTable = cache.Tables["ZONE_PRIMITIVE_VERTEX"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ZonePrimitiveVertexPrimaryKey pk;
                    pk.PrimitiveVertexId = (System.Int32)sourceRow["PRIMITIVE_VERTEX_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.PrimitiveVertexId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["PRIMITIVE_VERTEX_ID"] = sourceRow["PRIMITIVE_VERTEX_ID"];
                    targetRow["PRIMITIVE_ID"] = sourceRow["PRIMITIVE_ID"];
                    targetRow["VERTEX_ID"] = sourceRow["VERTEX_ID"];
                    targetRow["ORDER"] = sourceRow["ORDER"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processZoneType)
            {
                DataTable sourceTable = newdata.Tables["ZONE_TYPE"];
                DataTable targetTable = cache.Tables["ZONE_TYPE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ZoneTypePrimaryKey pk;
                    pk.ZoneTypeId = (System.Int32)sourceRow["ZONE_TYPE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ZoneTypeId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ZONE_TYPE_ID"] = sourceRow["ZONE_TYPE_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processZoneVehicle)
            {
                DataTable sourceTable = newdata.Tables["ZONE_VEHICLE"];
                DataTable targetTable = cache.Tables["ZONE_VEHICLE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ZoneVehiclePrimaryKey pk;
                    pk.ZoneVehicleId = (System.Int32)sourceRow["ZONE_VEHICLE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ZoneVehicleId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ZONE_VEHICLE_ID"] = sourceRow["ZONE_VEHICLE_ID"];
                    targetRow["ZONE_ID"] = sourceRow["ZONE_ID"];
                    targetRow["VEHICLE_ID"] = sourceRow["VEHICLE_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processZoneVehiclegroup)
            {
                DataTable sourceTable = newdata.Tables["ZONE_VEHICLEGROUP"];
                DataTable targetTable = cache.Tables["ZONE_VEHICLEGROUP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ZoneVehiclegroupPrimaryKey pk;
                    pk.ZoneVehiclegroupId = (System.Int32)sourceRow["ZONE_VEHICLEGROUP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ZoneVehiclegroupId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ZONE_VEHICLEGROUP_ID"] = sourceRow["ZONE_VEHICLEGROUP_ID"];
                    targetRow["ZONE_ID"] = sourceRow["ZONE_ID"];
                    targetRow["VEHICLEGROUP_ID"] = sourceRow["VEHICLEGROUP_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processZonegroup)
            {
                DataTable sourceTable = newdata.Tables["ZONEGROUP"];
                DataTable targetTable = cache.Tables["ZONEGROUP"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ZonegroupPrimaryKey pk;
                    pk.ZonegroupId = (System.Int32)sourceRow["ZONEGROUP_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ZonegroupId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ZONEGROUP_ID"] = sourceRow["ZONEGROUP_ID"];
                    targetRow["NAME"] = sourceRow["NAME"];
                    targetRow["DESCRIPTION"] = sourceRow["DESCRIPTION"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            if (processZonegroupZone)
            {
                DataTable sourceTable = newdata.Tables["ZONEGROUP_ZONE"];
                DataTable targetTable = cache.Tables["ZONEGROUP_ZONE"];
                foreach (DataRow sourceRow in sourceTable.Rows)
                {
                    ZonegroupZonePrimaryKey pk;
                    pk.ZonegroupZoneId = (System.Int32)sourceRow["ZONEGROUP_ZONE_ID"];
                    DataRow targetRow = targetTable.Rows.Find(pk.ZonegroupZoneId);
                    if (targetRow == null)
                    {
                        targetRow = targetTable.NewRow();
                    }
                    else
                    {
                        targetRow.BeginEdit();
                    }
                    // For each column in table
                    targetRow["ZONEGROUP_ZONE_ID"] = sourceRow["ZONEGROUP_ZONE_ID"];
                    targetRow["ZONEGROUP_ID"] = sourceRow["ZONEGROUP_ID"];
                    targetRow["ZONE_ID"] = sourceRow["ZONE_ID"];
                    if (targetRow.RowState == DataRowState.Detached)
                    {
                        targetTable.Rows.Add(targetRow); // constraints are disabled
                    }
                    else
                    {
                        targetRow.EndEdit();
                    }
                    targetRow.AcceptChanges();
                }
            }
            DatabaseSchema.MakeSchema(cache);
            //cache.EnforceConstraints = oldForce;
        }
    }
}
