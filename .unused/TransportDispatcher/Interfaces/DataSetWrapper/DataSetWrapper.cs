using System;
using System.Data;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary>
	/// Summary description for DatasetWrapper.
	/// </summary>
	public class DataSetWrapper
	{
		protected DataSet dataSet;
		protected DataTableCollectionWrapper dataTableCollectionWrapper;
		public DataTableCollectionWrapper Tables
		{
			get
			{
				return dataTableCollectionWrapper;
			}
		}
		public DataSetWrapper()
		{
			this.dataSet = new DataSet();
			this.dataTableCollectionWrapper = new DataTableCollectionWrapper(this, dataSet.Tables);
		}
		public DataSetWrapper(DataSet dataSet)
		{
			// Store upper objects
			this.dataSet = dataSet;
			// Create child wrappers
			this.dataTableCollectionWrapper = new DataTableCollectionWrapper(this, dataSet.Tables);
		}
	}
}
