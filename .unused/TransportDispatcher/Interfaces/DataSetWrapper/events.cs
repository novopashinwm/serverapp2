using System;
using System.Data;
using System.Data.Common;

namespace FORIS.TSS.BusinessLogic
{
	public class RowEventArgsBase
	{
		protected DataRowWrapper dataRow;
		public DataRowWrapper Row
		{
			get
			{
				return dataRow;
			}
			set
			{
				dataRow = value;
			}
		}
		public RowEventArgsBase()
		{
		}
		public RowEventArgsBase(DataRowWrapper dataRow)
		{
			this.dataRow = dataRow;
		}
	}
	public class RowWrapperAddedEventArgs : RowEventArgsBase
	{
		public RowWrapperAddedEventArgs(DataRowWrapper dataRow)
			: base(dataRow)
		{
		}
	}
	[Serializable]
	public delegate void RowWrapperAddedEventHandler(object sender, RowWrapperAddedEventArgs e);

	public class RowWrapperDeletedEventArgs : RowEventArgsBase
	{
		public RowWrapperDeletedEventArgs(DataRowWrapper dataRow)
			: base(dataRow)
		{
		}
	}
	[Serializable]
	public delegate void RowWrapperDeletedEventHandler(object sender, RowWrapperDeletedEventArgs e);

	public class RowWrapperModifiedEventArgs : RowEventArgsBase
	{
		public RowWrapperModifiedEventArgs(DataRowWrapper dataRow)
			: base(dataRow)
		{
		}
	}
	[Serializable]
	public delegate void RowWrapperModifiedEventHandler(object sender, RowWrapperModifiedEventArgs e);
}
