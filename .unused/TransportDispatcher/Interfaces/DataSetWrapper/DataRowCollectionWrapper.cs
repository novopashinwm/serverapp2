using System;
using System.Data;
using System.Collections;

namespace FORIS.TSS.BusinessLogic
{
    /// <summary>
    /// Strongly typed collection of FORIS.TSS.BusinessLogic.DataRowWrapper.
    /// </summary>
    public class DataRowCollectionWrapper : System.Collections.CollectionBase
    {
        protected DataTableWrapper dataTableWrapper;
		protected DataRowCollection dataRowCollection;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DataRowCollectionWrapper() : 
                base()
        {
			dataTableWrapper = null;
			dataRowCollection = null;
        }

		public DataRowCollectionWrapper(DataTableWrapper dataTableWrapper, DataRowCollection dataRowCollection) :
			base()
		{
			// Store upper objects
			this.dataTableWrapper = dataTableWrapper;
			this.dataRowCollection = dataRowCollection;
			// Create child wrappers
			foreach (DataRow row in dataRowCollection)
			{
				DataRowWrapper dataRowWrapper = new DataRowWrapper(this, row);
				this.InnerList.Add(dataRowWrapper);
			}
		}
        
        /// <summary>
        /// Gets or sets the value of the FORIS.TSS.BusinessLogic.DataRowWrapper at a specific position in the DataRowCollectionWrapper.
        /// </summary>
        public FORIS.TSS.BusinessLogic.DataRowWrapper this[int index]
        {
            get
            {
                return ((FORIS.TSS.BusinessLogic.DataRowWrapper)(this.List[index]));
            }
            set
            {
                this.List[index] = value;
            }
        }
        
        /// <summary>
        /// Append a FORIS.TSS.BusinessLogic.DataRowWrapper entry to this collection.
        /// </summary>
        /// <param name="value">FORIS.TSS.BusinessLogic.DataRowWrapper instance.</param>
        /// <returns>The position into which the new element was inserted.</returns>
        public int Add(FORIS.TSS.BusinessLogic.DataRowWrapper value)
        {
			// Add row to underlaying dataset
			DataRow dataRow = value.InternalGetDataRow();
			this.dataRowCollection.Add(dataRow);
			// Add wrapper to this wrappers
			int pos = this.List.Add(value);
			// Fire event
			RowWrapperAddedEventArgs e = new RowWrapperAddedEventArgs(value);
			OnRowAdded(this, e);

            return pos;
        }

		public event RowWrapperAddedEventHandler onRowWrapperAdded;

		protected virtual void OnRowAdded(object sender, RowWrapperAddedEventArgs e)
		{
			if (onRowWrapperAdded != null)
			{
				onRowWrapperAdded(sender, e);
			}
		}
        
        /// <summary>
        /// Determines whether a specified FORIS.TSS.BusinessLogic.DataRowWrapper instance is in this collection.
        /// </summary>
        /// <param name="value">FORIS.TSS.BusinessLogic.DataRowWrapper instance to search for.</param>
        /// <returns>True if the FORIS.TSS.BusinessLogic.DataRowWrapper instance is in the collection; otherwise false.</returns>
        public bool Contains(FORIS.TSS.BusinessLogic.DataRowWrapper value)
        {
            return this.List.Contains(value);
        }
        
        /// <summary>
        /// Retrieve the index a specified FORIS.TSS.BusinessLogic.DataRowWrapper instance is in this collection.
        /// </summary>
        /// <param name="value">FORIS.TSS.BusinessLogic.DataRowWrapper instance to find.</param>
        /// <returns>The zero-based index of the specified FORIS.TSS.BusinessLogic.DataRowWrapper instance. If the object is not found, the return value is -1.</returns>
        public int IndexOf(FORIS.TSS.BusinessLogic.DataRowWrapper value)
        {
            return this.List.IndexOf(value);
        }
        
        /// <summary>
        /// Removes a specified FORIS.TSS.BusinessLogic.DataRowWrapper instance from this collection.
        /// </summary>
        /// <param name="value">The FORIS.TSS.BusinessLogic.DataRowWrapper instance to remove.</param>
        public void Remove(FORIS.TSS.BusinessLogic.DataRowWrapper value)
        {
            this.List.Remove(value);
        }
        
        /// <summary>
        /// Returns an enumerator that can iterate through the FORIS.TSS.BusinessLogic.DataRowWrapper instance.
        /// </summary>
        /// <returns>An FORIS.TSS.BusinessLogic.DataRowWrapper's enumerator.</returns>
        public new DataRowCollectionWrapperEnumerator GetEnumerator()
        {
            return new DataRowCollectionWrapperEnumerator(this);
        }
        
        /// <summary>
        /// Insert a FORIS.TSS.BusinessLogic.DataRowWrapper instance into this collection at a specified index.
        /// </summary>
        /// <param name="index">Zero-based index.</param>
        /// <param name="value">The FORIS.TSS.BusinessLogic.DataRowWrapper instance to insert.</param>
        public void Insert(int index, FORIS.TSS.BusinessLogic.DataRowWrapper value)
        {
            this.List.Insert(index, value);
        }
        
        /// <summary>
        /// Strongly typed enumerator of FORIS.TSS.BusinessLogic.DataRowWrapper.
        /// </summary>
        public class DataRowCollectionWrapperEnumerator : object, System.Collections.IEnumerator
        {
            
            /// <summary>
            /// Current index
            /// </summary>
            private int _index;
            
            /// <summary>
            /// Current element pointed to.
            /// </summary>
            private FORIS.TSS.BusinessLogic.DataRowWrapper _currentElement;
            
            /// <summary>
            /// Collection to enumerate.
            /// </summary>
            private DataRowCollectionWrapper _collection;
            
            /// <summary>
            /// Default constructor for enumerator.
            /// </summary>
            /// <param name="collection">Instance of the collection to enumerate.</param>
            internal DataRowCollectionWrapperEnumerator(DataRowCollectionWrapper collection)
            {
                _index = -1;
                _collection = collection;
            }
            
            /// <summary>
            /// Gets the FORIS.TSS.BusinessLogic.DataRowWrapper object in the enumerated DataRowCollectionWrapper currently indexed by this instance.
            /// </summary>
            public FORIS.TSS.BusinessLogic.DataRowWrapper Current
            {
                get
                {
                    if (((_index == -1) 
                                || (_index >= _collection.Count)))
                    {
                        throw new System.IndexOutOfRangeException("Enumerator not started.");
                    }
                    else
                    {
                        return _currentElement;
                    }
                }
            }
            
            /// <summary>
            /// Gets the current element in the collection.
            /// </summary>
            object IEnumerator.Current
            {
                get
                {
                    if (((_index == -1) 
                                || (_index >= _collection.Count)))
                    {
                        throw new System.IndexOutOfRangeException("Enumerator not started.");
                    }
                    else
                    {
                        return _currentElement;
                    }
                }
            }
            
            /// <summary>
            /// Reset the cursor, so it points to the beginning of the enumerator.
            /// </summary>
            public void Reset()
            {
                _index = -1;
                _currentElement = null;
            }
            
            /// <summary>
            /// Advances the enumerator to the next queue of the enumeration, if one is currently available.
            /// </summary>
            /// <returns>true, if the enumerator was succesfully advanced to the next queue; false, if the enumerator has reached the end of the enumeration.</returns>
            public bool MoveNext()
            {
                if ((_index 
                            < (_collection.Count - 1)))
                {
                    _index = (_index + 1);
                    _currentElement = this._collection[_index];
                    return true;
                }
                _index = _collection.Count;
                return false;
            }
        }
    }
}
