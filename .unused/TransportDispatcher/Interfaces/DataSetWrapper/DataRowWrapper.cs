using System;
using System.Data;
using System.Collections;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary>
	/// Summary description for DataRowWrapper.
	/// </summary>
	public class DataRowWrapper : IEnumerable
	{
		protected DataRowCollectionWrapper dataRowCollectionWrapper;
		protected DataRow dataRow;
		public DataRowWrapper()
		{
			this.dataRowCollectionWrapper = null;
			this.dataRow = null;
		}
		public DataRowWrapper(DataRowCollectionWrapper dataRowCollectionWrapper, DataRow dataRow)
		{
			// Store upper objects
			this.dataRowCollectionWrapper = dataRowCollectionWrapper;
			this.dataRow = dataRow;
		}
		public DataRowState RowState
		{
			get
			{
				return dataRow.RowState; 
			}
		}
		/// <summary>
		/// </summary>
		/// <returns>Used from datarow collection when adding new row</returns>
		internal DataRow InternalGetDataRow()
		{
			return this.dataRow;
		}

		public void Delete()
		{
			// Delete underlaying object
			this.dataRow.Delete();
			// Fire event
			RowWrapperDeletedEventArgs e = new RowWrapperDeletedEventArgs(this);
			OnRowDeleted(this, e);
		}

		public event RowWrapperDeletedEventHandler onRowWrapperDeleted;

		protected virtual void OnRowDeleted(object sender, RowWrapperDeletedEventArgs e)
		{
			if (onRowWrapperDeleted != null)
			{
				onRowWrapperDeleted(sender, e);
			}
		}
		/// <summary>
		/// Gets or sets the value of the FORIS.TSS.BusinessLogic.DataFieldCollection at a specific position in the DataFieldCollectionWrapper.
		/// </summary>
		public object this[int index]
		{
			get
			{
				return dataRow[index];
			}
			set
			{
				if (dataRow[index].Equals(value) == false)
				{
					dataRow[index] = value;
					// Fire event
					RowWrapperModifiedEventArgs rowWrapperModifiedEventArgs = new RowWrapperModifiedEventArgs(this);
					OnRowUpdated(this, rowWrapperModifiedEventArgs);
				}
			}
		}

		public object this[string fieldName]
		{
			get
			{
				return dataRow[fieldName];
			}
			set
			{
				if (dataRow[fieldName].Equals(value) == false)
				{
					dataRow[fieldName] = value;
					// Fire event
					RowWrapperModifiedEventArgs rowWrapperModifiedEventArgs = new RowWrapperModifiedEventArgs(this);
					OnRowUpdated(this, rowWrapperModifiedEventArgs);
				}
			}
		}
        
		public event RowWrapperModifiedEventHandler onRowWrapperModified;

		protected virtual void OnRowUpdated(object sender, RowWrapperModifiedEventArgs e)
		{
			if (onRowWrapperModified != null)
			{
				onRowWrapperModified(sender, e);
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the FORIS.TSS.BusinessLogic.DataFieldCollection instance.
		/// </summary>
		/// <returns>An FORIS.TSS.BusinessLogic.DataFieldCollection's enumerator.</returns>
		IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return new DataFieldCollectionWrapperEnumerator(this);
		}
        
		/// <summary>
		/// Strongly typed enumerator of FORIS.TSS.BusinessLogic.DataFieldCollection.
		/// </summary>
		public class DataFieldCollectionWrapperEnumerator : object, System.Collections.IEnumerator
		{
            
			/// <summary>
			/// Current index
			/// </summary>
			private int _index;
            
			/// <summary>
			/// Current element pointed to.
			/// </summary>
			private object _currentElement;
            
			/// <summary>
			/// Collection to enumerate.
			/// </summary>
			private DataRowWrapper _collection;
            
			/// <summary>
			/// Default constructor for enumerator.
			/// </summary>
			/// <param name="collection">Instance of the collection to enumerate.</param>
			internal DataFieldCollectionWrapperEnumerator(DataRowWrapper collection)
			{
				_index = -1;
				_collection = collection;
			}
            
			protected int GetItemsCount()
			{
				return _collection.dataRow.Table.Columns.Count;
			}
			/// <summary>
			/// Gets the FORIS.TSS.BusinessLogic.DataFieldCollection object in the enumerated DataFieldCollectionWrapper currently indexed by this instance.
			/// </summary>
			public object Current
			{
				get
				{
					if (((_index == -1) 
						|| (_index >= GetItemsCount())))
					{
						throw new System.IndexOutOfRangeException("Enumerator not started.");
					}
					else
					{
						return _currentElement;
					}
				}
			}
            
			/// <summary>
			/// Gets the current element in the collection.
			/// </summary>
			object IEnumerator.Current
			{
				get
				{
					if (((_index == -1) 
						|| (_index >= GetItemsCount())))
					{
						throw new System.IndexOutOfRangeException("Enumerator not started.");
					}
					else
					{
						return _currentElement;
					}
				}
			}
            
			/// <summary>
			/// Reset the cursor, so it points to the beginning of the enumerator.
			/// </summary>
			public void Reset()
			{
				_index = -1;
				_currentElement = null;
			}
            
			/// <summary>
			/// Advances the enumerator to the next queue of the enumeration, if one is currently available.
			/// </summary>
			/// <returns>true, if the enumerator was succesfully advanced to the next queue; false, if the enumerator has reached the end of the enumeration.</returns>
			public bool MoveNext()
			{
				if ((_index 
					< (GetItemsCount() - 1)))
				{
					_index = (_index + 1);
					_currentElement = this._collection[_index];
					return true;
				}
				_index = GetItemsCount();
				return false;
			}
		}
	}
}
