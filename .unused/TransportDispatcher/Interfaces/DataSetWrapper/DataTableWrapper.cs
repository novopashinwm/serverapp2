using System;
using System.Data;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary>
	/// Summary description for DataTableWrapper.
	/// </summary>
	public class DataTableWrapper
	{
		protected DataTableCollectionWrapper dataTableCollectionWrapper;
		protected DataTable dataTable;
		protected DataRowCollectionWrapper dataRowWrapperCollection;
		public DataRowCollectionWrapper Rows
		{
			get
			{
				return dataRowWrapperCollection;
			}
		}
		public DataTableWrapper()
		{
			dataTableCollectionWrapper = null;
			dataTable = null;
			dataRowWrapperCollection = new DataRowCollectionWrapper();
		}
		public DataTableWrapper(DataTableCollectionWrapper DataTableCollectionWrapper, DataTable dataTable)
		{
			// Store upper objects
			this.dataTableCollectionWrapper = DataTableCollectionWrapper;
			this.dataTable = dataTable;
			// Create child wrappers
			dataRowWrapperCollection = new DataRowCollectionWrapper(this, dataTable.Rows);
		}
		public DataRowWrapper NewRow()
		{
			DataRow dataRow = dataTable.NewRow();
			DataRowWrapper dataRowWrapper = new DataRowWrapper(dataRowWrapperCollection, dataRow);
			return dataRowWrapper;
		}
	}
}
