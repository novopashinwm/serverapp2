using System;
using System.Data;
using System.Collections;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary>
    /// Strongly typed collection of FORIS.TSS.BusinessLogic.DataTableWrapper.
    /// </summary>
    public class DataTableCollectionWrapper : System.Collections.CollectionBase
    {
		protected DataSetWrapper dataSetWrapper;
		protected DataTableCollection dataTableCollection;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DataTableCollectionWrapper() : 
                base()
        {
			dataSetWrapper = null;
			DataSet ds = new DataSet();
			dataTableCollection = ds.Tables;
        }

		/// <summary>
		/// </summary>
		public DataTableCollectionWrapper(DataSetWrapper dataSetWrapper, DataTableCollection dataTableCollection) : 
			base()
		{
			// Store upper objects
			this.dataSetWrapper = dataSetWrapper;
			this.dataTableCollection = dataTableCollection;
			// Create child wrappers
			foreach (DataTable table in dataTableCollection)
			{
				DataTableWrapper dataTableWrapper = new DataTableWrapper(this, table);
				this.InnerList.Add(dataTableWrapper);
			}
		}
        
        /// <summary>
        /// Gets or sets the value of the FORIS.TSS.BusinessLogic.DataTableWrapper at a specific position in the DataTableCollectionWrapper.
        /// </summary>
        public FORIS.TSS.BusinessLogic.DataTableWrapper this[int index]
        {
            get
            {
                return ((FORIS.TSS.BusinessLogic.DataTableWrapper)(this.List[index]));
            }
            set
            {
                this.List[index] = value;
            }
        }
        
        /// <summary>
        /// Append a FORIS.TSS.BusinessLogic.DataTableWrapper entry to this collection.
        /// </summary>
        /// <param name="value">FORIS.TSS.BusinessLogic.DataTableWrapper instance.</param>
        /// <returns>The position into which the new element was inserted.</returns>
        public int Add(FORIS.TSS.BusinessLogic.DataTableWrapper value)
        {
            return this.List.Add(value);
        }
        
        /// <summary>
        /// Determines whether a specified FORIS.TSS.BusinessLogic.DataTableWrapper instance is in this collection.
        /// </summary>
        /// <param name="value">FORIS.TSS.BusinessLogic.DataTableWrapper instance to search for.</param>
        /// <returns>True if the FORIS.TSS.BusinessLogic.DataTableWrapper instance is in the collection; otherwise false.</returns>
        public bool Contains(FORIS.TSS.BusinessLogic.DataTableWrapper value)
        {
            return this.List.Contains(value);
        }
        
        /// <summary>
        /// Retrieve the index a specified FORIS.TSS.BusinessLogic.DataTableWrapper instance is in this collection.
        /// </summary>
        /// <param name="value">FORIS.TSS.BusinessLogic.DataTableWrapper instance to find.</param>
        /// <returns>The zero-based index of the specified FORIS.TSS.BusinessLogic.DataTableWrapper instance. If the object is not found, the return value is -1.</returns>
        public int IndexOf(FORIS.TSS.BusinessLogic.DataTableWrapper value)
        {
            return this.List.IndexOf(value);
        }
        
        /// <summary>
        /// Removes a specified FORIS.TSS.BusinessLogic.DataTableWrapper instance from this collection.
        /// </summary>
        /// <param name="value">The FORIS.TSS.BusinessLogic.DataTableWrapper instance to remove.</param>
        public void Remove(FORIS.TSS.BusinessLogic.DataTableWrapper value)
        {
            this.List.Remove(value);
        }
        
        /// <summary>
        /// Returns an enumerator that can iterate through the FORIS.TSS.BusinessLogic.DataTableWrapper instance.
        /// </summary>
        /// <returns>An FORIS.TSS.BusinessLogic.DataTableWrapper's enumerator.</returns>
        public new DataTableCollectionWrapperEnumerator GetEnumerator()
        {
            return new DataTableCollectionWrapperEnumerator(this);
        }
        
        /// <summary>
        /// Insert a FORIS.TSS.BusinessLogic.DataTableWrapper instance into this collection at a specified index.
        /// </summary>
        /// <param name="index">Zero-based index.</param>
        /// <param name="value">The FORIS.TSS.BusinessLogic.DataTableWrapper instance to insert.</param>
        public void Insert(int index, FORIS.TSS.BusinessLogic.DataTableWrapper value)
        {
            this.List.Insert(index, value);
        }
        
        /// <summary>
        /// Strongly typed enumerator of FORIS.TSS.BusinessLogic.DataTableWrapper.
        /// </summary>
        public class DataTableCollectionWrapperEnumerator : object, System.Collections.IEnumerator
        {
            
            /// <summary>
            /// Current index
            /// </summary>
            private int _index;
            
            /// <summary>
            /// Current element pointed to.
            /// </summary>
            private FORIS.TSS.BusinessLogic.DataTableWrapper _currentElement;
            
            /// <summary>
            /// Collection to enumerate.
            /// </summary>
            private DataTableCollectionWrapper _collection;
            
            /// <summary>
            /// Default constructor for enumerator.
            /// </summary>
            /// <param name="collection">Instance of the collection to enumerate.</param>
            internal DataTableCollectionWrapperEnumerator(DataTableCollectionWrapper collection)
            {
                _index = -1;
                _collection = collection;
            }
            
            /// <summary>
            /// Gets the FORIS.TSS.BusinessLogic.DataTableWrapper object in the enumerated DataTableCollectionWrapper currently indexed by this instance.
            /// </summary>
            public FORIS.TSS.BusinessLogic.DataTableWrapper Current
            {
                get
                {
                    if (((_index == -1) 
                                || (_index >= _collection.Count)))
                    {
                        throw new System.IndexOutOfRangeException("Enumerator not started.");
                    }
                    else
                    {
                        return _currentElement;
                    }
                }
            }
            
            /// <summary>
            /// Gets the current element in the collection.
            /// </summary>
            object IEnumerator.Current
            {
                get
                {
                    if (((_index == -1) 
                                || (_index >= _collection.Count)))
                    {
                        throw new System.IndexOutOfRangeException("Enumerator not started.");
                    }
                    else
                    {
                        return _currentElement;
                    }
                }
            }
            
            /// <summary>
            /// Reset the cursor, so it points to the beginning of the enumerator.
            /// </summary>
            public void Reset()
            {
                _index = -1;
                _currentElement = null;
            }
            
            /// <summary>
            /// Advances the enumerator to the next queue of the enumeration, if one is currently available.
            /// </summary>
            /// <returns>true, if the enumerator was succesfully advanced to the next queue; false, if the enumerator has reached the end of the enumeration.</returns>
            public bool MoveNext()
            {
                if ((_index 
                            < (_collection.Count - 1)))
                {
                    _index = (_index + 1);
                    _currentElement = this._collection[_index];
                    return true;
                }
                _index = _collection.Count;
                return false;
            }
        }
    }
}
