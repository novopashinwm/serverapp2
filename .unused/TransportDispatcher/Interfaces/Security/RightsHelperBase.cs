using System;
using System.Data;
using System.Text;
using System.Diagnostics;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary>
	/// Summary description for RightsHelperBase.
	/// </summary>
	public class RightsHelperBase
	{
		/// <summary>
		/// ���������� �������� "�����" �� ������ � ������� ��� ��������
		/// </summary>
		/// <param name="row">������ ������� RIGHT_OPERATOR ��� RIGHT_OPERATORGROUP</param>
		/// <returns>�������� "�����" �� ������ � ������� ��� ��������</returns>
		public static int IsAllowed(DataRow row)
		{
			if (row == null || row.RowState == DataRowState.Deleted)
			{
				// ������ �� �������, ����� �� ���������������
				return 0;
			}
			if ((bool)row["ALLOWED"])
			{
				// ������ "��������"
				return 1;
			}
			else
			{
				// ������ "��������"
				return 2;
			}
		}
		public static void SetRight(DataSet dataset, string table1, int key1, string table2, int key2, int newstate, int right_id)
		{
			DataTable table = dataset.Tables[table1 + "_" + table2];
			Debug.Assert(table.PrimaryKey.Length == 3);
			Debug.Assert(table.PrimaryKey[0] == table.Columns[table1 + "_ID"]);
			Debug.Assert(table.PrimaryKey[1] == table.Columns[table2 + "_ID"]);
			Debug.Assert(table.PrimaryKey[2] == table.Columns["RIGHT_ID"]);
			DataRow row = table.Rows.Find(new object[]{key1, key2, right_id});
			if (row == null)
			{
				// there is no such row in the set of rows
				switch (newstate)
				{
					case 0: // unknown
						return;
					case 1: // enabled
					case 2: // disabled
						StringBuilder filterExpression = new StringBuilder(100);
						filterExpression.AppendFormat("{0}_ID={1}", table1, key1);
						filterExpression.Append(" AND ");
						filterExpression.AppendFormat("{0}_ID={1}", table2, key2);
						filterExpression.Append(" AND ");
						filterExpression.AppendFormat("RIGHT_ID={0}", right_id);
						StringBuilder sortExpression = new StringBuilder(100);
						sortExpression.AppendFormat("{0}_ID", table1);
						sortExpression.Append(",");
						sortExpression.AppendFormat("{0}_ID", table2);
						sortExpression.Append(",");
						sortExpression.AppendFormat("RIGHT_ID");
						DataRow[] rows = table.Select(filterExpression.ToString(), sortExpression.ToString(), DataViewRowState.Deleted);
						if (rows.Length == 0)
						{
							row = table.NewRow();
							row[table1 + "_ID"] = key1;
							row[table2 + "_ID"] = key2;
							row["RIGHT_ID"] = right_id;
							row["ALLOWED"] = 2 - newstate;
							table.Rows.Add(row);
						}
						else
						{
							row = rows[0];
							row.RejectChanges();
							row["ALLOWED"] = 2 - newstate;
						}
						break;
				}
			}
			else
			{
				// there is such row
				switch (newstate)
				{
					case 0:
						row.Delete();
						return;
					case 1:
					case 2:
						row["ALLOWED"] = 2 - newstate;
						break;
				}
			}
		}

		public static void SetRightSystem(DataSet dataset, int right_id, int newstate, string table, int subject_id)
		{
			DataTable t = dataset.Tables["RIGHT_" + table];
			Debug.Assert(t.PrimaryKey.Length == 2);
			Debug.Assert(t.PrimaryKey[0] == t.Columns["RIGHT_ID"]);
			Debug.Assert(t.PrimaryKey[1] == t.Columns[table + "_ID"]);
			DataRow row = t.Rows.Find(new object[]{right_id, subject_id});
			if (row == null)
			{
				// there is no such row in the set of rows
				switch (newstate)
				{
					case 0: // unknown
						return;
					case 1: // enabled
					case 2: // disabled
						StringBuilder filterExpression = new StringBuilder(100);
						filterExpression.AppendFormat("RIGHT_ID={0}", right_id);
						filterExpression.Append(" AND ");
						filterExpression.AppendFormat("{0}_ID={1}", table, subject_id);
						StringBuilder sortExpression = new StringBuilder(100);
						sortExpression.Append("RIGHT_ID");
						sortExpression.Append(",");
						sortExpression.AppendFormat("{0}_ID", table);
						DataRow[] rows = t.Select(filterExpression.ToString(), sortExpression.ToString(), DataViewRowState.Deleted);
						if (rows.Length == 0)
						{
							row = t.NewRow();
							row["RIGHT_ID"] = right_id;
							row[table + "_ID"] = subject_id;
							row["ALLOWED"] = 2 - newstate;
							t.Rows.Add(row);
						}
						else
						{
							row = rows[0];
							row.RejectChanges();
							row["ALLOWED"] = 2 - newstate;
						}
						break;
				}
			}
			else
			{
				// there is such row
				switch (newstate)
				{
					case 0:
						row.Delete();
						return;
					case 1:
					case 2:
						row["ALLOWED"] = 2 - newstate;
						break;
				}
			}
		}

		public static void SetLink(DataSet dataset, string table1, int key1, string table2, int key2, bool newstate, int right_id)
		{
			DataTable table = dataset.Tables[table1 + "_" + table2];
			Debug.Assert(table.PrimaryKey.Length == 3);
			Debug.Assert(table.PrimaryKey[0] == table.Columns[table1 + "_ID"]);
			Debug.Assert(table.PrimaryKey[1] == table.Columns[table2 + "_ID"]);
			Debug.Assert(table.PrimaryKey[2] == table.Columns["RIGHT_ID"]);
			DataRow row = table.Rows.Find(new object[]{key1, key2, right_id});
			if (row == null)
			{
				// there is no such row in the set of rows
				if (newstate != false) 
				{
					StringBuilder filterExpression = new StringBuilder(100);
					filterExpression.AppendFormat("{0}_ID={1}", table1, key1);
					filterExpression.Append(" AND ");
					filterExpression.AppendFormat("{0}_ID={1}", table2, key2);
					filterExpression.Append(" AND ");
					filterExpression.AppendFormat("RIGHT_ID={0}", right_id);
					StringBuilder sortExpression = new StringBuilder(100);
					sortExpression.AppendFormat("{0}_ID", table1);
					sortExpression.Append(",");
					sortExpression.AppendFormat("{0}_ID", table2);
					sortExpression.Append(",");
					sortExpression.AppendFormat("RIGHT_ID");
					DataRow[] rows = table.Select(filterExpression.ToString(), sortExpression.ToString(), DataViewRowState.Deleted);
					if (rows.Length == 0)
					{
						row = table.NewRow();
						row[table1 + "_ID"] = key1;
						row[table2 + "_ID"] = key2;
						row["RIGHT_ID"] = right_id;
						row["ALLOWED"] = 1;
						table.Rows.Add(row);
					}
					else
					{
						row = rows[0];
						row.RejectChanges();
					}
				}
			}
			else
			{
				if (newstate != true)
				{
					row.Delete();
				}
			}
		}
		public static bool IsLinkExists(DataSet dataset, string table1, int key1, string table2, int key2, int right_id)
		{
			DataTable table = dataset.Tables[table1 + "_" + table2];
			Debug.Assert(table.PrimaryKey.Length == 3);
			Debug.Assert(table.PrimaryKey[0] == table.Columns[table1 + "_ID"]);
			Debug.Assert(table.PrimaryKey[1] == table.Columns[table2 + "_ID"]);
			Debug.Assert(table.PrimaryKey[2] == table.Columns["RIGHT_ID"]);
			DataRow row = table.Rows.Find(new object[]{key1, key2, right_id});
			if (row == null) return false;
			return row.RowState != DataRowState.Deleted;
		}

		public static int GetGroupGroupRights(DataSet dataset, int operatorgroup_id, string table, int drivergroup_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATORGROUP_" + table + "GROUP"].Rows.Find(new object[]{operatorgroup_id, drivergroup_id, right_id});
			return IsAllowed(row);
		}
		public static int GetGroupObjectRights(DataSet dataset, int operatorgroup_id, string table, int driver_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATORGROUP_" + table].Rows.Find(new object[]{operatorgroup_id, driver_id, right_id});
			return IsAllowed(row);
		}
		public static int GetOperatorObjectRights(DataSet dataset, int operator_id, string table, int driver_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATOR_" + table].Rows.Find(new object[]{operator_id, driver_id, right_id});
			return IsAllowed(row);
		}
		public static int GetOperatorGroupRights(DataSet dataset, int operator_id, string table, int drivergroup_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATOR_" + table + "GROUP"].Rows.Find(new object[]{operator_id, drivergroup_id, right_id});
			return IsAllowed(row);
		}

		public static int GetRightsSystem(DataSet dataset, int subject_id, int right_id)
		{
			DataRow row = dataset.Tables["RIGHT_OPERATOR"].Rows.Find(new object[]{right_id, subject_id});
			return IsAllowed(row);
		}

		public static int GetGroupRightsSystem(DataSet dataset, int subject_id, int right_id)
		{
			DataRow row = dataset.Tables["RIGHT_OPERATORGROUP"].Rows.Find(new object[]{right_id, subject_id});
			return IsAllowed(row);
		}

		public static int GetEffectiveGroupObjectRights(DataSet dataset, int operatorgroup_id, string table, int driver_id, int right_id)
		{
			bool enabled = false;
			int res = GetGroupObjectRights(dataset, operatorgroup_id, table, driver_id, right_id);
			if (res == 2) // disabled
			{
				return 2;
			}
			if (res == 1)
			{
				enabled = true;
			}
			DataView view = new DataView(dataset.Tables[table + "GROUP_" + table], table + "_ID="+driver_id, table + "GROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view)
			{
				int drivergroup_id = (int)v.Row[table + "GROUP_ID"];
				res = GetGroupGroupRights(dataset, operatorgroup_id, table, drivergroup_id, right_id);
				if (res == 2) // disabled
				{
					return 2;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			if (enabled)
			{
				return 1;
			}
			return 0;
		}

		public static int GetEffectiveOperatorGroupRights(DataSet dataset, int operator_id, string table, int drivergroup_id, int right_id)
		{
			bool enabled = false;
			int res = GetOperatorGroupRights(dataset, operator_id, table, drivergroup_id, right_id);
			if (res == 2) // disabled
			{
				return 2;
			}
			if (res == 1)
			{
				enabled = true;
			}
			DataView view = new DataView(dataset.Tables["OPERATORGROUP_OPERATOR"], "OPERATOR_ID="+operator_id, "OPERATORGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view)
			{
				int operatorgroup_id = (int)v.Row["OPERATORGROUP_ID"];
				res = GetGroupGroupRights(dataset, operatorgroup_id, table, drivergroup_id, right_id);
				if (res == 2) // disabled
				{
					return 2;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			if (enabled)
			{
				return 1;
			}
			return 0;
		}

		public static bool GetEffectiveOperatorObjectRights(DataSet dataset, int operator_id, string table, int driver_id, int right_id)
		{
			bool enabled = false;
			int res = GetOperatorObjectRights(dataset, operator_id, table, driver_id, right_id);
			if (res == 2) // disabled
			{
				return false;
			}
			if (res == 1)
			{
				enabled = true;
			}
			DataView view = new DataView(dataset.Tables[table + "GROUP_" + table], table + "_ID="+driver_id, table + "GROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view)
			{
				int drivergroup_id = (int)v.Row[table + "GROUP_ID"];
				res = GetEffectiveOperatorGroupRights(dataset, operator_id, table, drivergroup_id, right_id);
				if (res == 2) // disabled
				{
					return false;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			DataView view2 = new DataView(dataset.Tables["OPERATORGROUP_OPERATOR"], "OPERATOR_ID="+operator_id, "OPERATORGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view2)
			{
				int operatorgroup_id = (int)v.Row["OPERATORGROUP_ID"];
				res = GetEffectiveGroupObjectRights(dataset, operatorgroup_id, table, driver_id, right_id);
				if (res == 2) // disabled
				{
					return false;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			if (enabled)
			{
				return true;
			}
			return false;
		}
		public static bool GetEffectiveOperatorSystemRights(DataSet dataset, int operator_id, int right_id)
		{
			bool enabled = false;
			int res = GetRightsSystem(dataset, operator_id, right_id);
			if (res == 2) // disabled
			{
				return false;
			}
			if (res == 1)
			{
				enabled = true;
			}
			DataView view2 = new DataView(dataset.Tables["OPERATORGROUP_OPERATOR"], "OPERATOR_ID="+operator_id, "OPERATORGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view2)
			{
				int operatorgroup_id = (int)v.Row["OPERATORGROUP_ID"];
				res = GetGroupRightsSystem(dataset, operatorgroup_id, right_id);
				if (res == 2) // disabled
				{
					return false;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			if (enabled)
			{
				return true;
			}
			return false;
		}
	}
}
