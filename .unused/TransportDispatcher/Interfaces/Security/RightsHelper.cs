
#region /* ������� ������� ������������� ����� ��� �������� ���� �� ������� */
/*
using System;
using System.Data;

namespace FORIS.TSS.BusinessLogic
{
    /// <summary>
    /// Summary description for VehicleRightsHelper.
    /// </summary>
    public class RightsHelper : RightsHelperBase
    {
        public static int GetRightsOperatorObj(DataSet dsRights, int operator_id, string dtName, int obj_id, int right_id)
        {
            DataRow row = dsRights.Tables["OPERATOR_" + dtName].Rows.Find(new object[] { operator_id, obj_id, right_id });
            return IsAllowed(row);
        }

        public static int GetOperatorGroupObjGroupRights(DataSet dsRights, int operatorgroup_id, int vehiclegroup_id, int right_id)
        {
            DataRow row = dsRights.Tables["OPERATORGROUP_VEHICLEGROUP"].Rows.Find(new object[] { operatorgroup_id, vehiclegroup_id, right_id });
            return IsAllowed(row);
        }

        public static int GetOperatorGroupObjRights(DataSet dsRights, int operatorgroup_id, int obj_id, int right_id)
        {
            DataRow row = dsRights.Tables["OPERATORGROUP_VEHICLE"].Rows.Find(new object[] { operatorgroup_id, obj_id, right_id });
            return IsAllowed(row);
        }

        public static int GetOperatorObjGroupRights(DataSet dsRights, int operator_id, int vehiclegroup_id, int right_id)
        {
            DataRow row = dsRights.Tables["OPERATOR_VEHICLEGROUP"].Rows.Find(new object[] { operator_id, vehiclegroup_id, right_id });
            return IsAllowed(row);
        }


        public static int GetEffectiveGroupVehicleRights(DataSet dsRights, int operatorgroup_id, int obj_id, int right_id)
        {
            bool enabled = false;
            int res = GetGroupVehicleRights(dsRights, operatorgroup_id, obj_id, right_id);
            if (res == 2) // disabled
            {
                return 2;
            }
            if (res == 1)
            {
                enabled = true;
            }
            DataView view = new DataView(dsRights.Tables["VEHICLEGROUP_VEHICLE"], "VEHICLE_ID=" + obj_id, "VEHICLEGROUP_ID", DataViewRowState.CurrentRows);
            foreach (DataRowView v in view)
            {
                int vehiclegroup_id = (int)v.Row["VEHICLEGROUP_ID"];
                res = GetGroupGroupRights(dsRights, operatorgroup_id, vehiclegroup_id, right_id);
                if (res == 2) // disabled
                {
                    return 2;
                }
                if (res == 1)
                {
                    enabled = true;
                }
            }
            if (enabled)
            {
                return 1;
            }
            return 0;
        }

        public static int GetEffectiveOperatorGroupRights(DataSet dsRights, int operator_id, int vehiclegroup_id, int right_id)
        {
            bool enabled = false;
            int res = GetOperatorGroupRights(dsRights, operator_id, vehiclegroup_id, right_id);
            if (res == 2) // disabled
            {
                return 2;
            }
            if (res == 1)
            {
                enabled = true;
            }
            DataView view = new DataView(dsRights.Tables["OPERATORGROUP_OPERATOR"], "OPERATOR_ID=" + operator_id, "OPERATORGROUP_ID", DataViewRowState.CurrentRows);
            foreach (DataRowView v in view)
            {
                int operatorgroup_id = (int)v.Row["OPERATORGROUP_ID"];
                res = GetGroupGroupRights(dsRights, operatorgroup_id, vehiclegroup_id, right_id);
                if (res == 2) // disabled
                {
                    return 2;
                }
                if (res == 1)
                {
                    enabled = true;
                }
            }
            if (enabled)
            {
                return 1;
            }
            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dsRights"></param>
        /// <param name="operator_id"></param>
        /// <param name="dtName"></param>
        /// <param name="obj_id"></param>
        /// <param name="right_id"></param>
        /// <returns></returns>
        public static bool GetRights(DataSet dsRights, int operator_id, string dtName, int obj_id, int right_id)
        {
            bool enabled = false;
            int res = GetRightsOperatorObj(dsRights, operator_id, dtName, obj_id, right_id);
            if (res == 2) // disabled
            {
                return false;
            }
            if (res == 1)
            {
                enabled = true;
            }

            DataView view = new DataView(dsRights.Tables["VEHICLEGROUP_VEHICLE"], "VEHICLE_ID=" + obj_id, "VEHICLEGROUP_ID", DataViewRowState.CurrentRows);
            foreach (DataRowView v in view)
            {
                int vehiclegroup_id = (int)v.Row["VEHICLEGROUP_ID"];
                res = GetEffectiveOperatorGroupRights(dsRights, operator_id, vehiclegroup_id, right_id);
                if (res == 2) // disabled
                {
                    return false;
                }
                if (res == 1)
                {
                    enabled = true;
                }
            }
            DataView view2 = new DataView(dsRights.Tables["OPERATORGROUP_OPERATOR"], "OPERATOR_ID=" + operator_id, "OPERATORGROUP_ID", DataViewRowState.CurrentRows);
            foreach (DataRowView v in view2)
            {
                int operatorgroup_id = (int)v.Row["OPERATORGROUP_ID"];
                res = GetEffectiveGroupVehicleRights(dsRights, operatorgroup_id, obj_id, right_id);
                if (res == 2) // disabled
                {
                    return false;
                }
                if (res == 1)
                {
                    enabled = true;
                }
            }
            if (enabled)
            {
                return true;
            }
            return false;
        }

    }
}
*/
#endregion /* ������� ������� ������������� ����� ��� �������� ���� �� ������� */