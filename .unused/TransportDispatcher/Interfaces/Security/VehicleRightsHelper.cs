using System;
using System.Data;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary>
	/// Summary description for VehicleRightsHelper.
	/// </summary>
	public class VehicleRightsHelper : RightsHelperBase
	{
		public static int GetGroupGroupRights(DataSet dataset, int operatorgroup_id, int vehiclegroup_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATORGROUP_VEHICLEGROUP"].Rows.Find(new object[]{operatorgroup_id, vehiclegroup_id, right_id});
			return IsAllowed(row);
		}
		public static int GetGroupVehicleRights(DataSet dataset, int operatorgroup_id, int vehicle_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATORGROUP_VEHICLE"].Rows.Find(new object[]{operatorgroup_id, vehicle_id, right_id});
			return IsAllowed(row);
		}
		public static int GetOperatorVehicleRights(DataSet dataset, int operator_id, int vehicle_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATOR_VEHICLE"].Rows.Find(new object[]{operator_id, vehicle_id, right_id});
			return IsAllowed(row);
		}
		public static int GetOperatorGroupRights(DataSet dataset, int operator_id, int vehiclegroup_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATOR_VEHICLEGROUP"].Rows.Find(new object[]{operator_id, vehiclegroup_id, right_id});
			return IsAllowed(row);
		}

		public static void SetGroupGroupRights(DataSet dataset, int operatorgroup_id, int vehiclegroup_id, int newstate)
		{
			SetRight(dataset, "OPERATORGROUP", operatorgroup_id, "VEHICLEGROUP", vehiclegroup_id, newstate, (int)SystemRight.VehicleAccess);
		}
		public static void SetGroupVehicleRights(DataSet dataset, int operatorgroup_id, int vehicle_id, int newstate)
		{
			SetRight(dataset, "OPERATORGROUP", operatorgroup_id, "VEHICLE", vehicle_id, newstate, (int)SystemRight.VehicleAccess);
		}
		public static void SetOperatorVehicleRights(DataSet dataset, int operator_id, int vehicle_id, int newstate)
		{
			SetRight(dataset, "OPERATOR", operator_id, "VEHICLE", vehicle_id, newstate, (int)SystemRight.VehicleAccess);
		}
		public static void SetOperatorGroupRights(DataSet dataset, int operator_id, int vehiclegroup_id, int newstate)
		{
			SetRight(dataset, "OPERATOR", operator_id, "VEHICLEGROUP", vehiclegroup_id, newstate, (int)SystemRight.VehicleAccess);
		}

		public static int GetEffectiveGroupVehicleRights(DataSet dataset, int operatorgroup_id, int vehicle_id, int right_id)
		{
			bool enabled = false;
			int res = GetGroupVehicleRights(dataset, operatorgroup_id, vehicle_id, right_id);
			if (res == 2) // disabled
			{
				return 2;
			}
			if (res == 1)
			{
				enabled = true;
			}
			DataView view = new DataView(dataset.Tables["VEHICLEGROUP_VEHICLE"], "VEHICLE_ID="+vehicle_id, "VEHICLEGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view)
			{
				int vehiclegroup_id = (int)v.Row["VEHICLEGROUP_ID"];
				res = GetGroupGroupRights(dataset, operatorgroup_id, vehiclegroup_id, right_id);
				if (res == 2) // disabled
				{
					return 2;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			if (enabled)
			{
				return 1;
			}
			return 0;
		}
		public static int GetEffectiveOperatorGroupRights(DataSet dataset, int operator_id, int vehiclegroup_id, int right_id)
		{
			bool enabled = false;
			int res = GetOperatorGroupRights(dataset, operator_id, vehiclegroup_id, right_id);
			if (res == 2) // disabled
			{
				return 2;
			}
			if (res == 1)
			{
				enabled = true;
			}
			DataView view = new DataView(dataset.Tables["OPERATORGROUP_OPERATOR"], "OPERATOR_ID="+operator_id, "OPERATORGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view)
			{
				int operatorgroup_id = (int)v.Row["OPERATORGROUP_ID"];
				res = GetGroupGroupRights(dataset, operatorgroup_id, vehiclegroup_id, right_id);
				if (res == 2) // disabled
				{
					return 2;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			if (enabled)
			{
				return 1;
			}
			return 0;
		}
		public static bool GetEffectiveOperatorVehicleRights(DataSet dataset, int operator_id, int vehicle_id, int right_id)
		{
			bool enabled = false;
			int res = GetOperatorVehicleRights(dataset, operator_id, vehicle_id, right_id);
			if (res == 2) // disabled
			{
				return false;
			}
			if (res == 1)
			{
				enabled = true;
			}
			DataView view = new DataView(dataset.Tables["VEHICLEGROUP_VEHICLE"], "VEHICLE_ID="+vehicle_id, "VEHICLEGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view)
			{
				int vehiclegroup_id = (int)v.Row["VEHICLEGROUP_ID"];
				res = GetEffectiveOperatorGroupRights(dataset, operator_id, vehiclegroup_id, right_id);
				if (res == 2) // disabled
				{
					return false;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			DataView view2 = new DataView(dataset.Tables["OPERATORGROUP_OPERATOR"], "OPERATOR_ID="+operator_id, "OPERATORGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view2)
			{
				int operatorgroup_id = (int)v.Row["OPERATORGROUP_ID"];
				res = GetEffectiveGroupVehicleRights(dataset, operatorgroup_id, vehicle_id, right_id);
				if (res == 2) // disabled
				{
					return false;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			if (enabled)
			{
				return true;
			}
			return false;
		}
	}
}
