using System;
using System.Data;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary>
	/// Summary description for SystemRightsHelper.
	/// </summary>
	public class SystemRightsHelper : RightsHelperBase
	{
		public static int GetGroupRight(DataSet dataset, int operatorgroup_id, int right_id)
		{
			DataRow row = dataset.Tables["RIGHT_OPERATORGROUP"].Rows.Find(new object[]{right_id, operatorgroup_id});
			return IsAllowed(row);
		}
		public static int GetOperatorRight(DataSet dataset, int operator_id, int right_id)
		{
			DataRow row = dataset.Tables["RIGHT_OPERATOR"].Rows.Find(new object[]{right_id, operator_id});
			return IsAllowed(row);
		}
		public static void SetGroupRight(DataSet dataset, int operatorgroup_id, int right_id, int newstate)
		{
			SetRight(dataset, "RIGHT", right_id, "OPERATORGROUP", operatorgroup_id, newstate, right_id);
		}
		public static void SetOperatorRight(DataSet dataset, int operator_id, int right_id, int newstate)
		{
			SetRight(dataset, "RIGHT", right_id, "OPERATOR", operator_id, newstate, right_id);
		}
		public static bool GetEffectiveOperatorRight(DataSet dataset, int operator_id, int right_id)
		{
			bool enabled = false;
			int res = GetOperatorRight(dataset, operator_id, right_id);
			if (res == 2) // disabled
			{
				return false;
			}
			if (res == 1)
			{
				enabled = true;
			}
			
			DataView view = new DataView(dataset.Tables["OPERATORGROUP_OPERATOR"], "OPERATOR_ID="+operator_id, "OPERATORGROUP_ID", DataViewRowState.CurrentRows);

			foreach (DataRowView v in view)
			{
				int operatorgroup_id = (int)v.Row["OPERATORGROUP_ID"];
				res = GetGroupRight(dataset, operatorgroup_id, right_id);
				if (res == 2) // disabled
				{
					return false;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}

			return enabled;
		}
	}
}
