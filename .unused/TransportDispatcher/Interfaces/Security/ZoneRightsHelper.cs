﻿
using System;
using System.Data;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary>
	/// Summary description for ZoneRightsHelper. 
	/// </summary>
	public class ZoneRightsHelper : RightsHelperBase
	{
//		public static bool GetGroupGroupRights(DataSet dataset, int operatorgroup_id, int zonegroup_id, int right_id)
//		{
//			DataRow row = dataset.Tables["OPERATORGROUP_ZONEGROUP"].Rows.Find(new object[]{operatorgroup_id, zonegroup_id, right_id});
//			return IsExists(row);
//		}
		public static int GetGroupGroupRights(DataSet dataset, int operatorgroup_id, int zonegroup_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATORGROUP_ZONEGROUP"].Rows.Find(new object[]{operatorgroup_id, zonegroup_id, right_id});
			return IsAllowed(row);
		}
//		public static bool GetGroupZoneRights(DataSet dataset, int operatorgroup_id, int zone_id, int right_id)
//		{
//			DataRow row = dataset.Tables["OPERATORGROUP_ZONE"].Rows.Find(new object[]{operatorgroup_id, zone_id, right_id});
//			return IsExists(row);
//		}
		public static int GetGroupZoneRights(DataSet dataset, int operatorgroup_id, int zone_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATORGROUP_ZONE"].Rows.Find(new object[]{operatorgroup_id, zone_id, right_id});
			return IsAllowed(row);
		}
//		public static bool GetOperatorZoneRights(DataSet dataset, int operator_id, int zone_id, int right_id)
//		{
//			DataRow row = dataset.Tables["OPERATOR_ZONE"].Rows.Find(new object[]{operator_id, zone_id, right_id});
//			return IsExists(row);  
//		}
		public static int GetOperatorZoneRights(DataSet dataset, int operator_id, int zone_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATOR_ZONE"].Rows.Find(new object[]{operator_id, zone_id, right_id});
			return IsAllowed(row);
		} 
//		public static bool GetOperatorGroupRights(DataSet dataset, int operator_id, int zonegroup_id, int right_id)
//		{
//			DataRow row = dataset.Tables["OPERATOR_ZONEGROUP"].Rows.Find(new object[]{operator_id, zonegroup_id, right_id});
//			return IsExists(row);
//		}
		public static int GetOperatorGroupRights(DataSet dataset, int operator_id, int zonegroup_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATOR_ZONEGROUP"].Rows.Find(new object[]{operator_id, zonegroup_id, right_id});
			return IsAllowed(row);
		}
		public static void SetGroupGroupRights(DataSet dataset, int operatorgroup_id, int zonegroup_id, bool newstate, int right_id)
		{
			SetLink(dataset, "OPERATORGROUP", operatorgroup_id, "ZONEGROUP", zonegroup_id, newstate, right_id);
		}
		public static void SetGroupGroupRights(DataSet dataset, int operatorgroup_id, int zonegroup_id, int newstate)
		{
			SetRight(dataset, "OPERATORGROUP", operatorgroup_id, "ZONEGROUP", zonegroup_id, newstate, (int)SystemRight.ZoneAccess);
		}
		public static void SetGroupZoneRights(DataSet dataset, int operatorgroup_id, int zone_id, bool newstate, int right_id)
		{
			SetLink(dataset, "OPERATORGROUP", operatorgroup_id, "ZONE", zone_id, newstate, right_id);
		}
		public static void SetGroupZoneRights(DataSet dataset, int operatorgroup_id, int zone_id, int newstate)
		{
			SetRight(dataset, "OPERATORGROUP", operatorgroup_id, "ZONE", zone_id, newstate, (int)SystemRight.ZoneAccess);
		}
		public static void SetOperatorZoneRights(DataSet dataset, int operator_id, int zone_id, bool newstate, int right_id)
		{
            SetLink(dataset, "OPERATOR", operator_id, "ZONE", zone_id, newstate, right_id);
		}
		public static void SetOperatorZoneRights(DataSet dataset, int operator_id, int zone_id, int newstate, int right_id)
		{
            SetRight(dataset, "OPERATOR", operator_id, "ZONE", zone_id, newstate, right_id);
		}
//		public static void SetOperatorGroupRights(DataSet dataset, int operator_id, int zonegroup_id, bool newstate, int right_id)
//		{
//			SetLink(dataset, "OPERATOR", operator_id, "ZONEGROUP", zonegroup_id, newstate, right_id);
//		}
		public static void SetOperatorGroupRights(DataSet dataset, int operator_id, int zonegroup_id, int newstate, int right_id)
		{
            SetRight(dataset, "OPERATOR", operator_id, "ZONEGROUP", zonegroup_id, newstate, right_id);
		}
//		public static bool GetEffectiveGroupZoneRights(DataSet dataset, int operatorgroup_id, int zone_id, int right_id)
//		{
//			bool res = GetGroupZoneRights(dataset, operatorgroup_id, zone_id, right_id);
//			if (res == true)
//			{
//				return true;
//			}
//			DataView view = new DataView(dataset.Tables["ZONEGROUP_ZONE"], "ZONE_ID="+zone_id, "ZONEGROUP_ID", DataViewRowState.CurrentRows);
//			foreach (DataRowView v in view)
//			{
//				int zonegroup_id = (int)v.Row["ZONEGROUP_ID"];
//				res = GetGroupGroupRights(dataset, operatorgroup_id, zonegroup_id, right_id);
//				if (res == true)
//				{
//					return true;
//				}
//			}
//			return false;
//		}
		public static int GetEffectiveGroupZoneRights(DataSet dataset, int operatorgroup_id, int zone_id, int right_id)
		{
			bool enabled = false;
			int res = GetGroupZoneRights(dataset, operatorgroup_id, zone_id, right_id);
			if (res == 2) // disabled
			{
				return 2;
			}
			if (res == 1)
			{
				enabled = true;
			}
            DataView view = new DataView(dataset.Tables["ZONEGROUP_ZONE"], "ZONE_ID=" + zone_id, "ZONEGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view)
			{
                int zonegroup_id = (int)v.Row["ZONEGROUP_ID"];
				res = GetGroupGroupRights(dataset, operatorgroup_id, zonegroup_id, right_id);
				if (res == 2) // disabled
				{
					return 2;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			if (enabled)
			{
				return 1;
			}
			return 0;
		}
//		public static bool GetEffectiveOperatorGroupRights(DataSet dataset, int operator_id, int zonegroup_id, int right_id)
//		{
//			bool res = GetOperatorGroupRights(dataset, operator_id, zonegroup_id, right_id);
//			if (res == true)
//			{
//				return true;
//			}
//			DataView view = new DataView(dataset.Tables["OPERATORGROUP_OPERATOR"], "OPERATOR_ID="+operator_id, "OPERATORGROUP_ID", DataViewRowState.CurrentRows);
//			foreach (DataRowView v in view)
//			{
//				int operatorgroup_id = (int)v.Row["OPERATORGROUP_ID"];
//				res = GetGroupGroupRights(dataset, operatorgroup_id, zonegroup_id, right_id);
//				if (res == true)
//				{
//					return true;
//				}
//			}
//			return false;
//		}
		public static int GetEffectiveOperatorGroupRights(DataSet dataset, int operator_id, int zonegroup_id, int right_id)
		{
			bool enabled = false;
			int res = GetOperatorGroupRights(dataset, operator_id, zonegroup_id, right_id);
			if (res == 2) // disabled
			{
				return 2;
			}
			if (res == 1)
			{
				enabled = true;
			}
			DataView view = new DataView(dataset.Tables["OPERATORGROUP_OPERATOR"], "OPERATOR_ID="+operator_id, "OPERATORGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view)
			{
				int operatorgroup_id = (int)v.Row["OPERATORGROUP_ID"];
				res = GetGroupGroupRights(dataset, operatorgroup_id, zonegroup_id, right_id);
				if (res == 2) // disabled
				{
					return 2;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			if (enabled)
			{
				return 1;
			}
			return 0;
		}
//		public static bool GetEffectiveOperatorZoneRights(DataSet dataset, int operator_id, int zone_id, int right_id)
//		{
//			bool res = GetOperatorZoneRights(dataset, operator_id, zone_id, right_id);
//			if (res == true)
//			{
//				return true;
//			}
//			DataView view = new DataView(dataset.Tables["ZONEGROUP_ZONE"], "ZONE_ID="+zone_id, "ZONEGROUP_ID", DataViewRowState.CurrentRows);
//			foreach (DataRowView v in view)
//			{
//				int zonegroup_id = (int)v.Row["ZONEGROUP_ID"];
//				res = GetEffectiveOperatorGroupRights(dataset, operator_id, zonegroup_id, right_id);
//				if (res == true)
//				{
//					return true;
//				}
//			}
//			DataView view2 = new DataView(dataset.Tables["OPERATORGROUP_OPERATOR"], "OPERATOR_ID="+operator_id, "OPERATORGROUP_ID", DataViewRowState.CurrentRows);
//			foreach (DataRowView v in view2)
//			{
//				int operatorgroup_id = (int)v.Row["OPERATORGROUP_ID"];
//				res = GetEffectiveGroupZoneRights(dataset, operatorgroup_id, zone_id, right_id);
//				if (res == true)
//				{
//					return true;
//				}
//			}
//			return false;
//		}
		public static bool GetEffectiveOperatorZoneRights(DataSet dataset, int operator_id, int zone_id, int right_id)
		{
			bool enabled = false;
			int res = GetOperatorZoneRights(dataset, operator_id, zone_id, right_id);
			if (res == 2) // disabled
			{
				return false;
			}
			if (res == 1)
			{
				enabled = true;
			}
            DataView view = new DataView(dataset.Tables["ZONEGROUP_ZONE"], "ZONE_ID=" + zone_id, "ZONEGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view)
			{
                int zonegroup_id = (int)v.Row["ZONEGROUP_ID"];
				res = GetEffectiveOperatorGroupRights(dataset, operator_id, zonegroup_id, right_id);
				if (res == 2) // disabled
				{
					return false;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			DataView view2 = new DataView(dataset.Tables["OPERATORGROUP_OPERATOR"], "OPERATOR_ID="+operator_id, "OPERATORGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view2)
			{
				int operatorgroup_id = (int)v.Row["OPERATORGROUP_ID"];
				res = GetEffectiveGroupZoneRights(dataset, operatorgroup_id, zone_id, right_id);
				if (res == 2) // disabled
				{
					return false;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			if (enabled)
			{
				return true;
			}
			return false;
		}

		public static bool IsExists(DataRow row)
		{
			if (row == null)
			{
				return false;
			}
			return true;
		}
	}
}
