using System;
using System.Data;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary>
	/// Summary description for ReportRightsHelper.
	/// </summary>
	public class DepartmentRightsHelper : RightsHelperBase
	{
		public static int GetOperatorDepartmentRights(DataSet dataset, int operator_id, int department_id, int right_id)
		{
			//������� ��������� ����� � ���������� ������ ��� ������ �� ����.
			DataColumn[] dcolKeys = new DataColumn[3];
			dcolKeys[0] = dataset.Tables["OPERATOR_DEPARTMENT"].Columns["OPERATOR_ID"];
			dcolKeys[1] = dataset.Tables["OPERATOR_DEPARTMENT"].Columns["DEPARTMENT_ID"];
			dcolKeys[2] = dataset.Tables["OPERATOR_DEPARTMENT"].Columns["RIGHT_ID"];
			dataset.Tables["OPERATOR_DEPARTMENT"].PrimaryKey = dcolKeys;
			
			//�����  
			DataRow row = dataset.Tables["OPERATOR_DEPARTMENT"].Rows.Find(new object[]{operator_id, department_id, right_id});
			return IsAllowed(row);
		} 

		public static int GetGroupDepartmentRights(DataSet dataset, int operatorgroup_id, int department_id, int right_id)
		{
			//������� ��������� ����� � ���������� ������ ��� ������ �� ����.
			DataColumn[] dcolKeys = new DataColumn[3];
			dcolKeys[0] = dataset.Tables["OPERATORGROUP_DEPARTMENT"].Columns["OPERATORGROUP_ID"];
			dcolKeys[1] = dataset.Tables["OPERATORGROUP_DEPARTMENT"].Columns["DEPARTMENT_ID"];
			dcolKeys[2] = dataset.Tables["OPERATORGROUP_DEPARTMENT"].Columns["RIGHT_ID"];
			dataset.Tables["OPERATORGROUP_DEPARTMENT"].PrimaryKey = dcolKeys;
			
			//�����  
			DataRow row = dataset.Tables["OPERATORGROUP_DEPARTMENT"].Rows.Find(new object[]{operatorgroup_id, department_id, right_id});
			return IsAllowed(row);
		}
		
		public static bool GetEffectiveOperatorDepartmentRights(DataSet dataset, int operator_id, int department_id, int right_id)
		{
			bool enabled = false;

			int res = GetOperatorDepartmentRights(dataset, operator_id, department_id, right_id);
			if (res == 2) // disabled
			{
				//return 2;
				return false;
			}
			if (res == 1)
			{
				enabled = true;
			}

			DataView view = new DataView(dataset.Tables["OPERATORGROUP_OPERATOR"], "OPERATOR_ID=" + operator_id, "OPERATORGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view)
			{
				int operatorgroup_id = (int)v.Row["OPERATORGROUP_ID"];

				res = GetGroupDepartmentRights(dataset, operatorgroup_id, department_id, right_id);
				if (res == 2) // disabled
				{
					//return 2;
					return false;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			if (enabled)
			{
				//return 1; 
				return true;
			}
			//return 0; 
			return false;
		}
		
		public static void SetGroupReportRights(DataSet dataset, int operatorgroup_id, int report_id, int newstate)
		{
			SetRight(dataset, "OPERATORGROUP", operatorgroup_id, "DEPARTMENT", report_id, newstate, (int)SystemRight.VehicleAccess);
		}
	}
}
