using System;
using System.Data;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary>
	/// Summary description for ReportRightsHelper.
	/// </summary>
	public class ReportRightsHelper : RightsHelperBase
	{

		public static int GetOperatorReportRights(DataSet dataset, int operator_id, int report_id, int right_id)
		{
			//������� ��������� ����� � ���������� ������ ��� ������ �� ����.
			DataColumn[] dcolKeys = new DataColumn[3];
			dcolKeys[0] = dataset.Tables["OPERATOR_REPORT"].Columns["OPERATOR_ID"];
			dcolKeys[1] = dataset.Tables["OPERATOR_REPORT"].Columns["REPORT_ID"];
			dcolKeys[2] = dataset.Tables["OPERATOR_REPORT"].Columns["RIGHT_ID"];
			dataset.Tables["OPERATOR_REPORT"].PrimaryKey = dcolKeys;
			
			//�����  
			DataRow row = dataset.Tables["OPERATOR_REPORT"].Rows.Find(new object[]{operator_id, report_id, right_id});
			return IsAllowed(row);
		} 

		public static int GetGroupReportRights(DataSet dataset, int operatorgroup_id, int report_id, int right_id)
		{
			//������� ��������� ����� � ���������� ������ ��� ������ �� ����.
			DataColumn[] dcolKeys = new DataColumn[3];
			dcolKeys[0] = dataset.Tables["OPERATORGROUP_REPORT"].Columns["OPERATORGROUP_ID"];
			dcolKeys[1] = dataset.Tables["OPERATORGROUP_REPORT"].Columns["REPORT_ID"];
			dcolKeys[2] = dataset.Tables["OPERATORGROUP_REPORT"].Columns["RIGHT_ID"];
			dataset.Tables["OPERATORGROUP_REPORT"].PrimaryKey = dcolKeys;
			
			//�����  
			DataRow row = dataset.Tables["OPERATORGROUP_REPORT"].Rows.Find(new object[]{operatorgroup_id, report_id, right_id});
			return IsAllowed(row);
		}

		public static bool GetEffectiveOperatorReportRights(DataSet dataset, int operator_id, int report_id, int right_id)
		{
			bool enabled = false;

			int res = GetOperatorReportRights(dataset, operator_id, report_id, right_id);
			if (res == 2) // disabled
			{
				//return 2;
				return false;
			}
			if (res == 1)
			{
				enabled = true;
			}

			DataView view = new DataView(dataset.Tables["OPERATORGROUP_OPERATOR"], "OPERATOR_ID=" + operator_id, "OPERATORGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view)
			{
				int operatorgroup_id = (int)v.Row["OPERATORGROUP_ID"];

				res = GetGroupReportRights(dataset, operatorgroup_id, report_id, right_id);
				if (res == 2) // disabled
				{
					//return 2;
					return false;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			if (enabled)
			{
				//return 1; 
				return true;
			}
			//return 0; 
			return false;
		}
		
		public static void SetGroupReportRights(DataSet dataset, int operatorgroup_id, int report_id, int newstate)
		{
			SetRight(dataset, "OPERATORGROUP", operatorgroup_id, "REPORT", report_id, newstate, (int)SystemRight.VehicleAccess);
		}

	}
}
