using System;
using System.Data;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary>
	/// Summary description for DriverRightsHelper.
	/// </summary>
	public class DriverRightsHelper : RightsHelperBase
	{
		public static int GetGroupGroupRights(DataSet dataset, int operatorgroup_id, int drivergroup_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATORGROUP_DRIVERGROUP"].Rows.Find(new object[]{operatorgroup_id, drivergroup_id, right_id});
			return IsAllowed(row);
		}
		public static int GetGroupDriverRights(DataSet dataset, int operatorgroup_id, int driver_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATORGROUP_DRIVER"].Rows.Find(new object[]{operatorgroup_id, driver_id, right_id});
			return IsAllowed(row);
		}
		public static int GetOperatorDriverRights(DataSet dataset, int operator_id, int driver_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATOR_DRIVER"].Rows.Find(new object[]{operator_id, driver_id, right_id});
			return IsAllowed(row);
		}
		public static int GetOperatorGroupRights(DataSet dataset, int operator_id, int drivergroup_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATOR_DRIVERGROUP"].Rows.Find(new object[]{operator_id, drivergroup_id, right_id});
			return IsAllowed(row);
		}

		public static void SetGroupGroupRights(DataSet dataset, int operatorgroup_id, int drivergroup_id, int newstate, int right_id)
		{
			SetRight(dataset, "OPERATORGROUP", operatorgroup_id, "DRIVERGROUP", drivergroup_id, newstate, right_id);
		}
		public static void SetGroupGroupRights(DataSet dataset, int operatorgroup_id, int drivergroup_id, int newstate)
		{
			SetRight(dataset, "OPERATORGROUP", operatorgroup_id, "DRIVERGROUP", drivergroup_id, newstate, (int)SystemRight.DriverAccess);
		}
		public static void SetGroupDriverRights(DataSet dataset, int operatorgroup_id, int driver_id, int newstate, int right_id)
		{
			SetRight(dataset, "OPERATORGROUP", operatorgroup_id, "DRIVER", driver_id, newstate, right_id);
		}
		public static void SetGroupDriverRights(DataSet dataset, int operatorgroup_id, int driver_id, int newstate)
		{
			SetRight(dataset, "OPERATORGROUP", operatorgroup_id, "DRIVER", driver_id, newstate, (int)SystemRight.DriverAccess);
		}
		public static void SetOperatorDriverRights(DataSet dataset, int operator_id, int driver_id, int newstate, int right_id)
		{
			SetRight(dataset, "OPERATOR", operator_id, "DRIVER", driver_id, newstate, right_id);
		}
		public static void SetOperatorGroupRights(DataSet dataset, int operator_id, int drivergroup_id, int newstate, int right_id)
		{
			SetRight(dataset, "OPERATOR", operator_id, "DRIVERGROUP", drivergroup_id, newstate, right_id);
		}

		public static int GetEffectiveGroupDriverRights(DataSet dataset, int operatorgroup_id, int driver_id, int right_id)
		{
			bool enabled = false;
			int res = GetGroupDriverRights(dataset, operatorgroup_id, driver_id, right_id);
			if (res == 2) // disabled
			{
				return 2;
			}
			if (res == 1)
			{
				enabled = true;
			}
			DataView view = new DataView(dataset.Tables["DRIVERGROUP_DRIVER"], "DRIVER_ID="+driver_id, "DRIVERGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view)
			{
				int drivergroup_id = (int)v.Row["DRIVERGROUP_ID"];
				res = GetGroupGroupRights(dataset, operatorgroup_id, drivergroup_id, right_id);
				if (res == 2) // disabled
				{
					return 2;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			if (enabled)
			{
				return 1;
			}
			return 0;
		}

		public static int GetEffectiveOperatorGroupRights(DataSet dataset, int operator_id, int drivergroup_id, int right_id)
		{
			bool enabled = false;
			int res = GetOperatorGroupRights(dataset, operator_id, drivergroup_id, right_id);
			if (res == 2) // disabled
			{
				return 2;
			}
			if (res == 1)
			{
				enabled = true;
			}
			DataView view = new DataView(dataset.Tables["OPERATORGROUP_OPERATOR"], "OPERATOR_ID="+operator_id, "OPERATORGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view)
			{
				int operatorgroup_id = (int)v.Row["OPERATORGROUP_ID"];
				res = GetGroupGroupRights(dataset, operatorgroup_id, drivergroup_id, right_id);
				if (res == 2) // disabled
				{
					return 2;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			if (enabled)
			{
				return 1;
			}
			return 0;
		}

		public static bool GetEffectiveOperatorDriverRights(DataSet dataset, int operator_id, int driver_id, int right_id)
		{
			bool enabled = false;
			int res = GetOperatorDriverRights(dataset, operator_id, driver_id, right_id);
			if (res == 2) // disabled
			{
				return false;
			}
			if (res == 1)
			{
				enabled = true;
			}
			DataView view = new DataView(dataset.Tables["DRIVERGROUP_DRIVER"], "DRIVER_ID="+driver_id, "DRIVERGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view)
			{
				int drivergroup_id = (int)v.Row["DRIVERGROUP_ID"];
				res = GetEffectiveOperatorGroupRights(dataset, operator_id, drivergroup_id, right_id);
				if (res == 2) // disabled
				{
					return false;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			DataView view2 = new DataView(dataset.Tables["OPERATORGROUP_OPERATOR"], "OPERATOR_ID="+operator_id, "OPERATORGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view2)
			{
				int operatorgroup_id = (int)v.Row["OPERATORGROUP_ID"];
				res = GetEffectiveGroupDriverRights(dataset, operatorgroup_id, driver_id, right_id);
				if (res == 2) // disabled
				{
					return false;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			if (enabled)
			{
				return true;
			}
			return false;
		}
	}
}
