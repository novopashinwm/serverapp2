using System;
using System.Data;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary>
	/// Summary description for RouteRightsHelper.
	/// </summary>
	public class RouteRightsHelper : RightsHelperBase
	{
		public static int GetGroupGroupRights(DataSet dataset, int operatorgroup_id, int routegroup_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATORGROUP_ROUTEGROUP"].Rows.Find(new object[]{operatorgroup_id, routegroup_id, right_id});
			return IsAllowed(row);
		}
		public static int GetGroupRouteRights(DataSet dataset, int operatorgroup_id, int route_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATORGROUP_ROUTE"].Rows.Find(new object[]{operatorgroup_id, route_id, right_id});
			return IsAllowed(row);
		}
		public static int GetOperatorRouteRights(DataSet dataset, int operator_id, int route_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATOR_ROUTE"].Rows.Find(new object[]{operator_id, route_id, right_id});
			return IsAllowed(row);
		} 
		public static int GetOperatorGroupRights(DataSet dataset, int operator_id, int routegroup_id, int right_id)
		{
			DataRow row = dataset.Tables["OPERATOR_ROUTEGROUP"].Rows.Find(new object[]{operator_id, routegroup_id, right_id});
			return IsAllowed(row);
		}
		public static void SetGroupGroupRights(DataSet dataset, int operatorgroup_id, int routegroup_id, bool newstate, int right_id)
		{
			SetLink(dataset, "OPERATORGROUP", operatorgroup_id, "ROUTEGROUP", routegroup_id, newstate, right_id);
		}
		public static void SetGroupGroupRights(DataSet dataset, int operatorgroup_id, int routegroup_id, int newstate)
		{
			SetRight(dataset, "OPERATORGROUP", operatorgroup_id, "ROUTEGROUP", routegroup_id, newstate, (int)SystemRight.RouteAccess);
		}
		public static void SetGroupRouteRights(DataSet dataset, int operatorgroup_id, int route_id, bool newstate, int right_id)
		{
			SetLink(dataset, "OPERATORGROUP", operatorgroup_id, "ROUTE", route_id, newstate, right_id);
		}
		public static void SetGroupRouteRights(DataSet dataset, int operatorgroup_id, int route_id, int newstate)
		{
			SetRight(dataset, "OPERATORGROUP", operatorgroup_id, "ROUTE", route_id, newstate, (int)SystemRight.RouteAccess);
		}
		public static void SetOperatorRouteRights(DataSet dataset, int operator_id, int route_id, bool newstate, int right_id)
		{
			SetLink(dataset, "OPERATOR", operator_id, "ROUTE", route_id, newstate, right_id);
		}
		public static void SetOperatorRouteRights(DataSet dataset, int operator_id, int route_id, int newstate, int right_id)
		{
			SetRight(dataset, "OPERATOR", operator_id, "ROUTE", route_id, newstate, right_id);
		}
		public static void SetOperatorGroupRights(DataSet dataset, int operator_id, int routegroup_id, int newstate, int right_id)
		{
			SetRight(dataset, "OPERATOR", operator_id, "ROUTEGROUP", routegroup_id, newstate, right_id);
		}
		public static int GetEffectiveGroupRouteRights(DataSet dataset, int operatorgroup_id, int route_id, int right_id)
		{
			bool enabled = false;
			int res = GetGroupRouteRights(dataset, operatorgroup_id, route_id, right_id);
			if (res == 2) // disabled
			{
				return 2;
			}
			if (res == 1)
			{
				enabled = true;
			}
			DataView view = new DataView(dataset.Tables["ROUTEGROUP_ROUTE"], "ROUTE_ID="+route_id, "ROUTEGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view)
			{
				int routegroup_id = (int)v.Row["ROUTEGROUP_ID"];
				res = GetGroupGroupRights(dataset, operatorgroup_id, routegroup_id, right_id);
				if (res == 2) // disabled
				{
					return 2;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			if (enabled)
			{
				return 1;
			}
			return 0;
		}
		public static int GetEffectiveOperatorGroupRights(DataSet dataset, int operator_id, int routegroup_id, int right_id)
		{
			bool enabled = false;
			int res = GetOperatorGroupRights(dataset, operator_id, routegroup_id, right_id);
			if (res == 2) // disabled
			{
				return 2;
			}
			if (res == 1)
			{
				enabled = true;
			}
			DataView view = new DataView(dataset.Tables["OPERATORGROUP_OPERATOR"], "OPERATOR_ID="+operator_id, "OPERATORGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view)
			{
				int operatorgroup_id = (int)v.Row["OPERATORGROUP_ID"];
				res = GetGroupGroupRights(dataset, operatorgroup_id, routegroup_id, right_id);
				if (res == 2) // disabled
				{
					return 2;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			if (enabled)
			{
				return 1;
			}
			return 0;
		}
		public static bool GetEffectiveOperatorRouteRights(DataSet dataset, int operator_id, int route_id, int right_id)
		{
			bool enabled = false;
			int res = GetOperatorRouteRights(dataset, operator_id, route_id, right_id);
			if (res == 2) // disabled
			{
				return false;
			}
			if (res == 1)
			{
				enabled = true;
			}
			DataView view = new DataView(dataset.Tables["ROUTEGROUP_ROUTE"], "ROUTE_ID="+route_id, "ROUTEGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view)
			{
				int routegroup_id = (int)v.Row["ROUTEGROUP_ID"];
				res = GetEffectiveOperatorGroupRights(dataset, operator_id, routegroup_id, right_id);
				if (res == 2) // disabled
				{
					return false;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			DataView view2 = new DataView(dataset.Tables["OPERATORGROUP_OPERATOR"], "OPERATOR_ID="+operator_id, "OPERATORGROUP_ID", DataViewRowState.CurrentRows);
			foreach (DataRowView v in view2)
			{
				int operatorgroup_id = (int)v.Row["OPERATORGROUP_ID"];
				res = GetEffectiveGroupRouteRights(dataset, operatorgroup_id, route_id, right_id);
				if (res == 2) // disabled
				{
					return false;
				}
				if (res == 1)
				{
					enabled = true;
				}
			}
			if (enabled)
			{
				return true;
			}
			return false;
		}

		public static bool IsExists(DataRow row)
		{
			if (row == null)
			{
				return false;
			}
			return true;
		}
	}
}
