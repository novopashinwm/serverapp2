
namespace FORIS.TSS.BusinessLogic.Data
{
	public interface ISecureDataSupplier:
		IDataSupplier
	{
		IDataSupplier SecurityDataSupplier { get; }
	}
}
