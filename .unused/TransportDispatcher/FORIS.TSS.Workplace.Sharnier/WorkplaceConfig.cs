using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using FORIS.TSS.Workplace.Scharnier;
/* ����� WorkplaceConfig
 * 
 * ������������ ��� �������� �������� ����������� ����������,
 * ������� ����� ��������� � ����� ������������ 
 * 
 */

namespace FORIS.TSS.TransportDispatcher
{
	/// <summary>
	/// ��������� ����������� ����������
	/// </summary>
	public class WorkplaceConfig
	{
		private ConfigProperties m_DisplayOptions = new ConfigProperties();
		/// <summary>
		/// ������� ���������
		/// </summary>
		public ConfigProperties DisplayOptions
		{
			get{return m_DisplayOptions;}
		}

		/// <summary>
		/// XML ��������
		/// </summary>
		public static XmlDocument m_XmlDoc;
		public static XmlDocument XmlDoc
		{
			get{return m_XmlDoc;}
		}

		/// <summary>
		/// ����� ������ �� ���������
		/// </summary>
		private DataSet m_DsData=null;
		
		#region .ctor
		/// <summary>
		/// 
		/// </summary>
		public WorkplaceConfig()
		{
			WorkplaceConfig.m_XmlDoc=new XmlDocument();
			WorkplaceConfig.m_XmlDoc.AppendChild(WorkplaceConfig.m_XmlDoc.CreateXmlDeclaration("1.0", "UTF-8", ""));
			WorkplaceConfig.m_XmlDoc.AppendChild(WorkplaceConfig.m_XmlDoc.CreateElement("root"));	
		}


		#endregion .ctor

		#region ������
		/// <summary>
		/// �������� �������
		/// </summary>
		public void LoadProfile ()
		{
			try
			{
				this.m_DsData=Client.Instance.ILogicForUI.GetOperatorProfile(Client.Instance.CurrentUserID);
				if (this.m_DsData.Tables["OPERATOR_PROFILE"].Rows.Count > 0)
				{
					if (this.m_DsData.Tables["OPERATOR_PROFILE"].Rows[0]["PROFILE"]!=DBNull.Value)
					{
						byte[] data=(byte[])this.m_DsData.Tables["OPERATOR_PROFILE"].Rows[0]["PROFILE"];
						char[] ch=new char[data.Length];
						Encoding.GetEncoding(1251).GetDecoder().GetChars(data, 0, data.Length, ch, 0);
						WorkplaceConfig.m_XmlDoc=new XmlDocument();
						WorkplaceConfig.m_XmlDoc.LoadXml(new string(ch));	
						//�������� ��������
						XmlNode root = Client.Instance.Config.GetRootElement( "DisplayOptions" );
						if (root != null )
							Client.Instance.Config.DisplayOptions.SetProfile(new ProfileNodeList(root.ChildNodes));
					}
					else
					{
						this.m_DisplayOptions.LoadDefaultOptions();
					}
				}
			}
			catch (Exception ex)
			{
				Trace.WriteLine("������ ������ ������� �� ��\n"+ex.Message);
			}
		}

		/// <summary>
		/// ��������� ������� � ��
		/// </summary>
		/// <param name="data"></param>
		private void WriteProfile(byte[] data)
		{
			try
			{

				if (this.m_DsData != null )
				{
					if (this.m_DsData.Tables["OPERATOR_PROFILE"].Rows.Count > 0)
					{
						this.m_DsData.Tables["OPERATOR_PROFILE"].Rows[0]["PROFILE"]=data;
					}
					else
					{
						DataRow row=this.m_DsData.Tables["OPERATOR_PROFILE"].NewRow();
						row["OPERATOR_ID"]=Client.Instance.CurrentUserID;
						row["PROFILE"]=data;
						this.m_DsData.Tables["OPERATOR_PROFILE"].Rows.Add(row);
					}

					DataSet changes=this.m_DsData.GetChanges();
					
					if (changes != null)
						Client.Instance.Session.Update(changes);	

					this.m_DsData.Tables["OPERATOR_PROFILE"].AcceptChanges();
				}
			}
			catch (Exception ex)
			{
				Trace.WriteLine("������ ������ ������� � ��\n"+ex.Message);
			}
		}
		
		/// <summary>
		/// �������� ������ � �������
		/// </summary>
		/// <param name="name">��� ��������� ����</param>
		/// <param name="nodes">��������� �����</param>
		public void SaveProfile(string name, ProfileNodeList nodes)
		{
			try
			{
				XmlNode root=this.GetRootElement(name);
				root.RemoveAll();
				foreach (XmlNode node in nodes)
				{
					root.AppendChild(node);
				}
				char[] ch=WorkplaceConfig.m_XmlDoc.OuterXml.ToCharArray();
				byte[] data=new byte[ch.Length];
				Encoding.GetEncoding(1251).GetEncoder().GetBytes(ch, 0, ch.Length, data, 0, true);
				//WorkplaceConfig.m_XmlDoc.Save("c:\\ClientOptions.xml");
				this.WriteProfile(data);
			}
			catch (Exception ex)
			{
				Trace.WriteLine("������ ������ �������\n"+ex.Message);
			}
		}

		/// <summary>
		/// ����� �������
		/// </summary>
		/// <param name="parentnode">������������ ����</param>
		/// <param name="name">��� ��������</param>
		/// <returns></returns>
		private XmlNode FindElement (XmlNode parentnode, string name)
		{
			foreach (XmlNode node in parentnode.ChildNodes)
			{
				if (node.Name==name)
					return node;
			}
			return null;

		}

		/// <summary>
		/// �������� �������� ���� ��� ������
		/// </summary>
		/// <param name="name">��� ������</param>
		/// <returns></returns>
		public XmlNode GetRootElement(string name)
		{
			XmlNode node=this.FindElement(WorkplaceConfig.m_XmlDoc.DocumentElement, name);
			XmlNode result=node != null ? node : ProfileHelper.CreateElement(name);
			if (node == null )
			{
				WorkplaceConfig.m_XmlDoc.DocumentElement.AppendChild(result);
			}
			return result;
		}
		#endregion

	}

	/// <summary>
	/// ����� �������� ��� ������� ��������
	/// </summary>
	[AttributeUsage(AttributeTargets.Field)]
	public class ProfileAttribute : Attribute
	{
		private bool m_Auto=false;
		/// <summary>
		/// ������� ����-��������������
		/// </summary>
		/// <returns></returns>
		public bool Auto()
		{
			return m_Auto;
		}

		public ProfileAttribute() : this(true)
		{
		}
		
		/// <summary>
		/// .ctor
		/// </summary>
		/// <param name="auto"></param>
		public ProfileAttribute(bool auto)
		{
			m_Auto=auto;
		}
	}

	/// <summary>
	/// ���������� �������
	/// </summary>
	public class ProfileException : ApplicationException
	{
		private FieldInfo m_Field;
		/// <summary>
		/// ��� ����
		/// </summary>
		public string FieldName
		{
			get{return m_Field.Name;}
		}

		private string m_Message=string.Empty;
		public new string Message
		{
			get{return m_Message;}
		}

		public ProfileException (FieldInfo fieldname, string message)
		{
			this.m_Field=fieldname;
			this.m_Message=string.Format("������ ��� ������ � ��������\n���� {0} \n {1}", this.FieldName, message);
		}
	}
}
