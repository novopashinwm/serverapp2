using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net.Sockets;
using System.Runtime.Remoting;
using FORIS.TSS.Infrastructure.Authentication;
using FORIS.TSS.Workplace.Scharnier.Controls;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Interfaces;
using System.Reflection;
using System.Security.Principal;
using System.Threading;
using System.Windows.Forms;
using FORIS.TSS.Config;
using FORIS.TSS.WorkplaceSharnier;
using ClientState=FORIS.TSS.WorkplaceSharnier.ClientState;
using LoginHelper=FORIS.TSS.Workplace.Scharnier.Helpers.LoginHelper;

namespace FORIS.TSS.Workplace.Scharnier
{
	public class ClientBase: Component, ISessionItem, IClient
	{
		#region Properties

		/// <summary>
		/// ������������� ������
		/// </summary>
		/// <remarks>
		/// ��� ������ ���� ������ ���� ������ � ��� 
		/// ���� ������ ����������� ������ ���� ���. 
		/// � �� ������ ,���� ��������� � ������� �������, �������������� ����� 
		/// � ������� ������������ �� ������� �������� ������������� ������
		/// </remarks>
		private ISessionInfo sessionInfo = null;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public ISessionInfo SessionInfo
		{
			get { return this.sessionInfo; }
		}

		private readonly Guid clientID = Guid.NewGuid();
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public Guid ClientID
		{
			get { return this.clientID; }
		}

		private readonly string clientVersion;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public string ClientVersion
		{
			get { return this.clientVersion; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public IPersonalServer Session
		{
			get
			{
				return this.sdMain.Session;
			}
			set
			{
				this.sdMain.Session = value;
			}
		}

		#endregion // Properties

		#region Implement ISessionItem

		IPersonalServer ISessionItem.Session
		{
			get { return this.Session; }
			set { throw new NotSupportedException(); }
		}

		#endregion // Implement ISessionItem

		#region Controls & Components

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		protected FORIS.TSS.Workplace.Scharnier.Controls.SessionDispatcher sdMain;

		#endregion // Controls & Components

		#region Constructor & Dispose

		public ClientBase()
		{
			AssemblyName libName = this.GetType().Assembly.GetName();
			this.clientVersion = libName.Version.Major + "." + libName.Version.Minor + "." + libName.Version.Build + "." + libName.Version.Revision;

			InitializeComponent();

			this.Configure();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				#region pollingTimer

				/* pollingTimer ������������ ��� ���������� 
				 * ������ �� ���������� �������
				 */

				//if( this.pollingTimer != null )
				//{
				//    this.pollingTimer.Dispose();

				//    this.pollingTimer = null;
				//}

				#endregion // pollingTimer

				#region Session

				/* ������� �������� ������ �� ���� ��������� �������, 
				 * � �� ����� ����� ��������� ��
				 */

				IPersonalServer SessionHolder = this.sdMain.Session;

				this.sdMain.Session = null;
				this.sessionInfo = null;
				
				if( SessionHolder != null )
				{
					SessionHolder.Close();
				}

				#endregion // Session

				if( this.components != null )
				{
					components.Dispose();
				}

				this.dlgtLoginCompleted = null;
			}

			base.Dispose( disposing );
		}

		#endregion // Constructor & Dispose

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.sdMain = new FORIS.TSS.Workplace.Scharnier.Controls.SessionDispatcher( this.components );
			// 
			// sdMain
			// 
			this.sdMain.AfterChange += new System.EventHandler( this.sdMain_AfterChange );
			this.sdMain.BeforeChange += new System.EventHandler( this.sdMain_BeforeChange );

		}

		#endregion

		public void Init()
		{
			this.sessionInfo = Login();
		}

		#region Configure & Login & Reconnect

		//private ISessionList LoadServerLocally()
		//{
		//    // Create new domain to setup configuration file
		//    string nameOfNewDomain = "TSSserver";
		//    string nameOfConfigFile = "FORIS.TSS.ServerApplication.dll.config";
		//    string nameOfAssemblyFile = "FORIS.TSS.ServerApplication";
		//    string nameOfType = "FORIS.TSS.ServerApplication.InProcEntryPoint";
		//    DirectoryInfo root = new DirectoryInfo( AppDomain.CurrentDomain.SetupInformation.ApplicationBase );
		//    DirectoryHelper.AdjustForDebug( ref root, "..\\..\\..\\..\\" );
		//    AppDomain theDomain;
		//    IEntryPoint ep = (IEntryPoint)DomainHelper.CreateDomainAndGetEntryPoint(
		//                                    nameOfNewDomain, nameOfConfigFile, nameOfAssemblyFile, nameOfType,
		//                                    out theDomain,
		//                                    root.FullName,
		//                                    "FORIS.TSS.ServerApplication\\",
		//                                    "FORIS.TSS.ServerApplication\\bin\\debug\\"
		//                                    );
		//    try
		//    {
		//        return (ISessionList)ep.Start( false, true );
		//    }
		//    catch( Exception e )
		//    {
		//        Trace.WriteLine( "������ ������� TSS" + e.ToString() );
		//        throw;
		//    }
		//}

		private bool configured = false;

		private void Configure()
		{
			if( configured ) throw new InvalidOperationException( "Client is already configured" );

			this.configured = true;

			RemotingConfiguration.Configure( AppDomain.CurrentDomain.SetupInformation.ConfigurationFile, false );
		}

		private ISessionInfo Login()
		{
			if( this.sessionInfo != null ) throw new InvalidOperationException( "Only once" );
			
			#region Get Server (ISessionList) reference

			string ServerURL = Globals.AppSettings[ "Server" ];

			ISessionList Server = (ISessionList)RemotingServices.Connect( typeof( ISessionList ), ServerURL );

			#endregion // Get Server (ISessionList) reference

			IPersonalServer session = null;

			if( Globals.AppSettings[ "Autologon" ] != null && Globals.AppSettings[ "Autologon" ].ToLower() == "false" )
			{
                Trace.WriteLine("LoginThroughUI #1");
				session = LoginHelper.LoginThroughUI( Server, this.clientID, this.clientVersion );
			}
			else
			{
				#region SSPI

				try
				{
					// use SSPI authentication
					session = Server.CreateSession( this.clientID, clientVersion );
				}
				catch (SessionExistException ex)
				{
#if !DEBUG
                    Application.DoEvents();
#endif
                    FORIS.TSS.Resources.AboutDlg dlg = new FORIS.TSS.Resources.AboutDlg(ex.Message, FORIS.TSS.Resources.AboutDlg.IconStyle.CommonError);
					dlg.ShowDialog();
					Trace.TraceError("{0}", ex);
				}
				catch( Exception Ex )
				{
#if !DEBUG
                    Application.DoEvents();
#endif
                    // MessageBox.Show( Ex.Message, "SSPI authentication failed" );
					Trace.WriteLine(" SSPI authentication failed \\n" + Ex.Message);
				}

				#endregion // SSPI

				#region Login only

				/* �����: 
				 * ���� ������� �� ������� ��������� ������������,
				 * �� ���� �� �� ������ ��������� ����������� ���� 
				 * �� ����� ��� ����������� ������.
				 */

				try
				{
					// login failed, switch to login-only authentication
					if( session == null )
					{
                        WindowsIdentity identity = WindowsIdentity.GetCurrent();

                        string login = identity.Name;

                        Trace.WriteLine("Login only authentication login: " + login);

						GenericIdentity gi = new GenericIdentity( login );

                        Trace.WriteLine(
                            String.Format("Name = {0} IsAuthenticated={1} AuthenticationType={2}",
                            gi.Name,
                            gi.IsAuthenticated,
                            gi.AuthenticationType));

						Thread.CurrentPrincipal = new GenericPrincipal( gi, null );

						session = Server.CreateSession( this.clientID, this.clientVersion );
					}
				}
				catch( Exception Ex )
				{
#if !DEBUG
                    Application.DoEvents();
#endif
                    //MessageBox.Show( Ex.Message, "Login only authentication failed" );
					Trace.WriteLine(" Login only authentication failed \\n" + Ex.Message);
				}

				#endregion // Login only

				#region UI

				/* ��������� ���� ������ ��������������
				 * ������ ����������� ����������.
				 */

				// login failed, ask user
				if( session == null )
				{
                    Trace.WriteLine("LoginThroughUI #2");
					session = LoginHelper.LoginThroughUI( Server, this.clientID, this.clientVersion );
				}

				#endregion UI
			}

			this.sdMain.Session = session;

			#region Subscribe to RealProxy of Session object events

			//SessionProxy Proxy = (SessionProxy)RemotingServices.GetRealProxy( session );

			//if( Proxy != null )
			//{
			//    Proxy.ProxyExceptionEvent += new ProxyExceptionEventHandler( proxy_ExceptionEvent );
			//}

			#endregion // Subscribe to RealProxy of Session object events

			this.OnLoginCompleted();

			return session.SessionInfo;
		}

		private bool TryReconnect()
		{
			int nCount = Convert.ToInt32( FORIS.TSS.Config.Globals.AppSettings[ "ReconnectAttemptCount" ] );
			
			for( int i = 0; i < nCount; i++ )
			{
				try
				{
					#region Get Server (ISessionList) reference

					string ServerURL = Globals.AppSettings[ "Server" ];

					ISessionList Server = (ISessionList)RemotingServices.Connect( typeof( ISessionList ), ServerURL );

					#endregion // Get Server (ISessionList) reference

					Thread.Sleep(5000);
					IPersonalServer SessionHolder = Server.CreateSession( this.clientID, this.clientVersion );

					if( SessionHolder != null && SessionHolder.IsAlive() )
					{
						this.sdMain.Session = SessionHolder;

						return true;
					}
				}
				catch( Exception exc )
				{
					Trace.WriteLine( exc.Message );
				}
			}

			return false;
		}

		#endregion // Configure & Login & Reconnect

		#region Handle RealProxy of Session object exception

		//private void proxy_ExceptionEvent( object sender, ProxyExceptionEvent e )
		//{
		//    Trace.WriteLine( e.ex.ToString() );
		//    ShowLinkFailedMessage();
		//}


		#endregion // Handle RealProxy of Session object exception

		#region Routines

		private bool bFirstCall = true;

		/// <summary>
		/// ����������� ������� � ����������� � ������ ����� � ��������
		/// </summary>
		public void ShowLinkFailedMessage()
		{
			lock( this )
			{
				if( this.TryReconnect() )
					return;
				else
					bFirstCall = true;
				if( bFirstCall )
				{
					bFirstCall = false;

					FORIS.TSS.Resources.AboutDlg dlg =
						new FORIS.TSS.Resources.AboutDlg( Strings.LinkFailed, FORIS.TSS.Resources.AboutDlg.IconStyle.CommonError );
					dlg.ShowDialog();
					Application.Exit();
				}
			}
		}


		#endregion // Routines

		#region Events

		private EventHandler dlgtLoginCompleted;

		public event EventHandler LoginCompleted
		{
			add { this.dlgtLoginCompleted += value; }
			remove { this.dlgtLoginCompleted -= value; }
		}

		protected virtual void OnLoginCompleted()
		{
			if( this.dlgtLoginCompleted != null )
			{
				this.dlgtLoginCompleted( this, EventArgs.Empty );
			}
		}

		#endregion // Events

		#region Handle sdMain events

		private System.Threading.Timer pollingTimer;

		private void sdMain_BeforeChange( object sender, EventArgs e )
		{
			if( this.Session != null )
			{
				if( this.pollingTimer != null )
				{
					this.pollingTimer.Dispose();

					this.pollingTimer = null;
				}
			}
		}

		private void sdMain_AfterChange( object sender, EventArgs e )
		{
			SetPollingTimer();
		}

		private void SetPollingTimer()
		{
			if (Session != null)
			{
				if (pollingTimer == null)
				{
					pollingTimer =
						new System.Threading.Timer(
							new TimerCallback(pollingTimer_Callback),
							null,
							5000,
							0
							);
				}
				else
					pollingTimer.Change(5000, 0);
			}
		}

		#endregion // Handle sdMain events

		private void pollingTimer_Callback( object state )
		{
			try
			{
				//ThreadStart ts = delegate { Session.IsAlive(); };
				//ts.BeginInvoke(null, null);
				//IAsyncResult res;
				//ts.EndInvoke(null);
				Session.IsAlive();
				SetPollingTimer();
			}
			// TODO: � ��� �� ������, ���� ������ ������?
			catch (RemotingException rex)
			{
				// ��������� ������
				this.pollingTimer.Dispose();
				this.pollingTimer = null;

				Trace.WriteLine(rex.Message);
				Trace.WriteLine(rex.StackTrace);
				ShowLinkFailedMessage();

			}
			catch (SocketException sex)
			{
				// ��������� ������
				this.pollingTimer.Dispose();
				this.pollingTimer = null;
				
				Trace.WriteLine(sex.Message);
				Trace.WriteLine(sex.StackTrace);
				this.Session = null;
				ShowLinkFailedMessage();
			}
			catch (Exception ex)
			{
				//// ��������� ������
				//this.pollingTimer.Dispose();
				//this.pollingTimer = null;

				Trace.TraceError("{0}", ex);
				
			}
		}

		#region IClient Members
		
		public string ApplicationName
		{
			get { throw new NotImplementedException(); }
		}

	    public string ServerURL
	    {
            get { throw new System.NotImplementedException(); }
	        set { throw new System.NotImplementedException(); }
	    }

	    public ClientState State
		{
			get { throw new NotImplementedException(); }
		}

		FORIS.TSS.BusinessLogic.Server.IPersonalServer IClient.Session
		{
			get { throw new NotImplementedException(); }
		}

		public void Connect( string sessionManagerUrl )
		{
			throw new NotImplementedException();
		}

		public void Start()
		{
			throw new NotImplementedException();
		}

		public void Stop()
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}
