using System;
using System.Collections.Generic;
using System.Text;
using FORIS.TSS.BusinessLogic;

namespace FORIS.TSS.Workplace.Scharnier
{
	interface ISessionHolder
	{
		/// <summary>
		/// ������ ����� �� �������� �������
		/// </summary>
		IPersonalServer Session { get; }
	}
}
