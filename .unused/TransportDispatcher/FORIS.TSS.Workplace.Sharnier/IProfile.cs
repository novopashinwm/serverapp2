using System;
using System.Collections;
using System.Diagnostics;
using System.Reflection;
using System.Xml;
using System.Xml.XPath;
using FORIS.TSS.TransportDispatcher;

namespace FORIS.TSS.Workplace.Scharnier
{
	/// <summary>
	/// ��������� �����
	/// </summary>
	public class ProfileNodeList : IEnumerable
	{
		private Hashtable m_List;
		/// <summary>
		/// ���-�� ��������� ��������� �����
		/// </summary>
		public int Count
		{
			get{return this.m_List.Count;}
		}
		
		/// <summary>
		/// .ctor
		/// </summary>
		public ProfileNodeList()
		{
			this.m_List=new Hashtable();	
		}

		/// <summary>
		/// .ctor
		/// </summary>
		/// <param name="Nodes">������ �����</param>
		public ProfileNodeList( XmlNodeList Nodes )
		{
			this.m_List=new Hashtable(Nodes.Count);	
			
			foreach (XmlNode elem in Nodes)
			{
				this.m_List.Add(elem.Name, elem);
			}
		}

		/// <summary>
		/// �������� ����������
		/// </summary>
		/// <returns></returns>
		public IEnumerator GetEnumerator ()
		{
			return this.m_List.Values.GetEnumerator();
		}

		/// <summary>
		/// �������� ���� �� �����
		/// </summary>
		public XmlNode this[string name]
		{
			get
			{
				return (XmlNode)this.m_List[name];
			}
		}

		/// <summary>
		/// ���������� ���� � ���������
		/// </summary>
		/// <param name="name">��� ����</param>
		/// <returns></returns>
		public bool Contains (string name)
		{
			return this.m_List.ContainsKey(name);
		}

		/// <summary>
		/// �������� ����
		/// </summary>
		/// <param name="node"></param>
		public void Add (XmlNode node)
		{
			this.m_List.Add(node.Name, node);
		}

		
		/// <summary>
		/// �������� ����
		/// </summary>
		public void Clear ()
		{
			this.m_List.Clear();
		}
		
	}
	/// <summary>
	/// �������� ������ � ��������
	/// </summary>
	public interface IProfile
	{
		/// <summary>
		/// ������� �������
		/// </summary>
		/// <returns></returns>
		ProfileNodeList GetProfile();
		/// <summary>
		/// ���������� �������
		/// </summary>
		void SetProfile(ProfileNodeList nodes);
	}

	/// <summary>
	/// ����� ��� ������ � ��������
	/// </summary>
	public class ProfileHelper
	{
		/// <summary>
		/// �������� �����
		/// </summary>
		private ProfileNodeList m_Nodes=new ProfileNodeList();
		/// <summary>
		/// .ctor
		/// </summary>
		public ProfileHelper()
		{
			
		}

		/// <summary>
		/// �������� �������� �����
		/// </summary>
		/// <param name="ob">��������� ������</param>
		/// <param name="nodes">��������� �����</param>
		public void SetProfile(object ob, ProfileNodeList nodes)
		{
			FieldInfo currentField=null;
			try
			{
				this.m_Nodes=nodes;
				FieldInfo[] fields=ProfileHelper.GetOptionFields(ob);
				foreach (FieldInfo fi in fields)
				{
					currentField=fi;
					Object member=fi.GetValue(ob);
					#region ���� �������������� ��������� IProfile
					if (member is IProfile)
					{
						XmlNode node = this.m_Nodes.Contains(fi.Name) ? this.m_Nodes[fi.Name] : null;
						if (node != null )
							((IProfile)member).SetProfile( new ProfileNodeList(node.ChildNodes) ); 
					}		
					#endregion

					#region ���� ValueType
					else
					{
						if (member.GetType().IsValueType)
						{
							XmlNode node = this.m_Nodes.Contains(fi.Name) ? this.m_Nodes[fi.Name] : null;
							if (node != null)
							{
								object val=ProfileHelper.GetValue(fi, node.Attributes["value"].Value);
								fi.SetValue(ob, val);
							}
						}
					#endregion

					#region ���� � ������� ������
						else
						{
							new ProfileException (fi, "�������� ���������� �������� [ProfileAttribute]");
						}
					}
					#endregion
				}
			}
			catch (Exception ex)
			{
				Trace.TraceError("{0}", ex);
				//throw new ProfileException(currentField, "��������� ���������� ��������� �������");
			}
		}

		/// <summary>
		/// �������� ������� ��� �������
		/// </summary>
		/// <param name="ob">��������� ������</param>
		public ProfileNodeList GetProfile(object ob)
		{
			FieldInfo currentField=null;
			try
			{
				FieldInfo[] fields=ProfileHelper.GetOptionFields(ob);
				foreach (FieldInfo fi in fields)
				{
					currentField=fi;
					Object member=fi.GetValue(ob);
					#region ���� �������������� ��������� IProfile
					if (member is IProfile)
					{
						XmlNode node = this.m_Nodes.Contains(fi.Name) ? this.m_Nodes[fi.Name] : null;
						if (node == null)
						{
							node=ProfileHelper.CreateElement(fi.Name);
							this.m_Nodes.Add(node);
						}					
						else
						{
							node.RemoveAll();
						}

						foreach(XmlNode nodeChild in ((IProfile)member).GetProfile())
							node.AppendChild(nodeChild);
					}
					#endregion

					#region ���� ValueType
					else 
					{
						if (member.GetType().IsValueType)
						{
							XmlNode node = this.m_Nodes.Contains(fi.Name) ? this.m_Nodes[fi.Name] : null;
							if (node == null)
							{
								node=ProfileHelper.CreateElement(fi.Name, fi.GetValue(ob).ToString());
								this.m_Nodes.Add(node);						
							}
							else
							{
								node.Attributes["value"].Value=member.ToString();
							}
						}
					#endregion

					#region ���� � ������� ������
						else
						{
							new ProfileException (fi, "�������� ���������� �������� [ProfileAttribute]");
						}
					}
					#endregion
				}
				return this.m_Nodes;
			}
			catch (Exception ex)
			{
				Trace.TraceError("{0}", ex);
				return this.m_Nodes;
				//throw new ProfileException(currentField, "��������� ���������� ��������� �������");
			}
		}

		/// <summary>
		/// ������� �������
		/// </summary>
		/// <param name="name">��� ���������</param>
		/// <returns></returns>
		public static XmlElement CreateElement(string name)
		{
			XmlElement result=WorkplaceConfig.XmlDoc.CreateElement(name);
			return result;
		}

		/// <summary>
		/// ������� �������
		/// </summary>
		/// <param name="name">��� ��������</param>
		/// <param name="value">�������� ��������</param>
		/// <returns></returns>
		public static XmlNode CreateElement(string name, string value)
		{
			XmlElement result=WorkplaceConfig.XmlDoc.CreateElement(name);
			XmlAttribute atr=WorkplaceConfig.XmlDoc.CreateAttribute("value");
			atr.Value=value;
			result.Attributes.Append(atr);
			return result;
		}

		/// <summary>
		/// �������
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public XmlNode CreateCustomElement (string name)
		{
			XmlNode result=this.m_Nodes[name];
			if (result != null)
			{
				result.RemoveAll();
			}
			else
			{
				result= ProfileHelper.CreateElement(name);
				this.m_Nodes.Add(result);
			}
			return result;
		}

		/// <summary>
		/// �������� �������� �� ���� ����
		/// </summary>
		/// <param name="fi">����</param>
		/// <param name="value">��������� ������������� �������� ����</param>
		/// <returns>������ � ����� ����</returns>
		public static object GetValue(FieldInfo fi, string value)
		{
			object result=null;
			switch (fi.FieldType.ToString())
			{
				case "System.Int32":
					result=Convert.ToInt32(value);
					break;
				case "System.Double":
					result=Convert.ToDouble(value);
					break;
				case "System.String":
					result=value;
					break;
				case "System.Boolean":
					result=Convert.ToBoolean(value);
					break;
				default:
					if (fi.FieldType.IsEnum)
					{
						FieldInfo[] fields=fi.FieldType.GetFields();
						foreach (FieldInfo f in fields)
						{
							if (f.Name==value)
								result=f.GetValue(f.FieldType);
						}            

					}
					break;

			}
			return result;
			
		}

		/// <summary>
		/// �������� ��� �������� ������ ���������� ��������� UserOptionAttribute
		/// </summary>
		/// <param name="ob"></param>
		/// <returns></returns>
		public static FieldInfo[] GetOptionFields(object ob)
		{
			ArrayList temp=new ArrayList();
			foreach (FieldInfo fi in ob.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic))
			{
				if (fi.IsDefined(typeof(ProfileAttribute), false))
				{
					object[] attr=fi.GetCustomAttributes(typeof(ProfileAttribute), false);
					foreach (ProfileAttribute at in attr)
					{
						if (at.Auto())
							temp.Add(fi);
					}
					
				}
			}
			FieldInfo[] result=new FieldInfo[temp.Count];
			temp.CopyTo(result);
			return result;
		}

	}
}
