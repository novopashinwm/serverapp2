using System.Data;

namespace FORIS.TSS.TransportDispatcher
{
	/// <summary>
	/// Summary description for UserLoggedInEventArgs.
	/// </summary>
	public class UserLoggedInEventArgs
	{
		protected DataRow row = null;

		public DataRow Row
		{
			get { return row; }
			set { this.row = value; }
		}

		public UserLoggedInEventArgs(DataRow row)
		{
			this.row = row;
		}
	}
}
