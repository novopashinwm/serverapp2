using System;
using System.Collections;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common;

namespace FORIS.TSS.TransportDispatcher
{
	/// <summary>
	/// Temporary stub
	/// </summary>
	public class UIHookClientChannelSinkProvider : IClientChannelSinkProvider
	{
		private static object lockObject = new object();

		#region IClientChannelSinkProvider
		
		IClientChannelSinkProvider nextProvider = null;
		
		public IClientChannelSink CreateSink(IChannelSender channel, string url, object remoteChannelData)
		{
			IClientChannelSink next = nextProvider.CreateSink(channel, url, remoteChannelData);

			return new TacticalHack(next);
		}

		public IClientChannelSinkProvider Next
		{
			get
			{
				return nextProvider;
			}
			set
			{
				nextProvider = value;
			}
		}

		public UIHookClientChannelSinkProvider(IDictionary properties, ICollection providerData)
		{

		}

		#endregion
	}
}
