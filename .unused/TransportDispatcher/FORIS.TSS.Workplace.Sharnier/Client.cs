using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.Remoting;
using System.Threading;
using System.Windows.Forms;

using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using FORIS.TSS.TransportDispatcher.RuleManager;
using FORIS.TSS.Workplace.Scharnier;
using Globals = FORIS.TSS.Config.Globals;
using Timer = System.Threading.Timer;

namespace FORIS.TSS.TransportDispatcher
{
	/// <summary>
	/// </summary>
	public class Client :  
		ClientBase,
		ISecurityChecker
	{
		#region Events

		/// <summary>
		/// ������� ������ �������� �� �� �� �����
		/// </summary>
		public event ShowTraceHandler ShowTrace;

		/// <summary>
		/// ������� ������ ������� �� �����
		/// </summary>
		public event ShowHistoryHandler ShowHistory;

		#region ReceiveLog

		private ReceiveLogEventHandler dlgtReceiveLog;

		/// <summary>
		/// ������� ������� ������ � ��������� ������� � ������� ��������
		/// </summary>
		public event ReceiveLogEventHandler ReceiveLogEvent
		{
			add { this.dlgtReceiveLog += value; }
			remove { this.dlgtReceiveLog -= value; }
		}

		protected virtual void OnReceiveLog( ReceiveLogEventArgs e )
		{
			if (dlgtReceiveLog == null)
				return;

			if (e.MobilUnits.Count == 0)
			{
				MessageBox.Show("��� ���������� ������������� �������� ��� ������ ������� ��������",
								"������� �������� �� �" + e.GarageNumber,
								MessageBoxButtons.OK, MessageBoxIcon.Information);
				//return;
			}

			if (e.LogLength > e.LogLengthNeeded)
				MessageBox.Show("��� ���������� ������������� �������� �� ������ ������ ������� ����� ���������� �� ��� ������� �������\n(" + e.LogLengthNeeded.ToString() + " �� " + e.LogLength.ToString() + ").", "������� �������� �� �" + e.GarageNumber, MessageBoxButtons.OK, MessageBoxIcon.Information);
			
			// ����� ��� ������������ �����!!! ��. 1.3
			this.dlgtReceiveLog( e );

			MonitoringHelper.ReceivingHistory = false;
		}

		#endregion // ReceiveLog

		#region LastPositions

		private LastPositionsEventHandler dlgtLastPositions;

		/// <summary>
		/// ������� ������� �������� �������� ������ � ��������� �������� ��
		/// </summary>
		public event LastPositionsEventHandler LastPositionsEvent
		{
			add { this.dlgtLastPositions += value; }
			remove { this.dlgtLastPositions -= value; }
		}

		protected virtual void OnLastPositions( PositionsEventArgs e )
		{
			if( this.dlgtLastPositions != null )
			{
				this.dlgtLastPositions( e ); 
			}
		}

		#endregion // LastPositions

		#endregion // Events

		#region Switches

		/// <summary>
		/// ������������� ��������������� ��� ���������� ����������
		/// </summary>
		public static TraceSwitch ts_ScheduleGenerator = new TraceSwitch("ScheduleGenerator", "Schedule Generator related info");

		public static TraceSwitch ts_MonitoringControl = new TraceSwitch("MonitoringControl", "Monitoring Control related info");

		#endregion // Switches
		
		/// <summary>
		/// ���������� � ������ ������������
		/// </summary>
		private SystemRights systemRights;
		
		/// <summary>
		/// ���������� � ������ ������������
		/// </summary>
		public SystemRights SystemRights
		{
			get { return systemRights; }
		}
		
		public void ShowTraceOnMap(ShowTraceArgs args)
		{
			if(ShowTrace == null)
				return;
			ShowTrace(args);
		}
		
		public void ShowHistoryOnMap(ShowTraceArgs args)
		{
			if(ShowHistory == null)
				return;
			ShowHistory(args);
		}
		/// <summary>
		/// ��� ����������, ������� �� ����� ���������
		/// </summary>
		public string ApplicationName = "";

		/// <summary>
		/// ��������� ����������� ����������
		/// </summary>
		private readonly WorkplaceConfig config = new WorkplaceConfig(); 
	
		/// <summary>
		/// ��������� ����������� ����������
		/// </summary>
		public WorkplaceConfig Config
		{
			get { return config; }
		}

        private IRuleManager m_ruleManager;

		public IRuleManager RuleManager
		{
			get { return m_ruleManager; }
		}

		/// <summary>
		/// ������ ��� ��������� �������
		/// </summary>
		Timer historyTimer;
		int historyTimerInterval = 5000;  // �������� �������
		int historyTimerAttempts = 2;     // ���� ���-�� �������
		int historyTimerAttemptsCurr = 0; // ������� ���-�� �������
		int historyPositions = 0;         // ���-�� ���������� ������������ �������
		int historyTimeInterval = 60;     // �������� ����� ������������� ���������, ���.
		int historyMaxCount = 500;        // ������������ ���������� ������������ �������
		
		public int HistoryTimerInterval
		{
			get { return historyTimerInterval; }
		}
		
		public int HistoryTimerAttempts
		{
			get { return historyTimerAttempts; }
		}
		
		public int HistoryTimeInterval
		{
			get { return historyTimeInterval; }
		}

		public int HistoryMaxCount
		{
			get { return historyMaxCount; }
		}
		
		/// <summary>
		/// ����, ����������� ������������� ��������� ������ �� ������� �� �������
		/// </summary>
		bool receiveEventFlag = true;
		
		public bool ReceiveEventFlag
		{
			get { return receiveEventFlag; }
		}

		public ILogicForUI ILogicForUI
		{
			get { return base.Session.LogicForUI; }
		}

		/// <summary>
		/// ������������� ���������
		/// </summary>
		public int CurrentUserID
		{
			get { return this.SessionInfo.OperatorInfo.OperatorID; }
		}

		/// <summary>
		/// ������������� ������
		/// </summary>
		public int CurrentSessionID
		{
			get { return this.SessionInfo.SessionID; }
		}

		/// <summary>
		/// ������ ��� ��������� 
		/// </summary>
		public string CurrentUserName
		{
			get { return this.SessionInfo.OperatorInfo.Name; }
		}

		#region Instance

		// TODO: ������ ������ ������� � ���� ����������!
		// ��� ��������� ������ �������������� �����
		private static Client instance = null;

		private static readonly object createLock = new object();

		public static Client Instance
		{
			get
			{
				lock( Client.createLock )
				{
					if( Client.instance == null )
					{
						Client.instance = new Client();
					}

					return Client.instance;
				}
			}

		}


		#endregion // Instance

		#region Constructor & Dispose

		private Client()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				Trace.WriteLine("Client.Dispose(true)");

				this.dlgtLastPositions = null;
				this.dlgtReceiveLog = null;
			}

			base.Dispose(disposing);
		}
		
		#endregion // Constructor & Dispose

		#region Configure() & Login()

		protected override void OnLoginCompleted()
		{
			// Post Login actions

			object objForDebug = this.Session.LogicForUI.GetDataForStatusBar();
			DataSet ds2 = (DataSet)objForDebug;
			if( ds2.Tables[ "OPERATOR" ].Rows.Count == 1 )
			{
				DataRow row = ds2.Tables[ "OPERATOR" ].Rows[ 0 ];
				//				string name = row["NAME"].ToString();
				UserLoggedInEventArgs e = new UserLoggedInEventArgs( row );
				OnUserLoggedIn( e );
				// Obtain User ID
				//current_user_id = (int)row[ "OPERATOR_ID" ];
				//current_user_name = (string)row[ "NAME" ];
			}
			// �������������� ��������� �������
			if( Globals.AppSettings[ "ReceiveEvent" ] != null )
				receiveEventFlag = Boolean.Parse( Globals.AppSettings[ "ReceiveEvent" ] );

			// ��������� �������
			if( Globals.AppSettings[ "HistoryTimerInterval" ] != null )
				historyTimerInterval = Int32.Parse( Globals.AppSettings[ "HistoryTimerInterval" ] );  // �������� �������
			if( Globals.AppSettings[ "HistoryTimerAttemptCount" ] != null )
				historyTimerAttempts = Int32.Parse( Globals.AppSettings[ "HistoryTimerAttemptCount" ] );  // ���� ���-�� �������
			if( Globals.AppSettings[ "HistoryTimeInterval" ] != null )
				historyTimeInterval = Int32.Parse( Globals.AppSettings[ "HistoryTimeInterval" ] );  // �������� ����� ������������� ���������, ���.
			if( Globals.AppSettings[ "HistoryMaxCount" ] != null )
				historyMaxCount = Int32.Parse( Globals.AppSettings[ "HistoryMaxCount" ] );  // ������������ ���������� ������������ �������

			#region �������� ��� ����, ����� �������� RB_Monitoring

			// �������� ������� 

			// �������� ����������
			ApplicationName = this.Session.GetApplicationName();

			#endregion // �������� ��� ����, ����� �������� RB_Monitoring

			// ��������� ���������� � ������ ������������
			systemRights = this.Session.GetSystemRights();
			
			if( systemRights == null )
				systemRights = new SystemRights();

            if (Globals.AppSettings["RuleManager"] == "true")
            {
                FileInfo fliProccessingFileInfo = new FileInfo(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "\\FORIS.TSS.TransportDispatcher.RuleManager.dll");
                //��������� ��������� ��������������� .dll.
                Assembly assProcessingAssembly = Assembly.LoadFile(fliProccessingFileInfo.FullName);
                //����������� ��� ��������������� .dll.
                string strAssembleName = assProcessingAssembly.FullName;
                //������ ��� �������� ������ ����� � ��������������� .dll.
                //��������� ������ ����� � ��������������� .dll.
                Type[] typeList = assProcessingAssembly.GetTypes();

                //���������������� ������� ���� ����� � ��������������� .dll. - ����� ������ ������
                foreach (Type type in typeList) //���������������� ������� ���� ����������� � ��������������� ����
                    if (type.BaseType != null && type.BaseType.Name == typeof(RuleManagerBase).Name)
                    {
                        ObjectHandle h = Activator.CreateInstance(strAssembleName, type.ToString());
                        m_ruleManager = (IRuleManager)h.Unwrap();
                        break;
                    }
            }

            base.OnLoginCompleted();
		}

		#endregion // Configure() & Login()

		public UserLoggedInEventHandler UserLoggedIn;
		
		protected void OnUserLoggedIn(UserLoggedInEventArgs e)
		{
			if (UserLoggedIn != null)
			{
				UserLoggedIn(this, e);
			}
		}


		#region SendLastPositions(ArrayList lastPositions)

		/// <summary>
		/// ������� ������� �������� �������� ������ � ��������� �������� ��
		/// </summary>
		public void SendLastPositions( IMobilUnit[] lastPositions)
		{
			this.OnLastPositions( new PositionsEventArgs( lastPositions ) );
		}

		#endregion // SendLastPositions(ArrayList lastPositions)

		// �������� ��� �������� ���������� � �� ��� ��������� ������� �� ��������
		DataSet historyDS = null;
		string historyGarageNumber = String.Empty;
		int historyUnique = 0;
		private DateTime historyFrom;
		private DateTime historyTo;
		private DateTime historyBegin;
		/// <summary>
		/// �������� ��������� ������� (�������)
		/// </summary>
		private readonly TimeSpan historyWaitLimit = TimeSpan.FromSeconds(300); 
		
		/// <summary>
		/// ��������� ������� �� ��������/UDP
		/// </summary>
		/// <param name="vehicle_id">ID ��</param>
		/// <param name="from">�</param>
		/// <param name="to">��</param>
		/// <param name="mediaType">��� ���������, �� �������� ����� ������� �������</param>
		public void GetLog(int vehicle_id, DateTime from, DateTime to, Media mediaType)
		{
			// �������� ��� �������� ���������� � �� ��� ��������� ������� �� ��������
			historyDS = this.Session.GetDataForVehiclePropertiesEdit( vehicle_id );
			
			if(historyDS == null)
			{
				MessageBox.Show("� ���� ������ ��� ���������� � ������������� �� (id = " + vehicle_id + ")!" +
				                Environment.NewLine +
				                "���������� � ���������� ��������������",
				                "������ ��� ��������� ������� ��������",
				                MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;				
			}
			
			historyGarageNumber = 
				historyDS.Tables["VEHICLE"].Select("VEHICLE_ID = " + vehicle_id)[0]["GARAGE_NUMBER"].ToString();
			historyUnique = vehicle_id;
			historyFrom = from;
			historyTo = to;

			MonitoringHelper.ReceivingHistory = true;
			MonitoringHelper.DataLinkLog(vehicle_id, from, to, mediaType);
			
			//������ ������� ��� ��������� �������
			historyTimerAttemptsCurr = 0;
			historyPositions = 0;
			if(historyTimer == null)
				historyTimer = new Timer(new TimerCallback(CheckHistoryList), null, historyTimerInterval, 0);
			else
				historyTimer.Change(historyTimerInterval, 0);
			historyBegin = DateTime.Now;
		}

		/// <summary>
		/// �������� ������� �������� �� �� ��
		/// </summary>
		/// <param name="vehicle_id">ID ��</param>
		/// <param name="from">�</param>
		/// <param name="to">��</param>
		/// <param name="logInterval">� ����������, �</param>
		/// <param name="logCount">���������� �������</param>
		/// <remark>����� �������� ������� �������� �� ������� �� ��.</remark>
		public void GetLog(int vehicle_id, DateTime from, DateTime to, int logInterval, int logCount)
		{
			if(vehicle_id > 0)
			{
				//old version
				if(historyTimer != null)
					historyTimer.Change(Timeout.Infinite, 0);
				
				// �������� ��� �������� ���������� � �� ��� ��������� ������� �� ��������
				historyDS = this.Session.GetDataForVehiclePropertiesEdit(vehicle_id);
			
				if(historyDS == null)
				{
					MessageBox.Show("� ���� ������ ��� ���������� � ������������� �� (id = " + vehicle_id + ")!" +
					                Environment.NewLine +
					                "���������� � ���������� ��������������",
					                "������ ��� ��������� ������� ��������",
					                MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}

				historyGarageNumber = historyDS.Tables["VEHICLE"].Select(
					"VEHICLE_ID = " + vehicle_id)[0]["GARAGE_NUMBER"].ToString();
				historyUnique = vehicle_id;

				MonitoringHelper.ReceivingHistory = true;

				// ���������� � �������, ��� ���� �� ������� �������� �� �� 
				ReceiveLogEventArgs args = base.Session.GetLogFromDB(
					(time_t)from.ToUniversalTime(),
					(time_t)to.ToUniversalTime(),
					vehicle_id,
					logInterval,
					logCount);

				if (args != null)
					OnReceiveLog(args);
				else
				{
					Trace.WriteLine("GetLog");
					MonitoringHelper.ReceivingHistory = false;
				}
			}
		}

        protected SortedList ClientHL = new SortedList(300);
		protected int historyListCount = 0;
		/// <summary>
		/// ��������� ������� ������������ ������� 
		/// </summary>
		/// <param name="state"></param>
		protected void CheckHistoryList(object state)
		{
			try
			{
				Debug.WriteLine("CheckHistoryList");
				
				SortedList historyList = base.Session.GetHistory();
				historyListCount = 0;

                foreach(object key in historyList.Keys)
                {
                	IMobilUnit mu = (IMobilUnit) historyList[key];
                	DateTime muTime = TimeHelper.GetLocalTime(mu.Time);

					if (mu.Unique == historyUnique &&
						muTime >= historyFrom &&
						muTime <= historyTo)
					{
						if (!ClientHL.ContainsKey(key))
						{
							ClientHL.Add(key, historyList[key]);
							historyListCount++;
						}
					}
                }

				// ����������� ����� ��������
				bool limit = (DateTime.Now - historyBegin) > historyWaitLimit;

				if (historyListCount != 0 && !limit)
				{

					historyPositions += historyListCount;
					
                    historyTimer.Change(historyTimerInterval, 0);					
				}
				else
				{
					if (ClientHL.Count != 0)
					{
						OnReceiveLog(new ReceiveLogEventArgs(ClientHL, historyUnique, ClientHL.Count, historyMaxCount, historyGarageNumber));
						ClientHL.Clear();
					}
					// � ������ ��� ������������ �������
					// ��������� �������:
					// - ������ ������ ��� ��������� �������
					// - ������ ��� �� ����������
					else
					{
						if (++historyTimerAttemptsCurr >= historyTimerAttempts)
						{
							// ���� ������� ������ �� ����, �� �������� ������ ���������
							if (historyPositions == 0)
							{
								MessageBox.Show("��� ���������� ������������� ��������\r\n" +
								                "�� ������� �������� ������ ������� ��������\r\n" +
								                "� �����������.",
								                "������� �������� �� �" + historyGarageNumber,
								                MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
							// ������������� ������
							historyTimer.Change(Timeout.Infinite, 0);
							MonitoringHelper.ReceivingHistory = false;
						}
						else
							historyTimer.Change(historyTimerInterval, 0);
					}
				}
			}
			catch (Exception e)
			{
				Trace.WriteLine("Client.CheckHistoryList -> " + e.Message);
				//this.ThrowSocketException();
				MonitoringHelper.ReceivingHistory = false;
			}
        }

		#region ISecurityChecker Members

		public bool Check( string key )
		{
			SystemRights sr = this.SystemRights;

			switch ( key )
			{
				case "RB_vehicles":
					if ( sr.Contains( SystemRight.EditVehicles )
						|| sr.Contains( SystemRight.ServerAdministration ) )
					{
						return true;
					}
					break;
				case "RB_Drivers":
					if ( sr.Contains( SystemRight.EditDrivers )
						|| sr.Contains( SystemRight.ServerAdministration ) )
					{
						return true;
					}
					break;
				case "RB_Operators":
					if ( sr.Contains( SystemRight.SecurityAdministration )
						|| sr.Contains( SystemRight.ServerAdministration ) )
					{
						return true;
					}
					break;
				case "RB_2L_routes":
					if ( sr.Contains( SystemRight.EditRouteAndWaybill )
						|| sr.Contains( SystemRight.ServerAdministration ) )
					{
						return true;
					}
					break;
				case "RB_ServerAdministration":
					if ( sr.Contains( SystemRight.ServerAdministration ) )
					{
						return true;
					}
					break;
				case "RB_Analytic":
					if ( sr.Contains( SystemRight.ViewAnalytic )
						|| sr.Contains( SystemRight.ServerAdministration ) )
					{
						return true;
					}
					break;
				case "ControllersForm":
					if ( sr.Contains( SystemRight.ServerAdministration ) )
					{
						return true;
					}
					break;
				case "WorkstationsEditorForm":
					if ( sr.Contains( SystemRight.ServerAdministration ))
					{
						return true;
					}
					break;
				
				case "RB_WayBill":
					if ( sr.Contains( SystemRight.ViewWaybillHistory ) || sr.Contains( SystemRight.ServerAdministration ) )
					{
						return true;
					}
					break;

				case "RB_Journal":
					if ( sr.Contains( SystemRight.JournalAccess )
						|| sr.Contains( SystemRight.ServerAdministration ) )
					{
						return true;
					}
					break;
                case "RB_Journals":
                    if (sr.Contains(SystemRight.JournalAccess)
                        || sr.Contains(SystemRight.ServerAdministration))
                    {
                        return true;
                    }
                    break;
				case "RB_Calendar":
					return true;
				case "RB_Graphic":
					return true;
				case "RB_Order":
					return true;

				case "RB_DispatcherRoll":
				case "RB_LineRoll":
					if ( sr.Contains( SystemRight.DispatcherRoll ) 
                        || sr.Contains( SystemRight.ServerAdministration ) )
					{
						return true;
					}
					break;

				case "RB_Monitoring":
					if ( sr.Contains( SystemRight.ViewVehicleCurrentStatus )
						|| sr.Contains( SystemRight.ServerAdministration ) )
					{
						return true;
					}
					break;
				case "Map_GIS":
					if ( sr.Contains( SystemRight.SecurityMapGis )
						|| sr.Contains( SystemRight.ServerAdministration ) )
					{
						return true;
					}
					break;
				case "TSS_Monitoring":
                    if (sr.Contains(SystemRight.ServerAdministration))
                    {
                        return true;
                    }
                    break;

				case "RB_Rosters":
                    if (sr.Contains(SystemRight.Roster) 
                        || sr.Contains(SystemRight.ServerAdministration))
                    {
                        return true;
                    }
                    break;

				case "RuleManager":
					if ( sr.Contains( SystemRight.ReceiveMessages )
						|| sr.Contains( SystemRight.ServerAdministration ) )
					{
						return true;
					}
					break;

				case "RB_ScheduleGenerator":
					if ( sr.Contains( SystemRight.EditRouteAndWaybill ) 
                        || sr.Contains( SystemRight.ServerAdministration ) )
					{
						return true;
					}
					break;
				case "RB_FactorsManager":
					if ( sr.Contains( SystemRight.EditRouteAndWaybill ) 
                        || sr.Contains( SystemRight.ServerAdministration ) )
					{
						return true;
					}
					break;
				case "RB_ScheduleGeneratorManager":
					if ( sr.Contains( SystemRight.EditRouteAndWaybill ) 
                        || sr.Contains( SystemRight.ServerAdministration ) )
					{
						return true;
					}
					break;
				case "RB_TripsAndPoints":
					if ( sr.Contains( SystemRight.EditRouteAndWaybill ) 
                        || sr.Contains( SystemRight.ServerAdministration ) )
					{
						return true;
					}
					break;

				case "VehicleOwners":
                    if (sr.Contains(SystemRight.ServerAdministration))
                    {
                        return true;
                    }
                    break;

				case "RB_WayBillEditor":
					if ( sr.Contains( SystemRight.EditWaybill ) 
                        || sr.Contains( SystemRight.ServerAdministration ) )
					{
						return true;
					}
					break;

				case "RB_HtmlHelp":
					// always visible
					return true;

				case "RB_TaskLog":
					if ( sr.Contains( SystemRight.ServerAdministration ) )
					{
						return true;
					}
					break;

                case "RB_ServerMailAdministration":
                    if (sr.Contains(SystemRight.SecurityAdministration)
                        || sr.Contains(SystemRight.ServerAdministration))
                    {
                        return true;
                    }
                    break;

                case "RB_OperatorMailAdministration":
                    if (sr.Contains(SystemRight.AccessToTreeOnOperatorMail)
                        || sr.Contains(SystemRight.ServerAdministration))
                    {
                        return true;
                    }
                    break;
                case "RB_Zone":
                    if (sr.Contains(SystemRight.EditZone) 
                        || sr.Contains(SystemRight.ServerAdministration))
                    {
                        return true;
                    }
                    break;
            }

			return false;
		}

		#endregion  // ISecurityChecker Members
	}
	
	public class ShowTraceArgs : EventArgs
	{
		public int [] vehicle_id;
	}
	
	public delegate void ShowTraceHandler(ShowTraceArgs args);
	public delegate void ShowHistoryHandler(ShowTraceArgs args);
	
}