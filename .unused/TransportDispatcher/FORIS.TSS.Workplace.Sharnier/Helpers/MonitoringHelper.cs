using System;
using System.Collections;
using System.Diagnostics;

// FORIS
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Terminal;
using FORIS.TSS.MobilUnit;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common;

namespace FORIS.TSS.TransportDispatcher
{
	/// <summary>
	/// ����� �������� ��� ���������� GSM �����������
	/// </summary>
	public class MonitoringHelper
	{
		/// <summary>
		/// ���� ��������� �������
		/// </summary>
		private static bool receivingHistory = false;

		private static object lockReceivingHistory = new object();

		/// <summary>
		/// ���� ��������� �������
		/// </summary>
		public static bool ReceivingHistory
		{
			get
			{
				lock (lockReceivingHistory)
				{
					return receivingHistory;
				}
			}
			set
			{
				lock (lockReceivingHistory)
				{
					receivingHistory = value;
					OnAction(new MonitoringControlActionArgs(MonitoringControlAction.Log, receivingHistory));
				}
			}
		}

		#region Events

		/// <summary>
		/// ������� ����������� ��� ��������� ���������
		/// </summary>
		public static event OperatorEventHandler OperatorEvent;

		/// <summary>
		/// ������� ������������ ��� ���������/����������� ������-���� �������� � �������� �� ��
		/// </summary>
		public static event MonitoringControlActionHandler Action;

		#endregion Events

		/// <summary>
		/// ������ �� ��������� ��
		/// </summary>
		/// <param name="vehicle_id"></param>
		/// <param name="_switch">������� ������ ��� ��������</param>
		/// <param name="_overdriving">�������������� ��������</param>
		public static void DataLink(int vehicle_id, bool _switch, bool _overdriving)
		{
			// �������������� �������� ��� ��������������� ��������
			int add_par = _overdriving ? 1 : 0;
			// �������� ������
			PARAMS par = new PARAMS();
			par["switch"] = _switch ? 1 + add_par : 0 - add_par;
			if( Client.Instance.Session != null)
				Client.Instance.Session.SendCommand(CmdType.Trace, par, vehicle_id);
		}

		#region DataLinkLog(int vehicle_id, DateTime from, DateTime to)
		/// <summary>
		/// ��������� ������� �� ��������
		/// </summary>
		/// <param name="vehicle_id">ID ��</param>
		/// <param name="from">�</param>
		/// <param name="to">��</param>
        /// <param name="mediaType">��� ���������, �� �������� ����� ������� �������</param>
		public static void DataLinkLog(int vehicle_id, DateTime from, DateTime to, Media mediaType)
		{
			PARAMS par = new PARAMS();
			par["fromtime"] = from.Date.AddMinutes((int)from.TimeOfDay.TotalMinutes).ToUniversalTime();
			par["totime"] = to.Date.AddMinutes((int)to.TimeOfDay.TotalMinutes).ToUniversalTime();
			if(vehicle_id > 0)
				par["switch"] = 1; 
			else
				par["switch"] = 0;

			Client.Instance.Session.SendCommand(CmdType.GetLog, mediaType, par, vehicle_id);
		}

		/// <summary>
		/// ��������� ������� �� ��
		/// </summary>
		/// <param name="vehicle_id">ID ��</param>
		/// <param name="from">�</param>
		/// <param name="to">��</param>
		/// <param name="logInterval">� ����������, �</param>
		/// <param name="logCount">���������� �������</param>
		public static void DataLinkLog(int vehicle_id, DateTime from, DateTime to, int logInterval, int logCount)
		{
			#region /* �������� ������� �� ���� ������ */
			if(vehicle_id > 0)
			{
				PARAMS par = new PARAMS();
				par["fromtime"] = (int)(from.ToUniversalTime() - TimeHelper.year1970).TotalSeconds;
				par["totime"] = (int)(to.ToUniversalTime() - TimeHelper.year1970).TotalSeconds;
				par["switch"] = true; 
				par["interval"] = logInterval;
				par["count"] = logCount;
				// ���������� � �������
				Client.Instance.Session.SendCommand(CmdType.GetLogFromDB, par, new int[]{vehicle_id});
			}
			#endregion // �������� ������� �� ���� ������
		}
		#endregion // DataLinkLog(int vehicle_id, DateTime from, DateTime to)

		/// <summary>
		/// ������� SMS ��� ��������� ������� �������, ���� �� SMS 
		/// </summary>
		/// <param name="vehicle_id">id ��</param>
		/// <remarks>�������� ������ ��� ����������� MJ2016</remarks>
		public static void SendSMS(int vehicle_id)
		{
			try
			{
				if( Client.Instance.Session != null )
					Client.Instance.Session.SendCommand( CmdType.AskPosition, new int[] { vehicle_id } );
			}
			catch(Exception ex)
			{
				Trace.TraceError("{0}", ex);
			}
		}

		/// <summary>
		/// control the device. 
		/// pars: 1. ignition, 2. engine, 3. pump, 4. headlight, 5. lights, 6. horn.
		/// values: 1 - on, 0 - off
		/// </summary>
		/// <remarks>� ������ ������ �������� ������ 3 �������</remarks>
		public static void SendControlCommand(int vehicle_id, int operator_id, bool ignition, bool engine, bool pump, bool headlight, bool lights, bool horn)
		{
			try
			{
				PARAMS par = new PARAMS();
				par["ignition"] =  ignition ? 1 : 0;
				par["engine"] = engine ? 1 : 0;
				par["pump"] = pump ? 1 : 0;
				par["headlight"] = headlight ? 1 : 0;
				par["lights"] = lights ? 1 : 0;
				par["horn"] = horn ? 1 : 0;
				// ���������� � �������
				if( Client.Instance.Session != null )
					Client.Instance.Session.SendCommand( CmdType.Control, par, new int[] { vehicle_id } );

				DateTime time = DateTime.UtcNow;
				// ������ ���������� � �������� ��������� ���� ������� � �� � ���������� ���� ����������� �� ��� �������
				if(OperatorEvent != null)
				{
					if( Client.Instance.Session != null )
					{
						/*	
					1	��������� ������������                            
					2	��������� ��������                                
					3	���������� ����������� 
					*/
						if(lights)
						{
							RaiseOperatorEvent(new OperatorEventArgs(vehicle_id, operator_id, 1, time));
							Client.Instance.Session.LogOperatorEvent( operator_id, 1, vehicle_id, time );
						}
						if(horn)
						{
							RaiseOperatorEvent(new OperatorEventArgs(vehicle_id, operator_id, 2, time));
							Client.Instance.Session.LogOperatorEvent( operator_id, 2, vehicle_id, time );
						}
						if(!pump)
						{
							RaiseOperatorEvent(new OperatorEventArgs(vehicle_id, operator_id, 3, time));
							Client.Instance.Session.LogOperatorEvent( operator_id, 3, vehicle_id, time );
						}
					}
				}
			}
			catch(Exception ex)
			{
				Trace.TraceError("{0}", ex);
			}
		}

		/// <summary>
		/// ������������ ������� �������� ���������
		/// </summary>
		/// <param name="args">��������� �������� ���������</param>
		private static void RaiseOperatorEvent(OperatorEventArgs args)
		{
			Delegate[] delegates = OperatorEvent.GetInvocationList();
			IEnumerator ie = delegates.GetEnumerator();
			//
			while(ie.MoveNext())
			{
				OperatorEventHandler handler = (OperatorEventHandler)ie.Current;
				try
				{
					handler.BeginInvoke(args, new AsyncCallback(CallBackOperatorEvent), handler);  //null, null);
				}
				catch(Exception e)
				{
					throw e;
				}
			}
		}

		/// <summary>
		/// �������� ������������ ������ 
		/// </summary>
		private static void CallBackOperatorEvent(IAsyncResult ar)
		{
			// ������� ��������� ����� ���������
			OperatorEventHandler handler = (OperatorEventHandler)ar.AsyncState;
			try
			{
				handler.EndInvoke(ar);
			}
			catch(Exception e)
			{
				Trace.WriteLine("CallBackOperatorEvent -> " + e.Message);
			}
		}

		public static void SendText(int vid, string text)
		{
			try
			{
				PARAMS par = new PARAMS();
				par["text"] = text;
				if( Client.Instance.Session != null )
					Client.Instance.Session.SendCommand( CmdType.SendText, par, vid );
			}
			catch(Exception ex)
			{
				Trace.WriteLine(ex.Message, "Monitoring");
				throw;
			}
		}

		public static void OnAction(MonitoringControlActionArgs args)
		{
			if (Action != null)
				Action(args);
		}

	}

	public delegate void MonitoringControlActionHandler(MonitoringControlActionArgs args);

	public enum MonitoringControlAction
	{
		/// <summary>
		/// ������� ������ �� ����������� ����
		/// </summary>
		Today,
		/// <summary>
		/// ��������� �������
		/// </summary>
		Log,
		/// <summary>
		/// ��������� �����
		/// </summary>
		Call,
		/// <summary>
		/// ����� �������� (�� � ������ �����)
		/// </summary>
		Trace,
		/// <summary>
		/// ����� ���������� ������� ��� �������� �����
		/// </summary>
		FillFinder
	}

	public class MonitoringControlActionArgs
	{
		public MonitoringControlAction Action;
		public bool Pushed;

		public MonitoringControlActionArgs(MonitoringControlAction action, bool pushed)
		{
			Action = action;
			Pushed = pushed;
		}
	}

}