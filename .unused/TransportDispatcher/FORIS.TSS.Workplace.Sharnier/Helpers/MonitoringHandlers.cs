using System;
using System.Collections.Generic;
using System.Diagnostics;

// FORIS
using FORIS.TSS.Terminal;
using FORIS.TSS.MobilUnit;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common;

namespace FORIS.TSS.TransportDispatcher
{
	#region -- �������� --
	
	/// <summary>
	/// ������� ������� ����������� ��� ��������� ���������
	/// </summary>
	public delegate void OperatorEventHandler(OperatorEventArgs args);

	/// <summary>
	/// ������� ��� ������ ������� ��������� ������� �������� �� �� ��
	/// </summary>
	public delegate void GetLog_DB_Handler(int id, DateTime fromTime, DateTime toTime, int logInterval, int logCount);
	
	/// <summary>
	/// ������� ��� ������ ������� ��������� ������� �������� �� ����� GSM)
	/// </summary>
	public delegate void GetLog_GSM_Handler(int id, DateTime fromTime, DateTime toTime, Media mediaType);

	/// <summary>
	/// ������� ��� ������ ������� ������� ��������� ������ ��
	/// </summary>
	public delegate void SendText_Handler(List<int> ids, string text);

	#endregion // -- �������� -- //

	#region -- ��������� ������� --

	#region class OperatorEventArgs
	/// <summary>
	/// �������� ������� ����������� ��� ��������� ���������
	/// </summary>
	[Serializable]
	public class OperatorEventArgs : System.EventArgs
	{
		/// <summary>
		/// ������������� ��, � ������� �������� ��������
		/// </summary>
		private int vehicle_id;
		/// <summary>
		/// ������������� ��, � ������� �������� ��������
		/// </summary>
		public int VehicleID
		{
			get { return vehicle_id; }
		}

		/// <summary>
		/// ������������� ���������
		/// </summary>
		private int operator_id;
		/// <summary>
		/// ������������� ���������
		/// </summary>
		public int OperatorID
		{
			get { return operator_id; }
		}
		
		/// <summary>
		/// ������������� �������
		/// </summary>
		private int event_id;
		/// <summary>
		/// ������������� �������
		/// </summary>
		public int EventID
		{
			get { return event_id; }
		}

		/// <summary>
		/// ����� ������������� �������
		/// </summary>
		private DateTime time;
		/// <summary>
		/// ����� ������������� �������
		/// </summary>
		public DateTime Time
		{
			get { return time; }
		}

		#region .ctor
		/// <summary>
		/// �������� ������� ����������� ��� ��������� ���������
		/// </summary>
		/// <param name="vehicle_id">������������� ��, � ������� �������� ��������</param>
		/// <param name="operator_id">������������� ���������</param>
		/// <param name="event_id">������������� �������</param>
		/// <param name="time">����� ������������� �������</param>
		public OperatorEventArgs(int vehicle_id, int operator_id, int event_id, DateTime time)
		{
			this.vehicle_id = vehicle_id;
			this.operator_id = operator_id;
			this.event_id = event_id;
			this.time = time;
		}
		#endregion // .ctor
	}
	#endregion // class OperatorEventArgs //

	#endregion // -- ��������� ������� -- //
}