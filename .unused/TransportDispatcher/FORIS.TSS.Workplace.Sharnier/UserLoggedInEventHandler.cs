namespace FORIS.TSS.TransportDispatcher
{
	/// <summary>
	/// Summary description for UserLoggedInEventHandler.
	/// </summary>
	public delegate void UserLoggedInEventHandler(object sender, UserLoggedInEventArgs e);
}
