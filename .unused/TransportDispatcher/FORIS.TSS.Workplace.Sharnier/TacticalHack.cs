using System;
using System.Collections;
using System.Diagnostics;
using System.Data;
using System.Security.Principal;
using System.Threading;
//using System.Windows.Forms;
using System.Configuration;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Lifetime;
using System.Runtime.Remoting.Services;
using System.Runtime.Remoting.Messaging;
using System.Net.Sockets;

// FORIS
using FORIS.TSS.Common;
using FORIS.TSS.Terminal;
using FORIS.TSS.BusinessLogic;
//using FORIS.TelBill.CashRegister.GUI.Interfaces;
using FORIS.TSS.MobilUnit;
using Timer = System.Threading.Timer;

namespace FORIS.TSS.TransportDispatcher
{
	public class TacticalHack : IClientChannelSink, IMessageSink
	{
		protected IClientChannelSink nextClient = null;
		
		protected IMessageSink nextIMessageSink = null;
		
		public TacticalHack( IClientChannelSink nextClient)
		{
			this.nextClient = nextClient;

			this.nextIMessageSink = (IMessageSink)nextClient;
		}

		public void ProcessMessage(
			System.Runtime.Remoting.Messaging.IMessage msg, 
			ITransportHeaders requestHeaders, 
			System.IO.Stream requestStream, 
			out ITransportHeaders responseHeaders, 
			out System.IO.Stream responseStream)
		{
			try
			{
				nextClient.ProcessMessage(msg, requestHeaders, requestStream, out responseHeaders, out responseStream);
			}
			catch (RemotingException ex)
			{
				Trace.WriteLine(ex.ToString());
				Client.Instance.ShowLinkFailedMessage();
				responseHeaders = null;
				responseStream = null;
			}
			catch (SocketException ex)
			{
				Trace.WriteLine(ex.ToString());
				Client.Instance.ShowLinkFailedMessage();
				responseHeaders = null;
				responseStream = null;
			}
		}

		public IClientChannelSink NextChannelSink
		{
			get
			{
				return nextClient;
			}
		}

		public void AsyncProcessRequest(IClientChannelSinkStack sinkStack, System.Runtime.Remoting.Messaging.IMessage msg, ITransportHeaders headers, System.IO.Stream stream)
		{
			// TODO:  Add ClientSink.AsyncProcessRequest implementation
		}

		public void AsyncProcessResponse(IClientResponseChannelSinkStack sinkStack, object state, ITransportHeaders headers, System.IO.Stream stream)
		{
			// TODO:  Add ClientSink.AsyncProcessResponse implementation
		}

		public System.IO.Stream GetRequestStream(System.Runtime.Remoting.Messaging.IMessage msg, ITransportHeaders headers)
		{
			// TODO:  Add ClientSink.GetRequestStream implementation
			return null;
		}

		public IDictionary Properties
		{
			get
			{
				// TODO:  Add ClientSink.Properties getter implementation
				return null;
			}
		}

		public IMessage SyncProcessMessage(IMessage msg)
		{
			try
			{
				IMessage result = nextIMessageSink.SyncProcessMessage(msg);
				if (result is MethodResponse)
				{
					MethodResponse resp = (MethodResponse)result;
					if (resp.Exception != null)
					{
						Exception ex = resp.Exception;
						if (ex is RemotingException || ex is SocketException)
						{
							Trace.WriteLine(ex.ToString());
							Client.Instance.ShowLinkFailedMessage();
							return result;
						}
					}
				}
				return result;
			}
			catch (RemotingException ex)
			{
				Trace.WriteLine(ex.ToString());
				Client.Instance.ShowLinkFailedMessage();
				return null;
			}
			catch (SocketException ex)
			{
				Trace.WriteLine(ex.ToString());
				Client.Instance.ShowLinkFailedMessage();
				return null;
			}
		}

		public IMessageSink NextSink
		{
			get
			{
				return nextIMessageSink;
			}
		}

		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			// TODO:  Add Client.AsyncProcessMessage implementation
			return null;
		}
	}
}
