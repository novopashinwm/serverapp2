using System.ComponentModel;

using FORIS.TSS.Common;
using FORIS.TSS.BusinessLogic;

namespace FORIS.TSS.Workplace.Scharnier.Controls
{
	public class SessionDispatcher: Dispatcher<ISessionItem>, ISessionItem
	{
		private IPersonalServer session;
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public IPersonalServer Session
		{
			get
			{
				return this.session;
			}
			set
			{
				if( this.session != value )
				{
					this.OnBeforeChange();

					if( value != null )
					{
						this.session = value;

						for( int i = 0; i < this.Ambassadors.Count; i++ )
						{
							Ambassador<ISessionItem> ambassador = this.Ambassadors[ i ];

							if( ambassador.Item != null && ambassador.Enabled )
								ambassador.Item.Session = value;
						}
					}
					else
					{
						for( int i = this.Ambassadors.Count - 1; i >= 0; i-- )
						{
							Ambassador<ISessionItem> ambassador = this.Ambassadors[ i ];

							if( ambassador.Item != null && ambassador.Enabled )
								ambassador.Item.Session = value;
						}

						this.session = value;
					}

					this.OnAfterChange();

					/* ������ ��������, ��� ��������� Ambassadors 
					 * ��������� ����� ��������� OnAfterChange()
					 */
				}

			}
		}

		private readonly SessionAmbassadorCollection ambassadors = new SessionAmbassadorCollection();
		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
		public SessionAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}

		protected override AmbassadorCollection<ISessionItem> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

		public SessionDispatcher()
			: base()
		{

		}
		public SessionDispatcher( IContainer container )
			: base()
		{
			container.Add( this );

		}


		protected override void OnBeforeSetItem( Ambassador<ISessionItem> ambassador )
		{
			if( ambassador.Item != null )
			{
				ambassador.Item.Session = null;
			}
		}
		protected override void OnAfterSetItem( Ambassador<ISessionItem> ambassador )
		{
			if( ambassador.Item != null )
			{
				ambassador.Item.Session = this.session;
			}
		}
	}

	public class SessionAmbassadorCollection: AmbassadorCollection<ISessionItem>
	{
		public new SessionAmbassador this[ int Index ]
		{
			get { return (SessionAmbassador)base[ Index ]; }
		}
	}

	public class SessionAmbassador: Ambassador<ISessionItem>
	{

	}

	public interface ISessionItem
	{
		IPersonalServer Session { get; set; }
	}
}
