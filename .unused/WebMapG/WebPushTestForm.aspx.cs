﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FORIS.TSS.UI.WebMapG
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var contextHelper = new ContextHelper(Context, GetType());
            if(!contextHelper.OperatorID.HasValue)
                throw new SecurityException("not allowed");

            if(!contextHelper.PersonalWebServer.IsSuperAdministrator)
                throw new SecurityException("not allowed");
        }
    }
}