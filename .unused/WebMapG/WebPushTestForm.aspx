﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebPushTestForm.aspx.cs" Inherits="FORIS.TSS.UI.WebMapG.WebForm1" %>

<!DOCTYPE html>

<html>
<head>
    <title></title>
    <style type="text/css">
        .updated {
            background: red;
        }
    </style>
    <script type="text/javascript" src="includes/scripts/lib/jquery/jquery-1.6.4.min.js"></script>
    <script type="text/javascript" src="includes/scripts/requirejs/plugins/jquery.signalR-1.1.4.min.js"></script>
    <script type="text/javascript" src="signalr/hubs"></script>
    <script type="text/javascript">
        $(function () {
            $('#add').attr('disabled', 'disabled');

            var timeout;
            $.connection.hub.stateChanged(function (data) {
                // переподключение
                if (data.newState == 2) {
                    $.connection.hub.stop();
                    $.ajax({
                        url: 'services/messages.aspx?a=get-token',
                        success: function (userToken) {
                            // подключаемся к хабу и указываем токен в качестве аутентификации
                            $.connection.hub.qs = { 'userToken': userToken.token };
                            // важно указывать корректный относительный путь к приложению, иначе <script src="signalr/hubs"> не грузится
                            $.connection.hub.url = "/webmap/signalr";
                            $.connection.hub.start({ transport: ['webSockets', 'serverSentEvents', 'longPolling'] });
                            $('#add').removeAttr('disabled');
                        }
                    });
                }
            });

            // создаем хаб
            var hub = $.connection.messagesHub;
            // обработчик события которое отправляется с сервера
            hub.client.alarmVehicle = function(vehicleId, alarmed) {
                if (alarmed)
                    alert('alert' + vehicleId);
                else
                    alert('no alert' + vehicleId);
            };
            hub.client.receivePushMessage = function(message) {
                if (message.state !== 'Done') {
                    $.post('services/messages.aspx',
                        {
                            a: 'update-messages-status',
                            messageIds: JSON.stringify([message.id]),
                            status: 'Read'
                        })
                        .done(function(data) {
                            if ($.inArray(message.id, data.items) < 0)
                                alert('fail');
                        });
                } else {
                    alert('done' + message.id);
                }
            };
            hub.client.messageUpdates = function (updates) {
                clearTimeout(timeout);
                timeout = setTimeout(function () {
                    $('.updated').removeClass('updated');
                }, 2000);
                for (var msisdn in updates) {
                    if (updates.hasOwnProperty(msisdn)) {
                        var msisdnUpdates = updates[msisdn];
                        var messageUpdates = msisdnUpdates['update'];
                        var messageInserts = msisdnUpdates['insert'];
                        for (var messageUpdateId in messageUpdates) {
                            if (messageUpdates.hasOwnProperty(messageUpdateId)) {
                                var messageUpdate = messageUpdates[messageUpdateId].message;
                                $('#tabs #tab' + msisdn + ' #' + messageUpdate.id).addClass('updated');
                            }
                        }
                        for (var messageId in messageInserts) {
                            if (messageInserts.hasOwnProperty(messageId)) {
                                var message = messageInserts[messageId].message;
                                $('#tabs #tab' + msisdn).prepend('<li id="' + message.Id + '">' + message.id + ')' + message.body + '</li>');
                            }
                        }
                    }
                }
            };

            // получаем токен для авторизации на хабе
            $.ajax({
                url: 'services/messages.aspx?a=get-token',
                success: function (userToken) {
                    // подключаемся к хабу и указываем токен в качестве аутентификации
                    $.connection.hub.qs = { 'userToken': userToken.token };
                    // важно указывать корректный относительный путь к приложению, иначе <script src="signalr/hubs"> не грузится
                    $.connection.hub.url = "/webmap/signalr";
                    $.connection.hub.start({ transport: ['webSockets', 'serverSentEvents', 'longPolling'] });
                    $('#add').removeAttr('disabled');
                }
            });

            var recordsPerPage = 10;
            var firstMessageId = 0;
            $('#add').bind('click', function () {
                var msisdn = $('#msisdn').val();
                if ($('#tabs #tab' + msisdn).length == 0)
                    $('#tabs').append('<div id="tab' + msisdn + '"></div>');
                var page = Math.floor($('#tabs #tab' + msisdn).children().length / recordsPerPage) + 1;
                
                $.ajax({
                    url: 'services/messages.aspx?a=get-messages-to-msisdn&page=' + page + '&recordsPerPage=' + recordsPerPage + '&msisdn=' + msisdn,
                    success: function (result) {
                        if (page == 1) {
                            // нужно для корректной пагинации
                            firstMessageId = result.items[0].id;
                        }
                        for (var i in result.items) {
                            var message = result.items[i];
                            $('#tabs #tab' + msisdn).append('<li id="' + message.id + '">' + message.id + ')' + message.body + '</li>');
                        }

                        // подписываемся на обновления по заданному msisdn
                        hub.server.subscribeMsisdn(msisdn, result.historyId);
                    }
                });
            });

            $('#remove').bind('click', function() {
                var msisdn = $('#msisdn').val();
                // отписываемся от уведомлений по заданному msisdn
                hub.server.unsubscribeMsisdn(msisdn);
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="tabs">
            
        </div>
        <input type="text" name="msisdn" id="msisdn"/>
        <a id="add" href="#">add</a>
        <a id="remove" href="#">remove</a>
   </form>
</body>
</html>
