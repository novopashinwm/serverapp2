﻿using System;
using System.Web;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using RU.NVG.UI.WebMapG.Model.Common;

namespace FORIS.TSS.UI.WebMapG.mobile
{
	public partial class MobileDefault : BasePage
	{
		/// <summary>
		/// AS: Переопределяем свойство, проверяющее, залогинен ли юзер.
		/// Если он на странице логина, то эта проверка не нужна, нужна дальнейшая обработка
		/// </summary>
		protected override bool IsUserLogged
		{
			get { return base.IsUserLogged || Request.QueryString["a"] == null || Request.QueryString["a"] == "default"; }
		}
		
		protected override void Page_Load(object sender, EventArgs e)
		{
			MandatoryPageLoad();

			IsMobile = true;
			if (Request.Cookies["ssid"] != null &&
				(GetParamValue("a") == "default" || string.IsNullOrEmpty(GetParamValue("a"))))
			{
				SessionID = Request.Cookies["ssid"].Value;

				var operatorID = OperatorID;
				if (operatorID != null && SessionID == null)
				{
					//TODO: убрать это безобразие, страницу может создавать только контейнер приложений
					var baseLoginPage = new BaseLoginPage();
					var l = baseLoginPage.TryLogIn(operatorID.Value);
					if (l != LoginResult.Success)
					{
						var cookie = new HttpCookie("ssid") {Expires = DateTime.Now.AddDays(1)};
						Response.Cookies.Add(cookie);
					}
					else
					{
						RedirectResponse(@"Default.aspx?a=getvehicles&includeAddresses=true");
					}
				}
				else
				{
					var cookie = new HttpCookie("ssid") {Expires = DateTime.Now.AddDays(-1)};
					Response.Cookies.Add(cookie);
				}
			}
			base.Page_Load(sender, e);
		}

		//[MyAction("default")]
		[PageAction("getvehicles")]
		public void Default()
		{
			IsMobile = true;
			GetVehicles();
		}

		[PageAction("default")]
		public void LoginPage()
		{
			IsMobile = true;
			RenderLoginPage();
		}

		[PageAction("askPosition")]
		public void AskPosition()
		{
			int[] mobileIds = GetMobileIds();
			string mobileIDString = GetParamValue("mobileid");

			bool succeded = false;
			foreach (var mobileId in mobileIds)
			{
				var command = IWebPS.RegisterCommand(
					new Command {TargetID = mobileId, Type = CmdType.AskPosition});
				succeded = command.Result != null && command.Status == CmdStatus.Received;
			}

			//Браузер пользователя перенаправляется на другую страницу, 
			//чтобы избежать повторных отправок запроса при обновлении страницы
			RedirectResponse(
				string.Format(
					"Default.aspx?a=askPositionResult&mobileid={0}&succeded={1}", mobileIDString, succeded));
		}

		private int[] GetMobileIds()
		{
			var mobileIDString = GetParamValue("mobileid");
			var mobileidStrings = mobileIDString.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);

			return Array.ConvertAll(mobileidStrings, int.Parse);
		}

		[PageAction("askPositionResult")]
		public void AskPositionResult()
		{
			IsMobile = true;
			TemplateFile = "default";
			AddProperty("template", "messagePage");

			var succeded = Convert.ToBoolean(GetParamValue("succeded"));

			var monitoringObjects = Array.FindAll(Array.ConvertAll(GetMobileIds(), GetObjectById), item => item != null);

			foreach (var monitoringObject in monitoringObjects)
			{
				var name = string.IsNullOrEmpty(monitoringObject.GarageNumber)
							   ? ("<" + GetMessageData("Noname") + ">")
							   : monitoringObject.GarageNumber;

				AddMessage(
					new Message(
						Severity.Normal,
						string.Empty,
						name + ": " +
						GetMessageData(succeded ? "mlpRequestWasSent" : "mlpRequestWasFailed")));
			}
			SetHtmlContentType(); 
			ResponseSerializationBeg();
			foreach (var monitoringObject in monitoringObjects)
			{
				monitoringObject.WriteXml(Writer);
			}

			ResponseSerializationEnd();
		}

		[PageAction("logout")]
		public new void Logout()
		{
			try
			{
				LogOutInternal();

				RedirectResponse("default.aspx");
			}
			catch (Exception ex)
			{
				AddMessage(new Message(Severity.Error, GetMessageData("error"), ex.Message));
				WriteLog(ex);
			}
		}
	}
}