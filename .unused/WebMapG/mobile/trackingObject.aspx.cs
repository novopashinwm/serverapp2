﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;
using FORIS.TSS.UI.WebMapG;

public partial class mobile_trackingObject : BasePage
{
	const int MaxScale = 8;
	//static readonly TimeSpan _timeout = TimeSpan.FromSeconds(5);

	private static readonly WebProxy Proxy;
	private static readonly HMACSHA1 GoogleRequestSigningAlgorithm;
	private static bool ProxyGoogleStaticMapsRequests;

	private String GoogleMapsClientID
	{
		get
		{
			if (string.Equals(Request.Url.Host, "nika-glonass.ru", StringComparison.OrdinalIgnoreCase) ||
				Request.Url.Host.EndsWith(".nika-glonass.ru", StringComparison.OrdinalIgnoreCase))
				return "client=gme-sitronics";
			if (Request.Url.Host == "nika-glonass.local")
				return "key=ABQIAAAAMHPaklDnUYzsPoF0XzrbvhRKzu2Xd3WNR-SoxDp2CyGoycmbVBQUtKPDashwfnt-InCXYLx9BNsYvw";
			if (Request.Url.Host == "10.128.28.59")
				return "key=ABQIAAAA4ONJ9BR88PCz74A6dCmqQhS7CcibL6GDyHLvdo84z5WGl4rcTxTk3uAOwDXXgxNvacYdUp90IBjmXA";

			return string.Empty;
		}
	}
	
	static mobile_trackingObject()
	{
		Proxy = WebProxy.GetDefaultProxy();
		if (Proxy != null)
		{
			if (Proxy.Address == null || string.IsNullOrWhiteSpace(Proxy.Address.ToString()))
			{
				var proxyAddressFromConfig = ConfigurationManager.AppSettings["ProxyAddress"];
				
				if (!string.IsNullOrWhiteSpace(proxyAddressFromConfig))
				{
					try
					{
						Proxy.Address = new Uri(proxyAddressFromConfig);
					} 
					catch (Exception exception)
					{
						System.Diagnostics.Trace.TraceError("Unable to set proxy address from config {0}: {1}",
							proxyAddressFromConfig, exception);
					}
				}
				else
				{
					System.Diagnostics.Trace.TraceInformation("ProxyAddress config is empty");
				}
			}
			
			System.Diagnostics.Trace.TraceInformation(string.Format("Proxy address: {0}", Proxy.Address));
		}
		else
		{
			System.Diagnostics.Trace.TraceInformation("No default proxy");
		}
		Proxy.UseDefaultCredentials=true;


		string keyString = ConfigurationManager.AppSettings["GoogleStaticMapsApiKey"];
		if (!string.IsNullOrWhiteSpace(keyString))
		{
			// converting key to bytes will throw an exception, need to replace '-' and '_' characters first.
			string usablePrivateKey = keyString.Replace("-", "+").Replace("_", "/");
			byte[] privateKeyBytes = Convert.FromBase64String(usablePrivateKey);
			GoogleRequestSigningAlgorithm = new HMACSHA1(privateKeyBytes);
		}
		else
		{
			GoogleRequestSigningAlgorithm = null;
		}

		ProxyGoogleStaticMapsRequests = string.Equals(ConfigurationManager.AppSettings["ProxyGoogleStaticMapsRequests"], "true", StringComparison.OrdinalIgnoreCase);
	}
	
	/// <summary>
	/// AS: Получение позиции объекта слежения
	/// </summary>
	[PageAction("getPosition")]
	public void GetPosition()
	{
		IsMobile = true;

		int objectId;
		Int32.TryParse(GetParamValue("id"), out objectId);

		TemplateFile = "default";
		AddProperty("template", "mobileObjectPos");

		int picWidth, picHeight;
		GetPicWidthHeight(out picWidth, out picHeight);
		
		AddProperty("picHeight", picHeight.ToString());
		AddProperty("picWidth", picWidth.ToString());
		

		var trackObj = GetVehicleByID(objectId, true);
		trackObj.iconUrl = Vehicles.VehiclesInfo.ProcessIconUrl(trackObj.iconUrl);
		var scale = GetPicScale(trackObj);
		if (scale != null)
			AddProperty("scale", (scale+9).ToString());
		AddProperty("VehicleKind", trackObj.vehicleKind.ToString());
		AddProperty("maxScale", MaxScale.ToString());
		AddProperty("maxScale", MaxScale.ToString());

		if (trackObj.radius.HasValue)
		{
			AddProperty("radius", trackObj.radius.Value.ToString());
		}

		AddProperty("mapType", GetParamValue("mapType"));
		AddProperty("lat", trackObj.lat.ToString().Replace(",", "."));
		AddProperty("lng", trackObj.lng.ToString().Replace(",", "."));
		var jsonParams = JsonHelper.SerializeObjectToJson(trackObj);
		AddProperty("jsonObject", jsonParams);
		ResponseSerializationBeg();
		trackObj.WriteXml(Writer);
		if (trackObj.lat != null &&
			trackObj.lng != null &&
			trackObj.posLogTime != null &&
			-90 < trackObj.lat && trackObj.lat < 90 &&
			-180 < trackObj.lng && trackObj.lng < 180)
		{
			SerializeAddress((float)trackObj.lng, (float)trackObj.lat, MapGuid);
		}
		Writer.WriteStartElement("mapTypes");
		Writer.WriteElementString("mapType", "roadmap");
		Writer.WriteElementString("mapType", "hybrid");
		Writer.WriteEndElement();

		SerializeMapDescriptions();
		ResponseSerializationEnd();
	}

	[PageAction("getPositionImage")]
	public void GetPositionImage()
	{
		IsMobile = true;
		
		var vehicleId = int.Parse(GetParamValue("id"));
		var vehicle = GetVehicleByID(vehicleId);
		
		if (vehicle == null)
			return;

		int picWidth, picHeight;
		GetPicWidthHeight(out picWidth, out picHeight);

		var scale = GetPicScale(vehicle);
		var imageUrl = GetPositionImagePath(true, vehicle, scale, picWidth, picHeight, GetParamValue("mapType"));
		string icon = GetVehicleImagePath(vehicle);
		if (!string.IsNullOrWhiteSpace(icon))
		{
			AddProperty("icon", icon);
		}
		WriteLog("Requesting position image by url " + imageUrl);
		var httpRequest = (HttpWebRequest)WebRequest.CreateDefault(
			new Uri(imageUrl));
		httpRequest.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)";
		//httpRequest.Timeout = (int)_timeout.TotalMilliseconds;     // 5 secs
		httpRequest.Headers.Add("Accept-Language", CurrentCulture.ToString());
		httpRequest.UseDefaultCredentials = true;
		httpRequest.Method = "GET";
		httpRequest.KeepAlive = false;
		httpRequest.Proxy = Proxy;
		
		using (var webResponse = (HttpWebResponse) httpRequest.GetResponse())
		using (var responseStream = webResponse.GetResponseStream())
		{
			WriteLog("Response received, content type is " + webResponse.ContentType);
			if (responseStream != null)
			{
				using (var bufferedStream = new BufferedStream(responseStream))
				{
					Response.Clear();
					Response.ContentType = webResponse.ContentType;
					var buffer = new byte[4*1024];
					
					while (true)
					{
						var bytesRead = bufferedStream.Read(buffer, 0, buffer.Length);
						if (bytesRead == 0)
							break;			        	
						Response.OutputStream.Write(buffer, 0, bytesRead);
					}
					Response.End();

				}
				webResponse.Close();
				responseStream.Close();
			}
		}
	}

	private void GetPicWidthHeight(out int picWidth, out int picHeight)
	{
		//По умолчанию для PDA
		picWidth = 220;

		var httpCookie = Request.Cookies.Get("screenWidth");
		int widthFromCookie;
		if (httpCookie != null && !string.IsNullOrWhiteSpace(httpCookie.Value) && int.TryParse(httpCookie.Value, out widthFromCookie))
		{
			if (widthFromCookie > 105)
				picWidth = widthFromCookie - 5;
		}
		else
		{
			HttpBrowserCapabilities browser = Request.Browser;

			if (browser.ScreenPixelsWidth > 100)
			{
				picWidth = browser.ScreenPixelsWidth;
			}
			else
			{
				BrowserTypes bi = BrowserInfo.getBrowserType(browser);
				if (bi == BrowserTypes.mobilePhone)
				{
					//Для телефонов
					picWidth = 200;
				}
			}
		}
		if (picWidth > 512)
			picWidth = 512;

		picHeight = (int)(picWidth * 0.75);
	}
	
	private int? GetPicScale(Vehicle vehicle)
	{
		var scale = 
			vehicle != null &&
			vehicle.logTime != null && 
			vehicle.radius == 0 ? 6 : 4;
		
		if (!string.IsNullOrEmpty(GetParamValue("scale")))
		{
			int scaleValue;
			if (Int32.TryParse(GetParamValue("scale"), out scaleValue))
			{
				if (scaleValue > MaxScale)
				{
					scale = MaxScale;
				}
				else if (scaleValue < 0)
				{
					scale = 0;
				}
				else
				{
					scale = scaleValue;
				}
			}
		}
		
		return scale;
	}
	
	private static readonly NumberFormatInfo NumberFormatInfo =
		new NumberFormatInfo {NumberDecimalSeparator = "."};

	private string GetPositionImagePath(
		bool referToGoogle,
		Vehicle vehicle, 
		int? scale,
		int width,
		int height, 
		string mapType)
	{
		if (vehicle == null || 
			vehicle.posLogTime == null ||
			vehicle.lat == null ||
			vehicle.lng == null)
			return null;
		
		if (!referToGoogle)
			return "trackingObject.aspx?a=getPositionImage&id=" + vehicle.id + "&scale=" + scale + "&ssid=" + SessionID + "&isMobile=true";

		var sb = new StringBuilder(1000);
		sb.Append("/maps/api/staticmap?center=");
		sb.Append(vehicle.lat.Value.ToString(NumberFormatInfo));
		sb.Append(",");
		sb.Append(vehicle.lng.Value.ToString(NumberFormatInfo));
		if (scale != null)
		{
			sb.Append("&zoom=");
			sb.Append(scale.Value + 9);
		}
		else
		{
			sb.Append("&visible=");
		}
		sb.Append("&maptype=");
		sb.Append(mapType);
		sb.Append("&size=");
		sb.Append(width);
		sb.Append("x");
		sb.Append(height);
		sb.Append("&maptype=mobile");
		sb.Append("&markers=");
		string icon = GetVehicleImagePath(vehicle);

		if (!string.IsNullOrWhiteSpace(icon))
		{
			sb.Append("icon:");
			sb.Append(Uri.EscapeUriString(icon));
			sb.Append("%7C");
		}
		sb.Append(vehicle.lat.Value.ToString(NumberFormatInfo));
		sb.Append(",");
		sb.Append(vehicle.lng.Value.ToString(NumberFormatInfo));
		sb.Append("&");
		sb.Append(GoogleMapsClientID);
		sb.Append("&sensor=false");

		
		if (vehicle.radius != null && vehicle.radius > 0)
		{
			sb.Append('&');
			sb.Append(GetPositionRadiusPathString(vehicle.lat.Value, vehicle.lng.Value, vehicle.radius.Value));
		}

		var request = sb.ToString();
		if (GoogleRequestSigningAlgorithm != null)
			request = Sign(request);

		return "http://maps.google.com" + request;
	}

	string GetVehicleImagePath(Vehicle vehicle)
	{
		string icon = Vehicles.VehiclesInfo.GetStatusIcon(vehicle);
		if (string.IsNullOrWhiteSpace(icon) && !string.IsNullOrWhiteSpace(vehicle.iconUrl))
		{
			icon = ConfigurationManager.AppSettings["VehicleIconBaseExtPath"] + vehicle.iconUrl;
		}

		if (string.IsNullOrEmpty(icon))
			icon = ConfigurationManager.AppSettings["VehicleIconBaseExtPath"] + vehicle.DefaultIconUrl;

		return icon;
	}
	
	private static string GetPositionRadiusPathString(double latitude, double longitude, double radius)
	{
		const int pointsCount = 36;

		var center = new GeoPointStruct(latitude, longitude);

		var points = new List<GeoPointStruct>(pointsCount) {center.AddMeters(0, radius)};

		for (var i = 1; i <= pointsCount; ++i)
		{
			var angle = i * 2*Math.PI/pointsCount;
			points.Add(center.AddMeters(Math.Sin(angle) * radius, Math.Cos(angle) * radius));
		}

		var sb = new StringBuilder(20 + pointsCount * 20);

		sb.Append("path=weight:2px|color:red|fillcolor:0xFFFF0033");
		foreach (var point in points)
		{
			sb.Append('|');
			sb.Append(point.Lat.ToString("0.00000", CultureInfo.InvariantCulture));
			sb.Append(',');
			sb.Append(point.Lng.ToString("0.00000", CultureInfo.InvariantCulture));
		}

		return sb.ToString();
	}

	private static string Sign(string request)
	{
		byte[] encodedPathAndQueryBytes = Encoding.ASCII.GetBytes(request);

		// compute the hash

		byte[] hash = GoogleRequestSigningAlgorithm.ComputeHash(encodedPathAndQueryBytes);

		// convert the bytes to string and make url-safe by replacing '+' and '/' characters
		string signature = Convert.ToBase64String(hash).Replace("+", "-").Replace("/", "_");

		// Add the signature to the existing URI.
		return request + "&signature=" + signature;
	}

	[PageAction("zoomin")]
	public void MapZoomIn()
	{
	}
}