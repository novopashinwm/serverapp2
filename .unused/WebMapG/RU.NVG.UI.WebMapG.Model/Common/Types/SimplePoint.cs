﻿namespace RU.NVG.UI.WebMapG.Model.Common.Types
{
    public class SimplePoint
    {
        public float Lat { get; set; }

        public float Lng { get; set; }
    }
}
