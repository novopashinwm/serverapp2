﻿using FORIS.TSS.BusinessLogic.DTO;

namespace RU.NVG.UI.WebMapG.Model.Common
{
    public abstract class BaseResponse : IPagination
    {
        public Pagination Pagination { get; set; }
    }
}
