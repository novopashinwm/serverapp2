﻿namespace RU.NVG.UI.WebMapG.Model.Common
{
    public class GetListWithHistoryResponse<T> : GetListResponse<T>
    {
        public int HistoryId;
    }
}
