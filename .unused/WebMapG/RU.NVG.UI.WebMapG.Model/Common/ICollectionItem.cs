﻿using System.Collections.Generic;

namespace RU.NVG.UI.WebMapG.Model.Common
{
    public interface ICollectionItem<T>
    {
        List<T> Items { get; set; } 
    }
}
