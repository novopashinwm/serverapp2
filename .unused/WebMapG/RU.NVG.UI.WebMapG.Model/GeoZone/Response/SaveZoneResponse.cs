﻿using Interfaces.Web.Enums;
using RU.NVG.UI.WebMapG.Model.Common;

namespace RU.NVG.UI.WebMapG.Model.GeoZone.Response
{
    public class SaveZoneResponse : BaseResponse
    {
        public FORIS.TSS.BusinessLogic.DTO.GeoZone Zone { get; set; }

        public SaveZoneResult Result { get; set; }
    }
}
