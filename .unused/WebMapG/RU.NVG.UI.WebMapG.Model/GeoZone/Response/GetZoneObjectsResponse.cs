﻿using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO;
using RU.NVG.UI.WebMapG.Model.Common;

namespace RU.NVG.UI.WebMapG.Model.GeoZone.Response
{
    public class GetZoneObjectsResponse : BaseResponse
    {
        public IDictionary<int, FORIS.TSS.BusinessLogic.DTO.GeoZone> GeoZones { get; set; }

        public Group[] GeoZoneGroups { get; set; }

        public GetZoneObjectsResponse(IDictionary<int, FORIS.TSS.BusinessLogic.DTO.GeoZone> geoZones,
            Group[] geoZoneGroups)
        {
            GeoZones = geoZones ?? new Dictionary<int, FORIS.TSS.BusinessLogic.DTO.GeoZone>();
            GeoZoneGroups = geoZoneGroups ?? new Group[0];
        }
    }
}
