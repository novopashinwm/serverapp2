﻿using System.Collections.Generic;
using System.Drawing;

namespace RU.NVG.UI.WebMapG.Model.GeoZone.Request
{
    public interface ISavePolygonZoneRequest : ISaveZoneRequest
    {
        PointF[] Points { get; }
    }
}
