﻿using System;
using System.Drawing;
using System.Globalization;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;
using Jayrock.Json.Conversion;

namespace RU.NVG.UI.WebMapG.Model.GeoZone.Request
{
    /// <summary>
    /// Объект универсального запроса для добавления геозоны.
    /// </summary>
    public class SaveZoneRequest : ISaveCircleZoneRequest, ISavePolygonZoneRequest
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// mapId
        /// </summary>
        public Guid MapId { get; set; }

        /// <summary>
        /// name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// color
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// type
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// radius
        /// </summary>
        public float Radius { get; set; }

        /// <summary>
        /// points
        /// </summary>
        public PointF[] Points { get; set; }

        [JsonIgnore]
        public int ArgbColor
        {
            get
            {
                var colorString = Color.Trim();
                Color color;
                if (colorString.StartsWith("#"))
                {
                    colorString = colorString.Replace(" ", string.Empty);
                    colorString = colorString.Substring(1);
                    var rgb = int.Parse(colorString, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture);
                    color = System.Drawing.Color.FromArgb(rgb);
                }
                else 
                if (colorString.StartsWith("rgb"))
                {
                    colorString = colorString.Replace(" ", string.Empty);
                    colorString = colorString.Substring(4, colorString.Length - 5);
                    var colorParts = colorString.Split(',').Select(byte.Parse).ToList();
                    if (colorParts.Count > 4 && colorParts.Count < 1)
                    {
                        throw new ArgumentException("cant parse rgb(d,d,d,[d])");
                    }

                    while (colorParts.Count < 4)
                    {
                        colorParts.Insert(0, 0);
                    }

                    color = System.Drawing.Color.FromArgb(colorParts[0], colorParts[1], colorParts[2], colorParts[3]);
                }
                else
                {
                    int rgb;
                    var tempColorString = colorString.Replace(" ", string.Empty);
                    color = int.TryParse(tempColorString, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out rgb) 
                        ? System.Drawing.Color.FromArgb(rgb) 
                        : System.Drawing.Color.FromName(Color);
                }

                return color.ToArgb();
            }
        }

        [JsonIgnore]
        public ZoneTypes ZoneType
        {
            get { return (ZoneTypes) Type; }
        }

        [JsonIgnore]
        public PointF Point
        {
            get { return Points.Single(); }
        }
    }
}
