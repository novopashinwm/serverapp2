﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace RU.NVG.UI.WebMapG.Model.GeoZone.Request
{
    public interface ISaveZoneRequest
    {
        /// <summary>
        /// Идентификатор зоны.
        /// </summary>
        int Id { get; }

        /// <summary>
        /// Название зоны.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Описание зоны.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Цвет зоны.
        /// </summary>
        string Color { get; }

        /// <summary>
        /// Тип зоны.
        /// </summary>
        ZoneTypes ZoneType { get; }

        /// <summary>
        /// Идентификатор карты.
        /// </summary>
        Guid MapId { get; }

        /// <summary>
        /// Код цвета.
        /// </summary>
        int ArgbColor { get; }
    }
}
