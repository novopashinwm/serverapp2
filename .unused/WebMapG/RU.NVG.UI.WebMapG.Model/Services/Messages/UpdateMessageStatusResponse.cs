﻿using System.Collections.Generic;
using RU.NVG.UI.WebMapG.Model.Common;

namespace RU.NVG.UI.WebMapG.Model.Services.Messages
{
    public class UpdateMessageStatusResponse : BaseResponse, ICollectionItem<int>
    {
        /// <summary>
        /// Успешно обновленные сообщения
        /// </summary>
        public List<int> Items { get; set; }
    }
}