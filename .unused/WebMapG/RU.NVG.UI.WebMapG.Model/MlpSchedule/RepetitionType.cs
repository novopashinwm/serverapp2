﻿namespace RU.NVG.UI.WebMapG.Model.MlpSchedule
{
    public enum RepetitionType
    {
        /// <summary>
        /// null
        /// </summary>
        None,

        /// <summary>
        /// RepeatEveryTime
        /// </summary>
        RepeatEveryTime,

        /// <summary>
        /// RepeatEveryWeekGivenTimes
        /// </summary>
        RepeatEveryWeekGivenTimes,

        /// <summary>
        /// RepeatCustomWeekDayCustomTime
        /// </summary>
        RepeatCustomWeekDayCustomTime
    }
}
