﻿using System.Linq;
using System.Security;
using System.Threading.Tasks;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Helpers;
using Microsoft.AspNet.SignalR;

namespace RU.NVG.UI.WebMapG.Model.SignalRHubs
{
    public class MessagesHub : Hub
    {
        public override Task OnDisconnected()
        {
            MessagesHubSecurity.Instance.Unbind(Context.ConnectionId);
            return base.OnDisconnected();
        }

        public override Task OnConnected()
        {
            var userToken = Context.QueryString["UserToken"];
            MessagesHubSecurity.Instance.Bind(userToken, Context.ConnectionId);
            return base.OnConnected();
        }

        public void SubscribeMsisdn(string msisdn, int historyId)
        {
            msisdn = ContactHelper.GetNormalizedPhone(msisdn);

            var connectionId = Context.ConnectionId;
            var contextKey = MessagesHubSecurity.Instance.Authorize(connectionId);
            if (contextKey == null)
                throw new SecurityException("not allowed");

            if (MessagesHubSecurity.Instance.ExistSubscribe(contextKey, msisdn)) 
                return;

            MessageStateProcessor.Instance.Lock();
            var messageCheckDate = TimeHelper.GetDateTimeUTC(historyId);
            var stateProcessorCheckDate = MessageStateProcessor.Instance.LastUpdateTime;
            try
            {
                var contextInfo = MessagesHubSecurity.Instance.Subscribe(contextKey, msisdn);
                var contactIds = new[] {contextInfo.AsidId, contextInfo.MsisdnId}.Where(id => id != null).Cast<int>().ToArray();
                var connectionsChanges = MessageStateProcessor.Instance.GetMessageUpdates(contactIds, messageCheckDate, stateProcessorCheckDate);
                Groups.Add(connectionId, msisdn);
                if(connectionsChanges.ContainsKey(connectionId))
                    Clients.Caller.MessageUpdates(connectionsChanges[connectionId]);
            }
            finally
            {
                MessageStateProcessor.Instance.Unlock();                
            }
        }

        public Task UnsubscribeMsisdn(string msisdn)
        {
            msisdn = ContactHelper.GetNormalizedPhone(msisdn);

            var connectionId = Context.ConnectionId;
            var contextKey = MessagesHubSecurity.Instance.Authorize(connectionId);
            if (contextKey == null)
                throw new SecurityException("not allowed");

            MessagesHubSecurity.Instance.UnSubscribe(contextKey, msisdn);
            return Groups.Remove(connectionId, msisdn);
        }
    }
}
