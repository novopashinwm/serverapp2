﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Security;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.EntityModel;
using FORIS.TSS.ServerApplication;
using FORIS.TSS.TerminalService.Interfaces;
using Microsoft.AspNet.SignalR;

namespace RU.NVG.UI.WebMapG.Model.SignalRHubs
{
    public class TokenRegistrationInfo
    {
        /// <summary>
        /// Идентификатор оператора.
        /// </summary>
        public int OperatorId;

        /// <summary>
        /// Локализация оператора.
        /// </summary>
        public CultureInfo CultureInfo;
    }

    public class MessagesContextKey : TokenRegistrationInfo
    {
        /// <summary>
        /// Идентификатор подключения текущего пользователя.
        /// </summary>
        public string ConnectionId;

        /// <summary>
        /// Дата подключения к хабу.
        /// </summary>
        public DateTime ConnectTime;

        /// <summary>
        /// Время в секундах
        /// </summary>
        public int ConnectLogTime;
    }

    public class MessagesContextInfo
    {
        /// <summary>
        /// Идентификатор контакта для которого происходит мониторинг.
        /// </summary>
        public int? AsidId;

        /// <summary>
        /// Идентификатор контакта для которого происходит мониторинг.
        /// </summary>
        public int MsisdnId;

        /// <summary>
        /// Номер телефона.
        /// </summary>
        public string Msisdn;

        /// <summary>
        /// Идентификатор последнего сообщения из пагинации.
        /// </summary>
        public int? FirstMessageId;
    }

    public sealed class MessagesHubSecurity
    {
        private readonly ConcurrentDictionary<string, TokenRegistrationInfo> _tokenRegistration = new ConcurrentDictionary<string, TokenRegistrationInfo>();

        private readonly ConcurrentDictionary<int, ConcurrentDictionary<string, MessagesContextKey>> _operatorContextKeys = new ConcurrentDictionary<int, ConcurrentDictionary<string, MessagesContextKey>>();

        private readonly ConcurrentDictionary<string, MessagesContextKey> _connectionContextKeys = new ConcurrentDictionary<string, MessagesContextKey>();

        private readonly ConcurrentDictionary<MessagesContextKey, List<MessagesContextInfo>> _contextInfos = new ConcurrentDictionary<MessagesContextKey, List<MessagesContextInfo>>(); 
 
        private static readonly object Locker = new object();

        private static volatile MessagesHubSecurity _security;

        public static MessagesHubSecurity Instance
        {
            get
            {
                if (_security != null) 
                    return _security;

                lock (Locker)
                {
                    if (_security == null)
                    {
                        _security = new MessagesHubSecurity();
                    }
                }

                return _security;
            }
        }

        public bool Register(string token, TokenRegistrationInfo operatorId)
        {
            TokenRegistrationInfo existRegistration;
            return !_tokenRegistration.TryGetValue(token, out existRegistration) 
                && _tokenRegistration.TryAdd(token, operatorId);
        }

        public bool Bind(string userToken, string connectionId)
        {
            TokenRegistrationInfo tokenRegistration;
            if (!_tokenRegistration.TryRemove(userToken, out tokenRegistration))
                return false;

            ClearConnectionContext(connectionId);
            var connectTime = DateTime.UtcNow;
            var contextKey = new MessagesContextKey
            {
                ConnectionId = connectionId,
                OperatorId = tokenRegistration.OperatorId,
                CultureInfo = tokenRegistration.CultureInfo,
                ConnectTime = connectTime,
                ConnectLogTime = TimeHelper.GetSecondsFromBase(connectTime)
            };

            _connectionContextKeys.TryAdd(connectionId, contextKey);
            var operatorId = tokenRegistration.OperatorId;
            if(!_operatorContextKeys.ContainsKey(operatorId))
                _operatorContextKeys.TryAdd(operatorId, new ConcurrentDictionary<string, MessagesContextKey>());
            _operatorContextKeys[operatorId][connectionId] = contextKey;
            
            return _contextInfos.TryAdd(contextKey, new List<MessagesContextInfo>());
        }

        public bool Unbind(string connectionId)
        {
            return ClearConnectionContext(connectionId);
        }

        public MessagesContextKey Authorize(string connectionId)
        {
            MessagesContextKey contextKey;
            if (!_connectionContextKeys.TryGetValue(connectionId, out contextKey))
                return null;

            return contextKey;
        }

        public bool Authorized(string connectionId)
        {
            return _connectionContextKeys.ContainsKey(connectionId);
        }

        public MessagesContextInfo Subscribe(MessagesContextKey contextKey, string msisdn)
        {
            if (!Server.Instance().IsSuperAdmin(contextKey.OperatorId))
                throw new SecurityException("not allowed");

            List<MessagesContextInfo> contextInfos;
            if (!_contextInfos.TryGetValue(contextKey, out contextInfos))
                throw new ObjectNotFoundException("not context found for contextKey");

            var contextInfo = CreateContextInfo(msisdn);
            contextInfos.Add(contextInfo);

            return contextInfo;
        }

        public bool ExistSubscribe(MessagesContextKey contextKey, string msisdn)
        {
            List<MessagesContextInfo> contextInfos;
            if (!_contextInfos.TryGetValue(contextKey, out contextInfos))
                throw new ObjectNotFoundException("not context found for contextKey");

            return contextInfos.Any(ci => ci.Msisdn == msisdn);
        }

        public void UnSubscribe(MessagesContextKey contextKey, string msisdn)
        {
            List<MessagesContextInfo> contextInfos;
            if(!_contextInfos.TryGetValue(contextKey, out contextInfos))
                throw new ObjectNotFoundException("not context found for contextKey");

            contextInfos.RemoveAll(ci => ci.Msisdn == msisdn);
        }

        public int[] GetMonitoringIds()
        {
            return _contextInfos.Select(c => c.Value)
                .SelectMany(infos => infos
                    .SelectMany(ci => new[] { ci.AsidId, ci.MsisdnId }
                        .Where(id => id != null)
                        .Cast<int>())).ToArray();
        }

        public void AlarmVehicle(IMobilUnit mobilUnit, int[] operatorIds)
        {
            var contextKeys = operatorIds
                .Where(operatorId => _operatorContextKeys.ContainsKey(operatorId))
                .SelectMany(operatorId => _operatorContextKeys[operatorId].Values)
                .ToArray();

            foreach (var contextKey in contextKeys)
            {
                var hub = GlobalHost.ConnectionManager.GetHubContext<MessagesHub>();
                try
                {
                    hub.Clients.Client(contextKey.ConnectionId).AlarmVehicle(mobilUnit.Unique, true);
                }
                catch (Exception e)
                {
                    Trace.TraceWarning("{0}", e);
                }
            }
        }

        public MessagesContextKey[] GetConnectionsByMsisdn(string msisnd)
        {
            return _contextInfos
                .Where(ci => ci.Value.Any(c => c.Msisdn == msisnd))
                .Select(ci => ci.Key)
                .ToArray();
        }

        private bool ClearConnectionContext(string connectionId)
        {
            MessagesContextKey contextKey;
            if (!_connectionContextKeys.TryRemove(connectionId, out contextKey))
                return false;

            ConcurrentDictionary<string, MessagesContextKey> operatorContextKeys;
            if (_operatorContextKeys.TryGetValue(contextKey.OperatorId, out operatorContextKeys))
            {
                MessagesContextKey operatorContextKey;
                operatorContextKeys.TryRemove(connectionId, out operatorContextKey);
            }

            List<MessagesContextInfo> contextInfos;
            return _contextInfos.TryRemove(contextKey, out contextInfos);
        }

        private MessagesContextInfo CreateContextInfo(string msisdn)
        {
            using (var entities = new Entities())
            {
                var msisdnContact = entities.GetContact(ContactType.Phone, msisdn);
                var asidContact = entities.GetAsidContact(msisdnContact);
                var contextInfo = new MessagesContextInfo
                {
                    Msisdn = msisdn,
                    MsisdnId = msisdnContact.ID,
                    AsidId = asidContact != null ? asidContact.ID : (int?)null
                };

                return contextInfo;
            }
        }
    }
}
