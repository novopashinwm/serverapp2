﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Messages;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.ServerApplication;
using FORIS.TSS.ServerApplication.BackgroundProcessors;
using Microsoft.AspNet.SignalR;
using RU.NVG.UI.WebMapG.Model.Messages;

namespace RU.NVG.UI.WebMapG.Model.SignalRHubs
{
    /// <summary>
    /// Процессор, который определяет последние изменения в т
    /// </summary>
    public class MessageStateProcessor : BackgroundProcessorSingleton<MessageStateProcessor>
    {
        /// <summary>
        /// Событие для синхронизации получения обновлений при подписке.
        /// </summary>
        private readonly object _historyMessagesLock = new object();

        public MessageStateProcessor()
            : base(TimeSpan.FromSeconds(1))
        {
            BeforeStart += delegate 
            {
                LastUpdateTime = DateTime.UtcNow;
            };
        }

        public DateTime LastUpdateTime { get; set; } 

        public void Lock()
        {
            Monitor.Enter(_historyMessagesLock);
        }

        public void Unlock()
        {
            Monitor.Exit(_historyMessagesLock);
        }

        public Dictionary<string, Dictionary<string, Dictionary<ChangeType, Dictionary<int, MessagesChangeLocale>>>> GetMessageUpdates(int[] contactIds, DateTime from, DateTime to)
        {
            var connectionsChanges = new Dictionary<string, Dictionary<string, Dictionary<ChangeType, Dictionary<int, MessagesChangeLocale>>>>();
            var updatedMessages = Server.Instance().GetUpdatedMessages(contactIds, from, to);
            if (!updatedMessages.Any())
                return connectionsChanges;

            var phoneChanges = new Dictionary<string, Dictionary<int, MessageChange>>();
            foreach (var message in updatedMessages)
            {
                var messageChange = new MessageChange
                {
                    Message = message,
                    ChangeType = message.Created > from ? ChangeType.Insert : ChangeType.Update,
                };

                if (message.From != null)
                {
                    if (!phoneChanges.ContainsKey(message.From.Value))
                        phoneChanges.Add(message.From.Value, new Dictionary<int, MessageChange>());

                    var fromChanges = phoneChanges[message.From.Value];
                    if (!fromChanges.ContainsKey(message.Id))
                        fromChanges.Add(message.Id, messageChange);
                }

                if (message.To != null)
                {
                    if (!phoneChanges.ContainsKey(message.To.Value))
                        phoneChanges.Add(message.To.Value, new Dictionary<int, MessageChange>());

                    var toChanges = phoneChanges[message.To.Value];
                    if (!toChanges.ContainsKey(message.Id))
                        toChanges.Add(message.Id, messageChange);
                }
            }

            if (!phoneChanges.Any())
                return connectionsChanges;

            foreach (var msisdn in phoneChanges.Keys)
            {
                var connectionInfos = MessagesHubSecurity.Instance.GetConnectionsByMsisdn(msisdn);
                var messageChanges = phoneChanges[msisdn];
                foreach (var connectionInfo in connectionInfos)
                {
                    if (!connectionsChanges.ContainsKey(connectionInfo.ConnectionId))
                        connectionsChanges.Add(connectionInfo.ConnectionId, new Dictionary<string, Dictionary<ChangeType, Dictionary<int, MessagesChangeLocale>>>());

                    var connectionChanges = connectionsChanges[connectionInfo.ConnectionId];
                    if (!connectionChanges.ContainsKey(msisdn))
                        connectionChanges.Add(msisdn, new Dictionary<ChangeType, Dictionary<int, MessagesChangeLocale>>());

                    var msisdnChanges = connectionChanges[msisdn];
                    if (!msisdnChanges.ContainsKey(ChangeType.Insert))
                        msisdnChanges.Add(ChangeType.Insert, new Dictionary<int, MessagesChangeLocale>());
                    if (!msisdnChanges.ContainsKey(ChangeType.Update))
                        msisdnChanges.Add(ChangeType.Update, new Dictionary<int, MessagesChangeLocale>());
                    foreach (var messageId in messageChanges.Keys)
                    {
                        var messageChange = messageChanges[messageId];
                        var message = messageChange.Message;
                        var messageLocaleChange = new MessagesChangeLocale(messageChange, connectionInfo.CultureInfo)
                        {
                            Type = message.To != null && message.To.Value == msisdn
                                    ? MessageType.Outgoing
                                    : MessageType.Incoming
                        };
                        msisdnChanges[messageChange.ChangeType].Add(messageId, messageLocaleChange);
                    }
                }
            }

#if DEBUG
            var info = string.Format("Push to web GetMessageUpdates: {0}", JSONHelper.SerializeObjectToJSON(connectionsChanges));
            Trace.TraceInformation(info);
#endif

            return connectionsChanges;
        }

        protected override bool Do()
        {
            Lock();

            try
            {
                var contactIds = MessagesHubSecurity.Instance.GetMonitoringIds();
                if (!contactIds.Any())
                    return false;

                var time = DateTime.UtcNow;
                var connectionsChanges = GetMessageUpdates(contactIds, LastUpdateTime, time);
                LastUpdateTime = time;

                var hub = GlobalHost.ConnectionManager.GetHubContext<MessagesHub>();
                foreach (var connectionId in connectionsChanges.Keys)
                    hub.Clients.Client(connectionId).MessageUpdates(connectionsChanges[connectionId]);

                return connectionsChanges.Count != 0;
            }
            finally
            {
                Unlock();
            }
        }
    }
}
