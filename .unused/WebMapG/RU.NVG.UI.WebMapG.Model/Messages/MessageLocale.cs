﻿using System;
using System.Collections.Generic;
using System.Globalization;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Messages;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Resources;
using FORIS.TSS.Terminal.Messaging;

namespace RU.NVG.UI.WebMapG.Model.Messages
{
    public class MessageLocale
    {
        private readonly MessageWeb _message;

        private readonly CultureInfo _culture;

        public MessageLocale(MessageWeb message, CultureInfo culture)
        {
            _message = message;
            _culture = culture;
        }

        public int Id
        {
            get { return _message.Id; }
        }

        public string Subject
        {
            get { return _message.Subject; }
        }

        public string Body
        {
            get { return _message.Body; }
        }

        public DateTime Time
        {
            get { return _message.Time; }
        }

        public DateTime? Created
        {
            get { return _message.Created; }
        }

        public int? OwnerOperatorId
        {
            get { return _message.OwnerOperatorId; }
        }

        public string OwnerOperatorName
        {
            get { return _message.OwnerOperatorName; }
        }

        public string Status
        {
            get { return _message.Status; }
        }

        public string StatusText
        {
            get
            {
                var state = (MessageState) Enum.Parse(typeof (MessageState), _message.Status);
                return ResourceContainers.Get(_culture)[state];
            }
        }

        public MessageProcessingResult Result
        {
            get { return _message.Result; }
        }

        public string ResultText
        {
            get
            {
                return ResourceContainers.Get(_culture)[_message.Result];
            }
        }

        public MessageType MessageType
        {
            get { return _message.MessageType; }
            set { _message.MessageType = value; }
        }

        public string TemplateName
        {
            get { return _message.TemplateName; }
        }

        public MessageWeb.Contact From
        {
            get { return _message.From; }
        }

        public MessageWeb.Contact To
        {
            get { return _message.To; }
        }

        public Dictionary<string, string> Fields
        {
            get { return _message.Fields; }
        }
    }
}
