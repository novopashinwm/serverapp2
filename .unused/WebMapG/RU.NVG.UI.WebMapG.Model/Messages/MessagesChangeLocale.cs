﻿using System.Globalization;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Messages;

namespace RU.NVG.UI.WebMapG.Model.Messages
{
    public class MessagesChangeLocale
    {
        private readonly MessageLocale _localeMessage;

        public MessagesChangeLocale(MessageChange messageChange, CultureInfo culture)
        {
            _localeMessage = new MessageLocale(messageChange.Message, culture);
        }

        public MessageType Type
        {
            get { return _localeMessage.MessageType; }
            set { _localeMessage.MessageType = value; }
        }

        public MessageLocale Message
        {
            get { return _localeMessage; }
        }
    }
}
