﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Http;

namespace FORIS.TSS.UI.WebMapG.services
{
	public partial class ServicesSMS : BasePage
	{
		[PageAction("send")]
		public void SendSms()
		{
			var mobileidStrings = GetParamValue("mobileid").Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);

			var mobileIds = Array.ConvertAll(mobileidStrings, int.Parse);

			String message = GetParamValue("smstext");

			SetXmlContentType();
			ResponseSerializationBegin();
			try
			{
				foreach (var mobileId in mobileIds)
				{
					IWebPS.SendSms(mobileId, message);
				}
			}
			catch (Exception myEx)
			{
				AddMessage(new Message(Severity.Error, "", myEx.Message));
				WriteLog(myEx);
			}

			ResponseSerializationEnd();
		}

		[PageAction("getMessageHistory")]
		public void GetMessageHistory()
		{
			DateTime from = DateTime.UtcNow.AddHours(-2);
			DateTime to = DateTime.UtcNow;
			var mobileidStrings = GetParamValue("mobileid").Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);

			var mobileIds = Array.ConvertAll(mobileidStrings, int.Parse);

			SetXmlContentType();

			TemplateFile = "messages";
			AddProperty("template", "messageList");
			//AddTitleProperty();

			ResponseSerializationBegin();
			try
			{
				List<Message> msgList = IWebPS.GetMessages(mobileIds, from, to);

				if (msgList != null)
				{
					foreach (var message in msgList)
					{
						message.Time = TimeZoneInfo.ConvertTimeFromUtc(message.Time, TimeZoneInfo);
					}

					Message.SerializeMessages(msgList, Writer);
				}
			}
			catch (Exception exception)
			{
				WriteLog(exception);
			}
			ResponseSerializationEnd();

		}

		[PageAction("register-incoming-message", Method = HttpMethod.Post, RequestType = DataType.Json, ResponseType = DataType.Empty)]
		public void RegisterIncomingMessage(string phone, string text)
		{
			System.Diagnostics.Trace.TraceInformation(
				"{0}: incoming message {1}: {2}", 
				IWebPS.SessionInfo.OperatorInfo.Login, phone, text);
		}
	}
}