﻿using System;
using System.Data;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.UI.WebMapG;

public partial class route_tasks_Default : BasePage
{
	[PageAction("default")]
	public void Default()
	{
		TaskList();
	}

	/// <summary>
	/// AS: Отображение списка заданий
	/// </summary>
	public void TaskList()
	{
		TemplateFile = "routeTask";
		AddProperty("template", "tasklist");
		ResponseSerializationBeg();
		ResponseSerializationEnd();
	}

	/// <summary>
	/// AS: редактор маршрутных заданий
	/// </summary>
	[PageAction("editor")]
	public void EditorPage()
	{
		TemplateFile = "routeTask";
		AddProperty("template", "editor");

		ResponseSerializationBeg();

		// AS: сериализация водителей
		try
		{
			DataSet drivers = Driver.GetAllDrivers(this.IWebPS);
			drivers.DataSetName = "Drivers";
			drivers.WriteXml(Writer);
		}
		catch (Exception ex)
		{
			WriteLog(ex);
			AddMessage(new Message(Severity.Error, GetMessageData("ServerError"), ex.Message));
		}

		// AS: Сериализация авто

		ResponseSerializationEnd();
	}
}