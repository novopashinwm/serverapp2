﻿using System;
using System.Diagnostics;
using System.Threading;
using FORIS.TSS.Common;

namespace FORIS.TSS.UI.WebMapG.Old_App_Code
{
    public class BackgroundSessionKiller : BackgroundProcessor
    {
        private readonly mySessionManager _sessionManager;
        private readonly TimeSpan _killSessionsInterval;

        public BackgroundSessionKiller(mySessionManager sessionManager, int minutes)
        {
            _sessionManager = sessionManager;
            _killSessionsInterval = TimeSpan.FromMinutes(minutes);
        }

        protected override bool Do()
        {
            Thread.Sleep(_killSessionsInterval);
            Trace.WriteLine("Killing the old sessions started");
            _sessionManager.KillOldSessions();
            Trace.WriteLine("Killing the old sessions finished");
            return true;
        }
    }
}