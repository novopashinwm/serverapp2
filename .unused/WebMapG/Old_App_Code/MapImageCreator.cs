﻿using System;
using System.Diagnostics;
using System.Web;
using System.IO;
using System.Xml;
using System.Text;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Reflection;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Collections;
using System.Web.SessionState;
using System.Web.Security;
using System.Net;
using System.Drawing;
using System.Drawing.Imaging;

using FORIS.TSS.BusinessLogic;
using FORIS.TSS.UI.WebMapG;
using Interfaces.Web;

/// <summary>
/// Summary description for MapImageCreator
/// </summary>

public class MapImageCreator : HttpHandlerBase, IHttpHandler, IRequiresSessionState
{
    public new bool IsReusable
    {
        get { return false; }
    }

    //[MyAction("getPositionImage")]
    [PageAction("default")]
    public void GetPositionImage()
    {
        IsMobile = true;

        Guid mapGuid = Guid.Empty; //new Guid(GetParamValue("mapGuid"));

        MapDescription map = GetCurrentMapDescription();
        int objectId;
        Int32.TryParse(GetParamValue("id"), out objectId);
        Vehicles.VehiclesInfo trackObj = GetObjectById(objectId);
        trackObj.UpdateVehicle(TimeZoneInfo);
        PointF position = new PointF();
        position.X = (float)trackObj.PositionInfo.Longitude;
        position.Y = (float)trackObj.PositionInfo.Latitude;

        int scale;
        Int32.TryParse(GetParamValue("scale"), out scale);
        
        MapPictureWithOffset pic = map.GetMapPicture(scale, position.X, position.Y);
        System.IO.Stream simage = null;
        try
        {
            simage = GetCombinedImage(pic, trackObj, scale);
        }
        catch (Exception ex)
        {
            WriteLog(ex);
            //AddMessage(new Message(Severity.Normal, GetMessageData("error"), ex.Message));
        }
        if (simage != null)
        {
            byte[] imageBytes = new byte[simage.Length];
            simage.Position = 0;
            simage.Read(imageBytes, 0, (int)simage.Length);
            SetImageJpgContentType();
            myContext.Response.OutputStream.Write(imageBytes, 0, imageBytes.Length);
            //Response.OutputStream.Write(imageBytes, 0, imageBytes.Length);
        }
        else
        {
            /*ResponseSerializationBegin();
            ResponseSerializationEnd();*/
        }
    }

    /// <summary>
    /// AS: Получение контента картинки по URL-у
    /// </summary>
    /// <param name="url">URL картинки</param>
    /// <returns></returns>
    private System.Net.HttpWebResponse GetImageStream(String url)
    {
        System.Net.HttpWebResponse response = null;
        System.Net.WebClient client = new System.Net.WebClient();
        NetworkCredential cred = CredentialCache.DefaultNetworkCredentials;
        CredentialCache crcache = new CredentialCache();
        System.Uri uri = new Uri(url);
        crcache.Add(uri, "NTLM", cred);
        client.Credentials = crcache;
        System.Net.WebRequest req = System.Net.WebRequest.Create(url);
        req.Credentials = crcache;
        req.Timeout = 500;
        try
        {
            response = (System.Net.HttpWebResponse)req.GetResponse();
            /*if (response.StatusCode == HttpStatusCode.OK)
            {
                s = response.GetResponseStream();
            }*/
        }
        catch (Exception ex)
        {
            throw ex;
        }

        return response;
    }

    private System.IO.Stream GetCombinedImage(MapPictureWithOffset pic, Vehicles.VehiclesInfo tro, int scale)
    {
        String url = "";
        System.Net.HttpWebResponse[] imgStreams = new System.Net.HttpWebResponse[9];
        long byteCnt = 0;

        MapDescription map = GetCurrentMapDescription();

        int picx = pic.picNumX + 1;
        int picy = pic.picNumY + 1;
        url = map.BuildMapPictureUrl(picx, picy, scale);
        imgStreams[0] = GetImageStream(url);
        byteCnt += imgStreams[0].ContentLength;

        picx = pic.picNumX;
        picy = pic.picNumY + 1;
        url = map.BuildMapPictureUrl(picx, picy, scale);
        imgStreams[1] = GetImageStream(url);
        byteCnt += imgStreams[1].ContentLength;

        picx = pic.picNumX - 1;
        picy = pic.picNumY + 1;
        url = map.BuildMapPictureUrl(picx, picy, scale);
        imgStreams[2] = GetImageStream(url);
        byteCnt += imgStreams[2].ContentLength;

        picx = pic.picNumX + 1;
        picy = pic.picNumY;
        url = map.BuildMapPictureUrl(picx, picy, scale);
        imgStreams[3] = GetImageStream(url);
        byteCnt += imgStreams[3].ContentLength;

        picx = pic.picNumX;
        picy = pic.picNumY;
        url = map.BuildMapPictureUrl(picx, picy, scale);
        imgStreams[4] = GetImageStream(url);
        byteCnt += imgStreams[4].ContentLength;

        picx = pic.picNumX - 1;
        picy = pic.picNumY;
        url = map.BuildMapPictureUrl(picx, picy, scale);
        imgStreams[5] = GetImageStream(url);
        byteCnt += imgStreams[5].ContentLength;

        picx = pic.picNumX + 1;
        picy = pic.picNumY - 1;
        url = map.BuildMapPictureUrl(picx, picy, scale);
        imgStreams[6] = GetImageStream(url);
        byteCnt += imgStreams[6].ContentLength;

        picx = pic.picNumX;
        picy = pic.picNumY - 1;
        url = map.BuildMapPictureUrl(picx, picy, scale);
        imgStreams[7] = GetImageStream(url);
        byteCnt += imgStreams[7].ContentLength;

        picx = pic.picNumX - 1;
        picy = pic.picNumY - 1;
        url = map.BuildMapPictureUrl(picx, picy, scale);
        imgStreams[8] = GetImageStream(url);
        byteCnt += imgStreams[8].ContentLength;


        System.Net.HttpWebResponse trObjectIcon = GetImageStream(GetTrackingObjectImageUrl(tro));

        byte[] imgBuffer = new byte[byteCnt];

        Bitmap commonBmp = new Bitmap(256 * 3, 256 * 3);
        Graphics g = Graphics.FromImage(commonBmp);

        int j = 0;
        int k = 0;
        for (int i = 0; i < imgStreams.Length; i++)
        {
            //if (k > 3) k++;
            //imgStreams[i].GetResponseStream().Read(imgBuffer, (int)(i * imgStreams[i].ContentLength), (int)imgStreams[i].ContentLength);
            String msg = i.ToString();
            Bitmap b = new Bitmap(imgStreams[i].GetResponseStream());
            Point p = new Point();
            p.X = j * 256;
            p.Y = k * 256;

            /*ts = g.MeasureString(msg, f);
            RectangleF area = new RectangleF(p.X + 120, p.Y + 120, p.X + 120 + ts.Width, p.Y + 120 + ts.Height);
            g.FillRectangle(bc, area);*/

            g.DrawImage(b, p);

            //g.DrawString(msg, f, fc, area);

            b.Dispose();

            if (j++ > 1)
            {
                j = 0;
                k++;
            }
        }

        int semiWidth = (int)Math.Round((double)(256 * 3 / 2));
        int semiHeight = (int)Math.Round((double)(256 * 3 / 2));

        //int width = 256 * 3;
        //int height = 256 * 3;

        Bitmap objBmp = new Bitmap(Image.FromStream(trObjectIcon.GetResponseStream()), new Size(32, 32));

        Point objPoint = new Point(256 - pic.picOffsetX - 12, 256 - pic.picOffsetY - 12);
        g.DrawImage(objBmp, objPoint);

        objBmp.Dispose();


        /*bc.Dispose();
        fc.Dispose();
        f.Dispose();*/

        // AS: вырезаем картинку нужного размера
        int resBmpW = 220;
        int resBmpH = 165;

        // AS: установим качество JPG-а херовым
        int jpegQuality = 40;

        HttpBrowserCapabilities browser = myContext.Request.Browser;
        // AS: если телефон, то уменьшаем картинку
        if (browser.IsMobileDevice)
        {
            resBmpW = 160;
            resBmpH = 120;

            // AS: качество при этом можно чуть поднять
            jpegQuality = 60;
        }

        Bitmap resultBmp = new Bitmap(resBmpW, resBmpH);
        Graphics resg = Graphics.FromImage(resultBmp);
        int x = objPoint.X - (int)Math.Round((double)((resBmpW - 12 ) / 2));
        int y = objPoint.Y - (int)Math.Round((double)((resBmpH - 12) / 2));
        resg.DrawImage(commonBmp, new Rectangle(0, 0, resBmpW, resBmpH), x, y, resBmpW, resBmpH, GraphicsUnit.Pixel);

        System.IO.Stream combinedStream = new System.IO.MemoryStream();
        //combinedStream.Write(imgBuffer, 0, imgBuffer.Length);
        //commonBmp.Save(combinedStream, System.Drawing.Imaging.ImageFormat.Jpeg);
        //resultBmp.SetResolution(30, 20);


        EncoderParameters encoderParams = new EncoderParameters(1);
        encoderParams.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, jpegQuality);

        // DISABLED resultBmp.Save(combinedStream, System.Drawing.Imaging.ImageFormat.Jpeg);
        resultBmp.Save(combinedStream, GetJpgEncoderInfo(), encoderParams);
        resultBmp.Dispose();
        commonBmp.Dispose();
        //resultBmp.Dispose();
        g.Dispose();

        return combinedStream;
    }

    private ImageCodecInfo GetJpgEncoderInfo()
    {
        String jpgMime = "image/jpeg";
        ImageCodecInfo res = null;
        ImageCodecInfo[] Encoders = ImageCodecInfo.GetImageEncoders();
        for (int i = 0; res == null && i < Encoders.Length; i++)
        {
            if (Encoders[i].MimeType == jpgMime)
            {
                res = Encoders[i];
            }
        }
        return res;
    }

    private String GetTrackingObjectImageUrl(Vehicles.VehiclesInfo tro)
    {
        System.Text.StringBuilder troUrl = new System.Text.StringBuilder("");
        if (tro.Settings != null && tro.Settings.ContainsKey("icon"))
        {
            String icon = tro.Settings["icon"];
            troUrl.Append(icon);
        }
        else
        {
            troUrl.Append("http://");
            int course = tro.PositionInfo.Course;
            troUrl.Append(myContext.Server.MachineName);
            troUrl.Append(myContext.Request.ApplicationPath);
            /*troUrl.Append(Server.MachineName);
            troUrl.Append(Request.ApplicationPath);*/
            troUrl.Append("/includes/icons/");
            if (tro.PositionInfo.Speed == 0)
            {
                troUrl.Append("objects2/stop.png");
            }
            else if (tro.PositionInfo.Speed > 0 && tro.PositionInfo.Speed < 5)
            {
                troUrl.Append("objects2/rest.png");
            }
            else
            {
                troUrl.Append("online/rest");
                course = (int)Math.Round((double)(course / 10));
                if (course < 18)
                {
                    course = 18 + course;
                }
                else
                {
                    course = Math.Abs(18 - course);
                }
                troUrl.Append(course.ToString());
                troUrl.Append(".png");
            }
        }

        return troUrl.ToString();
    }

    protected override void SetImageJpgContentType()
    {
        myContext.Response.ContentType = "image/jpeg";
    }
}
