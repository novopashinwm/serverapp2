﻿using System.Web;

/// <summary>
/// Summary description for TestHttpHandler
/// </summary>
public class TestHttpHandler : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        context.Response.Write("test handle success");
    }

    public bool IsReusable
    {
        get { return true; }
    }
}
