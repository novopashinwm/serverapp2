﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using FORIS.TSS.WorkplaceSharnier;

/// <summary>
/// AS: Класс управляет сессией приложения. Убивает старые пользовательские сессии
/// </summary>
[Serializable]
public class mySessionManager
{
    private readonly ConcurrentDictionary<String, Dictionary<String, Object>> _sessionObject = 
        new ConcurrentDictionary<string, Dictionary<string, object>>();
    private int _deadMinutes;

    public int DeadMinutes
    {
        get { return _deadMinutes; }
        set { _deadMinutes = value; }
    }

    public Dictionary<String, Object>  this[string sessionID]
    {
        get{
            if (sessionID==null || !_sessionObject.ContainsKey(sessionID)) return null;
            
            return _sessionObject[sessionID];
        }
    }

    public void CreateSession(String sessionID, String userName)
    {
        CreateSession(sessionID, userName, true);
    }

    public void CreateSession(String sessionID, String userName, bool isKillAllSessionsForUser)
    {
        if (isKillAllSessionsForUser) 
            KillAllSessionsForUser(userName);
        
        _sessionObject.TryAdd(sessionID, new Dictionary<string, object>());
    }

    public mySessionManager(int dm)
    {
        _deadMinutes = dm;
    }

    //Так как только один инстанс может быть запушен для одного пользователя, то при каждом логине удалим предыдущую сессию
    private void KillAllSessionsForUser(string userName)
    {
        var oldSessionKeys = new List<string>();
        foreach (String sessId in _sessionObject.Keys)
        {
            Dictionary<String, Object> sessionElement;
            if (!_sessionObject.TryGetValue(sessId, out sessionElement))
                continue;

            if (!sessionElement.ContainsKey("UserLogin")) continue;

            if ((string) sessionElement["UserLogin"] == userName)
            {
                oldSessionKeys.Add(sessId);
            }
        }
        foreach (String key in oldSessionKeys)
        {
            RemoveSession(key);
        }
    }

    public void RemoveSession(string sessionID)
    {
        if (_sessionObject.ContainsKey(sessionID))
        {
            Dictionary<String, Object> sessionElement;
            if (!_sessionObject.TryRemove(sessionID, out sessionElement))
                return;
        }
    }

    /// <summary>
    /// AS: совершение убийства старых сессий
    /// </summary>
    public void KillOldSessions()
    {
        if (_sessionObject == null) return;
        DateTime now = DateTime.Now;
        
        var oldSessionKeys = new List<string>();
        foreach (String sessId in _sessionObject.Keys)
        {
            Dictionary<String, Object> sessionElement;
            if (!_sessionObject.TryGetValue(sessId, out sessionElement))
                continue;
            if (!sessionElement.ContainsKey("_TSS_SessionDateTime"))
                continue;
            var sessTime = (DateTime) sessionElement["_TSS_SessionDateTime"];
            var diff = now.Subtract(sessTime);
            if (diff.Minutes >= DeadMinutes)
            {
                oldSessionKeys.Add(sessId);
            }
        }

        Trace.TraceInformation("Removing " + oldSessionKeys.Count + " from " + _sessionObject.Count);

        foreach (String key in oldSessionKeys)
        {
            RemoveSession(key);
        }
    }
}
