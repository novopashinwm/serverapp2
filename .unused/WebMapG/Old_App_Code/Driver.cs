﻿using System;
using System.Data;
using FORIS.TSS.UI.WebMapG;

/// <summary>
/// Summary description for Driver
/// </summary>
[Serializable]
public class Driver : Person
{
	public Driver()
	{
	}

	public static DataSet GetAllDrivers(Interfaces.Web.IWebPersonalServer ps)
	{
		return ps.GetDriverGroupsAndDrivers();
	}
}