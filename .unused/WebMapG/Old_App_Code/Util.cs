using System;

public sealed class Util
{
	private Util()
	{
	}

	public static int StringToInt(string val, int defaultVal)
	{
		int res = defaultVal;
		try
		{
			res = Convert.ToInt32(val);
		}
		catch (ArgumentException)    { }
		catch (FormatException)      { }
		catch (InvalidCastException) { }

		return res;
	}
}