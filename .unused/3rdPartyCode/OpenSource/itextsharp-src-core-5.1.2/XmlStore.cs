﻿using System;
using System.IO;
using System.Xml;
using System.Diagnostics;

namespace iTextSharp
{
    /// <summary>
    /// Tabular controls, such as the DatagGridView, display rows of records and columns
    /// of fields in each record. Often, large data blobs are stored in a database. Storing
    /// small data blobs in a database can be overkill. This simple class stores "tabular" 
    /// data in a "flat" XML file instead of a database.
    /// </summary>
    class XmlStore
    {
        /// <summary>
        /// Data type of a field
        /// </summary>
        public enum DataType
        {
            String,     //default
        }

        /// <summary>
        /// Stores information about a field
        /// </summary>
        public struct Field
        {
            public string name;
            public string title;
            public DataType type;
            public string width; //relative width???

            public Field(string sName, string sTitle, DataType enType)
            {
                name = sName;
                title = sTitle;
                type = enType;
                width = "";     //Relative width; can be used for display, print, PDF, ...
            }
        }

        private XmlDocument mxDoc = null;              //Xml file is loaded and stored in this
        private XmlNode mxnodeSchema = null;           //Node containing the "schema"
        private XmlNodeList mxnodelistData = null;     //collection of nodes containing the records data

        private string msFile = "";
        private bool mbIsReadOnly = false;
        private string msDataNodeName = "";
        private Field[] mFields = null;
        private string msIdColumnName = "ID";
        private bool mbIdColumnAdded = false;
        private int mnIdColumn = -1;

        public bool IsReadOnly
        {
            get { return mbIsReadOnly; }
            set { mbIsReadOnly = value; }
        }

        /// <summary>
        /// Index of column (attribute) containing a Unique ID for the row (or node)
        /// </summary>
        public int ColumnUID
        {
            get { return mnIdColumn; }
            set { mnIdColumn = value; }
        }
        private bool mbThrowExceptions = false;

        /// <summary>
        /// Unique ID for each 'record' node.
        /// </summary>
        public string NameOfFieldWithUniqueID
        {
            get { return msIdColumnName; }
            set { msIdColumnName = value; }
        }

        /// <summary>
        /// Array of a structure named Field
        /// </summary>
        public Field[] Fields
        {
            get { return mFields; }
            set { mFields = value; }
        }

        /// <summary>
        /// Full name of the XmlStore file
        /// </summary>
        public string File
        {
            get { return msFile; }
            set { msFile = value; }
        }

        /// <summary>
        /// Base name of the data nodes containing the records
        /// </summary>
        public string RecordNodeName
        {
            get { return msDataNodeName; }
            set { msDataNodeName = value; }
        }

        /// <summary>
        /// Determines if exceptions are thrown or absorbed by this class
        /// </summary>
        public bool ThrowExceptions
        {
            get { return mbThrowExceptions; }
            set { mbThrowExceptions = value; }
        }

        private XmlStore()  //mainly to disable generic constructor
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="sFile">File name (XML)</param>
        public XmlStore(string sFile)
        {
            this.msFile = sFile;
            this.mbIsReadOnly = (new FileInfo(sFile)).IsReadOnly;
        }

#if !true
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="sFile">File name (XML)</param>
        /// <param name="nodeName">node name</param>
        public XmlStore(string sFile, string nodeName)
        {
            this.msFile = sFile;
            this.msDataNodeName = nodeName;
        }
#endif
        private bool DoAdjustForUniqueID()
        {
            int i = 0;
            this.mnIdColumn = -1;
            bool bHasFieldWithUniqueID = false;
            this.mbIdColumnAdded = false;
            for (i = 0; i < this.mFields.Length; i++)
            {
                //--- see if this field is tagged as the one with the unique value
                if (bHasFieldWithUniqueID == false)
                {
                    //--- in case there is "case" mismatch ...
                    //--- first check it by making both upper case
                    if ((mFields[i].name.ToUpper()
                        == this.NameOfFieldWithUniqueID.ToUpper()))
                    {
                        bHasFieldWithUniqueID = true;
                        //--- in case there is "case" mismatch, save it
                        this.NameOfFieldWithUniqueID = mFields[i].name;
                    }
                }
                else
                    this.mnIdColumn = i;

                i++;
            }

            if (bHasFieldWithUniqueID == false)
            {
                Field fieldNew = new Field(this.NameOfFieldWithUniqueID, this.NameOfFieldWithUniqueID, DataType.String);
                //this.DoFieldAdd(ref this.mFields, fieldNew);
                this.DoFieldAdd(fieldNew);
                this.mbIdColumnAdded = true;
                this.mnIdColumn = this.mFields.Length - 1;
            }

            return this.mbIdColumnAdded;

        }

        /// <summary>
        /// Loads the schema from the Xml file and identifies the field that
        /// contains a unique identifier for each record (which is needed for
        /// updating the correct record if user changes it)
        /// </summary>
        /// <param name="xDoc">XmlDocument containing the XmlStore file</param>
        /// <returns>number of fields loaded</returns>
        public int DoLoadSchema(XmlDocument xDoc)
        {
            int count = 0;
            this.mnIdColumn = -1;
            this.mbIdColumnAdded = false;
            try
            {
                if (xDoc != null)
                {
                    this.mxnodeSchema = xDoc.SelectSingleNode("//schema");

                    if (mxnodeSchema != null || count > 0)
                    {
                        //--- extract arributes (from the 'schema' node and it child 'field' nodes)
                        //    for example:
                        //      <schema datanodename="song">
                        //          <field name="ReleaseDate" title="ReleaseDate"/>
                        //          <field name="Track" title="Track"/>
                        //          <field name="Title" title="Title"/>
                        //          <field name="Artist" title="Artist"/>
                        //          <field name="Album" title="Album"/>
                        //      </schema>

                        //--- retrieve the base name of the record nodes
                        this.msDataNodeName = ((XmlElement)this.mxnodeSchema).GetAttribute("datanodename");

                        //--- retrieve the fields
                        XmlNodeList xnodelistFields = mxDoc.SelectNodes("//schema/field");

                        //--- initialize and populate struct Field array
                        if (xnodelistFields != null)
                        {
                            //--- set size to equal the number of 'field' nodes found
                            this.mFields = new Field[xnodelistFields.Count];
                            int i = 0;
                            foreach (XmlElement elem in xnodelistFields)
                            {
                                //--- extract the name and title for the field
                                this.mFields[i].name = elem.GetAttribute("name");
                                this.mFields[i].title = elem.GetAttribute("title");
                                this.mFields[i].type = DataType.String;
                                this.mFields[i].width = elem.GetAttribute("width");
                                i++;
                            }
                            //--- if necessary, Add a column with a "unique" ID for each record
                            this.DoAdjustForUniqueID();

                            count = this.mFields.Length;
                        }
                    }
                    else
                    {
                        //--- XML file did not contain any schema info, so "build" it from
                        //    the first record
                        XmlNodeList xnodes = this.mxDoc.DocumentElement.ChildNodes;
                        if (xnodes != null && xnodes.Count > 0)
                        {
                            XmlElement elem = (XmlElement)xnodes[0];
                            this.msDataNodeName = elem.Name;
                            int numAttributes = elem.Attributes.Count;
                            this.mFields = new Field[numAttributes];
                            for (int i = 0; i < numAttributes; i++)
                            {
                                this.mFields[i].name = elem.Attributes[i].Name;
                                this.mFields[i].title = this.mFields[i].name;
                                this.mFields[i].type = DataType.String;
                            }

                            this.DoAdjustForUniqueID();

                            count = this.mFields.Length;
                        }
                    }
                }// if (xDoc !=null)
            }
            catch (Exception ex)
            {
                string sMsg = ex.ToString();
                Debug.WriteLine(sMsg);
                if (this.mbThrowExceptions)
                    throw new AccessViolationException(sMsg);
            }

            return count;
        } //DoLoadSpecs

        /// <summary>
        /// Retrieves records from the XmlFile
        /// </summary>
        /// <returns></returns>
        public int DoLoadRecords()
        {
            return DoLoadRecords(this.msFile, this.msDataNodeName);
        }

        /// <summary>
        /// Retrieves records from the XmlFile
        /// </summary>
        /// <param name="nodeName">name of the record node (which contain th</param>
        /// <returns></returns>
        public int DoLoadRecords(string nameOfDataNode)
        {
            this.msDataNodeName = nameOfDataNode;
            return DoLoadRecords(this.msFile, nameOfDataNode);
        }

        /// <summary>
        /// Retrieves records from the XmlFile from with a specific datanodename
        /// </summary>
        /// <param name="sFile"></param>
        /// <param name="nodeName"></param>
        /// <returns></returns>
        public int DoLoadRecords(string nameOfXmlStoreFile, string nameOfDataNode)
        {
            int count = 0;
            this.msFile = nameOfXmlStoreFile;
            this.msDataNodeName = nameOfDataNode;
            try
            {
                //--- create new instance of XmlDocument
                this.mxDoc = new XmlDocument();
                //--- load it with the contents of the XmlStore file
                this.mxDoc.Load(this.msFile);
                //--- build Fields array with data in the schema
                this.DoLoadSchema(this.mxDoc);
                //--- now load the actual data records
                if (this.Fields.Length > 0)
                {
                    //--- select all the data record nodes 
                    this.mxnodelistData = this.mxDoc.SelectNodes("//" + this.msDataNodeName);
                    count = this.mxnodelistData.Count;
                }
            }
            catch (Exception ex)
            {
                string sMsg = ex.ToString();
                Debug.WriteLine(sMsg);
                if (this.mbThrowExceptions)
                    throw new AccessViolationException(sMsg);
            }

            return count;
        } //DoLoadRecords

        private void DoAppendString(ref string[] sArray, string sNew)
        {
            string[] sArrayNew = new string[sArray.Length + 1];
            sArray.CopyTo(sArrayNew, 0);
            sArrayNew[sArray.Length] = sNew;
            sArray = sArrayNew;
        }

        /// <summary>
        /// Add a new Field to the end of the Fields array
        /// </summary>
        /// <param name="sNew">Field to add</param>
        private void DoFieldAdd(Field sNew)
        {
            Field[] sArrayNew = new Field[this.mFields.Length + 1];
            this.mFields.CopyTo(sArrayNew, 0);
            sArrayNew[this.mFields.Length] = sNew;
            this.mFields = sArrayNew;
        }

        /// <summary>
        /// Add a new Field to the end of the Fields array
        /// </summary>
        /// <param name="sArray">the array to which the new field is to be added</param>
        /// <param name="sNew">Field to add</param>
        private void DoFieldAdd(ref Field[] sArray, Field sNew)
        {
            Field[] sArrayNew = new Field[sArray.Length + 1];
            sArray.CopyTo(sArrayNew, 0);
            sArrayNew[sArray.Length] = sNew;
            sArray = sArrayNew;
        }

        /// <summary>
        /// Generate a UID for a row, based on the the current clock. However, given
        /// that CPU can be fast, the row number is added to the time.
        /// </summary>
        /// <param name="nRow">index of row for which it is being generate</param>
        /// <returns>Binary (long) UID</returns>
        public long DoGenerateUID(int nRow)
        {
            DateTime dt = DateTime.Now.ToUniversalTime(); //converting to UTC is nice but may not be needed
            long nID = dt.ToBinary();
            if (this.mbIdColumnAdded)
                nID += nRow;
            return nID;
        }

        /// <summary>
        /// Generate a string UID for a row, based on the the current clock. However, given
        /// that CPU can be fast, the row number is added to the time.
        /// </summary>
        /// <param name="nRow">index of row for which it is being generated</param>
        /// <returns>String version of Binary (long) UID</returns>
        public string DoGenerateSID(int nRow)
        {
            string sID = this.DoGenerateUID(nRow).ToString(); //was nRow.ToString()
            return sID;
        }

        /// <summary>
        /// Returns an array of strings from the data record node. WARNING: they are in the order 
        /// in which they are found in the data node. See DoGetRecordOrdered(...) method
        /// </summary>
        /// <param name="nRow">index of row for which it is being requested</param>
        /// <returns>array of string</returns>
        public string[] DoGetRecord(int nRow)
        {
            string[] rowCells = null;
            try
            {
                XmlElement elem = (XmlElement)mxnodelistData[nRow];

                if (elem != null)
                {
                    if (elem.GetAttribute(this.NameOfFieldWithUniqueID) == "")
                    {
                        string sID = this.DoGenerateSID(nRow);
                        elem.SetAttribute(this.NameOfFieldWithUniqueID, sID);
                    }
                    XmlAttributeCollection attributes = elem.Attributes;
                    int cols = attributes.Count;
                    if (rowCells == null)
                    {
                        rowCells = new string[cols];
                    }

                    for (int n = 0; n < cols; n++)
                    {
                        rowCells[n] = attributes[n].Value;
                    }
                }
            }
            catch (Exception ex)
            {
                string sMsg = ex.ToString();
                Debug.WriteLine(sMsg);
                if (this.mbThrowExceptions)
                    throw new AccessViolationException(sMsg);
            }

            return rowCells;
        }// DoGetRecord(...)

        /// <summary>
        /// Returns an array of strings from the data record node. WARNING: they are in the order 
        /// in which they are found in the Fields array.
        /// </summary>
        /// <param name="nRow">index of row for which it is being requested</param>
        /// <returns>array of string</returns>
        public string[] DoGetRecordOrdered(int nRow)
        {
            string[] rowCells = null;
            try
            {
                XmlElement elem = (XmlElement)mxnodelistData[nRow];

                if (elem != null)
                {
                    if (elem.GetAttribute(this.NameOfFieldWithUniqueID) == "")
                    {
                        string sID = this.DoGenerateSID(nRow);
                        elem.SetAttribute(this.NameOfFieldWithUniqueID, sID);
                    }

                    rowCells = new string[this.mFields.Length];

                    for (int n = 0; n < this.mFields.Length; n++)
                    {
                        string cell = elem.GetAttribute(this.mFields[n].name);
                        rowCells[n] = cell;

                    }
                }
            }
            catch (Exception ex)
            {
                string sMsg = ex.ToString();
                Debug.WriteLine(sMsg);
                if (this.mbThrowExceptions)
                    throw new AccessViolationException(sMsg);
            }

            return rowCells;
        }// DoGetRecordOrdered(...)

        /// <summary>
        /// Returns a specific field value given its name and the row index
        /// </summary>
        /// <param name="nRow">the row index</param>
        /// <param name="nameOfField">name of the field</param>
        /// <returns>string containing the value of the field</returns>
        public string DoGetField(int nRow, string nameOfField)
        {
            string cell = "";
            try
            {
                XmlElement elem = (XmlElement)mxnodelistData[nRow];

                if (elem != null)
                {
                    cell = elem.GetAttribute(nameOfField);
                }
            }
            catch (Exception ex)
            {
                string sMsg = ex.ToString();
                Debug.WriteLine(sMsg);
                if (this.mbThrowExceptions)
                    throw new AccessViolationException(sMsg);
            }

            return cell;
        }// DoGetField(...)

        /// <summary>
        /// Returns an array of empty strings, but populates the one corresponding
        /// to the unique ID with a DateTime-based value
        /// </summary>
        /// <returns></returns>
        public string[] DoGetRecordNew()
        {
            string[] rowCells = null;
            try
            {
                if (this.Fields.Length > 0)
                {
                    int cols = this.Fields.Length;

                    rowCells = new string[cols];

                    for (int n = 0; n < cols; n++)
                    {
                        if (n != this.mnIdColumn)
                            rowCells[n] = "";
                        else
                            rowCells[n] = this.DoGenerateSID(0);
                    }
                }
            }
            catch (Exception ex)
            {
                string sMsg = ex.ToString();
                Debug.WriteLine(sMsg);
                if (this.mbThrowExceptions)
                    throw new AccessViolationException(sMsg);
            }

            return rowCells;
        }// DoGetRecordNew(...)

        /// <summary>
        /// Updates the data record node with the data in the values in the array of strings.
        /// WARNING: The values are assume to be in the same order as the in Fields array
        /// </summary>
        /// <param name="nRow">index of the row being update; not used</param>
        /// <param name="rowCells">array of values to be transferred to the data node</param>
        /// <returns>true, if successful</returns>
        public bool DoSetRecord(int nRow, string[] rowCells)
        {
            bool bRet = false;
            try
            {
                // locate the XML element to be updated
                XmlElement elem = null;

                if (this.mnIdColumn >= 0)
                {
                    //--- locate the element with a matching ID attribute
                    //      e.g., "datanodename[@attrib='value']"
                    int n = this.mnIdColumn;
                    string sAtrib = this.Fields[n].name;
                    string sValue = rowCells[n];
                    string sID = this.msIdColumnName;
                    string xpath = "//" + this.msDataNodeName + "[@" + sID + "='" + sValue + "']";
                    elem = (XmlElement)this.mxDoc.SelectSingleNode(xpath);
                }

                if (elem == null)   //this is a new record, so append an element
                {
                    elem = (XmlElement)mxDoc.CreateNode("element", this.msDataNodeName, "");
                    XmlElement root = mxDoc.DocumentElement;
                    root.AppendChild(elem);
                }

                if (elem != null)
                {
                    for (int n = 0; n < this.Fields.Length; n++)
                    {
                        elem.SetAttribute(this.Fields[n].name, rowCells[n]);
                    }

                    bRet = true;
                }
            }
            catch (Exception ex)
            {
                string sMsg = ex.ToString();
                Debug.WriteLine(sMsg);
                if (this.mbThrowExceptions)
                    throw new AccessViolationException(sMsg);
            }

            return bRet;
        }// DoSetRecord(...)


        /// <summary>
        /// Write the data in the XmlDocument into the file. If the original file
        /// did not contain any Unique row identifier, they would have been added
        /// when the file was Loaded, so the Save operation removes them before writing
        /// </summary>
        /// <param name="sFile"></param>
        /// <returns></returns>
        public int DoSaveRecords(string nameOfXmlStoreFile, bool bRemoveIdColIfAdded)
        {
            int count = 0;
            //this.msFile = nameOfXmlStoreFile;
            try
            {
                if ((new FileInfo(nameOfXmlStoreFile)).IsReadOnly)
                {
                    //MsgBox.Info();
                    Trace.Write("Sorry! The XmlStore file is 'ReadOnly'");
                    return count;
                }

                if (mxDoc != null)
                {
                    if (this.mbIdColumnAdded && bRemoveIdColIfAdded)
                    {
                        //--- be nice: remove ID nodes, if you added them

                        //--- first transfer the Xml into a temp XmlDocument
                        string sTempFile = "temp.xml";
                        this.mxDoc.Save(sTempFile);
                        XmlDocument xDoc = new XmlDocument();
                        xDoc.Load(sTempFile);
                        //--- locate all the data record nodes
                        XmlNodeList xNodes = xDoc.SelectNodes("//" + this.RecordNodeName);

                        if (xNodes != null && xNodes.Count > 0)
                        {
                            foreach (XmlElement elem in xNodes)
                            {
                                //elem.SetAttribute(this.IdColumnName, "X");
                                elem.RemoveAttributeNode(this.NameOfFieldWithUniqueID, "");
                                count++;
                            }
                        }
                        xDoc.Save(nameOfXmlStoreFile);
                        //VVX.File.Delete(sTempFile);
                        System.IO.File.Delete(sTempFile);
                    }
                    else
                    {
                        mxDoc.Save(nameOfXmlStoreFile);
                    }
                }
            }
            catch (Exception ex)
            {
                string sMsg = ex.ToString();
                Debug.WriteLine(sMsg);
                if (this.mbThrowExceptions)
                    throw new AccessViolationException(sMsg);
            }

            return count;
        } //DoSaveRecords

        public float DoGetColumnWidthsTotal()
        {
            float widthTotal = 0f;
            int col;
            for (col = 0; col < this.Fields.Length; col++)
            {
                widthTotal += this.DoGetColumnWidth(col);
            }

            return widthTotal;
        }

        public float DoGetColumnWidth(int col)
        {
            float widthCol = 0f;
            try
            {
                if (0 <= col && col < this.Fields.Length
                    && this.Fields[col].width.Length > 0
                    && this.Fields[col].width != null)
                    widthCol = (float)Convert.ToDouble(this.Fields[col].width);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                widthCol = 0f;
            }

            return widthCol;
        }

    } //class XmlStore
}
