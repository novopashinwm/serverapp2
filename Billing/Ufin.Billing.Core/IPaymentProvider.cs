﻿using System.ServiceModel;
using Compass.Ufin.Billing.Core.Enums;
using Compass.Ufin.Billing.Core.Models;

namespace Compass.Ufin.Billing.Core
{
	[ServiceContract]
	public partial interface IPaymentProvider
	{
		#region Object
		/// <summary> Получить продукт по умолчанию (с минимальной ценой и максимальной по идентификатору) для пользователя/клиента </summary>
		/// <param name="user"> Пользователь/Клиент </param>
		/// <param name="lang"> Язык ответа </param>
		/// <returns> Продукт </returns>
		/// <remarks> Данный метод используется для определения тарифа по умолчанию, для объектов при отсутствии в billing информации по объекту </remarks>
		[OperationContract] ProductItem GetObjectDefaultProductByUser(string user, string lang = "ru");
		/// <summary> Получить продукт по умолчанию (с минимальной ценой и максимальной по идентификатору) для типа клиента </summary>
		/// <param name="accountType"> Тип клиента </param>
		/// <param name="lang"> Язык ответа </param>
		/// <returns> Продукт </returns>
		/// <remarks> Данный метод используется для определения тарифа по умолчанию, для отсутствующих в billing пользователей </remarks>
		[OperationContract] ProductItem GetObjectDefaultProductByAccountType(AccountType accountType, string lang = "ru");
		/// <summary> Получить текущий продукт для объекта наблюдения </summary>
		/// <param name="monitoringObjectId"> Идентификатор объекта наблюдения </param>
		/// <param name="user"> Пользователь/Клиент </param>
		/// <param name="lang"> Язык ответа </param>
		/// <returns> Продукт </returns>
		/// <remarks> В случае отсутствия в billing информации об объекте наблюдения возвращается продукт по умолчанию для пользователя/клиента
		/// <see cref="GetObjectDefaultProductByUser"/>
		/// </remarks>
		[OperationContract] ProductItem GetObjectCurrentProduct(                     int monitoringObjectId, string user, string lang = "ru");
		/// <summary> Назначить продукт объекту наблюдения (смена тарифа или изменение доп. услуг) </summary>
		/// <param name="product"> Измененный продукт </param>
		/// <param name="monitoringObjectId"> Идентификатор объекта наблюдения </param>
		/// <param name="user"> Пользователь/Клиент </param>
		/// <param name="lang"> Язык ответа </param>
		/// <returns> Назначенный продукт </returns>
		[OperationContract] ProductItem SetObjectCurrentProduct(ProductItem product, int monitoringObjectId, string user, string lang = "ru");
		/// <summary> Получить доступные продукты типа "Объекты наблюдения"(<see cref="ProductType.MonitoringObject"/>) исключая текущий </summary>
		/// <param name="monitoringObjectId"> Идентификатор объекта наблюдения </param>
		/// <param name="user"> Пользователь/Клиент </param>
		/// <param name="lang"> Язык ответа </param>
		/// <returns> Список продуктов доступных для заказа исключая текущий </returns>
		/// <remarks>
		/// Текущий исключается т.к. предполагается, что для любого объекта наблюдения есть продукт
		/// (для которых реально нет, есть по умолчанию), поэтому текущий не нужен, а нужны только отличные от текущего
		/// </remarks>
		[OperationContract] ProductList GetObjectAvailableProducts(int monitoringObjectId, string user, string lang = "ru");
		/// <summary> Получить список продуктов типа "Объекты наблюдения"(<see cref="ProductType.MonitoringObject"/>) для указанных объектов наблюдения </summary>
		/// <param name="monitoringObjectIds"> Массив идентификаторов объектов наблюдения </param>
		/// <param name="user"> Пользователь/Клиент </param>
		/// <param name="lang"> Язык ответа </param>
		/// <returns> Список текущий продуктов для указанных объектов наблюдения </returns>
		/// <remarks> Данный метод используется для заполнения текущих тарифов в GetVehicles </remarks>
		[OperationContract] ProductList GetObjectCurrentProducts(int[] monitoringObjectIds, string user, string lang = "ru");
		/// <summary> Получить список продуктов для типа "Объекты наблюдения"(<see cref="ProductType.MonitoringObject"/>), доступные для заказа по типу клиента </summary>
		/// <param name="accountType"> Тип клиента <see cref="AccountType"/> </param>
		/// <param name="lang"> Язык, ответа </param>
		/// <returns> Список продуктов </returns>
		/// <remarks> Данный метод используется для получения списка продуктов, для отсутствующих в billing пользователей </remarks>
		[OperationContract] ProductList GetObjectAvailableProductsByAccountType(AccountType accountType, string lang = "ru");

		#endregion Object
		#region Account
		/// <summary> Получить информацию о балансе пользователя/клиента </summary>
		/// <param name="user"> Пользователь/клиент </param>
		/// <param name="lang"> Язык, ответа </param>
		/// <returns> Баланс </returns>
		[OperationContract] BalanceInfo GetAccountBalanceInfo(string user, string lang = "ru");
		/// <summary> Посчитать стоимость продукта и его дополнений </summary>
		/// <param name="product"> Продукт </param>
		/// <param name="user"> Пользователь/клиент </param>
		/// <param name="lang"> Язык, ответа </param>
		/// <returns> Пересчитанный продукт </returns>
		/// <remarks> Используется для пересчета цен с учетом скидок или заказом доп. ресурсов </remarks>
		[OperationContract] ProductItem GetAccountProductPrice(ProductItem product, string user, string lang = "ru");
		/// <summary> Получить список продуктов доступных для покупки по всем типам продуктов исключая объекты наблюдения </summary>
		/// <param name="user"> Пользователь/клиент </param>
		/// <param name="lang"> Язык, ответа </param>
		/// <returns> Список продуктов </returns>
		[OperationContract] ProductList GetAccountAvailableProducts(string user, string lang = "ru");
		/// <summary> Получить список продуктов в статусах, заказан, активен, обрабатывается (исключены: остановлен, удален); </summary>
		/// <param name="user"> Пользователь/клиент </param>
		/// <param name="lang"> Язык, ответа </param>
		/// <returns> Список продуктов </returns>
		[OperationContract] ProductList GetAccountCurrentProducts(string user, string lang = "ru");
		/// <summary> Получить список продуктов находящихся в статусах </summary>
		/// <param name="productStatuses"> Массив статусов </param>
		/// <param name="user"> Пользователь/клиент </param>
		/// <param name="lang"> Язык, ответа </param>
		/// <returns> Список продуктов </returns>
		[OperationContract] ProductList GetAccountProducts(ProductStatus[] productStatuses, string user, string lang = "ru");
		/// <summary> Добавить/редактировать продукт на учетную запись </summary>
		/// <param name="product"> Продукт </param>
		/// <param name="user"> Пользователь/Клиент </param>
		/// <param name="lang"> Язык ответа </param>
		[OperationContract] ProductItem SetAccountProduct(ProductItem product, string user, string lang = "ru");

		#endregion Account
		#region Payment

		/// <summary> Возвращает html как строку для перехода на оплату созданного платежа (пополнения кошелька) </summary>
		/// <param name="amount"> Сумма платежа/пополнения </param>
		/// <param name="user"> Пользователь/клиент </param>
		/// <returns> html </returns>
		/// <remarks> Внутри html скрипт для перехода на нужную страницу </remarks>
		[OperationContract] string GetNewPaymentHtml(decimal amount, string user);

		#endregion Payment
		#region User

		/// <summary> Проверить существование пользователя в billing </summary>
		/// <param name="user"> Пользователь </param>
		/// <returns> Есть? да/нет </returns>
		[OperationContract] bool UserExists(string user);
		/// <summary> Создать пользователя в billing </summary>
		/// <param name="email"> Почта, станет логином </param>
		/// <param name="pass"> Пароль </param>
		/// <param name="phone"> Телефон </param>
		/// <param name="type"> Тип клиента, для включения в группу клиентов </param>
		/// <returns> Успех? да/нет </returns>
		[OperationContract] bool CreateUser(string email, string pass, string phone, AccountType type = AccountType.Private);
		/// <summary> Получить для пользователя одноразовую ссылку для входа в billing </summary>
		/// <param name="user"> Пользователь </param>
		/// <param name="lang"> Язык пользователя </param>
		/// <returns> Ссылка для входа в панель billing </returns>
		[OperationContract] string GetOneTimeLoginLink(string user, string lang = "ru");

		#endregion User
	}
}