﻿using System;
using System.Runtime.Serialization;

namespace Compass.Ufin.Billing.Core.Enums
{
	/// <summary> Тип продукта (все тарифы делаются на основе типа продукта) </summary>
	[Serializable]
	[DataContract]
	public enum ProductType
	{
		/// <summary> Не указан </summary>
		[EnumMember] Unspecified,
		/// <summary> Объекты наблюдения (на объект наблюдения) </summary>
		[EnumMember] MonitoringObject,
		/// <summary> Поддержка по обращениям (на учетную запись) </summary>
		[EnumMember] PaidSupport,
		/// <summary> Пакет SMS (на учетную запись) </summary>
		[EnumMember] SmsKit,
		/// <summary> Пакет ETA (на учетную запись) </summary>
		[EnumMember] EtaKit,
		/// <summary> OEM Комплект (на учетную запись) </summary>
		[EnumMember] OemKit,
		/// <summary> Установка оборудования (на учетную запись) </summary>
		[EnumMember] InstallationOfEquipment,
	}
}