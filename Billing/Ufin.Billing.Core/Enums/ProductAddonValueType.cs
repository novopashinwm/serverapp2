﻿using System;
using System.Runtime.Serialization;

namespace Compass.Ufin.Billing.Core.Enums
{
	/// <summary> Тип значения дополнения продукта </summary>
	[Serializable]
	[DataContract]
	public enum ProductAddonValueType
	{
		[EnumMember] Boolean      = 1,
		[EnumMember] Integer      = 2,
		[EnumMember] Enumeratiron = 3,
	}
}