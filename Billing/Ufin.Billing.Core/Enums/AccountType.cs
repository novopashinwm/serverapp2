﻿using System;
using System.Runtime.Serialization;

namespace Compass.Ufin.Billing.Core.Enums
{
	/// <summary> Тип клиента </summary>
	[Serializable]
	[DataContract]
	public enum AccountType
	{
		/// <summary> Частный </summary>
		[EnumMember] Private,
		/// <summary> Корпоративный </summary>
		[EnumMember] Corporate,
	}
}