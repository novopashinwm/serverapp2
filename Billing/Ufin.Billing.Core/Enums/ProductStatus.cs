﻿using System;
using System.Runtime.Serialization;

namespace Compass.Ufin.Billing.Core.Enums
{
	/// <summary> Статус услуги </summary>
	[Serializable]
	[DataContract]
	public enum ProductStatus
	{
		/// <summary> Не определен </summary>
		[EnumMember] Unspecified = 0,
		/// <summary> Активен </summary>
		[EnumMember] Active      = 1,
		/// <summary> Остановлен </summary>
		[EnumMember] Stopped     = 2,
		/// <summary> Заказан </summary>
		[EnumMember] Ordered     = 3,
		/// <summary> Обрабатывается </summary>
		[EnumMember] Processing  = 4,
		/// <summary> Удален </summary>
		[EnumMember] Deleted     = 5,
	}
}