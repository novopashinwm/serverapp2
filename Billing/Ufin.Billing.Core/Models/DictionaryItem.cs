﻿using System;
using System.Runtime.Serialization;

namespace Compass.Ufin.Billing.Core.Models
{
	[Serializable]
	[DataContract]
	public class DictionaryItem
	{
		public const int EmptyId = -1;
		/// <summary> Целочисленный идентификатор </summary>
		[DataMember] public long   Id   { get; set; } = EmptyId;
		/// <summary> Строковый идентификатор </summary>
		[DataMember] public string Code { get; set; } = string.Empty;
		/// <summary> Наименование элемента справочника, может быть локализованным или отсутствовать </summary>
		[DataMember] public string Name { get; set; }
		/// <summary> Описание элемента справочника, может быть локализованным или отсутствовать </summary>
		[DataMember] public string Desc { get; set; }
	}
}