﻿using System;
using System.Runtime.Serialization;

namespace Compass.Ufin.Billing.Core.Models
{
	[Serializable]
	[DataContract]
	public class ProductParamItem : DictionaryItem
	{
		public const string MonitoringObjectIdenKey = "MonitoringObjectIdentifier";
		public const string MonitoringObjectNameKey = "MonitoringObjectName";
		[DataMember] public bool   Required { get; set; }
		[DataMember] public string Value    { get; set; }
	}
}