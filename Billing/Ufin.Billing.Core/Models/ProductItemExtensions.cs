﻿namespace Compass.Ufin.Billing.Core.Models
{
	public static class ProductItemExtensions
	{
		public static ProductItem WitoutPrices(this ProductItem product)
		{
			product.Price = null;
			if (0 < (product.Addons?.Count ?? 0))
				foreach (var productAddon in product.Addons)
					productAddon.Price = null;
			return product;
		}
		public static ProductItem WitoutIcons(this ProductItem product)
		{
			product.Icon = null;
			if (0 < (product.Addons?.Count ?? 0))
				foreach (var productAddon in product.Addons)
					productAddon.Icon = null;
			return product;
		}
		public static ProductItem WitoutAddonsMeta(this ProductItem product)
		{
			if (0 < (product.Addons?.Count ?? 0))
				foreach (var productAddon in product.Addons)
					productAddon.Meta = null;
			return product;
		}
		public static ProductItem WitoutAddons(this ProductItem product)
		{
			product.Addons = null;
			return product;
		}
		public static ProductItem WitoutParams(this ProductItem product)
		{
			product.Params = null;
			return product;
		}
		public static ProductItem WitoutNames(this ProductItem product)
		{
			product.Id        = DictionaryItem.EmptyId;
			product.Code      = null;
			product.Name      = null;
			product.Type.Id   = DictionaryItem.EmptyId;
			product.Type.Code = null;
			product.Type.Name = null;
			return product;
		}
	}
}