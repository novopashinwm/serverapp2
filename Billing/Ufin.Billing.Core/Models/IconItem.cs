﻿using System;
using System.Runtime.Serialization;

namespace Compass.Ufin.Billing.Core.Models
{
	[Serializable]
	[DataContract]
	public class IconItem
	{
		/// <summary> Ссылка на изображение </summary>
		[DataMember] public string IconUrl { get; set; }
		/// <summary> Изображение base64 </summary>
		[DataMember] public string IconB64 { get; set; }
	}
}