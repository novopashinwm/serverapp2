﻿using System;
using System.Runtime.Serialization;

namespace Compass.Ufin.Billing.Core.Models
{
	[Serializable]
	[DataContract]
	public class ProductAddonItem : DictionaryItem
	{
		[DataMember] public ProductAddonItemData  Data  { get; set; }
		[DataMember] public ProductAddonItemMeta  Meta  { get; set; }
		[DataMember] public ProductAddonItemPrice Price { get; set; }
		[DataMember] public IconItem              Icon  { get; set; }
	}
}