﻿using System;
using System.Runtime.Serialization;

namespace Compass.Ufin.Billing.Core.Models
{
	[Serializable]
	[DataContract]
	public class ProductAddonItemData
	{
		[DataMember] public long ValueUser { get; set; }
	}
}