﻿using System;
using System.Runtime.Serialization;

namespace Compass.Ufin.Billing.Core.Models
{
	[Serializable]
	[DataContract]
	public class ValueWithUnits<T>
	{
		[DataMember] public T        Cost { get; set; }
		[DataMember] public string   Unit { get; set; }
	}
}