﻿using System;
using System.Runtime.Serialization;
using ValueType = Compass.Ufin.Billing.Core.Enums.ProductAddonValueType;

namespace Compass.Ufin.Billing.Core.Models
{
	[Serializable]
	[DataContract]
	public class ProductAddonItemMeta
	{
		[DataMember] public ValueType ValueType     { get; set; } = ValueType.Integer;
		[DataMember] public string    ValueUnit     { get; set; } = string.Empty;
		[DataMember] public long      ValueInit     { get; set; } = 0L;
		[DataMember] public bool      CanBeModified { get; set; } = false;
		[DataMember] public long?     ValueMin      { get; set; }
		[DataMember] public long?     ValueMax      { get; set; }
		[DataMember] public long?     ValueStep     { get; set; }
	}
}