﻿using System;
using System.Runtime.Serialization;
using Compass.Ufin.Billing.Core.Enums;

namespace Compass.Ufin.Billing.Core.Models
{
	[Serializable]
	[DataContract]
	public class PriceItem
	{
		/// <summary> Цена, общая = (SetupAmount + PeriodAmount + Child.Sum(SetupAmount + PeriodAmount)) </summary>
		[DataMember] public decimal     TotalAmount    { get; set; } = 0M;
		/// <summary> Скидка, общая = (SetupDiscount + PeriodDiscount + Child.Sum(SetupDiscount + PeriodDiscount))</summary>
		[DataMember] public decimal     TotalDiscount  { get; set; } = 0M;
		/// <summary> Цена, на установку = (SetupAmount + Child.Sum(SetupAmount) </summary>
		[DataMember] public decimal     SetupAmount    { get; set; } = 0M;
		/// <summary> Скидка, на установку = (SetupDiscount + Child.Sum(SetupDiscount) </summary>
		[DataMember] public decimal     SetupDiscount  { get; set; } = 0M;
		/// <summary> Цена, за период = (PeriodAmount + Child.Sum(PeriodAmount) </summary>
		[DataMember] public decimal     PeriodAmount   { get; set; } = 0M;
		/// <summary> Скидка, за период = (PeriodDiscount + Child.Sum(PeriodDiscount) </summary>
		[DataMember] public decimal     PeriodDiscount { get; set; } = 0M;
		/// <summary> Длина периода </summary>
		[DataMember] public PricePeriod Period         { get; set; }
	}
}