﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Compass.Ufin.Billing.Core.Enums;
using FORIS.TSS.BusinessLogic.DTO;

namespace Compass.Ufin.Billing.Core.Models
{
	[Serializable]
	[CollectionDataContract]
	public class ProductList : List<ProductItem> { }
	/// <summary> Описание экземпляра продукта (тарифа/тарифного плана) </summary>
	[Serializable]
	[DataContract]
	public class ProductItem : DictionaryItem
	{
		/// <summary> Статус экземпляра продукта (тарифа/тарифного плана) </summary>
		[DataMember] public DictionaryEnum<ProductStatus> Status         { get; set; }
		/// <summary> Действует до. Дата, до которой оплачена услуга </summary>
		[DataMember] public DateTime?                     ExpirationDate { get; set; }
		/// <summary> Тип тарифного плана (с вложенным типом продукта) </summary>
		[DataMember] public ProductTypeItem               Type           { get; set; }
		/// <summary> Параметры тарифа, необходимы для привязки к основной системе </summary>
		[DataMember] public List<ProductParamItem>        Params         { get; set; } = new List<ProductParamItem>();
		/// <summary> Дополнения тарифа, наполнение тарифа (содержит наполнение заказанное в базовом варианте и с помощью дополнений) </summary>
		[DataMember] public List<ProductAddonItem>        Addons         { get; set; } = new List<ProductAddonItem>();
		[DataMember] public ProductPriceItem              Price          { get; set; }
		/// <summary> Изображение продукта </summary>
		[DataMember] public IconItem                      Icon           { get; set; }
	}
}