﻿using System;
using System.Runtime.Serialization;

namespace Compass.Ufin.Billing.Core.Models
{
	/// <summary> Тип продукта (Тарифный план) </summary>
	[Serializable]
	[DataContract]
	public class ProductTypeItem : DictionaryItem
	{
		/// <summary> Активность тарифного плана </summary>
		[DataMember] public bool           Active { get; set; } = false;
		/// <summary> Тип продукта, для которого создан этот тарифный план </summary>
		[DataMember] public DictionaryItem Base   { get; set; }
	}
}