﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Compass.Ufin.Billing.Core.Models
{
	[Serializable]
	[DataContract]
	public class BalanceInfo
	{
		[Serializable]
		[DataContract]
		public class ItemTypeInfo
		{
			[DataMember] public long    Id     { get; set; } = -1;
			[DataMember] public string  Code   { get; set; } = string.Empty;
			[DataMember] public string  Name   { get; set; } = string.Empty;
			[DataMember] public long    Count  { get; set; } = 0;
			[DataMember] public decimal Amount { get; set; } = 0M;
		}
		[Serializable]
		[DataContract]
		public class DiscountItem
		{
			[DataMember] public long    Id     { get; set; } = -1;
			[DataMember] public string  Name   { get; set; } = string.Empty;
			[DataMember] public string  Period { get; set; } = string.Empty;
			[DataMember] public decimal Value  { get; set; } = 0M;
			[DataMember] public string  Unit   { get; set; } = string.Empty;
		}
		[DataMember] public long               Id          { get; set; }
		[DataMember] public decimal            Balance     { get; set; }
		[DataMember] public decimal            TotalAmount { get; set; }
		[DataMember] public List<ItemTypeInfo> ItemTypes   { get; set; }
		[DataMember] public List<DiscountItem> Discounts   { get; set; }
		[DataMember] public CurrencyItem       Currency    { get; set; }
		[DataMember] public string             Creditlimit { get; set; }
	}
}