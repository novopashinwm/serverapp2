﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Security;
using Compass.Ufin.Billing.Providers.IspSystem.Models;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	public static partial class IspSystemBillApi
	{
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: payment, платежи </summary>
		public static async Task<PaymentList> PaymentListAsync(IspSystemBillApiContext context,
			string su = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"payment" },
				{ "su",   $@"{su}"    },
			}, cancellationToken))
				?.ToObject<PaymentList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: payment.su, платежи </summary>
		public static async Task<PaymentSuResult> PaymentSuAsync(IspSystemBillApiContext context,
			long paymentId, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"payment.su"  },
				{ "elid", $@"{paymentId}" },
			}, cancellationToken))
				?.ToObject<PaymentSuResult>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: payment.add, платежи </summary>
		public static async Task<PaymentAddResult> PaymentAddAsync(IspSystemBillApiContext context,
			decimal amount, long currencyId, long paymethodId, long profileId, string su, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
			{ "func",             $@"payment.add"                          },
			{ "randomnumber",     $@"{Membership.GeneratePassword(20, 0)}" },
			{ "amount",           $@"{amount}"                             },
			{ "payment_currency", $@"{currencyId}"                         },
			{ "paymethod",        $@"{paymethodId}"                        },
			{ "profile",          $@"{profileId}"                          },
			{ "sok",              $@"ok"                                   },
			{ "su",               $@"{su}"                                 },
			}, cancellationToken))
				?.ToObject<PaymentAddResult>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: addpayment, добавить платеж </summary>
		public static async Task<AddPaymentResult> PaymentAdd2Async(IspSystemBillApiContext context,
			decimal amount, long currencyId, long paymethodId, long subaccountId, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
		{
			{ "func",          $@"addpayment"     },
			{ "amount",        $@"{amount}"       },
			{ "currency",      $@"{currencyId}"   },
			{ "paymethod",     $@"{paymethodId}"  },
			{ "subaccount",    $@"{subaccountId}" },
		}, cancellationToken))
				?.ToObject<AddPaymentResult>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: payment.add.method, получить список доступных методов оплаты </summary>
		public static async Task<PaymentAddPaymethodList> PaymentAddPayMethodListAsync(IspSystemBillApiContext context,
			string su, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"payment.add.method" },
				{ "su",   $@"{su}"               },
			}, cancellationToken))
				?.ToObject<PaymentAddPaymethodList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: payment.add.redirect, получить список доступных методов оплаты </summary>
		public static async Task<PaymentAddRedirectResult> PaymentAddRedirectAsync(IspSystemBillApiContext context,
			long paymentId, string su = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"payment.add.redirect" },
				{ "elid", $@"{paymentId}"          },
				{ "sok",  $@"ok"                   },
				{ "su",   $@"{su}"                 },
			}, cancellationToken))
				?.ToObject<PaymentAddRedirectResult>();
		}
		////////////////////////////////////////////////////////////////////////////////
	}
}