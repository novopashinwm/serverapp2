﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Compass.Ufin.Billing.Core.Enums;
using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Compass.Ufin.Billing.Providers.IspSystem.Models;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	public static partial class IspSystemBillApi
	{
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: "monitoringobject" список заказов типа MonitoringObject </summary>
		public static async Task<MonitoringObjectList> MonitoringObjectListAsync(IspSystemBillApiContext context,
			string user = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			var productTypeName = nameof(ProductType.MonitoringObject).ToLowerInvariant();
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"{productTypeName}" },
				{ "su",   $@"{user}"            },
			}, cancellationToken))
				?.ToObject<MonitoringObjectList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: "monitoringobject.order.param" рассчитать параметры заказа или применить заказ типа MonitoringObject </summary>
		public static async Task<object> MonitoringObjectOrderParamAsync(IspSystemBillApiContext context,
			string user,
			long pricelistId,
			IEnumerable<KeyValuePair<string, string>> additionValues = null,
			Period periodId = Period.Monthly,
			long datacenterId = 1,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			additionValues = additionValues ?? Enumerable.Empty<KeyValuePair<string, string>>();
			var productTypeName = nameof(ProductType.MonitoringObject).ToLowerInvariant();
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func",       $@"{productTypeName}.order.param" },
				{ "pricelist",  $@"{pricelistId}"                 },
				{ "datacenter", $@"{datacenterId}"                },
				{ "period",     $@"{(long)periodId}"              },
				{ "sok",        $@"ok"                            },
				{ "su",         $@"{user}"                        },
			}.Union(additionValues), cancellationToken))
				?.ToObject<object>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: "monitoringobject.edit" редактировать услугу типа MonitoringObject </summary>
		public static async Task<ItemTypeInstanceEditResult> MonitoringObjectEditAsync(IspSystemBillApiContext context,
			string user,
			long currentServiceId,
			IEnumerable<KeyValuePair<string, string>> addonValues = null,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			addonValues = addonValues ?? Enumerable.Empty<KeyValuePair<string, string>>();
			var productTypeName = nameof(ProductType.MonitoringObject).ToLowerInvariant();
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func",  $@"{productTypeName}.edit" },
				{ "elid",  $@"{currentServiceId}"     },
				{ "sok",   $@"ok"                     },
				{ "su",    $@"{user}"                 },
			}.Union(addonValues), cancellationToken))
				?.ToObject<ItemTypeInstanceEditResult>();
		}
	}
}