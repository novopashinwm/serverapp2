﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Runtime.Caching;
using System.ServiceModel;
using Compass.Ufin.Billing.Core;
using Compass.Ufin.Billing.Core.Enums;
using Compass.Ufin.Billing.Core.Models;
using Compass.Ufin.Billing.Providers.IspSystem.Configuration;
using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Compass.Ufin.Billing.Providers.IspSystem.Models;
using Flurl;
using Flurl.Http;
using FORIS.TSS.Common.Helpers;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	//[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, MaxItemsInObjectGraph = 2147483646, IncludeExceptionDetailInFaults = true)]
	[ServiceBehavior(
		InstanceContextMode = InstanceContextMode.Single,
		ConcurrencyMode     = ConcurrencyMode.Multiple,
		IncludeExceptionDetailInFaults = true)]
	public partial class PaymentProvider : IPaymentProvider
	{
		////////////////////////////////////////////////////////////////////////////////
		private static MemoryCache _cache = MemoryCache.Default;
		////////////////////////////////////////////////////////////////////////////////
		public string Addr { get; }
		public string User { get; }
		public string Pass { get; }
		public string Proj { get; }
		private int FastCacheSecond { get; } = 030;
		private int SlowCacheSecond { get; } = 300;
		private DateTimeOffset FastCacheExpiration => DateTimeOffset.UtcNow.AddSeconds(FastCacheSecond);
		private DateTimeOffset SlowCacheExpiration => DateTimeOffset.UtcNow.AddSeconds(SlowCacheSecond);
		public PaymentProvider()
		{
			var config = IspSystemConfigurationSection.Instance;
			if (config.ElementInformation.IsPresent)
			{
				Addr = config.Addr;
				if (string.IsNullOrWhiteSpace(Addr))
					throw new ArgumentNullException(nameof(Addr));
				User = config.User;
				if (string.IsNullOrWhiteSpace(User))
					throw new ArgumentNullException(nameof(User));
				Pass = config.Pass;
				Proj = config.Proj;
				if (config.FastCacheSecond >= 0)
					FastCacheSecond = config.FastCacheSecond;
				if (config.SlowCacheSecond >= 0)
					SlowCacheSecond = config.SlowCacheSecond;
			}
			else
				throw new ConfigurationErrorsException(
					$@"Configuration section '{IspSystemConfigurationSection.Instance.SectionInformation.Name}' not found.");
		}
		public PaymentProvider(string addr, string user, string pass, string proj)
		{
			Addr = addr;
			if (string.IsNullOrWhiteSpace(Addr))
				throw new ArgumentNullException(nameof(Addr));
			User = user;
			if (string.IsNullOrWhiteSpace(User))
				throw new ArgumentNullException(nameof(User));
			Pass = pass;
			Proj = proj;
		}
		public IspSystemBillApiContext GetBillContext(string lang = "ru")
		{
			return IspSystemBillApi.GetContext(User, Pass, Addr, lang);
		}
		#region Private Methods

		////////////////////////////////////////////////////////////////////////////////////////
		/// Картинки
		private byte[] GetImage(string url)
		{
			return url.GetBytesAsync()
				?.Result;
		}
		private byte[] GetCachedImage(string url)
		{
			var key = url;
			return _cache.GetObjectFromCache(
				cacheDataKey:    url,
				cacheDataGet:    () => GetImage(url),
				cacheExpiration: null /*Вечно*/);
		}
		////////////////////////////////////////////////////////////////////////////////////////
		/// Языки
		private IEnumerable<LocaleItem> GetLocaleList(string lang = "ru")
		{
			using (var billCtx = GetBillContext(lang))
			{
				return IspSystemBillApi.LocaleListAsync(billCtx)
					?.Result
					?.Items ?? Enumerable.Empty<LocaleItem>();
			}
		}
		private IEnumerable<LocaleItem> GetCachedLocaleList(string lang = "ru")
		{
			return _cache.GetObjectFromCache(
				cacheDataKey:    $@"{lang}_{nameof(GetLocaleList)}",
				cacheDataGet:    () => GetLocaleList(lang),
				cacheExpiration: SlowCacheExpiration);
		}
		////////////////////////////////////////////////////////////////////////////////////////
		/// Страны
		private IEnumerable<CountryItem> GetCountryList(string lang = "ru")
		{
			using (var billCtx = GetBillContext(lang))
			{
				return IspSystemBillApi.CountryListAsync(billCtx)
					?.Result
					?.Items ?? Enumerable.Empty<CountryItem>();
			}
		}
		private IEnumerable<CountryItem> GetCachedCountryList(string lang = "ru")
		{
			return _cache.GetObjectFromCache(
				cacheDataKey:    $@"{lang}_{nameof(GetCountryList)}",
				cacheDataGet:    () => GetCountryList(lang),
				cacheExpiration: SlowCacheExpiration);
		}
		////////////////////////////////////////////////////////////////////////////////////////
		/// Валюты
		private IEnumerable<Models.CurrencyItem> GetCurrencyList(string lang = "ru")
		{
			using (var billCtx = GetBillContext(lang))
			{
				return IspSystemBillApi.CurrencyListAsync(billCtx)
					?.Result
					?.Items ?? Enumerable.Empty<Models.CurrencyItem>();
			}
		}
		private IEnumerable<Models.CurrencyItem> GetCachedCurrencyList(string lang = "ru")
		{
			return _cache.GetObjectFromCache(
				cacheDataKey:    $@"{lang}_{nameof(GetCurrencyList)}",
				cacheDataGet:    () => GetCurrencyList(lang),
				cacheExpiration: SlowCacheExpiration);
		}
		////////////////////////////////////////////////////////////////////////////////////////
		/// Единицы измерения
		private IEnumerable<MeasureItem> GetMeasureList(string lang = "ru")
		{
			using (var billCtx = GetBillContext(lang))
			{
				return IspSystemBillApi.MeasureListAsync(billCtx)
					?.Result
					?.Items ?? Enumerable.Empty<MeasureItem>();
			}
		}
		private IEnumerable<MeasureItem> GetCachedMeasureList(string lang = "ru")
		{
			return _cache.GetObjectFromCache(
				cacheDataKey:    $@"{lang}_{nameof(GetMeasureList)}",
				cacheDataGet:    () => GetMeasureList(lang),
				cacheExpiration: SlowCacheExpiration);
		}
		////////////////////////////////////////////////////////////////////////////////////////
		/// Типы продуктов
		private IEnumerable<ItemTypeItem> GetItemTypeList(string lang = "ru")
		{
			using (var billCtx = GetBillContext(lang))
			{
				return IspSystemBillApi.ItemTypeListAsync(billCtx)
					?.Result
					?.Items ?? Enumerable.Empty<ItemTypeItem>();
			}
		}
		private IEnumerable<ItemTypeItem> GetCachedItemTypeList(string lang = "ru")
		{
			return _cache.GetObjectFromCache(
				cacheDataKey:    $@"{lang}_{nameof(GetItemTypeList)}",
				cacheDataGet:    () => GetItemTypeList(lang),
				cacheExpiration: SlowCacheExpiration);
		}
		////////////////////////////////////////////////////////////////////////////////////////
		/// <summary> Получить доступные тарифные планы </summary>
		/// <param name="user"> Клиент, если null для всех </param>
		/// <param name="productType"> Тип продукта, если null для всех </param>
		/// <param name="lang"> Язык ответа </param>
		/// <returns></returns>
		private IEnumerable<PriceListExportItem> GetAvailablePriceListExportList(string user = null, ProductType? productType = null, string lang = "ru")
		{
			using (var billCtx = GetBillContext(lang))
			{
				return IspSystemBillApi.PriceListExportAsync(billCtx, user, productType)
					?.Result
					?.Items ?? Enumerable.Empty<PriceListExportItem>();
			}
		}
		////////////////////////////////////////////////////////////////////////////////////////
		/// <summary> Получить доступные тарифные планы (из кэша) </summary>
		/// <param name="user"> Клиент, если null для всех </param>
		/// <param name="productType"> Тип продукта, если null для всех </param>
		/// <param name="lang"> Язык ответа </param>
		/// <returns></returns>
		private IEnumerable<PriceListExportItem> GetCachedAvailablePriceListExportList(string user = null, ProductType? productType = null, string lang = "ru")
		{
			return _cache.GetObjectFromCache(
				cacheDataKey:    $@"{lang}_{user}_{productType?.ToString()}_{nameof(GetAvailablePriceListExportList)}",
				cacheDataGet:    () => GetAvailablePriceListExportList(user, productType, lang),
				cacheExpiration: SlowCacheExpiration);
		}
		////////////////////////////////////////////////////////////////////////////////////////
		/// <summary> Получить по идентификатору экспортированный тарифный план </summary>
		/// <param name="priceListId"> Идентификатор экспортированного тарифного плана </param>
		/// <param name="lang"> Язык ответа </param>
		/// <returns></returns>
		private PriceListExportItem GetPriceListExportById(long priceListId, string lang = "ru")
		{
			using (var billCtx = GetBillContext(lang))
			{
				return IspSystemBillApi.PriceListExportAsync(billCtx, new[] { priceListId })
					?.Result
					?.Items
					?.FirstOrDefault();
			}
		}
		private PriceListExportItem GetCachedPriceListExportById(long priceListId, string lang = "ru")
		{
			return _cache.GetObjectFromCache(
			cacheDataKey:    $@"{lang}_{nameof(GetPriceListExportById)}_{priceListId}",
			cacheDataGet:    () => GetPriceListExportById(priceListId, lang),
			cacheExpiration: SlowCacheExpiration);
		}
		////////////////////////////////////////////////////////////////////////////////////////
		private ItemTypeInstanceDetail GetProductItemDetail(string productTypeCode, long productId, string lang = "ru")
		{
			using (var billCtx = GetBillContext(lang))
			{
				return IspSystemBillApi.ItemTypeInstanceDetailAsync(billCtx, productTypeCode, productId)
					?.Result;
			}
		}
		private ItemTypeInstanceDetail GetCachedProductDetail(string productTypeCode, long productId, string lang = "ru")
		{
			return _cache.GetObjectFromCache(
				cacheDataKey: $@"{lang}_{productTypeCode.ToLowerInvariant()}_{nameof(GetProductItemDetail)}_{productId}",
				cacheDataGet: () => GetProductItemDetail(productTypeCode, productId, lang),
				cacheExpiration: SlowCacheExpiration);
		}
		////////////////////////////////////////////////////////////////////////////////////////
		/// Все активные продукты клиента
		private IEnumerable<ItemAllItem> GetAllActiveItemList(string user, string lang = "ru")
		{
			var activeStatuses = $@"{(int)ServiceStatus.Active},{(int)ServiceStatus.Ordered},{(int)ServiceStatus.Processing}";
			using (var billCtx = GetBillContext(lang)
				// Фильтруем только по активным
				.WithProperties(new Dictionary<string, object>
				{
					{ IspSystemBillApiContext.KEY_FILTER, $@"on"               },
					{ "status",                           $@"{activeStatuses}" },
				}))
			{
				return IspSystemBillApi.ItemAllListAsync(billCtx, user)
					?.Result
					?.Items
					?.OrderByDescending(s => s.Id) ?? Enumerable.Empty<ItemAllItem>();
			}
		}
		private IEnumerable<ItemAllItem> GetCachedAllActiveItemList(string user, string lang = "ru")
		{
			return _cache.GetObjectFromCache(
				cacheDataKey:    $@"{lang}_{user}_{nameof(GetAllActiveItemList)}",
				cacheDataGet:    () => GetAllActiveItemList(user, lang),
				cacheExpiration: SlowCacheExpiration);
		}
		////////////////////////////////////////////////////////////////////////////////////////
		/// Скидки клиента
		private IEnumerable<AccountDiscountItem> GetDiscountList(string user, string lang = "ru")
		{
			using (var billCtx = GetBillContext(lang))
			{
				return IspSystemBillApi.AccountDiscountInfoListAsync(billCtx, user)
					?.Result
					?.Items
					?.Where(d => d.Expired == OnOff.Off)
					?.OrderByDescending(s => s.Id) ?? Enumerable.Empty<AccountDiscountItem>();
			}
		}
		private IEnumerable<AccountDiscountItem> GetCachedDiscountList(string user, string lang = "ru")
		{
			return _cache.GetObjectFromCache(
				cacheDataKey:    $@"{lang}_{user}_{nameof(GetDiscountList)}",
				cacheDataGet:    () => GetDiscountList(user, lang),
				cacheExpiration: SlowCacheExpiration);
		}
		////////////////////////////////////////////////////////////////////////////////////////
		/// Счета (активные) клиента
		private IEnumerable<SubaccountItem> GetActiveSubaccountList(string user, string lang = "ru")
		{
			using (var billCtx = GetBillContext(lang))
			{
				return IspSystemBillApi.SubaccountListAsync(billCtx, user)
					?.Result
					?.Items
					?.Where(s => s.Active == OnOff.On)
					?.OrderByDescending(s => s.Id) ?? Enumerable.Empty<SubaccountItem>();
			}
		}
		private IEnumerable<SubaccountItem> GetCachedActiveSubaccountList(string user, string lang = "ru")
		{
			return _cache.GetObjectFromCache(
				cacheDataKey:    $@"{lang}_{user}_{nameof(GetActiveSubaccountList)}",
				cacheDataGet:    () => GetActiveSubaccountList(user, lang),
				cacheExpiration: SlowCacheExpiration);
		}
		////////////////////////////////////////////////////////////////////////////////////////
		/// Детализация счета для администратора
		private SubaccountEdit GetSubaccountDetail(long subaccountId, string lang = "ru")
		{
			using (var billCtx = GetBillContext(lang))
			{
				return IspSystemBillApi.SubaccountDetailAsync(billCtx, subaccountId)
					?.Result;
			}
		}
		private SubaccountEdit GetCachedSubaccountDetail(long subaccountId, string lang = "ru")
		{
			return _cache.GetObjectFromCache(
				cacheDataKey:    $@"{lang}_{nameof(GetSubaccountDetail)}_{subaccountId}",
				cacheDataGet:    () => GetSubaccountDetail(subaccountId, lang),
				cacheExpiration: SlowCacheExpiration);
		}
		////////////////////////////////////////////////////////////////////////////////////////
		/// Активные экземпляры продуктов/услуг...
		private IEnumerable<ItemTypeInstanceItem> GetItemTypeInstanceList(ProductType productType, string user, string lang = "ru")
		{
			using (var billCtx = GetBillContext(lang))
			{
				return IspSystemBillApi.ItemTypeInstanceListAsync(billCtx, productType.ToString(), user)
					?.Result
					?.Items ?? Enumerable.Empty<ItemTypeInstanceItem>();
			}
		}
		private IEnumerable<ItemTypeInstanceItem> GetCachedItemTypeInstanceList(ProductType productType, string user, string lang = "ru")
		{
			return _cache.GetObjectFromCache(
				cacheDataKey:    $@"{lang}_{user}_{productType.ToString().ToLowerInvariant()}_{nameof(GetItemTypeInstanceList)}",
				cacheDataGet:    () => GetItemTypeInstanceList(productType, user, lang),
				cacheExpiration: SlowCacheExpiration);
		}
		////////////////////////////////////////////////////////////////////////////////////////
		/// Активные тарифные планы, доступные для заказа (после любой покупки нужно чистить кэш)
		private IEnumerable<ItemTypeInstanceOrderPricelist.TarifList> GetItemTypeOrderPricelists(ProductType productType, string user, string lang = "ru")
		{
			using (var billCtx = GetBillContext(lang))
			{
				return IspSystemBillApi.ItemTypeInstanceOrderPricelistsAsync(billCtx, productType.ToString(), user)
					?.Result
					?.List
					?.TariffLists ?? Enumerable.Empty<ItemTypeInstanceOrderPricelist.TarifList>();
			}
		}
		private IEnumerable<ItemTypeInstanceOrderPricelist.TarifList> GetCachedItemTypeOrderPricelists(ProductType productType, string user, string lang = "ru")
		{
			return _cache.GetObjectFromCache(
				cacheDataKey:    $@"{lang}_{user}_{productType.ToString().ToLowerInvariant()}_{nameof(GetItemTypeOrderPricelists)}",
				cacheDataGet:    () => GetItemTypeOrderPricelists(productType, user, lang),
				cacheExpiration: SlowCacheExpiration);
		}
		////////////////////////////////////////////////////////////////////////////////////////
		/// Преобразование экспортированного тарифного плана в объект DTO
		private ProductItem GetProductFromPriceListExport(PriceListExportItem priceList, string lang = "ru")
		{
			if (null == priceList)
				return default(ProductItem);
			var productBase = GetCachedItemTypeList(lang)
				?.FirstOrDefault(x => x.Id == priceList.ItemType);
			var product = new ProductItem()
			{
				// Это идентификатор и имя для экземпляра, заполнять не нужно
				//Id      = ,
				//Code    = ,
				//Name    = ,
				//Desc    = ,
				Type = new ProductTypeItem // Тарифный план
				{
					Id     = priceList.Id,
					Code   = priceList.Intname,
					Name   = ("ru" == lang ? priceList.NameRu        : priceList.Name       )?.StripHtmlTags(),
					Desc   = ("ru" == lang ? priceList.DescriptionRu : priceList.Description)?.StripHtmlTags(),
					Base   = new DictionaryItem // Базовый тип продукта
					{
						Id   = productBase.Id,
						Code = productBase.Intname,
						Name = productBase.Name,
					},
					Active = priceList.Active == OnOff.On,
				},
			};
			var productImage = priceList.Orderimage;
			if (!string.IsNullOrWhiteSpace(productImage))
			{
				var iconUrl = Url.Combine(Addr, productImage);
				product.Icon = new IconItem
				{
					IconUrl = iconUrl,
					IconB64 = Convert.ToBase64String(GetCachedImage(iconUrl)),
				};
			}
			product.Params = priceList.ItemTypeParams
				?.Select(p => new ProductParamItem
				{
					Id       = p.Id,
					Code     = p.Intname,
					Name     = ("ru" == lang ? p.NameRu        : p.Name       )?.StripHtmlTags(),
					Desc     = ("ru" == lang ? p.DescriptionRu : p.Description)?.StripHtmlTags(),
					Required = (p?.ItemtypeparamRequired ?? OnOff.Off) == OnOff.On,
				})
				?.ToList();
			product.Addons = priceList.Addons
				?.Select(a =>
				{
					var productAddon = new ProductAddonItem
					{
						Id   = a.Id,
						Code = a.AddonItemtypeInfo?.Intname,
						Name = ("ru" == lang ? a.AddonItemtypeInfo?.NameRu        : a.AddonItemtypeInfo?.Name       )?.StripHtmlTags(),
						Desc = ("ru" == lang ? a.AddonItemtypeInfo?.DescriptionRu : a.AddonItemtypeInfo?.Description)?.StripHtmlTags(),
						Meta = new ProductAddonItemMeta
						{
							ValueType     = (ProductAddonValueType)a.Addontype,
							ValueUnit     = GetCachedMeasureList(lang)?.FirstOrDefault(m => m.Id == a.Measure)?.Name,
							ValueInit     = a.Addonlimit ?? (OnOff.On == a.Addonbool ? 1 : 0),
							ValueStep     = a.Addonstep,
							ValueMin      = a.Addonmin,
							ValueMax      = a.Addonmax,
							CanBeModified = !(1 == a.Billtype)
						},
						Price = new ProductAddonItemPrice
						{
							SetupAmount  = a?.Price
								?.Setup
								?.Cost ?? 0M,
							PeriodAmount = a?.Price
								?.Periods
								?.FirstOrDefault(p => p.Type == "month")
								?.Cost ?? 0M,
							Period = (PricePeriod?)priceList?.Price
								?.Periods
								?.FirstOrDefault(p => p.Type == "month")
								?.Length ?? PricePeriod.Trial,
						}
					};
					productAddon.Price.TotalAmount = productAddon.Price.SetupAmount + productAddon.Price.PeriodAmount;
					// Устанавливаем текущее значение, такое же, что и положено по тарифу
					productAddon.Data = new ProductAddonItemData
					{
						ValueUser = productAddon.Meta.ValueInit,
					};
					// Загружаем картинки через кэш
					var addonImage = a.AddonItemtypeInfo?.Orderimage;
					if (!string.IsNullOrWhiteSpace(addonImage))
					{
						var iconUrl = Url.Combine(Addr, @"/manimg/userdata/itemtype/", addonImage);
						productAddon.Icon = new IconItem
						{
							IconUrl = iconUrl,
							IconB64 = Convert.ToBase64String(GetCachedImage(iconUrl)),
						};
					}
					return productAddon;
				})
				?.ToList();
			product.Price = new ProductPriceItem
			{
				Currency = GetCachedCurrencyList(lang)
					?.Where(c => c.Iso == priceList?.Price?.Currency)
					?.Select(c => new Core.Models.CurrencyItem
					{
						Id   = c.Id,
						Code = c.Iso,
						Name = c.Name,
					})
					?.FirstOrDefault(),
				SetupAmount = priceList?.Price
					?.Setup
					?.Cost ?? 0M,
				PeriodAmount = priceList?.Price
					?.Periods
					?.FirstOrDefault(p => p.Type == "month")
					?.Cost ?? 0M,
				Period       = (PricePeriod?)priceList?.Price
					?.Periods
					?.FirstOrDefault(p => p.Type == "month")
					?.Length ?? PricePeriod.Trial,
			};
			product.Price.TotalAmount = product.Price.SetupAmount + product.Price.PeriodAmount;
			return product;
		}
		////////////////////////////////////////////////////////////////////////////////////////
		/// <summary> Заполнение базового тарифного плана данными конкретного экземпляра </summary>
		private ProductItem FillInstanceFields(ProductItem productItem, string lang = "ru")
		{
			// Проверяем условия и если, что не так выходим без изменений
			if (null == productItem?.Id || string.IsNullOrWhiteSpace(productItem?.Type?.Base?.Code))
				return productItem;
			// Ищем детали и если их нет выходим без изменений
			var productItemDetail = GetCachedProductDetail(productItem.Type.Base.Code, productItem.Id, lang);
			if (null == productItemDetail)
				return productItem;
			// Заполняем параметры экземпляра
			var productParams = productItem.Params ?? new List<ProductParamItem>();
			foreach (var productParam in productParams)
			{
				var productParamValue = productItemDetail.Model
					.FirstOrDefault(k =>
						k.Key == productParam.Code &&
						!string.IsNullOrWhiteSpace(k.Value?.ToString()))
					.Value;
				if (null != productParamValue)
					productParam.Value = productParamValue.ToString();
			}
			var boolVal = default(OnOff);
			var longVal = default(long);
			var deciVal = default(decimal);
			var dateVal = default(DateTime);
			// Дата окончания услуги, до которой услуга оплачена
			var expDateObj = productItemDetail.Model
				.FirstOrDefault(k =>
					k.Key == "expiredate" &&
					!string.IsNullOrWhiteSpace(k.Value?.ToString()))
				.Value;
			if (null != expDateObj)
			{
				if (DateTime.TryParse(expDateObj.ToString(), out dateVal))
					productItem.ExpirationDate = dateVal;
			}
			// Значения дополнений (доп. услуг) экземпляра
			var keyVals = productItemDetail.Model
				.Where(k =>
					k.Key?.StartsWith("addon_") ?? false &&
					!string.IsNullOrWhiteSpace(k.Value?.ToString()));
			foreach (var keyVal in keyVals)
			{
				var productItemAddon = productItem?.Addons?.FirstOrDefault(a => $"addon_{a.Id}" == keyVal.Key);

				if (null == productItemAddon                 ||
					null == productItemAddon.Meta?.ValueType ||
					null == productItemAddon.Meta?.ValueInit)
					continue;

				if (null == productItemAddon.Data)
					productItemAddon.Data = new ProductAddonItemData();

				switch (productItemAddon.Meta.ValueType)
				{
					case ProductAddonValueType.Boolean:
						if (Enum.TryParse(keyVal.Value.ToString(), true, out boolVal))
							productItemAddon.Data.ValueUser = OnOff.On == boolVal ? 1 : 0;
						break;
					case ProductAddonValueType.Integer:
						if (long.TryParse(keyVal.Value.ToString(), out longVal))
							// В longVal возвращается полное значение начальное плюс добавленное
							productItemAddon.Data.ValueUser = longVal;
						// Проверяем на выход за пределы для целых
						var min = productItemAddon.Meta.ValueMin ?? productItemAddon.Meta.ValueInit;
						var max = productItemAddon.Meta.ValueMax ?? productItemAddon.Meta.ValueInit;
						if (min > productItemAddon.Data.ValueUser)
							productItemAddon.Data.ValueUser = min;
						else if (max < productItemAddon.Data.ValueUser)
							productItemAddon.Data.ValueUser = max;
						break;
					case ProductAddonValueType.Enumeratiron:
					default:
						break;
				}
				// Добавить цены на доп услуги
				//if (null != productItemDetail.CustomFields)
				//{
				//	var addonCustomFieldValue = productItemDetail.CustomFields
				//		.FirstOrDefault(f => f.Key == keyVal.Key)
				//		.Value as IDictionary<string, object>;
				//	if (null != addonCustomFieldValue)
				//	{
				//		if (null == productItemAddon.Price)
				//			productItemAddon.Price = new ProductAddonItemPrice();
				//
				//		var addonPriceCstObj = addonCustomFieldValue.FirstOrDefault(f => f.Key == "cost").Value;
				//		if (addonPriceCstObj != null)
				//		{
				//			// У купленных продуктов нет ни скидок ни значений на установку (исходные непонятно как взять)
				//			productItemAddon.Price.SetupAmount    = 0M;
				//			productItemAddon.Price.SetupDiscount  = 0M;
				//			productItemAddon.Price.PeriodDiscount = 0M;
				//			var baseAddonPeriodAmount = productItemAddon.Price.PeriodAmount;
				//			if (decimal.TryParse(addonPriceCstObj?.ToString(), out deciVal))
				//				productItemAddon.Price.PeriodAmount   = deciVal;
				//		}
				//		var addonPricePrdObj = addonCustomFieldValue.FirstOrDefault(f => f.Key == "period").Value;
				//		if (addonPricePrdObj != null)
				//		{
				//			if (long.TryParse(addonPricePrdObj.ToString(), out longVal))
				//				productItemAddon.Price.Period = (PricePeriod)longVal;
				//		}
				//		productItemAddon.Price.TotalAmount   = productItemAddon.Price.SetupAmount   + productItemAddon.Price.PeriodAmount;
				//		productItemAddon.Price.TotalDiscount = productItemAddon.Price.SetupDiscount + productItemAddon.Price.PeriodDiscount;
				//	}
				//}
				productItemAddon.Price.SetupAmount    = 0M;
				productItemAddon.Price.SetupDiscount  = 0M;
				productItemAddon.Price.PeriodDiscount = 0M;
				productItemAddon.Price.PeriodAmount   = 0M;
				productItemAddon.Price.TotalAmount    = productItemAddon.Price.SetupAmount   + productItemAddon.Price.PeriodAmount;
				productItemAddon.Price.TotalDiscount  = productItemAddon.Price.SetupDiscount + productItemAddon.Price.PeriodDiscount;

			}
			var productItemPriceCstObj = productItemDetail.Model.FirstOrDefault(f => f.Key == "cost").Value;
			if (productItemPriceCstObj != null)
			{
				productItem.Price.SetupAmount    = 0M;
				productItem.Price.SetupDiscount  = 0M;
				productItem.Price.PeriodDiscount = 0M;
				var baseProductItemPeriodAmount = productItem.Price.PeriodAmount;
				if (decimal.TryParse(productItemPriceCstObj?.ToString(), out deciVal))
					productItem.Price.PeriodAmount   = deciVal;
			}
			var productItemPricePrdObj = productItemDetail.Model.FirstOrDefault(f => f.Key == "period").Value;
			if (productItemPricePrdObj != null)
			{
				if (long.TryParse(productItemPricePrdObj.ToString(), out longVal))
					productItem.Price.Period = (PricePeriod)longVal;
			}
			productItem.Price.TotalAmount    = 0M
				+ productItem.Price.SetupAmount
				+ productItem.Price.PeriodAmount
				+ (null != productItem.Addons ? productItem.Addons.Sum(a => a.Price?.TotalAmount   ?? 0M) : 0M);
			productItem.Price.TotalDiscount  = 0M
				+ productItem.Price.SetupDiscount
				+ productItem.Price.PeriodDiscount
				+ (null != productItem.Addons ? productItem.Addons.Sum(a => a.Price?.TotalDiscount ?? 0M) : 0M);
			return productItem;
		}
		////////////////////////////////////////////////////////////////////////////////////////
		/// <summary> Преобразование экземпляра услуги в продукт </summary>
		private ProductItem GetProductFromItemTypeInstance(ItemTypeInstanceItem instanceItem, string lang = "ru")
		{
			// Найти тариф для объекта мониторинга
			var result = GetProductFromPriceListExport(
				GetCachedPriceListExportById(instanceItem.PriceListId, lang), lang);
			if (null == result)
				return result;
			result.Id   = instanceItem.Id;
			result.Code = instanceItem.Name;
			result.Name = instanceItem.Name;
			switch ((ServiceStatus)instanceItem.Status)
			{
				case ServiceStatus.Active:
					result.Status = ProductStatus.Active.ToDictionaryEnum(CultureInfo.GetCultureInfo(lang));
					break;
				case ServiceStatus.Stopped:
					result.Status = ProductStatus.Stopped.ToDictionaryEnum(CultureInfo.GetCultureInfo(lang));
					break;
				case ServiceStatus.Ordered:
					result.Status = ProductStatus.Ordered.ToDictionaryEnum(CultureInfo.GetCultureInfo(lang));
					break;
				case ServiceStatus.Processing:
					result.Status = ProductStatus.Processing.ToDictionaryEnum(CultureInfo.GetCultureInfo(lang));
					break;
				case ServiceStatus.Deleted:
					result.Status = ProductStatus.Deleted.ToDictionaryEnum(CultureInfo.GetCultureInfo(lang));
					break;
				default:
					break;
			}
			return FillInstanceFields(result);
		}
		////////////////////////////////////////////////////////////////////////////////////////

		#endregion Private Methods
		#region IPaymentProvider

		public string GetOneTimeLoginLink(string user, string lang = "ru")
		{
			// Это кэшировать нельзя, т.к. ссылка одноразовая
			using (var billCtx = GetBillContext(lang))
			{
				return IspSystemBillApi.GetOneTimeLoginLink(billCtx, user)
					?.Result;
			}
		}

		#endregion IPaymentProvider
	}
}