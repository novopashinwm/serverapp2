﻿using System;
using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class ItemAllList : FuncResultList<ItemAllItem> { }
	public class ItemAllItem
	{
		[JsonProperty("id")]                     public long           Id          { get; set; }
		[JsonProperty("name")]                   public string         Name        { get; set; }
		[JsonProperty("itemtype")]               public string         ItemType    { get; set; }
		[JsonProperty("pricelist_id")]           public long           PriceListId { get; set; }
		[JsonProperty("status_orig")]            public long           StatusId    { get; set; }
		[JsonProperty("status")]                 public string         StatusName  { get; set; }
		[JsonProperty("cost")]                   public string         Cost        { get; set; }
		[JsonProperty("createdate")]             public DateTimeOffset Createdate  { get; set; }
		[JsonProperty("instruction")]            public OnOff          Instruction { get; set; }
	}
}