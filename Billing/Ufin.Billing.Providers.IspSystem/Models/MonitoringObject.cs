﻿using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class MonitoringObjectList : FuncResultList<MonitoringObjectItem> { }
	public class MonitoringObjectItem : ItemTypeInstanceItem
	{
		[JsonProperty("MonitoringObjectIdentifier")] public long   MonitoringObjectIdentifier { get; set; }
		[JsonProperty("MonitoringObjectName")]       public string MonitoringObjectName       { get; set; }
	}
}