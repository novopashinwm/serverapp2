﻿using System;
using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class ItemCmnList : FuncResultList<ItemCmnItem> { }
	public class ItemCmnItem
	{
		[JsonProperty("cost")]                    public string         Cost                   { get; set; }
		[JsonProperty("costperiod")]              public long           Costperiod             { get; set; }
		[JsonProperty("createdate")]              public DateTimeOffset Createdate             { get; set; }
		[JsonProperty("currency_id")]             public long           CurrencyId             { get; set; }
		[JsonProperty("currency_str")]            public string         CurrencyName           { get; set; }
		[JsonProperty("description")]             public string         Description            { get; set; }
		[JsonProperty("expiredate")]              public string         Expiredate             { get; set; }
		[JsonProperty("id")]                      public long           Id                     { get; set; }
		[JsonProperty("intname")]                 public string         Intname                { get; set; }
		[JsonProperty("item_cost")]               public decimal        ItemCost               { get; set; }
		[JsonProperty("item_status")]             public string         ItemStatusName         { get; set; }
		[JsonProperty("item_status_orig")]        public long           ItemStatusId           { get; set; }
		[JsonProperty("itemtype")]                public long           ItemType               { get; set; }
		[JsonProperty("name")]                    public string         Name                   { get; set; }
		[JsonProperty("period")]                  public long           PeriodId               { get; set; }
		[JsonProperty("pricelist")]               public string         PriceList              { get; set; }
		[JsonProperty("pricelist_id")]            public long           PriceListId            { get; set; }
		[JsonProperty("processingmodule_failed")] public OnOff          ProcessingmoduleFailed { get; set; }
		[JsonProperty("project")]                 public string         Project                { get; set; }
		[JsonProperty("real_expiredate")]         public DateTimeOffset RealExpiredate         { get; set; }
		[JsonProperty("specialstatus")]           public long           Specialstatus          { get; set; }
		[JsonProperty("stat_enabled")]            public OnOff          StatEnabled            { get; set; }
		[JsonProperty("status")]                  public long           Status                 { get; set; }
		[JsonProperty("subaccount")]              public long           Subaccount             { get; set; }
	}
}