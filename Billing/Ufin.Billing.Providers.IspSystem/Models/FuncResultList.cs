﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class FuncResultList<ListItem, ModelItem> : FuncResult<ModelItem>
	{
		[JsonProperty("page")] public PagingItem     Page  { get; set; }
		[JsonProperty("elem")] public List<ListItem> Items { get; set; }
		public static Dictionary<string, string> NoFilterMaxPage()
		{
			return new Dictionary<string, string>
			{
				{ "filter", $@"off"            },
				{ "p_num",  $@"1"              },
				{ "p_cnt",  $@"{int.MaxValue}" },
			};
		}
	}
	public class FuncResultList<ListItem> : FuncResultList<ListItem, ModelList> { }
	public class PagingItem
	{
		[JsonProperty("p_num")]   public long PageNum   { get; set; }
		[JsonProperty("p_cnt")]   public long PageSize  { get; set; }
		[JsonProperty("p_elems")] public long PageElems { get; set; }
	}
	public class ModelList
	{
		[JsonProperty("plid")]   public long?  Plid   { get; set; }
		[JsonProperty("plname")] public string Plname { get; set; }
	}
}