﻿using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class LocaleList : FuncResultList<LocaleItem> { }
	public class LocaleItem
	{
		[JsonProperty("active")]      public OnOff  Active      { get; set; }
		[JsonProperty("default_lang")]public OnOff  DefaultLang { get; set; }
		[JsonProperty("embedded")]    public OnOff  Embedded    { get; set; }
		[JsonProperty("id")]          public long   Id          { get; set; }
		[JsonProperty("langcode")]    public string Langcode    { get; set; }
		[JsonProperty("name")]        public string Name        { get; set; }
		[JsonProperty("processed")]   public OnOff  Processed   { get; set; }
	}
}