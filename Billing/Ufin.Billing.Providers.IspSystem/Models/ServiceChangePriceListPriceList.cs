﻿using System.Collections.Generic;
using System.Dynamic;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class ServiceChangePriceListPriceListResult : FuncResult<ExpandoObject>
	{
		[JsonProperty("slist")] public Slist List { get; set; }
		public class Slist
		{
			/// <summary> Перечень доступных для смены тарифных планов </summary>
			[JsonProperty("pricelist")] public List<ListItem> PriceLists { get; set; }
		}
		public class ListItem
		{
			/// <summary> Идентификатор тарифного плана, доступного для смены </summary>
			[JsonProperty("k")] public long   Id   { get; set; }
			/// <summary> Наименование тарифного плана, доступного для смены </summary>
			[JsonProperty("v")] public string Name { get; set; }
		}
	}
}