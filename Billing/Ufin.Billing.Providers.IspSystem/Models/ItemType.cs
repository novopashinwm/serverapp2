﻿using System.Dynamic;
using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class ItemTypeList : FuncResultList<ItemTypeItem, ExpandoObject> { }
	public class ItemTypeItem
	{
		[JsonProperty("embedded")]      public OnOff  Embedded      { get; set; }
		[JsonProperty("id")]            public long   Id            { get; set; }
		[JsonProperty("intname")]       public string Intname       { get; set; }
		[JsonProperty("name")]          public string Name          { get; set; }
		[JsonProperty("orderpriority")] public long   Orderpriority { get; set; }
	}
}