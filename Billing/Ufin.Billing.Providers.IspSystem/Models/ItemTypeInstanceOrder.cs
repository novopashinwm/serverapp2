﻿using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class ItemTypeInstanceOrderResult : ItemTypeInstanceEditResult
	{
		[JsonProperty("id")] public IdItem Id   { get; set; }
		public class IdItem
		{
			[JsonProperty("step")] public string Step { get; set; }
			[JsonProperty("v")]    public long   Id   { get; set; }
		}
	}
}