﻿using System.Dynamic;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class PaymentAddResult : FuncResult<ExpandoObject>
	{
		[JsonProperty("ok")] public OkItem Ok { get; set; }
		[JsonProperty("id")] public IdItem Id { get; set; }
		public class IdItem
		{
			[JsonProperty("saveoutput")] public string Saveoutput { get; set; }
			[JsonProperty("v")]          public long   Value      { get; set; }
		}
		public class OkItem
		{
			[JsonProperty("type")]       public string Type       { get; set; }
			[JsonProperty("v")]          public string Value      { get; set; }
		}
	}
	public class AddPaymentResult : FuncResult<AddPaymentModel> { }
	public class AddPaymentModel
	{
		[JsonProperty("payment.id")] public long PaymentId { get; set; }
	}
}