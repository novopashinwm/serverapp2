﻿using System;
using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class ProfileList : FuncResultList<ProfileItem> { }
	public class ProfileItem
	{
		[JsonProperty("account")]              public string    Account            { get; set; }
		[JsonProperty("id")]                   public long      Id                 { get; set; }
		[JsonProperty("name")]                 public string    Name               { get; set; }
		[JsonProperty("need_phone_validate")]  public OnOff     NeedPhoneValidate  { get; set; }
		[JsonProperty("pneed_activation")]     public OnOff     PneedActivation    { get; set; }
		[JsonProperty("pneed_manual_vetting")] public OnOff     PneedManualVetting { get; set; }
		[JsonProperty("profiletype")]          public string    Profiletype        { get; set; }
		[JsonProperty("profiletype_orig")]     public long?     ProfiletypeOrig    { get; set; }
		[JsonProperty("reconciliation")]       public OnOff     Reconciliation     { get; set; }
		[JsonProperty("registration_date")]    public DateTime? RegistrationDate   { get; set; }
	}
}