﻿using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class UserList : FuncResultList<UserItem> { }
	public class UserItem
	{
		[JsonProperty("account")]              public string Account            { get; set; }
		[JsonProperty("account_id")]           public long   AccountId          { get; set; }
		[JsonProperty("email")]                public string Email              { get; set; }
		[JsonProperty("enabled")]              public OnOff  Enabled            { get; set; }
		[JsonProperty("id")]                   public long   Id                 { get; set; }
		[JsonProperty("name")]                 public string Name               { get; set; }
		[JsonProperty("realname")]             public string Realname           { get; set; }
		[JsonProperty("self")]                 public OnOff  Self               { get; set; }
		[JsonProperty("default_access_allow")] public OnOff  DefaultAccessAllow { get; set; }
	}
}