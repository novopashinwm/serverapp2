﻿using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class PaymethodList : FuncResultList<PaymethodItem> { }
	public class PaymethodItem
	{
		[JsonProperty("active")]        public OnOff  Active        { get; set; }
		[JsonProperty("companies")]     public string Companies     { get; set; }
		[JsonProperty("currency_name")] public string CurrencyName  { get; set; }
		[JsonProperty("id")]            public long   Id            { get; set; }
		[JsonProperty("module")]        public string Module        { get; set; }
		[JsonProperty("module_orig")]   public string ModuleOrig    { get; set; }
		[JsonProperty("name")]          public string Name          { get; set; }
		[JsonProperty("orderpriority")] public long   Orderpriority { get; set; }
		[JsonProperty("projects")]      public string Projects      { get; set; }
	}
}