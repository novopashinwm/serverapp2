﻿using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class MeasureList : FuncResultList<MeasureItem> { }
	public class MeasureItem
	{
		[JsonProperty("id")]   public long   Id   { get; set; }
		[JsonProperty("name")] public string Name { get; set; }
	}
}