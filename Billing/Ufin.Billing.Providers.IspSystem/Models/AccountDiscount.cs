﻿using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class AccountDiscountList : FuncResultList<AccountDiscountItem> { }
	public class AccountDiscountItem
	{
		[JsonProperty("actiontype")]         public string Actiontype       { get; set; }
		[JsonProperty("actiontype_orig")]    public long   ActiontypeOrig   { get; set; }
		[JsonProperty("discount_type")]      public string DiscountType     { get; set; }
		[JsonProperty("discount_type_orig")] public string DiscountTypeOrig { get; set; }
		[JsonProperty("expired")]            public OnOff  Expired          { get; set; }
		[JsonProperty("id")]                 public long   Id               { get; set; }
		[JsonProperty("idname")]             public string Idname           { get; set; }
		[JsonProperty("name")]               public string Name             { get; set; }
		[JsonProperty("period")]             public string Period           { get; set; }
		[JsonProperty("project")]            public string Project          { get; set; }
		[JsonProperty("have_price")]         public string HavePrice        { get; set; }
		[JsonProperty("value")]              public string Value            { get; set; }
	}
}