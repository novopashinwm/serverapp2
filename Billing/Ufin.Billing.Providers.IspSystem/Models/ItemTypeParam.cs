﻿using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class ItemTypeParamList : FuncResultList<ItemTypeParamItem> { }
	public class ItemTypeParamItem
	{
		[JsonProperty("crypted")]         public OnOff  Crypted         { get; set; }
		[JsonProperty("description")]     public string Description     { get; set; }
		[JsonProperty("description_ru")]  public string DescriptionRu   { get; set; }
		[JsonProperty("editreadlevel")]   public long   Editreadlevel   { get; set; }
		[JsonProperty("editvisible")]     public OnOff  Editvisible     { get; set; }
		[JsonProperty("editwritelevel")]  public long   Editwritelevel  { get; set; }
		[JsonProperty("embedded")]        public OnOff  Embedded        { get; set; }
		[JsonProperty("hint")]            public string Hint            { get; set; }
		[JsonProperty("hint_ru")]         public string HintRu          { get; set; }
		[JsonProperty("id")]              public long   Id              { get; set; }
		[JsonProperty("info")]            public string Info            { get; set; }
		[JsonProperty("info_ru")]         public string InfoRu          { get; set; }
		[JsonProperty("inputtype")]       public long   Inputtype       { get; set; }
		[JsonProperty("intname")]         public string Intname         { get; set; }
		[JsonProperty("itemtype")]        public long   Itemtype        { get; set; }
		[JsonProperty("listvisible")]     public OnOff  Listvisible     { get; set; }
		[JsonProperty("locale_name")]     public string LocaleName      { get; set; }
		[JsonProperty("name")]            public string Name            { get; set; }
		[JsonProperty("name_ru")]         public string NameRu          { get; set; }
		[JsonProperty("needmodulecheck")] public OnOff  Needmodulecheck { get; set; }
		[JsonProperty("needsetparam")]    public OnOff  Needsetparam    { get; set; }
		[JsonProperty("nogroupedit")]     public OnOff  Nogroupedit     { get; set; }
		[JsonProperty("openvisible")]     public OnOff  Openvisible     { get; set; }
		[JsonProperty("orderpriority")]   public long   Orderpriority   { get; set; }
		[JsonProperty("ordervisible")]    public OnOff  Ordervisible    { get; set; }
		[JsonProperty("paramgroup")]      public long?  Paramgroup      { get; set; }
		[JsonProperty("required")]        public OnOff  ElemRequired    { get; set; }
		[JsonProperty("selecttype")]      public string Selecttype      { get; set; }
		[JsonProperty("tune_pricelists")] public OnOff  TunePricelists  { get; set; }
		[JsonProperty("validator")]       public string Validator       { get; set; }
		[JsonProperty("zoom")]            public OnOff  Zoom            { get; set; }
	}
}