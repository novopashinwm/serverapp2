﻿using System.Dynamic;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class ItemTypeInstanceEditResult : FuncResult<ExpandoObject>
	{
		[JsonProperty("ok")] public bool? IsOk { get; set; }
	}
}