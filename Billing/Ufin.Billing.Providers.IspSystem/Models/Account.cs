﻿using System;
using System.Dynamic;
using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class RegisterAddSimpleResult : FuncResult<ExpandoObject>
	{
		[JsonProperty("ok")] public OkItem Ok { get; set; }
		public class OkItem
		{
			[JsonProperty("type")] public string Type  { get; set; }
			[JsonProperty("v")]    public string Value { get; set; }
		}
	}
	public class AccountAddSimpleResult : FuncResult<ExpandoObject>
	{
		[JsonProperty("ok")] public bool? IsOk { get; set; }
		[JsonProperty("id")] public long? Id   { get; set; }
	}
	public class AccountList : FuncResultList<AccountItem> { }
	public class AccountItem
	{
		[JsonProperty("abuse_rate")]              public string          AbuseRate             { get; set; }
		[JsonProperty("account_status")]          public string          AccountStatus         { get; set; }
		[JsonProperty("account_status_orig")]     public long            AccountStatusOrig     { get; set; }
		[JsonProperty("allowdeleteitem")]         public OnOff           Allowdeleteitem       { get; set; }
		[JsonProperty("allowselfview")]           public OnOff           Allowselfview         { get; set; }
		[JsonProperty("allowsuspenditem")]        public OnOff           Allowsuspenditem      { get; set; }
		[JsonProperty("attitude")]                public string          Attitude              { get; set; }
		[JsonProperty("balance")]                 public string          Balance               { get; set; }
		[JsonProperty("balance_summary")]         public decimal         BalanceSummary        { get; set; }
		[JsonProperty("conversion_label_userid")] public OnOff           ConversionLabelUserid { get; set; }
		[JsonProperty("country")]                 public long            Country               { get; set; }
		[JsonProperty("enterprise")]              public OnOff           Enterprise            { get; set; }
		[JsonProperty("has_discount")]            public OnOff           HasDiscount           { get; set; }
		[JsonProperty("id")]                      public long            Id                    { get; set; }
		[JsonProperty("ignoreabuserate")]         public OnOff           Ignoreabuserate       { get; set; }
		[JsonProperty("ignoreitemmax")]           public OnOff           Ignoreitemmax         { get; set; }
		[JsonProperty("level")]                   public long            Level                 { get; set; }
		[JsonProperty("name")]                    public string          Name                  { get; set; }
		[JsonProperty("need_activation")]         public OnOff           NeedActivation        { get; set; }
		[JsonProperty("need_manual_vetting")]     public OnOff           NeedManualVetting     { get; set; }
		[JsonProperty("nocalcstat")]              public OnOff           Nocalcstat            { get; set; }
		[JsonProperty("paid_item")]               public long            PaidItem              { get; set; }
		[JsonProperty("paid_payment_count")]      public long            PaidPaymentCount      { get; set; }
		[JsonProperty("registration_date")]       public DateTimeOffset? RegistrationDate      { get; set; }
		[JsonProperty("taxcustompay")]            public OnOff           Taxcustompay          { get; set; }
		[JsonProperty("taxexclusive")]            public OnOff           Taxexclusive          { get; set; }
		[JsonProperty("trial_item")]              public long            TrialItem             { get; set; }
		[JsonProperty("tz_registration_date")]    public DateTimeOffset? TzRegistrationDate    { get; set; }
		[JsonProperty("conversion_label")]        public string          ConversionLabel       { get; set; }
		[JsonProperty("registration_ip")]         public string          RegistrationIp        { get; set; }
		[JsonProperty("valid_phone")]             public string          ValidPhone            { get; set; }
		[JsonProperty("note")]                    public string          Note                  { get; set; }
	}
}