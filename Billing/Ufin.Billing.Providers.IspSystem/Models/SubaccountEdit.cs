﻿using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class SubaccountEdit : FuncResult<SubaccountEditModel> { }
	public class SubaccountEditModel
	{
		[JsonProperty("active")]         public OnOff   Active        { get; set; }
		[JsonProperty("allowpostpaid")]  public OnOff   AllowPostPaid { get; set; }
		[JsonProperty("balance")]        public decimal Balance       { get; set; }
		[JsonProperty("creditlimit")]    public decimal CreditLimit   { get; set; }
		[JsonProperty("currency")]       public long    CurrencyId    { get; set; }
		[JsonProperty("elid")]           public long    Elid          { get; set; }
		[JsonProperty("id")]             public long    Id            { get; set; }
		[JsonProperty("incident_limit")] public string  IncidentLimit { get; set; }
		[JsonProperty("project")]        public long    ProjectId     { get; set; }
		[JsonProperty("project_name")]   public string  ProjectName   { get; set; }
		[JsonProperty("referer")]        public string  Referer       { get; set; }
	}
}