﻿using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;
namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class AccountProjectList : FuncResultList<AccountProjectItem> { }
	public class AccountProjectItem
	{
		[JsonProperty("active")] public OnOff  Active { get; set; }
		[JsonProperty("id")]     public long   Id     { get; set; }
		[JsonProperty("name")]   public string Name   { get; set; }
	}
}