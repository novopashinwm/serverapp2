﻿using System.Dynamic;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class PaymentSuResult : FuncResult<ExpandoObject>
	{
		[JsonProperty("ok")]   public OkItem Ok   { get; set; }
		[JsonProperty("auth")] public string Auth { get; set; }
		public class OkItem
		{
			[JsonProperty("type")] public string Type { get; set; }
		}
	}
}