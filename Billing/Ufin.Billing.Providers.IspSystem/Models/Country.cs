﻿using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class CountryList : FuncResultList<CountryItem> { }
	public class CountryItem
	{
		[JsonProperty("active")]      public OnOff  Active      { get; set; }
		[JsonProperty("id")]          public long   Id          { get; set; }
		[JsonProperty("iso2")]        public string Iso2        { get; set; }
		[JsonProperty("locale_name")] public string LocaleName  { get; set; }
		[JsonProperty("name")]        public string Name        { get; set; }
		[JsonProperty("name_ru")]     public string NameRu      { get; set; }
		[JsonProperty("phonecode")]   public long   Phonecode   { get; set; }
		[JsonProperty("phoneformat")] public string Phoneformat { get; set; }
	}
}