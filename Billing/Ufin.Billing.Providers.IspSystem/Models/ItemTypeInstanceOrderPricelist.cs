﻿using System.Collections.Generic;
using System.Dynamic;
using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class ItemTypeInstanceOrderPricelist : FuncResult<ExpandoObject>
	{
		[JsonProperty("list")]
		public ListNode List { get; set; }
		public class ListNode
		{
			[JsonProperty("tariflist")]
			public List<TarifList> TariffLists { get; set; }
		}
		public class TarifList
		{
			[JsonProperty("action")]          public string Action         { get; set; }
			[JsonProperty("desc")]            public string Desc           { get; set; }
			[JsonProperty("image")]           public string Image          { get; set; }
			[JsonProperty("order_available")] public OnOff  OrderAvailable { get; set; }
			[JsonProperty("price")]           public string Price          { get; set; }
			[JsonProperty("pricelist")]       public long   Pricelist      { get; set; }
		}
	}
}