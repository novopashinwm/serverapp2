﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class PriceListValues : FuncResult<PriceListValuesModel> { }
	public class PriceListValuesModel
	{
		[JsonProperty("cost")]         public decimal                    Cost        { get; set; }
		[JsonProperty("cost_details")] public PriceListValuesCostDetails CostDetails { get; set; }
	}
	public class PriceListValuesCostDetails
	{
		[JsonProperty("main")]  public PriceListMainValues       MainValues  { get; set; }
		[JsonProperty("addon")] public List<PriceListAddonValue> AddonValues { get; set; }
	}
	public class PriceListAddonValue
	{
		[JsonProperty("addonlimit")]         public string  AddonLimit        { get; set; }
		[JsonProperty("addonvalue")]         public string  AddonValue        { get; set; }
		[JsonProperty("amount")]             public decimal Amount            { get; set; }
		[JsonProperty("cost")]               public decimal Cost              { get; set; }
		[JsonProperty("currency_str")]       public string  CurrencyStr       { get; set; }
		[JsonProperty("desc")]               public string  Desc              { get; set; }
		[JsonProperty("discount")]           public decimal Discount          { get; set; }
		[JsonProperty("discount_amount")]    public decimal DiscountAmount    { get; set; }
		[JsonProperty("discount_setup")]     public decimal DiscountSetup     { get; set; }
		[JsonProperty("id")]                 public long    Id                { get; set; }
		[JsonProperty("itemtype")]           public string  ItemType          { get; set; }
		[JsonProperty("measure")]            public string  Measure           { get; set; }
		[JsonProperty("name")]               public string  Name              { get; set; }
		[JsonProperty("paramgroup_intname")] public string  ParamgroupIntname { get; set; }
		[JsonProperty("period_cost")]        public decimal PeriodCost        { get; set; }
		[JsonProperty("setup")]              public decimal Setup             { get; set; }
		[JsonProperty("value")]              public string  Value             { get; set; }
	}
	public class PriceListMainValues
	{
		[JsonProperty("amount")]          public decimal Amount         { get; set; }
		[JsonProperty("cost")]            public decimal Cost           { get; set; }
		[JsonProperty("currency_str")]    public string  CurrencyStr    { get; set; }
		[JsonProperty("desc")]            public string  Desc           { get; set; }
		[JsonProperty("discount")]        public decimal Discount       { get; set; }
		[JsonProperty("discount_amount")] public decimal DiscountAmount { get; set; }
		[JsonProperty("discount_setup")]  public decimal DiscountSetup  { get; set; }
		[JsonProperty("name")]            public string  Name           { get; set; }
		[JsonProperty("period_cost")]     public decimal PeriodCost     { get; set; }
		[JsonProperty("setup")]           public decimal Setup          { get; set; }
	}
}