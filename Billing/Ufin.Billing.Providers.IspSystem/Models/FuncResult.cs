﻿using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class FuncResult<ModelItem>
	{
		[JsonProperty("func")]  public string     Func  { get; set; }
		[JsonProperty("error")] public ErrorItem  Error { get; set; }
		[JsonProperty("model")] public ModelItem  Model { get; set; }
	}
	public class ErrorItem
	{
		[JsonProperty("type")]   public string Type   { get; set; }
		[JsonProperty("object")] public string Object { get; set; }
		[JsonProperty("msg")]    public string Msg    { get; set; }
		[JsonProperty("level")]  public long?  Level  { get; set; }
	}
}