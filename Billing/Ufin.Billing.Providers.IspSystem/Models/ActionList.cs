﻿using Newtonsoft.Json;
namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class ActionList : FuncResultList<ActionItem> { }
	public partial class ActionItem
	{
		[JsonProperty("access")]   public string Access   { get; set; }
		[JsonProperty("action")]   public string Action   { get; set; }
		[JsonProperty("mgr")]      public string Mgr      { get; set; }
		[JsonProperty("type")]     public string Type     { get; set; }
		[JsonProperty("titlemsg")] public string Titlemsg { get; set; }
		[JsonProperty("hint")]     public string Hint     { get; set; }
		[JsonProperty("short")]    public string Short    { get; set; }
	}
}