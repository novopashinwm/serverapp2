﻿using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class PriceListAddonList : FuncResultList<PriceListAddonItem> { }
	public class PriceListAddonItem
	{
		[JsonProperty("active")]               public OnOff  Active               { get; set; }
		[JsonProperty("addontype")]            public long   Addontype            { get; set; }
		[JsonProperty("billtype")]             public long   Billtype             { get; set; }
		[JsonProperty("id")]                   public long   Id                   { get; set; }
		[JsonProperty("manualprocessing")]     public OnOff  Manualprocessing     { get; set; }
		[JsonProperty("name")]                 public string Name                 { get; set; }
		[JsonProperty("orderpriority")]        public long   Orderpriority        { get; set; }
		[JsonProperty("price")]                public string Price                { get; set; }
		[JsonProperty("restrictclientchange")] public OnOff  Restrictclientchange { get; set; }
	}
}