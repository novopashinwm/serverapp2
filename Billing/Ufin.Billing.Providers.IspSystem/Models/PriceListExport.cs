﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class PriceListExportList : FuncResult<ExpandoObject>
	{
		[JsonProperty("pricelist")] public List<PriceListExportItem> Items { get; set; }
	}
	public class PriceListExportItem
	{
		[JsonProperty("access")]
		public long Access { get; set; }
		[JsonProperty("active")]
		public OnOff Active { get; set; }
		[JsonProperty("addon")]
		public List<PriceListExportAddon> Addons { get; set; }
		///[JsonProperty("addonbool")]
		///public OnOff Addonbool { get; set; }
		///[JsonProperty("addonlimit")]
		///public string Addonlimit { get; set; }
		///[JsonProperty("addonlimittrial")]
		///public string Addonlimittrial { get; set; }
		///[JsonProperty("addonmax")]
		///public string Addonmax { get; set; }
		///[JsonProperty("addonmaxtrial")]
		///public string Addonmaxtrial { get; set; }
		///[JsonProperty("addonmin")]
		///public string Addonmin { get; set; }
		///[JsonProperty("addonstatcalculation")]
		///public string Addonstatcalculation { get; set; }
		///[JsonProperty("addonstatchargestoped")]
		///public OnOff  Addonstatchargestoped { get; set; }
		///[JsonProperty("addonstatcomparison")]
		///public string Addonstatcomparison { get; set; }
		///[JsonProperty("addonstattype")]
		///public string Addonstattype { get; set; }
		///[JsonProperty("addonstep")]
		///public string Addonstep { get; set; }
		///[JsonProperty("addontype")]
		///public string Addontype { get; set; }
		///[JsonProperty("allownosuitable")]
		///public OnOff Allownosuitable { get; set; }
		///[JsonProperty("allowpostpaid")]
		///public OnOff Allowpostpaid { get; set; }
		///[JsonProperty("autocalcday")]
		///public OnOff Autocalcday { get; set; }
		///[JsonProperty("billdaily")]
		///public OnOff Billdaily { get; set; }
		///[JsonProperty("billhourly")]
		///public OnOff Billhourly { get; set; }
		///[JsonProperty("billprorata")]
		///public OnOff Billprorata { get; set; }
		///[JsonProperty("billtype")]
		///public string Billtype { get; set; }
		///[JsonProperty("changepolicy")]
		///public long Changepolicy { get; set; }
		///[JsonProperty("changeprolongpolicy")]
		///public long Changeprolongpolicy { get; set; }
		///[JsonProperty("chargestoped")]
		///public OnOff Chargestoped { get; set; }
		///[JsonProperty("compound")]
		///public string Compound { get; set; }
		///[JsonProperty("datacenter")]
		///public List<Datacenter> Datacenters { get; set; }
		[JsonProperty("description")]
		public string Description { get; set; }
		///[JsonProperty("description_markdown")]
		///public string DescriptionMarkdown { get; set; }
		///[JsonProperty("description_markdown_ru")]
		///public string DescriptionMarkdownRu { get; set; }
		[JsonProperty("description_ru")]
		public string DescriptionRu { get; set; }
		///[JsonProperty("enumeration")]
		///public string Enumeration { get; set; }
		///[JsonProperty("enumerationitem")]
		///public string Enumerationitem { get; set; }
		///[JsonProperty("forcecontractprint")]
		///public OnOff Forcecontractprint { get; set; }
		[JsonProperty("id")]
		public long Id { get; set; }
		[JsonProperty("intname")]
		public string Intname { get; set; }
		///[JsonProperty("isgroup")]
		///public OnOff Isgroup { get; set; }
		///[JsonProperty("itemmax")]
		///public string Itemmax { get; set; }
		[JsonProperty("itemtype")]
		public long ItemType { get; set; }
		///[JsonProperty("itemtype_info")]
		///public ItemTypeInfo ItemTypeInfo { get; set; }
		[JsonProperty("itemtypeparam")]
		public List<ItemTypeParam> ItemTypeParams { get; set; }
		///[JsonProperty("label")]
		///public string Label { get; set; }
		///[JsonProperty("license")]
		///public string License { get; set; }
		///[JsonProperty("manualname")]
		///public OnOff Manualname { get; set; }
		///[JsonProperty("manualprocessing")]
		///public OnOff Manualprocessing { get; set; }
		///[JsonProperty("manualprocessing_skipdefault")]
		///public OnOff ManualprocessingSkipdefault { get; set; }
		///[JsonProperty("measure")]
		///public string Measure { get; set; }
		///[JsonProperty("minperiodlen")]
		///public long Minperiodlen { get; set; }
		///[JsonProperty("minperiodtype")]
		///public long Minperiodtype { get; set; }
		[JsonProperty("name")]
		public string Name { get; set; }
		[JsonProperty("name_ru")]
		public string NameRu { get; set; }
		///[JsonProperty("nostopholidays")]
		///public string Nostopholidays { get; set; }
		///[JsonProperty("note")]
		///public string Note { get; set; }
		///[JsonProperty("opennotify")]
		///public string Opennotify { get; set; }
		[JsonProperty("orderimage")]
		public string Orderimage { get; set; }
		///[JsonProperty("orderlink")]
		///public Uri Orderlink { get; set; }
		///[JsonProperty("orderpolicy")]
		///public long Orderpolicy { get; set; }
		///[JsonProperty("orderpriority")]
		///public long Orderpriority { get; set; }
		///[JsonProperty("parent")]
		///public string Parent { get; set; }
		///[JsonProperty("payment_object")]
		///public long PaymentObject { get; set; }
		[JsonProperty("price")]
		public Price Price { get; set; }
		///[JsonProperty("pricelist_group")]
		///public string PricelistGroup { get; set; }
		///[JsonProperty("processing_feature")]
		///public string ProcessingFeature { get; set; }
		///[JsonProperty("project")]
		///public long Project { get; set; }
		///[JsonProperty("prorataday")]
		///public long Prorataday { get; set; }
		///[JsonProperty("quickorder")]
		///public OnOff Quickorder { get; set; }
		///[JsonProperty("remoteid")]
		///public string Remoteid { get; set; }
		///[JsonProperty("requery_phone")]
		///public OnOff RequeryPhone { get; set; }
		///[JsonProperty("restrictclientchange")]
		///public OnOff Restrictclientchange { get; set; }
		///[JsonProperty("returnpolicy")]
		///public long Returnpolicy { get; set; }
		///[JsonProperty("roundtype")]
		///public long Roundtype { get; set; }
		///[JsonProperty("scaletype")]
		///public string Scaletype { get; set; }
		///[JsonProperty("selecttype")]
		///public string Selecttype { get; set; }
		///[JsonProperty("show_addon_image")]
		///public string ShowAddonImage { get; set; }
		///[JsonProperty("show_on_dashboard")]
		///public OnOff ShowOnDashboard { get; set; }
		///[JsonProperty("siteinfo")]
		///public string Siteinfo { get; set; }
		///[JsonProperty("summarizeinvoice")]
		///public OnOff Summarizeinvoice { get; set; }
		///[JsonProperty("suspendpenaltypercent")]
		///public string Suspendpenaltypercent { get; set; }
		///[JsonProperty("suspendpenaltytype")]
		///public long Suspendpenaltytype { get; set; }
		///[JsonProperty("suspendperiod")]
		///public string Suspendperiod { get; set; }
		///[JsonProperty("trial")]
		///public string Trial { get; set; }
		///[JsonProperty("trial_itemmax")]
		///public string TrialItemmax { get; set; }
	}
	public class PriceListExportAddon
	{
		[JsonProperty("access")]
		public string Access { get; set; }
		[JsonProperty("active")]
		public OnOff Active { get; set; }
		[JsonProperty("addon_itemtype_info")]
		public ItemTypeInfo AddonItemtypeInfo { get; set; }
		[JsonProperty("addonbool")]
		public OnOff Addonbool { get; set; }
		[JsonProperty("addonlimit")]
		public long? Addonlimit { get; set; }
		[JsonProperty("addonlimittrial")]
		public long? Addonlimittrial { get; set; }
		[JsonProperty("addonmax")]
		public long? Addonmax { get; set; }
		[JsonProperty("addonmaxtrial")]
		public long? Addonmaxtrial { get; set; }
		[JsonProperty("addonmin")]
		public long? Addonmin { get; set; }
		[JsonProperty("addonstatcalculation")]
		public string Addonstatcalculation { get; set; }
		[JsonProperty("addonstatchargestoped")]
		public OnOff Addonstatchargestoped { get; set; }
		[JsonProperty("addonstatcomparison")]
		public string Addonstatcomparison { get; set; }
		[JsonProperty("addonstattype")]
		public string Addonstattype { get; set; }
		[JsonProperty("addonstep")]
		public long? Addonstep { get; set; }
		[JsonProperty("addontype")]
		public long Addontype { get; set; }
		[JsonProperty("allownosuitable")]
		public OnOff Allownosuitable { get; set; }
		[JsonProperty("allowpostpaid")]
		public OnOff Allowpostpaid { get; set; }
		[JsonProperty("autocalcday")]
		public OnOff Autocalcday { get; set; }
		[JsonProperty("billdaily")]
		public OnOff Billdaily { get; set; }
		[JsonProperty("billhourly")]
		public OnOff Billhourly { get; set; }
		[JsonProperty("billprorata")]
		public OnOff Billprorata { get; set; }
		[JsonProperty("billtype")]
		public long Billtype { get; set; }
		[JsonProperty("changepolicy")]
		public string Changepolicy { get; set; }
		[JsonProperty("changeprolongpolicy")]
		public long Changeprolongpolicy { get; set; }
		[JsonProperty("chargestoped")]
		public OnOff Chargestoped { get; set; }
		[JsonProperty("compound")]
		public string Compound { get; set; }
		[JsonProperty("description")]
		public string Description { get; set; }
		[JsonProperty("description_markdown")]
		public string DescriptionMarkdown { get; set; }
		[JsonProperty("description_markdown_ru")]
		public string DescriptionMarkdownRu { get; set; }
		[JsonProperty("description_ru")]
		public string DescriptionRu { get; set; }
		[JsonProperty("enumeration")]
		public string Enumeration { get; set; }
		[JsonProperty("enumerationitem")]
		public string Enumerationitem { get; set; }
		[JsonProperty("forcecontractprint")]
		public OnOff Forcecontractprint { get; set; }
		[JsonProperty("id")]
		public long Id { get; set; }
		[JsonProperty("intname")]
		public string Intname { get; set; }
		[JsonProperty("isgroup")]
		public OnOff Isgroup { get; set; }
		[JsonProperty("itemmax")]
		public string Itemmax { get; set; }
		[JsonProperty("itemtype")]
		public long Itemtype { get; set; }
		[JsonProperty("label")]
		public string Label { get; set; }
		[JsonProperty("license")]
		public string License { get; set; }
		[JsonProperty("manualname")]
		public OnOff Manualname { get; set; }
		[JsonProperty("manualprocessing")]
		public OnOff Manualprocessing { get; set; }
		[JsonProperty("manualprocessing_skipdefault")]
		public OnOff ManualprocessingSkipdefault { get; set; }
		[JsonProperty("measure")]
		public long? Measure { get; set; }
		[JsonProperty("minperiodlen")]
		public string Minperiodlen { get; set; }
		[JsonProperty("minperiodtype")]
		public string Minperiodtype { get; set; }
		[JsonProperty("name")]
		public string Name { get; set; }
		[JsonProperty("name_ru")]
		public string NameRu { get; set; }
		[JsonProperty("nostopholidays")]
		public string Nostopholidays { get; set; }
		[JsonProperty("note")]
		public string Note { get; set; }
		[JsonProperty("opennotify")]
		public string Opennotify { get; set; }
		[JsonProperty("orderimage")]
		public string Orderimage { get; set; }
		[JsonProperty("orderpolicy")]
		public long Orderpolicy { get; set; }
		[JsonProperty("orderpriority")]
		public long Orderpriority { get; set; }
		[JsonProperty("parent")]
		public long Parent { get; set; }
		[JsonProperty("payment_object")]
		public long PaymentObject { get; set; }
		[JsonProperty("price")]
		public Price Price { get; set; }
		[JsonProperty("pricelist_group")]
		public string PricelistGroup { get; set; }
		[JsonProperty("processing_feature")]
		public string ProcessingFeature { get; set; }
		[JsonProperty("project")]
		public long Project { get; set; }
		[JsonProperty("prorataday")]
		public long Prorataday { get; set; }
		[JsonProperty("quickorder")]
		public OnOff Quickorder { get; set; }
		[JsonProperty("remoteid")]
		public string Remoteid { get; set; }
		[JsonProperty("requery_phone")]
		public OnOff RequeryPhone { get; set; }
		[JsonProperty("restrictclientchange")]
		public OnOff Restrictclientchange { get; set; }
		[JsonProperty("returnpolicy")]
		public long Returnpolicy { get; set; }
		[JsonProperty("roundtype")]
		public long Roundtype { get; set; }
		[JsonProperty("scaletype")]
		public string Scaletype { get; set; }
		[JsonProperty("selecttype")]
		public string Selecttype { get; set; }
		[JsonProperty("show_addon_image")]
		public string ShowAddonImage { get; set; }
		[JsonProperty("siteinfo")]
		public string Siteinfo { get; set; }
		[JsonProperty("summarizeinvoice")]
		public OnOff Summarizeinvoice { get; set; }
		[JsonProperty("suspendpenaltypercent")]
		public string Suspendpenaltypercent { get; set; }
		[JsonProperty("suspendpenaltytype")]
		public long Suspendpenaltytype { get; set; }
		[JsonProperty("suspendperiod")]
		public string Suspendperiod { get; set; }
		[JsonProperty("trial")]
		public string Trial { get; set; }
		[JsonProperty("trial_itemmax")]
		public string TrialItemmax { get; set; }
	}
	public class ItemTypeInfo
	{
		[JsonProperty("annually")]
		public OnOff Annually { get; set; }
		[JsonProperty("biennial")]
		public OnOff Biennial { get; set; }
		[JsonProperty("closesubtype")]
		public long Closesubtype { get; set; }
		[JsonProperty("closetype")]
		public long Closetype { get; set; }
		[JsonProperty("day")]
		public OnOff Day { get; set; }
		[JsonProperty("decennial")]
		public OnOff Decennial { get; set; }
		[JsonProperty("description")]
		public string Description { get; set; }
		[JsonProperty("description_ru")]
		public string DescriptionRu { get; set; }
		[JsonProperty("embedded")]
		public OnOff Embedded { get; set; }
		[JsonProperty("id")]
		public long Id { get; set; }
		[JsonProperty("intname")]
		public string Intname { get; set; }
		[JsonProperty("itemname")]
		public string Itemname { get; set; }
		[JsonProperty("lifetime")]
		public OnOff Lifetime { get; set; }
		[JsonProperty("listimage")]
		public string Listimage { get; set; }
		[JsonProperty("monthly")]
		public OnOff Monthly { get; set; }
		[JsonProperty("name")]
		public string Name { get; set; }
		[JsonProperty("name_ru")]
		public string NameRu { get; set; }
		[JsonProperty("nostopholidays")]
		public OnOff Nostopholidays { get; set; }
		[JsonProperty("open_duration_limit")]
		public string OpenDurationLimit { get; set; }
		[JsonProperty("orderimage")]
		public string Orderimage { get; set; }
		[JsonProperty("orderpriority")]
		public long Orderpriority { get; set; }
		[JsonProperty("orderview")]
		public long Orderview { get; set; }
		[JsonProperty("paramgroup")]
		public string Paramgroup { get; set; }
		[JsonProperty("parent")]
		public string Parent { get; set; }
		[JsonProperty("payment_object")]
		public long PaymentObject { get; set; }
		[JsonProperty("quadrennial")]
		public OnOff Quadrennial { get; set; }
		[JsonProperty("quarterly")]
		public OnOff Quarterly { get; set; }
		[JsonProperty("quinquennial")]
		public OnOff Quinquennial { get; set; }
		[JsonProperty("semiannual")]
		public OnOff Semiannual { get; set; }
		[JsonProperty("show_addon_image")]
		public OnOff ShowAddonImage { get; set; }
		[JsonProperty("showoninfo")]
		public OnOff Showoninfo { get; set; }
		[JsonProperty("splitexpense")]
		public OnOff Splitexpense { get; set; }
		[JsonProperty("statparam")]
		public string Statparam { get; set; }
		[JsonProperty("suspendperiod")]
		public string Suspendperiod { get; set; }
		[JsonProperty("transfer")]
		public OnOff Transfer { get; set; }
		[JsonProperty("trial")]
		public OnOff Trial { get; set; }
		[JsonProperty("trialtype")]
		public long Trialtype { get; set; }
		[JsonProperty("triennial")]
		public OnOff Triennial { get; set; }
	}
	public class Datacenter
	{
		[JsonProperty("id")]            public long   Id            { get; set; }
		[JsonProperty("name")]          public string Name          { get; set; }
		[JsonProperty("name_ru")]       public string NameRu        { get; set; }
		[JsonProperty("orderpriority")] public long   Orderpriority { get; set; }
	}
	public class ItemTypeParam
	{
		[JsonProperty("check_range")]     public string CheckRange            { get; set; }
		[JsonProperty("crypted")]         public OnOff  Crypted               { get; set; }
		[JsonProperty("description")]     public string Description           { get; set; }
		[JsonProperty("description_ru")]  public string DescriptionRu         { get; set; }
		[JsonProperty("editreadlevel")]   public long   Editreadlevel         { get; set; }
		[JsonProperty("editvisible")]     public OnOff  Editvisible           { get; set; }
		[JsonProperty("editwritelevel")]  public long   Editwritelevel        { get; set; }
		[JsonProperty("embedded")]        public OnOff  Embedded              { get; set; }
		[JsonProperty("hint")]            public string Hint                  { get; set; }
		[JsonProperty("hint_ru")]         public string HintRu                { get; set; }
		[JsonProperty("id")]              public long   Id                    { get; set; }
		[JsonProperty("info")]            public string Info                  { get; set; }
		[JsonProperty("info_ru")]         public string InfoRu                { get; set; }
		[JsonProperty("inputlength")]     public string Inputlength           { get; set; }
		[JsonProperty("inputrows")]       public string Inputrows             { get; set; }
		[JsonProperty("inputtype")]       public long   Inputtype             { get; set; }
		[JsonProperty("intname")]         public string Intname               { get; set; }
		[JsonProperty("itemtype")]        public long   Itemtype              { get; set; }
		[JsonProperty("listvisible")]     public OnOff  Listvisible           { get; set; }
		[JsonProperty("name")]            public string Name                  { get; set; }
		[JsonProperty("name_ru")]         public string NameRu                { get; set; }
		[JsonProperty("needmodulecheck")] public OnOff  Needmodulecheck       { get; set; }
		[JsonProperty("needsetparam")]    public OnOff  Needsetparam          { get; set; }
		[JsonProperty("nogroupedit")]     public OnOff  Nogroupedit           { get; set; }
		[JsonProperty("openvisible")]     public OnOff  Openvisible           { get; set; }
		[JsonProperty("orderpriority")]   public long   Orderpriority         { get; set; }
		[JsonProperty("ordervisible")]    public OnOff  Ordervisible          { get; set; }
		[JsonProperty("paramgroup")]      public string Paramgroup            { get; set; }
		[JsonProperty("required")]        public OnOff  ItemtypeparamRequired { get; set; }
		[JsonProperty("selecttype")]      public string Selecttype            { get; set; }
		[JsonProperty("tune_pricelists")] public OnOff  TunePricelists        { get; set; }
		[JsonProperty("validator")]       public string Validator             { get; set; }
		[JsonProperty("zoom")]            public OnOff  Zoom                  { get; set; }
	}
	public class Price
	{
		[JsonProperty("currency")] public string            Currency { get; set; }
		[JsonProperty("period")]   public List<PeriodPrice> Periods  { get; set; }
		[JsonProperty("setup")]    public SetupPrice        Setup    { get; set; }
	}
	public class PeriodPrice
	{
		[JsonProperty("is_json_array")] public string   IsJsonArray { get; set; }
		[JsonProperty("cost")]          public decimal? Cost        { get; set; }
		[JsonProperty("prolong_cost")]  public decimal? ProlongCost { get; set; }
		[JsonProperty("type")]          public string   Type        { get; set; }
		[JsonProperty("length")]        public long     Length      { get; set; }
		[JsonProperty("orderlink")]     public Uri      Orderlink   { get; set; }
		[JsonProperty("v")]             public string   V           { get; set; }
	}
	public class SetupPrice
	{
		[JsonProperty("cost")] public decimal? Cost { get; set; }
		[JsonProperty("type")] public string   Type { get; set; }
	}
}