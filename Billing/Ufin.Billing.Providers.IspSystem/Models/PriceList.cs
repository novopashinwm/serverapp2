﻿using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class PriceListList : FuncResultList<PriceListItem> { }
	public class PriceListItem
	{
		[JsonProperty("access")]                 public long   Access                { get; set; }
		[JsonProperty("accessname")]             public string Accessname            { get; set; }
		[JsonProperty("active")]                 public OnOff  Active                { get; set; }
		[JsonProperty("billdaily")]              public OnOff  Billdaily             { get; set; }
		[JsonProperty("has_details")]            public OnOff  HasDetails            { get; set; }
		[JsonProperty("id")]                     public long   Id                    { get; set; }
		[JsonProperty("itemtype_name")]          public string ItemtypeName          { get; set; }
		[JsonProperty("name")]                   public string Name                  { get; set; }
		[JsonProperty("need_processing")]        public OnOff  NeedProcessing        { get; set; }
		[JsonProperty("orderpriority")]          public long   Orderpriority         { get; set; }
		[JsonProperty("pricelist_cost")]         public string PricelistCost         { get; set; }
		[JsonProperty("pricelist_cost_orig")]    public string PricelistCostOrig     { get; set; }
		[JsonProperty("processingmodule_count")] public long   ProcessingmoduleCount { get; set; }
		[JsonProperty("processingmodules")]      public string Processingmodules     { get; set; }
		[JsonProperty("project")]                public string Project               { get; set; }
		///[JsonProperty("quickorder")]             public OnOff?  Quickorder            { get; set; }
		///[JsonProperty("show_on_dashboard")]      public OnOff?  ShowOnDashboard       { get; set; }
	}
}