﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class ServiceDetailList : FuncResultList<ServiceDetailItem> { }
	public class ServiceDetailItem
	{
		[JsonProperty("addonlimit")] public string Addonlimit { get; set; }
		[JsonProperty("billtype")]   public long   Billtype   { get; set; }
		[JsonProperty("id")]         public long   Id         { get; set; }
		[JsonProperty("idkey")]      public long   Idkey      { get; set; }
		[JsonProperty("name")]       public string Name       { get; set; }
		[JsonProperty("props")]      public string Props      { get; set; }
		[JsonProperty("value")]      public string Value      { get; set; }
	}
}