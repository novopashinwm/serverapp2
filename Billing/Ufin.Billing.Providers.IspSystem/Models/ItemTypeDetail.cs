﻿using System.Dynamic;
using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class ItemTypeDetailList : FuncResultList<ItemTypeDetailItem, ExpandoObject> { }
	public class ItemTypeDetailItem
	{
		[JsonProperty("embedded")]      public OnOff  Embedded      { get; set; }
		[JsonProperty("id")]            public long   Id            { get; set; }
		[JsonProperty("intname")]       public string Intname       { get; set; }
		[JsonProperty("name")]          public string Name          { get; set; }
		[JsonProperty("orderpriority")] public long   Orderpriority { get; set; }
	}
}