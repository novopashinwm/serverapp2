﻿using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class Auth : FuncResult<AuthModel> { }
	public class AuthModel
	{
		[JsonProperty("auth")] public string Auth { get; set; }
	}
}