﻿using System.Dynamic;
using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class AccountGroupList : FuncResultList<AccountGroupItem, ExpandoObject> { }
	public class AccountGroupItem
	{
		[JsonProperty("abuse_rate")]       public long   AbuseRate        { get; set; }
		[JsonProperty("allowdeleteitem")]  public OnOff  Allowdeleteitem  { get; set; }
		[JsonProperty("allowselfview")]    public OnOff  Allowselfview    { get; set; }
		[JsonProperty("allowsuspenditem")] public OnOff  Allowsuspenditem { get; set; }
		[JsonProperty("attitude")]         public long   Attitude         { get; set; }
		[JsonProperty("enterprise")]       public OnOff  Enterprise       { get; set; }
		[JsonProperty("id")]               public long   Id               { get; set; }
		[JsonProperty("ignoreabuserate")]  public OnOff  Ignoreabuserate  { get; set; }
		[JsonProperty("ignoreitemmax")]    public OnOff  Ignoreitemmax    { get; set; }
		[JsonProperty("name")]             public string Name             { get; set; }
		[JsonProperty("nocalcstat")]       public OnOff  Nocalcstat       { get; set; }
		[JsonProperty("taxcustompay")]     public OnOff  Taxcustompay     { get; set; }
		[JsonProperty("taxexclusive")]     public OnOff  Taxexclusive     { get; set; }
	}
}