﻿using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class SubaccountList : FuncResultList<SubaccountItem> { }
	public class SubaccountItem
	{
		[JsonProperty("active")]      public OnOff  Active      { get; set; }
		[JsonProperty("balance")]     public string Balance     { get; set; }
		[JsonProperty("creditlimit")] public string Creditlimit { get; set; }
		[JsonProperty("id")]          public long   Id          { get; set; }
		[JsonProperty("project")]     public string Project     { get; set; }
	}
}