﻿using System;
using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class ItemTypeInstanceList : FuncResultList<ItemTypeInstanceItem> { }
	public class ItemTypeInstanceItem
	{
		[JsonProperty("abusesuspend")]               public OnOff          Abusesuspend               { get; set; }
		[JsonProperty("autosuspend")]                public OnOff          Autosuspend                { get; set; }
		[JsonProperty("billdaily")]                  public OnOff          Billdaily                  { get; set; }
		[JsonProperty("billhourly")]                 public OnOff          Billhourly                 { get; set; }
		[JsonProperty("change_pl")]                  public long           ChangePl                   { get; set; }
		[JsonProperty("change_pricelist")]           public OnOff          ChangePricelist            { get; set; }
		[JsonProperty("changepolicy")]               public long           Changepolicy               { get; set; }
		[JsonProperty("cost")]                       public string         Cost                       { get; set; }
		[JsonProperty("costperiod")]                 public long           Costperiod                 { get; set; }
		[JsonProperty("createdate")]                 public DateTimeOffset Createdate                 { get; set; }
		[JsonProperty("currency_id")]                public long           CurrencyId                 { get; set; }
		[JsonProperty("currency_str")]               public string         CurrencyStr                { get; set; }
		[JsonProperty("datacenter")]                 public long           Datacenter                 { get; set; }
		[JsonProperty("datacentername")]             public string         Datacentername             { get; set; }
		[JsonProperty("description")]                public string         Description                { get; set; }
		[JsonProperty("employeesuspend")]            public OnOff          Employeesuspend            { get; set; }
		[JsonProperty("expiredate")]                 public string         Expiredate                 { get; set; }
		[JsonProperty("has_ip_pricelist")]           public OnOff          HasIpPricelist             { get; set; }
		[JsonProperty("id")]                         public long           Id                         { get; set; }
		[JsonProperty("intname")]                    public string         Intname                    { get; set; }
		[JsonProperty("item_cost")]                  public decimal        ItemCost                   { get; set; }
		[JsonProperty("item_real_status")]           public long           ItemRealStatus             { get; set; }
		[JsonProperty("item_status")]                public string         ItemStatus                 { get; set; }
		//[JsonProperty("item_status_orig")]           public long           ItemStatusOrig             { get; set; }
		[JsonProperty("itemtype")]                   public long           ItemType                   { get; set; }
		[JsonProperty("module_failed")]              public OnOff          ModuleFailed               { get; set; }
		[JsonProperty("module_upload_doc")]          public OnOff          ModuleUploadDoc            { get; set; }
		[JsonProperty("name")]                       public string         Name                       { get; set; }
		[JsonProperty("no_instruction")]             public OnOff          NoInstruction              { get; set; }
		[JsonProperty("period")]                     public long           Period                     { get; set; }
		[JsonProperty("pmmodule")]                   public string         Pmmodule                   { get; set; }
		[JsonProperty("pricelist")]                  public string         PriceList                  { get; set; }
		[JsonProperty("pricelist_id")]               public long           PriceListId                { get; set; }
		[JsonProperty("processingmodule_failed")]    public OnOff          ProcessingmoduleFailed     { get; set; }
		[JsonProperty("processingnode")]             public string         Processingnode             { get; set; }
		[JsonProperty("project")]                    public string         Project                    { get; set; }
		[JsonProperty("real_expiredate")]            public DateTimeOffset RealExpiredate             { get; set; }
		[JsonProperty("scheduledclose")]             public OnOff          Scheduledclose             { get; set; }
		[JsonProperty("show_changepassword")]        public OnOff          ShowChangepassword         { get; set; }
		[JsonProperty("show_hardreboot")]            public OnOff          ShowHardreboot             { get; set; }
		[JsonProperty("show_movetovdc")]             public OnOff          ShowMovetovdc              { get; set; }
		[JsonProperty("show_reboot")]                public OnOff          ShowReboot                 { get; set; }
		[JsonProperty("show_supportpassword")]       public OnOff          ShowSupportpassword        { get; set; }
		[JsonProperty("show_vdc_loadbalancer")]      public OnOff          ShowVdcLoadbalancer        { get; set; }
		[JsonProperty("show_vdc_network")]           public OnOff          ShowVdcNetwork             { get; set; }
		[JsonProperty("show_vdc_router")]            public OnOff          ShowVdcRouter              { get; set; }
		[JsonProperty("show_vdc_vm")]                public OnOff          ShowVdcVm                  { get; set; }
		[JsonProperty("show_vdc_volume")]            public OnOff          ShowVdcVolume              { get; set; }
		[JsonProperty("show_webconsole")]            public OnOff          ShowWebconsole             { get; set; }
		[JsonProperty("specialstatus")]              public long           Specialstatus              { get; set; }
		[JsonProperty("stat_enabled")]               public OnOff          StatEnabled                { get; set; }
		[JsonProperty("status")]                     public long           Status                     { get; set; }
		[JsonProperty("subaccount")]                 public long           Subaccount                 { get; set; }
		[JsonProperty("sync_feature")]               public OnOff          SyncFeature                { get; set; }
		[JsonProperty("transition")]                 public OnOff          Transition                 { get; set; }
	}
}