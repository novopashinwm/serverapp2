﻿using System.Dynamic;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class PaymentAddRedirectResult : FuncResult<ExpandoObject>
	{
		[JsonProperty("ok")] public OkItem Ok { get; set; }
		public class OkItem
		{
			[JsonProperty("type")] public string Type  { get; set; }
			[JsonProperty("v")]    public string Value { get; set; }
		}
	}
}