﻿using System.Collections.Generic;
using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class PaymentAddPaymethodList : FuncResult<PaymentAddPaymethodModel>
	{
		[JsonProperty("list")] public ItemsList Items { get; set; }
		public class ItemsList
		{
			[JsonProperty("methodlist")] public List<MethodItem> Methods { get; set; }
			public class MethodItem
			{
				[JsonProperty("action")]                 public string   Action               { get; set; }
				[JsonProperty("blank")]                  public YesNo    Blank                { get; set; }
				[JsonProperty("desc")]                   public string   Desc                 { get; set; }
				[JsonProperty("description")]            public string   Description          { get; set; }
				[JsonProperty("image")]                  public string   Image                { get; set; }
				[JsonProperty("maxamount")]              public decimal? Maxamount            { get; set; }
				[JsonProperty("minamount")]              public decimal? Minamount            { get; set; }
				[JsonProperty("module")]                 public string   Module               { get; set; }
				[JsonProperty("name")]                   public string   Name                 { get; set; }
				[JsonProperty("payment_maxamount")]      public decimal? PaymentMaxamount     { get; set; }
				[JsonProperty("payment_minamount")]      public decimal? PaymentMinamount     { get; set; }
				[JsonProperty("paymethod")]              public long     Paymethod            { get; set; }
				[JsonProperty("paymethod_currency")]     public long     PaymethodCurrency    { get; set; }
				[JsonProperty("paymethod_currency_iso")] public string   PaymethodCurrencyIso { get; set; }
			}
		}
	}
	public class PaymentAddPaymethodModel
	{
		[JsonProperty("amount")]           public decimal? Amount          { get; set; }
		[JsonProperty("billorder")]        public string   Billorder       { get; set; }
		[JsonProperty("customer_account")] public string   CustomerAccount { get; set; }
		[JsonProperty("payment_currency")] public long     PaymentCurrency { get; set; }
		[JsonProperty("paymentupload")]    public string   Paymentupload   { get; set; }
		[JsonProperty("project")]          public string   Project         { get; set; }
		[JsonProperty("randomnumber")]     public string   Randomnumber    { get; set; }
	}
}