﻿using System.Dynamic;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class SessionNewKeyResult : FuncResult<ExpandoObject>
	{
		[JsonProperty("ok")] public bool? IsOk { get; set; }
	}
	public class SessionList : FuncResultList<SessionItem> { }
	public class SessionItem
	{
		[JsonProperty("count")]    public long   Count    { get; set; }
		[JsonProperty("id")]       public string Id       { get; set; }
		[JsonProperty("idle")]     public string Idle     { get; set; }
		[JsonProperty("ip")]       public string Ip       { get; set; }
		[JsonProperty("level")]    public long   Level    { get; set; }
		[JsonProperty("name")]     public string Name     { get; set; }
		[JsonProperty("realname")] public string Realname { get; set; }
	}
}