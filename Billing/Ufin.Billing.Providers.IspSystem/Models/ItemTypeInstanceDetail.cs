﻿using System.Dynamic;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class ItemTypeInstanceDetail : FuncResult<ExpandoObject>
	{
		[JsonProperty("customfields")] public ExpandoObject CustomFields { get; set; }
	}
}