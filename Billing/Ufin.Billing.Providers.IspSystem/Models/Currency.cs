﻿using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class CurrencyList : FuncResultList<CurrencyItem> { }
	public class CurrencyItem
	{
		[JsonProperty("active")] public OnOff  Active { get; set; }
		[JsonProperty("code")]   public long   Code   { get; set; }
		[JsonProperty("id")]     public long   Id     { get; set; }
		[JsonProperty("iso")]    public string Iso    { get; set; }
		[JsonProperty("name")]   public string Name   { get; set; }
		
	}
}