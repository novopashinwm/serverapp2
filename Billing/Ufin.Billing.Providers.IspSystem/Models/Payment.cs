﻿using System;
using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class PaymentList : FuncResultList<PaymentElem> { }

	public class PaymentElem
	{
		[JsonProperty("account_name")]            public string          AccountName          { get; set; }
		[JsonProperty("allowrefund")]             public OnOff           Allowrefund          { get; set; }
		[JsonProperty("create_date")]             public DateTimeOffset  CreateDate           { get; set; }
		[JsonProperty("create_date_l")]           public string          CreateDateL          { get; set; }
		[JsonProperty("create_date_locale_time")] public TimeSpan        CreateDateLocaleTime { get; set; }
		[JsonProperty("create_date_s")]           public string          CreateDateS          { get; set; }
		[JsonProperty("id")]                      public long            Id                   { get; set; }
		[JsonProperty("number")]                  public string          Number               { get; set; }
		[JsonProperty("paymethod_module")]        public string          PaymethodModule      { get; set; }
		[JsonProperty("paymethod_name")]          public string          PaymethodName        { get; set; }
		[JsonProperty("paymethodamount_iso")]     public string          PaymethodamountIso   { get; set; }
		[JsonProperty("project")]                 public string          Project              { get; set; }
		[JsonProperty("recipient_id")]            public long?           RecipientId          { get; set; }
		[JsonProperty("recipient_name")]          public string          RecipientName        { get; set; }
		[JsonProperty("refund")]                  public OnOff           Refund               { get; set; }
		[JsonProperty("return_payment_receipt")]  public OnOff           ReturnPaymentReceipt { get; set; }
		[JsonProperty("sender_id")]               public long?           SenderId             { get; set; }
		[JsonProperty("sender_name")]             public string          SenderName           { get; set; }
		[JsonProperty("status")]                  public string          Status               { get; set; }
		[JsonProperty("status_orig")]             public long            StatusOrig           { get; set; }
		[JsonProperty("subaccountamount_iso")]    public string          SubaccountamountIso  { get; set; }
		[JsonProperty("featurerefund")]           public OnOff?          Featurerefund        { get; set; }
		[JsonProperty("pay_date")]                public DateTimeOffset? PayDate              { get; set; }
		[JsonProperty("pay_date_l")]              public string          PayDateL             { get; set; }
		[JsonProperty("pay_date_locale_time")]    public TimeSpan?       PayDateLocaleTime    { get; set; }
		[JsonProperty("pay_date_s")]              public string          PayDateS             { get; set; }
	}
}