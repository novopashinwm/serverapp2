﻿using System;
using Newtonsoft.Json;

namespace Compass.Ufin.Billing.Providers.IspSystem.Models
{
	public class AccountDashboardInfoList : FuncResultList<AccountDashboardInfoItem> { }
	public class AccountDashboardInfoItem
	{
		[JsonProperty("annual_turnover")]        public string         AnnualTurnover        { get; set; }
		[JsonProperty("balance")]                public string         Balance               { get; set; }
		[JsonProperty("country")]                public string         Country               { get; set; }
		[JsonProperty("enoughmoneyto")]          public string         Enoughmoneyto         { get; set; }
		[JsonProperty("id")]                     public long           Id                    { get; set; }
		[JsonProperty("monitoringobject_total")] public long           MonitoringobjectTotal { get; set; }
		[JsonProperty("monitoringobject_used")]  public string         MonitoringobjectUsed  { get; set; }
		[JsonProperty("phone")]                  public string         Phone                 { get; set; }
		[JsonProperty("prolong_amount")]         public string         ProlongAmount         { get; set; }
		[JsonProperty("prolong_amount_min")]     public string         ProlongAmountMin      { get; set; }
		[JsonProperty("provider")]               public string         Provider              { get; set; }
		[JsonProperty("regdate")]                public DateTimeOffset Regdate               { get; set; }
		[JsonProperty("subaccount")]             public long           Subaccount            { get; set; }
	}
}