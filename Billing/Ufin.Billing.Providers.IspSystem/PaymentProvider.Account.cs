﻿using System;
using System.Collections.Generic;
using System.Linq;
using Compass.Ufin.Billing.Core.Enums;
using Compass.Ufin.Billing.Core.Models;
using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Compass.Ufin.Billing.Providers.IspSystem.Models;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	public partial class PaymentProvider
	{
		#region Private Methods

		////////////////////////////////////////////////////////////////////////////////////////
		private IEnumerable<AccountGroupItem> GetAccountGroupList()
		{
			using (var billCtx = GetBillContext())
			{
				return IspSystemBillApi.AccountGroupListAsync(billCtx)
					?.Result
					?.Items ?? Enumerable.Empty<AccountGroupItem>();
			}
		}
		////////////////////////////////////////////////////////////////////////////////////////
		private IEnumerable<AccountGroupItem> GetCachedAccountGroupList()
		{
			return _cache.GetObjectFromCache(
				cacheDataKey:    $@"{nameof(GetAccountGroupList)}",
				cacheDataGet:    () => GetAccountGroupList(),
				cacheExpiration: SlowCacheExpiration);
		}
		////////////////////////////////////////////////////////////////////////////////////////
		private BalanceInfo GetBalanceInfoInternal(string su, string lang = "ru")
		{
			// Получаем параметры текущего счета
			var subaccount = GetCachedActiveSubaccountList(su, lang)
				?.FirstOrDefault();
			if (null == subaccount)
				return default(BalanceInfo);
			// Получаем дополнительные параметры текущего счета
			var subaccountEdit = GetCachedSubaccountDetail(subaccount.Id, lang);
			// Получаем список типов продуктов/услуг
			var itemTypeList = GetCachedItemTypeList(lang);
			// Получаем все продукты по всем типам продуктов/услуг
			var productItems = GetCachedAllActiveItemList(su, lang)
				/// Фильтруем только продуктами, из перечисления <see cref="ProductType"/>
				?.Where(t => { ProductType ptype; return Enum.TryParse(t.ItemType, true, out ptype) && ptype != ProductType.Unspecified; })
				// Группируем по типу продукта/услуги
				?.GroupBy(g => g.ItemType)
				?.Select(g =>
				{
					var result   = new BalanceInfo.ItemTypeInfo();
					var itemType = itemTypeList.FirstOrDefault(i => i.Intname == g.Key);
					if (null == itemType)
						return result;
					result.Id     = itemType.Id;
					result.Code   = itemType.Intname;
					result.Name   = itemType.Name;
					result.Count  = g.Count();
					result.Amount = g.Sum(i => i.Cost
						?.ParseValueWithUnits((s) => { var val = default(decimal);  decimal.TryParse(s, out val); return val; })
						?.FirstOrDefault()
						?.Cost ?? 0);
					return result;
				});
			// Получаем скидки
			var discountItems = GetCachedDiscountList(su, lang)
				?.Select(d =>
				{
					var discountValue = (!string.IsNullOrEmpty(d.Value)
						? d.Value
						: d.HavePrice)
						?.ParseValueWithUnits((s) => { var val = default(decimal); decimal.TryParse(s, out val); return val; });
					return new BalanceInfo.DiscountItem
					{
						Id     = d.Id,
						Name   = d.Name,
						Period = d.Period,
						Value  = discountValue?.FirstOrDefault()?.Cost ?? 0,
						Unit   = discountValue?.FirstOrDefault()?.Unit,
					};
				});
			// Заполняем результирующую информацию
			return new BalanceInfo
			{
				Id          = subaccount.Id,
				Balance     = subaccountEdit?.Model?.Balance ?? 0M,
				ItemTypes   = productItems?.ToList(),
				TotalAmount = productItems?.Sum(v => v.Amount) ?? 0M,
				Discounts   = discountItems?.ToList(),
				Creditlimit = subaccount.Creditlimit,
				Currency    = GetCachedCurrencyList(lang)
					?.Where(c => c.Id == subaccountEdit?.Model?.CurrencyId)
					?.Select(c => new Core.Models.CurrencyItem
					{
						Id   = c.Id,
						Code = c.Iso,
						Name = c.Name,
					})
					?.FirstOrDefault()
			};
		}
		////////////////////////////////////////////////////////////////////////////////////////
		/// <summary> Получить список типов продуктов/услуг, исключая объекты наблюдения и неизвестные </summary>
		private IEnumerable<Tuple<long, ProductType>> GetAccountProductTypes(string lang = "ru")
		{
			// Получаем список типов продуктов/услуг, исключая объекты наблюдения и неизвестные
			return GetCachedItemTypeList(lang)
				?.Select(t =>
				{
					ProductType ptype;
					return new Tuple<long, ProductType>
					(
						t.Id,
						Enum.TryParse(t.Intname, true, out ptype) ? ptype : ProductType.Unspecified
					);
				})
				?.Where(t => t.Item2 != ProductType.MonitoringObject && t.Item2 != ProductType.Unspecified);
		}
		////////////////////////////////////////////////////////////////////////////////////////

		#endregion Private Methods
		#region IPaymentProvider
		public BalanceInfo GetAccountBalanceInfo(string user, string lang = "ru")
		{
			return _cache.GetObjectFromCache(
				cacheDataKey: $@"{lang}_{user}_{nameof(GetBalanceInfoInternal)}",
				cacheDataGet: () => GetBalanceInfoInternal(user, lang),
				cacheExpiration: SlowCacheExpiration);
		}
		public ProductItem GetAccountProductPrice(ProductItem product, string user, string lang = "ru")
		{
			// Это кэшировать нельзя, т.к. это расчет зависит от входных параметров
			if (null == product)
				return product;
			//////////////////////////////////////////////////////////////////
			/// Ищем тариф по идентификатору, и заполняем необходимое
			var productType = GetProductFromPriceListExport(
				GetCachedPriceListExportById(product.Type?.Id ?? -1), lang);
			if (productType != null)
			{
				product.Addons?.ForEach(a =>
				{
					a.Meta  = productType.Addons?.FirstOrDefault(o => a.Code == o.Code)?.Meta;
					a.Price = productType.Addons?.FirstOrDefault(o => a.Code == o.Code)?.Price;
				});
				product.Price = productType.Price;
			}
			//////////////////////////////////////////////////////////////////
			using (var billCtx = GetBillContext(lang))
			{
				var productPrices = IspSystemBillApi.PriceListCalculateAsync(
					billCtx,
					product.Type.Id,
					product.Price.Period.ToIspSystemPeriod(),
					product
						?.Addons
						?.Where(a => (a.Meta?.CanBeModified ?? true))
						?.Select(a =>
						{
							// Ключ
							var key = $@"addon_{a.Id}";
							// Значение, по умолчанию целое
							var val = a.Data.ValueUser.ToString();
							// Значение, если Boolean, то on|off
							if (a.Meta.ValueType == ProductAddonValueType.Boolean)
								val = 0 == a.Data.ValueUser
									? nameof(OnOff.Off).ToLowerInvariant()
									: nameof(OnOff.On).ToLowerInvariant();
							return new KeyValuePair<string, string>(key, val);
						}) ?? new KeyValuePair<string, string>[0],
					user)
					?.Result;
				if (null != productPrices?.Error)
					throw new ApplicationException(productPrices.Error.Msg ?? "Unknown error");
				return productPrices?.SetPricesToProduct(product);
			}
		}
		public ProductList GetAccountAvailableProducts(string user, string lang = "ru")
		{
			var itemTypes = GetAccountProductTypes(lang)
				?.ToArray();
			var result = new ProductList();
			foreach (var itemType in itemTypes)
			{
				var itemTypeAvailable = GetCachedItemTypeOrderPricelists(itemType.Item2, user, lang)
					?.Select(p =>
					{
						return GetAccountProductPrice(
							product: GetProductFromPriceListExport(GetCachedPriceListExportById(p.Pricelist, lang)),
							user:    user,
							lang:    lang);
					})
					?.ToArray();
				if (null != itemTypeAvailable)
					result.AddRange(itemTypeAvailable);
			}
			return result;
		}
		public ProductList GetAccountCurrentProducts(string user, string lang = "ru")
		{
			var itemTypes = GetAccountProductTypes(lang)
				?.ToArray();
			var result = new ProductList();
			foreach (var itemType in itemTypes)
			{
				var itemTypeInstances = GetCachedItemTypeInstanceList(itemType.Item2, user, lang)
					?.Select(i =>
					{
						// Используем расчет цены, как для не купленного | текущая цена на продукт с текущими скидками
						return GetAccountProductPrice(GetProductFromItemTypeInstance(i, lang).WitoutPrices(), user, lang);
					})
					?.ToArray();
				if (null != itemTypeInstances)
					result.AddRange(itemTypeInstances);
			}
			return result;
		}
		public ProductList GetAccountProducts(ProductStatus[] productStatuses, string user, string lang = "ru")
		{
			if (0 == (productStatuses?.Length ?? 0))
				productStatuses = Enum.GetValues(typeof(ProductStatus))
					.OfType<ProductStatus>()
					.ToArray();
			var productStatusIds = productStatuses.Select(s => (long)s).ToArray();
			var itemTypes = GetAccountProductTypes(lang)
				?.ToArray();
			var result = new ProductList();
			foreach (var itemType in itemTypes)
			{
				var itemTypeInstances = GetCachedItemTypeInstanceList(itemType.Item2, user, lang)
					?.Where(i => productStatusIds.Contains(i.Status))
					?.Select(i =>
					{
						// Используем расчет цены, как для не купленного | текущая цена на продукт с текущими скидками
						return GetAccountProductPrice(GetProductFromItemTypeInstance(i, lang).WitoutPrices(), user, lang);
					})
					?.ToArray();
				if (null != itemTypeInstances)
					result.AddRange(itemTypeInstances);
			}
			return result;
		}
		public ProductItem SetAccountProduct(ProductItem product, string user, string lang = "ru")
		{
			//////////////////////////////////////////////////////////////////
			if (-1 == (product?.Type?.Id ?? -1))
				throw new ArgumentException("Product pricelist not specified", nameof(product));
			if (null == product?.Price?.Period)
				throw new ArgumentException("Product price period not specified", nameof(product));
			if (string.IsNullOrWhiteSpace(product?.Type?.Base?.Code))
				throw new ArgumentException("Product type not specified", nameof(product));
			//////////////////////////////////////////////////////////////////
			// Если Meta для Addons не переданы, то заполняем их
			if (product?.Addons?.Any(a => null == a.Meta?.ValueType) ?? false)
			{
				var productOrig = GetProductFromPriceListExport(
					GetCachedPriceListExportById(product?.Type?.Id ?? -1), lang);
				if (productOrig != null)
					product.Addons.ForEach(a => a.Meta = a.Meta ?? productOrig?.Addons?.FirstOrDefault(o => a.Code == o.Code)?.Meta);
			}
			//////////////////////////////////////////////////////////////////
			using (var billCtx = GetBillContext(lang))
			{
				// Подготавливаем значения параметров
				var pars = product.Params
					?.Where(p => !string.IsNullOrWhiteSpace(p.Value))
					?.Select(p => new KeyValuePair<string, string>(p.Code, p.Value))
						?? Enumerable.Empty<KeyValuePair<string, string>>();
				// Подготавливаем значения дополнений (содержимого)
				var adds = product.Addons
					?.Where(a => null != a.Data?.ValueUser)
					.Select(a =>
					{
						// Ключ
						var key = $@"addon_{a.Id}";
						// Значение, по умолчанию целое
						var val = a.Data.ValueUser.ToString();
						// Значение, если Boolean, то on|off
						if (a.Meta.ValueType == ProductAddonValueType.Boolean)
							val = 0 == a.Data.ValueUser
								? nameof(OnOff.Off).ToLowerInvariant()
								: nameof(OnOff.On).ToLowerInvariant();
						return new KeyValuePair<string, string>(key, val);
					})
						?? Enumerable.Empty<KeyValuePair<string, string>>();
				var periodId = (long)(product?.Price?.Period.ToIspSystemPeriod() ?? PricePeriod.Monthly.ToIspSystemPeriod());
				if (0 < product.Id)
				{ // Изменить
					var edtResult = IspSystemBillApi.ItemTypeInstanceEditAsync(billCtx,
						product.Type.Base.Code,
						user,
						product.Id,
						new Dictionary<string, string>
						{
							// Пропустить корзину, сразу делать оплату
							{ "skipbasket",  $"on"                              },
							// Если тариф периодический ставим автоматическую пролонгацию через интервал заказа
							{ "autoprolong", $"{(0 < periodId ? periodId : 0)}" },
						}
							.Union(pars) // Добавить параметры
							.Union(adds) // Добавить дополнения (содержимое)
						)
						?.Result;
					if (!(edtResult?.IsOk ?? false))
						throw new ApplicationException(edtResult?.Error?.Msg ?? "Unknown error");
				}
				else
				{ // Заказать
					var ordResult = IspSystemBillApi.ItemTypeInstanceOrderParamAsync(billCtx,
						product.Type.Base.Code,
						user,
						product.Type.Id,
						new Dictionary<string, string>
						{
							// Пропустить корзину, сразу делать оплату
							{ "skipbasket",  $"on"                              },
							// Если тариф периодический ставим автоматическую пролонгацию через интервал заказа
							{ "autoprolong", $"{(0 < periodId ? periodId : 0)}" },
						}
							.Union(pars) // Добавить параметры
							.Union(adds), // Добавить дополнения (содержимое)
						product.Price.Period.ToIspSystemPeriod())
						?.Result;
					if (!(ordResult?.IsOk ?? false))
						throw new ApplicationException(ordResult?.Error?.Msg ?? "Unknown error");
					product.Id = ordResult.Id.Id;
				}
				// Очищаем кэш текущего тарифа
				_cache
					.Where(i => i.Key.Contains($@"_{product?.Type?.Base?.Code?.ToLowerInvariant()}_{nameof(GetItemTypeInstanceList)}"))
					.Select(kv => kv.Key)
					.ToArray()
					.ForEach((k) => _cache.Remove(k));
				_cache
					.Where(i => i.Key.Contains($@"_{product?.Type?.Base?.Code?.ToLowerInvariant()}_{nameof(GetProductItemDetail)}_{product?.Id}"))
					.Select(kv => kv.Key)
					.ToArray()
					.ForEach((k) => _cache.Remove(k));
				_cache
					.Where(i => i.Key.Contains($@"_{product?.Type?.Base?.Code?.ToLowerInvariant()}_{product?.Id}"))
					.Select(kv => kv.Key)
					.ToArray()
					.ForEach((k) => _cache.Remove(k));
				// Очищаем кэш перед опросом текущего тарифа (удаляем все для этого пользователя)
				_cache
						.Where(i => i.Key.Contains($@"_{user}_"))
						.Select(kv => kv.Key)
						.ToArray()
						.ForEach((k) => _cache.Remove(k));

				// Очищаем кэш перед опросом текущего тарифа (удаляем все для всех пользователей)
				_cache
					.Where(i => i.Key.Contains($@"__"))
					.Select(kv => kv.Key)
					.ToArray()
					.ForEach((k) => _cache.Remove(k));
			}
			var result = GetAccountProducts(null, user, lang)
				.FirstOrDefault(p => p.Id == product.Id);
			if (result == null)
				throw new ApplicationException("Resultant product not found");
			return result;
		}
		#endregion IPaymentProvider
	}
}