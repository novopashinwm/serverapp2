﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Compass.Ufin.Billing.Providers.IspSystem.Models;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	public static partial class IspSystemBillApi
	{
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: session, сессии </summary>
		public static async Task<SessionList> SessionListAsync(IspSystemBillApiContext context,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func",  $@"session" },
			}, cancellationToken))
				?.ToObject<SessionList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: session.newkey, получить одноразовую ссылку для входа как другой пользователь </summary>
		public static async Task<string> GetOneTimeLoginLink(IspSystemBillApiContext context,
			string authAsUser, CancellationToken cancellationToken = default(CancellationToken))
		{
			var sessionNewKey = Guid.NewGuid();
			var methodResult = (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func",     $@"session.newkey"  },
				{ "username", $@"{authAsUser}"    },
				{ "key",      $@"{sessionNewKey}" },
			}, cancellationToken))
				?.ToObject<SessionNewKeyResult>();
			if (!(methodResult.IsOk ?? false))
				throw new ApplicationException(methodResult.Error?.Msg ?? "Unknown error");
			return $@"{context.BaseUrl}?func=auth&username={authAsUser}&key={sessionNewKey}&checkcookie=no";
		}
		////////////////////////////////////////////////////////////////////////////////
	}
}