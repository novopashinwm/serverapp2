﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Compass.Ufin.Billing.Providers.IspSystem.Configuration;
using Compass.Ufin.Billing.Providers.IspSystem.Models;
using Flurl.Http;
using Flurl.Util;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using Newtonsoft.Json.Linq;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	//////////////////////////////////////////////////////////////////////
	/// <summary> Обертки вызовов IspSystem.BillManager (используется только json в формате bjson) </summary>
	public static partial class IspSystemBillApi
	{
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Получить HttpClient </summary>
		private static HttpClient GetHttpClient()
		{
			var handler = new HttpClientHandler()
			{
				UseCookies = false,
			};
			var client = new HttpClient(handler)
			{
				Timeout = TimeSpan.FromMinutes(5),
			};
			client.DefaultRequestHeaders.Accept.Clear();
			return client;
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Получить IspSystemApiContext </summary>
		public static IspSystemBillApiContext GetContext(string url, string lang = "ru")
		{
			return new IspSystemBillApiContext(GetHttpClient()) { BaseUrl = url }
				.WithProperties(new Dictionary<string, object>
				{
					{ IspSystemBillApiContext.KEY_OUT,    $@"bjson"          },
					{ IspSystemBillApiContext.KEY_LANG,   $@"{lang}"         },
					{ IspSystemBillApiContext.KEY_FILTER, $@"off"            },
					{ IspSystemBillApiContext.KEY_PCNT,   $@"{int.MaxValue}" },
				});
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Получить IspSystemApiContext </summary>
		public static IspSystemBillApiContext GetContext(string user, string pass, string url, string lang = "ru")
		{
			return GetContext(url, lang)
				.WithProperty(IspSystemBillApiContext.KEY_AUTHINFO, $@"{user}:{pass}");
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Общая обертка для всех функций (можно добавить любые обработки) </summary>
		public static async Task<T> WebApiHttpMethod<T>(
			Task<IFlurlResponse> mess,
			Func<HttpContent> args,
			string name)
		{
			////////////////////////////////////////////////////
			if (IspSystemConfigurationSection.Instance.Verbose)
			{
				Trace.TraceInformation($"{name} REQ\n{mess.Result.ResponseMessage.RequestMessage}");
				Trace.TraceInformation($"{name} REQ Content (url decoded)\n{HttpUtility.UrlDecode(args().ReadAsStringAsync().Result)}");
				Trace.TraceInformation($"{name} RES\n{mess.Result.ResponseMessage}");
			}
			var content = await mess.ReceiveString();
			////////////////////////////////////////////////////
			if (IspSystemConfigurationSection.Instance.Verbose)
			{
				Trace.TraceInformation($"{name} RES Content\n{content}");
			}
			////////////////////////////////////////////////////
			return content.ToJToken().ToObject<T>();
			/////////////////////////////////////////////////////////
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Общая обертка для всех функций, выполняет через HTTP GET </summary>
		public static async Task<JToken> AnyGet(IspSystemBillApiContext context,
			IEnumerable<KeyValuePair<string, string>> nameValueCollection,
			CancellationToken cancellationToken = default(CancellationToken),
			[CallerMemberName] string callerName = null)
		{
			/////////////////////////////////////////////////////////
			Func<FormUrlEncodedContent> formArgs = () => new FormUrlEncodedContent(
				(context?.Properties ?? new Dictionary<string, object>())
				.ToDictionary(k => k.Key, v => v.Value?.ToString())
				.Union(nameValueCollection)
				.Where(v => !string.IsNullOrWhiteSpace(v.Value)));
			////////////////////////////////////////////////////
			var stopwatcher = new Stopwatcher(
				$"{nameof(IspSystemBillApi)}.{nameof(AnyGet)}",
				(time) => $"{callerName}({string.Join("&", nameValueCollection?.Where(kv => !string.IsNullOrWhiteSpace(kv.Value))?.Select(kv => $"{kv.Key}={kv.Value}") ?? Enumerable.Empty<string>())})",
				IspSystemConfigurationSection.Instance.Verbose ? TimeSpan.Zero : TimeSpan.FromTicks(1));
			using (stopwatcher)
				return await WebApiHttpMethod<JToken>(context.Request()
					.GetAsync(cancellationToken), formArgs, callerName);
			////////////////////////////////////////////////////
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Общая обертка для всех функций, выполняет через HTTP POST </summary>
		public static async Task<JToken> AnyPost(IspSystemBillApiContext context,
			IEnumerable<KeyValuePair<string, string>> nameValueCollection,
			CancellationToken cancellationToken = default(CancellationToken),
			[CallerMemberName] string callerName = null)
		{
			/////////////////////////////////////////////////////////
			Func<FormUrlEncodedContent> formArgs = () => new FormUrlEncodedContent(
				(context?.Properties ?? new Dictionary<string, object>())
				.ToDictionary(k => k.Key, v => v.Value?.ToString())
				.Union(nameValueCollection)
				.Where(v => !string.IsNullOrWhiteSpace(v.Value)));
			////////////////////////////////////////////////////
			var stopwatcher = new Stopwatcher(
				$"{nameof(IspSystemBillApi)}.{nameof(AnyPost)}",
				(time) => $"{callerName}({string.Join("&", nameValueCollection?.Where(kv => !string.IsNullOrWhiteSpace(kv.Value))?.Select(kv => $"{kv.Key}={kv.Value}") ?? Enumerable.Empty<string>())})",
				IspSystemConfigurationSection.Instance.Verbose ? TimeSpan.Zero : TimeSpan.FromTicks(1));
			using (stopwatcher)
				return await WebApiHttpMethod<JToken>(context.Request()
					.PostAsync(formArgs(), cancellationToken), formArgs, callerName);
			////////////////////////////////////////////////////
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: auth, аутентификация </summary>
		public static async Task<Auth> AuthAsync(IspSystemBillApiContext context,
			string username, string password, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func",     $@"auth"       },
				{ "username", $@"{username}" },
				{ "password", $@"{password}" },
			}, cancellationToken))
				?.ToObject<Auth>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: actionlist, список всех доступных функций, некоторые с описанием </summary>
		public static async Task<ActionList> ActionListAsync(IspSystemBillApiContext context,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"actionlist" },
			}, cancellationToken))
				?.ToObject<ActionList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: item_all, список всех продуктов всех типов </summary>
		public static async Task<ItemAllList> ItemAllListAsync(IspSystemBillApiContext context,
			string user, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"item_all" },
				{ "su",   $@"{user}"   },
			}, cancellationToken))
				?.ToObject<ItemAllList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Вспомогательный метод преобразования массива в строку для передачи набора идентификаторов </summary>
		public static string GetElidArrayString<T>(T[] elids)
		{
			if (null == elids)
				return null;
			return string.Join(", ", elids.Select(elid => elid.ToString()));
		}
		////////////////////////////////////////////////////////////////////////////////
	}
}