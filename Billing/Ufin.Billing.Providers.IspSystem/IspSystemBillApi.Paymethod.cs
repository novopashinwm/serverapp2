﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Compass.Ufin.Billing.Providers.IspSystem.Models;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	public static partial class IspSystemBillApi
	{
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: paymethod, способы оплаты </summary>
		public static async Task<PaymethodList> PaymethodListAsync(IspSystemBillApiContext context,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"paymethod" },
			}, cancellationToken))
				?.ToObject<PaymethodList>();
		}
		////////////////////////////////////////////////////////////////////////////////
	}
}