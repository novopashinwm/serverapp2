﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Compass.Ufin.Billing.Providers.IspSystem.Models;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	public static partial class IspSystemBillApi
	{
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: subaccount, лицевые счета клиентов </summary>
		public static async Task<SubaccountList> SubaccountListAsync(IspSystemBillApiContext context,
			string user, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"subaccount" },
				{ "su",   $@"{user}"     },
			}, cancellationToken))
				?.ToObject<SubaccountList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: subaccount, лицевые счета клиентов </summary>
		public static async Task<SubaccountList> SubaccountListAsync(IspSystemBillApiContext context,
			long accountId, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"subaccount"  },
				{ "elid", $@"{accountId}" },
			}, cancellationToken))
				?.ToObject<SubaccountList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: subaccount.edit, получить детали счета </summary>
		/// <remarks> Без параметра sok методы '.edit' выдают детальную информацию об запрашиваемой сущности </remarks>
		public static async Task<SubaccountEdit> SubaccountDetailAsync(IspSystemBillApiContext context,
			long subaccountId, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"subaccount.edit" },
				{ "elid", $@"{subaccountId}"  },
			}, cancellationToken))
				?.ToObject<SubaccountEdit>();
		}
		////////////////////////////////////////////////////////////////////////////////
	}
}