﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Compass.Ufin.Billing.Providers.IspSystem.Models;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	public static partial class IspSystemBillApi
	{
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: profile, плательщики </summary>
		public static async Task<ProfileList> ProfileListAsync(IspSystemBillApiContext context,
			string su = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"profile" },
				{ "su",   $@"{su}"    },
			}, cancellationToken))
				?.ToObject<ProfileList>();
		}
		////////////////////////////////////////////////////////////////////////////////
	}
}