﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Compass.Ufin.Billing.Core.Enums;
using Compass.Ufin.Billing.Core.Models;
using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Compass.Ufin.Billing.Providers.IspSystem.Models;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	public partial class PaymentProvider
	{
		#region Private Methods

		////////////////////////////////////////////////////////////////////////////////////////
		private IEnumerable<MonitoringObjectItem> GetMonitoringObjectList(string user, string lang = "ru")
		{
			using (var billCtx = GetBillContext(lang))
			{
				return IspSystemBillApi.MonitoringObjectListAsync(billCtx, user)
					?.Result
					?.Items;
			}
		}
		private IEnumerable<MonitoringObjectItem> GetCachedMonitoringObjectList(string user, string lang = "ru")
		{
			return _cache.GetObjectFromCache(
				cacheDataKey:    $@"{lang}_{user}_{nameof(GetMonitoringObjectList)}",
				cacheDataGet:    () => GetMonitoringObjectList(user, lang),
				cacheExpiration: SlowCacheExpiration);
		}
		////////////////////////////////////////////////////////////////////////////////////////
		private ProductItem GetObjectCurrentProductInternal(int monitoringObjectId, string user, string lang = "ru")
		{
			// Получаем текущий тариф, путем фильтрации услуг по параметру
			var monitoringObject = GetCachedMonitoringObjectList(user, lang)
				?.OrderByDescending(e => e.Id)
				?.FirstOrDefault(m => m.MonitoringObjectIdentifier == monitoringObjectId);
			// Если не найден возвращаем тариф по умолчанию (последний созданный тариф без цены)
			if (null == monitoringObject)
				return GetObjectDefaultProductByUser(user, lang);
			// Используем расчет цены, как для не купленного | текущая цена на продукт с текущими скидками
			return GetAccountProductPrice(GetProductFromItemTypeInstance(monitoringObject, lang).WitoutPrices(), user, lang);
		}
		////////////////////////////////////////////////////////////////////////////////////////
		private IEnumerable<ProductItem> GetObjectCurrentProductsInternal(int[] monitoringObjectIds, string user, string lang = "ru")
		{
			var monitoringObjects = GetCachedMonitoringObjectList(user, lang)
				?.OrderByDescending(e => e.Id)
				?.Where(m => 0 == (monitoringObjectIds?.Length ?? 0) || monitoringObjectIds.Contains((int)m.MonitoringObjectIdentifier));
			return monitoringObjects
				?.Select(m =>
				{
					// Используем расчет цены, как для не купленного | текущая цена на продукт с текущими скидками
					return GetAccountProductPrice(GetProductFromItemTypeInstance(m, lang).WitoutPrices(), user, lang);
				})
				?.Where(m => null != m);
		}
		////////////////////////////////////////////////////////////////////////////////////////

		#endregion Private Methods
		#region IPaymentProvider

		public ProductItem GetObjectDefaultProductByUser(string user, string lang = "ru")
		{
			var defaultProduct = GetCachedItemTypeOrderPricelists(ProductType.MonitoringObject, user, lang)
				?.Select(p =>
				{
					return GetProductFromPriceListExport(GetCachedPriceListExportById(p.Pricelist, lang));
				})
				?.Where(p => null != p)
				// Минимальный по цене
				?.OrderBy(p => p.Price?.TotalAmount ?? 0M)
				// Максимальный по идентификатору
				?.ThenByDescending(p => p.Id)
				?.FirstOrDefault();
			if (null != defaultProduct)
			{
				defaultProduct.Id     = 0; // 0 у тарифа по умолчанию, -1 у доступного тарифа, 1 и более у купленного тарифа
				defaultProduct.Status = ProductStatus.Active.ToDictionaryEnum(CultureInfo.GetCultureInfo(lang));
			}
			return defaultProduct;
		}
		public ProductItem GetObjectCurrentProduct(int monitoringObjectId, string user, string lang = "ru")
		{
			return _cache.GetObjectFromCache(
				cacheDataKey:    $@"{lang}_{user}_{nameof(GetObjectCurrentProductInternal)}_{monitoringObjectId}",
				cacheDataGet:    () => GetObjectCurrentProductInternal(monitoringObjectId, user, lang),
				cacheExpiration: SlowCacheExpiration);
		}
		public ProductItem SetObjectCurrentProduct(ProductItem product, int monitoringObjectId, string user, string lang = "ru")
		{
			//////////////////////////////////////////////////////////////////
			if (-1 == (product?.Type?.Id ?? -1))
				throw new ArgumentException("Product pricelist not specified", nameof(product));
			// Не понятно передаются или нет
			if (null == product?.Price?.Period)
				"Product price period not specified".CallTraceError();
			if (string.IsNullOrWhiteSpace(product?.Type?.Base?.Code))
				"Product type not specified".CallTraceError();
			//////////////////////////////////////////////////////////////////
			// 1) Получить текущий тариф
			var currentService = GetObjectCurrentProduct(monitoringObjectId, user, lang);
			if (null == product)
				return currentService;
			//////////////////////////////////////////////////////////////////
			// Если Meta для Addons не переданы, то заполняем их
			if (product?.Addons?.Any(a => null == a.Meta?.ValueType) ?? false)
			{
				var productOrig = GetProductFromPriceListExport(
					GetCachedPriceListExportById(product?.Type?.Id ?? -1), lang);
				if (productOrig != null)
					product.Addons.ForEach(a => a.Meta = a.Meta ?? productOrig?.Addons?.FirstOrDefault(o => a.Code == o.Code)?.Meta);
			}
			//////////////////////////////////////////////////////////////////
			using (var billCtx = GetBillContext(lang))
			{
				// Подготавливаем значения параметров
				var pars = product.Params
					?.Where(p => !string.IsNullOrWhiteSpace(p.Value))
					?.Select(p => new KeyValuePair<string, string>(p.Code, p.Value))
						?? Enumerable.Empty<KeyValuePair<string, string>>();
				// Подготавливаем значения дополнений (содержимого)
				var adds = product.Addons
					?.Where(a => null != a.Data?.ValueUser)
					.Select(a =>
					{
						// Ключ
						var key = $@"addon_{a.Id}";
						// Значение, по умолчанию целое
						var val = a.Data.ValueUser.ToString();
						// Значение, если Boolean, то on|off
						if (a.Meta.ValueType == ProductAddonValueType.Boolean)
							val = 0 == a.Data.ValueUser
								? nameof(OnOff.Off).ToLowerInvariant()
								: nameof(OnOff.On).ToLowerInvariant();
						return new KeyValuePair<string, string>(key, val);
					})
						?? Enumerable.Empty<KeyValuePair<string, string>>();
				var periodId = (long)(product?.Price?.Period.ToIspSystemPeriod() ?? PricePeriod.Monthly.ToIspSystemPeriod());
				if (0 < (currentService?.Id ?? -1))
				{ // Изменить
					if (product.Type.Id != currentService.Type.Id)
					{
						var avlPriceLists = IspSystemBillApi.ServiceChangePriceListPriceListAsync(billCtx, currentService.Id, user)
							?.Result;
						if (null != avlPriceLists?.Error)
							throw new ApplicationException(avlPriceLists.Error.Msg ?? "Unknown error");
						if (0 == (avlPriceLists?.List?.PriceLists?.Count ?? 0))
							throw new ApplicationException("No tariffs available");
						var newPriceList = IspSystemBillApi.ServiceChangePriceListChangePeriodAsync(billCtx, currentService.Id, product.Type.Id, user)
							?.Result;
						if (!(newPriceList?.IsOk ?? false))
							throw new ApplicationException(newPriceList?.Error?.Msg ?? "Unknown error");
					}
					var edtResult = IspSystemBillApi.ItemTypeInstanceEditAsync(billCtx,
						// Ставим константу, чтобы блокировать возможную передачу другого типа
						nameof(ProductType.MonitoringObject).ToLowerInvariant(),
						user,
						currentService.Id,
						new Dictionary<string, string>
						{
							// Пропустить корзину, сразу делать оплату
							{ "skipbasket",  $"on"                              },
							// Если тариф периодический ставим автоматическую пролонгацию через интервал заказа
							{ "autoprolong", $"{(0 < periodId ? periodId : 0)}" },
						}
							.Union(pars) // Добавить параметры
							.Union(adds) // Добавить дополнения (содержимое)
						)
						?.Result;
					if (!(edtResult?.IsOk ?? false))
						throw new ApplicationException(edtResult?.Error?.Msg ?? "Unknown error");
				}
				else
				{ // Заказать
					var ordResult = IspSystemBillApi.ItemTypeInstanceOrderParamAsync(billCtx,
						// Ставим константу, чтобы блокировать возможную передачу другого типа
						nameof(ProductType.MonitoringObject).ToLowerInvariant(),
						user,
						product.Type.Id,
						new Dictionary<string, string>
						{
							// Пропустить корзину, сразу делать оплату
							{ "skipbasket",  $"on"                              },
							// Если тариф периодический ставим автоматическую пролонгацию через интервал заказа
							{ "autoprolong", $"{(0 < periodId ? periodId : 0)}" },
						}
							.Union(pars)  // Добавить параметры
							.Union(adds), // Добавить дополнения (содержимое)
						product.Price.Period.ToIspSystemPeriod())
						?.Result;
					if (!(ordResult?.IsOk ?? false))
						throw new ApplicationException(ordResult?.Error?.Msg ?? "Unknown error");
				}
			}
			// Очищаем кэш текущего тарифа
			_cache
				.Where(i => i.Key.Contains($@"_{user}_{nameof(GetMonitoringObjectList)}"))
				.Select(kv => kv.Key)
				.ToArray()
				.ForEach((k) => _cache.Remove(k));
			_cache
				.Where(i => i.Key.Contains($@"_{currentService?.Type?.Base?.Code?.ToLowerInvariant()}_{nameof(GetItemTypeInstanceList)}"))
				.Select(kv => kv.Key)
				.ToArray()
				.ForEach((k) => _cache.Remove(k));
			_cache
				.Where(i => i.Key.Contains($@"_{currentService?.Type?.Base?.Code?.ToLowerInvariant()}_{nameof(GetProductItemDetail)}_{currentService?.Id}"))
				.Select(kv => kv.Key)
				.ToArray()
				.ForEach((k) => _cache.Remove(k));
			_cache
				.Where(i => i.Key.Contains($@"_{currentService?.Type?.Base?.Code?.ToLowerInvariant()}_{currentService?.Id}"))
				.Select(kv => kv.Key)
				.ToArray()
				.ForEach((k) => _cache.Remove(k));
			// Очищаем кэш перед опросом текущего тарифа (удаляем все для этого пользователя)
			_cache
				.Where(i => i.Key.Contains($@"_{user}_"))
				.Select(kv => kv.Key)
				.ToArray()
				.ForEach((k) => _cache.Remove(k));
			// Очищаем кэш перед опросом текущего тарифа (удаляем все для всех пользователей)
			_cache
				.Where(i => i.Key.Contains($@"__"))
				.Select(kv => kv.Key)
				.ToArray()
				.ForEach((k) => _cache.Remove(k));
			// В результате тоже хотим получить тариф этого объекта уже измененный
			return GetObjectCurrentProduct(monitoringObjectId, user, lang);
		}
		public ProductList GetObjectAvailableProducts(int monitoringObjectId, string user, string lang = "ru")
		{
			var result = new ProductList();
			// Находим текущий
			var currentProduct = GetObjectCurrentProduct(monitoringObjectId, user, lang);
			if (null != currentProduct)
				result.Add(currentProduct);
			var anotherProducts = GetCachedItemTypeOrderPricelists(ProductType.MonitoringObject, user, lang)
				?.Select(p =>
				{
					return GetProductFromPriceListExport(GetCachedPriceListExportById(p.Pricelist, lang));
				})
				?.Where(p => p?.Type?.Id != (currentProduct?.Type?.Id ?? -1))
				?.Select(p =>
				{
					return GetAccountProductPrice(
						product: p,
						user:    user,
						lang:    lang);
				})
				?.OrderByDescending(p => p.Price?.TotalAmount ?? 0)
				?.ToArray();
			if (null != anotherProducts)
				result.AddRange(anotherProducts);
			return result;
		}
		public ProductList GetObjectCurrentProducts(int[] monitoringObjectIds, string user, string lang = "ru")
		{
			var result = new ProductList();
			var ritems = GetObjectCurrentProductsInternal(monitoringObjectIds, user, lang)
				?.ToList();
			if (null != ritems)
				result.AddRange(ritems);
			return result;
		}
		public ProductItem GetObjectDefaultProductByAccountType(AccountType accountType, string lang = "ru")
		{
			var defaultProduct = GetObjectAvailableProductsByAccountType(accountType, lang)
				// Минимальный по цене
				?.OrderBy(p => p.Price?.TotalAmount ?? 0M)
				// Максимальный по идентификатору
				?.ThenByDescending(p => p?.Type?.Id)
				?.FirstOrDefault();
			if (null != defaultProduct)
			{
				defaultProduct.Id     = 0; // 0 у тарифа по умолчанию, -1 у доступного тарифа, 1 и более у купленного тарифа
				defaultProduct.Status = ProductStatus.Active.ToDictionaryEnum(CultureInfo.GetCultureInfo(lang));
			}
			return defaultProduct;
		}
		public ProductList GetObjectAvailableProductsByAccountType(AccountType accountType, string lang = "ru")
		{
			var result = new ProductList();
			var accountTypeName = accountType.ToString().ToLowerInvariant();
			var accountTypeItem = GetCachedAccountGroupList()
				?.FirstOrDefault(g => g.Name.ToLowerInvariant() == accountTypeName);
			var accountTypePrds = GetCachedAvailablePriceListExportList(
				user:        null,
				productType: ProductType.MonitoringObject,
				lang:        lang)
				?.Where(p => p.Access == (accountTypeItem?.Id ?? -1))
				?.Select(p => GetProductFromPriceListExport(p, lang))
				?.ToArray();
			if (null != accountTypePrds)
				result.AddRange(accountTypePrds);
			return result;
		}

		#endregion IPaymentProvider
	}
}