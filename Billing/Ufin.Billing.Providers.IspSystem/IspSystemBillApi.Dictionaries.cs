﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Compass.Ufin.Billing.Providers.IspSystem.Models;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	public static partial class IspSystemBillApi
	{
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: country, страны </summary>
		public static async Task<CountryList> CountryListAsync(IspSystemBillApiContext context,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"country" },
			}, cancellationToken))
				?.ToObject<CountryList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: currency, валюты </summary>
		public static async Task<CurrencyList> CurrencyListAsync(IspSystemBillApiContext context,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"currency" },
			}, cancellationToken))
				?.ToObject<CurrencyList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: locale, локализации </summary>
		public static async Task<LocaleList> LocaleListAsync(IspSystemBillApiContext context,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"locale" },
			}, cancellationToken))
					?.ToObject<LocaleList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: measure, единицы измерения </summary>
		public static async Task<MeasureList> MeasureListAsync(IspSystemBillApiContext context,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"measure" },
			}, cancellationToken))
					?.ToObject<MeasureList>();
		}
		////////////////////////////////////////////////////////////////////////////////
	}
}