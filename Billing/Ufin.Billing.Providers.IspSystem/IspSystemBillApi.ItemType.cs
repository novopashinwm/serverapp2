﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Compass.Ufin.Billing.Core.Enums;
using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Compass.Ufin.Billing.Providers.IspSystem.Models;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	public static partial class IspSystemBillApi
	{
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: itemtype, Типы продуктов </summary>
		public static async Task<ItemTypeList> ItemTypeListAsync(IspSystemBillApiContext context,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func",   $@"itemtype"       },
			}, cancellationToken))
				?.ToObject<ItemTypeList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: itemtype.param Параметры типа продукта </summary>
		public static async Task<ItemTypeParamList> ItemTypeParamListAsync(IspSystemBillApiContext context,
			long itemTypeId, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"itemtype.param"   },
				{ "elid", $@"{itemTypeId}"     },
			}, cancellationToken))
				?.ToObject<ItemTypeParamList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: itemtype.detail Дополнения типа продукта </summary>
		public static async Task<ItemTypeDetailList> ItemTypeDetailListAsync(IspSystemBillApiContext context,
			long itemTypeId, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"itemtype.detail"  },
				{ "elid", $@"{itemTypeId}"     },
			}, cancellationToken))
				?.ToObject<ItemTypeDetailList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: "itemtype" список заказов типа "itemtype" </summary>
		public static async Task<ItemTypeInstanceList> ItemTypeInstanceListAsync(IspSystemBillApiContext context,
			string itemTypeCode, string user = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"{itemTypeCode.ToLowerInvariant()}" },
				{ "su",   $@"{user}"                            },
			}, cancellationToken))
				?.ToObject<ItemTypeInstanceList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: "itemtype.order.pricelist" список доступных для заказа тарифов типа "itemtype" </summary>
		public static async Task<ItemTypeInstanceOrderPricelist> ItemTypeInstanceOrderPricelistsAsync(IspSystemBillApiContext context,
			string itemTypeCode, string user, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"{itemTypeCode.ToLowerInvariant()}.order.pricelist" },
				{ "su",   $@"{user}"                                            },
			}, cancellationToken))
				?.ToObject<ItemTypeInstanceOrderPricelist>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: "itemtype.order.param" рассчитать параметры заказа или применить заказ типа itemtype </summary>
		public static async Task<ItemTypeInstanceOrderResult> ItemTypeInstanceOrderParamAsync(IspSystemBillApiContext context,
			string itemTypeCode,
			string user,
			long pricelistId,
			IEnumerable<KeyValuePair<string, string>> additionValues = null,
			Period periodId = Period.Monthly,
			long datacenterId = 1,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			additionValues = additionValues ?? Enumerable.Empty<KeyValuePair<string, string>>();
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func",        $@"{itemTypeCode.ToLowerInvariant()}.order.param" },
				{ "pricelist",   $@"{pricelistId}"                                 },
				{ "datacenter",  $@"{datacenterId}"                                },
				{ "period",      $@"{(long)periodId}"                              },
				{ "sok",         $@"ok"                                            },
				{ "su",          $@"{user}"                                        },
			}.Union(additionValues), cancellationToken))
				?.ToObject<ItemTypeInstanceOrderResult>();
		}

		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: "itemtype.edit" редактировать услугу типа "itemtype" </summary>
		public static async Task<ItemTypeInstanceEditResult> ItemTypeInstanceEditAsync(IspSystemBillApiContext context,
			string itemTypeCode,
			string user,
			long currentServiceId,
			IEnumerable<KeyValuePair<string, string>> addonValues = null,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			addonValues = addonValues ?? Enumerable.Empty<KeyValuePair<string, string>>();
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"{itemTypeCode.ToLowerInvariant()}.edit" },
				{ "elid", $@"{currentServiceId}"                     },
				{ "sok",  $@"ok"                                     },
				{ "su",   $@"{user}"                                 },
			}.Union(addonValues), cancellationToken))
				?.ToObject<ItemTypeInstanceEditResult>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: "itemtype.edit" получить детали экземпляра тарифного плана типа "itemtype" </summary>
		public static async Task<ItemTypeInstanceDetail> ItemTypeInstanceDetailAsync(IspSystemBillApiContext context,
			string itemTypeCode, long elid, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"{itemTypeCode.ToLowerInvariant()}.edit" },
				{ "elid", $@"{elid}"                                 },
			}, cancellationToken))
				?.ToObject<ItemTypeInstanceDetail>();
		}
		////////////////////////////////////////////////////////////////////////////////
	}
}