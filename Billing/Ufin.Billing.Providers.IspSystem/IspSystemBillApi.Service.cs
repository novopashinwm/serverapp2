﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Compass.Ufin.Billing.Providers.IspSystem.Models;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	public static partial class IspSystemBillApi
	{
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: "service.changepricelist.changeperiod" изменить тарифный план (дополнения по умолчанию для тарифа) </summary>
		public static async Task<ServiceChangePriceListChangePeriodResult> ServiceChangePriceListChangePeriodAsync(IspSystemBillApiContext context,
			long currentServiceId, long newPriceId, string user, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func",      $@"service.changepricelist.changeperiod" },
				{ "elid",      $@"{currentServiceId}"                   },
				{ "pricelist", $@"{newPriceId}"                         },
				{ "sok",       $@"ok"                                   },
				{ "su",        $@"{user}"                               },
			}))
				?.ToObject<ServiceChangePriceListChangePeriodResult>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: "service.changepricelist.pricelist" получить перечень доступных к изменению планов </summary>
		public static async Task<ServiceChangePriceListPriceListResult> ServiceChangePriceListPriceListAsync(IspSystemBillApiContext context,
			long currentServiceId, string user, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func",      $@"service.changepricelist.pricelist" },
				{ "elid",      $@"{currentServiceId}"                },
				{ "su",        $@"{user}"                            },
			}))
				?.ToObject<ServiceChangePriceListPriceListResult>();
		}
	}
}