﻿<?xml version="1.0" encoding="utf-8"?>
<configurationSectionModel xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="2043f8fb-9dff-4e90-b70e-49249c03f2af" namespace="Compass.Ufin.Billing.Providers.IspSystem.Configuration" xmlSchemaNamespace="urn:Compass.Ufin.Billing.Providers.IspSystem.Configuration" xmlns="http://schemas.microsoft.com/dsltools/ConfigurationSectionDesigner">
  <typeDefinitions>
    <externalType name="String" namespace="System" />
    <externalType name="Boolean" namespace="System" />
    <externalType name="Int32" namespace="System" />
    <externalType name="Int64" namespace="System" />
    <externalType name="Single" namespace="System" />
    <externalType name="Double" namespace="System" />
    <externalType name="DateTime" namespace="System" />
    <externalType name="TimeSpan" namespace="System" />
  </typeDefinitions>
  <configurationElements>
    <configurationSection name="IspSystemConfigurationSection" codeGenOptions="Singleton, XmlnsProperty" xmlSectionName="IspSystem">
      <attributeProperties>
        <attributeProperty name="Addr" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="Addr" isReadOnly="true" documentation="Адрес BillManager">
          <type>
            <externalTypeMoniker name="/2043f8fb-9dff-4e90-b70e-49249c03f2af/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="User" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="User" isReadOnly="true" documentation="Имя пользователя для доступа к BillManager">
          <type>
            <externalTypeMoniker name="/2043f8fb-9dff-4e90-b70e-49249c03f2af/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="Pass" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="Pass" isReadOnly="true" documentation="Пароль пользователя для доступа к BillManager">
          <type>
            <externalTypeMoniker name="/2043f8fb-9dff-4e90-b70e-49249c03f2af/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="Proj" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="Proj" isReadOnly="true" documentation="Проект (провайдер) в BillManager">
          <type>
            <externalTypeMoniker name="/2043f8fb-9dff-4e90-b70e-49249c03f2af/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="Verbose" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="Verbose" isReadOnly="false" defaultValue="false">
          <type>
            <externalTypeMoniker name="/2043f8fb-9dff-4e90-b70e-49249c03f2af/Boolean" />
          </type>
        </attributeProperty>
        <attributeProperty name="FastCacheSecond" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="FastCacheSecond" isReadOnly="false" defaultValue="030">
          <type>
            <externalTypeMoniker name="/2043f8fb-9dff-4e90-b70e-49249c03f2af/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="SlowCacheSecond" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="SlowCacheSecond" isReadOnly="false" defaultValue="300">
          <type>
            <externalTypeMoniker name="/2043f8fb-9dff-4e90-b70e-49249c03f2af/Int32" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationSection>
  </configurationElements>
  <propertyValidators>
    <validators />
  </propertyValidators>
</configurationSectionModel>