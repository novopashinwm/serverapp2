﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Compass.Ufin.Billing.Providers.IspSystem.Models;
using Flurl.Http;
using FORIS.TSS.Common.Helpers;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	public partial class PaymentProvider
	{
		#region Private Methods
		private IEnumerable<ProfileItem> GetProfileList(string user = null)
		{
			using (var billCtx = GetBillContext())
			{
				return IspSystemBillApi.ProfileListAsync(billCtx, user)
					?.Result
					?.Items;
			}
		}
		private IEnumerable<ProfileItem> GetCachedProfileList(string user = null)
		{
			return _cache.GetObjectFromCache(
				cacheDataKey:    $@"{user}_{nameof(GetProfileList)}",
				cacheDataGet:    () => GetProfileList(user),
				cacheExpiration: SlowCacheExpiration);
		}
		private IEnumerable<PaymethodItem> GetPaymethodList()
		{
			using (var billCtx = GetBillContext())
			{
				return IspSystemBillApi.PaymethodListAsync(billCtx)
					?.Result
					?.Items;
			}
		}
		private IEnumerable<PaymethodItem> GetCachedPaymethodList()
		{
			return _cache.GetObjectFromCache(
				cacheDataKey:    $@"{nameof(GetPaymethodList)}",
				cacheDataGet:    () => GetPaymethodList(),
				cacheExpiration: SlowCacheExpiration);
		}
		private IEnumerable<PaymentAddPaymethodList.ItemsList.MethodItem> GetPaymentAddPaymethodList(string user = null)
		{
			using (var billCtx = GetBillContext())
			{
				return IspSystemBillApi.PaymentAddPayMethodListAsync(billCtx, user)
					?.Result
					?.Items
					?.Methods;
			}
		}
		private IEnumerable<PaymentAddPaymethodList.ItemsList.MethodItem> GetCachedPaymentAddPaymethodList(string user = null)
		{
			return _cache.GetObjectFromCache(
				cacheDataKey:    $@"{user}_{nameof(GetPaymentAddPaymethodList)}",
				cacheDataGet:    () => GetPaymentAddPaymethodList(user),
				cacheExpiration: SlowCacheExpiration);
		}

		#endregion Private Methods
		#region IPaymentProvider
		public string GetNewPaymentHtml(decimal amount, string user)
		{
			var payer = GetCachedProfileList(user)
				?.FirstOrDefault();
			var allMethods = GetCachedPaymethodList();
			var avlMethods = GetCachedPaymentAddPaymethodList(user);
			var paymethod = allMethods
				?.Join(avlMethods,
					all => all.Id,
					avl => avl.Paymethod,
					(all, avl) =>
						new { sort = all.Orderpriority, all, avl })
				?.OrderBy(m => m.sort)
				?.FirstOrDefault();
			var subaccount = GetCachedActiveSubaccountList(user)
				?.FirstOrDefault();
			using (var billCtx = GetBillContext())
			{
				var payment = IspSystemBillApi.PaymentAdd2Async(billCtx,
					amount,
					paymethod?.avl?.PaymethodCurrency ?? -1,
					paymethod?.avl?.Paymethod ?? -1,
					subaccount?.Id ?? -1)
					?.Result;
				if (null != payment?.Error)
					throw new ApplicationException(payment.Error.Msg ?? "Unknown error");
				var redirect = IspSystemBillApi.PaymentAddRedirectAsync(billCtx, payment?.Model?.PaymentId ?? -1, user)
					?.Result;
				if (null != redirect?.Error)
					throw new ApplicationException(redirect.Error.Msg ?? "Unknown error");
				var payauth = IspSystemBillApi.PaymentSuAsync(billCtx, payment?.Model?.PaymentId ?? -1)
					?.Result;
				if (null != payauth?.Error)
					throw new ApplicationException(payauth.Error.Msg ?? "Unknown error");
				return billCtx
					?.Request(redirect?.Ok?.Value ?? string.Empty)
					?.SetQueryParam("auth", payauth.Auth)
					?.GetAsync(default(CancellationToken))
					?.Result
					?.ResponseMessage
					?.Content
					?.ReadAsStringAsync()
					?.Result;
			}
		}
		#endregion IPaymentProvider
	}
}