﻿using System.Collections.Generic;
using System.Dynamic;
using System.Threading;
using System.Threading.Tasks;
using Compass.Ufin.Billing.Core.Enums;
using Compass.Ufin.Billing.Providers.IspSystem.Models;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	public static partial class IspSystemBillApi
	{
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: account, клиенты </summary>
		public static async Task<AccountList> AccountListAsync(IspSystemBillApiContext context,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"account" },
			}, cancellationToken))
				?.ToObject<AccountList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: accountgroup, группы клиентов </summary>
		public static async Task<AccountGroupList> AccountGroupListAsync(IspSystemBillApiContext context,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"accountgroup" },
			}, cancellationToken))
				?.ToObject<AccountGroupList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: account, клиенты </summary>
		public static async Task<AccountProjectList> AccountProjectListAsync(IspSystemBillApiContext context,
			long accountId, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"account.project" },
				{ "elid", $@"{accountId}"    },
			}, cancellationToken))
				?.ToObject<AccountProjectList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: account.discount, Персональные скидки клиента </summary>
		public static async Task<AccountDiscountList> AccountDiscountListAsync(IspSystemBillApiContext context,
			long accountId, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"account.discount" },
				{ "elid", $@"{accountId}"      },
			}, cancellationToken))
				?.ToObject<AccountDiscountList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: account.discount, Персональные скидки клиента </summary>
		public static async Task<AccountDiscountList> AccountDiscountInfoListAsync(IspSystemBillApiContext context,
			string user, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"account.discountinfo" },
				{ "su",   $@"{user}"               },
			}, cancellationToken))
				?.ToObject<AccountDiscountList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: account.payment (admin), платежи </summary>
		public static async Task<AccountPaymentList> AccountPaymentListAsync(IspSystemBillApiContext context,
			long accountId, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"account.payment" },
				{ "elid", $@"{accountId}"     },
			}, cancellationToken))
				?.ToObject<AccountPaymentList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: account.payment (user), платежи </summary>
		public static async Task<AccountPaymentList> AccountPaymentListAsync(IspSystemBillApiContext context,
			string user, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"payment" },
				{ "su",   $@"{user}"     },
			}, cancellationToken))
				?.ToObject<AccountPaymentList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary>  Функция: register, регистрация клиента  </summary>
		public static async Task<RegisterAddSimpleResult> RegisterAccountSimpleAsync(IspSystemBillApiContext context,
			string email, string passw, string phone, long countryId, AccountType accountType = AccountType.Private, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func",              $@"register"      },
				{ "email",             $@"{email}"       },
				{ "passwd",            $@"{passw}"       },
				{ "realname",          $@"{email}"       },
				{ "AccountType",       $@"{accountType}" },
				{ "phone",             $@"{phone}"       },
				{ "phone_country",     $@"{countryId}"   },
				{ "country_legal",     $@"{countryId}"   },
				{ "postcode_legal",    $@"-"             },
				{ "city_legal",        $@"-"             },
				{ "address_legal",     $@"-"             },
				{ "country_physical",  $@"{countryId}"   },
				{ "postcode_physical", $@"-"             },
				{ "city_physical",     $@"-"             },
				{ "address_physical",  $@"-"             },
				{ "sok",               $@"ok"            },
			}, cancellationToken))
				?.ToObject<RegisterAddSimpleResult>();
		}
		////////////////////////////////////////////////////////////////////////////////
		public static async Task<AccountAddSimpleResult> AccountAddSimpleAsync(IspSystemBillApiContext context,
			string email, string passwd, string phone, long countryId, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func",     $@"account.edit" },
				{ "country",  $@"{countryId}"  },
				{ "email",    $@"{email}"      },
				{ "passwd",   $@"{passwd}"     },
				{ "phone",    $@"{phone}"      },
				{ "sok",      $@"ok"           },
			}, cancellationToken))
				?.ToObject<AccountAddSimpleResult>();
		}
		////////////////////////////////////////////////////////////////////////////////
		///<summary> Получить детали лицевого счета </summary>
		/// <remarks> Без параметра sok методы '.edit' выдают детальную информацию об запрашиваемой сущности </remarks>
		public static async Task<FuncResultList<ExpandoObject>> AccountDetailAsync(IspSystemBillApiContext context,
			long accountId, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func",     $@"account.edit" },
				{ "elid",     $@"{accountId}"  },
			}, cancellationToken))
				?.ToObject<FuncResultList<ExpandoObject>>();
		}
		////////////////////////////////////////////////////////////////////////////////
	}
}