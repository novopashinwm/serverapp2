﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Compass.Ufin.Billing.Providers.IspSystem.Models;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	public static partial class IspSystemBillApi
	{
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: dashboard.info (для уровня User, т.е. информация для самого клиента) </summary>
		public static async Task<AccountDashboardInfoList> AccountDashboardInfoListAsync(IspSystemBillApiContext context,
			string user, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"dashboard.info" },
				{ "su",   $@"{user}"         },
			}, cancellationToken))
				?.ToObject<AccountDashboardInfoList>();
		}
	}
}