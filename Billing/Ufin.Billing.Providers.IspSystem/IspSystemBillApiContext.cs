﻿using System.Collections.Generic;
using System.Net.Http;
using Flurl.Http;
using Flurl.Util;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	/// <summary> Контекст выполнения для методов IspSystem.BillManager </summary>
	public class IspSystemBillApiContext : FlurlClient
	{
		/// <summary> Формат вывода, всегда bjson </summary>
		public const string KEY_OUT      = "out";
		/// <summary> Язык </summary>
		public const string KEY_LANG     = "lang";
		/// <summary> Параметр аутентификации, вызова </summary>
		public const string KEY_AUTHINFO = "authinfo";
		/// <summary> Идентификатор сессии, нельзя использовать с authinfo </summary>
		public const string KEY_AUTH     = "auth";
		/// <summary> Выполнить от имени </summary>
		public const string KEY_SU       = "su";
		/// <summary> Включение и выключение фильтра </summary>
		public const string KEY_FILTER   = "filter";
		/// <summary> Длина страницы фильтра </summary>
		public const string KEY_PCNT     = "p_cnt";
		/// <summary> Словарь параметров, если значение пустое, то параметр игнорируется </summary>
		public Dictionary<string, object> Properties { get; set; } = new Dictionary<string, object>();
		public IspSystemBillApiContext(string baseUrl = null) : base(baseUrl) { }
		public IspSystemBillApiContext(HttpClient httpClient) : base(httpClient) { }
	}
	public static class IspSystemBillApiContextExtensions
	{
		public static IspSystemBillApiContext WithProperty(this IspSystemBillApiContext context,
			string name, object value)
		{
			if (null == value && context.Properties.ContainsKey(name))
				context.Properties.Remove(name);
			else if (null != value)
				context.Properties[name] = value;
			return context;
		}
		public static IspSystemBillApiContext WithProperties(this IspSystemBillApiContext context,
			IEnumerable<KeyValuePair<string, object>> properties)
		{
			if (null == properties)
				return context;
			foreach (var property in properties)
				context.WithProperty(property.Key, property.Value);
			return context;
		}
		public static IspSystemBillApiContext WithProperties(this IspSystemBillApiContext context,
			object properties)
		{
			if (null == properties)
				return context;
			return context.WithProperties(CommonExtensions.ToKeyValuePairs(properties));
		}
	}
}