﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Compass.Ufin.Billing.Core.Enums;
using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Compass.Ufin.Billing.Providers.IspSystem.Models;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	public static partial class IspSystemBillApi
	{
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: pricelist.export, тарифные планы (полные), по набору идентификаторов или все активные в системе </summary>
		/// <remarks>
		/// elid=PROJECT_ID   — Id провайдера в BILLmanager;
		/// onlyavailable     — выводить только доступные для заказа тарифные планы.Может принимать значения "On" и "Off";
		/// pricelist         — Id тарифов через запятую, указывается только если нужно получить данные не всех тарифов;
		/// addonitemtype     — Id типов продуктов дополнений к тарифным планам, указывается, если в выводе нужны не все дополнения;
		/// itemtype          — Id типа продукта тарифов, которые нужно вывести, указывается, когда нужны тарифы только определенного типа;
		/// exclude_pricelist — Id тарифов через запятую, которые нужно исключить из вывода функции;
		/// othercurrency     — ISO код валюты, в которую нужно пересчитать цены тарифов и дополнений,
		/// если требуется вывод в валюте отличной от валюты провайдера.Пересчет идет по текущему курсу в BILLmanager;
		/// ..........................................................................................................
		/// Если указан su, поле Access=0, в остальных случаях указана группа клиентов для которой доступен этот тариф;
		/// itemtype может принимать как число, так и код продукта(intname)
		/// </remarks>
		public static async Task<PriceListExportList> PriceListExportAsync(IspSystemBillApiContext context,
			long[] priceListIds = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func",          $@"pricelist.export"                   },
				{ "pricelist",     $@"{GetElidArrayString(priceListIds)}" },
			}, cancellationToken))
				?.ToObject<PriceListExportList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: pricelist.export, тарифные планы (полные), все доступные для пользователя или все активные </summary>
		/// <remarks>
		/// Если указан пользователь, то выдаются доступные для пользователя, иначе выдаются все.
		/// Если указан конкретный тип продукта, то выдается только этот тип, иначе все.
		/// </remarks>
		public static async Task<PriceListExportList> PriceListExportAsync(IspSystemBillApiContext context,
			string user = null, ProductType? productType = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func",          $@"pricelist.export"                              },
				{ "su",            $@"{user}"                                        },
				{ "itemtype",      $@"{productType?.ToString()?.ToLowerInvariant()}" },
			}, cancellationToken))
				?.ToObject<PriceListExportList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: pricelist, тарифные планы (все тарифные планы, включая выключенные) </summary>
		public static async Task<PriceListList> PriceListAsync(IspSystemBillApiContext context,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"pricelist" },
			}, cancellationToken))
				?.ToObject<PriceListList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: pricelist.detail, тарифы </summary>
		public static async Task<PriceListAddonList> PriceListAddonListAsync(IspSystemBillApiContext context,
			long priceListId, CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func", $@"pricelist.detail" },
				{ "elid", $@"{priceListId}"    },
			}, cancellationToken))
				?.ToObject<PriceListAddonList>();
		}
		////////////////////////////////////////////////////////////////////////////////
		/// <summary> Функция: pricelist.calculate, рассчитать тариф (на уровне User, учитывает скидки) </summary>
		public static async Task<PriceListValues> PriceListCalculateAsync(IspSystemBillApiContext context,
			long   priceListId,
			Period periodId,
			IEnumerable<KeyValuePair<string, string>> addonValues,
			string user = null, // Рассчитать для клиента с его скидками
			CancellationToken cancellationToken = default(CancellationToken))
		{
			return (await AnyPost(context, new Dictionary<string, string>
			{
				{ "func",       $@"pricelist.calculate" },
				{ "pricelist",  $@"{priceListId}"       },
				{ "period",     $@"{(long)periodId}"    },
				{ "su",         $@"{user}"              },
			}.Union(addonValues), cancellationToken))
				?.ToObject<PriceListValues>();
		}
		////////////////////////////////////////////////////////////////////////////////
	}
}