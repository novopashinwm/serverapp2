﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Compass.Ufin.Billing.Core.Enums;
using Compass.Ufin.Billing.Core.Models;
using Compass.Ufin.Billing.Providers.IspSystem.Enums;
using Compass.Ufin.Billing.Providers.IspSystem.Models;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	public static class IspSystemBillApiExtensions
	{
		////////////////////////////////////////////////////////////////////////////////
		public static Period ToIspSystemPeriod(this PricePeriod pricePeriod)
		{
			return (Period)pricePeriod;
		}
		public static ProductItem SetPricesToProduct(this PriceListValues productPrices, ProductItem product)
		{
			if (null != product.Addons)
			{
				foreach (var productAddon in product.Addons)
				{
					var addonValue = productPrices?.Model?.CostDetails
						?.AddonValues
						?.FirstOrDefault(a => a.Id == productAddon.Id);
					if (productAddon.Price == null)
						productAddon.Price = new ProductAddonItemPrice();
					productAddon.Price.PeriodAmount   = addonValue?.PeriodCost    ?? 0M;
					productAddon.Price.PeriodDiscount = addonValue?.Discount      ?? 0M;
					productAddon.Price.SetupAmount    = addonValue?.Setup         ?? 0M;
					productAddon.Price.SetupDiscount  = addonValue?.DiscountSetup ?? 0M;
					productAddon.Price.TotalAmount    = productAddon.Price.SetupAmount   + productAddon.Price.PeriodAmount;
					productAddon.Price.TotalDiscount  = productAddon.Price.SetupDiscount + productAddon.Price.PeriodDiscount;
				}
			}
			if (product.Price == null)
				product.Price = new ProductPriceItem();
			product.Price.PeriodAmount   = productPrices?.Model?.CostDetails?.MainValues?.PeriodCost    ?? 0M;
			product.Price.PeriodDiscount = productPrices?.Model?.CostDetails?.MainValues?.Discount      ?? 0M;
			product.Price.SetupAmount    = productPrices?.Model?.CostDetails?.MainValues?.Setup         ?? 0M;
			product.Price.SetupDiscount  = productPrices?.Model?.CostDetails?.MainValues?.DiscountSetup ?? 0M;
			product.Price.TotalAmount    = 0M
				+ product.Price.SetupAmount
				+ product.Price.PeriodAmount
				+ (null != product.Addons ? product.Addons.Sum(a => a.Price?.TotalAmount   ?? 0M) : 0M);
			product.Price.TotalDiscount  = 0M
				+ product.Price.SetupDiscount
				+ product.Price.PeriodDiscount
				+ (null != product.Addons ? product.Addons.Sum(a => a.Price?.TotalDiscount ?? 0M) : 0M);
			return product;
		}
		////////////////////////////////////////////////////////////////////////////////
		private static readonly Regex parseValueWithUnitsRegex = new Regex(string.Empty
			+ $@"(?<Cost>(\-|\+)?[\s\d]+(?:\.\d+)?)(?<Unit>[^\d\+\-]+)",
			RegexOptions.Singleline | RegexOptions.Compiled);
		////////////////////////////////////////////////////////////////////////////////
		private static readonly Regex removeHtmlTagsRegex = new Regex(string.Empty
			+ $@"<.*?>",
			RegexOptions.Singleline | RegexOptions.Compiled);
		////////////////////////////////////////////////////////////////////////////////
		public static IEnumerable<ValueWithUnits<T>> ParseValueWithUnits<T>(this string valueWithUnits, Func<string, T> parseValue)
		{
			// Предполагаем, что цена начинается с знака или числа, а ед. измерения дальше
			return parseValueWithUnitsRegex.Matches(removeHtmlTagsRegex.Replace(valueWithUnits, string.Empty))
				.OfType<Match>()
				.Select(m =>
				{
					var val = m.Groups["Cost"].Value.Trim();
					var unt = m.Groups["Unit"].Value.Trim();
					var cst = default(T);
					if (null != parseValue)
						cst = parseValue(val);
					return new ValueWithUnits<T>
					{
						Cost = cst,
						Unit = unt,
					};
				});
		}
		////////////////////////////////////////////////////////////////////////////////
	}
}