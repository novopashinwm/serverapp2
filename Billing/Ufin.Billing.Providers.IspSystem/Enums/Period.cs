﻿namespace Compass.Ufin.Billing.Providers.IspSystem.Enums
{
	/// <summary> Триальный: -100, день: -50, вечный: 0. Если значение больше нуля, то длина периода указана в месяцах. </summary>
	public enum Period : int
	{
		Trial        = -100,
		Day          = -050,
		Lifetime     = +000,
		Monthly      = +001,
		Quarterly    = +003,
		Semiannual   = +006,
		Annually     = +012,
		Biennial     = +024,
		Triennial    = +036,
		Quadrennial  = +048,
		Quinquennial = +060,
		Decennial    = +120,
		Max          = int.MaxValue
	}
}