﻿namespace Compass.Ufin.Billing.Providers.IspSystem.Enums
{
	/// <summary> Перечисление для представления Nullable<bool> в терминах Api </summary>
	public enum YesNo
	{
		Undefined,
		Yes,
		No
	};
}