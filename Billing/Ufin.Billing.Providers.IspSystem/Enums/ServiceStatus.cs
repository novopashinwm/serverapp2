﻿namespace Compass.Ufin.Billing.Providers.IspSystem.Enums
{
	/// <summary> Статус услуги </summary>
	public enum ServiceStatus
	{
		/// <summary> 0 - Не указан </summary>
		Unspecified = 0,
		/// <summary> 1 - Заказан </summary>
		Ordered     = 1,
		/// <summary> 2 - Активен </summary>
		Active      = 2,
		/// <summary> 3 - Остановлен </summary>
		Stopped     = 3,
		/// <summary> 4 - Удален </summary>
		Deleted     = 4,
		/// <summary> 5 - Обрабатывается </summary>
		Processing  = 5,
	}
}