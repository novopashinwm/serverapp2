﻿using System;
using System.Collections.Generic;
using System.Linq;
using Compass.Ufin.Billing.Core.Enums;
using Compass.Ufin.Billing.Providers.IspSystem.Models;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;

namespace Compass.Ufin.Billing.Providers.IspSystem
{
	public partial class PaymentProvider
	{
		#region Private Methods
		private IEnumerable<UserItem> GetUserList()
		{
			using (var billCtx = GetBillContext())
			{
				return IspSystemBillApi.UserListAsync(billCtx)
					?.Result
					?.Items ?? Enumerable.Empty<UserItem>();
			}
		}
		private IEnumerable<UserItem> GetCachedUserList()
		{
			return _cache.GetObjectFromCache(
				cacheDataKey:    $@"{nameof(GetUserList)}",
				cacheDataGet:    () => GetUserList(),
				cacheExpiration: SlowCacheExpiration);
		}
		#endregion Private Methods
		#region IPaymentProvider
		public bool UserExists(string user)
		{
			user = user.ToLowerInvariant();
			return GetCachedUserList()
				?.Any(u => u.Name.ToLowerInvariant() == user) ?? false;
		}
		public bool CreateUser(string email, string pass, string phone, AccountType type = AccountType.Private)
		{
			if (string.IsNullOrWhiteSpace(email))
				throw new ApplicationException($@"Email cannot be empty.");
			if (string.IsNullOrWhiteSpace(pass))
				throw new ApplicationException($@"Password cannot be empty.");
			if (string.IsNullOrWhiteSpace(phone))
				throw new ApplicationException($@"Phone cannot be empty.");
			if (!ContactHelper.IsValidEmail(email))
				throw new ApplicationException($@"Email '{email}' is not valid.");
			if (!ContactHelper.IsValidPhone(ContactHelper.GetNormalizedPhone(phone)))
				throw new ApplicationException($@"Phone '{phone}' is not valid.");
			var phoneCountries = GetCachedCountryList()
				.Where(c => phone.StartsWith(c.Phonecode.ToString()))
				.OrderByDescending(c => c.Phonecode)
				.ThenBy(c => c.Id);
			var phoneCountry = phoneCountries
				.FirstOrDefault();
			if (null == phoneCountry && 1 != phoneCountries.Count())
				throw new ApplicationException($@"Unable to accurately identify the country by phone number '{phone}'.");

			using (var billCtx = GetBillContext())
			{
				var accountAddResult = IspSystemBillApi
					.RegisterAccountSimpleAsync(billCtx, email, pass, phone, phoneCountry.Id, type)
						?.Result;
				if (null != accountAddResult?.Error)
					throw new ApplicationException(accountAddResult.Error.Msg ?? "Unknown error");
				// Очищаем кэш перед опросом текущего тарифа (удаляем все для всех пользователей)
				_cache.Where(i => i.Key == $@"{nameof(GetUserList)}")
					.Select(kv => kv.Key)
					.ToArray()
					.ForEach((k) => _cache.Remove(k));
				return null != accountAddResult.Ok;
			}
		}
		#endregion IPaymentProvider
	}
}