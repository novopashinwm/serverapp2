﻿using System;
using System.Configuration.Install;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Windows.Forms;
using CommandLine;
using CommandLine.Text;
using FORIS.TSS.Common;
using FORIS.TSS.ServerLauncher.Application;

namespace Compass.Ufin.Billing.Server.Host
{
	static class Program
	{
		static void Main(string[] args)
		{
			try
			{
				var servicesToRun = new ServiceBase[]
				{
					new BillingServerService(),
				};
				if (Environment.UserInteractive)
				{
					if (Debugger.IsAttached)
					{
						new RunServicesVerb().RunVerb(servicesToRun, "Billing Server");
					}
					else
					{
						var results = Parser.Default.ParseArguments<
							RunServicesVerb,
							InstallServicesVerb,
							UninstallServicesVerb,
							StartServicesVerb,
							StopServicesVerb>(args);
						results
							.WithParsed<RunServicesVerb>      (verb => verb.RunVerb(servicesToRun, "Billing Server"))
							.WithParsed<InstallServicesVerb>  (verb => verb.RunVerb(servicesToRun))
							.WithParsed<UninstallServicesVerb>(verb => verb.RunVerb(servicesToRun))
							.WithParsed<StartServicesVerb>    (verb => verb.RunVerb(servicesToRun))
							.WithParsed<StopServicesVerb>     (verb => verb.RunVerb(servicesToRun))
							.WithNotParsed(                    errs =>
							{
								var consoleAttached = NativeMethods.AttachConsole(-1);
								if (!consoleAttached)
									NativeMethods.AllocConsole();
								var executingAssembly = Assembly.GetExecutingAssembly();
								var helpText = HelpText
									.AutoBuild(results);
								helpText.AutoVersion = false;
								helpText.AutoHelp    = false;
								helpText.Copyright   = executingAssembly.Copyright();
								helpText.Heading     = Environment.GetCommandLineArgs().FirstOrDefault();
								helpText.AdditionalNewLineAfterOption = false;
								helpText
									.AddVerbs(
										typeof(RunServicesVerb),
										typeof(InstallServicesVerb),
										typeof(UninstallServicesVerb),
										typeof(StartServicesVerb),
										typeof(StopServicesVerb));
								Console.Write(helpText.ToString());
								if (!consoleAttached)
								{
									Console.WriteLine("Press \"Enter\" for exit.");
									Console.ReadLine();
								}
							});
					}
				}
				else
					ServiceBase.Run(servicesToRun);
			}
			catch (Exception ex)
			{
				Trace.TraceInformation("Application start error: {0}", ex);
			}
		}
		[Verb("run-services",       HelpText = "Run the services in interactive mode.")]
		internal class RunServicesVerb
		{
			internal void RunVerb(ServiceBase[] servicesToRun, string serverName)
			{
				using (var appContext = new TrayIconApplicationContext(serverName))
				{
					var onStartMethod = typeof(ServiceBase).GetMethod("OnStart", BindingFlags.Instance | BindingFlags.NonPublic);
					var onStopMethod  = typeof(ServiceBase).GetMethod("OnStop",  BindingFlags.Instance | BindingFlags.NonPublic);
					foreach (ServiceBase service in servicesToRun)
					{
						using var logger = Stopwatcher.GetElapsedLogger($"Start '{service.ServiceName}'", null, TimeSpan.Zero);
						onStartMethod.Invoke(service, new object[] { new string[] { } });
					}
					Application.Run(appContext);
					foreach (ServiceBase service in servicesToRun)
					{
						using var logger = Stopwatcher.GetElapsedLogger($"Stop '{service.ServiceName}'", null, TimeSpan.Zero);
						onStopMethod.Invoke(service, null);
					}
				}
			}
		}
		[Verb("install-services",   HelpText = "Install the services.")]
		internal class InstallServicesVerb
		{
			internal void RunVerb(ServiceBase[] servicesToRun)
			{
				if (!NativeMethods.AttachConsole(-1))
					NativeMethods.AllocConsole();
				try
				{
					ManagedInstallerClass.InstallHelper(new string[] { typeof(Program).Assembly.Location });
				}
				finally
				{
					//NativeMethods.FreeConsole();
				}
			}
		}
		[Verb("uninstall-services", HelpText = "Uninstall the services.")]
		internal class UninstallServicesVerb
		{
			internal void RunVerb(ServiceBase[] servicesToRun)
			{
				if (!NativeMethods.AttachConsole(-1))
					NativeMethods.AllocConsole();
				try
				{
					ManagedInstallerClass.InstallHelper(new string[] { "/u", typeof(Program).Assembly.Location });
				}
				finally
				{
					//NativeMethods.FreeConsole();
				}
			}
		}
		[Verb("start-services",     HelpText = "Start the services.")]
		internal class StartServicesVerb
		{
			internal void RunVerb(ServiceBase[] servicesToRun)
			{
				if (!NativeMethods.AttachConsole(-1))
					NativeMethods.AllocConsole();
				try
				{
					foreach (var service in servicesToRun)
					{
						var sc = new ServiceController(service.ServiceName);
						sc.Start();
						sc.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(10));
					}
				}
				finally
				{
					//NativeMethods.FreeConsole();
				}
			}
		}
		[Verb("stop-services",      HelpText = "Stop the services.")]
		internal class StopServicesVerb
		{
			internal void RunVerb(ServiceBase[] servicesToRun)
			{
				if (!NativeMethods.AttachConsole(-1))
					NativeMethods.AllocConsole();
				try
				{
					foreach (var service in servicesToRun)
					{
						var sc = new ServiceController(service.ServiceName);
						sc.Stop();
						sc.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(10));
					}
				}
				finally
				{
					//NativeMethods.FreeConsole();
				}
			}
		}
	}
}