﻿using System.ComponentModel;
using System.Configuration.Install;

namespace Compass.Ufin.Billing.Server
{
	[RunInstaller(true)]
	public partial class ProjectInstaller : Installer
	{
		public ProjectInstaller()
		{
			InitializeComponent();
		}
	}
}