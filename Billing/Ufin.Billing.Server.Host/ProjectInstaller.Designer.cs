﻿namespace Compass.Ufin.Billing.Server
{
	partial class ProjectInstaller
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.serviceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
			this.serviceInstallerBillingServerService = new System.ServiceProcess.ServiceInstaller();
			// 
			// serviceProcessInstaller
			// 
			this.serviceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
			this.serviceProcessInstaller.Password = null;
			this.serviceProcessInstaller.Username = null;
			// 
			// serviceInstallerBillingServerService
			// 
			this.serviceInstallerBillingServerService.DelayedAutoStart = true;
			this.serviceInstallerBillingServerService.DisplayName = "Compass.Ufin.Billing.Server";
			this.serviceInstallerBillingServerService.ServiceName = "Compass.Ufin.Billing.Server";
			this.serviceInstallerBillingServerService.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
			// 
			// ProjectInstaller
			// 
			this.Installers.AddRange(new System.Configuration.Install.Installer[] {
			this.serviceProcessInstaller,
			this.serviceInstallerBillingServerService});

		}

		#endregion

		internal System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller;
		internal System.ServiceProcess.ServiceInstaller serviceInstallerBillingServerService;
	}
}