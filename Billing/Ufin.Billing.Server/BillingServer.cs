﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using FORIS.TSS.Common;

namespace Compass.Ufin.Billing.Server
{
	/// <summary> Сервер биллинга </summary>
	public class BillingServer : MarshalByRefObject, IEntryPoint
	{
		static BillingServer()
		{
			CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("ru-RU");
			CultureInfo.DefaultThreadCurrentCulture.NumberFormat.CurrencyDecimalSeparator = ".";
			CultureInfo.DefaultThreadCurrentCulture.NumberFormat.CurrencyGroupSeparator   = "";
			CultureInfo.DefaultThreadCurrentCulture.NumberFormat.NumberDecimalSeparator   = ".";
			CultureInfo.DefaultThreadCurrentCulture.NumberFormat.NumberGroupSeparator     = "";
			Assembly.Load(typeof(Providers.IspSystem.PaymentProvider).Assembly.GetName());
		}
		/// <summary> Стек размещенных сервисов (чтобы сохранить последовательность запуска и обратной последовательности завершения) </summary>
		private Stack<ServiceHost> _serviceHosts = new Stack<ServiceHost>();
		private void CloseAllServices()
		{
			while (_serviceHosts.Any())
			{
				var _serviceHost = _serviceHosts.Pop();
				if (_serviceHost?.State != CommunicationState.Closed)
					_serviceHost?.Close();
			}
		}
		private void StartAllServices()
		{
			CloseAllServices();
			var services = ServiceModelSectionGroup.GetSectionGroup(
				ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None))
				?.Services
				?.Services
				?.OfType<ServiceElement>() ?? Enumerable.Empty<ServiceElement>();
			var domtypes = AppDomain.CurrentDomain.GetAssemblies()
				.SelectMany(x => x.GetTypes());
			foreach (var service in services)
			{
				var serviceName = service.Name;
				if (string.IsNullOrEmpty(serviceName))
					continue;
				var serviceType = domtypes.FirstOrDefault(t => t.FullName == serviceName);
				if (null == serviceType)
					continue;
				var _serviceHost = new ServiceHost(serviceType);
				_serviceHosts.Push(_serviceHost);
				_serviceHost.Open();
			}
		}
		#region IEntryPoint
		public void Start(bool runLoop, bool activate, bool staticServer)
		{
			using (new Stopwatcher($@"Start '{GetType().FullName}'", null, TimeSpan.Zero))
			{
				StartAllServices();
				Trace.TraceInformation($@"{GetType().FullName} up and running services:");
				foreach (var _serviceHost in _serviceHosts)
				{
					foreach (var endpoint in _serviceHost.Description.Endpoints)
						Trace.TraceInformation($"\t{_serviceHost.Description.ServiceType.FullName}:{endpoint.Address}");
				}
			}
		}
		public void Stop()
		{
			using (new Stopwatcher($@"Stop '{GetType().FullName}'", null, TimeSpan.Zero))
			{
				CloseAllServices();
			}
		}
		#endregion IEntryPoint
	}
}