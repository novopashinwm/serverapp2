﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Compass.Ufin.Orders.Data.Entities;

namespace Compass.Ufin.Orders.Data
{
    public partial class OrdersContext : DbContext
    {
        public OrdersContext()
        {
        }

        public OrdersContext(DbContextOptions<OrdersContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Operator> Operators { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderEvent> OrderEvents { get; set; }
        public virtual DbSet<OrderVehicle> OrderVehicles { get; set; }
        public virtual DbSet<OrderZonegroup> OrderZonegroups { get; set; }
        public virtual DbSet<RoutePoint> RoutePoints { get; set; }
        public virtual DbSet<RoutePointAlert> RoutePointAlerts { get; set; }
        public virtual DbSet<Vehicle> Vehicles { get; set; }
        public virtual DbSet<view_operator_department_right> view_operator_department_rights { get; set; }
        public virtual DbSet<view_operator_right> view_operator_rights { get; set; }
        public virtual DbSet<view_operator_vehicle_right> view_operator_vehicle_rights { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>(entity =>
            {
                entity.HasComment("Таблица клиентов заказа");

                entity.Property(e => e.ClientId).HasComment("Суррогатный ключ клиента заказа");

                entity.Property(e => e.ClientExtId).HasComment("Внешний идентификатор клиента заказа, по информации клиента системы");

                entity.Property(e => e.ClientInn).HasComment("ИНН клиента заказа (часть естественного ключа)");

                entity.Property(e => e.ClientKpp).HasComment("КПП клиента заказа (часть естественного ключа)");

                entity.Property(e => e.ClientName).HasComment("Наименование клиента заказа");

                entity.Property(e => e.CustomerId).HasComment("Идентификатор клиента системы (департамент/корпорант), наименования для одного клиента могут быть разные у двух клиентов системы");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Clients)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CLIENT_DEPARTMENT");
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasIndex(e => e.ExtNumber)
                    .HasName("UIX_EXT_NUMBER")
                    .IsUnique();

                entity.HasOne(d => d.Department)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.DepartmentId)
                    .HasConstraintName("FK_CUSTOMER_DEPARTMENT");
            });

            modelBuilder.Entity<Department>(entity =>
            {
                entity.HasIndex(e => e.ExtId)
                    .HasName("IX_DEPARTMENT_EXTID");

                entity.Property(e => e.IsCommercial).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<Operator>(entity =>
            {
                entity.HasIndex(e => e.Login)
                    .HasName("IX_OPERATOR_UNIQUELOGIN")
                    .IsUnique()
                    .HasFilter("([LOGIN] IS NOT NULL AND [LOGIN]<>'')");

                entity.HasIndex(e => e.Name);

                entity.Property(e => e.TimeZoneInfo).IsUnicode(false);
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.HasComment("Таблица заказов");

                entity.Property(e => e.OrderId).HasComment("Суррогатный ключ заказа");

                entity.Property(e => e.ClientId).HasComment("Ссылка на таблицу клиентов заказов");

                entity.Property(e => e.CreatedAt)
                    .HasDefaultValueSql("([dbo].[utc2lt](getutcdate()))")
                    .HasComment("Дата создания заказа (в секундах от 1970-01-01 UTC)");

                entity.Property(e => e.DeletedAt).HasComment("Дата удаления заказа (в секундах от 1970-01-01 UTC)");

                entity.Property(e => e.OrderExtId).HasComment("Внешний идентификатор заказа, уникальность не проверяем");

                entity.Property(e => e.OrderOwnerId).HasComment("Идентификатор владельца заказа (департамент/корпорант)");

                entity.Property(e => e.OrderWatcherId).HasComment("Ссылка на таблицу операторов системы (наблюдающий, ответственный за заказ)");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ORDER_CLIENT");

                entity.HasOne(d => d.OrderOwner)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.OrderOwnerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ORDER_DEPARTMENT");

                entity.HasOne(d => d.OrderWatcher)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.OrderWatcherId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ORDER_OPERATOR");
            });

            modelBuilder.Entity<OrderEvent>(entity =>
            {
                entity.HasComment("Таблица событий заказов");

                entity.Property(e => e.OrderEventId).HasComment("Суррогатный ключ события заказа");

                entity.Property(e => e.LogTime)
                    .HasDefaultValueSql("([dbo].[utc2lt](getutcdate()))")
                    .HasComment("Метка времени события заказа (в секундах от 1970-01-01 UTC)");

                entity.Property(e => e.Message).HasComment("Текст события заказа");

                entity.Property(e => e.OrderId).HasComment("Ссылка на таблицу заказов");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.OrderEvents)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ORDER_EVENT_ORDER");
            });

            modelBuilder.Entity<OrderVehicle>(entity =>
            {
                entity.HasComment("Таблица связей заказов и объектов наблюдения");

                entity.Property(e => e.OrderVehicleId).HasComment("Суррогатный ключ связи заказов и объектов наблюдения");

                entity.Property(e => e.OrderId).HasComment("Ссылка на таблицу заказов");

                entity.Property(e => e.VehicleId).HasComment("Ссылка на таблицу объектов наблюдения");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.OrderVehicles)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ORDER_VEHICLE_ORDER");

                entity.HasOne(d => d.Vehicle)
                    .WithMany(p => p.OrderVehicles)
                    .HasForeignKey(d => d.VehicleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ORDER_VEHICLE_VEHICLE");
            });

            modelBuilder.Entity<OrderZonegroup>(entity =>
            {
                entity.HasComment("Таблица связей заказа с группой геозон");

                entity.Property(e => e.OrderZonegroupId).HasComment("Суррогатный ключ связи заказа с группой геозон");

                entity.Property(e => e.OrderId).HasComment("Ссылка на таблицу заказов");

                entity.Property(e => e.ZonegroupId).HasComment("Ссылка на таблицу групп геозон");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.OrderZonegroups)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ORDER_ZONEGROUP_ORDER");
            });

            modelBuilder.Entity<RoutePoint>(entity =>
            {
                entity.HasComment("Таблица контрольных точек заказов");

                entity.Property(e => e.RoutePointId).HasComment("Суррогатный контрольной точки заказа");

                entity.Property(e => e.ArrivalLogTime).HasComment("Время прибытия в контрольную точку (в секундах от 1970-01-01 UTC)");

                entity.Property(e => e.Lat).HasComment("Широта контрольной точки");

                entity.Property(e => e.Lng).HasComment("Долгота контрольной точки");

                entity.Property(e => e.Name).HasComment("Наименование контрольной точки");

                entity.Property(e => e.OrderId).HasComment("Ссылка на таблицу заказов");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.RoutePoints)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ROUTE_POINT_ORDER");
            });

            modelBuilder.Entity<RoutePointAlert>(entity =>
            {
                entity.HasComment("Таблица оповещений для контрольных точек");

                entity.Property(e => e.RoutePointAlertId).HasComment("Суррогатный ключ оповещения для контрольной точки");

                entity.Property(e => e.AlertMinutesBeforeArrivalNotInRadius).HasComment("За сколько минут до времени прибытия в контрольную точку уведомить, если объект не внутри радиуса");

                entity.Property(e => e.RadiusInMeters).HasComment("Радиус вокруг контрольной точки");

                entity.Property(e => e.RoutePointId).HasComment("Ссылка на таблицу контрольных точек");

                entity.HasOne(d => d.RoutePoint)
                    .WithMany(p => p.RoutePointAlerts)
                    .HasForeignKey(d => d.RoutePointId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ROUTE_POINT_ALERT_ROUTE_POINT");
            });

            modelBuilder.Entity<Vehicle>(entity =>
            {
                entity.HasIndex(e => e.Department)
                    .HasName("IX_FK_Vehicle_Department");

                entity.Property(e => e.Guard).HasDefaultValueSql("(0)");

                entity.Property(e => e.StoreLog).HasDefaultValueSql("((1))");

                entity.Property(e => e.TimePreparation).HasDefaultValueSql("(0)");

                entity.Property(e => e.ValidatorPresent).HasDefaultValueSql("(1)");

                entity.Property(e => e.VehicleKindId).HasDefaultValueSql("(1)");

                entity.Property(e => e.VehicleStatusId).HasDefaultValueSql("(1)");

                entity.Property(e => e.Vin).IsUnicode(false);

                entity.HasOne(d => d.DepartmentNavigation)
                    .WithMany(p => p.Vehicles)
                    .HasForeignKey(d => d.Department)
                    .HasConstraintName("FK_VEHICLE_DEPARTMENT");
            });

            modelBuilder.Entity<view_operator_department_right>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("v_operator_department_right");
            });

            modelBuilder.Entity<view_operator_right>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("v_operator_rights");
            });

            modelBuilder.Entity<view_operator_vehicle_right>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("v_operator_vehicle_right");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}