﻿using System;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Internal;
using Dto = Compass.Ufin.Orders.Core.Models;
using Orm = Compass.Ufin.Orders.Data.Entities;

namespace Compass.Ufin.Orders.Data
{
	public static class OrdersContextExtensions
	{
		public static Dto::OrderCustomer ToDto(this Orm::Client client)
		{
			return new Dto::OrderCustomer
			{
				OrderCustomerId   = client.ClientExtId,
				OrderCustomerInn  = client.ClientInn,
				OrderCustomerKpp  = client.ClientKpp,
				OrderCustomerName = client.ClientName,
			};
		}
		public static Dto::AlertOutOfRadius ToDto(this Orm::RoutePointAlert alert)
		{
			return new Dto::AlertOutOfRadius
			{
				RadiusInMeters                        = (uint)alert.RadiusInMeters,
				MinutesBeforeDateOfArrivalNotInRadius = (uint)alert.AlertMinutesBeforeArrivalNotInRadius,
				Sms                                   = null,
				Mail                                  = null,
				Push                                  = null,
			};
		}
		public static Dto::OrderRoutePoint ToDto(this Orm::RoutePoint orderRoutePoint)
		{
			return new Dto::OrderRoutePoint
			{
				Lat              = orderRoutePoint.Lat,
				Lng              = orderRoutePoint.Lng,
				Name             = orderRoutePoint.Name,
				Radiuses         = orderRoutePoint.RoutePointAlerts
					?.Select(r => r.ToDto())
					?.ToArray(),
				DateOfArrivalUtc = orderRoutePoint.ArrivalLogTime.ToUtcDateTime(),
			};
		}
		public static Dto::OrderEvent ToDto(this Orm::OrderEvent orderEvent)
		{
			return new Dto::OrderEvent
			{
				OrderEventId           = orderEvent.OrderEventId,
				OrderEventTimestampUtc = orderEvent.LogTime.ToUtcDateTime(),
				OrderEventText         = orderEvent.Message,
			};
		}
		public static Dto::Order ToDto(this Orm::Order order)
		{
			return new Dto::Order
			{
				OrderId                           = order.OrderId,
				ExternalOrderId                   = order.OrderExtId,
				ResponsibleOperatorLogin          = order.OrderWatcher?.Login,
				OrderCustomer                     = order.Client?.ToDto(),
				OrderRoute                        = order.RoutePoints
					?.Select(p => p.ToDto())
					?.ToArray(),
				CreatedAtUtc                      = order.CreatedAt.ToUtcDateTime(),
				DeletedAtUtc                      = order.DeletedAt?.ToUtcDateTime(),
				Events                            = order.OrderEvents
					?.Select(e => e.ToDto())
					?.ToArray(),
				Transporter                       = new Dto::Transporter { ObjectId = order?.OrderVehicles?.FirstOrDefault()?.VehicleId ?? 0 }
			};
		}
		public static EntityEntry<Orm::Order> SaveChanges(this EntityEntry<Orm::Order> entryOrder)
		{
			(entryOrder.Context as OrdersContext)?.SaveChanges();
			return entryOrder;
		}
		public static EntityEntry<Orm::Order> OrderWhatcherUpsert(this EntityEntry<Orm::Order> entryOrder, Dto::OrderBase dtoOrder)
		{
			if (dtoOrder == default)
				throw new ArgumentException("No order specified.");
			if (string.IsNullOrWhiteSpace(dtoOrder.ResponsibleOperatorLogin))
				throw new ArgumentException($"Responsible Operator not specified.", nameof(dtoOrder.ResponsibleOperatorLogin));
			entryOrder
				.Reference(o => o.OrderWatcher)
				.Load();
			var ormOrder = entryOrder.Entity;
			if (ormOrder.OrderWatcher?.Login != dtoOrder.ResponsibleOperatorLogin)
			{
				var ormWatcherOperator = (entryOrder.Context as OrdersContext)
					?.GetOperatorsQuery().FirstOrDefault(o => o.Login == dtoOrder.ResponsibleOperatorLogin);
				if (ormWatcherOperator == default)
					throw new ArgumentException($"Responsible Operator not found by login='{dtoOrder.ResponsibleOperatorLogin}'.", nameof(dtoOrder.ResponsibleOperatorLogin));
				ormOrder.OrderWatcher = ormWatcherOperator;
			}

			return entryOrder;
		}
		public static EntityEntry<Orm::Order> OrderCustomerUpsert(this EntityEntry<Orm::Order> entryOrder, Dto::OrderBase dtoOrder)
		{
			if (dtoOrder == default)
				throw new ArgumentException("No order specified.");
			var dtoOrderCustomer = dtoOrder.OrderCustomer;
			if (dtoOrderCustomer == default)
				throw new ArgumentException("No customer specified for the order.", nameof(dtoOrder.OrderCustomer));
			if (string.IsNullOrWhiteSpace(dtoOrderCustomer.OrderCustomerId))
				throw new ArgumentException("Customer external id not specified.", nameof(dtoOrderCustomer.OrderCustomerId));
			if (string.IsNullOrWhiteSpace(dtoOrderCustomer.OrderCustomerInn))
				throw new ArgumentException("Customer INN not specified.", nameof(dtoOrderCustomer.OrderCustomerInn));
			if (string.IsNullOrWhiteSpace(dtoOrderCustomer.OrderCustomerName))
				throw new ArgumentException("Customer name not specified.", nameof(dtoOrderCustomer.OrderCustomerName));

			entryOrder
				.Reference(o => o.Client)
				.Load();
			var ormOrder = entryOrder.Entity;
			var ormClient = (entryOrder.Context as OrdersContext)
				?.Clients
				?.FirstOrDefault(c =>
					c.CustomerId  == ormOrder.OrderOwnerId &&
					c.ClientExtId == dtoOrderCustomer.OrderCustomerId);
			if (ormClient == default)
				ormClient = new Orm::Client { ClientExtId = dtoOrderCustomer.OrderCustomerId };
			ormClient.CustomerId = ormOrder.OrderOwnerId;
			ormClient.ClientInn  = dtoOrderCustomer.OrderCustomerInn;
			ormClient.ClientKpp  = dtoOrderCustomer.OrderCustomerKpp;
			ormClient.ClientName = dtoOrderCustomer.OrderCustomerName;

			ormOrder.Client = ormClient;

			return entryOrder;
		}
		public static EntityEntry<Orm::Order> TransportersCreate(this EntityEntry<Orm::Order> entryOrder, Dto::OrderBase dtoOrder, int orderCreatorOperatorId)
		{
			if (dtoOrder == default)
				throw new ArgumentException("No order specified.");
			var dtoOrderTransporter = dtoOrder.Transporter;
			if (dtoOrderTransporter == default)
				throw new ArgumentException($"Transporter of order not filled.");

			// Ищем объект наблюдения как собственный для департамента владельца заказа, и если нет, исключение
			var ormVechicle = (entryOrder.Context as OrdersContext)
				?.GetDepartmentVehiclesQuery(entryOrder.Entity.OrderOwnerId)
				?.FirstOrDefault(o => o.VehicleId == dtoOrderTransporter.ObjectId);
			if (ormVechicle == default)
				throw new ArgumentException($"Transporter not found by id='{dtoOrderTransporter.ObjectId}'.", nameof(dtoOrder.Transporter));

			var neededRights = new[] { SystemRight.VehicleAccess, SystemRight.PathAccess, SystemRight.ViewingSensorValues };
			// Проверяем наличие прав на объект перевозчика у оператора наблюдателя
			if (!(entryOrder.Context as OrdersContext).IsAllowedVehicleAll(entryOrder.Entity.OrderWatcher.OperatorId, ormVechicle.VehicleId, neededRights))
				throw new ArgumentException($"Watcher does not have the necessary rights to the object of transporter.", nameof(dtoOrder.Transporter));
			// Проверяем наличие прав на объект перевозчика у оператора наблюдателя
			if (!(entryOrder.Context as OrdersContext).IsAllowedVehicleAll(orderCreatorOperatorId, ormVechicle.VehicleId, neededRights))
				throw new ArgumentException($"Creator does not have the necessary rights to the object of transporter.", nameof(dtoOrder.Transporter));

			(entryOrder.Context as OrdersContext)
				?.Add(new Orm::OrderVehicle { Order = entryOrder.Entity, Vehicle = ormVechicle });

			return entryOrder;
		}
		public static EntityEntry<Orm::Order> TransportersDelete(this EntityEntry<Orm::Order> entryOrder)
		{
			// Если навигационное свойство еще не загружено, загружаем (Load работает если ранее не было загружено свойство)
			entryOrder
				.Collection(o => o.OrderVehicles)
				.Load();
			if (entryOrder.Entity.OrderVehicles.Any())
				(entryOrder.Context as OrdersContext).RemoveRange(entryOrder.Entity.OrderVehicles);
			return entryOrder;
		}
		public static EntityEntry<Orm::Order> OrderRouteCreate(this EntityEntry<Orm::Order> entryOrder, Dto::OrderRoutePoint[] dtoOrderRoutePoints)
		{
			if ((dtoOrderRoutePoints?.Length ?? 0) == 0)
				throw new ArgumentException($"Route of order not filled.");
			foreach (var dtoOrderRoutePoint in dtoOrderRoutePoints)
			{
				if (string.IsNullOrWhiteSpace(dtoOrderRoutePoint.Name))
					throw new ArgumentException("Order point name not specified.", nameof(dtoOrderRoutePoint.Name));
				if (dtoOrderRoutePoint.DateOfArrivalUtc < DateTime.UtcNow)
					throw new ArgumentException("The date of arrival at the point of order must be in the future.", nameof(dtoOrderRoutePoint.DateOfArrivalUtc));

				var entryRoutePoint = (entryOrder.Context as OrdersContext)
					?.Add(new Orm::RoutePoint
					{
						Order          = entryOrder.Entity,
						Lat            = dtoOrderRoutePoint.Lat,
						Lng            = dtoOrderRoutePoint.Lng,
						Name           = dtoOrderRoutePoint.Name,
						ArrivalLogTime = dtoOrderRoutePoint.DateOfArrivalUtc.ToLogTime(),
					});
				foreach (var dtoOrderRoutePointRadius in dtoOrderRoutePoint.Radiuses)
				{
					(entryRoutePoint.Context as OrdersContext)
						?.Add(new Orm::RoutePointAlert
						{
							RoutePoint                           = entryRoutePoint.Entity,
							AlertMinutesBeforeArrivalNotInRadius = (int)dtoOrderRoutePointRadius.MinutesBeforeDateOfArrivalNotInRadius,
							RadiusInMeters                       = (int)dtoOrderRoutePointRadius.RadiusInMeters,
						});
				}
			}
			return entryOrder;
		}
		public static EntityEntry<Orm::Order> OrderRouteDelete(this EntityEntry<Orm::Order> entryOrder)
		{
			entryOrder
				.Collection(o => o.RoutePoints)
				.Load();
			foreach (var ormRoutePoint in entryOrder.Entity.RoutePoints.ToArray())
			{
				(entryOrder.Context as OrdersContext).Entry(ormRoutePoint)
					.Collection(p => p.RoutePointAlerts)
					.Load();
				(entryOrder.Context as OrdersContext).RemoveRange(ormRoutePoint.RoutePointAlerts);
				(entryOrder.Context as OrdersContext).Remove(ormRoutePoint);
			}
			return entryOrder;
		}
		public static EntityEntry<Orm::Order> AlertsCreate(this EntityEntry<Orm::Order> entryOrder, int orderCreatorOperatorId, Dto::OrderRoutePoint[] dtoOrderRoutePoints)
		{
			entryOrder
				.Reference(o => o.Client)
				.Load();
			entryOrder
				.Collection(o => o.OrderZonegroups)
				.Load();
			entryOrder
				.Collection(o => o.RoutePoints)
				.Load();

			var ormOrder = entryOrder.Entity;

			// 1. Добавляем группу зон заказа
			var dtoZoneGroup = (entryOrder.Context as OrdersContext).ZoneGroupCreate4Order(
				orderCreatorOperatorId,
				$"Точки заказа № '{ormOrder.OrderExtId}'.",
				ormOrder.OrderOwnerId,
				ormOrder.OrderWatcher.OperatorId);
			ormOrder.OrderZonegroups.Add(
				new Orm::OrderZonegroup { OrderId = ormOrder.OrderId, ZonegroupId = dtoZoneGroup.Id }
			);
			var ormOrderRoutePoints = ormOrder.RoutePoints
				.OrderBy(p => p.ArrivalLogTime)
				.ToArray();
			// 2. Добавляем зоны заказа
			foreach (var ormOrderRoutePoint in ormOrderRoutePoints)
			{
				var dtoOrderRoutePoint = dtoOrderRoutePoints
					.FirstOrDefault(p =>
						p.Lat                          == ormOrderRoutePoint.Lat  &&
						p.Lng                          == ormOrderRoutePoint.Lng  &&
						p.Name                         == ormOrderRoutePoint.Name &&
						p.DateOfArrivalUtc.ToLogTime() == ormOrderRoutePoint.ArrivalLogTime);
				var ormOrderRoutePointIndex = Array.IndexOf(ormOrderRoutePoints, ormOrderRoutePoint) + 1;
				(entryOrder.Context as OrdersContext).Entry(ormOrderRoutePoint)
					.Collection(p => p.RoutePointAlerts)
					.Load();
				var ormOrderRoutePointAlerts = ormOrderRoutePoint.RoutePointAlerts
					.OrderByDescending(a => a.RadiusInMeters)
					.ToArray();
				foreach (var ormOrderRoutePointAlert in ormOrderRoutePointAlerts)
				{
					var dtoOrderRoutePointAlert = dtoOrderRoutePoint.Radiuses
						.FirstOrDefault(r =>
							r.RadiusInMeters                        == ormOrderRoutePointAlert.RadiusInMeters &&
							r.MinutesBeforeDateOfArrivalNotInRadius == ormOrderRoutePointAlert.AlertMinutesBeforeArrivalNotInRadius);
					// 2.1 Создаем зону заказа
					var dtoZone = (entryOrder.Context as OrdersContext).ZoneCreate4Order(
						orderCreatorOperatorId,
						new GeoZone
						{
							Name = $"Точка № {ormOrderRoutePointIndex} '{ormOrderRoutePoint.Name}', радиус {ormOrderRoutePointAlert.RadiusInMeters} м.",
							Type = ZoneTypes.Circle,
							Points = new[]
							{
								new GeoZonePoint
								{
									Lat    = (float)ormOrderRoutePoint.Lat,
									Lng    = (float)ormOrderRoutePoint.Lng,
									Radius = ormOrderRoutePointAlert.RadiusInMeters,
								}
							}
						},
						ormOrder.OrderOwnerId,
						ormOrder.OrderWatcher.OperatorId);
					// 2.2 Добавляем зону заказа в группу зон заказа
					(entryOrder.Context as OrdersContext).ZoneGroupIncludeZone(
						orderCreatorOperatorId,
						dtoZoneGroup.Id,
						dtoZone.Id);
					// 2.3. Добавляем правила для зоны заказа и перевозчиков
					foreach (var ormOrderVehicle in ormOrder.OrderVehicles)
					{
						// 2.3.1 Добавляем правило перевозчика для зоны заказа, отсутствие в точке
						(entryOrder.Context as OrdersContext).VehicleZoneRule1Create(
							ormOrderRoutePointAlert,
							ormOrderVehicle,
							dtoOrderRoutePointAlert,
							dtoZone,
							$"Заказ № '{ormOrder.OrderExtId}'. Проверка отсутствия в точке № {ormOrderRoutePointIndex} '{ormOrderRoutePoint.Name}', радиус {ormOrderRoutePointAlert.RadiusInMeters} м.");
						// 2.3.2 Добавляем правило перевозчика для зоны заказа, вход в зону
						(entryOrder.Context as OrdersContext).VehicleZoneRule2Create(
							ormOrderRoutePointAlert,
							ormOrderVehicle,
							dtoOrderRoutePointAlert,
							dtoZone,
							$"Заказ № '{ormOrder.OrderExtId}'. Проверка вхождения в точку № {ormOrderRoutePointIndex} '{ormOrderRoutePoint.Name}', радиус {ormOrderRoutePointAlert.RadiusInMeters} м.");
					}
				}
			}
			return entryOrder;
		}
		public static EntityEntry<Orm::Order> AlertsDelete(this EntityEntry<Orm::Order> entryOrder, int orderCreatorOperatorId)
		{
			entryOrder
				.Reference(o => o.Client)
				.Load();
			entryOrder
				.Collection(o => o.OrderZonegroups)
				.Load();
			var ormOrder = entryOrder.Entity;
			foreach (var ormOrderZonegroup in ormOrder.OrderZonegroups)
			{
				var dtoZones = (entryOrder.Context as OrdersContext).GetGeoZonesByGroup(orderCreatorOperatorId, ormOrderZonegroup.ZonegroupId);
				foreach (var dtoZone in dtoZones)
				{
					// 1. Удаляем правила для зоны и перевозчиков
					foreach (var ormOrderVehicle in ormOrder.OrderVehicles)
					{
						// 1.1 Удаляем правила перевозчика для зоны заказа
						(entryOrder.Context as OrdersContext).VehicleZoneRulesDelete(
							ormOrder.OrderWatcherId, ormOrderVehicle.VehicleId, dtoZone.Id);
					}
					// 2. Удаляем зону заказа
					(entryOrder.Context as OrdersContext).ZoneDelete(orderCreatorOperatorId, dtoZone.Id);
				}
				// 3. Удаляем группу зон заказа
				(entryOrder.Context as OrdersContext).ZoneGroupDelete(orderCreatorOperatorId, ormOrderZonegroup.ZonegroupId);
			}
			return entryOrder;
		}
	}
}