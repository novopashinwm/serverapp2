﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Dto = Compass.Ufin.Orders.Core.Models;
using Orm = Compass.Ufin.Orders.Data.Entities;

namespace Compass.Ufin.Orders.Data
{
	public partial class OrdersContext
	{
		/// <summary> Получить шаблон запроса для поиска заказов </summary>
		/// <param name="orderOwnerId"> Идентификатор владельца заказа (департамента) </param>
		/// <returns> Шаблон запроса AsQueryable </returns>
		public IQueryable<Orm.Order> GetOrdersQuery(int? orderOwnerId, int? orderWhatcherId) =>
			Orders
				?.Include(o => o.OrderOwner)
				?.Include(o => o.OrderWatcher)
				?.Include(o => o.OrderVehicles)
				?.Include(o => o.RoutePoints)
					?.ThenInclude(p => p.RoutePointAlerts)
				?.Include(o => o.Client)
				?.Include(o => o.OrderEvents)
				?.Include(o => o.OrderZonegroups)
				?.Where(o => o.OrderOwnerId == orderOwnerId || o.OrderWatcherId == orderWhatcherId);
		/// <summary> Получить шаблон запроса для поиска объектов наблюдения департамента </summary>
		/// <param name="vehicleDepartmentId"> Идентификатор департамента(он же владелец заказа) </param>
		/// <returns> Шаблон запроса AsQueryable </returns>
		public IQueryable<Orm.Vehicle> GetDepartmentVehiclesQuery(int vehicleDepartmentId) =>
			Vehicles
				?.Where(v => v.Department == vehicleDepartmentId);
		/// <summary> Получить шаблон запроса для поиска операторов </summary>
		/// <returns> Шаблон запроса AsQueryable </returns>
		public IQueryable<Orm.Operator> GetOperatorsQuery() => Operators.AsQueryable();
	}
}