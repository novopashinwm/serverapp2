﻿using System;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;
using Dt1 = Compass.Ufin.Orders.Core.Models;
using Dt2 = FORIS.TSS.BusinessLogic.DTO;
using Enm = FORIS.TSS.BusinessLogic.Enums;
using Old = FORIS.TSS.EntityModel;
using Orm = Compass.Ufin.Orders.Data.Entities;
using Rht = FORIS.TSS.BusinessLogic.DTO.OperatorRights;
using Rul = FORIS.TSS.BusinessLogic.DTO.Rules;

namespace Compass.Ufin.Orders.Data
{
	public partial class OrdersContext
	{
		public Dt2::Department GetDefaultDepartmentByOperatorId(int operatorId)
		{
			using var entities = new Old::Entities();
			return entities.GetDefaultDepartmentByOperatorId(operatorId)
				?.ToDto();
		}
		public Rht::Operator GetOperatorByLogin(string login)
		{
			using var entities = new Old::Entities();
			return entities.GetOperatorOrDefault(login)
				?.ToDto();
		}
		public bool IsAllowedVehicleAll(int operatorId, int vehicleId, params SystemRight[] rights)
		{
			using var entities = new Old::Entities();
			return entities.IsAllowedVehicleAll(operatorId, vehicleId, rights);
		}
		public bool IsAllowedVehicleAny(int operatorId, int vehicleId, params SystemRight[] rights)
		{
			using var entities = new Old::Entities();
			return entities.IsAllowedVehicleAny(operatorId, vehicleId, rights);
		}
		public Dt2::Group ZoneGroupCreate4Order(int operatorId, string name, int? departmentId, int orderWatcherOperatorId)
		{
			using var entities = new Old::Entities();
			// Создаем группу зон
			var ormZoneGroup = entities.ZoneGroupCreate(operatorId, name, departmentId);
			var ormWatcher   = entities.GetOperator(orderWatcherOperatorId, nameof(Old::OPERATOR.OPERATOR_ZONEGROUP));
			// Добавляем права на группу зон наблюдателю
			entities.Allow(ormWatcher, ormZoneGroup, Enm::UserRole.DefaultZoneGroupAccessRights);
			entities.SaveChangesByOperator(operatorId);
			return ormZoneGroup.ToDto();
		}
		public void ZoneGroupDelete(int operatorId, int zoneGroupId)
		{
			using var entities = new Old::Entities();
			entities.ZoneGroupDelete(operatorId, zoneGroupId);
			entities.SaveChangesByOperator(operatorId);
		}
		public Dt2::GeoZone[] GetGeoZonesByGroup(int operatorId, int geoZoneGroupId)
		{
			using var entities = new Old::Entities();
			return entities.GetGeoZonesByGroup(operatorId, geoZoneGroupId)
				.Select(z => new Dt2::GeoZone
				{
					Id          = z.ZONE_ID,
					Name        = z.NAME,
					Description = z.DESCRIPTION,
				})
				.ToArray();
		}
		public Dt2::GeoZone ZoneCreate4Order(int operatorId, Dt2::GeoZone geoZone, int? departmentId, int orderWatcherOperatorId)
		{
			using var entities = new Old::Entities();
			using var transact = new Old::Transaction();
			// Создаем зону
			var ormGeoZone  = entities.GeoZoneCreate(operatorId, geoZone, departmentId);
			entities.SaveChangesByOperator(operatorId);
			// Создаем матрицу геозоны для поиска ответа на вопрос о вхождении в зону
			entities.ZoneMatrixUpsert(ormGeoZone.ZONE_ID, geoZone.GetMatrixNodes());
			entities.SaveChangesByOperator(operatorId);
			// Получаем права оператора, для выдачи прав на зону
			var ormOperator = entities.GetOperator(orderWatcherOperatorId, nameof(Old::OPERATOR.OPERATOR_ZONE));
			// Добавляем права на зону наблюдателю
			entities.Allow(ormOperator, ormGeoZone, Enm::UserRole.DefaultZoneAccessRights);
			entities.SaveChangesByOperator(operatorId);
			transact.Complete();
			geoZone.Id = ormGeoZone.ZONE_ID;
			return geoZone;
		}
		public void ZoneDelete(int operatorId, int zoneId)
		{
			using var entities = new Old::Entities();
			entities.GeoZoneDelete(operatorId, zoneId);
			entities.SaveChangesByOperator(operatorId);
		}
		public void ZoneGroupIncludeZone(int operatorId, int zoneGroupId, int zoneId)
		{
			using var entities = new Old::Entities();
			entities.ZoneGroupIncludeZone(operatorId, zoneGroupId, zoneId);
			entities.SaveChangesByOperator(operatorId);
		}
		private static Rul.RuleSubscription GetRuleSubscription(
			Orm::Order ormOrder,
			Dt1::AlertOutOfRadius dtoRoutePointAlert,
			Old::Entities entities)
		{
			var ruleSubscription = new Rul::RuleSubscription
			{
				OperatorId        = ormOrder.OrderWatcherId,
				ToOwnPhoneMaxQty  = (int?)dtoRoutePointAlert.Sms.MaxCount,
				ToOwnEmailMaxQty  = (int?)dtoRoutePointAlert.Mail.MaxCount,
				ToAppClientMaxQty = (int?)dtoRoutePointAlert.Push.MaxCount,
			};

			ruleSubscription.ToAppClient = ruleSubscription.ToAppClientMaxQty > 0;

			ruleSubscription.PhoneIds = entities.GetOperatorPhones(ormOrder.OrderWatcherId)
				.Where(p => p.Id.HasValue && (p.IsConfirmed ?? false) && ruleSubscription.ToOwnPhoneMaxQty > 0)
				.Select(p => p.Id.Value)
				.ToArray();
			ruleSubscription.ToOwnPhone = ruleSubscription.PhoneIds.Length > 0;

			ruleSubscription.EmailIds = entities.GetOperatorEmails(ormOrder.OrderWatcherId)
				.Where(e => e.Id.HasValue && (e.IsConfirmed ?? false) && ruleSubscription.ToOwnEmailMaxQty > 0)
				.Select(e => e.Id.Value)
				.ToArray();
			ruleSubscription.ToOwnEmail = ruleSubscription.EmailIds.Length > 0;

			return ruleSubscription;
		}
		public void VehicleZoneRule1Create(
			Orm::RoutePointAlert  ormRoutePointAlert,
			Orm::OrderVehicle     ormOrderVehicle,
			Dt1::AlertOutOfRadius dtoRoutePointAlert,
			Dt2::GeoZone dtoZone,
			string description)
		{
			var ormPoint  = ormRoutePointAlert.RoutePoint;
			var ormOrder  = ormPoint.Order;

			using var entities = new Old::Entities();

			var ruleSubscription = GetRuleSubscription(
				ormOrder,
				dtoRoutePointAlert,
				entities);

			var zoneCondition = new Rul::ZoneCondition
			{
				ZoneId     = dtoZone.Id,
				ZoneIdType = IdType.Zone,
				EventType  = Enm::ZoneEventType.OutOfZone,
			};
			var timePointCondition = new Rul::TimePointCondition
			{
				LogTime = ormPoint.ArrivalLogTime - ormRoutePointAlert.AlertMinutesBeforeArrivalNotInRadius * 60,
			};
			var rule = new Rul::Rule
			{
				Subscriptions       = new[] { ruleSubscription },
				SensorConditions    = Array.Empty<Rul::SensorCondition>(),
				ZoneConditions      = new[] { zoneCondition },
				TimePointConditions = new[] { timePointCondition },
				Enabled             = true,
				Description         = description,
				ObjectId            = ormOrderVehicle.VehicleId,
				ObjectIdType        = IdType.Vehicle,
				OrderId             = ormOrder.OrderId,
				ProcessingEndAt     = ormPoint.ArrivalLogTime,
			};
			entities.AddOrUpdateRule(ormOrder.OrderWatcherId, rule);
			entities.SaveChangesByOperator(ormOrder.OrderWatcherId);
		}
		public void VehicleZoneRule2Create(
			Orm::RoutePointAlert  ormRoutePointAlert,
			Orm::OrderVehicle ormOrderVehicle,
			Dt1::AlertOutOfRadius dtoRoutePointAlert,
			Dt2::GeoZone dtoZone,
			string description)
		{
			var ormPoint  = ormRoutePointAlert.RoutePoint;
			var ormOrder  = ormPoint.Order;

			using var entities = new Old::Entities();

			var ruleSubscription = GetRuleSubscription(
				ormOrder,
				dtoRoutePointAlert,
				entities);

			var zoneCondition = new Rul::ZoneCondition
			{
				ZoneId     = dtoZone.Id,
				ZoneIdType = IdType.Zone,
				EventType  = Enm::ZoneEventType.Incoming,
			};
			var rule = new Rul::Rule
			{
				Subscriptions       = new[] { ruleSubscription },
				SensorConditions    = Array.Empty<Rul::SensorCondition>(),
				ZoneConditions      = new[] { zoneCondition },
				TimePointConditions = Array.Empty<Rul::TimePointCondition>(),
				Enabled             = true,
				Description         = description,
				ObjectId            = ormOrderVehicle.VehicleId,
				ObjectIdType        = IdType.Vehicle,
				OrderId             = ormOrder.OrderId,
				ProcessingEndAt     = ormPoint.ArrivalLogTime + (24*60*60),
			};
			entities.AddOrUpdateRule(ormOrder.OrderWatcherId, rule);
			entities.SaveChangesByOperator(ormOrder.OrderWatcherId);
		}
		public void VehicleZoneRulesDelete(int operatorId, int vehicleId, int zoneId)
		{
			using var entities = new Old::Entities();
			var compoundRules = entities
				.GetVehicleZoneCompoundRules(operatorId, vehicleId, zoneId)
				.ToArray();
			foreach (var compoundRule in compoundRules)
				entities.DeleteCompoundRule(compoundRule);
			entities.SaveChangesByOperator(operatorId);
		}
	}
}