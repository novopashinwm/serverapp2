﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Compass.Ufin.Orders.Data.Entities
{
    [Table("DEPARTMENT")]
    public partial class Department
    {
        public Department()
        {
            Clients = new HashSet<Client>();
            Customers = new HashSet<Customer>();
            Orders = new HashSet<Order>();
            Vehicles = new HashSet<Vehicle>();
        }

        [Key]
        [Column("DEPARTMENT_ID")]
        public int DepartmentId { get; set; }
        [Required]
        [Column("NAME")]
        [StringLength(255)]
        public string Name { get; set; }
        [Column("ExtID")]
        [StringLength(255)]
        public string ExtId { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        [Column("Country_ID")]
        public int? CountryId { get; set; }
        public int? Type { get; set; }
        [Required]
        public bool? IsCommercial { get; set; }
        public bool ViugaUserExists { get; set; }
        [Column("Billing_Service_Provider_ID")]
        public int? BillingServiceProviderId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LockDate { get; set; }

        [InverseProperty(nameof(Client.Customer))]
        public virtual ICollection<Client> Clients { get; set; }
        [InverseProperty(nameof(Customer.Department))]
        public virtual ICollection<Customer> Customers { get; set; }
        [InverseProperty(nameof(Order.OrderOwner))]
        public virtual ICollection<Order> Orders { get; set; }
        [InverseProperty(nameof(Vehicle.DepartmentNavigation))]
        public virtual ICollection<Vehicle> Vehicles { get; set; }
    }
}