﻿using System;
using System.Linq;
using System.Security;
using Compass.Ufin.Orders.Core;
using Compass.Ufin.Orders.Core.Models;
using Compass.Ufin.Orders.Data;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using Microsoft.EntityFrameworkCore;
using Dto = Compass.Ufin.Orders.Core.Models;
using Orm = Compass.Ufin.Orders.Data.Entities;

namespace Compass.Ufin.Orders.Services
{
	public class OrdersService : IOrdersManagement
	{
		private readonly OrdersContext _dataContext;
		public OrdersService(string connectionString)
			: this(new OrdersContext(new DbContextOptionsBuilder<OrdersContext>().UseSqlServer(connectionString).Options))
		{
		}
		public OrdersService(OrdersContext context)
		{
			_dataContext = context;
		}
		public int? DepartmentId { get; set; }
		public int? OperatorId   { get; set; }
		private IQueryable<Orm::Order>    GetOrdersQuery()             => _dataContext.GetOrdersQuery(DepartmentId, OperatorId);
		private IQueryable<Orm::Operator> GetOperatorsQuery()          => _dataContext.Operators.AsQueryable();
		private IQueryable<Orm::Vehicle>  GetDepartmentVehiclesQuery() => _dataContext.GetDepartmentVehiclesQuery(DepartmentId.Value);
		private void OperationPermissionsDepartmentCheck()
		{
			if (!DepartmentId.HasValue)
				throw new SecurityException($"Access forbidden for operation");
		}
		private void OperationPermissionsOperatorCheck()
		{
			if (!OperatorId.HasValue)
				throw new SecurityException($"Access forbidden for operation");
		}
		private void DtoOrderRequestCheck(OrderBase request)
		{
			if (request == default)
				throw new ArgumentException($"The request to create an order cannot be missing.", nameof(request));
			if (string.IsNullOrWhiteSpace(request.ExternalOrderId))
				throw new ArgumentException($"Order external identifier not specified.", nameof(request.ExternalOrderId));
			//////////////////////////////////////////////////
			if (string.IsNullOrWhiteSpace(request.ResponsibleOperatorLogin))
				throw new ArgumentException($"Responsible Operator not specified.", nameof(request.ResponsibleOperatorLogin));
			//////////////////////////////////////////////////
			if (request.OrderCustomer == default)
				throw new ArgumentException($"No customer specified for the order.", nameof(request.OrderCustomer));
			//////////////////////////////////////////////////
			if (0 == (request?.OrderRoute?.Length ?? 0))
				throw new ArgumentException($"The route of the order is not filled.", nameof(request.OrderRoute));
			if (request.Transporter == default)
				throw new ArgumentException($"The vehicle of execution of the order is not specified.", nameof(request.Transporter));
		}
		public Dto::Order[] GetOrders(DateTime intervalBegUtc, DateTime intervalEndUtc, Pagination pagination)
		{
			// Проверка прав на исполнение операции
			OperationPermissionsOperatorCheck();

			var logTimeBeg = intervalBegUtc.ToLogTime();
			var logTimeEnd = intervalEndUtc.ToLogTime();
			var ordersQuery = GetOrdersQuery()
				?.AsNoTracking()
				?.Where(o => logTimeBeg <= o.CreatedAt && o.CreatedAt <= logTimeEnd);

			// Подсчет сколько всего записей
			if (pagination?.ComputeTotal ?? false)
				pagination.TotalRecords = ordersQuery.Count();

			// Изменить запрос на выборку части данных
			ordersQuery = ordersQuery.OrderBy(e => e.OrderId);
			if (pagination != null)
				ordersQuery = ordersQuery
					.Skip(pagination.Skip)
					.Take(pagination.RecordsPerPage);

			// Сделать выборку
			var results = ordersQuery
				?.Select(o => o.ToDto())
				?.ToArray();

			// Заполнить количество выбранных данных
			if (null != pagination)
				pagination.TotalPageRecords = results.Count();

			// Выдать результаты
			return results;
		}
		public Dto::Order GetOrder(long orderId)
		{
			// Проверка прав на исполнение операции
			OperationPermissionsOperatorCheck();

			return GetOrdersQuery()
				?.AsNoTracking()
				?.FirstOrDefault(o => o.OrderId == (int)orderId)
				?.ToDto();
		}
		public Dto::Order GetOrderByExternalId(string externalOrderId)
		{
			// Проверка прав на исполнение операции
			OperationPermissionsOperatorCheck();

			return GetOrdersQuery()
				?.AsNoTracking()
				?.FirstOrDefault(o => o.OrderExtId == externalOrderId)
				?.ToDto();
		}
		public Dto::OrderCreationResponse Create(Dto::OrderCreationRequest request)
		{
			// Проверка прав на исполнение операции
			OperationPermissionsDepartmentCheck();
			OperationPermissionsOperatorCheck();

			// Проверяем данные запроса без использования БД (заполненность полей)
			DtoOrderRequestCheck(request);
			// Ищем заказ по ExternalOrderId, и если такой уже есть выдаем исключение
			var ormOrder = GetOrdersQuery()?.FirstOrDefault(o => o.OrderExtId == request.ExternalOrderId);
			if (ormOrder != default)
				throw new ArgumentException($"Order already exists by Id='{request.ExternalOrderId}'.", nameof(request.ExternalOrderId));

			// Создаем заказ
			ormOrder = new Orm::Order
			{
				OrderOwnerId = DepartmentId.Value,
				OrderExtId   = request.ExternalOrderId,
			};
			// Заполняем связанные сущности
			_dataContext.Add(ormOrder)
				?.OrderWhatcherUpsert(request)
				?.OrderCustomerUpsert(request)
				?.TransportersCreate(request, OperatorId.Value)
				?.OrderRouteCreate(request.OrderRoute)
				?.SaveChanges();

			// Создаем оповещения
			_dataContext.Entry(ormOrder)
				?.AlertsCreate(OperatorId.Value, request.OrderRoute)
				?.SaveChanges();
			/////////////////////////////////////////////////////////////
			return new Dto::OrderCreationResponse
			{
				OrderId      = ormOrder.OrderId,
				CreatedAtUtc = ormOrder.CreatedAt.ToUtcDateTime(),
			};
		}
		public Dto::Order Update(Dto::OrderUpdateRequest request)
		{
			// Проверка прав на исполнение операции
			OperationPermissionsDepartmentCheck();
			OperationPermissionsOperatorCheck();

			// Проверяем данные запроса без использования БД (заполненность полей)
			DtoOrderRequestCheck(request);
			// Ищем заказ по OrderId, и если такого нет выдаем исключение
			var ormOrder = GetOrdersQuery().FirstOrDefault(o => o.OrderId == request.OrderId);
			if (ormOrder == null)
				throw new ArgumentException($"Order not found by OrderId='{request.OrderId}'.", nameof(request.OrderId));

			// Удаляем оповещения связанные данные
			_dataContext.Entry(ormOrder)
				?.AlertsDelete(OperatorId.Value)
				?.SaveChanges();

			// Удаляем связанные данные
			_dataContext.Entry(ormOrder)
				?.OrderRouteDelete()
				?.TransportersDelete()
				?.SaveChanges();

			// Обновляем заказ
			ormOrder.OrderOwnerId = DepartmentId.Value;
			ormOrder.OrderExtId   = request.ExternalOrderId;

			// Заполняем связанные сущности
			_dataContext.Entry(ormOrder)
				?.OrderWhatcherUpsert(request)
				?.OrderCustomerUpsert(request)
				?.TransportersCreate(request, OperatorId.Value)
				?.OrderRouteCreate(request.OrderRoute)
				?.SaveChanges();

			// Создаем оповещения
			_dataContext.Entry(ormOrder)
				?.AlertsCreate(OperatorId.Value, request.OrderRoute)
				?.SaveChanges();

			/////////////////////////////////////////////////////////////
			// TODO: Ставить в очередь на обработку
			return ormOrder.ToDto();
		}
		public bool Delete(long orderId)
		{
			// Проверка прав на исполнение операции
			OperationPermissionsDepartmentCheck();
			OperationPermissionsOperatorCheck();

			var ormOrder = GetOrdersQuery()
				.FirstOrDefault(o => o.OrderId == (int)orderId);
			if (ormOrder == null)
				return false;
			ormOrder.DeletedAt = DateTime.UtcNow.ToLogTime();
			// Помечаем как удаленный
			_dataContext.Entry(ormOrder)
				.SaveChanges();
			// Удаляем оповещения
			_dataContext.Entry(ormOrder)
				?.AlertsDelete(OperatorId.Value)
				?.SaveChanges();
			return true;
		}
	}
}