﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Compass.Ufin.Orders.Core.Models
{
	[Serializable]
	[DataContract]
	public partial class OrderRoutePoint
	{
		[DataMember]           public string             Name             { get; set; }
		[DataMember][Required] public decimal            Lat              { get; set; }
		[DataMember][Required] public decimal            Lng              { get; set; }
		[DataMember][Required] public DateTime           DateOfArrivalUtc { get; set; }
		[DataMember][Required] public AlertOutOfRadius[] Radiuses         { get; set; }
	}
}