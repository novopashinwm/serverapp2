﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Compass.Ufin.Orders.Core.Models
{
	[Serializable]
	[DataContract]
	public partial class AlertOutOfRange
	{
		[DataMember][Required]  public int        Min  { get; set; }
		[DataMember][Required]  public int        Max  { get; set; }
		[DataMember]            public AlertRange Sms  { get; set; }
		[DataMember]            public AlertRange Mail { get; set; }
		[DataMember]            public AlertRange Push { get; set; }
	}

}