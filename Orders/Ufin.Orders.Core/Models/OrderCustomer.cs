﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Compass.Ufin.Orders.Core.Models
{
	[Serializable]
	[DataContract]
	public partial class OrderCustomer
	{
		[DataMember][Required] public string OrderCustomerId   { get; set; }
		[DataMember][Required] public string OrderCustomerInn  { get; set; }
		[DataMember]           public string OrderCustomerKpp  { get; set; }
		[DataMember][Required] public string OrderCustomerName { get; set; }
	}
}