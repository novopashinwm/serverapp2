﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Compass.Ufin.Orders.Core.Models
{
	[Serializable]
	[DataContract]
	public partial class OrderBase
	{
		[DataMember][Required] public string            ExternalOrderId          { get; set; }
		[DataMember][Required] public string            ResponsibleOperatorLogin { get; set; }
		[DataMember][Required] public Transporter       Transporter              { get; set; }
		[DataMember][Required] public OrderCustomer     OrderCustomer            { get; set; }
		[DataMember][Required] public OrderRoutePoint[] OrderRoute               { get; set; }
		[DataMember]           public AlertOutOfRange   TemperatureRangeAlert    { get; set; }
	}
}