﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Compass.Ufin.Orders.Core.Models
{
	[Serializable]
	[DataContract]
	public partial class OrderEvent
	{
		[DataMember][Required] public long     OrderEventId           { get; set; }
		[DataMember][Required] public DateTime OrderEventTimestampUtc { get; set; }
		[DataMember][Required] public string   OrderEventText         { get; set; }
	}
}