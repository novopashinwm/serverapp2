﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Compass.Ufin.Orders.Core.Models
{
	[Serializable]
	[DataContract]
	public partial class Transporter
	{
		[DataMember][Required] public int ObjectId { get; set; }
	}
}