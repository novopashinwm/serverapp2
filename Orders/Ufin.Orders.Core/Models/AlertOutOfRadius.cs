﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Compass.Ufin.Orders.Core.Models
{
	[Serializable]
	[DataContract]
	public partial class AlertOutOfRadius
	{
		[DataMember][Required] public uint       RadiusInMeters                        { get; set; }
		[DataMember][Required] public uint       MinutesBeforeDateOfArrivalNotInRadius { get; set; }
		[DataMember]           public AlertRange Sms                                   { get; set; }
		[DataMember]           public AlertRange Mail                                  { get; set; }
		[DataMember]           public AlertRange Push                                  { get; set; }
	}
}