﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Compass.Ufin.Orders.Core.Models
{
	[Serializable]
	[DataContract]
	public partial class Order : OrderBase
	{
		[DataMember][Required] public long         OrderId      { get; set; }
		[DataMember][Required] public DateTime     CreatedAtUtc { get; set; }
		[DataMember]           public DateTime?    DeletedAtUtc { get; set; }
		[DataMember]           public OrderEvent[] Events       { get; set; }
	}
}