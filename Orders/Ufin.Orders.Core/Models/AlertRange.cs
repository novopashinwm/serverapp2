﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Compass.Ufin.Orders.Core.Models
{
	[Serializable]
	[DataContract]
	public partial class AlertRange
	{
		[DataMember][Required] public uint PeriodInMinutes { get; set; }
		[DataMember][Required] public uint MaxCount        { get; set; }
	}
}