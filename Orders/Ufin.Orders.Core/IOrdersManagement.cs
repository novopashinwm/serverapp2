﻿using System;
using Compass.Ufin.Orders.Core.Models;
using FORIS.TSS.BusinessLogic.DTO;

namespace Compass.Ufin.Orders.Core
{
	public interface IOrdersManagement
	{
		int? OperatorId   { get; set; }
		int? DepartmentId { get; set; }
		/// <summary> Получить список заказов </summary>
		/// <param name="intervalBegUtc"> Начало интервала (UTC) </param>
		/// <param name="intervalEndUtc"> Конец интервала (UTC) </param>
		/// <param name="pagination"> Параметры нумерации и разбиения страниц </param>
		/// <returns> Массив заказов </returns>
		Order[] GetOrders(DateTime intervalBegUtc, DateTime intervalEndUtc, Pagination pagination = null);
		/// <summary> Получить заказ </summary>
		/// <param name="orderId"> Идентификатор заказа </param>
		/// <returns> Заказ </returns>
		Order GetOrder(long orderId);
		/// <summary> Получить заказ </summary>
		/// <param name="externalOrderId"> Идентификатор заказа во внешней системе </param>
		/// <returns> Заказ </returns>
		Order GetOrderByExternalId(string externalOrderId);
		/// <summary> Создать заказ </summary>
		/// <param name="request"> Запрос на создание заказа </param>
		/// <returns> Ответ/отчет о создании заказа </returns>
		OrderCreationResponse Create(OrderCreationRequest request);
		/// <summary> Обновить/изменить заказ </summary>
		/// <param name="request"> Обновить/изменить заказа </param>
		/// <returns> Обновленный заказ </returns>
		Order Update(OrderUpdateRequest request);
		/// <summary> Удалить заказ </summary>
		/// <param name="orderId"> Идентификатор заказа </param>
		/// <returns> Результат </returns>
		bool Delete(long orderId);
	}
}