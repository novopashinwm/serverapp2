﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.Remoting;
using System.Xml;
using FORIS.TSS.BusinessLogic.Server;

namespace FORIS.TSS.ServerApplication
{
	public class ServersSectionHandler :
		System.Configuration.IConfigurationSectionHandler
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="configContext"></param>
		/// <param name="section"></param>
		/// <returns>
		/// Хэш конфигураций серверов, описанных в 
		/// файле конфигурации
		/// </returns>
		public object Create(object parent, object configContext, XmlNode section)
		{
			/* Нет уверенности, что за время работы 
			 * приложения этот метод вызывается лишь однажды
			 */

			Dictionary<string, ServerApplicationConfiguration> Servers =
				new Dictionary<string, ServerApplicationConfiguration>();

			foreach (XmlNode node in section.ChildNodes)
			{
				switch (node.Name)
				{
					#region server

					case "server":

						Servers.Add(
							node.Attributes["name"].Value,
							new ServerApplicationConfiguration(node)
							);

						break;

					#endregion // server

					#region #comment

					case "#comment":
						/* Комментарии в файле конфигурации не
						 * обрабатываются приложением
						 */
						break;

					#endregion // #comment

					#region other

					default:
						throw new ApplicationException(
							String.Format(
								"Unexpected node {0} in section 'servers'",
								node.Name
								)
							);

						#endregion // other
				}
			}

			return Servers;
		}
	}

	public class ServerApplicationConfiguration
	{
		private readonly string              name;
		private readonly Type                serverType;
		private readonly Type                sessionManagerType;
		private readonly string              uri;
		private readonly string              channel;
		private readonly NameValueCollection properties;

		public ServerApplicationConfiguration(XmlNode node)
		{
			this.name               = node.Attributes["name"].Value;
			this.serverType         = Type.GetType(node.Attributes["serverType"].Value);
			this.sessionManagerType = Type.GetType(node.Attributes["sessionManagerType"].Value);
			this.uri                = node.Attributes["uri"].Value;
			this.channel            = node.Attributes["channel"].Value;

			#region properties

			this.properties = new NameValueCollection();

			foreach (XmlNode childNode in node.ChildNodes)
			{
				switch (childNode.Name)
				{
					#region add

					case "add":
						this.properties.Set(
							childNode.Attributes["key"].Value,
							childNode.Attributes["value"].Value
							);
						break;

					#endregion // add

					#region #comment

					case "#comment":
						/* Комментарии не обрабатываются
						 */
						break;

					#endregion // #comment

					#region other

					default:
						throw new ApplicationException(
							String.Format(
								"Unexpected node {0} in server section {1}",
								childNode.Name,
								this.name
								)
							);

						#endregion // other
				}
			}

			#endregion // Properties
		}

		/// <summary>
		/// Регистрирует Wellknown объект в ремотинге,
		/// создает экземпляр сервера
		/// </summary>
		/// <returns></returns>
		public IServer CreateInstance()
		{
			#region Preconditions

			if (this.serverType == null)
			{
				throw new ApplicationException("Server type not found for " + this.name);
			}

			if (this.sessionManagerType == null)
			{
				throw new ApplicationException("Session manager type no found for " + this.name);
			}

			#endregion // Preconditions

			/* Похоже, что региструемый объект будет
			 * доступен по всем каналам, зарегестрированным 
			 * в инфраструктуре .NET Remoting
			 */

			// IChannel[] Channels = ChannelServices.RegisteredChannels;

			RemotingConfiguration.RegisterWellKnownServiceType(
				this.sessionManagerType,
				this.uri,
				WellKnownObjectMode.Singleton
				);

			ISessionManager sessionManager =
				(ISessionManager)Activator.GetObject(this.sessionManagerType, this.channel + this.uri);

			IServer Result =
				(IServer)Activator.CreateInstance(
					this.serverType,
					new object[] { sessionManager, this.properties }
					);

			return Result;
		}
	}
}