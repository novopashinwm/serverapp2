﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Remoting;
using System.Security.Permissions;
using System.Threading;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Config;
using FORIS.TSS.Protection;

namespace FORIS.TSS.ServerApplication
{
	/// <summary> Корневой класс домена серверов (как объект с поддержкой удаленного взаимодействия, через границы доменов и процессов) </summary>
	/// <remarks> Обычно используется в конфигурационном файле для создания нового домена, поэтому прямых ссылок не имеет </remarks>
	public class EntryPoint : MarshalByRefObject, IEntryPoint
	{
		/// <summary> Получает объект службы времени существования для управления политикой времени существования для этого экземпляра </summary>
		/// <returns>
		/// Теоретически на этот объект существует ссылка только в инфраструктуре ремотинга.
		/// Домен приложения ланчера вызывает методы Start() и Stop() этого объекта.
		/// Если изготавливать стандартную лицензию, то этот объект по истечению лицензии в виду отсутствия спонсоров будет отключен от
		/// ремотинга и при выгрузке сервера попытка  вызова метода Stop() приведет к исключению.
		/// </returns>
		public override object InitializeLifetimeService()
		{
			/* Вечная лицензия */
			return null;
		}
		/// <summary> Конструктор </summary>
		public EntryPoint()
		{
			using var logger = Stopwatcher.GetElapsedLogger($"Create Entry, Domain: '{AppDomain.CurrentDomain.FriendlyName}'");
			_servers = new CollectionWithEvents<IServer>();
			_servers.Inserted += Servers_Inserted;
			_servers.Removing += Servers_Removing;
			_servers.Clearing += Servers_Clearing;
		}
		/// <summary> Запуск сервера </summary>
		public void Start(bool runLoop, bool activate, bool staticServer)
		{
			using var logger = Stopwatcher.GetElapsedLogger($"Start Entry, Domain: '{AppDomain.CurrentDomain.FriendlyName}'");
			try
			{
				// К событиям домена подключаемся только если сервис стартует
				AppDomain.CurrentDomain.DomainUnload       += CurrentDomain_DomainUnload;
				AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

				// Настройка Remoting
				RemotingConfiguration.Configure(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile, false);

				if (activate)
				{
					/* Необходимо чтоб точка входа запускала сервер,
					 * указанный в настройках приложения,
					 * а не предопределенного в коде класса
					 * Класс сервера должен реализовывать интерфейс IServer,
					 * чтоб его можно было запустить и остановить
					 */

					var serverConfigurations = ConfigurationManager.GetSection("servers") as IDictionary<string, ServerApplicationConfiguration>
						?? new Dictionary<string, ServerApplicationConfiguration>();

					/* Используем ConfigGlobals.AppSettings
					 * вместо ConfigurationManager.AppSettings, чтоб секции
					 * машин оказывали влияние на запуск серверов
					 */
					var serverNames = GlobalsConfig.AppSettings["Server"]?.Split(',') ?? Array.Empty<string>();
					if (serverNames.Length != 0)
						foreach (string serverName in serverNames)
						{
							var server = serverConfigurations.FirstOrDefault(s => s.Key == serverName).Value?.CreateInstance();
							if (server != null)
								_servers.Add(server);
							else
								$"В секции 'servers' файла конфигурации нет описания сервера '{serverName}'"
									.TraceWarning();
						}
					else
						$"Серверы не загружены. Не указан параметр 'Server' в секции 'appSettings' файла конфигурации"
							.TraceWarning();
				}

				#region Run loop

				/* Танас:
				 * Несмотря на то что этот метод выполняется в
				 * отдельном домене приложения серверов, однако,
				 * вызов его из основного домена службы (приложения)
				 * синхронный. Это означает, что ему дано
				 * не более 30 секунд на выполнение и запускать
				 * здесь standart application message loop нельзя
				 */

				/* Интересно. Если поток домена завершится,
				 * то будет ли домен выгружен?
				 */

				#endregion Run loop
			}
			catch (Exception ex)
			{
				$"Start Entry, Domain: '{AppDomain.CurrentDomain.FriendlyName}'"
					.WithException(ex)
					.CallTraceError();
				throw;
			}
		}
		/// <summary> Остановка сервера </summary>
		public void Stop()
		{
			using var logger = Stopwatcher.GetElapsedLogger($"Stop Entry, Domain: '{AppDomain.CurrentDomain.FriendlyName}'");
			_servers.Clear();
		}
		private void Server_Exit(IServer server, string message)
		{
			using var logger = Stopwatcher.GetElapsedLogger($"Server '{server?.GetTypeName(false)}' Exit, Domain: '{AppDomain.CurrentDomain.FriendlyName}'");
			/* Танас:
			 * Этакая наивная попытка произвести корректное завершение
			 * сервера, вероятно, при еще невыполненном старте
			 *
			 * Видимо, с целью по возможности корректно
			 * отключиться от вспомогательных сервисов
			 * (например от сервера терминалов или карты)
			 */

			_servers.Remove(server);

			throw new HaspProtectionException(message);
		}
		[SecurityPermission(SecurityAction.LinkDemand, ControlAppDomain = true)]
		private void CurrentDomain_DomainUnload(object sender, EventArgs e)
		{
			$"Domain Unload: {AppDomain.CurrentDomain.FriendlyName}".TraceInformation();
		}
		[SecurityPermission(SecurityAction.LinkDemand, ControlAppDomain = true)]
		private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs ex)
		{
			(string.Empty
				+ $"!!!!!!!!!!!!!!!!! CurrentDomain_UnhandledException !!!!!!!!!!!!!!!!!"
				+ $"\nSender - {sender?.GetTypeName() ?? "Unknown"}")
				.WithException(ex?.ExceptionObject as Exception, true)
				.CallTraceError();
		}

		#region Handle _servers collection events

		private readonly CollectionWithEvents<IServer> _servers;
		private void Servers_Inserted(object sender, CollectionChangeEventArgs<IServer> e)
		{
			var server = e?.Item;
			if (server == null)
				return;
			server.Exit += Server_Exit;

			if (ProtectionKey.CanStartServer())
			{
				/* Танас
				 * Так как на запуск домена приложения
				 * отведено всего 30 сек, то стартовать
				 * серверы будем асинхронно
				 */
				var startHandler = new ThreadStart(server.Start);
				var asyncResult  = startHandler.BeginInvoke(ServerStart_Callback, startHandler);
				if (!asyncResult.AsyncWaitHandle.WaitOne(90000, true))
					throw new TimeoutException();
			}
			else
				throw new HaspProtectionException("No protection key or license has expired.");
		}
		private void ServerStart_Callback(IAsyncResult asyncResult)
		{
			var startHandler = (ThreadStart)asyncResult.AsyncState;
			try
			{
				startHandler.EndInvoke(asyncResult);
			}
			catch (Exception ex)
			{
				$"Exception in serverStart_Callback method!"
					.WithException(ex)
					.CallTraceError();
				_servers.Remove((IServer)startHandler.Target);

				throw ex;

				/* Похоже ревыброс исключения произойдет прямо
				 * в домен и станет необработанным исключением,
				 * что приведет к выгрузке домена и завершению
				 * всех серверов.
				 */
			}
		}
		private void Servers_Removing(object sender, CollectionChangeEventArgs<IServer> e)
		{
			var server = e.Item;
			server?.Stop();
			server?.Dispose();
		}
		private void Servers_Clearing(object sender, CollectionEventArgs<IServer> e)
		{
			var servers = e.Collection ?? new CollectionWithEvents<IServer>();
			foreach (var server in servers)
			{
				server?.Stop();
				server?.Dispose();
			}
		}

		#endregion Handle _servers collection events

	}
}