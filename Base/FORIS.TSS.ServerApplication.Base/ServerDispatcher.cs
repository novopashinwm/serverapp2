﻿using System;
using System.ComponentModel;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;

namespace FORIS.TSS.ServerApplication
{
	public abstract class ServerDispatcher<TServer> :
		Dispatcher<IServerItem<TServer>>,
		IServerItem<TServer>
		where TServer : ServerBase<TServer>
	{
		#region IServerItem Members

		private TServer server;
		[Browsable(false)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public TServer Server
		{
			get { return server; }
			set
			{
				if (server != value)
				{
					OnBeforeChange();

					if (value != null)
					{
						server = value;

						for (int i = 0; i < Ambassadors.Count; i++)
						{
							ServerAmbassador<TServer> ambassador = Ambassadors[i];

							if (ambassador.Item != null && ambassador.Enabled)
								ambassador.Item.Server = value;
						}
					}
					else
					{
						for (int i = Ambassadors.Count - 1; i >= 0; i--)
						{
							ServerAmbassador<TServer> ambassador = Ambassadors[i];

							if (ambassador.Item != null && ambassador.Enabled)
								ambassador.Item.Server = value;
						}

						server = value;
					}

					OnAfterChange();

					/* Вполне вероятно, что коллекция Ambassadors 
					 * изменится после обработки OnAfterChange()
					 */
				}
			}
		}

		#endregion IServerItem Members

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public ServerAmbassadorCollection<TServer> Ambassadors
		{
			get { return (ServerAmbassadorCollection<TServer>)GetAmbassadorCollection(); }
		}

		protected override void OnBeforeSetItem(Ambassador<IServerItem<TServer>> ambassador)
		{
			if (ambassador.Item != null)
			{
				ambassador.Item.Server = null;
			}
		}
		protected override void OnAfterSetItem(Ambassador<IServerItem<TServer>> ambassador)
		{
			if (ambassador.Item != null)
			{
				try
				{
					ambassador.Item.Server = server;
				}
				catch (Exception ex)
				{
					string.Empty
						.WithException(ex)
						.CallTraceError();
					throw ex;
				}
			}
		}
	}
	public class ServerAmbassadorCollection<TServer> :
		AmbassadorCollection<IServerItem<TServer>>
		where TServer : ServerBase<TServer>
	{
		public new ServerAmbassador<TServer> this[int index]
		{
			get { return (ServerAmbassador<TServer>)base[index]; }
		}
	}
	public class ServerAmbassador<TServer> : Ambassador<IServerItem<TServer>>
		where TServer : ServerBase<TServer>
	{
	}
	public interface IServerItem<TServer>
		where TServer : ServerBase<TServer>
	{
		TServer Server { get; set; }
	}
}