﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Lifetime;
using System.Security;
using System.Security.Principal;
using System.Threading;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Config;
using FORIS.TSS.Helpers;
using FORIS.TSS.Infrastructure.Authentication;
using FORIS.TSS.Infrastructure.Interfaces.UserName;
using FORIS.TSS.ServerApplication.Data.Server;
using FORIS.TSS.ServerApplication.Helpers;

namespace FORIS.TSS.ServerApplication
{
	/// <summary> Класс, реализующий базовые методы для работы с сессиями </summary>
	/// <remarks> Следит за сессиями, создает их по запросам клиентов </remarks>
	public abstract class SessionManager<TServer> : Component, IServerItem<TServer>, ISessionManager, IAdminSessionManager where TServer : ServerBase<TServer>
	{
		protected Dictionary<Guid, Common.WeakReference<PersonalServerBase<TServer>>> sessionHash =
			new Dictionary<Guid, Common.WeakReference<PersonalServerBase<TServer>>>();

		#region Controls & Components

		private IContainer components;
		private ServerServerData serverData;
		/// <summary> Время обновления списка сессий, по умолчанию 10 сек </summary>
		private TimeSpan refreshSessionListInterval = TimeSpan.FromSeconds(10f);

		#endregion Controls & Components

		#region ManualResetEvent

		private readonly ManualResetEvent m_serverWaitEvent;

		#endregion ManualResetEvent

		#region Constructor & Dispose

		/// <summary> Класс, реализующий базовые методы для работы с сессиями </summary>
		protected SessionManager()
		{
			Initialize();
			InitializeComponent();
			sessionKiller = new SessionKiller(this, refreshSessionListInterval);
			sessionKiller.Start();
			m_serverWaitEvent = new ManualResetEvent(false);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				sessionKiller?.Stop();
				m_serverWaitEvent.Close();
				components?.Dispose();
			}
			base.Dispose(disposing);
		}

		#endregion Constructor & Dispose

		#region ServerDispatcher

		private ServerDispatcher<TServer> serverDispatcher;

		protected abstract ServerDispatcher<TServer> GetServerDispatcher();

		#endregion ServerDispatcher

		#region IAdminSessionManager Members

		public ServerServerData ServerData
		{
			get { return serverData; }
		}

		public IDatabaseDataSupplier DatabaseDataSupplier
		{
			get { return Server.Database; }
		}

		void IAdminSessionManager.CloseSession(int sessionId)
		{
			lock (sessionHash)
			{
				sessionHash.Values
					?.FirstOrDefault(s => s.Target?.SessionId == sessionId)
					?.Target
					?.Close();
			}
		}
		void IAdminSessionManager.CloseSessionsByOperatorId(int operatorId)
		{
			lock (sessionHash)
			{
				// Вместо закрытия, указываем дату окончания сессии, тогда при попытке входа будет зарыта и сессия IIS
				var stopSessions = sessionHash.Values
					.Where(s => s.Target?.SessionInfo?.OperatorInfo?.OperatorId == operatorId)
					.ToArray();
				foreach (var stopSession in stopSessions)
				{
					var sessionInfo = stopSession?.Target?.SessionInfo;
					if (null != sessionInfo)
						sessionInfo.StopOn = DateTime.UtcNow;
				}
			}
		}

		#endregion IAdminSessionManager Members

		#region IServerItem<TServer> Members

		/// <summary>
		///
		/// </summary>
		/// <remarks>
		/// Публичные методы должны быть приостановлены
		/// для ожидания передачи объекта Server в менеджер сессий.
		/// TODO: Остановка менеджера сессий
		/// При остановке сервера в это свойство будет передан null,
		/// при этом сессии пользователей должны быть аккуратно
		/// завершены, а не просто стать неработоспособными в
		/// виду отсутствия Server.
		/// </remarks>
		public TServer Server
		{
			get { return serverDispatcher.Server; }
			set
			{
				serverDispatcher.Server = value;
				if (null != serverDispatcher.Server)
					m_serverWaitEvent.Set();
				/* Не должны выполняться методы, до задания сервера */
			}
		}

		#endregion IServerItem<TServer> Members

		#region Initialize

		private void Initialize()
		{
			serverDispatcher = GetServerDispatcher();

			string strRefreshSessionListInterval = GlobalsConfig.AppSettings["RefreshSessionListInterval"];
			if (!string.IsNullOrEmpty(strRefreshSessionListInterval))
				refreshSessionListInterval = TimeSpan.FromSeconds(double.Parse(strRefreshSessionListInterval));
		}

		#endregion Initialize

		#region Component Designer generated code

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			serverData = new ServerServerData(components);
		}

		#endregion Component Designer generated code

		public override object InitializeLifetimeService()
		{
			return null;
		}

		#region ISessionManager Members

		public IPersonalServer CreateSession(int operatorId, Guid clientId, string clientVersion, IPAddress address)
		{
			var operatorInfo = OperatorInfoHelper.Create(operatorId, Server.Database);
			if (operatorInfo == null)
				return null;
			return CreateSession(operatorInfo, clientId, clientVersion, address);
		}
		/// <summary> Создание новой сессии </summary>
		/// <returns> Ссылка на новую сессию </returns>
		public IPersonalServer CreateSession(Guid clientId, string clientVersion)
		{
			#region Preconditions

			if (!m_serverWaitEvent.WaitOne(30000, false))
				throw new ApplicationException("Can not create session without server");


			/* Иногда случается так, что клиент вызывает этот метод
			 * до того как сервер при старте передаст в этот метод
			 * ссылку на себя (IServerItem(this)).Server {set {...} }
			 *
			 * Надо сделать так чтоб здесь было ожидание до тех пор
			 * пока не будет установлен this.Server
			 */

			#endregion Preconditions

			/* Танас:
			 * Вообще положено считать в нашей системе, что вызов
			 * клиента может дойти сюда лишь в том случае,
			 * если ClientAuthentication и SSPIAuthentication разрешили.
			 * А потому проверять правильность пароля здесь мы не будем никак.
			 * А лишь создадим по имени необходимые описывающие пользователя
			 * структуры и будем их использовать в дальнейшем.
			 */

			if (Thread.CurrentPrincipal == null)
			{
				Trace.TraceError("CreateSession: Thread.CurrentPrincipal == null, session will not be created");
				// client should provide some login information
				return null;
			}

			#region Create OperatorInfo structure on user Identity

			/* Если удастся создать структуру, значит с пользователем
			 * все в порядке, заодно нормализуем его данные (регистр букв в логине).
			 */

			IIdentity identity = Thread.CurrentPrincipal.Identity;

			var operatorInfo = OperatorInfoHelper.Create(identity.Name, Server.Database);
			if (operatorInfo == null)
				return null;

			#endregion Create OperatorInfo structure on user Identity

			var address = GetAddressFromPrincipal();

			return CreateSession(operatorInfo, clientId, clientVersion, address);
		}
		/// <summary> Returns current connection user IP address </summary>
		private static IPAddress GetAddressFromPrincipal()
		{
			IPrincipal principal = Thread.CurrentPrincipal;

			var customPrincipal = principal as CustomPrincipal;
			if (customPrincipal != null)
			{
				// Только имя пользователя передано в заголовке.
				// Аутентификация на самом деле не проведена, пароль не проверен
				return customPrincipal.IPAddress;
			}
			return IPAddress.Loopback;
		}
		private object thisLock = new object();
		private IPersonalServer CreateSession(IOperatorInfo operatorInfo, Guid clientId, string clientVersion, IPAddress address)
		{
			var sw = new Stopwatch();
			sw.Start();

			lock (thisLock)
			{
				sw.Stop();
				LogStopwatch(sw, "CreateSession lock(this)");
				sw.Restart();

				try
				{
					#region One session per login
					if (Server.DeleteExistSession || Server.OneSessionPerLogin)
					{
						IPersonalServer existingSession = null;
						string guestLogin = GlobalsConfig.AppSettings["GuestLogin"];
						if (string.IsNullOrEmpty(guestLogin) || !string.Equals(operatorInfo.Login, guestLogin))
						{
							lock (sessionHash)
							{
								foreach (var sessionReference in sessionHash.Values)
								{
									var sessionHolder = sessionReference.Target;
									if (sessionHolder != null &&
										sessionHolder.SessionInfo.OperatorInfo.Equals(operatorInfo) &&
										sessionHolder.IsAlive())
									{
										existingSession = sessionHolder;
										break;
									}
								}
							}

							if (existingSession != null)
							{
								// если установлен флаг удаления предыдущей сессии
								if (Server.DeleteExistSession)
								{
									existingSession.Close();
								}
								// иначе надо проверить возможность существования сразу нескольких сессий
								else if (Server.OneSessionPerLogin)
								{
									throw new SessionExistException();
								}
							}
						}
					}

					#endregion One session per login

					#region Try find existed PersonalServer

					// Получить сессию из sessionHash
					var result = GetPersonalServer(clientId);
					if (result != null)
					{
						// Проверка соответствия оператора в SessionHash и того, кто запрашивает сессию, при несоответствии исключение
						if (!result.SessionInfo.OperatorInfo.Equals(operatorInfo))
						{
							throw new SecurityException(string.Format(
								"Fake session, session id = {0}, provider operator info = {1}, expected operator info = {2}",
								clientId, operatorInfo, result.SessionInfo.OperatorInfo));
						}
						// Обновление информации об операторе
						result.SessionInfo.OperatorInfo = operatorInfo;
						// Проверка идентичности IP адреса, в случае изменения пишем в лог и сохраняем в памяти новый IP
						if (!result.SessionInfo.IP.Equals(address))
						{
							Trace.TraceInformation("Session {0} IP was changed from {1} to {2} for operator {3}",
								clientId, result.SessionInfo.IP, address, operatorInfo);
							result.SessionInfo.IP = address;
						}
						// Возвращаем существующий, обновленный объект сессии из памяти
						return result;
					}

					#endregion Try find existed PersonalServer

					// Если сессии нет в SessionHash, то создаем заново
					var sessionInfo = CreateNewSessionInDatabase(
						operatorInfo,
						DateTime.UtcNow,
						clientId,
						clientVersion,
						address);
					// Вызываем создание сессии в конкретном менеджере сессий (обычно: return new TServer())
					result = OnCreatePersonalServer();
					// Вызываем инициализацию сессии
					result.Init(serverDispatcher.Server, this, sessionInfo);
					// Подписываемся на события закрытия сессии
					result.Closing += Session_Closing;
					result.Closed  += Session_Closed;

					#region Put weak reference of PersonalServer object into sessionHash

					/* Так как хэш сессий ведется по совсем-совсем уникальному ключу,
					* то в общем-то нет задачи заменять объекты сессий под ключами.
					*/

					lock (sessionHash)
					{
						sessionHash.Add(clientId, new Common.WeakReference<PersonalServerBase<TServer>>(result));
					}

					#endregion Put weak reference of PersonalServer object into InnerHashtable

					#region Sync to serverData

					using (var treater = new ServerDataTreater(serverData, null))
					{
						/* В это время объект сессии создан, но еще ни разу
						 * не был передан через инфраструктуру ремотинга
						 * и поэтому для него еще не было создано объекта
						 * лицензии управления временем жизни
						 */
						treater.SessionInsert(result.SessionInfo.SessionId, result.SessionInfo.OperatorInfo.Login);
					}

					OnCreateSession(result);

					#endregion Sync to serverData

					return result;
				}
				finally
				{
					LogStopwatch(sw, "CreateSession exclusive");
				}
			}
		}
		private static void LogStopwatch(Stopwatch sw, string tag)
		{
			if (500 < sw.ElapsedMilliseconds)
				Trace.TraceWarning("{0} is slow, operation has  taken {1}", tag, sw.Elapsed);
		}

		#endregion ISessionManager Members

		/// <summary> Метод создает объект сессии на сервере </summary>
		/// <returns> Объект сессии - персональный сервер </returns>
		/// <remarks>
		/// Возможно следует передавать какие-нибудь параметры
		/// в этот метод, чтоб можно было создавать объекты
		/// сессий различных типов в зависимости от запроса клиента
		/// </remarks>
		protected abstract PersonalServerBase<TServer> OnCreatePersonalServer();
		protected PersonalServerBase<TServer> GetPersonalServer(Guid clientId)
		{
			if (sessionHash.ContainsKey(clientId))
			{
				var sessionHolder = sessionHash[clientId].Target;
				if (sessionHolder == null || !sessionHolder.IsAlive())
				{
					lock (sessionHash)
					{
						sessionHash.Remove(clientId);
					}
					// А чья это была сессия уже никак сказать нельзя
					Trace.TraceInformation(this.GetTypeName() + ": Session '{0}' was removed'", clientId);
					return null;
				}
				return sessionHolder;
			}
			return null;
		}
		protected ISessionInfo CreateNewSessionInDatabase(IOperatorInfo operatorInfo, DateTime sessionStart, Guid clientId, string clientVersion, IPAddress address)
		{
			IPAddress ip = address;
			IPrincipal principal = Thread.CurrentPrincipal;

			if (address == null)
			{
				if (principal is CustomPrincipal)
				{
					CustomPrincipal customPrincipal = (CustomPrincipal)principal;
					ip = customPrincipal.IPAddress;
				}
				else
				{
					ip = IPAddress.Loopback;
				}
			}

			string hostname = ip.ToString();

			using (var sp = DatabaseDataSupplier.Procedure("dbo.InsertSession"))
			{
				sp["@OPERATOR_ID"].Value    = operatorInfo.OperatorId;
				sp["@HOST_IP"].Value        = ip.ToString();
				sp["@HOST_NAME"].Value      = hostname;
				sp["@SESSION_START"].Value  = sessionStart;
				sp["@SESSION_END"].Value    = DBNull.Value;
				sp["@CLIENT_VERSION"].Value = clientVersion;
				int session_id              = (int)sp.ExecuteScalar();

				return new SessionInfo(
					session_id,
					operatorInfo,
					clientId,
					clientVersion,
					hostname,
					ip,
					sessionStart);
			}
		}
		protected void CloseSessionInDatabase(int session_id, DateTime endTime)
		{
			try
			{
				using (var sp = DatabaseDataSupplier.Procedure("dbo.CloseSession"))
				{
					sp["@SESSION_ID"].Value  = session_id;
					sp["@SESSION_END"].Value = endTime;
					sp.ExecuteNonQuery();
				}
			}
			catch(Exception ex)
			{
				Trace.TraceError(this.GetTypeName() + ": CloseSessionInDatabase error\n{0}", ex);
			}
		}
		private void Session_Closing(object sender, EventArgs e)
		{
			var session = sender as PersonalServerBase<TServer>;
			if (null == session)
				return;
		}
		private void Session_Closed(object sender, EventArgs e)
		{
			var session = sender as PersonalServerBase<TServer>;
			if (null == session)
				return;

			#region Sync to serverData
			using (var treater = new ServerDataTreater(serverData, null))
			{
				treater.SessionDelete(session.SessionInfo.SessionId);
			}

			OnCloseSession(session);

			#endregion Sync to ServerData

			session.SessionInfo.StopOn = DateTime.UtcNow;
			CloseSessionInDatabase(session.SessionInfo.SessionId, session.SessionInfo.StopOn.Value);

			lock (sessionHash)
			{
				sessionHash.Remove(session.SessionInfo.ClientId);
			}
		}
		private readonly SessionKiller sessionKiller;
		private class SessionKiller : BackgroundProcessor
		{
			private readonly SessionManager<TServer> _sessionManager;
			private readonly TimeSpan                _sessionListInterval;
			public SessionKiller(SessionManager<TServer> sessionManager, TimeSpan sessionListInterval)
			{
				_sessionManager      = sessionManager;
				_sessionListInterval = sessionListInterval;
			}
			protected override bool Do()
			{
				Thread.Sleep(_sessionListInterval);

				var dead          = new List<Guid>(_sessionManager.sessionHash.Count);
				var expired       = new List<Guid>(_sessionManager.sessionHash.Count);
				var sessionLeases = new Dictionary<int, ILease>(_sessionManager.sessionHash.Count);

				lock (_sessionManager.sessionHash)
				{
					#region Find dead references and Session object with expired Lease

					foreach (var entry in _sessionManager.sessionHash)
					{
						var session = entry.Value.Target;

						if (session != null)
						{
							var lease = (ILease)RemotingServices.GetLifetimeService(session);

							if (lease != null && lease.CurrentState == LeaseState.Expired)
							{
								expired.Add(entry.Key);
							}
							else
							{
								sessionLeases.Add(session.SessionId, lease);
							}
						}
						else
						{
							dead.Add(entry.Key);
						}
					}

					#endregion Find dead references and Session objects with expired Lease
					#region Remove dead references

					var countBeforeCleanup = _sessionManager.sessionHash.Count;

					if (dead.Count != 0)
						Trace.TraceInformation("Dead sessions removing: {0} / {1}", dead.Count, countBeforeCleanup);
					foreach (Guid key in dead)
						_sessionManager.sessionHash.Remove(key);

					#endregion Remove dead references
					#region Call Dispose for Session objects with expired Lease

					if (expired.Count != 0)
						Trace.TraceInformation("Expired sessions closing: {0} / {1}", expired.Count, countBeforeCleanup);
					foreach (Guid key in expired)
						_sessionManager.sessionHash[key].Target?.Dispose();

					#endregion Call Dispose for Session objects with expired Lease
				}

				using (var treater = new ServerDataTreater(_sessionManager.serverData, null))
				{
					treater.UpdateSessionLeaseTimes(sessionLeases);
				}

				_sessionManager.OnUpdateSessionLease(sessionLeases);

				return true;
			}
		}

		#region Session
		protected TraceSwitch traceSwitch = new TraceSwitch("SessionManager", "SessionManager");
		protected virtual void OnCreateSession(PersonalServerBase<TServer> session)
		{
			if (traceSwitch.Level >= TraceLevel.Info)
				Trace.TraceInformation(GetType().Name + ": CreateSession({0}, {1}, {2}, {3}, {4}), SessionCount={5}",
					session.SessionInfo?.OperatorInfo?.OperatorId,
					session.SessionInfo?.ClientId,
					session.SessionInfo?.ClientVersion,
					session.SessionInfo?.IP,
					session.SessionInfo?.StartOn,
					sessionHash.Count);
		}
		protected virtual void OnCloseSession(PersonalServerBase<TServer> session)
		{
			if (traceSwitch.Level >= TraceLevel.Info)
				Trace.TraceInformation(GetType().Name + ": CloseSession({0}, {1}, {2}, {3}, {4}), SessionCount={5}",
					session.SessionInfo?.OperatorInfo?.OperatorId,
					session.SessionInfo?.ClientId,
					session.SessionInfo?.ClientVersion,
					session.SessionInfo?.IP,
					session.SessionInfo?.StartOn,
					sessionHash.Count);
		}
		protected virtual void OnUpdateSessionLease(Dictionary<int, ILease> sessionLeases)
		{
		}

		#endregion Session
	}
}