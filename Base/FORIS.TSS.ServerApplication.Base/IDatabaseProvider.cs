﻿using FORIS.TSS.Helpers;

namespace FORIS.TSS.ServerApplication
{
	public interface IDatabaseProvider
	{
		IDatabaseDataSupplier Database { get; }
	}
}