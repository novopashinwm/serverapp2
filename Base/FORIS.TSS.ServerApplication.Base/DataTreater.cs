﻿using System;
using System.ComponentModel;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Helpers;
using FORIS.TSS.Helpers.Data;

namespace FORIS.TSS.ServerApplication
{
	/// <summary>
	/// Базовый класс объектов изменения
	/// данных на стороне сервера
	/// </summary>
	/// <typeparam name="TData"></typeparam>
	/// <remarks>
	/// Объекты изменения данных, используемые на стороне 
	/// клиента создаются объектами сессии пользователей.
	/// В то же время возникает необходимость в редактирования 
	/// данных на стороне сервера согласно событиям окружения 
	/// сервера ни с каким из пользователей или сессией 
	/// пользователя не связанных.
	/// </remarks>
	public class DataTreater<TData> : Component, IDataTreater where TData : IData
	{
		public DataTreater(IDatabaseDataSupplier databaseDataSupplier, TData data, ISessionInfo sessionInfo)
		{
			#region Conditions

			if (!(data.DataSupplier is FinalDataSupplier))
				throw new ArgumentException("Data container for treater must be server side root data container", nameof(data));

			#endregion Conditions

			this.databaseDataSupplier = databaseDataSupplier;
			this.data = data;
			this.sessionInfo = sessionInfo;
		}

		protected readonly IDatabaseDataSupplier databaseDataSupplier;
		private readonly TData data;
		private readonly ISessionInfo sessionInfo;

		#region Properties

		[Browsable(false)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public TData Data
		{
			get { return data; }
		}

		[Browsable(false)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public ISessionInfo SessionInfo
		{
			get { return sessionInfo; }
		}

		#endregion // Properties

		#region AcceptChanges & RejectChanges

		protected void AcceptChanges()
		{
			AcceptChanges(true);
		}
		protected void AcceptChanges(bool applyToDatabase)
		{
			Data.AcceptChanges(Guid.NewGuid(), applyToDatabase ? databaseDataSupplier : null);
		}

		protected void RejectChanges()
		{
			Data.RejectChanges();
		}

		#endregion AcceptChanges & RejectChanges

	}

	public delegate void TreatDataHandler<TData>(TData data) where TData : IData;
}