﻿using System;
using System.ComponentModel;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Data.Server;
using FORIS.TSS.Helpers.Data;
using FORIS.TSS.Helpers.Data.Server;

namespace FORIS.TSS.ServerApplication.Data.Server
{
	public class ServerServerData : ServerData
	{
		public ServerServerData(IContainer container)
			: this()
		{
			container.Add(this);
		}
		public ServerServerData()
		{
			DataSupplier = new ServerCustomDataSupplier();
		}
	}
	public class ServerCustomDataSupplier :
		FinalDataSupplier,
		IDataSupplier<IServerDataTreater>
	{
		#region IDataSupplier<IServerDataTreater> Members

		public IServerDataTreater GetDataTreater()
		{
			throw new NotSupportedException();
		}

		#endregion IDataSupplier<IServerDataTreater> Members
		protected override DataInfo GetData()
		{
			return new DataInfo(new ServerDataSet(), Guid.NewGuid());
		}
	}
}