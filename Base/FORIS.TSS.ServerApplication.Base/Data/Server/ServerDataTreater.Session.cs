﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Lifetime;
using FORIS.TSS.Helpers.Data.Server;

namespace FORIS.TSS.ServerApplication.Data.Server
{
	public partial class ServerDataTreater
	{
		public void SessionInsert(int sessionId, string login)
		{
			lock (Data)
			{
				try
				{
					SessionRow sessionRow = Data.Session.NewRow();

					sessionRow.SessionId  = sessionId;
					sessionRow.Login      = login;
					sessionRow.LeaseTime  = null;
					sessionRow.LeaseState = null;

					Data.Session.Rows.Add(sessionRow);

					AcceptChanges();
				}
				catch (Exception)
				{
					RejectChanges();
					throw;
				}
			}
		}
		public void SessionDelete(int sessionId)
		{
			lock (Data)
			{
				try
				{
					SessionRow sessionRow = Data.Session.FindRow(sessionId);

					sessionRow.Delete();

					AcceptChanges();
				}
				catch (Exception)
				{
					RejectChanges();
					throw;
				}
			}
		}
		public void UpdateSessionLeaseTimes(IDictionary<int, ILease> sessionLeases)
		{
			lock (Data)
			{
				try
				{
					foreach (SessionRow sessionRow in Data.Session.Rows.Clone())
					{
						if (sessionLeases.ContainsKey(sessionRow.SessionId))
						{
							ILease lease = sessionLeases[sessionRow.SessionId];

							sessionRow.BeginEdit();

							sessionRow.LeaseTime = lease != null
								? lease.CurrentLeaseTime
								: (TimeSpan?)null;
							sessionRow.LeaseState = lease != null
								? lease.CurrentState
								: (LeaseState?)null;

							sessionRow.EndEdit();
						}
					}

					AcceptChanges();
				}
				catch (Exception)
				{
					RejectChanges();
					throw;
				}
			}
		}
	}
}