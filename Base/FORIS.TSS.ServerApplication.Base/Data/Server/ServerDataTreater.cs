﻿using FORIS.TSS.BusinessLogic.Data.Server;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Helpers.Data.Server;

namespace FORIS.TSS.ServerApplication.Data.Server
{
	public partial class ServerDataTreater : DataTreater<ServerData>, IServerDataTreater
	{
		public ServerDataTreater(ServerData serverData, ISessionInfo sessionInfo)
			: base(null, serverData, sessionInfo)
		{
		}
	}
}