﻿using System;
using System.ComponentModel;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Lifetime;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.BusinessLogic.Server;

namespace FORIS.TSS.ServerApplication
{
	/// <summary> Объект сессии пользователя </summary>
	/// <remarks>
	/// В этом классе должны быть только самые общие
	/// механизмы управления сессией. Никаких вещей
	/// связанных с логикой предметной области
	/// </remarks>
	public abstract class PersonalServerBase<TServer> : Component, IPersonalServer where TServer : ServerBase<TServer>
	{
		public abstract string ApplicationName { get; }

		protected TServer                 serverInstance;
		protected SessionManager<TServer> sessionManager;

		#region Constructor & Dispose

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				Close();
				closingDelegate = null;
				closedDelegate  = null;

				RemotingServices.Disconnect(this);
			}

			base.Dispose(disposing);
		}

		#endregion Constructor & Dispose

		#region Initialize

		private ILease lease;

		public override object InitializeLifetimeService()
		{
			lease = (ILease)base.InitializeLifetimeService();

			// Лицензия у объекта будет. С правилами, но без спонсора
			//if (Lease.CurrentState == LeaseState.Initial)
			//{
			//    this.lease.Register(this);
			//}

			return lease;
		}
		public virtual void Init(TServer serverInstance, SessionManager<TServer> sessionManager, ISessionInfo sessionInfo)
		{
			#region Preconditions

			switch (serverState)
			{
				case PersonalServerState.Initialized:
					throw new InvalidOperationException("Session is already initialized");
				case PersonalServerState.Closed:
					throw new InvalidOperationException("Session is already closed");
				case PersonalServerState.Undefined:
					break; //Именно этого состояния и ждем, чтобы поменять на Initialized
			}

			#endregion Preconditions

			this.serverInstance = serverInstance;
			this.sessionManager = sessionManager;
			this.SessionInfo    = sessionInfo;
			this.serverState    = PersonalServerState.Initialized;
		}

		#endregion Initialize

		public ISessionInfo SessionInfo { get; private set; } = null;

		/// <summary> Identifier of the session in the database </summary>
		public int SessionId => SessionInfo?.SessionId ?? int.MinValue;

		#region Close

		public enum PersonalServerState
		{
			Undefined   = 0,
			Initialized = 1,
			Closed      = 2
		}

		private PersonalServerState serverState = PersonalServerState.Undefined;
		/// <summary> Закрыть сессию </summary>
		public void Close()
		{
			#region Preconditions

			if (serverState == PersonalServerState.Undefined)
				throw new InvalidOperationException("Session is not initialized yet");

			#endregion Preconditions

			if (serverState != PersonalServerState.Closed)
			{
				OnClosing();
				serverState = PersonalServerState.Closed;
				OnClosed();
				// Нет возможности генерировать события после вызова Dispose()
				Dispose();
			}
		}

		#endregion Close

		TimeSpan IPersonalServer.LeaseRenewOnCallTime
		{
			get
			{
				if (null == lease)
					throw new InvalidOperationException("Call this property only through remoting");
				return lease.RenewOnCallTime;
			}
		}
		public bool IsAlive()
		{
			switch (serverState)
			{
				case PersonalServerState.Closed:
					return false;
				default:
					using (var sp = serverInstance.Database.Procedure("dbo.LogSessionActivity"))
					{
						sp["@session_id"].Value = SessionId;
						sp.ExecuteNonQuery();
					}
					return true;
			}
		}
		#region Events

		#region Closing & Closed

		private      EventHandler closingDelegate;
		public event EventHandler Closing
		{
			add    { closingDelegate -= value; closingDelegate += value; }
			remove { closingDelegate -= value; }
		}
		protected virtual void OnClosing()
		{
			closingDelegate?.Invoke(this, EventArgs.Empty);
		}
		private      EventHandler closedDelegate;
		public event EventHandler Closed
		{
			add    { closedDelegate -= value; closedDelegate += value; }
			remove { closedDelegate -= value; }
		}
		protected virtual void OnClosed()
		{
			closedDelegate?.Invoke(this, EventArgs.Empty);
		}

		#endregion Closing & Closed

		#endregion Events

		#region ISponsor Members

		TimeSpan ISponsor.Renewal(ILease lease)
		{
			TimeSpan result;

			/* Объект сессии пользователя является спонсором
			 * для персональных поставщиков данных контейнеров,
			 * Спонсирование их на время остатка собственной
			 * лицензии объекта сессии пользователя полезно
			 * освобождением ресурсов используемых этими
			 * объектами почти одновременно с объектом сессии.
			 */

			ILease ownLease = (ILease)RemotingServices.GetLifetimeService(this);

			result = ownLease.CurrentLeaseTime < lease.InitialLeaseTime
				? ownLease.CurrentLeaseTime
				: lease.InitialLeaseTime;

			return result; //ownLease.CurrentLeaseTime;
		}

		#endregion ISponsor Members

		#region IPersonalServer Members

		SetNewPasswordResult IPersonalServer.SetPassword(string password)
		{
			return SetPassword(password, SessionInfo.OperatorInfo.OperatorId);
		}
		SetNewLoginResult IPersonalServer.SetNewLogin(string login)
		{
			return SetNewLogin(login);
		}

		public abstract TssPrincipalInfo GetPrincipal();

		#endregion IPersonalServer Members

		protected abstract bool CheckPassword(string password);
		protected abstract SetNewPasswordResult SetPassword(string password, int operatorId);
		protected abstract SetNewLoginResult SetNewLogin(string login);
	}
}