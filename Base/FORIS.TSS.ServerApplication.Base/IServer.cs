﻿using System;
namespace FORIS.TSS.ServerApplication
{
	/// <summary> Общий интерфейс для всех серверов </summary>
	/// <remarks> Используется для управления запуском/остановом сервера, для контроля состояния сервера </remarks>
	public interface IServer : IDisposable
	{
		ServerState State { get; }
		void Start();
		void Stop();
		event ServerExitEventHandler Exit;
	}
	public enum ServerState
	{
		Undefined,
		Configuring,
		Configured,
		Starting,
		Running,
		Stopping,
		Stopped
	}
	public delegate void ServerExitEventHandler(IServer server, string message);
}