﻿using System;
using System.ComponentModel;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Lifetime;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.BusinessLogic.Server;

namespace FORIS.TSS.ServerApplication
{
	/// <summary> Класс объекта административной сессии </summary>
	/// <remarks> Предоставляет доступ к поставщику данных сервера </remarks>
	public class AdminPersonalServer : Component, IAdminPersonalServer
	{
		#region Constructor & Dispose

		public AdminPersonalServer(IAdminSessionManager sessionManager, ISessionInfo sessionInfo)
		{
			this.sessionManager = sessionManager;
			this.sessionInfo    = sessionInfo;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				Close();
				RemotingServices.Disconnect(this);
			}
			base.Dispose(disposing);
		}

		#endregion Constructor & Dispose

		#region Initialize

		private ILease lease;

		public override object InitializeLifetimeService()
		{
			lease = (ILease)base.InitializeLifetimeService();
			return lease;
		}

		#endregion Initialize

		private readonly IAdminSessionManager sessionManager;
		private readonly ISessionInfo         sessionInfo;

		#region IPersonalServer Members

		public string ApplicationName
		{
			get { throw new NotSupportedException(); }
		}
		public ISessionInfo SessionInfo
		{
			get { return sessionInfo; }
		}
		private bool closed = false;
		/// <summary> Закрыть сессию </summary>
		public void Close()
		{
			if (!closed)
			{
				OnClosing();
				closed = true;
				OnClosed();
				Dispose(true);
			}
		}
		TimeSpan IPersonalServer.LeaseRenewOnCallTime
		{
			get
			{
				if (lease == null)
					throw new InvalidOperationException("Call this property only through remoting");
				return lease.RenewOnCallTime;
			}
		}
		/// <summary> Метод ничего не делает. Нужен только для проверки жизнеспособности </summary>
		public bool IsAlive()
		{
			return true;
		}

		/// <summary> Изменяет/задает собственный пароль оператора </summary>
		/// <param name="password"></param>
		/// <remarks>
		/// Может возникнуть исключение в случае 
		/// если пользователю не разрешено менять
		/// собственный пароль
		/// </remarks>
		public SetNewPasswordResult SetPassword(string password)
		{
			throw new NotImplementedException();
		}
		public SetNewLoginResult SetNewLogin(string newLogin)
		{
			throw new NotImplementedException();
		}
		public TssPrincipalInfo GetPrincipal()
		{
			return default(TssPrincipalInfo);
		}

		#endregion IPersonalServer Members

		#region ISponsor Members

		///<summary> Requests a sponsoring client to renew the lease for the specified object </summary>
		///<returns> The additional lease time for the specified object </returns>
		///<param name="lease"> The lifetime lease of the object that requires lease renewal </param>
		///<exception cref="T:System.Security.SecurityException"> The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public TimeSpan Renewal(ILease lease)
		{
			if (!closed)
				return lease.InitialLeaseTime;
			else
				return new TimeSpan(0);
		}

		#endregion ISponsor Members

		#region Closing & Closed

		private      EventHandler closingDelegate;
		public event EventHandler Closing
		{
			add    { closingDelegate -= value; closingDelegate += value; }
			remove { closingDelegate -= value; }
		}
		protected virtual void OnClosing()
		{
			closingDelegate?.Invoke(this, EventArgs.Empty);
		}
		private      EventHandler closedDelegate;
		public event EventHandler Closed
		{
			add    { closedDelegate -= value; closedDelegate += value; }
			remove { closedDelegate -= value; }
		}
		protected virtual void OnClosed()
		{
			closedDelegate?.Invoke(this, EventArgs.Empty);
		}
		#endregion Closing & Closed
	}
}