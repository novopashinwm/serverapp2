﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;

namespace FORIS.TSS.ServerApplication
{
	/// <summary> Базовый класс сервера </summary>
	/// <remarks>
	/// Так как это обобщенный тип, то ни он ни наследованный 
	/// от него компонент не может разрабатываться в дизайнере
	/// </remarks>
	public abstract class ServerBase<TServer> : Component, IServer, IDatabaseProvider where TServer : ServerBase<TServer>
	{
		#region Controls & Components

		private IContainer components;

		#endregion Controls & Components

		#region Constructor & Dispose

		protected ServerBase(SessionManager<TServer> sessionManager, NameValueCollection properties)
		{
			OneSessionPerLogin = string.Equals(properties["oneSessionPerLogin"], "true", StringComparison.OrdinalIgnoreCase);
			DeleteExistSession = string.Equals(properties["deleteExistSession"], "true", StringComparison.OrdinalIgnoreCase);

			if (properties["adminGroup"] != null)
				AdminGroup = properties["adminGroup"];

			InitializeComponent();

			this.sessionManager = sessionManager;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				components?.Dispose();
				components = null;

				exitDelegate = null;

				sessionManager?.Dispose();
				sessionManager = null;
			}
			base.Dispose(disposing);
		}

		#endregion Constructor & Dispose

		#region Properties

		/// <summary> Флаг, позволяющий иметь несколько сессий под одним логином </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool OneSessionPerLogin { get; }

		/// <summary> Флаг, удаления уже существующей с тем же логином сессии </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool DeleteExistSession { get; }
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public string AdminGroup { get; } = string.Empty;

		/// <summary> Массив байт для хранения паролей, применяемых в симметричных алгоритмах </summary>
		public static byte[] ValidateBytes = null;

		#endregion Properties

		#region serverDispatcher

		private ServerDispatcher<TServer> serverDispatcher;
		private ServerAmbassador<TServer> serverAmbassador;

		/// <summary>
		/// В этом методе создайте и верните подходящий экземпляр диспетчера.
		/// При уничтожении сервера не забудьте вызвать Dispose() для диспетчера.
		/// </summary>
		/// <returns></returns>
		protected abstract ServerDispatcher<TServer> GetServerDispatcher();

		#endregion serverDispatcher

		#region Init

		protected void Init()
		{
			Database = GetDatabaseDataSupplier();

			serverDispatcher = GetServerDispatcher();
			serverAmbassador = new ServerAmbassador<TServer>();
			serverDispatcher.Ambassadors.Add(serverAmbassador);

			serverDispatcher.Server = (TServer)this;
		}

		#endregion Init

		#region Component Designer generated code

		private void InitializeComponent()
		{
			components = new Container();
		}

		#endregion Component Designer generated code

		#region Members

		private SessionManager<TServer> sessionManager;

		#endregion Members

		#region IServer members

		private ServerState state = ServerState.Undefined;

		ServerState IServer.State
		{
			get { return state; }
		}

		void IServer.Start()
		{
			using var logger = new Stopwatcher($"################ Start server '{this.GetTypeName(false)}'", null, TimeSpan.Zero);
			state = ServerState.Starting;
			Start();

			#region sessionManager

			if (sessionManager == null)
				throw new ApplicationException("Server can not finish starting without session manager");
			try
			{
				serverAmbassador.Item = sessionManager;
			}
			catch (Exception ex)
			{
				Trace.TraceError(GetType().Name + ": Start error {0}\n{1}", ex.Message, ex.StackTrace);
				throw ex;
			}

			#endregion sessionManager

			state = ServerState.Running;
		}

		void IServer.Stop()
		{
			using var logger = new Stopwatcher($"################ Stop server '{this.GetTypeName(false)}'", null, TimeSpan.Zero);
			state = ServerState.Stopping;

			#region sessionManager

			serverAmbassador.Item = null;

			#endregion sessionManager

			Stop();
			state = ServerState.Stopped;
		}

		private ServerExitEventHandler         exitDelegate;
		event   ServerExitEventHandler IServer.Exit
		{
			add    { exitDelegate -= value; exitDelegate += value; }
			remove { exitDelegate -= value; }
		}
		
		protected virtual void OnExit(string message)
		{
			exitDelegate?.Invoke(this, message);
		}

		#endregion IServer members

		#region Start & Stop

		/// <summary> Формирует объекты инфраструктуры сервера </summary>
		protected abstract void Start();
		/// <summary> Освобождает объекты инфраструктуры сервера </summary>
		protected abstract void Stop();

		#endregion Start & Stop

		#region Properties

		protected abstract IDatabaseDataSupplier GetDatabaseDataSupplier();

		public IDatabaseDataSupplier Database { get; private set; }

		#endregion Properties
	}
}