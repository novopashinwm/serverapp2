﻿using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Helpers;

namespace FORIS.TSS.ServerApplication.Helpers
{
	public static class OperatorInfoHelper
	{
		private static readonly string[] TableNames = new[] { "OPERATOR" };

		public static IOperatorInfo Create(int operatorId, IDatabaseDataSupplier databaseDataSupplier)
		{
			var operatorTable = databaseDataSupplier.GetDataFromDB(
				new[] { new ParamValue("@operator_id", operatorId) }, "dbo.GetOperatorInfoByID", TableNames).Tables["OPERATOR"];

			return CreateOperatorInfo(operatorTable);
		}
		public static IOperatorInfo Create(string login, IDatabaseDataSupplier databaseDataSupplier)
		{
			if (string.IsNullOrWhiteSpace(login))
				throw new ArgumentException(@"Value cannot be null or whitespace", nameof(login));

			var operatorTable = databaseDataSupplier.GetDataFromDB(
				new[] { new ParamValue("@login", login) }, "dbo.GetOperatorInfoByLogin", TableNames).Tables["OPERATOR"];

			return CreateOperatorInfo(operatorTable);
		}
		private static IOperatorInfo CreateOperatorInfo(DataTable operatorTable)
		{
			if (operatorTable.Rows.Count == 0)
				return null;

			var row = operatorTable.Rows[0];

			return new OperatorInfo(
				(int)row["OPERATOR_ID"],
				row["LOGIN"] as string,
				row["NAME"] as string
				);
		}
	}
}