﻿using System.Collections.Generic;
using System.Diagnostics;

namespace FORIS.TSS.ServerApplication.Diagnostic
{
	public class Counter
	{
		public string                 Name  { get; set; }
		public string                 Help  { get; set; }
		public CounterGroup           Group { get; set; }
		public PerformanceCounterType Type  { get; set; }
		public readonly List<string>  Instances = new List<string>();
		public override string ToString()
		{
			return string.Format("{0} {1} {2}", Name, Group, Type);
		}
	}
}