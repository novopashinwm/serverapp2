﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace FORIS.TSS.ServerApplication.Diagnostic
{
	public class Counters
	{
		struct CounterContext
		{
			public readonly CounterGroup Group;
			public readonly string       Instance;
			public CounterContext(CounterGroup counterGroup, string instance)
			{
				Group    = counterGroup;
				Instance = instance;
			}
			private bool Equals(CounterContext other)
			{
				return Group == other.Group && string.Equals(Instance, other.Instance);
			}
			public override int GetHashCode()
			{
				unchecked
				{
					return ((int)Group * 397) ^ (Instance != null ? Instance.GetHashCode() : 0);
				}
			}
			public override bool Equals(object obj)
			{
				if (ReferenceEquals(null, obj))
				{
					return false;
				}
				return obj is CounterContext && Equals((CounterContext)obj);
			}
		}
		public class Instances
		{
			public const string Tcp            = "tcp";
			public const string Http           = "http";
			public const string SaveQueue      = "saveQueue";
			public const string ReceiveQueue   = "receiveQueue";
			public const string TcpConnections = "tcp_connections";
			public const string Publisher      = "publisher";
			public const string SaverTask      = "saverTask";
		}
		public const string CategoryName       = "Ufin Counters";
		private static readonly Dictionary<CounterGroup, Counter> Groups = new List<Counter>
		{
			new Counter
			{
				Name  = "Queue counts",
				Help  = "Количество элементов в очереди",
				Group = CounterGroup.Count,
				Type  = PerformanceCounterType.NumberOfItems32,
			},
			new Counter
			{
				Name  = "Average counts",
				Help  = "Количество элементов, обработанных в секунду",
				Group = CounterGroup.CountPerSecond,
				Type  = PerformanceCounterType.RateOfCountsPerSecond32,
			}
		}.ToDictionary(c => c.Group);
		public static void CreateCounters()
		{
			if (PerformanceCounterCategory.Exists(CategoryName))
				PerformanceCounterCategory.Delete(CategoryName);

			var counters = new CounterCreationDataCollection();
			foreach (var counter in Groups.Values)
			{
				counters.Add(new CounterCreationData
				{
					CounterName = counter.Name,
					CounterHelp = counter.Help,
					CounterType = counter.Type
				});
			}
			PerformanceCounterCategory.Create(CategoryName, CategoryName, PerformanceCounterCategoryType.MultiInstance, counters);
		}
		private static readonly object Lock = new object();
		private static Counters _instance;
		public static Counters Instance
		{
			get
			{
				if (_instance != null)
					return _instance;

				lock (Lock)
				{
					if (_instance == null)
					{
						var instance = new Counters();
						Thread.MemoryBarrier();
						_instance = instance;
					}
				}
				return _instance;
			}
		}
		public PerformanceCounter GetCounterReadOnly(CounterGroup counterGroup, string instance)
		{
			if (PerformanceCounterCategory.Exists(CategoryName))
			{
				var performanceCounterCategory = new PerformanceCounterCategory(CategoryName);
				var counter = default(Counter);
				if (Groups.TryGetValue(counterGroup, out counter))
				{
					if (performanceCounterCategory.CounterExists(counter.Name) &&
						performanceCounterCategory.InstanceExists(instance))
					{
						return new PerformanceCounter(CategoryName, counter.Name, instance);
					}
				}
			}
			return default(PerformanceCounter);
		}
		public PerformanceCounter GetCounterReadOnly(CounterGroup counterGroup, string instance, string subinstance)
		{
			return GetCounterReadOnly(counterGroup, $"{instance}_{subinstance}");
		}
		public void ResetCounter(CounterGroup counterGroup, string instance)
		{
			ResetCounter(counterGroup, instance, 0);
		}
		public void ResetCounter(CounterGroup counterGroup, string instance, string subinstance)
		{
			ResetCounter(counterGroup, $"{instance}_{subinstance}", 0);
		}
		private void ResetCounter(CounterGroup counterGroup, string instance, int value = 0)
		{
			if (!Groups.ContainsKey(counterGroup))
				return;

			var counter = Groups[counterGroup];
			if (!counter.Instances.Contains(instance))
				counter.Instances.Add(instance);

			var performanceCounter = GetCounter(counter, instance);
			if (performanceCounter != null)
				performanceCounter.RawValue = value;
		}
		public void Increment(CounterGroup counterGroup, string instance, string subinstance, int value = 1)
		{
			var counter = string.Format("{0}_{1}", instance, subinstance);
			Increment(counterGroup, counter, value);
		}
		public void Decrement(CounterGroup counterGroup, string instance, string subinstance, int value = 1)
		{
			var counter = string.Format("{0}_{1}", instance, subinstance);
			Decrement(counterGroup, counter, value);
		}
		public void Increment(CounterGroup counterGroup, string instance, int value = 1)
		{
			IncrementCounter(counterGroup, instance, value);
		}
		public void Decrement(CounterGroup counterGroup, string instance, int value = 1)
		{
			IncrementCounter(counterGroup, instance, -value);
		}
		private void IncrementCounter(CounterGroup counterGroup, string instance, int value)
		{
			if (value == 0)
				return;

			var counter = Groups[counterGroup];
			var performanceCounter = GetCounter(counter, instance);
			if (performanceCounter == null)
				return;

			try
			{
				if (value == 1)
				{
					performanceCounter.Increment();
					return;
				}

				if (value == -1)
				{
					performanceCounter.Decrement();
					return;
				}

				performanceCounter.IncrementBy(value);
			}
			catch (Exception ex)
			{
				Trace.TraceError("IncrementCounter({0}, {1}, {2}): {3}",
					counterGroup, instance, value, ex);
			}
		}
		private PerformanceCounter GetCounter(Counter counter, string instance)
		{
			var counterContext = new CounterContext(counter.Group, instance);
			var performanceCounter = _counters.GetOrAdd(counterContext, group => CreateCounter(counter, instance));
			return performanceCounter;
		}
		private PerformanceCounter CreateCounter(Counter counter, string instance)
		{
			PerformanceCounter performanceCounter = null;
			try
			{
				performanceCounter = new PerformanceCounter(CategoryName, counter.Name, instance, false);
			}
			catch (Exception ex)
			{
				Trace.TraceError("{0} couldn't create performance counter: {1}. {2}", this, counter, ex.Message);
			}

			return performanceCounter;
		}
		private readonly ConcurrentDictionary<CounterContext, PerformanceCounter> _counters = new ConcurrentDictionary<CounterContext, PerformanceCounter>();
	}
}