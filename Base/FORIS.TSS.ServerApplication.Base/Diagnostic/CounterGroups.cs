﻿namespace FORIS.TSS.ServerApplication.Diagnostic
{
	public enum CounterGroup
	{
		Count,
		CountPerSecond
	}
}