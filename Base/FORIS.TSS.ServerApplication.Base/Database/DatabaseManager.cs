﻿using System;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using FORIS.DataAccess;
using FORIS.DataAccess.Interfaces;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;

namespace FORIS.TSS.ServerApplication.Database
{
	/// <summary> Менеджер базы данных </summary>
	public class DatabaseManager : Component, IDatabaseDataSupplier
	{
		private IDatasetAdapterFactory _adapterFactory;
		/// <summary> Фабрика адаптеров </summary>
		[Browsable(true)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public IDatasetAdapterFactory AdapterFactory
		{
			get { return _adapterFactory; }
			set { _adapterFactory = value; }
		}

		private IDatabaseSchema _databaseSchema;
		/// <summary> Схема БД </summary>
		[Browsable(true)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public IDatabaseSchema DatabaseSchema
		{
			get { return _databaseSchema; }
			set { _databaseSchema = value; }
		}

		private TssDatabase _database;
		[Browsable(true)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public TssDatabase Database
		{
			get { return _database; }
			set { _database = value; }
		}

		#region Constructor & Dispose

		public DatabaseManager()
		{
		}
		/// <summary> Конструктор </summary>
		/// <param name="container"></param>
		public DatabaseManager(IContainer container)
			: this()
		{
			container.Add(this);
		}

		#endregion Constructor & Dispose

		#region Implement IDatabaseDataSupplier

		IStoredProceduresParametersCache IDatabaseDataSupplier.ParametersCache
		{
			get
			{
				if (_database == null)
					throw new InvalidOperationException("There is no database object");

				return _database.ParametersCache;
			}
		}
		IStoredProcedure IDatabaseDataSupplier.Procedure(string procedureName)
		{
			if (_database == null)
				throw new InvalidOperationException("There is no database object");

			return _database.Procedure(procedureName);
		}
		DataSet IDatabaseDataSupplier.GetRecords(string tableName, string whereClause)
		{
			if (_database == null)
				throw new InvalidOperationException("There is no database object");

			string text = "select * from [" + tableName + "]";

			if (whereClause != null && whereClause.Trim().Length > 0)
			{
				text = text + " where " + whereClause;
			}

			using (IDbCommand selectCommand = _database.Command(text))
			{
				selectCommand.Connection  = _database.Connection();
				selectCommand.CommandType = CommandType.Text;

				var dataSet = new DataSet();

				try
				{
					IDbDataAdapter adapter = _database.Adapter();
					adapter.SelectCommand = selectCommand;
					adapter.Fill(dataSet);
					dataSet.Tables[0].TableName = tableName;
				}
				finally
				{
					selectCommand.Parameters.Clear();
				}

				return dataSet;
			}
		}
		DataSet IDatabaseDataSupplier.GetDataFromDB(ParamValue[] @params, string procedureName, string[] strTablesNames)
		{
			if (_database == null)
				throw new InvalidOperationException("There is no database object");

			using (var sp = _database.Procedure(procedureName))
			{
				foreach (ParamValue param in @params)
					sp[param.strParam].Value = param.value;

				var dataSet = sp.ExecuteDataSet();

				// Для изменяющих процедур, которые ничего не возвращают
				if (strTablesNames == null)
					return dataSet;

				DatasetHelperBase.RenameTables(dataSet, strTablesNames);

				return dataSet;
			}
		}
		void IDatabaseDataSupplier.Update(DataSet dataSet)
		{
			$"!!!!!!<Depreacated, any hit here is an error>!!!!!!".CallTraceError();
			if (_databaseSchema == null)
				throw new InvalidOperationException("There is no database schema object");
		
			if (_adapterFactory == null)
				throw new InvalidOperationException("There is no adapter factory object");

			var adapter = _adapterFactory.GetAdapter();

			DatabaseManager.Update(adapter, dataSet, _databaseSchema);

			/* Возможно конечно не очень хорошо создавать
			 * новый объект адаптера для каждого обновления?
			 */
		}
		string IDatabaseDataSupplier.GetConstant(string name)
		{
			if (_database == null)
				throw new InvalidOperationException("There is no database object");

			using (var sp = _database.Procedure("dbo.GetConstant"))
			{
				sp["@NAME"].Value = name;

				return (string)sp.ExecuteScalar();
			}
		}
		void IDatabaseDataSupplier.SetConstant(string name, string value)
		{
			if (_database == null)
				throw new InvalidOperationException("There is no database object");

			using (var sp = _database.Procedure("dbo.SetConstant"))
			{
				sp["@NAME"].Value = name;
				sp["@VALUE"].Value = value;

				sp.ExecuteNonQuery();
			}
		}

		#endregion Implement IDatabaseDataSupplier

		#region Update

		/// <summary> Обновляет набор данных в БД </summary>
		/// <param name="da"> ссылка на адаптер для обновления набора данных в БД </param>
		/// <param name="dataset"> ссылка на набор данных, который требуется обновить в БД </param>
		/// <param name="databaseSchema"> объект способный изготовить подходящую схему данных </param>
		/// <remarks> Вызывается из объекта персонального сервера </remarks>
		public static void Update(IDatasetAdapter da, DataSet dataset, IDatabaseSchema databaseSchema)
		{
			using (var connection = Globals.TssDatabaseManager.DefaultDataBase.Connection())
			{
				connection.Open();
				try
				{
					using (var transaction = connection.BeginTransaction())
					{
						DatabaseManager.Update(da, dataset, connection, transaction, databaseSchema);
						transaction.Commit();
					}
				}
				finally
				{
					connection.Close();
				}
			}
		}

		/// <summary> Updates dataset through given connection with transaction </summary>
		/// <param name="da"></param>
		/// <param name="dataset"></param>
		/// <param name="connection"></param>
		/// <param name="transaction"></param>
		/// <param name="databaseSchema"></param>
		public static void Update(IDatasetAdapter da, DataSet dataset, IDbConnection connection, IDbTransaction transaction, IDatabaseSchema databaseSchema)
		{
			Trace.WriteLineIf(DatabaseManager.TraceSwitch.TraceInfo, "PSB::Update entrance");

			DatasetHelperBase.DumpTraceDiff(dataset);

			Trace.WriteLineIf(DatabaseManager.TraceSwitch.TraceInfo, "PSB::Update remove fresh records");

			DatasetHelperBase.CompactTables(dataset, databaseSchema); // don't remove this line

			DatasetHelperBase.DumpTraceDiff(dataset);

			// update primary dataset
			Debug.Assert(connection.State == ConnectionState.Open);
			da.Update(dataset, connection, transaction);
		}

		#endregion Update

		#region TraceSwitch

		public static readonly TraceSwitch TraceSwitch =
			new TraceSwitch("DatabaseManager", "", "Info");

		#endregion TraceSwitch
	}
}