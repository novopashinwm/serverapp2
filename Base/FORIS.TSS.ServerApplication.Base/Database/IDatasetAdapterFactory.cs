﻿using FORIS.DataAccess;

namespace FORIS.TSS.ServerApplication.Database
{
	public interface IDatasetAdapterFactory
	{
		IDatasetAdapter GetAdapter();
	}
}