﻿using FORIS.TSS.Helpers;
using FORIS.TSS.ServerApplication.Data.Server;

namespace FORIS.TSS.ServerApplication
{
	/// <summary> Для внутреннего использования в сервере </summary>
	public interface IAdminSessionManager
	{
		ServerServerData ServerData { get; }
		IDatabaseDataSupplier DatabaseDataSupplier { get; }
		void CloseSession(int sessionId);
		/// <summary> Закрыть все другие сессии оператора </summary>
		/// <param name="operatorId"></param>
		void CloseSessionsByOperatorId(int operatorId);
	}
}