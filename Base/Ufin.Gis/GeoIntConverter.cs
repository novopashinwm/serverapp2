﻿using System;
using System.Drawing;

namespace Compass.Ufin.Gis
{
	/// <summary>
	/// Класс для преобразования geo-координат в целочисленные данные.
	/// Применяется для борьбы с неточностью типа данных Float.
	/// Округление geo-координат идет до 5 знака после запятой (0.00001 градус примерно равен 1 метр).
	/// Так же борется с отрицательными координатами для южного и западного полушарий
	/// </summary>
	public static class GeoIntConverter
	{
		/// <summary> Величина округления - Умножаем на эту величину </summary>
		public const int ROUND_SCALE = 100000;
		/// <summary> Преобразование широты в int </summary>
		/// <param name="lat">широта в градусах</param>
		/// <returns></returns>
		public static int LatToInt(double lat)
		{
			return (int)Math.Round((lat + 90) * ROUND_SCALE);
		}
		/// <summary> Преобразование целочисленного представления широты в градусы </summary>
		/// <param name="intLat">целочисленное представление широты</param>
		/// <returns>Широта в градусах</returns>
		public static double IntToLat(int intLat)
		{
			var res = intLat / (double)ROUND_SCALE;
			res = res - 90;
			return res;
		}
		/// <summary> Преобразование долготы в int </summary>
		/// <returns></returns>
		public static int LngToInt(double lng)
		{
			int res = 0;
			if (lng < -90)
			{
				//Если ближе к пересечению меридиан 180 | -180
				res = (int)Math.Round((360 + 180 + lng) * ROUND_SCALE);
			}
			else
			{
				res = (int)Math.Round((lng + 180) * ROUND_SCALE);
			}

			return res;
		}
		/// <summary> Преобразование целочисленного представления долготы в градусы </summary>
		/// <param name="intLng">целочисленное представление долготы</param>
		/// <returns>Долгота в градусах</returns>
		public static double IntToLng(int intLng)
		{
			var res = intLng / (double)ROUND_SCALE;
			if (res <= 360)
			{
				res = res - 180;
			}
			else
			{
				res = res - 360 - 180;
			}

			return res;
		}
		/// <summary> Преобразование точки с geo-координатами в точку в целочисленном представлении </summary>
		/// <param name="geoPoint"></param>
		/// <returns></returns>
		public static Point GeoPointToIntPoint(PointF geoPoint)
		{
			return new Point(LngToInt(geoPoint.X), LatToInt(geoPoint.Y));
		}
		/// <summary> Преобразование точки в целочисленном представлении в точку с geo-координатами </summary>
		/// <param name="intPoint"></param>
		/// <returns></returns>
		public static PointF IntPointToGeoPoint(Point intPoint)
		{
			return new PointF((float)IntToLng(intPoint.X), (float)IntToLat(intPoint.Y));
		}
	}
}