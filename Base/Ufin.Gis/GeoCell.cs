﻿using System;
using System.Drawing;
using FORIS.TSS.BusinessLogic.Map;

namespace Compass.Ufin.Gis
{
	public class GeoCell
	{
		public PointF Start;
		public PointF End;
		public MatrixNode Node;
		public PointF Center
		{
			get
			{
				return new PointF((End.X + Start.X) / 2, (End.Y + Start.Y) / 2);
			}
		}
		public float SizeX
		{
			get { return End.X - Start.X; }
		}
		public float SizeY
		{
			get { return End.Y - Start.Y; }
		}
		public override string ToString()
		{
			return string.Format("[{0}:{1}] - [{2}:{3}] {4}",
				Start.X,
				Start.Y,
				End.X,
				End.Y,
				Node.Node);
		}
		public static implicit operator GeoCell(MatrixNode node)
		{
			var scale     = (int)(node.Node / Math.Pow(10, 15));
			var koords    = (long)(node.Node - scale * Math.Pow(10, 15));
			var scale10   = (int)Math.Pow(10, scale);
			var intPointX = (int)(koords / Math.Pow(10, 8 - scale));
			var intPointY = (int)(koords - intPointX * Math.Pow(10, 8 - scale));

			intPointX *= scale10;
			intPointY *= scale10;

			var bottomLeftPoint = new Point(intPointX - scale10, intPointY - scale10);
			var topRightPoint = new Point(intPointX, intPointY);

			return new GeoCell
			{
				Start = GeoIntConverter.IntPointToGeoPoint(bottomLeftPoint),
				End   = GeoIntConverter.IntPointToGeoPoint(topRightPoint),
				Node  = node
			};
		}
	}
}