﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Map;
using GeoAPI.Geometries;
using NetTopologySuite.Geometries;
using Point = System.Drawing.Point;

namespace Compass.Ufin.Gis
{
	/// <summary> Класс с функциями для индексации geo-объектов для их быстрого поиска </summary>
	public static class GeoIndex
	{
		public const int MaxMatrixNodeCount = 1000;

		/// <summary> Преобразует набор точек в полигон в модели NetTopologySuite </summary>
		/// <returns></returns>
		private static IPolygon ConvertPointsToPolygon(this GeometryFactory factory, IList<GeoZonePoint> points)
		{
			if (points == null || points.Count == 0)
			{
				return null;
			}

			Coordinate[] coords;
			//для замкнутого полигона последняя точка должна быть равна первой точке
			if ((decimal)points[0].Lng != (decimal)points[points.Count - 1].Lng
				|| (decimal)points[0].Lat != (decimal)points[points.Count - 1].Lat)
			{
				coords = new Coordinate[points.Count + 1];
				coords[points.Count] = new Coordinate(GeoIntConverter.LngToInt(points[0].Lng), GeoIntConverter.LatToInt(points[0].Lat));
			}
			else
			{
				coords = new Coordinate[points.Count];
			}

			for (var i = 0; i < points.Count; i++)
			{
				var point = points[i];
				coords[i] = new Coordinate(GeoIntConverter.LngToInt(point.Lng), GeoIntConverter.LatToInt(point.Lat));
			}

			var poly = factory.CreatePolygon(factory.CreateLinearRing(coords), null);

			return poly;
		}
		/// <summary> Рассчитывает ячейки матрицы для круга </summary>
		/// <param name="centerPoint">точка центра круга</param>
		/// <param name="maxMatrixNodeCount"></param>
		/// <returns>список ячеек матрицы</returns>
		public static IEnumerable<MatrixNode> GetMatrixForCircle(this GeoZonePoint centerPoint, int maxMatrixNodeCount = MaxMatrixNodeCount)
		{
			return centerPoint
				.GetOutsidePolygon4CircleZone()
				.GetMatrixForPolygon(maxMatrixNodeCount);
		}
		public static GeoZonePoint[] GetOutsidePolygon4CircleZone(this GeoZonePoint centerPoint, int vertexCount = 16)
		{
			if (centerPoint.Radius == null)
				return Array.Empty<GeoZonePoint>();
			// Меняем радиус точки, так чтобы полигон был описан вокруг круговой зоны
			var outerCenterPoint = new GeoZonePoint
			{
				Lat    = centerPoint.Lat,
				Lng    = centerPoint.Lng,
				Radius = (float)(centerPoint.Radius / Math.Cos(Math.PI / vertexCount))
			};

			return GetInsidePolygon4CircleZone(outerCenterPoint, vertexCount);
		}
		public static GeoZonePoint[] GetInsidePolygon4CircleZone(this GeoZonePoint centerPoint, int vertexCount = 8)
		{
			if (centerPoint.Radius == null)
				return Array.Empty<GeoZonePoint>();

			var radius = centerPoint.Radius.Value;
			var lat    = centerPoint.Lat;
			var lng    = centerPoint.Lng;

			var result = new List<GeoZonePoint>(vertexCount + 1);
			for (var i = 0; i < vertexCount; i++)
			{
				var latRadius = radius / 111111f;
				var lngRadius = radius / (111111 * Math.Cos(lat));
				var radiusLat = latRadius * Math.Sin(2 * Math.PI * i / vertexCount);
				var radiusLng = lngRadius * Math.Cos(2 * Math.PI * i / vertexCount);
				var lati = lat + radiusLat;
				var lngi = lng + radiusLng;
				var point = new GeoZonePoint { Lat = (float)lati, Lng = (float)lngi };
				result.Add(point);
			}
			// Добавляем в конец полигона начальную точку,
			// указывая таким образом замкнутость полигона
			result.Add(result[0]);
			return result.ToArray();
		}
		/// <summary> Рассчитывает ячейки матрицы для полигона </summary>
		/// <remarks>Индексного поиска вхождения точки в полигон</remarks>
		/// <param name="points">точки полигона</param>
		/// <param name="maxMatrixNodeCount">Максимальное кол-во результирующих узлов матрицы</param>
		/// <returns>список ячеек матрицы</returns>
		public static List<MatrixNode> GetMatrixForPolygon(this GeoZonePoint[] points, int maxMatrixNodeCount = MaxMatrixNodeCount)
		{
			if (points == null || points.Length == 0)
			{
				return null;
			}

			var factory = new GeometryFactory(new PrecisionModel(PrecisionModels.Fixed));
			// Создаем объект полигона в модели NetTopologySuite
			var polygon = factory.ConvertPointsToPolygon(points);
			return factory.GetMatrixNodesForPolygon(polygon, maxMatrixNodeCount);
		}
		/// <summary> Проверка валидности полигона </summary>
		/// <param name="points"></param>
		/// <returns></returns>
		public static bool ValidPolygon(this GeoZonePoint[] points)
		{
			if (points == null || points.Length == 0 || points.Any(p => p.Radius != null))
			{
				return false;
			}

			var factory = new GeometryFactory(new PrecisionModel(PrecisionModels.Fixed));
			// Создаем объект полигона в модели NetTopologySuite
			var polygon = factory.ConvertPointsToPolygon(points);
			return polygon.IsValid;
		}
		/// <summary> Проверка валидности круга </summary>
		/// <param name="points"></param>
		/// <returns></returns>
		public static bool ValidCircle(this GeoZonePoint[] points)
		{
			var centerPoint = points.SingleOrDefault();
			return centerPoint != null && centerPoint.Radius != null && centerPoint.Radius > 0;
		}
		private static List<MatrixNode> GetMatrixNodesForPolygon(this GeometryFactory factory, IPolygon polygon, int maxMatrixNodeCount)
		{
			//Оболочка полигона
			var envelope = polygon.EnvelopeInternal;
			/*
			С какого масштаба начать. (1 градус = 111 км)
			масштабы матрицы:
			0 = 1 м
			1 = 10 м
			2 = 100 м
			3 = 1000 м
			4 = 10000 м
			5 = 100000 м
			*/

			int matrixScale;
			const int minMinMatrixScale = 2;
			//Минимальный масштаб матрицы, до которого следует опускаться при указанных размерах полигона
			int minMatrixScale = minMinMatrixScale;
			//Определяем меньшую грань прямоугольника вокруг полигона
			var smallEdge = envelope.Width > envelope.Height ? envelope.Height : envelope.Width;
			smallEdge = smallEdge * 111 * 1000 / GeoIntConverter.ROUND_SCALE; //примерно в метрах

			//прикидываем чтобы для начала было от 4-х ячеек матрицы по самой короткой стороне
			if (smallEdge > 400000)
			{
				matrixScale = 5;
				minMatrixScale = 4;
			}
			else if (smallEdge > 40000)
			{
				matrixScale = 5;
				minMatrixScale = 3;
			}
			else if (smallEdge > 4000)
			{
				matrixScale = 5;
				minMatrixScale = 2;
			}
			else if (smallEdge > 400)
			{
				matrixScale = 4;
				minMatrixScale = minMinMatrixScale;
			}
			else if (smallEdge > 40)
			{
				matrixScale = 3;
				minMatrixScale = minMinMatrixScale;
			}
			else
			{
				matrixScale = minMatrixScale;
				minMatrixScale = minMinMatrixScale;
			}

			var matrixNodes = new List<MatrixNode>();
			factory.GetMatrixNodes(
				new Point((int)envelope.MinX, (int)envelope.MinY),
				new Point((int)envelope.MaxX, (int)envelope.MaxY),
				matrixNodes,
				matrixScale,
				minMatrixScale,
				maxMatrixNodeCount,
				polygon);

			return matrixNodes;
		}
		/// <summary> Рекурсивная функция для вычисления узлов матрицы </summary>
		/// <param name="factory"></param>
		/// <param name="bottomLeftRegionPoint">Левая нижняя точка прямоугольного региона для индексирования</param>
		/// <param name="topRightRegionPoint">Правая верхняя точка прямоугольного региона для индексирования</param>
		/// <param name="matrixNodes">Коллекция узлов матрицы</param>
		/// <param name="matrixScale">Масштаб матрицы</param>
		/// <param name="minMatrixScale">Минимальный масштаб матрицы, до которого следует опускаться при указанных размерах полигона</param>
		/// <param name="maxMatrixNodeCount">Максимальное кол-во результирующих узлов матрицы</param>
		/// <param name="polygon">Полигон</param>
		public static void GetMatrixNodes(this GeometryFactory factory,
			Point bottomLeftRegionPoint,
			Point topRightRegionPoint,
			List<MatrixNode> matrixNodes,
			int matrixScale,
			int minMatrixScale,
			int maxMatrixNodeCount,
			IPolygon polygon)
		{
			const double intersectionPercent = 0.5;

			//Определяем ячейки матрицы перекрывающиеся с полигоном
			Point bottomLeftNodeCorner = GetMatrixNodePoint(bottomLeftRegionPoint, matrixScale);
			Point topRightNodeCorner   = GetMatrixNodePoint(topRightRegionPoint, matrixScale);

			int step = GetMatrixStep(matrixScale);
			double matrixNodeArea = (double)step * step;
			const int fakeAddition = 1; //(float) 0.000006;
			for (int x = bottomLeftNodeCorner.X; x <= topRightNodeCorner.X; x += step)
			{
				for (int y = bottomLeftNodeCorner.Y; y <= topRightNodeCorner.Y; y += step)
				{
					//создаем квадрат узла матрицы
					var topRightCorner = new Point(x, y);
					var bottomLeftCorner = new Point(x - step, y - step);
					var matrixNode = factory.CreatePolygon(
						factory.CreateLinearRing(new Coordinate[]
						{
							new Coordinate(topRightCorner.X,   topRightCorner.Y),
							new Coordinate(topRightCorner.X,   bottomLeftCorner.Y),
							new Coordinate(bottomLeftCorner.X, bottomLeftCorner.Y),
							new Coordinate(bottomLeftCorner.X, topRightCorner.Y),
							new Coordinate(topRightCorner.X,   topRightCorner.Y)
						}),
						null);

					//проверяем перекрытие с полигоном
					try
					{
						var intersection = polygon.Intersection(matrixNode);

						if (intersection != null && !intersection.IsEmpty && intersection.Area > 0)
						{
							var intersectionArea = intersection.Area;
							if (intersectionArea / matrixNodeArea > intersectionPercent
								|| matrixScale == minMatrixScale
								|| matrixNodes.Count > maxMatrixNodeCount)
							{
								matrixNodes.Add(GetMatrixNode(new Point(x, y), matrixScale, (decimal)intersectionArea == (decimal)matrixNodeArea));
							}
							else
							{
								//Идем дальше по рекурсии
								factory.GetMatrixNodes(
									new Point(topRightCorner.X - step + fakeAddition, topRightCorner.Y - step + fakeAddition),
									new Point(topRightCorner.X,                       topRightCorner.Y),
									matrixNodes,
									matrixScale - 1,
									minMatrixScale,
									maxMatrixNodeCount,
									polygon);
							}
						}
					}
					catch (Exception ex)
					{
						Trace.TraceError("{0}", ex);
						matrixNodes.Add(GetMatrixNode(new Point(x, y), matrixScale, null));
					}
				}
			}
		}
		/// <summary> Возвращает шаг изменения координат для данного масштаба матрицы </summary>
		/// <param name="matrixScale">Масштаб матрицы</param>
		/// <returns>шаг изменения координат для данного масштаба матрицы</returns>
		private static int GetMatrixStep(int matrixScale)
		{
			return (int)(GeoIntConverter.ROUND_SCALE / Math.Pow(10, 5 - matrixScale));
		}
		/// <summary> Возвращает правый верхний угол узла матрицы, в которую попадает точка (максимальные X и Y) </summary>
		/// <param name="point"> Точка, для которой ведется расчет </param>
		/// <param name="scale"> Масштаб матрицы </param>
		/// <returns></returns>
		private static Point GetMatrixNodePoint(Point point, int scale)
		{
			var nodePoint = new Point();
			nodePoint.X = (int)(Math.Ceiling(point.X / Math.Pow(10, scale)) * Math.Pow(10, scale));
			nodePoint.Y = (int)(Math.Ceiling(point.Y / Math.Pow(10, scale)) * Math.Pow(10, scale));
			return nodePoint;
		}
		/// <summary> Рассчитывает Узел индексной матрицы </summary>
		/// <param name="point"> Точка, для которой ведется расчет </param>
		/// <param name="scale"> Масштаб матрицы </param>
		/// <param name="isFull"> true, если узел полностью лежит внутри зоны </param>
		/// <returns></returns>
		private static MatrixNode GetMatrixNode(Point point, int scale, bool? isFull)
		{
			long node = (long)(scale * Math.Pow(10, 15)
				+ Math.Ceiling((point.X) / Math.Pow(10, scale)) * Math.Pow(10, 8 - scale)
				+ Math.Ceiling((point.Y) / Math.Pow(10, scale)));
			return new MatrixNode { Node = node, Scale = scale, IsFull = isFull };
		}
	}
}