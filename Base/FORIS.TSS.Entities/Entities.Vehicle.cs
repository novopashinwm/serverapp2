﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Server;

namespace FORIS.TSS.EntityModel
{
	public partial class Entities
	{
		/// <summary>
		/// Установить доступ на ТС
		/// </summary>
		/// <param name="operatorId">Идентификатор оператора, которому предоставляется право</param>
		/// <param name="vehicle">ТС</param>
		/// <param name="rights">Права на объект</param>
		/// <param name="allowed">Флаг разрешения</param>
		public void SetAccessToVehicle(int operatorId, VEHICLE vehicle, SystemRight[] rights, bool allowed)
		{
			var @operator = GetOperator(operatorId);
			if (@operator == null)
				throw new ArgumentOutOfRangeException("operatorId", operatorId, @"Operator not found by id");

			if (allowed)
				Allow(@operator, vehicle, rights);
			else
				Disallow(@operator, vehicle, rights);
		}

		public OWNER GetVehicleOwner(VEHICLE vehicle)
		{
			var owner = VEHICLE_OWNER
					.Where(o => o.VEHICLE.VEHICLE_ID == vehicle.VEHICLE_ID
							 && o.OWNER_NUMBER == 1)
					.Select(o => o.OWNER)
					.FirstOrDefault();
			if (owner == null)
			{
				owner = new OWNER
				{
					FIRST_NAME = string.Empty,
					LAST_NAME = string.Empty,
					SECOND_NAME = string.Empty,
					PHONE_NUMBER1 = string.Empty,
					PHONE_NUMBER2 = string.Empty,
					PHONE_NUMBER3 = string.Empty
				};
				OWNER.AddObject(owner);

				VEHICLE_OWNER.AddObject(new VEHICLE_OWNER
				{
					OWNER = owner,
					OWNER_NUMBER = 1,
					VEHICLE = vehicle
				});
			}

			return owner;
		}

		public string GetVehicleProfileField(int vehicleId, string fieldName)
		{
			return VEHICLE_PROFILE
				.Where(p => p.PROPERTY_NAME == fieldName &&
							p.VEHICLE_ID == vehicleId)
				.Select(p => p.Value)
				.FirstOrDefault();
		}

		public void SetVehicleProfileField(int vehicleId, string fieldName, string fieldValue)
		{
			var vehicleProfileValue =
				VEHICLE_PROFILE.FirstOrDefault(
					p => p.VEHICLE_ID == vehicleId && p.PROPERTY_NAME == fieldName);
			if (vehicleProfileValue != null)
				vehicleProfileValue.Value = fieldValue;
			else
				VEHICLE_PROFILE.AddObject(new VEHICLE_PROFILE
				{
					VEHICLE_ID    = vehicleId,
					PROPERTY_NAME = fieldName,
					Value         = fieldValue
				});
		}

		public Contact GetVehiclePhone(int vehicleId)
		{
			var result = CONTROLLER
				.Where(c => c.VEHICLE.VEHICLE_ID == vehicleId)
				.Select(c => c.PhoneContact)
				.FirstOrDefault();

			if (result == null)
				result = CONTROLLER
					.Where(c => c.VEHICLE.VEHICLE_ID == vehicleId)
					.Select(c => c.MLP_Controller.Asid.Contact.DemaskedPhone)
					.FirstOrDefault();

			return result;
		}

		public Contact GetAsidPhone(Asid asid)
		{
			Contact contact = null;
			if (asid.Contact != null && asid.Contact.DemaskedPhone != null)
				contact = asid.Contact.DemaskedPhone;

			if (contact == null && asid.OPERATOR != null)
				contact = GetOwnPhoneContact(asid.OPERATOR.OPERATOR_ID);

			if (contact == null && asid.MLP_Controller != null && asid.MLP_Controller.CONTROLLER != null)
				contact = asid.MLP_Controller.CONTROLLER.PhoneContact;

			return contact;
		}

		/// <summary>
		/// Идентификатор записи в таблице SCHEDULEREVENT, которой соответствует запрос определения координат по расписанию
		/// </summary>
		public const int SchedulerEventPositionAsker = 60;

		public IEnumerable<SCHEDULERQUEUE> GetVehicleMlpSchedules(int operatorId, int id)
		{
			if (!IsAllowedVehicleAll(operatorId, id, SystemRight.VehicleAccess))
				throw new SecurityException("Forbidden");
			return (from sq in SCHEDULERQUEUE.Include(EntityModel.SCHEDULERQUEUE.VEHICLEIncludePath)
					where sq.Owner.OPERATOR_ID == operatorId &&
						  sq.VEHICLE.VEHICLE_ID == id &&
						  sq.SCHEDULEREVENT.SCHEDULEREVENT_ID == SchedulerEventPositionAsker
					select sq).ToList();
		}

		public IEnumerable<SCHEDULERQUEUE> GetVehicleGroupMlpSchedules(int operatorId, int id)
		{
			if (!IsAllowedVehicleGroupAll(operatorId, id, SystemRight.VehicleAccess))
				throw new SecurityException("Forbidden");
			return (from sq in SCHEDULERQUEUE.Include("VEHICLEGROUP")
					where sq.Owner.OPERATOR_ID == operatorId &&
						  sq.VEHICLEGROUP.VEHICLEGROUP_ID == id &&
						  sq.SCHEDULEREVENT.SCHEDULEREVENT_ID == SchedulerEventPositionAsker
					select sq).ToList();
		}

		public IEnumerable<SCHEDULERQUEUE> GetMlpSchedules(int operatorId, IdType idType, int id)
		{
			switch (idType)
			{
				case IdType.Vehicle:
					return GetVehicleMlpSchedules(operatorId, id);
				case IdType.VehicleGroup:
					return GetVehicleGroupMlpSchedules(operatorId, id);
			}

			throw new ArgumentOutOfRangeException("idType", idType, @"Unexpected value");
		}
	}
}