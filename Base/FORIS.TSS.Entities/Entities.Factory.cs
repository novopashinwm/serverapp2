﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Common.Sms;
using FORIS.TSS.EntityModel.History;
using FORIS.TSS.Helpers;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace FORIS.TSS.EntityModel
{
	public partial class Entities
	{
		private List<MESSAGE> SendSmsGroups(Contact contact, MESSAGE_TEMPLATE template, List<List<string>> breakedToGroups)
		{
			var gatewayId = GetMessageGatewayId((ContactType)contact.Type, contact.Value, template != null ? template.NAME : null);
			if (contact.Type == (int)ContactType.Phone && gatewayId == null)
				return new List<MESSAGE>();

			var result = new List<MESSAGE>(breakedToGroups.Count);
			var utcNow = DateTime.UtcNow;
			foreach (var smsPartGroup in breakedToGroups)
			{
				var messageStringBuilder = new StringBuilder();
				for (var i = 0; i != smsPartGroup.Count; ++i)
				{
					messageStringBuilder.Append(smsPartGroup[i]);
				}

				var message = new MESSAGE { BODY = messageStringBuilder.ToString() };
				MESSAGE.AddObject(message);

				if (gatewayId != null)
					message.DestinationType_ID = gatewayId.Value;
				message.TIME = utcNow;
				message.AddDestination(contact);
				message.ProcessingResult = (int) MessageProcessingResult.Received;
				result.Add(message);
			}
			return result;
		}
		private enum SmsSourceType
		{
			Service,
			User
		}
		public List<MESSAGE> SendSmsToUser(Contact targetContact, Contact sourceContact, MESSAGE_TEMPLATE template, params MessagePart[] messageParts)
		{
			if (targetContact.Type != (int)ContactType.Phone || sourceContact == null)
				return SendSmsToUser(targetContact, SmsSourceType.Service, template, messageParts);

			var result = SendSmsToUser(targetContact, SmsSourceType.User, template, messageParts);
			foreach (var message in result)
				message.AddSource(sourceContact);

			return result;
		}
		public List<MESSAGE> SendSmsToUser(Contact targetContact, MESSAGE_TEMPLATE template, params MessagePart[] messageParts)
		{
			return SendSmsToUser(targetContact, SmsSourceType.Service, template, messageParts);
		}
		private List<MESSAGE> SendSmsToUser(Contact targetContact, SmsSourceType sourceType, MESSAGE_TEMPLATE template, params MessagePart[] messageParts)
		{
			if (messageParts.Length == 0 || Array.TrueForAll(messageParts, messagePart => string.IsNullOrEmpty(messagePart.Value)))
				throw new ArgumentException(@"Message parts must be nonempty", nameof(messageParts));

			switch ((ContactType)targetContact.Type)
			{
				case ContactType.Phone:
					if ((ContactType)targetContact.Type == ContactType.Phone)
					{
						var serviceName = GetConstant(Constant.ServiceName);

						if (!string.IsNullOrWhiteSpace(serviceName))
						{
							var messagePartsWithServiceName = new MessagePart[messageParts.Length + 1];
							switch (sourceType)
							{
								case SmsSourceType.Service:
									messagePartsWithServiceName[0] = "Услуга " + serviceName + ": ";
									Array.Copy(messageParts, 0, messagePartsWithServiceName, 1, messageParts.Length);
									break;
								case SmsSourceType.User:
									Array.Copy(messageParts, 0, messagePartsWithServiceName, 0, messageParts.Length);
									messagePartsWithServiceName[messageParts.Length] = "\nУслуга " + serviceName + ".";
									break;
								default:
									throw new ArgumentOutOfRangeException(nameof(sourceType), sourceType,
										@"Value is not supported");
							}
							messageParts = messagePartsWithServiceName;
						}
					}

					var breakedToGroups = Helper.BreakToGroups(
						Helper.AlignMessageParts(messageParts));

					return SendSmsGroups(targetContact, template, breakedToGroups);
				default:
					throw new NotSupportedException("Contact type " + ((ContactType)targetContact.Type) + " is not supported");
			}
		}
		public MESSAGE CreateOutgoingMessage(Contact contact, string subject, string body)
		{
			if (contact == null)
				throw new ArgumentNullException(nameof(contact));
			if (contact.Type != (int) ContactType.Email && contact.Type != (int) ContactType.Phone)
				throw new ArgumentOutOfRangeException(nameof(contact), contact,
					@"Only ContactType." + ContactType.Email +
					@" and ContactType." + ContactType.Phone + @" are supported");

			if (contact.Type == (int)ContactType.Email && subject == null)
				throw new ArgumentNullException(nameof(subject));

			var message = new MESSAGE
			{
				SUBJECT = subject,
				BODY    = body,
				TIME    = DateTime.UtcNow
			};

			message.AddDestination(contact);
			message.ProcessingResult = (int)MessageProcessingResult.Received;
			MESSAGE.AddObject(message);

			return message;
		}
		/// <summary> Создаёт исходящее сообщение электронной почты для отправки на указанный адрес электронной почты </summary>
		/// <param name="email"> Хранимый в БД (получивший EMAIL_ID) объект адреса электронной почты </param>
		/// <param name="subject"> Тема сообщения </param>
		/// <param name="body"> Тело сообщения </param>
		public MESSAGE CreateOutgoingEmail(string email, string subject, string body)
		{
			if (string.IsNullOrWhiteSpace(email))
				throw new ArgumentNullException(nameof(email));

			email = email.Trim();
			var contact = GetContact(ContactType.Email, email);
			return CreateOutgoingMessage(contact, subject, body);
		}
		/// <summary>
		/// Создаёт исходящее сообщение электронной почты для отправки на указанный <see cref="EMAIL"/>
		/// Важно: адрес электронной почты должен к этому моменту получить EMAIL_ID
		/// </summary>
		/// <param name="template">Шаблон сообщения</param>
		/// <param name="emails">Хранимые в БД (получивший EMAIL_ID) объект адреса электронной почты</param>
		/// <param name="subject">Тема сообщения</param>
		/// <param name="body">Тело сообщения</param>
		/// <param name="cultureInfo">Культура, для которой требуется найти подходящий шаблон</param>
		/// <param name="getFieldValue">Функция для получения значения поля по ключу</param>
		/// <param name="model">Модель, на основе которой заполняется шаблон</param>
		public List<MESSAGE> CreateOutgoingMessages(
			MessageTemplate template,
			IEnumerable<Contact> emails,
			string subject,
			string body,
			CultureInfo cultureInfo,
			Func<Entities, string, object, CultureInfo, string> getFieldValue = null,
			object model = null)
		{
			var templateName = template.ToString();

			var templateQuery = MESSAGE_TEMPLATE
				.Include("MESSAGE_TEMPLATE_FIELD")
				.Include("MESSAGE_TEMPLATE_FIELD.DOTNET_TYPE")
				.Where(mt => mt.NAME == templateName);

			MESSAGE_TEMPLATE messageTemplate = null;
			if (cultureInfo != null)
				messageTemplate = templateQuery.FirstOrDefault(mt => mt.Culture.Code == cultureInfo.Name);
			if (messageTemplate == null)
				messageTemplate = templateQuery.FirstOrDefault(mt => mt.Culture == null);

			if (messageTemplate == null)
				throw new ArgumentOutOfRangeException(nameof(template), template, @"Unable to find template " + template + @" and culture " + cultureInfo);

			// TODO: переписать для использования шаблонов, например Razor
			var generateSubjectFromTemplate = subject == null && messageTemplate.HEADER != null;
			var generateBodyFromTemplate = body == null && messageTemplate.BODY != null;

			if (getFieldValue != null && (generateSubjectFromTemplate || generateBodyFromTemplate))
			{
				if (generateSubjectFromTemplate)
					subject = messageTemplate.HEADER;

				if (generateBodyFromTemplate)
					body = messageTemplate.BODY;

				foreach (var field in messageTemplate.MESSAGE_TEMPLATE_FIELD)
				{
					var fieldValue = getFieldValue(this, field.NAME, model, cultureInfo);

					var fieldRef = "$" + field.NAME + "$";
					if (generateSubjectFromTemplate)
						subject = subject.Replace(fieldRef, fieldValue);
					if (generateBodyFromTemplate)
						body = body.Replace(fieldRef, fieldValue);
				}
			}

			var messages = new List<MESSAGE>();
			foreach (var email in emails)
			{
				var message = CreateOutgoingMessage(email, subject, body);
				message.MESSAGE_TEMPLATE = messageTemplate;
				messages.Add(message);
			}

			return messages;
		}
		public List<MESSAGE> CreateOutgoingSms(Contact contact, string body)
		{
			if (contact == null)
				throw new ArgumentNullException(nameof(contact));

			var destinationTypeID = GetMessageGatewayId((ContactType)contact.Type, contact.Value, null);
			if (destinationTypeID == null)
				return new List<MESSAGE>();

			var message = new MESSAGE
			{
				BODY = body
			};

			message.AddDestination(contact);
			message.DestinationType_ID = destinationTypeID.Value;
			MESSAGE.AddObject(message);

			return new List<MESSAGE> { message };
		}
		public MESSAGE CreateOutgoingSmsToMsisdn(string phone, string body)
		{
			var normalizedPhone = ContactHelper.GetNormalizedPhone(phone);
			var contact = GetContact(ContactType.Phone, normalizedPhone);
			return CreateOutgoingSms(contact, body).FirstOrDefault();
		}
		public MESSAGE CreateAppNotification(Contact appClientContact, string shortText, string fullText)
		{
			var message = new MESSAGE
			{
				SUBJECT = shortText,
				BODY    = fullText
			};

			MESSAGE.AddObject(message);
			message.AddDestination(appClientContact);

			return message;
		}
		public List<MESSAGE> CreateAppNotification(int operatorId, string shortText, string fullText, string eventDate = null)
		{
			var result = new List<MESSAGE>();

			var contacts = GetCloudContactsForOperator(operatorId);

			var blank = new MESSAGE.Blank(shortText, fullText, eventDate);

			foreach (var contact in contacts)
			{
				var message = CreateAppNotification(contact, null, null);
				message.Fill(blank);
				message.Owner_Operator_ID = operatorId;
				result.Add(message);
			}

			return result;
		}
		public MESSAGE CreateWebNotification(int operatorId, string shortText, string fullText)
		{
			var message = new MESSAGE
			{
				DestinationType_ID = (int)MessageDestinationType.Web,
				STATUS             = (int)MessageState.Done, // Статус обработки не требуется, поэтому сразу в Done
			};

			MESSAGE.AddObject(message);
			message.Owner_Operator_ID = operatorId;
			message.SUBJECT = shortText;
			message.BODY    = fullText;
			return message;
		}
		public void AddMessageField(MESSAGE message, string key, int value)
		{
			AddMessageField(message, key, XmlConvert.ToString(value), typeof (int));
		}
		public void AddMessageField(MESSAGE message, string key, DateTime value)
		{
			AddMessageField(message, key, XmlConvert.ToString(value, XmlDateTimeSerializationMode.Utc), typeof(DateTime));
		}
		public void AddMessageField(MESSAGE message, string key, double value)
		{
			AddMessageField(message, key, XmlConvert.ToString(value), typeof(double));
		}
		public void AddMessageField(MESSAGE message, string key, bool value)
		{
			AddMessageField(message, key, XmlConvert.ToString(value), typeof(bool));
		}
		public void AddMessageField(MESSAGE message, string key, string value)
		{
			AddMessageField(message, key, value, typeof (string));
		}
		private void AddMessageField(MESSAGE message, string key, string value, Type type)
		{
			if (message == null)
				throw new ArgumentNullException(nameof(message));

			var templateField = GetOrCreateMessageTemplateField(message.MESSAGE_TEMPLATE, key, type);

			if (templateField == null)
				throw new ArgumentOutOfRangeException(nameof(key), key, @"Template field not found");

			var field = new MESSAGE_FIELD
				{
					MESSAGE                = message,
					MESSAGE_TEMPLATE_FIELD = templateField
				};
			field.SetContent(value);
			MESSAGE_FIELD.AddObject(field);
		}
		private MESSAGE_TEMPLATE_FIELD GetOrCreateMessageTemplateField(MESSAGE_TEMPLATE template, string key, Type type)
		{
			Func<Entities, MESSAGE_TEMPLATE_FIELD> getField;
			if (template == null)
				getField = entities =>entities.MESSAGE_TEMPLATE_FIELD
					.FirstOrDefault(mtf => mtf.NAME == key && mtf.MESSAGE_TEMPLATE == null);
			else
				getField = entities => entities.MESSAGE_TEMPLATE_FIELD
					.FirstOrDefault(mtf => mtf.NAME == key && mtf.MESSAGE_TEMPLATE.MESSAGE_TEMPLATE_ID == template.MESSAGE_TEMPLATE_ID);

			var existingField = getField(this);
			if (existingField == null)
			{
				using (var entities = new Entities())
				using (var transaction = new Transaction())
				{
					var field = getField(entities);
					if (field == null)
					{
						entities.MESSAGE_TEMPLATE_FIELD.AddObject(
							new MESSAGE_TEMPLATE_FIELD
								{
									MESSAGE_TEMPLATE =
										template != null
											? entities.MESSAGE_TEMPLATE.First(
												t => t.MESSAGE_TEMPLATE_ID == template.MESSAGE_TEMPLATE_ID)
											: null,
									NAME = key,
									DOTNET_TYPE = entities.DOTNET_TYPE.First(t => t.TYPE_NAME == type.FullName)
								});
						entities.SaveChanges();
					}
					transaction.Complete();
				}
				existingField = getField(this);
			}

			return existingField;
		}
		public void ChangeControllerType(CONTROLLER controller, CONTROLLER_TYPE newControllerType)
		{
			if (controller.CONTROLLER_TYPE == newControllerType)
				return;

			DeleteDefaultRules(controller);
			controller.CONTROLLER_TYPE = newControllerType;

			if ((controller.EntityState & EntityState.Added) == 0)
			{
				//Забыть device_id, если был задан на старом, а на новом не поддерживается
				if (!controller.CONTROLLER_INFOReference.IsLoaded)
					controller.CONTROLLER_INFOReference.Load();

				if (controller.CONTROLLER_INFO != null &&
					controller.CONTROLLER_INFO.DEVICE_ID != null &&
					!(ControllerTypeHelper.SupportsDeviceId(newControllerType.TYPE_NAME) ?? false))
				{
					controller.CONTROLLER_INFO.DEVICE_ID = null;
				}

				if (!newControllerType.CONTROLLER_SENSOR.IsLoaded)
					newControllerType.CONTROLLER_SENSOR.Load();

				var newControllerTypeSensors = newControllerType.CONTROLLER_SENSOR.ToLookup(s => s.Descript);

				if (!controller.CONTROLLER_SENSOR_MAP.IsLoaded)
					controller.CONTROLLER_SENSOR_MAP.Load();

				foreach (var csm in controller.CONTROLLER_SENSOR_MAP.ToArray())
				{
					if (!csm.CONTROLLER_SENSORReference.IsLoaded)
						csm.CONTROLLER_SENSORReference.Load();

					if (newControllerTypeSensors[csm.CONTROLLER_SENSOR.Descript].Count() == 1)
					{
						csm.CONTROLLER_SENSOR = newControllerTypeSensors[csm.CONTROLLER_SENSOR.Descript].Single();
					}
					else
					{
						CONTROLLER_SENSOR_MAP.DeleteObject(csm);
					}
				}
			}

			FillDefaultSensors(controller);
			AddDefaultRules(controller);
		}
		public void FillDefaultSensors(CONTROLLER controller)
		{
			var controllerType = controller.CONTROLLER_TYPE;
			if (controllerType == null)
				throw new InvalidOperationException("CONTROLLER.CONTROLLER_TYPE is null");

			foreach (var controllerSensor in controllerType.CONTROLLER_SENSOR)
			{
				if (!controllerSensor.Mandatory ||
					controllerSensor.CONTROLLER_SENSOR_LEGEND == null ||
					controllerSensor.Default_Multiplier == null ||
					controller.CONTROLLER_SENSOR_MAP.Any(csm => csm.CONTROLLER_SENSOR_LEGEND == controllerSensor.CONTROLLER_SENSOR_LEGEND))
					continue;

				CONTROLLER_SENSOR_MAP.AddObject(
					new CONTROLLER_SENSOR_MAP
						{
							CONTROLLER = controller,
							CONTROLLER_SENSOR = controllerSensor,
							CONTROLLER_SENSOR_LEGEND = controllerSensor.CONTROLLER_SENSOR_LEGEND,
							Multiplier = controllerSensor.Default_Multiplier,
							Constant = controllerSensor.Default_Constant ?? 0
						});
			}
		}
		public void AddDefaultRules(CONTROLLER controller)
		{
			var controllerType = controller.CONTROLLER_TYPE;
			if(controllerType == null)
				throw new InvalidOperationException("CONTROLLER.CONTROLLER_TYPE is null");

			if(controller.VEHICLE == null)
				throw new InvalidOperationException("VEHICLE is null");

			var @operator = GetOwner(controller.VEHICLE);
			if (@operator == null)
				return;

			foreach (var controllerSensor in controllerType.CONTROLLER_SENSOR.Where(s => s.CONTROLLER_SENSOR_LEGEND != null))
			{
				var legend = controllerSensor.CONTROLLER_SENSOR_LEGEND;
				if(legend != null)
					AddDefaultRules(controller, legend);
			}
		}
		public void DeleteDefaultRules(CONTROLLER controller)
		{
			var controllerType = controller.CONTROLLER_TYPE;
			if (controllerType == null)
				return;

			if (controller.VEHICLE == null)
				return;

			var @operator = GetOwner(controller.VEHICLE);
			if (@operator == null)
				return;

			foreach (var controllerSensor in controllerType.CONTROLLER_SENSOR.Where(s => s.CONTROLLER_SENSOR_LEGEND != null))
			{
				var legend = controllerSensor.CONTROLLER_SENSOR_LEGEND;
				if (legend != null)
					DeleteDefaultRules(controller, legend);
			}
		}
		public void AddDefaultRules(CONTROLLER controller, CONTROLLER_SENSOR_LEGEND legend)
		{
			if (!GetConstantAsBool(Constant.AddDefaultRules))
				return;

			if (legend.Number != (int)SensorLegend.Alarm)
				return;

			if (controller.VEHICLE == null)
				return;

			var vehicleId = controller.VEHICLE.VEHICLE_ID;
			var operatorIds = GetVehicleOwnersQuery(vehicleId)
				.Select(o => o.OPERATOR_ID)
				.ToList();

			foreach (var operatorId in operatorIds)
			{
				if (AlarmRuleExists(operatorId, vehicleId)) return;

				var alarmRule = CreateAlarmRule(operatorId, vehicleId);
				AddOrUpdateRule(operatorId, alarmRule);
			}
		}
		public void DeleteDefaultRules(CONTROLLER controller, CONTROLLER_SENSOR_LEGEND legend)
		{
			if (legend.Number != (int)SensorLegend.Alarm)
				return;

			if (controller.VEHICLE == null)
				return;

			var vehicleId = controller.VEHICLE.VEHICLE_ID;
			foreach (var alarmRule in DefaultAlarmRules(vehicleId))
				DeleteCompoundRule(alarmRule);
		}
		public Billing_Service CreateBillingService(string serviceType)
		{
			var billingService =
				new Billing_Service
				{
					Type = Billing_Service_Type.FirstOrDefault(bst => bst.Service_Type == serviceType)
						?? CreateBillingServiceType(serviceType)
				};
			Billing_Service.AddObject(billingService);
			return billingService;
		}
		private Billing_Service_Type CreateBillingServiceType(string serviceType)
		{
			using (var entities = new Entities())
			{
				bool created = false;
				try
				{
					entities.Billing_Service_Type.AddObject(
					new Billing_Service_Type
					{
						Service_Type_Category = ServiceTypeCategory.Generic.ToString(),
						Service_Type          = serviceType
					});
					entities.SaveChanges();
					created = true;
				}
				catch (EntityCommandExecutionException)
				{
					//TODO: check unique index violation only
				}

				if (created)
				{
					entities.SendMessageToSupport(
						"Unknown billing service type",
						"Billing service type was registered: " + serviceType);
					entities.SaveChanges();
				}
			}

			return Billing_Service_Type.First(x => x.Service_Type == serviceType);
		}
		public void EnsureControllerExists(VEHICLE vehicle)
		{
			if (vehicle.CONTROLLER == null)
			{
				vehicle.CONTROLLER = new CONTROLLER
				{
					CONTROLLER_TYPE = GetControllerType(ControllerType.Names.Unspecified),
				};
				CONTROLLER.AddObject(vehicle.CONTROLLER);
			}
		}
		public void EnsureControllerInfoExists(VEHICLE vehicle)
		{
			EnsureControllerExists(vehicle);

			if (vehicle.CONTROLLER.CONTROLLER_INFO == null)
			{
				vehicle.CONTROLLER.CONTROLLER_INFO = new CONTROLLER_INFO
				{
					TIME = DateTime.UtcNow
				};
				CONTROLLER_INFO.AddObject(vehicle.CONTROLLER.CONTROLLER_INFO);
			}
		}
		/// <summary> Создать группу друзей оператора </summary>
		/// <param name="ormOperator"></param>
		/// <returns></returns>
		public OPERATORGROUP CreateFriendGroup(OPERATOR ormOperator)
		{
			// Проверка существования
			var groupOfFriends = FriendsGroupQuery(ormOperator.OPERATOR_ID)
				.FirstOrDefault();
			if (groupOfFriends == null)
			{
				// Создаем
				groupOfFriends = new OPERATORGROUP();
				// Добавляем в таблицу
				OPERATORGROUP.AddObject(groupOfFriends);
			}
			var groupOfFriendsRight = groupOfFriends.Accesses
				.FirstOrDefault(a =>
					a.Operator_ID == ormOperator.OPERATOR_ID &&
					a.Right_ID == (int)SystemRight.Friendship);
			// Добавляем права группы или восстанавливаем
			if (groupOfFriendsRight == null)
			{
				Operator_OperatorGroup.AddObject(new Operator_OperatorGroup
				{
					OPERATOR      = ormOperator,
					OPERATORGROUP = groupOfFriends,
					Right_ID      = (int)SystemRight.Friendship,
					Allowed       = true
				});
			}
			else
			{
				if (!groupOfFriendsRight.Allowed)
					groupOfFriendsRight.Allowed = true;
			}
			return groupOfFriends;
		}
		public Billing_Service CreateBillingService(Asid asidRecord, Billing_Service_Type billingServiceType, int? quantity, bool activated = true)
		{
			var newBillingService = new Billing_Service
			{
				Asid               = asidRecord,
				Type               = billingServiceType,
				StartDate          = DateTime.UtcNow,
				MaxQuantity        = quantity ?? billingServiceType.MaxQuantity,
				MinIntervalSeconds = billingServiceType.MinIntervalSeconds,
				Activated          = activated
			};

			if (billingServiceType.PeriodDays != null)
			{
				if (activated)
					newBillingService.EndDate = newBillingService.StartDate.AddDays(billingServiceType.PeriodDays.Value);
			}
			else
			{
				Billing_Service trialService;

				using (new Stopwatcher("Entities.CreateBillingService.Find trial service to delete"))
					trialService = Billing_Service.FirstOrDefault(
					x => x.Asid.ID == asidRecord.ID &&
						 x.Type.Service_Type_Category == billingServiceType.Service_Type_Category &&
						 x.Type.PeriodDays != null);
				if (trialService != null)
				{
					DeleteBillingService(trialService);
				}
			}

			Billing_Service.AddObject(newBillingService);

			if (newBillingService.MaxQuantity != null && billingServiceType.ResetCounter == "monthly")
			{
				DateTime? previouslyDeletedServiceStartDate = null;

				using (var history = new HistoricalEntities())
				{
					// Эта проверка нужна, чтобы абонент не переподключал услугу каждый раз, когда у него заканчивается пакет
					// Без этой проверка пользователь, может за один день многократно переподключать услугу,
					// т.к. после переподключения пакет восстанавливается
					var monthAgo = DateTime.UtcNow.AddMonths(-1);
					var previouslyDeletedBillingService = history.H_Billing_Service
						.Where(
								bs => bs.Billing_Service_Type_ID == newBillingService.Type.ID &&
										bs.Asid_ID == asidRecord.ID &&
										monthAgo < bs.ACTUAL_TIME)
						.OrderByDescending(bs => bs.ACTUAL_TIME)
						.FirstOrDefault();

					if (previouslyDeletedBillingService != null)
					{
						previouslyDeletedServiceStartDate = previouslyDeletedBillingService.StartDate;
						foreach (var typeId in Rendered_Service_Type.Select(rst => rst.ID))
						{
							var deletedRenderedService =
								history.H_Rendered_Service.
										Where(
											rs =>
											rs.Billing_Service_ID == previouslyDeletedBillingService.ID &&
											rs.Rendered_Service_Type_ID == typeId).
										OrderByDescending(rs => rs.ACTUAL_TIME).
										FirstOrDefault();

							if (deletedRenderedService != null)
							{
								var renderedService = new Rendered_Service
								{
									BillingService = newBillingService,
									Rendered_Service_Type_ID = typeId,
									Quantity = deletedRenderedService.Quantity,
									LastTime = deletedRenderedService.LastTime
								};
								Rendered_Service.AddObject(renderedService);
							}
						}
					}
				}

				var startDate = previouslyDeletedServiceStartDate ?? newBillingService.StartDate;

				var repetition = CreateMonthlyRepetition(startDate);

				newBillingService.ScheduledTask =
					new SCHEDULERQUEUE
					{
						Enabled = true,
						SCHEDULEREVENT = SCHEDULEREVENT.FirstOrDefault(
							se => se.SCHEDULEREVENT_ID == EntityModel.SCHEDULEREVENT.BillingServiceCounterReset),
						ErrorCount = 0,
						REPETITION_XML = SerializationHelper.ToXML(repetition),
						NEAREST_TIME = repetition.GetNextTimeInUTC(startDate) ?? DateTime.MaxValue
					};
				SCHEDULERQUEUE.AddObject(newBillingService.ScheduledTask);
			}

			if (billingServiceType.CONTROLLER_TYPE != null &&
				billingServiceType.VEHICLE_KIND != null)
			{
				//Проверим, есть ли Vehicle с точно таким же Asid
				var vehicle = GetVehicle(asidRecord).FirstOrDefault();

				if (!asidRecord.ContactReference.IsLoaded)
					asidRecord.ContactReference.Load();

				if (vehicle == null && asidRecord.Contact != null && asidRecord.Contact.Demasked_ID != null)
				{
					//Если Asid уже демаскирован и находится на том же контракте, и пользователь завёл трекер, указав номер телефона, то нужно подключить услугу к этому трекеру
					vehicle = GetVehicleQuery()
						.FirstOrDefault(v =>
							v.CONTROLLER.PhoneContactID == asidRecord.Contact.Demasked_ID &&
							v.Department_ID             == asidRecord.Department_ID);

					if (vehicle != null)
					{
						if (vehicle.CONTROLLER.MLP_Controller != null)
							vehicle.CONTROLLER.MLP_Controller.Asid = asidRecord;
						else
							MLP_Controller.AddObject(
								new MLP_Controller
								{
									Asid = asidRecord,
									CONTROLLER = vehicle.CONTROLLER
								});
					}
				}

				if (vehicle == null)
				{
					var department = asidRecord.DEPARTMENT;

					vehicle = new VEHICLE
					{
						//Имя не задаётся, т.к. оно будет отображаться в UI из демаскированного телефона
						GARAGE_NUMBER  = string.Empty,
						CONTROLLER     = new CONTROLLER
						{
							MLP_Controller = new MLP_Controller { Asid = asidRecord }
						},
						VEHICLE_STATUS = VEHICLE_STATUS.OrderBy(vs => vs.VEHICLE_STATUS_ID).First(),
						PUBLIC_NUMBER  = string.Empty
					};
					if (asidRecord.Contact != null)
					{
						vehicle.CONTROLLER.PHONE = asidRecord.Contact.DemaskedPhone.Value;
						vehicle.CONTROLLER.PhoneContact = asidRecord.Contact.DemaskedPhone;
					}
					else if (asidRecord.OPERATOR != null)
					{
						var contact = Operator_Contact
							.Where(oc => oc.Operator_ID == asidRecord.OPERATOR.OPERATOR_ID &&
										 oc.Confirmed &&
										 oc.Contact.Type == (int) ContactType.Phone)
							.Select(oc => oc.Contact)
							.FirstOrDefault();
						if (contact != null)
						{
							vehicle.CONTROLLER.PHONE = contact.Value;
							vehicle.CONTROLLER.PhoneContact = contact.DemaskedPhone;
						}
					}
					vehicle.SetDepartment(this, department);
					VEHICLE.AddObject(vehicle);
					CONTROLLER.AddObject(vehicle.CONTROLLER);
					MLP_Controller.AddObject(vehicle.CONTROLLER.MLP_Controller);
				}

				//Менять тип на LBS можно только при условии, что других услуг с типом такого контроллера не осталось
				//При удалении также должен быть обеспечен этот инвариант.

				if (ControllerTypeChangeIsRequired(asidRecord, billingServiceType, vehicle))
				{
					vehicle.VEHICLE_KIND = billingServiceType.VEHICLE_KIND;
					ChangeControllerType(vehicle.CONTROLLER, billingServiceType.CONTROLLER_TYPE);
				}
			}

			SaveChangesWithHistory();

			return newBillingService;
		}
		private bool ControllerTypeChangeIsRequired(Asid asidRecord, Billing_Service_Type billingServiceType, VEHICLE vehicle)
		{
			if (vehicle.CONTROLLER.CONTROLLER_TYPE == null)
				return true;
			return false;
		}
		internal IRepetitionClass CreateMonthlyRepetition(DateTime dateTime)
		{
			return new RepeatOnNthDayOfMonth(dateTime);
		}
		public string GetOrCreateSimIdByOperatorId(int operatorId, int? departmentId, SaveContext saveContext = null)
		{
			if (saveContext == null)
				saveContext = SaveContext.Operator(operatorId);

			var asid = Asid
				.Where(a => a.OPERATOR.OPERATOR_ID == operatorId)
				.Select(a => new { a.ID })
				.FirstOrDefault();

			if (asid == null)
			{
				asid = Operator_Contact
					.Where(oc =>
						oc.Confirmed                  &&
						oc.Operator_ID  == operatorId &&
						oc.Contact.Type == (int)ContactType.Phone)
					.SelectMany(oc => oc.Contact.MaskedAsids)
					.SelectMany(ac => ac.Asid)
					.Where(a => a != null)
					.Select(a => new { a.ID })
					.FirstOrDefault();
			}

			if (asid == null)
			{
				using (var transaction = new Transaction())
				{
					asid = Asid
						.Where(a => a.OPERATOR.OPERATOR_ID == operatorId)
						.Select(a => new { a.ID })
						.FirstOrDefault();

					if (asid == null)
					{
						var createdAsid = new Asid
						{
							OPERATOR = GetOperator(operatorId)
						};
						Asid.AddObject(createdAsid);
						AddWelcomingService(createdAsid);
						SaveChanges(saveContext);
						asid = new { createdAsid.ID };
					}
					transaction.Complete();
				}
			}

			if (!VEHICLE.Any(v => v.CONTROLLER.MLP_Controller.Asid.ID == asid.ID))
			{
				var ormOperator = GetOperator(operatorId);
				var vehicleName = GetVehicleName(ormOperator);

				var ormVehicle = CreateVehicle(ormOperator, vehicleName, Asid.First(a => a.ID == asid.ID), UserRole.DefaultOwnRightsOnVehicle, saveContext);
				if (departmentId != null)
				{
					ormVehicle.SetDepartment(this, DEPARTMENT.First(d => d.DEPARTMENT_ID == departmentId.Value));
					SaveChanges(saveContext);
				}
			}

			var simId = GetSimIdByAsidId(asid.ID);

			return simId;

		}
		private string GetVehicleName(OPERATOR ormOoperator)
		{
			var sb = new StringBuilder();
			if (!string.IsNullOrWhiteSpace(ormOoperator.NAME))
				sb.Append(ormOoperator.NAME);

			if (!string.IsNullOrWhiteSpace(ormOoperator.LOGIN) &&
				!string.Equals(ormOoperator.LOGIN, ormOoperator.NAME, StringComparison.OrdinalIgnoreCase))
			{
				if (sb.Length != 0)
					sb.Append(" ");
				sb.Append("[");
				sb.Append(ormOoperator.LOGIN);
				sb.Append("]");
			}
			return sb.ToString();
		}
		public void CreateAppNotificationAboutOperatorDataChange(int operatorId, UpdatedDataType updatedDataType)
		{
			var appContacts =
				AppClient
					.Where(
						ac => ac.Operator_ID == operatorId &&
							  ac.Contact_ID != null)
					.Select(ac => ac.Contact)
					.ToList();

			foreach (var appContact in appContacts)
			{
				var message = CreateAppNotification(appContact, null, null);
				message.MESSAGE_TEMPLATE = MESSAGE_TEMPLATE.First(mt => mt.NAME == Msg::MessageTemplate.DataUpdate);
				AddMessageField(message, EntityModel.MESSAGE_TEMPLATE_FIELD.DataType, updatedDataType.ToString(), typeof(string));
			}

			if (updatedDataType == UpdatedDataType.BillingServices)
			{
				//Услуги влияют на работу расписаний запросов

				var vehicleIds =
					TrackingSchedule
						.Where(ts => ts.Operator_ID == operatorId && ts.Enabled)
						.Select(ts => ts.Vehicle_ID)
						.ToList();
				foreach (var vehicleId in vehicleIds)
				{
					CreateAppNotificationAboutVehicleDataChange(vehicleId, UpdatedDataType.Schedule);
				}
			}
		}
		public void CreateAppNotificationAboutVehicleDataChange(int vehicleId, UpdatedDataType updatedDataType)
		{
			var appContacts =
				AppClient
					.Where(
						ac =>
							ac.OPERATOR.Asid.MLP_Controller.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId &&
							ac.Contact_ID != null)
					.Select(ac => ac.Contact)
					.ToList();

			foreach (var appContact in appContacts)
			{
				var message = CreateAppNotification(appContact, null, null);
				message.MESSAGE_TEMPLATE = MESSAGE_TEMPLATE.First(mt => mt.NAME == Msg::MessageTemplate.DataUpdate);
				AddMessageField(message, EntityModel.MESSAGE_TEMPLATE_FIELD.DataType, updatedDataType.ToString(), typeof(string));
				AddMessageField(message, EntityModel.MESSAGE_TEMPLATE_FIELD.VehicleID, vehicleId);
			}
		}
		public void SendMessageToSupport(string subject, string body)
		{
			foreach (var contact in GetTechnicalSupportEmails())
			{
				CreateOutgoingMessage(contact, subject, body);
			}
		}
		public SCHEDULEREVENT GetOrCreateSchedulerEvent(Type type)
		{
			if (type == null)
				throw new ArgumentNullException("type");
			if (type.AssemblyQualifiedName == null)
				throw new ArgumentOutOfRangeException("type", type, @"Unable to retrieve AssemblyQualifiedName");

			//Имя типа без версии и публичного ключа
			var typeName = type.AssemblyQualifiedName.Split(',').Take(2).Join(",");

			var result = SCHEDULEREVENT.FirstOrDefault(x => x.PLUGIN_NAME == typeName);
			if (result != null)
				return result;

			using (var entities = new Entities())
			{
				try
				{
					entities.SCHEDULEREVENT.AddObject(
					new SCHEDULEREVENT
					{
						PLUGIN_NAME = typeName
					});
					entities.SaveChanges();
				}
				catch (EntityCommandExecutionException)
				{
					//TODO: check unique index violation only
				}
			}

			return SCHEDULEREVENT.First(x => x.PLUGIN_NAME == typeName);
		}
		public IQueryable<OPERATOR> GetVehiclePayers(int vehicleId)
		{
			return OPERATOR_VEHICLE
				.Where(
					ov =>
						ov.VEHICLE_ID == vehicleId &&
						ov.RIGHT_ID   == (int)SystemRight.SecurityAdministration &&
						ov.ALLOWED)
				.Select(ov => ov.OPERATOR);
		}
	}
}