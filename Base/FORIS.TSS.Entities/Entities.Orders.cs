﻿using System;
using System.Linq;

namespace FORIS.TSS.EntityModel
{
	public partial class Entities
	{
		public ORDER GetOrderById(int orderId)
		{
			var result = ORDER
				.FirstOrDefault(o => o.ORDER_ID == orderId);
			if (result == null)
				throw new ArgumentOutOfRangeException(nameof(orderId), orderId, "No corresponding value in DB");
			return result;
		}
	}
}