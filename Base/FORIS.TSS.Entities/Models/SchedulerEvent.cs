﻿namespace FORIS.TSS.EntityModel
{
	public partial class SCHEDULEREVENT
	{
		/// <summary>Идентификатор SchedulerEvent'а для периодического (обычно ежемесячного) сброса счётчиков услуг</summary>
		public static readonly int BillingServiceCounterReset = 76;
		public const string ReportIncludePath = "REPORT";
	}
}