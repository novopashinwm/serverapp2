﻿namespace FORIS.TSS.EntityModel
{
	public partial class Asid
	{
		public Asid()
		{
			AllowMlpRequest               = true;
			PushNotificationAboutLocation = true;
			AllowsCdrPayment              = true;
		}
	}
}