﻿using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.EntityModel
{
	// ReSharper disable InconsistentNaming
	public partial class CONSTANTS
	// ReSharper restore InconsistentNaming
	{
		public static readonly string NewLoginPrefix                  = "NewLoginPrefix";
		public static readonly string VideoAppDomain                  = "VideoAppDomain";
		public static readonly string BillingUrl                      = "Viuga.BillingUrl";
		public static readonly string ViugaBillingDebugLevel          = "Viuga.Billing.DebugLevel";
		public static readonly string ProlongingMediaAcceptorName     = "ProlongingMediaAcceptorName";
		public static readonly string TechnicalSupportEmail           = "TechnicalSupportEmail";
		public static readonly string CalculatorEmail                 = "CalculatorEmail";
		public static readonly string MobileAppLink                   = "MobileAppLink";
		public static readonly string WebSiteLink                     = "WebSiteLink";
		/// <summary>Идентификатор клиента, для которого реализован SMS-сервис определения ближайших автобусных остановок</summary>
		public static readonly string RedHatSmsServiceDepartmentID    = "RedHatSmsServiceDepartmentID";
		/// <summary> F(MSISDN) sim-карты, установленной в GSM-модем сервера </summary>
		public static readonly string ModemAsid                       = "ModemAsid";
		/// <summary>IP адрес сервера, который следует использовать для настройки трекеров </summary>
		public static readonly string ServerIP                        = "ServerIP";
		/// <summary>Порт  сервера, который следует использовать для настройки трекеров </summary>
		public static readonly string ServerPort                      = "ServerPort";
		/// <summary>Название точки доступа для подключения к Интернету</summary>
		public static readonly string NetworkAPN                      = Constant.NetworkAPN.ToString();
		/// <summary>Имя пользователя для подключения к точке доступа</summary>
		public static readonly string NetworkAPNUser                  = "NetworkAPNUser";
		/// <summary>Имя пользователя для подключения к точке доступа</summary>
		public static readonly string NetworkAPNPassword              = "NetworkAPNPassword";
		/// <summary>Короткий номер телефона, на который трекер должен отправлять ответ на принятую SMS-команду</summary>
		public static readonly string ServiceShortNumber              = "ServiceShortNumber";
		/// <summary>Номер телефона, на который трекер должен отправлять ответ на принятую SMS-команду, если отправка ответов на короткие номера невозможна</summary>
		public static readonly string ServiceInternationalPhoneNumber = "ServiceInternationalPhoneNumber";
		/// <summary> Время на которое нужно блокировать отправку пуш-уведомлений на мобильное приложение, которое не отвечает подтверждениями </summary>
		public static readonly string PushLockTimeSpan                = "PushLockTimeSpan";
		public static readonly string RemovedDepartmentExtId          = "RemovedDepartmentExtId";
	}
}