﻿using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.EntityModel
{
	public partial class Contact
	{
		public override string ToString()
		{
			return (ContactType)Type + ":" + Value;
		}

		public BusinessLogic.DTO.Contact ToDto()
		{
			var contactType = (ContactType)Type;
			return new BusinessLogic.DTO.Contact(contactType, Value)
			{
				Id = ID
			};
		}
	}
}