﻿namespace FORIS.TSS.EntityModel
{
	public partial class CompoundRule_Operator_Contact
	{
		public override string ToString()
		{
			return "O: " + Operator_ID + " C: " + Contact_ID;
		}
	}
}