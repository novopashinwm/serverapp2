﻿using System;

namespace FORIS.TSS.EntityModel
{
	public partial class Country
	{
		public static readonly string India  = "India";
		public static readonly string Russia = "Russia";

		public bool Is(string country)
		{
			return string.Equals(Name, country, StringComparison.OrdinalIgnoreCase);
		}
	}
}