﻿using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.EntityModel
{
	public partial class DEPARTMENT
	{
		public DEPARTMENT()
		{
			IsCommercial = true;
		}
		public Department ToDto()
		{
			var dtoDepartment = new Department
			{
				id              = DEPARTMENT_ID,
				name            = NAME,
				description     = Description,
				extID           = ExtID,
				Type            = Type.HasValue ? (DepartmentType)Type : DepartmentType.Corporate,
				ViugaUserExists = ViugaUserExists,
				LockDate        = LockDate
			};
			if (Country != null)
				dtoDepartment.countryName = Country.Name;
			else
				dtoDepartment.countryName = Country.Russia;
			return dtoDepartment;
		}
	}
}