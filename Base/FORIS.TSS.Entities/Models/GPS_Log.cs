﻿namespace FORIS.TSS.EntityModel
{
	public partial class GPS_Log
	{
		public void SetCourseHours(double? directionHours)
		{
			if (directionHours == null)
			{
				Course = null;
				return;
			}
			Course = (byte)(directionHours.Value * 30 * 256 / 360);
		}
	}
}