﻿using System;
using FORIS.TSS.BusinessLogic.DTO;

namespace FORIS.TSS.EntityModel
{
	public partial class Billing_Service
	{
		public static readonly string TypeReferencePath = "Type";

		internal Billing_Service()
		{
			StartDate = DateTime.UtcNow;
		}

		public BillingService ToDTO()
		{
			var result = new BillingService();

			result.Id        = ID;
			result.StartDate = StartDate;
			result.EndDate   = EndDate;

			if (!TypeReference.IsLoaded)
				TypeReference.Load();

			result.Name = Type.Name;

			var isActivated    = Type.PeriodDays != null;
			result.IsActivated = isActivated;
			result.IsActivated = isActivated && result.EndDate != null;

			return result;
		}
	}
}