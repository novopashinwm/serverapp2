﻿using System;
using System.Linq;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Resources;

namespace FORIS.TSS.EntityModel
{
	public partial class CONTROLLER_TYPE
	{
		public string NormalizeDeviceId(string deviceId)
		{
			if (string.Equals(TYPE_NAME, "AvtoGraf",    StringComparison.OrdinalIgnoreCase) ||
				string.Equals(TYPE_NAME, "AvtoGrafCAN", StringComparison.OrdinalIgnoreCase))
			{
				return deviceId.TrimStart('0');
			}
			return deviceId;
		}
		public ControllerType ToDto(ResourceContainers container)
		{
			return new ControllerType
			{
				Id                         = CONTROLLER_TYPE_ID,
				Name                       = TYPE_NAME,
				UserFriendlyName           = container.GetLocalizedText<ControllerType>(TYPE_NAME),
				SupportsPassword           = SupportsPassword,
				SupportsPositionAsking     = CommandTypes.Any(t => t.id == (int)CmdType.AskPosition),
				DeviceIdIsRequiredForSetup = DeviceIdIsRequiredForSetup,
				DeviceIdIsImei             = DeviceIdIsImei,
				ImagePath                  = ImagePath,
				SortOrder                  = SortOrder
			};
		}
	}
}