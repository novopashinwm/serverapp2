﻿using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;
using CommandDto = FORIS.TSS.BusinessLogic.DTO.Command;

namespace FORIS.TSS.EntityModel
{
	public partial class Command
	{
		public const string Command_ParameterIncludePath = "Command_Parameter";
		public CommandDto ToDto()
		{
			var result = new CommandDto
			{
				Id         = ID,
				Created    = Date_Received,
				TargetID   = Target_ID,
				Result     = (CmdResult)Result_Type_ID,
				Status     = (CmdStatus)Status,
				Type       = (CmdType)Type_ID,
				ResultDate = Result_Date_Received
			};

			if (Command_Parameter.Count != 0)
			{
				result.Parameters = new Dictionary<string, string>();
				foreach (var parameter in Command_Parameter)
				{
					result.Parameters[parameter.Key] = parameter.Value;
				}
			}

			return result;
		}
		public static CommandDto ToDto(Command command)
		{
			return command.ToDto();
		}
		public bool HasParameter(string key)
		{
			return Command_Parameter.Any(p => p.Key == key);
		}
		public bool HasParameter(string key, string value)
		{
			return Command_Parameter.Any(p => p.Key == key && p.Value == value);
		}
	}
}