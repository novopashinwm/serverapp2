﻿using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.EntityModel
{
	public partial class CONTROLLER_SENSOR_LEGEND
	{
		public Sensor ToDTO()
		{
			return new Sensor
			{
				SensorId = Number,
				Number   = (SensorLegend)Number,
				Type     = (SensorType)CONTROLLER_SENSOR_TYPE.CONTROLLER_SENSOR_TYPE_ID
			};
		}
	}
}