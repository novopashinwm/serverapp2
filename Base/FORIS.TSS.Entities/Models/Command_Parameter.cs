﻿namespace FORIS.TSS.EntityModel
{
	public partial class Command_Parameter
	{
		public static readonly string CloudPushSent    = "CloudPushSent";
		/// <summary>Идентификатор сессии, в которой была создана команда</summary>
		public static readonly string SessionID        = "SessionID";
		public static readonly string Created          = "Created";
		public static readonly string NotificationSent = nameof(NotificationSent);
	}
}