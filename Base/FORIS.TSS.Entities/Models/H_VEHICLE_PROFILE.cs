﻿using FORIS.TSS.BusinessLogic.DTO.Historical;

namespace FORIS.TSS.EntityModel.History
{
	public partial class H_VEHICLE_PROFILE
	{
		public VehicleProfileHistory ToDTO()
		{
			return new VehicleProfileHistory
			{
				ActualTime = ACTUAL_TIME,
				Name       = PROPERTY_NAME,
				Value      = Value
			};
		}
	}
}