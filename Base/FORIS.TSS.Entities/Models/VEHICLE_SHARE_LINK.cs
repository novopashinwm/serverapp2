﻿using System;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;

namespace FORIS.TSS.EntityModel
{
	public partial class VEHICLE_SHARE_LINK
	{
		public ShareLink ToDTO()
		{
			return new ShareLink
			{
				LinkGuid    = VEHICLE_SHARE_LINK_GUID,
				CreatorId   = VEHICLE_SHARE_LINK_CREATOR_ID,
				LogTimeBeg  = VEHICLE_SHARE_LINK_BEG.ToLogTime(),
				LogTimeEnd  = VEHICLE_SHARE_LINK_END.ToLogTime(),
				VehicleId   = VEHICLE_SHARE_LINK_VEHICLE_ID,
				VehicleName = VEHICLE_SHARE_LINK_NAME,
				VehicleDesc = VEHICLE_SHARE_LINK_DESC,
			};
		}
		public static VEHICLE_SHARE_LINK CreateFromDTO(ShareLink dtoShareLink, Entities entities = null)
		{
			var ormShareLink = new VEHICLE_SHARE_LINK
			{
				VEHICLE_SHARE_LINK_CREATOR_ID = dtoShareLink.CreatorId,
				VEHICLE_SHARE_LINK_GUID       = ShareLink.EmptyGuid != dtoShareLink.LinkGuid
					? dtoShareLink.LinkGuid
					: Guid.NewGuid(),
			};
			return ormShareLink.UpdateFromDTO(dtoShareLink, entities);
		}
		public VEHICLE_SHARE_LINK UpdateFromDTO(ShareLink dtoShareLink, Entities entities = null)
		{
			if (null != dtoShareLink)
			{
				VEHICLE_SHARE_LINK_BEG        = dtoShareLink.LogTimeBeg.ToUtcDateTime();
				VEHICLE_SHARE_LINK_END        = dtoShareLink.LogTimeEnd.ToUtcDateTime();
				VEHICLE_SHARE_LINK_CREATOR_ID = dtoShareLink.CreatorId;
				VEHICLE_SHARE_LINK_VEHICLE_ID = dtoShareLink.VehicleId;
				VEHICLE_SHARE_LINK_NAME       = dtoShareLink.VehicleName;
				VEHICLE_SHARE_LINK_DESC       = dtoShareLink.VehicleDesc;
			}
			return this;
		}
	}
}