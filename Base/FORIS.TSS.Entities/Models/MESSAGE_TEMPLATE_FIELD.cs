﻿using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace FORIS.TSS.EntityModel
{
	public partial class MESSAGE_TEMPLATE_FIELD
	{
		public const string VehicleID             = "VehicleID";
		/// <summary> Идентификатор объекта наблюдения, у которого нужно взять изображение, чтобы приложить к письму </summary>
		public const string PictureVehicleID      = "Picture.VehicleID";
		/// <summary> Дата захвата изображения, которое нужно приложить к письму </summary>
		public const string PictureLogTime        = "Picture.LogTime";
		/// <summary> Название файла, который которое нужно приложить к письму </summary>
		public const string PictureName           = "Picture.Name";
		/// <summary>Дата события, о котором идёт речь в сообщении</summary>
		public const string Date                  = "date";
		/// <summary> Контакты, на которые должен придти ответ. В случае телефона - подменяет адреса отправителя. </summary>
		public const string ReplyTo               = "ReplyTo";
		/// <summary> Код подтверждения </summary>
		public const string ConfirmationCode      = "ConfirmationCode";
		/// <summary>Счётчик количества оставшихся попыток</summary>
		public const string RemainingAttemptCount = "RemainingAttemptCount";
		/// <summary> Использован ли код подтверждения </summary>
		public const string ConfirmationIsUsed    = "ConfirmationIsUsed";
		/// <summary> Уникальный идентификатор заявки, например, на демаскирование </summary>
		public const string RequestUid            = "RequestUid";
		public const string CommandId             = "CommandId";
		/// <summary> Номер телефона друга, отправившего приглашение </summary>
		public const string FriendMsisdn          = "FriendMsisdn";
		/// <summary> Маскированный номер телефона пользователя, запросившего местоположение </summary>
		public const string SenderAsid            = "SenderAsid";
		/// <summary> Флаг указывает нужно ли отображать push-уведомление в мобильном приложении </summary>
		public const string PushIsVisible         = "visible";
		/// <summary> Путь к сгенерированному файлу отчета (путь к временному файлу). </summary>
		public const string ReportFilePath        = "Report.FilePath";
		/// <summary> Название файла сгенерированного отчета, как должно отображаться. </summary>
		public const string ReportName            = "Report.Name";
		/// <summary> Сообщение для пуша при отправке смс-сообщения. </summary>
		public const string PushMessage           = "PushMessage";
		/// <summary> Идентификатор оператора, которому предназначается сообщение </summary>
		public const string OperatorId            = "OperatorId";
		/// <summary> Тип данных </summary>
		public const string DataType              = "DataType";
		/// <summary> Номер телефона для пуша </summary>
		public static string Msisdn               = "Msisdn";
		/// <summary> Сообщение для пользователя </summary>
		public static string MessageText          = "text";
		/// <summary> Тип оказываемой услуги </summary>
		public static string RenderedServiceType  = "RenderedServiceType";

		public Msg::MessageTemplateField ToDTO()
		{
			return new Msg::MessageTemplateField
			{
				Id           = MESSAGE_TEMPLATE_FIELD_ID,
				TemplateId   = MESSAGE_TEMPLATE != null ? MESSAGE_TEMPLATE.MESSAGE_TEMPLATE_ID : (int?)null,
				DotNetType   = DOTNET_TYPE.TYPE_NAME,
				DotNetTypeId = DOTNET_TYPE.DOTNET_TYPE_ID,
				Name         = NAME,
			};
		}
	}
}