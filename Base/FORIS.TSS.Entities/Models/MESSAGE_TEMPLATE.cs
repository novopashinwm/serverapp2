﻿using System.Collections.Generic;
using System.Globalization;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace FORIS.TSS.EntityModel
{
	public partial class MESSAGE_TEMPLATE
	{
		public string FormatHeader(IEnumerable<MESSAGE_FIELD> messageFields)
		{
			return Format(HEADER, messageFields);
		}
		public string FormatMessage(IEnumerable<MESSAGE_FIELD> messageFields)
		{
			return Format(BODY, messageFields);
		}
		private static string Format(string template, IEnumerable<MESSAGE_FIELD> messageFields)
		{
			foreach (var messageField in messageFields)
			{
				// Нет у поля имени или значения подстановка не обрабатывается
				if (string.IsNullOrWhiteSpace(messageField.MESSAGE_TEMPLATE_FIELD?.NAME) || string.IsNullOrWhiteSpace(messageField.CONTENT))
					continue;
				template = template.Replace("$$" + messageField.MESSAGE_TEMPLATE_FIELD.NAME + "$$", messageField.CONTENT);
			}
			return template;
		}
		public Msg::MessageTemplate ToDTO()
		{
			return new Msg::MessageTemplate
			{
				Id      = MESSAGE_TEMPLATE_ID,
				Body    = BODY,
				Header  = HEADER,
				Name    = NAME,
				Culture = Culture != null ? CultureInfo.GetCultureInfo(Culture.Code) : null
			};
		}
	}
}