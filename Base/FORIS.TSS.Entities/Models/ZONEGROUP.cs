﻿using FORIS.TSS.BusinessLogic.DTO;

namespace FORIS.TSS.EntityModel
{
	public partial class ZONEGROUP
	{
		public const string MembersIncludePath = "Members";

		public Group ToDto()
		{
			return new Group
			{
				Id          = ZONEGROUP_ID,
				Name        = NAME,
				Description = DESCRIPTION
			};
		}
	}
}