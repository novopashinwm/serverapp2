﻿using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.EntityModel
{
	partial class VehicleControlDate
	{
		public BusinessLogic.DTO.VehicleControlDate ToDto()
		{
			return new BusinessLogic.DTO.VehicleControlDate
			{
				TypeId = (VehicleControlDateTypes) Type_ID,
				Value  = Value
			};
		}
	}
}