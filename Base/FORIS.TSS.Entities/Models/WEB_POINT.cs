﻿using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Resources;

namespace FORIS.TSS.EntityModel
{
	public partial class WEB_POINT
	{
		public WebPoint ToDto()
		{
			return new WebPoint
			{
				Id   = POINT_ID,
				Name = NAME,
				Desc = DESCRIPTION,
				Type = (WebPointType?)TYPE_ID,
				X    = MAP_VERTEX?.X ?? double.NaN,
				Y    = MAP_VERTEX?.Y ?? double.NaN,
			};
		}
		public static WEB_POINT CreateFromDto(WebPoint dtoWebPoint, Entities entities = null)
		{
			var ormWebPoint = new WEB_POINT
			{
			};
			if (WebPoint.EmptyId != dtoWebPoint.Id)
				ormWebPoint.POINT_ID = dtoWebPoint.Id;
			ormWebPoint.UpdateFromDto(dtoWebPoint, entities);
			return ormWebPoint;
		}
		public void UpdateFromDto(WebPoint dtoWebPoint, Entities entities = null)
		{
			if (null == dtoWebPoint)
				return;
			NAME        = dtoWebPoint.Name;
			DESCRIPTION = dtoWebPoint.Desc;
			TYPE_ID     = (int?)dtoWebPoint?.Type;
			if (null == MAP_VERTEX)
			{
				var MAP_VERTEX = new MAP_VERTEX
				{
					X      = dtoWebPoint.X,
					Y      = dtoWebPoint.Y,
					ENABLE = true,
				};
				entities.MAP_VERTEX.AddObject(MAP_VERTEX);
			}
			else
			{
				MAP_VERTEX.X = dtoWebPoint.X;
				MAP_VERTEX.Y = dtoWebPoint.Y;
			}
		}
	}
	public partial class GetPointsWeb_Result
	{
		public WebPoint ToDto()
		{
			return new WebPoint
			{
				Id   = POINT_ID,
				Name = NAME,
				Desc = DESCRIPTION,
				Type = (WebPointType?)TYPE_ID,
				X    = X,
				Y    = Y,
			};
		}
	}
}