﻿using FORIS.TSS.BusinessLogic.DTO.Messages;

namespace FORIS.TSS.EntityModel
{
	public partial class MESSAGE_FIELD
	{
		public static readonly string MessageTemplateFieldIncludePath = "MESSAGE_TEMPLATE_FIELD";

		public MessageField ToDTO()
		{
			var result = new MessageField();
			result.Id              = MESSAGE_FIELD_ID;
			result.MessageId       = MESSAGE != null ? MESSAGE.MESSAGE_ID : default(int);
			result.TemplateFieldId = MESSAGE != null && MESSAGE.MESSAGE_TEMPLATE != null ? MESSAGE.MESSAGE_TEMPLATE.MESSAGE_TEMPLATE_ID : default(int);
			result.Value           = LargeContent ?? CONTENT;
			result.Template        = MESSAGE_TEMPLATE_FIELD != null ? MESSAGE_TEMPLATE_FIELD.ToDTO() : null;
			return result;
		}

		public string GetContent()
		{
			if (!string.IsNullOrWhiteSpace(LargeContent))
				return LargeContent;

			return CONTENT;
		}

		public void SetContent(string value)
		{
			if (value == null)
			{
				CONTENT = null;
				LargeContent = null;
				return;
			}

			if (255 < value.Length)
			{
				CONTENT = value.Substring(0, 255);
				LargeContent = value;
				return;
			}

			CONTENT = value;
			LargeContent = null;
		}
	}
}