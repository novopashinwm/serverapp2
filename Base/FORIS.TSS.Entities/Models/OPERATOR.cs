﻿using System;
using System.Globalization;
using System.Text;
using FORIS.TSS.BusinessLogic.DTO.OperatorRights;

namespace FORIS.TSS.EntityModel
{
	public partial class OPERATOR
	{
		public OPERATOR()
		{
			NAME     = string.Empty;
			LOGIN    = string.Empty;
			PASSWORD = string.Empty;
		}
		public static readonly int DefaultPasswordLength = 10;
		public static string GeneratePassword()
		{
			var random = new Random();
			var password = new StringBuilder(DefaultPasswordLength);
			for (var i = 0; i != DefaultPasswordLength; ++i)
				password.Append(random.Next(0, 9));
			return password.ToString();
		}
		public TimeZoneInfo TimeZoneInfoInstance
		{
			get
			{
				if (!TimeZoneReference.IsLoaded)
					TimeZoneReference.Load();
				return TimeZone != null
					? System.TimeZoneInfo.FindSystemTimeZoneById(TimeZone.Code)
					: System.TimeZoneInfo.Local;
			}
		}
		public CultureInfo CultureInfoInstance
		{
			get
			{
				if (!CultureReference.IsLoaded)
					CultureReference.Load();
				return CultureInfo.GetCultureInfo(
					Culture?.Code ?? CultureInfo.CurrentCulture.TwoLetterISOLanguageName);
			}
		}
		public bool CheckPassword(string password)
		{
			return string.Equals(PASSWORD, password);
		}
		public Operator ToDto()
		{
			return new Operator
			{
				id    = OPERATOR_ID,
				name  = NAME,
				login = LOGIN
			};
		}
	}
}