﻿using System;
using System.Linq;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Messages;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Helpers;
using Message = FORIS.TSS.BusinessLogic.DTO.Message;

namespace FORIS.TSS.EntityModel
{
	public partial class MESSAGE
	{
		public const string MessageTemplateIncludePath = "MESSAGE_TEMPLATE";
		public const string ContactsIncludePath        = "Contacts";
		public const string MessageFieldIncludePath    = "MESSAGE_FIELD";

		public MESSAGE()
		{
			var now = DateTime.UtcNow;
			Created          = now;
			TIME             = now;
			ProcessingResult = (int)MessageProcessingResult.Received;
			STATUS           = (int)MessageState.Wait;
		}
		public void SetContent(MESSAGE_TEMPLATE template)
		{
			SUBJECT = template.FormatHeader(MESSAGE_FIELD);
			BODY    = template.FormatMessage(MESSAGE_FIELD);
		}
		private void AddContact(MessageContactType type, Contact contact)
		{
			Contacts.Add(new Message_Contact
			{
				Contact = contact,
				Type    = (int)type
			});
		}
		public void AddDestination(Contact contact)
		{
			AddContact(MessageContactType.Destination, contact);
		}
		public void AddSource(Contact contact)
		{
			AddContact(MessageContactType.Source, contact);
		}
		public Message ToDto()
		{
			var severity = SEVERITY_ID != null ? (Severity)SEVERITY_ID.Value : Severity.Unknown;

			var result = new Message
			{
				Time         = TIME,
				Severity     = severity,
				MessageTitle = SUBJECT,
				MessageBody  = BODY
			};

			if (DestinationType_ID == (int) MessageDestinationType.MpxSms)
			{
				//TODO: вообще, \f должны добавляться в сообщение только в момент отправки, т.к. это часть протокола с MPX
				if (result.MessageTitle != null)
					result.MessageTitle = result.MessageTitle.Replace("\f", string.Empty);

				if (result.MessageBody!= null)
					result.MessageBody = result.MessageBody.Replace("\f", string.Empty);
			}

			return result;
		}
		public MessageWeb ToWeb()
		{
			var fromContact = Contacts
				.Where(c => c.Type == (int) MessageContactType.Source)
				.Select(c => c.Contact)
				.FirstOrDefault();
			var toContact = Contacts
				.Where(c => c.Type == (int)MessageContactType.Destination)
				.Select(c => c.Contact)
				.FirstOrDefault();
			var from = fromContact != null
				? new MessageWeb.Contact
				{
					Id    = fromContact.ID,
					Type  = ((ContactType) fromContact.Type).ToString(),
					Value = fromContact.Value
				} : null;
			var to = toContact != null
				? new MessageWeb.Contact
				{
					Id    = toContact.ID,
					Type  = ((ContactType)toContact.Type).ToString(),
					Value = toContact.Value
				} : null;
			var message = new MessageWeb
			{
				Id                = MESSAGE_ID,
				Created           = Created,
				Status            = (STATUS.HasValue ? (MessageState)STATUS.Value : MessageState.Wait).ToString(),
				Result            = (MessageProcessingResult)ProcessingResult,
				Body              = (BODY ?? string.Empty).Replace("\f", string.Empty),
				Subject           = (SUBJECT ?? string.Empty).Replace("\f", string.Empty),
				Time              = TIME,
				OwnerOperatorId   = Owner_Operator_ID,
				OwnerOperatorName = Owner_Operator_ID.HasValue ? OwnerOperator.NAME : null,
				TemplateName      = MESSAGE_TEMPLATE != null ? MESSAGE_TEMPLATE.NAME : null,
				From              = from,
				To                = to,
				Fields            = MESSAGE_FIELD.Select(mf => new
				{
					Name  = mf.MESSAGE_TEMPLATE_FIELD != null ? mf.MESSAGE_TEMPLATE_FIELD.NAME : string.Empty,
					Value = mf.GetContent()
				}).ToDictionary(key => key.Name, value => value.Value)
			};

			message.MessageType = message.To != null ? MessageType.Outgoing : MessageType.Incoming;
			return message;
		}
		public void AddReference(int contactId)
		{
			Contacts.Add(new Message_Contact
			{
				Type       = (int)MessageContactType.Reference,
				Contact_ID = contactId
			});
		}
		public string GetFieldValue(string name)
		{
			if (!MESSAGE_FIELD.IsLoaded)
				MESSAGE_FIELD.Load();

			return MESSAGE_FIELD
				.Where(f => f.MESSAGE_TEMPLATE_FIELD.NAME == name)
				.Select(f => f.CONTENT)
				.FirstOrDefault();
		}
		public class Blank
		{
			public readonly string Name;
			public readonly string Text;
			public readonly string Date;
			public Blank(string name, string text, string date = null)
			{
				Name = name;
				Text = text;
				Date = date;
			}
		}
		public void Fill(Blank blank)
		{
			var destinationType = (ContactType)Contacts.First(c => c.Type == (int)MessageContactType.Destination).Contact.Type;
			switch (destinationType)
			{
				case ContactType.Android:
					SUBJECT = blank.Name;

					BODY = new[] { blank.Date, blank.Text }.Join(" ", ignoreEmpty: true);

					if (string.IsNullOrWhiteSpace(blank.Text))
						BODY = blank.Name;
					else if (blank.Text.StartsWith(blank.Name))
						BODY = blank.Text;
					else
						BODY = $"{(blank.Date != null ? blank.Date + " " : "")} {blank.Text}";
					break;
				case ContactType.Apple:
				case ContactType.iOsDebug:
					SUBJECT = null;

					BODY = new[] { blank.Date, blank.Name, blank.Text }.Join(" ", ignoreEmpty: true);

					if (string.IsNullOrWhiteSpace(blank.Name))
						BODY = blank.Text;
					else if (string.IsNullOrWhiteSpace(blank.Text))
						BODY = blank.Name;
					else if (blank.Text.StartsWith(blank.Name))
						BODY = blank.Text;
					else
						BODY = $"{(blank.Date != null ? blank.Date + " " : "")}{blank.Name} {blank.Text}";
					break;
				default:
					throw new InvalidOperationException("destinationType is not supported: " + destinationType);
			}

		}
	}
}