﻿using System;
using System.Collections;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.EntityModel
{
	public partial class Entities
	{
		public Command CreateNewCommand(OPERATOR @operator, VEHICLE vehicle, int cmdType, IDictionary parameters)
		{
			var date = DateTime.UtcNow;

			var command = new Command();
			AddToCommand(command);
			command.Sender = @operator;
			command.Date_Received = date;
			command.Type_ID = cmdType;
			command.Target = vehicle;
			command.Status = (int) CmdStatus.Received;
			command.Result_Date_Received = date;
			command.Result_Type_ID = (int) CmdResult.Received;

			if (parameters != null)
			{
				foreach (DictionaryEntry entry in parameters)
				{
					var key = entry.Key as string;
					if (string.IsNullOrEmpty(key)) continue;
					var value = entry.Value as string;
					AddCommandParameter(command, key, value);
				}
			}

			AddCommandParameter(command, EntityModel.Command_Parameter.Created, TimeHelper.ToXmlFormat(DateTime.UtcNow));

			return command;
		}

		public void AddCommandParameter(Command command, string key, string value = null)
		{
			if (command.HasParameter(key))
				return;

			var commandParameter = new Command_Parameter();
			Command_Parameter.AddObject(commandParameter);
			commandParameter.Key = key;
			commandParameter.Value = value;
			command.Command_Parameter.Add(commandParameter);
		}

		public void SetCommandParameter(Command command, string key, string value = null)
		{
			var commandParameter = command.Command_Parameter.FirstOrDefault(x => x.Key == key);
			if (commandParameter == null)
			{
				commandParameter = new Command_Parameter();
				Command_Parameter.AddObject(commandParameter);
				commandParameter.Key = key;
				command.Command_Parameter.Add(commandParameter);
			}
			commandParameter.Value = value;
		}
	}
}