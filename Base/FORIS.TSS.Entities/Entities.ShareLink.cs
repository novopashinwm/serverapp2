﻿using System;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common.Helpers;

namespace FORIS.TSS.EntityModel
{
	public partial class Entities
	{
		/// <summary> Получить или создать оператора, используемого для предоставления права просмотра по общей ссылке </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		public OPERATOR GetOrCreateOperator4ShareLink(int vehicleId)
		{
			var vehGuid = vehicleId.ToGuid();
			var ormOper = OPERATOR
				.FirstOrDefault(o => o.GUID == vehGuid);
			if (null == ormOper)
			{
				ormOper = new OPERATOR
				{
					NAME = $@"ShareLink({vehicleId:D10})",
					GUID = vehGuid,
				};
				OPERATOR.AddObject(ormOper);
				// Сохранить оператора (история без указания текущего оператора, т.к. создается при первом создании ссылки на объект)
				SaveChangesWithHistory();
			}
			// Системные права оператора
			if (!(IsAllowed(ormOper, SystemRight.VehicleAccess) && IsAllowed(ormOper, SystemRight.SecurityMapGis)))
				Allow(ormOper, new[] { SystemRight.VehicleAccess, SystemRight.SecurityMapGis });
			var ormVehicle = GetVehicle(vehicleId);
			// Права на конкретный объект наблюдения
			if (!IsAllowedAll(ormOper, ormVehicle, SystemRight.VehicleAccess))
				Allow(ormOper, ormVehicle, new[] { SystemRight.VehicleAccess });
			// Сохранить права оператора (история без указания текущего оператора, т.к. создается при первом создании ссылки на объект)
			SaveChangesWithHistory();
			return ormOper;
		}
		public IQueryable<VEHICLE_SHARE_LINK> GetQuery4ShareLinks(
			Guid?  linkGuid    = null,
			int?   creatorId   = null,
			int?   vehicleId   = null)
		{
			var queryShareLinks = (IQueryable<VEHICLE_SHARE_LINK>)VEHICLE_SHARE_LINK;
			// Добавить фильтр по идентификатору
			if (linkGuid.HasValue)
				queryShareLinks = queryShareLinks.Where(l => l.VEHICLE_SHARE_LINK_GUID       == linkGuid.Value);
			// Добавить фильтр по создателю
			if (creatorId.HasValue)
				queryShareLinks = queryShareLinks.Where(l => l.VEHICLE_SHARE_LINK_CREATOR_ID == creatorId.Value);
			// Добавить фильтр по объекту
			if (vehicleId.HasValue)
				queryShareLinks = queryShareLinks.Where(l => l.VEHICLE_SHARE_LINK_VEHICLE_ID == vehicleId.Value);
			return queryShareLinks;
		}
	}
}