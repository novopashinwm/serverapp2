﻿using System;
using System.Linq;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.EntityModel
{
	public partial class Entities
	{
		public void SetSensorLegendMapping(CONTROLLER controller, SensorLegend sensorLegend, SensorMappingValue[] mappings)
		{
			var sensorLegendNumber = (int)sensorLegend;
			var legend = (from l in CONTROLLER_SENSOR_LEGEND
							where l.Number == sensorLegendNumber
							select l).FirstOrDefault();
			if (legend == null)
				return;

			var sensorNumbers = mappings.Select(m => m.InputNumber).ToArray();
			var controllerTypeID = controller.CONTROLLER_TYPE.CONTROLLER_TYPE_ID;
			var controllerSensors =
			(
				from cs in CONTROLLER_SENSOR
				where sensorNumbers.Contains(cs.NUMBER) &&
					cs.CONTROLLER_TYPE.CONTROLLER_TYPE_ID == controllerTypeID
				select cs
			)
				.ToDictionary(key => key.NUMBER);

			var existingMappingsToLegend = CONTROLLER_SENSOR_MAP
				.Where(m =>
						m.CONTROLLER.CONTROLLER_ID == controller.CONTROLLER_ID &&
						m.CONTROLLER_SENSOR_LEGEND.CONTROLLER_SENSOR_LEGEND_ID == legend.CONTROLLER_SENSOR_LEGEND_ID)
				.ToArray();
			foreach (var controllerSensorMap in existingMappingsToLegend)
				CONTROLLER_SENSOR_MAP.DeleteObject(controllerSensorMap);

			foreach (var group in mappings.GroupBy(m => new Tuple<int, long?, long?>(m.InputNumber, m.MinValue, m.MaxValue)))
			{
				var multiplier = group.Sum(g => g.Multiplier);
				var constant   = group.Sum(g => g.Constant);

				if(!controllerSensors.ContainsKey(group.Key.Item1))
					throw new ArgumentException(string.Format("input number {0} not found for vehicle {1}", group.Key.Item1, controller.VEHICLE.VEHICLE_ID));
				var controllerSensor = controllerSensors[group.Key.Item1];
				var map = new CONTROLLER_SENSOR_MAP
				{
					CONTROLLER               = controller,
					CONTROLLER_SENSOR        = controllerSensor,
					CONTROLLER_SENSOR_LEGEND = legend,
					Multiplier               = multiplier,
					Constant                 = constant,
					Min_Value                = group.Key.Item2,
					Max_Value                = group.Key.Item3
				};

				AddToCONTROLLER_SENSOR_MAP(map);
				AddDefaultRules(controller, legend);
			}
		}
	}
}