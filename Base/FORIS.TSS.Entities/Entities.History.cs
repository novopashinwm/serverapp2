﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.EntityModel.History;

namespace FORIS.TSS.EntityModel
{
	public partial class Entities
	{
		private readonly List<IHistoricalItem> _historicalItems = new List<IHistoricalItem>();
		private readonly HistoricalEntities    _historyEntities = new HistoricalEntities();

		internal const string DeleteAction = "DELETE";
		internal const string InsertAction = "INSERT";
		internal const string UpdateAction = "UPDATE";

		/// <summary> Вызывает <see cref="ObjectContext.SaveChanges()"/>, а затем создает отметки в исторические таблицы и сохраняет их в БД </summary>
		/// <param name="sessionId"> Идентификатор сессии (опционально), к которому привязывается исторические записи </param>
		/// <returns> Возвращает результат первого вызова <see cref="ObjectContext.SaveChanges()"/> </returns>
		public int SaveChangesBySession(int sessionId)
		{
			var session = (from s in _historyEntities.SESSION where s.SESSION_ID == sessionId select s).FirstOrDefault();
			if (session == null)
				throw new ArgumentOutOfRangeException(nameof(sessionId), sessionId, @"Session not found by ID");

			var result = SaveChanges(t => t.SESSION = session);
			return result;
		}
		/// <summary> Сохраняет изменения, фиксируя историю в БД с привязкой к пользователю </summary>
		/// <param name="operator"> Пользователь, если не указан, история привязывается к анонимной сессии </param>
		/// <returns> The number of objects in an Added, Modified, or Deleted state when SaveChanges() was called </returns>
		public int SaveChanges(OPERATOR @operator)
		{
			return SaveChangesByOperator(@operator != null ? @operator.OPERATOR_ID : (int?)null);
		}
		/// <summary> Сохраняет изменения, фиксируя историю в БД с привязкой к сообщению </summary>
		/// <param name="message"></param>
		/// <returns></returns>
		public int SaveChanges(MESSAGE message)
		{
			return SaveChangesByMessage(message.MESSAGE_ID);
		}
		/// <summary> Сохраняет изменения, фиксируя историю в БД с привязкой к сессии </summary>
		/// <param name="session"> Сессия, если не указана - привязывается к анонимному trail'у </param>
		/// <returns> The number of objects in an Added, Modified, or Deleted state when SaveChanges() was called </returns>
		public int SaveChanges(SESSION session)
		{
			return SaveChanges(t => t.SESSION = session);
		}
		public int SaveChangesByOperator(int? operatorId)
		{
			return SaveChanges(t => t.Operator_ID = operatorId);
		}
		public int SaveChangesByMessage(int messageId)
		{
			return SaveChanges(t => t.Message_ID = messageId);
		}
		public int SaveChangesBySchedulerQueue(int schedulerQueueId)
		{
			return SaveChanges(t => t.SchedulerQueue_ID = schedulerQueueId);
		}
		/// <summary> Сохранить историю без указания текущего оператора </summary>
		/// <returns></returns>
		public int SaveChangesWithHistory()
		{
			return SaveChanges((OPERATOR)null);
		}
		public int SaveChanges(SaveContext context)
		{
			switch (context.Type)
			{
				case SaveContextIdType.None:
					return SaveChangesWithHistory();
				case SaveContextIdType.Message:
					return SaveChangesByMessage(context.Id);
				case SaveContextIdType.SchedulerQueue:
					return SaveChangesBySchedulerQueue(context.Id);
				case SaveContextIdType.Operator:
					return SaveChangesByOperator(context.Id);
				case SaveContextIdType.Session:
					return SaveChangesBySession(context.Id);
				default:
					throw new NotImplementedException(context.Type.ToString());
			}
		}
		public SESSION CreateSession(int? operatorID, string hostIP = "127.0.0.1", DateTime? sessionStart = null)
		{
			if (hostIP == null)
				throw new ArgumentNullException("hostIP");

			var session = new SESSION
			{
				OPERATOR_ID = operatorID,
				SESSION_START = sessionStart ?? DateTime.UtcNow,
				HOST_IP = hostIP,
				HOST_NAME = hostIP
			};
			_historyEntities.SESSION.AddObject(session);
			return session;
		}
		private int SaveChanges(Action<TRAIL> commentTrail)
		{
			var addedEntries    = ObjectStateManager.GetObjectStateEntries(EntityState.Added).ToArray();
			var modifiedEntries = ObjectStateManager.GetObjectStateEntries(EntityState.Modified).ToArray();
			var deletedEntries  = ObjectStateManager.GetObjectStateEntries(EntityState.Deleted).ToArray();

			CreateHistoricalEntries(EntityState.Deleted,  deletedEntries);
			CreateHistoricalEntries(EntityState.Modified, modifiedEntries);

			var result = SaveChanges();

			CreateHistoricalEntries(EntityState.Added, addedEntries);

			if (_historicalItems.Count != 0)
			{
				var trailTime = DateTime.UtcNow;

				var trail = TRAIL.CreateTRAIL(-1, trailTime);
				_historyEntities.AddToTRAIL(trail);
				commentTrail(trail);

				var actionString = GetActionString(EntityState.Deleted);
				foreach (var historicalItem in _historicalItems)
				{
					if (String.IsNullOrEmpty(historicalItem.ACTION))
						historicalItem.ACTION = actionString;
					historicalItem.ACTUAL_TIME = trailTime;
					historicalItem.TRAIL = trail;
				}

				_historicalItems.Clear();
			}

			_historyEntities.SaveChanges();

			return result;
		}
		private void CreateHistoricalEntries(EntityState entityState, IEnumerable<ObjectStateEntry> entries)
		{
			var actionString = GetActionString(entityState);
			foreach (var entry in entries)
			{
				if (entry.IsRelationship)
					continue;
				var historicalEntity = entry.Entity as IHistorical;
				if (historicalEntity == null)
					continue;
				var historicalItem = historicalEntity.CreateHistoricalItem(_historyEntities);
				historicalItem.ACTION = actionString;
				_historicalItems.Add(historicalItem);
			}
		}
		private static string GetActionString(EntityState state)
		{
			switch (state)
			{
				case EntityState.Added:
					return InsertAction;
				case EntityState.Deleted:
					return DeleteAction;
				case EntityState.Modified:
					return UpdateAction;
				default:
					throw new ArgumentOutOfRangeException(nameof(state));
			}
		}
		protected override void Dispose(bool disposing)
		{
			_historyEntities.Dispose();
			base.Dispose(disposing);
		}
	}
}