﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Globalization;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;

namespace FORIS.TSS.EntityModel
{
	/// <summary> Методы для извлечения данных из модели по внешним ключам </summary>
	public partial class Entities
	{
		public int? SalesOperatorGroupID
		{
			get
			{
				var data = CONSTANTS.FirstOrDefault(c => c.NAME == "SalesOperatorGroupID");
				int result;
				if (data == null || !int.TryParse(data.VALUE, out result))
					return null;
				return result;
			}
		}
		public CONTROLLER_SENSOR_LEGEND GetSensorLegend(SensorLegend sensorLegend)
		{
			var number = (int) sensorLegend;
			return (from legend in CONTROLLER_SENSOR_LEGEND
				where legend.Number == number
				select legend).First();
		}
		//TODO: перенести остальные методы из EntityModelExtensions.cs
		/// <summary>Извлекает из справочника хостов или создает новый хост по имени</summary>
		public Host GetOrCreateHostByName(string hostName)
		{
			var host = Host.FirstOrDefault(h => h.Name == hostName);
			if (host == null)
			{
				host = new Host {Name = hostName};
				Host.AddObject(host);
			}
			return host;
		}
		/// <summary> Получить запрос департаментов в которые у operatorId есть доступ (право DepartmentsAccess) </summary>
		/// <param name="operatorId"></param>
		/// <returns></returns>
		public IQueryable<DEPARTMENT> GetAvailableDepartmentsByOperatorIdQuery(int operatorId)
		{
			return DEPARTMENT.Where(d =>
				v_operator_department_right.Any(odr =>
					odr.Department_ID == d.DEPARTMENT_ID                    &&
					odr.right_id      == (int)SystemRight.DepartmentsAccess &&
					odr.operator_id   == operatorId));
		}
		public VEHICLE GetVehicleByDeviceId(string controllerDeviceId)
		{
			var deviceId = Encoding.ASCII.GetBytes(controllerDeviceId);
			return CONTROLLER_INFO
				.Where(ci => ci.DEVICE_ID == deviceId)
				.Select(ci => ci.CONTROLLER.VEHICLE)
				.FirstOrDefault();
		}
		public WEB_POINT_TYPE GetWebPointType(WebPointType webPointType)
		{
			var result = WEB_POINT_TYPE.FirstOrDefault(wpt => wpt.TYPE_ID == (int)webPointType);

			if (result == null)
				throw new InvalidOperationException("WebPointType " + webPointType + " does not present in DB");

			return result;
		}
		public WEB_POINT_TYPE GetWebPointType(string name)
		{
			var result = WEB_POINT_TYPE.FirstOrDefault(wpt => wpt.NAME == name);

			if (result == null)
			{
				result = new WEB_POINT_TYPE {NAME = name};
				WEB_POINT_TYPE.AddObject(result);
			}

			return result;
		}
		public decimal? GetRuleValue(int vehicleId, RuleNumber ruleNumber)
		{
			//TODO: возвращать значение норматива из группы
			var value =
			(
				from vr in VEHICLE_RULE
				where
					vr.VEHICLE.VEHICLE_ID == vehicleId &&
					vr.RULE.NUMBER        == (int)ruleNumber
				select vr.VALUE
			);
			return value.Any() ? value.First() : (decimal?)null;
		}
		public MessageDestinationType? GetSmsDestinationType(string phone)
		{
			foreach (var country in Country.Where(c => c.PhonePrefix != null))
			{
				if (phone.StartsWith(country.PhonePrefix))
				{
					var smsDestinationType = country.SmsDestinationType;
					if (smsDestinationType == null)
						return null;
					return (MessageDestinationType)smsDestinationType.Value;
				}
			}
			return null;
		}
		public IQueryable<Billing_Service_Provider> GetBillingProviderByIP(string hostIP)
		{
			return Billing_Service_Provider.Where(
				bsp => bsp.Billing_Service_Provider_Host.Any(h => h.IP == hostIP));
		}
		public ObjectQuery<VEHICLE> GetVehicleQuery(params string[] includePaths)
		{
			ObjectQuery<VEHICLE> result = VEHICLE;
			if (includePaths != null && includePaths.Length != 0)
			{
				foreach (var includePath in includePaths)
					result = result.Include(includePath);
			}
			else
			{
				result = result
					.Include(EntityModel.VEHICLE.VEHICLE_KINDIncludePath)
					.Include(EntityModel.VEHICLE.CONTROLLERIncludePath)
					.Include(EntityModel.VEHICLE.CONTROLLERIncludePath + "." + EntityModel.CONTROLLER.CONTROLLER_TYPEIncludePath);
			}
			return result;
		}
		public ObjectQuery<CONTROLLER> GetControllerQuery(params string[] includePaths)
		{
			ObjectQuery<CONTROLLER> result = CONTROLLER;
			if (includePaths != null && includePaths.Length != 0)
			{
				foreach (var path in includePaths)
					result = result.Include(path);
			}
			else
			{
				result = result
					.Include(EntityModel.CONTROLLER.VEHICLEIncludePath)
					.Include(EntityModel.CONTROLLER.CONTROLLER_TYPEIncludePath);
			}
			return result;
		}
		public IQueryable<VEHICLE> GetVehicle(Asid asid, params string[] includePaths)
		{
			var result = GetVehicleQuery(includePaths);

			return (from v in result
				where v.CONTROLLER.MLP_Controller.Asid.ID == asid.ID
				select v);
		}
		public IQueryable<VEHICLE> GetVehicle(int operatorId, int vehicleId, SystemRight right, params string[] includePaths)
		{
			var result = GetVehicleQuery(includePaths);

			return result.Where(v =>
				v.VEHICLE_ID == vehicleId &&
				v.Accesses.Any(ovr =>
					ovr.operator_id == operatorId && ovr.right_id == (int) right));
		}
		/// <summary> Найти объект наблюдения по идентификатору контроллера (трекера) </summary>
		/// <param name="controllerId"> Идентификатор контроллера (трекера) </param>
		/// <returns></returns>
		public VEHICLE GetVehicleByControllerId(int controllerId)
		{
			var vehicleQry = GetVehicleQuery();
			var vehicleRes = vehicleQry
				.FirstOrDefault(v => v.CONTROLLER.CONTROLLER_ID == controllerId);
			if (null == vehicleRes)
				throw new ArgumentOutOfRangeException(nameof(controllerId), controllerId,
					@"No corresponding VEHICLE.CONTROLLER.CONTROLLER_ID in DB");
			return vehicleRes;
		}
		public CONTROLLER_TYPE GetControllerType(string controllerTypeName)
		{
			var controllerType = (from ct in CONTROLLER_TYPE
				where ct.TYPE_NAME == controllerTypeName
				select ct).FirstOrDefault();
			if (controllerType == null)
				throw new ArgumentOutOfRangeException(nameof(controllerTypeName), controllerTypeName,
					@"No corresponding CONTROLLER_TYPE.TYPE_NAME in DB");

			return controllerType;
		}
		public CONTROLLER_TYPE GetControllerType(int controllerTypeId)
		{
			var controllerType = (from ct in CONTROLLER_TYPE
				where ct.CONTROLLER_TYPE_ID == controllerTypeId
				select ct).FirstOrDefault();
			if (controllerType == null)
				throw new ArgumentOutOfRangeException(nameof(controllerTypeId), controllerTypeId,
					@"No corresponding CONTROLLER_TYPE.CONTROLLER_TYPE_ID in DB");

			return controllerType;
		}
		public IQueryable<Asid> GetAsidByVehicleIdQuery(int vehicleId)
		{
			return Asid
				.Where(a => a.MLP_Controller.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId);
		}
		/// <summary> Создать запрос получения групп друзей оператора operatorId, определяем по праву Friendship на группу </summary>
		/// <param name="operatorId"> Идентификатор оператора </param>
		/// <returns></returns>
		public IQueryable<OPERATORGROUP> FriendsGroupQuery(int operatorId)
		{
			return OPERATORGROUP
				.Where(g =>
					g.Accesses.Any(a =>
						a.Operator_ID == operatorId &&
						a.Right_ID    == (int)SystemRight.Friendship));
		}
		/// <summary> Создать запрос на получение всех пользователей департамента departmentId </summary>
		/// <param name="departmentId"> Идентификатор департамента </param>
		/// <returns></returns>
		public IQueryable<OPERATOR> GetDepartmentOperatorsQuery(int departmentId)
		{
			return OPERATORGROUP
				.Where(og =>
					// Группе назначено право DepartmentsAccess
					og.OPERATORGROUP_DEPARTMENT.Any(ogd =>
						ogd.DEPARTMENT_ID == departmentId                            &&
						ogd.RIGHT_ID      == (int)SystemRight.DepartmentsAccess      &&
						ogd.ALLOWED)
					&&
					// Группе не назначено право SecurityAdministration
					!og.OPERATORGROUP_DEPARTMENT.Any(ogd =>
						ogd.DEPARTMENT_ID == departmentId                            &&
						ogd.RIGHT_ID      == (int)SystemRight.SecurityAdministration &&
						ogd.ALLOWED))
				// Выбрать всех операторов отобранных групп с правом DepartmentsAccess и без права SecurityAdministration
				.SelectMany(og => og.Members)
				// Отобрать, только тех операторов у которых в представлении v_operator_department_right на департамент departmentId есть право DepartmentsAccess
				.Where(o =>
					v_operator_department_right.Any(odr =>
						odr.operator_id   == o.OPERATOR_ID                           &&
						odr.right_id      == (int)SystemRight.DepartmentsAccess      &&
						odr.Department_ID == departmentId));
		}
		/// <summary> Создать запрос на получение всех администраторов департамента departmentId </summary>
		/// <param name="departmentId"> Идентификатор департамента </param>
		public IQueryable<OPERATOR> GetDepartmentAdministratorsQuery(int departmentId)
		{
			return OPERATORGROUP
				.Where(og =>
					// Группе назначено право DepartmentsAccess
					og.OPERATORGROUP_DEPARTMENT.Any(ogd =>
						ogd.DEPARTMENT_ID == departmentId &&
						ogd.RIGHT_ID      == (int)SystemRight.DepartmentsAccess      &&
						ogd.ALLOWED)
					&&
					// Группе не назначено право SecurityAdministration
					!og.OPERATORGROUP_DEPARTMENT.Any(ogd =>
						ogd.DEPARTMENT_ID == departmentId &&
						ogd.RIGHT_ID      == (int)SystemRight.SecurityAdministration &&
						ogd.ALLOWED))
				// Выбрать всех операторов отобранных групп с правом DepartmentsAccess и без права SecurityAdministration
				.SelectMany(og => og.Members)
				// Отобрать, только тех операторов у которых в представлении v_operator_department_right на департамент departmentId есть право DepartmentsAccess и право SecurityAdministration
				.Where(o =>
					v_operator_department_right.Any(odr =>
						odr.operator_id   == o.OPERATOR_ID                           &&
						odr.right_id      == (int)SystemRight.DepartmentsAccess      &&
						odr.Department_ID == departmentId)
					&&
					v_operator_department_right.Any(odr =>
						odr.operator_id   == o.OPERATOR_ID                           &&
						odr.right_id      == (int)SystemRight.SecurityAdministration &&
						odr.Department_ID == departmentId));
		}
		public List<Contact> GetTechnicalSupportEmails()
		{
			return GetEmailsFromConstant(EntityModel.CONSTANTS.TechnicalSupportEmail);
		}
		public List<Contact> GetCalculatorEmails()
		{
			return GetEmailsFromConstant(EntityModel.CONSTANTS.CalculatorEmail);
		}
		private List<Contact> GetEmailsFromConstant(string constantName)
		{
			var emailConfig = GetConstant(constantName);

			if (emailConfig == null)
				return null;

			var emailAddresses = emailConfig.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries);

			return emailAddresses
				.Where(ContactHelper.IsValidEmail)
				.Select(email => GetContact(ContactType.Email, email))
				.ToList();
		}
		public BusinessLogic.DTO.Contact[] GetOperatorEmails(int operatorId)
		{
			return GetOperatorContacts(operatorId, ContactType.Email);
		}
		public Contact GetContact(int operatorId, ContactType type, string value)
		{
			var contact = Operator_Contact.Where(c =>
					c.Operator_ID == operatorId
					&& c.Contact.Type == (int) type
					&& c.Contact.Value == value
					&& c.Contact.Valid)
				.Select(c => c.Contact)
				.FirstOrDefault();
			return contact;
		}
		//TODO: Убрать этот метод
		public BusinessLogic.DTO.Contact[] GetOperatorContacts(int operatorId, ContactType type)
		{
			var confirmationDate = DateTime.UtcNow.AddMinutes(-GetConfirmationLifetimeMinutes());
			var result = Operator_Contact
				.Where(x =>
					x.OPERATOR.OPERATOR_ID == operatorId &&
					x.Contact.Type         == (int)type)
				.Select(
					oc => new BusinessLogic.DTO.Contact
					{
						Id                     = oc.Contact_ID,
						Value                  = oc.Value ?? oc.Contact.Value,
						Name                   = oc.Name,
						Comment                = oc.Description,
						IsConfirmed            = oc.Confirmed,
						IsConfirmationCodeSent =
							oc.ConfirmationSentDate.HasValue && oc.ConfirmationSentDate > confirmationDate,
						IsRemovable = oc.Removable
					}).ToArray();

			var confirmable = !result.Any(x => x.IsConfirmed ?? false);
			foreach (var item in result)
				item.IsConfirmable = confirmable;

			return result;
		}
		public BusinessLogic.DTO.Contact[] GetOperatorPhones(int operatorId)
		{
			return GetOperatorContacts(operatorId, ContactType.Phone);
		}
		public IQueryable<OPERATOR> GetAdmins(Asid asid)
		{
			return v_operator_vehicle_right
				.Where(
					a => a.VEHICLE.CONTROLLER.MLP_Controller.Asid.ID == asid.ID &&
						 a.right_id == (int) SystemRight.SecurityAdministration)
				.Select(a => a.OPERATOR);
		}
		public IQueryable<Asid> GetAdminAsids(Asid asid)
		{
			return GetAdmins(asid)
				.Where(o => o.Asid != null && o.Asid.Contact != null)
				.Select(o => o.Asid);
		}
		/// <summary> Получить контакт по указанному типу и значению. Если его нет, он будет создан в БД. </summary>
		/// <param name="type"> Тип </param>
		/// <param name="value"> Значение </param>
		/// <returns></returns>
		public Contact GetContact(ContactType type, string value)
		{
			var id = GetContactId(type, value);
			return Contact.First(c => c.ID == id);
		}
		/// <summary> Получить контакт номера телефона (MSISDN), из другого контакта </summary>
		/// <param name="contact"></param>
		/// <returns></returns>
		public Contact GetMsisdnContact(Contact contact)
		{
			var contactType = (ContactType)contact.Type;
			switch (contactType)
			{
				case ContactType.Phone:
					return contact;
			}
			throw new ArgumentOutOfRangeException(nameof(contact), contactType, contact.Value);
		}
		/// <summary> Получить идентификатор СИМ карты по идентификатору маскированного номера телефона F(MSISDN). </summary>
		/// <param name="asidId"></param>
		/// <returns></returns>
		public string GetSimIdByAsidId(int asidId)
		{
			return GetOrCreateSimIdByAsidId(asidId).FirstOrDefault();
		}
		/// <summary>
		/// Получить идентификатор контакта по указанному типу и значению.
		/// Если его нет, он будет создан в БД.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public int GetContactId(ContactType type, string value)
		{
			if (string.IsNullOrWhiteSpace(value))
				throw new ArgumentOutOfRangeException(nameof(value), value, @"Value cannot be null or whitespace");

			switch (type)
			{
				case ContactType.Phone:
					if (!ContactHelper.IsValidPhone(value))
						throw new ArgumentOutOfRangeException(nameof(value), value, @"Value is not valid " + type);
					break;
				case ContactType.Email:
					if (!ContactHelper.IsValidEmail(value))
						throw new ArgumentOutOfRangeException(nameof(value), value, @"Value is not valid " + type);
					break;
			}

			var contactId = GetContactID((int)type, value)
				.FirstOrDefault();
			if (contactId == null)
				throw new InvalidOperationException("Unable to get contact.ID from pair " + type + " / " + value);

			return contactId.Value;
		}
		/// <summary> Получить маскированный номер телефона F(MSISDN) по идентификатору СИМ карты </summary>
		/// <param name="simId"></param>
		/// <returns></returns>
		public Asid GetAsidBySimId(string simId)
		{
			if (string.IsNullOrWhiteSpace(simId))
				throw new ArgumentException(@"Value cannot be null or whitespace", "simId");

			return Asid.FirstOrDefault(a => a.SimID == simId);
		}
		/// <summary> Получить запрос для выборки сообщений для номера телефона (MSISDN). </summary>
		/// <param name="msisdn"></param>
		/// <returns></returns>
		public IQueryable<MESSAGE> MessageToMsisdn(string msisdn)
		{
			return MESSAGE
				.Where(m =>
					m.Contacts.Any(mc =>
						mc.Type          == (int)MessageContactType.Destination &&
						mc.Contact.Type  == (int)ContactType.Phone              &&
						mc.Contact.Value == msisdn));
		}
		/// <summary> Получить целочисленное поле сообщения </summary>
		/// <param name="messageId"></param>
		/// <param name="name"></param>
		/// <returns></returns>
		public int? GetIntegerFieldValue(int messageId, string name)
		{
			var field = MESSAGE_FIELD.FirstOrDefault(
				f => f.MESSAGE.MESSAGE_ID == messageId &&
					 f.MESSAGE_TEMPLATE_FIELD.NAME == name);
			if (field == null)
				return null;
			int result;
			if (!int.TryParse(field.CONTENT, out result))
				return null;
			return result;
		}

		/// <summary> Получить все контакты оператора в облачных службах </summary>
		/// <param name="operatorId"></param>
		/// <returns></returns>
		public List<Contact> GetCloudContactsForOperator(int operatorId)
		{
			var cloudContacts = new List<Contact>();
			// Контакты Android
			cloudContacts.AddRange(AppClient
				.Where(ac => ac.Operator_ID == operatorId)
				.Select(ac => ac.Contact)
				.Where(c =>
					(c.Type == (int)ContactType.Android) &&
					(c.Valid)                            &&
					(c.LockedUntil == null || c.LockedUntil < DateTime.UtcNow))
				.Distinct());
			// Контакты Apple
			cloudContacts.AddRange(AppClient
				.Where(ac => ac.Operator_ID == operatorId)
				.Select(ac => ac.Contact)
				.Where(c =>
					(c.Type == (int)ContactType.Apple) &&
					(c.Valid)                          &&
					(c.LockedUntil == null || c.LockedUntil < DateTime.UtcNow))
				.Distinct());
			// Контакты Apple для отладки
			cloudContacts.AddRange(AppClient
				.Where(ac => ac.Operator_ID == operatorId)
				.Select(ac => ac.Contact)
				.Where(c =>
					(c.Type == (int)ContactType.iOsDebug) &&
					(c.Valid)                             &&
					(c.LockedUntil == null || c.LockedUntil < DateTime.UtcNow))
				.Distinct());
			return cloudContacts;
		}

		/// <summary> Получить облачные контакты для объекта наблюдения </summary>
		/// <param name="vehicleId"></param>
		/// <returns></returns>
		public List<Contact> GetCloudContactsForVehicle(int vehicleId)
		{
			var cloudContacts = new List<Contact>();
			// Получить идентификатор оператора объекта наблюдения.
			var operatorId = GetOperatorByVehicleId(vehicleId)
				.Select(o => o.OPERATOR_ID)
				.FirstOrDefault();

			if (operatorId != default(int))
				cloudContacts.AddRange(GetCloudContactsForOperator(operatorId));

			return cloudContacts;
		}

		/// <summary> Получить первый подтвержденный номер телефона оператора </summary>
		/// <param name="operatorId"></param>
		/// <returns></returns>
		public string GetOwnMsisdn(int operatorId)
		{
			var ownContact = GetOwnPhoneContact(operatorId);
			return ownContact != null ? ownContact.Value : null;
		}

		/// <summary> Получить первый подтвержденный контакт оператора типа телефон. </summary>
		/// <param name="operatorId"></param>
		/// <returns></returns>
		public Contact GetOwnPhoneContact(int operatorId)
		{
			return Operator_Contact
				.Where(oc =>
					oc.Operator_ID  == operatorId && oc.Confirmed &&
					oc.Contact.Type == (int)ContactType.Phone)
				.Select(oc => oc.Contact)
				.FirstOrDefault();
		}

		/// <summary> Получить контакт оператора типа Asid </summary>
		/// <param name="operatorId"></param>
		/// <returns></returns>
		public Contact GetOwnAsidContact(int operatorId)
		{
			return Asid
				.Where(a => a.OPERATOR.OPERATOR_ID == operatorId)
				.Select(a => a.Contact)
				.FirstOrDefault();
		}
		/// <summary> Получить минимальный интервал, при превышении которого и отсутствии движения считать остановкой ТС </summary>
		/// <returns></returns>
		public TimeSpan GetStopPeriod()
		{
			int stopPeriod;
			var stopPeriodString = GetConstant(Constant.StopPeriod);
			if (!int.TryParse(stopPeriodString, out stopPeriod))
				stopPeriod = (int) FullLogOptions.DefaultStopPeriod.TotalSeconds;
			return TimeSpan.FromSeconds(stopPeriod);
		}
		/// <summary> Получить интервал времени жизни запроса на дружбу </summary>
		/// <returns></returns>
		public TimeSpan GetFriendRequestExpired()
		{
			int expired;
			var value = GetConstant(Constant.FriendRequestExpired);
			if (int.TryParse(value, out expired))
				return TimeSpan.FromSeconds(expired);
			return TimeSpan.FromMinutes(20);
		}
		/// <summary> Получить время жизни "запроса на подтверждение", до завершения по таймауту </summary>
		/// <returns> Время жизни, мин. </returns>
		public int GetConfirmationLifetimeMinutes()
		{
			return GetConstantAsInt(Constant.ConfirmationLifetimeMinutes) ?? 5;
		}
		public Dictionary<string, string> GetOperatorMsisdnToNameDictionary(int operatorId)
		{
			return Operator_Contact
				.Where(oc =>
					oc.Operator_ID  == operatorId &&
					oc.Contact.Type == (int)ContactType.Phone)
				.Select(oc => new { Msisdn = oc.Contact.Value, oc.Name })
				.ToList()
				.Where(oc => !string.IsNullOrWhiteSpace(oc.Name))
				.ToDictionary(c => c.Msisdn, c => c.Name);
		}
		/// <summary> Получить идентификатор оператора по номеру телефона (номер должен быть подтвержден) </summary>
		/// <param name="msisdn"></param>
		/// <returns></returns>
		public int? GetOperatorIdByConfirmedMsisdn(string msisdn)
		{
			var contactId = GetContactId(ContactType.Phone, msisdn);
			var ormOperator = Operator_Contact
				.Where(oc => oc.Contact_ID == contactId && oc.Confirmed)
				.Select(oc => new { ID = oc.Operator_ID })
				.FirstOrDefault();
			if (ormOperator == null)
				return null;
			return ormOperator.ID;
		}
		/// <summary> Получить телефон подтвержденный оператора </summary>
		/// <param name="operatorId"></param>
		/// <returns></returns>
		public Contact GetOperatorConfirmedPhone(int operatorId)
		{
			return Operator_Contact
				.Where(
					oc => oc.Confirmed            &&
					oc.Operator_ID  == operatorId &&
					oc.Contact.Type == (int)ContactType.Phone)
				.Select(oc => oc.Contact)
				.FirstOrDefault();
		}
		/// <summary> Получить язык и региональные параметры оператора </summary>
		/// <param name="operatorId"></param>
		/// <returns></returns>
		public CultureInfo GetOperatorCulture(int operatorId)
		{
			var cultureCode = OPERATOR
				.Where(o => o.OPERATOR_ID == operatorId)
				.Select(o => o.Culture.Code)
				.FirstOrDefault();
			if (cultureCode == null)
				return CultureInfo.InvariantCulture;
			return CultureInfo.GetCultureInfo(cultureCode);
		}
		/// <summary> Получить часовой пояс оператора </summary>
		/// <param name="operatorId"></param>
		/// <returns></returns>
		public TimeZoneInfo GetOperatorTimeZone(int operatorId)
		{
			var code = OPERATOR
				.Where(o => o.OPERATOR_ID == operatorId)
				.Select(o => o.TimeZone.Code)
				.FirstOrDefault();
			if (code == null)
				return TimeZoneInfo.Local;
			return TimeZoneInfo.FindSystemTimeZoneById(code);
		}
	}
}