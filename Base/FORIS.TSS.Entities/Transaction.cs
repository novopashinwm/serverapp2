﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Transactions;

namespace FORIS.TSS.EntityModel
{
    /// <summary>
    /// Обертка для <see cref="System.Transactions.TransactionScope"/>: позволяет включать/отключать
    /// использование <see cref="System.Transactions.TransactionScope"/>
    /// </summary>
    /// <remarks>Отключение транзакционности понадобилось из-за того, что сервер БД не принимал удаленные транзакции.</remarks>
    public class Transaction : IDisposable
    {
        private readonly TransactionScope _transactionScope;
        private bool _completed;

        public Transaction()
        {
            if (string.Equals(
                ConfigurationManager.AppSettings["TransactionEnabled"],
                "true",
                StringComparison.OrdinalIgnoreCase))
                _transactionScope = new TransactionScope();
            else
                _completed = false;
        }

        public void Dispose()
        {
            if (_transactionScope != null)
            {
                _transactionScope.Dispose();
            }
            else
            {
                if (!_completed)
                    Trace.TraceWarning("Transaction is not completed at {0}", Environment.StackTrace);
            }

        }

        public void Complete()
        {
            if (_transactionScope != null)
                _transactionScope.Complete();
            _completed = true;
        }
    }
}