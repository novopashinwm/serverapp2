﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.EntityModel
{
	public partial class Entities
	{
		public void SetConstant(Constant constant, string value)
		{
			var key = constant.ToString();
			var record = CONSTANTS.FirstOrDefault(c => c.NAME == key);
			if (record == null)
			{
				record = CONSTANTS.CreateObject();
				record.NAME = key;
				CONSTANTS.AddObject(record);
			}
			record.VALUE = value;
		}
		public string GetConstant(string name)
		{
			return CONSTANTS.Where(c => c.NAME == name).Select(c => c.VALUE).FirstOrDefault();
		}
		public void SetConstant(Constant name, int value)
		{
			SetConstant(name, value.ToString());
		}
		public int? GetConstantAsInt(string name)
		{
			var value = GetConstant(name);
			if (value == null)
				return null;
			int result;
			return int.TryParse(value, out result) ? result : (int?)null;
		}
		public HashSet<int> GetConstantAsIntHashSet(string name)
		{
			var storedString = GetConstant(name);
			var result = new HashSet<int>();

			if (string.IsNullOrWhiteSpace(storedString))
				return result;

			var strings = storedString.Split(';');
			foreach (var s in strings)
			{
				int i;
				if (int.TryParse(s, out i))
					result.Add(i);
			}

			return result;
		}
		public string GetConstant(Constant constant)
		{
			return GetConstant(constant.ToString());
		}
		public int? GetConstantAsInt(Constant constant)
		{
			return GetConstantAsInt(constant.ToString());
		}
		public void SetConstant(Constant constant, bool value)
		{
			SetConstant(constant, value.ToString());
		}
		public bool GetConstantAsBool(Constant constant)
		{
			return "true".Equals(GetConstant(constant), StringComparison.OrdinalIgnoreCase);
		}
		public TimeSpan? GetConstantAsTimeSpan(Constant constant)
		{
			var s = GetConstant(constant);
			if (string.IsNullOrWhiteSpace(s))
				return null;
			try
			{
				return XmlConvert.ToTimeSpan(s);
			}
			catch (FormatException)
			{
				return null;
			}
		}
		public void SetConstant(Constant constant, long value)
		{
			SetConstant(constant, value.ToString());
		}
		public long? GetConstantAsLong(Constant constant)
		{
			var value = GetConstant(constant);
			if (value == null)
				return null;
			long result;
			return long.TryParse(value, out result) ? result : (long?)null;
		}
	}
}