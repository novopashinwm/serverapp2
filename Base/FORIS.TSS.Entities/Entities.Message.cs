﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.EntityModel
{
	public partial class Entities
	{
		public int? GetMessageGatewayId(ContactType contactType, string contactValue, string messageTemplate)
		{
			var candidates = MessageGateway.Where(gw =>
				gw.ContactType_Id == (int) contactType &&
				gw.Enabled &&
				(!gw.MESSAGE_TEMPLATE.Any() || gw.MESSAGE_TEMPLATE.Any(t => t.NAME == messageTemplate)))
				.Select(gw => new
				{
					gw.ID,
					gw.ContactRegex
				})
				.ToList();

			foreach (var candidate in candidates)
			{
				if (!string.IsNullOrWhiteSpace(candidate.ContactRegex) &&
					!new Regex(candidate.ContactRegex).IsMatch(contactValue))
				{
					continue;
				}
				return candidate.ID;
			}
			return null;
		}

		public int? GetMessageGatewayId(MESSAGE message)
		{
			var destinationContact =
				message.Contacts.Where(mc => mc.Type == (int)MessageContactType.Destination)
					.Select(mc => new { mc.Contact.Type, mc.Contact.Value })
					.FirstOrDefault();
			if (destinationContact == null)
				return null;

			return GetMessageGatewayId(
				(ContactType)destinationContact.Type,
				destinationContact.Value,
				message.MESSAGE_TEMPLATE != null ? message.MESSAGE_TEMPLATE.NAME : null);
		}
		public MESSAGE_TEMPLATE GetMessageTemplate(string templateName, Culture culture = null)
		{
			var templateQuery = MESSAGE_TEMPLATE
				.Include("MESSAGE_TEMPLATE_FIELD")
				.Include("MESSAGE_TEMPLATE_FIELD.DOTNET_TYPE")
				.Where(mt => mt.NAME == templateName);
			MESSAGE_TEMPLATE messageTemplate = null;
			do
			{
				if (culture != null)
					messageTemplate = templateQuery.FirstOrDefault(mt => mt.Culture.Code == culture.Code);
				if (messageTemplate == null)
					messageTemplate = templateQuery.FirstOrDefault(mt => mt.Culture == null);

				if (messageTemplate == null)
				{
					using (var entities = new Entities())
					{
						var mt = entities.MESSAGE_TEMPLATE.CreateObject();
						entities.MESSAGE_TEMPLATE.AddObject(mt);
						mt.NAME    = templateName;
						mt.Culture = culture != null
							? entities.Culture.FirstOrDefault(ct => ct.Code == culture.Code)
							: null;
						entities.SaveChanges();
					}
				}
			} while (messageTemplate == null);

			return messageTemplate;
		}
	}
}