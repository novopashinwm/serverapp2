﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel.History;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace FORIS.TSS.EntityModel
{
	public partial class Entities
	{
		/// <summary> Возвращает запрос поиска оператора, аутентификация которого возможна с помощью заданного asid'а. </summary>
		public IQueryable<OPERATOR> GetOperatorsByAsidQuery(Asid ormAsid)
		{
			if (ormAsid == null)
				throw new ArgumentNullException(nameof(ormAsid));

			return
			(
				from a in Asid
				where
					a.ID       == ormAsid.ID &&
					a.OPERATOR != null
				select a.OPERATOR
			);
		}
		/// <summary> Возвращает оператора, у которых имеется подтвержденный телефон msisdn. </summary>
		/// <param name="msisdn"></param>
		/// <returns></returns>
		public OPERATOR GetOperatorByConfirmedMsisdn(string msisdn)
		{
			if (string.IsNullOrWhiteSpace(msisdn))
				throw new ArgumentException(@"Value cannot be null or empty or whitespace", "msisdn");

			return GetOperatorByConfirmedContact(ContactType.Phone, msisdn);
		}
		/// <summary> Возвращает оператора, у которых имеется подтвержденный контакт указанного типа </summary>
		public OPERATOR GetOperatorByConfirmedContact(ContactType dtoContactType, string value)
		{
			if (string.IsNullOrWhiteSpace(value))
				throw new ArgumentException(@"Value cannot be null or empty or whitespace", "value");

			return
			(
				from oc in Operator_Contact
				where
					oc.Contact.Value == value               &&
					oc.Contact.Type  == (int)dtoContactType &&
					oc.Confirmed
				select oc.OPERATOR
			)
				.FirstOrDefault();
		}
		public OPERATOR GetOperatorByConfirmedContact(Contact ormContact)
		{
			var dtoContactType = (ContactType)ormContact.Type;
			switch (dtoContactType)
			{
				case ContactType.Phone:
				case ContactType.Email:
					return GetOperatorByConfirmedContact(dtoContactType, ormContact.Value);
			}
			throw new NotImplementedException("contact type " + dtoContactType);
		}
		/// <summary> Возвращает запрос поиска asid'ов, с помощью которых может быть идентифицирован заданный оператор </summary>
		/// <param name="ormOperator"></param>
		/// <returns></returns>
		public IQueryable<Asid> GetAsidByOperatorQuery(OPERATOR ormOperator)
		{
			return (from o in OPERATOR
					where o.OPERATOR_ID == ormOperator.OPERATOR_ID
					select o.Asid);
		}
		/// <summary> Удаляет asid, Удаляем все его billing_service и Billing_blocking, а так же обнуляет поле asid_id в MLPController </summary>
		/// <param name="ormAsid"></param>
		/// <param name="ormOperator"> Оператор, совершающий действо. Может быть = null </param>
		public void DeleteAsid(Asid ormAsid, OPERATOR ormOperator)
		{
			DeleteAsid(ormAsid);

			SaveChangesByOperator(ormOperator != null ? ormOperator.OPERATOR_ID : (int?)null);
		}
		/// <summary> Возвращает запрос поиска машин, имеющих заданный asid, для которых у заданного оператора имеется указанное право. </summary>
		public IQueryable<VEHICLE> GetVehiclesByAsidQuery(int operatorId, SystemRight right, string asid)
		{
			var rightID = (int) right;
			return
			(
				from ovr in v_operator_vehicle_right
				from vehicle in VEHICLE
				where
					ovr.operator_id                                      == operatorId &&
					ovr.right_id                                         == rightID &&
					vehicle.VEHICLE_ID                                   == ovr.vehicle_id &&
					vehicle.CONTROLLER.MLP_Controller.Asid.Contact.Value == asid
				select vehicle
			);
		}
		public IQueryable<VEHICLE> GetVehiclesByAsidQuery(string asid)
		{
			return (from v in VEHICLE
					where v.CONTROLLER.MLP_Controller.Asid.Contact.Value == asid
					select v);
		}
		public VEHICLE_KIND GetSmartphoneVehicleKind()
		{
			return GetVehicleKindByName(nameof(VehicleKind.Smartphone));
		}
		public VEHICLE_KIND GetCellphoneVehicleKind()
		{
			const string cellphoneVehicleKindName = "мобильный телефон";
			return GetVehicleKindByName(cellphoneVehicleKindName);
		}
		public VEHICLE_KIND GetPersonalTrackerVehicleKind()
		{
			const string personalTrackerVehicleKindName = "трекер";
			return GetVehicleKindByName(personalTrackerVehicleKindName);
		}
		public VEHICLE_KIND GetVehicleKindByName(string name)
		{
			var vehicleKind =
			(
				from vk in VEHICLE_KIND
				where vk.NAME == name
				select vk
			)
				.FirstOrDefault();
			if (vehicleKind == null)
			{
				vehicleKind = EntityModel.VEHICLE_KIND.CreateVEHICLE_KIND(0, name);
				AddToVEHICLE_KIND(vehicleKind);
			}
			return vehicleKind;
		}
		public VEHICLE_KIND GetVehicleKind(VehicleKind vehicleKind)
		{
			var result = VEHICLE_KIND
				.FirstOrDefault(k => k.VEHICLE_KIND_ID == (int)vehicleKind);
			if (null == result)
				throw new ArgumentOutOfRangeException(nameof(vehicleKind), vehicleKind, @"No corresponding value in DB");
			return result;
		}
		public VEHICLE_STATUS GetDefaultVehicleStatus()
		{
			return (from vs in VEHICLE_STATUS
					where vs.VEHICLE_STATUS_NAME == "рабочее"
					select vs).First();
		}
		public CONTROLLER_TYPE GetMLPControllerType()
		{
			return (from ct in CONTROLLER_TYPE
					where ct.TYPE_NAME == "MLP"
					select ct).First();
		}
		public CONTROLLER_TYPE GetSoftTrackerControllerType()
		{
			return (from ct in CONTROLLER_TYPE
					where ct.TYPE_NAME == ControllerType.Names.SoftTracker
					select ct).First();
		}
		public VEHICLE GetVehicle(int vehicleId, params string[] includes)
		{
			var result = VEHICLE
				.Include(includes)
				.FirstOrDefault(v => v.VEHICLE_ID == vehicleId);
			if (null == result)
				throw new ArgumentOutOfRangeException(nameof(vehicleId), vehicleId, @"No corresponding value in DB");
			return result;
		}
		public VEHICLEGROUP GetVehicleGroup(int vehicleGroupId)
		{
			var result = VEHICLEGROUP
				.FirstOrDefault(g => g.VEHICLEGROUP_ID == vehicleGroupId);
			if (null == result)
				throw new ArgumentOutOfRangeException(nameof(vehicleGroupId), vehicleGroupId, @"No corresponding value in DB");
			return result;
		}
		public OPERATOR GetOperatorOrDefault(string login)
		{
			if (string.IsNullOrWhiteSpace(login))
				return default;
			return OPERATOR
				.FirstOrDefault(o => o.LOGIN == login);
		}
		public OPERATOR GetOperator(string login)
		{
			var result = GetOperatorOrDefault(login);
			if (null == result)
				throw new ArgumentOutOfRangeException(nameof(login), login, @"No corresponding value in DB");
			return result;
		}
		public OPERATOR GetOperator(int operatorId, params string[] includes)
		{
			var result = OPERATOR
				.Include(includes)
				.FirstOrDefault(o => o.OPERATOR_ID == operatorId);
			if (null == result)
				throw new ArgumentOutOfRangeException(nameof(operatorId), operatorId, @"No corresponding value in DB");
			return result;
		}
		/// <summary> Создать оператора (частный) по контакту, который подтвержден и является F(MSISDN) </summary>
		/// <param name="name"> Имя оператора </param>
		/// <param name="ormAsid"> Asid содержащий ссылку на демаскированный контакт </param>
		/// <param name="sendWelcomingSms"></param>
		/// <param name="addWelcomingService"></param>
		/// <param name="saveContext"></param>
		/// <returns></returns>
		public OPERATOR CreateOperatorByAsidWithDemaskedContact(string name, Asid ormAsid, bool sendWelcomingSms = true, bool addWelcomingService = true, SaveContext saveContext = null)
		{
			if (ormAsid.Contact != null && ormAsid.Contact.DemaskedPhone == null)
				throw new ArgumentException(@"Asid should be demasked for this operation", "asid");

			if (ormAsid.OPERATOR != null)
				throw new ArgumentException("Asid may not contain associated OPERATOR");

			OPERATOR ormOperator = null;

			if (ormAsid.Contact != null)
			{
				ormOperator = Operator_Contact
					.Where(oc =>
						oc.Contact_ID == ormAsid.Contact.Demasked_ID.Value &&
						oc.Confirmed)
					.Select(oc => oc.OPERATOR)
					.FirstOrDefault();
			}

			if (ormOperator != null && ormOperator.Asid != null && ormOperator.Asid.Contact_ID != null)
				throw new InvalidOperationException($"Phone {ormAsid.Contact.DemaskedPhone.Value} already has associated operator with asid");

			if (ormOperator == null)
			{
				ormOperator = CreateOperatorPhysical(name,
					ormAsid.Contact != null
						? ormAsid.Contact.DemaskedPhone.Value
						: string.Empty,
					ormAsid.Contact != null);
			}

			ormAsid.OPERATOR = ormOperator;

			if (ormAsid.Contact != null &&
				ormAsid.Contact.DemaskedPhone != null &&
				!Operator_Contact.Any(oc => oc.Contact_ID == ormAsid.Contact.Demasked_ID.Value && oc.Confirmed))
			{
				Operator_Contact.AddObject(new Operator_Contact
				{
					OPERATOR  = ormOperator,
					Contact   = ormAsid.Contact.DemaskedPhone,
					Confirmed = true,
					Removable = false
				});
				$"Create operator by asid contact '{ormAsid.Contact}'"
					.CallTraceInformation();
			}

			var ormVehicle = CreateVehicle(ormOperator, name, ormAsid, UserRole.DefaultOwnRightsOnVehicle, saveContext);
			// Отправляются приветственные SMS
			if (sendWelcomingSms && ormAsid.Contact != null)
				SendWelcomingSms(ormAsid);

			// Добавляются приветственные услуги
			if (addWelcomingService && ormAsid.Contact != null)
				AddWelcomingService(ormAsid);

			AddExistFriends(ormOperator, ormVehicle);

			return ormOperator;
		}
		/// <summary> Добавляем друзей - из тех пользователей, которым ранее был дан доступ на определение местоположения этого телефона </summary>
		/// <param name="ormOperator"></param>
		/// <param name="ormVehicle"></param>
		private void AddExistFriends(OPERATOR ormOperator, VEHICLE ormVehicle)
		{
			var ormFriends = OPERATOR_VEHICLE
				.Where(ov =>
					ov.VEHICLE.VEHICLE_ID == ormVehicle.VEHICLE_ID   &&
					ov.OPERATOR_ID        != ormOperator.OPERATOR_ID &&
					ov.RIGHT_ID           == (int)SystemRight.VehicleAccess)
				.Select(ov => ov.OPERATOR);

			foreach (var ormFriend in ormFriends)
				AddFriend(ormOperator.OPERATOR_ID, ormFriend.OPERATOR_ID);
		}
		/// <summary> Создает оператора (частный) по контакту </summary>
		public OPERATOR GetOrCreateOperatorByContact(Contact ormContact, SaveContext saveContext)
		{
			var ormOperator = GetOperatorByConfirmedContact(ormContact);
			if (ormOperator == null)
			{
				ormOperator = CreateOperatorPhysical(ormContact.Value, ormContact.Value);
				Operator_Contact.AddObject(new Operator_Contact
				{
					OPERATOR  = ormOperator,
					Contact   = ormContact,
					Confirmed = true,
					Removable = false,
				});
				$"Create operator by contact '{ormContact}'"
					.CallTraceInformation();

				// Для телефона дополнительно создаем объект наблюдения
				if (ContactType.Phone == (ContactType)ormContact.Type)
				{
					var ormVehicle = GetOrCreateVehicle(
						ormOperator,
						ormContact.Value,
						ormContact.Value,
						// Выдаем права по умолчанию для владельца объекта
						UserRole.DefaultOwnRightsOnVehicle,
						saveContext);

					if (!ormVehicle.CONTROLLER.MLP_ControllerReference.IsLoaded)
						ormVehicle.CONTROLLER.MLP_ControllerReference.Load();

					if (ormVehicle.CONTROLLER.MLP_Controller == null)
					{
						var asid = Asid.FirstOrDefault(a => a.Contact.DemaskedPhone.Value == ormContact.Value);
						bool asidCreated = false;
						if (asid == null)
						{
							asid        = new Asid { OPERATOR = ormOperator };
							asidCreated = true;
						}
						ormVehicle.CONTROLLER.MLP_Controller = new MLP_Controller { Asid = asid };
						asid.OPERATOR = ormOperator;

						// Удаляем административные права других пользователей на этот объект наблюдения
						var operatorVehicles = OPERATOR_VEHICLE
							.Where(ov =>
								ov.ALLOWED                                                &&
								ov.RIGHT_ID    == (int)SystemRight.SecurityAdministration &&
								ov.VEHICLE_ID  == ormVehicle.VEHICLE_ID                   &&
								ov.OPERATOR_ID != ormOperator.OPERATOR_ID)
							.ToList();
						foreach (var operatorVehicle in operatorVehicles)
							OPERATOR_VEHICLE.DeleteObject(operatorVehicle);
						if (operatorVehicles.Count != 0)
							SaveChanges(saveContext);

						if (asidCreated)
						{
							SaveChanges(saveContext);
							AddWelcomingService(asid);
						}
					}
					AddExistFriends(ormOperator, ormVehicle);
				}
			}
			// Возвращаем созданного оператора
			return ormOperator;
		}
		public OPERATOR CreateOperatorPhysical(string name, string login, bool assignLoginAndPassword = true)
		{
			// Получить префикс логина для нового оператора
			var newLoginPrefix = (GetConstant(EntityModel.CONSTANTS.NewLoginPrefix) ?? string.Empty);
			// Создаем объект ORM для оператора
			var ormOperator = new OPERATOR();
			// Заполняем имя
			ormOperator.NAME = name ?? string.Empty;
			// Заполняем логин и пароль
			if (assignLoginAndPassword)
			{
				ormOperator.LOGIN    = newLoginPrefix + login;
				ormOperator.PASSWORD = EntityModel.OPERATOR.GeneratePassword();
			}
			// Добавляем созданного оператора в таблицу
			OPERATOR.AddObject(ormOperator);
			// Создаем группу друзей оператора
			CreateFriendGroup(ormOperator);
			// Делаем промежуточное сохранение
			SaveChangesWithHistory();
			// Добавляем права (видимо должны быть права частного клиента)
			Allow(ormOperator, UserRole.UfinPrivUser.UserRights);
			// Если логин пустой, то создаем имя из префикса и идентификатора
			if (assignLoginAndPassword && string.IsNullOrEmpty(login))
			{
				SaveChangesWithHistory();
				ormOperator.LOGIN = newLoginPrefix +
					ormOperator.OPERATOR_ID.ToString(CultureInfo.InvariantCulture);
			}
			// Делаем окончательное сохранение
			SaveChangesWithHistory();
			// Возвращаем оператора
			return ormOperator;
		}
		public void SendWelcomingSms(Asid asid)
		{
			// Действий не нужно, но если понадобится можно добавить
		}
		public void AddWelcomingService(Asid asid)
		{
			// Действий не нужно, но если понадобится можно добавить
		}
		public VEHICLE GetOrCreateVehicle(OPERATOR ormOperator, string alias, string msisdn, SystemRight[] rights, SaveContext saveContext)
		{
			var vehicle = GetOrCreateVehicle(alias, msisdn, saveContext);
			Allow(ormOperator, vehicle, rights);
			SaveChanges(saveContext);
			return vehicle;
		}
		public VEHICLE CreateVehicle(OPERATOR ormOperator, string alias, Asid asid, SystemRight[] rights, SaveContext saveContext)
		{
			var vehicle = GetOrCreateVehicle(alias, asid);

			if (string.IsNullOrWhiteSpace(vehicle.GARAGE_NUMBER) && !string.IsNullOrWhiteSpace(alias))
				vehicle.GARAGE_NUMBER = alias;

			Allow(ormOperator, vehicle, rights);

			SaveChanges(ormOperator);

			return vehicle;
		}
		public VEHICLE GetOrCreateVehicle(string alias, Asid asid)
		{
			VEHICLE vehicle = null;

			if (asid != null)
				vehicle = VEHICLE.FirstOrDefault(v => v.CONTROLLER.MLP_Controller.Asid.ID == asid.ID);

			if (vehicle == null)
			{
				vehicle = CreateVehicle(alias, GetSmartphoneVehicleKind(), GetSoftTrackerControllerType());
				var controller = vehicle.CONTROLLER;
				if (asid != null)
				{
					var mlpController = EntityModel.MLP_Controller.CreateMLP_Controller(controller.CONTROLLER_ID);
					mlpController.Asid = asid;
					AddToMLP_Controller(mlpController);
					controller.MLP_Controller = mlpController;

					var msisdnId = asid.Contact != null && asid.Contact.DemaskedPhone != null
						? asid.Contact.DemaskedPhone.ID
						: (int?) null;
					if (msisdnId.HasValue)
					{
						var controllers = CONTROLLER.Where(
							c => c.VEHICLE.VEHICLE_ID != vehicle.VEHICLE_ID &&
								 c.PhoneContactID == msisdnId)
							.ToList();

						if (controllers.Count != 0)
						{
							foreach (var trackerController in controllers)
							{
								trackerController.PHONE = string.Empty;
								trackerController.PhoneContactID = null;
							}
						}
					}
				}

				vehicle.VEHICLE_STATUS = (from vs in VEHICLE_STATUS select vs).First();
				SaveChangesByOperator(null);
			}
			return vehicle;
		}
		public VEHICLE GetOrCreateVehicle(string alias, string msisdn, SaveContext saveContext)
		{
			VEHICLE vehicle = VEHICLE
				.FirstOrDefault(
					v => v.CONTROLLER.PHONE == msisdn ||
						 v.CONTROLLER.PhoneContact.Value == msisdn ||
						 v.CONTROLLER.MLP_Controller.Asid.Contact.DemaskedPhone.Value == msisdn);

			if (vehicle == null)
			{
				vehicle = CreateVehicle(alias, GetSmartphoneVehicleKind(), GetSoftTrackerControllerType());
				var contact = GetContact(ContactType.Phone, msisdn);
				vehicle.VEHICLE_STATUS = (from vs in VEHICLE_STATUS select vs).First();
				vehicle.CONTROLLER.PhoneContactID = contact.ID;
				vehicle.CONTROLLER.PHONE = contact.Value;

				SaveChanges(saveContext);
			}

			return vehicle;
		}
		public VEHICLE CreateVehicle(Tracker tracker, string alias)
		{
			if (!tracker.PatternVehicleReference.IsLoaded)
				tracker.PatternVehicleReference.Load();

			var patternVehicle = tracker.PatternVehicle;
			if (patternVehicle == null)
				throw new ArgumentException(@"tracker.PatternVehicle should not be null", "tracker");

			var vehicle = Copy(patternVehicle);

			vehicle.GARAGE_NUMBER = string.IsNullOrEmpty(alias) ? tracker.Number : alias;
			int parsedNumber;
			vehicle.CONTROLLER.NUMBER = int.TryParse(tracker.Number, out parsedNumber) ? parsedNumber : 0;
			vehicle.CONTROLLER.CONTROLLER_INFO.DEVICE_ID = Encoding.ASCII.GetBytes(tracker.Number);

			return vehicle;
		}
		public VEHICLE Copy(VEHICLE ormVehicleSrc)
		{
			if (!ormVehicleSrc.VEHICLE_KINDReference.IsLoaded)
				ormVehicleSrc.VEHICLE_KINDReference.Load();

			if (!ormVehicleSrc.VEHICLE_STATUSReference.IsLoaded)
				ormVehicleSrc.VEHICLE_STATUSReference.Load();

			if (!ormVehicleSrc.VEHICLE_STATUSReference.IsLoaded)
				ormVehicleSrc.VEHICLE_STATUSReference.Load();

			var ormVehicleDst = new VEHICLE
			{
				VALIDATOR_PRESENT = ormVehicleSrc.VALIDATOR_PRESENT,
				PUBLIC_NUMBER     = string.Empty,
				GUARD             = ormVehicleSrc.GUARD,
				TIME_PREPARATION  = ormVehicleSrc.TIME_PREPARATION,
				VEHICLE_KIND      = ormVehicleSrc.VEHICLE_KIND,
				VEHICLE_STATUS    = ormVehicleSrc.VEHICLE_STATUS,
				GRAPHIC_BEGIN     = null
			};
			AddToVEHICLE(ormVehicleDst);

			if (!ormVehicleSrc.CONTROLLERReference.IsLoaded)
				ormVehicleSrc.CONTROLLERReference.Load();

			if (!ormVehicleSrc.CONTROLLER.CONTROLLER_TYPEReference.IsLoaded)
				ormVehicleSrc.CONTROLLER.CONTROLLER_TYPEReference.Load();

			var ormControllerDst = new CONTROLLER
			{
				CONTROLLER_TYPE = ormVehicleSrc.CONTROLLER.CONTROLLER_TYPE,
				VEHICLE         = ormVehicleDst,
			};
			AddToCONTROLLER(ormControllerDst);

			if (!ormVehicleSrc.CONTROLLER.CONTROLLER_INFOReference.IsLoaded)
				ormVehicleSrc.CONTROLLER.CONTROLLER_INFOReference.Load();

			var ormControllerInfoSrc = ormVehicleSrc.CONTROLLER.CONTROLLER_INFO;
			if (ormControllerInfoSrc != null)
			{
				var ormControllerInfoDst = new CONTROLLER_INFO
				{
					CONTROLLER            = ormControllerDst,
					FUEL_FACTOR           = ormControllerInfoSrc.FUEL_FACTOR,
					FUEL_LVAL             = ormControllerInfoSrc.FUEL_LVAL,
					FUEL_UVAL             = ormControllerInfoSrc.FUEL_UVAL,
					FUEL_SENSOR_DIRECTION = ormControllerInfoSrc.FUEL_SENSOR_DIRECTION,
					LOGIN                 = ormControllerInfoSrc.LOGIN,
					PASSWORD              = ormControllerInfoSrc.PASSWORD,
					INTERVAL              = ormControllerInfoSrc.INTERVAL,
					FIRMWARE              = ormControllerInfoSrc.FIRMWARE,
					TIME                  = ormControllerInfoSrc.TIME
				};
				ormControllerDst.CONTROLLER_INFO = ormControllerInfoDst;
				AddToCONTROLLER_INFO(ormControllerInfoDst);
			}

			if (!ormVehicleSrc.CONTROLLER.CONTROLLER_SENSOR_MAP.IsLoaded)
				ormVehicleSrc.CONTROLLER.CONTROLLER_SENSOR_MAP.Load();

			foreach (var ormControllerSensorMapSrc in ormVehicleSrc.CONTROLLER.CONTROLLER_SENSOR_MAP)
			{
				if (!ormControllerSensorMapSrc.CONTROLLER_SENSORReference.IsLoaded)
					ormControllerSensorMapSrc.CONTROLLER_SENSORReference.Load();
				if (!ormControllerSensorMapSrc.CONTROLLER_SENSOR_LEGENDReference.IsLoaded)
					ormControllerSensorMapSrc.CONTROLLER_SENSOR_LEGENDReference.Load();
				var ormControllerSensorMapDst = new CONTROLLER_SENSOR_MAP
				{
					CONTROLLER_SENSOR        = ormControllerSensorMapSrc.CONTROLLER_SENSOR,
					CONTROLLER_SENSOR_LEGEND = ormControllerSensorMapSrc.CONTROLLER_SENSOR_LEGEND,
					DESCRIPTION              = ormControllerSensorMapSrc.DESCRIPTION,
					Multiplier               = ormControllerSensorMapSrc.Multiplier,
					Constant                 = ormControllerSensorMapSrc.Constant,
					Min_Value                = ormControllerSensorMapSrc.Min_Value,
					Max_Value                = ormControllerSensorMapSrc.Max_Value
				};
				ormVehicleDst.CONTROLLER.CONTROLLER_SENSOR_MAP.Add(ormControllerSensorMapDst);
				AddToCONTROLLER_SENSOR_MAP(ormControllerSensorMapDst);
			}
			return ormVehicleDst;
		}
		public VEHICLE CreateVehicle(string alias, VEHICLE_KIND vehicleKind, CONTROLLER_TYPE controllerType)
		{
			var vehicle = new VEHICLE
			{
				GARAGE_NUMBER = alias ?? string.Empty,
			};
			AddToVEHICLE(vehicle);

			vehicle.VEHICLE_KIND = vehicleKind;

			vehicle.VEHICLE_STATUS = GetDefaultVehicleStatus();

			var controller = new CONTROLLER();
			controller.CONTROLLER_TYPE = controllerType;
			controller.VEHICLE = vehicle;
			AddToCONTROLLER(controller);

			return vehicle;
		}
		/// <summary> Создает в системе нового оператора </summary>
		/// <param name="login"></param>
		/// <param name="password"></param>
		/// <param name="nickname"> Имя оператора </param>
		/// <param name="email"> Email для восстановления пароля </param>
		/// <param name="newOperator"> Созданный оператор </param>
		/// <returns></returns>
		public CreateOperatorResult CreateNewOperator(string login, string password, string nickname, string email, out OPERATOR newOperator)
		{
			newOperator = null;
			if (string.IsNullOrEmpty(password))
				return CreateOperatorResult.InvalidPassword;

			var existingOperatorWithSameLogin =
			(
				from o in OPERATOR
				where o.LOGIN == login
				select o
			)
				.FirstOrDefault();

			if (existingOperatorWithSameLogin != null)
				return CreateOperatorResult.LoginAlreadyExists;

			// Создаем оператора
			newOperator = new OPERATOR
			{
				LOGIN    = login,
				NAME     = nickname,
				PASSWORD = password
			};

			AddToOPERATOR(newOperator);
			SaveChangesByOperator(null);

			// Добавляем почту, если передали
			if (!string.IsNullOrEmpty(email))
			{
				var emailObject = new EMAIL
				{
					NAME     = nickname,
					EMAIL1   = email,
					COMMENT  = "Added during operator creation",
					OPERATOR = newOperator,
				};
				AddToEMAIL(emailObject);
			}

			// Добавляем системные права оператора
			foreach (var right in GetSystemRights(UserRole.UfinCorpUser.UserRights))
			{
				var rightOperator      = new RIGHT_OPERATOR();
				rightOperator.RIGHT    = right;
				rightOperator.OPERATOR = newOperator;
				rightOperator.ALLOWED  = true;
				AddToRIGHT_OPERATOR(rightOperator);
			}
			SaveChangesByOperator(newOperator.OPERATOR_ID);

			return CreateOperatorResult.Success;
		}
		/// <summary> Создает новый контроллер и добавляет приписывает его к указному vehicle </summary>
		/// <param name="operator"> Выполняющий действие оператор </param>
		/// <param name="vehicle"> Vehicle к которому назначать новый контроллер </param>
		/// <param name="asid"> Для MLP контроллеров </param>
		/// <param name="deviceID"> ID устройства для приема данных </param>
		/// <param name="deviceNumber"> Просто номер устройства </param>
		/// <param name="phone"> Телефон </param>
		/// <param name="controllerTypeName"> Название типа контроллера </param>
		/// <returns> Созданный объект контроллера </returns>
		public CONTROLLER CreateController(OPERATOR @operator, VEHICLE vehicle, Asid asid, string deviceID, int deviceNumber, string phone, string controllerTypeName)
		{
			if (string.IsNullOrEmpty(phone))
				throw new ArgumentOutOfRangeException("phone", @"phone is required field for controller");

			Attach(@operator);
			Attach(vehicle);
			if (asid != null) Attach(asid);

			var controller = new CONTROLLER
			{
				NUMBER = deviceNumber,
				PHONE = phone
			};
			if (vehicle != null) controller.VEHICLE = vehicle;
			controller.CONTROLLER_TYPE = GetControllerType(controllerTypeName);
			AddToCONTROLLER(controller);

			if (asid != null)
			{
				var mlpController = EntityModel.MLP_Controller.CreateMLP_Controller(controller.CONTROLLER_ID);
				mlpController.Asid = asid;
				AddToMLP_Controller(mlpController);
				controller.MLP_Controller = mlpController;
			}

			if (!string.IsNullOrEmpty(deviceID))
			{
				var controllerInfo = new CONTROLLER_INFO
				{
					CONTROLLER_ID = controller.CONTROLLER_ID,
					FUEL_SENSOR_DIRECTION = true,
					TIME = DateTime.UtcNow
				};
				controllerInfo.DEVICE_ID = Encoding.ASCII.GetBytes(deviceID);
				AddToCONTROLLER_INFO(controllerInfo);
				controllerInfo.CONTROLLER = controller;
			}

			SaveChangesByOperator(@operator.OPERATOR_ID);
			return controller;
		}
		/// <summary> Создает новый объект мониторинга </summary>
		/// <param name="operator"> Создающий оператор </param>
		/// <param name="garageNumber"> Название объекта </param>
		/// <param name="publicNumber"> Гос. номер </param>
		/// <param name="vehicleKind"> Тип устройства </param>
		/// <param name="rights"> Массив прав оператора на этот объект </param>
		/// <param name="saveContext"> Контекст, к которому привязываются изменения пользователя </param>
		/// <returns> Новый объект наблюдения </returns>
		public VEHICLE CreateVehicle(OPERATOR @operator, string garageNumber, string publicNumber, VehicleKind vehicleKind, SystemRight[] rights, SaveContext saveContext)
		{
			var vehicle = new VEHICLE
			{
				GARAGE_NUMBER = garageNumber ?? string.Empty,
				PUBLIC_NUMBER = publicNumber ?? string.Empty
			};
			AddToVEHICLE(vehicle);

			vehicle.VEHICLE_KIND = GetVehicleKind(vehicleKind);

			vehicle.VEHICLE_STATUS = GetDefaultVehicleStatus();

			foreach (var systemRight in rights)
			{
				var operatorVehicle = new OPERATOR_VEHICLE();
				AddToOPERATOR_VEHICLE(operatorVehicle);

				operatorVehicle.OPERATOR = @operator;
				operatorVehicle.VEHICLE = vehicle;
				operatorVehicle.RIGHT = GetSystemRight(systemRight);
			}

			SaveChanges(saveContext);

			return vehicle;
		}
		public Asid CreateAsid(string asidValue, string contractNumber, long? terminalDeviceNumner)
		{
			var asidRecord = new Asid
			{
				AllowMlpRequest             = true,
				AllowSecurityAdministration = true,
				WarnAboutLocation           = false,
				Terminal_Device_Number      = terminalDeviceNumner
			};
			AddToAsid(asidRecord);

			return asidRecord;
		}
		/// <summary> Возвращает запись, соответствующую заданному значению <see cref="Asid"/>.Value </summary>
		/// <remarks> Если для переданного значения <see cref="Asid"/>.Value в БД не существует записи, возвращается Null. </remarks>
		public Asid FindAsidByValue(string value)
		{

			var asidRecord = (from a in Asid
							  where a.Contact.Value == value
							  select a).FirstOrDefault();

			return asidRecord;
		}
		/// <summary> Возвращает запись, соответствующую заданному значению <see cref="Asid"/>.Value </summary>
		/// <remarks> Если для переданного значения <see cref="Asid"/>.Value в БД не существует записи, возвращается Null. </remarks>
		public Asid FindAsidByTerminalDeviceNumber(long terminalDeviceNumber)
		{
			var asidRecord = (from a in Asid
							  where a.Terminal_Device_Number == terminalDeviceNumber
							  select a).FirstOrDefault();

			return asidRecord;
		}
		public IQueryable<VEHICLE> GetVehicleByNicknameAndRight(int operatorID, string nickname, SystemRight right)
		{
			var rightID = (int)right;
			return (from ovr in v_operator_vehicle_right
					from vehicle in VEHICLE
					where ovr.operator_id == operatorID &&
						  ovr.right_id == rightID &&
						  vehicle.VEHICLE_ID == ovr.vehicle_id &&
						  vehicle.GARAGE_NUMBER == nickname
					select vehicle);
		}
		public IQueryable<VEHICLE> GetVehiclesByRight(int operatorID, SystemRight right)
		{
			var rightID = (int)right;
			return (from ovr in v_operator_vehicle_right
					from vehicle in VEHICLE
					where ovr.operator_id == operatorID &&
						  ovr.right_id == rightID &&
						  ovr.vehicle_id == vehicle.VEHICLE_ID
					select vehicle);
		}
		/// <summary> Возвращает объект по id и типу id для оператора; на объект должно быть доступно право <see cref="SystemRight.VehicleAccess"/> </summary>
		/// <param name="operatorId"> Идентификатор оператор </param>
		/// <param name="idType"> Тип идентификатора объекта </param>
		/// <param name="id"> Идентификатор объекта </param>
		/// <exception cref="NotSupportedException"> Бросается, если idType не равен ни <see cref="IdType.Vehicle"/>, ни <see cref="IdType.VehicleGroup"/> </exception>
		public object GetEntity(int operatorId, IdType idType, int id)
		{
			const int vehicleAccess = (int)SystemRight.VehicleAccess;
			switch (idType)
			{
				case IdType.Vehicle:
					return (from ovr in v_operator_vehicle_right
							from vehicle in VEHICLE
							where ovr.right_id == vehicleAccess &&
								  ovr.operator_id == operatorId &&
								  ovr.vehicle_id == id &&
								  vehicle.VEHICLE_ID == ovr.vehicle_id
							select vehicle).FirstOrDefault();
				case IdType.VehicleGroup:
					return (from ovgr in v_operator_vehicle_groups_right
							from vg in VEHICLEGROUP
							where ovgr.right_id == vehicleAccess &&
								  ovgr.operator_id == operatorId &&
								  ovgr.vehiclegroup_id == id &&
								  vg.VEHICLEGROUP_ID == ovgr.vehiclegroup_id
							select vg).FirstOrDefault();
				default:
					throw new NotSupportedException("IdType not supported: " + idType);
			}
		}
		/// <summary> Возвращает машину, представляющую собой оператора </summary>
		/// <param name="operatorId"></param>
		/// <returns></returns>
		public VEHICLE GetOperatorPersonalVehicle(int operatorId)
		{
			const int securityAdministrationRightID = (int)SystemRight.SecurityAdministration;

			var operPersonalVehicle =
			(
				from ovr in v_operator_vehicle_right
				from vehicle in VEHICLE
				where
					ovr.operator_id == operatorId &&
					ovr.right_id == securityAdministrationRightID &&
					ovr.vehicle_id == vehicle.VEHICLE_ID &&
					(
						vehicle.CONTROLLER.CONTROLLER_TYPE.TYPE_NAME                == "Sygic" ||
						vehicle.CONTROLLER.MLP_Controller.Asid.OPERATOR.OPERATOR_ID == operatorId
					)
				select vehicle
			)
				.FirstOrDefault();

			if (operPersonalVehicle != null)
			{
				if (!operPersonalVehicle.CONTROLLERReference.IsLoaded)
					operPersonalVehicle.CONTROLLERReference.Load();
				if (operPersonalVehicle.CONTROLLER != null)
				{
					if (!operPersonalVehicle.CONTROLLER.MLP_ControllerReference.IsLoaded)
						operPersonalVehicle.CONTROLLER.MLP_ControllerReference.Load();

					if (operPersonalVehicle.CONTROLLER.MLP_Controller != null &&
						!operPersonalVehicle.CONTROLLER.MLP_Controller.AsidReference.IsLoaded)
						operPersonalVehicle.CONTROLLER.MLP_Controller.AsidReference.Load();
				}
			}

			return operPersonalVehicle;
		}
		public OPERATOR GetOwner(VEHICLE vehicle)
		{
			var oper = GetVehicleOwnersQuery(vehicle.VEHICLE_ID).FirstOrDefault();
			//для MLP объектов ищем владельца по оператору asid-а
			if (oper == null)
			{
				oper = (from v in VEHICLE
						where v.VEHICLE_ID == vehicle.VEHICLE_ID
						select v.CONTROLLER.MLP_Controller.Asid.OPERATOR).FirstOrDefault();
			}

			return oper;

		}
		public IQueryable<OPERATOR> GetVehicleOwnersQuery(int vehicleId)
		{
			const int rightID = (int)SystemRight.SecurityAdministration;

			return
			(
				from ov in v_operator_vehicle_right
				from op in OPERATOR
				where
					ov.right_id    == rightID &&
					ov.vehicle_id  == vehicleId &&
					ov.operator_id == op.OPERATOR_ID
				select op
			);
		}
		/// <summary> Является ли vehicle MLP контроллером </summary>
		/// <param name="vehicle"></param>
		/// <returns></returns>
		/// <remarks> Оставлен для примера использования связки CONTROLLER, MLP_Controller, Asid </remarks>
		public bool IsVehicleMLP(VEHICLE vehicle)
		{
			if (!vehicle.CONTROLLERReference.IsLoaded || vehicle.CONTROLLER == null
				|| !vehicle.CONTROLLER.MLP_ControllerReference.IsLoaded
				|| vehicle.CONTROLLER.MLP_Controller == null || !vehicle.CONTROLLER.MLP_Controller.AsidReference.IsLoaded
				|| !vehicle.CONTROLLER.CONTROLLER_TYPEReference.IsLoaded)
			{
				return (from v in VEHICLE
						where v.VEHICLE_ID == vehicle.VEHICLE_ID
							  && v.CONTROLLER.MLP_Controller.Asid.ID > 0
							  && v.CONTROLLER.CONTROLLER_TYPE.TYPE_NAME == "MLP"
						select v).Any();
			}

			return vehicle.CONTROLLER != null
				   && vehicle.CONTROLLER.MLP_Controller != null
				   && vehicle.CONTROLLER.MLP_Controller.Asid != null
				   && vehicle.CONTROLLER.CONTROLLER_TYPE.TYPE_NAME == "MLP";
		}
		public IQueryable<DEPARTMENT> GetDepartmentByContractNumberQuery(string contractNumber)
		{
			return (from d in DEPARTMENT where d.ExtID == contractNumber select d);
		}
		public DEPARTMENT GetDepartment(int departmentId)
		{
			return DEPARTMENT
				.FirstOrDefault(d => d.DEPARTMENT_ID == departmentId);
		}
		/// <summary> Возвращает запрос для получения списка прав оператора на объект наблюдения </summary>
		/// <param name="ormOperator"> Оператор </param>
		/// <param name="ormVehicle"> Объект наблюдения </param>
		public IQueryable<SystemRight> GetVehicleRightsQuery(OPERATOR ormOperator, VEHICLE ormVehicle)
		{
			if (ormOperator == null)
				throw new ArgumentNullException(nameof(ormOperator));
			if (ormVehicle == null)
				throw new ArgumentNullException(nameof(ormVehicle));

			return
			(
				from ovr in v_operator_vehicle_right
				where
					ovr.operator_id == ormOperator.OPERATOR_ID &&
					ovr.vehicle_id  == ormVehicle.VEHICLE_ID
				select (SystemRight)ovr.right_id
			);
		}
		private static TimeSpan? _emailConfirmationLifeTime;
		private static TimeSpan EmailConfirmationLifeTime
		{
			get
			{
				if (!_emailConfirmationLifeTime.HasValue)
				{
					var configString = ConfigurationManager.AppSettings["EmailConfirmationLifeTime"];
					try
					{
						_emailConfirmationLifeTime = XmlConvert.ToTimeSpan(configString);
					}
					catch (ThreadAbortException)
					{
						throw;
					}
					catch (Exception exception)
					{
						Trace.TraceWarning("Unable to parse EmailConfirmationLifeTime {0}, exception: {1}", configString, exception);
						_emailConfirmationLifeTime = TimeSpan.FromMinutes(10);
					}
				}
				return _emailConfirmationLifeTime.Value;
			}
		}
		public IQueryable<Contact> GetEmailByConfirmationKey(string confirmationKey)
		{
			var minAllowedDateTime = DateTime.UtcNow.Subtract(EmailConfirmationLifeTime);

			return MESSAGE
				.Where(m =>
					m.MESSAGE_TEMPLATE.NAME == Msg::MessageTemplate.GenericConfirmation &&
					m.TIME >= minAllowedDateTime &&
					m.MESSAGE_FIELD.Any(f =>
						f.CONTENT == confirmationKey &&
						f.MESSAGE_TEMPLATE_FIELD.NAME == EntityModel.MESSAGE_TEMPLATE_FIELD.ConfirmationCode))
				.SelectMany(m =>
					m.Contacts
						.Where(mc =>
							mc.Type == (int)MessageContactType.Destination &&
							mc.Contact.Type == (int)ContactType.Email)
						.Select(mc => mc.Contact));
		}
		public TimeZone GetDefaultTimeZone()
		{
			var defaultTimeZoneId = TimeZoneInfo.Utc.Id;
			return TimeZone.First(item => item.Code == defaultTimeZoneId);
		}
		/// <summary> Определяет оператора по asid или создает нового </summary>
		/// <remarks> Необходимо для получения доступа по SimId </remarks>
		public OPERATOR GetOrCreateOperatorByAsid(Asid ormAsid, string defaultAlias)
		{
			var result = GetOperatorsByAsidQuery(ormAsid).FirstOrDefault();
			if (result != null)
				return result;

			if (ormAsid.Billing_Service.IsLoaded)
				ormAsid.Billing_Service.Load();
			var allowLogin = ormAsid.Billing_Service.All(bs => bs.Type.AllowLoginOperator);
			if (!allowLogin)
				throw new InvalidOperationException(@"Asid has Ufin department");

			result = CreateOperatorByAsidWithDemaskedContact(defaultAlias, ormAsid);

			return result;
		}
		public bool SaveUnsavedChanges()
		{
			if (!ObjectStateManager.GetObjectStateEntries(
				EntityState.Added | EntityState.Deleted | EntityState.Modified).Any())
				return false;

			SaveChangesByOperator(null);
			return true;
		}
		public void ConnectServiceAppToController(CONTROLLER controller, Asid serviceApp)
		{
			if (!controller.MLP_ControllerReference.IsLoaded)
				controller.MLP_ControllerReference.Load();

			if (controller.MLP_Controller != null)
			{
				if (serviceApp.MLP_Controller != null)
				{
					MLP_Controller.DeleteObject(serviceApp.MLP_Controller);

					//EF не может обойти ошибку:
					//Cannot insert duplicate key row in object 'dbo.MLP_Controller' with unique index 'UQ_MLP_Controller_Asid_ID'.
					SaveChanges();
				}
				controller.MLP_Controller.Asid = serviceApp;
			}
			else
			{
				if (serviceApp.MLP_Controller != null)
					serviceApp.MLP_Controller.CONTROLLER = controller;
				else
					MLP_Controller.AddObject(new MLP_Controller
					{
						Asid = serviceApp,
						CONTROLLER = controller
					});
			}
		}
		public void ChangeDepartmentType(DEPARTMENT department, DepartmentType departmentType)
		{
			department.Type = (int)departmentType;

			foreach (var operatorGroup in GetDepartmentOperatorGroups(department))
			{
				switch (departmentType)
				{
					case DepartmentType.Corporate:
						Allow(operatorGroup, department, new[] { SystemRight.PayForCaller });
						break;
					case DepartmentType.Physical:
						DeleteAccess(operatorGroup, department, SystemRight.PayForCaller);
						break;
				}

			}
		}
		private void UpdateVehicleGarageNumberByAsidContact(Contact asidContact, Contact demaskedContact)
		{
			var vehicles = (
				from c in Contact
				where c.ID == asidContact.ID
				select c.Asid into asids
				from a in asids
				join v in VEHICLE on a equals v.CONTROLLER.MLP_Controller.Asid
				where string.IsNullOrEmpty(v.GARAGE_NUMBER)
				select v
			).ToArray();

			if (vehicles.Length <= 0)
				return;

			foreach (var vehicle in vehicles)
			{
				vehicle.GARAGE_NUMBER = demaskedContact.Value;
			}
		}
		/// <summary> Заменяет контакт oldValue типа type на newValue </summary>
		/// <param name="type"> Тип контакта </param>
		/// <param name="oldValue"> Старое значение контакта </param>
		/// <param name="newValue"> Новое значение контакта </param>
		public void ReplaceContact(ContactType type, string oldValue, string newValue)
		{
			if (null == oldValue)
				throw new ArgumentNullException("oldValue");

			if (null == newValue)
				throw new ArgumentNullException("newValue");

			if (ContactType.Apple == type || ContactType.Android == type)
				throw new ArgumentOutOfRangeException("type", type, @"Replacing is not supported");

			if (oldValue.Equals(newValue, StringComparison.OrdinalIgnoreCase))
				return;

			var newContact = GetContact(type, newValue);
			var oldContact = GetContact(type, oldValue);

			//Решение: взять старый и новый контакт, удалить старые связи нового контакта, если есть; а затем все связи нового контакта перевести на старый
			//Обоснование: связи являются историческими, поэтому восстановить старые значения будет возможно,
			//а Contact не является исторической сущностью поэтому с восстановлением будут сложности
			//Если же просто заменить Contact.Value со старого значения на новый,
			//эта операция всё равно потребует удаления связей от нового контакта


			switch ((ContactType) oldContact.Type)
			{
				case ContactType.Phone:
					if (!oldContact.MaskedAsids.IsLoaded)
						oldContact.MaskedAsids.Load();
					if (!newContact.MaskedAsids.IsLoaded)
						newContact.MaskedAsids.Load();

					if (1 < oldContact.MaskedAsids.Count)
						throw new ArgumentException(@"Contact cannot has more than one masked phone", "oldValue");
					if (1 < newContact.MaskedAsids.Count)
						throw new ArgumentException(@"Contact cannot has more than one masked phone", "newValue");

					var oldMaskedContact = oldContact.MaskedAsids.FirstOrDefault();
					var newMaskedContact = newContact.MaskedAsids.FirstOrDefault();

					if (oldMaskedContact != null)
					{
						if (newMaskedContact == null)
							throw new ArgumentException(@"Old contact has masked value but new one does not", "newValue");

						ReplaceContact(oldMaskedContact, newMaskedContact);
					}
					else if (newMaskedContact != null)
						throw new ArgumentException(@"Old contact has not masked value but new one does", "newValue");
					break;
			}

			ReplaceContact(oldContact, newContact);
		}
		private void ReplaceContact(Contact oldContact, Contact newContact)
		{
			RemoveContactLinks(newContact);

			UpdateLink(oldContact.Asid, x => x.Contact = newContact);
			UpdateLink(newContact.ControllersWithPhone, x => x.PhoneContact = newContact);
			UpdateLink(newContact.CompoundRule_Operator_Contact, x => x.Contact = newContact);
			UpdateLink(newContact.Messages, x => x.Contact = newContact);
			UpdateLink(newContact.Operators, x => x.Contact = newContact);
		}
		/// <summary> Удаляет ссылки на данный контакт из других сущностей </summary>
		/// <param name="newContact"></param>
		public void RemoveContactLinks(Contact newContact)
		{
			ClearCollection(newContact.Asid);
			ClearCollection(newContact.ControllersWithPhone);
			ClearCollection(newContact.CompoundRule_Operator_Contact);
			ClearCollection(newContact.Messages);
			ClearCollection(newContact.Operators);
		}
		private void UpdateLink<T>(EntityCollection<T> collection, Action<T> update) where T : class
		{
			if (!collection.IsLoaded)
				collection.Load();
			foreach (var item in collection.ToArray())
				update(item);
		}
		public bool IsUserVehicle(int vehicleId)
		{
			return Asid.Any(a =>
				a.MLP_Controller.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId &&
				a.OPERATOR                                     != null      &&
				a.OPERATOR.LOGIN                               != null);
		}
		public bool IsCorporateCustomer(int operatorId)
		{
			return v_operator_department_right
				.Any(odr =>
					odr.operator_id == operatorId                         &&
					odr.right_id    == (int)SystemRight.DepartmentsAccess &&
					(
						odr.Department.Type == null ||
						odr.Department.Type == (int)DepartmentType.Corporate
					));
		}
		public bool IsPositionRequestVisible(int senderID, int targetID)
		{
			//скрывать PUSH'и SMS от "босса" - администратора того же корпоративного контракта
			//скрывать запросы родителей к детям
			//скрыть запрос, если неизвестен номер телефона
			//т.е. скрывать все запросы от владельцев
			var msisdn = GetOwnMsisdn(senderID);
			return !string.IsNullOrEmpty(msisdn) &&
				!v_operator_vehicle_right.Any(
					ovr =>
						ovr.operator_id == senderID &&
						ovr.vehicle_id == targetID &&
						ovr.right_id == (int) SystemRight.SecurityAdministration);
		}
		public void DisconnectServiceApp(VEHICLE vehicle)
		{
			var serviceApp =
				Asid
				.Include(EntityModel.Asid.MLP_ControllerIncludePath)
				.FirstOrDefault(
					a => a.MLP_Controller.CONTROLLER.VEHICLE.VEHICLE_ID == vehicle.VEHICLE_ID);

			if (serviceApp == null || serviceApp.Contact_ID == null)
				return;

			MLP_Controller.DeleteObject(serviceApp.MLP_Controller);
		}
		/// <returns> Возвращает true, если приложение обслуживания не существует или успешно привязано </returns>
		public TryConnectServiceAppResult TryConnectServiceApp(VEHICLE vehicle, int contactId)
		{
			if (Asid.Any(
				a => a.Contact.Demasked_ID == contactId &&
					 a.MLP_Controller.CONTROLLER.VEHICLE.VEHICLE_ID == vehicle.VEHICLE_ID))
			{
				// Приложение обслуживания уже привязано к этому объекту наблюдения, ничего делать не нужно
				return TryConnectServiceAppResult.Success;
			}

			// Если приложение обслуживания уже подключено, то не нужно привязывать ПО

			// Проверяем, можно ли привязать не активированную услугу с приложения обслуживания по указанному номеру
			var serviceApp =
				Asid.Where(a => a.Contact.Demasked_ID == contactId)
					.Select(a => new {a.ID, a.Contact_ID})
					.FirstOrDefault();

			if (serviceApp == null)
				return TryConnectServiceAppResult.ServiceAppDoesNotExist;

			// Проверяем, есть ли услуга
			if (!Billing_Service.Any(bs =>
				bs.Asid.ID == serviceApp.ID &&
				(bs.Type.Service_Type_Category == ServiceTypeCategory.GNSS.ToString() ||
				 bs.Type.Service_Type_Category == ServiceTypeCategory.SoftTracker.ToString()) &&
					// Услуга должна быть платной
					   bs.Type.MinPrice > 0))
				return TryConnectServiceAppResult.NoService;

			var mlpController =
				MLP_Controller
					.Where(mc => mc.Asid.ID == serviceApp.ID)
					.Select(mc => new {mc.CONTROLLER.CONTROLLER_ID})
					.FirstOrDefault();

			if (mlpController != null)
			{
				//Есть что привязать, то привязываем, но вначале проверяем, активная ли услуга
				var serviceAppVehicleID = VEHICLE
					.Where(v => v.CONTROLLER.CONTROLLER_ID == mlpController.CONTROLLER_ID)
					.Select(v => new {Value = v.VEHICLE_ID})
					.FirstOrDefault();

				// Услуга уже использовалась - получены данные от устройства - без запроса в поддержку не обойтись
				if (serviceAppVehicleID != null && Log_Time.Any(l => l.Vehicle_ID == serviceAppVehicleID.Value))
					return TryConnectServiceAppResult.ServiceIsAlreadyUsed;

				// Услуга уже использовалась - есть входящие SMS от устройства - без запроса в поддержку не обойтись
				if (Message_Contact.Any(mc =>
					mc.Type       == (int)MessageContactType.Source &&
					mc.Contact_ID == serviceApp.Contact_ID))
					return TryConnectServiceAppResult.ServiceIsAlreadyUsed;

				// Последняя проверка - есть ли сессия по номеру телефона

				var serviceAppOperatorId = OPERATOR
					.Where(o => o.Asid.ID == serviceApp.ID)
					.Select(o => o.OPERATOR_ID)
					.FirstOrDefault();

				if (serviceAppOperatorId != default(int))
				{
					using (var historical = new HistoricalEntities())
					{
						if (historical.SESSION.Any(s => s.OPERATOR_ID == serviceAppOperatorId))
							return TryConnectServiceAppResult.ServiceIsAlreadyUsed;
					}
				}
			}

			var serviceAppAsid = Asid
				.Include(EntityModel.Asid.MLP_ControllerIncludePath)
				.Include(EntityModel.Asid.MLP_ControllerIncludePath + "." + EntityModel.MLP_Controller.CONTROLLERIncludePath)
				.Include(EntityModel.Asid.MLP_ControllerIncludePath + "." + EntityModel.MLP_Controller.CONTROLLERIncludePath + "." + EntityModel.CONTROLLER.VEHICLEIncludePath)
				.First(a => a.ID == serviceApp.ID);

			ConnectServiceAppToController(vehicle.CONTROLLER, serviceAppAsid);

			return TryConnectServiceAppResult.Success;
		}
		public void ResendFriendshipRequests(int operatorID, SaveContext saveContext, List<Contact> cloudContacts)
		{
			if (cloudContacts.Count == 0)
				return;

			var activeFriendRequests = GetFriendshipRequestsTo(operatorID);
			if (activeFriendRequests.Count == 0)
				return;

			foreach (var activeFriendRequest in activeFriendRequests)
			{
				cloudContacts = FilterSendedFriendRequest(activeFriendRequest, cloudContacts);
				foreach (var cloudContact in cloudContacts)
					SendFriendRequestToContact(activeFriendRequest, cloudContact);
			}
			SaveChanges(saveContext);
		}
	}
}