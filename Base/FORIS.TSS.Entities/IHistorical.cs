﻿using System;

namespace FORIS.TSS.EntityModel
{
	/// <summary>
	/// Этот интерфейс реализует сущность,
	/// для которой при сохранении данных в БД производится запись в соответствующие исторические таблицы
	/// </summary>
	interface IHistorical
	{
		IHistoricalItem CreateHistoricalItem(History.HistoricalEntities entities);
	}
}