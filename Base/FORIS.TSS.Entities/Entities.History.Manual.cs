﻿using FORIS.TSS.EntityModel.History;

namespace FORIS.TSS.EntityModel
{
	namespace History
	{
		public partial class H_VEHICLEGROUP_VEHICLE : IHistoricalItem
		{
		}
		public partial class H_Message_Attachment : IHistoricalItem
		{
		}
		public partial class H_CompoundRule_Vehicle : IHistoricalItem
		{
		}
		public partial class H_CompoundRule_VehicleGroup : IHistoricalItem
		{
		}
		public partial class H_CompoundRule_SubscriptionEmail : IHistoricalItem
		{
		}
		public partial class H_CompoundRule_SubscriptionPhone : IHistoricalItem
		{
		}
		public partial class H_CompoundRule_ORDER : IHistoricalItem
		{
		}
		public partial class H_Vehicle_ZoneGroup : IHistoricalItem
		{
		}
	}
	public partial class Entities
	{
		public void Add(VEHICLEGROUP vehicleGroup, VEHICLE vehicle)
		{
			LogHistory(InsertAction, vehicleGroup, vehicle);
			vehicleGroup.VEHICLE.Add(vehicle);
		}
		public void Remove(VEHICLEGROUP vehicleGroup, VEHICLE vehicle)
		{
			if (vehicleGroup.VEHICLE.IsLoaded)
			{
				if (vehicleGroup.VEHICLE.Remove(vehicle))
					LogHistory(DeleteAction, vehicleGroup, vehicle);
			}
			else
			{
				if (!vehicle.VEHICLEGROUP.IsLoaded)
					vehicle.VEHICLEGROUP.Load();
				if (vehicle.VEHICLEGROUP.Remove(vehicleGroup))
					LogHistory(DeleteAction, vehicleGroup, vehicle);
			}
		}
		private void LogHistory(string action, VEHICLEGROUP vehicleGroup, VEHICLE vehicle)
		{
			var h = new H_VEHICLEGROUP_VEHICLE
			{
				ACTION = action,
				VEHICLE_ID = vehicle.VEHICLE_ID,
				VEHICLEGROUP_ID = vehicleGroup.VEHICLEGROUP_ID
			};
			_historyEntities.H_VEHICLEGROUP_VEHICLE.AddObject(h);
			_historicalItems.Add(h);
		}
		public void Add(MESSAGE container, Attachment item)
		{
			LogHistory(InsertAction, container, item);
			container.Attachment.Add(item);
		}
		public void Remove(MESSAGE container, Attachment item)
		{
			if (container.Attachment.IsLoaded)
			{
				if (container.Attachment.Remove(item))
					LogHistory(DeleteAction, container, item);
			}
			else
			{
				if (!item.MESSAGE.IsLoaded)
					item.MESSAGE.Load();
				if (item.MESSAGE.Remove(container))
					LogHistory(DeleteAction, container, item);
			}
		}
		private void LogHistory(string action, MESSAGE container, Attachment item)
		{
			var h = new H_Message_Attachment
			{
				ACTION = action,
				Attachment_ID = item.ID,
				Message_ID = container.MESSAGE_ID
			};
			_historyEntities.H_Message_Attachment.AddObject(h);
			_historicalItems.Add(h);
		}
		public void Add(CompoundRule compoundRule, VEHICLE vehicle)
		{
			LogHistory(InsertAction, compoundRule, vehicle);
			compoundRule.CompoundRule_Vehicle.Add(vehicle);
		}
		public void Remove(CompoundRule compoundRule, VEHICLE vehicle)
		{
			if (!compoundRule.CompoundRule_Vehicle.IsLoaded)
				compoundRule.CompoundRule_Vehicle.Load();
			if (compoundRule.CompoundRule_Vehicle.Remove(vehicle))
				LogHistory(DeleteAction, compoundRule, vehicle);
		}
		private void LogHistory(string action, CompoundRule compoundRule, VEHICLE vehicle)
		{
			var h = new H_CompoundRule_Vehicle
			{
				ACTION          = action,
				Vehicle_ID      = vehicle.VEHICLE_ID,
				CompoundRule_ID = compoundRule.CompoundRule_ID
			};
			_historyEntities.H_CompoundRule_Vehicle.AddObject(h);
			_historicalItems.Add(h);
		}
		public void Add(CompoundRule compoundRule, VEHICLEGROUP vehicleGroup)
		{
			LogHistory(InsertAction, compoundRule, vehicleGroup);
			compoundRule.CompoundRule_VehicleGroup.Add(vehicleGroup);
		}
		public void Remove(CompoundRule compoundRule, VEHICLEGROUP vehicleGroup)
		{
			if (!compoundRule.CompoundRule_VehicleGroup.IsLoaded)
				compoundRule.CompoundRule_VehicleGroup.Load();
			if (compoundRule.CompoundRule_VehicleGroup.Remove(vehicleGroup))
				LogHistory(DeleteAction, compoundRule, vehicleGroup);
		}
		private void LogHistory(string action, CompoundRule compoundRule, VEHICLEGROUP vehicleGroup)
		{
			var h = new H_CompoundRule_VehicleGroup
			{
				ACTION = action,
				VehicleGroup_ID = vehicleGroup.VEHICLEGROUP_ID,
				CompoundRule_ID = compoundRule.CompoundRule_ID
			};
			_historyEntities.H_CompoundRule_VehicleGroup.AddObject(h);
			_historicalItems.Add(h);
		}
		public void Add(VEHICLE vehicle, ZONEGROUP zoneGroup)
		{
			LogHistory(InsertAction, vehicle, zoneGroup);
			vehicle.ZoneGroups.Add(zoneGroup);
		}
		private void LogHistory(string action, VEHICLE vehicle, ZONEGROUP zoneGroup)
		{
			var h = new H_Vehicle_ZoneGroup
			{
				ACTION = action,
				Vehicle_ID = vehicle.VEHICLE_ID,
				ZoneGroup_ID = zoneGroup.ZONEGROUP_ID
			};
			_historyEntities.H_Vehicle_ZoneGroup.AddObject(h);
			_historicalItems.Add(h);
		}
		public void Add(CompoundRule compoundRule, ORDER order)
		{
			LogHistory(InsertAction, compoundRule, order);
			compoundRule.CompoundRule_ORDER.Add(order);
		}
		public void Remove(CompoundRule compoundRule, ORDER order)
		{
			if (!compoundRule.CompoundRule_ORDER.IsLoaded)
				compoundRule.CompoundRule_ORDER.Load();
			if (compoundRule.CompoundRule_ORDER.Remove(order))
				LogHistory(DeleteAction, compoundRule, order);
		}
		private void LogHistory(string action, CompoundRule compoundRule, ORDER order)
		{
			var h = new H_CompoundRule_ORDER
			{
				ACTION          = action,
				ORDER_ID        = order.ORDER_ID,
				CompoundRule_ID = compoundRule.CompoundRule_ID
			};
			_historyEntities.H_CompoundRule_ORDER.AddObject(h);
			_historicalItems.Add(h);
		}
	}
}