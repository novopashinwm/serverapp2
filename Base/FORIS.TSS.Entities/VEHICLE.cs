﻿namespace FORIS.TSS.EntityModel
{
	public partial class VEHICLE
	{
		public VEHICLE()
		{
			StoreLog          = true;
			GARAGE_NUMBER     = string.Empty;
			PUBLIC_NUMBER     = string.Empty;
			VALIDATOR_PRESENT = false;
			GUARD             = false;
			TIME_PREPARATION  = 0;
		}
		public string GetAsid()
		{
			var asidRecord = GetAsidRecord();
			return asidRecord != null ? asidRecord.Contact.Value : null;
		}
		public Asid GetAsidRecord()
		{
			if (!CONTROLLERReference.IsLoaded)
				CONTROLLERReference.Load();
			if (CONTROLLER == null)
				return null;
			if (!CONTROLLER.MLP_ControllerReference.IsLoaded)
				CONTROLLER.MLP_ControllerReference.Load();
			var mlpController = CONTROLLER.MLP_Controller;
			if (mlpController == null)
				return null;
			if (!mlpController.AsidReference.IsLoaded)
				mlpController.AsidReference.Load();
			var asid = mlpController.Asid;
			if (!asid.ContactReference.IsLoaded)
				asid.ContactReference.Load();
			return mlpController.Asid;
		}
		/// <summary> Установить департамент для объекта наблюдения </summary>
		/// <param name="entities"></param>
		/// <param name="department"></param>
		public void SetDepartment(Entities entities, DEPARTMENT department)
		{
			DEPARTMENT = department;

			if (department == null)
				return;
			// TODO: Отключаем до момента реализации биллинга по странам и корпоративных клиентов
			return;

			if (!department.CountryReference.IsLoaded)
				department.CountryReference.Load();

			if (department.Country?.Name != Country.India)
				return;
		}
	}
}