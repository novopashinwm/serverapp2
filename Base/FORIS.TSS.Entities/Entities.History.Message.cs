﻿using System.Linq;

namespace FORIS.TSS.EntityModel
{
	public partial class Entities
	{
		public int GetLastMessageUpdateId()
		{
			return _historyEntities.H_MESSAGE.Max(m => m.ID);
		}
	}
}