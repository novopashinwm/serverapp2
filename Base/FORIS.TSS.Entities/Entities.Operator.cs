﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;

namespace FORIS.TSS.EntityModel
{
	public partial class Entities
	{
		public bool IsSuperAdministrator(int operatorId)
		{
			if (!SalesOperatorGroupID.HasValue)
				return false;
			return OPERATORGROUP
				.Any(og =>
					og.OPERATORGROUP_ID == SalesOperatorGroupID.Value &&
					og.Members
						.Any(o => o.OPERATOR_ID == operatorId));
		}
		public bool IsSalesManager(int operatorId, int? departmentId)
		{
			return
				// Обязательно пользователь департамента
				departmentId.HasValue
				&&
				// Имеется право оператора ServiceManagement
				v_operator_rights
					.Any(x =>
						x.operator_id == operatorId &&
						x.right_id == (int)SystemRight.ServiceManagement)
				&&
				(
					// Оператор является пользователем этого департамента
					GetDepartmentOperatorsQuery(departmentId.Value)
						.Any(x =>
							x.OPERATOR_ID == operatorId)
					||
					// Оператор имеет право SystemRight.SecurityAdministration на департамент
					v_operator_department_right
						.Any(x =>
							x.operator_id   == operatorId &&
							x.right_id      == (int)SystemRight.SecurityAdministration &&
							x.Department_ID == departmentId.Value)
				);
		}
		public bool IsDefaultDepartment(int operatorId, int? departmentId)
		{
			return GetDefaultDepartmentByOperatorId(operatorId)
				?.DEPARTMENT_ID == departmentId;
		}
		public DEPARTMENT GetDefaultDepartmentByOperatorId(int operatorId)
		{
			return GetAvailableDepartmentsByOperatorIdQuery(operatorId)
				?.OrderBy(d => d.DEPARTMENT_ID)
				?.FirstOrDefault();
		}
		/// <summary> Привязывает оператора к данному департаменту </summary>
		/// <param name="ormOperator"> Оператор: к моменту вызова метода должен быть сохранён в БД </param>
		/// <param name="ormDepartment"> Абонент: к моменту вызова метода должен быть сохранён в БД </param>
		/// <remarks>Метод не сохраняет данные в БД</remarks>
		public bool AddOperatorToDepartment(OPERATOR ormOperator, DEPARTMENT ormDepartment)
		{
			if (ormOperator == null)
				throw new ArgumentNullException("operator");
			if ((ormOperator.EntityState & EntityState.Added) != 0)
				throw new ArgumentException("Value should get primary key in DB", "operator");
			if (ormDepartment == null)
				throw new ArgumentNullException("department");
			if ((ormDepartment.EntityState & EntityState.Added) != 0)
				throw new ArgumentException("Value should get primary key in DB", "department");

			//Группы операторов, у которых есть доступ к текущему департаменту и у которых нет административных прав на департамент
			var departmentOperatorGroups = GetDepartmentOperatorGroups(ormDepartment)
				.Where(og => !og.Members.Any(o => o.OPERATOR_ID == ormOperator.OPERATOR_ID))
				.ToList();

			if (departmentOperatorGroups.Count == 0)
				return false;

			foreach (var og in departmentOperatorGroups)
				og.Members.Add(ormOperator);

			return true;
		}
		/// <summary> Возвращает запрос на получение группы операторов данного клиента </summary>
		/// <param name="department"> Клиент </param>
		public IQueryable<OPERATORGROUP> GetDepartmentOperatorGroups(DEPARTMENT department)
		{
			return OPERATORGROUP
				.Where(x =>
					OPERATORGROUP_DEPARTMENT.Any(ogd =>
						ogd.DEPARTMENT_ID    == department.DEPARTMENT_ID           &&
						ogd.OPERATORGROUP_ID == x.OPERATORGROUP_ID                 &&
						ogd.RIGHT_ID         == (int)SystemRight.DepartmentsAccess &&
						ogd.ALLOWED)
					&&
					!OPERATORGROUP_DEPARTMENT.Any(ogd =>
						ogd.DEPARTMENT_ID    == department.DEPARTMENT_ID                &&
						ogd.OPERATORGROUP_ID == x.OPERATORGROUP_ID                      &&
						ogd.RIGHT_ID         == (int)SystemRight.SecurityAdministration &&
						ogd.ALLOWED)
				);
		}
		public void DeleteOperatorFromDepartment(OPERATOR @operator, DEPARTMENT department)
		{
			//Excluding operator from operator group associated with department
			var operatorGroups =
				GetDepartmentOperatorGroups(department).Where(
					og => og.Members.Any(o => o.OPERATOR_ID == @operator.OPERATOR_ID)).ToList();

			if (!@operator.ContainingGroups.IsLoaded)
				@operator.ContainingGroups.Load();

			foreach (var group in operatorGroups)
			{
				@operator.ContainingGroups.Remove(group);
			}

			// Excluding possible operator rights from department.
			foreach (var od in OPERATOR_DEPARTMENT.Where(x =>
				x.OPERATOR_ID == @operator.OPERATOR_ID
				&& x.ALLOWED
				))
				od.ALLOWED = false;

		}
		public bool AddFriend(int operatorId, int friendId, int? departmentId = null)
		{
			var groupOfFriends = FriendsGroupQuery(operatorId).FirstOrDefault();
			if (groupOfFriends == null)
				groupOfFriends = CreateFriendGroup(GetOperator(operatorId));

			//0.1. Проверяем, не друг ли уже
			if (groupOfFriends.Members.Any(m => m.OPERATOR_ID == friendId))
				return false;
			//0.2. или входит в данного клиента
			if (departmentId != null &&
				GetDepartmentOperatorsQuery(departmentId.Value).Any(o => o.OPERATOR_ID == friendId))
				return false;

			//1. Группа друзей
			if (groupOfFriends.EntityState != EntityState.Added && !groupOfFriends.Members.IsLoaded)
				groupOfFriends.Members.Load();

			var friend = GetOperator(friendId);
			if (!groupOfFriends.Members.Contains(friend))
				groupOfFriends.Members.Add(friend);

			return true;
		}
		public bool IsAdminOfVehicle(int operatorId, int vehicleId)
		{
			return v_operator_vehicle_right
				.Any(ovr =>
					ovr.operator_id == operatorId &&
					ovr.vehicle_id  == vehicleId &&
					ovr.right_id    == (int)SystemRight.SecurityAdministration);
		}
		public void CheckAdminOfVehicle(int operatorId, int vehicleId)
		{
			if (!IsAdminOfVehicle(operatorId, vehicleId))
				throw new SecurityException($@"Disallowed: current OperatorId={operatorId} is not admin of VehicleId={vehicleId}");
		}
		public bool IsAdminOfVehicleGroup(int operatorId, int vehicleGroupId)
		{
			return v_operator_vehicle_groups_right
				.Any(ovr =>
					ovr.operator_id     == operatorId &&
					ovr.vehiclegroup_id == vehicleGroupId &&
					ovr.right_id        == (int)SystemRight.GroupAdministration);
		}
		public void CheckAdminOfVehicleGroup(int operatorId, int vehicleGroupId)
		{
			if (!IsAdminOfVehicleGroup(operatorId, vehicleGroupId))
				throw new SecurityException($@"Disallowed: current OperatorId={operatorId} is not admin of vehicle GroupId={vehicleGroupId}");
		}
		/// <summary> Получает список запросов дружбы, полученные на номер телефона </summary>
		/// <param name="msisdnContactId"> Идентификатор номер телефона запрашиваемого абонента </param>
		/// <param name="recently"> Интервал в течении которого происходили запросы </param>
		/// <returns></returns>
		private IQueryable<MESSAGE> GetFriendshipRequestsToMsisdn(int msisdnContactId, DateTime? recently)
		{
			return MESSAGE.Where(m => (!recently.HasValue || m.TIME > recently)
				&& !m.GROUP.HasValue // Групповое сообщение
				&& m.ProcessingResult != (int) MessageProcessingResult.Success
				&& m.MESSAGE_TEMPLATE.NAME == Msg::MessageTemplate.ProposeFriendship
				&& m.Contacts.Any(mc => mc.Type == (int) MessageContactType.Destination
				&& mc.Contact_ID == msisdnContactId));
		}
		public IQueryable<MESSAGE> GetFriendshipRequestsTo(string msisdn, DateTime? recently)
		{
			var msisdnContactId = GetContactId(ContactType.Phone, msisdn);
			var friendshipRequestsTo = GetFriendshipRequestsToMsisdn(msisdnContactId, recently);
			return friendshipRequestsTo;
		}
		public List<MESSAGE> GetFriendshipRequestsTo(int operatorId, DateTime? recently)
		{
			var ownMsisdn = GetOwnPhoneContact(operatorId);
			if (ownMsisdn == null)
				return new List<MESSAGE>();
			return GetFriendshipRequestsToMsisdn(ownMsisdn.ID, recently).ToList();
		}
		public IQueryable<MESSAGE> GetFriendshipRequestNotifications(MESSAGE friendshipRequest)
		{
			return
				MESSAGE.Where(m =>
					m.Owner_Operator_ID == friendshipRequest.Owner_Operator_ID &&
					m.MESSAGE_TEMPLATE.NAME == Msg::MessageTemplate.ProposeFriendship &&
					m.GROUP == friendshipRequest.MESSAGE_ID &&
					m.MESSAGE_ID > friendshipRequest.MESSAGE_ID);
		}
		/// <summary> Есть ли уже зарегистрированный запрос дружбы </summary>
		/// <param name="operatorId"> Идентификатор оператора запрашивающего абонента </param>
		/// <param name="friendMsisdn"> Номер телефона запрашиваемого абонента </param>
		/// <returns></returns>
		public bool FriendshipRequestExists(int operatorId, string friendMsisdn)
		{
			var requestExpired = GetFriendRequestExpired();
			var recently = DateTime.UtcNow.Add(-requestExpired);
			return GetFriendshipRequestsTo(friendMsisdn, recently).Any(m => m.Owner_Operator_ID == operatorId);
		}
		/// <summary> Возвращает единственный неподтвержденный запрос дружбы </summary>
		/// <param name="msisdn"> Номер телефона определяемого абонента </param>
		/// <returns> Контакт определяющего абонента, если поступил только один запрос </returns>
		public MESSAGE GetLastFriendshipRequest(string msisdn)
		{
			var requestExpired = GetFriendRequestExpired();
			var recently       = DateTime.UtcNow.Add(-requestExpired);
			var friendRequests = GetFriendshipRequestsTo(msisdn, recently).ToList();
			var friendRequest  = friendRequests.Count == 1 ? friendRequests.Single() : null;

			return friendRequest;
		}
		public IQueryable<MESSAGE> GetFrienshipRequestsBetween(int operatorId, string friendMsisdn)
		{
			var requestExpired       = GetFriendRequestExpired();
			var recently             = DateTime.UtcNow.Add(-requestExpired);
			var friendshipRequestsTo = GetFriendshipRequestsTo(friendMsisdn, recently);
			return friendshipRequestsTo.Where(m => m.Owner_Operator_ID == operatorId);
		}
		public List<MESSAGE> GetFriendshipRequestsTo(int operatorId)
		{
			var requestExpired = GetFriendRequestExpired();
			var recently = DateTime.UtcNow.Add(-requestExpired);
			return GetFriendshipRequestsTo(operatorId, recently);
		}
		/// <summary> Регистрирует сообщение дружбы </summary>
		/// <param name="operatorId"> Идентификатор запрашивающего абонента </param>
		/// <param name="friendContact"> Контакт запрашиваемого абонента </param>
		/// <returns></returns>
		public MESSAGE RegisterFriendshipRequest(int operatorId, Contact friendContact)
		{
			var friendshipRequest = new MESSAGE
			{
				MESSAGE_TEMPLATE =
					MESSAGE_TEMPLATE.FirstOrDefault(mt => mt.NAME == Msg::MessageTemplate.ProposeFriendship),
				BODY = string.Empty,
				Owner_Operator_ID = operatorId,
				// TODO: Перенести обработку группового сообщения в обработчик.
				STATUS = (int) MessageState.Done,
				ProcessingResult = (int) MessageProcessingResult.Received
			};

			MESSAGE.AddObject(friendshipRequest);
			friendshipRequest.Contacts.Add(new Message_Contact
			{
				Contact_ID = friendContact.ID,
				Type = (int) MessageContactType.Destination
			});

			return friendshipRequest;
		}
		/// <summary> Помечает запрос дружбы выполненным </summary>
		/// <param name="operatorId"></param>
		/// <param name="friendOperatorId"></param>
		public bool UnregisterFriendshipRequest(int operatorId, int friendOperatorId)
		{
			var friendshipRequests = GetFriendshipRequestsTo(friendOperatorId, null)
				.Where(m => m.Owner_Operator_ID == operatorId)
				.ToArray();
			foreach (var friendshipRequest in friendshipRequests)
				friendshipRequest.ProcessingResult = (int) MessageProcessingResult.Success;

			return friendshipRequests.Any();
		}
		public bool FriendRequestIsSendedTo(MESSAGE friendRequest, Contact contact)
		{
			return
				MESSAGE.Any(
					m =>
						m.GROUP == friendRequest.MESSAGE_ID &&
						m.MESSAGE_TEMPLATE.NAME == Msg::MessageTemplate.ProposeFriendship &&
						m.Contacts.Any(c => c.Contact_ID == contact.ID && c.Type == (int) MessageContactType.Destination));
		}
		public List<Contact> FilterSendedFriendRequest(MESSAGE friendRequest, IEnumerable<Contact> contacts)
		{
			return contacts.Where(contact => !FriendRequestIsSendedTo(friendRequest, contact)).ToList();
		}
		/// <summary> Посылает запрос дружбы контакту </summary>
		/// <param name="friendRequest"></param>
		/// <param name="contact"></param>
		/// <returns></returns>
		public MESSAGE SendFriendRequestToContact(MESSAGE friendRequest, Contact contact)
		{
			if (!friendRequest.Owner_Operator_ID.HasValue)
				throw new ArgumentNullException("friendRequest", "Owner_Operator_ID is null");

			var ownContact = GetOwnPhoneContact(friendRequest.Owner_Operator_ID.Value);
			if (ownContact == null)
				return null;

			var message = CreateAppNotification(contact, null, null);
			message.MESSAGE_TEMPLATE =
				MESSAGE_TEMPLATE.First(mt => mt.NAME == Msg::MessageTemplate.ProposeFriendship);
			message.GROUP = friendRequest.MESSAGE_ID;
			AddMessageField(message, EntityModel.MESSAGE_TEMPLATE_FIELD.FriendMsisdn, ownContact.Value, typeof(string));
			message.Owner_Operator_ID = friendRequest.Owner_Operator_ID;

			return message;
		}
		public void SavePhoneBook(int operatorId, string xmlPhoneBook)
		{
			var phoneBook = Operator_PhoneBook.FirstOrDefault(b => b.OPERATOR.OPERATOR_ID == operatorId);
			if (phoneBook != null)
			{
				phoneBook.PhoneBook = xmlPhoneBook;
			}
			else
			{
				Operator_PhoneBook.AddObject(new Operator_PhoneBook
				{
					OperatorId = operatorId,
					PhoneBook = xmlPhoneBook
				});
			}
		}
		public void UpdateContactForAppClient(Contact oldContact, string newRegistrationId)
		{
			if (!string.IsNullOrWhiteSpace(newRegistrationId))
			{
				var newContact = GetContact(ContactType.Android, newRegistrationId);
				if (!newContact.AppClient.IsLoaded)
					newContact.AppClient.Load();
				if (!oldContact.AppClient.IsLoaded)
					oldContact.AppClient.Load();

				if (!newContact.AppClient.Any())
				{
					foreach (var ac in oldContact.AppClient.ToArray())
						ac.Contact = newContact;
				}

				//TODO: Пометить контакт не валидным?
				//oldContact.Valid = false;
			}
		}
		public IEnumerable<Contact> GetContacts(ContactType type, params string[] values)
		{
			return Contact.Where(c => c.Type == (int) type && values.Contains(c.Value));
		}
		/// <summary> Возвращает пользователя, чьим собственным объектом наблюдения является указанный объект </summary>
		/// <param name="vehicleId"> Идентификатор собственного объекта наблюдения </param>
		public IQueryable<OPERATOR> GetOperatorByVehicleId(int vehicleId)
		{
			return OPERATOR.Where(o => o.Asid.MLP_Controller.CONTROLLER.VEHICLE.VEHICLE_ID == vehicleId);
		}
		public string GetOperatorOwnName(int operatorId)
		{
			return OPERATOR.Where(o => o.OPERATOR_ID == operatorId).Select(o => o.NAME).FirstOrDefault();
		}
		/// <summary> Получить операторов зависимых операторов </summary>
		/// <param name="operatorId"></param>
		/// <param name="departmentId"></param>
		/// <returns></returns>
		public IEnumerable<OPERATOR> GetAllBelongOperators(int operatorId, int departmentId)
		{
			// Если оператор не имеет прав администрирования на департамент, то возвращаем пустой набор
			if (!v_operator_department_right.Any(x =>
					x.operator_id   == operatorId                              &&
					x.right_id      == (int)SystemRight.SecurityAdministration &&
					x.Department_ID == departmentId))
			{
				return Enumerable.Empty<OPERATOR>();
			}
			// Получаем список всех пользователей департамента (не админов), кроме себя
			return GetDepartmentOperatorsQuery(departmentId)
				.Where(o => o.OPERATOR_ID != operatorId);
		}
		public int? GetOperatorIdBySimId(string simId)
		{
			if (string.IsNullOrWhiteSpace(simId))
				return null;

			var operatorId = Asid
				.Where(a => a.SimID == simId)
				.Select(a => a.OPERATOR.OPERATOR_ID)
				.FirstOrDefault();
			if (operatorId == default(int))
				return null;
			return operatorId;
		}
		public IOperatorInfo GetOperatorInfo(int operatorId)
		{
			var ormOperator = OPERATOR?.FirstOrDefault(o => o.OPERATOR_ID == operatorId);
			return null != ormOperator
				? new OperatorInfo(ormOperator.OPERATOR_ID, ormOperator.LOGIN, ormOperator.NAME)
				: default(OperatorInfo);
		}
	}
}