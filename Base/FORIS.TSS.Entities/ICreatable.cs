﻿namespace FORIS.TSS.EntityModel
{
    public interface ICreatable
    {
        void AddTo(Entities entities);
    }
}
