﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using FORIS.TSS.BusinessLogic.DTO.Rules;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common.Helpers;

namespace FORIS.TSS.EntityModel
{
	public static partial class EntitiesExtensions
	{
		public static IQueryable<T> Include<T>(this ObjectQuery<T> queryable, params string[] includes) where T : class
		{
			if (includes == null || includes.Length == 0)
				return queryable;

			var result = queryable;

			foreach (var include in includes)
				result = result.Include(include);

			return result;
		}
		public static void ExecuteGuarantly(this Entities entities, Action<Entities> action)
		{
			var executed = false;
			while (!executed)
			{
				try
				{
					executed = true;
					action.Invoke(entities);
				}
				catch (DataException ex)
				{
					if (ex.IsSqlTimeoutException())
						executed = false;
					else
						throw;
				}
			}
		}
		public static List<BusinessLogic.DTO.Rules.Rule> GetRulesDTOs(this Entities entities, IEnumerable<CompoundRule> compoundRules, IdType objectIdType, int objectId)
		{
			var compoundRuleIds              = compoundRules.Select(                       item => item.CompoundRule_ID).ToArray();
			var compoundRuleSensors          = entities.CompoundRule_Sensor.Where(         item => compoundRuleIds.Contains(item.CompoundRule.CompoundRule_ID)).ToLookup(item => item.CompoundRule.CompoundRule_ID);
			var compoundRuleSpeeds           = entities.CompoundRule_Speed.Where(          item => compoundRuleIds.Contains(item.CompoundRule.CompoundRule_ID)).ToLookup(item => item.CompoundRule.CompoundRule_ID);
			var compoundRuleTimes            = entities.CompoundRule_Time.Where(           item => compoundRuleIds.Contains(item.CompoundRule.CompoundRule_ID)).ToLookup(item => item.CompoundRule.CompoundRule_ID);
			var compoundRuleTimePoints       = entities.CompoundRule_TimePoint.Where(      item => compoundRuleIds.Contains(item.CompoundRule.CompoundRule_ID)).ToLookup(item => item.CompoundRule.CompoundRule_ID);
			var compoundRuleDataAbsences     = entities.CompoundRule_DataAbsence.Where(    item => compoundRuleIds.Contains(item.CompoundRule.CompoundRule_ID)).ToLookup(item => item.CompoundRule.CompoundRule_ID);
			var compoundRulePositionAbsences = entities.CompoundRule_PositionAbsence.Where(item => compoundRuleIds.Contains(item.CompoundRule.CompoundRule_ID)).ToLookup(item => item.CompoundRule.CompoundRule_ID);
			var compoundRuleZones            = entities.CompoundRule_Zone.Where(           item => compoundRuleIds.Contains(item.CompoundRule.CompoundRule_ID)).ToLookup(item => item.CompoundRule.CompoundRule_ID);
			var compoundRuleZoneGroups       = entities.CompoundRule_ZoneGroup.Where(      item => compoundRuleIds.Contains(item.CompoundRule.CompoundRule_ID)).ToLookup(item => item.CompoundRule.CompoundRule_ID);
			var compoundRuleMessageTemplates = entities.CompoundRule_Message_Template
				.Where(item => compoundRuleIds.Contains(item.CompoundRule.CompoundRule_ID))
				.Select(item => new
					{
						item.CompoundRule_Message_Template_ID,
						item.CompoundRule.CompoundRule_ID,
						item.OPERATOR.OPERATOR_ID,
						item.ToAppClient,
						item.ToOwnEmail,
						item.ToOwnPhone,
						item.ToOwnEmailMaxQty,
						item.ToOwnPhoneMaxQty,
						item.ToAppClientMaxQty,
					})
				.ToLookup(item => item.CompoundRule_ID);
			var compoundRuleOperatorContacts = entities.CompoundRule_Operator_Contact
				.Where(item => compoundRuleIds.Contains(item.CompoundRule.CompoundRule_ID))
				.ToLookup(item => item.CompoundRule.CompoundRule_ID, item => item.Contact);

			var result = compoundRules
				.Select(cr => new BusinessLogic.DTO.Rules.Rule
				{
					Id               = cr.CompoundRule_ID,
					ObjectId         = objectId,
					ObjectIdType     = objectIdType,
					OrderId          = cr.CompoundRule_ORDER
						?.FirstOrDefault()
						?.ORDER_ID,
					Enabled          = cr.Active,
					Description      = cr.Description,
					MotionStart      = cr.MotionStart,
					SensorConditions = compoundRuleSensors[cr.CompoundRule_ID]
						.Select(item => new SensorCondition
						{
							Id     = item.CompoundRule_Sensor_ID,
							Sensor = (SensorLegend)item.CONTROLLER_SENSOR_LEGEND.Number,
							Type   = (ScalarComparisonType)item.Comparison_Type,
							Value  = item.Value
						})
							.ToArray(),
					SpeedConditions = compoundRuleSpeeds[cr.CompoundRule_ID]
						.Select(item => new SpeedCondition
						{
							Id    = item.CompoundRule_Speed_ID,
							Type  = (ScalarComparisonType)item.Type,
							Value = item.Value
						})
							.ToArray(),
					TimeConditions = compoundRuleTimes[cr.CompoundRule_ID]
						.Select(item => new TimeCondition
						{
							Id          = item.CompoundRule_Time_ID,
							HoursFrom   = item.Hours_From,
							MinutesFrom = item.Minutes_From,
							HoursTo     = item.Hours_To,
							MinutesTo   = item.Minutes_To
						})
							.ToArray(),
					TimePointConditions = compoundRuleTimePoints[cr.CompoundRule_ID]
						.Select(item => new TimePointCondition
						{
							Id      = item.CompoundRule_TimePoint_ID,
							LogTime = item.LogTime,
						})
							.ToArray(),
					DataAbsenceConditions = compoundRuleDataAbsences[cr.CompoundRule_ID]
						.Select(item => new DataAbsenceCondition
						{
							Id      = item.CompoundRule.CompoundRule_ID,
							Minutes = item.Minutes
						})
							.ToArray(),
					PositionAbsenceConditions = compoundRulePositionAbsences[cr.CompoundRule_ID]
						.Select(item => new PositionAbsenceCondition
						{
							Id      = item.CompoundRule.CompoundRule_ID,
							Minutes = item.Minutes
						})
							.ToArray(),
					ZoneConditions = compoundRuleZones[cr.CompoundRule_ID]
						.Select(item => new ZoneCondition
						{
							Id         = item.CompoundRule_Zone_ID,
							EventType  = (ZoneEventType)item.Type,
							ZoneIdType = IdType.Zone,
							ZoneId     = item.GEO_ZONE.ZONE_ID,
						})
						.Concat(compoundRuleZoneGroups[cr.CompoundRule_ID]
							.Select(item => new ZoneCondition
							{
								Id         = item.CompoundRule_ZoneGroup_ID,
								EventType  = (ZoneEventType)item.Type,
								ZoneIdType = IdType.ZoneGroup,
								ZoneId     = item.ZONEGROUP.ZONEGROUP_ID
							}))
						.ToArray(),
					Subscriptions = compoundRuleMessageTemplates[cr.CompoundRule_ID]
						.Select(item =>
						{
							var compoundRuleContacts = compoundRuleOperatorContacts[cr.CompoundRule_ID]
								.Select(c => new
								{
									Id = c.ID,
									c.Value,
									Type = (ContactType)c.Type
								})
								.ToArray();

							var compoundRuleContactIds = compoundRuleContacts
								.Select(c => c.Id)
								.ToArray();
							var operatorId       = item.OPERATOR_ID;
							var operatorContacts = entities.Operator_Contact
								.Where(c => c.Operator_ID == operatorId && compoundRuleContactIds.Contains(c.Contact_ID))
								.Select(c => new
								{
									Id    = c.Contact_ID,
									Value = c.Contact.Value,
									Type  = (ContactType)c.Contact.Type
								})
								.ToArray();
							var notOperatorContacts = compoundRuleContacts
								.Where(c => operatorContacts.All(oc => oc.Id != c.Id))
								.ToArray();

							var result = new RuleSubscription
							{
								Id                = item.CompoundRule_Message_Template_ID,
								ToOwnPhone        = item.ToOwnPhone,
								ToOwnPhoneMaxQty  = item.ToOwnPhoneMaxQty,
								ToOwnEmail        = item.ToOwnEmail,
								ToOwnEmailMaxQty  = item.ToOwnEmailMaxQty,
								ToAppClient       = item.ToAppClient,
								ToAppClientMaxQty = item.ToAppClientMaxQty,
								EmailIds = operatorContacts
									.Where(x => x.Type == ContactType.Email)
									.Select(x => x.Id)
									.ToArray(),
								PhoneIds = operatorContacts
									.Where(x => x.Type == ContactType.Phone)
									.Select(x => x.Id)
									.ToArray(),
								Emails = notOperatorContacts
									.Where(x => x.Type == ContactType.Email)
									.Select(x => x.Value)
									.ToArray(),
								Phones = notOperatorContacts
									.Where(x => x.Type == ContactType.Phone)
									.Select(x => x.Value)
									.ToArray(),
								OperatorId = operatorId
							};
							return result;
						})
						.ToArray()
				})
				.ToList();
			return result;
		}
	}
}