﻿using System;
using FORIS.TSS.EntityModel.History;

namespace FORIS.TSS.EntityModel
{
	interface IHistoricalItem
	{
		TRAIL TRAIL { get; set; }
		string ACTION { get; set; }
		DateTime ACTUAL_TIME { get; set; }
	}
}