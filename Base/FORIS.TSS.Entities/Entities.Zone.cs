﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Map;
using FORIS.TSS.Common;

namespace FORIS.TSS.EntityModel
{
	public partial class Entities
	{
		public IQueryable<GEO_ZONE> GetZone(int operatorId, int zoneId, params string[] includes)
		{
			return GetZone(zoneId, includes)
				.Where(z => z.Accesses.Any(ozr =>
					ozr.operator_id == operatorId &&
					ozr.right_id    == (int)SystemRight.ZoneAccess));
		}
		public IQueryable<GEO_ZONE> GetZone(int zoneId, params string[] includes)
		{
			return GEO_ZONE
				.Include(includes)
				.Where(z => z.ZONE_ID == zoneId);
		}
		public IQueryable<GEO_ZONE> GetGeoZonesByGroup(int zoneGroupId, params string[] includes)
		{
			return GetZoneGroup(zoneGroupId, includes)
				.SelectMany(g => g.Members.Select(z => z.Zone));
		}
		public IQueryable<GEO_ZONE> GetGeoZonesByGroup(int operatorId, int zoneGroupId, params string[] includes)
		{
			return GetZoneGroup(operatorId, zoneGroupId, includes)
				.SelectMany(g => g.Members.Select(z => z.Zone));
		}
		public IQueryable<ZONEGROUP> GetZoneGroup(int operatorId, int zoneGroupId, params string[] includes)
		{
			return GetZoneGroup(zoneGroupId, includes)
				.Where(zg => zg.Accesses.Any(ozr =>
					ozr.operator_id == operatorId &&
					ozr.right_id    == (int)SystemRight.ZoneAccess));
		}
		public IQueryable<ZONEGROUP> GetZoneGroup(int zoneGroupId, params string[] includes)
		{
			return ZONEGROUP
				.Include(includes)
				.Where(zg => zg.ZONEGROUP_ID == zoneGroupId);
		}
		public GEO_ZONE GeoZoneCreate(int operatorId, GeoZone dtoGeoZone, int? departmentId)
		{
			if (!IsAllowed(operatorId, SystemRight.EditZone))
				throw new SecurityException($"Not enough rights for create zone: {dtoGeoZone.Id} for operator: {operatorId}");

			var ormGeoZone = new GEO_ZONE
			{
				COLOR         = ColorTranslator.FromHtml(dtoGeoZone.Color).ToArgb(),
				NAME          = dtoGeoZone.Name ?? string.Empty,
				DESCRIPTION   = dtoGeoZone.Description,
				Department_ID = departmentId,
			};

			GEO_ZONE.AddObject(ormGeoZone);

			var ormZonePrimitive = new ZONE_PRIMITIVE
			{
				ZONE_TYPE = ZONE_TYPE.FirstOrDefault(zt => zt.ZONE_TYPE_ID == (int) dtoGeoZone.Type),
				RADIUS    = dtoGeoZone.Type == ZoneTypes.Circle
					? dtoGeoZone.Points[0].Radius
					: null
			};
			ZONE_PRIMITIVE.AddObject(ormZonePrimitive);

			GEO_ZONE_PRIMITIVE.AddObject(
				new GEO_ZONE_PRIMITIVE
				{
					GEO_ZONE       = ormGeoZone,
					ZONE_PRIMITIVE = ormZonePrimitive,
					ORDER          = 1
				});

			ZonePrimitiveAddVertices(ormZonePrimitive, dtoGeoZone.Points.ExcludeNeighborDuplicates());

			var ormOperator = GetOperator(operatorId);

			foreach (var ormRight in GetSystemRights(UserRole.DefaultZoneCreatorRights))
			{
				OPERATOR_ZONE.AddObject(
					new OPERATOR_ZONE
					{
						OPERATOR = ormOperator,
						GEO_ZONE = ormGeoZone,
						RIGHT    = ormRight,
						ALLOWED  = true
					});
			}

			return ormGeoZone;
		}
		public GEO_ZONE GeoZoneUpdate(int operatorId, GeoZone dtoGeoZone)
		{
			if (!IsAllowedZoneAll(operatorId, dtoGeoZone.Id, SystemRight.EditZone))
				throw new SecurityException($"Not enough rights for update zone: {dtoGeoZone.Id} for operator: {operatorId}");

			var ormGeoZone = GetZone(operatorId, dtoGeoZone.Id,
				$"{nameof(GEO_ZONE_PRIMITIVE)}",
				$"{nameof(GEO_ZONE_PRIMITIVE)}.{nameof(ZONE_PRIMITIVE)}",
				$"{nameof(GEO_ZONE_PRIMITIVE)}.{nameof(ZONE_PRIMITIVE)}.{nameof(ZONE_PRIMITIVE_VERTEX)}",
				$"{nameof(GEO_ZONE_PRIMITIVE)}.{nameof(ZONE_PRIMITIVE)}.{nameof(ZONE_PRIMITIVE_VERTEX)}.{nameof(MAP_VERTEX)}",
				$"{nameof(GEO_ZONE_PRIMITIVE)}.{nameof(ZONE_PRIMITIVE)}.{nameof(ZONE_PRIMITIVE_VERTEX)}.{nameof(MAP_VERTEX)}.{nameof(MAPS)}")
				.FirstOrDefault();
			if (ormGeoZone == null)
				throw new KeyNotFoundException($"Zone: {dtoGeoZone.Id} not found for operator: {operatorId}");

			ormGeoZone.NAME        = dtoGeoZone.Name;
			ormGeoZone.DESCRIPTION = dtoGeoZone.Description;
			ormGeoZone.COLOR       = ColorTranslator.FromHtml(dtoGeoZone.Color).ToArgb();

			var ormGeoZonePrimitive = ormGeoZone.GEO_ZONE_PRIMITIVE.FirstOrDefault();
			if (ormGeoZonePrimitive != null)
			{
				var ormZonePrimitive = ormGeoZonePrimitive.ZONE_PRIMITIVE;
				if (ormZonePrimitive != null)
				{
					ormZonePrimitive.ZONE_TYPE = ZONE_TYPE
						.FirstOrDefault(zt => zt.ZONE_TYPE_ID == (int)dtoGeoZone.Type);
					ormZonePrimitive.RADIUS    = dtoGeoZone.Type == ZoneTypes.Circle
						? dtoGeoZone.Points.Single().Radius
						: null;
					foreach (var primitiveVertex in ormZonePrimitive.ZONE_PRIMITIVE_VERTEX.ToArray())
						ZONE_PRIMITIVE_VERTEX.DeleteObject(primitiveVertex);
					ZonePrimitiveAddVertices(ormZonePrimitive, dtoGeoZone.Points);
				}
			}
			return ormGeoZone;
		}
		public void GeoZoneDelete(int operatorId, int zoneId)
		{
			if (!IsAllowedZoneAll(operatorId, zoneId, SystemRight.EditZone))
				throw new SecurityException($"Not enough rights for delete zone: {zoneId} for operator: {operatorId}");

			var ormGeoZone = GetZone(operatorId, zoneId).FirstOrDefault();
			if (ormGeoZone == null)
				throw new KeyNotFoundException($"Zone: {zoneId} not found for operator: {operatorId}");

			DeleteCollection(OPERATORGROUP_ZONE, ormGeoZone.OPERATORGROUP_ZONE);
			DeleteCollection(OPERATOR_ZONE,      ormGeoZone.OPERATOR_ZONE);
			DeleteCollection(ZONE_VEHICLE,       ormGeoZone.ZONE_VEHICLE);
			DeleteCollection(ZONE_VEHICLEGROUP,  ormGeoZone.ZONE_VEHICLEGROUP);
			DeleteCollection(CompoundRule_Zone,  ormGeoZone.CompoundRule_Zone);
			DeleteCollection(ZONEGROUP_ZONE,     ormGeoZone.ZONEGROUP_ZONE);
			DeleteCollection(Zone_Matrix,        ormGeoZone.Zone_Matrix);
			DeleteCollection(
				GEO_ZONE_PRIMITIVE,
				ormGeoZone.GEO_ZONE_PRIMITIVE,
				gzp => DeleteReference(
					ZONE_PRIMITIVE,
					gzp.ZONE_PRIMITIVEReference,
					zp => DeleteCollection(
						ZONE_PRIMITIVE_VERTEX,
						zp.ZONE_PRIMITIVE_VERTEX,
						zpv => DeleteReference(MAP_VERTEX, zpv.MAP_VERTEXReference))));

			GEO_ZONE.DeleteObject(ormGeoZone);
		}
		private void ZonePrimitiveAddVertices(ZONE_PRIMITIVE ormZonePrimitive, IEnumerable<GeoZonePoint> points)
		{
			var vertexOrder = 1;
			foreach (var point in points)
			{
				var ormMapVertex = new MAP_VERTEX
				{
					ENABLE  = true,
					VISIBLE = true,
					X       = point.Lng,
					Y       = point.Lat,
				};
				MAP_VERTEX.AddObject(ormMapVertex);

				ZONE_PRIMITIVE_VERTEX.AddObject(
					new ZONE_PRIMITIVE_VERTEX
					{
						ZONE_PRIMITIVE = ormZonePrimitive,
						MAP_VERTEX     = ormMapVertex,
						ORDER          = vertexOrder
					});
				++vertexOrder;
			}
		}
		public ZONEGROUP ZoneGroupCreate(int operatorId, string name, int? departmentId)
		{
			var ormZoneGroup = new ZONEGROUP
			{
				NAME          = name,
				Department_ID = departmentId,
			};

			ZONEGROUP.AddObject(ormZoneGroup);

			foreach (var right in UserRole.DefaultZoneGroupCreatorRights)
			{
				OPERATOR_ZONEGROUP.AddObject(new OPERATOR_ZONEGROUP
				{
					ZONEGROUP   = ormZoneGroup,
					OPERATOR_ID = operatorId,
					RIGHT_ID    = (int)right,
					ALLOWED     = true
				});
			}
			return ormZoneGroup;
		}
		public ZONEGROUP ZoneGroupUpdate(int operatorId, int zoneGroupId, string name)
		{
			if (!IsAllowedZoneGroupAll(operatorId, zoneGroupId, SystemRight.EditZone))
				throw new SecurityException($"Not enough rights for update zone group: {zoneGroupId} for operator: {operatorId}");

			var ormZoneGroup = GetZoneGroup(operatorId, zoneGroupId).FirstOrDefault();
			if (ormZoneGroup == null)
				throw new KeyNotFoundException($"Zone group: {zoneGroupId} not found for operator: {operatorId}");

			ormZoneGroup.NAME = name;

			return ormZoneGroup;
		}
		public void ZoneGroupDelete(int operatorId, int zoneGroupId)
		{
			if (!IsAllowedZoneGroupAll(operatorId, zoneGroupId, SystemRight.EditZone))
				throw new SecurityException($"Not enough rights for delete zone group: {zoneGroupId} for operator: {operatorId}");

			var ormZoneGroup = GetZoneGroup(operatorId, zoneGroupId).FirstOrDefault();
			if (ormZoneGroup == null)
				throw new KeyNotFoundException($"Zone group: {zoneGroupId} not found for operator: {operatorId}");

			if (ormZoneGroup != null)
			{
				var zonegroupzone =
					from z in ZONEGROUP_ZONE
					where z.ZONEGROUP_ID == zoneGroupId
					select z;
				foreach (var zonegroupZone in zonegroupzone)
				{
					DeleteObject(zonegroupZone);
				}
				var operatorzonegroup =
					from o in OPERATOR_ZONEGROUP
					where o.ZONEGROUP_ID == zoneGroupId
					select o;
				foreach (var operatorZonegroup in operatorzonegroup)
				{
					DeleteObject(operatorZonegroup);
				}

				var operatorgroupzonegroup =
					from o in OPERATORGROUP_ZONEGROUP
					where o.ZONEGROUP_ID == zoneGroupId
					select o;
				foreach (var operatorGroupZoneGroup in operatorgroupzonegroup)
				{
					DeleteObject(operatorGroupZoneGroup);
				}

				var compoundrulezonegroup =
					from r in CompoundRule_ZoneGroup
					where r.ZONEGROUP.ZONEGROUP_ID == zoneGroupId
					select r;
				foreach (var compoundRuleZoneGroup in compoundrulezonegroup)
				{
					DeleteObject(compoundRuleZoneGroup);
				}

				var orderzonegroups =
					from r in ORDER_ZONEGROUP
					where r.ZONEGROUP_ID == zoneGroupId
					select r;
				foreach (var orderzonegroup in orderzonegroups)
				{
					DeleteObject(orderzonegroup);
				}

				DeleteObject(ormZoneGroup);
			}
		}
		public void ZoneMatrixUpsert(int zoneId, IEnumerable<MatrixNode> matrixNodes)
		{
			//Удалить все предыдущие индексы зоны, если они есть
			ZoneMatrixDelete(zoneId);
			//Добавляем индексы по зоне
			foreach (var matrixNode in matrixNodes)
			{
				var newZoneMatrix = new Zone_Matrix
				{
					ZONE_ID  = zoneId,
					NODE     = matrixNode.Node,
					IsFull   = matrixNode.IsFull ?? false,
					SCALE    = (byte)matrixNode.Scale
				};
				Zone_Matrix.AddObject(newZoneMatrix);
			}
		}
		public void ZoneMatrixDelete(int zoneId)
		{
			foreach (var zoneMatrixItem in Zone_Matrix.Where(n => n.ZONE_ID == zoneId).ToArray())
				Zone_Matrix.DeleteObject(zoneMatrixItem);
		}
		public void ZoneGroupIncludeZone(int operatorId, int zoneGroupId, int zoneId)
		{
			if (!IsAllowedZoneAll(operatorId, zoneId, SystemRight.ZoneAccess))
				throw new SecurityException($"Zone '{zoneId}' could not be accessed by operator '{operatorId}'");

			if(!IsAllowedZoneGroupAll(operatorId, zoneGroupId, SystemRight.ZoneAccess, SystemRight.EditGroup))
				throw new SecurityException($"Zone '{zoneId}' cannot be edited by operator '{operatorId}'");

			if (ZONEGROUP_ZONE.Any(zgz => zgz.ZONEGROUP_ID == zoneGroupId && zgz.ZONE_ID == zoneId))
				return;

			var link = new ZONEGROUP_ZONE { ZONEGROUP_ID = zoneGroupId, ZONE_ID = zoneId };
			ZONEGROUP_ZONE.AddObject(link);
		}
		public void ZoneGroupExcludeZone(int operatorId, int zoneGroupId, int zoneId)
		{
			if (!IsAllowedZoneAll(operatorId, zoneId, SystemRight.ZoneAccess))
				throw new SecurityException($"Zone '{zoneId}' could not be accessed by operator '{operatorId}'");

			if (!IsAllowedZoneGroupAll(operatorId, zoneGroupId, SystemRight.ZoneAccess, SystemRight.EditGroup))
				throw new SecurityException($"Zone '{zoneId}' cannot be edited by operator '{operatorId}'");

			var link = ZONEGROUP_ZONE
				.FirstOrDefault(zgz =>
					zgz.ZONE_ID      == zoneId &&
					zgz.ZONEGROUP_ID == zoneGroupId);
			if (link == null)
				return;
			ZONEGROUP_ZONE.DeleteObject(link);
		}
	}
}