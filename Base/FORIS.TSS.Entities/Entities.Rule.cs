﻿using System;
using System.Collections.Generic;
using System.Data.Objects.DataClasses;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO.Rules;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Helpers;
using Msg = FORIS.TSS.BusinessLogic.DTO.Messages;
using Orm = FORIS.TSS.EntityModel;


namespace FORIS.TSS.EntityModel
{
	public partial class Entities
	{
		public static void Syncronize<TRemote, TLocal>(
			Entities entities,
			IEnumerable<TRemote> remotes, IEnumerable<TLocal> locals, Action<TRemote, TLocal> copyValues)
			where TRemote : EntityObject, ICreatable, IIdentifiable, new()
			where TLocal : IIdentifiable
		{
			if (locals != null)
			{
				// Вначале удаление
				var localItemById = locals.Where(item => item.ID > 0).ToDictionary(item => item.ID);
				var remotesToDelete = remotes.Where(
					remote => !localItemById.ContainsKey(remote.ID)).ToArray();
				foreach (var remoteItemToDelete in remotesToDelete)
					entities.DeleteObject(remoteItemToDelete);

				// Затем добавление или изменение
				var remoteItemById = remotes.ToDictionary(item => item.ID);
				foreach (var local in locals)
				{
					var conditionId = local.ID;
					TRemote remote;
					if (!remoteItemById.TryGetValue(conditionId, out remote))
					{
						remote = new TRemote();
						remote.AddTo(entities);
					}
					copyValues(remote, local);
				}
			}
			else
			{
				// Только удаление
				var remotesToDelete = remotes.ToArray();
				foreach (var remoteItemToDelete in remotesToDelete)
					entities.DeleteObject(remoteItemToDelete);
			}
		}
		public void ProcessRuleSubscriptions(RuleSubscription[] subscriptions)
		{
			foreach (var subscription in subscriptions)
			{
				if (subscription.Emails != null)
				{
					var emails = subscription.Emails
						.Select(p => p.Replace(" ", string.Empty))
						.Where(ContactHelper.IsValidEmail)
						.ToArray();
					var existEmails = GetContacts(ContactType.Email, emails)
						.Select(c => c.Value)
						.ToArray();
					foreach (var email in emails.Except(existEmails))
						GetContactId(ContactType.Email, email);
				}

				if (subscription.Phones != null)
				{
					var phones = subscription.Phones
						.Select(ContactHelper.GetNormalizedPhone)
						.Where(ContactHelper.IsValidPhone)
						.ToArray();
					var existPhones = GetContacts(ContactType.Phone, phones)
						.Select(c => c.Value)
						.ToArray();
					foreach (var phone in phones.Except(existPhones))
						GetContactId(ContactType.Phone, phone);
				}
			}
		}
		public RuleValidationError[] CheckRule(int operatorId, Rule rule)
		{
			var validationMessages = new List<RuleValidationError>();
			var timeConditionExist      = rule.TimeConditions            != null && rule.TimeConditions.Any();
			var timePointConditionExist = rule.TimePointConditions       != null && rule.TimePointConditions.Any();
			var sensorConditionsExists  = rule.SensorConditions          != null && rule.SensorConditions.Any();
			var speedConditionsExists   = rule.SpeedConditions           != null && rule.SpeedConditions.Any();
			var zoneConditionsExists    = rule.ZoneConditions            != null && rule.ZoneConditions.Any();
			var dataAbsenceExists       = rule.DataAbsenceConditions     != null && rule.DataAbsenceConditions.Any();
			var positionAbsenceExists   = rule.PositionAbsenceConditions != null && rule.PositionAbsenceConditions.Any();
			var subscriptionsExists     = rule.Subscriptions             != null && rule.Subscriptions.Any();
			var anyRuleCanProcess       =
				sensorConditionsExists ||
				speedConditionsExists  ||
				zoneConditionsExists   ||
				dataAbsenceExists      ||
				positionAbsenceExists  ||
				rule.MotionStart;
			if ((timeConditionExist || timePointConditionExist || subscriptionsExists) && !anyRuleCanProcess)
			{
				validationMessages.Add(new RuleValidationError
				{
					Message = "RuleCannotBeProcess"
				});
				return validationMessages.ToArray();
			}

			if (!subscriptionsExists)
			{
				validationMessages.Add(new RuleValidationError
				{
					Message         = "SubscriptionsInvalid",
					ValidationGroup = RuleValidationGroup.Subscriptions,
					Value           = rule.Subscriptions
				});
				return validationMessages.ToArray();
			}

			foreach (var subscription in rule.Subscriptions.Where(subscription => subscription.OperatorId == default(int)))
				subscription.OperatorId = operatorId;

			var subscriptionOperatorIds = rule.Subscriptions.Select(s => s.OperatorId).Distinct();
			var invalidOperatorIds = subscriptionOperatorIds.Where(id => id != operatorId).ToArray();
			if(invalidOperatorIds.Any())
			{
				validationMessages.AddRange(
					invalidOperatorIds.Select(id => new RuleValidationError
					{
						Message         = "Subscriptions.OperatorIdInvalid",
						ValidationGroup = RuleValidationGroup.Subscriptions,
						Value = id
					}));
			}

			if(validationMessages.Any())
				return validationMessages.ToArray();

			if (sensorConditionsExists)
			{
				var sensorLegends = rule.SensorConditions.Select(c => c.Sensor).Cast<int>().ToArray();
				var existLegends = CONTROLLER_SENSOR_LEGEND.Where(l => sensorLegends.Contains(l.Number)).Select(l => l.Number).ToArray();
				var missingLegends = sensorLegends.Except(existLegends);
				validationMessages.AddRange(
					missingLegends.Select(controllerSensorLegend => new RuleValidationError
					{
						Message         = "SensorConditions.SensorInvalid",
						ValidationGroup = RuleValidationGroup.SensorConditions,
						Value           = controllerSensorLegend
					}));
			}

			if (zoneConditionsExists)
			{
				var zoneIds = rule.ZoneConditions.Where(c => c.ZoneIdType == IdType.Zone).Select(c => c.ZoneId).ToArray();
				var existZones = v_operator_zone_right
					.Where(r => zoneIds.Contains(r.ZONE_ID) && r.right_id == (int)SystemRight.ZoneAccess && r.operator_id == operatorId)
					.Select(r => new
					{
						Id   = r.ZONE_ID,
						Name = r.GEO_ZONE.NAME
					})
					.ToArray();
				var missingZoneIds = zoneIds.Except(existZones.Select(z => z.Id));
				validationMessages.AddRange(
					missingZoneIds.Select(zone => new RuleValidationError
					{
						Message         = "ZoneConditions.ZoneInvalid",
						ValidationGroup = RuleValidationGroup.ZoneConditions,
						Value           = zone
					}));

				var zoneGroupIds = rule.ZoneConditions.Where(c => c.ZoneIdType == IdType.ZoneGroup).Select(c => c.ZoneId).ToArray();
				var existZoneGroups = v_operator_zone_groups_right
					.Where(r => zoneGroupIds.Contains(r.zonegroup_id) && r.right_id == (int)SystemRight.ZoneAccess && r.operator_id == operatorId)
					.Select(r => new
					{
						Id   = r.zonegroup_id,
						Name = r.ZONEGROUP.NAME
					})
					.ToArray();
				var missingZoneGroupIds = zoneGroupIds.Except(existZoneGroups.Select(g => g.Id));
				validationMessages.AddRange(
					missingZoneGroupIds.Select(zoneGroup => new RuleValidationError
					{
						Message = "ZoneConditions.ZoneGroupInvalid",
						ValidationGroup = RuleValidationGroup.ZoneConditions,
						Value = zoneGroup
					}));
			}

			return validationMessages.ToArray();
		}
		public CompoundRule AddOrUpdateRule(int operatorId, Rule rule)
		{
			var compoundRule =
			(
				from cr in CompoundRule
					.Include(nameof(Orm::CompoundRule.CompoundRule_Vehicle))
					.Include(nameof(Orm::CompoundRule.CompoundRule_VehicleGroup))
					.Include(nameof(Orm::CompoundRule.CompoundRule_Sensor))
					.Include(nameof(Orm::CompoundRule.CompoundRule_Speed))
					.Include(nameof(Orm::CompoundRule.CompoundRule_Time))
					.Include(nameof(Orm::CompoundRule.CompoundRule_Zone))
					.Include(nameof(Orm::CompoundRule.CompoundRule_ZoneGroup))
					.Include(nameof(Orm::CompoundRule.CompoundRule_Message_Template))
					.Include(nameof(Orm::CompoundRule.CompoundRule_DataAbsence))
					.Include(nameof(Orm::CompoundRule.CompoundRule_TimePoint))
					.Include(nameof(Orm::CompoundRule.CompoundRule_ORDER))
				where cr.CompoundRule_ID == rule.Id
				select cr
			)
				.FirstOrDefault();
			if (compoundRule == null)
			{
				compoundRule = new CompoundRule();
				AddToCompoundRule(compoundRule);
			}

			compoundRule.Active      = rule.Enabled;
			compoundRule.Description = rule.Description;
			compoundRule.MotionStart = rule.MotionStart;

			switch (rule.ObjectIdType)
			{
				case IdType.Vehicle:
					if (compoundRule.CompoundRule_Vehicle.All(v => v.VEHICLE_ID != rule.ObjectId))
						Add(compoundRule, GetVehicle(rule.ObjectId));
					break;
				case IdType.VehicleGroup:
					if (compoundRule.CompoundRule_VehicleGroup.All(vg => vg.VEHICLEGROUP_ID != rule.ObjectId))
						Add(compoundRule, GetVehicleGroup(rule.ObjectId));
					break;
				default:
					throw new NotSupportedException(rule.ObjectIdType.ToString());
			}
			if (rule.OrderId.HasValue && compoundRule.CompoundRule_ORDER.All(o => o.ORDER_ID != rule.OrderId.Value))
				Add(compoundRule, GetOrderById(rule.OrderId.Value));

			if (rule.ProcessingBegAt.HasValue)
				compoundRule.ProcessingBegAt = rule.ProcessingBegAt;
			if (rule.ProcessingEndAt.HasValue)
				compoundRule.ProcessingEndAt = rule.ProcessingEndAt;

			// Правила на датчики
			Syncronize(this, compoundRule.CompoundRule_Sensor, rule.SensorConditions,
				(remote, local) =>
				{
					if (local.Id > 0)
					{
						if (!remote.CONTROLLER_SENSOR_LEGENDReference.IsLoaded)
							remote.CONTROLLER_SENSOR_LEGENDReference.Load();

						if (remote.CompoundRule             == compoundRule &&
							remote.Comparison_Type          == (int)local.Type &&
							remote.Value                    == local.Value &&
							remote.CONTROLLER_SENSOR_LEGEND == GetSensorLegend(local.Sensor))
							return;
					}

					remote.CompoundRule             = compoundRule;
					remote.Comparison_Type          = (int)local.Type;
					remote.Value                    = local.Value;
					remote.CONTROLLER_SENSOR_LEGEND = GetSensorLegend(local.Sensor);
				});
			// Правила на скорость
			Syncronize(this, compoundRule.CompoundRule_Speed, rule.SpeedConditions,
				(remote, local) =>
				{
					if (local.Id > 0)
					{
						if (remote.CompoundRule == compoundRule    &&
							remote.Type         == (int)local.Type &&
							remote.Value        == local.Value)
							return;
					}

					remote.CompoundRule = compoundRule;
					remote.Type         = (int)local.Type;
					remote.Value        = local.Value;
				});
			// Правила на время дня
			TimeZone timeZoneToUse = null;
			Syncronize(this, compoundRule.CompoundRule_Time, rule.TimeConditions,
				(remote, local) =>
				{
					if (timeZoneToUse == null)
					{
						var @operator = GetOperator(operatorId);
						if (!@operator.TimeZoneReference.IsLoaded)
							@operator.TimeZoneReference.Load();
						timeZoneToUse = @operator.TimeZone ?? GetDefaultTimeZone();
					}

					if (local.Id > 0)
					{
						if (!remote.TimeZoneReference.IsLoaded)
							remote.TimeZoneReference.Load();

						if (remote.CompoundRule == compoundRule      &&
							remote.Hours_From   == local.HoursFrom   &&
							remote.Minutes_From == local.MinutesFrom &&
							remote.Hours_To     == local.HoursTo     &&
							remote.Minutes_To   == local.MinutesTo   &&
							remote.TimeZone     == timeZoneToUse)
							return;
					}

					remote.CompoundRule = compoundRule;
					remote.Hours_From   = local.HoursFrom;
					remote.Minutes_From = local.MinutesFrom;
					remote.Hours_To     = local.HoursTo;
					remote.Minutes_To   = local.MinutesTo;
					remote.TimeZone     = timeZoneToUse;
				});
			// Правила на точки времени
			Syncronize(this, compoundRule.CompoundRule_TimePoint, rule.TimePointConditions,
				(remote, local) =>
				{
					if (local.Id > 0)
					{
						if (remote.CompoundRule == compoundRule &&
							remote.LogTime      == local.LogTime)
							return;
					}
					remote.CompoundRule = compoundRule;
					remote.LogTime      = local.LogTime;
				});
			// Правила на зоны и группы зон
			if (rule.ZoneConditions != null)
			{
				// Правила на зоны
				Syncronize(this, compoundRule.CompoundRule_Zone,
					rule.ZoneConditions.Where(zc => zc.ZoneIdType == IdType.Zone && GetZone(operatorId, zc.ZoneId).Any()),
					(remote, local) =>
					{
						if (local.Id > 0)
						{
							if (!remote.GEO_ZONEReference.IsLoaded)
								remote.GEO_ZONEReference.Load();

							if ((remote.CompoundRule    == compoundRule  &&
								remote.GEO_ZONE.ZONE_ID == local.ZoneId) &&
								remote.Type             == (int)local.EventType)
								return;
						}

						remote.CompoundRule = compoundRule;
						remote.GEO_ZONE     = GetZone(operatorId, local.ZoneId).First();
						remote.Type         = (int)local.EventType;
					});
				// Правила на группы зон
				Syncronize(this, compoundRule.CompoundRule_ZoneGroup,
					rule.ZoneConditions.Where(zc => zc.ZoneIdType == IdType.ZoneGroup && GetZoneGroup(operatorId, zc.ZoneId).Any()),
					(remote, local) =>
					{
						var zonegroup = GetZoneGroup(operatorId, local.ZoneId).First();

						if (local.Id > 0)
						{
							if (!remote.ZONEGROUPReference.IsLoaded)
								remote.ZONEGROUPReference.Load();

							if (remote.CompoundRule == compoundRule &&
								remote.ZONEGROUP    == zonegroup    &&
								remote.Type         == (int)local.EventType)
								return;
						}

						remote.CompoundRule = compoundRule;
						remote.ZONEGROUP    = zonegroup;
						remote.Type         = (int)local.EventType;
					});
			}
			// Правила на отсутствие данных
			Syncronize(this, compoundRule.CompoundRule_DataAbsence, rule.DataAbsenceConditions,
				(remote, local) =>
				{
					if (local.Id > 0)
					{
						if (remote.CompoundRule == compoundRule &&
							remote.Minutes      == local.Minutes)
							return;
					}

					remote.CompoundRule = compoundRule;
					remote.Minutes      = local.Minutes;
				});
			// Правила на отсутствие позиции
			Syncronize(this, compoundRule.CompoundRule_PositionAbsence, rule.PositionAbsenceConditions,
				(remote, local) =>
				{
					if (local.Id > 0)
					{
						if (remote.CompoundRule == compoundRule &&
							remote.Minutes      == local.Minutes)
							return;
					}

					remote.CompoundRule = compoundRule;
					remote.Minutes      = local.Minutes;
				});

			Syncronize(this, compoundRule.CompoundRule_Message_Template, rule.Subscriptions,
				(remote, local) =>
				{
					var @operator = GetOperator(local.OperatorId);
					var messageTemplate = MESSAGE_TEMPLATE.First(mt => mt.NAME == Msg::MessageTemplate.GenericNotification);
					var normalizedPhones = local.Phones != null
						? local.Phones.Select(ContactHelper.GetNormalizedPhone).ToArray()
						: new string[0];

					var operatorEmailIds = local.EmailIds != null
						? Operator_Contact.Where(c => c.OPERATOR.OPERATOR_ID == operatorId && local.EmailIds.Contains(c.Contact_ID)).Select(c => c.Contact_ID).ToArray()
						: new int[0];
					var operatorPhoneIds = local.PhoneIds != null
						? Operator_Contact.Where(c => c.OPERATOR.OPERATOR_ID == operatorId && local.PhoneIds.Contains(c.Contact_ID)).Select(c => c.Contact_ID).ToArray()
						: new int[0];
					var notOperatorPhoneIds = local.Phones != null
						? Contact.Where(c => c.Type == (int)ContactType.Phone && normalizedPhones.Contains(c.Value)).Select(c => c.ID).ToArray()
						: new int[0];
					var notOperatorEmailIds = local.Emails != null
						? Contact.Where(c => c.Type == (int)ContactType.Email && local.Emails.Contains(c.Value)).Select(c => c.ID).ToArray()
						: new int[0];

					var contactIds = new HashSet<int>();
					contactIds.AddRange(operatorEmailIds);
					contactIds.AddRange(operatorPhoneIds);
					contactIds.AddRange(notOperatorPhoneIds);
					contactIds.AddRange(notOperatorEmailIds);

					if (local.Id > 0)
					{
						if (!remote.OPERATORReference.IsLoaded)
							remote.OPERATORReference.Load();

						if (remote.OPERATOR.OPERATOR_ID != operatorId)
							return; //Можно редактировать только свои правила

						if (!remote.MESSAGE_TEMPLATEReference.IsLoaded)
							remote.MESSAGE_TEMPLATEReference.Load();

						if (!remote.CompoundRule.Contacts.IsLoaded)
							remote.CompoundRule.Contacts.Load();

						if (remote.CompoundRule      == compoundRule            &&
							remote.OPERATOR          == @operator               &&
							remote.MESSAGE_TEMPLATE  == messageTemplate         &&
							remote.ToOwnPhone        == local.ToOwnPhone        &&
							remote.ToOwnPhoneMaxQty  == local.ToOwnPhoneMaxQty  &&
							remote.ToOwnEmail        == local.ToOwnEmail        &&
							remote.ToOwnEmailMaxQty  == local.ToOwnEmailMaxQty  &&
							remote.ToAppClient       == local.ToAppClient       &&
							remote.ToAppClientMaxQty == local.ToAppClientMaxQty &&
							remote.CompoundRule.Contacts.Select(c => c.Contact_ID).OrderBy(x => x).SequenceEqual(contactIds))
							return; //изменения отсутствуют
					}
					else
					{
						remote.CompoundRule     = compoundRule;
						remote.OPERATOR         = @operator;
						remote.MESSAGE_TEMPLATE = messageTemplate;
					}

					remote.ToOwnPhone        = local.ToOwnPhone;
					remote.ToOwnPhoneMaxQty  = local.ToOwnPhoneMaxQty;
					remote.ToOwnEmail        = local.ToOwnEmail;
					remote.ToOwnEmailMaxQty  = local.ToOwnEmailMaxQty;
					remote.ToAppClient       = local.ToAppClient;
					remote.ToAppClientMaxQty = local.ToAppClientMaxQty;

					if (contactIds.Any())
					{
						foreach (var contactId in contactIds.Where(contactId => remote.CompoundRule.Contacts.All(x => x.Contact_ID != contactId)))
						{
							remote.CompoundRule.Contacts.Add(new CompoundRule_Operator_Contact
							{
								Operator_ID = operatorId,
								Contact_ID  = contactId
							});
						}
					}

					var contactsToDelete = remote.CompoundRule
						.Contacts
						.Where(remoteContact =>
							remoteContact.OPERATOR == @operator &&
							!contactIds.Contains(remoteContact.Contact_ID))
						.ToList();

					foreach (var contact in contactsToDelete)
						remote.CompoundRule.Contacts.Remove(contact);
				});

			return compoundRule;
		}
		public Rule CreateAlarmRule(int operatorId, int vehicleId)
		{
			var ruleSubscription = new RuleSubscription
			{
				OperatorId  = operatorId,
				ToAppClient = true,
				PhoneIds    = new int[0],
				EmailIds    = new int[0]
			};
			var sensorCondition = new SensorCondition
			{
				Sensor = SensorLegend.Alarm,
				Type   = ScalarComparisonType.Equal,
				Value  = 1
			};

			var rule = new Rule
			{
				Subscriptions    = new[] { ruleSubscription },
				SensorConditions = new[] { sensorCondition },
				ZoneConditions   = new ZoneCondition[0],
				Enabled          = true,
				Description      = string.Empty,
				ObjectId         = vehicleId,
				ObjectIdType     = IdType.Vehicle,
			};

			return rule;
		}
		public IQueryable<CompoundRule> DefaultAlarmRules(int vehicleId)
		{
			return CompoundRule
				.Where(r =>
					r.CompoundRule_Sensor.Count == 1 &&
					r.CompoundRule_Sensor.Any(s => s.CONTROLLER_SENSOR_LEGEND.Number == (int)SensorLegend.Alarm) &&
					r.CompoundRule_Vehicle.Any(v => v.VEHICLE_ID == vehicleId));
		}
		public bool AlarmRuleExists(int operatorId, int vehicleId)
		{
			return DefaultAlarmRules(vehicleId)
				.Any(r =>
					r.CompoundRule_Message_Template.Count == 1 &&
					r.CompoundRule_Message_Template.Any(
						mt => mt.OPERATOR.OPERATOR_ID == operatorId && mt.ToAppClient));
		}
		public void AddVehicleZoneCompoundRule(int operatorId, int vehicleId, int zoneId, ZoneEventType zoneEventType)
		{
			var compoundRule = CompoundRule
				.Include(nameof(Orm::CompoundRule.CompoundRule_Zone))
				.Include(nameof(Orm::CompoundRule.CompoundRule_Zone) + "." + nameof(Orm::CompoundRule_Zone.GEO_ZONE))
				.FirstOrDefault(x => x.CompoundRule_Message_Template
					.Any(mt => mt.OPERATOR.OPERATOR_ID == operatorId) && x.CompoundRule_Vehicle
						.Any(v => v.VEHICLE_ID == vehicleId) && x.CompoundRule_Zone
							.Any(z => z.Type == (int) zoneEventType));

			if (compoundRule == null)
			{
				compoundRule = new Orm::CompoundRule();
				CompoundRule.AddObject(compoundRule);
				compoundRule.CompoundRule_Message_Template.Add(
					new Orm::CompoundRule_Message_Template
					{
						OPERATOR         = GetOperator(operatorId),
						MESSAGE_TEMPLATE = MESSAGE_TEMPLATE.First(mt => mt.NAME == Msg::MessageTemplate.GenericNotification),
						ToAppClient      = true,
					});
			}

			if (compoundRule.CompoundRule_Zone.Any(x =>
				x.Type == (int) zoneEventType && x.GEO_ZONE.ZONE_ID == zoneId))
				return;

			compoundRule.CompoundRule_Zone.Add(
				new Orm::CompoundRule_Zone
				{
					GEO_ZONE = GetZone(zoneId).First(),
					Type     = (int) zoneEventType,
				});

			SaveChangesByOperator(operatorId);
		}
		public IQueryable<CompoundRule> GetVehicleZoneCompoundRules(int operatorId, int vehicleId, int zoneId)
		{
			return CompoundRule
				.Where(x =>
					x.CompoundRule_Zone
						.Any(z => z.GEO_ZONE.ZONE_ID == zoneId) &&
					x.CompoundRule_Message_Template
						.Any(m => m.OPERATOR.OPERATOR_ID == operatorId) &&
					x.CompoundRule_Vehicle
						.Any(v => v.VEHICLE_ID == vehicleId));
		}
	}
}