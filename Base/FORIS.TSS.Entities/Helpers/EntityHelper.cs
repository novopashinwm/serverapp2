﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using FORIS.TSS.Common.Database;

namespace FORIS.TSS.EntityModel.Helpers
{
	public static class EntityHelper
	{
		public static SqlCommand ToSqlCommand(this Entities entities, ObjectQuery query, SqlConnection connection)
		{
			var sql = query.ToTraceString();
			var parameters = query.Parameters.Select(p =>
				new SqlParameter(p.Name, DbTypeConverter.GetDbType(p.ParameterType))
				{
					Value = p.Value
				}).ToArray();
			var command = new SqlCommand(sql, connection);
			command.Parameters.AddRange(parameters);
			return command;
		}

		public static string GetConnectionString()
		{
			using (var entities = new Entities())
				return ((EntityConnection)entities.Connection).StoreConnection.ConnectionString;
		}
		public static DataTable ToDataTable<T>(IEnumerable<T> items, string tableName = null)
		{
			var dataTable = new DataTable(null != tableName ? tableName : typeof(T).Name);

			//Get all the properties
			var props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
			foreach (PropertyInfo prop in props)
			{
				//Setting column names as Property names
				var dataCol = dataTable.Columns.Add(prop.Name);

				if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
				{
					dataCol.DataType    = Nullable.GetUnderlyingType(prop.PropertyType);
					dataCol.AllowDBNull = true;
				}
				else
				{
					dataCol.DataType    = prop.PropertyType;
					dataCol.AllowDBNull = !prop.PropertyType.IsValueType;
				}
			}
			foreach (T item in items)
			{
				var values = new object[props.Length];
				for (int i = 0; i < props.Length; i++)
				{
					//inserting property values to datatable rows
					values[i] = props[i].GetValue(item, null);
				}
				dataTable.Rows.Add(values);
			}
			//put a breakpoint here and check datatable
			return dataTable;
		}
	}
}