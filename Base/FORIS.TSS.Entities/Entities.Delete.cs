﻿using System;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using FORIS.TSS.EntityModel.History;

namespace FORIS.TSS.EntityModel
{
	public partial class Entities
	{
		public void DeleteAsid(Asid asid)
		{
			if (asid == null)
				throw new ArgumentNullException(nameof(asid));

			//Удаляем все услуги
			if (!asid.Billing_Service.IsLoaded)
				asid.Billing_Service.Load();

			if (asid.Billing_Service != null && asid.Billing_Service.Count > 0)
			{
				foreach (Billing_Service billingservice in asid.Billing_Service.ToArray())
				{
					DeleteBillingService(billingservice);
				}
			}

			//Удаляем все блокировки
			if (!asid.Billing_BlockingReference.IsLoaded)
				asid.Billing_BlockingReference.Load();

			if (asid.Billing_Blocking != null)
			{
				var blocking = asid.Billing_Blocking;
				DeleteObject(blocking);
			}

			//обнуляем asid_id в MLPController
			if (!asid.MLP_ControllerReference.IsLoaded)
				asid.MLP_ControllerReference.Load();

			if (asid.MLP_Controller != null)
			{
				DeleteObject(asid.MLP_Controller);
			}

			if (!asid.AsidMQF.IsLoaded)
				asid.AsidMQF.Load();
			foreach (var asidMQF in asid.AsidMQF.ToArray())
				AsidMQF.DeleteObject(asidMQF);

			//Удаляем asid
			DeleteObject(asid);
		}
		public void DeleteBillingService(Billing_Service billingService)
		{
			if (billingService == null)
				throw new ArgumentNullException(nameof(billingService));

			if (!billingService.ScheduledTaskReference.IsLoaded)
				billingService.ScheduledTaskReference.Load();

			if (billingService.ScheduledTask != null)
			{
				SCHEDULERQUEUE.DeleteObject(billingService.ScheduledTask);
			}

			if (!billingService.RenderedServices.IsLoaded)
				billingService.RenderedServices.Load();

			foreach (var rs in billingService.RenderedServices.ToArray())
				Rendered_Service.DeleteObject(rs);

			Billing_Service.DeleteObject(billingService);
		}
		public void DeleteMapping(CONTROLLER_SENSOR_MAP map)
		{
			if (map == null)
				throw new ArgumentNullException(nameof(map));

			DeleteObject(map);
		}
		public void Delete(Operator_Contact operatorContact)
		{
			if (operatorContact == null)
				throw new ArgumentNullException(nameof(operatorContact));

			LoadAndProcess(
				CompoundRule_Operator_Contact
					.Where(c =>
						c.Operator_ID == operatorContact.Operator_ID &&
						c.Contact_ID  == operatorContact.Contact_ID),
					item => CompoundRule_Operator_Contact.DeleteObject(item));
			DeleteObject(operatorContact);
		}
		public void DeleteCompoundRule(CompoundRule compoundRule)
		{
			if (compoundRule == null)
				throw new ArgumentNullException(nameof(compoundRule));
			LoadAndProcess(compoundRule.CompoundRule_Message_Template, item => CompoundRule_Message_Template.DeleteObject(item));
			LoadAndProcess(compoundRule.CompoundRule_Sensor,           item => CompoundRule_Sensor.DeleteObject(item));
			LoadAndProcess(compoundRule.CompoundRule_Speed,            item => CompoundRule_Speed.DeleteObject(item));
			LoadAndProcess(compoundRule.CompoundRule_Time,             item => CompoundRule_Time.DeleteObject(item));
			LoadAndProcess(compoundRule.CompoundRule_DataAbsence,      item => CompoundRule_DataAbsence.DeleteObject(item));
			LoadAndProcess(compoundRule.CompoundRule_PositionAbsence,  item => CompoundRule_PositionAbsence.DeleteObject(item));
			LoadAndProcess(compoundRule.CompoundRule_Zone,             item => CompoundRule_Zone.DeleteObject(item));
			LoadAndProcess(compoundRule.CompoundRule_ZoneGroup,        item => CompoundRule_ZoneGroup.DeleteObject(item));
			LoadAndProcess(compoundRule.CompoundRule_TimePoint,        item => CompoundRule_TimePoint.DeleteObject(item));
			LoadAndProcess(compoundRule.CompoundRule_ORDER,            item => Remove(compoundRule, item));
			LoadAndProcess(compoundRule.EMAIL,                         item => Remove(compoundRule, item));
			LoadAndProcess(compoundRule.Phone,                         item => Remove(compoundRule, item));
			LoadAndProcess(compoundRule.CompoundRule_Vehicle,          item => Remove(compoundRule, item));
			LoadAndProcess(compoundRule.CompoundRule_VehicleGroup,     item => Remove(compoundRule, item));
			LoadAndProcess(compoundRule.Contacts,                      item => compoundRule.Contacts.Remove(item));
			CompoundRule.DeleteObject(compoundRule);
		}
		private void Remove(CompoundRule compoundRule, EMAIL email)
		{
			if (!compoundRule.EMAIL.IsLoaded)
				compoundRule.EMAIL.Load();
			if (compoundRule.EMAIL.Remove(email))
				LogHistory(InsertAction, compoundRule, email);
		}
		private void LogHistory(string action, CompoundRule compoundRule, EMAIL email)
		{
			var h = new H_CompoundRule_SubscriptionEmail
			{
				ACTION          = action,
				Email_ID        = email.EMAIL_ID,
				CompoundRule_ID = compoundRule.CompoundRule_ID
			};
			_historyEntities.H_CompoundRule_SubscriptionEmail.AddObject(h);
			_historicalItems.Add(h);
		}
		private void LogHistory(string action, CompoundRule compoundRule, Phone phone)
		{
			var h = new H_CompoundRule_SubscriptionPhone
			{
				ACTION = action,
				Phone_ID = phone.Phone_ID,
				CompoundRule_ID = compoundRule.CompoundRule_ID
			};
			_historyEntities.H_CompoundRule_SubscriptionPhone.AddObject(h);
			_historicalItems.Add(h);
		}
		private void Remove(CompoundRule compoundRule, Phone phone)
		{
			if (!compoundRule.Phone.IsLoaded)
				compoundRule.Phone.Load();
			if (compoundRule.Phone.Remove(phone))
				LogHistory(InsertAction, compoundRule, phone);
		}
		private static void LoadAndProcess<T>(EntityCollection<T> collection, Action<T> doAction) where T : EntityObject
		{
			if (!collection.IsLoaded)
				collection.Load();
			var array = collection.ToArray();
			foreach (var item in array)
				doAction(item);
		}
		private static void LoadAndProcess<T>(IQueryable<T> collection, Action<T> doAction) where T : EntityObject
		{
			var array = collection.ToArray();
			foreach (var item in array)
				doAction(item);
		}
		public void DeleteSensorValueChange(SensorValueChange sensorValueChange)
		{
			DeleteObject(sensorValueChange);
		}
		public void DeleteWebPoint(WEB_POINT point)
		{
			if (!point.MAP_VERTEXReference.IsLoaded)
				point.MAP_VERTEXReference.Load();
			if (point.MAP_VERTEX != null)
				MAP_VERTEX.DeleteObject(point.MAP_VERTEX);
			WEB_POINT.DeleteObject(point);
		}
		public void DeleteVehicleGroup(VEHICLEGROUP vehiclegroup)
		{
			vehiclegroup.OPERATOR_VEHICLEGROUP.Load();
			foreach (var item in vehiclegroup.OPERATOR_VEHICLEGROUP.ToList())
				DeleteObject(item);

			vehiclegroup.OPERATORGROUP_VEHICLEGROUP.Load();
			foreach (var item in vehiclegroup.OPERATORGROUP_VEHICLEGROUP.ToList())
				DeleteObject(item);

			vehiclegroup.SCHEDULERQUEUE.Load();
			foreach (var sq in vehiclegroup.SCHEDULERQUEUE.ToList())
				DeleteObject(sq);

			vehiclegroup.VEHICLE.Load();
			foreach (var vehicle in vehiclegroup.VEHICLE.ToArray())
				Remove(vehiclegroup, vehicle);

			vehiclegroup.ZONE_VEHICLEGROUP.Load();
			foreach (var zvg in vehiclegroup.ZONE_VEHICLEGROUP.ToList())
				DeleteObject(zvg);

			vehiclegroup.VEHICLEGROUP_RULE.Load();
			foreach (var vgr in vehiclegroup.VEHICLEGROUP_RULE.ToList())
				DeleteObject(vgr);

			if (!vehiclegroup.CompoundRule.IsLoaded)
				vehiclegroup.CompoundRule.Load();
			foreach (var cr in vehiclegroup.CompoundRule.ToArray())
				DeleteCompoundRule(cr);

			DeleteObject(vehiclegroup);
		}
		internal void DeletePhone(Phone phone)
		{
			if (!phone.Confirmations.IsLoaded)
				phone.Confirmations.Load();

			foreach (var pc in phone.Confirmations.ToList())
				Phone_Confirmation.DeleteObject(pc);

			if (!phone.SCHEDULEREVENT.IsLoaded)
				phone.SCHEDULEREVENT.Load();

			var phoneschedules = phone.SCHEDULEREVENT.ToArray();
			foreach (var psc in phoneschedules)
				psc.Phone.Remove(phone);

			if (!phone.SCHEDULERQUEUE.IsLoaded)
				phone.SCHEDULERQUEUE.Load();

			var phonequeues = phone.SCHEDULERQUEUE.ToArray();
			foreach (var psc in phonequeues)
			{
				if (!psc.Phone.IsLoaded)
					psc.Phone.Load();
				psc.Phone.Remove(phone);

				if (!psc.Phone.Any())
					DeleteObject(psc);
			}

			if (!phone.CompoundRule.IsLoaded)
				phone.CompoundRule.Load();
			var subscribtion = phone.CompoundRule.ToArray();
			foreach (var cr in subscribtion)
			{
				if (!cr.Phone.IsLoaded)
					cr.Phone.Load();
				cr.Phone.Remove(phone);
			}

			if (!phone.PhoneMQF.IsLoaded)
				phone.PhoneMQF.Load();
			foreach (var phoneMQF in phone.PhoneMQF.ToArray())
				PhoneMQF.DeleteObject(phoneMQF);

			Phone.DeleteObject(phone);
		}
		internal void DeletePhone(Phone phone, bool deleteAll)
		{
			if (deleteAll)
			{
				DeletePhone(phone);
				return;
			}

			if (!phone.Confirmations.IsLoaded)
				phone.Confirmations.Load();

			foreach (var pc in phone.Confirmations.ToList())
				Phone_Confirmation.DeleteObject(pc);

			if (!phone.SCHEDULEREVENT.IsLoaded)
				phone.SCHEDULEREVENT.Load();

			var phoneschedules = phone.SCHEDULEREVENT.ToArray();
			foreach (var psc in phoneschedules)
				psc.Phone.Remove(phone);

			if (!phone.SCHEDULERQUEUE.IsLoaded)
				phone.SCHEDULERQUEUE.Load();

			var phonequeues = phone.SCHEDULERQUEUE.ToArray();
			foreach (var psc in phonequeues)
			{
				if (!psc.Phone.IsLoaded)
					psc.Phone.Load();
				psc.Phone.Remove(phone);

				if (!psc.Phone.Any())
					DeleteObject(psc);
			}

			Phone.DeleteObject(phone);
		}
		/// <summary>Удаляет контроллер и связанные дочерние сущности</summary>
		/// <param name="controller">Удаляемый экземпляр</param>
		public void DeleteController(CONTROLLER controller)
		{
			if (!controller.CONTROLLER_INFO.CONTROLLERReference.IsLoaded)
				controller.CONTROLLER_INFO.CONTROLLERReference.Load();
			CONTROLLER_INFO.DeleteObject(controller.CONTROLLER_INFO);
			CONTROLLER.DeleteObject(controller);

			//TODO: удалять настройки датчиков и т.п.
		}
		public void DeleteZone(GEO_ZONE geoZone)
		{
			if (geoZone == null)
				throw new ArgumentNullException(nameof(geoZone));

			DeleteCollection(OPERATORGROUP_ZONE, geoZone.OPERATORGROUP_ZONE);
			DeleteCollection(OPERATOR_ZONE,      geoZone.OPERATOR_ZONE);
			DeleteCollection(ZONE_VEHICLE,       geoZone.ZONE_VEHICLE);
			DeleteCollection(ZONE_VEHICLEGROUP,  geoZone.ZONE_VEHICLEGROUP);
			DeleteCollection(CompoundRule_Zone,  geoZone.CompoundRule_Zone);
			DeleteCollection(ZONEGROUP_ZONE,     geoZone.ZONEGROUP_ZONE);
			DeleteCollection(Zone_Matrix,        geoZone.Zone_Matrix);
			DeleteCollection(
				GEO_ZONE_PRIMITIVE,
				geoZone.GEO_ZONE_PRIMITIVE,
				gzp => DeleteReference(
					ZONE_PRIMITIVE,
					gzp.ZONE_PRIMITIVEReference,
					zp => DeleteCollection(
						ZONE_PRIMITIVE_VERTEX,
						zp.ZONE_PRIMITIVE_VERTEX,
						zpv => DeleteReference(MAP_VERTEX, zpv.MAP_VERTEXReference))));

			GEO_ZONE.DeleteObject(geoZone);
		}
		private void DeleteCollection<T>(ObjectSet<T> objectSet, EntityCollection<T> entityCollection) where T : class
		{
			if (!entityCollection.IsLoaded)
				entityCollection.Load();
			foreach (var entity in entityCollection.ToArray())
				objectSet.DeleteObject(entity);
		}
		private void DeleteCollection<T>(ObjectSet<T> objectSet, EntityCollection<T> entityCollection, Action<T> beforeRemove) where T : class
		{
			if (!entityCollection.IsLoaded)
				entityCollection.Load();
			foreach (var entity in entityCollection.ToArray())
			{
				beforeRemove(entity);
				objectSet.DeleteObject(entity);
			}
		}
		private void DeleteReference<T>(ObjectSet<T> objectSet, EntityReference<T> entityReference) where T : class
		{
			DeleteReference(objectSet, entityReference, null);
		}
		private void DeleteReference<T>(ObjectSet<T> objectSet, EntityReference<T> entityReference, Action<T> beforeRemove) where T : class
		{
			if (!entityReference.IsLoaded)
				entityReference.Load();
			var entity = entityReference.Value;
			if (entity == null)
				return;

			if (beforeRemove != null)
				beforeRemove(entity);

			objectSet.DeleteObject(entity);
		}
		public void DeleteMessage(MESSAGE message)
		{
			DeleteCollection(MESSAGE_FIELD, message.MESSAGE_FIELD);
			DeleteCollection(MESSAGE_OPERATOR, message.MESSAGE_OPERATOR);
			MESSAGE.DeleteObject(message);
		}
		public void DeleteVehicleZoneGroup(VEHICLE vehicle, ZONEGROUP zoneGroup)
		{
			if (!vehicle.ZoneGroups.IsLoaded)
				vehicle.ZoneGroups.Load();
			if (vehicle.ZoneGroups.Remove(zoneGroup))
				LogHistory(InsertAction, vehicle, zoneGroup);
		}
		public void Delete(SCHEDULERQUEUE schedulerqueue)
		{
			schedulerqueue.Contact.Clear();
			schedulerqueue.EMAIL.Clear();
			SCHEDULERQUEUE.DeleteObject(schedulerqueue);
		}
		public void ClearCollection<T>(EntityCollection<T> collection) where T : class
		{
			if (!collection.IsLoaded)
				collection.Load();
			collection.Clear();
		}
	}
}