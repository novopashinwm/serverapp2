﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic;

namespace FORIS.TSS.EntityModel
{
	public partial class Entities
	{
		/// <summary> Проверяет есть ли у оператора все указанные права на vehicle </summary>
		public bool IsAllowedAll(OPERATOR ormOperator, VEHICLE ormVehicle, params SystemRight[] rights)
		{
			return IsAllowedVehicleAll(ormOperator.OPERATOR_ID, ormVehicle.VEHICLE_ID, rights);
		}
		/// <summary> Проверяет есть ли у оператора все указанные права на vehiclegroup </summary>
		public bool IsAllowedAll(OPERATOR ormOperator, VEHICLEGROUP ormVehicleGroup, params SystemRight[] rights)
		{
			return IsAllowedVehicleGroupAll(ormOperator.OPERATOR_ID, ormVehicleGroup.VEHICLEGROUP_ID, rights);
		}
		/// <summary> Проверяет есть ли у оператора все указанные права на vehicle </summary>
		/// <remarks>
		/// При кажущейся не оптимальности делается перебор по каждому праву,
		/// работает быстрее других аналогов из-за одинаковости плана для этого метода и IsAllowedVehicleAny.
		/// Выполнение одинаковых запросов позволяет СУБД не строить планы, и держать данные в кэш.
		/// Также влияет, тот факт, что в основном проверка идет по одному праву или двум.
		/// </remarks>
		public bool IsAllowedVehicleAll(int operatorId, int vehicleId, params SystemRight[] rights)
		{
			return 0 < rights.Length && rights
				.Select(r => (int)r)
				.All(r => 0 < v_operator_vehicle_right
					.Count(v =>
						v.operator_id == operatorId &&
						v.vehicle_id  == vehicleId  &&
						v.right_id    == r));
		}
		/// <summary> Проверяет есть ли у оператора любое из указанных прав на vehicle </summary>
		/// <remarks>
		/// При кажущейся не оптимальности делается перебор по каждому праву,
		/// работает быстрее других аналогов из-за одинаковости плана для этого метода и IsAllowedVehicleAll.
		/// Выполнение одинаковых запросов позволяет СУБД не строить планы, и держать данные в кэш.
		/// Также влияет, тот факт, что в основном проверка идет по одному праву или двум.
		/// </remarks>
		public bool IsAllowedVehicleAny(int operatorId, int vehicleId, params SystemRight[] rights)
		{
			return 0 < rights.Length && rights
				.Select(r => (int)r)
				.Any(r => 0 < v_operator_vehicle_right
					.Count(v =>
						v.operator_id == operatorId &&
						v.vehicle_id  == vehicleId  &&
						v.right_id    == r));
		}
		/// <summary> Проверяет есть ли у оператора все указанные права на VehicleGroup </summary>
		public bool IsAllowedVehicleGroupAll(int operatorId, int vehicleGroupId, params SystemRight[] rights)
		{
			return 0 < rights.Length && rights
				.Select(r => (int)r)
				.All(r => 0 < v_operator_vehicle_groups_right
					.Count(v =>
						v.operator_id     == operatorId     &&
						v.vehiclegroup_id == vehicleGroupId &&
						v.right_id        == r));
		}
		/// <summary> Проверяет есть ли у оператора любое из указанных прав на VehicleGroup </summary>
		public bool IsAllowedVehicleGroupAny(int operatorId, int vehicleGroupId, params SystemRight[] rights)
		{
			return 0 < rights.Length && rights
				.Select(r => (int)r)
				.Any(r => 0 < v_operator_vehicle_groups_right
					.Count(v =>
						v.operator_id     == operatorId     &&
						v.vehiclegroup_id == vehicleGroupId &&
						v.right_id        == r));
		}

		/// <summary> Проверяет есть ли у оператора все указанные права на геозону </summary>
		public bool IsAllowedZoneAll(int operatorId, int zoneId, params SystemRight[] rights)
		{
			return 0 < rights.Length && rights
				.Select(r => (int)r)
				.All(r => 0 < v_operator_zone_right
					.Count(z =>
						z.operator_id == operatorId &&
						z.ZONE_ID     == zoneId     &&
						z.right_id    == r));
		}
		/// <summary> Проверяет есть ли у оператора любое из указанных прав на геозону </summary>
		public bool IsAllowedZoneAny(int operatorId, int zoneId, params SystemRight[] rights)
		{
			return 0 < rights.Length && rights
				.Select(r => (int)r)
				.Any(r => 0 < v_operator_zone_right
					.Count(z =>
						z.operator_id == operatorId &&
						z.ZONE_ID     == zoneId     &&
						z.right_id    == r));
		}
		/// <summary> Проверяет есть ли у оператора все указанные права на группу геозон </summary>
		public bool IsAllowedZoneGroupAll(int operatorId, int zoneGroupId, params SystemRight[] rights)
		{
			return 0 < rights.Length && rights
				.Select(r => (int)r)
				.All(r => 0 < v_operator_zone_groups_right
					.Count(g =>
						g.operator_id  == operatorId  &&
						g.zonegroup_id == zoneGroupId &&
						g.right_id     == r));
		}
		/// <summary> Проверяет есть ли у оператора любое из указанных прав на  группу геозон </summary>
		public bool IsAllowedZoneGroupAny(int operatorId, int zoneGroupId, params SystemRight[] rights)
		{
			return 0 < rights.Length && rights
				.Select(r => (int)r)
				.Any(r => 0 < v_operator_zone_groups_right
					.Count(g =>
						g.operator_id  == operatorId  &&
						g.zonegroup_id == zoneGroupId &&
						g.right_id     == r));
		}
		/// <summary> Добавление указанных прав на трекер другому оператору </summary>
		/// <param name="ormOperator">Кому дать права</param>
		/// <param name="ormVehicle">На какой трекер</param>
		/// <param name="ormRights">список прав</param>
		public void Allow(OPERATOR ormOperator, VEHICLE ormVehicle, params RIGHT[] ormRights)
		{
			if (!ormOperator.OPERATOR_VEHICLE.IsLoaded)
				ormOperator.OPERATOR_VEHICLE.Load();

			foreach (var ormRight in ormRights)
			{
				var permission = ormOperator.OPERATOR_VEHICLE
					.FirstOrDefault(ov =>
						ov.VEHICLE_ID == ormVehicle.VEHICLE_ID &&
						ov.RIGHT_ID   == ormRight.RIGHT_ID);
				if (null != permission)
				{
					if (!permission.ALLOWED)
						permission.ALLOWED = true;
				}
				else
				{
					OPERATOR_VEHICLE.AddObject(new OPERATOR_VEHICLE
					{
						OPERATOR = ormOperator,
						VEHICLE  = ormVehicle,
						RIGHT    = ormRight,
						ALLOWED  = true,
					});
				}
			}
		}
		/// <summary> Добавление указанных прав на зону оператору </summary>
		/// <param name="ormOperator">Кому дать права</param>
		/// <param name="ormGeoZone">На какой трекер</param>
		/// <param name="rights">список прав</param>
		public void Allow(OPERATOR ormOperator, GEO_ZONE ormGeoZone, params SystemRight[] rights)
		{
			if (ormOperator == null)
				throw new ArgumentNullException("operator");
			if (ormGeoZone == null)
				throw new ArgumentNullException("zone");
			if (rights == null || rights.Length == 0)
				return;

			var zoneClosure = ormGeoZone;
			if (!ormOperator.OPERATOR_ZONE.IsLoaded)
				ormOperator.OPERATOR_ZONE.Load();

			foreach (var right in rights)
			{
				var rightClosure = right;
				var existingPermission = ormOperator.OPERATOR_ZONE
					.FirstOrDefault(ov =>
						ov.ZONE_ID  == zoneClosure.ZONE_ID &&
						ov.RIGHT_ID == (int)right);

				if (existingPermission != null)
				{
					if (!existingPermission.ALLOWED)
						existingPermission.ALLOWED = true;
					continue;
				}

				var newPermission = new OPERATOR_ZONE
				{
					OPERATOR = ormOperator,
					GEO_ZONE = zoneClosure,
					RIGHT_ID = (int)right,
					ALLOWED  = true,
				};
				OPERATOR_ZONE.AddObject(newPermission);
			}
		}
		public void Disallow(OPERATOR ormOperator, GEO_ZONE ormGeoZone, params SystemRight[] rights)
		{
			if (rights == null || rights.Length == 0)
				rights = (from access in v_operator_zone_right
						  where access.operator_id == ormOperator.OPERATOR_ID &&
								access.ZONE_ID == ormGeoZone.ZONE_ID
						  select (SystemRight)access.right_id).ToArray();

			var systemRightsIds = rights.Select(r => (int)r).ToArray();
			if (systemRightsIds.Length == 0)
				return;

			var existingPermissions = OPERATOR_ZONE.Where(
				p => p.OPERATOR_ID == ormOperator.OPERATOR_ID &&
					 p.ZONE_ID == ormGeoZone.ZONE_ID &&
					 systemRightsIds.Contains(p.RIGHT_ID));

			foreach (var permission in existingPermissions.ToArray())
			{
				if (permission.ALLOWED)
					permission.ALLOWED = false;
			}

			var notExistingRightIds = systemRightsIds.Except(existingPermissions.Select(p => p.RIGHT_ID).ToArray());

			foreach (var rightId in notExistingRightIds)
			{
				OPERATOR_ZONE.AddObject(
					new OPERATOR_ZONE
					{
						OPERATOR = ormOperator,
						GEO_ZONE = ormGeoZone,
						RIGHT_ID = rightId,
						ALLOWED = false
					});
			}
		}
		/// <summary> Добавление указанных прав на зону оператору </summary>
		/// <param name="operator">Кому дать права</param>
		/// <param name="zone">На какой трекер</param>
		/// <param name="rights">список прав</param>
		public void Allow(OPERATOR @operator, ZONEGROUP zoneGroup, params SystemRight[] rights)
		{
			if (!@operator.OPERATOR_ZONEGROUP.IsLoaded)
				@operator.OPERATOR_ZONEGROUP.Load();

			foreach (var right in rights)
			{
				var existingPermission = @operator.OPERATOR_ZONEGROUP.FirstOrDefault(
					ov => ov.ZONEGROUP_ID == zoneGroup.ZONEGROUP_ID && ov.RIGHT_ID == (int)right);

				if (existingPermission != null)
				{
					if (!existingPermission.ALLOWED)
						existingPermission.ALLOWED = true;
					continue;
				}

				var newPermission = new OPERATOR_ZONEGROUP
				{
					OPERATOR  = @operator,
					ZONEGROUP = zoneGroup,
					RIGHT_ID  = (int)right,
					ALLOWED   = true,
				};

				OPERATOR_ZONEGROUP.AddObject(newPermission);
			}
		}
		public void Disallow(OPERATOR @operator, ZONEGROUP zoneGroup, params SystemRight[] systemRights)
		{
			if (systemRights == null || systemRights.Length == 0)
				systemRights = (from access in v_operator_zone_groups_right
								where access.operator_id == @operator.OPERATOR_ID &&
									  access.zonegroup_id == zoneGroup.ZONEGROUP_ID
								select (SystemRight) access.right_id).ToArray();

			var systemRightsIds = systemRights.Select(r => (int)r).ToArray();
			if (systemRightsIds.Length == 0)
				return;

			var existingPermissions = OPERATOR_ZONEGROUP.Where(
				p => p.OPERATOR_ID == @operator.OPERATOR_ID &&
					 p.ZONEGROUP_ID == zoneGroup.ZONEGROUP_ID &&
					 systemRightsIds.Contains(p.RIGHT_ID));

			foreach (var permission in existingPermissions.ToArray())
			{
				if (permission.ALLOWED)
					permission.ALLOWED = false;
			}

			var notExistingRightIds = systemRightsIds.Except(existingPermissions.Select(p => p.RIGHT_ID).ToArray());

			foreach (var rightId in notExistingRightIds)
			{
				OPERATOR_ZONEGROUP.AddObject(
					new OPERATOR_ZONEGROUP
					{
						OPERATOR  = @operator,
						ZONEGROUP = zoneGroup,
						RIGHT_ID  = rightId,
						ALLOWED   = false
					});
			}
		}
		public void Disallow(OPERATOR @operator, VEHICLE vehicle)
		{
			if (!@operator.OPERATOR_VEHICLE.IsLoaded)
				@operator.OPERATOR_VEHICLE.Load();

			var rights = v_operator_vehicle_right
				.Where(
					access => access.operator_id == @operator.OPERATOR_ID &&
							  access.vehicle_id == vehicle.VEHICLE_ID)
				.Select(access => (SystemRight) access.right_id)
				.ToList();

			Disallow(@operator, vehicle, rights);
		}
		public void Disallow(OPERATOR @operator, VEHICLE vehicle, IEnumerable<SystemRight> systemRights)
		{
			if (@operator == null)
				throw new ArgumentNullException("operator");
			if (vehicle == null)
				throw new ArgumentNullException("vehicle");
			if (systemRights == null)
				throw new ArgumentNullException("systemRights");

			if (@operator.OPERATOR_ID == 0)
				throw new ArgumentException(@"operator.OPERATOR_ID cannot be 0", "operator");
			if (vehicle.VEHICLE_ID == 0)
				throw new ArgumentException(@"vehicle.VEHICLE_ID cannot be 0", "vehicle");

			var systemRightsIds = systemRights.Select(r => (int)r).ToArray();
			if (systemRightsIds.Length == 0)
				return;

			var existingPermissions = this.OPERATOR_VEHICLE.Where(
				p => p.OPERATOR_ID == @operator.OPERATOR_ID &&
					 p.VEHICLE_ID == vehicle.VEHICLE_ID &&
					 systemRightsIds.Contains(p.RIGHT_ID));

			foreach (var permission in existingPermissions.ToArray())
			{
				if (permission.ALLOWED)
					permission.ALLOWED = false;
			}

			var notExistingRightIds = systemRightsIds.Except(existingPermissions.Select(p => p.RIGHT_ID).ToArray());

			foreach (var rightId in notExistingRightIds)
			{
				OPERATOR_VEHICLE.AddObject(
					new OPERATOR_VEHICLE
						{
							OPERATOR = @operator,
							VEHICLE = vehicle,
							RIGHT_ID = rightId,
							ALLOWED = false
						});
			}
		}
		public void Disallow(OPERATOR ormOperator, VEHICLE ormVehicle, RIGHT ormRight)
		{
			if (!ormOperator.OPERATOR_VEHICLE.IsLoaded)
				ormOperator.OPERATOR_VEHICLE.Load();

			var permissions = ormOperator.OPERATOR_VEHICLE
				.Where(p =>
					p.VEHICLE_ID == ormVehicle.VEHICLE_ID &&
					p.RIGHT_ID   == ormRight.RIGHT_ID);

			if (permissions.Count() > 0)
			{
				foreach (var permission in permissions)
				{
					permission.ALLOWED = false;
				}
			}
			else
			{
				//Если нет разрешающих прав, то создадим запрещающие
				var newPermission = new OPERATOR_VEHICLE
				{
					VEHICLE  = ormVehicle,
					RIGHT    = ormRight,
					ALLOWED  = false,
					OPERATOR = ormOperator
				};
				this.AddToOPERATOR_VEHICLE(newPermission);
			}
		}
		public void Disallow(OPERATOR @operator, IEnumerable<SystemRight> systemRights)
		{
			SetAllowed(@operator, systemRights, false);
		}
		private void SetAllowed(OPERATOR @operator, IEnumerable<SystemRight> systemRights, bool allowed)
		{
			if (@operator == null)
				throw new ArgumentNullException(nameof(@operator));
			if (systemRights == null)
				throw new ArgumentNullException(nameof(systemRights));

			var systemRightsIds = systemRights.Select(r => (int)r).ToArray();

			if (systemRightsIds.Length == 0)
				return;

			var existingPermissions = this.RIGHT_OPERATOR.Where(
				p => p.OPERATOR_ID == @operator.OPERATOR_ID &&
					 systemRightsIds.Contains(p.RIGHT_ID));

			foreach (var permission in existingPermissions.ToArray())
				permission.ALLOWED = allowed;

			var notExistingRightIds = systemRightsIds.Except(existingPermissions.Select(p => p.RIGHT_ID).ToArray());

			foreach (var rightId in notExistingRightIds)
			{
				RIGHT_OPERATOR.AddObject(
					new RIGHT_OPERATOR
					{
						OPERATOR = @operator,
						RIGHT_ID = rightId,
						ALLOWED  = allowed
					});
			}
		}
		public void Allow(OPERATOR @operator, VEHICLEGROUP vehicleGroup, RIGHT right)
		{
			SetDirectAccess(@operator, vehicleGroup, right, true);
		}
		public void Allow(OPERATOR @operator, VEHICLEGROUP vehicleGroup, IEnumerable<SystemRight> systemRights)
		{
			foreach (var right in this.GetSystemRights(systemRights.ToArray()))
			{
				Allow(@operator, vehicleGroup, right);
			}
		}
		private void SetDirectAccess(OPERATOR @operator, DEPARTMENT department, IEnumerable<SystemRight> systemRights, bool allowed)
		{
			if (@operator == null)
				throw new ArgumentNullException(nameof(@operator));
			if (department == null)
				throw new ArgumentNullException(nameof(department));
			if (systemRights == null)
				throw new ArgumentNullException(nameof(systemRights));

			var systemRightsIds = systemRights
				.Select(r => (int)r)
				.ToArray();
			if (systemRightsIds.Length == 0)
				return;

			var existingPermissions = OPERATOR_DEPARTMENT
				.Where(p =>
					p.OPERATOR_ID   == @operator.OPERATOR_ID    &&
					p.DEPARTMENT_ID == department.DEPARTMENT_ID &&
					systemRightsIds.Contains(p.RIGHT_ID));

			foreach (var permission in existingPermissions.ToArray())
			{
				permission.ALLOWED = allowed;
			}

			var notExistingRightIds = systemRightsIds
				.Except(existingPermissions
					.Select(p => p.RIGHT_ID)
					.ToArray());

			foreach (var rightId in notExistingRightIds)
			{
				OPERATOR_DEPARTMENT.AddObject(
					new OPERATOR_DEPARTMENT
					{
						OPERATOR   = @operator,
						DEPARTMENT = department,
						RIGHT_ID   = rightId,
						ALLOWED    = allowed
					});
			}
		}
		public void Allow(OPERATOR @operator, DEPARTMENT department, params SystemRight[] systemRights)
		{
			SetDirectAccess(@operator, department, systemRights, true);
		}
		public void Allow(OPERATOR @operator, DEPARTMENT department, IEnumerable<SystemRight> systemRights)
		{
			SetDirectAccess(@operator, department, systemRights, true);
		}
		public void Disallow(OPERATOR @operator, DEPARTMENT department, params SystemRight[] systemRights)
		{
			SetDirectAccess(@operator, department, systemRights, false);
		}
		public void Disallow(OPERATOR @operator, DEPARTMENT department, IEnumerable<SystemRight> systemRights)
		{
			SetDirectAccess(@operator, department, systemRights, false);
		}
		public void Allow(OPERATOR @operator, IEnumerable<SystemRight> systemRights)
		{
			if (!@operator.RIGHT_OPERATOR.IsLoaded)
				@operator.RIGHT_OPERATOR.Load();

			var rightIsAdded = systemRights.ToDictionary(right => right, right => false);

			foreach (var existingRight in @operator.RIGHT_OPERATOR)
			{
				if (!rightIsAdded.ContainsKey((SystemRight)existingRight.RIGHT_ID))
					continue;
				rightIsAdded[(SystemRight)existingRight.RIGHT_ID] = true;
				if (!existingRight.ALLOWED)
					existingRight.ALLOWED = true;
			}

			foreach (var right in rightIsAdded.Keys)
			{
				if (rightIsAdded[right])
					continue;

				AddToRIGHT_OPERATOR(new RIGHT_OPERATOR
				{
					OPERATOR = @operator,
					RIGHT_ID = (int)right,
					ALLOWED  = true
				});
			}
		}
		public void Allow(OPERATORGROUP operatorGroup, VEHICLEGROUP vehicleGroup, IEnumerable<SystemRight> systemRights)
		{
			var rightIds = systemRights
				.Select(r => (int)r)
				.ToArray();

			var existingRights =
				(
					from er in OPERATORGROUP_VEHICLEGROUP
					where
						er.OPERATORGROUP_ID == operatorGroup.OPERATORGROUP_ID &&
						er.VEHICLEGROUP_ID  == vehicleGroup.VEHICLEGROUP_ID   &&
						rightIds.Contains(er.RIGHT_ID)
					select er
				)
				.ToArray();

			var rightIsAdded = rightIds.ToDictionary(right => right, right => false);

			foreach (var er in existingRights)
			{
				if (!rightIsAdded.ContainsKey(er.RIGHT_ID))
					continue;
				rightIsAdded[er.RIGHT_ID] = true;
				if (!er.ALLOWED)
					er.ALLOWED = true;
			}

			foreach (var rightID in rightIsAdded.Keys)
			{
				if (rightIsAdded[rightID])
					continue;

				OPERATORGROUP_VEHICLEGROUP.AddObject(
					new OPERATORGROUP_VEHICLEGROUP
						{
							OPERATORGROUP = @operatorGroup,
							VEHICLEGROUP = vehicleGroup,
							RIGHT_ID = rightID,
							ALLOWED = true
						});
			}
		}
		public void Allow(OPERATOR @operator, VEHICLE vehicle, IEnumerable<SystemRight> systemRights)
		{
			var operatorDepartmentRights =
				(
					from od in OPERATOR_VEHICLE
					where
						od.OPERATOR_ID == @operator.OPERATOR_ID &&
						od.VEHICLE_ID  == vehicle.VEHICLE_ID
					select od
				)
				.ToArray();

			var rightIsAdded = systemRights.ToDictionary(right => right, right => false);

			foreach (var od in operatorDepartmentRights)
			{
				if (!rightIsAdded.ContainsKey((SystemRight)od.RIGHT_ID))
					continue;
				rightIsAdded[(SystemRight)od.RIGHT_ID] = true;
				if (!od.ALLOWED)
					od.ALLOWED = true;
			}

			foreach (var right in rightIsAdded.Keys)
			{
				if (rightIsAdded[right])
					continue;
				OPERATOR_VEHICLE.AddObject(new OPERATOR_VEHICLE
				{
					OPERATOR = @operator,
					VEHICLE  = vehicle,
					RIGHT_ID = (int)right,
					ALLOWED  = true
				});
			}
		}
		/// <summary> Запрещает все права оператора @operator на группу vehicleGroup </summary>
		public void Disallow(OPERATOR @operator, VEHICLEGROUP vehicleGroup)
		{
			foreach (var right in (from ovgr in this.v_operator_vehicle_groups_right
								   from r in this.RIGHT
								   where ovgr.operator_id == @operator.OPERATOR_ID &&
										 ovgr.vehiclegroup_id == vehicleGroup.VEHICLEGROUP_ID &&
										 r.RIGHT_ID == ovgr.right_id
								   select r))
			{
				SetDirectAccess(@operator, vehicleGroup, right, false);
			}
		}
		/// <summary> Запрещает право оператора @operator на группу vehicleGroup </summary>
		public void Disallow(OPERATOR @operator, VEHICLEGROUP vehicleGroup, RIGHT right)
		{
			SetDirectAccess(@operator, vehicleGroup, right, false);
		}
		/// <summary> Запрещает право оператора @operator на группу vehicleGroup </summary>
		public void Disallow(OPERATOR @operator, VEHICLEGROUP vehicleGroup, IEnumerable<SystemRight> rights)
		{
			foreach (var right in rights)
			{
				Disallow(@operator, vehicleGroup, GetSystemRight(right));
			}
		}
		public void SetDirectAccess(OPERATOR @operator, VEHICLEGROUP vehicleGroup, RIGHT right, bool allowed)
		{
			if (@operator == null) throw new ArgumentNullException("operator");
			if (vehicleGroup == null) throw new ArgumentNullException("vehicleGroup");
			if (right == null) throw new ArgumentNullException("right");
			if (!@operator.OPERATOR_VEHICLEGROUP.IsLoaded)
				@operator.OPERATOR_VEHICLEGROUP.Load();

			var existingPermission = @operator.OPERATOR_VEHICLEGROUP.ToList().Find(
				ovg => ovg.VEHICLEGROUP_ID == vehicleGroup.VEHICLEGROUP_ID && ovg.RIGHT_ID == right.RIGHT_ID);

			if (existingPermission != null)
			{
				if (existingPermission.ALLOWED != allowed)
					existingPermission.ALLOWED = allowed;
				return;
			}

			var newPermission = new OPERATOR_VEHICLEGROUP
			{
				OPERATOR     = @operator,
				VEHICLEGROUP = vehicleGroup,
				RIGHT        = right,
				ALLOWED      = allowed
			};
			AddToOPERATOR_VEHICLEGROUP(newPermission);
		}
		public void SetDirectAccess(OPERATOR @operator, VEHICLE vehicle, RIGHT right, bool allowed)
		{
			if (@operator == null) throw new ArgumentNullException("operator");
			if (vehicle == null) throw new ArgumentNullException("vehicle");
			if (right == null) throw new ArgumentNullException("right");
			if (!@operator.OPERATOR_VEHICLEGROUP.IsLoaded)
				@operator.OPERATOR_VEHICLEGROUP.Load();

			var existingPermission = @operator.OPERATOR_VEHICLE.ToList().Find(
				ovg => ovg.VEHICLE_ID == vehicle.VEHICLE_ID && ovg.RIGHT_ID == right.RIGHT_ID);

			if (existingPermission != null)
			{
				if (existingPermission.ALLOWED != allowed)
					existingPermission.ALLOWED = allowed;
				return;
			}

			var newPermission = new OPERATOR_VEHICLE
			{
				OPERATOR = @operator,
				VEHICLE  = vehicle,
				RIGHT    = right,
				ALLOWED  = allowed
			};

			OPERATOR_VEHICLE.AddObject(newPermission);
		}
		public void SetDirectAccess(OPERATOR @operator, GEO_ZONE zone, RIGHT right, bool allowed)
		{
			if (@operator == null) throw new ArgumentNullException("operator");
			if (zone == null) throw new ArgumentNullException("zone");
			if (right == null) throw new ArgumentNullException("right");
			if (!@operator.OPERATOR_ZONE.IsLoaded)
				@operator.OPERATOR_ZONE.Load();

			var existingPermission = @operator.OPERATOR_ZONE.ToList().Find(
				ovg => ovg.ZONE_ID == zone.ZONE_ID && ovg.RIGHT_ID == right.RIGHT_ID);

			if (existingPermission != null)
			{
				if (existingPermission.ALLOWED != allowed)
					existingPermission.ALLOWED = allowed;
				return;
			}

			var newPermission = new OPERATOR_ZONE
			{
				OPERATOR = @operator,
				GEO_ZONE = zone,
				RIGHT    = right,
				ALLOWED  = allowed
			};

			OPERATOR_ZONE.AddObject(newPermission);
		}
		public void SetDirectAccess(OPERATOR @operator, ZONEGROUP zoneGroup, RIGHT right, bool allowed)
		{
			if (@operator == null) throw new ArgumentNullException("operator");
			if (zoneGroup == null) throw new ArgumentNullException("zoneGroup");
			if (right == null) throw new ArgumentNullException("right");
			if (!@operator.OPERATOR_ZONEGROUP.IsLoaded)
				@operator.OPERATOR_ZONEGROUP.Load();

			var existingPermission = @operator.OPERATOR_ZONEGROUP.ToList().Find(
				ovg => ovg.ZONEGROUP_ID == zoneGroup.ZONEGROUP_ID && ovg.RIGHT_ID == right.RIGHT_ID);

			if (existingPermission != null)
			{
				if (existingPermission.ALLOWED != allowed)
					existingPermission.ALLOWED = allowed;
				return;
			}

			var newPermission = new OPERATOR_ZONEGROUP
			{
				OPERATOR  = @operator,
				ZONEGROUP = zoneGroup,
				RIGHT     = right,
				ALLOWED   = allowed
			};

			OPERATOR_ZONEGROUP.AddObject(newPermission);
		}
		public bool IsAllowed(OPERATOR ormOperator, SystemRight systemRight)
		{
			return IsAllowed(ormOperator.OPERATOR_ID, systemRight);
		}
		public bool IsAllowed(int operatorId, SystemRight systemRight)
		{
			return v_operator_rights
				.Any(or =>
					or.operator_id == operatorId &&
					or.right_id    == (int)systemRight);
		}
		public void Allow(OPERATORGROUP operatorGroup, DEPARTMENT department, IEnumerable<SystemRight> rights)
		{
			foreach (var right in rights)
			{
				var existing = OPERATORGROUP_DEPARTMENT
					.FirstOrDefault(e =>
						e.OPERATORGROUP_ID == operatorGroup.OPERATORGROUP_ID &&
						e.DEPARTMENT_ID    == department.DEPARTMENT_ID       &&
						e.RIGHT_ID         == (int)right);

				if (existing != null)
				{
					if (!existing.ALLOWED)
						existing.ALLOWED = true;
					continue;
				}

				OPERATORGROUP_DEPARTMENT.AddObject(
					new OPERATORGROUP_DEPARTMENT
						{
							ALLOWED = true,
							RIGHT = GetSystemRight(right),
							DEPARTMENT = department,
							OPERATORGROUP = operatorGroup
						});
			}
		}
		public RIGHT GetSystemRight(SystemRight systemRight)
		{
			var systemRightID = (int)systemRight;
			return (from r in RIGHT
					where r.RIGHT_ID == systemRightID
					select r).First();
		}
		public RIGHT[] GetSystemRights(SystemRight[] systemRight)
		{
			var systemRightIDs = Array.ConvertAll(systemRight, r => (int)r);
			return (from r in RIGHT
					where systemRightIDs.Contains(r.RIGHT_ID)
					select r).ToArray();
		}
		public bool DeleteAccess(OPERATOR @operator, VEHICLE vehicle, params SystemRight[] rights)
		{
			var ids = rights.Cast<int>().ToArray();
			var accesses = OPERATOR_VEHICLE.Where(
				x => x.OPERATOR_ID == @operator.OPERATOR_ID &&
					 x.VEHICLE_ID == @vehicle.VEHICLE_ID &&
					 ids.Any(rightId => rightId == x.RIGHT_ID)).ToList();

			if (accesses.Count == 0)
				return false;

			foreach (var operatorVehicle in accesses)
			{
				OPERATOR_VEHICLE.DeleteObject(operatorVehicle);
			}

			return true;
		}
		public void DeleteAccess(OPERATOR @operator, VEHICLE vehicle, RIGHT right)
		{
			var access = OPERATOR_VEHICLE.FirstOrDefault(
				x => x.OPERATOR_ID == @operator.OPERATOR_ID &&
					 x.VEHICLE_ID == @vehicle.VEHICLE_ID &&
					 x.RIGHT_ID == @right.RIGHT_ID);

			if (access == null)
				return;

			OPERATOR_VEHICLE.DeleteObject(access);
		}
		public void DeleteAccess(OPERATOR @operator, VEHICLE vehicle)
		{
			var accesses = OPERATOR_VEHICLE
				.Where(
					x => x.OPERATOR_ID == @operator.OPERATOR_ID &&
						 x.VEHICLE_ID == @vehicle.VEHICLE_ID &&
						 x.RIGHT_ID != (int)SystemRight.SecurityAdministration &&
						 x.RIGHT_ID != (int)SystemRight.Ignore)
				.ToList();

			foreach (var access in accesses)
				OPERATOR_VEHICLE.DeleteObject(access);
		}
		public void DeleteAccess(OPERATOR @operator, VEHICLEGROUP vehicleGroup)
		{
			var accesses = OPERATOR_VEHICLEGROUP
				.Where(
					x => x.OPERATOR_ID == @operator.OPERATOR_ID &&
						 x.VEHICLEGROUP_ID == @vehicleGroup.VEHICLEGROUP_ID &&
						 x.RIGHT_ID != (int)SystemRight.SecurityAdministration &&
						 x.RIGHT_ID != (int)SystemRight.Ignore)
				.ToList();

			foreach (var access in accesses)
				OPERATOR_VEHICLEGROUP.DeleteObject(access);
		}
		public void DeleteAccess(OPERATOR @operator, GEO_ZONE zone)
		{
			var accesses = OPERATOR_ZONE
				.Where(
					x => x.OPERATOR_ID == @operator.OPERATOR_ID &&
						 x.ZONE_ID == @zone.ZONE_ID &&
						 x.RIGHT_ID != (int)SystemRight.SecurityAdministration &&
						 x.RIGHT_ID != (int)SystemRight.Ignore)
				.ToList();

			foreach (var access in accesses)
				OPERATOR_ZONE.DeleteObject(access);
		}
		public void DeleteAccess(OPERATOR @operator, ZONEGROUP zoneGroup)
		{
			var accesses = OPERATOR_ZONEGROUP
				.Where(
					x => x.OPERATOR_ID == @operator.OPERATOR_ID &&
						 x.ZONEGROUP_ID == @zoneGroup.ZONEGROUP_ID &&
						 x.RIGHT_ID != (int)SystemRight.SecurityAdministration &&
						 x.RIGHT_ID != (int)SystemRight.Ignore)
				.ToList();

			foreach (var access in accesses)
				OPERATOR_ZONEGROUP.DeleteObject(access);
		}
		public void DeleteOperatorAccessOnVehicle(int operatorId, int vehicleId, SystemRight right)
		{
			var access = OPERATOR_VEHICLE.FirstOrDefault(
				x => x.OPERATOR_ID == operatorId &&
					 x.VEHICLE_ID == vehicleId &&
					 x.RIGHT_ID == (int)right);

			if (access == null)
				return;

			OPERATOR_VEHICLE.DeleteObject(access);
		}
		public void DeleteAccess(OPERATOR @operator, DEPARTMENT department, SystemRight right)
		{
			var access = OPERATOR_DEPARTMENT.FirstOrDefault(
				x => x.OPERATOR_ID == @operator.OPERATOR_ID &&
					 x.DEPARTMENT_ID == @department.DEPARTMENT_ID &&
					 x.RIGHT_ID == (int)right);

			if (access == null)
				return;

			OPERATOR_DEPARTMENT.DeleteObject(access);
		}
		public void DeleteAccess(OPERATOR @operator, VEHICLEGROUP group, RIGHT right)
		{
			var access = OPERATOR_VEHICLEGROUP.FirstOrDefault(
				x => x.OPERATOR_ID == @operator.OPERATOR_ID &&
					 x.VEHICLEGROUP_ID == group.VEHICLEGROUP_ID &&
					 x.RIGHT_ID == @right.RIGHT_ID);

			if (access == null)
				return;

			OPERATOR_VEHICLEGROUP.DeleteObject(access);
		}
		public void DeleteAccess(OPERATOR @operator, GEO_ZONE zone, RIGHT right)
		{
			var access = OPERATOR_ZONE.FirstOrDefault(
				x => x.OPERATOR_ID == @operator.OPERATOR_ID &&
					 x.ZONE_ID == zone.ZONE_ID &&
					 x.RIGHT_ID == @right.RIGHT_ID);

			if (access == null)
				return;

			OPERATOR_ZONE.DeleteObject(access);
		}
		public void DeleteAccess(OPERATOR @operator, ZONEGROUP ormZoneGroup, RIGHT right)
		{
			var access = OPERATOR_ZONEGROUP.FirstOrDefault(
				x => x.OPERATOR_ID == @operator.OPERATOR_ID &&
					 x.ZONEGROUP_ID == ormZoneGroup.ZONEGROUP_ID &&
					 x.RIGHT_ID == @right.RIGHT_ID);

			if (access == null)
				return;

			OPERATOR_ZONEGROUP.DeleteObject(access);
		}
		private void DeleteAccess(OPERATORGROUP operatorGroup, DEPARTMENT department, params SystemRight[] rights)
		{
			foreach (var right in rights)
			{
				var access = OPERATORGROUP_DEPARTMENT.FirstOrDefault(
					og => og.OPERATORGROUP_ID == operatorGroup.OPERATORGROUP_ID &&
						  og.DEPARTMENT_ID == department.DEPARTMENT_ID &&
						  og.RIGHT_ID == (int) right);

				if (access == null)
					continue;

				OPERATORGROUP_DEPARTMENT.DeleteObject(access);
			}
		}
	}
}