﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;
using Microsoft.VisualBasic.Logging;

namespace FORIS.TSS.TraceListener
{
	public class CustomTraceListener : FileLogTraceListener
	{
		internal static string AppName = Path.GetFileName(Environment.GetCommandLineArgs()[0]);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="fileName"></param>
		public CustomTraceListener(string fileName) :
			base(fileName)
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="eventCache"></param>
		/// <param name="source"></param>
		/// <param name="eventType"></param>
		/// <param name="id"></param>
		/// <param name="data"></param>
		public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data)
		{
			base.TraceData(eventCache, source, eventType, id, data);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="eventCache"></param>
		/// <param name="source"></param>
		/// <param name="eventType"></param>
		/// <param name="id"></param>
		/// <param name="data"></param>
		public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, params object[] data)
		{
			base.TraceData(eventCache, source, eventType, id, PreprocessFormatArguments(data));
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		private object[] PreprocessFormatArguments(object[] data)
		{
			if (data == null || data.Length == 0)
				return data;

			if (Array.TrueForAll(data, x => !(x is byte[])))
				return data;

			return Array.ConvertAll(data, x =>
				{
					var byteArray = x as byte[];
					if (byteArray != null)
					{
						//TODO: use smart byte array formatter - show ASCII string when possible
						return BitConverter.ToString(byteArray);
					}

					return x;
				});
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		public override void Write(string message)
		{
			if (!string.IsNullOrEmpty(message))
			{
				string[] lines = message.Split('\n');
				for (int i = 0; i < lines.Length - 1; i++)
					base.WriteLine(lines[i]);
				base.Write(lines[lines.GetUpperBound(0)]);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		public override void WriteLine(string message)
		{
			base.WriteLine(string.Format("{0}|{1,3:N0}| {2}", DateTime.Now.ToString("yyMMdd-HHmmss.fff"), Thread.CurrentThread.ManagedThreadId, message));
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="eventCache"></param>
		/// <param name="source"></param>
		/// <param name="eventType"></param>
		/// <param name="id"></param>
		public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id)
		{
			TraceEvent(eventCache, source, eventType, id, string.Empty);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="eventCache"></param>
		/// <param name="source"></param>
		/// <param name="eventType"></param>
		/// <param name="id"></param>
		/// <param name="message"></param>
		public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string message)
		{
			TraceEvent(eventCache, source, eventType, id, "{0}", message);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="eventCache"></param>
		/// <param name="source"></param>
		/// <param name="eventType"></param>
		/// <param name="id"></param>
		/// <param name="format"></param>
		/// <param name="args"></param>
		public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string format, params object[] args)
		{
			if ((Filter == null) || Filter.ShouldTrace(eventCache, source, eventType, id, format, args, null, null))
			{
				string eventTypeText = string.Empty;
				switch (eventType)
				{
					case TraceEventType.Critical:
						eventTypeText = "CRT";
						break;
					case TraceEventType.Error:
						eventTypeText = "ERR";
						break;
					case TraceEventType.Warning:
						eventTypeText = "WRN";
						break;
					case TraceEventType.Information:
						eventTypeText = "INF";
						break;
					case TraceEventType.Verbose:
						eventTypeText = "VRB";
						break;
					default:
						eventTypeText = "MSG";
						break;
				}

				if (source?.Equals(AppName) ?? false)
					format = string.Format("{0}: ",     eventTypeText)         + format;
				else
					format = string.Format("{0}: {1}:", eventTypeText, source) + format;
				if (null != args)
					WriteLine(string.Format(CultureInfo.InvariantCulture, format, args));
				else
					WriteLine(format);
			}
		}
	}
}