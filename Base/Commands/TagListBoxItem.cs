﻿using System;

namespace FORIS.TSS.TransportDispatcher
{
	/// <summary> Класс элемента выпадающего списка, который позволяет хранить дополнительную ссылку на любой объект </summary>
	[Serializable]
	public class TagListBoxItem
	{
		//Переменная для хранения сязанного элемента.
		object mobjTagElement;
		//Переменная для хранения текста описания.
		string mtxtTextElement;
		/// <summary> Связанный с данным элементом объект </summary>
		public object Tag
		{
			get { return mobjTagElement; }
			set { mobjTagElement = value; }
		}
		/// <summary> Текст описания </summary>
		public string Text
		{
			get { return mtxtTextElement; }
			set { mtxtTextElement = value; }
		}
		/// <summary>
		/// Класс элемента выпадающего списка, который позволяет хранить дополнительную
		/// ссылку на любой объект.
		/// </summary>
		/// <param name="mtxtTextElement">текст, описывающий элемент списка</param>
		/// <param name="mobjTagElement">ссылка на связанный объект</param>
		public TagListBoxItem(string mtxtTextElement, object mobjTagElement)
		{
			this.mtxtTextElement = mtxtTextElement;
			this.mobjTagElement = mobjTagElement;
		}
		public TagListBoxItem()
		{
		}
		/// <summary> Возвращение не самого объекта, а текста описания </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return mtxtTextElement;
		}
	}
}