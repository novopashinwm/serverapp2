﻿using System;
using System.Collections;
using System.Data;

namespace FORIS.TSS.TransportDispatcher.Editors
{
	/// <summary> Класс элемента выпадающего списка, который будет передаваться ProprtyGrid в качестве value </summary>
	[Serializable]
	public class PropValue
	{
		// Переменная для хранения текстового описания
		string strText;

		/// <summary> Текст описания </summary>
		public string Text
		{
			get
			{
				return strText;
			}
			set
			{
				strText = value;
			}
		}

		/// <summary> Возвращение не самого объекта, а текста описания </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return strText;
		}
		#region Таблица и название колонки в качестве источника данных для контрола

		// Источник данных для контрола
		DataTable dtSource;
		// Имя отображаемой колонки
		string strColumn;

		/// <summary> Связанный с данным элементом объект </summary>
		public DataTable dt
		{
			get
			{
				return dtSource;
			}
			set
			{
				dtSource = value;
			}
		}

		/// <summary> Название колонки </summary>
		public string Column
		{
			get
			{
				return strColumn;
			}
			set
			{
				strColumn = value;
			}
		}

		/// <summary> Колонка с данными (например ID) </summary>
		public string TagColumn { get; set; }

		/// <summary> Конструктор, если в качестве источника данных используется таблица </summary>
		public PropValue(DataTable dtSource, string strColumn, string strText)
		{
			this.dtSource = dtSource;
			this.strColumn = strColumn;
			this.strText = strText;
		}

		#endregion Таблица и название колонки в качестве источника данных для контрола
		#region Массив в качестве источника данных для контрола

		// Источник данных для контрола
		ArrayList arlSource;

		/// <summary> Связанный с данным элементом объект </summary>
		public ArrayList list
		{
			get
			{
				return arlSource;
			}
			set
			{
				arlSource = value;
			}
		}

		private ArrayList checkedItems = new ArrayList();

		public ArrayList CheckedItems
		{
			get { return checkedItems; }
			set { checkedItems = value; }
		}

		/// <summary> Конструктор, если в качестве источника данных используется ArrayList </summary>
		/// <param name=""></param>
		/// <param name=""></param>
		public PropValue(ArrayList arlSource, string strText)
		{
			this.arlSource = arlSource;
			this.strText = strText;
		}

		#endregion Таблица и название колонки в качестве источника данных для контрола
	}
}