﻿using System;
using System.ComponentModel;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Common;

namespace FORIS.TSS.Helpers.Data
{
	/// <summary>
	/// Базовый класс для финальных поставщиков данных
	/// стороны сервера. Декларирован в общей сборке,
	/// а не в серверной, чтоб ссылаться на него в
	/// проверке правомерности модификации данных.
	/// </summary>
	public abstract class FinalDataSupplier :
		Component,
		IDataSupplier
	{
		#region IDataSupplier Members
		DataInfo IDataSupplier.add_Connection(AnonymousEventHandler<DataChangedEventArgs> handler)
		{
			return this.GetData();
		}
		void IDataSupplier.remove_Connection(AnonymousEventHandler<DataChangedEventArgs> handler)
		{
		}
		void IDataSupplier.Tick()
		{
		}
		event AnonymousEventHandler<EventArgs> IDataSupplier.Changed
		{
			add    { }
			remove { }
		}
		#endregion IDataSupplier Members
		protected abstract DataInfo GetData();
	}
}