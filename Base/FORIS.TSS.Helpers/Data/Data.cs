﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Runtime.Remoting.Lifetime;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Common;

namespace FORIS.TSS.Helpers.Data
{
	public abstract class Data :
		Component,
		IData
	{
		#region LifeTime

		/// <summary> Влияет на лицензию, выдаваемую контейнерам данных </summary>
		public static readonly TimeSpan InitialLifeTime = TimeSpan.FromHours(2);

		public override object InitializeLifetimeService()
		{
			ILease lease = (ILease)base.InitializeLifetimeService();

			// Lease.InitialLeaseTime = Data.InitialLifeTime;
			// Lease.RenewOnCallTime = Data.InitialLifeTime;

			return lease;
		}

		#endregion // LifeTime

		#region Properties

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public DataSet DataSet
		{
			get { return dsDataSet; }
		}

		#endregion Properties

		#region Controls & Components

		private System.ComponentModel.IContainer components;

		protected DataSet dsDataSet;


		#endregion Controls & Components

		#region Properties

		private readonly Guid m_GUID = Guid.NewGuid();
		public Guid GUID
		{
			get { return m_GUID; }
		}

		private IDataSupplier dataSupplier;
		/// <summary>
		/// Поставщик данных. Контейнер синхронизируется с ним 
		/// сразу и до смерти или замены поставщика данных.
		/// </summary>
		[Browsable(true)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public IDataSupplier DataSupplier
		{
			get { return dataSupplier; }
			set
			{
				#region Preconditions

				if (value == this)
				{
					throw new ArgumentException("Can not get data from meself", "value");
				}

				#endregion // Preconditions

				if (dataSupplier != null)
				{
					dataSupplier.Changed -= dataSupplier_Changed;

					Disconnect();
				}

				dataSupplier = value;

				if (dataSupplier != null)
				{
					Connect();

					dataSupplier.Changed += dataSupplier_Changed;
				}

				OnChanged();
			}
		}

		#endregion Properties

		#region Load

		private void Disconnect()
		{
			/* Не должно быть никаких изменений пока 
			 * данные в неустойчивом состоянии
			 */

			lock (this)
			{
				OnLoading();

				Destroy();

				version = Guid.Empty;
				dsDataSet = new DataSet();

				dataSupplier.remove_Connection(
					new AnonymousEventHandler<DataChangedEventArgs>(dataSupplier_DataChanged)
					);
			}
		}

		private void Connect()
		{
			/* Не должно быть никаких изменений пока 
			 * данные в неустойчивом состоянии
			 */

			lock (this)
			{
				DataInfo Info =
					dataSupplier.add_Connection(
						new AnonymousEventHandler<DataChangedEventArgs>(dataSupplier_DataChanged)
						);

				dsDataSet = Info.DataSet;
				version = Info.Version;

				// this.applyingChanges = true;

				Build();

				// this.applyingChanges = false;

				OnLoaded();
			}
		}

		private EventHandler delegateLoading;
		[Browsable(true)]
		[Category("Load data")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public event EventHandler Loading
		{
			add { delegateLoading += value; }
			remove { delegateLoading -= value; }
		}

		protected virtual void OnLoading()
		{
			if (delegateLoading != null)
			{
				delegateLoading(this, EventArgs.Empty);
			}
		}

		private EventHandler delegateLoaded;
		[Browsable(true)]
		[Category("Load data")]
		[DesignerSerializationVisibility((DesignerSerializationVisibility.Visible))]
		public event EventHandler Loaded
		{
			add { delegateLoaded += value; }
			remove { delegateLoaded -= value; }
		}

		protected virtual void OnLoaded()
		{
			if (delegateLoaded != null)
			{
				delegateLoaded(this, EventArgs.Empty);
			}
		}

		#endregion // Load

		#region Data

		protected virtual void OnBeforeSetData()
		{

		}
		protected virtual void OnAfterSetData()
		{

		}

		#endregion // Data

		#region View

		/// <summary>
		/// порядок таблиц
		/// </summary>
		/// <remarks>
		/// используется в методе ApplyChanges
		/// для определения порядка таблиц при
		/// удалении/добавлении/изменении данных
		/// </remarks>
		protected abstract ITableOrder Tables { get; }

		#region Build & Destroy

		/// <summary>
		/// Разрушить обертки
		/// </summary>
		protected abstract void Destroy();

		/// <summary>
		/// Построить обертки
		/// </summary>
		/// <remarks>
		/// Так как базовый класс не имеет понятия о типе 
		/// наследуемого класса, то он не знает и о типе 
		/// диспетчера контейнер данных (обобщенный тип)
		/// </remarks>
		protected abstract void Build();

		#endregion // Build & Destroy

		#endregion // View

		#region Constructor & Dispose

		protected Data()
		{
			InitializeComponent();
		}

		protected Data(IContainer container)
		{
			container.Add(this);

			InitializeComponent();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				DataSupplier = null;

				delegateDataChanged = null;

				if (components != null)
				{
					components.Dispose();
				}

				beforeChange = null;
				afterChange = null;

				Trace.WriteLineIf(
					Data.TraceSwitch.TraceWarning,
					string.Format(
						"{0}.Dispose() ",
						GetType().Name
						),
					"DATA"
					);
			}

			base.Dispose(disposing);
		}


		#endregion Constructor & Dipsose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			dsDataSet = new System.Data.DataSet();
			((System.ComponentModel.ISupportInitialize)(dsDataSet)).BeginInit();
			// 
			// dsDataSet
			// 
			dsDataSet.DataSetName = "Data";
			dsDataSet.Locale = new System.Globalization.CultureInfo("ru-RU");
			((System.ComponentModel.ISupportInitialize)(dsDataSet)).EndInit();
		}


		#endregion Component Designer generated code

		#region IData Members

		#region BeforeChange

		private EventHandler beforeChange;

		public event EventHandler BeforeChange
		{
			add { beforeChange += value; }
			remove { beforeChange -= value; }
		}

		protected virtual void OnBeforeChange()
		{
			if (beforeChange != null)
			{
				beforeChange(this, EventArgs.Empty);
			}
		}

		#endregion // BeforeChange

		#region AfterChange

		private EventHandler afterChange;

		public event EventHandler AfterChange
		{
			add { afterChange += value; }
			remove { afterChange -= value; }
		}

		protected virtual void OnAfterChange()
		{
			if (afterChange != null)
			{
				afterChange(this, EventArgs.Empty);
			}
		}

		#endregion // AfterChange

		#endregion // IData Members

		#region IDataSupplier Members

		/// <summary>
		/// Возвращает основной датасет
		/// контейнера данных
		/// </summary>
		/// <returns></returns>
		DataInfo IDataSupplier.add_Connection(AnonymousEventHandler<DataChangedEventArgs> handler)
		{
			Trace.WriteLineIf(
				Data.TraceSwitch.TraceInfo,
				string.Format(
					"{0}.add_Connection()",
					GetType().Name
					),
				"DATA"
				);

			/* Никто не должен менять данные, пока снимок не будет собран. 
			 * Уведомления о следующих изменениях данных пойдут в подписавщегося потребителя.
			 */

			lock (this)
			{
				delegateDataChanged += handler;

				return
					new DataInfo(
						dsDataSet.Copy(),
						version
						);
			}
		}

		void IDataSupplier.remove_Connection(AnonymousEventHandler<DataChangedEventArgs> handler)
		{
			lock (this)
			{
				delegateDataChanged -= handler;
			}
		}

		[OneWay]
		public void Tick()
		{
			Debug.WriteLine(GetType().FullName + ".Tick()");
		}

		#region Event DataChanged

		private AnonymousEventHandler<DataChangedEventArgs> delegateDataChanged;

		protected virtual void OnDataChanged(DataChangedEventArgs e)
		{
			List<WaitHandle> waitHandles = null;

			if (delegateDataChanged != null)
			{
				Delegate[] handlers = delegateDataChanged.GetInvocationList();

				waitHandles = new List<WaitHandle>(handlers.Length);

				foreach (AnonymousEventHandler<DataChangedEventArgs> handler in handlers)
				{
					IAsyncResult asyncResult =
						handler.BeginInvoke(
							e,
							new AsyncCallback(DataChanged_Callback),
							handler
							);

					waitHandles.Add(asyncResult.AsyncWaitHandle);
				}

				//WaitHandle.WaitAll( waitHandles.ToArray() );
			}
		}

		private void DataChanged_Callback(IAsyncResult asyncResult)
		{
			AnonymousEventHandler<DataChangedEventArgs> Handler =
				(AnonymousEventHandler<DataChangedEventArgs>)asyncResult.AsyncState;

			try
			{
				Handler.EndInvoke(asyncResult);
			}
			catch (Exception)
			{
				delegateDataChanged -= Handler;
			}
		}

		#endregion // Event DataChanged

		#region Event Changed

		private AnonymousEventHandler<EventArgs> delegateChanged;
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public event AnonymousEventHandler<EventArgs> Changed
		{
			add { delegateChanged += value; }
			remove { delegateChanged -= value; }
		}

		protected virtual void OnChanged()
		{
			if (delegateChanged != null)
			{
				delegateChanged(EventArgs.Empty);
			}
		}

		#endregion // Event Changed

		#endregion // IDataSupplier Members

		/// <summary>
		/// Версия данных
		/// </summary>
		/// <remarks>
		/// Контейнер рождается без версии. Первая непустая версия 
		/// возникает при первом получении данных вместе с ними.
		/// Версия от данных неотделима и без них не существует.
		/// </remarks>
		private Guid version = Guid.Empty;

#if DEBUG
		public Guid Version
		{
			get { return version; }
		}
#endif

		private bool applyingChanges = false;
		[Browsable(false)]
		[DefaultValue(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool ApplyingChanges
		{
			get { return applyingChanges; }
		}

		/// <summary>
		/// Принимает изменения датасета.
		/// Собирает информацию для уведомления об изменении данных.
		/// Уведомляет об изменении данных.
		/// </summary>
		/// <param name="currentVersion"></param>
		/// <param name="databaseDataSupplier"></param>
		/// <remarks>
		/// метод изменяет данные в dsDataSet и потому весь помещен в lock
		/// </remarks>
		public virtual void AcceptChanges(Guid currentVersion, IDatabaseDataSupplier databaseDataSupplier)
		{
			lock (this)
			{
				DataSet AddedData    = new DataSet();
				DataSet DeletedData  = new DataSet();
				DataSet ModifiedData = new DataSet();

				if (dsDataSet.HasChanges())
				{
					DataSet Changes = dsDataSet.GetChanges();

					#region rowChangingHandler

					DataRowChangeEventHandler rowChangingHandler =
						delegate (object sender, DataRowChangeEventArgs e)
						{
							#region Trace 

							Trace.WriteLineIf(
								Data.TraceSwitch.TraceInfo,
								string.Format(
									"-> {0}.AcceptChanges().RowChangingHandler( e.Action = {1}, TableName={2})",
									GetType().Name,
									e.Action,
									e.Row.Table.TableName
									),
								"DATA:"
								);

							#endregion // Trace

							/* Обработчик только собирает полученные
							 * из БД значения ключей добавленных записей
							 * и применяет их к набору изменений (Added),
							 * который будет отправлен извещающим об
							 * изменении событием
							 */

							if (e.Action == DataRowAction.Change &&
								e.Row.RowState == DataRowState.Added
								)
							{
								#region key

								object[] key = new object[e.Row.Table.PrimaryKey.Length];

								for (int i = 0; i < e.Row.Table.PrimaryKey.Length; i++)
								{
									key[i] = e.Row[e.Row.Table.PrimaryKey[i], DataRowVersion.Current];
								}

								#endregion // key

								DataRow RowAdded = AddedData.Tables[e.Row.Table.TableName].Rows.Find(key);

								RowAdded.ItemArray = (object[])e.Row.ItemArray.Clone();

								/* Устанавливаем новые значения ключей (полученные из БД) 
								 * для добавленных строк. И должны обновиться значения 
								 * в полях внешних ключей связаных таблиц.
								 * 
								 * Также необходимо отследить как меняются 
								 * значения внешних ключей в дочерних записях.
								 * 
								 * События оберток строк Row.BeforeChange и Row.AfterChange 
								 * возникают как реакция на события таблиц оборачиваемого 
								 * датасета DataTable.RowChanging и DataTable.RowChanged.
								 * Это обеспечивает правильное размещение оберток строк
								 * (в коллециях дочерних строк) в контейнере данных.
								 */

								DataRow RowData = dsDataSet.Tables[e.Row.Table.TableName].Rows.Find(key);

								RowData.ItemArray = (object[])e.Row.ItemArray.Clone();
							}

							#region Trace

							Trace.WriteLineIf(
								Data.TraceSwitch.TraceInfo,
								string.Format(
									"-< {0}.AcceptChanges().RowChangingHandler( e.Action = {1}, TableName={2})",
									GetType().Name,
									e.Action,
									e.Row.Table.TableName
									),
								"DATA:"
								);

							#endregion // Trace
						};

					#endregion // rowChangingHandler

					#region rowChangedHandler

					DataRowChangeEventHandler rowChangedHandler =
						delegate (object sender, DataRowChangeEventArgs e)
							{
								#region Trace

								Trace.WriteLineIf(
									Data.TraceSwitch.TraceInfo,
									string.Format(
										"-> {0}.AcceptChanges().RowChangedHandler( e.Action = {1}, TableName={2})",
										GetType().Name,
										e.Action,
										e.Row.Table.TableName
										),
									"DATA:"
									);

								#endregion // Trace

								#region Trace

								Trace.WriteLineIf(
									Data.TraceSwitch.TraceInfo,
									string.Format(
										"-< {0}.AcceptChanges().RowChangedHandler( e.Action = {1}, TableName={2})",
										GetType().Name,
										e.Action,
										e.Row.Table.TableName
										),
									"DATA:"
									);

								#endregion // Trace
							};

					#endregion // rowChangedHandler

					#region Cut relations & foreign key constraints

					/* Целесообразно снести схему с набора данных изменений сразу.
				 * И после этого производить отдельную упаковку 
				 * удаленных, измененных, и добавленных записей.
				 */

					/* DataTable.Clone() 
					 * Clones the structure of the DataTable, 
					 * including all DataTable schemas and constraints. 
					 */

					Changes.Relations.Clear();

					foreach (DataTable table in Changes.Tables)
					{
						List<ForeignKeyConstraint> ForeignKeys =
							new List<ForeignKeyConstraint>(table.Constraints.Count);

						foreach (Constraint constraint in table.Constraints)
						{
							if (constraint is ForeignKeyConstraint)
							{
								ForeignKeys.Add((ForeignKeyConstraint)constraint);
							}
						}

						foreach (ForeignKeyConstraint constraint in ForeignKeys)
						{
							table.Constraints.Remove(constraint);
						}
					}

					#endregion // Cut relations & foreign key constraints

					#region Build deleted

					/* В базе данных могут быть записи в других таблицах, 
				 * связанные с удаляемой записью внешним ключом.
				 * Если эти записи тоже находятся в этом же наборе и 
				 * удалены, то не возникает проблем с удалением данных,
				 * в противном случае будет ошибка сохранения данных в БД.
				 * 
				 * Более того в этом же наборе данных могут быть такие связанные по 
				 * внешнему ключу записи, которые необходимо изменить (чтоб перенаправить 
				 * связь внешнего ключа на другую запись таблицы, может быть даже 
				 * добавляемую запись) прежде чем проводить удаление данных.
				 * То есть в наборе данных все согласовано, однако при "кусочном"
				 * удалить-вставить-изменить могут наблюдаться косяки. 
				 * При внесении изменений в пределах одной таблицы такой порядок вполне
				 * оправдан, чтоб не было конфликтов при вставке записей с существующими
				 * значениями ключа некоторого уникального индекса.
				 * 
				 * По пунктам/случаям. Берем первую таблицу. Сначала получаем записи, 
				 * которые необходимо удалить. Смотрим на запись. Если запись имеет 
				 * дочерние в оригинальной версии (которые должны быть изменены
				 */

					/* Прямо сразу можно собрать идентификаторы удаляемых записей
					 * Довольно сложно колдовать с удаленными записями.
					 * Датасет содержит удаленные записи, те которые реально удалены, 
					 * а также, те которые связаны с удаленными отношениями. 
					 * По идее передавать связанные записи по сети - избыточно и не нужно,
					 * однако нет пути преобразовать датасет так, чтоб он содержал только
					 * удаленные записи. Можно однако создать новый датасет 
					 * с хитрой схемой данных в таблицах которой создать только ключи.
					 * И насоздавать там записей с ключами удаленных записей. Это
					 * будет самый экономичный вариант.
					 */

					DataSet DeletedChanges = Changes.GetChanges(DataRowState.Deleted);

					if (DeletedChanges != null)
					{
						foreach (DataTable table in DeletedChanges.Tables)
						{
							DataTable Table = new DataTable(table.TableName);

							#region Create columns for PrimaryKey only

							foreach (DataColumn column in table.PrimaryKey)
							{
								Table.Columns.Add(
									new DataColumn(
										column.ColumnName,
										column.DataType
										)
									);
							}

							#endregion // Create columns for PrimaryKey only

							#region Define primary key

							DataColumn[] Key = new DataColumn[Table.Columns.Count];

							Table.Columns.CopyTo(Key, 0);

							Table.PrimaryKey = Key;

							#endregion // Define primary key

							#region Copy rows (key only)

							foreach (DataRow row in table.Rows)
								if (row.RowState == DataRowState.Deleted)
								{
									DataRow Row = Table.NewRow();

									foreach (DataColumn column in table.PrimaryKey)
									{
										Row[column.ColumnName] = row[column, DataRowVersion.Original];
									}

									Table.Rows.Add(Row);
								}

							#endregion // Copy rows (key only)

							if (Table.Rows.Count > 0) DeletedData.Tables.Add(Table);
						}

						DeletedData.AcceptChanges();
					}

					/* Вот эту штуку уже вполне целесообразно передавать по сетям
						 */

					#endregion // Deleted

					#region Build added

					/* При синхронизации добавленных записей нет необходимости передавать
				 * на сторону клиента отрицательные значения новых ключей добавленных 
				 * записей. Так как в клиентском наборе данных таких значений ключей 
				 * нет вовсе.
				 */

					DataSet AddedChanges = Changes.GetChanges(DataRowState.Added);

					if (AddedChanges != null)
					{
						foreach (DataTable table in AddedChanges.Tables)
						{
							DataTable Table = table.Clone();

							#region Copy rows

							foreach (DataRow row in table.Rows)
								if (row.RowState == DataRowState.Added)
								{
									DataRow Row = Table.NewRow();

									Row.ItemArray = (object[])row.ItemArray.Clone();

									Table.Rows.Add(Row);
								}

							#endregion // Copy rows

							if (Table.Rows.Count > 0) AddedData.Tables.Add(Table);
						}

						AddedData.AcceptChanges();
					}

					#endregion // Build added

					#region Build modified

					/* Модифицированные записи нужно передавать целиком, так как 
				 * мы совсем не знаем, что именно в этих записях изменено.
				 * Ну и конечно же нет никакой необходимости передавать 
				 * связанные записи. 
				 */

					DataSet ModifiedChanges = Changes.GetChanges(DataRowState.Modified);

					if (ModifiedChanges != null)
					{
						foreach (DataTable table in ModifiedChanges.Tables)
						{
							DataTable Table = table.Clone();

							#region Copy rows

							foreach (DataRow row in table.Rows)
								if (row.RowState == DataRowState.Modified)
								{
									DataRow Row = Table.NewRow();

									Row.ItemArray = (object[])row.ItemArray.Clone();

									Table.Rows.Add(Row);
								}

							#endregion // Copy rows

							if (Table.Rows.Count > 0) ModifiedData.Tables.Add(Table);
						}

						ModifiedData.AcceptChanges();
					}

					#endregion Build modified

					// TODO: Не осталось вызовов, которые попадут в if, убрать
					if (databaseDataSupplier != null)
					{
						#region Collect new row identities from database server during Update

						/* Вообще можно собрать и отрицательные идентификаторы 
					 * добавляемых записей, ну если только для того, чтоб потом 
					 * проконтролировать, что все-все добавляемые записи
					 * имеют реальные, полученные из БД идентификаторы.
					 */

						foreach (DataTable table in Changes.Tables)
						{
							table.RowChanging += rowChangingHandler;
							table.RowChanged += rowChangedHandler;
						}

						/* При вызове этого метода все новые значения ключей будут получены. 
						 * Однако и Added и Modified записи меняют состояние на Unmodified.
						 * И как же узнать после этого как их синхронизировать?
						 * А никак не узнавать. Если запись есть то изменить ее на новые 
						 * значения полей, а если нет, то добавить новую с соответствующими
						 * значениями полей. А вот удаленные записи 
						 */

						databaseDataSupplier.Update(Changes);

						foreach (DataTable table in Changes.Tables)
						{
							table.RowChanging -= rowChangingHandler;
							table.RowChanged -= rowChangedHandler;
						}

						#endregion // Collect new row identities from database server during Update

						for (int i = 0; i < Tables.Count; i++)
						{
							ITable table = Tables[i];

							foreach (IRow row in table.Rows)
							{
								if (row.ID < 0)
								{
									Trace.WriteLineIf(
										Data.TraceSwitch.TraceError,
										string.Format(
											"{0}.AcceptChanges() Data update error: table '{1}', ID < 0",
											GetType().Name,
											table.Name
											),
										"DATA"
										);
								}
							}
						}
					}
					else
					{
						#region Nothing

						/* А если databaseDataSupplier не задан,тогда собранная 
					 * информация о значениях ключей добавленных записей,
					 * о значениях полей измененных записей и 
					 * удаленных записях уже является подходящей для 
					 * распространения изменений по цепи связанных контейнеров 
					 * данных.
					 * 
					 * А значит совсем ничего делать не нужно,
					 * а лишь сформировать событие изменения данных
					 */

						#endregion // Nothing
					}

					Changes.AcceptChanges();

					dsDataSet.AcceptChanges();
				}

				Guid UpdatableVersion = version;

				version = currentVersion;

				OnDataChanged(
				new DataChangedEventArgs(
					UpdatableVersion,
					version,
					DeletedData,
					AddedData,
					ModifiedData
					)
				);

			}
		}

		public virtual void RejectChanges()
		{
			dsDataSet.RejectChanges();
		}

		private void dataSupplier_Changed(EventArgs e)
		{
			Disconnect();
			Connect();
		}

		/// <summary>
		/// Очередь изменений контейнера данных
		/// </summary>
		private readonly Dictionary<Guid, DataChangedEventArgs> changes =
			new Dictionary<Guid, DataChangedEventArgs>();

		/// <summary>
		/// Обработчик события уведомления об изменении данных
		/// </summary>
		/// <param name="e">
		/// аргументы события с набором изменений, 
		/// которые следует применить к данным
		/// </param>
		/// <remarks>
		/// 
		/// </remarks>
		private void dataSupplier_DataChanged(DataChangedEventArgs e)
		{
			lock (this)
			{
				changes.Add(e.UpdatableVersion, e);
				ApplyChanges();
			}
		}

		/// <summary>
		/// Применяет очередные (те которые очереди) изменения данных
		/// </summary>
		/// <remarks>
		/// метод изменяет данные в dsDataSet и потому весь помещен в Lock
		/// </remarks>
		private void ApplyChanges()
		{
			/* Пытаемся применить все изменения из очереди.
			 */
			while (changes.ContainsKey(version))
			{
				ApplyChanges(changes[version]);
			}

			/* Если не все изменения из очереди применились это значит, 
			 * что изменение для текущей версии еще не пришло.
			 * Если изменений в очереди накопилось слишком много - считаем, что 
			 * обновление для текущей версии контейнера потеряно и выбрасываем исключение.
			 */
			if (changes.Count > 15)
				throw new InvalidOperationException("There is no appropriate data change");
		}

		private void ApplyChanges(DataChangedEventArgs e)
		{
			#region Preconditions

			if (e.UpdatableVersion != version)
			{
				throw new InvalidOperationException(
					"Invalid data change version"
					);
			}

			#endregion  // Preconditions

			OnBeforeChange();

			/* Изменения должны применяться последовательно.
			 * Это абсолютно обязательное условие.
			 * А еще лучше если они будут применяться в главном 
			 * потоке приложения, тогда компоненты пользовательского 
			 * интерфейса смогут обновляться безопасно из родного 
			 * потока просто обрабатывая события объектов
			 * контейнера данных
			 */
			lock (this)
			{
				Trace.WriteLineIf(Data.TraceSwitch.TraceInfo,
								  string.Format(
									"this.changes.Count={0} this.version:{1}",
									changes.Count,
									version
									),
								  "DATA"
					);

				changes.Remove(version);

				Trace.WriteLineIf(
					Data.TraceSwitch.TraceInfo,
					string.Format(
						"{0}.ApplyChanges() Queue length: {1}",
						GetType().Name,
						changes.Count
						),
					"DATA"
					);

				//this.ActionBegin();

				//Trace.WriteLine(
				//    string.Format(
				//        "{0}.ApplyChanges Updatable: {1} to Current: {2}",
				//        this.GetType().Name,
				//        e.UpdatableVersion,
				//        e.CurrentVersion
				//        )
				//    );

				applyingChanges = true;

				#region Apply Deleted

				DataSet Deleted = e.Deleted.Copy();

				for (int iTable = Tables.Count - 1; iTable >= 0; iTable--)
				{
					ITable PresentTable = Tables[iTable];

					if (Deleted.Tables.Contains(PresentTable.Name))
					{
						DataTable DeletedDataTable = Deleted.Tables[PresentTable.Name];

						foreach (IRow presentRow in PresentTable.Rows.Clone())
						{
							#region key

							object[] key = new object[PresentTable.DataTable.PrimaryKey.Length];

							for (int i = 0; i < PresentTable.DataTable.PrimaryKey.Length; i++)
							{
								key[i] = presentRow.DataRow[PresentTable.DataTable.PrimaryKey[i], DataRowVersion.Current];
							}

							#endregion // Key

							DataRow DeletedDataRow = DeletedDataTable.Rows.Find(key);

							if (DeletedDataRow != null)
							{
								presentRow.Delete();
							}
						}

						Deleted.Tables.Remove(DeletedDataTable);
					}
				}

				#region Sync deleted for tables without wrappers

				/* Некоторые таблицы, такие как 
				 * VEHICLEGROUP_VEHICLE не имеют оберток 
				 */

				foreach (DataTable deletedDataTable in Deleted.Tables)
				{
					DataTable PresentDataTable = dsDataSet.Tables[deletedDataTable.TableName];

					foreach (DataRow deletedDataRow in deletedDataTable.Rows)
					{
						#region key

						object[] key = new object[PresentDataTable.PrimaryKey.Length];

						for (int i = 0; i < PresentDataTable.PrimaryKey.Length; i++)
						{
							key[i] = deletedDataRow[PresentDataTable.PrimaryKey[i].ColumnName, DataRowVersion.Current];
						}

						#endregion // Key

						DataRow PresentDataRow = PresentDataTable.Rows.Find(key);

						if (PresentDataRow != null)
						{
							PresentDataRow.Delete();
						}
					}
				}

				#endregion // Sync deleted for tables without wrappers

				#endregion // Apply deleted

				#region Apply Added

				DataSet Added = e.Added.Copy();

				for (int iTable = 0; iTable < Tables.Count; iTable++)
				{
					ITable PresentTable = Tables[iTable];

					if (Added.Tables.Contains(PresentTable.Name))
					{
						DataTable AddedDataTable = Added.Tables[PresentTable.Name];

						foreach (DataRow addedDataRow in AddedDataTable.Rows)
						{
							DataRow FuturePresentDataRow = PresentTable.DataTable.NewRow();

							FuturePresentDataRow.ItemArray = (object[])addedDataRow.ItemArray.Clone();

							PresentTable.DataTable.Rows.Add(FuturePresentDataRow);
						}

						Added.Tables.Remove(AddedDataTable);
					}
				}

				#region Sync added for tables without wrappers

				/* Некоторые таблицы, такие как 
			 * VEHICLEGROUP_VEHICLE не имеют оберток 
			 */

				foreach (DataTable addedDataTable in Added.Tables)
				{
					DataTable PresentDataTable = dsDataSet.Tables[addedDataTable.TableName];

					foreach (DataRow addedDataRow in addedDataTable.Rows)
					{
						DataRow FuturePresentDataRow = PresentDataTable.NewRow();

						FuturePresentDataRow.ItemArray = (object[])addedDataRow.ItemArray.Clone();

						PresentDataTable.Rows.Add(FuturePresentDataRow);

						#region Update view of parent rows

						foreach (DataRelation relation in FuturePresentDataRow.Table.ParentRelations)
						{
							foreach (DataRow ParentDataRow in FuturePresentDataRow.GetParentRows(relation))
							{
								ParentDataRow.BeginEdit();
								ParentDataRow.EndEdit();
							}
						}

						#endregion // Update view of parent row
					}
				}

				#endregion // Sync added for tables without wrappers

				#endregion // Apply added

				#region Apply Modified

				DataSet Modified = e.Modified.Copy();

				for (int iTable = Tables.Count - 1; iTable >= 0; iTable--)
				{
					ITable PresentTable = Tables[iTable];

					if (Modified.Tables.Contains(PresentTable.Name))
					{
						DataTable ModifiedDataTable = Modified.Tables[PresentTable.Name];

						foreach (DataRow modifiedDataRow in ModifiedDataTable.Rows)
						{
							#region key

							object[] key = new object[ModifiedDataTable.PrimaryKey.Length];

							for (int i = 0; i < ModifiedDataTable.PrimaryKey.Length; i++)
							{
								key[i] = modifiedDataRow[ModifiedDataTable.PrimaryKey[i], DataRowVersion.Current];
							}

							#endregion // Key

							DataRow presentDataRow = PresentTable.DataTable.Rows.Find(key);

							if (presentDataRow != null)
							{
								presentDataRow.ItemArray = modifiedDataRow.ItemArray;
							}
							else
							{
								/* В этом месте отлавливаются строки, которых нет в текущей таблице, 
								 * но которые пришли в таблице с измененными строками.
								 * Можно либо ругаться, либо тихо добавлять эти строки к существующей
								 * таблице. В данном случае делается второе.
								 */
								DataRow dataRow = PresentTable.DataTable.NewRow();
								dataRow.ItemArray = modifiedDataRow.ItemArray;
								PresentTable.DataTable.Rows.Add(dataRow);
							}
						}
					}
				}

				#endregion // Apply Modified

				AcceptChanges(e.CurrentVersion, null);

				applyingChanges = false;

				// this.ActionEnd();
			}

			OnAfterChange();
		}

		public static readonly TraceSwitch TraceSwitch =
			new TraceSwitch("Data", "", "Warning");
	}

	public abstract class Data<TDataTreater> :
		Data,
		IDataSupplier<TDataTreater>
		where TDataTreater : IDataTreater
	{
		public virtual TDataTreater GetDataTreater()
		{
			return DataSupplier.GetDataTreater();
		}

		public new IDataSupplier<TDataTreater> DataSupplier
		{
			get { return (IDataSupplier<TDataTreater>)base.DataSupplier; }
			set { base.DataSupplier = value; }
		}
	}
}