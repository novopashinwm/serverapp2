using System;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using FORIS.TSS.Common.Collections;

namespace FORIS.TSS.Helpers.Data
{
	public abstract class Row<TRow,TTable, TData>: 
		IDisposable, 
		IRow<TTable, TData>
		where TRow: Row<TRow,TTable, TData>
		where TTable: Table<TRow, TTable, TData>
		where TData: IData
	{
		#region Implement IDataItem

		private TData data;
		[Browsable( false )]
		[DefaultValue( null )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public virtual TData Data
		{
			get { return this.data; }
			set
			{
				if( !object.Equals( this.Data, default( TData ) ) )
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.data = value;

				if( !object.Equals( this.Data, default( TData ) ) )
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		#endregion // Implement IDataItem

		#region ISortable members

		object ISortable.Key
		{
			get { return this; }
		}

		#endregion // ISortable members

		#region IDisposable members

		public void Dispose()
		{
			this.Dispose( true );
		}


		#endregion // IDisposable members

		#region Fields

		/* ���������� � ������� �������� ID ������������ 
		 * ��� ��������� ������������ ������������� ������
		 */

		private int id = 0;
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int ID
		{
			get { return this.id; }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns>������������� ������</returns>
		protected abstract int GetId();

		#endregion Fields

		#region Properties

		private TTable table;
		[Browsable(false)]
		public TTable Table
		{
			get { return this.table; }
		}

		private readonly DataRow dataRow;
		public virtual DataRow DataRow
		{
			get { return dataRow; }
		}

		#endregion // Properties

		#region IRow Members

		int IRow.ID
		{
			get { return this.ID; }
		}
		DataRow IRow.DataRow
		{
			get { return this.DataRow; }
		}

		void IRow.BeginEdit()
		{
			this.OnBeforeChange();

			#region View

			Trace.Assert( !object.Equals( this.Data, default( TData ) ) );

			this.OnDestroyView();

			#endregion // View
		}
		void IRow.CancelEdit()
		{
			#region View

			Trace.Assert( !object.Equals( this.Data, default( TData ) ) );

			this.OnBuildView();
			this.OnUpdateView();

			#endregion // View

			this.OnAfterChange();
		}

		void IRow.EndEdit()
		{
			#region View

			this.OnBuildView();
			this.OnUpdateView();

			#endregion // View

			this.OnAfterChange();
		}

		void IRow.Delete()
		{
			this.Delete();
		}

		#endregion // IRow Members

		#region IRow<TData> members

		TTable IRow<TTable, TData>.Table
		{
			set
			{
				#region Preconditions

				if( this.table != null && !object.Equals( value, default( TTable ) ) )
				{
					throw new ArgumentException();
				}

				#endregion // Preconditions

				this.table = value;
			}
		}

		#endregion // IRow<TData> Members

		#region IComparable members

		public int CompareTo( object obj )
		{
			return this.CompareTo( (TRow)obj );
		}

		protected virtual int CompareTo( TRow row )
		{
			return this.ID.CompareTo( row.ID );
		}

		#endregion IComparable members

		#region Data

		protected virtual void OnBeforeSetData()
		{

		}
		protected virtual void OnAfterSetData()
		{

		}

		#endregion Data

		#region View

		protected virtual void OnDestroyView()
		{
			this.id = -1;
		}
		protected virtual void OnBuildView()
		{
			if( this.DataRow != null )
			{
				this.id = this.GetId();

				this.OnBuildFields();
			}
		}
		protected virtual void OnUpdateView()
		{
		}


		#endregion // View

		#region Consructor & Dispose

		protected Row( DataRow dataRow )
		{
			this.dataRow = dataRow;
		}

		protected virtual void Dispose( bool disposing )
		{
			if( disposing )
			{
				dlgtBeforeChange = null;
				dlgtAfterChange = null;
			}
		}


		#endregion // Constructor & Dispose

		#region Manage

		/// <summary>
		/// ������ ��������� �� ���������
		/// </summary>
		public void Delete()
		{
			Debug.Assert(
				this.DataRow.RowState == DataRowState.Added ||
				this.DataRow.RowState == DataRowState.Modified ||
				this.DataRow.RowState == DataRowState.Unchanged
				);

			this.DataRow.Delete();
		}

		public Row<TRow, TTable, TData> CloneRow()
		{
			throw new NotImplementedException();
		}

		public void SetData( Row<TRow, TTable, TData> Row )
		{
			if( !object.Equals( this.Data, default( TData ) ) )
			{
				this.OnDestroyView();
			}

			this.DataRow.ItemArray = (object[])Row.DataRow.ItemArray.Clone();

			if( !object.Equals( this.Data, default( TData ) ) )
			{
				this.OnBuildView();
				this.OnUpdateView();
			}
		}


		#endregion // Manage

		#region Edit Begin/Cancel/End

		/* ������� �������������� � ���������� ��������� 
		 * �������� ������ �� ������� �������� DataTable
		 */

		public void BeginEdit()
		{
			this.DataRow.BeginEdit();
		}

		public void EndEdit()
		{
			this.DataRow.EndEdit();
		}		
		
		public void CancelEdit()
		{
			this.DataRow.CancelEdit();
		}

		#endregion Edit Begin/Cancel/End

		#region Build fields

		protected abstract void OnBuildFields();

		#endregion Build fields

		#region Events

		private EventHandler dlgtBeforeChange;
		private EventHandler dlgtAfterChange;


		/// <summary>
		/// ������� ����� ���������� ������ ������ (��� ������ BeginEdit() )
		/// </summary>
		public event EventHandler BeforeChange
		{
			add { this.dlgtBeforeChange += value; }
			remove { this.dlgtBeforeChange -= value; }
		}
		/// <summary>
		/// ������� ��������� ������ ������ (��� ������ EndEdit() )
		/// </summary>
		public event EventHandler AfterChange
		{
			add { this.dlgtAfterChange += value; }
			remove { this.dlgtAfterChange -= value; }
		}


		protected virtual void OnBeforeChange()
		{
			if( this.dlgtBeforeChange != null /* && !this.Table.Data.UnderConstruction */ )
			{
				this.dlgtBeforeChange( this, EventArgs.Empty );
			}
		}

		protected virtual void OnAfterChange()
		{
			if( this.dlgtAfterChange != null /* && !this.Table.Data.UnderConstruction */ )
			{
				this.dlgtAfterChange( this, EventArgs.Empty );
			}
		}

		//		protected virtual void OnChanging( RowEventArgs e )
		//		{
		//			
		//		}


		#endregion Events
	}
	
	internal interface IRow<TTable, TData>: 
		IRow,
		IDataItem<TData>, 
		ISortable, 
		IComparable
		where TTable: ITable
		where TData: IData
	{
		TTable Table { set; }
	}

	public interface IRow
	{
		int ID { get; }
		DataRow DataRow { get; }
		
		void BeginEdit();
		void CancelEdit();
		void EndEdit();

		void Delete();
	}

	public delegate void RowEventHandler<TRow, TTable, TData>( object sender, RowEventArgs<TRow, TTable, TData> e )
		where TRow: Row<TRow, TTable, TData>
		where TTable: Table<TRow, TTable, TData>
		where TData: IData;


	public class RowEventArgs<TRow, TTable, TData>: EventArgs
		where TRow: Row<TRow, TTable, TData>
		where TTable: Table<TRow, TTable, TData>
		where TData: IData
	{
		#region Properties

		private readonly TRow row;

		public TRow Row
		{
			get { return this.row; }
		}

		#endregion // Properties

		#region Constructor

		public RowEventArgs( TRow row )
		{
			this.row = row;
		}

		#endregion // Constructor
	}
}