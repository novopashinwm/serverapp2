namespace FORIS.TSS.Helpers.Data
{
	public interface ITableOrder
	{
		ITable this[int index] { get; }

		int Count { get; }
	}
}
