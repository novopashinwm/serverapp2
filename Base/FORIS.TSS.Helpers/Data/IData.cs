﻿using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Data;

namespace FORIS.TSS.Helpers.Data
{
	public interface IData<TDataTreater> : IData, IDataSupplier<TDataTreater> where TDataTreater : IDataTreater
	{
	}
	public interface IData : IDataSupplier
	{
		DataSet DataSet { get; }
		Guid GUID { get; }
		IDataSupplier DataSupplier { get; set; }
		bool ApplyingChanges { get; }
		void AcceptChanges(Guid version, IDatabaseDataSupplier databaseDataSupplier);
		void RejectChanges();

		event EventHandler BeforeChange;
		event EventHandler AfterChange;
	}
}