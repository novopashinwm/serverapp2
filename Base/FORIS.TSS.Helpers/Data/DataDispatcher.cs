﻿using System;
using System.ComponentModel;
using System.Diagnostics;

using FORIS.TSS.Common;

namespace FORIS.TSS.Helpers.Data
{
	/// <summary>
	/// Summary description for DataDispatcher.
	/// </summary>
	[ToolboxItem(true)]
	public class DataDispatcher<TData> :
		Dispatcher<IDataItem<TData>>,
		IDataItem<TData>,
		ITableOrder
		where TData : IData
	{
		#region Public properties

		private TData data = default(TData);
		[Browsable(false)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public TData Data
		{
			get { return this.data; }
			set
			{
				if (!object.Equals(this.Data, value))
				{
					this.OnBeforeChange();

					if (!object.Equals(value, default(TData)))
					{
						this.data = value;

						for (int i = 0; i < this.Ambassadors.Count; i++)
						{
							Ambassador<IDataItem<TData>> ambassador = this.GetAmbassadorCollection()[i];

							if (ambassador.Item != null && ambassador.Enabled)
								ambassador.Item.Data = value;
						}
					}
					else
					{
						for (int i = this.Ambassadors.Count - 1; i >= 0; i--)
						{
							Ambassador<IDataItem<TData>> ambassador = this.GetAmbassadorCollection()[i];

							if (ambassador.Item != null && ambassador.Enabled)
								ambassador.Item.Data = value;
						}

						this.data = value;
					}

					this.OnAfterChange();

					/* Вполне вероятно, что коллекция Ambassadors 
					 * изменится после обработки OnAfterChange()
					 */
				}
			}
		}

		#endregion Public properties

		#region Ambassadors

		private readonly DataAmbassadorCollection<TData> ambassadors =
			new DataAmbassadorCollection<TData>();
		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public DataAmbassadorCollection<TData> Ambassadors
		{
			get { return (DataAmbassadorCollection<TData>)this.GetAmbassadorCollection(); }
		}

		protected override AmbassadorCollection<IDataItem<TData>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

		#endregion // Ambassadors

		#region Constructor & Dispose

		public DataDispatcher(IContainer container) : this()
		{
			container.Add(this);
		}
		public DataDispatcher()
		{

		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.Data = default(TData);

				this.delegateBeforeSetData = null;
				this.delegateAfterSetData = null;

				this.delegateDestroyView = null;
				this.delegateBuildView = null;
				this.delegateUpdateView = null;
			}

			base.Dispose(disposing);
		}


		#endregion // Constructor & Dispose

		#region ITableOrder Members

		/* Диспетчер данных используется в общем случае для 
		 * распространения контейнера по любым компонентам,
		 * а не только по таблицам, но именно порядок таблиц
		 * так важен для применения изменений в контейнере данных.
		 * И чтобы не нагружать разработчика реализующего контейнер
		 * данных позволим ему возвращать в качестве упорядочивающего 
		 * объекта диспетчер данных, в который он помещает послов таблиц
		 * своего контейнера
		 */

		int ITableOrder.Count
		{
			get { return this.Ambassadors.Count; }
		}

		ITable ITableOrder.this[int index]
		{
			get { return (ITable)this.Ambassadors[index].Item; }
		}

		#endregion // ITableOrder Members

		protected override void OnBeforeSetItem(Ambassador<IDataItem<TData>> Ambassador)
		{
			if (Ambassador.Item != null)
			{
				Ambassador.Item.Data = default(TData);
			}
		}
		protected override void OnAfterSetItem(Ambassador<IDataItem<TData>> Ambassador)
		{
			if (Ambassador.Item != null)
			{
				Ambassador.Item.Data = this.data;
			}
		}

		#region Events

		private EventHandler delegateBeforeSetData;
		private EventHandler delegateAfterSetData;

		private EventHandler delegateDestroyView;
		private EventHandler delegateBuildView;
		private EventHandler delegateUpdateView;

		public event EventHandler BeforeSetData
		{
			add { delegateBeforeSetData += value; }
			remove { delegateBeforeSetData -= value; }
		}
		public event EventHandler AfterSetData
		{
			add { delegateAfterSetData += value; }
			remove { delegateAfterSetData -= value; }
		}


		public event EventHandler DestroyView
		{
			add { delegateDestroyView += value; }
			remove { delegateDestroyView -= value; }
		}
		public event EventHandler BuildView
		{
			add { delegateBuildView += value; }
			remove { delegateBuildView -= value; }
		}
		public event EventHandler UpdateView
		{
			add { delegateUpdateView += value; }
			remove { delegateUpdateView -= value; }
		}


		private EventHandler m_BeforeAction;
		private EventHandler m_AfterAction;

		[Category("Actions")]
		public event EventHandler BeforeAction
		{
			add { m_BeforeAction += value; }
			remove { m_BeforeAction -= value; }
		}
		[Category("Actions")]
		public event EventHandler AfterAction
		{
			add { m_AfterAction += value; }
			remove { m_AfterAction -= value; }
		}


		protected virtual void OnBeforeAction(EventArgs e)
		{
			m_BeforeAction?.Invoke(this, e);
		}
		protected virtual void OnAfterAction(EventArgs e)
		{
			m_AfterAction?.Invoke(this, e);
		}


		protected override void OnBeforeChange()
		{
			if (!object.Equals(this.Data, default(TData)))
			{
				try
				{
					delegateBeforeSetData?.Invoke(this, EventArgs.Empty);
					delegateDestroyView?.Invoke(this, EventArgs.Empty);
				}
				catch (Exception Ex)
				{
					Debug.WriteLine("DataDispatcher.OnBeforeChange() Exception: " + Ex.Message);
					Debug.WriteLine(Ex.StackTrace);
				}
			}

			base.OnBeforeChange();
		}
		protected override void OnAfterChange()
		{
			base.OnAfterChange();

			if (!object.Equals(this.Data, default(TData)))
			{
				try
				{
					delegateBuildView?.Invoke(this, EventArgs.Empty);
					delegateUpdateView?.Invoke(this, EventArgs.Empty);
					delegateAfterSetData?.Invoke(this, EventArgs.Empty);
				}
				catch (Exception Ex)
				{
					Debug.WriteLine("DataDispatcher.OnAfterChange() Exception: " + Ex.Message);
					Debug.WriteLine(Ex.StackTrace);
				}
			}
		}


		#endregion Events
	}

	public class DataAmbassadorCollection<TData> :
		AmbassadorCollection<IDataItem<TData>>
		where TData : IData
	{
		public new DataAmbassador<TData> this[int Index]
		{
			get { return (DataAmbassador<TData>)base[Index]; }
		}
	}

	[DesignTimeVisible(false)]
	public class DataAmbassador<TData> : Ambassador<IDataItem<TData>>
		where TData : IData
	{
	}
}