﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Text;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;

namespace FORIS.TSS.Helpers.Data
{
	[DesignTimeVisible(false)]
	public abstract class Table<TRow, TTable, TData> :
		Component,
		ITable,
		IDataItem<TData>
		where TRow : Row<TRow, TTable, TData>
		where TTable : Table<TRow, TTable, TData>
		where TData : IData
	{
		#region Implement IDataItem

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public virtual TData Data
		{
			get { return this.ddMain.Data; }
			set
			{
				if (
					!object.Equals(this.ddMain.Data, default(TData)) &&
					this.ddMain.Data.DataSet.Tables.Contains(this.Name)
					)
				{
					this.OnBeforeSetData();

					this.OnDestroyView();
				}

				this.ddMain.Data = value;

				if (
					!object.Equals(this.ddMain.Data, default(TData)) &&
					this.ddMain.Data.DataSet.Tables.Contains(this.Name)
					)
				{
					this.OnBuildView();
					this.OnUpdateView();

					this.OnAfterSetData();
				}
			}
		}

		#endregion Implement IDataItem

		#region ITable Members

		public abstract string Name { get; }


		#endregion ITable Members

		#region Rows

		/// <summary> Коллекция оберток строк </summary>
		protected internal readonly InternalRowCollection<TRow, TTable, TData> rows =
			new InternalRowCollection<TRow, TTable, TData>();
		/// <summary> Коллекция оберток строк, изменяемых через обертку </summary>
		private readonly InternalRowCollection<TRow, TTable, TData> changingRows =
			new InternalRowCollection<TRow, TTable, TData>();

		public IRowCollection<TRow, TTable, TData> Rows
		{
			get { return this.rows; }
		}

		#endregion Rows

		#region ITable Members

		IRowCollection ITable.Rows
		{
			get { return this.rows; }
		}

		#endregion ITable Members

		#region Data

		private DataTable dataTable;
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public DataTable DataTable
		{
			get { return dataTable; }
		}
		protected virtual void OnBeforeSetData()
		{
			#region DataTable supervision

			if (this.DataTable != null)
			{
				this.DataTable.RowDeleting -= new DataRowChangeEventHandler(DataTable_RowDeleting);
				this.DataTable.RowDeleted -= new DataRowChangeEventHandler(DataTable_RowDeleted);
				this.DataTable.RowChanging -= new DataRowChangeEventHandler(DataTable_RowChanging);
				this.DataTable.RowChanged -= new DataRowChangeEventHandler(DataTable_RowChanged);
			}

			#endregion DataTable supervision
		}
		protected virtual void OnAfterSetData()
		{
			#region DataTable supervision

			if (this.DataTable != null)
			{
				this.DataTable.RowDeleting += new DataRowChangeEventHandler(DataTable_RowDeleting);
				this.DataTable.RowDeleted += new DataRowChangeEventHandler(DataTable_RowDeleted);
				this.DataTable.RowChanging += new DataRowChangeEventHandler(DataTable_RowChanging);
				this.DataTable.RowChanged += new DataRowChangeEventHandler(DataTable_RowChanged);
			}

			#endregion DataTable supervision
		}

		#endregion Data

		#region Data events

		/// <summary> Используется для сохранения объектов оберток измененных строк, с целью возвращения их в this.rows при завершении изменений </summary>
		private readonly InternalRowCollection<TRow, TTable, TData> dataChangingRows =
			new InternalRowCollection<TRow, TTable, TData>();
		/// <summary> Используется для сохранения объектов оберток удаленных строк, с целью перемещения их в this.transactionDeletedRows при завершении удаления </summary>
		private readonly Dictionary<DataRow, TRow> dataDeletingRows =
			new Dictionary<DataRow, TRow>();
		private readonly Dictionary<DataRow, IRow> transactionChangedRows =
			new Dictionary<DataRow, IRow>();
		/// <summary> Используется для сохранения объектов оберток удаленных строк, с целью возвращения их в this.rows при завершении отката изменений </summary>
		private readonly Dictionary<DataRow, TRow> transactionDeletedRows =
			new Dictionary<DataRow, TRow>();
		private void DataTable_RowDeleting(object sender, DataRowChangeEventArgs e)
		{
			#region Preconditions

			if (this.dataTable == null)
			{
				throw new InvalidOperationException(
					"В обертке нет таблицы данных при обработке оберткой события таблицы данных");
			}

			if (
				!(this.Data.DataSupplier is FinalDataSupplier) &&
				!this.Data.ApplyingChanges
				)
			{
				throw new InvalidOperationException(
					"Only root server data can be modified outside Data.ApplyChanges() method");
			}

			#endregion Preconditions

			switch (e.Action)
			{
				#region Delete

				case DataRowAction.Delete:
					{
						#region Assert

						Trace.Assert(
							this.rows.ContainsDataRow(e.Row),
							"Deleting row without wrapper is not allowed"
							);

						#endregion Assert

						TRow Row = this.rows[e.Row];

						this.dataDeletingRows.Add(e.Row, Row);

						try
						{
							#region Force removing child rows first

							/* Форсируем удаление строк по дочерним связям,
						 * чтоб их удаление происходило ДО того как строка 
						 * исчезнет из коллекции Rows 
						 */

							foreach (DataRelation rel in this.DataTable.ChildRelations)
							{
								foreach (DataRow row in Row.DataRow.GetChildRows(rel))
									row.Delete();
							}

							#endregion Force removing child rows first

							this.rows.Remove(Row);

						}
						finally
						{
							((IRow<TTable, TData>)Row).Table = null;
						}
					}
					break;

					#endregion Delete
			}
		}
		private void DataTable_RowDeleted(object sender, DataRowChangeEventArgs e)
		{
			#region Preconditions

			if (this.dataTable == null)
			{
				throw new InvalidOperationException(
					"В обертке нет таблицы данных при обработке оберткой события таблицы данных"
					);
			}

			#endregion Preconditions

			switch (e.Action)
			{
				#region Delete

				case DataRowAction.Delete:
					{
						Trace.Assert(
							this.dataDeletingRows.ContainsKey(e.Row),
							"Deleted row's wrapper has been lost"
							);

						TRow Row = this.dataDeletingRows[e.Row];

						this.dataDeletingRows.Remove(e.Row);

						this.transactionDeletedRows.Add(e.Row, Row);
					}
					break;

					#endregion Delete
			}
		}
		private void DataTable_RowChanging(object sender, DataRowChangeEventArgs e)
		{
			#region Preconditions

			if (this.dataTable == null)
			{
				throw new InvalidOperationException(
					"В обертке нет таблицы данных при обработке оберткой события таблицы данных"
					);
			}

			if (
				!(this.Data.DataSupplier is FinalDataSupplier) &&
				!this.Data.ApplyingChanges
				)
			{
				throw new InvalidOperationException(
					"Only root server data can be modified outside Data.ApplyChanges() method"
					);
			}

			/* Отладочную информацию с данными строки 
			 * нельзя получить для удаляемой строки 
			 * в действии Commit 
			 */

			#endregion Preconditions

			switch (e.Action)
			{
				#region Change

				case DataRowAction.Change:
					{
						#region Assert

						Trace.Assert(
							this.rows.ContainsDataRow(e.Row),
							"Editing row without wrapper is not allowed"
							);

						#endregion Assert

						#region Текст

						/* Возникает такая хитрая фишка.
						 * Если разработчик вызвал BeginEdit() 
						 * для обертки заранее, то строки уже нет в 
						 * this.rows
						 * 
						 * Cобытие это возникает лишь при 
						 * вызове EndEdit() для строки данных.
						 * 
						 * Когда выполняется Update для датасета,
						 * значение ключа меняется в строке на
						 * полученное из БД без вызова BeginEdit() 
						 * для обертки строки данных. И поэтому
						 * строка находится все еще в this.rows
						 */

						#endregion Текст

						TRow Row = this.rows[e.Row];

						((IRow)Row).BeginEdit();

						this.dataChangingRows.Add(Row);
					}
					break;

				#endregion Change

				#region Commit

				case DataRowAction.Commit:
					{
						if (!(
							this.rows.ContainsDataRow(e.Row) ||
							this.transactionChangedRows.ContainsKey(e.Row) ||
							this.transactionDeletedRows.ContainsKey(e.Row)
							))
						{
							throw new InvalidOperationException(
								"Row's wrapper has been lost before Commit"
								);
						}

						/* Не надо ничего делать обертка уже в том 
						 * состоянии, в каком будет после Commit
						 */
					}
					break;

				#endregion Commit

				#region Rollback

				case DataRowAction.Rollback:
					{
						#region Preconditions

						if (
							!this.rows.ContainsDataRow(e.Row) &&
							!this.transactionChangedRows.ContainsKey(e.Row) &&
							!this.transactionDeletedRows.ContainsKey(e.Row)
							)
						{
							throw new InvalidOperationException(
								"Row's wrapper has been lost before Rollback"
								);
						}

						#endregion Preconditions

						if (this.rows.ContainsDataRow(e.Row))
						{
							IRow Row = this.rows[e.Row];

							Row.BeginEdit();

							this.transactionChangedRows.Add(e.Row, Row);
						}
					}
					break;

					#endregion Rollback
			}
		}
		private void DataTable_RowChanged(object sender, DataRowChangeEventArgs e)
		{
			#region Preconditions

			if (this.dataTable == null)
			{
				throw new InvalidOperationException(
					"В обертке нет таблицы данных при обработке оберткой события таблицы данных"
					);
			}

			/* Отладочную информацию с данными строки 
			 * нельзя получить для удаляемой строки
			 * в действии Commit
			 */

			#endregion Preconditions

			switch (e.Action)
			{
				#region Add

				case DataRowAction.Add:
					{
						TRow Row;

						if (this.hashQueue.ContainsKey(e.Row))
						{
							Row = this.hashQueue[e.Row];

							this.hashQueue.Remove(e.Row);
						}
						else
						{
							/* Строка добавлена мимо обертки таблицы ( Table.NewRow() )
							 * Значит обертку нужно создать (по всем правилам) 
							 */

							Trace.WriteLine("RowCollection.DataTable_RowChanged() row added beside queue");

							Row = this.OnCreateRow(e.Row);

							((IRow<TTable, TData>)Row).Table = (TTable)this;
						}

						/* В этом месте так как обертка в таблицу еще не добавлена,
						 * значения полей (и ключа) не посчитаны и все по нулям.
						 * 
						 * А следовательно Row.ID всегда 0 и this.rows 
						 * такого ключа не содержит. Глупая неуместная проверка.
						 */

						Debug.Assert(Row.Table == this);

						Debug.Assert(!this.rows.ContainsID(Row.ID));

						try
						{
							this.rows.Add(Row);
						}
						catch (Exception ex)
						{
							Row.DataRow.RowError = ex.Message;
						}
					}
					break;

				#endregion Add

				#region Change

				case DataRowAction.Change:
					{
						#region Assert

						Trace.Assert(
							!this.rows.ContainsDataRow(e.Row) &&
							this.dataChangingRows.ContainsDataRow(e.Row),
							"Call BeginEdit() is required before change data row"
							);

						#endregion Assert

						if (this.dataChangingRows.ContainsDataRow(e.Row))
						{
							TRow Row = this.dataChangingRows[e.Row];

							this.dataChangingRows.Remove(Row);

							((IRow)Row).EndEdit();
						}
					}
					break;

				#endregion Change

				#region Commit

				case DataRowAction.Commit:

					Trace.Assert(
						this.rows.ContainsDataRow(e.Row) ||
						this.transactionChangedRows.ContainsKey(e.Row) ||
						this.transactionDeletedRows.ContainsKey(e.Row),
						"Row's wrapper has been lost at Commit"
						);

					if (this.transactionChangedRows.ContainsKey(e.Row))
					{
						this.transactionChangedRows.Remove(e.Row);
					}
					else if (this.transactionDeletedRows.ContainsKey(e.Row))
					{
						this.transactionDeletedRows.Remove(e.Row);
					}
					else if (this.rows.ContainsDataRow(e.Row))
					{
					}
					else
						throw new ApplicationException();

					break;

				#endregion Commit

				#region Rollback

				case DataRowAction.Rollback:
					{
						#region Preconditions

						if (
							!this.rows.ContainsDataRow(e.Row) &&
							!this.transactionChangedRows.ContainsKey(e.Row) &&
							!this.transactionDeletedRows.ContainsKey(e.Row)
							)
						{
							throw new InvalidOperationException(
								"Row's wrapper has been lost at Rollback"
								);
						}

						#endregion Preconditions

						if (this.transactionChangedRows.ContainsKey(e.Row))
						{
							IRow Row = this.transactionChangedRows[e.Row];

							Row.CancelEdit();

							this.transactionChangedRows.Remove(e.Row);
						}
						else if (this.transactionDeletedRows.ContainsKey(e.Row))
						{
							TRow Row = this.transactionDeletedRows[e.Row];

							#region return Row to this.rows

							((IRow<TTable, TData>)Row).Table = (TTable)this;

							try
							{
								this.rows.Add(Row);
							}
							catch (Exception ex)
							{
								Row.DataRow.RowError = ex.Message;
							}

							#endregion Return Row to this.rows

							this.transactionDeletedRows.Remove(e.Row);
						}
						else if (this.rows.ContainsDataRow(e.Row))
						{
						}
						else
							throw new ApplicationException();
					}
					break;

					#endregion Rollback
			}
		}

		#endregion Data events

		#region View

		protected virtual void OnDestroyView()
		{
			try
			{
				lock (this.rows)
				{
					this.rows.Clear();
				}

				dataTable = null;
			}
			catch (Exception ex)
			{
				this.GetTypeName(false)
					.WithException(ex, true)
					.CallDebugError();
			}
		}
		protected virtual void OnBuildView()
		{
			try
			{
				Debug.Assert(this.Name != null && this.Name != String.Empty);

				this.dataTable = this.Data.DataSet.Tables[this.Name];
			}
			catch (Exception ex)
			{
				this.GetTypeName(false)
					.WithException(ex, true)
					.CallDebugError();
			}
		}
		protected virtual void OnUpdateView()
		{
			try
			{
				if (this.DataTable != null)
				{
					lock (this.rows)
					{
						foreach (DataRow rowData in this.DataTable.Rows)
							if (rowData.RowState != DataRowState.Detached)
							{
								TRow Row = this.OnCreateRow(rowData);
								((IRow<TTable, TData>)Row).Table = (TTable)this;
								this.rows.Add(Row);
							}
					}
				}
			}
			catch (Exception ex)
			{
				this.GetTypeName(false)
					.WithException(ex, true)
					.CallDebugError();
			}
		}

		#endregion View

		#region Members

		private Dictionary<DataRow, TRow> hashQueue;
		private EventHandler rowBeforeChangeHandler;
		private EventHandler rowAfterChangeHandler;

		#endregion Members

		#region Components

		private System.ComponentModel.IContainer components;
		protected FORIS.TSS.Helpers.Data.DataDispatcher<TData> ddMain;
		protected FORIS.TSS.Helpers.Data.DataDispatcher<TData> ddRows;
		private FORIS.TSS.Helpers.Data.DataDispatcher<TData> ddChangingRows;
		protected FORIS.TSS.Helpers.Data.DataDispatcher<TData> ddClones;
		private FORIS.TSS.Helpers.Data.DataAmbassador<TData> daClonesDataDispatcher;
		protected FORIS.TSS.Helpers.Data.DataAmbassador<TData> daRowsDataDispatcher;
		private FORIS.TSS.Helpers.Data.DataAmbassador<TData> daChangingRowsDataDispatcher;

		#endregion Components

		#region Constructor & Dispose

		/// <summary> Default constructor. For designer only </summary>
		protected Table()
		{
			InitializeComponent();
			Initialize();
		}
		protected Table(IContainer Container)
		{
			Container.Add(this);
			InitializeComponent();
			Initialize();
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.rows.Clear();
				this.changingRows.Clear();

				if (components != null)
				{
					components.Dispose();
				}
			}

			base.Dispose(disposing);
		}

		#endregion Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.ddMain = new FORIS.TSS.Helpers.Data.DataDispatcher<TData>(this.components);
			this.daRowsDataDispatcher = new FORIS.TSS.Helpers.Data.DataAmbassador<TData>();
			this.daChangingRowsDataDispatcher = new FORIS.TSS.Helpers.Data.DataAmbassador<TData>();
			this.ddRows = new FORIS.TSS.Helpers.Data.DataDispatcher<TData>(this.components);
			this.ddChangingRows = new FORIS.TSS.Helpers.Data.DataDispatcher<TData>(this.components);
			this.ddClones = new FORIS.TSS.Helpers.Data.DataDispatcher<TData>(this.components);
			this.daClonesDataDispatcher = new FORIS.TSS.Helpers.Data.DataAmbassador<TData>();
			// 
			// ddMain
			// 
			this.ddMain.Ambassadors.Add(this.daRowsDataDispatcher);
			this.ddMain.Ambassadors.Add(this.daChangingRowsDataDispatcher);
			this.ddMain.Ambassadors.Add(this.daClonesDataDispatcher);
			// 
			// daRowsDataDispatcher
			// 
			this.daRowsDataDispatcher.Item = this.ddRows;
			//
			// daChangingRowsDataDispatcher
			//
			this.daChangingRowsDataDispatcher.Item = this.ddChangingRows;
			// 
			// daClonesDataDispatcher
			// 
			this.daClonesDataDispatcher.Item = this.ddClones;

		}

		#endregion Component Designer generated code

		#region Initialize

		private void Initialize()
		{
			this.rowBeforeChangeHandler = new EventHandler(Row_BeforeChange);
			this.rowAfterChangeHandler = new EventHandler(Row_AfterChange);

			this.rows.AddRow += new RowEventHandler<TRow, TTable, TData>(rows_AddRow);
			this.rows.Inserting += new CollectionChangeEventHandler<TRow>(rows_Inserting);
			this.rows.Inserted += new CollectionChangeEventHandler<TRow>(rows_Inserted);
			this.rows.Removing += new CollectionChangeEventHandler<TRow>(rows_Removing);
			this.rows.Removed += new CollectionChangeEventHandler<TRow>(rows_Removed);
			this.rows.Clearing += new CollectionEventHandler<TRow>(rows_Clearing);
			this.rows.Cleared += new CollectionEventHandler<TRow>(rows_Cleared);

			this.changingRows.Inserting += new CollectionChangeEventHandler<TRow>(changingRows_Inserting);
			this.changingRows.Inserted += new CollectionChangeEventHandler<TRow>(changingRows_Inserted);
			this.changingRows.Removing += new CollectionChangeEventHandler<TRow>(changingRows_Removing);
			this.changingRows.Removed += new CollectionChangeEventHandler<TRow>(changingRows_Removed);
			this.changingRows.Clearing += new CollectionEventHandler<TRow>(changingRows_Clearing);
			this.changingRows.Cleared += new CollectionEventHandler<TRow>(changingRows_Cleared);

			this.hashQueue = new Dictionary<DataRow, TRow>();
		}

		#endregion Initialize

		#region Actions

		/// <summary> Ищет строку с указанным идентификатором, если строка c таким идентификатором не найдена, то возвращает null </summary>
		/// <param name="id"> идентификатор строки данных </param>
		/// <returns> строка данных </returns>
		public TRow FindRow(int? id)
		{
			if (id != null)
			{
				if (this.rows.ContainsID((int)id))
				{
					return this.rows[(int)id];
				}
				else if (this.dataChangingRows.ContainsID((int)id))
				{
					return this.dataChangingRows[(int)id];
				}
			}

			return null;
		}
		public TRow CloneRow(TRow Row)
		{
			Trace.Assert(Row.Table == this);

			TRow Result = this.NewRow();

			Result.DataRow.ItemArray = (object[])Row.DataRow.ItemArray.Clone();

			this.ddClones.Ambassadors.Add(Result);

			return Result;
		}

		#endregion Actions

		#region rows events

		private void rows_AddRow(object sender, RowEventArgs<TRow, TTable, TData> e)
		{
			Debug.Assert(this.DataTable != null, "Нет обертки " + this.GetType().FullName);

			this.hashQueue.Add(e.Row.DataRow, e.Row);

			this.DataTable.Rows.Add(e.Row.DataRow);

			if (e.Row.DataRow.HasErrors)
			{
				Trace.WriteLine("Row {" + e.Row.ID + "} was added in " + this.DataTable.TableName + " abortively with error: " + e.Row.DataRow.RowError);

				this.DataTable.Rows.Find(e.Row.ID).Delete();

				throw new ApplicationException(e.Row.DataRow.RowError);
			}
		}
		private void rows_Inserting(object sender, CollectionChangeEventArgs<TRow> e)
		{
			TRow Row = e.Item;

			this.ddRows.Ambassadors.Add(Row);
		}
		private void rows_Inserted(object sender, CollectionChangeEventArgs<TRow> e)
		{
			TRow Row = e.Item;

			Row.BeforeChange += this.rowBeforeChangeHandler;
			Row.AfterChange += this.rowAfterChangeHandler;

			this.OnRowActionShow(Row);
		}
		private void rows_Removing(object sender, CollectionChangeEventArgs<TRow> e)
		{
			TRow Row = e.Item;

			this.OnRowActionHide(Row);

			Row.BeforeChange -= this.rowBeforeChangeHandler;
			Row.AfterChange -= this.rowAfterChangeHandler;
		}
		private void rows_Removed(object sender, CollectionChangeEventArgs<TRow> e)
		{
			TRow Row = e.Item;

			this.ddRows.Ambassadors.Remove(Row);
		}
		private void rows_Clearing(object sender, CollectionEventArgs<TRow> e)
		{
			foreach (TRow row in this.rows)
			{
				this.OnRowActionHide(row);

				row.BeforeChange -= this.rowBeforeChangeHandler;
				row.AfterChange -= this.rowAfterChangeHandler;
			}
		}
		private void rows_Cleared(object sender, CollectionEventArgs<TRow> e)
		{
			this.ddRows.Ambassadors.Clear();
		}

		#endregion rows events

		#region Handle changingRows events

		private void changingRows_Inserting(object sender, CollectionChangeEventArgs<TRow> e)
		{
			TRow Row = e.Item;

			this.ddChangingRows.Ambassadors.Add(Row);
		}
		private void changingRows_Inserted(object sender, CollectionChangeEventArgs<TRow> e)
		{
			TRow Row = e.Item;

			Row.BeforeChange += this.rowBeforeChangeHandler;
			Row.AfterChange += this.rowAfterChangeHandler;
		}
		private void changingRows_Removing(object sender, CollectionChangeEventArgs<TRow> e)
		{
			TRow Row = e.Item;

			Row.BeforeChange -= this.rowBeforeChangeHandler;
			Row.AfterChange -= this.rowAfterChangeHandler;
		}
		private void changingRows_Removed(object sender, CollectionChangeEventArgs<TRow> e)
		{
			TRow Row = e.Item;

			this.ddChangingRows.Ambassadors.Remove(Row);
		}
		private void changingRows_Clearing(object sender, CollectionEventArgs<TRow> e)
		{
			foreach (TRow row in this.changingRows)
			{
				row.BeforeChange -= this.rowBeforeChangeHandler;
				row.AfterChange -= this.rowAfterChangeHandler;
			}
		}
		private void changingRows_Cleared(object sender, CollectionEventArgs<TRow> e)
		{
			this.ddChangingRows.Ambassadors.Clear();
		}

		#endregion Handle changingRows events

		#region Row level

		public TRow NewRow()
		{
			if (this.DataTable != null)
			{
				DataRow DataRow = this.DataTable.NewRow();

				TRow Row = this.OnCreateRow(DataRow);

				/* Совершенно необходимо чтоб обертка строки 
				 * сразу после создания могла обращаться к 
				 * обертке таблицы, чтоб пользоваться свойствами
				 * индексов полей таблицы.
				 * 
				 * Возможно даже следует добавить обертку таблицы 
				 * в параметры конструктора обертки строки
				 * И сделать свойство Row.Table только для чтения.
				 */
				((IRow<TTable, TData>)Row).Table = (TTable)this;

				return Row;
			}
			else
				throw new InvalidOperationException("Table must be builded before call NewRow()");
		}

		#endregion Row level

		#region Row events

		/* Как же быть?
		 * Мы не можем просто удалить обертку строки из коллекции 
		 * оберток строк (this.rows) обертки таблицы при изменении строки, 
		 * так как при этом будут оторваны местные обработчики событий 
		 * изменения строки и мы потеряем эту строку навсегда. (вместе с оберткой ;-)
		 *
		 * Но мы можем не просто удалить обертку из коллекции this.rows
		 * а поместить ее при этом в специальную коллекцию изменяемых строк.
		 */

		private void Row_BeforeChange(object sender, EventArgs e)
		{
			TRow Row = (TRow)sender;

			this.rows.Remove(Row);
			this.changingRows.Add(Row);
		}
		private void Row_AfterChange(object sender, EventArgs e)
		{
			TRow Row = (TRow)sender;

			this.changingRows.Remove(Row);
			this.rows.Add(Row);
		}
		protected abstract TRow OnCreateRow(DataRow dataRow);
		protected virtual void OnRowActionHide(TRow row)
		{
		}
		protected virtual void OnRowActionShow(TRow row)
		{
		}

		#endregion Row events

		#region Events

		private      EventHandler m_BeforeBuild;
		private      EventHandler m_AfterBuild;
		public event EventHandler BeforeBuild
		{
			add { m_BeforeBuild += value; }
			remove { m_BeforeBuild -= value; }
		}
		public event EventHandler AfterChange
		{
			add { m_AfterBuild += value; }
			remove { m_AfterBuild -= value; }
		}

		protected virtual void OnBeforeBuild()
		{
			if (m_BeforeBuild != null)
				m_BeforeBuild(this, EventArgs.Empty);
		}
		protected virtual void OnAfterBuild()
		{
			if (m_AfterBuild != null)
				m_AfterBuild(this, EventArgs.Empty);
		}

		#endregion Events 
	}

	public interface IRowCollection<TRow, TTable, TData> :
		ICollectionWithEvents<TRow>,
		IRowCollection
		where TRow : Row<TRow, TTable, TData>
		where TTable : Table<TRow, TTable, TData>
		where TData : IData
	{
		new TRow this[int id] { get; }
		new void Add(TRow Row);
		new int Count { get; }
		bool Contains(int id);
	}
	public interface IRowCollection :
		ICollection
	{
		IRowCollection Clone();
	}
	public interface ITable
	{
		string Name { get; }
		DataTable DataTable { get; }
		IRowCollection Rows { get; }
	}
}