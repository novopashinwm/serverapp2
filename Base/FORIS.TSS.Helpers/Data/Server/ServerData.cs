﻿using System.ComponentModel;
using FORIS.TSS.BusinessLogic.Data.Server;

namespace FORIS.TSS.Helpers.Data.Server
{
	public class ServerData :
		Data<IServerDataTreater>,
		IServerData
	{
		#region Controls & Components

		private System.ComponentModel.IContainer components;
		private ServerDataDispatcher tablesDataDispatcher;

		private SessionTable session;
		private ParamTable param;

		private ServerDataAmbassador sessionAmbassador;
		private ServerDataAmbassador paramAmbassador;

		#endregion Controls & Components

		#region Constructor & Dispose

		public ServerData()
		{
			InitializeComponent();
		}
		public ServerData(IContainer container) : this()
		{
			container.Add(this);
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
				components?.Dispose();

			base.Dispose(disposing);
		}

		#endregion Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			tablesDataDispatcher = new ServerDataDispatcher();

			session = new SessionTable(components);
			param = new ParamTable(components);

			sessionAmbassador = new ServerDataAmbassador();
			paramAmbassador = new ServerDataAmbassador();

			sessionAmbassador.Item = session;
			paramAmbassador.Item = param;

			tablesDataDispatcher.Ambassadors.Add(sessionAmbassador);
			tablesDataDispatcher.Ambassadors.Add(paramAmbassador);
		}

		#endregion Component Designer generated code

		#region Tables' order

		protected override ITableOrder Tables
		{
			get { return tablesDataDispatcher; }
		}

		#endregion Tables' order

		#region Build & Destroy

		protected override void Destroy()
		{
			tablesDataDispatcher.Data = null;
		}
		protected override void Build()
		{
			tablesDataDispatcher.Data = this;
		}

		#endregion Build & Destroy

		#region Tables properties

		[Browsable(false)]
		[Category("Tables")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public SessionTable Session
		{
			get { return session; }
		}
		[Browsable(false)]
		[Category("Tables")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public ParamTable Param
		{
			get { return param; }
		}

		#endregion Tables properties
	}
}