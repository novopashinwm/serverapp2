using System.ComponentModel;
using FORIS.TSS.Common;

namespace FORIS.TSS.Helpers.Data.Server
{
	public class ServerDataDispatcher:
		DataDispatcher<IServerData>
	{
		public ServerDataDispatcher()
		{
			
		}
		public ServerDataDispatcher( IContainer container ): this()
		{
			container.Add( this );
		}

		private readonly ServerDataAmbassadorCollection ambassadors =
			new ServerDataAmbassadorCollection();

		protected override AmbassadorCollection<IDataItem<IServerData>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public new ServerDataAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}
	}

	public class ServerDataAmbassadorCollection:
		DataAmbassadorCollection<IServerData>
	{
		public new ServerDataAmbassador this[ int index ]
		{
			get { return (ServerDataAmbassador)base[index]; }
		}
	}

	public class ServerDataAmbassador:
		DataAmbassador<IServerData>
	{
		
	}
}
