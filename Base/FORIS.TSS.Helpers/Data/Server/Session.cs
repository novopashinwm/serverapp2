using System;
using System.ComponentModel;
using System.Data;
using System.Runtime.Remoting.Lifetime;

namespace FORIS.TSS.Helpers.Data.Server
{
	public class SessionTable: 
		Table<SessionRow, SessionTable, IServerData>
	{
		#region Properties

		public override string Name
		{
			get { return "SESSION"; }
		}

		#endregion // Properties

		public SessionTable()
		{
			
		}
		public SessionTable( IContainer container )
		{
			container.Add( this );
		}

		protected override SessionRow OnCreateRow( DataRow dataRow )
		{
			return new SessionRow( dataRow );
		}
	}

	public class SessionRow:
		Row<SessionRow, SessionTable, IServerData>
	{
		#region Fields

		private const int SESSION_ID = 0;
		private const int LOGIN = 1;
		private const int LEASE_TIME = 2;
		private const int LEASE_STATE = 3;
		
		public int SessionId
		{
			get { return (int)this.DataRow[SessionRow.SESSION_ID, DataRowVersion.Current]; }
			set { this.DataRow[SessionRow.SESSION_ID] = value; }
		}

		public string Login
		{
			get { return (string)this.DataRow[SessionRow.LOGIN, DataRowVersion.Current]; }
			set { this.DataRow[SessionRow.LOGIN] = value; }
		}

		public TimeSpan? LeaseTime
		{
			get
			{
				return
					!this.DataRow.IsNull( SessionRow.LEASE_TIME )
						?
                            (TimeSpan)this.DataRow[SessionRow.LEASE_TIME, DataRowVersion.Current]
						:
							(TimeSpan?)null;
			}
			set { this.DataRow[SessionRow.LEASE_TIME] = value ?? (object)DBNull.Value;}
		}

		public LeaseState? LeaseState
		{
			get
			{
				return
					!this.DataRow.IsNull( SessionRow.LEASE_STATE )
						?
                            (LeaseState)this.DataRow[SessionRow.LEASE_STATE, DataRowVersion.Current]
						:
							(LeaseState?)null;
			}
			set { this.DataRow[SessionRow.LEASE_STATE] = value ?? (object)DBNull.Value;}
		}

		#endregion // Fields

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[SessionRow.SESSION_ID];
		}

		#endregion // Id

		public SessionRow( DataRow dataRow ): base( dataRow )
		{
			
		}

		protected override void OnBuildFields()
		{
			
		}
	}
}
