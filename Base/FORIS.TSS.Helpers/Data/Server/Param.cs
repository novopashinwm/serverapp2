using System.ComponentModel;
using System.Data;

namespace FORIS.TSS.Helpers.Data.Server
{
	public class ParamTable:
		Table<ParamRow, ParamTable, IServerData>
	{
		#region Properties

		public override string Name
		{
			get { return "PARAM"; }
		}

		#endregion // Properties

		public ParamTable()
		{
			
		}
		public ParamTable( IContainer container ): this()
		{
			container.Add( this );
		}

		protected override ParamRow OnCreateRow( DataRow dataRow )
		{
			return new ParamRow( dataRow );
		}
	}

	public class ParamRow:
		Row<ParamRow, ParamTable, IServerData>
	{
		#region Fields

		private const int PARAM_ID = 0;
		private const int NAME = 1;
		private const int VALUE = 2;

		public string Name
		{
			get { return (string)this.DataRow[ParamRow.NAME, DataRowVersion.Current]; }
		}

		public string Value
		{
			get { return (string)this.DataRow[ParamRow.VALUE, DataRowVersion.Current]; }
		}

		#endregion // Fields

		#region Id

		protected override int GetId()
		{
			return (int)this.DataRow[ParamRow.PARAM_ID];
		}

		#endregion // Id

		public ParamRow( DataRow dataRow ): base( dataRow )
		{
			
		}

		protected override void OnBuildFields()
		{
		
		}
	}
}
