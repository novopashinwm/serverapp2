﻿using FORIS.TSS.BusinessLogic.Data.Server;

namespace FORIS.TSS.Helpers.Data.Server
{
	public interface IServerData : IData<IServerDataTreater>, IServerDataSupplier
	{
		SessionTable Session { get; }
		ParamTable Param { get; }
	}
}