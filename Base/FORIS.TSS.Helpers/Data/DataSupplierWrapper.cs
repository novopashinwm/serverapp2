﻿using System;
using System.ComponentModel;
using System.Security.Principal;
using System.Threading;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.Common;

namespace FORIS.TSS.Helpers.Data
{
	public class DataSupplierWrapper<TDataSupplier, TDataTreater> :
		Component,
		IDataSupplier<TDataTreater>
		where TDataSupplier : IDataSupplier<TDataTreater>
		where TDataTreater : IDataTreater
	{
		#region Constructor & Dispose

		public DataSupplierWrapper(TDataSupplier dataSupplier)
		{
			#region Preconditions

			if (object.Equals(dataSupplier, default(TDataSupplier)))
			{
				throw new ArgumentException("Value cannot be null", "dataSupplier");
			}

			#endregion Preconditions

			this.dataSupplier = dataSupplier;
		}
		protected override void Dispose(bool disposing)
		{
			/* Этого делать не стоит т.к. клиент мог отключиться не закрыв сессию.
			 * Иначе получится, что упавший клиент роняет сервер, а это неправильно.
			 */
			//if( disposing )
			//{
			//    if ( this.handler != null )
			//    {
			//        throw new InvalidOperationException(
			//            "Consumer's connection exists yet.\r\n" +
			//            "Type: " + this.GetType().Name
			//            );
			//    }
			//}

			base.Dispose(disposing);
		}

		#endregion Constructor & Dispose

		private readonly TDataSupplier dataSupplier;
		[Browsable(false)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public TDataSupplier DataSupplier
		{
			get { return this.dataSupplier; }
		}

		#region IDataSupplier Members

		private AnonymousEventHandler<DataChangedEventArgs> handler;
		DataInfo IDataSupplier.add_Connection(AnonymousEventHandler<DataChangedEventArgs> handler)
		{
			#region Preconditions

			if (this.handler != null)
				throw new InvalidOperationException($@"Connection already exists Type: {GetType().Name}");

			#endregion // Preconditions

			this.handler += handler;

			IPrincipal p = Thread.CurrentPrincipal;

			return this.GetDataInfo();
		}
		void IDataSupplier.remove_Connection(AnonymousEventHandler<DataChangedEventArgs> handler)
		{
			if (this.handler == null)
				throw new InvalidOperationException($@"Connection does not exist yet Type: {GetType().Name}");

			this.dataSupplier.remove_Connection(this.dataSupplier_DataChanged);

			this.handler -= handler;
		}
		public void Tick()
		{
			this.dataSupplier.Tick();
		}
		public event AnonymousEventHandler<EventArgs> Changed
		{
			add    { this.dataSupplier.Changed += value; }
			remove { this.dataSupplier.Changed -= value; }
		}

		#endregion IDataSupplier Members

		/// <summary> Данные в измененном виде из оборачиваемого поставщика. Переопределите этот метод, чтоб отфильтровать данные </summary>
		/// <returns></returns>
		protected virtual DataInfo GetDataInfo()
		{
			return this.dataSupplier.add_Connection(this.dataSupplier_DataChanged);
		}
		protected virtual void dataSupplier_DataChanged(DataChangedEventArgs e)
		{
			#region Preconditions

			if (this.handler == null)
			{
				throw new InvalidOperationException();
			}

			#endregion Preconditions

			this.handler(e);
		}

		#region IDataSupplier<TDataTreater> Members

		public virtual TDataTreater GetDataTreater()
		{
			return this.dataSupplier.GetDataTreater();
		}

		#endregion IDataSupplier<TDataTreater> Members
	}
}