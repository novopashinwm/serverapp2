using System;
using System.Collections.Generic;
using System.Data;
using FORIS.TSS.Common;

namespace FORIS.TSS.Helpers.Data
{
	public class InternalRowCollection<TRow, TTable, TData>: 
		CollectionWithEvents<TRow>, 
		IRowCollection<TRow, TTable, TData>
		where TRow: Row<TRow, TTable, TData>
		where TTable: Table<TRow, TTable, TData>
		where TData: IData
	{
		#region IRowCollection Members

		IRowCollection IRowCollection.Clone()
		{
			return new InternalRowCollection<TRow, TTable, TData>( this.Items );
		}

		#endregion // IRowCollection Members

		#region IRowCollection<TRow, TTable, TData> Members

		TRow IRowCollection<TRow, TTable, TData>.this[ int ID ]
		{
			get { return this.hashByID[ ID ]; }
		}

		void IRowCollection<TRow, TTable, TData>.Add( TRow Row )
		{
			this.OnAddRow( new RowEventArgs<TRow, TTable, TData>( Row ) );
		}

		bool IRowCollection<TRow, TTable, TData>.Contains( int id )
		{
			return this.hashByID.ContainsKey( id );
		}

		#endregion // IRowCollection<TRow, TTable, TData> members

		#region Indexer

		public new TRow this[ int id ]
		{
			get { return this.hashByID[id]; }
		}

		public TRow this[ DataRow dataRow ]
		{
			get { return this.hashByDataRows[dataRow]; }
		}

		#endregion // Indexer

		#region Components

		private readonly Dictionary<int, TRow> hashByID;
		private readonly Dictionary<DataRow, TRow> hashByDataRows;

		#endregion // Component

		#region Constructor 

		public InternalRowCollection()
		{
			this.hashByID = new Dictionary<int, TRow>();
			this.hashByDataRows = new Dictionary<DataRow, TRow>();
		}
		public InternalRowCollection( int capacity )
		{
			this.hashByID = new Dictionary<int, TRow>( capacity );
			this.hashByDataRows = new Dictionary<DataRow, TRow>( capacity );
		}

		private InternalRowCollection( ICollection<TRow> rows )
			: this( rows.Count )
		{
			foreach( TRow row in rows )
			{
				this.Add( row );
			}
		}

		#endregion // Constructor

		#region Manipulation methods

		public bool ContainsID( int ID )
		{
			return this.hashByID.ContainsKey( ID );
		}
		public bool ContainsDataRow( DataRow dataRow )
		{
			return this.hashByDataRows.ContainsKey( dataRow );
		}
		
		#endregion // Manipulation methods

		#region Maintain hash

		/* ��������, ��� DataRow ������ ����������� 
		 * ����������� � ��������� �������.
		 * 
		 * ������ ���������� �������� ����� (� �����) � �������
		 * ����������� ����� ������ ����� ���������� �������
		 * � ��������� ������ ����� ������� �������.
		 * ���������� ��� �� ����� ��������� ������� Inserting 
		 * ���� ���������. ������������ ������� base.OnInserting()
		 * 
		 * �������� ����� (� �����) � ������� ��������� ���� �� ���� ���
		 * ��� ����� ������� �� ���������� ������ ����� ������� �������.
		 * ���������� ��� �� ����� ��������� ������� Removed
		 * ���� ���������. ������������ ������� base.OnRemoved()
		 */

		protected override void OnInserting( CollectionChangeEventArgs<TRow> e )
		{
			base.OnInserting( e );

			#region Preconditions

			if(
				this.Count != this.hashByID.Count ||
				this.Count != this.hashByDataRows.Count
				)
			{
				throw new ApplicationException( "Internal row collection integrity corruption" );
			}

			#endregion Preconditions

			TRow Row = e.Item;

			this.hashByID.Add( Row.ID, Row );
			this.hashByDataRows.Add( Row.DataRow, Row );
		}
		protected override void OnInserted(CollectionChangeEventArgs<TRow> e)
		{
			#region Postconditions

			if (
				this.Count != this.hashByID.Count ||
				this.Count != this.hashByDataRows.Count
				)
			{
				throw new ApplicationException("Internal row collection integrity corruption");
			}

			#endregion Postconditions

			base.OnInserted(e);
		}

		protected override void OnRemoving(CollectionChangeEventArgs<TRow> e)
		{
			base.OnRemoving(e);
			
			#region Preconditions

			if (
				this.Count != this.hashByID.Count ||
				this.Count != this.hashByDataRows.Count
				)
			{
				throw new ApplicationException("Internal row collection integrity corruption");
			}

			#endregion Preconditions
		}
		protected override void OnRemoved( CollectionChangeEventArgs<TRow> e )
		{
			TRow Row = e.Item;

			this.hashByID.Remove( Row.ID );
			this.hashByDataRows.Remove( Row.DataRow );

			#region Postconditions

			if(
				this.Count != this.hashByID.Count ||
				this.Count != this.hashByDataRows.Count
				)
			{
				throw new ApplicationException( "Internal row collection integrity corruption" );
			}

			#endregion Postconditions

			base.OnRemoved( e );
		}

		protected override void OnClearing(CollectionEventArgs<TRow> e)
		{
			base.OnClearing( e );

			#region Preconditions

			if(
				this.Count != this.hashByID.Count ||
				this.Count != this.hashByDataRows.Count
				)
			{
				throw new ApplicationException( "Internal row collection integrity corruption" );
			}

			#endregion Preconditions
		}

		protected override void OnCleared( CollectionEventArgs<TRow> e )
		{
			this.hashByID.Clear();
			this.hashByDataRows.Clear();

			#region Postconditions

			if(
				this.Count != this.hashByID.Count ||
				this.Count != this.hashByDataRows.Count
				)
			{
				throw new ApplicationException( "Internal row collection integrity corruption" );
			}

			#endregion Postconditions

			base.OnCleared( e );
		}

		#endregion // Maintain hash

		#region Events

		private RowEventHandler<TRow, TTable, TData> dlgtAddRow;

		public event RowEventHandler<TRow, TTable, TData> AddRow
		{
			add { this.dlgtAddRow += value; }
			remove { this.dlgtAddRow -= value; }
		}


		protected virtual void OnAddRow( RowEventArgs<TRow, TTable, TData> e )
		{
			if( this.dlgtAddRow != null )
			{
				this.dlgtAddRow( this, e );
			}
		}


		#endregion // Events
	}
}