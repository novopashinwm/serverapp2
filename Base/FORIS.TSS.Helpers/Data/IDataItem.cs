namespace FORIS.TSS.Helpers.Data
{
	//public interface IDataItem<TData, TParams>
	//    where TData: DataBase<TParams>
	//    where TParams: DataParams
	//{
	//    TData Data { get; set; }
	//}

	//public interface IDataItem
	//{
	//    Data Data { get; set; }
	//}

	public interface IDataItem<TData>
		where TData: IData
	{
		TData Data { get; set; }
	}
}
