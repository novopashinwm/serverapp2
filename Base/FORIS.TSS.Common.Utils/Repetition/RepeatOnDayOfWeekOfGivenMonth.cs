﻿using System;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary> Summary description for RepeatOnDayOfWeekOfGivenMonth </summary>
	[Serializable]
	public class RepeatOnDayOfWeekOfGivenMonth : IRepetitionClass
	{
		protected int n;
		/// <summary> Parameters for repeating </summary>
		public int N
		{
			get
			{
				return n;
			}
			set
			{
				n = value;
			}
		}
		protected int dayOfWeek;
		/// <summary> Parameters for repeating </summary>
		public int DayOfWeek
		{
			get
			{
				return dayOfWeek;
			}
			set
			{
				dayOfWeek = value;
			}
		}
		protected int month;
		/// <summary> Parameters for repeating </summary>
		public int Month
		{
			get
			{
				return month;
			}
			set
			{
				month = value;
			}
		}

		protected string comment;
		/// <summary> Parameters for Comment </summary>
		public string Comment
		{
			get
			{
				return comment;
			}
			set
			{
				comment = value;
			}
		}


		public DateTime? GetNextTimeInUTC(DateTime taskStart)
		{
			DateTime nextTime = taskStart;
			// Adjust month
			nextTime = ScheduleHelper.MoveForwardToBeginOfMonth(nextTime, this.Month);
			// Adjust day of week
			nextTime = ScheduleHelper.MoveToStartOfMonth(nextTime);
			for (int i = 0; i < this.N; ++i)
			{
				nextTime = ScheduleHelper.MoveForwardToDayOfWeek(nextTime, this.DayOfWeek);
			}
			return nextTime;
		}

		#region IRepetitionClass Members

		public string GetComment()
		{
			return this.comment;
		}

		#endregion

		public override string ToString()
		{
			return base.ToString();
		}

	}
}