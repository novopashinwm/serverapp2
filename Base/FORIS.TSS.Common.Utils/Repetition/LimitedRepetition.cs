﻿using System;
using System.Xml;
using System.Xml.Serialization;
using Jayrock.Json.Conversion;

namespace FORIS.TSS.BusinessLogic.Mail
{
	/// <summary> Базовый класс для расписаний, имеющих начало и, возможно, конец действия расписания </summary>
	[Serializable]
	public abstract class LimitedRepetition : IRepetitionClass
	{
		// [JsonIgnore]
		public DateTime Start;

		/// <summary>Дата окончания действия расписания</summary>
		/// <remarks>Не может быть <see cref="Nullable&lt;DateTime&gt;"/>, 
		/// поскольку класс сериализуется с помощью <see cref="System.Runtime.Serialization.Formatters.Soap.SoapFormatter"/>,
		/// который не поддерживает generic'и.</remarks>
		// [JsonIgnore]
		public DateTime End;

		[NonSerialized]
		private string _start;
		[XmlIgnore]
		public string StartDate
		{
			get
			{
				_start = Start != DateTime.MinValue ? XmlConvert.ToString(Start, XmlDateTimeSerializationMode.RoundtripKind) : null;
				return _start;
			}
			set
			{
				DateTime datetime;
				Start = DateTime.TryParse(value, out datetime) ? datetime : DateTime.MinValue;
				_start = string.Empty;
			}
		}

		/// <summary> Дата окончания действия расписания </summary>
		/// <remarks> Не может быть <see cref="Nullable&lt;DateTime&gt;"/>, 
		/// поскольку класс сериализуется с помощью <see cref="System.Runtime.Serialization.Formatters.Soap.SoapFormatter"/>,
		/// который не поддерживает generic'и.</remarks>
		[NonSerialized]
		private string _end;
		[XmlIgnore]
		public string EndDate
		{
			get
			{
				_end = End != DateTime.MaxValue ? XmlConvert.ToString(End, XmlDateTimeSerializationMode.RoundtripKind) : null;
				return _end;
			}
			set
			{
				DateTime datetime;
				End = DateTime.TryParse(value, out datetime) ? datetime : DateTime.MaxValue;
				_end = string.Empty;
			}
		}

		[JsonIgnore]
		public TimeSpan TimeZoneOffset;

		public string Comment;

		protected LimitedRepetition()
		{
			var now = DateTime.Now;
			var nowUtc = now.ToUniversalTime();
			Start = now;
			TimeZoneOffset = now - nowUtc;
		}

		public DateTime? GetNextTimeInUTC(DateTime taskStart)
		{
			var taskStartCurrent = taskStart.Add(TimeZoneOffset);
			var nextDateTime = GetNextDateTime(taskStartCurrent < Start ? Start : taskStartCurrent);
			if (GetEffectiveEnd() < nextDateTime)
				return null;
			if (nextDateTime == null)
				return null;
			return nextDateTime.Value.Add(-TimeZoneOffset);
		}

		public DateTime GetEffectiveEnd()
		{
			return End == DateTime.MaxValue ? End : End.Add(new TimeSpan(23, 59, 59));
		}

		protected abstract DateTime? GetNextDateTime(DateTime taskStart);

		public string GetComment()
		{
			return Comment;
		}

		[NonSerialized]
		private string _type;
		[XmlIgnore]
		public string Type
		{
			get
			{
				_type = GetType().Name;
				return _type;
			}
			set
			{
				//нужно для возможности десериализации этого свойства
			}
		}
	}
}