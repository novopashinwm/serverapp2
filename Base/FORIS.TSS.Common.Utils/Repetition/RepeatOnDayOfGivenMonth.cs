﻿using System;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary> Summary description for RepeatOnDayOfGivenMonth </summary>
	[Serializable]
	public class RepeatOnDayOfGivenMonth : IRepetitionClass
	{
		protected int n;
		/// <summary> Parameters for repeating </summary>
		public int N
		{
			get
			{
				return n;
			}
			set
			{
				n = value;
			}
		}
		protected int month;
		/// <summary> Parameters for repeating </summary>
		public int Month
		{
			get
			{
				return month;
			}
			set
			{
				month = value;
			}
		}

		protected string comment;
		/// <summary> Parameters for Comment </summary>
		public string Comment
		{
			get
			{
				return comment;
			}
			set
			{
				comment = value;
			}
		}

		public DateTime? GetNextTimeInUTC(DateTime taskStart)
		{
			// adjust month and day
			var nextTime = new DateTime(taskStart.Year, this.Month, this.N, taskStart.Hour, taskStart.Minute, taskStart.Second, taskStart.Millisecond);
			if (nextTime <= taskStart)
				nextTime = nextTime.AddYears(1);
			return nextTime;
		}

		#region IRepetitionClass Members

		public string GetComment()
		{
			return this.comment;
		}

		#endregion

		public override string ToString()
		{
			return base.ToString();
		}
	}
}