﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Jayrock.Json.Conversion;

namespace FORIS.TSS.BusinessLogic.Mail
{
	public class WeekCustomScheduler
	{
		public bool Sunday;
		public bool Monday;
		public bool Tuesday;
		public bool Wednesday;
		public bool Thursday;
		public bool Friday;
		public bool Saturday;
	}

	///<summary> Привязанное к неделе расписание, выполняющееся в заданным дням недели в указанное время </summary>
	[Serializable]
	public class RepeatEveryWeekGivenTimes : RepeatWeekly
	{
		private readonly bool[] _isWeekDayEnabled = new bool[7];

		///<summary> Позволяет устанавливать или получать информацию о том, выполняется ли расписание в указанный день недели </summary>
		[JsonIgnore]
		public bool this[DayOfWeek dayOfWeek]
		{
			get
			{
				return _isWeekDayEnabled[(int) dayOfWeek];
			}
			set
			{
				_isWeekDayEnabled[(int)dayOfWeek] = value;
			}
		}

		[XmlIgnore]
		public WeekCustomScheduler IsWeekDayEnabled
		{
			get
			{
				var value       = new WeekCustomScheduler();
				value.Sunday    = this[DayOfWeek.Sunday];
				value.Monday    = this[DayOfWeek.Monday];
				value.Tuesday   = this[DayOfWeek.Tuesday];
				value.Wednesday = this[DayOfWeek.Wednesday];
				value.Thursday  = this[DayOfWeek.Thursday];
				value.Friday    = this[DayOfWeek.Friday];
				value.Saturday  = this[DayOfWeek.Saturday];
				return value;
			}
			set
			{
				this[DayOfWeek.Sunday]    = value.Sunday;
				this[DayOfWeek.Monday]    = value.Monday;
				this[DayOfWeek.Tuesday]   = value.Tuesday;
				this[DayOfWeek.Wednesday] = value.Wednesday;
				this[DayOfWeek.Thursday]  = value.Thursday;
				this[DayOfWeek.Friday]    = value.Friday;
				this[DayOfWeek.Saturday]  = value.Saturday;
			}
		} 

		///<summary> Устанавливает или возвращает время дня, когда выполняется расписание </summary>
		private TimeSpan[] _times;
		public IEnumerable<TimeSpan> Times
		{
			get
			{
				return _times;
			}
			set
			{
				_times = Intern(value);
			}
		}

		///<summary> Устанавливает или возвращает время дня, когда выполняется расписание </summary>
		public string[] Time
		{
			get
			{
				return _times.Select(t => t.ToString("g")).ToArray();
			}
			set
			{
				_times = Intern(value.Select(v =>
				{
					TimeSpan time;
					return TimeSpan.TryParse(v, out time) ? time : (TimeSpan?) null;
				})
				.Where(v => v != null)
				.Cast<TimeSpan>()
				.ToArray());
			}
		}

		protected override TimeSpan[] GetScheduleByWeekDay(DayOfWeek weekDay)
		{
			return _isWeekDayEnabled[(int)weekDay] ? _times : null;
		}
	}
}