﻿using System;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary> Summary description for RepeatOnDayOfWeekOfMonth </summary>
	[Serializable]
	public class RepeatOnDayOfWeekOfMonth : IRepetitionClass
	{
		protected int n;
		/// <summary> Parameters for repeating </summary>
		public int N
		{
			get
			{
				return n;
			}
			set
			{
				n = value;
			}
		}
		protected int dayOfWeek;
		/// <summary> Parameters for repeating </summary>
		public int DayOfWeek
		{
			get
			{
				return dayOfWeek;
			}
			set
			{
				dayOfWeek = value;
			}
		}
		protected int month;
		/// <summary> Parameters for repeating </summary>
		public int Month
		{
			get
			{
				return month;
			}
			set
			{
				month = value;
			}
		}

		protected string comment;
		/// <summary> Parameters for Comment </summary>
		public string Comment
		{
			get
			{
				return comment;
			}
			set
			{
				comment = value;
			}
		}

		public DateTime? GetNextTimeInUTC(DateTime taskStart)
		{
			DateTime nextTime = taskStart;
			// Get start of the month
			nextTime = ScheduleHelper.MoveToStartOfMonth(nextTime);
			// Get next month
			nextTime = nextTime.AddMonths(this.Month);
			// Get required day
			nextTime = ScheduleHelper.MoveForwardToDayOfWeek(taskStart, this.dayOfWeek);
			// Get required repetition of the day
			nextTime = nextTime.AddDays((n - 1) * 7);
			return nextTime;
		}

		#region IRepetitionClass Members

		public string GetComment()
		{
			return this.comment;
		}

		#endregion

		public override string ToString()
		{
			return base.ToString();
		}

	}
}