﻿using System;
using System.Text;

namespace FORIS.TSS.BusinessLogic.Mail
{
	/// <summary> Summary description for RepeatEveryWeek </summary>
	[Serializable]
	public class RepeatEveryWeek : IRepetitionClass
	{
		protected int n;
		/// <summary> Parameters for repeating </summary>
		public int N
		{
			get
			{
				return n;
			}
			set
			{
				n = value;
			}
		}

		public bool Monday    = false;
		public bool Tuesday   = false;
		public bool Wednesday = false;
		public bool Thursday  = false;
		public bool Friday    = false;
		public bool Saturday  = false;
		public bool Sunday    = false;

		protected string comment;
		/// <summary> Parameters for Comment </summary>
		public string Comment
		{
			get
			{
				return comment;
			}
			set
			{
				comment = value;
			}
		}

		public DateTime? GetNextTimeInUTC(DateTime taskStart)
		{
			DateTime nextTime = taskStart;
			for (int i = 0; i < 7; ++i)
			{
				nextTime = nextTime + new TimeSpan(1, 0, 0, 0, 0);
				if (nextTime.DayOfWeek == DayOfWeek.Monday && this.Monday)
				{
					return nextTime;
				}

				// AS: - Здесь была ошибка - Thursday, а это четверг, а не вторник.
				if (nextTime.DayOfWeek == DayOfWeek.Tuesday && this.Tuesday)
				{
					return nextTime;
				}
				if (nextTime.DayOfWeek == DayOfWeek.Wednesday && this.Wednesday)
				{
					return nextTime;
				}
				if (nextTime.DayOfWeek == DayOfWeek.Thursday && this.Thursday)
				{
					return nextTime;
				}
				if (nextTime.DayOfWeek == DayOfWeek.Friday && this.Friday)
				{
					return nextTime;
				}
				if (nextTime.DayOfWeek == DayOfWeek.Saturday && this.Saturday)
				{
					return nextTime;
				}
				if (nextTime.DayOfWeek == DayOfWeek.Sunday && this.Sunday)
				{
					return nextTime;
				}
			}
			return taskStart;
		}

		#region IRepetitionClass Members

		public string GetComment()
		{
			return this.comment;
		}

		#endregion

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("Еженедельно в:");

			if (Monday)
				sb.Append(" Понедельник");
			if (Tuesday)
				sb.Append(" Вторник");
			if (Wednesday)
				sb.Append(" Среду");
			if (Thursday)
				sb.Append(" Четверг");
			if (Friday)
				sb.Append(" Пятницу");
			if (Saturday)
				sb.Append(" Субботу");
			if (Sunday)
				sb.Append(" Воскресенье");

			return sb.ToString();
		}
	}
}