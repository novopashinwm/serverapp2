﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Serialization;
using Jayrock.Json.Conversion;

namespace FORIS.TSS.BusinessLogic.Mail
{
	public class WeekCustomDayScheduler
	{
		public string[] Sunday;
		public string[] Monday;
		public string[] Tuesday;
		public string[] Wednesday;
		public string[] Thursday;
		public string[] Friday;
		public string[] Saturday;
	}
	/// <summary> Для каждого дня недели задается свое расписание </summary>
	[Serializable]
	public class RepeatCustomWeekDayCustomTime : RepeatWeekly
	{
		private readonly TimeSpan[][] _schedule = new TimeSpan[7][];

		/// <summary> Расписание: для каждого дня недели - перечисление времен дня </summary>
		[JsonIgnore]
		public IEnumerable<TimeSpan> this[DayOfWeek dayOfWeek]
		{ 
			get
			{
				return _schedule[(int) dayOfWeek] ?? Enumerable.Empty<TimeSpan>();
			}
			set
			{

				_schedule[(int) dayOfWeek] = Intern(value);
			}
		}

		[XmlIgnore]
		public WeekCustomDayScheduler Scheduler
		{
			get
			{
				var value = new WeekCustomDayScheduler
				{
					Sunday    = this[DayOfWeek.Sunday].Select(v => v.ToString("g")).ToArray(),
					Monday    = this[DayOfWeek.Monday].Select(v => v.ToString("g")).ToArray(),
					Tuesday   = this[DayOfWeek.Tuesday].Select(v => v.ToString("g")).ToArray(),
					Wednesday = this[DayOfWeek.Wednesday].Select(v => v.ToString("g")).ToArray(),
					Thursday  = this[DayOfWeek.Thursday].Select(v => v.ToString("g")).ToArray(),
					Friday    = this[DayOfWeek.Friday].Select(v => v.ToString("g")).ToArray(),
					Saturday  = this[DayOfWeek.Saturday].Select(v => v.ToString("g")).ToArray()
				};
				return value;
			}
			set
			{
				this[DayOfWeek.Sunday] = value.Sunday.Select(v =>
				{
					TimeSpan time;
					return TimeSpan.TryParse(v, CultureInfo.InvariantCulture, out time) ? time : (TimeSpan?)null;
				}).Where(t => t != null).Cast<TimeSpan>();
				this[DayOfWeek.Monday] = value.Monday.Select(v => 
				{
					TimeSpan time;
					return TimeSpan.TryParse(v, CultureInfo.InvariantCulture, out time) ? time : (TimeSpan?)null;
				}).Where(t => t != null).Cast<TimeSpan>();
				this[DayOfWeek.Tuesday] = value.Tuesday.Select(v => 
				{
					TimeSpan time;
					return TimeSpan.TryParse(v, CultureInfo.InvariantCulture, out time) ? time : (TimeSpan?)null;
				}).Where(t => t != null).Cast<TimeSpan>();
				this[DayOfWeek.Wednesday] = value.Wednesday.Select(v => 
				{
					TimeSpan time;
					return TimeSpan.TryParse(v, CultureInfo.InvariantCulture, out time) ? time : (TimeSpan?)null;
				}).Where(t => t != null).Cast<TimeSpan>();
				this[DayOfWeek.Thursday] = value.Thursday.Select(v => 
				{
					TimeSpan time;
					return TimeSpan.TryParse(v, CultureInfo.InvariantCulture, out time) ? time : (TimeSpan?)null;
				}).Where(t => t != null).Cast<TimeSpan>();
				this[DayOfWeek.Friday] = value.Friday.Select(v => 
				{
					TimeSpan time;
					return TimeSpan.TryParse(v, CultureInfo.InvariantCulture, out time) ? time : (TimeSpan?)null;
				}).Where(t => t != null).Cast<TimeSpan>();
				this[DayOfWeek.Saturday] = value.Saturday.Select(v => 
				{
					TimeSpan time;
					return TimeSpan.TryParse(v, CultureInfo.InvariantCulture, out time) ? time : (TimeSpan?)null;
				}).Where(t => t != null).Cast<TimeSpan>();
			}
		}

		/// <summary> Удалить как веб-перестанет использовать </summary>
		[NonSerialized]
		private Dictionary<DayOfWeek, IEnumerable<TimeSpan>> _scheduleDictionary;
		[Obsolete]
		[XmlIgnore]
		public Dictionary<DayOfWeek, IEnumerable<TimeSpan>> Schedule
		{
			get
			{
				_scheduleDictionary = Enum.GetValues(typeof (DayOfWeek))
					.Cast<DayOfWeek>()
					.ToDictionary(
						key => key,
						value => this[value]);
				return _scheduleDictionary;
			}
		} 

		protected override TimeSpan[] GetScheduleByWeekDay(DayOfWeek weekDay)
		{
			return _schedule[(int)weekDay];
		}
	}
}