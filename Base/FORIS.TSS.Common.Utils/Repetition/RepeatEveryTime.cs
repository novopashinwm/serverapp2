﻿using System;
using System.Globalization;
using System.Xml.Serialization;
using Jayrock.Json.Conversion;

namespace FORIS.TSS.BusinessLogic.Mail
{
	/// <summary> repeat every specified interval </summary>
	[Serializable]
	public class RepeatEveryTime : LimitedRepetition
	{
		#region IRepetitionClass Members

		protected override DateTime? GetNextDateTime(DateTime taskStart)
		{
			if (Interval <= TimeSpan.Zero)
				throw new InvalidOperationException("Invalid tsInterval: " + Interval);

			DateTime next;
			if (taskStart < Start + StartTime)
				next = Start + StartTime;
			else
				next = taskStart.Add(Interval);

			if (StartTime < EndTime)
			{
				if (next.TimeOfDay < StartTime)
					next = next.Date.Add(StartTime);

				if (EndTime < next.TimeOfDay)
					next = next.Date.AddDays(1).Add(StartTime);
			}
			else
			{
				var time = next.TimeOfDay;
				if (EndTime < time && time < StartTime)
					next = next.Date.Add(StartTime);
			}

			return next;
		}

		#endregion

		// Для совместимости с сериализованными данными невозможно переименовать
// ReSharper disable InconsistentNaming
		protected TimeSpan tsInterval;
// ReSharper restore InconsistentNaming
		[JsonIgnore]
		protected TimeSpan Interval
		{
			get
			{
				if (tsInterval != TimeSpan.Zero) 
					return tsInterval;

				UpdateInterval();
				return tsInterval;
			}
		}

		/// <summary> get repetition interval </summary>
		/// <returns> TimeSpan interval </returns>
		public TimeSpan GetInterval()
		{
			return Interval;
		}

		public void SetInterval(TimeSpan intv)
		{
			tsInterval = intv;
		}

		[NonSerialized]
		private TimeUnit _intervalUnit;
		[NonSerialized]
		private int _intervalValue;

		[XmlIgnore]
		public TimeUnit IntervalUnit
		{
			get
			{
				if(tsInterval.Days > 0)
					_intervalUnit = TimeUnit.Days;
				if(tsInterval.Hours > 0)
					_intervalUnit = TimeUnit.Hours;
				if(tsInterval.Minutes > 0)
					_intervalUnit = TimeUnit.Minutes;
				if(tsInterval.Seconds > 0)
					_intervalUnit = TimeUnit.Seconds;
				return _intervalUnit;
			}
			set
			{
				_intervalUnit = value;
				UpdateInterval();
			}
		}

		[XmlIgnore]
		public int IntervalValue
		{
			get
			{
				switch (IntervalUnit)
				{
					case TimeUnit.Days:
						_intervalValue = tsInterval.Days;
						break;
					case TimeUnit.Hours:
						_intervalValue = tsInterval.Hours;
						break;
					case TimeUnit.Minutes:
						_intervalValue = tsInterval.Minutes;
						break;
					case TimeUnit.Seconds:
						_intervalValue = tsInterval.Seconds;
						break;
				}

				return _intervalValue;
			}
			set
			{
				_intervalValue = value;
				UpdateInterval();
			}
		}

		/// <summary> Время "с" </summary>
		//TODO: Не выводить в JSON
//        [JsonIgnore]
		public TimeSpan StartTime;

		/// <summary> Время "по" </summary>
		//TODO: Не выводить в JSON
//        [JsonIgnore]
		public TimeSpan EndTime;

		[XmlIgnore]
		public string FromTime
		{
			get { return StartTime.ToString("g"); }
			set
			{
				TimeSpan time;
				StartTime = TimeSpan.TryParse(value, CultureInfo.InvariantCulture, out time) ? time : TimeSpan.MinValue;
			}
		}

		[XmlIgnore]
		public string ToTime
		{
			get { return EndTime.ToString("g"); }
			set
			{
				TimeSpan time;
				EndTime = TimeSpan.TryParse(value, CultureInfo.InvariantCulture, out time) ? time : TimeSpan.MaxValue;
			}
		}

		private void UpdateInterval()
		{
			switch (IntervalUnit)
			{
				case TimeUnit.Days:
					tsInterval = new TimeSpan(_intervalValue, 0, 0, 0);
					break;
				case TimeUnit.Hours:
					tsInterval = new TimeSpan(0, _intervalValue, 0, 0);
					break;
				case TimeUnit.Minutes:
					tsInterval = new TimeSpan(0, 0, _intervalValue, 0);
					break;
				case TimeUnit.Seconds:
					tsInterval = new TimeSpan(0, 0, 0, _intervalValue);
					break;
			}
		}
	}

	public enum TimeUnit
	{
		Hours,
		Minutes,
		Seconds,
		Days
	}
}