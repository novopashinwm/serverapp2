﻿using System;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary> Repeats task every working day </summary>
	[Serializable]
	public class RepeatOnWorkdays : IRepetitionClass
	{
		protected string comment;
		/// <summary> Parameters for Comment </summary>
		public string Comment
		{
			get
			{
				return comment;
			}
			set
			{
				comment = value;
			}
		}


		#region IRepetitionClass Members
		
		public DateTime? GetNextTimeInUTC(DateTime taskStart)
		{
			DateTime nextTime = taskStart;
			for (int i = 0; i < 7; ++i)
			{
				nextTime = nextTime + new TimeSpan(1, 0, 0, 0, 0);
				if (nextTime.DayOfWeek != DayOfWeek.Saturday && nextTime.DayOfWeek != DayOfWeek.Saturday)
				{
					return nextTime;
				}
			}
			return taskStart;
		}

		public string GetComment()
		{
			return comment;
		}

		#endregion

		public override string ToString()
		{
			return "Каждый рабочий день"; 
		}

	}
}