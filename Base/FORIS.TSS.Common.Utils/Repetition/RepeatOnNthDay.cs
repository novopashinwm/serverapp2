﻿using System;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary> Repeats task each N-th day </summary>
	[Serializable]
	public class RepeatOnNthDay : IRepetitionClass
	{
		protected int n;
		/// <summary> Parameters for repeating </summary>
		public int N
		{
			get
			{
				return n;
			}
			set
			{
				n = value;
			}
		}

		protected string comment;
		/// <summary> Parameters for Comment </summary>
		public string Comment
		{
			get
			{
				return comment;
			}
			set
			{
				comment = value;
			}
		}

		#region IRepetitionClass Members

		public DateTime? GetNextTimeInUTC(DateTime taskStart)
		{
			return taskStart + new TimeSpan(n, 0, 0, 0, 0);
		}

		public string GetComment()
		{
			return comment;
		}

		#endregion

		public override string ToString()
		{
			switch(n)
			{
				case 1:
					return string.Format("Ежедневно");
				default:
					return string.Format("Каждый {0}-й день", n);
			}
		}
	}
}