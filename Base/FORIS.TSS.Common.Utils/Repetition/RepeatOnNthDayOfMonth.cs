﻿using System;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary> Summary description for RepeatOnNthDayOfMonth </summary>
	[Serializable]
	public class RepeatOnNthDayOfMonth : IRepetitionClass
	{
		protected int n;
		/// <summary> Parameters for repeating </summary>
		public int N
		{
			get
			{
				return n;
			}
			set
			{
				n = value;
			}
		}
		protected int month;
		/// <summary> Parameters for repeating </summary>
		public int Month
		{
			get
			{
				return month;
			}
			set
			{
				month = value;
			}
		}

		protected string comment;

		public RepeatOnNthDayOfMonth(DateTime dateTime)
		{
			N = dateTime.Day;
		}

		public RepeatOnNthDayOfMonth()
		{
		}

		/// <summary> Parameters for Comment </summary>
		public string Comment
		{
			get
			{
				return comment;
			}
			set
			{
				comment = value;
			}
		}

		#region IRepetitionClass Members
		
		public DateTime? GetNextTimeInUTC(DateTime taskStart)
		{
			var nextTime = taskStart.AddMonths(1);

			if (taskStart.Day == N)
				return nextTime;

			var nextMonthBegin = new DateTime(nextTime.Year, nextTime.Month, 1);
			var nthDayOfNextMonth = nextMonthBegin.AddDays(N - 1);
			if (nextMonthBegin.AddMonths(1) <= nthDayOfNextMonth)
			{
				var nextMonthEnd = nextMonthBegin.AddMonths(1).AddDays(-1);
				return nextMonthEnd;
			}

			return nthDayOfNextMonth;
		}

		public string GetComment()
		{
			return this.comment;
		}

		#endregion

		public override string ToString()
		{
			return string.Format("Каждого {0}-го числа каждого месяца", n);
		}
	}
}