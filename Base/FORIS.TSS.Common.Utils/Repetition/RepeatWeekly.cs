﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FORIS.TSS.BusinessLogic.Mail
{
	/// <summary> Абстрактный класс расписаний, привязанных к дням недели </summary>
	[Serializable]
	public abstract class RepeatWeekly : LimitedRepetition
	{
		private static readonly TimeSpan MinTime = TimeSpan.FromSeconds(0);
		private static readonly TimeSpan MaxTime = new TimeSpan(23, 59, 59);
		private static readonly TimeSpan MinDiff = TimeSpan.FromSeconds(1);

		protected TimeSpan[] Intern(IEnumerable<TimeSpan> times)
		{
			if (times == null)
				return null;
			
			foreach (var time in times)
			{
				if (time < MinTime || MaxTime <= time)
					throw new ArgumentException("Value elements should be between 00:00:00 and 23:59:59");
			}

			var result = times.ToArray();
			Array.Sort(result);

			if (result.Length > 1)
			{
				for (var i = 1; i != result.Length; ++i)
				{
					if ((result[i] - result[i-1]) < MinDiff)
						throw new ArgumentException("Value elements should differ at least " + MinDiff);
				}
			}

			return result;
		}


		protected override DateTime? GetNextDateTime(DateTime taskStart)
		{
			var startWeekDay = taskStart.DayOfWeek;
			var currentWeekDay = startWeekDay;
			var taskStartDate = taskStart.Date;
			var dayTime = (taskStart - taskStartDate).Add(TimeSpan.FromSeconds(1));

			var times = GetScheduleByWeekDay(currentWeekDay);

			if (times != null)
			{
				//TODO: use Array.BinarySearch to improve performance
				foreach (var time in times)
				{
					if (dayTime < time)
						return taskStartDate.Add(time);
				}
			}

			var day = 0;
			do
			{
				currentWeekDay = (DayOfWeek)(((int)currentWeekDay + 1) % 7);
				++day;
				times = GetScheduleByWeekDay(currentWeekDay);
				if (times != null && times.Length != 0)
					return taskStartDate.AddDays(day).Add(times[0]);
			} while (day != 7);

			return null;
		}

		protected abstract TimeSpan[] GetScheduleByWeekDay(DayOfWeek weekDay);
	}
}