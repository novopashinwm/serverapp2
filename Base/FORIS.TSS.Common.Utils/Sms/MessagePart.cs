﻿namespace FORIS.TSS.Common.Sms
{
	/// <summary> Класс-обертка вокруг строки - части sms сообщения. </summary>
	/// <remarks> Класс позволяет устранить на этапе компиляции ошибки передачи в метод SendSms объектов, которые не являются строками или asid'ами. </remarks>
	public class MessagePart
	{
		public string Value;

		public static implicit operator MessagePart(string value)
		{
			return new MessagePart { Value = value };
		}
	}
}