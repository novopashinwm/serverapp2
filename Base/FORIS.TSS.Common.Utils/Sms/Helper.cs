﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using FORIS.TSS.Common.Helpers;

namespace FORIS.TSS.Common.Sms
{
	public class Helper
	{
		static Helper()
		{
			int value;
			MaxSmsPartSizeUtf = int.TryParse(ConfigurationManager.AppSettings["MaxSmsPartSize"], out value)
				? value
				: 63;
			MaxSmsPartSize = int.TryParse(ConfigurationManager.AppSettings["MaxSmsPartSize"], out value)
				? value
				: 158;
			MaxSmsPartCount = int.TryParse(ConfigurationManager.AppSettings["MaxSmsPartCount"], out value)
				? value
				: 3;
		}

		private static readonly int MaxSmsPartSize;
		private static readonly int MaxSmsPartSizeUtf;
		private static readonly int MaxSmsPartCount;

		public static List<string> AlignMessageParts(MessagePart[] messageParts)
		{
			const int msidLength = 11; //7123 456 7890
			const int asidLength = 2 + 32; //##00e05059ed9c581f4407754dc2ce5c81

			var maxLength = messageParts.Any(p => p.Value.Any(c => 127 < c)) ? MaxSmsPartSizeUtf : MaxSmsPartSize;
			var stringBuilder = new StringBuilder(maxLength + asidLength - msidLength);
			var alignedMessageParts = new List<string>();
			var customerSmsLength = 0;
			foreach (var messagePart in messageParts)
			{
				//TODO: при добавлении новых типов частей SMS переписать метод с использованием шаблона "Строитель"

				var @string = messagePart.Value;

				if (String.IsNullOrEmpty(@string))
					continue;

				foreach (var c in @string)
				{
					//  место в sms закончилось, открываем новую часть SMS
					if (customerSmsLength + MpxHelper.FeedString.Length == maxLength)
					{
						alignedMessageParts.Add(stringBuilder.ToString());
						stringBuilder.Length = 0;
						customerSmsLength = 0;
					}
					stringBuilder.Append(c);
					++customerSmsLength;
				}
			}

			if (stringBuilder.Length != 0)
				alignedMessageParts.Add(stringBuilder.ToString());

			//Если последнюю часть можно целиком поместить в свободное место, предназначенное для feed-символа, то так и делаем
			if (1 < alignedMessageParts.Count && alignedMessageParts.Last().Length <= MpxHelper.FeedString.Length)
			{
				var lastIndex = alignedMessageParts.Count - 1;
				var preLastIndex = alignedMessageParts.Count - 2;
				alignedMessageParts[preLastIndex] = alignedMessageParts[preLastIndex] + alignedMessageParts[lastIndex];
				alignedMessageParts.RemoveAt(lastIndex);
			}

			return alignedMessageParts;
		}

		public static List<List<string>> BreakToGroups(IEnumerable<string> list)
		{
			var groupSize = MaxSmsPartCount;
			var result = new List<List<String>>();
			List<String> resultItem = null;
			foreach (var item in list)
			{
				if (resultItem == null)
				{
					resultItem = new List<String>(groupSize);
					result.Add(resultItem);
				}
				resultItem.Add(item);

				if (resultItem.Count == groupSize)
					resultItem = null;
			}
			return result;
		}
	}
}