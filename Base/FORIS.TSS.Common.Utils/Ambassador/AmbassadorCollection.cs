﻿using System.Collections.Generic;
using System.Diagnostics;

namespace FORIS.TSS.Common
{
	public class AmbassadorCollection<T> : CollectionWithEvents<Ambassador<T>>
	{
		private readonly Dictionary<T, Ambassador<T>> hash =
			new Dictionary<T, Ambassador<T>>();
		public void Add(T Item)
		{
			base.Add(new Ambassador<T>(Item));
		}
		public void Insert(int index, T Item)
		{
			base.Insert(index, new Ambassador<T>(Item));
		}
		public void Remove(T Item)
		{
			Trace.Assert(hash.ContainsKey(Item));
			base.Remove(hash[Item]);
		}
		protected override void OnInserting(CollectionChangeEventArgs<Ambassador<T>> e)
		{
			Ambassador<T> Ambassador = e.Item;
			if (!object.Equals(Ambassador.Item, default(T)))
				hash.Add(Ambassador.Item, Ambassador);
			base.OnInserting(e);
		}
		protected override void OnInserted(CollectionChangeEventArgs<Ambassador<T>> e)
		{
			Ambassador<T> Ambassador = e.Item;
			Ambassador.BeforeSetItem += new AmbassadorEventHandler<T>(Ambassador_BeforeSetItem);
			Ambassador.AfterSetItem += new AmbassadorEventHandler<T>(Ambassador_AfterSetItem);
			base.OnInserted(e);
		}
		protected override void OnRemoving(CollectionChangeEventArgs<Ambassador<T>> e)
		{
			base.OnRemoving(e);
			Ambassador<T> Ambassador = e.Item;
			Ambassador.BeforeSetItem -= new AmbassadorEventHandler<T>(Ambassador_BeforeSetItem);
			Ambassador.AfterSetItem -= new AmbassadorEventHandler<T>(Ambassador_AfterSetItem);
		}
		protected override void OnRemoved(CollectionChangeEventArgs<Ambassador<T>> e)
		{
			base.OnRemoved(e);
			Ambassador<T> Ambassador = e.Item;
			if (!object.Equals(Ambassador.Item, default(T)))
				hash.Remove(Ambassador.Item);
		}
		protected override void OnClearing(CollectionEventArgs<Ambassador<T>> e)
		{
			base.OnClearing(e);
			foreach (Ambassador<T> ambassador in this)
			{
				ambassador.BeforeSetItem -= new AmbassadorEventHandler<T>(Ambassador_BeforeSetItem);
				ambassador.AfterSetItem -= new AmbassadorEventHandler<T>(Ambassador_AfterSetItem);
			}
		}
		protected override void OnCleared(CollectionEventArgs<Ambassador<T>> e)
		{
			base.OnCleared(e);
			hash.Clear();
		}
		private void Ambassador_BeforeSetItem(object sender, AmbassadorEventArgs<T> e)
		{
			Ambassador<T> Ambassador = e.Ambassador;
			if (!object.Equals(Ambassador.Item, default(T)))
				hash.Remove(Ambassador.Item);
		}
		private void Ambassador_AfterSetItem(object sender, AmbassadorEventArgs<T> e)
		{
			Ambassador<T> Ambassador = e.Ambassador;
			if (!object.Equals(Ambassador.Item, default(T)))
				hash.Add(Ambassador.Item, Ambassador);
		}
	}
}