﻿using System;
using System.ComponentModel;

namespace FORIS.TSS.Common
{
	/// <summary> Класс предназначенный для хранения списка Объектов-Посредников и для генерации событий, связанных с изменением этого списка </summary>
	/// <typeparam name="T"></typeparam>
	[ToolboxItem(false)]
	public abstract class Dispatcher<T> : Component
	{
		#region Constructor & Dispose

		public Dispatcher()
		{
			Initialize();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				foreach (Ambassador<T> ambassador in ambassadors)
					ambassador.Dispose();

				ambassadors.Clear();

				ambassadors.Inserted -= new CollectionChangeEventHandler<Ambassador<T>>(ambassadors_Inserted);
				ambassadors.Removing -= new CollectionChangeEventHandler<Ambassador<T>>(ambassadors_Removing);
				ambassadors.Clearing -= new CollectionEventHandler<Ambassador<T>>(ambassadors_Clearing);

				dlgtBeforeChange = null;
				dlgtAfterChange  = null;
			}

			base.Dispose(disposing);
		}

		#endregion Constructor & Dispose

		#region Initialize

		private void Initialize()
		{
			ambassadors = GetAmbassadorCollection();

			ambassadors.Inserted += new CollectionChangeEventHandler<Ambassador<T>>(ambassadors_Inserted);
			ambassadors.Removing += new CollectionChangeEventHandler<Ambassador<T>>(ambassadors_Removing);
			ambassadors.Clearing += new CollectionEventHandler<Ambassador<T>>(ambassadors_Clearing);

			eventHandlerBeforeSetItem = new AmbassadorEventHandler<T>(ambassador_BeforeSetItem);
			eventHandlerAfterSetItem  = new AmbassadorEventHandler<T>(ambassador_AfterSetItem);
#if DEBUG
			eventHandlerDisable = new AmbassadorEventHandler<T>(ambassador_Disable);
			eventHandlerEnable  = new AmbassadorEventHandler<T>(ambassador_Enable);
#endif
		}
		#endregion // Initialize

		protected abstract AmbassadorCollection<T> GetAmbassadorCollection();
		protected abstract void OnAfterSetItem(Ambassador<T> ambassador);
		protected abstract void OnBeforeSetItem(Ambassador<T> ambassador);

		#region Ambassadors

		private AmbassadorCollection<T> ambassadors;

		private void ambassadors_Inserted(object sender, CollectionChangeEventArgs<Ambassador<T>> e)
		{
			Ambassador<T> Ambassador = e.Item;

			OnAfterSetItem(Ambassador);

			Ambassador.BeforeSetItem += eventHandlerBeforeSetItem;
			Ambassador.AfterSetItem  += eventHandlerAfterSetItem;
#if DEBUG
			Ambassador.Disable += eventHandlerDisable;
			Ambassador.Enable  += eventHandlerEnable;
#endif
		}
		private void ambassadors_Removing(object sender, CollectionChangeEventArgs<Ambassador<T>> e)
		{
			Ambassador<T> Ambassador = e.Item;

			OnBeforeSetItem(Ambassador);

			Ambassador.BeforeSetItem -= eventHandlerBeforeSetItem;
			Ambassador.AfterSetItem  -= eventHandlerAfterSetItem;

#if DEBUG
			Ambassador.Disable -= eventHandlerDisable;
			Ambassador.Enable  -= eventHandlerEnable;
#endif
		}
		private void ambassadors_Clearing(object sender, CollectionEventArgs<Ambassador<T>> e)
		{
			foreach (Ambassador<T> ambassador in ambassadors)
			{
				OnBeforeSetItem(ambassador);

				ambassador.BeforeSetItem -= eventHandlerBeforeSetItem;
				ambassador.AfterSetItem  -= eventHandlerAfterSetItem;
#if DEBUG
				ambassador.Disable -= eventHandlerDisable;
				ambassador.Enable  -= eventHandlerEnable;
#endif
			}
		}


		#endregion //Ambassadors

		#region Ambassador events

		private AmbassadorEventHandler<T> eventHandlerBeforeSetItem;
		private AmbassadorEventHandler<T> eventHandlerAfterSetItem;
#if DEBUG
		private AmbassadorEventHandler<T> eventHandlerDisable;
		private AmbassadorEventHandler<T> eventHandlerEnable;
#endif

		private void ambassador_BeforeSetItem(object sender, AmbassadorEventArgs<T> e)
		{
			OnBeforeSetItem(e.Ambassador);
		}
		private void ambassador_AfterSetItem(object sender, AmbassadorEventArgs<T> e)
		{
			OnAfterSetItem(e.Ambassador);
		}

#if DEBUG
		private void ambassador_Disable(object sender, AmbassadorEventArgs<T> e)
		{
			OnBeforeSetItem(e.Ambassador);
		}
		private void ambassador_Enable(object sender, AmbassadorEventArgs<T> e)
		{
			OnAfterSetItem(e.Ambassador);
		}
#endif

		#endregion //Ambassador events

		#region Events

		private EventHandler dlgtBeforeChange;
		private EventHandler dlgtAfterChange;

		public event EventHandler BeforeChange
		{
			add    { dlgtBeforeChange += value; }
			remove { dlgtBeforeChange -= value; }
		}
		public event EventHandler AfterChange
		{
			add    { dlgtAfterChange += value; }
			remove { dlgtAfterChange -= value; }
		}
		protected virtual void OnBeforeChange()
		{
			dlgtBeforeChange?.Invoke(this, EventArgs.Empty);
		}
		protected virtual void OnAfterChange()
		{
			dlgtAfterChange?.Invoke(this, EventArgs.Empty);
		}

		#endregion Events
	}
}