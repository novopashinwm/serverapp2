﻿using System;

namespace FORIS.TSS.Common
{
	public delegate void AmbassadorEventHandler<T>(object sender, AmbassadorEventArgs<T> e);

	public class AmbassadorEventArgs<T> : EventArgs
	{
		private readonly Ambassador<T> ambassador;
		public Ambassador<T> Ambassador
		{
			get { return this.ambassador; }
		}
		public AmbassadorEventArgs(Ambassador<T> ambassador)
		{
			this.ambassador = ambassador;
		}
	}
}