﻿using System.ComponentModel;

namespace FORIS.TSS.Common
{
	/// <summary> Объект-Посредник </summary>
	/// <typeparam name="T"></typeparam>
	[ToolboxItem(false)]
	[DesignTimeVisible(false)]
	public class Ambassador<T> : Component
	{
		#region Public properties

		private bool m_Enabled = true;
		[Browsable(true)]
		[DefaultValue(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool Enabled
		{
			get { return m_Enabled; }
			set
			{
				if (m_Enabled != value)
				{
					m_Enabled = value;
					if (m_Enabled)
						OnEnable(new AmbassadorEventArgs<T>(this));
					else
						OnDisable(new AmbassadorEventArgs<T>(this));
				}
			}
		}
		private T item = default(T);
		[Browsable(true)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public T Item
		{
			get { return item; }
			set
			{
				OnBeforeSetItem(new AmbassadorEventArgs<T>(this));
				item = value;
				OnAfterSetItem(new AmbassadorEventArgs<T>(this));
			}
		}

		#endregion Public properties

		#region Constructor & Dispose

		public Ambassador()
		{
		}
		public Ambassador(T Item)
		{
			this.Item = Item;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				dlgtBeforeSetItem = null;
				dlgtAfterSetItem  = null;
				dlgtEnable        = null;
				dlgtDisable       = null;
			}
			base.Dispose(disposing);
		}

		#endregion // Constructor & Dispose

		#region Events

		private AmbassadorEventHandler<T> dlgtBeforeSetItem;
		private AmbassadorEventHandler<T> dlgtAfterSetItem;
		private AmbassadorEventHandler<T> dlgtEnable;
		private AmbassadorEventHandler<T> dlgtDisable;

		public event AmbassadorEventHandler<T> BeforeSetItem
		{
			add    { dlgtBeforeSetItem += value; }
			remove { dlgtBeforeSetItem -= value; }
		}
		public event AmbassadorEventHandler<T> AfterSetItem
		{
			add    { dlgtAfterSetItem += value; }
			remove { dlgtAfterSetItem -= value; }
		}
		public event AmbassadorEventHandler<T> Enable
		{
			add    { dlgtEnable += value; }
			remove { dlgtEnable -= value; }
		}
		public event AmbassadorEventHandler<T> Disable
		{
			add    { dlgtDisable += value; }
			remove { dlgtDisable -= value; }
		}
		protected virtual void OnBeforeSetItem(AmbassadorEventArgs<T> e)
		{
			dlgtBeforeSetItem?.Invoke(this, e);
		}
		protected virtual void OnAfterSetItem(AmbassadorEventArgs<T> e)
		{
			dlgtAfterSetItem?.Invoke(this, e);
		}
		protected virtual void OnEnable(AmbassadorEventArgs<T> e)
		{
			dlgtEnable?.Invoke(this, e);
		}
		protected virtual void OnDisable(AmbassadorEventArgs<T> e)
		{
			dlgtDisable?.Invoke(this, e);
		}

		#endregion // Events
	}
}