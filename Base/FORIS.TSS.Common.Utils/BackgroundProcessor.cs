﻿using System;
using System.Diagnostics;
using System.Threading;
using FORIS.TSS.Common.Helpers;

namespace FORIS.TSS.Common
{
	public abstract class BackgroundProcessor
	{
		private readonly EventWaitHandle _eventWaitHandle = new ManualResetEvent(true);

		private readonly Thread   _thread;
		private readonly TimeSpan _sleepInterval;
		private          bool     _continue;

		protected event EventHandler BeforeStart;
		protected event EventHandler AfterStop;

		protected BackgroundProcessor(TimeSpan? sleepInterval)
		{
			_sleepInterval = sleepInterval ?? TimeSpan.FromSeconds(1);
			_thread        = new Thread(ThreadLoop) { IsBackground = true };
		}
		protected BackgroundProcessor()
			: this(TimeSpan.FromSeconds(1))
		{
		}
		public bool Running
		{
			get
			{
				return _continue;
			}
		}
		public void Start()
		{
			try
			{
				OnBeforeStart();
				_continue = true;
				// Все настройки внутренних переменных BackgroundProcessor нужно производить до вызова _thread.Start();
				_thread.Start();

				$"{this.GetTypeName(false)} started, ThreadId={_thread?.ManagedThreadId ?? -1}"
					.CallTraceInformation();
			}
			catch (Exception ex)
			{
				$"{this.GetTypeName(false)} not started"
					.WithException(ex)
					.CallTraceError();
			}
		}
		public void Stop()
		{
			if (!_continue)
				return;

			_continue = false;
			_eventWaitHandle.Set();

			var commandQueueReader = _thread;
			if (commandQueueReader != null && commandQueueReader.IsAlive)
			{
				commandQueueReader.Join(_sleepInterval);
				if (commandQueueReader.IsAlive)
				{
					commandQueueReader.Interrupt();
					commandQueueReader.Join(_sleepInterval);
				}
			}

			$"{this.GetTypeName(false)} stopped, ThreadId={_thread?.ManagedThreadId ?? -1}"
				.CallTraceInformation();

			OnAfterStop();
		}
		private readonly Stopwatch _stopwatch = new Stopwatch();
		private void ThreadLoop()
		{
			while (_continue)
			{
				try
				{
					if (!Do())
						Wait();
				}
				catch (ThreadAbortException)
				{
					return;
				}
				catch(ThreadInterruptedException)
				{
					return;
				}
				catch (Exception ex)
				{
					$"{this.GetTypeName(false)} unexpected error"
						.WithException(ex, true)
						.CallTraceError();
				}
			}
		}
		/// <summary> Тело цикла фоновой работы </summary>
		/// <returns> Возвращает true, если что-либо было сделано и false в противном случае </returns>
		protected abstract bool Do();
		protected void Wait(TimeSpan? timeSpan = null)
		{
			if (!_stopwatch.IsRunning)
			{
				_stopwatch.Start();
				return;
			}
			_stopwatch.Stop();
			var sleepInterval = timeSpan ?? _sleepInterval;
			if (sleepInterval <= _stopwatch.Elapsed)
			{
				_stopwatch.Restart();
				return;
			}

			_eventWaitHandle.Reset();
			_eventWaitHandle.WaitOne(sleepInterval - _stopwatch.Elapsed);

			_stopwatch.Restart();
		}
		protected void OnBeforeStart()
		{
			BeforeStart?.Invoke(this, new EventArgs());
		}
		protected void OnAfterStop()
		{
			AfterStop?.Invoke(this, new EventArgs());
		}
	}
}