﻿using System;
using System.Collections.Concurrent;

namespace FORIS.TSS.Common
{
	public class BulkProcessor<T> : BackgroundProcessor
	{
		private readonly ConcurrentQueue<T> _queue = new ConcurrentQueue<T>();
		private readonly Action<T[]>        _process;
		private readonly int                _maxDequeuedCount;

		public void Enqueue(T item)
		{
			_queue.Enqueue(item);
		}
		public BulkProcessor(Action<T[]> process, int maxDequeuedCount = int.MaxValue)
			: base(TimeSpan.FromMilliseconds(250))
		{
			_process          = process;
			_maxDequeuedCount = maxDequeuedCount;
		}
		protected override bool Do()
		{
			var countToDequeue = 0 < _maxDequeuedCount.CompareTo(_queue.Count)
				? _queue.Count
				: _maxDequeuedCount;

			T[] items;

			if (countToDequeue != 0)
			{
				items = new T[countToDequeue];
				for (var i = 0; i != countToDequeue; ++i)
				{
					if (_queue.TryDequeue(out T item))
						items[i] = item;
				}
			}
			else
			{
				items = new T[] { };
			}

			// Необходимо поддерживать WeakReferences в Remoting, для этого генерируются пустые события.
			_process(items);

			return items.Length != 0;
		}
	}
}