using System;
using System.IO;
using System.Diagnostics;
using System.Reflection;

namespace FORIS.TSS.Common
{
	/// <summary>
	/// Summary description for DirectoryHelper.
	/// </summary>
	public class DirectoryHelper
	{
		static readonly string program_files = "PROGRAM FILES";

		[Conditional("DEBUG")]
		public static void AdjustForDebug(ref DirectoryInfo di, string pathDiff)
		{
			if (di.FullName.ToUpper().IndexOf(program_files) < 0)
			{
				di = new DirectoryInfo(di.FullName + pathDiff);
				Debug.Assert(di.Exists);
			}
		}

		[Conditional("DEBUG")]
		public static void AdjustForDebug(ref FileInfo fi, string pathDiff)
		{
			if (fi.FullName.ToUpper().IndexOf(program_files) < 0)
			{
				DirectoryInfo dir = new DirectoryInfo(fi.DirectoryName + pathDiff);
				fi = new FileInfo(dir.FullName + "\\" + fi.Name);
				Debug.Assert(fi.Exists);
			}
		}

		/// <summary> 
		/// Creates a full file name from the path of the executing assembly and a specified file name. 
		/// </summary> 
		public static string GetFolderNameForExecutingAssembly() 
		{ 
			System.Uri uri = new Uri(string.Format("{0}\\", 
				System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase)
				)); 
			return uri.LocalPath; 
		} 

		/// <summary> 
		/// Creates a full file name from the path of the executing assembly and a specified file name. 
		/// </summary> 
		public static string CreateFileNameInExecutingAssemblyPath(string fileName) 
		{ 
			System.Uri uri = new Uri(string.Format("{0}\\{1}", 
				System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase), 
				fileName)); 
			return uri.LocalPath; 
		} 

	}
}
