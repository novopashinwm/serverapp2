﻿namespace FORIS.TSS.Common
{
	public interface IEntryPoint
	{
		void Start(bool runLoop, bool activate, bool staticServer);
		void Stop();
	}
}