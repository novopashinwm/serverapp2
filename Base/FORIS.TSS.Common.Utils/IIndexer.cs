using System.Collections;

namespace FORIS.TSS.Common
{
	public interface IIndexer<T>
	{
		T this[ int index ] { get; }
	}

	public class Indexer<T, TList>:
		IIndexer<T>
		where TList: IList
	{
		public Indexer( TList list )
		{
			this.list = list;
		}

		private readonly TList list;

		public T this[ int index ]
		{
			get { return (T)this.list[index]; }
		}
	}
}