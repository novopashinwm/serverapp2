﻿using System;
using System.Linq;
using System.Configuration;
using System.Diagnostics;
using System.Xml;
using FORIS.TSS.BusinessLogic;

namespace FORIS.TSS.Common
{
	public static class ConfigHelper
	{
		public static string GetConnectionString(this string connectionStringKey)
		{
			return !string.IsNullOrWhiteSpace(connectionStringKey)
				? ConfigurationManager.ConnectionStrings[connectionStringKey]?.ConnectionString
				: default;
		}
		public static string GetAppSettingAsString(this string appSettingKey, string defaultValue = null)
		{
			return !string.IsNullOrWhiteSpace(appSettingKey)
				? ConfigurationManager.AppSettings[appSettingKey] ?? defaultValue
				: defaultValue;
		}
		/// <summary> Получить параметр конфигурации по ключу, в конфигурационном файле параметр называется "Имя класса"."ключ" </summary>
		public static string GetAppSettingAsString(this Type type, string key, string defaultValue = null)
		{
			return $@"{type.Name}.{key}".GetAppSettingAsString(defaultValue);
		}
		public static string GetAppSettingAsString(this object o, string key, string defaultValue = null)
		{
			return o?.GetType()?.GetAppSettingAsString(key, defaultValue) ?? defaultValue;
		}
		public static bool GetAppSettingAsBoolean(this string appSettingKey, bool defaultValue = false)
		{
			var appSettingValue = appSettingKey?.GetAppSettingAsString();
			if (string.IsNullOrWhiteSpace(appSettingValue))
				return defaultValue;
			return bool.TrueString.Equals(appSettingValue, StringComparison.OrdinalIgnoreCase);
		}
		public static bool GetAppSettingAsBoolean(this Type type, string key, bool defaultValue = false)
		{
			return $@"{type.Name}.{key}"?.GetAppSettingAsBoolean(defaultValue) ?? defaultValue;
		}
		public static bool GetAppSettingAsBoolean(this object o, string key, bool defaultValue = false)
		{
			return o?.GetType()?.GetAppSettingAsBoolean(key, defaultValue) ?? defaultValue;
		}
		public static int GetAppSettingAsInt32(this string appSettingKey, int defaultValue)
		{
			var appSettingValue = appSettingKey?.GetAppSettingAsString();
			if (string.IsNullOrWhiteSpace(appSettingValue))
				return defaultValue;
			int result = int.TryParse(appSettingValue, out result)
				? result
				: defaultValue;
			return result;
		}
		public static int GetAppSettingAsInt32(this Type type, string key, int defaultValue)
		{
			return $@"{type.Name}.{key}"?.GetAppSettingAsInt32(defaultValue) ?? defaultValue;
		}
		public static int GetAppSettingAsInt32(this object o, string key, int defaultValue)
		{
			return o?.GetType()?.GetAppSettingAsInt32(key, defaultValue) ?? defaultValue;
		}
		public static TimeSpan? GetAppSettingAsTimeSpan(this string appSettingKey, TimeSpan? defaultValue = null)
		{
			var appSettingValue = appSettingKey?.GetAppSettingAsString();
			if (string.IsNullOrWhiteSpace(appSettingValue))
				return defaultValue;
			try
			{
				return XmlConvert.ToTimeSpan(appSettingValue);
			}
			catch (FormatException ex)
			{
				Trace.TraceError(
					"Unable to parse AppSettings[Key={0}].Value={1}:{2}",
					appSettingKey, appSettingValue, ex);
			}
			return defaultValue;
		}
		public static TimeSpan? GetAppSettingAsTimeSpan(this Type type, string key, TimeSpan? defaultValue = null)
		{
			return $@"{type.Name}.{key}"?.GetAppSettingAsTimeSpan(defaultValue);
		}
		public static TimeSpan? GetAppSettingAsTimeSpan(this object o, string key, TimeSpan? defaultValue = null)
		{
			return o?.GetType()?.GetAppSettingAsTimeSpan(key, defaultValue) ?? defaultValue;
		}
		public static DateTime? GetAppSettingAsDateTime(this string appSettingKey, DateTime? defaultValue = null)
		{
			var appSettingValue = appSettingKey?.GetAppSettingAsString();
			if (string.IsNullOrWhiteSpace(appSettingValue))
				return defaultValue;
			return TimeHelper.FromXmlFormat(appSettingValue) ?? defaultValue;
		}
		public static DateTime? GetAppSettingAsDateTime(this Type type, string key, DateTime? defaultValue = null)
		{
			return $@"{type.Name}.{key}"?.GetAppSettingAsDateTime(defaultValue);
		}
		public static DateTime? GetAppSettingAsDateTime(this object o, string key, DateTime? defaultValue = null)
		{
			return o?.GetType()?.GetAppSettingAsDateTime(key, defaultValue) ?? defaultValue;
		}
	}
}