﻿using System;
using System.Collections.Specialized;
using System.Configuration;

namespace FORIS.TSS.Config
{
	/// <summary> Provides access to machine-specific sections of domain-default configuration file </summary>
	public static class GlobalsConfig
	{
		public static NameValueCollection AppSettings
		{
			get
			{
				var res             = new NameValueCollection();
				var defaultSettings = ConfigurationManager.AppSettings;
				if (defaultSettings != null)
				{
					foreach (string key in defaultSettings.Keys)
						res.Set(key, defaultSettings[key]);
				}
				var machineName    = Environment.MachineName;
				var customSettings = ConfigurationManager.GetSection(machineName) as NameValueCollection;
				if (customSettings != null)
				{
					foreach (string key in customSettings.Keys)
						res.Set(key, customSettings[key]);
				}
				return res;
			}
		}
	}
}