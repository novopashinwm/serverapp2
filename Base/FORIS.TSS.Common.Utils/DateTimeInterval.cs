﻿using System;

namespace FORIS.TSS.BusinessLogic
{
	[Serializable]
	public class DateTimeInterval
	{
		public DateTime         DateFrom { get; set; }
		public DateTime         DateTo   { get; set; }
		public IntervalAccuracy Accuracy { get; set; }
		private DateTimeInterval() { }
		public DateTimeInterval(IntervalAccuracy intervalAccuracy)
		{
			Accuracy = intervalAccuracy;
		}
		public DateTimeInterval(DateTime dateTimeFrom, DateTime dateTimeTo)
		{
			DateFrom = dateTimeFrom;
			DateTo   = dateTimeTo;
		}
		public DateTimeInterval(DateTime dateTimeFrom, DateTime dateTimeTo, IntervalAccuracy accuracy)
			: this(dateTimeFrom, dateTimeTo)
		{
			Accuracy = accuracy;
		}
		public void SetAccuracy(IntervalAccuracy intervalAccuracy)
		{
			Accuracy = intervalAccuracy;
		}
		public override string ToString()
		{
			return $"{DateFrom}-{DateTo}:{Accuracy}";
		}
	}
}