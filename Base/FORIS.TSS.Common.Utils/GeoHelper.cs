﻿using System;
using System.Collections.Generic;
using System.Linq;
using Compass.Ufin.Gis;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Map;
using FORIS.TSS.Resources;
using Interfaces.Geo;

namespace FORIS.TSS.Common
{
	public static class GeoHelper
	{
		public static readonly double InvalidLongitude = 999;
		public static readonly double InvalidLatitude  = 999;
		/// <summary> Окружность Земли по экватору </summary>
		public static readonly double EarthCircleInMeters = 40000000;
		/// <summary> Градусов широты в 1 метре </summary>
		public static readonly double DegreesPerMeter = 360.0 / EarthCircleInMeters;
		public static double AngleBetweenLocations(
			double lat1Degrees, double lng1Degrees,
			double lat2Degrees, double lng2Degrees)
		{
			double lat1Radians = Radians(lat1Degrees);
			double lng1Radians = Radians(lng1Degrees);
			double lat2Radians = Radians(lat2Degrees);
			double lng2Radians = Radians(lng2Degrees);

			double a = lng1Radians - lng2Radians;

			if (a < 0.0)
			{
				a = -a;
			}

			if (a > Math.PI)
			{
				a = 2.0 * Math.PI - a;
			}

			var cos =
				Math.Sin(lat2Radians) * Math.Sin(lat1Radians) +
				Math.Cos(lat2Radians) * Math.Cos(lat1Radians) * Math.Cos(a);

			if (1 <= cos)
				return 0;

			if (cos <= -1)
				return Math.PI;

			return Math.Acos(cos);
		}
		public static double Radians(double degrees)
		{
			return degrees * Math.PI / 180.0;
		}
		/// <summary> Вычисляет расстояние в метрах между двумя точками, координаты которых указаны в градусах </summary>
		/// <param name="lat1Degrees"> Широта первой точки </param>
		/// <param name="lng1Degrees"> Долгота первой точки </param>
		/// <param name="lat2Degrees"> Широта второй точки </param>
		/// <param name="lng2Degrees"> Долгота второй точки </param>
		/// <returns> Возвращает расстояние в метрах </returns>
		public static double DistanceBetweenLocations(
			double lat1Degrees, double lng1Degrees,
			double lat2Degrees, double lng2Degrees)
		{
			double angle = AngleBetweenLocations(
				lat1Degrees, lng1Degrees, lat2Degrees, lng2Degrees);

			const double circumference = 40075160; // meters at equator

			return circumference * angle / (2.0 * Math.PI);
		}
		public static double DistanceToLine(
			double fromLat,   double fromLng,
			double toLat,     double toLng,
			double targetLat, double targetLng)
		{
			//NB: Работает только для небольших расстояний, для общего случая нужно вывести аналитическую формулу

			var lineDLng = toLng - fromLng;
			if (180 < lineDLng)
				lineDLng = 180 - lineDLng;
			else if (lineDLng < -180)
				lineDLng = 360 + lineDLng;

			var lineDLat = toLat - fromLat;
			var lineLength = Math.Sqrt(lineDLat * lineDLat + lineDLng * lineDLng);

			if (lineLength == 0)
				return DistanceBetweenLocations(fromLat, fromLng, targetLat, targetLng);

			var pointDLat = targetLat - fromLat;
			var pointDLng = targetLng - fromLng;

			if (180 < pointDLng)
				pointDLng = 180 - pointDLng;
			else if (pointDLng < -180)
				pointDLng = 360 + pointDLng;

			var eLat = lineDLat / lineLength;
			var eLng = lineDLng / lineLength;

			var t = eLat * pointDLat + eLng * pointDLng;
			if (t <= 0)
				return DistanceBetweenLocations(fromLat, fromLng, targetLat, targetLng);
			if (lineLength <= t)
				return DistanceBetweenLocations(toLat, toLng, targetLat, targetLng);
			return DistanceBetweenLocations(
				fromLat + eLat * t, fromLng + eLng * t,
				targetLat, targetLng);
		}
		/// <summary>
		/// Проверяет вхождение точки с координатами (pointLat, pointLng) в область,
		/// ограниченную параллелями south и north и меридианами west и east
		/// </summary>
		public static bool Contains(double pointLat, double pointLng,
			double south, double west, double north, double east)
		{
			if (pointLat < south)
				return false;
			if (north < pointLat)
				return false;
			west += 180;
			east += 180;
			pointLng += 180;

			return west < east
				? west     <= pointLng && pointLng <= east
				: pointLng <= east     || west     <= pointLng;
		}
		public static double GetMetersPerDegreeLatitude(double lat)
		{
			return 111132.92d - 559.82d * Math.Cos(2d * lat) + 1.175d * Math.Cos(4d * lat);
		}
		public static double GetMetersPerDegreeLongitude(double lng)
		{
			return 111412.84d * Math.Cos(lng) - 93.5d * Math.Cos(3d * lng);
		}
		public static bool ValidLocation(double lat, double lng)
		{
			return
				-090d <= lat && lat <= 090d &&
				-180d <= lng && lng <= 180d;
		}
		/// <summary> Получить текст координат, по координатам </summary>
		/// <param name="lat"> Широта </param>
		/// <param name="lng"> Долгота </param>
		/// <returns></returns>
		public static string GetPositionString(double lat, double lng)
		{
			return $"{lat.ToString("0.#####", NumberHelper.FormatInfo)} {lng.ToString("0.#####", NumberHelper.FormatInfo)}";
		}
		/// <summary> Получить текст расстояния в метрах </summary>
		/// <param name="distanceInmeters"> Расстояние в метрах </param>
		/// <param name="language"> Язык </param>
		/// <returns></returns>
		public static string GetDistaceString(int distanceInmeters, string language)
		{
			return $"{distanceInmeters}{ResourceContainers.Get(language)[UnitOfMeasure.Meters]}";
		}
		public static string GetInaccurateAddressString(AddressRequest req, AddressResponse res, string lang)
		{
			return string.Empty
				+ $"{GetDistaceString((res?.Distance ?? 0) / 100 * 100, lang)}"
				+ $" "
				+ $"{ResourceContainers.Get(lang).GetLocalizedMessagePart("ToPoint")}"
				+ $" "
				+ $"{res?.Address}"
				+ $" "
				+ $"({GetPositionString(req.Lat, req.Lng)})";
		}
		public static IEnumerable<GeoZonePoint> ExcludeNeighborDuplicates(this IEnumerable<GeoZonePoint> points, double precisionInMeters = 1.0d)
		{
			var prevPoint = default(GeoZonePoint);
			foreach (var point in points)
			{
				if (prevPoint != null)
				{
					var distanceFromPrevPoint = DistanceBetweenLocations(point.Lat, point.Lng, prevPoint.Lat, prevPoint.Lng);
					if (distanceFromPrevPoint < precisionInMeters)
						continue;
				}
				prevPoint = point;
				yield return point;
			}
		}
		public static bool CheckZonePrimitive(this IEnumerable<GeoZonePoint> points, ZoneTypes type)
		{
			var pointArray = points.ToArray();
			switch (type)
			{
				case ZoneTypes.Circle:
					return pointArray.ValidCircle();
				case ZoneTypes.Polygon:
					return pointArray.ValidPolygon();
				default:
					throw new ArgumentOutOfRangeException(nameof(type), type, @"Check zone primitive is not implemented");
			}
		}
		public static bool CheckZonePrimitive(this GeoZone dtoGeoZone)
		{
			return dtoGeoZone.Points.CheckZonePrimitive(dtoGeoZone.Type);
		}
		public static IEnumerable<MatrixNode> GetMatrixNodes(this IEnumerable<GeoZonePoint> points, ZoneTypes type)
		{
			var pointArray = points.ToArray();
			switch (type)
			{
				case ZoneTypes.Circle:
					return pointArray.Single().GetMatrixForCircle();
				case ZoneTypes.Polygon:
					return pointArray.GetMatrixForPolygon();
				default:
					throw new NotImplementedException("Zone type matrix index is not implemented");
			}
		}
		public static IEnumerable<MatrixNode> GetMatrixNodes(this GeoZone dtoGeoZone)
		{
			return dtoGeoZone.Points.GetMatrixNodes(dtoGeoZone.Type);
		}
	}
}