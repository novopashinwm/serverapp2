﻿using System;
using System.Linq;
using System.Reflection;

namespace FORIS.TSS.Common
{
	/// <summary> Свойства сборки </summary>
	public static class AssemblyExtensions
	{
		public static string  Company        (this Assembly assembly) => assembly?.GetCustomAttributes()?.OfType<AssemblyCompanyAttribute>      ()?.FirstOrDefault()?.Company;
		public static string  Product        (this Assembly assembly) => assembly?.GetCustomAttributes()?.OfType<AssemblyProductAttribute>      ()?.FirstOrDefault()?.Product;
		public static string  Copyright      (this Assembly assembly) => assembly?.GetCustomAttributes()?.OfType<AssemblyCopyrightAttribute>    ()?.FirstOrDefault()?.Copyright;
		public static string  Trademark      (this Assembly assembly) => assembly?.GetCustomAttributes()?.OfType<AssemblyTrademarkAttribute>    ()?.FirstOrDefault()?.Trademark;
		public static string  Title          (this Assembly assembly) => assembly?.GetCustomAttributes()?.OfType<AssemblyTitleAttribute>        ()?.FirstOrDefault()?.Title;
		public static string  Description    (this Assembly assembly) => assembly?.GetCustomAttributes()?.OfType<AssemblyDescriptionAttribute>  ()?.FirstOrDefault()?.Description;
		public static string  Configuration  (this Assembly assembly) => assembly?.GetCustomAttributes()?.OfType<AssemblyConfigurationAttribute>()?.FirstOrDefault()?.Configuration;
		public static string  FileVersion    (this Assembly assembly) => assembly?.GetCustomAttributes()?.OfType<AssemblyFileVersionAttribute>  ()?.FirstOrDefault()?.Version;
		public static string  Location       (this Assembly assembly) => assembly?.Location;
		public static Version Version        (this Assembly assembly) => assembly?.GetName()?.Version;
		public static string  VersionFull    (this Assembly assembly) => assembly?.Version()?.ToString();
		public static string  VersionMajor   (this Assembly assembly) => assembly?.Version()?.Major.ToString();
		public static string  VersionMinor   (this Assembly assembly) => assembly?.Version()?.Minor.ToString();
		public static string  VersionBuild   (this Assembly assembly) => assembly?.Version()?.Build.ToString();
		public static string  VersionRevision(this Assembly assembly) => assembly?.Version()?.Revision.ToString();
	}
}