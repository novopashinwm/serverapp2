﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FORIS.TSS.Common
{
    /// <summary>
    /// долгота
    /// </summary>
    public struct Longitude
    {
        private int _dig, _min;
        private float _sec;
        private string _hemisphere;

        public Longitude(int dig, int min, float sec, string hemisphere)
        {
            if(dig > 180 || dig < -180)
                throw new ArgumentOutOfRangeException("dig", "Longitude dig range is from -180 to 180.");
            if (min > 60 || min < 0)
                throw new ArgumentOutOfRangeException("min", "Longitude min range is from 0 to 60.");
            if (sec > 60 || sec < 0)
                throw new ArgumentOutOfRangeException("sec", "Longitude sec range is from 0 to 60.");
            this._dig = dig;
            this._min = min;
            this._sec = sec;
            _hemisphere = hemisphere;
        }

        public Longitude(decimal coordinate)
        {
            if (coordinate < 0)
            {
                _hemisphere = "W";
                coordinate = -coordinate;
            }
            else
            {
                _hemisphere = "E";
            }


            this._dig = (int)coordinate;
            this._min = (int)((coordinate - _dig) * 60);
            this._sec = (float)(((coordinate - _dig) * 60) - this._min) * 60;

            if (_dig > 180 || _dig < -180)
                throw new ArgumentOutOfRangeException("dig", "Longitude dig range is from -180 to 180.");
            if (_min > 60 || _min < 0)
                throw new ArgumentOutOfRangeException("min", "Longitude min range is from 0 to 60.");
            if (_sec > 60 || _sec < 0)
                throw new ArgumentOutOfRangeException("sec", "Longitude sec range is from 0 to 60.");
        }

        /// <summary>
        /// получить строку вида 60°25'446.9916"E
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return _dig + "°"+_min+"'"+Math.Round(_sec, 2)+"\"" + _hemisphere;
        }
    }
}
