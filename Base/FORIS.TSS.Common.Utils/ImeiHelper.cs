﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FORIS.TSS.Common
{
	public class ImeiHelper
	{
		public static readonly int ImeiLength = 15;

		private static int GetEvenImeiCharValue(int c)
		{
			switch (c)
			{
				case 0: return 0;
				case 1: return 2;
				case 2: return 4;
				case 3: return 6;
				case 4: return 8;
				case 5: return 1; // 5*2=10, 1+0=1
				case 6: return 3; // 6*2=12, 1+2=3
				case 7: return 5; // 7*2=14, 1+4=5
				case 8: return 7; // 8*2=16, 1+6=7
				case 9: return 9; // 9*2=18, 1+8=9
				default:
					throw new ArgumentOutOfRangeException("c", c, @"Digit value is expected");
			}
		}

		public static bool IsImeiCorrect(string imei)
		{
			if (imei.Length != ImeiLength)
				return false;

			var control = GetImeiControlDigit(imei.Take(ImeiLength - 1));

			return imei[ImeiLength - 1] - '0' == control;
		}

		public static int? GetImeiControlDigit(IEnumerable<char> imei)
		{
			var sum = 0;
			var index = 0;
			var count = 0;

			foreach (var c in imei)
			{
				if (c < '0' || '9' < c)
					return null;

				++index;
				var value = (c - '0');
				if (index % 2 == 0)
					sum += GetEvenImeiCharValue(value);
				else
					sum += value;

				++count;
			}

			if (count != 14)
				return null;

			return sum % 10 == 0 ? 0 : (10 - sum % 10);
		}
	}
}