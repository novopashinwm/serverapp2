﻿using System;
using System.Collections.Generic;

namespace FORIS.TSS.Common
{
	public static class CollectionHelper
	{
		public static BinarySearchResult BinarySearch<T>(
			this IList<T> collection,
			Func<T, int> comparison)
		{
			if (comparison == null)
				throw new ArgumentNullException(nameof(comparison));

			if (collection == null || collection.Count == 0)
				return new BinarySearchResult(null, null);

			var i0 = 0;
			var compareToFirstItem = comparison(collection[i0]);
			if (compareToFirstItem < 0) //Искомый элемент меньше первого элемента коллекции
				return new BinarySearchResult(null, i0);

			if (compareToFirstItem == 0) //Искомый элемент равен первому элементу коллекции
				return new BinarySearchResult(i0, i0);

			var i2 = collection.Count - 1;
			var compareToLastItem = comparison(collection[i2]);
			if (0 < compareToLastItem) //Искомый элемент больше последнего элемента коллекции
				return new BinarySearchResult(i2, null);
			if (0 == compareToLastItem)
				return new BinarySearchResult(i2, i2); //Искомый элемент равен последнему элементу коллекции

			while (true)
			{
				var i1 = (i2 + i0) / 2;
				var c1 = comparison(collection[i1]);
				if (0 < c1)
					i0 = i1;
				else if (c1 < 0)
					i2 = i1;
				else
					return new BinarySearchResult(i1, i1);

				if (i2 - i0 <= 1)
					return new BinarySearchResult(i0, i2);
			}
		}

		public static bool EndsWith<T>(this T[] collection, T[] items)
		{
			if (collection == null)
				throw new ArgumentNullException("collection");
			if (items == null)
				throw new ArgumentNullException("items");

			if (collection.Length < items.Length)
				return false;

			for (var i = 0; i != items.Length; ++i)
			{
				if (!Equals(collection[collection.Length - items.Length + i], items[i]))
					return false;
			}

			return true;
		}
	}

	public struct BinarySearchResult
	{
		/// <summary>
		/// Есть меньший или равный искомому значению элемент.
		/// </summary>
		public readonly bool HasFrom;
		/// <summary>
		/// Индекс элемента в коллекции меньший или равный искомому значению.
		/// </summary>
		private readonly int _from;
		/// <summary>
		/// Есть больший или равный искомому значению элемент.
		/// </summary>
		public readonly bool HasTo;
		/// <summary>
		/// Индекс элемента в коллекции больший или равный искомому значению.
		/// </summary>
		private readonly int _to;
		/// <summary>
		/// Коллекция не содержит элементов.
		/// </summary>
		public bool Empty
		{
			get { return !HasFrom && !HasTo; }
		}
		/// <summary>
		/// true если найдено точное совпадения с искомым значением.
		/// </summary>
		public bool Single
		{
			get { return HasFrom && HasTo && _from == _to; }
		}

		/// <summary>
		/// Индекс элемента в коллекции меньший или равный искомому значению.
		/// </summary>
		public int From
		{
			get
			{
				if (!HasFrom)
					throw new InvalidOperationException("Попытка получить значение, когда HasFrom = false");
				return _from;
			}
		}

		/// <summary>
		/// Индекс элемента в коллекции больший или равный искомому значению.
		/// </summary>
		public int To
		{
			get
			{
				if (!HasTo)
					throw new InvalidOperationException("Попытка получить значение, когда HasTo = false");
				return _to;
			}
		}

		internal BinarySearchResult(int? from, int? to)
		{
			_from = from ?? -1;
			_to = to ?? -1;
			HasFrom = from.HasValue;
			HasTo = to.HasValue;
		}
	}
}