﻿namespace FORIS.TSS.Helpers
{
	public static class CrcHelper
	{
		public static byte Crc8(this byte[] data)
		{
			return Common.Checksum.Crc8.ComputeChecksum(data);
		}

		public static ushort Crc16Ccitt(this byte[] data, ushort init = (ushort)Common.Checksum.Crc16Ccitt.InitialCrcValue.InitFFFF)
		{
			return Common.Checksum.Crc16Ccitt.ComputeChecksum(data, init);
		}
	}
}