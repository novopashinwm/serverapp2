﻿using System;

namespace FORIS.TSS.BusinessLogic
{
	[Serializable]
	public class WorkingHoursInterval
	{
		public TimeSpan WorkFrom { get; set; }
		public TimeSpan WorkTo   { get; set; }
		private WorkingHoursInterval()
		{
		}
		public WorkingHoursInterval(TimeSpan workFrom, TimeSpan workTo)
		{
			WorkFrom = workFrom;
			WorkTo   = workTo;
		}
		public override string ToString()
		{
			return WorkFrom + "-" + WorkTo;
		}
	}
}