using System;
using System.Diagnostics;

namespace FORIS.TSS.Common
{
	/// <summary>
	/// bit mask routines
	/// </summary>
	public class BitMask
	{
		/// <summary>
		/// bit mask
		/// </summary>
		long lMask;
		
		int iLen;
		/// <summary>
		/// mask len
		/// </summary>
		public int Count
		{
			get
			{
				return iLen;
			}
		}
		
		/// <summary>
		/// ctor
		/// </summary>
		/// <param name="mask">bit mask</param>
		/// <param name="len">mask len. [1..64]</param>
		public BitMask(long mask, int len)
		{
			if(len < 1 || len > 64) throw new ArgumentOutOfRangeException("len", len, "len must be [1..64]");
			if(mask < 0 && len < 64) 
				throw new ArithmeticException("Signed mask specified without use of sign bit");
			if((mask & ~((1L << len) - 1)) != 0) 
				throw new ArgumentException("Bits outside len must be 0", "mask");
			
			lMask = mask;
			iLen = len;
		}

		/// <summary>
		/// get cross of 2 masks
		/// </summary>
		/// <param name="mask">1st mask</param>
		/// <param name="val">2nd mask</param>
		/// <returns>cross value</returns>
		public static int GetValue(int mask, int val)
		{
			return mask & val;
		}

		/// <summary>
		/// get cross of 2 masks
		/// </summary>
		/// <param name="mask">1st mask</param>
		/// <param name="val">2nd mask</param>
		/// <returns>cross value</returns>
		public static long GetValue(long mask, long val)
		{
			return mask & val;
		}

		/// <summary>
		/// add bits to mask
		/// </summary>
		/// <param name="mask">destination mask</param>
		/// <param name="val">source mask</param>
		public static void SetValue(ref int mask, int val)
		{
			mask |= val;
		}

		/// <summary>
		/// add bits to mask
		/// </summary>
		/// <param name="mask">destination mask</param>
		/// <param name="val">source mask</param>
		public static void SetValue(ref long mask, long val)
		{
			mask |= val;
		}

		/// <summary>
		/// remove bits from mask
		/// </summary>
		/// <param name="mask">destination mask</param>
		/// <param name="val">source mask</param>
		public static void UnsetValue(ref int mask, int val)
		{
			mask &= ~val;
		}

		/// <summary>
		/// remove bits from mask
		/// </summary>
		/// <param name="mask">destination mask</param>
		/// <param name="val">source mask</param>
		public static void UnsetValue(ref long mask, long val)
		{
			mask &= ~val;
		}

		/// <summary>
		/// get bit in mask by pos
		/// </summary>
		/// <param name="mask">mask to be researched</param>
		/// <param name="pos">0-based pos of bit</param>
		/// <returns>true if set, otherwise - false</returns>
		public static bool GetBit(int mask, int pos)
		{
			Debug.Assert(pos < 32, "pos outside mask");
			return GetValue(mask, 1 << pos) != 0;
		}

		/// <summary>
		/// get bit in mask by pos
		/// </summary>
		/// <param name="mask">mask to be researched</param>
		/// <param name="pos">0-based pos of bit</param>
		/// <returns>true if set, otherwise - false</returns>
		public static bool GetBit(long mask, int pos)
		{
			Debug.Assert(pos < 64, "pos outside mask");
			return GetValue(mask, 1 << pos) != 0;
		}

		/// <summary>
		/// set bit in mask by pos
		/// </summary>
		/// <param name="mask">mask to be modified</param>
		/// <param name="pos">0-based pos of bit</param>
		/// <param name="set">set/unset</param>
		public static void SetBit(ref int mask, int pos, bool set)
		{
			Debug.Assert(pos < 32, "pos outside mask");
			if(set) SetValue(ref mask, 1 << pos);
			else UnsetValue(ref mask, 1 << pos);
		}

		/// <summary>
		/// set bit in mask by pos
		/// </summary>
		/// <param name="mask">mask to be modified</param>
		/// <param name="pos">0-based pos of bit</param>
		/// <param name="set">set/unset</param>
		public static void SetBit(ref long mask, int pos, bool set)
		{
			Debug.Assert(pos < 64, "pos outside mask");
			if(set) SetValue(ref mask, 1 << pos);
			else UnsetValue(ref mask, 1 << pos);
		}

		/// <summary>
		/// set bit in mask by pos
		/// </summary>
		/// <param name="mask">mask to be modified</param>
		/// <param name="pos">0-based pos of bit</param>
		/// <param name="set">set/unset</param>
		public static void SetBit(ref byte mask, int pos, bool set)
		{
			Debug.Assert(pos < 8, "pos outside mask");
			int msk = mask;
			SetBit(ref msk, pos, set);
			mask = (byte)msk;
		}
		
		public static implicit operator int(BitMask mask)
		{
			Debug.Assert(mask.Count < 33, "mask len not fit in uint");
			return (int)mask.lMask;
		}
		
		public static implicit operator long(BitMask mask)
		{
			return mask.lMask;
		}
		
		public bool this[int i]
		{
			get
			{
				if(i < 0 || i >= iLen) throw new ArgumentOutOfRangeException("Index must be [1..Count]");
				return GetBit(lMask, i);
			}
			set
			{
				if(i < 0 || i >= iLen) throw new ArgumentOutOfRangeException("Index must be [1..Count]");
				SetBit(ref lMask, i, value);
			}
		}
	}
}