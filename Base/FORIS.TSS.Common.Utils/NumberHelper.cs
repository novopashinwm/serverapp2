﻿using System;
using System.Globalization;

namespace FORIS.TSS.Common
{
	/// <summary> Вспомогательные методы для работы с числами </summary>
	public static class NumberHelper
	{
		/// <summary> Общая для всей системы информация для разбора чисел с плавающей точкой </summary>
		public static readonly NumberFormatInfo FormatInfo = new NumberFormatInfo { NumberDecimalSeparator = "." };
		/// <summary > Сравнивает два числа с плавающей точкой на совпадение в пределах указанной точности </summary>
		public static bool AreEqual(double x, double y, double error)
		{
			return Math.Abs(x - y) < error;
		}
		#region LE
		/// <summary>Конвертирует число в массив байт c little-endian порядком байт</summary>
		public static byte[] ToLE(this UInt16 value)
		{
			var result = new byte[sizeof(UInt16)];
			for (var i = 0; i < result.Length && value != 0; i++)
				result[i] = (byte)((UInt16)(value >> i * 8) & 0xFF);
			return result;
		}
		public static byte[] ToLE(this UInt32 value)
		{
			var result = new byte[sizeof(UInt32)];
			for (var i = 0; i < result.Length && value != 0; i++)
				result[i] = (byte)((UInt32)(value >> i * 8) & 0xFF);
			return result;
		}
		public static byte[] ToLE(this UInt64 value)
		{
			var result = new byte[sizeof(UInt64)];
			for (var i = 0; i < result.Length && value != 0; i++)
				result[i] = (byte)((UInt64)(value >> i * 8) & 0xFF);
			return result;
		}
		/// <summary> Конвертирует массив байт c little-endian порядком байт в число </summary>
		public static UInt16 FromLE2UInt16(this byte[] data, int offset, int length)
		{
			var itemSize = sizeof(UInt16);
			if (length < 1 || itemSize < length)
				throw new ArgumentOutOfRangeException(nameof(length), length, $@"Value must be between 1 and {itemSize}");
			if (data.Length < (offset + length))
				throw new ArgumentOutOfRangeException("data.Length", data.Length, $@"Value must be between 1 and {offset + length}");

			UInt16 itemData = 0;
			for (var i = 0; i < length; i++)
				itemData = (UInt16)(itemData | ((UInt16)data[i + offset]) << (i * 8));
			return itemData;
		}
		public static UInt32 FromLE2UInt32(this byte[] data, int offset, int length)
		{
			var itemSize = sizeof(UInt32);
			if (length < 1 || itemSize < length)
				throw new ArgumentOutOfRangeException(nameof(length), length, $@"Value must be between 1 and {itemSize}");
			if (data.Length < (offset + length))
				throw new ArgumentOutOfRangeException("data.Length", data.Length, $@"Value must be between 1 and {offset + length}");

			UInt32 itemData = 0;
			for (var i = 0; i < length; i++)
				itemData = itemData | ((UInt32)data[i + offset]) << (i * 8);
			return itemData;
		}
		public static UInt64 FromLE2UInt64(this byte[] data, int offset, int length)
		{
			var itemSize = sizeof(UInt64);
			if (length < 1 || itemSize < length)
				throw new ArgumentOutOfRangeException(nameof(length), length, $@"Value must be between 1 and {itemSize}");
			if (data.Length < (offset + length))
				throw new ArgumentOutOfRangeException("data.Length", data.Length, $@"Value must be between 1 and {offset + length}");

			UInt64 itemData = 0;
			for (var i = 0; i < length; i++)
				itemData = itemData | ((UInt64)data[i + offset]) << (i * 8);
			return itemData;
		}
		#endregion LE
		#region BE
		/// <summary> Конвертирует число в массив байт с big-endian порядком байт </summary>
		public static byte[] ToBE(this UInt16 value)
		{
			var result = new byte[sizeof(UInt16)];
			for (var i = result.Length - 1; i >= 0; i--)
				result[result.Length - 1 - i] = (byte)((UInt16)(value >> (i * 8)) & 0xFF);
			return result;
		}
		public static byte[] ToBE(this UInt32 value)
		{
			var result = new byte[sizeof(UInt32)];
			for (var i = result.Length - 1; i >= 0; i--)
				result[result.Length - 1 - i] = (byte)((UInt32)(value >> i * 8) & 0xFF);
			return result;
		}
		public static byte[] ToBE(this UInt64 value)
		{
			var result = new byte[sizeof(UInt64)];
			for (var i = result.Length - 1; i >= 0; i--)
				result[result.Length - 1 - i] = (byte)((UInt64)(value >> i * 8) & 0xFF);
			return result;
		}
		/// <summary> Конвертирует массив байт c big-endian порядком байт в число </summary>
		public static UInt16 FromBE2UInt16(this byte[] data, int offset, int length)
		{
			var itemSize = sizeof(UInt16);
			if (length < 1 || itemSize < length)
				throw new ArgumentOutOfRangeException(nameof(length), length, $@"Value must be between 1 and {itemSize}");
			if (data.Length < (offset + length))
				throw new ArgumentOutOfRangeException("data.Length", data.Length, $@"Value must be between 1 and {offset + length}");

			UInt16 itemData = 0;
			for (int i = 0; i < length; i++)
				itemData = (UInt16)(itemData << 8 | ((UInt16)data[i + offset]));
			return itemData;
		}
		public static UInt32 FromBE2UInt32(this byte[] data, int offset, int length)
		{
			var itemSize = sizeof(UInt32);
			if (length < 1 || itemSize < length)
				throw new ArgumentOutOfRangeException(nameof(length), length, $@"Value must be between 1 and {itemSize}");
			if (data.Length < (offset + length))
				throw new ArgumentOutOfRangeException("data.Length", data.Length, $@"Value must be between 1 and {offset + length}");

			UInt32 itemData = 0;
			for (int i = 0; i < length; i++)
				itemData = (UInt32)(itemData << 8 | ((UInt32)data[i + offset]));
			return itemData;
		}
		public static UInt64 FromBE2UInt64(this byte[] data, int offset, int length)
		{
			var itemSize = sizeof(UInt64);
			if (length < 1 || itemSize < length)
				throw new ArgumentOutOfRangeException(nameof(length), length, $@"Value must be between 1 and {itemSize}");
			if (data.Length < (offset + length))
				throw new ArgumentOutOfRangeException("data.Length", data.Length, $@"Value must be between 1 and {offset + length}");

			UInt64 itemData = 0;
			for (int i = 0; i < length; i++)
				itemData = (UInt64)(itemData << 8 | ((UInt64)data[i + offset]));
			return itemData;
		}
		#endregion BE
		#region BCD
		public static UInt64 FromBcd(UInt64 src, int count)
		{
			if (count < 1 || 8 < count)
				throw new ArgumentOutOfRangeException(nameof(count), count, @"Value must be between 1 and 8");

			ulong result = 0;
			ulong pos10 = 1;
			for (var i = 0; i != count; ++i)
			{
				var b = (src >> (i * 8)) & 0xff;
				result += ((b >> 4) * 10 + (b & 0xf)) * pos10;
				pos10 *= 100;
			}
			return result;
		}
		public static UInt16 FromBcd2UInt16(this byte[] data, int offset, int length)
		{
			var itemSize = sizeof(UInt16);
			if (length < 1 || itemSize < length)
				throw new ArgumentOutOfRangeException(nameof(length), length, $@"Value must be between 1 and {itemSize}");
			if (data.Length < (offset + length))
				throw new ArgumentOutOfRangeException("data.Length", data.Length, $@"Value must be between 1 and {offset + length}");

			UInt16 itemData = 0;
			for (int i = 0; i < length; i++)
			{
				var bcd = data[i + offset];
				itemData *= (UInt16)100;
				itemData += (UInt16)(10 * (bcd >> 4));
				itemData += (UInt16)(bcd & 0x0F);
			}
			return itemData;
		}
		public static UInt32 FromBcd2UInt32(this byte[] data, int offset, int length)
		{
			var itemSize = sizeof(UInt32);
			if (length < 1 || itemSize < length)
				throw new ArgumentOutOfRangeException(nameof(length), length, $@"Value must be between 1 and {itemSize}");
			if (data.Length < (offset + length))
				throw new ArgumentOutOfRangeException("data.Length", data.Length, $@"Value must be between 1 and {offset + length}");

			UInt32 itemData = 0;
			for (int i = 0; i < length; i++)
			{
				var bcd = data[i + offset];
				itemData *= (UInt32)100;
				itemData += (UInt32)(10 * (bcd >> 4));
				itemData += (UInt32)(bcd & 0x0F);
			}
			return itemData;
		}
		public static UInt64 FromBcd2UInt64(this byte[] data, int offset, int length)
		{
			var itemSize = sizeof(UInt64);
			if (length < 1 || itemSize < length)
				throw new ArgumentOutOfRangeException(nameof(length), length, $@"Value must be between 1 and {itemSize}");
			if (data.Length < (offset + length))
				throw new ArgumentOutOfRangeException("data.Length", data.Length, $@"Value must be between 1 and {offset + length}");

			UInt64 itemData = 0;
			for (int i = 0; i < length; i++)
			{
				var bcd = data[i + offset];
				itemData *= (UInt64)100;
				itemData += (UInt64)(10 * (bcd >> 4));
				itemData += (UInt64)(bcd & 0x0F);
			}
			return itemData;
		}
		#endregion BCD
		/// <summary> Парсинг строки с double, для различных вариантов культур </summary>
		public static bool TryParse(string s, out double result)
		{
			var isParse = false;
			isParse = double.TryParse(s, NumberStyles.Float, NumberHelper.FormatInfo, out result);
			if (isParse)
				return isParse;
			isParse = double.TryParse(s, NumberStyles.Float, CultureInfo.GetCultureInfo("ru-RU"), out result);
			if (isParse)
				return isParse;
			isParse = double.TryParse(s, NumberStyles.Float, CultureInfo.GetCultureInfo("en-US"), out result);
			if (isParse)
				return isParse;
			isParse = double.TryParse(s, NumberStyles.Float, CultureInfo.InvariantCulture, out result);
			if (isParse)
				return isParse;
			return false;
		}
	}
}