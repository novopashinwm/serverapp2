﻿namespace FORIS.TSS.BusinessLogic
{
	public enum IntervalAccuracy
	{
		ToWeek   = 0,
		ToDay    = 1,
		ToHour   = 2,
		ToMinute = 3,
		ToSecond = 4,
	}
}