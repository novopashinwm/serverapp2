﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using FORIS.TSS.Common.Helpers;

namespace FORIS.TSS.Common
{
	/// <summary> Helper class for creating application domains </summary>
	public class DomainHelper
	{
		/// <summary> Creates new domain </summary>
		/// <param name="nameOfNewDomain">Name of new domain</param>
		/// <param name="nameOfConfigFile">Name of configuration file, which will be default configuration file for domain</param>
		/// <param name="nameOfAssemblyFile">Name of assembly to load first</param>
		/// <param name="nameOfType">Name of type which supports IEntryPoint</param>
		/// <param name="appDomain">Created app domain object</param>
		/// <param name="applicationBase">Where to search dlls, may be null (when it is null, then current application base is used)</param>
		/// <param name="additionalPaths">additional paths to search (only for debug mode)</param>
		/// <returns></returns>
		public static object CreateDomainAndGetEntryPoint(
			string nameOfNewDomain,
			string nameOfConfigFile,
			string nameOfAssemblyFile,
			string nameOfType,
			out AppDomain appDomain,
			string applicationBase,
			params string[] additionalPaths)
		{

			AppDomainSetup setup = new AppDomainSetup();

			if (applicationBase != null)
			{
				setup.ApplicationBase = applicationBase;
			}
			else
			{
				setup.ApplicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
			}

			// Copy settings from current domain
			setup.ApplicationName          = AppDomain.CurrentDomain.SetupInformation.ApplicationName;
			setup.CachePath                = AppDomain.CurrentDomain.SetupInformation.CachePath;
			setup.DisallowBindingRedirects = AppDomain.CurrentDomain.SetupInformation.DisallowBindingRedirects;
			setup.DisallowCodeDownload     = AppDomain.CurrentDomain.SetupInformation.DisallowCodeDownload;
			setup.DisallowPublisherPolicy  = AppDomain.CurrentDomain.SetupInformation.DisallowPublisherPolicy;
			setup.DynamicBase              = AppDomain.CurrentDomain.SetupInformation.DynamicBase;
			setup.LicenseFile              = AppDomain.CurrentDomain.SetupInformation.LicenseFile;
			setup.LoaderOptimization       = AppDomain.CurrentDomain.SetupInformation.LoaderOptimization;
			setup.PrivateBinPath           = AppDomain.CurrentDomain.SetupInformation.PrivateBinPath;
			setup.PrivateBinPathProbe      = AppDomain.CurrentDomain.SetupInformation.PrivateBinPathProbe;
			setup.ShadowCopyDirectories    = AppDomain.CurrentDomain.SetupInformation.ShadowCopyDirectories;
			setup.ShadowCopyFiles          = AppDomain.CurrentDomain.SetupInformation.ShadowCopyFiles;

#if DEBUG
			StringBuilder sb = new StringBuilder(setup.PrivateBinPath);
			foreach (string str in additionalPaths)
			{
				sb.Append(";");
				sb.Append(str);
			}
			setup.PrivateBinPath = sb.ToString();
#endif
			// Calculate name of configuration file
			setup.ConfigurationFile = GetFileName(nameOfConfigFile, setup.ApplicationBase, setup.PrivateBinPath);

			// Create domain
			appDomain = AppDomain.CreateDomain(nameOfNewDomain, AppDomain.CurrentDomain.Evidence, setup);

			// configure server side
			object res = appDomain.CreateInstanceAndUnwrap(nameOfAssemblyFile, nameOfType);
			return res;
		}

		/// <summary>
		/// Searches for configuration file within application base and privateBinPath
		/// </summary>
		/// <param name="nameOfFile"></param>
		/// <param name="applicationBase"></param>
		/// <param name="privateBinPath"></param>
		/// <returns></returns>
		public static string GetFileName(string nameOfFile, string applicationBase, string privateBinPath)
		{
			Trace.TraceInformation("GetFileName for '{0}'",    nameOfFile);
			Trace.TraceInformation("applicationBase is '{0}'", applicationBase);
			Trace.TraceInformation("privateBinPath is '{0}'",  privateBinPath);

			FileInfo fi = new FileInfo(nameOfFile);
			if (fi.Exists)
			{
				return fi.FullName;
			}

			FileInfo fi0 = new FileInfo(applicationBase + nameOfFile);
			if (fi0.Exists)
			{
				return fi0.FullName;
			}

			if (privateBinPath != null)
			{
				string[] pathes = privateBinPath.Split(';');
				foreach (string path in pathes)
				{
					FileInfo fi2 = new FileInfo(applicationBase + path + nameOfFile);
					if (fi2.Exists)
					{
						return fi2.FullName;
					}
				}
			}

			throw new FileNotFoundException("during domain configuration", nameOfFile);
		}

		public static void UnloadDomainOrStopProcess(AppDomain appDomain)
		{
			if (appDomain == null)
				return;
			try
			{
				$"Unloading domain"
					.CallTraceInformation();
				for (int i = 0; i < 2; ++i)
				{
					$"Attempt No {i}"
						.CallTraceInformation();
					try
					{
						if (i != 0)
						{
							Thread.Sleep(1);
							GC.Collect();
							GC.WaitForPendingFinalizers();
							GC.Collect();
						}

						AppDomain.Unload(appDomain);
						$"Domain was unloaded"
							.CallTraceInformation();

						GC.Collect();
						GC.WaitForPendingFinalizers();
						GC.Collect();
						$"Garbage was collected"
							.CallTraceInformation();
						Trace.Flush();
						return;
					}
					catch (CannotUnloadAppDomainException ex)
					{
						default(string)
							.WithException(ex, true)
							.CallTraceError();
					}
					catch (Exception ex)
					{
						default(string)
							.WithException(ex, true)
							.CallTraceError();
						continue;
					}
				}
				$"Domain unloading failed. Stopping the entire process..."
					.CallTraceWarning();
				Trace.Flush();
				Environment.Exit(0);
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();
				throw;
			}
			finally
			{
				$"Exit"
					.CallTraceInformation();
			}
		}
	}
}