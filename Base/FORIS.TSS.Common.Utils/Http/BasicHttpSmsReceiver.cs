﻿using System.Net;
using System.Text;

namespace FORIS.TSS.Common.Http
{
	public abstract class BasicHttpSmsReceiver : BasicHttpReceiver
	{
		protected BasicHttpSmsReceiver(string httpPrefix) : base(httpPrefix)
		{
		}
		protected override Encoding GetEncoding(HttpListenerRequest request)
		{
			return Encoding.Unicode.Equals(request.ContentEncoding) ? Encoding.BigEndianUnicode : request.ContentEncoding;
		}
		protected override HttpResponse Process(HttpListenerRequest request, string entityBody)
		{
			var answer = ProcessSmsHttpRequest(request, entityBody) ?? "OK";

			return new HttpResponse {Text = answer};
		}
		protected abstract string ProcessSmsHttpRequest(HttpListenerRequest request, string messageText);
	}
}