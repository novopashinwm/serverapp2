﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FORIS.TSS.Common.Http
{
	public abstract class BasicHttpReceiver
	{
		public event Action<object, EventArgs> RequestStarted;
		public event Action<object, EventArgs> RequestFinished;
		public event Action<object, EventArgs> BeforeStart;
		public event Action<object, EventArgs> AfterStop;

		private readonly HttpListener _listener;
		private readonly Thread _thread;
		private readonly string[] _httpPrefixes;

		protected BasicHttpReceiver(params string[] httpPrefixes)
		{
			_listener = new HttpListener();
			_httpPrefixes = httpPrefixes;
			foreach (var prefix in httpPrefixes)
				_listener.Prefixes.Add(prefix);

			_thread = new Thread(ListenToHttp) { IsBackground = true };
		}

		public void Start()
		{
			BeforeStart?.Invoke(this, new EventArgs());

			_thread.Start();
		}

		private void ListenToHttp()
		{
			try
			{
				_listener.Start();
			}
			catch (Exception e)
			{
				Trace.TraceError("{0}: unable to listen to {1}: {2}",
					this, string.Join(", ", _httpPrefixes), e);
				return;
			}

			try
			{
				IAsyncResult result = null;
				do
				{
					if (_listener.IsListening)
					{
						result = _listener.BeginGetContext(Callback, _listener);
					}
				} while (result != null && result.AsyncWaitHandle.WaitOne());
			}
			catch (HttpListenerException)
			{
				// Вроде не должно бросаться, но может при вызове Stop
				Trace.TraceInformation("{0}: _listener.BeginGetContext invoked with IsListening = false", this);
			}
			catch (ThreadAbortException)
			{
				Trace.TraceInformation("{0}: thread was aborted", this);
			}
			catch (ThreadInterruptedException)
			{
				Trace.TraceInformation("{0}: thread was interrupted", this);
			}
			catch (Exception ex)
			{
				Trace.TraceError("{0} unhandled error :{1}", this, ex);
			}
		}

		public void Stop()
		{
			_listener.Stop();

			if (_thread.IsAlive)
			{
				Trace.TraceInformation("{0}: Waiting for listener thread", this);
				_thread.Interrupt();
				_thread.Join();
			}

			AfterStop?.Invoke(this, new EventArgs());
		}

		public bool IsListening
		{
			get { return _listener.IsListening; }
		}

		private void Callback(IAsyncResult result)
		{
			OnRequestStarted();

			var listener = (HttpListener) result.AsyncState;
			if (!listener.IsListening)
				return;

			HttpListenerContext context;
			try
			{
				context = listener.EndGetContext(result);
			}
			catch (HttpListenerException)
			{
				// Вроде не должно бросаться, но может при вызове Stop
				Trace.TraceInformation("{0}: _listener.EndGetContext invoked with IsListening = false", this);
				return;
			}
			catch (ThreadAbortException)
			{
				Trace.TraceInformation("{0}: thread was aborted", this);
				return;
			}
			catch (ThreadInterruptedException)
			{
				Trace.TraceInformation("{0}: thread was interrupted", this);
				return;
			}
			catch (Exception e)
			{
				Trace.TraceError("{0}: unhandled error {1}", this, e);
				return;
			}

			if (context.Request.HasEntityBody)
			{
				var streamReader = new StreamReader(context.Request.InputStream, GetEncoding(context.Request));
				streamReader.ReadToEndAsync().ContinueWith(
					delegate(Task<string> task)
					{
						string entityText;
						try
						{
							entityText = task.Result;
						}
						catch (Exception e)
						{
							Trace.TraceError("{0}.Callback - async reading: {1}", this, e);
							OnRequestFinished();
							return;
						}

						try
						{
							Process(context, entityText);
						}
						catch (Exception e)
						{
							Trace.TraceError("{0}.Callback - processing after async reading: {1}", this, e);
							OnRequestFinished();
						}
					});
			}
			else
			{
				try
				{
					Process(context, null);
				}
				catch (Exception e)
				{
					Trace.TraceError("{0}.Callback: {1}", this, e);
				}

				OnRequestFinished();
			}
		}

		private void OnRequestStarted()
		{
			RequestStarted?.Invoke(this, new EventArgs());
		}

		private void OnRequestFinished()
		{
			RequestFinished?.Invoke(this, new EventArgs());
		}

		private void Process(HttpListenerContext context, string entityBody)
		{
			HttpResponse processResult;
			try
			{
				processResult = Process(context.Request, entityBody);
			}
			catch (Exception e)
			{
				Trace.TraceError("{0}.Process: {1}", this, e);
				processResult = HttpResponse.InternalServerError();
			}

			SendResponse(context.Response, processResult);
		}

		void SendResponse(HttpListenerResponse httpResponse, HttpResponse processResponse)
		{
			try
			{
				if (processResponse == null)
				{
					httpResponse.StatusCode = 500;
					try
					{
						httpResponse.Close();
					}
					catch (Exception e)
					{
						Trace.TraceError("{0}.SendResponse without entity: {1}", this, e);
					}
					return;
				}

				httpResponse.StatusCode = processResponse.StatusCode;
				httpResponse.StatusDescription = processResponse.StatusDescription;

				if (processResponse.Expires != null)
					httpResponse.AddHeader(HttpRequestHeader.Expires.ToString(), processResponse.Expires.Value.ToString("r"));

				if (processResponse.AdditionalHeaders != null)
				{
					foreach (var pair in processResponse.AdditionalHeaders)
						httpResponse.AddHeader(pair.Key, pair.Value);
				}

				if (processResponse.Type != DataType.Unspecified)
					httpResponse.ContentType = HttpHelper.GetContentType(processResponse.Type);

				httpResponse.StatusCode = processResponse.StatusCode;
				httpResponse.StatusDescription = processResponse.StatusDescription ?? string.Empty;

				if (string.IsNullOrWhiteSpace(processResponse.Text))
				{
					try
					{
						httpResponse.Close();
					}
					catch (Exception e)
					{
						Trace.TraceError("{0}.SendResponse with entity: {1}", this, e);
					}
					return;
				}

				if (processResponse.Type != DataType.Unspecified)
					httpResponse.ContentType = HttpHelper.GetContentType(processResponse.Type);

				var contentEncoding = httpResponse.ContentEncoding ?? Encoding.ASCII;

				try
				{
					httpResponse.Close(contentEncoding.GetBytes(processResponse.Text), false);
				}
				catch (Exception e)
				{
					Trace.TraceError("{0}.SendResponse with entity: {1}", this, e);
				}
			}
			finally
			{
				OnRequestFinished();
			}
		}

		protected abstract HttpResponse Process(HttpListenerRequest request, string entityBody);

		protected virtual Encoding GetEncoding(HttpListenerRequest request)
		{
			return request.ContentEncoding;
		}
	}
}