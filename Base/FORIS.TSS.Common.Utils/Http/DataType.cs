﻿namespace FORIS.TSS.Common.Http
{
	/// <summary> Тип данных для запросов и ответов WEB </summary>
	public enum DataType
	{
		/// <summary>Не указано</summary>
		Unspecified,
		/// <summary>JavaScript Object Notation</summary>
		Json,
		/// <summary>Xml Object Notation</summary>
		Xml,
		/// <summary> Пустой ответ, результат вызова в заголовке HTTP-ответа </summary>
		Empty
	}
}