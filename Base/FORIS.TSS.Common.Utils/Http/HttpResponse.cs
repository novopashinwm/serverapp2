﻿using System;
using System.Collections.Generic;

namespace FORIS.TSS.Common.Http
{
	/// <summary> Информация об ответе на входящий http-запрос </summary>
	public class HttpResponse
	{
		public HttpResponse()
		{
			StatusCode        = 200;
			StatusDescription = "200 OK";
			Text              = string.Empty;
		}

		/// <summary>Текст ответа</summary>
		public string Text { get; set; }

		/// <summary>Код ответа, например 404</summary>
		public int StatusCode { get; set; }

		/// <summary>Заголовок ответа, например 404 Not Found</summary>
		public string StatusDescription { get; set; }

		/// <summary> Значение заголовка Expires </summary>
		public DateTime? Expires { get; set; }

		public DataType Type { get; set; }

		public List<KeyValuePair<string, string>> AdditionalHeaders;

		public static HttpResponse NotFound()
		{
			return new HttpResponse { StatusCode = 404, StatusDescription = "404 Not found" };
		}

		public static HttpResponse BadRequest(string text = "")
		{
			return new HttpResponse { StatusCode = 400, StatusDescription = "400 Bad request", Text = text };
		}

		public static HttpResponse InternalServerError()
		{
			return new HttpResponse { StatusCode = 500, StatusDescription = "500 Internal server error", Text = "Internal server error" };
		}

		public static HttpResponse Forbidden()
		{
			return new HttpResponse { StatusCode = 403, StatusDescription = "Forbidden" };
		}

		public static HttpResponse Ok()
		{
			return new HttpResponse();
		}
	}
}