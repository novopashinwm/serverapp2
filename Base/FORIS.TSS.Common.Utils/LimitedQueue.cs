﻿using System;
using System.Collections.Generic;

namespace FORIS.TSS.Common
{
    public class LimitedQueue<T>
    {
        private readonly T[] _storage;
        private int _index;
        private int _count;

        public LimitedQueue(int capacity)
        {
            if (capacity < 1)
                throw new ArgumentOutOfRangeException("capacity", capacity, @"Value must be greater than zero");

            _storage = new T[capacity];
        }

        public void Add(T t)
        {
            _storage[(_index + _count) % _storage.Length] = t;
            if (_count == _storage.Length)
                _index = (_index + 1)%_storage.Length;
            else
                ++_count;
        }

        public int Count
        {
            get { return _count; }
        }

        public T Last(int i = 0)
        {
            if (_count <= i)
                throw new ArgumentOutOfRangeException("i", i, @"Index is out of range");

            var effectiveIndex = GetEffectiveIndexOfLastItem(i);

            return _storage[effectiveIndex];
        }

        private int GetEffectiveIndexOfLastItem(int i)
        {
            var effectiveIndex = (_index + _count - 1 - i)%_storage.Length;
            return effectiveIndex;
        }

        public IEnumerable<T> Items
        {
            get
            {
                for (var i = 0; i != _count; ++i)
                    yield return Last(i);
            }
        }

        public void Update(T t)
        {
            if (_count == 0)
                throw new InvalidOperationException("Collection is empty");

            var effectiveIndex = GetEffectiveIndexOfLastItem(0);

            _storage[effectiveIndex] = t;
        }
    }
}
