﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Xml;

namespace FORIS.TSS.Common
{
	public class Stopwatcher : IDisposable
	{
		private readonly string _tag;
		private readonly Func<TimeSpan, string> _getAdditionalInfo;
		private readonly Stopwatch _sw;
		private readonly TimeSpan _threshold;
		private readonly static TimeSpan DefaultThreshold;
		static Stopwatcher()
		{
			DefaultThreshold = GetDefaultThresholdFromConfig();
		}
		private static TimeSpan GetDefaultThresholdFromConfig()
		{
			var config = ConfigurationManager.AppSettings["Stopwatcher.DefaultThreshold"];

			if (!string.IsNullOrWhiteSpace(config))
			{
				try
				{
					return XmlConvert.ToTimeSpan(config);
				}
				catch (Exception e)
				{
					Trace.TraceError(
						"Unable to parse AppSetting Stopwatcher.DefaultThreshold {0}: {1}",
						config, e);
				}
			}

			return TimeSpan.FromMilliseconds(250);
		}
		public static Stopwatcher GetElapsedLogger(string tag, Func<TimeSpan, string> getAdditionalInfo = null, TimeSpan? threshold = null)
		{
			return new Stopwatcher(tag, getAdditionalInfo, threshold ?? TimeSpan.FromTicks(1));
		}
		public Stopwatcher(string tag, Func<TimeSpan, string> getAdditionalInfo = null, TimeSpan? threshold = null)
		{
			_tag               = tag;
			_getAdditionalInfo = getAdditionalInfo;
			_threshold         = threshold ?? DefaultThreshold;
			if (_threshold.Equals(TimeSpan.Zero))
				Trace.TraceInformation("{0}...", _tag);
			_sw = new Stopwatch();
			_sw.Start();
		}
		public void Dispose()
		{
			_sw.Stop();
			if (_sw.Elapsed < _threshold)
				return;

			if (null != _getAdditionalInfo)
			{
				var additionalInfo = _getAdditionalInfo(_sw.Elapsed);
				if (!string.IsNullOrWhiteSpace(additionalInfo))
					Trace.TraceInformation("{0} duration time {1} {2}", _tag, _sw.Elapsed, additionalInfo);
			}
			else
				Trace.TraceInformation("{0} duration time {1}", _tag, _sw.Elapsed);
		}
	}
}