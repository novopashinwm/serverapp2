﻿using System.Collections.Generic;

namespace FORIS.TSS.BusinessLogic.Filtering
{
	public interface IFilter<T>
	{
		IEnumerable<T> Filter(IEnumerable<T> input);
	}
}