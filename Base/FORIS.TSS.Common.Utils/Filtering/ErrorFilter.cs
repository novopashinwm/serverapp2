﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.Filtering;

namespace FORIS.TSS.Common.Filtering
{
	/// <summary> Фильтр для удаления из входной последовательности записей с заведомо ложными значениями </summary>
	/// <typeparam name="T"></typeparam>
	public class ErrorFilter<T> : IFilter<T> where T : ILogTimeContainer
	{
		private readonly int              _dLogTime;
		private readonly decimal          _dValue;
		private readonly Func<T, decimal> _getValue;
		private readonly Queue<T>         _next = new Queue<T>();
		private readonly Queue<T>         _prev = new Queue<T>();
		private decimal                   _nextSum;
		private decimal                   _prevSum;

		public ErrorFilter(int dLogTime, decimal dValue, Func<T, decimal> getValue)
		{
			if (dLogTime <= 0)
				throw new ArgumentOutOfRangeException(nameof(dLogTime), dLogTime, "Value should be positive");
			if (dValue <= 0)
				throw new ArgumentOutOfRangeException(nameof(dLogTime), dLogTime, "Value should be positive");
			if (getValue == null)
				throw new ArgumentNullException(nameof(getValue));

			_dLogTime = dLogTime;
			_dValue   = dValue;
			_getValue = getValue;
		}
		public IEnumerable<T> Filter(IEnumerable<T> input)
		{
			_next.Clear();
			_prev.Clear();
			_nextSum = 0m;
			_prevSum = 0m;

			foreach (var nextItem in input)
			{
				_next.Enqueue(nextItem);
				var nextItemValue = _getValue(nextItem);
				_nextSum += nextItemValue;
				if (_dLogTime < nextItem.GetLogTime() - _next.Peek().GetLogTime())
				{
					if (DequeueFromNext(out T dequeuedItem))
						yield return dequeuedItem;
				}
			}

			while (_next.Count != 0)
			{
				if (DequeueFromNext(out T dequeuedItem))
					yield return dequeuedItem;
			}
		}
		private bool DequeueFromNext(out T t)
		{
			var currentItem = _next.Dequeue();
			var currentValue = _getValue(currentItem);
			_nextSum -= currentValue;

			if (_next.Count != 0 && _prev.Count != 0)
			{
				var avgPrev = _prevSum/_prev.Count;
				var avgNext = _nextSum/_next.Count;

				if (
					(avgPrev <= avgNext && (currentValue < avgPrev - _dValue || avgNext + _dValue < currentValue)) ||
					(avgNext <= avgPrev && (currentValue < avgNext - _dValue || avgPrev + _dValue < currentValue))
				)
				{
					t = default(T);
					return false;
				}
			}

			_prev.Enqueue(currentItem);
			_prevSum += currentValue;

			if (_dLogTime < currentItem.GetLogTime() - _prev.Peek().GetLogTime())
			{
				var dequeuedFromPrev = _prev.Dequeue();
				_prevSum -= _getValue(dequeuedFromPrev);
			}

			t = currentItem;
			return true;
		}
	}
}