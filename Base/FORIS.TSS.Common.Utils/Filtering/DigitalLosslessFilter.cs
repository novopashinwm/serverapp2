﻿using System;
using System.Collections.Generic;
using TArgument = System.Int32;

// ReSharper disable once CheckNamespace
namespace FORIS.TSS.BusinessLogic.Filtering
{
	public class DigitalLosslessFilter<TItem, TValue> : IFilter<TItem> 
		where TItem: struct
		where TValue: struct
	{
		private readonly Func<TItem, TArgument> _getArgument;
		private readonly Func<TItem, TValue>    _getValue;
		private readonly int                    _maxArgumentDelay;

		public DigitalLosslessFilter(Func<TItem, TArgument> getArgument, Func<TItem, TValue> getValue, TArgument maxArgumentDelay)
		{
			if (getArgument == null)
				throw new ArgumentNullException(nameof(getArgument));
			if (getValue == null)
				throw new ArgumentNullException(nameof(getValue));

			_getArgument      = getArgument;
			_getValue         = getValue;
			_maxArgumentDelay = maxArgumentDelay;
		}

		#region IFilter Members

		TItem? _prev;
		TItem? _prevYielded;

		private TItem PrepareToYield(TItem item)
		{
			_prevYielded = item;
			_prev        = null;
			return item;
		}
		public IEnumerable<TItem> Filter(IEnumerable<TItem> input)
		{
			foreach (var item in input)
			{
				if (_prevYielded == null)
				{
					yield return PrepareToYield(item);
					continue;
				}

				if (!_getValue(_prevYielded.Value).Equals(_getValue(item)))
				{
					yield return PrepareToYield(item);
					continue;
				}

				if (_getArgument(item) - _getArgument(_prevYielded.Value) < _maxArgumentDelay)
				{
					_prev = item;
					continue;
				}

				if (_prev != null && !_getArgument(_prev.Value).Equals(_getArgument(_prevYielded.Value)))
				{
					yield return PrepareToYield(_prev.Value);
					continue;
				}

				yield return PrepareToYield(item);
			}

			if (_prev != null)
				yield return PrepareToYield(_prev.Value);
		}

		#endregion
	}
}