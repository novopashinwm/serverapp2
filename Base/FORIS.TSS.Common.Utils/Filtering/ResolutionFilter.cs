﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic.Filtering;

namespace FORIS.TSS.Common.Filtering
{
	/// <summary>
	/// Прореживает точки, чтобы обеспечить условие: количество точек не более заданного <see cref="_maximumResolution"/>
	/// </summary>
	/// <remarks>
	/// Принцип работы.
	/// Разбивает весь интервал на <see cref="_maximumResolution"/>/4 участков.
	/// На каждом участке находит крайнюю точку слева, крайнюю точку справа, точку с максимальным значением и точку с минимальным значением.
	/// </remarks>
	/// <typeparam name="T"></typeparam>
	public class ResolutionFilter<T> : IFilter<T>
	{
		private readonly Func<T, double> _getArgument;
		private readonly Func<T, double> _getValue;
		private readonly int _maximumResolution;

		private static BinarySearchResult BinarySearch(List<T> list, Func<T, double> diff)
		{
			return list.BinarySearch(
				t =>
					{
						var d = diff(t);
						if (d == 0)
							return 0;
						return d < 0 ? -1 : 1;
					}
				);
		}
		public ResolutionFilter(int maximumResolution, Func<T, double> getArgument, Func<T, double> getValue)
		{
			_maximumResolution = maximumResolution;
			_getArgument       = getArgument;
			_getValue          = getValue;
		}
		public IEnumerable<T> Filter(IEnumerable<T> input)
		{
			var points = input.ToList();

			if (points.Count < _maximumResolution)
				return points;

			var firstArgument = _getArgument(points.First());
			var lastArgument  = _getArgument(points.Last());

			var stepCount = _maximumResolution / 4;
			var step = (lastArgument - firstArgument) / stepCount;

			var result = new List<T>(_maximumResolution);

			for (var stepI = 0; stepI != stepCount; stepI++)
			{
				// Определяем границы шага
				var from = firstArgument + step * stepI;
				var to   = from + step;
				// Ищем точки ближайшие по аргументу к началу шага
				var fromIndices = BinarySearch(points, x => from - _getArgument(x));
				if (!fromIndices.HasTo)
					continue;
				// Заполняем первую точку интервала
				var firstPoint = points[fromIndices.To];

				// Ищем точки ближайшие по аргументу к концу шага
				var toIndices = BinarySearch(points, x => to - _getArgument(x));
				if (!toIndices.HasFrom)
					continue;
				// Заполняем последнюю точку интервала
				var lastPoint = points[toIndices.From];

				// Заполняем минимальную(по значению) и максимальную(по значению) точки, первой точкой
				T minPoint = firstPoint, maxPoint = firstPoint;
				// Проходим по точкам шага и обновляем минимум(по значению) и максимум(по значению)
				for (var pointI = fromIndices.To + 1; pointI <= toIndices.From; pointI++)
				{
					var point = points[pointI];
					var pointValue = _getValue(point);
					if (pointValue < _getValue(minPoint))
						minPoint = point;
					if (_getValue(maxPoint) < pointValue)
						maxPoint = point;
				}
				// Вместо точек шага используем первую, последнюю, минимальную (по значению) и максимальную (по значению)
				var selectedPoints = new List<T> { firstPoint, minPoint, maxPoint, lastPoint };
				selectedPoints.Sort((x,y) => _getArgument(x).CompareTo(_getArgument(y)));

				for (var pointI = 0; pointI != selectedPoints.Count; pointI++)
				{
					// Добавляем в результат, точки первую и остальные не повторяющиеся из firstPoint, minPoint, maxPoint, lastPoint
					if (0 == pointI || _getArgument(selectedPoints[pointI - 1]) != _getArgument(selectedPoints[pointI]))
						// Добавляем только если уже не добавлено (такая ситуация возможна, т.к. используется приблизительный поиск ближайших точек)
						if (0 == result.Count || _getArgument(result[result.Count - 1]) < _getArgument(selectedPoints[pointI]))
							result.Add(selectedPoints[pointI]);
				}
			}

			return result;
		}
	}
}