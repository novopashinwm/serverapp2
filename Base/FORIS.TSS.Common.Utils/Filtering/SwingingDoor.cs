﻿using System;
using System.Collections.Generic;

namespace FORIS.TSS.BusinessLogic.Filtering
{
	public class SwingingDoor<T> : IFilter<T>
	{
		private FilterPoint  _startUpper;
		private FilterPoint  _startLower;
		private double?      _upperTan;
		private double?      _lowerTan;
		private double       _error;
		private FilterPoint? _startPoint;
		private FilterPoint? _lastPoint;
		private FilterPoint  _previousPoint;

		private bool         _lastReturnedItemSet;
		private T            _lastReturnedItem;
		private T            _previousItem;

		private bool         _lastItemSet;
		private T            _lastItem;

		private readonly Func<T, double> _getArgument;
		private readonly Func<T, double> _getValue;
		private readonly Func<T, double> _getError;
		private readonly Func<T, bool>   _isMandatory;
		private double?                  _maxArgumentDelta;

		public double? MaxArgumentDelta
		{
			get { return _maxArgumentDelta; }
			set { _maxArgumentDelta = value; }
		}
		public SwingingDoor(Func<T, double> getArgument, Func<T, double> getValue, Func<T, double> getError)
			: this(getArgument, getValue, getError, null)
		{ }
		public SwingingDoor(Func<T, double> getArgument, Func<T, double> getValue, Func<T, double> getError, Func<T, bool> isMandatory)
		{
			if (getArgument == null)
				throw new ArgumentNullException(nameof(getArgument));
			if (getValue == null)
				throw new ArgumentNullException(nameof(getValue));
			if (getError == null)
				throw new ArgumentNullException(nameof(getError));

			_getArgument = getArgument;
			_getValue    = getValue;
			_getError    = getError;
			_isMandatory = isMandatory;
		}
		public IEnumerable<T> Filter(IEnumerable<T> input)
		{
			if (input == null)
				throw new ArgumentNullException(nameof(input));
			foreach (var item in input)
			{
				if (_isMandatory != null && _isMandatory(item))
				{
					yield return item;
					continue;
				}

				var argument = _getArgument(item);

				if (_lastPoint != null)
				{
					if (argument == _lastPoint.Value.X)
						continue;

					if (argument < _lastPoint.Value.X)
						throw new ArgumentException(@"Argument value must not decrease, argument = " + argument + @" _lastPoint.Value.X = " + _lastPoint.Value.X, nameof(input));

					_previousPoint = _lastPoint.Value;
					_previousItem  = _lastItem;
				}

				_lastItemSet = true;
				_lastItem    = item;
				_lastPoint   = new FilterPoint(argument, _getValue(item));

				if (_startPoint == null)
				{
					AssignStartPoint(item);
					_lastReturnedItem = item;
					_lastReturnedItemSet = true;
					yield return _lastReturnedItem;
					continue;
				}

				if (_maxArgumentDelta == null || argument - _getArgument(_lastReturnedItem) < _maxArgumentDelta)
				{
					var currentUpperTan = _lastPoint.Value.TanFrom(_startUpper);
					var currentLowerTan = _lastPoint.Value.TanFrom(_startLower);

					if (_upperTan == null || _upperTan < currentUpperTan)
						_upperTan = currentUpperTan;

					if (_lowerTan == null || currentLowerTan < _lowerTan)
						_lowerTan = currentLowerTan;

					if (_upperTan < _lowerTan && //Коридор закрыт
						_error < _getError(item)*2) //Показания датчиков не стали сильно точнее
					{
						continue;
					}
				}

				AssignStartPoint(_previousItem);
				_upperTan = _lastPoint.Value.TanFrom(_startUpper);
				_lowerTan = _lastPoint.Value.TanFrom(_startLower);

				_lastReturnedItem = _previousItem;
				_lastReturnedItemSet = true;
				yield return _lastReturnedItem;
			}

			if (_lastItemSet && (!_lastReturnedItemSet || _getArgument(_lastReturnedItem) != _getArgument(_lastItem)))
			{
				_lastReturnedItem = _lastItem;
				_lastReturnedItemSet = true;
				yield return _lastReturnedItem;
			}
		}
		private void AssignStartPoint(T item)
		{
			var x = _getArgument(item);
			var y = _getValue(item);
			_error = _getError(item);

			if (_startPoint != null)
			{
				//Если текущее значение в пределах погрешности совпадает с предыдущим, то считаем текущее значение равным предыдущему
				//Таким образом, на интервалах, где значение не меняется в пределах погрешности, функция выглядит как постоянная
				if (Math.Abs(y - _startPoint.Value.Y) < _error)
					y = _startPoint.Value.Y;
			}
			_startPoint = new FilterPoint(x, y);
			_startUpper = new FilterPoint(x, y + _error);
			_startLower = new FilterPoint(x, y - _error);
		}
	}
}