﻿namespace FORIS.TSS.BusinessLogic.Filtering
{
	public struct FilterPoint
	{
		public readonly double X;
		public readonly double Y;

		public FilterPoint(double x, double y)
		{
			X = x;
			Y = y;
		}

		public double TanFrom(FilterPoint from)
		{
			return (Y - from.Y) / (X - from.X);
		}
	}
}