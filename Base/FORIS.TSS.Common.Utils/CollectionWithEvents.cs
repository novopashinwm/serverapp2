﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using FORIS.TSS.Common.Collections;

namespace FORIS.TSS.Common
{
	public class CollectionWithEvents<TItem> : Collection<TItem>, ICollectionWithEvents<TItem>
	{
		#region Events

		private CollectionChangeEventHandler<TItem> dlgtInserting;
		private CollectionChangeEventHandler<TItem> dlgtInserted;
		private CollectionChangeEventHandler<TItem> dlgtRemoving;
		private CollectionChangeEventHandler<TItem> dlgtRemoved;

		#region ICollectionWithEvents<TItem> Members

		public bool Empty
		{
			get { return Count == 0; }
		}

		public event CollectionChangeEventHandler<TItem> Inserting
		{
			add    { dlgtInserting += value; }
			remove { dlgtInserting -= value; }
		}
		public event CollectionChangeEventHandler<TItem> Inserted
		{
			add    { dlgtInserted += value; }
			remove { dlgtInserted -= value; }
		}
		public event CollectionChangeEventHandler<TItem> Removing
		{
			add    { dlgtRemoving += value; }
			remove { dlgtRemoving -= value; }
		}
		public event CollectionChangeEventHandler<TItem> Removed
		{
			add    { dlgtRemoved += value; }
			remove { dlgtRemoved -= value; }
		}

		protected virtual void OnInserting(CollectionChangeEventArgs<TItem> e)
		{
			dlgtInserting?.Invoke(this, e);
		}
		protected virtual void OnInserted(CollectionChangeEventArgs<TItem> e)
		{
			dlgtInserted?.Invoke(this, e);
		}
		protected virtual void OnRemoving(CollectionChangeEventArgs<TItem> e)
		{
			dlgtRemoving?.Invoke(this, e);
		}
		protected virtual void OnRemoved(CollectionChangeEventArgs<TItem> e)
		{
			dlgtRemoved?.Invoke(this, e);
		}

		private CollectionEventHandler<TItem> dlgtClearing;
		private CollectionEventHandler<TItem> dlgtCleared;

		public event CollectionEventHandler<TItem> Clearing
		{
			add    { dlgtClearing += value; }
			remove { dlgtClearing -= value; }
		}
		public event CollectionEventHandler<TItem> Cleared
		{
			add    { dlgtCleared += value; }
			remove { dlgtCleared -= value; }
		}

		protected virtual void OnClearing(CollectionEventArgs<TItem> e)
		{
			dlgtClearing?.Invoke(this, e);
		}
		protected virtual void OnCleared(CollectionEventArgs<TItem> e)
		{
			dlgtCleared?.Invoke(this, e);
		}

		#endregion ICollectionWithEvents<TItem> Members

		#endregion Events

		#region Handle base events

		protected override void InsertItem(int index, TItem item)
		{
			OnInserting(new CollectionChangeEventArgs<TItem>(index, item));

			base.InsertItem(index, item);

			OnInserted(new CollectionChangeEventArgs<TItem>(index, item));
		}
		protected override void RemoveItem(int index)
		{
			TItem Item = base[index];

			OnRemoving(new CollectionChangeEventArgs<TItem>(index, Item));

			base.RemoveItem(index);

			OnRemoved(new CollectionChangeEventArgs<TItem>(index, Item));
		}

		protected override void ClearItems()
		{
			TItem[] a = new TItem[Count];

			CopyTo(a, 0);

			ICollection<TItem> Collection = new Collection<TItem>(a);

			OnClearing(new CollectionEventArgs<TItem>(this));

			base.ClearItems();

			OnCleared(new CollectionEventArgs<TItem>(Collection));
		}


		#endregion Handle base events

		public TItem[] ToArray()
		{
			TItem[] result = new TItem[Count];

			base.Items.CopyTo(result, 0);

			return result;
		}
	}


	public delegate void CollectionEventHandler<TItem>(object sender, CollectionEventArgs<TItem> e);

	public class CollectionEventArgs<TItem> : EventArgs
	{
		#region Properties

		private readonly ICollection<TItem> collection;
		public ICollection<TItem> Collection
		{
			get { return collection; }
		}

		#endregion Properties

		#region Constructor &

		public CollectionEventArgs(ICollection<TItem> Collection)
		{
			collection = Collection;
		}


		#endregion Constructor &
	}


	public delegate void CollectionChangeEventHandler<TItem>(object sender, CollectionChangeEventArgs<TItem> e);

	public class CollectionChangeEventArgs<TItem> : EventArgs
	{
		#region Properties

		private readonly TItem item;
		public TItem Item
		{
			get { return item; }
		}

		private readonly int index;
		public int Index
		{
			get { return index; }
		}

		#endregion Properties

		#region Constructor &

		public CollectionChangeEventArgs(int Index, TItem Item)
		{
			item  = Item;
			index = Index;
		}


		#endregion Constructor &
	}

	public class DictionaryWithEvents<TKey, TValue> : IDictionary<TKey, TValue>
	{
		private readonly Dictionary<TKey, TValue> hash = new Dictionary<TKey, TValue>();

		#region IDictionary<TKey,TItem> Members

		public void Add(TKey key, TValue value)
		{
			OnInserting(key, value);

			hash.Add(key, value);

			OnInserted(key, value);
		}

		public bool ContainsKey(TKey key)
		{
			return hash.ContainsKey(key);
		}

		public ICollection<TKey> Keys
		{
			get { return hash.Keys; }
		}

		public bool Remove(TKey key)
		{
			Trace.Assert(hash.ContainsKey(key));

			TValue value = hash[key];

			OnRemoving(key, value);

			bool result = hash.Remove(key);

			OnRemoved(key, value);

			return result;
		}

		public bool TryGetValue(TKey key, out TValue value)
		{
			return hash.TryGetValue(key, out value);
		}

		public ICollection<TValue> Values
		{
			get { return hash.Values; }
		}

		public TValue this[TKey key]
		{
			get
			{
				return hash[key];
			}
			set
			{
				OnInserting(key, value);

				hash[key] = value;

				OnInserted(key, value);
			}
		}

		#endregion

		#region ICollection<KeyValuePair<TKey,TValue>> Members

		void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item)
		{
			((ICollection<KeyValuePair<TKey, TValue>>)hash).Add(item);
		}

		public void Clear()
		{
			OnClearing();

			hash.Clear();

			OnCleared();
		}

		bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> item)
		{
			return ((ICollection<KeyValuePair<TKey, TValue>>)hash).Contains(item);
		}

		void ICollection<KeyValuePair<TKey, TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
		{
			((ICollection<KeyValuePair<TKey, TValue>>)hash).CopyTo(array, arrayIndex);
		}

		public int Count
		{
			get { return hash.Count; }
		}

		bool ICollection<KeyValuePair<TKey, TValue>>.IsReadOnly
		{
			get { return ((ICollection<KeyValuePair<TKey, TValue>>)hash).IsReadOnly; }
		}

		bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item)
		{
			return ((ICollection<KeyValuePair<TKey, TValue>>)hash).Remove(item);
		}

		#endregion

		#region IEnumerable<KeyValuePair<TKey,TItem>> Members

		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			return ((IEnumerable<KeyValuePair<TKey, TValue>>)hash).GetEnumerator();
		}

		#endregion

		#region IEnumerable Members

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return hash.GetEnumerator();
		}

		#endregion

		#region Events

		private HashChangeEventHandler<TKey, TValue> dlgtInserting;
		private HashChangeEventHandler<TKey, TValue> dlgtInserted;
		private HashChangeEventHandler<TKey, TValue> dlgtRemoving;
		private HashChangeEventHandler<TKey, TValue> dlgtRemoved;

		public event HashChangeEventHandler<TKey, TValue> Inserting
		{
			add    { dlgtInserting += value; }
			remove { dlgtInserting -= value; }
		}

		public event HashChangeEventHandler<TKey, TValue> Inserted
		{
			add    { dlgtInserted += value; }
			remove { dlgtInserted -= value; }
		}

		public event HashChangeEventHandler<TKey, TValue> Removing
		{
			add    { dlgtRemoving += value; }
			remove { dlgtRemoving -= value; }
		}
		public event HashChangeEventHandler<TKey, TValue> Removed
		{
			add    { dlgtRemoved += value; }
			remove { dlgtRemoved -= value; }
		}

		protected virtual void OnInserting(TKey key, TValue value)
		{
			dlgtInserting?.Invoke(this, new HashChangeEventArgs<TKey, TValue>(key, value));
		}
		protected virtual void OnInserted(TKey key, TValue value)
		{
			dlgtInserted?.Invoke(this, new HashChangeEventArgs<TKey, TValue>(key, value));
		}
		protected virtual void OnRemoving(TKey key, TValue value)
		{
			dlgtRemoving?.Invoke(this, new HashChangeEventArgs<TKey, TValue>(key, value));
		}
		protected virtual void OnRemoved(TKey key, TValue value)
		{
			dlgtRemoved?.Invoke(this, new HashChangeEventArgs<TKey, TValue>(key, value));
		}

		private HashEventHandler<TKey, TValue> dlgtClearing;
		private HashEventHandler<TKey, TValue> dlgtCleared;

		public event HashEventHandler<TKey, TValue> Clearing
		{
			add    { dlgtClearing += value; }
			remove { dlgtClearing -= value; }
		}
		public event HashEventHandler<TKey, TValue> Cleared
		{
			add    { dlgtCleared += value; }
			remove { dlgtCleared -= value; }
		}

		protected virtual void OnClearing()
		{
			dlgtClearing?.Invoke(this, new HashEventArgs<TKey, TValue>(this));
		}
		protected virtual void OnCleared()
		{
			dlgtCleared?.Invoke(this, new HashEventArgs<TKey, TValue>(this));
		}

		#endregion Events
	}

	public delegate void HashChangeEventHandler<TKey, TValue>(object sender, HashChangeEventArgs<TKey, TValue> e);

	public class HashChangeEventArgs<TKey, TValue> : EventArgs
	{
		private readonly TKey key;
		public TKey Key
		{
			get { return key; }
		}

		private readonly TValue value;
		public TValue Value
		{
			get { return value; }
		}

		public HashChangeEventArgs(TKey key, TValue value)
		{
			this.key = key;
			this.value = value;
		}
	}

	public delegate void HashEventHandler<TKey, TValue>(object sender, HashEventArgs<TKey, TValue> e);

	public class HashEventArgs<TKey, TValue> : EventArgs
	{
		private readonly IDictionary<TKey, TValue> dictionary;
		public IDictionary<TKey, TValue> Dictionary
		{
			get { return dictionary; }
		}

		public HashEventArgs(IDictionary<TKey, TValue> dictionary)
		{
			this.dictionary = dictionary;
		}
	}

	public class SortedCollectionWithEvents<TItem> :
		ICollectionWithEvents<TItem>
		where TItem : ISortable
	{
		private readonly SortedList<object, TItem> list =
			new SortedList<object, TItem>();

		#region ICollection<TItem> Members

		///<summary>
		///Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1"></see>.
		///</summary>
		///
		///<param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1"></see>.</param>
		///<exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1"></see> is read-only.</exception>
		public void Add(TItem item)
		{
			OnInserting(new CollectionChangeEventArgs<TItem>(-1, item));

			list.Add(item.Key, item);

			int Index = list.IndexOfKey(item.Key);

			OnInserted(new CollectionChangeEventArgs<TItem>(Index, item));
		}

		///<summary>
		///Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1"></see>.
		///</summary>
		///
		///<exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1"></see> is read-only. </exception>
		public void Clear()
		{
			TItem[] a = new TItem[Count];

			list.Values.CopyTo(a, 0);

			ICollection<TItem> Collection = new Collection<TItem>(a);

			OnClearing(new CollectionEventArgs<TItem>(list.Values));

			list.Clear();

			OnCleared(new CollectionEventArgs<TItem>(Collection));

		}

		///<summary>
		///Determines whether the <see cref="T:System.Collections.Generic.ICollection`1"></see> contains a specific value.
		///</summary>
		///
		///<returns>
		///true if item is found in the <see cref="T:System.Collections.Generic.ICollection`1"></see>; otherwise, false.
		///</returns>
		///
		///<param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1"></see>.</param>
		public bool Contains(TItem item)
		{
			return list.Values.Contains(item);
		}

		///<summary>
		///Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1"></see> to an <see cref="T:System.Array"></see>, starting at a particular <see cref="T:System.Array"></see> index.
		///</summary>
		///
		///<param name="array">The one-dimensional <see cref="T:System.Array"></see> that is the destination of the elements copied from <see cref="T:System.Collections.Generic.ICollection`1"></see>. The <see cref="T:System.Array"></see> must have zero-based indexing.</param>
		///<param name="arrayIndex">The zero-based index in array at which copying begins.</param>
		///<exception cref="T:System.ArgumentOutOfRangeException">arrayIndex is less than 0.</exception>
		///<exception cref="T:System.ArgumentNullException">array is null.</exception>
		///<exception cref="T:System.ArgumentException">array is multidimensional.-or-arrayIndex is equal to or greater than the length of array.-or-The number of elements in the source <see cref="T:System.Collections.Generic.ICollection`1"></see> is greater than the available space from arrayIndex to the end of the destination array.-or-Type T cannot be cast automatically to the type of the destination array.</exception>
		public void CopyTo(TItem[] array, int arrayIndex)
		{
			list.Values.CopyTo(array, arrayIndex);
		}

		///<summary>
		///Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1"></see>.
		///</summary>
		///
		///<returns>
		///true if item was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1"></see>; otherwise, false. This method also returns false if item is not found in the original <see cref="T:System.Collections.Generic.ICollection`1"></see>.
		///</returns>
		///
		///<param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1"></see>.</param>
		///<exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1"></see> is read-only.</exception>
		public bool Remove(TItem item)
		{
			int Index = list.IndexOfKey(item.Key);

			OnRemoving(new CollectionChangeEventArgs<TItem>(Index, item));

			bool Result = list.Remove(item.Key);

			OnRemoved(new CollectionChangeEventArgs<TItem>(Index, item));

			return Result;
		}

		///<summary>
		///Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1"></see>.
		///</summary>
		///
		///<returns>
		///The number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1"></see>.
		///</returns>
		///
		public int Count
		{
			get { return list.Count; }
		}

		///<summary>
		///Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1"></see> is read-only.
		///</summary>
		///
		///<returns>
		///true if the <see cref="T:System.Collections.Generic.ICollection`1"></see> is read-only; otherwise, false.
		///</returns>
		///
		public bool IsReadOnly
		{
			get { return list.Values.IsReadOnly; }
		}

		#endregion

		#region IEnumerable Members

		///<summary>
		///Returns an enumerator that iterates through a collection.
		///</summary>
		///
		///<returns>
		///An <see cref="T:System.Collections.IEnumerator"></see> object that can be used to iterate through the collection.
		///</returns>
		///<filterpriority>2</filterpriority>
		public IEnumerator GetEnumerator()
		{
			return list.Values.GetEnumerator();
		}

		#endregion

		#region ICollectionWithEvents<TItem> Members

		public TItem this[int index]
		{
			get { return list.Values[index]; }
		}

		public bool Empty
		{
			get { return Count == 0; }
		}

		private CollectionChangeEventHandler<TItem> dlgtInserting;

		public event CollectionChangeEventHandler<TItem> Inserting
		{
			add    { dlgtInserting += value; }
			remove { dlgtInserting -= value; }
		}

		protected virtual void OnInserting(CollectionChangeEventArgs<TItem> e)
		{
			dlgtInserting?.Invoke(this, e);
		}

		private CollectionChangeEventHandler<TItem> dlgtInserted;

		public event CollectionChangeEventHandler<TItem> Inserted
		{
			add    { dlgtInserted += value; }
			remove { dlgtInserted -= value; }
		}

		protected virtual void OnInserted(CollectionChangeEventArgs<TItem> e)
		{
			dlgtInserted?.Invoke(this, e);
		}

		private CollectionChangeEventHandler<TItem> dlgtRemoving;

		public event CollectionChangeEventHandler<TItem> Removing
		{
			add    { dlgtRemoving += value; }
			remove { dlgtRemoving -= value; }
		}

		protected virtual void OnRemoving(CollectionChangeEventArgs<TItem> e)
		{
			dlgtRemoving?.Invoke(this, e);
		}

		private CollectionChangeEventHandler<TItem> dlgtRemoved;

		public event CollectionChangeEventHandler<TItem> Removed
		{
			add    { dlgtRemoved += value; }
			remove { dlgtRemoved -= value; }
		}

		protected virtual void OnRemoved(CollectionChangeEventArgs<TItem> e)
		{
			dlgtRemoved?.Invoke(this, e);
		}

		private CollectionEventHandler<TItem> dlgtClearing;

		public event CollectionEventHandler<TItem> Clearing
		{
			add    { dlgtClearing += value; }
			remove { dlgtClearing -= value; }
		}

		protected virtual void OnClearing(CollectionEventArgs<TItem> e)
		{
			dlgtClearing?.Invoke(this, e);
		}

		private CollectionEventHandler<TItem> dlgtCleared;

		public event CollectionEventHandler<TItem> Cleared
		{
			add    { dlgtCleared += value; }
			remove { dlgtCleared -= value; }
		}

		protected virtual void OnCleared(CollectionEventArgs<TItem> e)
		{
			dlgtCleared?.Invoke(this, e);
		}

		#endregion

		#region IEnumerable<TItem> Members

		///<summary>
		///Returns an enumerator that iterates through the collection.
		///</summary>
		///
		///<returns>
		///A <see cref="T:System.Collections.Generic.IEnumerator`1"></see> that can be used to iterate through the collection.
		///</returns>
		///<filterpriority>1</filterpriority>
		IEnumerator<TItem> IEnumerable<TItem>.GetEnumerator()
		{
			return list.Values.GetEnumerator();
		}

		#endregion

		public int IndexOf(TItem item)
		{
			return list.IndexOfValue(item);
		}

		public IList<TItem> Clone()
		{
			TItem[] a = new TItem[Count];

			list.Values.CopyTo(a, 0);

			return new List<TItem>(a);
		}
	}

	public interface ICollectionWithEvents<TItem> : ICollection<TItem>
	{
		TItem this[int index] { get; }

		bool Empty { get; }

		event CollectionChangeEventHandler<TItem> Inserting;
		event CollectionChangeEventHandler<TItem> Inserted;
		event CollectionChangeEventHandler<TItem> Removing;
		event CollectionChangeEventHandler<TItem> Removed;
		event CollectionEventHandler<TItem>       Clearing;
		event CollectionEventHandler<TItem>       Cleared;
	}
}