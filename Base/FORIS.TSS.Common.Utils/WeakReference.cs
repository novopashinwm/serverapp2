﻿using System;
using System.Configuration;

namespace FORIS.TSS.Common
{
	public class WeakReference<T> where T : class
	{
		private static readonly bool     IsStrong;
		private static readonly TimeSpan ExpirationTimeout;

		static WeakReference()
		{
			var configPrefix = "FORIS.TSS.Common.WeakReference[" + typeof(T) + "].";
			var isStrongConfig = ConfigurationManager.AppSettings[configPrefix + "IsStrong"];
			IsStrong = string.Equals(isStrongConfig, "true", StringComparison.OrdinalIgnoreCase);

			var expirationConfig = ConfigurationManager.AppSettings[configPrefix + "ExpirationTimeout"];
			if (!TimeSpan.TryParse(expirationConfig, out ExpirationTimeout))
				ExpirationTimeout = TimeSpan.MaxValue; //never expired
		}
		public T Target
		{
			get
			{
				if (IsStrong)
				{
					if (ExpirationTimeout != TimeSpan.MaxValue)
					{
						var currentRetrieveTime = DateTime.UtcNow;
						if (ExpirationTimeout < currentRetrieveTime - _lastRetrieveTime)
							return null;
						_lastRetrieveTime = currentRetrieveTime;
					}
					return strongReference;
				}
				return (T)reference.Target;
			}
		}
		private readonly WeakReference reference;
		private readonly T strongReference;
		private DateTime _lastRetrieveTime;
		public WeakReference(T target)
		{
			if (IsStrong)
			{
				strongReference = target;
				_lastRetrieveTime = DateTime.UtcNow;
			}
			else
				reference = new WeakReference(target);
		}
	}
}