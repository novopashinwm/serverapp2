﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using FORIS.TSS.Common.Http;

namespace FORIS.TSS.Common
{
	/// <summary> Вспомогательный класс для работы с HTTP </summary>
	public static class HttpHelper
	{
		/// <summary> Возвращает строку MIME для указанного типа данных </summary>
		/// <param name="type"> Тип данных </param>
		public static string GetContentType(DataType type)
		{
			switch (type)
			{
				case DataType.Unspecified:
					throw new ArgumentOutOfRangeException("type", type, @"Value cannot be used here");
				case DataType.Json:
					return "application/json";
				case DataType.Xml:
					return "text/xml";
				default:
					throw new NotImplementedException("Type " + type + " is not implemented yet");
			}
		}
		public static IEnumerable<KeyValuePair<string, string>> ParseQueryString(this Uri uri)
		{
			var query  = uri?.Query?.Trim('?', ' ');
			if (string.IsNullOrWhiteSpace(query))
				query = string.Empty;
			foreach (var paramQry in Regex.Split(query, "&"))
			{
				var paramArr = Regex.Split(paramQry, "=") ?? new string[0];
				if (paramArr.Length < 1)
					continue;
				var paramKey = default(string);
				if (paramArr.Length > 0)
					paramKey = paramArr[0].Trim();
				var paramVal = default(string);
				if (paramArr.Length > 1)
					paramVal = paramArr[1].Trim();
				yield return new KeyValuePair<string, string>(paramKey, paramVal);
			}
		}
	}
}