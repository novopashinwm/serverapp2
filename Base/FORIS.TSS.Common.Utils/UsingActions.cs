﻿using System;

namespace FORIS.TSS.Common
{
	public class UsingActions : IDisposable
	{
		private readonly Action _createAction = () => { };
		private readonly Action _deleteAction = () => { };
		public UsingActions(Action createAction, Action deleteAction)
		{
			_createAction = createAction ?? _createAction;
			_deleteAction = deleteAction ?? _deleteAction;
			_createAction();
		}
		public void Dispose()
		{
			_deleteAction();
		}
	}
}