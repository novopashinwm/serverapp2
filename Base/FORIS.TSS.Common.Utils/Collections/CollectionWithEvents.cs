﻿using System;
using System.Collections;
using System.Diagnostics;

namespace FORIS.TSS.Common.Collections
{
	// Declare the event signatures
	public delegate void CollectionChangeEventHandler(object sender, CollectionChangeEventArgs e);
	public delegate void HashChangeEventHandler(object sender, HashChangeEventArgs e);

	public class CollectionChangeEventArgs : EventArgs
	{
		#region Properties

		private int m_Index;
		public int Index { get { return m_Index; } }

		private object m_Value;
		public object Value { get { return m_Value; } }

		private object m_ValueOld;
		public object ValueOld { get { return m_ValueOld; } }


		#endregion // Properties

		#region Constructor

		public CollectionChangeEventArgs(int Index, object Value)
		{
			m_Index = Index;
			m_Value = Value;
			m_ValueOld = null;
		}
		public CollectionChangeEventArgs(int Index, object Value, object ValueOld)
		{
			m_Index = Index;
			m_Value = Value;
			m_ValueOld = ValueOld;
		}


		#endregion // Constructor 
	}
	public class HashChangeEventArgs : EventArgs
	{
		#region Properties

		private object m_Key;
		public object Key { get { return m_Key; } }


		private object m_Value;
		public object Value { get { return m_Value; } }


		#endregion // Properties

		#region Constructor 

		public HashChangeEventArgs(object Key, object Value)
		{
			m_Key = Key;
			m_Value = Value;
		}


		#endregion // Constructor 
	}

	public class CollectionWithEvents : CollectionBase, ICollectionWithEvents
	{
		#region Properties

		private int _suspendCount;
		// Are change events currently suspended?
		public bool IsSuspended
		{
			get { return (_suspendCount > 0); }
		}


		public bool Empty
		{
			get { return this.Count == 0; }
		}


		#endregion // Properties

		#region Manipulation methods

		public IList Clone()
		{
			return (IList)base.InnerList.Clone();
		}

		public void Sort(IComparer Comparer)
		{
			this.OnSort();

			base.InnerList.Sort(Comparer);

			this.OnSortComplete();
		}

		protected int IndexOf(object value)
		{
			// Find the 0 based index of the requested entry
			return base.List.IndexOf(value);
		}

		#endregion Manipulation methods

		#region Constructor & 

		public CollectionWithEvents()
		{
			// Default to not suspended
			_suspendCount = 0;
		}


		#endregion Constructor & 

		#region Manage methods

		// Do not generate change events until resumed
		public void SuspendEvents()
		{
			_suspendCount++;
		}

		// Safe to resume change events.
		public void ResumeEvents()
		{
			--_suspendCount;
		}


		#endregion // Manage methods

		#region Events

		private EventHandler m_Sorting;
		private EventHandler m_Sorted;

		private EventHandler m_Clearing;
		private EventHandler m_Cleared;

		private CollectionChangeEventHandler m_Removing;
		private CollectionChangeEventHandler m_Removed;

		private CollectionChangeEventHandler m_Inserting;
		private CollectionChangeEventHandler m_Inserted;

		private CollectionChangeEventHandler m_Setting;
		private CollectionChangeEventHandler m_Set;


		public event EventHandler Sorting
		{
			add { m_Sorting += value; }
			remove { m_Sorting -= value; }
		}
		public event EventHandler Sorted
		{
			add { m_Sorted += value; }
			remove { m_Sorted -= value; }
		}

		public event EventHandler Clearing
		{
			add { m_Clearing += value; }
			remove { m_Clearing -= value; }
		}
		public event EventHandler Cleared
		{
			add { m_Cleared += value; }
			remove { m_Cleared -= value; }
		}


		public event CollectionChangeEventHandler Inserting
		{
			add { m_Inserting += value; }
			remove { m_Inserting -= value; }
		}
		public event CollectionChangeEventHandler Inserted
		{
			add { m_Inserted += value; }
			remove { m_Inserted -= value; }
		}


		public event CollectionChangeEventHandler Removing
		{
			add { m_Removing += value; }
			remove { m_Removing -= value; }
		}
		public event CollectionChangeEventHandler Removed
		{
			add { m_Removed += value; }
			remove { m_Removed -= value; }
		}


		public event CollectionChangeEventHandler Setting
		{
			add { m_Setting += value; }
			remove { m_Setting -= value; }
		}
		public event CollectionChangeEventHandler Set
		{
			add { m_Set += value; }
			remove { m_Set -= value; }
		}


		protected virtual void OnSort()
		{
			if (!IsSuspended)
			{
				// Any attached event handlers?
				if (m_Sorting != null)
					m_Sorting(this, EventArgs.Empty);
			}
		}
		protected virtual void OnSortComplete()
		{
			if (!IsSuspended)
			{
				// Any attached event handlers?
				if (m_Sorted != null)
					m_Sorted(this, EventArgs.Empty);
			}
		}

		protected override void OnClear()
		{
			if (!IsSuspended)
			{
				// Any attached event handlers?
				if (m_Clearing != null)
					m_Clearing(this, EventArgs.Empty);
			}
		}

		protected override void OnClearComplete()
		{
			if (!IsSuspended)
			{
				// Any attached event handlers?
				if (m_Cleared != null)
					m_Cleared(this, EventArgs.Empty);
			}
		}

		protected override void OnInsert(int Index, object Value)
		{
			if (!IsSuspended)
			{
				// Any attached event handlers?
				if (m_Inserting != null)
					m_Inserting(this, new CollectionChangeEventArgs(Index, Value));
			}
		}

		protected override void OnInsertComplete(int Index, object Value)
		{
			if (!IsSuspended)
			{
				// Any attached event handlers?
				if (m_Inserted != null)
					m_Inserted(this, new CollectionChangeEventArgs(Index, Value));
			}
		}

		protected override void OnRemove(int Index, object Value)
		{
			if (!IsSuspended)
			{
				// Any attached event handlers?
				if (m_Removing != null)
					m_Removing(this, new CollectionChangeEventArgs(Index, Value));
			}
		}

		protected override void OnRemoveComplete(int Index, object Value)
		{
			if (!IsSuspended)
			{
				// Any attached event handlers?
				if (m_Removed != null)
					m_Removed(this, new CollectionChangeEventArgs(Index, Value));
			}
		}
		protected override void OnSet(int Index, object oldValue, object newValue)
		{
			if (!IsSuspended)
			{
				if (m_Setting != null)
					m_Setting(this, new CollectionChangeEventArgs(Index, newValue, oldValue));
			}
		}
		protected override void OnSetComplete(int Index, object oldValue, object newValue)
		{
			if (!IsSuspended)
			{
				if (m_Set != null)
					m_Set(this, new CollectionChangeEventArgs(Index, newValue, oldValue));
			}
		}




		#endregion Events
	}

	public class SortedCollectionWithEvents : ICollectionWithEvents, ISortedList
	{
		#region Properties

		public int Count
		{
			get { return m_List.Count; }
		}

		private int m_CountSuspend = 0;

		public bool IsSuspended
		{
			get { return (m_CountSuspend > 0); }
		}


		protected ISortedList List
		{
			get { return this; }
		}


		#endregion // Properties

		#region ISortedList members

		ISortable ISortedList.this[int Index]
		{
			get { return (ISortable)m_List.GetByIndex(Index); }
		}


		int ISortedList.Add(ISortable Item)
		{
			this.OnInserting(Item);


			Debug.Assert(!m_List.Contains(Item.Key));
			m_List.Add(Item.Key, Item);

			this.OnInserted(m_List.IndexOfKey(Item.Key), Item);

			return m_List.IndexOfKey(Item.Key);
		}

		void ISortedList.Remove(ISortable Item)
		{
			int Index = m_List.IndexOfKey(Item.Key);

			this.OnRemoving(Index, Item);

			Debug.Assert(m_List.Contains(Item.Key));
			m_List.Remove(Item.Key);

			this.OnRemoved(Index, Item);
		}


		int ISortedList.IndexOf(ISortable Item)
		{
			return m_List.IndexOfKey(Item.Key);
		}


		bool ISortedList.ContainsKey(object Key)
		{
			return m_List.ContainsKey(Key);
		}
		ISortable ISortedList.GetByKey(object Key)
		{
			return (ISortable)m_List[Key];
		}



		#endregion // ISortedList members

		#region Manipulation methods

		private SortedList m_List = new SortedList();

		public SortedList Clone()
		{
			return (SortedList)m_List.Clone();
		}

		public void Clear()
		{
			this.OnClearing();

			m_List.Clear();

			this.OnCleared();
		}



		#endregion // Manipulation methods

		#region Manage methods

		public void SuspendEvents()
		{
			m_CountSuspend++;

		}
		public void ResumeEvents()
		{
			m_CountSuspend--;
		}


		#endregion Manage methods

		#region IEnumerable members

		public IEnumerator GetEnumerator()
		{
			return m_List.Values.GetEnumerator();
		}

		#endregion // IEnumerable members

		#region Events

		private CollectionChangeEventHandler m_Inserting;
		private CollectionChangeEventHandler m_Inserted;
		private CollectionChangeEventHandler m_Removing;
		private CollectionChangeEventHandler m_Removed;
		private EventHandler m_Clearing;
		private EventHandler m_Cleared;

		public event CollectionChangeEventHandler Inserting
		{
			add { m_Inserting += value; }
			remove { m_Inserting -= value; }
		}
		public event CollectionChangeEventHandler Inserted
		{
			add { m_Inserted += value; }
			remove { m_Inserted -= value; }
		}

		public event CollectionChangeEventHandler Removing
		{
			add { m_Removing += value; }
			remove { m_Removing -= value; }
		}
		public event CollectionChangeEventHandler Removed
		{
			add { m_Removed += value; }
			remove { m_Removed -= value; }
		}

		public event EventHandler Clearing
		{
			add { m_Clearing += value; }
			remove { m_Clearing -= value; }
		}
		public event EventHandler Cleared
		{
			add { m_Cleared += value; }
			remove { m_Cleared -= value; }
		}


		protected virtual void OnInserting(object Value)
		{
			if (m_Inserting != null)
				m_Inserting(this, new CollectionChangeEventArgs(-1, Value));
		}
		protected virtual void OnInserted(int Index, object Value)
		{
			if (m_Inserted != null)
				m_Inserted(this, new CollectionChangeEventArgs(Index, Value));
		}
		protected virtual void OnRemoving(int Index, object Value)
		{
			if (m_Removing != null)
				m_Removing(this, new CollectionChangeEventArgs(Index, Value));
		}
		protected virtual void OnRemoved(int Index, object Value)
		{
			if (m_Removed != null)
				m_Removed(this, new CollectionChangeEventArgs(Index, Value));
		}
		protected virtual void OnClearing()
		{
			if (m_Clearing != null)
				m_Clearing(this, EventArgs.Empty);
		}
		protected virtual void OnCleared()
		{
			if (!this.IsSuspended)
			{
				if (m_Cleared != null)
					m_Cleared(this, EventArgs.Empty);
			}
		}


		#endregion Events
	}

	public class HashWithEvents : IEnumerable
	{
		#region Indexer

		private Hashtable m_Hash;

		public object this[object Key]
		{
			get { return m_Hash[Key]; }
		}

		#endregion // Indexer

		#region Properties

		private int m_CountSuspend = 0;

		public bool IsSuspended
		{
			get { return m_CountSuspend > 0; }
		}


		public int Count
		{
			get { return m_Hash.Count; }
		}


		public bool Empty
		{
			get { return m_Hash.Count == 0; }
		}

		public ICollection Keys
		{
			get { return m_Hash.Keys; }
		}

		public ICollection Values
		{
			get { return m_Hash.Values; }
		}


		#endregion // Properties

		#region Constructor

		public HashWithEvents()
		{
			m_Hash = new Hashtable();
		}

		#endregion // Constructor 

		#region Manipulation methods

		protected void Remove(object Key)
		{
			object Value = this[Key];

			this.OnRemove(Key, Value);

			Debug.Assert(this.Contains(Key));
			m_Hash.Remove(Key);

			this.OnRemoveComplete(Key, Value);
		}
		protected void Add(object Key, object Value)
		{
			this.OnInsert(Key, Value);

			Debug.Assert(!this.Contains(Key));
			m_Hash.Add(Key, Value);

			this.OnInsertComplete(Key, Value);
		}

		public Hashtable Clone()
		{
			return (Hashtable)m_Hash.Clone();
		}

		public void Clear()
		{
			this.OnClear();

			m_Hash.Clear();

			this.OnClearComplete();
		}


		public bool Contains(object Key)
		{
			return m_Hash.Contains(Key);
		}


		#endregion // Manipulation methods

		#region Events

		private HashChangeEventHandler m_Removing;
		private HashChangeEventHandler m_Removed;
		private HashChangeEventHandler m_Inserting;
		private HashChangeEventHandler m_Inserted;

		private EventHandler m_Clearing;
		private EventHandler m_Cleared;

		public event HashChangeEventHandler Removing
		{
			add { m_Removing += value; }
			remove { m_Removing -= value; }
		}
		public event HashChangeEventHandler Removed
		{
			add { m_Removed += value; }
			remove { m_Removed -= value; }
		}
		public event HashChangeEventHandler Inserting
		{
			add { m_Inserting += value; }
			remove { m_Inserting -= value; }
		}
		public event HashChangeEventHandler Inserted
		{
			add { m_Inserted += value; }
			remove { m_Inserted -= value; }
		}

		public event EventHandler Clearing
		{
			add { m_Clearing += value; }
			remove { m_Clearing -= value; }
		}
		public event EventHandler Cleared
		{
			add { m_Cleared += value; }
			remove { m_Cleared -= value; }
		}


		protected virtual void OnRemove(object Key, object Value)
		{
			if (m_Removing != null && !this.IsSuspended)
				m_Removing(this, new HashChangeEventArgs(Key, Value));
		}
		protected virtual void OnRemoveComplete(object Key, object Value)
		{
			if (m_Removed != null && !this.IsSuspended)
				m_Removed(this, new HashChangeEventArgs(Key, Value));
		}
		protected virtual void OnInsert(object Key, object Value)
		{
			if (m_Inserting != null && !this.IsSuspended)
				m_Inserting(this, new HashChangeEventArgs(Key, Value));
		}
		protected virtual void OnInsertComplete(object Key, object Value)
		{
			if (m_Inserted != null && !this.IsSuspended)
				m_Inserted(this, new HashChangeEventArgs(Key, Value));
		}
		protected virtual void OnClear()
		{
			if (m_Clearing != null && !this.IsSuspended)
				m_Clearing(this, EventArgs.Empty);
		}
		protected virtual void OnClearComplete()
		{
			if (m_Cleared != null && !this.IsSuspended)
				m_Cleared(this, EventArgs.Empty);
		}

		#endregion // Events

		#region IEnumerable Members

		public IEnumerator GetEnumerator()
		{
			return m_Hash.GetEnumerator();
		}

		#endregion
	}

	public interface ICollectionWithEvents : IEnumerable
	{
		#region Properties

		bool IsSuspended { get; }


		#endregion // Properties

		#region Manage

		void SuspendEvents();
		void ResumeEvents();

		#endregion // Manage

		#region Events

		event CollectionChangeEventHandler Removing;
		event CollectionChangeEventHandler Removed;
		event CollectionChangeEventHandler Inserting;
		event CollectionChangeEventHandler Inserted;

		event EventHandler Clearing;
		event EventHandler Cleared;

		#endregion // Events
	}

	public interface ISortable
	{
		object Key { get; }
	}
	public interface ISortedList
	{
		ISortable this[int Index] { get; }

		int Add(ISortable Item);
		void Remove(ISortable Item);

		int IndexOf(ISortable Item);

		bool ContainsKey(object Key);
		ISortable GetByKey(object Key);
	}
}