using System;

namespace FORIS.TSS.Common
{
	/// <summary>
	/// Summary description for Weekdays.
	/// </summary>
	#region public class WeekDays
	public class WeekDays
	{
		public static byte Holiday 
		{
			get { return 128; }
		}
		public static byte Monday 
		{
			get { return 64; }
		}
		public static byte Tuesday 		
		{
			get { return 32; }
		}
		public static byte Wednesday 
		{
			get { return 16; }
		}
		public static byte Thursday 
		{
			get { return 8; }
		}
		public static byte Friday 
		{
			get { return 4; }
		}
		public static byte Saturday 
		{
			get { return 2; }
		}
		public static byte Sunday
		{
			get { return 1; }
		}
		public static byte Workdays
		{
			get { return (byte)( Monday | Tuesday | Wednesday | Thursday | Friday );}
		}

		public static byte Mask( DateTime Date )
		{
			byte mask = 0;
			switch( Date.DayOfWeek )
			{
				case DayOfWeek.Monday: mask = WeekDays.Monday;		break;
				case DayOfWeek.Tuesday: mask = WeekDays.Tuesday;		break;
				case DayOfWeek.Wednesday: mask = WeekDays.Wednesday;	break;
				case DayOfWeek.Thursday: mask = WeekDays.Thursday;	break;
				case DayOfWeek.Friday: mask = WeekDays.Friday;		break;
				case DayOfWeek.Saturday: mask = WeekDays.Saturday;	break;
				case DayOfWeek.Sunday: mask = WeekDays.Sunday;		break;
			}
			return mask;
		}
	}
	#endregion
}
