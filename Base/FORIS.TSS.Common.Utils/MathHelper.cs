﻿using System;

namespace FORIS.TSS.Common
{
	public static class MathHelper
	{
		public static double DegreesToRadians(double degrees)
		{
			return degrees * Math.PI / 180;
		}
	}
}