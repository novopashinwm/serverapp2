﻿using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Common.Helpers
{
	/// <summary> Вспомогательный класс для работы с <see cref="IMobilUnit"/> </summary>
	public static class MobilUnitHelper
	{
		/// <summary> Конвертирует данные, полученные от службы терминалов в DTO-объект, готовый к передаче в WEB </summary>
		public static BusinessLogic.DTO.Vehicle ToDTO(this IMobilUnit mu)
		{
			if (null == mu)
				return default(BusinessLogic.DTO.Vehicle);
			var vehicle = new BusinessLogic.DTO.Vehicle
			{
				id           = mu.Unique,
				isValid      = mu.ValidPosition && GeoHelper.InvalidLatitude  != mu.Latitude && GeoHelper.InvalidLongitude != mu.Longitude,
				lat          = mu.ValidPosition && GeoHelper.InvalidLatitude  != mu.Latitude  ? mu.Latitude  : (double?)null,
				lng          = mu.ValidPosition && GeoHelper.InvalidLongitude != mu.Longitude ? mu.Longitude : (double?)null,
				course       = mu.Course,
				gpscourse    = mu.Course,
				speed        = mu.Speed,
				radius       = mu.Radius,
				PositionType = mu.Type,
				logTime      = mu.Time,
				posLogTime   = mu.CorrectTime,
				Sensors      = mu.GetSensors()
			};
			return vehicle;
		}
		public static List<Sensor> GetSensors(this IMobilUnit mu)
		{
			if (0 == (mu?.SensorMap?.Count ?? 0))
				return default(List<Sensor>);

			var result = new List<Sensor>(mu.SensorMap.Count);

			foreach (var sensorLegend in mu.SensorMap.Keys)
			{
				var sensorValue = mu.GetSensorValue(sensorLegend);
				if (sensorValue == null)
					continue;
				result.Add(new Sensor
				{
					Number     = sensorLegend,
					Type       = sensorLegend.GetSensorType(),
					LogRecords = new List<SensorLogRecord> { new SensorLogRecord(mu.Time, sensorValue.Value) }
				});
			}

			return result;
		}
		public static string GetProtocolName(this IMobilUnit mu)
		{
			return mu.Properties?.GetValueOrDefault(DeviceProperty.Protocol);
		}
		public static IEnumerable<long?> GetRawValuesByLegend(this IMobilUnit mu, SensorLegend legend)
		{
			if (mu.SensorMap != null && mu.SensorMap.TryGetValue(legend, out var maps) && maps != null && mu.SensorValues != null)
				foreach (var map in maps)
					if (mu.SensorValues.TryGetValue(map.InputNumber, out var rawSensorValue))
						yield return rawSensorValue;
		}
	}
}