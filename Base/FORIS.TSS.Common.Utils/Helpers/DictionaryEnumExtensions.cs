﻿using System;
using System.Globalization;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.Resources;

namespace FORIS.TSS.Common.Helpers
{
	public static class DictionaryEnumExtensions
	{
		public static DictionaryEnum<TEnum> ToDictionaryEnum<TEnum>(this TEnum srcItem, CultureInfo culture) where TEnum : struct, IConvertible
		{
			return ToDictionaryEnum(srcItem, ResourceContainers.Get(culture));
		}
		public static DictionaryEnum<TEnum> ToDictionaryEnum<TEnum>(this TEnum srcItem, ResourceContainers resourceContainers) where TEnum : struct, IConvertible
		{
			var objItem = Enum.ToObject(typeof(TEnum), srcItem);
			if (null != objItem)
			{
				var enmItem = (TEnum)objItem;
				return new DictionaryEnum<TEnum>()
				{
					Id   = enmItem.ToInt32(null),
					Code = enmItem.ToString(),
					Name = resourceContainers.GetLocalizedText<TEnum>(enmItem.ToString()),
				};
			}
			return new DictionaryEnum<TEnum>();
		}
	}
}