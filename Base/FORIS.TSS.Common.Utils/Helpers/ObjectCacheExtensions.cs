﻿using System;
using System.Linq;
using System.Runtime.Caching;

namespace FORIS.TSS.Common.Helpers
{
	public static class ObjectCacheExtensions
	{
		public static T AddOrGetExisting<T>(this ObjectCache cacheObj, string key, Func<T> valueFactory, CacheItemPolicy policy)
		{
			if (cacheObj == null)
				return default(T);
			var oldValue = default(Lazy<T>);
			var newValue = new Lazy<T>(valueFactory, true);
			lock (cacheObj)
				oldValue = cacheObj.AddOrGetExisting(key, newValue, policy) as Lazy<T>;
			try
			{
				return (oldValue ?? newValue).Value;
			}
			catch
			{
				cacheObj.Remove(key);
				throw;
			}
		}
		public static T AddOrGetExisting<T>(this ObjectCache cacheObj, string key, Func<T> valueFactory, DateTimeOffset absoluteExpiration)
		{
			if (cacheObj == null)
				return default(T);
			return cacheObj.AddOrGetExisting<T>(key, valueFactory, new CacheItemPolicy { AbsoluteExpiration = absoluteExpiration });
		}
		public static T AddOrGetExisting<T>(this ObjectCache cacheObj, string key, Func<T> valueFactory, TimeSpan lifeTime)
		{
			if (cacheObj == null)
				return default(T);
			return cacheObj.AddOrGetExisting<T>(key, valueFactory, DateTimeOffset.Now.Add(lifeTime));
		}
		public static void Remove<T>(this ObjectCache cacheObj, string cacheKey)
		{
			if (cacheObj == null)
				return;
			cacheObj.Remove(cacheKey);
		}
		public static void Clear<T>(this ObjectCache cacheObj)
		{
			if (cacheObj == null)
				return;
			foreach (var cacheKey in cacheObj.Select(k => k.Key).ToArray())
				cacheObj.Remove(cacheKey);
		}
		public static bool IsSet(this ObjectCache cacheObj, string cacheKey)
		{
			if (cacheObj == null)
				return false;
			return cacheObj.Contains(cacheKey);
		}
		public static T GetObjectFromCache<T>(this ObjectCache cacheObj, string cacheDataKey, Func<T> cacheDataGet = null, DateTimeOffset? cacheExpiration = null)
				where T : class
		{
			if (cacheObj == null)
				return default(T);
			var cacheDataVal = cacheObj[cacheDataKey] as T;
			if (cacheDataVal == null)
			{
				if (cacheDataGet == null)
					return cacheDataVal;
				cacheDataVal = cacheDataGet();
				if (null != cacheDataVal)
					cacheObj.Set(
						cacheDataKey,
						cacheDataVal,
						new CacheItemPolicy()
						{
							AbsoluteExpiration = cacheExpiration ?? ObjectCache.InfiniteAbsoluteExpiration
						});
			}
			return cacheDataVal;
		}
	}
}