﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace FORIS.TSS.Helpers
{
	public static class EnumerableHelper
	{
		/// <summary> Возвращает непрерывные подпоследовательности входной последовательности, удовлетворяющие заданному условию </summary>
		public static IEnumerable<List<T>> GetSequences<T>(this IEnumerable<T> input, Predicate<T> condition)
		{
			List<T> currentSet = null;
			foreach (var item in input)
			{
				if (condition(item))
				{
					if (currentSet == null)
						currentSet = new List<T>();
					currentSet.Add(item);
				}
				else
				{
					if (currentSet != null)
					{
						yield return currentSet;
						currentSet = null;
					}
				}
			}

			if (currentSet != null)
				yield return currentSet;
		}
		/// <summary> Возвращает непрерывные подпоследовательности входной последовательности, удовлетворяющие заданному условию </summary>
		/// <param name="input"> Исходная последовательность </param>
		/// <param name="condition"> Предикат (prev, curr)=>true/false, возвращающий true, если curr относится к подпоследовательности того же типа, что и prev </param>
		public static IEnumerable<List<T>> GetSequences<T>(this IEnumerable<T> input, Func<T, T, bool> condition)
		{
			var currentSet = new List<T>();
			foreach (var item in input)
			{
				if (currentSet.Count == 0 || condition(currentSet[currentSet.Count - 1], item))
				{
					currentSet.Add(item);
				}
				else
				{
					yield return currentSet;
					currentSet = new List<T> { item };
				}
			}

			if (currentSet.Count != 0)
				yield return currentSet;
		}
		/// <summary> Возвращает непрерывные подпоследовательности входной последовательности, удовлетворяющие заданному условию, наращивая текущую последовательность </summary>
		/// <param name="input"> Исходная последовательность </param>
		/// <param name="condition"> Предикат (prev, curr)=>true/false, возвращающий true, если curr относится к подпоследовательности того же типа, что и prev </param>
		public static IEnumerable<List<T>> GetGreedySequences<T>(this IEnumerable<T> input, Func<List<T>, T, bool> condition)
		{
			var currentSet = new List<T>();
			foreach (var item in input)
			{
				if (currentSet.Count == 0 || condition(currentSet, item))
				{
					currentSet.Add(item);
				}
				else
				{
					yield return currentSet;
					currentSet = new List<T> { item };
				}
			}

			if (currentSet.Count != 0)
				yield return currentSet;
		}
		public static IEnumerable<T> TakeAfter<T>(this IEnumerable<T> items, int startIndex)
		{
			var i = 0;
			foreach (var item in items)
			{
				if (startIndex < i)
					yield return item;
				++i;
			}
		}
		public static IEnumerable<T> SubSequence<T>(this IEnumerable<T> items, int startIndex, int endIndex)
		{
			var i = 0;
			foreach (var item in items)
			{
				if (endIndex <= i)
					break;
				if (startIndex <= i)
					yield return item;
				++i;
			}
		}
		public static bool SequenceEqual<T>(IEnumerable<T> x, IEnumerable<T> y)
		{
			if (x == null)
				return y == null;
			if (y == null)
				return false;
			return x.SequenceEqual(y);
		}
		/// <summary> Проверка, начинается ли данный массив с указанного подмассива </summary>
		/// <typeparam name="T">Тип элемента массива</typeparam>
		/// <param name="this">Проверяемый массив</param>
		/// <param name="subArray">Подмассив</param>
		/// <param name="startPosition">Стартовая позиция.</param>
		public static bool StartsWith<T>(this T[] @this, T[] subArray, int startPosition = 0) where T : struct
		{
			if (@this == null)
				throw new ArgumentNullException(nameof(@this));
			if (subArray == null)
				throw new ArgumentNullException(nameof(subArray));

			if (@this.Length < subArray.Length)
				return false;

			for (var i = 0; i != subArray.Length; ++i)
			{
				if (!@this[i + startPosition].Equals(subArray[i]))
					return false;
			}

			return true;
		}
		/// <summary> Подсчитывает количество элементов в перечислении в интервале </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="this"></param>
		/// <param name="elem"></param>
		/// <param name="startPosition"></param>
		/// <param name="endPosition"></param>
		/// <returns></returns>
		public static int Count<T>(this T[] @this, T elem, int startPosition = 0, int endPosition = 0) where T : struct
		{
			if (@this == null)
				throw new ArgumentNullException(nameof(@this));

			endPosition = endPosition <= 0
				? @this.Length - 1
				: endPosition < @this.Length ? endPosition : @this.Length - 1;
			if (startPosition > endPosition)
				throw new ArgumentException("startPosition > endPosition");

			var count = 0;
			for (var i = startPosition; i <= endPosition; i++)
			{
				if (@this[i].Equals(elem))
				{
					count++;
				}
			}

			return count;
		}
		public static int? IndexOf<T>(this T[] @this, T item, Func<T, T, bool> equals)
		{
			if (@this == null)
				throw new ArgumentNullException(nameof(@this));
			if (equals == null)
				throw new ArgumentNullException(nameof(@equals));

			for (var i = 0; i != @this.Length; ++i)
			{
				if (@equals(@this[i], item))
					return i;
			}

			return null;
		}
		/// <summary> Поиск индекса  первого вхождения подмассива в массив </summary>
		/// <typeparam name="T"> Тип элемента массива </typeparam>
		/// <param name="this"> Массив, в котором производится поиск подмассива </param>
		/// <param name="that"> Искомый подмассив </param>
		/// <param name="from"> Индекс, с которого нужно начинать поиск </param>
		/// <returns> Возвращает индекс элемента в @this, если такой есть или null в противном случае </returns>
		public static int? IndexOf<T>(this T[] @this, T[] that, int from = 0) where T : struct
		{
			if (@this == null)
				throw new ArgumentNullException(nameof(@this));
			if (that == null)
				throw new ArgumentNullException(nameof(that));

			if (that.Length == 0)
				return null;

			if (@this.Length - from < that.Length)
				return null;

			for (var i = from; i != @this.Length - that.Length + 1; ++i)
			{
				var j = 0;
				for (; j != that.Length; ++j)
				{
					var @thisElement = @this[i + j];
					var @thatElement = @that[j];

					if (!@thisElement.Equals(@thatElement))
						break;
				}
				if (j != that.Length)
					continue;

				return i;
			}

			return null;
		}
		public static T[] TakeToArray<T>(this T[] x, int count)
		{
			var result = new T[count];
			Array.Copy(x, 0, result, 0, count);
			return result;
		}
		public static T[][] SplitWithStarting<T>(this T[] x, T[] delimeter)
		{
			var results = new List<List<T>>();
			if (x.Length == 0)
				return results.Select(r => r.ToArray()).ToArray();

			List<T> result = null;
			for (var i = 0; i < x.Length; i++)
			{
				var j = 0;
				while (i + j < x.Length && j < delimeter.Length && x[i + j].Equals(delimeter[j]))
					j++;

				if (j == delimeter.Length)
				{
					result = new List<T>();
					results.Add(result);
				}

				if (result == null)
					continue;

				if (j == 0)
					result.Add(x[i]);
				else
				{
					for (var k = 0; k < j; k++)
						result.Add(x[i + k]);

					i += j - 1;
				}
			}

			return results.Where(r => r.Any()).Select(r => r.ToArray()).ToArray();
		}
		public static void AddRange<TCollection, TItem>(this TCollection collection, IEnumerable<TItem> items)
			where TCollection : class, ICollection<TItem>
		{
			if (collection == null)
				throw new ArgumentNullException(nameof(collection));

			if (items == null)
				return;

			foreach (var item in items)
			{
				collection.Add(item);
			}
		}
		/// <summary> Разбивает входную коллекцию на порции из не более чем count элементов </summary>
		/// <typeparam name="T"> Тип элемента коллекции </typeparam>
		/// <param name="items"> Исходная коллекция </param>
		/// <param name="count"> Размер порции </param>
		/// <returns></returns>
		public static IEnumerable<List<T>> SplitByCount<T>(this IEnumerable<T> items, int count)
		{
			if (items == null)
				throw new ArgumentNullException(nameof(items));

			if (count < 1)
				throw new ArgumentOutOfRangeException(nameof(count), count, "Value cannot be less then 1");

			var partition = new List<T>(count);
			foreach (var item in items)
			{
				partition.Add(item);
				if (partition.Count == count)
				{
					yield return partition;
					partition = new List<T>(count);
				}
			}
			if (0 < partition.Count)
				yield return partition;
		}
		public static int? IndexOf<T>(this IEnumerable<T> collection, Predicate<T> predicate)
		{
			var i = 0;
			foreach (var item in collection)
			{
				if (predicate(item))
					return i;
				++i;
			}
			return null;
		}
		public static HashSet<T> ToHashSet<T>(this IEnumerable<T> source)
		{
			return new HashSet<T>(source);
		}
		public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> action)
		{
			var size = enumeration?.Count() ?? 0;
			if (size == 0)
				return;
			for (int i = 0; i < size; i++)
				action?.Invoke(enumeration.ElementAt(i));
		}
		public static DataTable ToDataTable<T>(this IEnumerable<T> items, string tableName = null)
		{
			var dataTable = new DataTable(tableName ?? typeof(T).Name);

			//Get all the properties
			var props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
			foreach (PropertyInfo prop in props)
			{
				//Setting column names as Property names
				var dataCol = dataTable.Columns.Add(prop.Name);

				if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
				{
					dataCol.DataType = Nullable.GetUnderlyingType(prop.PropertyType);
					dataCol.AllowDBNull = true;
				}
				else
				{
					dataCol.DataType = prop.PropertyType;
					dataCol.AllowDBNull = !prop.PropertyType.IsValueType;
				}
			}
			foreach (T item in items)
			{
				var values = new object[props.Length];
				for (int i = 0; i < props.Length; i++)
				{
					//inserting property values to datatable rows
					values[i] = props[i].GetValue(item, null);
				}
				dataTable.Rows.Add(values);
			}
			//put a breakpoint here and check datatable
			return dataTable;
		}
	}
}