﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace FORIS.TSS.Common.Helpers
{
	public static class TraceHelper
	{
		#region Base trace
		[Conditional("TRACE")]
		public static void TraceInformation(this string s) { Trace.TraceInformation(s); }
		[Conditional("TRACE")]
		public static void TraceWarning    (this string s) { Trace.TraceWarning(s);     }
		[Conditional("TRACE")]
		public static void TraceError      (this string s) { Trace.TraceError(s);       }
		[Conditional("DEBUG")]
		public static void DebugInformation(this string s) { Trace.TraceInformation(s); }
		[Conditional("DEBUG")]
		public static void DebugWarning    (this string s) { Trace.TraceWarning(s);     }
		[Conditional("DEBUG")]
		public static void DebugError      (this string s) { Trace.TraceError(s);       }
		#endregion Base trace
		#region Call trace
		[MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static string CallTraceMessage(this string message, int level = 1)
		{
			return message.GetCallerMethodFullNameMessage(level + 1);
		}
		[Conditional("TRACE"), MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static void CallTraceInformation(this string s, int level = 1) { s.CallTraceMessage(level + 1).TraceInformation(); }
		[Conditional("TRACE"), MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static void CallTraceWarning    (this string s, int level = 1) { s.CallTraceMessage(level + 1).TraceWarning();     }
		[Conditional("TRACE"), MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static void CallTraceError      (this string s, int level = 1) { s.CallTraceMessage(level + 1).TraceError();       }
		[Conditional("DEBUG"), MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static void CallDebugInformation(this string s, int level = 1) { s.CallTraceMessage(level + 1).DebugInformation(); }
		[Conditional("DEBUG"), MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static void CallDebugWarning    (this string s, int level = 1) { s.CallTraceMessage(level + 1).DebugWarning();     }
		[Conditional("DEBUG"), MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static void CallDebugError      (this string s, int level = 1) { s.CallTraceMessage(level + 1).DebugError();       }
		#endregion Call trace
	}
}