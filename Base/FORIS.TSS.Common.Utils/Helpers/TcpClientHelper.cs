﻿using System;
using System.Net.Sockets;

namespace FORIS.TSS.Helpers
{
	public static class TcpClientHelper
	{
		public static void Connect(this TcpClient client, string host, int port, TimeSpan timeout)
		{
			var result = client.BeginConnect(host, port, null, null);
			var success = result.AsyncWaitHandle.WaitOne(timeout);
			if (!success)
				throw new TimeoutException("Couldn't connect to " + host + ":" + port);
			client.EndConnect(result);
		}
	}
}