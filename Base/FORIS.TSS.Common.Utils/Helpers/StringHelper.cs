﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace FORIS.TSS.Common.Helpers
{
	public static class StringHelper
	{
		public static double? GetDouble(this Match match, string name)
		{
			return match.Groups[name].Success
				? double.Parse(match.Groups[name].Value, NumberHelper.FormatInfo)
				: (double?)null;
		}
		public static int? GetInt(this Match match, string name)
		{
			return match.Groups[name].Success
				? int.Parse(match.Groups[name].Value)
				: (int?)null;
		}
		public static string Join(this IEnumerable<int> collection, string separator)
		{
			var result = new StringBuilder(collection.Count() * 12);

			var first = true;
			foreach (var item in collection)
			{
				if (first)
					first = false;
				else
					result.Append(separator);
				result.Append(item);
			}

			return result.ToString();
		}
		public static string Join(this IEnumerable<string> collection, string separator, bool ignoreEmpty = false)
		{
			if (collection == null)
				return null;

			var result = new StringBuilder();

			bool first = true;

			foreach (var item in collection)
			{
				if (ignoreEmpty && string.IsNullOrEmpty(item))
					continue;

				if (first)
					first = false;
				else
					result.Append(separator);

				result.Append(item);
			}

			return result.ToString();
		}
		public static string Join(this IEnumerable<long> collection, string separator)
		{
			var result = new StringBuilder(collection.Count() * 12);

			foreach (var item in collection)
			{
				result.Append(item);
				result.Append(separator);
			}

			return result.ToString();
		}
		public static string ConvertToSafeForUrlString(string symbols, byte[] bytes)
		{
			var bitCount = (int)Math.Log(symbols.Length, 2);

			var resultLength = bytes.Length * 8 / bitCount + ((bytes.Length * 8 % bitCount == 0) ? 0 : 1);
			var sb = new StringBuilder(resultLength);

			for (var i = 0; i != resultLength; ++i)
			{
				var byteIndex0 = i * bitCount / 8;
				var bitIndex0 = i * bitCount % 8;

				var byteIndex1 = ((i + 1) * bitCount - 1) / 8;
				var bitIndex1 = ((i + 1) * bitCount - 1) % 8;

				int charIndex;
				if (byteIndex0 == byteIndex1)
				{
					charIndex = ((bytes[byteIndex0] << (7 - bitIndex1)) & 0xFF) >> (8 - bitCount);
				}
				else
				{
					charIndex = (bytes[byteIndex0] >> bitIndex0);
					if (byteIndex1 != bytes.Length)
					{
						charIndex |= ((bytes[byteIndex1] << (7 - bitIndex1)) & 0xFF) >> (7 - bitCount + 1);
					}
				}

				sb.Append(symbols[charIndex]);
			}

			return sb.ToString();
		}
		public static string ToHexString(this byte[] bytes)
		{
			if (bytes == null)
				throw new ArgumentNullException("bytes");

			const string hexTable = "0123456789ABCDEF";

			var result = new StringBuilder(bytes.Length * 2);
			foreach (var @byte in bytes)
			{
				result.Append(hexTable[@byte / 16]);
				result.Append(hexTable[@byte % 16]);
			}
			return result.ToString();
		}
		public static byte[] HexStringToByteArray(this string hexString)
		{
			if (hexString == null)
				return null;
			var chars = hexString.Where(c => c.IsHexDigit()).ToArray();
			hexString = new string(chars);
			var res = new byte[hexString.Length / 2];
			for (int i = 0; i < hexString.Length - 1; i += 2)
			{
				byte b;
				if (byte.TryParse(hexString.Substring(i, 2), NumberStyles.HexNumber, null, out b))
					res[i / 2] = b;
				else
					return null;
			}
			return res;
		}
		public static byte[] DecStringToByteArray(this string decString)
		{
			if (decString == null)
				return null;
			var chars = decString.Where(char.IsDigit).ToArray();
			decString = new string(chars);
			var res = new byte[decString.Length / 2];
			for (int i = 0; i < decString.Length - 1; i += 2)
			{
				byte b;
				if (byte.TryParse(decString.Substring(i, 2), NumberStyles.Number, null, out b))
					res[i / 2] = b;
				else
					return null;
			}
			return res;
		}
		public static bool IsHexDigit(this char c)
		{
			return
				'0' <= c && c <= '9' ||
				'a' <= c && c <= 'f' ||
				'A' <= c && c <= 'F';
		}
		public static string CapitalizeFirstChar(this string s)
		{
			return ProcessFirstChar(s, part => part.ToUpper());
		}
		public static string FirstCharToLowercase(this string s)
		{
			return ProcessFirstChar(s, part => part.ToLower());
		}
		private static string ProcessFirstChar(string s, Func<string, string> f)
		{
			if (s == null)
				return null;
			if (s.Length == 0)
				return s;
			if (s.Length == 1)
				return f(s);
			return f(s.Substring(0, 1)) + s.Substring(1);
		}
		public static string SmartFormatBytes(byte[] bytes, int? count = null)
		{
			if (bytes == null)
				return null;

			string result;

			var asciiBytes = bytes.Take(count ?? bytes.Length).TakeWhile(b => 32 <= b && b <= 127 || b == '\n' || b == '\r').ToArray();

			if (asciiBytes.Length != 0)
			{
				result = Encoding.ASCII.GetString(asciiBytes);
				if (asciiBytes.Length != bytes.Length)
					result += "... " + BitConverter.ToString(bytes);
			}
			else
			{
				result = BitConverter.ToString(bytes);
			}

			return result;
		}
		public static string ToCamel(this string s)
		{
			if (string.IsNullOrWhiteSpace(s)) return s;
			if (!char.IsUpper(s[0]))
				return s;
			var sb = new StringBuilder(s.Length);
			sb.Append(s.Substring(0, 1).ToLower());
			if (1 < s.Length)
				sb.Append(s.Substring(1, s.Length - 1));
			return sb.ToString();
		}
		public static string FirstNonWhiteSpace(params string[] strings)
		{
			foreach (var s in strings)
			{
				if (string.IsNullOrWhiteSpace(s))
					continue;
				return s;
			}
			return null;
		}
		public static string UnescapeDataString(this string s)
		{
			return Uri.UnescapeDataString(s);
		}
		public static string EscapeDataString(this string s)
		{
			return Uri.EscapeDataString(s);
		}
		private static readonly Regex stripHtmlTagsRegex = new Regex(@"<(.|\n)*?>", RegexOptions.Compiled);
		public static string StripHtmlTags(this string s)
		{
			if (null == s)
				return s;
			return stripHtmlTagsRegex.Replace(s, string.Empty);
		}
	}
}