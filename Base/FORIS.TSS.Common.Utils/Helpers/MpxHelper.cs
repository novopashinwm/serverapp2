﻿using System;
using System.Globalization;
using System.Text;

namespace FORIS.TSS.Common.Helpers
{
	public static class MpxHelper
	{
		/// <summary>Строка-разделитель для разбиения передачи составных SMS-сообщений через платформу MPX</summary>
		public const string FeedString = "\f";

		/// <summary> Префикс, который используется для передачи в теле SMS маскированного номера - следующий за ним F(MSISDN) будет демаскирован
		/// на платформе перед доставкой SMS абоненту </summary>
		public const string AsidDemaskingPrefix = "##";

		public const string MpxTimeFormat = "yyyyMMddHHmmss";

		public static string FormatTime(DateTime dateTime)
		{
			return dateTime.ToString(MpxTimeFormat, CultureInfo.InvariantCulture);
		}

		public static string FormatCoordinate(double coordinate, char negativeHemisphere, char positiveHemisphere)
		{
			var sb = new StringBuilder(10);
			char semisphere;

			if (coordinate < 0)
			{
				semisphere = negativeHemisphere;
				coordinate = -coordinate;
			}
			else
			{
				semisphere = positiveHemisphere;
			}

			var wholeDegrees = (int)coordinate;
			var minutes = (coordinate - wholeDegrees) * 60;
			var wholeMinutes = (int)minutes;
			var seconds = (minutes - wholeMinutes) * 60;

			sb.Append(wholeDegrees);
			sb.Append(' ');
			sb.Append(wholeMinutes);
			sb.Append(' ');
			sb.Append(seconds.ToString(".0", CultureInfo.InvariantCulture));
			sb.Append(semisphere);

			return sb.ToString();
		}
	}
}