﻿using System.Linq;

namespace FORIS.TSS.Common.Helpers
{
	public static class DeviceNumberHelper
	{
		public static bool ValidTrackerNumber(string trackerNumber)
		{
			if (string.IsNullOrWhiteSpace(trackerNumber))
				return false;
			return trackerNumber.All(c => char.IsDigit(c) || 'A' <= c && c <= 'Z' || 'a' <= c && c <= 'z' || c == '_');
		}
	}
}