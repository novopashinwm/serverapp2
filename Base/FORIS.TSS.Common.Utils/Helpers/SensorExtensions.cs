﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic.Attributes;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Resources;

namespace FORIS.TSS.Common.Helpers
{
	/// <summary> Расширение для работы с датчиками <see cref="Sensor"/> </summary>
	public static class SensorExtensions
	{
		public static readonly Dictionary<SensorLegend, UnitOfMeasure> SensorLegendUnits =
			new Dictionary<SensorLegend, UnitOfMeasure>();
		static SensorExtensions()
		{
			// Заполняем единицы измерения для категорий датчиков
			var enumType = typeof(SensorLegend);
			foreach (SensorLegend sensorLegend in Enum.GetValues(enumType))
			{
				var member = enumType.GetMember(sensorLegend.ToString()).FirstOrDefault();
				if (member == null)
					continue;
				var measureAttribute = member
					.GetCustomAttributes(typeof(MeasureAttribute), false)
					.OfType<MeasureAttribute>()
					.FirstOrDefault();
				if (measureAttribute == null)
					continue;
				SensorLegendUnits.Add(sensorLegend, measureAttribute.Unit);
			}
		}
		public static Sensor GetDefaultSensor(this SensorLegend sensorLegend, ResourceContainers resourceContainers)
		{
			return new Sensor
			{
				Number     = sensorLegend,
				Type       = sensorLegend.GetSensorType(),
				LogRecords = Enumerable.Empty<SensorLogRecord>().ToList(),
			}
				.LocalizeSensorFull(resourceContainers);
		}
		public static SensorType GetSensorType(this SensorLegend sensorLegend)
		{
			if (SensorLegendUnits.ContainsKey(sensorLegend))
				return SensorType.Analog;
			return SensorType.Digital;
		}
		public static UnitOfMeasure? GetUnitOfMeasure(this SensorLegend sensorLegend)
		{
			UnitOfMeasure result;
			if (!SensorLegendUnits.TryGetValue(sensorLegend, out result))
				return null;
			return result;
		}
		/// <summary> Возвращает локализованное представление значения датчика </summary>
		/// <param name="sensor"> Сенсор </param>
		/// <param name="resourceContaines">Контейнер ресурсов</param>
		/// <param name="value"> Значение датчика </param>
		public static string GetDisplayedValue(this Sensor sensor, decimal value, ResourceContainers resourceContaines)
		{
			switch (sensor.Type)
			{
				case SensorType.Analog:
					return GetDisplayValueOfAnalogSensor(sensor, value, resourceContaines);
				case SensorType.Digital:
					return GetDisplayValueOfDigitalSensor(sensor, value, resourceContaines);
				default:
					return null;
			}
		}
		private static string GetDisplayValueOfAnalogSensor(Sensor sensor, decimal value, ResourceContainers resourceContaines)
		{
			return $"{value.ToString("0.###", NumberHelper.FormatInfo)} {sensor.Unit}"?.Trim();
		}
		private static string GetDisplayValueOfDigitalSensor(Sensor sensor, decimal value, ResourceContainers resourceContaines)
		{
			switch (sensor.Number)
			{
				case SensorLegend.Door:
				case SensorLegend.Door1:
				case SensorLegend.Door2:
				case SensorLegend.Door3:
				case SensorLegend.Door4:
				case SensorLegend.Door5:
				case SensorLegend.Door6:
				case SensorLegend.Door7:
					return resourceContaines.GetLocalizedMessagePart(value == 0 ? "ClosedForDoor"        : "OpenedForDoor");
				case SensorLegend.Door1Sabotage:
				case SensorLegend.Door2Sabotage:
				case SensorLegend.Door3Sabotage:
				case SensorLegend.Door4Sabotage:
				case SensorLegend.Door5Sabotage:
				case SensorLegend.Door6Sabotage:
				case SensorLegend.Door7Sabotage:
					return resourceContaines.GetLocalizedMessagePart(value == 0 ? "DoorXSabotageNo"      : "DoorXSabotageYes");
				case SensorLegend.Door1Offline:
				case SensorLegend.Door2Offline:
				case SensorLegend.Door3Offline:
				case SensorLegend.Door4Offline:
				case SensorLegend.Door5Offline:
				case SensorLegend.Door6Offline:
				case SensorLegend.Door7Offline:
					return resourceContaines.GetLocalizedMessagePart(value == 0 ? "No" : "Yes");
				case SensorLegend.TruckBody:
					return resourceContaines.GetLocalizedMessagePart(value == 0 ? "TruckBodyLowered"     : "TruckBodyRaised");
				case SensorLegend.UsedFuelType:
					return resourceContaines.GetLocalizedMessagePart(value == 0 ? "UsedFuelTypeGasoline" : "UsedFuelTypeGas");
				case SensorLegend.Tower:
					return resourceContaines.GetLocalizedMessagePart(value == 0 ? "TowerLowered"         : "TowerRaised");
				case SensorLegend.IsPlugged:
					return resourceContaines.GetLocalizedMessagePart(value == 0 ? "No"                   : "Yes");
				case SensorLegend.EngineMode: //TODO: добавить третье состояние - "выключен"
					return resourceContaines.GetLocalizedMessagePart(value == 0 ? "EngineIsIdling"       : "EngineIsUnderLoad");
			}
			return resourceContaines.GetLocalizedMessagePart(value == 0 ? "TurnedOff"                    : "TurnedOn");
		}
		/// <summary> Локализация описания (Meta) датчика </summary>
		/// <param name="resourceContainers"></param>
		/// <param name="sensor"></param>
		public static Sensor LocalizeSensorMeta(this Sensor sensor, ResourceContainers resourceContainers)
		{
			if (sensor == null || resourceContainers == null)
				return sensor;

			if (string.IsNullOrEmpty(sensor.Name))
				sensor.Name = resourceContainers[sensor.Number];

			if (string.IsNullOrEmpty(sensor.Unit))
			{
				var unitOfMeasure = sensor.Number.GetUnitOfMeasure();
				sensor.Unit = unitOfMeasure.HasValue
					? resourceContainers[unitOfMeasure.Value]
					: string.Empty;
			}
			if (sensor.Type == SensorType.Digital)
			{
				sensor.ValueNames = new[]
				{
					sensor.GetDisplayedValue(0, resourceContainers),
					sensor.GetDisplayedValue(1, resourceContainers)
				};
			}
			return sensor;
		}
		/// <summary> Локализация данных (Data) датчика </summary>
		/// <param name="resourceContainers"></param>
		/// <param name="sensor"></param>
		public static Sensor LocalizeSensorData(this Sensor sensor, ResourceContainers resourceContainers)
		{
			if (sensor == null || resourceContainers == null)
				return sensor;

			if (0 >= (sensor?.LogRecords?.Count ?? 0))
				return sensor;

			for (var i = 0; i < sensor.LogRecords.Count; i++)
			{
				var sensorLogRecord = sensor.LogRecords[i];
				sensor.LogRecords[i] = sensorLogRecord.SetDisplayedValue(
					sensor.GetDisplayedValue(sensorLogRecord.Value, resourceContainers));
			}
			return sensor;
		}
		/// <summary> Локализация описания (Meta) и данных (Data) датчика </summary>
		/// <param name="resourceContainers"></param>
		/// <param name="sensor"></param>
		public static Sensor LocalizeSensorFull(this Sensor sensor, ResourceContainers resourceContainers)
		{
			return sensor
				?.LocalizeSensorMeta(resourceContainers)
				?.LocalizeSensorData(resourceContainers);
		}
	}
}