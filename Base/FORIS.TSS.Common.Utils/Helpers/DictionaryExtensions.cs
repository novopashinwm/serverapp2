﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FORIS.TSS.Common.Helpers
{
	public static class DictionaryExtensions
	{
		public static IEnumerable<KeyValuePair<string, object>> Flatten(this Dictionary<string, object> dic, string key = null)
		{
			return dic
				?.SelectMany(kv =>
					kv.Value is Dictionary<string, object>
						? ((Dictionary<string, object>)kv.Value).Flatten(string.IsNullOrWhiteSpace(key) ? kv.Key : string.Join(".", key, kv.Key))
						: new List<KeyValuePair<string, object>>()
						{
						new KeyValuePair<string, object>(string.IsNullOrWhiteSpace(key)
							? kv.Key
							: string.Join(".", key, kv.Key), kv.Value)
						});
		}
		public static TValue? GetValueOrNull<TKey, TValue>(this IDictionary<TKey, TValue> dic, TKey key) where TValue : struct
		{
			var value = default(TValue);
			return (null != key && (dic?.TryGetValue(key, out value) ?? false))
				? value
				: default(TValue?);
		}
		public static TValue GetObjectOrNull<TKey, TValue>(this IDictionary<TKey, TValue> dic, TKey key) where TValue : class
		{
			var value = default(TValue);
			return (null != key && (dic?.TryGetValue(key, out value) ?? false))
				? value
				: default(TValue);
		}
		public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dic, TKey key, TValue defaultValue = default(TValue))
		{
			var value = defaultValue;
			return (null != key && (dic?.TryGetValue(key, out value) ?? false))
				? value
				: defaultValue;
		}
	}
}