﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Filtering;
using FORIS.TSS.Common.Filtering;

namespace FORIS.TSS.Common.Helpers
{
	/// <summary> Класс, содержащий расширения для чтения <see cref="FullLog"/> из базы данных</summary>
	public static class FullLogDatabaseReader
	{
		private static readonly HashSet<SensorLegend> DigitalSensorsToSkipFiltering = new HashSet<SensorLegend>
		{
			SensorLegend.MotionAlarm,
			SensorLegend.LineCrossed,
			SensorLegend.FaceDetected,
			SensorLegend.AbandonedAlarm,
			SensorLegend.LoiteringAlarm
		};

		public static void Read(this FullLog @this, IDataReader reader)
		{
			ReadData(@this, reader);

			FillMissingRuns(@this);
			FillInterruptions(@this);

			if (@this.Options.CutOutstandingData)
				CutOutstandingData(@this);

			if (@this.Options.MaxGeoPointsCount < @this.Geo.Count && @this.Options.IsGeoPointsFilterOn)
			{
				var filteredGeo = @this.Geo;
				for (var i = 0; i != 2; ++i)
					filteredGeo = FilterGeoPositions(@this, filteredGeo);

				var filteredGeoLogTimes = filteredGeo.Select(r => r.LogTime).ToList();
				var filteredGpsLogTimes = @this.FilterGpsPositions(@this.Gps).Select(r => r.LogTime).ToList();
				foreach (var interruption in @this.Interruptions)
				{
					var record = interruption;
					var leftPoint = @this.Gps.BinarySearch(r => record.LogTime.CompareTo(r.LogTime));
					if (!leftPoint.HasFrom || leftPoint.From + 1 >= @this.Gps.Count || leftPoint.From - 1 < 0)
						continue;

					var gpsLogBegoreInterruption = @this.Gps[leftPoint.From - 1];
					var gpsLogAfterInterruption = @this.Gps[leftPoint.From + 1];
					filteredGpsLogTimes.Add(gpsLogBegoreInterruption.LogTime);
					filteredGpsLogTimes.Add(gpsLogAfterInterruption.LogTime);
				}

				var logTimes = new SortedSet<int>(filteredGeoLogTimes.Concat(filteredGpsLogTimes)).ToList();

				var gps = new Dictionary<int, GPSLogRecord>();
				foreach (var record in @this.Gps)
					gps[record.LogTime] = record;
				var geo = new Dictionary<int, GeoLogRecord>();
				foreach (var record in @this.Geo)
					geo[record.LogTime] = record;
				var runs = new Dictionary<int, RunLogRecord>();
				foreach (var record in @this.Runs)
					runs[record.LogTime] = record;
				var accuracy = new Dictionary<int, AccuracyRecord>();
				foreach (var record in @this.Accuracy)
					accuracy[record.LogTime] = record;

				@this.Gps = new List<GPSLogRecord>(gps.Count);
				@this.Geo = new List<GeoLogRecord>(geo.Count);
				@this.Runs = new List<RunLogRecord>(runs.Count);
				@this.Accuracy = new List<AccuracyRecord>(accuracy.Count);
				foreach (var logTime in logTimes)
				{
					if (gps.ContainsKey(logTime))
						@this.Gps.Add(gps[logTime]);
					if (geo.ContainsKey(logTime))
						@this.Geo.Add(geo[logTime]);
					if (runs.ContainsKey(logTime))
						@this.Runs.Add(runs[logTime]);
					if (accuracy.ContainsKey(logTime))
						@this.Accuracy.Add(accuracy[logTime]);
				}
			}

			foreach (var sensor in @this.Sensors.Values)
			{
				if (sensor.Type != SensorType.Digital)
					continue;

				if (DigitalSensorsToSkipFiltering.Contains(sensor.Number))
					continue;
				// Фильтруем двоичные датчики без потери данных, просто пропускаем одинаковые точки
				var filter = new DigitalLosslessFilter<SensorLogRecord, bool>(
					item => item.LogTime, item => item.Value != 0,
					(int)@this.Options.NoDataPeriod.TotalSeconds);
				sensor.LogRecords = filter.Filter(sensor.LogRecords).ToList();
				// Пересчитываем статистику цифрового датчика, после фильтрации
				sensor.SensorStatistics = new List<SensorStatistics>
				{
					@this.CalculateSensorStatistics(sensor.Number, @this.LogTimeFrom, @this.LogTimeTo)
				};
			}
		}
		private static void FillMissingRuns(FullLog @this)
		{
			if (@this.Geo.Count < 2)
				return;

			if (@this.Runs.Count == 0)
			{
				@this.Runs.Add(new RunLogRecord
				{
					LogTime  = @this.Geo[0].LogTime,
					Odometer = 0
				});
			}

			var bsr = @this.Geo.BinarySearch(g => @this.Runs[@this.Runs.Count - 1].LogTime - g.LogTime);

			for (int i = bsr.HasFrom ? bsr.From : 0; i != @this.Geo.Count; ++i)
			{
				if (i == 0)
					continue;

				var prevPos = @this.Geo[i - 1];
				var currPos = @this.Geo[i];

				var lastRun = @this.Runs[@this.Runs.Count - 1];
				if (lastRun.LogTime == @this.Geo[i].LogTime)
					continue;

				var distance = GeoHelper.DistanceBetweenLocations(
					(double)prevPos.Lat,
					(double)prevPos.Lng,
					(double)currPos.Lat,
					(double)currPos.Lng);
				//Если скорость для перемещения от предыдущей точки > 1000км/ч, то отбраковываем позицию
				if (1000 < distance * 1000 / ((currPos.LogTime - prevPos.LogTime) / 3600.0))
					continue;

				@this.Runs.Add(new RunLogRecord
				{
					LogTime  = currPos.LogTime,
					Odometer = @this.Runs[@this.Runs.Count - 1].Odometer + (long)(distance * 100)
				});
			}
		}
		/// <summary> Прореживает точки, с тем, чтобы их суммарное количество не превышало <see cref="FullLogOptions.MaxPointsCount"/> </summary>
		private static void CutExtraPoints(FullLog @this)
		{
			if (@this.Geo.Count <= @this.Options.MaxGeoPointsCount)
				return;

			var result = new List<GeoLogRecord>(@this.Options.MaxPointsCount) { @this.Geo.First() };

			var lastI = 0;
			var factor = @this.Options.MaxPointsCount / (double)@this.Geo.Count;
			for (var i = 1; i != @this.Geo.Count - 1; ++i)
			{
				var currentI = (int)(factor * i);
				if (currentI == lastI)
					continue;
				lastI = currentI;
				result.Add(@this.Geo[i]);
			}

			result.Add(@this.Geo.Last());

			if (@this.Geo.Count != result.Count)
				@this.Filtered = true;

			@this.Geo = result;
		}
		/// <summary>
		/// Удаляет точки, дата которых выходит за пределы запрошенного интервала.
		/// При необходимости добавляет точки на границах запрошенного интервала, используя интерполяцию.
		/// </summary>
		private static void CutOutstandingData(FullLog @this)
		{
			@this.Gps = @this.CutOutstandingPoints(
				@this.Gps,
				(logTime, p0, p1) => new GPSLogRecord
				{
					LogTime = logTime,
					Speed   = (byte)(((p1.Speed ?? 0) - (p0.Speed ?? 0)) * (logTime - p0.LogTime) / (p1.LogTime - p0.LogTime) + p0.Speed ?? 0)
				});

			foreach (var sensor in @this.Sensors.Values)
			{
				sensor.LogRecords =
					sensor.Type == SensorType.Digital
					? @this.CutOutstandingPoints(
						sensor.LogRecords,
						(logTime, p0, p1) => new SensorLogRecord(logTime, p0.Value))
					: @this.CutOutstandingPoints(
						sensor.LogRecords,
						(logTime, p0, p1) => new SensorLogRecord(
							logTime,
							(p1.Value - p0.Value) * (logTime - p0.LogTime) / (p1.LogTime - p0.LogTime) + p0.Value)
					);
			}

			@this.Geo = @this.CutOutstandingPoints(
				@this.Geo,
				(logTime, p0, p1) => new GeoLogRecord
				{
					LogTime = logTime,
					Lat     = ((p1.Lat - p0.Lat) * (logTime - p0.LogTime) / (p1.LogTime - p0.LogTime) + p0.Lat),
					Lng     = ((p1.Lng - p0.Lng) * (logTime - p0.LogTime) / (p1.LogTime - p0.LogTime) + p0.Lng)
				}
				);

		}
		private static List<TItem> CutOutstandingPoints<TItem>(
			this FullLog @this,
			List<TItem> points, Func<int, TItem, TItem, TItem> interpolate) where TItem : ILogTimeContainer
		{
			if (points.Count == 0)
				return points;
			var result = new List<TItem>(points.Count);
			var index = points.FindIndex(item => @this.LogTimeFrom <= item.GetLogTime());
			if (0 <= index && index < points.Count)
			{
				int lastLogTime;
				var item = points[index];
				var logTime = item.GetLogTime();
				if (logTime == @this.LogTimeFrom)
				{
					result.Add(item);
					lastLogTime = @this.LogTimeFrom;
				}
				else
				{
					if (index != 0)
						result.Add(interpolate(@this.LogTimeFrom, points[index - 1], item));

					if (logTime < @this.LogTimeTo)
					{
						result.Add(item);
						lastLogTime = item.GetLogTime();
					}
					else
					{
						lastLogTime = @this.LogTimeFrom;
					}
				}

				for (; index < points.Count; ++index)
				{
					item = points[index];
					logTime = item.GetLogTime();
					if (logTime == lastLogTime)
						continue;
					if (logTime < @this.LogTimeTo)
					{
						lastLogTime = logTime;
						result.Add(item);
						continue;
					}

					if (index != 0)
						result.Add(item.GetLogTime() == @this.LogTimeTo
							? item
							: interpolate(@this.LogTimeTo, points[index - 1], points[index]));
					break;
				}
			}
			else
			{
				if (points.Any())
					result.Add(points.Last());
			}
			return result;
		}
		private static void ReadData(FullLog @this, IDataReader reader)
		{
			var fullLogTime = new Dictionary<int, bool>();

			ReadGeo              (@this, reader, fullLogTime);
			ReadGps              (@this, reader, fullLogTime);
			ReadAccuracy         (@this, reader, fullLogTime);
			ReadControllerSensors(@this, reader, fullLogTime);
			ReadPictures         (@this, reader, fullLogTime);
			ReadRuns             (@this, reader);
			ReadStatistics       (@this, reader);

			@this.FullLogTimes = fullLogTime.Keys.ToList();
			@this.FullLogTimes.Sort();
		}
		private static void ReadRuns(this FullLog @this, IDataReader reader)
		{
			if (!reader.NextResult() || !reader.Read())
				throw new ApplicationException("Unable to read row count for run statistics");

			var statLogRecordsCount = reader.GetInt32(0);

			if (!reader.NextResult())
				throw new ApplicationException("Unable to read row count for run statistics");

			@this.Runs = new List<RunLogRecord>(statLogRecordsCount);
			while (reader.Read())
				@this.Runs.Add(new RunLogRecord
				{
					LogTime  = reader.GetInt32(0),
					Odometer = reader.GetInt64(1)
				});

			var runDictionary = @this.Runs.ToDictionary(x => x.LogTime, x => x.Odometer);

			for (var i = 0; i != @this.Geo.Count; ++i)
			{
				var geo = @this.Geo[i];

				long run;
				if (!runDictionary.TryGetValue(geo.LogTime, out run))
					continue;
				geo.Run = run;

				@this.Geo[i] = geo;
			}
		}
		private static void ReadPictures(this FullLog @this, IDataReader reader, Dictionary<int, bool> fullLogTime)
		{
			if (!reader.NextResult() || !reader.Read())
				throw new ApplicationException("Unable to read row count for pictures");

			var count = reader.GetInt32(0);

			@this.Pictures = new List<Picture>(count);
			if (!reader.NextResult())
				throw new ApplicationException("Unable to read from table with pictures");

			var logTimeOrdinal = reader.GetOrdinal("log_time");
			while (reader.Read())
			{
				var logTime = reader.GetInt32(logTimeOrdinal);
				fullLogTime[logTime] = true;
				@this.Pictures.Add(new Picture { LogTime = logTime, Date = (logTime.ToUtcDateTime()) });
			}
		}
		private static void ReadStatistics(this FullLog @this, IDataReader reader)
		{
			if (!reader.NextResult() || !reader.Read())
				throw new ApplicationException("Unable to read table with statistics");

			@this.Run = reader.GetDecimal(0);
		}
		private static void ReadControllerSensors(this FullLog @this, IDataReader reader, Dictionary<int, bool> fullLogTime)
		{
			// 1. Маппинг датчиков
			@this.ReadSensorMappings(reader);
			if (!reader.NextResult() || !reader.Read())
				throw new ApplicationException("Unable to read table with vehicle attributes");
			// 2. Данные о размере бака
			var fuelTank = reader.IsDBNull(reader.GetOrdinal("Fuel_Tank"))
				? (decimal?)null
				: reader.GetDecimal(reader.GetOrdinal("Fuel_Tank"));
			var vehicleKind = (VehicleKind)reader.GetInt32(reader.GetOrdinal("Vehicle_Kind_ID"));
			// 3. Значения физических датчиков
			var sensorsValues = ReadSensorValues(reader);
			// 4. Получаем производные датчики
			foreach (var sensorMapping in @this.SensorMappings)
			{
				var logTimesDictionary = new Dictionary<int, bool>();
				foreach (var sm in sensorMapping.Value)
				{
					if (!sensorsValues.TryGetValue(sm.InputNumber, out InputSample sensorValues))
						continue;
					foreach (var logTime in sensorValues.LogTimes)
						logTimesDictionary[logTime] = true;
				}

				if (logTimesDictionary.Count == 0)
					continue; //Вообще нет значений
				var logTimes = logTimesDictionary.Keys.ToArray();
				Array.Sort(logTimes);
				var logRecords = new List<SensorLogRecord>(logTimesDictionary.Count);
				foreach (var logTime in logTimes)
				{
					decimal? mapValue = null;
					decimal? maxError = null;
					foreach (var m in sensorMapping.Value)
					{
						if (!sensorsValues.TryGetValue(m.InputNumber, out InputSample currentSensorValues))
							continue; //Вообще нет данных по этому входу
						var rawValue = currentSensorValues.Get(logTime, m.ValueExpired);
						if (rawValue == null || rawValue < m.MinValue || m.MaxValue < rawValue)
							continue;
						if (!mapValue.HasValue)
							mapValue = 0;
						mapValue += m.Multiplier * rawValue + m.Constant;

						if (m.Error.HasValue)
						{
							if (!maxError.HasValue || maxError.Value < m.Error)
								maxError = m.Error;
						}
					}

					if (mapValue.HasValue)
					{
						fullLogTime[logTime] = true;
						logRecords.Add(new SensorLogRecord
						(
							logTime,
							mapValue.Value,
							maxError
						));
					}
				}

				@this.Sensors[sensorMapping.Key].LogRecords = logRecords;
			}
			// 5. Читаем данные шины CAN
			@this.ReadSensorsFromCANLog(reader, vehicleKind, fuelTank, fullLogTime);
		}
		private static void ReadSensorsFromCANLog(this FullLog @this, IDataReader reader, VehicleKind vehicleKind, decimal? fuelTank, Dictionary<int, bool> fullLogTime)
		{
			if (!reader.NextResult())
				throw new ApplicationException("Unable to read from CAN log");

			if (vehicleKind == VehicleKind.MobilePhone ||
				vehicleKind == VehicleKind.Tracker)
				return;

			var canFuelSensor = @this.Sensors.ContainsKey(SensorLegend.CANFuel)
				? null
				: new Sensor
				{
					LogRecords = new List<SensorLogRecord>(),
					Number     = SensorLegend.CANFuel,
					Type       = SensorType.Analog
				};
			var canTotalRunSensor = @this.Sensors.ContainsKey(SensorLegend.CANTotalRun)
				? null
				: new Sensor
				{
					LogRecords = new List<SensorLogRecord>(),
					Number     = SensorLegend.CANTotalRun,
					Type       = SensorType.Analog
				};

			if (canFuelSensor == null && canTotalRunSensor == null)
				return;

			var logTimeOrdinal     = reader.GetOrdinal("log_time");
			var canFuelOrdinal     = reader.GetOrdinal("Fuel_Level1");
			var canTotalRunOrdinal = reader.GetOrdinal("Total_Run");

			var fuelTankFactor = (fuelTank ?? 0) / 1000.0m; //для расчета топлива по CAN сразу в литрах (в CAN_INFO указывается в десятых долях процента от бака)

			while (reader.Read())
			{
				var logTime = reader.GetInt32(logTimeOrdinal);
				fullLogTime[logTime] = true;
				if (canFuelSensor != null)
				{
					var value = reader.IsDBNull(canFuelOrdinal) ? (Int16?)null : reader.GetInt16(canFuelOrdinal);
					if (value != null && value != 0)
						canFuelSensor.LogRecords.Add(new SensorLogRecord(logTime, value.Value * fuelTankFactor));
				}

				if (canTotalRunSensor != null)
				{
					var value = reader.IsDBNull(canTotalRunOrdinal) ? (Int32?)null : reader.GetInt32(canTotalRunOrdinal);
					if (value != null && value != 0)
						canTotalRunSensor.LogRecords.Add(new SensorLogRecord(logTime, value.Value));
				}
			}

			if (canFuelSensor != null && canFuelSensor.LogRecords.Count != 0)
				@this.Sensors.Add(canFuelSensor.Number, canFuelSensor);

			if (canTotalRunSensor != null && canTotalRunSensor.LogRecords.Count != 0)
				@this.Sensors.Add(canTotalRunSensor.Number, canTotalRunSensor);
		}
		private static Dictionary<int, InputSample> ReadSensorValues(IDataReader reader)
		{
			var sensorsValueCounts = ReadSensorsValuesCounts(reader);
			if (!reader.NextResult())
				throw new ApplicationException("Unable to read table with sensor value counts");

			var logTimeOrdinal = reader.GetOrdinal("log_time");
			var numberOrdinal  = reader.GetOrdinal("Number");
			var valueOrdinal   = reader.GetOrdinal("value");

			var sensorInputRecords  = sensorsValueCounts.ToDictionary(
				pair => pair.Key,
				pair => new List<InputSample.InputRecord>(pair.Value));
			var currentSensorNumber = -1;

			var currentInputList = default(List<InputSample.InputRecord>);
			while (reader.Read())
			{
				var sensorNumber = reader.GetInt32(numberOrdinal);
				var logTime      = reader.GetInt32(logTimeOrdinal);
				var value        = reader.GetInt64(valueOrdinal);
				var inputRecord  = new InputSample.InputRecord(logTime, value);
				if (sensorNumber != currentSensorNumber)
				{
					currentInputList    = sensorInputRecords[sensorNumber];
					currentSensorNumber = sensorNumber;
				}

				currentInputList.Add(inputRecord);
			}

			return sensorInputRecords.ToDictionary(
				pair => pair.Key,
				pair => new InputSample(pair.Value, false));
		}
		private static Dictionary<int, int> ReadSensorsValuesCounts(IDataReader reader)
		{
			if (!reader.NextResult())
				throw new ApplicationException("Unable to read table with sensor values count");
			var sensorsValues       = new Dictionary<int, int>();
			var sensorNumberOrdinal = reader.GetOrdinal("Number");
			var countOrdinal        = reader.GetOrdinal("Count");
			while (reader.Read())
			{
				sensorsValues.Add(reader.GetInt32(sensorNumberOrdinal), reader.GetInt32(countOrdinal));
			}
			return sensorsValues;
		}
		private static void ReadSensorMappings(this FullLog @this, IDataReader reader)
		{
			if (!reader.NextResult())
				throw new ApplicationException("Unable to read table with sensor mappings");

			@this.SensorMappings = new Dictionary<SensorLegend, List<SensorMap>>();
			@this.Sensors        = new Dictionary<SensorLegend, Sensor>();

			var sensorLegendOrdinal = reader.GetOrdinal("SensorLegend");
			var sensorNumberOrdinal = reader.GetOrdinal("SensorNumber");
			var multiplierOrdinal   = reader.GetOrdinal("Multiplier");
			var constantOrdinal     = reader.GetOrdinal("Constant");
			var minValueOrdinal     = reader.GetOrdinal("Min_Value");
			var maxValueOrdinal     = reader.GetOrdinal("Max_Value");
			var valueExpiredOrdinal = reader.GetOrdinal("Value_Expired");
			var errorOrdinal        = reader.GetOrdinal("Error");
			var sensorTypeOrdinal   = reader.GetOrdinal("CONTROLLER_SENSOR_TYPE_ID");
			while (reader.Read())
			{
				var sensorLegend = (SensorLegend)reader.GetInt32(sensorLegendOrdinal);
				if (!@this.Sensors.ContainsKey(sensorLegend))
				{
					var sensor = new Sensor
					{
						LogRecords = new List<SensorLogRecord>(),
						Number     = sensorLegend,
						Type       = (SensorType)reader.GetInt32(sensorTypeOrdinal),
					};
					@this.Sensors[sensorLegend] = sensor;
				}

				var sensorMap = new SensorMap
				{
					SensorLegend = sensorLegend,
					InputNumber  = reader.GetInt32(sensorNumberOrdinal),
					Multiplier   = reader.IsDBNull(multiplierOrdinal)
						? 1
						: reader.GetDecimal(multiplierOrdinal),
					Constant     = reader.IsDBNull(constantOrdinal)
						? 0
						: reader.GetDecimal(constantOrdinal),
					MinValue     = reader.IsDBNull(minValueOrdinal)
						? (long?)null
						: reader.GetInt64(minValueOrdinal),
					MaxValue     = reader.IsDBNull(maxValueOrdinal)
						? (long?)null
						: reader.GetInt64(maxValueOrdinal),
					ValueExpired = reader.IsDBNull(valueExpiredOrdinal)
						? (int?)null
						: reader.GetInt32(valueExpiredOrdinal),
					Error        = reader.IsDBNull(errorOrdinal)
						? (decimal?)null
						: reader.GetDecimal(errorOrdinal),
				};
				if (!@this.SensorMappings.ContainsKey(sensorLegend))
				{
					@this.SensorMappings.Add(sensorLegend, new List<SensorMap>());
				}

				@this.SensorMappings[sensorLegend].Add(sensorMap);
			}
		}
		private static void ReadAccuracy(
			this FullLog @this,
			IDataReader reader, Dictionary<int, bool> fullLogTime)
		{
			if (!reader.NextResult() || !reader.Read())
				throw new ApplicationException("Unable to read table with position radius records count");
			var positionAccuracyRecordsCount = reader.GetInt32(0);
			if (!reader.NextResult())
				throw new ApplicationException("Unable to read table with geo log records");
			@this.Accuracy = new List<AccuracyRecord>(positionAccuracyRecordsCount);
			while (reader.Read())
			{
				var logTime = reader.GetInt32(0);
				fullLogTime[logTime] = true;
				var accuracyRecord = new AccuracyRecord
				{
					LogTime = logTime,
					Radius  = reader.IsDBNull(1) ? 0 : reader.GetInt32(1),
					Type    = (PositionType)reader.GetByte(2)
				};
				@this.Accuracy.Add(accuracyRecord);
			}

			var accuracyDictionary = @this.Accuracy.ToDictionary(x => x.LogTime);
			var gpsDictionary = @this.Gps.ToDictionary(x => x.LogTime);
			for (var i = 0; i != @this.Geo.Count; ++i)
			{
				var geo = @this.Geo[i];
				AccuracyRecord accuracy;
				if (accuracyDictionary.TryGetValue(geo.LogTime, out accuracy))
				{
					geo.Radius = accuracy.Radius;
					geo.Type   = accuracy.Type;
				}
				else
				{
					geo.Type = PositionType.GPS;
				}
				GPSLogRecord gps;
				if (gpsDictionary.TryGetValue(geo.LogTime, out gps))
				{
					geo.Altitude = gps.Altitude;
					geo.Course   = gps.Course;
					geo.Speed    = gps.Speed;
				}
				@this.Geo[i] = geo;
			}
		}
		private static void ReadGps(FullLog @this, IDataReader reader, Dictionary<int, bool> fullLogTime)
		{
			if (!reader.NextResult() || !reader.Read())
				throw new ApplicationException("Unable to read table with gps log records count");
			var gpsLogRecordsCount = reader.GetInt32(0);
			if (!reader.NextResult())
				throw new ApplicationException("Unable to read table with gps log records");

			@this.Gps = new List<GPSLogRecord>(gpsLogRecordsCount);

			var logTimeOrdinal    = reader.GetOrdinal("log_time");
			var speedOrdinal      = reader.GetOrdinal("speed");
			var courseOrdinal     = reader.GetOrdinal("course");
			var satellitesOrdinal = reader.GetOrdinal("satellites");
			var altitudeOrdinal   = reader.GetOrdinal("altitude");

			while (reader.Read())
			{
				var logTime = reader.GetInt32(logTimeOrdinal);
				fullLogTime[logTime] = true;
				var gpsRecord = new GPSLogRecord
				{
					LogTime = logTime,
					Speed = reader.IsDBNull(speedOrdinal)
						? (byte?)null
						: reader.GetByte(speedOrdinal),
					Course = reader.IsDBNull(courseOrdinal)
						? (short?)null
						: (short)(reader.GetByte(courseOrdinal) * 360 / 256),
					Satellites = (byte)(reader.IsDBNull(satellitesOrdinal)
						? 0
						: reader.GetByte(satellitesOrdinal)),
					Altitude = reader.IsDBNull(altitudeOrdinal) ? (int?)null : (int)Math.Round(reader.GetInt32(altitudeOrdinal) / 100m)
				};
				@this.Gps.Add(gpsRecord);
			}
		}
		private static void ReadGeo(FullLog @this, IDataReader reader, Dictionary<int, bool> fullLogTime)
		{
			if (!reader.Read())
				throw new ApplicationException("Unable to read table with geo log records count");
			var geoLogRecordsCount = reader.GetInt32(0);
			if (!reader.NextResult())
				throw new ApplicationException("Unable to read table with geo log records");
			@this.Geo = new List<GeoLogRecord>(geoLogRecordsCount);
			while (reader.Read())
			{
				var logTime = reader.GetInt32(0);
				fullLogTime[logTime] = true;
				@this.Geo.Add(new GeoLogRecord
				{
					LogTime = logTime,
					Lng     = reader.GetDecimal(1),
					Lat     = reader.GetDecimal(2),
				});
			}
		}
		private static void FillInterruptions(FullLog @this)
		{
			@this.Interruptions = new List<IntervalRecord>();

			var allLogTimes = @this.GetAllLogTimesWithBoundaries(false);
			var geoLogTimes = @this.GetLogTimesWithBoundaries(@this.Gps.Select(r => r.LogTime).ToList());
			@this.Interruptions = @this.GetInterruptions(allLogTimes, @this.Options.NoDataPeriod.TotalSeconds);
			@this.GpsAbsence    = @this.GetInterruptions(geoLogTimes, @this.Options.NoDataPeriod.TotalSeconds);
		}
		public static FullLog Filter(this FullLog @this)
		{
			if (!@this.Filtered)
			{
				FilterSpeed(@this);
				FilterSensors(@this);
				CutExtraPoints(@this);
			}
			return @this;
		}
		private static void FilterSpeed(FullLog @this)
		{
			var countBeforeFilter = @this.Gps.Count;

			var filter = new ResolutionFilter<GPSLogRecord>(
				@this.Options.MaxPointsCount, item => item.LogTime, item => item.Speed ?? 0);
			@this.Gps = filter.Filter(@this.Gps).ToList();

			if (countBeforeFilter < @this.Gps.Count)
				@this.Filtered = true;
		}
		internal static int ErrorInMetersBySpeed(this FullLog @this, int speed, bool? ignition)
		{
			return @this.Options.PositionAccuracy ?? (
				(ignition ?? true)
				? (speed < 15 ? 50 : 10)
				: (speed < 10 ? 100 : 10));
		}
		private static int ErrorInMetersByLogTime(this FullLog @this, int logTime)
		{
			var positionRadiusRecord = FullLogHelper.BinarySearch(@this.Accuracy, logTime);
			if (positionRadiusRecord != null)
				return positionRadiusRecord.Value.Radius;

			return @this.ErrorInMetersBySpeed(@this.SpeedByLogTime(logTime) ?? 0, @this.IgnitionByLogTime(logTime));
		}
		private static List<GeoLogRecord> FilterGeoPositions(
			FullLog @this,
			List<GeoLogRecord> input)
		{
			if (input.Count < 3)
				return input;

			Func<GeoLogRecord, double> latError;
			Func<GeoLogRecord, double> lngError;

			var positionAccuracyClosure = @this.Options.PositionAccuracy;
			if (positionAccuracyClosure != null)
			{
				latError = item => positionAccuracyClosure.Value * 360.0 / 40000000;
				lngError = item => (360.0 * (positionAccuracyClosure.Value / (40000000 * Math.Cos((double)item.Lat / 180.0 * Math.PI))));
			}
			else
			{
				latError = item => @this.ErrorInMetersByLogTime(item.LogTime) * 360.0 / 40000000;
				lngError = item => 360.0 * (@this.ErrorInMetersByLogTime(item.LogTime) / (40000000 * Math.Cos((double)item.Lat / 180.0 * Math.PI)));
			}

			var latSwingingDoor = new SwingingDoor<GeoLogRecord>(
				item => item.LogTime,
				item => (double)item.Lat,
				latError);

			var lngSwingingDoor = new SwingingDoor<GeoLogRecord>(
				item => item.LogTime,
				item => (double)item.Lng,
				lngError);

			var latRecords = latSwingingDoor.Filter(input).ToList();
			var lngRecords = lngSwingingDoor.Filter(input).ToList();

			var logRecords = new List<GeoLogRecord>(latRecords.Count + lngRecords.Count);
			for (int latIndex = 0, lngIndex = 0; latIndex != latRecords.Count || lngIndex != lngRecords.Count; )
			{
				var latPoint = latRecords[latIndex];
				var lngPoint = lngRecords[lngIndex];

				if (latPoint.LogTime == lngPoint.LogTime)
				{
					logRecords.Add(latPoint);
					++lngIndex;
					++latIndex;
				}
				else if (latPoint.LogTime < lngPoint.LogTime)
				{
					logRecords.Add(latPoint);
					++latIndex;
				}
				else //if (lngPoint.LogTime < latPoint.LogTime)
				{
					logRecords.Add(lngPoint);
					++lngIndex;
				}
			}

			if (@this.Geo.Count != logRecords.Count)
				@this.Filtered = true;

			return logRecords;
		}
		private static List<GPSLogRecord> FilterGpsPositions(
			this FullLog @this,
			List<GPSLogRecord> input)
		{
			if (input.Count < 3)
				return input;

			var fromLogTime = input.First().LogTime;
			var toLogTime = input.Last().LogTime;
			var interval = Math.Ceiling((toLogTime - fromLogTime) / (double)@this.Options.MaxGeoPointsCount);
			var result = new List<GPSLogRecord>(input.Count)
			{
				input.First()
			};

			for (var i = 1; i < input.Count - 2; i++)
			{
				var inGpsRecord = input[i];
				if (inGpsRecord.Speed != 0)
					continue;

				GPSLogRecord outGpsRecord;
				do
				{
					i++;
					outGpsRecord = input[i];
				} while (input[i + 1].Speed == 0 && i < input.Count - 2);

				if (outGpsRecord.LogTime - inGpsRecord.LogTime <= interval)
					continue;

				result.Add(inGpsRecord);
				result.Add(outGpsRecord);
			}

			result.Add(input.Last());
			return result;
		}
		private static void FilterSensors(this FullLog @this)
		{
			var filters = new Dictionary<SensorLegend, IFilter<SensorLogRecord>>();

			foreach (var sensor in @this.Sensors.Values)
			{
				IFilter<SensorLogRecord> filter = new ResolutionFilter<SensorLogRecord>(
					@this.Options.MaxPointsCount, item => (double)item.LogTime, item => (double)item.Value);
				//const int FuelTank = 910;
				//const double FuelRelativeError = 0.05;
				//switch (sensor.Type)
				//{
				//    case SensorType.Analog:
				//        Func<SensorLogRecord, double> getError;
				//        switch (sensor.Number)
				//        {
				//            case SensorLegend.Fuel:
				//            case SensorLegend.CANFuel:
				//                getError = delegate { return FuelTank * FuelRelativeError; };
				//                break;
				//            default:
				//                getError = delegate { return 0.5; /*Актуально для датчика температуры, других аналоговых датчиков пока нет */ };
				//                break;
				//        }
				//        filter = new SwingingDoor<SensorLogRecord>(item => item.LogTime, item => (double)item.Value, getError);
				//        break;
				//    case SensorType.Digital:
				//        filter = new DigitalLosslessFilter<SensorLogRecord, int, bool>(item => item.LogTime, item => item.Value != 0);
				//        break;
				//    default:
				//        throw new NotSupportedException(sensor.Type.ToString());
				//}
				filters.Add(sensor.Number, filter);
			}

			foreach (var sensor in @this.Sensors.Values)
			{
				var list  = sensor.LogRecords;
				var count = sensor.LogRecords.Count;
				sensor.LogRecords = new List<SensorLogRecord>(filters[sensor.Number].Filter(list));
				if (sensor.LogRecords.Count != count)
					@this.Filtered = true;
			}
		}
	}
}