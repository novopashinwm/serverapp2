﻿using System.Globalization;
using System.Linq;
using System.Xml.Linq;

namespace FORIS.TSS.Common.Helpers
{
	public static class XDocumentHelper
	{
		public static double? GetDouble(this XElement document, string name)
		{
			var nodeValue = document
				?.Descendants(name)
				?.FirstOrDefault()
				?.Value;
			if (nodeValue == null)
				return null;
			if (!double.TryParse(nodeValue, NumberStyles.Any, NumberHelper.FormatInfo, out var result))
				return null;
			return result;
		}
		public static int? GetInt32(this XElement document, string name)
		{
			var nodeValue = document
				?.Descendants(name)
				?.FirstOrDefault()
				?.Value;
			if (nodeValue == null)
				return null;
			if (!int.TryParse(nodeValue, out var result))
				return null;
			return result;
		}
		public static long? GetInt64(this XElement document, string name)
		{
			var nodeValue = document
				?.Descendants(name)
				?.FirstOrDefault()
				?.Value;
			if (nodeValue == null)
				return null;
			if (!long.TryParse(nodeValue, out var result))
				return null;
			return result;
		}
	}
}