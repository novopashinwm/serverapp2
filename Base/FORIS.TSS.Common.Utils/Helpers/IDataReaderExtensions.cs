﻿using System;
using System.Data;

namespace FORIS.TSS.Common.Helpers
{
	/// <summary> Расширения для более удобной работы с интерфейсом <see cref="IDataReader"/> </summary>
	public static class IDataReaderExtensions
	{
		/// <summary> Возвращает строку, если значение в колонке не равно NULL </summary>
		public static string GetStringOrNull(this IDataReader reader, int ordinal)
		{
			return reader.IsDBNull(ordinal) ? null : reader.GetString(ordinal);
		}
		/// <summary> Возвращает дату и время, если значение в колонке не равно NULL </summary>
		public static DateTime? GetDateTimeOrNull(this IDataReader reader, int ordinal)
		{
			return reader.IsDBNull(ordinal) ? (DateTime?)null : reader.GetDateTime(ordinal);
		}
		/// <summary> Возвращает целое число, если значение в колонке не равно NULL </summary>
		public static int? GetInt32OrNull(this IDataReader reader, int ordinal)
		{
			return reader.IsDBNull(ordinal) ? (int?)null : reader.GetInt32(ordinal);
		}
		/// <summary> Возвращает true, если значение в колонке не равно NULL и не равно false </summary>
		public static bool? GetTrueOrNull(this IDataReader reader, int ordinal)
		{
			if (reader.IsDBNull(ordinal))
				return null;

			if (!reader.GetBoolean(ordinal))
				return null;

			return true;
		}
	}
}