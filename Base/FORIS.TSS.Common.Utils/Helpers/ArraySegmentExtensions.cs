﻿using System;

namespace FORIS.TSS.Common.Helpers
{
	public static class ArraySegmentExtensions
	{
		public static ArraySegment<T> ReadNext<T, R>(this ArraySegment<T> prevSegment, uint count, Func<ArraySegment<T>, R> resultGetter, out R result)
		{
			result = default;
			if (prevSegment == default)
				return default;
			return prevSegment.ReadNext(
				(prev) => prev.GetArraySegmentByCount(count),
				resultGetter,
				out result);
		}
		public static ArraySegment<T> ReadNext<T, R>(this ArraySegment<T> prevSegment, Func<ArraySegment<T>, ArraySegment<T>> currSegmentGetter, Func<ArraySegment<T>, R> resultGetter, out R result)
		{
			result = default;
			if (prevSegment == default)
				return default;
			var currSegment = currSegmentGetter(prevSegment);
			result = resultGetter(currSegment);
			return currSegment;
		}
		public static ArraySegment<T> ReadNext<T>(this ArraySegment<T> prevSegment, uint count, Action<ArraySegment<T>> action)
		{
			if (prevSegment == default)
				return default;
			return prevSegment.ReadNext(
				(prev) => prev.GetArraySegmentByCount(count),
				action);
		}
		public static ArraySegment<T> ReadNext<T>(this ArraySegment<T> prevSegment, Func<ArraySegment<T>, ArraySegment<T>> currSegmentGetter, Action<ArraySegment<T>> action)
		{
			if (prevSegment == default)
				return default;
			var currSegment = currSegmentGetter(prevSegment);
			action(currSegment);
			return currSegment;
		}
		public static ArraySegment<T> GetArraySegmentByCount<T>(this ArraySegment<T> prevSegment, uint count)
		{
			if (prevSegment == default)
				return default;
			return prevSegment.Count == prevSegment.Array.Length
				// Сегмент всего массива
				? new ArraySegment<T>(prevSegment.Array, 0, (int)count)
				// Сегмент части массива
				: new ArraySegment<T>(prevSegment.Array, prevSegment.Offset + prevSegment.Count, (int)count);
		}
	}
}