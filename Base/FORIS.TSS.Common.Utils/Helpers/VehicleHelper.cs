﻿using System;
using System.Globalization;
using System.Linq;
using System.Xml;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.Resources;

namespace FORIS.TSS.Common.Helpers
{
	public static class VehicleHelper
	{
		public static void WriteXml(this Vehicle @this, XmlTextWriter writer)
		{
			writer.WriteStartElement("vehicle");
			writer.WriteAttributeString("id", @this.id.ToString());
			writer.WriteAttributeString("old", @this.isOld.ToString().ToLower());
			writer.WriteAttributeString("vehicleKind", @this.vehicleKind.ToString());

			if (@this.posLogTime != null)
			{
				writer.WriteAttributeString("posLogTime", @this.posLogTime.Value.ToString(CultureInfo.InvariantCulture));

				if (@this.course != null)
				{
					int rest = @this.course.Value % 5;
					var courseUpTo5Degrees = (int)(Math.Round((double)(@this.course) - rest));
					writer.WriteAttributeString("angle", courseUpTo5Degrees.ToString(CultureInfo.InvariantCulture));
					writer.WriteAttributeString("angle2", ((int)(@this.course / 10)).ToString(CultureInfo.InvariantCulture));
				}
				writer.WriteAttributeString("positionInfoId", @this.id.ToString(CultureInfo.InvariantCulture));
				if (@this.lng != null && @this.lat != null)
				{
					writer.WriteAttributeString("longitude", @this.lng.Value.ToString(NumberHelper.FormatInfo));
					writer.WriteAttributeString("latitude", @this.lat.Value.ToString(NumberHelper.FormatInfo));
				}
				if (@this.status != null)
					writer.WriteAttributeString("status", ((int)(@this.status.Value)).ToString(CultureInfo.InvariantCulture));
				writer.WriteAttributeString("validPosition", (@this.isValid).ToString().ToLower());
				if (@this.latCP != null && @this.lngCP != null)
				{
					writer.WriteAttributeString("latitudeCP", @this.latCP.Value.ToString(NumberHelper.FormatInfo));
					writer.WriteAttributeString("longitudeCP", @this.lngCP.Value.ToString(NumberHelper.FormatInfo));
				}

				if (@this.IsLBSPosition != null)
					writer.WriteAttributeString("IsLBSPosition", @this.IsLBSPosition.Value.ToString());

				if (@this.radius != null)
					writer.WriteAttributeString("radius", @this.radius.Value.ToString(NumberHelper.FormatInfo));
			}

			writer.WriteAttributeString("GarageNumber", @this.Name);
			writer.WriteAttributeString("RegNumber", @this.regNum);

			writer.WriteAttributeString("currSpeed", @this.speed.ToString());
			writer.WriteAttributeString("ignition", @this.ignition ?? false ? "on" : "off");
			// AS: адрес
			writer.WriteAttributeString("address", @this.address ?? "");

			writer.WriteAttributeString("iconStatus", @this.StatusIcon.ToString());

			writer.WriteStartElement("settings");
			if (!string.IsNullOrEmpty(@this.iconUrl))
			{
				//<settings>

				writer.WriteElementString("icon", @this.iconUrl);

				//</settings>
			}
			writer.WriteElementString("defaultIcon", @this.DefaultIconUrl);
			writer.WriteEndElement();
			// Возможности контроллера
			if (@this.capabilities != null)
			{
				writer.WriteStartElement("Capabilities");
				foreach (var capability in @this.capabilities)
					writer.WriteElementString("Capability", capability.Key.ToString());
				writer.WriteEndElement(); // Возможности контроллера
			}
			//TODO: реализовать сериализацию команд

			if (@this.Sensors != null && @this.Sensors.Count != 0)
			{
				writer.WriteStartElement("Sensors");
				foreach (var sensor in @this.Sensors)
				{
					if (sensor.LogRecords == null || sensor.LogRecords.Count == 0)
						continue;

					writer.WriteStartElement("Sensor");
					writer.WriteAttributeString("Legend", sensor.Number.ToString());
					writer.WriteAttributeString("Type",   sensor.Type.ToString());
					writer.WriteAttributeString("Value",  XmlConvert.ToString(
						sensor.LogRecords
							.OrderByDescending(r => r.LogTime)
							.FirstOrDefault()
							.Value));
					writer.WriteEndElement();

				}
				writer.WriteEndElement();
			}

			writer.WriteEndElement(); // vehicle
		}

		/// <summary> Локализация описаний (Meta) датчиков </summary>
		/// <param name="vehicle"></param>
		/// <param name="resourceContainers"></param>
		public static void LocalizeSensorsMeta(this Vehicle vehicle,
			ResourceContainers resourceContainers)
		{
			if (null == vehicle.Sensors)
				return;
			foreach (var sensor in vehicle.Sensors)
				sensor.LocalizeSensorMeta(resourceContainers);
		}

		/// <summary> Локализация описаний (Meta) датчиков и данных (Data) </summary>
		/// <param name="vehicle"></param>
		/// <param name="resourceContainers"></param>
		public static void LocalizeSensorsFull(this Vehicle vehicle,
			ResourceContainers resourceContainers)
		{
			if (null == vehicle.Sensors)
				return;
			// Очищаем все датчики без значений
			vehicle.Sensors.RemoveAll(s => s.LogRecords == null);

			foreach (var sensor in vehicle.Sensors)
				sensor.LocalizeSensorFull(resourceContainers);
		}
	}
}