﻿using System;
using System.Data.SqlClient;

namespace FORIS.TSS.Common.Helpers
{
	public static class ExceptionHelper
	{
		public static bool IsSqlTimeoutException(this Exception exception)
		{
			if (exception == null)
				return false;

			var sqlException = exception as SqlException;
			if (sqlException == null)
			{
				if (exception.InnerException == null)
					return false;

				sqlException = exception.InnerException as SqlException;
				if (sqlException == null)
					return false;
			}

			return sqlException.Number == -1    // нет связи с сервером
				|| sqlException.Number == -2    // таймаут
				|| sqlException.Number == 1205  // транзакция жертва
				|| sqlException.Number == 233;  // дисконнект

		}
		private static string GetExceptionMessage(this Exception ex, bool stackTrace)
		{
			if (null == ex)
				return default;
			return string.Concat(
				$"\nERROR:\n{ex.GetTypeName()}('{ex.Message?.Trim()}')",
				stackTrace ? $"\nSTACK:\n{ex.StackTrace?.Trim()}" : string.Empty);
		}
		public static string WithException(this string message, Exception ex, bool stackTrace = false)
		{
			if (null == ex)
				return message;
			return string.Concat(
				message?.Trim()                    ?? string.Empty,
				ex.GetExceptionMessage(stackTrace) ?? string.Empty);
		}
	}
}