﻿using System;

namespace FORIS.TSS.Common.Helpers
{
	public static class TimeSpanHelper
	{
		public static string FormatTimeOfDay(this TimeSpan timeSpan)
		{
			return timeSpan.Hours.ToString().PadLeft(2, '0') + ":" + timeSpan.Minutes.ToString().PadLeft(2, '0');
		}

	}
}