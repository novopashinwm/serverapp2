﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace FORIS.TSS.Common.Helpers
{
	public static class JsonHelper2
	{
		private static readonly Func<JsonSerializerSettings> defaultSettings = () =>
		{
			var jsonSerializerSettings = new JsonSerializerSettings
			{
				Formatting        = Newtonsoft.Json.Formatting.Indented,
				NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
				ContractResolver  = (new PropertyRenameAndIgnoreSerializerContractResolver())
					// Имена свойств нужно писать в Camel формате
					.IgnoreProperties(typeof(BusinessLogic.DTO.Command), "isActual"),
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
			};
			jsonSerializerSettings.Converters.Add(
				new StringEnumConverter());
			jsonSerializerSettings.Converters.Add(
				new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss.fff'Z'" });
			jsonSerializerSettings.Converters.Add(
				new TimespanConverter());
			return jsonSerializerSettings;
		};
		////////////////////////////////////////////////////////////////////////////////
		public class TimespanConverter : JsonConverter
		{
			public override bool CanConvert(Type objectType)
			{
				return objectType == typeof(TimeSpan) || objectType == typeof(TimeSpan?);
			}
			public override bool CanRead => true;
			public override bool CanWrite => true;
			public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
			{
				if (objectType != typeof(TimeSpan) && objectType != typeof(TimeSpan?))
					throw new ArgumentException();
				var spanString = reader.Value as string;
				if (spanString == null)
					return null;
				return XmlConvert.ToTimeSpan(spanString);
			}
			public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
			{
				if (value is TimeSpan)
					writer.WriteValue(XmlConvert.ToString((TimeSpan)value));
				else if (value is TimeSpan? && ((TimeSpan?)value).HasValue)
					writer.WriteValue(XmlConvert.ToString(((TimeSpan?)value).Value));
			}
		}
		////////////////////////////////////////////////////////////////////////////////
		public class PropertyRenameAndIgnoreSerializerContractResolver : CamelCasePropertyNamesContractResolver
		{
			private readonly Dictionary<Type, HashSet<string>>            _ignores;
			private readonly Dictionary<Type, Dictionary<string, string>> _renames;
			public PropertyRenameAndIgnoreSerializerContractResolver()
			{
				_ignores = new Dictionary<Type, HashSet<string>>();
				_renames = new Dictionary<Type, Dictionary<string, string>>();
			}
			/// <summary> Указать игнорируемые у конкретного типа свойства </summary>
			/// <param name="type"> Тип </param>
			/// <param name="jsonPropertyNames"> Имена игнорируемых свойств </param>
			/// <returns></returns>
			/// <remarks> Т.к. PropertyRenameAndIgnoreSerializerContractResolver унаследован от CamelCasePropertyNamesContractResolver
			/// Имена свойств нужно писать в Camel формате
			///</remarks>
			public PropertyRenameAndIgnoreSerializerContractResolver IgnoreProperties(
				Type type, params string[] jsonPropertyNames)
			{
				if (!_ignores.ContainsKey(type))
					_ignores[type] = new HashSet<string>();
				foreach (var prop in jsonPropertyNames)
					_ignores[type].Add(prop);
				return this;
			}
			public PropertyRenameAndIgnoreSerializerContractResolver RenameProperty(
				Type type, string propertyName, string newJsonPropertyName)
			{
				if (!_renames.ContainsKey(type))
					_renames[type] = new Dictionary<string, string>();
				_renames[type][propertyName] = newJsonPropertyName;
				return this;
			}
			protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
			{
				var property = base.CreateProperty(member, memberSerialization);

				if (IsIgnored(property.DeclaringType, property.PropertyName))
				{
					property.ShouldSerialize = i => false;
					property.Ignored = true;
				}
				var newJsonPropertyName = default(string);
				if (IsRenamed(property.DeclaringType, property.PropertyName, out newJsonPropertyName))
					property.PropertyName = newJsonPropertyName;

				return property;
			}
			private bool IsIgnored(Type type, string jsonPropertyName)
			{
				if (!_ignores.ContainsKey(type))
					return false;

				return _ignores[type].Contains(jsonPropertyName);
			}
			private bool IsRenamed(Type type, string jsonPropertyName, out string newJsonPropertyName)
			{
				Dictionary<string, string> renames;

				if (!_renames.TryGetValue(type, out renames) || !renames.TryGetValue(jsonPropertyName, out newJsonPropertyName))
				{
					newJsonPropertyName = null;
					return false;
				}
				return true;
			}
		}
		////////////////////////////////////////////////////////////////////////////////
		public static TTarget Convert<TSource, TTarget>(
			TSource sourceItem, ObjectCreationHandling objectCreationHandling = ObjectCreationHandling.Replace)
		{
			if (Equals(sourceItem, default(TSource)))
				return default(TTarget);
			var jsonSerializerSettings = JsonConvert.DefaultSettings?.Invoke() ?? new JsonSerializerSettings();
			jsonSerializerSettings.ObjectCreationHandling = objectCreationHandling;
			return JsonConvert.DeserializeObject<TTarget>(
				JsonConvert.SerializeObject(sourceItem, jsonSerializerSettings));
		}
		////////////////////////////////////////////////////////////////////////////////
		public static string ToJson(this object obj)
		{
			if (null == obj)
				return null;
			if (obj is string)
				return obj as string;
			if (obj.GetType().IsEnum)
				return Enum.GetName(obj.GetType(), obj);
			return JsonConvert.SerializeObject(
				obj, obj.GetType(), defaultSettings?.Invoke());
		}
		////////////////////////////////////////////////////////////////////////////////
		public static object FromJson(this string json)
		{
			if (string.IsNullOrWhiteSpace(json))
				return default(object);
			return JsonConvert.DeserializeObject(json, defaultSettings?.Invoke());
		}
		public static object FromJson(this string json, Type type)
		{
			if (string.IsNullOrWhiteSpace(json))
				return default(object);
			if (type == typeof(string))
				return json;
			if ((type.IsEnum && !char.IsDigit(json, 0)) || type == typeof(DateTime) || type == typeof(TimeSpan))
				return JsonConvert.DeserializeObject($"\"{json.Trim('"', '\'')}\"", type, defaultSettings?.Invoke());
			return JsonConvert.DeserializeObject(json, type, defaultSettings?.Invoke());
		}
		public static T FromJson<T>(this string json)
		{
			if (string.IsNullOrWhiteSpace(json))
				return default(T);
			return (T)FromJson(json, typeof(T));
		}
		public static bool IsJToken(this string json)
		{
			try
			{
				JToken.Parse(json);
				return true;
			}
			catch (JsonReaderException)
			{
				return false;
			}
		}
		////////////////////////////////////////////////////////////////////////////////
		public static JToken ToJToken(this string json)
		{
			return JToken.Parse(json);
		}
		////////////////////////////////////////////////////////////////////////////////
		public static async Task<JToken> ToJToken(this Task<string> jsonTask)
		{
			return JToken.Parse(await jsonTask);
		}
		////////////////////////////////////////////////////////////////////////////////
		public static T ToAnonymousType<T>(this JToken source, T destinationType) // destinationType не удалять, нужен при вызове для передачи типа анонимного
		{
			return source.ToObject<T>(JsonSerializer.Create(defaultSettings?.Invoke()));
		}
		////////////////////////////////////////////////////////////////////////////////
		public static async Task<T> ToAnonymousType<T>(this Task<JToken> source, T destinationType) // destinationType не удалять, нужен при вызове для передачи типа анонимного
		{
			return (await source).ToObject<T>(JsonSerializer.Create(defaultSettings?.Invoke()));
		}
		////////////////////////////////////////////////////////////////////////////////
	}
}