﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Runtime.Serialization.Formatters.Soap;
using System.Xml.Serialization;

namespace FORIS.TSS.BusinessLogic
{
	public static class SerializationHelper
	{
		private static readonly SoapFormatter Formatter = new SoapFormatter
		{
			AssemblyFormat = FormatterAssemblyStyle.Simple
		};
		public static string ToXML(object graph)
		{
			return ToXML(graph, Encoding.Default);
		}
		public static string ToXML(object graph, Encoding encoding)
		{
			using (MemoryStream stm = new MemoryStream())
			{
				Formatter.Serialize(stm, graph);
				byte[] array = stm.ToArray();
				return encoding.GetString(array);
			}
		}
		public static object FromXmlOrThrowException(string xmlString)
		{
			//Меняем ссылки на сборки для совместимости с версией ПО, когда классы повторов лежали в другой сборке

			xmlString = FixTypeReferences(xmlString);

			var bytes = Encoding.Default.GetBytes(xmlString);
			using (var ms = new MemoryStream(bytes))
			{
				return Formatter.Deserialize(ms);
			}
		}
		public static string FixTypeReferences(string xmlString)
		{
			xmlString =
				xmlString.Replace(
					@"xmlns:a1=""http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic.Mail/Interfaces""",
					@"xmlns:a1=""http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic.Mail/FORIS.TSS.Common.Utils"""
					)
					.Replace(
						@"xmlns:a1=""http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic/Interfaces""",
						@"xmlns:a1=""http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.BusinessLogic/FORIS.TSS.Common.Utils"""
					)
					.Replace(
						@"xmlns:a1=""http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.Common/FORIS.TSS.Common.Utils""",
						@"xmlns:a1=""http://schemas.microsoft.com/clr/nsassem/FORIS.TSS.Common/FORIS.TSS.Interfaces"""
					);
			return xmlString;
		}
		public static object FromXML(string xmlString)
		{
			try
			{
				return FromXmlOrThrowException(xmlString);
			}
			catch (Exception ex)
			{
				Trace.TraceError("Exception during serialization: {0}", ex);
				return null;
			}
		}
		public static T DeserializeXml<T>(string xml) where T: class
		{
			var serializer = new XmlSerializer(typeof (T));
			using (var reader = new StringReader(xml))
				return serializer.Deserialize(reader) as T;
		}
		public static string SerializeXml<T>(T t) where T : class
		{
			var serializer = new XmlSerializer(typeof(T));
			var ns = new XmlSerializerNamespaces();
			// добавлено, не добавлялся namespace
			ns.Add(string.Empty, string.Empty);

			using (var stream = new MemoryStream())
			{
				serializer.Serialize(stream, t, ns);
				return Encoding.UTF8.GetString(stream.ToArray());
			}
		}
	}
}