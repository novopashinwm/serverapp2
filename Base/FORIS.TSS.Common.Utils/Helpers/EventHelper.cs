﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace FORIS.TSS.Common.Helpers
{
	public static class EventHelper
	{
		/// <summary> Запускает каждого подписавшегося последовательно в цикле </summary>
		/// <typeparam name="T"> Тип параметра события </typeparam>
		/// <param name="evntArgs"> Параметр события </param>
		/// <param name="evntHandlerGetInvocationList"> Метод получения списка подписавшихся на события </param>
		/// <param name="evntHandlerRemove"> Метод удаления подписавшегося на событие в случае ошибки во время вызова (если null удалять не надо) </param>
		public static void Raise<T>(
			T evntArgs,
			Func<Delegate[]> evntHandlerGetInvocationList,
			Action<AnonymousEventHandler<T>> evntHandlerRemove)
			where T : EventArgs
		{
			var handlers = evntHandlerGetInvocationList
				?.Invoke()
				?.OfType<AnonymousEventHandler<T>>()
				?.ToArray();

			if (handlers == null)
				return;

			var methodFullName = CallStackHelper.GetCallerMethodFullName();
			foreach (var handler in handlers)
			{
				var handlerFullName = CallStackHelper.GetMethodFullName(handler?.Method);
				try
				{
					using (new Stopwatcher($@"Raise '{methodFullName}' by calling '{handlerFullName}'"))
					{
						handler?.Invoke(evntArgs);
					}
				}
				catch (Exception e)
				{
					Trace.TraceError("Raise '{0}' by calling '{1}'\nError:\n{2}",
						methodFullName, handlerFullName, e);
					try
					{
						if (null != evntHandlerRemove)
							Trace.TraceWarning("Unsubscribing '{1}' from '{0}' because of call error",
								methodFullName, handlerFullName);
						evntHandlerRemove?.Invoke(handler);
					}
					catch (Exception ex)
					{
						Trace.TraceError("'{1}' - unable to unsubscribe from '{0}': {2}",
							methodFullName, handlerFullName, ex);
					}
				}
			}
		}
		/// <summary> Запускает каждого подписавшегося на событие в отдельном потоке и не ждет завершения </summary>
		/// <typeparam name="T"> Тип параметра события </typeparam>
		/// <param name="evntArgs"> Параметр события </param>
		/// <param name="evntHandlerGetInvocationList"> Метод получения списка подписавшихся на события </param>
		/// <param name="evntHandlerRemove"> Метод удаления подписавшегося на событие в случае ошибки во время вызова (если null удалять не надо) </param>
		public static void RaiseParallelWithoutWait<T>(
			T evntArgs,
			Func<Delegate[]> evntHandlerGetInvocationList,
			Action<AnonymousEventHandler<T>> evntHandlerRemove)
			where T : EventArgs
		{
			var handlers = evntHandlerGetInvocationList
				?.Invoke()
				?.OfType<AnonymousEventHandler<T>>()
				?.ToArray();

			if (handlers == null)
				return;

			var methodFullName = CallStackHelper.GetCallerMethodFullName();
			Parallel.ForEach(handlers, handler =>
			{
				var handlerFullName = CallStackHelper.GetMethodFullName(handler?.Method);
				try
				{
					using (new Stopwatcher($@"Raise '{methodFullName}' by calling '{handlerFullName}'"))
					{
						handler?.Invoke(evntArgs);
					}
				}
				catch (Exception e)
				{
					Trace.TraceError("Raise '{0}' by calling '{1}'\nError:\n{2}",
						methodFullName, handlerFullName, e);
					try
					{
						if (null != evntHandlerRemove)
							Trace.TraceWarning("Unsubscribing '{1}' from '{0}' because of call error",
								methodFullName, handlerFullName);
						evntHandlerRemove?.Invoke(handler);
					}
					catch (Exception ex)
					{
						Trace.TraceError("'{1}' - unable to unsubscribe from '{0}': {2}",
							methodFullName, handlerFullName, ex);
					}
				}
			});
		}
		/// <summary> Запускает каждого подписавшегося на событие в отдельном потоке и ждет завершения всех потоков </summary>
		/// <typeparam name="T"> Тип параметра события </typeparam>
		/// <param name="evntArgs"> Параметр события </param>
		/// <param name="evntHandlerGetInvocationList"> Метод получения списка подписавшихся на события </param>
		/// <param name="evntHandlerRemove"> Метод удаления подписавшегося на событие в случае ошибки во время вызова (если null удалять не надо) </param>
		public static void RaiseParallelWithWait<T>(
			T evntArgs,
			Func<Delegate[]> evntHandlerGetInvocationList,
			Action<AnonymousEventHandler<T>> evntHandlerRemove)
			where T : EventArgs
		{
			var handlers = evntHandlerGetInvocationList
				?.Invoke()
				?.OfType<AnonymousEventHandler<T>>()
				?.ToArray();
			if (handlers == null)
				return;

			var methodFullName = CallStackHelper.GetCallerMethodFullName();
			/////////////////////////////////////////////////////
			var tasks = new List<Task>();
			foreach (var handler in handlers)
			{
				tasks.Add(Task.Run(() => {
					var handlerFullName = CallStackHelper.GetMethodFullName(handler?.Method);
					try
					{
						using (new Stopwatcher($@"Raise '{methodFullName}' by calling '{handlerFullName}'"))
						{
							handler?.Invoke(evntArgs);
						}
					}
					catch (Exception e)
					{
						Trace.TraceError("Raise '{0}' by calling '{1}'\nError:\n{2}",
							methodFullName, handlerFullName, e);
						try
						{
							if (null != evntHandlerRemove)
								Trace.TraceWarning("Unsubscribing '{1}' from '{0}' because of call error",
									methodFullName, handlerFullName);
							evntHandlerRemove?.Invoke(handler);
						}
						catch (Exception ex)
						{
							Trace.TraceError("'{1}' - unable to unsubscribe from '{0}': {2}",
								methodFullName, handlerFullName, ex);
						}
					}
				}));
				/////////////////////////////////////////////////////
			}
			try
			{
				// Ожидаем завершения всех запущенных потоков
				Task.WaitAll(tasks.ToArray());
			}
			catch (AggregateException e)
			{
				Trace.TraceError("\nThe following exceptions have been thrown by WaitAll(): (THIS WAS EXPECTED)");
				for (int j = 0; j < e.InnerExceptions.Count; j++)
					Trace.TraceError("\n{0}\n{1}", "".PadLeft(50, '-'), e.InnerExceptions[j].ToString());
			}
		}
	}
}