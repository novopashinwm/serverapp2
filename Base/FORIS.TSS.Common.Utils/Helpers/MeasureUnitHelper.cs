﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.Common
{
	public static class MeasureUnitHelper
	{
		public static int KnotsToKMPH(double knots)
		{
			var kmph = (knots * 1.852);
			return (int)Math.Round(kmph);
		}
		/// <summary> Преобразование скорости м/с в км/ч </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static double MpsToKmph(double value)
		{
			return value * 3600 / 1000;
		}

		public static decimal ConvertTo(decimal value, UnitOfMeasure sourceType, UnitOfMeasure targetType)
		{
			if (sourceType == targetType)
				return value;

			switch (sourceType)
			{
				case UnitOfMeasure.Cbm:
					switch (targetType)
					{
						case UnitOfMeasure.Litre:
							return value * 1000;
					}
					break;
				case UnitOfMeasure.Litre:
					switch (targetType)
					{
						case UnitOfMeasure.Cbm:
							return value / 1000;
					}
					break;
				case UnitOfMeasure.KmPerLitre:
					switch (targetType)
					{
						case UnitOfMeasure.LitrePer100Km:
							return 100 / value;
					}
					break;
				case UnitOfMeasure.KmPerM3:
					switch (targetType)
					{
						case UnitOfMeasure.LitrePer100Km:
							return 100 * 1000 / value;
						case UnitOfMeasure.KmPerLitre:
							return value / 1000;
					}
					break;
				case UnitOfMeasure.LitrePer100Km:
					switch (targetType)
					{
						case UnitOfMeasure.KmPerLitre:
							return 100 * value;
						case UnitOfMeasure.LitrePerKm:
							return value / 100;
					}
					break;
				case UnitOfMeasure.Kilometers:
					switch (targetType)
					{
						case UnitOfMeasure.Kilometers100:
							return value / 100;
					}
					break;
				case UnitOfMeasure.Kilometers100:
					switch (targetType)
					{
						case UnitOfMeasure.Kilometers:
							return 100 * value;
					}
					break;
			}

			throw new NotImplementedException("cant convert from " + sourceType + " to " + targetType);
		}
	}
}