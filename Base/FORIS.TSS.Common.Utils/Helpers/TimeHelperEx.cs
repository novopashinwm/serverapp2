﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using FORIS.TSS.Resources;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary> Class for converting int to DateTime </summary>
	public static class TimeHelperEx
	{
		public static DateTime GetLocalTime(DateTime time, TimeZoneInfo timeZoneInfo)
		{
			return TimeZoneInfo.ConvertTimeFromUtc(time, timeZoneInfo);
		}
		[Obsolete("Culture should be specified")]
		public static string GetTimeSpanReadable(TimeSpan timeSpan)
		{
			return GetTimeSpanReadable(timeSpan, CultureInfo.CurrentCulture);
		}
		public static StringBuilder AppendTimeInterval(this StringBuilder sb, TimeSpan timeSpan, CultureInfo culture)
		{
			var @string = new ResourceStringContainer(
				culture,
				typeof(TimeHelperEx).Assembly
					?.GetManifestResourceNames()
					?.FirstOrDefault(x => x.EndsWith(".Strings.resources"))
					?.Replace(".resources", ""),
				typeof(TimeHelperEx).Assembly);
			if (timeSpan.TotalSeconds < 60)
			{
				sb.Append((int) timeSpan.TotalSeconds);
				sb.Append(@string["TimeSpanSecondsShort"]);
				return sb;
			}

			if (timeSpan.TotalMinutes < 60)
			{
				sb.Append((int)timeSpan.TotalMinutes);
				sb.Append(@string["TimeSpanMinutesShort"]);
				return sb;
			}

			if (timeSpan.TotalHours < 24)
			{
				sb.Append((int)timeSpan.TotalHours);
				sb.Append(@string["TimeSpanHoursShort"]);
				return sb;
			}

			if (timeSpan.TotalDays < 30)
			{
				sb.Append((int)timeSpan.TotalHours);
				sb.Append(@string["TimeSpanDaysShort"]);
				return sb;
			}

			sb.Append(@string["TimeSpanMoreMonthShort"]);
			return sb;
		}
		public static string GetIntervalPreset(int seconds, CultureInfo cultureInfo)
		{
			var @string = new ResourceStringContainer(cultureInfo, "FORIS.TSS.Common.Strings", typeof(TimeHelperEx).Assembly);
			switch (seconds)
			{
				case 60:
					return string.Format("60 {0}", @string["TimeSpanSecondsShort"]);
				case 60 * 2:
					return string.Format("2 {0}", @string["TimeSpanMinutesShort"]);
				case 60 * 5:
					return string.Format("5 {0}", @string["TimeSpanMinutesShort"]);
				case 60 * 10:
					return string.Format("10 {0}", @string["TimeSpanMinutesShort"]);
				case 60 * 30:
					return string.Format("30 {0}", @string["TimeSpanMinutesShort"]);

				case 60 * 60 * 1:
					return string.Format("1 {0}", @string["TimeSpanHoursShort"]);
				case 60 * 60 * 6:
					return string.Format("6 {0}", @string["TimeSpanHoursShort"]);
				case 60 * 60 * 12:
					return string.Format("12 {0}", @string["TimeSpanHoursShort"]);
				default:
					return GetTimeSpanReadable(new TimeSpan(0, 0, 0, seconds), cultureInfo);
			}
		}
		public static string GetTimeSpanReadable(TimeSpan timeSpan, CultureInfo cultureInfo)
		{
			var @string = new ResourceStringContainer(cultureInfo, "FORIS.TSS.Common.Strings", typeof(TimeHelperEx).Assembly);

			var sb = new StringBuilder();
			var days = Math.Floor(timeSpan.TotalDays);
			if (days > 0)
				sb.Append(string.Format("{0} {1} ",
					days,
					@string["TimeSpanDaysShort"]));
			var hours = Math.Floor(timeSpan.TotalHours);
			if (hours > 0)
				sb.Append(string.Format("{0} {1} ",
					timeSpan.Hours,
					@string["TimeSpanHoursShort"]
					));
			var minutes = Math.Floor(timeSpan.TotalMinutes);
			if (minutes > 0)
				sb.Append(string.Format("{0} {1} ",
					timeSpan.Minutes,
					@string["TimeSpanMinutesShort"]
					));
			sb.Append(string.Format("{0} {1}",
				timeSpan.Seconds,
				@string["TimeSpanSecondsShort"]
				));

			return sb.ToString();
		}
		public static TimeSpan? TimeSpanFromSeconds(int? seconds)
		{
			return seconds != null ? TimeSpan.FromSeconds(seconds.Value) : (TimeSpan?) null;
		}
		public struct DateTimeInterval
		{
			public readonly DateTime From;
			public readonly DateTime To;
			public DateTimeInterval(DateTime from, DateTime to)
			{
				From = from;
				To   = to;
			}
		}
		public static IEnumerable<DateTimeInterval> GetLocalDailyIntervals(
			DateTime dateFrom, DateTime dateTo,
			int hoursFrom, int minutesFrom,
			int hoursTo, int minutesTo)
		{
			var currentDate = new DateTime(dateFrom.Year, dateFrom.Month, dateFrom.Day, 0, 0, 0, DateTimeKind.Local);

			if (hoursFrom * 60 + minutesFrom < hoursTo * 60 + minutesTo)
			{
				while (currentDate <= dateTo)
				{
					var @from = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, hoursFrom, minutesFrom, 0, DateTimeKind.Local);
					var to = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, hoursTo, minutesTo, 0, DateTimeKind.Local);

					yield return new DateTimeInterval(@from, to);

					currentDate = currentDate.AddDays(1);
				}
			}
			else
			{
				while (currentDate <= dateTo)
				{
					var @from = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, hoursFrom, minutesFrom, 0, DateTimeKind.Local);

					currentDate = currentDate.AddDays(1);

					var to = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, hoursTo, minutesTo, 0, DateTimeKind.Local);

					yield return new DateTimeInterval(@from, to);
				}
			}
		}
		public static DateTime? Max(DateTime? x, DateTime? y)
		{
			if (x == null)
				return y;
			if (y == null)
				return x;

			return x < y ? y : x;
		}
		public static void LogStopwatch(Stopwatch sw, string tag)
		{
			if (500 < sw.ElapsedMilliseconds)
				Trace.TraceWarning("{0} is slow, operation has  taken {1}", tag, sw.Elapsed);
		}
		/// <summary> Возвращает время, соответствующее последней секунде того же дня </summary>
		public static DateTime ToEndOfDay(this DateTime dateTime)
		{
			return dateTime.Date.AddDays(1).AddSeconds(-1);
		}
	}
}