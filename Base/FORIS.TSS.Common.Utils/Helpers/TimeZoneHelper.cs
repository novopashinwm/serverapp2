﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using Jayrock.Json.Conversion;

namespace FORIS.TSS.Common.Helpers
{
	public static class TimeZoneHelper
	{
		public static IEnumerable<TimeZoneInfo> GetEqualTimeZoneInfos(TimeZoneInfo customTimeZone)
		{
			if (customTimeZone == null)
				throw new ArgumentNullException("customTimeZone");

			var timeZoneInfos = TimeZoneInfo.GetSystemTimeZones();

			var customTimeZoneAsSystem = timeZoneInfos.FirstOrDefault(tz => tz.Id == customTimeZone.Id);
			if (customTimeZoneAsSystem != null)
			{
				yield return customTimeZone;
				yield break;
			}

			foreach (var systemTimeZone in timeZoneInfos)
			{
				if (systemTimeZone.BaseUtcOffset != customTimeZone.BaseUtcOffset)
					continue;

				if (CompareTimeZoneInfos(systemTimeZone, customTimeZone))
					yield return systemTimeZone;
			}

			foreach (var systemTimeZone in timeZoneInfos)
			{
				if (systemTimeZone.BaseUtcOffset != customTimeZone.BaseUtcOffset ||
					systemTimeZone.SupportsDaylightSavingTime ||
					systemTimeZone.GetAdjustmentRules().Length != 0)
					continue;

				yield return systemTimeZone;
			}
		}

		private static bool CompareTimeZoneInfos(TimeZoneInfo systemTimeZone, TimeZoneInfo customTimeZone)
		{
			if (customTimeZone.SupportsDaylightSavingTime != systemTimeZone.SupportsDaylightSavingTime)
				return false;

			foreach (var customRule in customTimeZone.GetAdjustmentRules())
			{
				var count = 0;
				foreach (var systemRule in systemTimeZone.GetAdjustmentRules())
				{
					if (systemRule.DaylightDelta != customRule.DaylightDelta)
						continue;
					if (customRule.DateStart < systemRule.DateStart || systemRule.DateEnd < customRule.DateEnd)
						continue;

					var year = customRule.DateStart.Year;
					if (Similar(year, customTimeZone.BaseUtcOffset, customRule.DaylightTransitionStart, systemRule.DaylightTransitionStart) &&
						Similar(year, customTimeZone.BaseUtcOffset + customRule.DaylightDelta, customRule.DaylightTransitionEnd, systemRule.DaylightTransitionEnd) ||
						Similar(year, customTimeZone.BaseUtcOffset, customRule.DaylightTransitionEnd, systemRule.DaylightTransitionStart) &&
						Similar(year, customTimeZone.BaseUtcOffset + customRule.DaylightDelta, customRule.DaylightTransitionStart, systemRule.DaylightTransitionEnd))
						++count;

				}
				if (count != 1)
					return false;
			}

			return true;
		}

		private static bool Similar(int year, TimeSpan @fixedUtcOffset, TimeZoneInfo.TransitionTime @fixed, TimeZoneInfo.TransitionTime @float)
		{
			DateTime @fixedChangeDateTime = @fixed.IsFixedDateRule
					? new DateTime(year, @fixed.Month, @fixed.Day).Add(@fixed.TimeOfDay.TimeOfDay) + @fixedUtcOffset
					: new DateTime(year, @fixed.Month, 1).Add(@fixed.TimeOfDay.TimeOfDay);
			var @floatChangeDateTime = new DateTime(year, @float.Month, 1).Add(@float.TimeOfDay.TimeOfDay);
			//.AddDays((@float.Week-1) * 7).AddDays((int)@float.DayOfWeek);

			if (@float.Week == 5) //Последняя неделя месяца
			{
				@floatChangeDateTime = @floatChangeDateTime.AddMonths(1);
				do
				{
					@floatChangeDateTime = @floatChangeDateTime.AddDays(-1);
				} while (@floatChangeDateTime.DayOfWeek != @float.DayOfWeek);
			}
			else
			{
				var week = 1;
				while (true)
				{
					if (@floatChangeDateTime.DayOfWeek == @float.DayOfWeek)
					{
						if (week == @float.Week)
							break;
						++week;
					}
					@floatChangeDateTime = @floatChangeDateTime.AddDays(1);
				}
			}

			return @fixedChangeDateTime.Month == @floatChangeDateTime.Month &&
				@fixedChangeDateTime.Day == @floatChangeDateTime.Day &&
				@fixedChangeDateTime.TimeOfDay.Hours == @floatChangeDateTime.TimeOfDay.Hours &&
				@fixedChangeDateTime.TimeOfDay.Minutes == @floatChangeDateTime.TimeOfDay.Minutes &&
				@fixedChangeDateTime.TimeOfDay.Seconds == @floatChangeDateTime.TimeOfDay.Seconds;
		}

		public static TimeZoneFingerprint ToFingerprint(this TimeZoneInfo tz, int? startYear = null)
		{
			if (startYear == null)
				startYear = DateTime.UtcNow.Year;

			var result = new TimeZoneFingerprint();

			result.BaseOffset = (int)tz.BaseUtcOffset.TotalMinutes;
			result.DstChanges = GetLastDaylightSavingChanges(tz, 3, startYear.Value).ToArray();

			return result;
		}

		private static int GetUtcOffset(TimeZoneInfo tz, DateTime dt)
		{
			return (int)tz.GetUtcOffset(dt).TotalMinutes;
		}

		//Бинарный поиск момента времени, когда происходит перевод часов
		private static DateTime? FindOffsetChange(TimeZoneInfo tz, DateTime dateStart, DateTime dateEnd)
		{
			if (GetUtcOffset(tz, dateStart) == GetUtcOffset(tz, dateEnd))
				return null;

			var timeStart = TimeHelper.GetSecondsFromBase(dateStart);
			var timeEnd = TimeHelper.GetSecondsFromBase(dateEnd);
			while (1 < timeEnd - timeStart)
			{

				var middleTime = (timeEnd - timeStart) / 2 + timeStart;
				var middleDate = TimeHelper.GetLocalTime(middleTime, tz);
				if (GetUtcOffset(tz, middleDate) == GetUtcOffset(tz, dateEnd))
				{
					dateEnd = middleDate;
					timeEnd = middleTime;
				}
				else
				{
					dateStart = middleDate;
					timeStart = middleTime;
				}
			}
			return TimeHelper.GetLocalTime(timeEnd - (timeEnd % 60), tz);
		}

		private static List<TimeZoneFingerprint.DstChange> GetLastDaylightSavingChanges(TimeZoneInfo tz, int yearCount, int currentYear)
		{
			var result = new List<TimeZoneFingerprint.DstChange>();

			if (!tz.SupportsDaylightSavingTime)
				return result;

			var firstMonth = 1;
			var middleMonth = 5;
			var lastMonth = 12;
			var i0 = -yearCount;

			var offsetWinter = GetUtcOffset(tz, new DateTime(currentYear - i0, firstMonth, 1, 0, 0, 0, 0));
			var offsetSummer = GetUtcOffset(tz, new DateTime(currentYear - i0, middleMonth, 15, 0, 0, 0, 0));
			var baseOffset = offsetSummer < offsetWinter ? offsetSummer : offsetWinter;

			/*Получение данных за yearCount вперед и назад, чтобы исключить проблемы при формировании отчетов по подписке*/
			for (var i = -yearCount; i != yearCount; ++i)
			{
				var yearBegin = new DateTime(currentYear - i, firstMonth, 1, 0, 0, 0, 0);
				var halfYear = new DateTime(currentYear - i, middleMonth, 15, 0, 0, 0, 0);
				var yearEnd = new DateTime(currentYear - i, lastMonth, 31, 23, 59, 59, 0);

				var delta = GetUtcOffset(tz, halfYear) - baseOffset;
				if (delta == 0)
					continue;

				result.Add(new TimeZoneFingerprint.DstChange
				{
					On = FindOffsetChange(tz, yearBegin, halfYear),
					Off = FindOffsetChange(tz, halfYear, yearEnd),
					Delta = delta
				});
			}

			return result;
		}

		public static TimeZoneInfo FromFingerprint(TimeZoneFingerprint fingerprint)
		{
			if (fingerprint == null)
				return null;

			var baseOffset = TimeSpan.FromMinutes(fingerprint.BaseOffset);
			var dstChanges = fingerprint.DstChanges;
			var rules = new List<TimeZoneInfo.AdjustmentRule>(dstChanges != null ? dstChanges.Length : 0);

			if (dstChanges != null)
			{
				foreach (var dstChange in dstChanges)
				{
					var onChange = dstChange.On;

					if (dstChange.Off == null)
						continue;

					var off = dstChange.Off.Value;
					var delta = TimeSpan.FromMinutes(dstChange.Delta);

					DateTime dateStart;
					TimeZoneInfo.TransitionTime dayLightTransitionStart;
					if (onChange.HasValue)
					{
						var on = onChange.Value;
						dateStart = new DateTime(on.Year, on.Month, on.Day, 0, 0, 0, 0, DateTimeKind.Unspecified);
						var dayLightTransitionStartDate = new DateTime(1, 1, 1,
																	   on.TimeOfDay.Hours, on.TimeOfDay.Minutes,
																	   on.TimeOfDay.Seconds,
																	   DateTimeKind.Unspecified);
						dayLightTransitionStart = TimeZoneInfo.TransitionTime.CreateFixedDateRule(
							dayLightTransitionStartDate,
							dateStart.Month, dateStart.Day);

					}
					else
					{
						// Chrome передает on = null, а IE 0001.01.01T00:00:00
						dateStart = new DateTime(off.Year, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified);
						dayLightTransitionStart = TimeZoneInfo.TransitionTime.CreateFixedDateRule(
							new DateTime(1, 1, 1, 0, 0, 0, DateTimeKind.Unspecified),
							1, 1);
					}

					var dateEnd = new DateTime(off.Year, off.Month, off.Day, 0, 0, 0, 0, DateTimeKind.Unspecified);
					var dayLightTransitionEndDate = new DateTime(1, 1, 1,
																 off.TimeOfDay.Hours, off.TimeOfDay.Minutes, off.TimeOfDay.Seconds,
																 DateTimeKind.Unspecified);
					var dayLightTransitionEnd = TimeZoneInfo.TransitionTime.CreateFixedDateRule(
						dayLightTransitionEndDate,
						off.Month, off.Day);

					rules.Add(
						TimeZoneInfo.AdjustmentRule.CreateAdjustmentRule(
							dateStart,
							dateEnd,
							delta,
							dayLightTransitionStart,
							dayLightTransitionEnd)
						);
				}
			}

			rules.Sort((x, y) => x.DateStart == DateTime.MinValue ? x.DateStart.CompareTo(y.DateStart) : x.DateEnd.CompareTo(y.DateEnd));

			return TimeZoneInfo.CreateCustomTimeZone(
				Guid.NewGuid().ToString(),
				baseOffset,
				"Custom Timezone",
				"Standart",
				"Daylight saving",
				rules.ToArray(),
				false);
		}

		public static TimeZoneInfo ParseDstString(string dstString)
		{
			if (string.IsNullOrEmpty(dstString))
				return null;

			TimeZoneFingerprint dstObject;

			using (var textReader = new StringReader(dstString))
				dstObject = JsonConvert.Import(typeof(TimeZoneFingerprint), textReader) as TimeZoneFingerprint;
			if (dstObject == null)
				return null;

			return FromFingerprint(dstObject);
		}
	}
}