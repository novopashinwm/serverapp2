﻿using System;
using System.Linq;
using Jayrock.Json.Conversion;

namespace FORIS.TSS.Helpers
{
	/// <summary> Хелпер для работы с Json </summary>
	public class JsonHelper
	{
		public static string SerializeObjectToJson(object obj)
		{
			return JsonConvert.ExportToString(obj);
		}
		public static object DeserializeObjectFromJson(string json)
		{
			return JsonConvert.Import(json);
		}
		public static object DeserializeObjectFromJson(Type type, string json)
		{
			if (type == typeof(string))
				return json;

			if (string.IsNullOrEmpty(json))
				return null;

			if (type.IsConstructedGenericType &&
				type.GetGenericTypeDefinition() == typeof(Nullable<>))
			{
				type = type.GenericTypeArguments.Single();
			}

			if ((type == typeof(DateTime) || type == typeof(TimeSpan)) &&
				json[0] != '\'' && json[0] != '\"')
				json = "'" + json + "'";

			return JsonConvert.Import(type, json);
		}
		public static T DeserializeObjectFromJson<T>(string json)
		{
			return (T)DeserializeObjectFromJson(typeof(T), json);
		}
	}
}