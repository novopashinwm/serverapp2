﻿using System;
using System.Threading;

namespace FORIS.TSS.Common.Helpers
{
	public static class ReaderWriterLockSlimExtensions
	{
		private struct WriteLockToken : IDisposable
		{
			private readonly ReaderWriterLockSlim _rwLock;
			public WriteLockToken(ReaderWriterLockSlim rwLock)
			{
				_rwLock = rwLock;
				_rwLock.EnterWriteLock();
			}
			public void Dispose() => _rwLock.ExitWriteLock();
		}
		public static IDisposable WriteLock(this ReaderWriterLockSlim rwLock) => new WriteLockToken(rwLock);
		private struct ReadLockToken : IDisposable
		{
			private readonly ReaderWriterLockSlim _rwLock;
			public ReadLockToken(ReaderWriterLockSlim rwLock)
			{
				_rwLock = rwLock;
				_rwLock.EnterReadLock();
			}
			public void Dispose() => _rwLock.ExitReadLock();
		}
		public static IDisposable ReadLock(this ReaderWriterLockSlim rwLock) => new ReadLockToken(rwLock);
	}
}