﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Filtering;
using FORIS.TSS.Common.Filtering;
using FORIS.TSS.Helpers;

namespace FORIS.TSS.Common.Helpers
{
	/// <summary> Расширения для работы с классом <see cref="FullLog"/> </summary>
	public static class FullLogHelper
	{
		/// <summary> Поиск интервала действия по времени </summary>
		public static ActionIntervalDbInterval GetActionInterval(this List<ActionIntervalDbInterval> actionIntervals, int logTime)
		{
			// Поиск
			var bsr = actionIntervals.BinarySearch(x => logTime - x.Start.ToLogTime());
			// Все интервалы начинаются после logTime
			if (!bsr.HasFrom)
				return null;
			// Точное совпадение с началом интервала
			if (bsr.Single)
				return actionIntervals[bsr.From];
			// Интервал, начало которого слева от logTime ближе всех к logTime
			var interval = actionIntervals[bsr.From];
			// Интервал кончился до logTime
			if (interval.End.ToLogTime() < logTime)
				return null;
			// Найденный интервал
			return interval;
		}
		/// <summary> Поиск группы истории по времени </summary>
		public static HistoryGroup GetHistoryGroup(this List<HistoryGroup> groups, int logTime)
		{
			// Поиск
			var bsr = groups.BinarySearch(x => logTime - x.LogTimeFrom);
			// Все интервалы начинаются после logTime
			if (!bsr.HasFrom)
				return null;
			// Точное совпадение с началом интервала
			if (bsr.Single)
				return groups[bsr.From];
			// Интервал, начало которого слева от logTime ближе всех к logTime
			var interval = groups[bsr.From];
			// Интервал кончился до logTime
			if (interval.LogTimeTo < logTime)
				return null;
			// Найденный интервал
			return interval;
		}
		/// <summary> Группа истории относится к разновидности групп с движением </summary>
		public static bool IsMove(this HistoryGroup group)
		{
			return new[] { GroupingType.Motion, GroupingType.Slow }
				.Contains(group.grType);
		}
		public static IEnumerable<SensorChangeInterval> FindSensorChangeIntervals(this FullLog @this,
			SensorLegend sensorLegend, int logTimeDelta, decimal valueError)
		{
			if (!@this.Sensors.TryGetValue(sensorLegend, out Sensor sensor) || sensor.LogRecords.Count < 2)
				yield break;

			// 1. Отсечь грубые выбросы - всплески относительно среднего значения
			var errorFilter = new ErrorFilter<SensorLogRecord>(logTimeDelta, valueError, r => r.Value);
			var logRecords = errorFilter.Filter(sensor.LogRecords);

#if DEBUG
			logRecords = logRecords.ToArray();
#endif
			// 2. Оставить только точки перегиба
			var swingingDoor = new SwingingDoor<SensorLogRecord>(r => r.LogTime, r => (double)r.Value, r => (double)valueError)
			{
				MaxArgumentDelta = logTimeDelta
			};
			logRecords = swingingDoor.Filter(logRecords);

#if DEBUG
			logRecords = logRecords.ToArray();
#endif

			// 3. Преобразовать в интервалы
			var t = (SensorLogRecord?)null;
			foreach (var current in logRecords)
			{
				if (t != null)
				{
					var previous = t.Value;
					yield return new SensorChangeInterval
					{
						From = previous,
						To   = current
					};
				}
				t = current;
			}
		}
		/// <summary> Возвращает пробег в метрах между временными точками logTimeFrom и logTimeTo </summary>
		public static decimal GetRunDistance(this FullLog @this, int logTimeFrom, int logTimeTo)
		{
			if (logTimeTo < logTimeFrom)
				throw new ArgumentException(@"logTimeTo could not be less than logTimeFrom", nameof(logTimeTo));

			var fromOdometer = @this.GetInterpolatedOdometerValue(logTimeFrom);
			var toOdometer   = @this.GetInterpolatedOdometerValue(logTimeTo);

			return ((toOdometer - fromOdometer) / 100m) ?? 0m;
		}
		/// <summary> Возвращает значение суммарного значения пробега в сантиметрах со дня подключения устройства по logTime </summary>
		/// <remarks> Если для данной точки точное значение неизвестно, возвращается линейно интерполированное </remarks>
		public static decimal? GetInterpolatedOdometerValue(this FullLog @this, int logTime)
		{
			var runs = @this.Runs;

			var bsr = runs.BinarySearch(x => logTime.CompareTo(x.LogTime));

			if (bsr.Empty)
				return null;

			if (bsr.Single)
				return runs[bsr.From].Odometer;

			if (bsr.HasFrom && !bsr.HasTo)
				return runs[bsr.From].Odometer;

			if (!bsr.HasFrom && bsr.HasTo)
				return runs[bsr.To].Odometer;

			var from = runs[bsr.From];
			var to   = runs[bsr.To];

			//Линейная интерполяция
			return (to.Odometer - from.Odometer) * (logTime - from.LogTime) / (to.LogTime - from.LogTime) + from.Odometer;
		}
		public static SensorStatistics CalculateSensorStatistics(this FullLog @this, SensorLegend sensorLegend, int logTimeFrom, int logTimeTo)
		{
			var fullLog = @this;
			// Создаем результат с параметрами запроса данных (интервал запроса статистики)
			var result = new SensorStatistics
			{
				LogTimeFrom = logTimeFrom,
				LogTimeTo   = logTimeTo,
				OutTime     = logTimeTo - logTimeFrom, // Если датчика не будет, то выдаем весь интервал отсутствия данных
			};
			// Ищем датчик
			var sensor = fullLog.Sensors
				.FirstOrDefault(x => x.Key == sensorLegend)
				.Value;
			// Если датчика нет возвращаем пустую статистику
			if (sensor == null)
				return result;
			// Массив точек всего лога (включая точки границ)
			var allLogTimes = fullLog.GetAllLogTimesWithBoundaries(false);
			// Нет точек, возвращаем пустоту
			if (allLogTimes.Count < 2)
				return result;
			// Вычисляем интервалы отсутствия данных, считаем всегда для точек всего лога, далее корректируем
			var interruptions = fullLog.GetInterruptions(allLogTimes, fullLog.Options.NoDataPeriod.TotalSeconds);
			// Считаем общую сумму отсутствия данных на интервале
			result.OutTime = interruptions
				// Удаляем все интервалы отсутствия данных вне запрашиваемого интервала
				.Where(i =>
					(logTimeFrom <= i.LogTime              && i.LogTime              <= logTimeTo) || // Начало внутри запрашиваемого интервала
					(logTimeFrom <= i.LogTime + i.Duration && i.LogTime + i.Duration <= logTimeTo)    // Окончание внутри запрашиваемого интервала
				)
				.Sum(i =>
				{
					// Отрезаем время слева и справа когда прерывание выходит за пределы обозначенного.
					if (i.LogTime < logTimeFrom)
						return i.Duration - (logTimeFrom - i.LogTime);
					if (logTimeTo < i.LogTime + i.Duration)
						return logTimeTo - i.LogTime;
					return i.Duration;
				});
			var groups = fullLog.GetHistoryGroups();
			// Массив точек запрашиваемой части (включая точки границ)
			var subLogTimes = fullLog.GetSubLogTimesWithBoundaries(false, logTimeFrom, logTimeTo);
			var sensorLogRecordsWithRuns = subLogTimes
				.Select(logTime => new
				{
					LogTime   = logTime,
					Date      = logTime.ToUtcDateTime().ToLocalTime(),
					Value     = fullLog.SensorByLogTime(sensorLegend, logTime),
					Odometer  = fullLog.GetInterpolatedOdometerValue(logTime) / 100.0m,
					Group     = groups.GetHistoryGroup(logTime)
				})
				.Where(v => v.Value.HasValue && v.Odometer.HasValue && v.Group != null)
				.ToArray();
			var stats = sensorLogRecordsWithRuns
				.Skip(1)
				.Zip(sensorLogRecordsWithRuns, (curr, prev) => new { prev, curr });
			foreach(var i in stats)
			{
				if (i.prev.Group.grType == GroupingType.None)
					continue;
				var time = i.curr.LogTime        - i.prev.LogTime;
				var dist = i.curr.Odometer.Value - i.prev.Odometer.Value;
				var prev = i.prev.Value;
				var curr = i.curr.Value;
				var move = i.prev.Group.IsMove();

				if (move)
					result.TimeWhenRun += time;
				if (prev == 1)
				{
					if (move)
						result.TimeWhenOnAndRun  += time;
					else
						result.TimeWhenOnAndStop += time;

					result.OnTime += time;
					if (curr == 0)
						result.OffCount++;
					result.RunWhenOn += dist;
				}
				else
				{
					result.OffTime += time;
					if (curr == 1)
						result.OnCount++;
					result.RunWhenOff += dist;
				}
			}
			// Заполняем максимальную ошибку
			result.MaxError = sensor.LogRecords
				.Where(x => x.LogTime >= logTimeFrom && x.LogTime <= logTimeTo)
				.Max(l => l.Error);

			// Переводим пробеги в километры
			result.RunWhenOn  /= 1000m;
			result.RunWhenOff /= 1000m;


			return result;
		}
		/// <summary> Выявление интервалов отсутствия данных на массиве временных меток </summary>
		/// <param name="this"> Лог </param>
		/// <param name="allLogTimes"> Набор временных меток (предполагаем, что список отсортирован) </param>
		/// <param name="threshold"> Порог наличия отсутствия данных (максимально допустимое время между соседними точками) </param>
		/// <returns> Набор интервалов отсутствия данных </returns>
		/// <remarks> Возможно стоит снабдить параметрами начала и конца интервала для расчета для части данных </remarks>
		public static List<IntervalRecord> GetInterruptions(this FullLog @this, List<int> allLogTimes, double threshold)
		{
			// Проверка входных данных
			if (@this == null || allLogTimes == null)
				return default;
			// Нет данных один большой разрыв
			if (allLogTimes.Count == 0)
			{
				if (threshold < @this.LogTimeTo - @this.LogTimeFrom)
					return new List<IntervalRecord>()
					{
						new IntervalRecord
						{
							LogTime  = @this.LogTimeFrom,
							Duration = @this.LogTimeTo - @this.LogTimeFrom // Только разница, т.к. интервал целый и нет следующих за ним
						}
					};
			}
			// Массив входных точек не может быть больше чем сам интервал запроса лога
			allLogTimes = allLogTimes
				.Where(x => @this.LogTimeFrom <= x && x <= @this.LogTimeTo)
				.ToList();
			// Рассчитываем интервалы прерывания данных
			return allLogTimes
				.Skip(1)
				.Zip(allLogTimes, (curr, prev) => new { Beg = prev, End = curr })
				.Where(interval => threshold < interval.End - interval.Beg)
				.Select(interval => new IntervalRecord
				{
					LogTime  = interval.Beg,
					Duration = interval.End - interval.Beg
				})
				.ToList();
		}
		public static int? SpeedByLogTime(this FullLog @this, int logTime)
		{
			var currentOrNextGPS = BinarySearch(@this.Gps, logTime, out GPSLogRecord? previousGPS);
			if (currentOrNextGPS == null && previousGPS == null)
				return null;
			if (currentOrNextGPS == null)
				return 0; //Т.е. previousGPS != null
			if (previousGPS == null)
				return currentOrNextGPS.Value.Speed; //Т.е. currentOrNextGPS != null
			var k = ((double)
				  (currentOrNextGPS.Value.Speed   - previousGPS.Value.Speed))
				/ (currentOrNextGPS.Value.LogTime - previousGPS.Value.LogTime);

			return (byte)(k * (logTime - previousGPS.Value.LogTime) + previousGPS.Value.Speed);
		}
		public static bool? IgnitionByLogTime(this FullLog @this, int logTime)
		{
			if (!@this.Sensors.TryGetValue(SensorLegend.Ignition, out Sensor sensor))
				return null;
			var list = sensor.LogRecords;
			if (list.Count == 0)
				return null;

			var index = list.BinarySearch(new SensorLogRecord(logTime, 0), SensorLogRecord.LogTimeComparer);

			if (index >= 0)
				return list[index].Value == 1;

			index = ~index; //Теперь index соответствует минимальному элементу, у которого logTime больше искомого

			if (index == list.Count || index == 0)
				return list[list.Count - 1].Value == 1;

			return list[index - 1].Value == 1;
		}
		/// <summary> Получить значение датчика, по времени </summary>
		/// <param name="this"> Текущий лог </param>
		/// <param name="sensorLegend"> Датчик </param>
		/// <param name="logTime"> Время </param>
		/// <returns></returns>
		public static decimal? SensorByLogTime(this FullLog @this, SensorLegend sensorLegend, int logTime)
		{
			if (!@this.Sensors.TryGetValue(sensorLegend, out Sensor sensor))
				return null;
			var list = sensor.LogRecords;
			if (list.Count == 0)
				return null;

			var bsr = list.BinarySearch(x => logTime.CompareTo(x.LogTime));

			if (bsr.Empty)
				return null;

			if (bsr.HasFrom)
				return list[bsr.From].Value;

			return list[bsr.To].Value;
		}
		internal static T? BinarySearch<T>(List<T> records, int logTime)
		where T : struct, ILogTimeContainer
		{
			var r = records.BinarySearch(x => logTime.CompareTo(x.GetLogTime()));
			return r.Single ? records[r.From] : (T?)null;
		}
		internal static T? BinarySearch<T>(List<T> records, int logTime, out T? previous)
			where T : struct, ILogTimeContainer
		{
			var r = records.BinarySearch(x => logTime.CompareTo(x.GetLogTime()));

			previous = !r.Single && r.HasFrom ? records[r.From] : (T?)null;

			return r.HasTo ? records[r.To] : (T?)null;
		}
		public static GeoLogRecord? GetCurrentOrPreviousGeoRecord(this FullLog @this, int logTime)
		{
			var bsr = @this.Geo.BinarySearch(x => logTime.CompareTo(x.LogTime));
			return bsr.HasFrom ? @this.Geo[bsr.From] : (GeoLogRecord?)null;
		}
		public static GPSLogRecord? GetCurrentOrPreviousGpsRecord(this FullLog @this, int logTime)
		{
			var bsr = @this.Gps.BinarySearch(x => logTime.CompareTo(x.LogTime));
			return bsr.HasFrom ? @this.Gps[bsr.From] : (GPSLogRecord?)null;
		}
		const decimal RelativeFuelSensorError = 0.05m;
		/// <summary> Вычисляем заправки и сливы на основании датчиков топлива </summary>
		public static void FillRefuelsAndDrains(
			this FullLog @this,
			int      vehicleId,
			decimal  vehicleFuelTankVolume,
			decimal  vehicleFuelSpendStandardKilometer,
			decimal  vehicleFuelSpendStandardLiter,
			decimal? vehicleFuelSpendStandardByMH = null,
			SensorLegend[] fuelSensors            = null)
		{
			if (0 >= vehicleId)
				return;
			if (0 >= vehicleFuelTankVolume)
				return;

			// Если датчики уровня топлива (ДУТ) не указаны подставляем подходящие по смыслу
			if (fuelSensors == null)
				fuelSensors = new[] { SensorLegend.Fuel, SensorLegend.CANFuel };

			// Выбираем лучший датчик уровня топлива (ДУТ), по количеству записей
			var fuelSensor = fuelSensors
				?.Where(s => @this.Sensors.ContainsKey(s))
				?.Select(s => new { Sensor = s, RecordCount = @this.Sensors[s].LogRecords.Count })
				?.OrderByDescending(s => s.RecordCount)
				?.FirstOrDefault()
				?.Sensor;
			if (!fuelSensor.HasValue)
				return;

			// Вычисляем статистику по выбранному ДУТ
			var fuelStatistics = @this.CalculateSensorStatistics(fuelSensor.Value, @this.LogTimeFrom, @this.LogTimeTo);
			// Определяем значение ДУТ на начало периода
			var begFuelLevel = @this.SensorByLogTime(fuelSensor.Value, @this.LogTimeFrom);
			if (begFuelLevel == null)
				return;
			// Определяем значение ДУТ на конец периода
			var endFuelLevel   = @this.SensorByLogTime(fuelSensor.Value, @this.LogTimeTo);
			if (endFuelLevel == null)
				return;

			var litresPerKmStandard = 0 != vehicleFuelSpendStandardKilometer
				? vehicleFuelSpendStandardLiter / vehicleFuelSpendStandardKilometer
				: (decimal?)null;

			var absoluteFuelSensorError = fuelStatistics.MaxError
				?? vehicleFuelTankVolume * RelativeFuelSensorError;

			var intervals = @this.FindSensorChangeIntervals(fuelSensor.Value, 5 * 60, absoluteFuelSensorError).ToArray();
			// Заправки
			var refuels = intervals
				.GetSequences(i => 0 < i.To.Value - i.From.Value)
				.Select(g => new SensorChangeInterval { From = g.First().From, To = g.Last().To })
				.Where(r => 1.5m * absoluteFuelSensorError < r.To.Value - r.From.Value)
				.ToList();

			const decimal criticalFuelSpendPerSecond = 1m / 60; // 1 литр в минуту = слив

			// Добавляем к заправкам участки резкого убывания после заправки.
			// В баке может быть перегородка с очень маленьким отверстием.
			// Когда заливают топливо, оно вначале заполняет одну половину бака,
			// после чего начинает постепенно перетекать в другую по закону сообщающихся сосудов.
			// Это занимает значительное время, но считаем, что не более получаса.
			foreach (var refuel in refuels)
			{
				var refuelEnd = refuel.To;
				var lastDrainInterval = intervals
					.Where(i => refuel.To.LogTime <= i.From.LogTime)
					.TakeWhile(i => i.To.LogTime - refuelEnd.LogTime < 30 * 60)
					.LastOrDefault(delegate (SensorChangeInterval i)
					{
						var @to = i.To;
						var fuelConsumption = refuelEnd.Value - @to.Value;

						if (vehicleFuelSpendStandardByMH != null)
						{
							var stats = @this.CalculateSensorStatistics(
								SensorLegend.Ignition, refuelEnd.LogTime, @to.LogTime);
							fuelConsumption -= vehicleFuelSpendStandardByMH.Value * (stats.OnTime / 3600m);
						}
						var drainFuelConsumption = criticalFuelSpendPerSecond * (@to.LogTime - refuelEnd.LogTime);
						return drainFuelConsumption < fuelConsumption;
					});

				if (lastDrainInterval != null)
					refuel.To = lastDrainInterval.To;
			}
			//Сливы
			var drains = intervals
				.GetSequences(delegate (SensorChangeInterval i)
				{
					var @from = i.From;
					var @to = i.To;

					return 0 < @from.Value - @to.Value &&
						!refuels.Any(r => r.From.LogTime <= @from.LogTime && @from.LogTime <= r.To.LogTime);
				})
				.Where(delegate (IEnumerable<SensorChangeInterval> sq)
				{
					var @from = sq.First().From;
					var @to = sq.Last().To;

					var fuelConsumption = @from.Value - @to.Value;

					if (fuelConsumption < 1.5m * absoluteFuelSensorError)
						return false;

					decimal? calculatedFuelConsumption = null;

					if (litresPerKmStandard != null)
					{
						var run = @this.GetRunDistance(@from.LogTime, @to.LogTime);

						calculatedFuelConsumption = litresPerKmStandard.Value * (run / 1000m);
					}

					if (vehicleFuelSpendStandardByMH != null && vehicleFuelSpendStandardByMH.Value != 0)
					{
						var stats = @this.CalculateSensorStatistics(SensorLegend.Ignition, @from.LogTime, @to.LogTime);

						calculatedFuelConsumption = (calculatedFuelConsumption ?? 0) +
							vehicleFuelSpendStandardByMH.Value * (stats.TimeWhenOnAndStop / 3600m);
					}

					if (calculatedFuelConsumption != null)
					{
						//Расход превышает нормальный расход по пробегу и моточасам (на стоянках) за вычетом погрешности датчика
						return 1.5m * absoluteFuelSensorError < fuelConsumption - calculatedFuelConsumption;
					}

					return (@to.LogTime - @from.LogTime) > 0 &&
							criticalFuelSpendPerSecond < fuelConsumption / (@to.LogTime - @from.LogTime);
				})
				.Select(drainSequence => new SensorChangeInterval
				{
					From = drainSequence.First().From,
					To   = drainSequence.Last().To
				})
				.ToList();
			refuels.RemoveAll(r => r.To.Value - r.From.Value < 1.5m * absoluteFuelSensorError);
			drains.RemoveAll(r => r.From.Value - r.To.Value < 1.5m * absoluteFuelSensorError);
			@this.Refuels = refuels;
			@this.Drains  = drains;
		}
		/// <summary> Расчет интервалов действий этап 1 </summary>
		/// <remarks>
		/// Расчёт пробегов для каждой пары точек и определения состояния движения или остановки
		/// Этап 1 вполне самостоятелен, этап 2 нужен не всем.
		/// </remarks>
		/// <param name="fullLog"> Лог </param>
		public static List<ActionIntervalDbInterval> GetActionIntervalsStep1(this FullLog @this)
		{
			var fullLog = @this;
			// блокируем отсутствие значения одометра, при полном отсутствии координат
			var runs = fullLog
				.GetAllLogTimesWithBoundaries(false)
				.Select(logTime => new
				{
					LogTime  = logTime,
					Odometer = (long?)fullLog.GetInterpolatedOdometerValue(logTime)
				})
				.Where(x => x.Odometer.HasValue)
				.Select(x => new RunLogRecord
				{
					LogTime  = x.LogTime,
					Odometer = x.Odometer.Value
				})
				.ToList();
			// Интервалы пробегов (при наличии меньше 2 точек, будет пустой набор интервалов)
			return runs
				.Skip(1)
				.Zip(runs, (curr, prev) =>
				{
					var dist   = curr.Odometer - prev.Odometer;             // Длина интервала в сантиметрах
					var time   = curr.LogTime  - prev.LogTime;              // Длина интервала в секундах
					var isStop = dist == 0 || dist < 30 * 100 && 60 < time; // Остановка - это перемещение менее 30 метров за 1 минуту
					return new ActionIntervalDbInterval
					{
						Start           = prev.LogTime.ToUtcDateTime(),
						End             = curr.LogTime.ToUtcDateTime(),
						TimeSpanSeconds = time,
						Run             = dist / 100.0m,
						AvgSpeed        = dist / 100000.0m / (time / 3600m),
						IsMotion        = !isStop
					};
				})
				.ToList();
		}
		/// <summary> Расчет интервалов действий этап 2 </summary>
		/// <remarks>
		/// Агрегация однотипных интервалов в один (укрупнение интервалов), а также добавление интервалов с 0 размером для указания событий определения координат не GPS
		/// Этап 1 вполне самостоятелен, этап 2 нужен не всем.
		/// </remarks>
		/// <param name="data"> Результат первого этапа обработки </param>
		/// <param name="geoRecords"> Массив данных географических точек </param>
		/// <returns></returns>
		[Obsolete("Похоже больше не нужно (перенесено из TDPersonalServer), изменился алгоритм")]
		public static List<ActionIntervalDbInterval> GetActionIntervalsStep2(this List<ActionIntervalDbInterval> data, List<GeoLogRecord> geoRecords)
		{
			// Объединяем все последовательно идущие интервалы с одинаковым IsMotion
			var sequences = data
				?.GetSequences((prev, curr) =>
					prev.IsMotion == curr.IsMotion &&
					prev.End      == curr.Start)
				?.ToList();
			if (sequences != null)
			{
				// Ищем все точки определения координат (где тип координаты не GPS)
				var splitPositions = geoRecords
					.Where(a => (a.Type ?? PositionType.GPS) != PositionType.GPS)
					.ToList();
				var splitPositionIndex = 0;
				// Изменяем последовательности с учетом точек определения координат (где тип координаты не GPS)
				sequences = sequences.Select(sequence =>
				{
					// Результат деления текущей последовательности интервалов (List<List<ActionIntervalDbInterval>>),
					// на каждую входную последовательность, набор последовательностей
					var result        = new List<List<ActionIntervalDbInterval>>();
					// Начало первого интервала, исходного и результирующего списка
					var sequenceStart = TimeHelper.GetSecondsFromBase(sequence.First().Start);
					// Окончание последнего интервала, исходного и результирующего списка
					var sequenceEnd   = TimeHelper.GetSecondsFromBase(sequence.Last().End);
					// Заполняем список точек, с координатами не GPS для конкретной последовательности интервалов
					var notGpsPositions = new List<GeoLogRecord>();
					while (splitPositions.Count > splitPositionIndex)
					{
						var notGpsPosition = splitPositions[splitPositionIndex];
						if (notGpsPosition.LogTime >= sequenceStart && notGpsPosition.LogTime <= sequenceEnd)
							notGpsPositions.Add(notGpsPosition);

						if (notGpsPosition.LogTime > sequenceEnd)
							break;

						splitPositionIndex++;
					}
					// Проверяем наличие точек с координатами не GPS и если они есть меняем последовательности
					if (notGpsPositions.Any())
					{
						// Берем текущую последовательность интервалов
						var newSequence = sequence;
						// Перебираем все точки с координатами не GPS, для текущей последовательности интервалов
						foreach (var notGpsPosition in notGpsPositions)
						{
							var positionDate = TimeHelper.GetDateTimeUTC(notGpsPosition.LogTime);
							// Делим последовательность на 2, не понятно как ???
							var sequenceItemSearchResult = newSequence.BinarySearch(x =>
							{
								if (x.Start > positionDate)
									return -1;
								return x.End < positionDate ? 1 : 0;
							});

							if (!sequenceItemSearchResult.HasFrom)
								continue;

							var sequenceItemIndex = sequenceItemSearchResult.From;
							// Формируем последовательность до разрыва, включая разрываемую (последняя)
							var currentSequence = newSequence
								.Take(sequenceItemIndex + 1) // +1, чтобы взять разрываемый интервал
								.ToList();
							// Обновляем конечную точку до разрыва в первой последовательности
							currentSequence.Last().End = positionDate;
							// Добавляем первую часть в результат
							result.Add(currentSequence);
							// Формируем последовательность после интервала который разорвали, включая разрываемую (первая)
							newSequence = newSequence
								.TakeAfter(sequenceItemIndex)
								.ToList();
							if (!newSequence.Any())
								continue;
							// Обновляем начальную точку после разрыва в первой последовательности
							var firstElement = newSequence.First();
							if (firstElement.Start < positionDate)
								firstElement.Start = positionDate;
							// Добавляем вторую часть в результат
							result.Add(newSequence);
						}
					}
					else
					{ // если точек не GPS нет, то просто возвращаем последовательность обратно
						result.Add(sequence);
					}

					return result;
				})
				.SelectMany(s => s)
				.ToList();
			}
			// Создаем новые интервалы активностей, после обработки
			return sequences?.Select(intervals =>
			{
				var beg = intervals[0].Start;
				var end = intervals[intervals.Count - 1].End;
				var run = intervals.Sum(interval => interval.Run);
				var dur = (int)(end - beg).TotalSeconds;
				return new ActionIntervalDbInterval
				{
					Start           = beg,
					End             = end,
					TimeSpanSeconds = dur,
					Run             = run,
					IsMotion        = intervals[0].IsMotion,
					AvgSpeed        = run / 1000.0m / (dur / 3600m)
				};
			})
				.ToList();
		}
		public static List<int> GetLogTimesWithBoundaries(this FullLog @this, List<int> logTimes, int? logTimeFrom = default, int? logTimeTo = default)
		{
			logTimeFrom ??= @this.LogTimeFrom;
			logTimeTo   ??= @this.LogTimeTo;
			logTimes = logTimes
				.Where(i => logTimeFrom.Value <= i && i <= logTimeTo.Value)
				.ToList();
			// Добавляем точку начала интервала
			if (!logTimes.Contains(logTimeFrom.Value))
				logTimes.Add(logTimeFrom.Value);
			// Добавляем точку окончания интервала
			if (!logTimes.Contains(logTimeTo.Value))
				logTimes.Add(logTimeTo.Value);
			// Сортируем список временных меток
			logTimes.Sort();
			// Возвращаем новый массив
			return logTimes;
		}
		/// <summary> Получить все точки времени с добавлением точных точек границ запроса лога (истории) </summary>
		/// <param name="this"> Лог (история) </param>
		/// <param name="recompute"> Пересчитывать время по массивам? </param>
		public static List<int> GetAllLogTimesWithBoundaries(this FullLog @this, bool recompute = true)
		{
			return @this.GetLogTimesWithBoundaries(@this.GetAllLogTimes(recompute));
		}
		/// <summary> Получить часть точек времени (нужного интервала) с добавлением точных точек границ запроса лога (истории) </summary>
		/// <param name="this"> Лог (история) </param>
		/// <param name="recompute"> Пересчитывать время по массивам? </param>
		public static List<int> GetSubLogTimesWithBoundaries(this FullLog @this, bool recompute = true, int? logTimeFrom = default, int? logTimeTo = default)
		{
			return @this.GetLogTimesWithBoundaries(@this.GetAllLogTimes(recompute), logTimeFrom, logTimeTo);
		}
		/// <summary> Получить статистику истории по группам </summary>
		/// <param name="this"> Лог </param>
		/// <returns></returns>
		public static HistoryStatistics GetHistoryStatistics(this FullLog @this)
		{
			return @this.GetHistoryStatistics(
				@this.GetHistoryGroups());
		}
		/// <summary> Получить статистику истории по группам </summary>
		/// <param name="this"> Лог </param>
		/// <param name="groups"> Группы </param>
		/// <returns></returns>
		public static HistoryStatistics GetHistoryStatistics(this FullLog @this, List<HistoryGroup> groups)
		{
			var fullLog     = @this;
			var logTimeFrom = fullLog.LogTimeFrom;
			var logTimeTo   = fullLog.LogTimeTo;
			var options     = fullLog.Options;

			var statistics = new HistoryStatistics
			{
				moveTime          = 0,
				stopTime          = 0,
				run               = fullLog.Run,
				gpsAbsenceSeconds = 0
			};

			GPSLogRecord? prevPos = null;

			foreach (var pos in fullLog.Gps)
			{
				var secondsFromPrevPos = pos.LogTime - (prevPos != null ? prevPos.Value.LogTime : logTimeFrom);
				if (options.NoDataPeriod.TotalSeconds < secondsFromPrevPos)
					statistics.gpsAbsenceSeconds += secondsFromPrevPos;
				prevPos = pos;
			}

			if (fullLog.Gps.Any())
				statistics.maxSpeed = fullLog.Gps.Max(gps => gps.Speed);

			var sensorCanRun = fullLog
				?.GetSensorsBySensorLegend(SensorLegend.CANTotalRun)
				?.FirstOrDefault(s => s.LogRecords.Any());
			if (null != sensorCanRun)
				statistics.runCAN = (double)(sensorCanRun.LogRecords.Last().Value - sensorCanRun.LogRecords.First().Value);

			if (options.MaxGeoPointsCount < fullLog.Geo.Count)
			{
				var sourceGeoCount = fullLog.Geo.Count;
				statistics.percentageShown = sourceGeoCount != 0
					? fullLog.Geo.Count * 100 / sourceGeoCount
					: 100;
			}
			else
			{
				statistics.percentageShown = 100;
			}
			// Время движения, это сумма интервалов групп движения
			statistics.moveTime = groups
				.Where(g =>  g.IsMove())
				.Sum(g => g.dur);
			// Время без движения, это сумма интервалов групп без движения (исключая отсутствие данных)
			statistics.stopTime = groups
				.Where(g => !g.IsMove() && g.grType != GroupingType.None)
				.Sum(g => g.dur);

			return statistics;
		}
		public static List<HistoryGroup> GetHistoryGroups(this FullLog @this)
		{
			//return @this.GetHistoryGroups_V1();
			return @this.GetHistoryGroups_V2();
		}
		/// <summary> Получить группы истории </summary>
		/// <param name="this"> Лог </param>
		public static List<HistoryGroup> GetHistoryGroups_V1(this FullLog @this)
		{
			var fullLog       = @this;
			var logTimeFrom   = fullLog.LogTimeFrom;
			var logTimeTo     = fullLog.LogTimeTo;
			var allLogTimesWithBoundaries = fullLog.GetAllLogTimesWithBoundaries(false);
			var interruptions = fullLog.GetInterruptions(allLogTimesWithBoundaries, fullLog.Options.NoDataPeriod.TotalSeconds);
			var actionIntervals = fullLog
				// Получаем интервалы действий
				?.GetActionIntervalsStep1()
				// Объединяем одинаковые интервалы действий
				?.GetSequences((prev, curr) =>
					prev.IsMotion == curr.IsMotion &&
					prev.End      == curr.Start)
				?.Select(intervals =>
				{
					var beg = intervals[0].Start;
					var end = intervals[intervals.Count - 1].End;
					var run = intervals.Sum(interval => interval.Run);
					var dur = (int)(end - beg).TotalSeconds;
					return new ActionIntervalDbInterval
					{
						Start           = beg,
						End             = end,
						TimeSpanSeconds = dur,
						Run             = run,
						IsMotion        = intervals[0].IsMotion,
						AvgSpeed        = run / 1000.0m / (dur / 3600m)
					};
				})
				?.ToList();

			var groups = allLogTimesWithBoundaries
				.Select(logTime =>
				{
					var geoPosition = fullLog.Geo.BinarySearch(x => logTime - x.LogTime);
					return new
					{
						LogTime        = logTime,
						PositionType   = geoPosition.Single ? fullLog.Geo[geoPosition.From].Type ?? PositionType.GPS : PositionType.GPS,
						ActionInterval = actionIntervals.GetActionInterval(logTime),
						Interrupt      = interruptions.Any(i => i.LogTime <= logTime && logTime <= i.LogTime + i.Duration)
					};
				})
				// Удаляем точки без длительности и для которых нет интервалов движения, но оставляем любые точки если PositionType не GPS
				.Where(x => x.PositionType != PositionType.GPS || (x.ActionInterval != null && 0 < x.ActionInterval.TimeSpanSeconds))
				.GetGreedySequences((prev, curr) =>
				{
					var prevPosType   = prev[0].PositionType;
					var currPosType   = curr.PositionType;
					var prevActionI   = prev[0].ActionInterval;
					var currActionI   = curr.ActionInterval;
					var prevInterrupt = prev[0].Interrupt;
					var currInterrupt = curr.Interrupt;
					var stopPeriod  = (fullLog.Options.StopPeriod ?? FullLogOptions.DefaultStopPeriod).TotalSeconds;
					// Выделяем как отдельный элемент выходной последовательности любую точку у которой тип координаты не GPS
					if (prevPosType != PositionType.GPS || currPosType != PositionType.GPS)
						return false;
					if (prevInterrupt != currInterrupt)
						return false;
					// Продолжаем если переходим к остановке, но curr меньше интервала остановки (поглощаем короткие остановки движением)
					if (prevActionI.IsMotion && !currActionI.IsMotion && currActionI.TimeSpanSeconds < stopPeriod)
						return true;
					// Продолжаем предыдущую последовательность если не изменился параметр IsMotion
					return prevActionI.IsMotion == curr.ActionInterval.IsMotion;
				})
				.Select(x =>
				{
					var fstInterval  = x.First().ActionInterval;
					var lstInterval  = x.Last().ActionInterval;
					var actIntervals = x?.Select(y => y.ActionInterval)?.Distinct();
					var result = new HistoryGroup
					{
						LogTimeFrom = fstInterval != null ? fstInterval.Start.ToLogTime() : x.First().LogTime,
						LogTimeTo   = lstInterval != null ? lstInterval.End.ToLogTime()   : x.Last().LogTime,
						run         = actIntervals?.Sum(y => y.Run),
						moveTime    = actIntervals?.Where(y =>  y.IsMotion)?.Sum(y => y.TimeSpanSeconds),
						stopTime    = actIntervals?.Where(y => !y.IsMotion)?.Sum(y => y.TimeSpanSeconds),
					};

					if (result.LogTimeFrom < logTimeFrom)
						result.LogTimeFrom = logTimeFrom;
					if (result.LogTimeTo > logTimeTo)
						result.LogTimeTo = logTimeTo;

					var positionType = x.First().PositionType;
					if (positionType != PositionType.GPS)
					{
						result.dur       = 0;
						result.LogTimeTo = result.LogTimeFrom = x.First().LogTime;
						result.grType    = (GroupingType)(int)positionType;
					}
					else
					{
						var duration  = result.LogTimeTo - result.LogTimeFrom;
						result.dur    = duration;
						if (!x.First().Interrupt)
							result.grType = fstInterval != null && fstInterval.IsMotion
								? GroupingType.Motion
								: GroupingType.Stop;
						else
							result.grType = GroupingType.None;
					}
					return result;
				})
				.ToList();
			// Если групп нет, то возвращаем одну на весь интервал
			if (!groups.Any())
			{
				groups.Add(new HistoryGroup
				{
					grType      = GroupingType.None,
					LogTimeFrom = logTimeFrom,
					LogTimeTo   = logTimeTo,
					dur         = logTimeTo - logTimeFrom,
					run         = 0,
				});
			}

			return groups;
		}
		/// <summary> Получить группы истории </summary>
		/// <param name="this"> Лог </param>
		public static List<HistoryGroup> GetHistoryGroups_V2(this FullLog @this)
		{
			var fullLog         = @this;
			var logTimeFrom     = fullLog.LogTimeFrom;
			var logTimeTo       = fullLog.LogTimeTo;
			var actionIntervals = fullLog
				?.GetActionIntervalsStep1()
				?.Select(interval =>
				{
					var logTimeBeg  = interval.Start.ToLogTime();
					var logTimeEnd  = interval.End.ToLogTime();
					var logTimeDif  = logTimeEnd - logTimeBeg;
					var posDataBeg  = fullLog.Geo.BinarySearch(x => logTimeBeg - x.LogTime);
					var posDataEnd  = fullLog.Geo.BinarySearch(x => logTimeEnd - x.LogTime);
					var posTypeBeg  = posDataBeg.Single ? fullLog.Geo[posDataBeg.From].Type ?? PositionType.GPS : PositionType.GPS;
					var posTypeEnd  = posDataEnd.Single ? fullLog.Geo[posDataEnd.To  ].Type ?? PositionType.GPS : PositionType.GPS;
					var isMotion    = interval.IsMotion;
					var isInterrupt = logTimeDif > fullLog.Options.NoDataPeriod.TotalSeconds;
					return new
					{
						LogTimeBeg  = logTimeBeg,
						LogTimeEnd  = logTimeEnd,
						PosTypeBeg  = posTypeBeg,
						PosTypeEnd  = posTypeEnd,
						Run         = interval.Run,
						AvgSpeed    = interval.AvgSpeed,
						IsMotion    = isMotion,
						IsInterrupt = isInterrupt,
					};
				});
			var groups = actionIntervals
				// Объединяем одинаковые
				?.GetSequences((prev, curr) =>
					prev.IsMotion    == curr.IsMotion    &&
					prev.IsInterrupt == curr.IsInterrupt &&
					prev.LogTimeEnd  == curr.LogTimeBeg)
				?.Select(intervals =>
				{
					var logTimeBeg = intervals[0].LogTimeBeg;
					var logTimeEnd = intervals[intervals.Count - 1].LogTimeEnd;
					var posTypeBeg = intervals[0].PosTypeBeg;
					var posTypeEnd = intervals[intervals.Count - 1].PosTypeEnd;
					var run        = intervals.Sum(interval => interval.Run);
					return new
					{
						LogTimeBeg  = logTimeBeg,
						LogTimeEnd  = logTimeEnd,
						PosTypeBeg  = intervals[0].PosTypeBeg,
						PosTypeEnd  = intervals[intervals.Count - 1].PosTypeEnd,
						Run         = run,
						AvgSpeed    = run / 1000.0m / ((logTimeEnd - logTimeBeg) / 3600m),
						IsMotion    = intervals[0].IsMotion,
						IsInterrupt = intervals[0].IsInterrupt,
					};
				})
				?.GetGreedySequences((prev, curr) =>
				{
					var stopPeriod    = (fullLog.Options.StopPeriod ?? FullLogOptions.DefaultStopPeriod).TotalSeconds;
					// Выделяем как отдельный элемент выходной последовательности любую точку у которой тип координаты не GPS
					if (prev[0].PosTypeBeg != PositionType.GPS || prev[0].PosTypeEnd != PositionType.GPS || curr.PosTypeBeg != PositionType.GPS || curr.PosTypeEnd != PositionType.GPS)
						return false;
					if (prev[0].IsInterrupt != curr.IsInterrupt)
						return false;
					// Продолжаем если переходим к остановке, но curr меньше интервала остановки (поглощаем короткие остановки движением)
					if (prev[0].IsMotion && !curr.IsMotion && (curr.LogTimeEnd - curr.LogTimeBeg) < stopPeriod)
						return true;
					// Продолжаем предыдущую последовательность если не изменился параметр IsMotion
					return prev[0].IsMotion == curr.IsMotion;
				})
				?.Select(intervals =>
				{
					var beg = intervals[0].LogTimeBeg;
					var end = intervals[intervals.Count - 1].LogTimeEnd;
					var run = intervals.Sum(interval => interval.Run);
					var dur = end - beg;
					var result = new HistoryGroup
					{
						LogTimeFrom = beg,
						LogTimeTo   = end,
						dur         = end - beg,
						run         = run,
						moveTime    = intervals?.Where(y =>  y.IsMotion)?.Sum(y => y.LogTimeEnd - y.LogTimeBeg),
						stopTime    = intervals?.Where(y => !y.IsMotion)?.Sum(y => y.LogTimeEnd - y.LogTimeBeg),
					};

					if (result.LogTimeFrom < logTimeFrom)
						result.LogTimeFrom = logTimeFrom;
					if (result.LogTimeTo > logTimeTo)
						result.LogTimeTo = logTimeTo;

					var positionType = intervals[0].PosTypeBeg;
					if (positionType != PositionType.GPS)
					{
						result.dur       = 0;
						result.LogTimeTo = result.LogTimeFrom = intervals[0].LogTimeBeg;
						result.grType    = (GroupingType)(int)positionType;
					}
					else
					{
						if (intervals[0].IsInterrupt)
							result.grType = GroupingType.None;
						else
							result.grType = intervals[0].IsMotion
								? GroupingType.Motion
								: GroupingType.Stop;
					}
					return result;
				})
				?.ToList();
			// Если групп нет, то возвращаем одну на весь интервал
			if (!groups.Any())
			{
				groups.Add(new HistoryGroup
				{
					grType      = GroupingType.None,
					LogTimeFrom = logTimeFrom,
					LogTimeTo   = logTimeTo,
					dur         = logTimeTo - logTimeFrom,
					run         = 0,
				});
			}

			return groups;
		}
		/// <summary> Обновить группы с учетом параметра vehicle (замена типа или другие зависимости от объекта) </summary>
		/// <remarks> Подразумевается, что эти обновления не влияют на расчет <see cref="HistoryStatistics"/> </remarks>
		/// <param name="groups"> Группы </param>
		/// <param name="vehicle"> Объект наблюдения </param>
		/// <returns> Измененные входные группы </returns>
		public static List<HistoryGroup> UpdHistoryGroups(this List<HistoryGroup> groups, Vehicle vehicle)
		{
			groups.ForEach(g =>
			{
				// Заменяем остановку выше 5 мин на стоянку, для машин, для других объектов не нужно
				if (g.grType == GroupingType.Stop && g.dur > 5 * 60 && vehicle.IsVehicle)
					g.grType = GroupingType.Stand;
			});
			return groups;
		}
	}
}