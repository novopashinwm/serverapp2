﻿namespace FORIS.TSS.Common.Helpers
{
    public static class IpHelper
    {
        public static bool IsLoopback(string ip)
        {
            return ip == "127.0.0.1" || ip == "::1";
        }
    }
}
