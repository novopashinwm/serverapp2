﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace FORIS.TSS.Common.Helpers
{
	public static class CallStackHelper
	{
		[MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static StackFrame GetCallerStackFrame(int level = 1)
		{
			return new StackFrame(level, true);
		}
		[MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static string GetCallerMethodFullNameMessage(this string message, int level = 1)
		{
			return $"{GetCallerMethodFullName(level + 1)}: {message}";
		}
		[MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static string GetCallerMethodNameMessage(this string message, int level = 1)
		{
			return $"{GetCallerMethodName(level + 1)}: {message}";
		}
		[MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static string GetCallerClassFullNameMessage(this string message, int level = 1)
		{
			return $"{GetCallerClassFullName(level + 1)}: {message}";
		}
		[MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static string GetCallerClassNameMessage(this string message, int level = 1)
		{
			return $"{GetCallerClassName(level + 1)}: {message}";
		}
		[MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static string GetCallerMethodFullName(int level = 1)
		{
			return GetMethodFullName(GetCallerStackFrame(level + 1)?.GetMethod());
		}
		[MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static string GetCallerMethodName(int level = 1)
		{
			return GetMethodName(GetCallerStackFrame(level + 1)?.GetMethod());
		}
		[MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static string GetCallerClassFullName(int level = 1)
		{
			return GetMethodClassFullName(GetCallerStackFrame(level + 1)?.GetMethod());
		}
		[MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static string GetCallerClassName(int level = 1)
		{
			return GetMethodClassName(GetCallerStackFrame(level + 1)?.GetMethod());
		}
		[MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static string GetMethodFullName(MethodBase method)
		{
			var methodClass = GetMethodClassFullNameInternal(method);
			return !string.IsNullOrWhiteSpace(methodClass)
				? $"{methodClass}{Type.Delimiter}{method?.Name ?? string.Empty}"
				: "Unknown";
		}
		[MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static string GetMethodName(MethodBase method)
		{
			var methodClass = GetMethodClassNameInternal(method);
			return !string.IsNullOrWhiteSpace(methodClass)
				? $"{methodClass}{Type.Delimiter}{method?.Name ?? string.Empty}"
				: "Unknown";
		}
		[MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static string GetMethodClassFullName(MethodBase method)
		{
			return GetMethodClassFullNameInternal(method) ?? "Unknown";
		}
		[MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		public static string GetMethodClassName(MethodBase method)
		{
			return GetMethodClassNameInternal(method) ?? "Unknown";
		}
		[MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		private static string GetMethodClassFullNameInternal(MethodBase method)
		{
			return method?.DeclaringType?.GetTypeName(true);
		}
		[MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
		private static string GetMethodClassNameInternal(MethodBase method)
		{
			return method?.DeclaringType?.GetTypeName(false);
		}
	}
}