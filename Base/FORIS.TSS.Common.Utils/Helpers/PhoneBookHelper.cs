﻿using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO.Contacts;
using FORIS.TSS.BusinessLogic.Helpers;

namespace FORIS.TSS.Common.Helpers
{
	public static class PhoneBookHelper
	{
		public static void Filter(this PhoneBook phoneBook)
		{
			var validContacts = new List<PhoneBookContact>(phoneBook.Contacts.Length);
			foreach (var contact in phoneBook.Contacts)
			{
				contact.Value = ContactHelper.GetNormalizedPhone(contact.Value);
				validContacts.Add(contact);
			}

			phoneBook.Contacts = validContacts.ToArray();
		}
	}
}