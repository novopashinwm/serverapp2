﻿using System.Xml;

namespace FORIS.TSS.Helpers
{
    public static class XmlWriterHelper
    {
        public static void WriteElementWithClass(
            this XmlTextWriter writer,
            string name, string @class, string value)
        {
            writer.WriteStartElement(name);
            writer.WriteAttributeString("class", @class);
            writer.WriteString(value);
            writer.WriteEndElement();
        }
    }
}
