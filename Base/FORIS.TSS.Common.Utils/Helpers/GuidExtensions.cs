﻿using System;

namespace FORIS.TSS.Common.Helpers
{
	/// <summary> Расширение для работы с <see cref="Guid"/> </summary>
	public static class GuidExtensions
	{
		public static Guid ToGuid(this int value)
		{
			var guidBytes = Guid.Empty.ToByteArray();
			BitConverter.GetBytes(value).CopyTo(guidBytes, 0);
			return new Guid(guidBytes);
		}
	}
}