﻿namespace FORIS.TSS.Common.Helpers
{
	/// <summary> Расширения для работы с классом <see cref="InputSample"/> </summary>
	/// <remarks> Непосредственно в DTO-объект эту логику добавить нельзя, т.к. функция сортировки недоступна там </remarks>
	public static class InputSampleHelper
	{
		public static long? Get(this InputSample sample, int logTime, int? valueExpired)
		{
			if (sample.Records.Count == 0)
				return null;

			var binarySearchResult = sample.Records.BinarySearch(x => logTime - x.LogTime);
			if (binarySearchResult.Empty)
				return null;
			if (binarySearchResult.Single)
				return sample.Records[binarySearchResult.From].Value;
			if (valueExpired == null)
				return null;
			if (!binarySearchResult.HasFrom)
				return null;
			var firstNewValue = sample.Records[binarySearchResult.From];
			return valueExpired.Value < logTime - firstNewValue.LogTime
				? null
				: firstNewValue.Value;
		}
	}
}