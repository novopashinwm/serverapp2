﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace FORIS.TSS.BusinessLogic.Helpers
{
	public static class ContactHelper
	{
		private static readonly Regex EmailValidationRegex = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");

		private static bool ValidEmail(string email)
		{
			if (email == null)
				throw new ArgumentNullException("email");

			if (email.Length < 5 || 254 < email.Length)
				return false;

			if (EmailValidationRegex.IsMatch(email))
				return true;

			//TODO: найти подходящее регулярное выражение для проверки email, содержащий национальные символы или хотя бы кириллицу - для домена .рф
			//TODO: также можно использовать проверку с помощью MailAddress, но она позволяет отличить правильный адрес от неправильного только через исключение

			//Ожидается наличие символа @
			var atIndex = email.IndexOf('@');
			if (atIndex == -1)
				return false;
			//И @ не может быть ни в первой, ни в последней позиции
			if (atIndex == 0 || atIndex == email.Length - 1)
				return false;
			//Второго @ быть не должно
			var secondAtIndex = email.IndexOf('@', atIndex + 1);
			if (secondAtIndex != -1)
				return false;
			//После @ должен быть домен как минимум второго уровня
			var dotIndex = email.IndexOf('.', atIndex + 1);
			if (dotIndex == -1)
				return false;
			if (dotIndex == atIndex + 1 || dotIndex == email.Length - 1)
				return false;

			return true;
		}

		public static bool IsValidEmail(string email)
		{
			return ValidEmail(email);
		}

		public static IEnumerable<string> ParseEmails(string contacts)
		{
			var result = new List<string>();
			var potentialEmails = contacts.Split('!', '#', '$', '%', '&', ':', ';', ' ', ',', '\r', '\n', '\t')
				.Where(c => c.Contains("@"))
				.ToArray();
			foreach (var potentialEmail in potentialEmails)
			{
				var matches = EmailValidationRegex.Matches(potentialEmail);
				foreach (Match match in matches)
					result.Add(match.Value);
			}

			return result;
		}

		public const int MinPhoneLength = 11;
		public const int MaxPhoneLength = 15;

		private static readonly Regex PhoneValidationStage1Regex = new Regex($@"^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$");
		private static readonly Regex PhoneValidationStage2Regex = new Regex($@"^[0-9]{{{MinPhoneLength},{MaxPhoneLength}}}$");

		public static bool IsValidPhone(string value)
		{
			if (value == null)
				throw new ArgumentNullException("value");
			return PhoneValidationStage1Regex.IsMatch(value)
				? PhoneValidationStage2Regex.IsMatch(Regex.Replace(value, "[^0-9]", ""))
				: false;
		}

		public static string GetNormalizedPhone(string phone)
		{
			// Номер телефона отсутствует
			if (string.IsNullOrWhiteSpace(phone))
				return null;
			// Выбираем только цифры из введенного текста
			if (!phone.All(char.IsDigit))
				phone = new string(phone.Where(char.IsDigit).ToArray());
			// Убираем любую коррекцию номера телефона
			//// При наличии только 10 цифр считаем, что это Россия (внутренний номер)
			//if (phone.Length == 10)
			//	phone = '7' + phone;
			//// При наличии 11 цифр и первой '8' считаем, что это Россия (внутренний номер)
			//if (phone.Length == 11 && phone[0] == '8')
			//	phone = '7' + phone.Substring(1);
			// Не принимаем номера менее 10 цифр и не более 15 (https://ru.wikipedia.org/wiki/E.164)
			if (MinPhoneLength > phone.Length || phone.Length > MaxPhoneLength)
				return null;
			return phone;
		}
	}
}