﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace FORIS.TSS.Common.Helpers
{
	public static class ObjectExtensions
	{
		public static Dictionary<string, object> ToPropertyDictionary(this object obj)
		{
			return obj.GetType()
				.GetProperties()
				.Select(pi => new { Name = pi.Name, Value = pi.GetValue(obj, null) })
				.Union(obj.GetType()
					.GetFields()
					.Select(fi => new { Name = fi.Name, Value = fi.GetValue(obj) }))
				.ToDictionary(ks => ks.Name, vs => vs.Value);
		}
		public static T CloneObject<T>(this T obj) where T : class
		{
			using (var memStream = new MemoryStream())
			{
				var binaryFormatter = new BinaryFormatter(
					null,
					new StreamingContext(StreamingContextStates.Clone));
				binaryFormatter.Serialize(memStream, obj);
				memStream.Seek(0, SeekOrigin.Begin);
				return binaryFormatter.Deserialize(memStream) as T;
			}
		}
		public static void CopyPropertiesTo<SrcType, DstType>(this SrcType src, DstType dst)
		{
			var srcProps = typeof(SrcType).GetProperties()
				.Where(x => x.CanRead)
				.ToList();
			var dstProps = typeof(DstType).GetProperties()
				.Where(x => x.CanWrite)
				.ToList();
			foreach (var srcProp in srcProps)
			{
				if (dstProps.Any(x => x.Name == srcProp.Name))
				{
					var p = dstProps.First(x => x.Name == srcProp.Name);
					if (p.CanWrite)
						p.SetValue(dst, srcProp.GetValue(src, null), null);
				}
			}
		}
		public static bool HasProperty(this Type obj, string propertyName)
		{
			return null != obj.GetProperty(propertyName);
		}
		public static bool HasProperty(this object obj, string propertyName)
		{
			return null != obj.GetType().GetProperty(propertyName);
		}
		/// <summary> Получить все дочерние объекты </summary>
		/// <typeparam name="T"> Тип объекта </typeparam>
		/// <param name="component"> Текущий объект </param>
		/// <param name="getChildren"> Метод получения дочерних объектов </param>
		/// <param name="self"> Возвращать или нет, сам родительский объект </param>
		/// <returns> Дочерние объекты </returns>
		public static IEnumerable<T> GetDescendants<T>(this T composite, Func<T, IEnumerable<T>> getChildren, bool self = false)
		{
			if (self)
				yield return composite;
			foreach (var child in getChildren(composite))
			{
				yield return child;
				foreach (var subChild in child.GetDescendants(getChildren, false))
					yield return subChild;
			}
		}
		public static string GetTypeName(this Type typ, bool withNamespaces = true)
		{
			if (null == typ)
				return "Unknown";
			var fullName = CodeDomProvider.CreateProvider("C#")
					.GetTypeOutput(new CodeTypeReference(typ));
			if (!withNamespaces)
			{
				var types = typ
					.GetDescendants((t) => (!t.IsArray ? t : t.GetElementType()).GenericTypeArguments, true)
					.Distinct()
					// Нужно для удаления сначала длинных, а потом коротких, т.к. могут быть вложенные
					.OrderByDescending(t => t.Namespace);
				foreach (var type in types)
					if (!string.IsNullOrWhiteSpace(type.Namespace))
						fullName = fullName.Replace($"{type.Namespace}{Type.Delimiter}", "");
			}
			return fullName;
		}
		public static string GetTypeName(this object obj, bool withNamespaces = true)
		{
			return obj.GetType().GetTypeName(withNamespaces);
		}
	}
}