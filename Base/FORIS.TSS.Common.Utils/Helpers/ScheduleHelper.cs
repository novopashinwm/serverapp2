﻿using System;

namespace FORIS.TSS.BusinessLogic
{
	/// <summary>
	/// Summary description for ScheduleHelper.
	/// </summary>
	public class ScheduleHelper
	{
		public static DateTime MoveToStartOfMonth(DateTime datetime)
		{
			DateTime nextTime = new DateTime(datetime.Year, datetime.Month, 1, datetime.Hour, datetime.Minute, datetime.Second, datetime.Millisecond);
			return nextTime;
		}
		public static DateTime MoveToStartOfWeek(DateTime datetime)
		{
			if (datetime.DayOfWeek == DayOfWeek.Tuesday)
			{
				return datetime.AddDays(-1);
			}
			if (datetime.DayOfWeek == DayOfWeek.Wednesday)
			{
				return datetime.AddDays(-2);
			}
			if (datetime.DayOfWeek == DayOfWeek.Thursday)
			{
				return datetime.AddDays(-3);
			}
			if (datetime.DayOfWeek == DayOfWeek.Friday)
			{
				return datetime.AddDays(-4);
			}
			if (datetime.DayOfWeek == DayOfWeek.Saturday)
			{
				return datetime.AddDays(-5);
			}
			if (datetime.DayOfWeek == DayOfWeek.Sunday)
			{
				return datetime.AddDays(-6);
			}
			return datetime;
		}
		public static DateTime MoveForwardToDayOfWeek(DateTime datetime, int dayOfWeek)
		{
			DateTime nextTime = datetime;
			for (int i = 0; i < 7; ++i)
			{
				if (nextTime.DayOfWeek == System.DayOfWeek.Monday && dayOfWeek == 1)
				{
					break;
				}
				if (nextTime.DayOfWeek == System.DayOfWeek.Tuesday && dayOfWeek == 2)
				{
					break;
				}
				if (nextTime.DayOfWeek == System.DayOfWeek.Wednesday && dayOfWeek == 3)
				{
					break;
				}
				if (nextTime.DayOfWeek == System.DayOfWeek.Thursday && dayOfWeek == 4)
				{
					break;
				}
				if (nextTime.DayOfWeek == System.DayOfWeek.Friday && dayOfWeek == 5)
				{
					break;
				}
				if (nextTime.DayOfWeek == System.DayOfWeek.Saturday && dayOfWeek == 6)
				{
					break;
				}
				if (nextTime.DayOfWeek == System.DayOfWeek.Sunday && dayOfWeek == 7)
				{
					break;
				}
				nextTime = nextTime.AddDays(1);
			}
			return nextTime;
		}
		public static DateTime MoveForwardToBeginOfMonth(DateTime datetime, int dayOfWeek)
		{
			DateTime nextTime = datetime;
			for (int i = 1; i < 31; ++i)
			{
				nextTime = nextTime.AddDays(1);
				if (nextTime.Day == 1)
				{
					break;
				}
			}
			return nextTime;
		}
		public static DateTime MoveForwardToStartOfYear(DateTime datetime)
		{
			DateTime nextTime = new DateTime(datetime.Year, 1, 1, datetime.Hour, datetime.Minute, datetime.Second, datetime.Millisecond);
			return nextTime;
		}
	}
}