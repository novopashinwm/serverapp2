﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FORIS.TSS.Common
{
	///<summary> Реализация логики для поточного чтения данных из массива байт </summary>
	public class DataReader
	{
		protected readonly byte[] _data;
		private   readonly int    _endIndex;
		private   readonly int    _startIndex;
		private            int    _index;

		///<summary> Конструктор для чтения всех байт из массива </summary>
		///<param name="data"> Массив байт </param>
		public DataReader(byte[] data)
			: this(data, 0, data.Length)
		{
		}
		/// <summary> Конструктор для чтения байт начиная со startIndex до конца </summary>
		public DataReader(byte[] data, int startIndex)
			: this(data, startIndex, data.Length)
		{
		}
		/// <summary> Конструктор для чтения байт начиная со startIndex по endIndex (не включая endIndex) </summary>
		public DataReader(byte[] data, int startIndex, int endIndex)
		{
			_data       = data;
			_startIndex = startIndex;
			_index      = startIndex;
			_endIndex   = endIndex;
		}
		/// <summary> Возвращает позицию в входном массиве </summary>
		public int Position
		{
			get { return _index; }
		}
		/// <summary> Возвращает количество непрочитанных байт </summary>
		public int RemainingBytesCount
		{
			get { return _endIndex - _index; }
		}
		/// <summary> Возвращает количество прочитанных байт </summary>
		public int BytesRead
		{
			get { return _index - _startIndex; }
		}
		/// <summary> Возвращает количество байт </summary>
		public int TotalBytes
		{
			get { return _endIndex; }
		}
		public byte[] GetRemainingBytes()
		{
			if (!Any())
				return null;
			return ReadBytes(RemainingBytesCount);
		}
		///<summary> Читает 1 байт </summary>
		public byte ReadByte()
		{
			if (_endIndex <= _index)
				throw new InvalidOperationException("Nothing to read");
			return _data[_index++];
		}
		/// <summary> Возвращает true, если есть непрочитанные данные </summary>
		public bool Any()
		{
			return _index != _endIndex;
		}
		/// <summary> Пропускает 1 байт </summary>
		public DataReader Skip()
		{
			if (_index == _endIndex)
				throw new InvalidOperationException("Nothing to read");

			++_index;
			return this;
		}
		/// <summary> Пропускает count байт </summary>
		public DataReader Skip(int count)
		{
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count), count, @"Value could not be less than zero");
			if (_endIndex - _index < count)
				throw new ArgumentOutOfRangeException(nameof(count), count, @"Too many data has been requested");
			_index += count;
			return this;
		}
		/// <summary> Пропускает массив байт с точными значениями </summary>
		public DataReader Skip(byte[] bytes)
		{
			foreach (var expected in bytes)
			{
				var actual = ReadByte();
				if (actual != expected)
					throw new InvalidOperationException("Wrong byte " + _data[_index] + " at position " + _index + ", expected " + expected);
			}
			return this;
		}
		/// <summary> Пропускает указанный символ или выбрасывает исключение, если символ на текущей позиции отличается от указанного </summary>
		/// <param name="c"> Символ </param>
		public DataReader SkipChar(char c)
		{
			if (_data[_index] != c)
				throw new InvalidOperationException("Wrong symbol " + _data[_index] +  " at position " + _index + ", expected " + c);
			++_index;
			return this;
		}
		/// <summary>Читает count байт как ASCII-строку</summary>
		public string ReadAsciiString(int count)
		{
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count), count, @"Value could not be less than zero");
			if (_endIndex - _index < count)
				throw new ArgumentOutOfRangeException(nameof(count), count, @"Too many data has been requested");
			
			var result = Encoding.ASCII.GetString(_data, _index, count);
			_index += count;
			return result;
		}
		public DataReader SkipAsciiString(string s)
		{
			foreach (var c in s)
				SkipChar(c);
			return this;
		}
		/// <summary> Читает count байт </summary>
		public byte[] ReadBytes(int count)
		{
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count), count, @"Value could not be less than zero");
			if (_endIndex - _index < count)
				throw new ArgumentOutOfRangeException(nameof(count), count, @"Too many data has been requested");
			var result = new byte[count];
			Array.Copy(_data, _index, result, 0, count);
			_index += count;
			return result;
		}
		public IEnumerable<byte> ReadBytesWhile(Func<byte, bool> predicate)
		{
			// Нет условия выходим сразу
			if (predicate == null)
				yield break;
			// Возвращаем байты пока они есть и удовлетворяют условию
			while (Any() && predicate(_data[_index]))
				yield return ReadByte();
		}
		/// <summary> Читает count байт как десятичное положительное целое число, записанное в виде ASCII строки </summary>
		public int ReadAsciiNumber10(int count)
		{
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count), count, @"Value could not be less or equal to zero");
			if (8 < count)
				throw new ArgumentOutOfRangeException(nameof(count), count, @"Value could not be greater than 8");
			if (_endIndex - _index < count)
				throw new ArgumentOutOfRangeException(nameof(count), count, @"Too many data has been requested");
			var result = 0;
			for (var i = 0; i != count; ++i)
			{
				var @byte = ReadByte();
				if (@byte < '0' || '9' < @byte)
					throw new InvalidOperationException("ASCII digit sequence expected, but not " + @byte);
				result *= 10;
				result += @byte - '0';
			}

			return result;
		}
		public DataReader Rewind()
		{
			_index = _startIndex;
			return this;
		}
		public DataReader Rewind(int count)
		{
			_index -= count;
			return this;
		}
		/// <summary> Читает count байт как целое без знаковое число в big-endian </summary>
		public UInt16 ReadBigEndian16(int count = sizeof(UInt16))
		{
			return ReadBytes(count).FromBE2UInt16(0, count);
		}
		/// <summary> Читает count байт как целое без знаковое число в big-endian </summary>
		public UInt32 ReadBigEndian32(int count = sizeof(UInt32))
		{
			return ReadBytes(count).FromBE2UInt32(0, count);
		}
		public UInt64 ReadBigEndian64(int count = sizeof(UInt64))
		{
			return ReadBytes(count).FromBE2UInt64(0, count);
		}
		public float ReadSingle()
		{
			var result = BitConverter.ToSingle(_data, _index);
			Skip(sizeof(float));
			return result;
		}
		public UInt16 ReadLittleEndian16(int count = sizeof(UInt16))
		{
			return ReadBytes(count).FromLE2UInt16(0, count);
		}
		public UInt32 ReadLittleEndian32(int count = sizeof(UInt32))
		{
			return ReadBytes(count).FromLE2UInt32(0, count);
		}
		public UInt64 ReadLittleEndian64(int count = sizeof(UInt64))
		{
			return ReadBytes(count).FromLE2UInt64(0, count);
		}
		public UInt16 Bcd16(int count = sizeof(UInt16))
		{
			return ReadBytes(count).FromBcd2UInt16(0, count);
		}
		/// <summary> Читает count байт как двоично-десятичное число </summary>
		public UInt32 Bcd32(int count = sizeof(UInt32))
		{
			return ReadBytes(count).FromBcd2UInt32(0, count);
		}
		/// <summary> Читает count байт как двоично-десятичное число </summary>
		public UInt64 Bcd64(int count = sizeof(UInt64))
		{
			return ReadBytes(count).FromBcd2UInt64(0, count);
		}
	}
}