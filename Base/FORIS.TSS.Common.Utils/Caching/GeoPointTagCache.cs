﻿using System;

namespace FORIS.TSS.Common.Caching
{
	/// <summary> Кэш для данных, привязанных к географическим координатам </summary>
	/// <typeparam name="TValue"> Тип данных, которые можно получить по координате </typeparam>
	public sealed class GeoPointTagCache<TValue>
	{
		public int CellSizeMeters { get; }
		/// <summary> Кэш на основе geo данных и языка </summary>
		/// <param name="getValue"> Метод получения данных </param>
		/// <param name="capacity"> Размер кэша </param>
		/// <param name="cacheNulls"> Кэшировать ли значения если loader вернул null </param>
		/// <param name="cellSizeMeters"> Размер географической ячейки кэша в метрах </param>
		public GeoPointTagCache(Func<double, double, string, TValue> getValue, int capacity, bool cacheNulls, int cellSizeMeters = GeoPointTag.CellSize)
		{
			CellSizeMeters = cellSizeMeters;

			_valueByPoint = new TimingCache<GeoPointTag, TValue>(
				TimeSpan.MaxValue,
				point => getValue(point.Lat, point.Lng, point.Tag),
				capacity, cacheNulls);
		}
		public TValue this[double lat, double lng, string tag]
		{
			get
			{
				return _valueByPoint[new GeoPointTag(lat, lng, tag, CellSizeMeters)];
			}
			set
			{
				_valueByPoint[new GeoPointTag(lat, lng, tag, CellSizeMeters)] = value;
			}
		}
		public bool TryGetValue(double lat, double lng, string tag, out TValue value)
		{
			return _valueByPoint.TryGetValue(new GeoPointTag(lat, lng, tag, CellSizeMeters), out value);
		}
		private readonly TimingCache<GeoPointTag, TValue> _valueByPoint;
	}
}