﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;

namespace FORIS.TSS.Common.Caching
{
	///<summary> Кэш, объекты внутри которого обновляются через заданный интервал времени </summary>
	///<typeparam name="TKey"> Тип ключа, по которому можно искать кэшированные объекты </typeparam>
	///<typeparam name="TValue"> Тип кэшируемого значения. Должно не поддерживать <see cref="IDisposable"/> </typeparam>
	public class TimingCache<TKey, TValue> : ICache<TKey, TValue>
	{
		class Record
		{
			public DateTime SetTimeStamp;
			private TValue _value;
			public DateTime GetTimeStamp
			{
				get; private set;
			}
			public TValue Value
			{
				get
				{
					GetTimeStamp = DateTime.UtcNow;
					return _value;
				}
				set
				{
					_value = value;
				}
			}
			public Record(DateTime timeStamp)
			{
				SetTimeStamp = timeStamp;
			}
		}

		private int GetTimeStampComparison(TKey x, TKey y)
		{
			return _cache[x].GetTimeStamp.CompareTo(_cache[y].GetTimeStamp);
		}

		private readonly TimeSpan                           _lifeTime;
		private readonly bool                               _cacheNulls;
		private readonly Converter<TKey, TValue>            _loader;
		private readonly ConcurrentDictionary<TKey, Record> _cache;
		private readonly int                                _capacity;
		private readonly Random                             _random = new Random();

		///<summary> Инициализирует кэш </summary>
		///<param name="lifeTime"> Время жизни объектов, по истечению которого они должны обновляться </param>
		///<param name="loader"> Метод, который по ключу создает новый экземпляр кэшируемого объекта </param>
		///<param name="capacity"> Оценка количества объектов в кэше </param>
		/// <param name="cacheNulls"> Кэшировать ли значения если loader вернул null </param>
		public TimingCache(TimeSpan lifeTime, Converter<TKey, TValue> loader, int capacity, bool cacheNulls = true)
		{
			if (loader == null)
				throw new ArgumentNullException(nameof(loader));

			_lifeTime   = lifeTime;
			_loader     = loader;
			_cache      = new ConcurrentDictionary<TKey, Record>();
			_capacity   = capacity;
			_cacheNulls = cacheNulls;
		}
		///<summary> Инициализирует кэш </summary>
		///<param name="lifeTime"> Время жизни объектов, по истечению которого они должны обновляться </param>
		///<param name="loader"> Метод, который по ключу создает новый экземпляр кэшируемого объекта </param>
		public TimingCache(TimeSpan lifeTime, Converter<TKey, TValue> loader)
			: this(lifeTime, loader, int.MaxValue)
		{
		}

		#region ICache<TKey,TValue> Members

		private Record CreateRecord(TKey key)
		{
			return new Record(DateTime.MinValue);
		}

		///<summary> Возвращает значение из кэша по ключу </summary>
		///<param name="key"> Ключ, по которому идентифицируется объект </param>
		public TValue this[TKey key]
		{
			get
			{
				if (_lifeTime == TimeSpan.Zero)
					return _loader(key);

				Record record;
				if (!_cache.TryGetValue(key, out record))
				{
					ClearOldItemsIfNeeded();
					record              = new Record(DateTime.MinValue);
					record.Value        = _loader(key);
					record.SetTimeStamp = CreateSetTimeStampFirstTime();

					// Не добавляем в кэш если loader вернул null и тип не является структурой
					if (!_cacheNulls                &&
						!typeof(TValue).IsValueType &&
						EqualityComparer<TValue>.Default.Equals(record.Value, default(TValue)))
					{
						return record.Value;
					}

					_cache.TryAdd(key, record);
					return record.Value;
				}

				// Проверка не устарела ли запись
				if (record.SetTimeStamp < DateTime.UtcNow)
				{
					lock (record)
					{
						if (record.SetTimeStamp < DateTime.UtcNow)
						{
							var newValue = _loader(key);
							// Удаляем из кэша если loader вернул null и тип не является структурой
							if (!_cacheNulls                &&
								!typeof(TValue).IsValueType &&
								EqualityComparer<TValue>.Default.Equals(newValue, default(TValue)))
							{
								Record outRec;
								_cache.TryRemove(key, out outRec);
							}

							record.Value = newValue;

							// Хранится время, когда значение будет считаться устаревшим.
							// Таким образом, сумма вычисляется ровно на каждый вызов _loader
							record.SetTimeStamp = CreateSetTimeStamp();
						}
					}
				}

				return record.Value;
			}
			set
			{
				Record record;

				if (!_cache.TryGetValue(key, out record))
				{
					ClearOldItemsIfNeeded();
				}

				record = new Record(CreateSetTimeStamp()) { Value = value };

				record = _cache
					.AddOrUpdate(key, record, (updateKey, existingVal) =>
					{
						existingVal.Value        = value;
						existingVal.SetTimeStamp = CreateSetTimeStamp();
						return existingVal;
					});
			}
		}
		/// <summary> Достать значение из кэша без загрузки </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public bool TryGetValue(TKey key, out TValue value)
		{
			Record record;
			var result = _cache.TryGetValue(key, out record);
			value = result && record != null && record.SetTimeStamp > DateTime.UtcNow
				? record.Value
				: default(TValue);
			return result;
		}
		private void ClearOldItemsIfNeeded()
		{
			if (_cache.Count < _capacity)
				return;
			//TODO: организовать очередь с приоритетами, чтобы не блокировать весь кэш на время очистки, а просто удалять самый неиспользуемый элемент при вставке нового
			lock (_cache)
			{
				var dictionaryContent = _cache.Keys.ToList();
				dictionaryContent.Sort(GetTimeStampComparison);

				foreach (var item in dictionaryContent.Take(dictionaryContent.Count / 2))
				{
					Record outRec;
					_cache.TryRemove(item, out outRec);
				}
			}
		}
		private DateTime CreateSetTimeStamp()
		{
			if (_lifeTime == TimeSpan.MaxValue)
				return DateTime.MaxValue;

			return DateTime.UtcNow.Add(_lifeTime);
		}
		private DateTime CreateSetTimeStampFirstTime()
		{
			if (_lifeTime == TimeSpan.MaxValue)
				return DateTime.MaxValue;

			var result = DateTime.UtcNow.Add(_lifeTime);

			//Добавляем случайную величину из времени жизни, чтобы уменьшить вероятность ситуации,
			//когда сразу все записи потребуют
			result = result.AddMilliseconds(_random.Next((int) _lifeTime.TotalMilliseconds));

			return result;
		}
		///<summary> Принудительно очищает кэш </summary>
		public void Clear()
		{
			lock (_cache)
			{
				_cache.Clear();
			}
		}

		#endregion ICache<TKey,TValue> Members
	}
}