﻿using System;

namespace FORIS.TSS.Common.Caching
{
	///<summary> Интерфейс для кэширования объектов </summary>
	///<typeparam name="TKey"> Тип ключа, по которому можно искать объекты в кэше </typeparam>
	///<typeparam name="TValue"> Тип кэшируемого значения. Должно не поддерживать <see cref="IDisposable"/>.</typeparam>
	public interface ICache<TKey, TValue> : IClearableCache
	{
		///<summary> Возвращает значение из кэша по ключу </summary>
		///<param name="key"> Ключ, по которому идентифицируется объект </param>
		TValue this[TKey key] { get; }
	}
}