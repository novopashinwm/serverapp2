﻿namespace FORIS.TSS.Common.Caching
{
	/// <summary> Очищаемый кэш </summary>
	public interface IClearableCache
	{
		///<summary> Принудительно очищает кэш </summary>
		void Clear();
	}
}