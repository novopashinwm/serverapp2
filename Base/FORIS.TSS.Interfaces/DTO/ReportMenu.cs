﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	[DataContract]
	public enum ReportMenuGroupType
	{
		[EnumMember] Unknown,
		[EnumMember] MlpReports,
		[EnumMember] CommonReports,
		[EnumMember] GeoPointReports,
		[EnumMember] GpsReports,
		[EnumMember] SensorsReports,
		[EnumMember] GeocodingViaExcelReports,
		[EnumMember] DrivingSafety,
	}
	/// <summary> Группа меню для отчетов </summary>
	[Serializable]
	[DataContract]
	[XmlRoot("ReportMenuGroup")]
	public class ReportMenuGroup
	{
		[DataMember] public int                  Id              { get; set; }
		[DataMember] public string               Name            { get; set; }
		[DataMember] public ReportMenuGroupType  GroupType       { get; set; }
		[DataMember] public List<ReportMenuItem> ReportMenuItems { get; set; }
		[DataMember] public bool                 IsAvailable     { get; set; }
	}
	/// <summary> Класс - пункт меню отчетов </summary>
	[Serializable]
	[DataContract]
	[XmlRoot("ReportMenuItem")]
	public class ReportMenuItem
	{
		[DataMember]       public int                      Id                    { get; set; }
		[DataMember]       public string                   Name                  { get; set; }
		[DataMember]       public string                   Description           { get; set; }
		[DataMember]       public int                      ReportMenuGroupId     { get; set; }
		[DataMember]       public bool                     Selected              { get; set; }
		[DataMember]       public bool                     IsAvailable           { get; set; }
		[DataMember]       public bool                     IsSubscriptionVisible { get; set; }
		[IgnoreDataMember] public string                   AssemblyName          { get; set; }
		[DataMember]       public ReportMenuGroupType      GroupType             { get; set; }
		[IgnoreDataMember] public string                   ClassParametersName   { get; set; }
		[IgnoreDataMember] public string                   ClassResourcesName    { get; set; }
		[DataMember]       public string                   Guid                  { get; set; }
		[DataMember]       public int                      Order                 { get; set; }
		[DataMember]       public ReportTypeEnum[]         Formats               { get; set; }
		[DataMember]       public List<ReportMenuProperty> Properties            { get; private set; } = new List<ReportMenuProperty>();
	}
	/// <summary> Свойство пункта меню </summary>
	[Serializable]
	[DataContract]
	[XmlRoot("ReportMenuProperty")]
	public class ReportMenuProperty
	{
		[DataMember] public string                   Name        { get; set; }
		[DataMember] public Type                     Type        { get; set; }
		[DataMember] public IEnumerable<SystemRight> Rights      { get; set; }
		[DataMember] public string                   Control     { get; set; }
		[DataMember] public string                   Description { get; set; }
		[DataMember] public string                   Options     { get; set; }
		[DataMember] public int                      Order       { get; set; }
	}
	/// <summary> Параметры необходимые для вычисления доступности групп меню </summary>
	[Serializable]
	[DataContract]
	[XmlRoot("ReportMenuVehiclesCapabilities")]
	public class ReportMenuVehiclesCapabilities
	{
		[DataMember] public bool IsAskPositionAvailable         { get; set; }
		[DataMember] public bool IsIginitionMonitoringAvailable { get; set; }
		[DataMember] public bool HasSpeedMonitoring             { get; set; }
		[DataMember] public bool HasDigitalSensors              { get; set; }
		[DataMember] public bool IsSalesAvailable               { get; set; }
		[DataMember] public bool IsWhoAskedMeAvailable          { get; set; }
		[DataMember] public bool IsPathAccess                   { get; set; } = false;
	}
}