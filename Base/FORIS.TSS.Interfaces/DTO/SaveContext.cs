﻿using System.Globalization;

namespace FORIS.TSS.BusinessLogic.DTO
{
    public enum SaveContextIdType
    {
        None,
        Message,
        Operator,
        Session,
        SchedulerQueue
    }

    public class SaveContext
    {
        public SaveContextIdType Type;

        public int Id;

        private SaveContext(SaveContextIdType type, int id)
        {
            Type = type;
            Id = id;
        }

        public override string ToString()
        {
            return string.Format(CultureInfo.CurrentCulture, "{0} {1}", Type, Id);
        }

        public static SaveContext Session(int id)
        {
            return new SaveContext(SaveContextIdType.Session, id);
        }

        public static SaveContext Message(int id)
        {
            return new SaveContext(SaveContextIdType.Message, id);
        }

        public static SaveContext Operator(int id)
        {
            return new SaveContext(SaveContextIdType.Operator, id);
        }

        public static SaveContext None()
        {
            return new SaveContext(SaveContextIdType.None, 0);
        }

        public static SaveContext Scheduler(int schedulerQueueId)
        {
            return new SaveContext(SaveContextIdType.SchedulerQueue, schedulerQueueId);
        }
    }
}
