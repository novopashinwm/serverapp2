﻿using System;
using System.Runtime.Serialization;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[DataContract(IsReference = true)]
	[Serializable()]
	public class WebPoint
	{
		public const int                  EmptyId            = -1;
		[DataMember] public int           Id   { get; set; } = EmptyId;
		[DataMember] public string        Name { get; set; }
		[DataMember] public string        Desc { get; set; }
		[DataMember] public WebPointType? Type { get; set; }
		[DataMember] public double        X    { get; set; }
		[DataMember] public double        Y    { get; set; }
	}
}