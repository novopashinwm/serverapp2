﻿using System;
using System.Xml;

namespace FORIS.TSS.BusinessLogic.DTO
{
    [Serializable]
    public class Driver
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SecondName { get; set; }
        public string[] Phones { get; set; }
        public bool Editable { get; set; }

        public void WriteXml(XmlTextWriter writer)
        {
            writer.WriteStartElement("Driver");
            writer.WriteAttributeString("Editable", XmlConvert.ToString(Editable));
            writer.WriteAttributeString("FirstName", FirstName);
            writer.WriteAttributeString("LastName", LastName);
            writer.WriteAttributeString("SecondName", SecondName);
            writer.WriteAttributeString("Id", Id.ToString());
            writer.WriteStartElement("Phones");
            if (Phones != null)
            {
                foreach (var phone in Phones)
                {
                    writer.WriteStartElement("Phone");
                    writer.WriteAttributeString("value", phone);
                    writer.WriteEndElement();
                }
            }
            writer.WriteEndElement();
            writer.WriteEndElement();

        }
    }

    
}
