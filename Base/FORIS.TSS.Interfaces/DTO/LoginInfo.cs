﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class LoginInfo
	{
		public string Login;
		public string Password;
	}
}