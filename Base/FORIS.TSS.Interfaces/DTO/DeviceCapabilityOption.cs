﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
	/// <summary> Описание, есть ли доступ к той или иной способности устройства и информация почему она недоступна </summary>
	[Serializable]
	public class DeviceCapabilityOption
	{
		/// <summary> Возможно или нет использовать данную способность устройства </summary>
		public bool                         Allowed;
		/// <summary> Причина запрета </summary>
		public CapabilityDisallowingReason? DisallowingReason;
		/// <summary> Дата, когда данная способность станет доступной </summary>
		public DateTime?                    WaitTill;
		/// <summary> User-friendly текст, описывающий причину, по которой данная способность недоступна </summary>
		public string                       DisallowingReasonText;
	}
}