﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
	/// <summary> Услуги в FORIS </summary>
	[Serializable]
	public class BillingService
	{
		/// <summary> Категория услуги </summary>
		public ServiceTypeCategory Type;
		/// <summary > Идентификатор экземпляра услуги </summary>
		public int?                Id;
		/// <summary> Входит ли SMS в состав услуги </summary>
		public bool?               Sms;
		/// <summary> Входит ли LBS в состав услуги </summary>
		public bool?               Lbs;
		/// <summary> Статус оказания услуги </summary>
		public Dictionary<RenderedServiceType, BillingServiceStatus> Status;
		[Obsolete("Оставлено для совместимости со старыми версиями iPhone")]
		public Dictionary<RenderedServiceType, int> QuantityLeft
		{
			get
			{
				return Status
					?.Where(kv => kv.Value.QuantityLeft.HasValue)
					?.ToDictionary(
						k => k.Key,
						v => v.Value.QuantityLeft.Value);
			}
		}
		/// <summary> Пользовательское название услуги </summary>
		public string    Name;
		/// <summary> Дата начала действия услуги </summary>
		public DateTime? StartDate;
		/// <summary> Дата окончания действия услуги </summary>
		public DateTime? EndDate;
		/// <summary> Является ли услуга пробной </summary>
		public bool?     IsTrial;
		/// <summary> Активирована ли данная услуга </summary>
		public bool?     IsActivated;
		/// <summary> Можно ли использовать эту услугу на других объектах наблюдения </summary>
		public bool?     IsShared;
		/// <summary> Объём пакета - сколько единиц услуг включено в объём данной периодической услуги </summary>
		public int?      MaxQuantity;
		/// <summary> Минимальный интервал между двумя последовательными оказанием услуги - для безлимитных услуг </summary>
		public int?      MinIntervalSeconds;
		/// <summary> Идентификатор объекта наблюдения, к которому подключена услуга </summary>
		public int?      VehicleId;
		/// <summary> Заблокирована ли данная услуга </summary>
		public bool?     IsBlocked;
		/// <summary> Глубина просмотра истории объектов в часах </summary>
		public int?      MaxLogDepthHours;
		/// <summary> Возможно ли управление интервалами слежения </summary>
		public bool?     TrackingSchedule;
	}
}