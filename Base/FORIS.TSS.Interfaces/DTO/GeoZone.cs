﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class GeoZone
	{
		/// <summary> Идентификатор записи </summary>
		public int            Id;
		/// <summary> Название геозоны </summary>
		public string         Name;
		/// <summary> Описание геозоны </summary>
		public string         Description;
		/// <summary> Цвет геозоны. (RGB) </summary>
		public string         Color;
		/// <summary> Тип геозоны </summary>
		public ZoneTypes      Type;
		/// <summary> Точки геозоны </summary>
		public GeoZonePoint[] Points;
		/// <summary> Редактируемая </summary>
		public bool           Editable;
	}
}