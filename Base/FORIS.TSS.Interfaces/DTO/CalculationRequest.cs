﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class CalculationRequest
	{
		public string     Vehicles;
		public bool       FuelLevelSensor;
		public bool       IsAlreadyMTSCustomer;
		public string     City;
		public string     CompanyName;
		public string     UserName;
		public string     Email;
		public string     Phone;
		public string     AdditionalInformation;
		public int        VehicleQuantity;
		public int        EmployeeQuantity;
		public bool       InstallByMTSPartner;
		public bool       Glonass;
		public Attachment Attachment;
	}
}