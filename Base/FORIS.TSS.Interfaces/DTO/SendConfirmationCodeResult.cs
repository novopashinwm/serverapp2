﻿using System;
using FORIS.TSS.BusinessLogic.ResultCodes;

namespace FORIS.TSS.BusinessLogic.DTO
{
    [Serializable]
    public class SendConfirmationCodeResult
    {
        /// <summary>Код результата</summary>
        public SetNewContactResult Code;
        /// <summary>Дата окончания действия результата</summary>
        public DateTime? EndDate;

        /// <summary>Время в секундах до окончания срока действия результата или ноль, если срок вышел.</summary>
        public int? RemainingSeconds
        {
            get
            {
                if (EndDate == null)
                    return null;
                var seconds = (EndDate.Value - DateTime.UtcNow).TotalSeconds;
                return 0 < seconds ? (int) seconds : 0;
            }
        }

        public static implicit operator SendConfirmationCodeResult(SetNewContactResult code)
        {
            return new SendConfirmationCodeResult {Code = code};
        }
        
    }
}
