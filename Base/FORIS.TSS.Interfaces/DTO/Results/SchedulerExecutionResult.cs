﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO.Results
{
    public class SchedulerExecutionResult
    {
        public readonly ResultCodes.SchedulerExecutionResult Code;
        public readonly DateTime? NextTime;

        public SchedulerExecutionResult(ResultCodes.SchedulerExecutionResult code)
        {
            Code = code;
        }

        public SchedulerExecutionResult(DateTime nextTime)
        {
            Code = ResultCodes.SchedulerExecutionResult.Success;
            NextTime = nextTime;
        }

        public static implicit operator SchedulerExecutionResult(ResultCodes.SchedulerExecutionResult code)
        {
            return new SchedulerExecutionResult(code);
        }

        public static implicit operator SchedulerExecutionResult(DateTime nextTime)
        {
            return new SchedulerExecutionResult(nextTime);
        }
    }
}
