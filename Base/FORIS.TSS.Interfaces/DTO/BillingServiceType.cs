﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
	/// <summary> Описание типа услуги </summary>
	[Serializable]
	public class BillingServiceType
	{
		/// <summary> Идентификатор типа услуги </summary>
		public int                 Id;
		/// <summary> Пользовательское название услуги </summary>
		public string              Name;
		/// <summary> Входит ли SMS в состав услуги </summary>
		public bool                Sms;
		/// <summary> Входит ли LBS в состав услуги </summary>
		public bool                Lbs;
		/// <summary> Тип услуги </summary>
		public ServiceTypeCategory Category;
	}
}