﻿using System;
using System.Runtime.Serialization;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	[DataContract]
	public class ShareLink
	{
		public const           int    EmptyId   = -1;
		public static readonly Guid   EmptyGuid = Guid.Empty;
		[DataMember] public    Guid   LinkGuid    { get; set; } = EmptyGuid;
		[DataMember] public    int    LogTimeBeg  { get; set; }
		[DataMember] public    int    LogTimeEnd  { get; set; }
		[DataMember] public    int    VehicleId   { get; set; } = EmptyId;
		[DataMember] public    string VehicleName { get; set; }
		[DataMember] public    string VehicleDesc { get; set; }
		[DataMember] public    int    CreatorId   { get; set; } = EmptyId;
	}
}