﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	/// <summary> Результат обработки вызова общего вида </summary>
	/// <typeparam name="TCodeEnum"> Тип для кода результата </typeparam>
	[Serializable]
	public class CallResult<TCodeEnum>
	{
		public TCodeEnum Result;
		public string    ResultText;
	}
}