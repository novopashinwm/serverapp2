﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	[Obsolete]
	//TODO: Удалить этот класс, вместо него использовать SensorLegendMapping
	public class SensorMap : SensorMappingValue
	{
		public SensorLegend SensorLegend;
	}
}