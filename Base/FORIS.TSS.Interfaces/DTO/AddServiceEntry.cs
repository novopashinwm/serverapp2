﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
    /// <summary> Приложение обслуживания </summary>
    /// <remarks> Информация о приложении обслуживания, добавляемом в услугу "Мобильные сотрудники"</remarks>
    [Serializable]
    public class AddServiceEntry
    {
        public string Alias;
        public string Asid;
    }
}
