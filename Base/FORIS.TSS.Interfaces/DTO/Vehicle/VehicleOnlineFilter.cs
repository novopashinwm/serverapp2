﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
    /// <summary>
    /// Какие объекты включать в результирующий список.
    /// </summary>
    [Flags]
    public enum VehicleOnlineFilter
    {
        /// <summary>
        /// Ничего не выбрано
        /// </summary>
        None = 0,
        /// <summary> Объекты онлайн. </summary>
        Online = 1,

        /// <summary> Объекты оффлайн </summary>
        Offline = 2,
    }
}
