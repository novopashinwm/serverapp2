﻿namespace FORIS.TSS.BusinessLogic.DTO
{
	/// <summary> Значение атрибута </summary>
	public class VehicleAttributeValue
	{
		/// <summary> Идентификатор значения </summary>
		public string Id;
		/// <summary> Текст значения </summary>
		public string Name;
		/// <summary> Путь к иконке </summary>
		public string Icon;
		/// <summary> Иконка в формате base64 </summary>
		public string IconBase64;
	}
}