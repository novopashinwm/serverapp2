﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public struct VehicleLogTimeParam
	{
		public readonly int VehicleId;
		public readonly int LogTime;

		public VehicleLogTimeParam(int vehicleId, int logTime)
		{
			VehicleId = vehicleId;
			LogTime   = logTime;
		}
		public override bool Equals(object obj)
		{
			if (obj == null)
				return false;
			if (obj.GetType() != typeof(VehicleLogTimeParam))
				return false;
			var other = (VehicleLogTimeParam)obj;
			return VehicleId == other.VehicleId && LogTime == other.LogTime;
		}
		public override int GetHashCode()
		{
			return VehicleId.GetHashCode() ^ LogTime.GetHashCode();
		}
	}
}