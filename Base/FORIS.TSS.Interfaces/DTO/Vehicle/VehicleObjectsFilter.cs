﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Flags]
	public enum VehicleObjectsFilter
	{
		/// <summary> Не выбрана ни одна опция </summary>
		None = 0,
		/// <summary> Игнорируемые объекты </summary>
		Ignored = 1,
		/// <summary> Активные объекты </summary>
		Active = 2
	}
}