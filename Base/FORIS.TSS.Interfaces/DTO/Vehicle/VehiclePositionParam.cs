﻿namespace FORIS.TSS.BusinessLogic.DTO
{
	public struct VehiclePositionParam
	{
		public readonly VehicleLogTimeParam Id;
		public readonly decimal             Lat;
		public readonly decimal             Lng;

		public int VehicleId => Id.VehicleId;
		public int LogTime   => Id.LogTime;
		public VehiclePositionParam(VehicleLogTimeParam id, decimal lat, decimal lng)
		{
			Id  = id;
			Lat = lat;
			Lng = lng;
		}
		public VehiclePositionParam(int vehicleId, int logTime, decimal lat, decimal lng)
		{
			Id  = new VehicleLogTimeParam(vehicleId, logTime);
			Lat = lat;
			Lng = lng;
		}
		public VehiclePositionParam(int vehicleId, int logTime, double lat, double lng)
			: this(vehicleId, logTime, (decimal)lat, (decimal)lng)
		{
		}
	}
}