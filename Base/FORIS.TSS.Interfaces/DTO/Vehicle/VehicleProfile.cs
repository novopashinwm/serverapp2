﻿namespace FORIS.TSS.BusinessLogic.DTO
{
	public enum VehicleProfile
	{
		/// <summary> Изображение на карте, URL </summary>
		Icon,
		/// <summary> Особые отметки, описание </summary>
		Notes,
		/// <summary> OnlineGeocoding, свойство объекта, которое разрешает поиск адреса при любом изменении координат в WebPush </summary>
		/// <remarks> Разрешается видеть всем, а изменять только суперадмину, если выставлен параметр конфигурации "OnlineGeocoding4All" со значением true, то атрибут становится неизменяемым, а значение true для всех </remarks>
		OnlineGeocoding,
	}
}