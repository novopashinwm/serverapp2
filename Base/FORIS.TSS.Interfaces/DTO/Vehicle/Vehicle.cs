﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.DTO.OperatorRights;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class Vehicle
	{
		public int    id;
		public int?   asidId;
		public int?   departmentId;
		public string departmentName;

		public bool LastDataIncluded;
		public bool AttributesIncluded;

		public bool? KnownMaskedMsisdn;
		public bool? allowMlpRequest;

		public double? lat;
		public double? lng;
		public double? latCP;
		public double? lngCP;
		public bool? IsLBSPosition { get; set; }

		/// <summary>Владелец данного объекта наблюдения</summary>
		/// <seealso cref="IsOwnVehicle"/>
		public Operator Owner;
		/// <summary>Данный объект наблюдения представляет владельца <see cref="Owner"/></summary>
		public bool? IsOwnVehicle;

		[Obsolete("Будет удалено в последующих итерациях; следует использовать Owner и IsOwnVehicle")]
		public bool? IsCurrentUser;
		public bool? IsOnlineGeocoding;

		public PositionType? PositionType;

		public string Name;
		public string Notes;

		public string garageNum;
		public string regNum;
		public string brand;
		public int?   speed;
		public bool?  ignition;
		public Dictionary<VehicleStateType, VehicleState> State;

		/// <summary>Курс движения (азимут) по данным устройства</summary>
		public int? gpscourse;
		/// <summary>Курс движения (азимут) относительно предыдущей позиции</summary>
		public int? MotionCourse;
		/// <summary>Обобщённый курс для использования в приложениях</summary>
		public int? course;
		
		public int?      statusLogTime;
		public Status?   status;

		public int?      posLogTime;
		public DateTime? posDate;

		public string    iconUrl;
		public string    address;
		public string    Thumbnail;
		/// <summary> Требует ли объект наблюдения настройки </summary>
		public bool? RequiresSetup
		{
			get
			{
				if (controllerType == null)
					return null;
				if (controllerType.Name == ControllerType.Names.SoftTracker)
					return null;
				return logTime == null && rights != null && rights.Contains(SystemRight.SecurityAdministration);
			}
		}
		/// <summary>IMEI или номер блока</summary>
		public string ctrlNum;
		/// <summary>Телефон сим-карты, установленной в блок. Требуется право <see cref="SystemRight.ViewControllerPhone"/></summary>
		public string phone;
		/// <summary>Можно ли редактировать номер телефона SIM-карты - редактировать нельзя для номеров телефонов, полученных из FORIS</summary>
		public bool? PhoneIsEditable;

		public decimal? fuelTankVolume;
		public decimal? fuel;

		public double? radius;
		public Command[] commands;
		public Dictionary<DeviceCapability, DeviceCapabilityOption> capabilities;
		public VehicleKind      vehicleKind;
		public VehicleCategory? category;
		public bool?            canBeDisactivated;

		public int? logTime;

		/// <summary>Задержка в миллисекундах с момента, 
		/// когда данные по устройству были последний раз записаны в БД, 
		/// до момента извлечения этих данных из БД</summary>
		public int? InsertDelayS;

		/// <summary> Дата фиксации последних данных от устройства </summary>
		public DateTime? Date;

		/// <summary>true, если координата актуальна и false в противном случае</summary>
		public bool? isValid;
		/// <summary>true, если данные объекта устарели и false, если последние данные от объекта актуальны на текущий момент</summary>
		public bool? isOld;

		/// <summary>  Доступ к ТС был предоставлен партнером текущего пользователя. </summary>
		public bool isFriendVehicle;

		/// <summary> Разрешено ли удалять данный объект </summary>
		public bool? Removable
		{
			get
			{
				if (rights == null)
					return null;
				if (isFriendVehicle)
					return true;
				if (IsOwnVehicle ?? false)
					return false;
				return rights.Contains(SystemRight.SecurityAdministration);
			}
		}
		/// <summary>true, если трекер объекта наблюдения в настоящий момент сливает данные из "чёрного ящика" и false</summary>
		public bool? Dumping;
		/// <summary> Порог, после превышения которого считается, что устройство Offline </summary>
		private static readonly TimeSpan OfflineThreshold = TimeSpan.FromHours(3);
		/// <summary> Порог, после превышения которого считается, что позиция устройства устарела </summary>
		private static readonly TimeSpan PositionIsOutOfDateThreshold = TimeSpan.FromMinutes(30);
		/// <summary>Актуальность данных от устройства</summary>
		public OnlineStatus? OnlineStatus
		{
			get
			{
				if (!LastDataIncluded)
					return null;
				if (BillingBlocking != null)
					return Enums.OnlineStatus.ServiceIsBlocked;
				if (Dumping == null)
					return Enums.OnlineStatus.Never;
				if (Dumping.Value)
					return Enums.OnlineStatus.Dumping;
				if (OfflineThreshold.TotalSeconds < InsertDelayS)
					return Enums.OnlineStatus.Offline;
				if (posDate == null || PositionIsOutOfDateThreshold < DateTime.UtcNow - posDate.Value)
					return Enums.OnlineStatus.PositionIsOutOfDate;
				return Enums.OnlineStatus.Online;
			}
		}
		/// <summary> Для фильтрации Онлайн|Все|Офлайн </summary>
		public bool? Online
		{
			get
			{
				var onlineStatus = OnlineStatus;
				if (onlineStatus == null)
					return null;
				return
					onlineStatus.Value == Enums.OnlineStatus.Online ||
					onlineStatus.Value == Enums.OnlineStatus.PositionIsOutOfDate ||
					onlineStatus.Value == Enums.OnlineStatus.Dumping;
			}
		}
		public List<BillingService>  billingServices;
		public BillingBlocking       BillingBlocking;
		public List<RenderedService> RenderedServices;

		// TODO: Пока не получается разместить тут конкретный тип из-за неправильного расположения классов
		/// <summary> Текущий тариф </summary>
		[DataMember] public object CurrentTariff { get; set; } = null;

		public List<int> ZoneIds = new List<int>();

		public ControllerType controllerType;

		/// <summary> Указан ли на устройстве IP-адрес </summary>
		public bool? HasIP;

		/// <summary>Время последнего захвата изображения с камеры на объекте</summary>
		public List<Picture> Pictures;

		/// <summary> Дополнительные  права на объект наблюдения, т.е. все кроме <see cref="SystemRight.VehicleAccess"/> </summary>
		public SystemRight[] rights;
		public List<Sensor> Sensors;

		/// <summary> Максимальная допустимая скорость, км/ч </summary>
		public int? MaxAllowedSpeed;

		/// <summary> Возвращает true, если объект поддерживает указанную возможность и false в противном случае </summary>
		public bool Supports(DeviceCapability capability)
		{
			DeviceCapabilityOption option;
			return capabilities != null && capabilities.TryGetValue(capability, out option) && option.Allowed;
		}
		public DeviceCapabilityOption CreateCapability(DeviceCapability capability)
		{
			//TODO: использовать метод GetRequiredRightForCommandType, чтобы определять, какое право нужно для данной возможности
			var result = new DeviceCapabilityOption { Allowed = true };
			if (controllerType.Name == ControllerType.Names.Sonim && capability == DeviceCapability.ChangeDeviceSettings)
			{
				result.Allowed = rights.Any(r => r == SystemRight.ChangeDeviceConfigBySMS);
				if (!result.Allowed)
					result.DisallowingReason = CapabilityDisallowingReason.Forbidden;
			}
			else
			{
				return null;
			}
			return result;
		}
		private static readonly Dictionary<VehicleKind, string> VehicleDefaultIconUrl =
			new Dictionary<VehicleKind, string>
			{
				{ VehicleKind.Tracker,     "defaults/default-tracker.png"    },
				{ VehicleKind.MobilePhone, "defaults/default-cellphone.png"  },
				{ VehicleKind.WebCamera,   "defaults/default-web-camera.png" },
			};
		public string ipAddress;
		public string DefaultIconUrl { get { return VehicleDefaultIconUrl.ContainsKey(vehicleKind) ? VehicleDefaultIconUrl[vehicleKind] : ""; } }
		public Sensor GetSensor(SensorLegend sensorLegend)
		{
			if (Sensors == null)
				return null;
			return Sensors.FirstOrDefault(s => s.Number == sensorLegend);
		}
		public SensorLogRecord? GetSensorLogRecord(SensorLegend sensorLegend)
		{
			var sensor = GetSensor(sensorLegend);

			if (sensor == null || sensor.LogRecords == null || sensor.LogRecords.Count == 0)
				return null;

			return sensor
				.LogRecords
				.OrderByDescending(r => r.LogTime)
				.FirstOrDefault();
		}
		public decimal? GetSensorValue(SensorLegend sensorLegend)
		{
			var logRecord = GetSensorLogRecord(sensorLegend);
			if (logRecord == null)
				return null;
			return logRecord.Value.Value;
		}
		public bool IsVehicle
		{
			get
			{
				switch (vehicleKind)
				{
					case VehicleKind.Bus:
					case VehicleKind.Truck:
					case VehicleKind.Trailer:
					case VehicleKind.Vehicle:
						return true;
					default:
						return false;
				}
			}
		}
		public bool IsMobile
		{
			get
			{
				switch (vehicleKind)
				{
					case VehicleKind.MobilePhone:
					case VehicleKind.Smartphone:
						return true;
					default:
						return false;
				}
			}
		}
		/// <summary> Статус изображения устройства </summary>
		public VehicleStatusIcon StatusIcon
		{
			get
			{
				var alarm = GetSensorValue(SensorLegend.Alarm);
				if (alarm != null && alarm.Value != 0)
					return VehicleStatusIcon.Alarm;

				var immobilized = GetSensorValue(SensorLegend.Immobilized);
				if (immobilized != null && immobilized.Value != 0)
					return VehicleStatusIcon.Immobilized;

				if (PositionType != null)
				{
					switch (PositionType.Value)
					{
						case Enums.PositionType.LBS: 
							return VehicleStatusIcon.MLP;
						case Enums.PositionType.YandexGsm:
						case Enums.PositionType.YandexWifi:
						case Enums.PositionType.Yandex: 
							return VehicleStatusIcon.Yandex;
					}
				}

				if (!(isValid ?? false)) 
					return VehicleStatusIcon.Off;
				if (status.HasValue && status == Status.Alarm) return VehicleStatusIcon.RestAngle;
				if (status.HasValue && status == Status.ParcingMoving) return VehicleStatusIcon.ParcingMoving;
				if (status.HasValue && status == (Status.Alarm | Status.ParcingMoving)) return VehicleStatusIcon.Park;
				if (speed == 0)
				{
					if (IsVehicle && ignition != null && !ignition.Value)
						return VehicleStatusIcon.Parking;
					
					return VehicleStatusIcon.Stop;
				}
				if (speed == null || 0 < speed && speed < 5) 
					return VehicleStatusIcon.Rest;

				return VehicleStatusIcon.RestAngle;

			}
		}
		public override string ToString()
		{
			return "[" + id + "]" + Name;
		}
		public void DetectCategory(Department department)
		{
			if (IsOwnVehicle ?? false)
			{
				category = VehicleCategory.Own;
				return;
			}

			var choosenDepartmentType = department != null ? department.Type : (DepartmentType?)null;
			var choosenDepartmentId   = department != null ? department.id : (int?)null;
			var hasAdminRights        = rights.Contains(SystemRight.SecurityAdministration);
			var sameCorporativeClient = choosenDepartmentType == DepartmentType.Corporate &&
										choosenDepartmentId   == departmentId;
			if (sameCorporativeClient)
			{
				if (IsMobile)
					category = VehicleCategory.Partner;
				else if (IsVehicle)
					category = VehicleCategory.Autopark;
				else
					category = VehicleCategory.CorporativeTracker;
			}
			else
			{
				if (hasAdminRights)
				{
					if (IsMobile)
						category = VehicleCategory.Child;
					else if (IsVehicle)
						category = VehicleCategory.Car;
					else if (vehicleKind == VehicleKind.Tracker)
						category = VehicleCategory.Tracker;
					else
						category = VehicleCategory.Other;
				}
				else
					category = VehicleCategory.Friend;
			}
		}
	}
}