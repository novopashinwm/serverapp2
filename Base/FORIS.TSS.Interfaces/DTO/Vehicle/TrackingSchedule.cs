﻿using System;
using FORIS.TSS.BusinessLogic.DTO.OperatorRights;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class TrackingSchedule
	{
		[NonSerialized]
		public int Id;
		[NonSerialized]
		public bool Enabled;

		public DayOfWeek[] Days;
		public TimeOfDay From;
		public TimeOfDay To;

		[NonSerialized]
		public Owner Owner;

		[NonSerialized]
		public bool Editable;
	}
}