﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class VehicleAttribute
	{
		public VehicleAttributeName        Name;
		public string                      UserFriendlyName;
		public string                      Value;
		public string                      UserFriendlyValue;
		public AttributeType               Type;
		public bool                        Editable;
		public string                      Unit;
		public List<VehicleAttributeValue> Values;
	}
}