﻿using System;
using System.Xml;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class VehicleControlDate
	{
		public VehicleControlDateTypes TypeId { get; set; }
		public DateTime Value { get; set; }

		public void WriteXml(XmlTextWriter writer)
		{
			writer.WriteStartElement("VehicleControlDate");
			writer.WriteAttributeString("Type", TypeId.ToString());
			writer.WriteAttributeString("Value", Value.ToString("dd.MM.yyyy"));
			writer.WriteEndElement();
		}
	}
}