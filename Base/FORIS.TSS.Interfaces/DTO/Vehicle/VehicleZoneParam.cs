﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public struct VehicleZoneParam
	{
		public readonly int VehicleId;
		public readonly int ZoneId;

		public VehicleZoneParam(int vehicleId, int zoneId)
		{
			VehicleId = vehicleId;
			ZoneId    = zoneId;
		}
	}
}