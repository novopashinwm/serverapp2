﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
    [Serializable]
    public class Attachment
    {
        public string Name;
        public byte[] Bytes;
    }
}
