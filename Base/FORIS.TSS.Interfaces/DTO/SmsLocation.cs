﻿using System.Globalization;

namespace FORIS.TSS.BusinessLogic.DTO
{
	public class SmsLocation
	{
		public double Latitude  { get; set; }
		public double Longitude { get; set; }
		public double Radius    { get; set; }
		public int    Timestamp { get; set; }
		public int    Status    { get; set; }

		public static SmsLocation Parse(string xLocation)
		{
			int? status = null;
			double? latitude = null;
			double? longitude = null;
			double? radius = null;
			long? timestamp = null;

			var pairs = xLocation.Split(',', '=');
			for (var i = 0; i < pairs.Length; i += 2)
			{
				var key = pairs[i];
				var value = pairs[i + 1];
				switch (key)
				{
					case "status":
						int statusValue;
						if (int.TryParse(value, out statusValue))
							status = statusValue;
						break;
					case "lat":
						double latitudeValue;
						if (double.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out latitudeValue))
							latitude = latitudeValue;
						break;
					case "lon":
						double longitudeValue;
						if (double.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out longitudeValue))
							longitude = longitudeValue;
						break;
					case "radius":
						double radiusValue;
						if (double.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out radiusValue))
							radius = radiusValue;
						break;
					case "timestamp":
						long timestampValue;
						if (long.TryParse(value, out timestampValue))
							timestamp = timestampValue;
						break;
				}
			}

			if (!status.HasValue ||
				!latitude.HasValue ||
				!longitude.HasValue ||
				!radius.HasValue ||
				!timestamp.HasValue)
			{
				return null;
			}

			return new SmsLocation
			{
				Status = status.Value,
				Latitude = latitude.Value,
				Longitude = longitude.Value,
				Radius = (int)radius.Value,
				Timestamp = (int)(timestamp.Value / 1000)
			};
		}
	}
}