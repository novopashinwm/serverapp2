using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
    public class LogSessionResult
    {
        public LogSessionResultType Result;

        public int? DepartmentId;
    }
}