﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class Contact
	{
		public int?         Id                     { get; set; }
		public ContactType? Type                   { get; set; }
		public string       Value                  { get; set; }
		public string       Name                   { get; set; }
		public string       Comment                { get; set; }
		public bool?        IsConfirmed            { get; set; }
		public bool?        IsConfirmationCodeSent { get; set; }
		public bool?        IsRemovable            { get; set; }
		public bool?        IsConfirmable          { get; set; }

		public Contact()
		{
		}

		public Contact(ContactType type, string value)
		{
			Type  = type;
			Value = value;
		}

		public override string ToString()
		{
			return $"{Type} {Value}";
		}
	}
}