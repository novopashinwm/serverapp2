﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class SensorLegendMapping
	{
		public SensorLegend         SensorLegend;
		public SensorMappingValue[] Mappings;
	}
}