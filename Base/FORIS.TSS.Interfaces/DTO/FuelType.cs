﻿using System;
using System.Xml;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
	/// <summary> Запись из справочника "Тип топлива" </summary>
	[Serializable]
	public class FuelType
	{
		public int           ID;
		public string        Name;
		public UnitOfMeasure Unit;
		public UnitOfMeasure TankUnit;

		public void WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("FuelType");
			writer.WriteAttributeString("ID", XmlConvert.ToString(ID));
			writer.WriteAttributeString("Name", Name);
			writer.WriteAttributeString("Unit", Unit.ToString());
			writer.WriteAttributeString("TankUnit", TankUnit.ToString());
			writer.WriteEndElement();
		}
	}
}