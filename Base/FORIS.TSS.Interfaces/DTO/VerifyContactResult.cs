﻿using System;
using FORIS.TSS.BusinessLogic.ResultCodes;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class VerifyContactResult
	{
		public SetNewContactResult Code;
		public string              SimId;
		public int?                RemainingAttemptCount;

		public static implicit operator VerifyContactResult(SetNewContactResult code)
		{
			return new VerifyContactResult { Code = code };
		}
	}
}