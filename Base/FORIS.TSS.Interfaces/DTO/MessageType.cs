﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public enum MessageType
	{
		Incoming,
		Outgoing
	}
}