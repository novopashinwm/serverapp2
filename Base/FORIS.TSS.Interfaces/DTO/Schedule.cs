﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
    [Serializable]
    public class Schedule
    {
        public string RepetitionXml;
        public int? VehicleID;
        public int? VehicleGroupID;
    }
}
