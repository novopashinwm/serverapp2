﻿// ReSharper disable InconsistentNaming
namespace FORIS.TSS.BusinessLogic.DTO.PlatformSpecific.Android
{
    public class Purchase
    {
        public bool autoRenewing;
        public string orderId;
        public string packageName;
        public string productId;
        public long purchaseTime;
        public int purchaseState;
        public string developerPayload;
        public string purchaseToken;
    }
}
