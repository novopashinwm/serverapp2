﻿using System;
using System.Globalization;
using System.Xml;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class Department
	{
		public int            id;
		public string         name;
		public string         extID;
		public string         description;
		public string         countryName;
		public DepartmentType Type;
		public bool?          TypeIsChangeable;
		public SystemRight[]  Rights;
		public bool?          ViugaUserExists;
		public DateTime?      LockDate;
		public bool Locked
		{
			get { return LockDate.HasValue; }
		}
		public void WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("Department");

			writer.WriteElementString("ID", id.ToString(CultureInfo.InvariantCulture));
			writer.WriteElementString("Name", name);
			writer.WriteElementString("Country", countryName);
			writer.WriteElementString("Type", Type.ToString());
			if (TypeIsChangeable != null)
				writer.WriteElementString("TypeIsChangeable", XmlConvert.ToString(TypeIsChangeable.Value));

			writer.WriteEndElement();
		}
	}
}