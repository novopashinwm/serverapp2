﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
	/// <summary> Описание типа команды </summary>
	[Serializable]
	public class CommandType
	{
		/// <summary> Идентификатор типа команды </summary>
		public CmdType Id;
		/// <summary> Локализованное название команды </summary>
		public string Name;
		/// <summary> Описание команды </summary>
		public string Description;
		/// <summary> Предупреждение перед запуском команды </summary>
		public string Warning;
		/// <summary> Информация о том, можно ли запускать эту команду</summary>
		/// <remarks>
		/// Опционально, подгружается только если в запросе списка команд явно указано,
		/// что сценарий требуется для загрузки
		/// </remarks>
		public CommandScenario Scenario;

#if false
		/// <summary> Список параметров, необходимых для запуска команды</summary>
		public List<Parameter> Parameters = new List<Parameter>();
		/// <summary>Параметр команды</summary>
		public class Parameter
		{
			/// <summary>Идентификатор параметра команды</summary>
			public string Id;
			/// <summary>Локализованное название параметра команды</summary>
			public string Name;
			/// <summary>Описание команды</summary>
			public string Description;
			/// <summary>Тип команды</summary>
			public ParameterType Type;
		}

		/// <summary>Тип параметра команды</summary>
		public enum ParameterType
		{
			/// <summary>Номер телефона</summary>
			Msisdn = 1
		}
#endif
		/// <summary> Регистронезависимое сравнение двух экземпляров по имени </summary>
		public static int CompareByName(CommandType x, CommandType y)
		{
			return StringComparer.OrdinalIgnoreCase.Compare(x != null ? x.Name : null, y != null ? y.Name : null);
		}
	}
}