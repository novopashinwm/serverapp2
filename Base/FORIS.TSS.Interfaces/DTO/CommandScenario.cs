﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class CommandScenario
	{
		[Serializable]
		public class CommandSms
		{
			public string Text;
			public bool   WaitAnswer = true;
		}

		public CmdResult    Result;
		public string       ResultText;
		public CommandSms[] Sms;
	}
}