﻿using System.Xml.Serialization;

namespace FORIS.TSS.BusinessLogic.DTO.Contacts
{
	[XmlRoot("contacts")]
	public class PhoneBook
	{
		[XmlElement("contact")]
		public PhoneBookContact[] Contacts;
	}
}