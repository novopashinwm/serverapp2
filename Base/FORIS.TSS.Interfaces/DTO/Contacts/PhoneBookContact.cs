﻿using System;
using System.Xml.Serialization;
using FORIS.TSS.BusinessLogic.Enums;
using Jayrock.Json.Conversion;

namespace FORIS.TSS.BusinessLogic.DTO.Contacts
{
	public class PhoneBookContact : IEquatable<PhoneBookContact>
	{
		[XmlIgnore]
		public ContactType Type = ContactType.Phone;

		public string Value;

		public string Name;

		/// <summary> XmlSerializer не умеет сериализовать nullable атрибуты, а private поля он не сериализует </summary>
		private bool? _updated;

		[JsonIgnore]
		[XmlAttribute]
		public bool Updated
		{
			get { return _updated ?? false; }
			set { _updated = value; }
		}

		/// <summary> Метод определяет, будет ли добавляться атрибут Updated при сериализации </summary>
		/// <returns></returns>
		public bool ShouldSerializeUpdated()
		{
			return _updated.HasValue;
		}

		public bool Equals(PhoneBookContact other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return string.Equals(Value, other.Value);
		}

		public override int GetHashCode()
		{
			return (Value != null ? Value.GetHashCode() : 0);
		}

		public static bool operator ==(PhoneBookContact left, PhoneBookContact right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(PhoneBookContact left, PhoneBookContact right)
		{
			return !Equals(left, right);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((PhoneBookContact)obj);
		}
	}
}