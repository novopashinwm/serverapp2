﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO.OperatorRights
{
    /// <summary> Описывает пользователя-владельца какого-либо объекта </summary>
    [Serializable]
    public class Owner
    {
        public int OperatorId;
        public Contact Contact;
    }
}
