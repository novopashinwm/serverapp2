﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO.OperatorRights
{
	[Serializable]
	public class GeozoneGroup : RightDTO
	{
		public string    name;
		public string    description;
		public Geozone[] geozones;
		public int[]     geozoneIds;
	}
}