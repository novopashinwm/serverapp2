﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO.OperatorRights
{
	[Serializable]
	public class Geozone : RightDTO
	{
		public string name;
		public string description;
		public int[]  parents;
		public int?   color;
	}
}