﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO.OperatorRights
{
	[Serializable]
	public class Right
	{
		public bool?        allow;
		public int          right_id;
		//TODO: убрать Nullable после перехода на новый UI Администрирования
		public SystemRight? right;
	}
}