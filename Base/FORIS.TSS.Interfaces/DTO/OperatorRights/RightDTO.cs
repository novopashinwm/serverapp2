﻿using System.Linq;

namespace FORIS.TSS.BusinessLogic.DTO.OperatorRights
{
	public abstract class RightDTO
	{
		public int     id;
		public Right[] rights;
		public bool Allowed(SystemRight right)
		{
			return rights != null &&
				rights.Any(r => (r.allow ?? false) && r.right_id == (int)right);
		}
	}
}