﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO.OperatorRights
{
	[Serializable]
	public class Operator
	{
		public int            id;
		public string         name;
		public string         login;
		public string         pwd;
		public string         description;
		public Vehicle[]      vehicles;
		/// <summary> Список групп и прав на них для данного пользователя </summary>
		public VehicleGroup[] groups;
		public Geozone[]      geozones;
		public GeozoneGroup[] geozonegroups;
		public Contact[]      Emails;
		public Contact[]      Phones;
		public Right[]        rights;
		public bool?          mandatoryEmail;
		public bool?          mandatoryPhone;
		/// <summary>Есть ли у пользователя маскированный номер телефона</summary>
		public bool?          HasMaskedPhone;
		/// <summary> Можно ли изменять атрибуты данного пользователя </summary>
		public bool?          AllowChangeAttributes;
		/// <summary> Пользователь добавлен в список друзей </summary>
		public bool?          IsFriend;
		public void FormatContactInformation()
		{
			if (name == null)
				name = string.Empty;
			if (string.IsNullOrWhiteSpace(name) && Phones != null && Phones.Length != 0)
			{
				name = Phones[0].Value;
			}
			if (string.IsNullOrWhiteSpace(name) && Emails != null && Emails.Length != 0)
			{
				name = Emails[0].Value;
			}
		}
	}
}