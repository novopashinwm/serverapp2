﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO.OperatorRights
{
	[Serializable]
	public class VehicleGroup : RightDTO
	{
		public string    name;
		public string    description;
		public Vehicle[] vehicles;
		public int[]     vehicleIds;
	}
}