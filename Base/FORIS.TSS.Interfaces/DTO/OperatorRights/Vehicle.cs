﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO.OperatorRights
{
	[Serializable]
	public class Vehicle : RightDTO
	{
		public string gNum;
		public string iconUrl;
		public bool   isMLP;
	}
}