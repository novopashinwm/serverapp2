﻿using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO.Push
{
	public class PushMessage
	{
		public int                        Id;
		public MessageState               State;
		public string                     Subject;
		public string                     Body;
		public string                     Template;
		public Dictionary<string, string> Parameters = new Dictionary<string, string>();
	}
}