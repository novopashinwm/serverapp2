﻿using System.Text.RegularExpressions;
using Jayrock.Json;
using Jayrock.Json.Conversion;

namespace FORIS.TSS.BusinessLogic.DTO
{
    //TODO: реализовать сериализацию в JSON и десериализацию в формате HH:mm
    public struct TimeOfDay : IJsonExportable, IJsonImportable
    {
        public int Hours;
        public int Minutes;

        public TimeOfDay(int hours, int minutes)
        {
            Hours = hours;
            Minutes = minutes;
        }

        public override string ToString()
        {
            return (Hours < 10 ? ("0" + Hours) : Hours.ToString()) + ":" +
                   (Minutes < 10 ? ("0" + Minutes) : Minutes.ToString());
        }

        public void Export(ExportContext context, JsonWriter writer)
        {
            writer.WriteString(ToString());
        }

        private static readonly Regex Regex = new Regex(@"^(?<h>\d{1,2})\:(?<m>\d{1,2})$");

        public void Import(ImportContext context, JsonReader reader)
        {
            var s = reader.ReadString();

            var match = Regex.Match(s);
            var h = match.Groups["h"].Value;
            var m = match.Groups["m"].Value;

            Hours = int.Parse(h);
            Minutes = int.Parse(m);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (obj.GetType() != GetType())
                return false;
            var x = (TimeOfDay) obj;
            return x.Hours == Hours && x.Minutes == Minutes;
        }

        public override int GetHashCode()
        {
            // ReSharper disable NonReadonlyMemberInGetHashCode
            return Hours.GetHashCode() ^ Minutes.GetHashCode();
            // ReSharper restore NonReadonlyMemberInGetHashCode
        }
    }
}
