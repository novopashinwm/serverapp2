﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class TimeZoneFingerprint
	{
		[Serializable]
		public class DstChange
		{
			public DateTime? On;
			public DateTime? Off;
			public int       Delta;
		}

		public int         BaseOffset;
		public DstChange[] DstChanges;
	}
}