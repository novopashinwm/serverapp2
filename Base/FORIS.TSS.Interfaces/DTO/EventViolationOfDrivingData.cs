﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
	/// <summary> Данные события о нарушении вождения </summary>
	[Serializable]
	[DataContract]
	public class EventViolationOfDrivingData
	{
		/// <summary> Идентификатор объекта наблюдения (устройства/трекера) </summary>
		[DataMember][Required] public string                 DeviceId      { get; set; }
		/// <summary> Идентификатор водителя </summary>
		[DataMember][Required] public string                 DriverId      { get; set; }
		/// <summary> Наименование водителя/ФИО </summary>
		[DataMember][Required] public string                 DriverName    { get; set; }
		/// <summary> Дата и время события нарушения режима вождения UTC </summary>
		[DataMember][Required] public DateTime               EventTimeUtc  { get; set; }
		/// <summary> Тип нарушения режима вождения </summary>
		[DataMember][Required] public ViolationOfDrivingType EventType     { get; set; }
		/// <summary> Ссылка на видео нарушения </summary>
		[DataMember][Required] public string                 EventVideoUrl { get; set; }
	}
}