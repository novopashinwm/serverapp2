﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
    /// <summary>
    /// Информация об отсутствии данных по объекту наблюдения
    /// </summary>
    [Serializable]
    public class DataAbsenceRecord
    {
        public int? DepartmentID;
        public string DepartmentName;
        public string Emails;
        public string Phones;
        public int VehicleID;
        public string VehicleName;
        public string VehicleDeviceID;
        public DateTime? LastInsertTime;
        public DateTime? LastLogTime;
        public DateTime? LastGeoTime;
        public DateTime? LastCANTime;
        public DateTime? LastFuelTime;
    }
}
