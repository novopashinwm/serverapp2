﻿using System;
using FORIS.TSS.BusinessLogic.Helpers;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class ControllerType
	{
		public int Id;
		public string Name;

		/// <summary>
		/// Человеко-понятное название типа контроллера.
		/// </summary>
		public string UserFriendlyName;

		/// <summary>
		/// Есть ли у контроллера данного типа входы для приёма внешних (например, уровень батареи) или внутренних (например, зажигание) данных; null, если не заполнен.
		/// </summary>
		public bool? HasInputs;

		/// <summary> Поддерживает ли контроллер защиту паролем; null, если не заполнен. </summary>
		public bool? SupportsPassword;

		/// <summary> Требуется ли IMEI для конфигурации трекера </summary>
		public bool? DeviceIdIsRequiredForSetup;

		/// <summary> Идентификатором устройства является IMEI - 15-значный уникальный номер оборудования GPS: ru.wikipedia.org/wiki/IMEI</summary>
		public bool? DeviceIdIsImei;

		/// <summary> Ссылка на фотографию трекера </summary>
		public string ImagePath;

		/// <summary> Фотография трекера в виде массива байт base64 </summary>
		public string ImageBase64;

		/// <summary> Поддерживает ли устройство ввод идентификатора устройства (обычно IMEI) </summary>
		public bool? SupportsDeviceId
		{
			get { return ControllerTypeHelper.SupportsDeviceId(Name); }
		}

		public bool? SupportsPositionAsking;

		public int? SortOrder;

		public static class Names
		{
			public const string Unspecified    = "Unspecified";
			public const string SoftTracker    = "SoftTracker";
			public const string FtpWebCam      = "FTP Webcam";
			public const string UltracomCollar = "Ultracom Collar";
			public const string Sonim          = "Sonim";
			public const string AvtoGraf       = "AvtoGraf";
			public const string AvtoGrafCan    = "AvtoGrafCan";
			public const string TM4            = "TM4";
			public const string NavisetGT20    = "NavisetGT20";
			public const string FORT300        = "FORT300";
			public const string IntelliTrac    = "IntelliTrac";
			public const string DIMTS          = "DIMTS";
			public const string TL2000         = "Autocop TL-2000";
			public const string TR06A          = "TR06A";
			public const string WP2030C        = "WP2030C";
			public const string WP20DIMTS      = "WP20DIMTS";
			public const string Mercury        = "Mercury";
			public const string Ruptela        = "Ruptela";
		}
	}
}