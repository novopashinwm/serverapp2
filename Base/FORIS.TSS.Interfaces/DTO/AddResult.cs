﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	public class AddResult<T>
	{
		public T      Value;
		public Enum   Result;
		public string ResultText;
	}
}