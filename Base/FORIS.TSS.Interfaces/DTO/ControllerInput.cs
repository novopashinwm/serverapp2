﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class ControllerInput
	{
		public int           Number;
		public string        Name;
		public SensorLegend? DefaultLegend;
		public decimal?      DefaultMultiplier;
		public decimal?      DefaultConstant;
		public long?         Value;
	}
}