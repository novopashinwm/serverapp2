﻿using System;
using System.Globalization;
using System.Xml;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class Group
	{
		/// <summary> Идентификатор псевдогруппы "Не в группе" </summary>
		public const int NotInGroupId = -2;
		/// <summary> Идентификатор псевдогруппы "Все объекты" </summary>
		public const int AllId = -1;
		/// <summary> Идентификатор группы </summary>
		public int Id;
		/// <summary> Название группы </summary>
		public string Name;
		/// <summary> Описание группы </summary>
		public string Description;
		/// <summary> Список идентификаторов объектов, входящих в группу </summary>
		public int[] ObjectIds;
		/// <summary> Можно ли оператору редактировать содержимое группы </summary>
		public bool AllowEditContent;
		/// <summary> Можно ли оператору добавлять зоны в группы, но не удалять </summary>
		public bool AllowAddToGroup;
		/// <summary> Группа создана оператором </summary>
		public bool UserDefined;
		public void WriteXml(XmlTextWriter writer, string rootName = null)
		{
				writer.WriteStartElement(rootName ?? "Group");
				writer.WriteAttributeString("Id", Id.ToString(CultureInfo.InvariantCulture));
				writer.WriteAttributeString("Name", Name);
				writer.WriteAttributeString("Description", Description);
				writer.WriteEndElement();
		}
	}
}