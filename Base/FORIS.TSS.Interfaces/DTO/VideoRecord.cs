﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class VideoRecord
	{
		/// <summary>Сссылка, по которой можно просмотреть видео из архива</summary>
		public string   Uri;
		/// <summary>Дата начал записи архива</summary>
		public DateTime Start;
	}
}