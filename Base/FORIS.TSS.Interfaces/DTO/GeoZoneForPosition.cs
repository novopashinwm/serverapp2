﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public struct GeoZoneForPosition
	{
		public readonly int VehicleId;
		public readonly int LogTime;
		public readonly int ZoneId;
		public GeoZoneForPosition(int vehicleId, int logTime, int zoneId)
		{
			VehicleId = vehicleId;
			LogTime   = logTime;
			ZoneId    = zoneId;
		}
	}
}