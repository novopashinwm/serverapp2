﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class TrackerRegistrationTuple
	{
		/// <summary> Номер трекера </summary>
		public string TrackerNumber;
		/// <summary> Контрольный код </summary>
		public string ControlCode;
	}
}