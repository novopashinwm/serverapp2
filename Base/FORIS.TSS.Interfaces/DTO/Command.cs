﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class Command
	{
		/// <summary> Идентификатор команды </summary>
		public int Id;
		/// <summary> Тип команды </summary>
		public CmdType Type;
		/// <summary> Идентификатор объекта наблюдения, которому адресована команда </summary>
		public int TargetID;
		/// <summary>Дата создания команды</summary>
		public DateTime? Created;
		/// <summary> Результат выполнения команды, если есть </summary>
		public CmdResult? Result;
		/// <summary> Статус выполнения команды </summary>
		public CmdStatus Status;
		/// <summary> User-friendly описание результата работы команды </summary>
		public string ResultText;
		/// <summary> Дата получения результата </summary>
		public DateTime? ResultDate;
		/// <summary>Идентификатор объекта наблюдения, за чей счёт была выполнена команда (если платная)</summary>
		public int? PayerVehicleID;
		/// <summary> Дополнительные параметры команды </summary>
		public Dictionary<string, string> Parameters;

		/// <summary> Является ли команда актуальной - для пользователя. </summary>
		/// <remarks> Используется в UI для определения, показывать команду или нет. </remarks>
		[Obsolete]
		public bool? IsActual
		{
			get { return true; }
		}
		public void SetParameter(string key, string value)
		{
			if (Parameters == null)
				Parameters = new Dictionary<string, string>();

			Parameters[key] = value;
		}
		public void SetResult(CmdResult result)
		{
			Result = result;
			Status = CmdStatus.Completed;
		}
		public bool HasParameter(string key)
		{
			return Parameters != null && Parameters.ContainsKey(key);
		}
	}
}