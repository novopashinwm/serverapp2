﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class GeoZonePoint
	{
		public float  Lat    { get; set; }
		public float  Lng    { get; set; }
		public float? Radius { get; set; }
	}
}