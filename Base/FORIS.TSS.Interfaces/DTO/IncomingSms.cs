using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	public class IncomingSms
	{
		public string      Text         { get; set; }
		public DateTime    TimeReceived { get; set; }
		public SmsLocation Location     { get; set; }
		/// <summary> ������� ��������, � �������� ���������� ������ ��������� (������������� ��� �������� ����� ��������) </summary>
		public Contact     Requestor    { get; set; }
		public override string ToString()
		{
			return string.Format("From: {0}, Text: {1}", Requestor, Text);
		}
	}
}