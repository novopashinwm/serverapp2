﻿using System;
using Jayrock.Json.Conversion;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class Pagination
	{
		public Pagination()
		{
			ComputeTotal = true;
		}

		/// <summary> Номер страницы </summary>
		private int _page;
		public int Page
		{
			get { return _page; }
			set { _page = value > 0 ? value : 1; }
		}

		/// <summary> Записей на странице </summary>
		public int RecordsPerPage { get; set; }

		/// <summary> Всего страниц </summary>
		public int? TotalPages
		{
			get { return TotalRecords.HasValue ? (int)Math.Ceiling(TotalRecords.Value/(float)RecordsPerPage) : (int?)null; }
		}

		/// <summary> Всего записей </summary>
		public int? TotalRecords { get; set; }

		/// <summary> Всего записей на странице </summary>
		public int? TotalPageRecords { get; set; }

		[JsonIgnore]
		public bool ComputeTotal { get; set; }

		[JsonIgnore]
		public int Skip
		{
			get { return (Page - 1) * RecordsPerPage; }
		}
	}
}