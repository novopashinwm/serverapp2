﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	public class FriendRequest
	{
		public int      FromId;
		public int      ToId;
		public DateTime Time;
	}
}