﻿using System;
using System.Collections.Generic;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class AddRemoveServicesReportData
	{
		public List<SummaryRecord> SummaryRecords;

		public List<DetailRecord> DetailRecords;
			
		[Serializable]
		public class SummaryRecord
		{
			public int      Id;
			public string   Name;
			public decimal? MinPrice;
			public decimal? MaxPrice;
			public int      CountOnStart;
			public int      CountOfAdded;
			public int      CountOfRemoved;
			public int      CountOnEnd;
			public int      CountDelta { get { return CountOnEnd - CountOnStart; } }
		}
		[Serializable]
		public class DetailRecord
		{
			public int       ServiceId;
			public long?     TerminalDeviceNumber;
			public int       VehicleId;
			public string    VehicleName;
			public string    ContractNumber;
			public bool      Added;
			public bool      Removed;
			public DateTime  StartDate;
			public DateTime? EndDate;
		}
	}
}