﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class AddTrackerResult
	{
		public ResultCodes.AddTrackerResult Code;
		/// <summary>В случае успешного добавления трекера возвращает идентификатор объекта наблюдения</summary>
		public int? VehicleId;
		public AddTrackerResult(ResultCodes.AddTrackerResult code, int? vehicleId)
		{
			Code      = code;
			VehicleId = vehicleId;
		}
		public static implicit operator AddTrackerResult(ResultCodes.AddTrackerResult code)
		{
			return new AddTrackerResult(code, null);
		}
	}
}