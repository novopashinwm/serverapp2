﻿using System;
using System.Globalization;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class GetVehiclesArgs
	{
		/// <summary> Идентификаторы объектов для фильтра. </summary>
		public int[] VehicleIDs;
		/// <summary> Фильтр онлайн оффлайн объекты. </summary>
		public VehicleOnlineFilter? OnlineFilter;
		/// <summary> Фильтр по типам объектов. </summary>
		public VehicleObjectsFilter? ObjectsFilter;
		/// <summary> Включать адреса. </summary>
		public bool IncludeAddresses;
		/// <summary> Идентификатор карты </summary>
		public Guid? MapGuid;
		/// <summary> Включить информацию о состоянии объектов наблюдения (местоположение, скорость etc) </summary>
		public bool IncludeLastData;
		/// <summary> Включить информацию об атрибутах объектов наблюдения (название, подключенные услуги etc) </summary>
		public bool IncludeAttributes = true;
		/// <summary> Включить эскизы изображения </summary>
		public bool IncludeThumbnails = false;
		/// <summary>Объекты наблюдения считаются актуальными, если данные по ним отстают от текущего времени не более чем на данное время</summary>
		public TimeSpan? OnlineTime = TimeSpan.FromHours(1);
		/// <summary> Дата, на которую проверяется состояние объектов. </summary>
		public DateTime? CurrentDate;
		/// <summary> Включить информацию о геозонах, в которых находится ТС. </summary>
		public bool IncludeZones;
		/// <summary> Нужно ли делать фильтр по выбранному клиенту. </summary>
		public bool? FilterByDepartment;
		/// <summary> Культура пользователя для локализации. </summary>
		public CultureInfo Culture;
	}
}