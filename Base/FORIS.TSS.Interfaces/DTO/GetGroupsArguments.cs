﻿using System;
using System.Globalization;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class GetGroupsArguments
	{
		public int[] Ids;
		public bool IncludeIgnored;
		public bool IncludeNotInGroups;
		public bool IncludeAll;
		public bool IncludeStandard;
		public CultureInfo Culture = CultureInfo.CurrentCulture;
		public bool FilterByDepartment = true;
	}
}