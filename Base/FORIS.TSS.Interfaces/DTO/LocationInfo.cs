﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class LocationInfo
	{
		public int     IPCityId     { get; set; }
		public string  CityName     { get; set; }
		public string  RegionName   { get; set; }
		public string  DistrictName { get; set; }
		public decimal Latitude     { get; set; }
		public decimal Longitude    { get; set; }
	}
}