﻿using System;
using FORIS.TSS.Common;

namespace FORIS.TSS.BusinessLogic.DTO
{
    [Serializable]
    public class Picture : ILogTimeContainer
    {
        public int LogTime;
        public DateTime Date;
        public int CameraNumber;
        public int PhotoNumber;
        public string Url;
        public byte[] Bytes;

        public static int CompareByLogTime(Picture x, Picture y)
        {
            return x.LogTime.CompareTo(y.LogTime);
        }

        public int GetLogTime()
        {
            return LogTime;
        }
    }
}
