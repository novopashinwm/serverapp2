﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public enum Severity
	{
		Unknown,
		Normal,
		Warning,
		Error
	}
}