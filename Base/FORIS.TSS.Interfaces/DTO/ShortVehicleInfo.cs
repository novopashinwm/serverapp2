﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FORIS.TSS.BusinessLogic.DTO
{
    [Serializable]
    public class ShortVehicleInfo
    {
        public ShortVehicleInfo() { }

        public int id;
        public string gNum;
    }
}
