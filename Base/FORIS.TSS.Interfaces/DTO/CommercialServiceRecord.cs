﻿using System;
using System.Collections.Generic;

namespace FORIS.TSS.BusinessLogic.DTO
{
    [Serializable]
    public class CommercialServiceRecord
    {
        public string Country;
        public string MacroRegion;
        public string CustomerName;
        public string ContractNumber;
        public string ServiceTypeCategory;
        public string ServiceName;
        public int ServiceCount;
        public int RenderedCount;
        public DateTime? RenderedLastTime;

        public List<ServiceRecord> Services;

        public class ServiceRecord
        {
            public string Country;
            public string MacroRegion;
            public string CustomerName;
            public string ContractNumber;
            public string TerminalDeviceNumber;
            public string ServiceTypeCategory;
            public string ServiceName;
            public DateTime? StartDate;
            public DateTime? EndDate;
            public int RenderedCount;
            public int LbsPositionCount;
            public int LbsPositionRequested;
            public DateTime? RenderedLastTime;
        }
    }
}
