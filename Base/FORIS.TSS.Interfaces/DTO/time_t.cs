using System;

namespace FORIS.TSS.Common
{
	/// <summary>
	/// time_t
	/// </summary>
	public class time_t
	{
		int iSecs;
		
		public static implicit operator int(time_t tm)
		{
			return tm.iSecs;
		}
		
		public static implicit operator time_t(int sec)
		{
			time_t tm = new time_t();
			tm.iSecs = sec;
			return tm;
		}
		
		public static implicit operator DateTime(time_t tm)
		{
			return new DateTime(1970, 1, 1) + TimeSpan.FromSeconds(tm.iSecs);
		}
		
		public static implicit operator time_t(DateTime dt)
		{
			time_t tm = new time_t();
			tm.iSecs = (int)(dt - new DateTime(1970, 1, 1)).TotalSeconds;
			return tm;
		}
	
		public override string ToString()
		{
			return ((DateTime)(this)).ToString();
		}
	}
}