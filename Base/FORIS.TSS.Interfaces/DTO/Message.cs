﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace FORIS.TSS.BusinessLogic.DTO
{
	/// <summary> класс - сообщение </summary>
	[Serializable]
	[XmlRoot("message")]
	public class Message
	{
		private static readonly XmlSerializer Serializer = new XmlSerializer(typeof(Message));
		[XmlElement] public DateTime    Time         { get; set; }
		[XmlElement] public MessageType Type         { get; set; }
		[XmlElement] public string      Template     { get; set; }
		[XmlElement] public Severity    Severity     { get; set; }
		[XmlElement] public string      MessageTitle { get; set; }
		[XmlElement] public string      MessageBody  { get; set; }
		[XmlElement] public string      Destination  { get; set; }
		[XmlElement] public string      Source       { get; set; }
		public Message()
		{
			Severity     = Severity.Unknown;
			MessageTitle = "";
			MessageBody  = "";
		}
		public Message(Severity s, string title, string body)
		{
			Severity     = s;
			MessageTitle = title;
			MessageBody  = body;
		}
		public virtual void WriteXml(XmlTextWriter writer)
		{
			Serializer.Serialize(writer, this);
		}
		/// <summary> инициализация объекта значениями по умолчанию </summary>
		protected virtual void Init()
		{
			Severity     = Severity.Unknown;
			MessageTitle = "";
			MessageBody  = "";
		}
		/// <summary>
		/// инициализация сообщения из датасета
		/// Датасет содержит историю сообщений оператора с контроллером объекта мониторинга
		/// </summary>
		/// <param name="msgSet"></param>
		public static List<Message> GetMessagesFromDataSet(DataSet msgSet)
		{
			if (msgSet == null || msgSet.Tables.Count == 0) return null;
			DataTable tab = msgSet.Tables[0];

			var msgList = new List<Message>(tab.Rows.Count);
			foreach (DataRow row in tab.Rows)
			{
				var msg = new Message
				{
					MessageTitle = row["SUBJECT"].ToString(),
					MessageBody  = row["BODY"].ToString()
				};

				var time = row["TIME"] as DateTime?;
				msg.Time = time ?? DateTime.UtcNow;

				int type;
				int.TryParse(row["TYPE"].ToString(), out type);
				msg.Type = (type & 0x800) != 0
					? MessageType.Incoming
					: MessageType.Outgoing;

				msgList.Add(msg);
			}

			return msgList;
		}
		/// <summary> Сериализация списка сообщений </summary>
		public static void SerializeMessages(List<Message> messages, XmlTextWriter writer)
		{
			//<messages>
			writer.WriteStartElement("messages");
			foreach (Message msg in messages)
				msg.WriteXml(writer);
			writer.WriteEndElement();
			//</messages>
		}
	}
}