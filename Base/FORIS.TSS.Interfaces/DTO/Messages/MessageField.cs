﻿using System;
using System.Xml;
using Jayrock.Json.Conversion;

namespace FORIS.TSS.BusinessLogic.DTO.Messages
{
	public class MessageField
	{
		public int    Id              { get; set; }
		[JsonIgnore]
		public long   SortKey         { get; set; }
		[JsonIgnore]
		public int    TemplateFieldId { get; set; }
		[JsonIgnore]
		public int    MessageId       { get; set; }
		public string Value           { get; set; }
		public object StronglyTypeValue
		{
			get { return Template != null ? GetStrongTypedValue(Template.DotNetType, Value) : Value; }
		}
		public MessageTemplateField Template { get; set; }
		private static object GetStrongTypedValue(string dotNetTypeName, string rawValue)
		{
			switch (dotNetTypeName)
			{
				case "System.Int32":
					return XmlConvert.ToInt32(rawValue);
				case "System.DateTime":
					return XmlConvert.ToDateTime(rawValue, XmlDateTimeSerializationMode.Utc);
				case "System.Boolean":
					return XmlConvert.ToBoolean(rawValue);
				case "System.Double":
					return XmlConvert.ToDouble(rawValue);
				case "System.String":
					return rawValue;
				default:
					throw new ArgumentOutOfRangeException(nameof(dotNetTypeName), dotNetTypeName, @"Value is not supported");
			}
		}
	}
}