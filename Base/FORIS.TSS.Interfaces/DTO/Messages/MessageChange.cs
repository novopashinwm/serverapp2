﻿using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO.Messages
{
	public class MessageChange
	{
		public ChangeType ChangeType;
		public MessageWeb Message;
	}
}