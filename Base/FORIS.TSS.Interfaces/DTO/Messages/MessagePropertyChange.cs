﻿using System;
using Jayrock.Json.Conversion;

namespace FORIS.TSS.BusinessLogic.DTO.Messages
{
	public class MessagePropertyChange
	{
		[JsonIgnore]
		public int      UpdateId;
		public string   Name;
		public DateTime ActualTime;
		public string   Value;
	}
}