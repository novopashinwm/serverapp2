﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FORIS.TSS.BusinessLogic.DTO.Messages
{
	public class MessageFormat
	{
		private readonly Message _message;
		public MessageFormat(Message message)
		{
			_message = message;
		}
		public int Id
		{
			get { return _message.Id; }
		}
		public int? OwnerOperatorId
		{
			get { return _message.OwnerOperatorId; }
		}
		public DateTime Time
		{
			get { return _message.Time; }
		}
		public string Subject
		{
			get { return _message.Subject; }
		}
		public string Body
		{
			get { return _message.Body; }
		}
		public IDictionary<string, object> Fields
		{
			get { return _message.Fields.ToDictionary(key => key.Template.Name, value => value.StronglyTypeValue); }
		}
		public string Template
		{
			get { return _message.Template.Name; }
		}
		public Contact From
		{
			get { return _message.From; }
		}
		public Contact To
		{
			get { return _message.To; }
		}
	}
}