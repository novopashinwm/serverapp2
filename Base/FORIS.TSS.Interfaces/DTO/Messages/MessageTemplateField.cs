﻿using Jayrock.Json.Conversion;

namespace FORIS.TSS.BusinessLogic.DTO.Messages
{
	public class MessageTemplateField
	{
		public const string VehicleID = "VehicleID";
		public int    Id           { get; set; }
		[JsonIgnore]
		public int?   TemplateId   { get; set; }
		public int    DotNetTypeId { get; set; }
		public string DotNetType   { get; set; }
		public string Name         { get; set; }
	}
}