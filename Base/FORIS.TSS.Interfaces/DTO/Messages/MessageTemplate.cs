﻿using System.Globalization;

namespace FORIS.TSS.BusinessLogic.DTO.Messages
{
	public class MessageTemplate
	{
		/// <summary> Уведомление общего типа о срабатывании правил </summary>
		public const string GenericNotification             = "GenericNotification";
		/// <summary> Сообщение, содержащее код подтверждения для проверки, что контакт принадлежит пользователю </summary>
		public const string GenericConfirmation             = "GenericConfirmation";
		/// <summary> Уведомление о срабатывании правил, отправленное через мобильное приложение </summary>
		public const string AppClientNotification           = "AppClientNotification";
		/// <summary> Предложение доступа к объекту наблюдения </summary>
		public const string ProposeViewVehicle              = "ProposeViewVehicle";
		/// <summary> Предложение дружбы </summary>
		public const string ProposeFriendship               = "ProposeFriendship";
		/// <summary> Сообщение, отправляемое для демаскирования F(MSISDN) </summary>
		public const string DemaskAsidRequest               = "DemaskAsidRequest";
		/// <summary> Запрос на определение местоположения по данным смартфона </summary>
		public const string PositionRequest                 = "PositionRequest";
		/// <summary> Тревога </summary>
		public const string AlarmNotification               = "Alarm";
		/// <summary>Исчерпание пакета услуг</summary>
		public const string BillingServiceExhaust           = "BillingServiceExhaust";
		/// <summary> Друг был зарегистрирован в системе </summary>
		public const string FriendWasRegisteredNotification = "FriendWasRegistered";
		/// <summary> Шаблон сообщения о потери доступа для объекта наблюдения </summary>
		public const string LossRightsToObject              = "LossRightsToObject";
		/// <summary> Шаблон сообщения о результате выполнения заказанной команды </summary>
		public const string CommandResult                   = "CommandResult";
		/// <summary> Шаблон сообщения об изменении данных пользователя </summary>
		public const string UserAccount                     = "UserAccount";
		/// <summary> Результат запроса </summary>
		public const string RequestResult                   = "RequestResult";
		/// <summary> Ping-push для активации приложения </summary>
		/// <remarks> Сервер предполагает, что приложение на целевом устройстве заснуло было выгружено и отправляет этот push, чтобы "разбудить" приложение </remarks>
		public const string WakeUp                          = "WakeUp";
		/// <summary> Обновление данных - необходимо обновить локальную копию с сервера </summary>
		/// <remarks> Дополнительное поле "Тип данных" определяет, какие именно данные были изменены, например, Интервалы слежения </remarks>
		public const string DataUpdate                      = "DataUpdate";
		/// <summary> Нарушение режима вождения </summary>
		public const string EventViolationOfDriving         = "EventViolationOfDriving";
		public int         Id      { get; set; }
		public string      Name    { get; set; }
		public string      Header  { get; set; }
		public string      Body    { get; set; }
		public CultureInfo Culture { get; set; }
	}
}