﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;
using Jayrock.Json.Conversion;

namespace FORIS.TSS.BusinessLogic.DTO.Messages
{
	public class Message
	{
		public int                     Id              { get; set; }
		[JsonIgnore]
		public long                    SortKey         { get; set; }
		[JsonIgnore]
		public int?                    TemplateId      { get; set; }
		public string                  Subject         { get; set; }
		public string                  Body            { get; set; }
		public int?                    OwnerOperatorId { get; set; }
		public DateTime                Time            { get; set; }
		public DateTime?               Created         { get; set; }
		public MessageStatus           Status          { get; set; }
		public MessageProcessingResult Result          { get; set; }
		public MessageTemplate         Template        { get; set; }
		public Contact                 From            { get; set; }
		public Contact                 To              { get; set; }
		public List<MessageField>      Fields          { get; set; }
		[JsonIgnore]
		public int? VehicleId
		{
			get
			{
				var vehicleId = Fields
					.Where(field => field.Template != null)
					.Any(field => field.Template.Name == MessageTemplateField.VehicleID)
						? Fields.Where(field => field.Template != null)
							.First(field => field.Template.Name == MessageTemplateField.VehicleID).StronglyTypeValue
						: null;
				return (int?)vehicleId;
			}
		}
	}
}