﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO.Messages
{
	/// <summary> Класс сообщений используемый для отправки смс </summary>
	public class MessageWeb
	{
		/// <summary> Класс контакта </summary>
		public class Contact
		{
			public int    Id;
			public string Type;
			public string Value;
		}

		/// <summary> Идентификатор сообщения </summary>
		public int Id;
		/// <summary> Заголовок сообщения </summary>
		public string Subject;
		/// <summary> Тело сообщения </summary>
		public string Body;
		/// <summary> Дата последнего обновления сообщения </summary>
		public DateTime Time;
		/// <summary> Дата создания сообщения </summary>
		public DateTime? Created;
		/// <summary> Идентификатор пользователя владельца сообщения </summary>
		public int? OwnerOperatorId;
		/// <summary> Имя пользователя создателя сообщения </summary>
		public string OwnerOperatorName;
		/// <summary> Текущий статус сообщения </summary>
		public string Status;
		/// <summary> Результат обработки сообщения </summary>
		public MessageProcessingResult Result;
		/// <summary> Тип сообщения (входящее, исходящее) </summary>
		public MessageType MessageType;
		/// <summary> Шаблон сообщения </summary>
		public string TemplateName;
		/// <summary> Контакт от которого получено сообщение </summary>
		public Contact From;
		/// <summary> Контакт, которым получено сообщение </summary>
		public Contact To;
		/// <summary> Поля сообщения </summary>
		public Dictionary<string, string> Fields;
	}
}