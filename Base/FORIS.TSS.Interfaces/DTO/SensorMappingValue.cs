﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	public class SensorMappingValue
	{
		public int      Id;
		public int      InputNumber;
		public decimal  Multiplier;
		public decimal  Constant;
		public long?    MinValue;
		public long?    MaxValue;
		public int?     ValueExpired;
		public decimal? Error;
	}
}