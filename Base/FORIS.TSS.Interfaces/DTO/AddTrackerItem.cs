﻿using System;
using System.Runtime.Serialization;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	[DataContract]
	public class AddTrackerItem
	{
		/// <summary> Идентификатор трекера </summary>
		[DataMember] public string           TrackerId    { get; set; }
		/// <summary> Номер телефона трекера </summary>
		[DataMember] public string           TrackerPhone { get; set; }
		/// <summary> Имя объекта наблюдения </summary>
		[DataMember] public string           Name         { get; set; }
		/// <summary> Результат добавления </summary>
		[DataMember] public AddTrackerResult Result       { get; set; }
	}
}