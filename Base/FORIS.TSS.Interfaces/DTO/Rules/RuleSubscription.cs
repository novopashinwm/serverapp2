﻿using System;
using FORIS.TSS.BusinessLogic.Interfaces;

namespace FORIS.TSS.BusinessLogic.DTO.Rules
{
	[Serializable]
	public class RuleSubscription : IIdentifiable
	{
		public int Id;
		public int OperatorId;

		/// <summary> Оповещение отправляется на подтвержденный номер телефона </summary>
		public bool ToOwnPhone;
		public int? ToOwnPhoneMaxQty;

		/// <summary> Оповещение отправляется на подтвержденный адрес электронной почты </summary>
		public bool ToOwnEmail;
		public int? ToOwnEmailMaxQty;

		/// <summary> Оповещение отправляется в клиентское приложение </summary>
		public bool ToAppClient;
		public int? ToAppClientMaxQty;

		public int[]    EmailIds;
		public int[]    PhoneIds;

		public string[] Emails;
		public string[] Phones;

		int IIdentifiable.ID { get { return Id; } }
	}
}