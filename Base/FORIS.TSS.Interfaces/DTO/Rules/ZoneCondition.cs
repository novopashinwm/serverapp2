﻿using System;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.BusinessLogic.Server;

namespace FORIS.TSS.BusinessLogic.DTO.Rules
{
	[Serializable]
	public class ZoneCondition : IIdentifiable
	{
		public int           Id;
		public ZoneEventType EventType;
		/// <summary> Тип идентификатора для указания на зону: <see cref="IdType.Zone"/> или <see cref="IdType.ZoneGroup"/> </summary>
		public IdType        ZoneIdType;
		/// <summary>В зависимости от <see cref="ZoneIdType"/>, идентификатор зоны или группы зон</summary>
		public int           ZoneId;

		int IIdentifiable.ID { get { return Id; } }
	}
}