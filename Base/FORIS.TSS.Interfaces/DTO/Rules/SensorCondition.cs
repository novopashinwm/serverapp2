﻿using System;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces;

namespace FORIS.TSS.BusinessLogic.DTO.Rules
{
	[Serializable]
	public class SensorCondition : IIdentifiable
	{
		public int                  Id;
		public SensorLegend         Sensor;
		public ScalarComparisonType Type;
		public decimal              Value;

		int IIdentifiable.ID { get { return Id; } }
	}
}