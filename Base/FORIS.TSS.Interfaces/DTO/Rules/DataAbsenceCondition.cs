﻿using System;
using FORIS.TSS.BusinessLogic.Interfaces;

namespace FORIS.TSS.BusinessLogic.DTO.Rules
{
	/// <summary> Условие по отсутствию данных на сервере </summary>
	[Serializable]
	public class DataAbsenceCondition : IIdentifiable
	{
		public int Id;
		/// <summary> Время отсутствия данных, при превышение которого должно срабатывать правило </summary>
		public int Minutes;

		int IIdentifiable.ID { get { return Id; } }
	}
}