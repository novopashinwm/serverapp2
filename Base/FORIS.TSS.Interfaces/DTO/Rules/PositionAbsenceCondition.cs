﻿using System;
using FORIS.TSS.BusinessLogic.Interfaces;

namespace FORIS.TSS.BusinessLogic.DTO.Rules
{
	/// <summary> Условие по отсутствию координат на сервере </summary>
	[Serializable]
	public class PositionAbsenceCondition : IIdentifiable
	{
		public int Id;
		/// <summary> Время отсутствия координат, при превышение которого должно срабатывать правило </summary>
		public int Minutes;

		int IIdentifiable.ID { get { return Id; } }
	}
}