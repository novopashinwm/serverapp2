﻿using System;
using FORIS.TSS.BusinessLogic.Interfaces;

namespace FORIS.TSS.BusinessLogic.DTO.Rules
{
	[Serializable]
	public class TimePointCondition : IIdentifiable
	{
		public int Id;
		public int LogTime;

		int IIdentifiable.ID { get { return Id; } }
	}
}