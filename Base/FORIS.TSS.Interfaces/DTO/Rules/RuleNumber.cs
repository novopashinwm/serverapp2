﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO
{
	/// <summary> Номера правил. Номера не должны меняться от версии к версии! </summary>
	[Serializable]
	public enum RuleNumber : int
	{
		None                    = 00,
		/// <summary> Отсутствие GPS больше заданного времени </summary>
		NoGPS                   = 01,
		/// <summary> Отсутствие GSM больше заданного времени </summary>
		NoGSM                   = 02,
		/// <summary> Стоянка больше заданного времени </summary>
		Stop                    = 03,
		/// <summary> зажигание есть </summary>
		Ignition                = 04,
		/// <summary> Отклонение от маршрута </summary>
		RouteDeviation          = 05,
		/// <summary> Статус ТС </summary>
		Status                  = 06,
		/// <summary> Выход из зоны </summary>
		Zone                    = 07,
		/// <summary> Прохождение геозон </summary>
		Fuel                    = 08,
		/// <summary> Статус скорой помощи </summary>
		Ambulance               = 09,
		/// <summary> Норматив расхода топлива по моточасам (л/моточас) </summary>
		FuelSpendStandardPerMH  = 12,
		/// <summary> Игнорировать слив перед заправкой </summary>
		/// <remarks> Правило для выделения машин, у которых плохо обусловлен бак с ДУТ,
		/// когда при заправке образуется воронка, в результате чего фиксируется ложный слив.</remarks>
		IgnoreDrainBeforeRefuel = 13,
		/// <summary> Получен сигнал от цифрового датчика </summary>
		DigitalSensor           = 14,
		/// <summary> Версия отчёта по топливу </summary>
		FuelReportVersion       = 15,
		/// <summary>Определение местоположения локатором по данным сотовой сети</summary>
		LbsGsm                  = 16,
		/// <summary>Определение местоположения локатором по данным сетей WiFi</summary>
		LbsWifi                 = 17,
		/// <summary> Периодичность запроса фото </summary>
		CapturePictureInterval  = 18
	}
}