﻿using System;
using FORIS.TSS.BusinessLogic.Interfaces;

namespace FORIS.TSS.BusinessLogic.DTO.Rules
{
	[Serializable]
	public class TimeCondition : IIdentifiable
	{
		public int  Id;
		public byte HoursFrom;
		public byte MinutesFrom;
		public byte HoursTo;
		public byte MinutesTo;

		int IIdentifiable.ID { get { return Id; } }
	}
}