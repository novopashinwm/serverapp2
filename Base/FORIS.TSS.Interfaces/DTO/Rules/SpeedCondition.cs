﻿using System;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces;

namespace FORIS.TSS.BusinessLogic.DTO.Rules
{
	[Serializable]
	public class SpeedCondition : IIdentifiable
	{
		public int                  Id;
		public ScalarComparisonType Type;
		public int                  Value;

		int IIdentifiable.ID { get { return Id; } }
	}
}