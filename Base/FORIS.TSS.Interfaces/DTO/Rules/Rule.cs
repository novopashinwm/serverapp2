﻿using System;
using FORIS.TSS.BusinessLogic.Server;

namespace FORIS.TSS.BusinessLogic.DTO.Rules
{
	public enum RuleValidationGroup
	{
		TimeConditions,
		TimePointConditions,
		SensorConditions,
		SpeedConditions,
		ZoneConditions,
		DataAbsenceConditions,
		PositionAbsenceConditions,
		Subscriptions
	}

	public class RuleValidationError
	{
		public string              Message;
		public RuleValidationGroup ValidationGroup;
		public object              Value;
	}

	/// <summary> Правило слежения за мобильным объектом </summary>
	[Serializable]
	public class Rule
	{
		/// <summary> PK для объекта правила </summary>
		public int    Id;
		/// <summary> Тип идентификатора объекта наблюдения <see cref="ObjectId"/></summary>
		public IdType ObjectIdType;
		/// <summary> Идентификатор объекта наблюдения, зависит от <see cref="ObjectIdType"/></summary>
		public int    ObjectId;
		/// <summary> Идентификатор заказа (нужен для отделения правил заказов от остальных) </summary>
		public int?   OrderId;
		/// <summary> Флаг, показывающий, включено или выключено правило в настоящий момент </summary>
		public bool   Enabled;
		/// <summary> Описание правила </summary>
		public string Description;
		/// <summary> Условие по срабатыванию в начале движения </summary>
		public bool   MotionStart;
		/// <summary> Время начала обработки правила </summary>
		public int?   ProcessingBegAt;
		/// <summary> Время окончания обработки правила </summary>
		public int?   ProcessingEndAt;
		/// <summary> Условия по датчикам </summary>
		public SensorCondition[]          SensorConditions;
		/// <summary> Условия по скорости </summary>
		public SpeedCondition[]           SpeedConditions;
		/// <summary> Условия по времени дня </summary>
		public TimeCondition[]            TimeConditions;
		/// <summary> Условия по точкам времени </summary>
		public TimePointCondition[]       TimePointConditions;
		/// <summary> Условия по геозонам (readonly) </summary>
		public ZoneCondition[]            ZoneConditions;
		/// <summary> Условия по отсутствию данных на сервере </summary>
		public DataAbsenceCondition[]     DataAbsenceConditions;
		/// <summary> Условия по отсутствию координат на сервере </summary>
		public PositionAbsenceCondition[] PositionAbsenceConditions;
		/// <summary> Информация о подписках на событие </summary>
		public RuleSubscription[]         Subscriptions;
		public override bool Equals(object obj)
		{
			if (obj == null)
				return false;
			var rule = obj as Rule;
			if (rule == null)
				return false;
			return Id == rule.Id && ObjectIdType == rule.ObjectIdType;
		}
		public override int GetHashCode()
		{
			return Id.GetHashCode();
		}
	}
}