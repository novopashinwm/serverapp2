﻿using System;
using System.Runtime.Serialization;

namespace FORIS.TSS.BusinessLogic.DTO
{
	[Serializable]
	[DataContract]
	public class DictionaryEnum<TEnum> where TEnum : struct, IConvertible
	{
		/// <summary> Целочисленный идентификатор </summary>
		[DataMember] public int    Id   { get; set; } = -1;
		/// <summary> Строковый идентификатор </summary>
		[DataMember] public string Code { get; set; } = string.Empty;
		/// <summary> Наименование элемента справочника, может быть локализованным или отсутствовать </summary>
		[DataMember] public string Name { get; set; } = string.Empty;
		/// <summary> Преобразование DictionaryEntity в TEnum? </summary>
		/// <param name="entityItem"></param>
		/// <returns></returns>
		public TEnum? ToEnum()
		{
			var enmItem = default(TEnum);
			if (Enum.TryParse(Code, true, out enmItem))
				return enmItem;
			if (Enum.TryParse(Id.ToString(), true, out enmItem))
				return enmItem;
			return default(TEnum?);
		}
	}
}