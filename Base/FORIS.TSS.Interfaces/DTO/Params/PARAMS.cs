﻿using System;
using System.Collections;

namespace FORIS.TSS.Common
{
	/// <summary> command params class </summary>
	[Serializable]
	public class PARAMS : DictionaryBase
	{
		/// <summary> Ключи для доступа к параметрам </summary>
		public static class Keys
		{
			/// <summary> Отправлен ли запрос к мобильному устройству через облако< /summary>
			public static readonly string CloudPushSent  = "CloudPushSent";
			/// <summary> Отправлен ли запрос местоположения </summary>
			public static readonly string LbsRequestSent = "LbsRequestSent";
			/// <summary> Номер телефона </summary>
			public static readonly string Phone          = "Phone";
			/// <summary> Номер телефона, от имени которого нужно отправлять ответ на SMS </summary>
			public static readonly string ReplyToPhone   = "ReplyToPhone";

			/// <summary> Параметры точки доступа для подключения к Интернету </summary>
			public static class InternetApn
			{
				/// <summary>Имя точки доступа</summary>
				public static readonly string Name     = "apnName";
				/// <summary>Имя пользователя</summary>
				public static readonly string User     = "apnUser";
				/// <summary>Пароль для подключения</summary>
				public static readonly string Password = "apnPassword";
			}
		}

		/// <summary> param name case sensitivity </summary>
		bool bCaseSense;

		public PARAMS()
		{
		}
		public PARAMS(bool caseSense)
		{
			bCaseSense = caseSense;
		}
		public bool ContainsKey(string key)
		{
			return Dictionary.Contains(bCaseSense ? key : key.ToUpper());
		}
		public object this[string key]
		{
			get
			{
				return Dictionary[bCaseSense ? key : key.ToUpper()];
			}
			set
			{
				Dictionary[bCaseSense ? key : key.ToUpper()] = value;
			}
		}
		public void Add(string key, object value)
		{
			Dictionary.Add(bCaseSense ? key : key.ToUpper(), value);
		}
		public void Add(Enum key, object value)
		{
			Add(key.ToString(), value);
		}
		public void Remove(string key)
		{
			Dictionary.Remove(bCaseSense ? key : key.ToUpper());
		}
		protected override void OnInsert(object key, object value)
		{
			if (key.GetType() != typeof(string))
				throw new ArgumentException(@"key must be of type String.", "key");
		}
		protected override void OnRemove(object key, object value)
		{
			if (key.GetType() != typeof(string))
				throw new ArgumentException(@"key must be of type String.", "key");
		}
		protected override void OnSet(object key, object oldValue, object newValue)
		{
			if (key.GetType() != typeof(string))
				throw new ArgumentException(@"key must be of type String.", "key");
		}
		protected override void OnValidate(object key, object value)
		{
			if (key.GetType() != typeof(string))
				throw new ArgumentException(@"key must be of type String.", "key");
		}
	}
}