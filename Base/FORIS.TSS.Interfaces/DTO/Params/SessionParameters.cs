﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO.Params
{
	[Serializable]
	public class SessionParameters
	{
		private bool   _reasonModified;
		private string _reason;
		private bool   _parentSessionIdModified;
		private int    _parentSessionId;
		public string Reason
		{
			get { return _reason; }
			set
			{
				_reason         = value;
				_reasonModified = true;
			}
		}
		public bool   ReasonModified
		{
			get
			{
				return _reasonModified;
			}
		}
		public int    ParentSessionId
		{
			get { return _parentSessionId; }
			set
			{
				_parentSessionId         = value;
				_parentSessionIdModified = true;
			}
		}
		public bool   ParentSessionIdModified
		{
			get { return _parentSessionIdModified; }
		}
	}
}