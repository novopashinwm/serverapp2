﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO.Params
{
	[Serializable]
	public struct LogTimeSpanParam
	{
		public readonly int LogTimeFrom;
		public readonly int LogTimeTo;

		public LogTimeSpanParam(int logTimeFrom, int logTimeTo)
		{
			LogTimeFrom = logTimeFrom;
			LogTimeTo   = logTimeTo;
		}
	}
}