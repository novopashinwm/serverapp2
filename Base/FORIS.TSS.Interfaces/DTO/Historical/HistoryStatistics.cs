﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	[Serializable]
	public class HistoryStatistics
	{
		public decimal? run;
		public int?     maxSpeed;
		public double?  runCAN;
		public int?     moveTime;
		public int?     stopTime;
		/// <summary> Суммарное время отсутствия сигнала GPS, в секундах </summary>
		public int      gpsAbsenceSeconds;
		public int?     percentageShown;
	}
}