﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	[Serializable]
	public class FullLogOptions
	{
		public static readonly TimeSpan DefaultStopPeriod = TimeSpan.FromMinutes(3);
		/// <summary> Минимальный период отсутствия сигналов, для которого считается, что данных не было весь этот период </summary>
		/// <remarks>
		/// От устройства поступают значения в определенные промежутки времени.
		/// Во время между этими значениями считается, что измеряемая величина изменяется линейно.
		/// Однако если сигнала не было более определенного промежутка времени,
		/// это означает, что мы ничего не можем сказать о поведении величины в это время.
		/// </remarks>
		public TimeSpan NoDataPeriod = TimeSpan.FromMinutes(30);
		/// <summary> Точность местоположения в метрах, используемая для фильтрации местоположения </summary>
		/// <remarks>
		/// Это значение следует заполнять для управления детализацией истории движения.
		/// Чем больше значение, тем менее детализирована история.
		/// </remarks>
		public int? PositionAccuracy;
		/// <summary> Максимальное количество точек для скалярных данных </summary>
		public int MaxPointsCount = 1000;
		/// <summary> Максимальное количество точек для географических данных </summary>
		public int MaxGeoPointsCount = 10000;
		/// <summary> Удалять точки вне заданного интервала </summary>
		public bool CutOutstandingData = true;
		/// <summary> Признак фильтрации позиций объекта </summary>
		public bool IsGeoPointsFilterOn = true;
		/// <summary> Максимальное количество точек </summary>
		public int? MaxTotalPointCount;
		/// <summary> Необходимо определять адреса точек? </summary>
		public bool IncludeAddresses;
		/// <summary> Минимальный интервал времени, за пределами которого позиция считается остановкой </summary>
		public TimeSpan? StopPeriod;
	}
}