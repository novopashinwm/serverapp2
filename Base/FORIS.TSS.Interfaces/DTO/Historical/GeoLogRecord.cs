﻿using System;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	[Serializable]
	public struct GeoLogRecord : ILogTimeContainer
	{
		public int           LogTime;
		public decimal       Lng;
		public decimal       Lat;
		public int?          Radius;
		public byte?         Speed;
		public short?        Course;
		/// <summary> Значение накопленного пробега в сантиметрах </summary>
		public long?         Run;
		/// <summary>Высота над уровнем моря, в метрах</summary>
		public int?          Altitude;
		public PositionType? Type;

		public bool? IsMLP
		{
			get { return Type.HasValue ? Type.Value == PositionType.LBS : (bool?)null; }
		}
		public static int CompareByLogTime(GeoLogRecord x, GeoLogRecord y)
		{
			return x.LogTime.CompareTo(y.LogTime);
		}
		public int GetLogTime()
		{
			return LogTime;
		}
	}
}