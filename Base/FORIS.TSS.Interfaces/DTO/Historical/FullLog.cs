﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	[Serializable]
	public class FullLog
	{
		/// <summary> Набор записей о geo-позиции, её точности, типе, скорости, курсе </summary>
		public List<GeoLogRecord>                         Geo;
		/// <summary> Набор записей полученных от спутниковой системы о скорости и курсе (с указанием количества спутников) </summary>
		public List<GPSLogRecord>                         Gps;
		/// <summary> Набор записей о точности geo-позиции и о типе определения </summary>
		[Obsolete("Do not use, will be moved to GeoLogRecord")]
		public List<AccuracyRecord>                       Accuracy;
		/// <summary> Набор датчиков физических </summary>
		public IDictionary<SensorLegend, Sensor>          Sensors;
		/// <summary> Информация о привязке и преобразованию, физических датчиков в производные </summary>
		public IDictionary<SensorLegend, List<SensorMap>> SensorMappings;
		/// <summary> Полный набор временных меток для всех элементов лога </summary>
		public List<int>                                  FullLogTimes;
		/// <summary> Список интервалов отсутствия любых данных </summary>
		public List<IntervalRecord>                       Interruptions;
		/// <summary> Список интервалов отсутствия данных из <see cref="FullLog.Gps"/> </summary>
		public List<IntervalRecord>                       GpsAbsence;
		/// <summary> Набор записей одометра </summary>
		public List<RunLogRecord>                         Runs;
		/// <summary> Заправки </summary>
		public List<SensorChangeInterval>                 Refuels;
		/// <summary> Сливы </summary>
		public List<SensorChangeInterval>                 Drains;
		public int                                        LogTimeFrom;
		public int                                        LogTimeTo;
		public decimal                                    Run;
		public FullLogOptions                             Options;
		public List<Picture>                              Pictures;
		public bool                                       Filtered;

		public FullLog(int logTimeFrom, int logTimeTo, FullLogOptions options)
		{
			if (options == null)
				throw new ArgumentNullException(nameof(options));

			LogTimeFrom = logTimeFrom;
			LogTimeTo   = logTimeTo;
			Options     = options;
		}
		public List<int> GetAllLogTimes(bool recompute = true)
		{
			if (recompute)
			{
				var result = new Dictionary<int, bool>(Geo.Count);
				foreach (var item in Geo)
					result[item.GetLogTime()] = true;
				foreach (var item in Gps)
					result[item.GetLogTime()] = true;
				foreach (var item in Accuracy)
					result[item.GetLogTime()] = true;
				foreach (var sensor in Sensors.Values)
				{
					foreach (var item in sensor.LogRecords)
						result[item.GetLogTime()] = true;
				}
				foreach (var picture in Pictures)
					result[picture.LogTime] = true;

				FullLogTimes = result.Keys.ToList();
				FullLogTimes.Sort();
			}

			return FullLogTimes;
		}
		public IEnumerable<Sensor> GetSensorsBySensorLegend(params SensorLegend[] sensorLegends)
		{
			return Sensors
				?.Where(s => sensorLegends.Contains(s.Key))
				?.Select(s => s.Value);
		}
	}
}