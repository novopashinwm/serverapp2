﻿using System;
using System.Collections.Generic;
using FORIS.TSS.Common;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	[Serializable]
	public struct SensorLogRecord : ILogTimeContainer
	{
		public int      LogTime;
		public DateTime Date;
		public decimal  Value;
		public string   DisplayedValue;
		/// <summary> Абсолютная погрешность измерения </summary>
		public decimal? Error;
		public SensorLogRecord(int logTime, decimal value, decimal? error = null)
		{
			LogTime        = logTime;
			Date           = TimeHelper.GetDateTimeUTC(logTime);
			Value          = value;
			Error          = error;
			DisplayedValue = null;
		}
		public static int CompareByLogTime(SensorLogRecord x, SensorLogRecord y)
		{
			return x.LogTime.CompareTo(y.LogTime);
		}
		private class LogTimeComparerImplementation : Comparer<SensorLogRecord>
		{
			public override int Compare(SensorLogRecord x, SensorLogRecord y)
			{
				return CompareByLogTime(x, y);
			}
		}
		public static readonly IComparer<SensorLogRecord> LogTimeComparer = new LogTimeComparerImplementation();
		public int GetLogTime()
		{
			return LogTime;
		}
		public override string ToString()
		{
			return TimeHelper.GetDateTimeUTC(LogTime).ToLocalTime() + " : " + Value;
		}
		public SensorLogRecord SetDisplayedValue(string displayedValue)
		{
			var result = this;
			result.DisplayedValue = displayedValue;
			return result;
		}
	}
}