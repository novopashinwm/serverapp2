﻿using System.Collections.Generic;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	public class HistoryMarked<T>
	{
		public List<T> Items;
		public int HistoryId;
	}
}