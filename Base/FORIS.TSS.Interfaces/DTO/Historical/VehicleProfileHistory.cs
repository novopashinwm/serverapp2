﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	public class VehicleProfileHistory
	{
		public DateTime ActualTime;
		public string   Name;
		public string   Value;
	}
}