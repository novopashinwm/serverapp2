﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	[Serializable]
	public class ActionIntervalDbInterval
	{
		/// <summary> В движении? (объект может перемещаться, но не быть в движении, если перемещения малы) </summary>
		public bool     IsMotion;
		/// <summary> Дата начало интервала </summary>
		public DateTime Start;
		/// <summary> Дата окончания интервала </summary>
		public DateTime End;
		/// <summary> Длина интервала в секундах </summary>
		public int      TimeSpanSeconds;
		/// <summary> Пройденное расстояние, м </summary>
		public decimal  Run;
		/// <summary>  Средняя скорость, км/ч </summary>
		public decimal  AvgSpeed;
	}
}