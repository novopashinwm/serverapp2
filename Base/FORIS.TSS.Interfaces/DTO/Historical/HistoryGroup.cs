﻿using System;
using System.Collections.Generic;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	[Serializable]
	public class HistoryGroup
	{
		public HistoryGroup()
		{
		}
		/// <summary> Тип группы точек истории </summary>
		public GroupingType grType;
		/// <summary> Duration </summary>
		public int          dur;
		/// <summary> Пробег в рамах данной группы, может быть для любого типа (стоянка остановка, и даже отсутствие данных) </summary>
		public decimal?     run;
		/// <summary> Время в движении, в любом типе группы может быть движение и стоянки, т.к. группа это приближенное объединение </summary>
		public int?         moveTime;
		/// <summary> Время без движения, в любом типе группы может быть движение и стоянки, т.к. группа это приближенное объединение </summary>
		public int?         stopTime;
		/// <summary> Время первой точки группы </summary>
		public int          LogTimeFrom;
		/// <summary> Время последней точки группы </summary>
		public int          LogTimeTo;
		public IEnumerable<int> GetFromAndToLogTimes()
		{
			yield return LogTimeFrom;
			if (LogTimeTo != LogTimeFrom)
				yield return LogTimeTo;
		}
	}
}