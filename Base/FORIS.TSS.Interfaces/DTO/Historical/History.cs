﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO.Messages;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	[Serializable]
	public class History
	{
		public Vehicle                    vehicle;
		/// <summary> Сводка о треке - группы точек по типу: движение / остановки / запросы местоположения и т.п. </summary>
		public List<HistoryGroup>         groups;
		public HistoryStatistics          statistics;
		public FullLog                    log;
		/// <summary> Адреса для некоторых Log_Time </summary>
		public Dictionary<int, string>    Addresses;
		public List<MessageFormat>        Messages;
		public History(Vehicle vehicleParam)
		{
			vehicle    = vehicleParam;
			groups     = new List<HistoryGroup>();
			statistics = new HistoryStatistics();
		}
	}
}