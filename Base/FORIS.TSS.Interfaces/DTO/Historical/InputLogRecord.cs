﻿using System;
using System.Collections.Generic;
using FORIS.TSS.Common;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	[Serializable]
	public struct InputLogRecord : ILogTimeContainer
	{
		public readonly int  LogTime;
		public readonly int  Number;
		public readonly long Value;
		public InputLogRecord(int logTime, int number, long value)
		{
			LogTime = logTime;
			Number  = number;
			Value   = value;
		}
		public static int CompareByLogTime(SensorLogRecord x, SensorLogRecord y)
		{
			return x.LogTime.CompareTo(y.LogTime);
		}
		private class LogTimeComparerImplementation : Comparer<SensorLogRecord>
		{
			public override int Compare(SensorLogRecord x, SensorLogRecord y)
			{
				return CompareByLogTime(x, y);
			}
		}
		public static readonly IComparer<SensorLogRecord> LogTimeComparer = new LogTimeComparerImplementation();
		public int GetLogTime()
		{
			return LogTime;
		}
	}
}