﻿using System;
using FORIS.TSS.Common;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	[Serializable]
	public struct CellNetworkRecord : ILogTimeContainer
	{
		public int    LogTime;
		public int    Number;
		public string CountryCode;
		public string NetworkCode;
		public int    CellID;
		public int    LAC;
		public int?   SAC;
		public int?   ECI;
		public int    SignalStrength;

		public int GetLogTime()
		{
			return LogTime;
		}
	}
}