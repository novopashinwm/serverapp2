﻿using System.Collections.Generic;
using System.Linq;

namespace FORIS.TSS.Common
{
	public class InputSample
	{
		public struct InputRecord : ILogTimeContainer
		{
			public int   LogTime;
			public long? Value;
			public int GetLogTime()
			{
				return LogTime;
			}
			public InputRecord(int logTime, long? value)
			{
				LogTime = logTime;
				Value   = value;
			}
			internal static readonly LogTimeComparer<InputSample.InputRecord> LogTimeComparer = new LogTimeComparer<InputSample.InputRecord>();
		}
		public readonly List<InputRecord> Records;
		/// <summary> Входное значение </summary>
		/// <param name="records">Массив записей - может быть отсортирован или нет</param>
		/// <param name="needSorting">Нужен для оптимизации при условии сортировки в бд</param>
		public InputSample(List<InputRecord> records, bool needSorting = true)
		{
			Records = records;
			if (needSorting)
			{
				Records.Sort(InputRecord.LogTimeComparer);
			}
		}
		public IEnumerable<int> LogTimes
		{
			get
			{
				return Records.Select(record => record.LogTime);
			}
		}
	}
}