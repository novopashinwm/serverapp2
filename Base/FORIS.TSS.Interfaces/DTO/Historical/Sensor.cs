﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	[Serializable]
	public class Sensor
	{
		public int                    SensorId;
		public string                 Name;
		public SensorLegend           Number;
		public SensorType             Type;
		public string                 Unit;
		public List<SensorLogRecord>  LogRecords;
		public List<SensorStatistics> SensorStatistics;
		/// <summary> Справочник локализованных значений для дискретных датчиков </summary>
		/// <remarks>Если не задан, для дискретных датчиков следует использовать "вкл"/"выкл"</remarks>
		public string[] ValueNames;
		public static int CompareByName(Sensor x, Sensor y)
		{
			var xValue = x != null ? x.Name : null;
			var yValue = y != null ? y.Name : null;
			return StringComparer.OrdinalIgnoreCase.Compare(xValue, yValue);
		}
	}
}