﻿namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	/// <summary> Интервал однородного изменения значения датчика </summary>
	/// <remarks>
	/// Используется для построения отчета по топливу.
	/// Зная все интервалы, на которых топливо изменяется одинаково - одно и то же значение первой производной по времени,
	/// можно выделить моменты сливов и заправок.
	/// </remarks>
	public class SensorChangeInterval
	{
		public SensorLogRecord From;
		public SensorLogRecord To;
	}
}