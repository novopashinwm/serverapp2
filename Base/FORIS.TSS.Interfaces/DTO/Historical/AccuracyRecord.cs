﻿using System;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	[Serializable]
	public struct AccuracyRecord : ILogTimeContainer
	{
		public int          LogTime;
		public int          Radius;
		public PositionType Type;

		public bool IsMLP
		{
			get { return Type == PositionType.LBS; }
		}
		public static int CompareByLogTime(AccuracyRecord x, AccuracyRecord y)
		{
			return x.LogTime.CompareTo(y.LogTime);
		}
		public int GetLogTime()
		{
			return LogTime;
		}
	}
}