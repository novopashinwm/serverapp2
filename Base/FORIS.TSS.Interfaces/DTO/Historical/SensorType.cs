﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	[Serializable]
	public enum SensorType
	{
		Analog  = 1,
		Digital = 2,
	}
}