﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	[Serializable]
	public enum GroupingType
	{
		/// <summary> Отсутствие описания группировки </summary>
		None        = 0,
		/// <summary> Координата получена с помощью MLP </summary>
		MLP         = 1,
		/// <summary> Позиция по данным службы Яндекс.Локатор </summary>
		Yandex      = 2,
		/// <summary> Позиция по данным службы Яндекс.Локатор - GSM сети </summary>
		YandexGsm   = 3,
		/// <summary> Позиция по данным службы Яндекс.Локатор - WiFi сети </summary>
		YandexWifi  = 4,
		/// <summary> Позицию задал пользователь </summary>
		UserDefined = 5,
		/// <summary> Стоянка (больше 5 мин.) </summary>
		Stand       = 6,
		/// <summary> Остановка </summary>
		Stop        = 7,
		/// <summary> Медленное движение (до 5 км/ч) </summary>
		Slow        = 8,
		/// <summary> Движение </summary>
		Motion      = 9,
	}
}