﻿using System;
using System.Collections.Generic;
using FORIS.TSS.Common;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	[Serializable]
	public struct RunLogRecord : ILogTimeContainer
	{
		public int  VehicleId;
		public int  LogTime;
		/// <summary> Значение накопленного пробега в сантиметрах </summary>
		public long Odometer;

		public static int CompareByLogTime(RunLogRecord x, RunLogRecord y)
		{
			return x.LogTime.CompareTo(y.LogTime);
		}
		private class LogTimeComparerImplementation : Comparer<RunLogRecord>
		{
			public override int Compare(RunLogRecord x, RunLogRecord y)
			{
				return CompareByLogTime(x, y);
			}
		}
		public static readonly IComparer<RunLogRecord> LogTimeComparer = new LogTimeComparerImplementation();

		public int GetLogTime()
		{
			return LogTime;
		}
	}
}