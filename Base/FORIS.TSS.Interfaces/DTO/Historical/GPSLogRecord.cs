﻿using System;
using System.Collections.Generic;
using FORIS.TSS.Common;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	[Serializable]
	public struct GPSLogRecord : ILogTimeContainer
	{
		public int    LogTime;
		public byte?  Speed;
		public short? Course;
		public byte   Satellites;

		public DateTime Date
		{
			get { return TimeHelper.GetDateTimeUTC(LogTime); }
		}

		/// <summary> Высота над уровнем моря, в метрах </summary>
		public int? Altitude;

		public static int CompareByLogTime(GPSLogRecord x, GPSLogRecord y)
		{
			return x.LogTime.CompareTo(y.LogTime);
		}

		private class LogTimeComparerImplementation : Comparer<GPSLogRecord>
		{
			public override int Compare(GPSLogRecord x, GPSLogRecord y)
			{
				return CompareByLogTime(x, y);
			}
		}

		public static readonly IComparer<GPSLogRecord> LogTimeComparer = new LogTimeComparerImplementation();

		public int GetLogTime()
		{
			return LogTime;
		}
	}
}