﻿using System;
using FORIS.TSS.Common;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	/// <summary> Временной интервал </summary>
	[Serializable]
	public struct IntervalRecord : ILogTimeContainer
	{
		/// <summary> Время начала пакета данных </summary>
		public int LogTime;
		/// <summary> Длина интервала в секундах </summary>
		public int Duration;

		#region ILogTimeContainer Members

		public int GetLogTime()
		{
			return LogTime;
		}

		#endregion

		public override string ToString()
		{
			return TimeHelper.GetDateTimeUTC(LogTime).ToLocalTime() + " : " + TimeSpan.FromSeconds(Duration);
		}
	}
}