﻿using System;
using FORIS.TSS.Common;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	[Serializable]
	public struct WlanRecord : ILogTimeContainer
	{
		public int    LogTime;
		public int    Number;
		public string WlanMacAddress;
		public int    SignalStrength;
		public string WlanSSID;
		public byte?  ChannelNumber;

		public int GetLogTime()
		{
			return LogTime;
		}
	}
}