﻿using System;

namespace FORIS.TSS.BusinessLogic.DTO.Historical
{
	/// <summary> Описывает статистику по датчику </summary>
	[Serializable]
	public struct SensorStatistics
	{
		/// <summary> Время накопления статистики с, int </summary>
		public int      LogTimeFrom;
		/// <summary> Время накопления статистики по, int </summary>
		public int      LogTimeTo;
		/// <summary> Всего включений датчика, количество </summary>
		public int      OnCount;
		/// <summary> Всего выключений датчика, количество </summary>
		public int      OffCount;
		/// <summary> Всего включено, секунды </summary>
		public int      OnTime;
		/// <summary> Всего выключено, секунды </summary>
		public int      OffTime;
		/// <summary> Всего нет данных, секунды </summary>
		public int      OutTime;
		/// <summary> Пробег, когда датчик был включен, км </summary>
		public decimal  RunWhenOn;
		/// <summary> Пробег, когда датчик был выключен, км </summary>
		public decimal  RunWhenOff;
		/// <summary> Время в движении </summary>
		public int      TimeWhenRun;
		/// <summary> Время в движении со включенным датчиком </summary>
		public int      TimeWhenOnAndRun;
		/// <summary> Время в покое со включенным датчиком </summary>
		public int      TimeWhenOnAndStop;
		/// <summary> Максимальная погрешность значения на интервале </summary>
		public decimal? MaxError;
	}
}