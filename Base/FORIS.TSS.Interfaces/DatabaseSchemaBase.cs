﻿using System.Collections;
using System.Data;

namespace FORIS.TSS.BusinessLogic
{
	public class DatabaseSchemaBase
	{
		protected static ArrayList tables = new ArrayList();
		protected static ArrayList h_tables = new ArrayList();
		public static ArrayList Tables
		{
			get
			{
				return tables;
			}
			set
			{
				tables = value;
			}
		}
		public static bool ContainsTable(string name)
		{
			return tables.IndexOf(name.ToUpper()) >= 0;
		}
	}

	public interface IDatabaseSchema
	{
		void MakeSchema(DataSet dataSet);
	}
}