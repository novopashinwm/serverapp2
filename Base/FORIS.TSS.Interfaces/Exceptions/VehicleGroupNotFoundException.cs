﻿using System;
using System.Runtime.Serialization;

namespace FORIS.TSS.BusinessLogic.Exceptions
{
    /// <summary>
    /// Группа объектов наблюдения не найдена по идентификатору
    /// </summary>
    [Serializable]
    public class VehicleGroupNotFoundException : ApplicationException
    {
        //
        // For guidelines regarding the creation of new exception types, see
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
        // and
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
        //

        /// <summary>
        /// Конструктор по-умолчанию
        /// </summary>
        public VehicleGroupNotFoundException()
        {
        }

        /// <summary>
        /// Конструктор с параметром message
        /// </summary>
        public VehicleGroupNotFoundException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Конструктор с передачей внутреннего исключения
        /// </summary>
        public VehicleGroupNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected VehicleGroupNotFoundException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}
