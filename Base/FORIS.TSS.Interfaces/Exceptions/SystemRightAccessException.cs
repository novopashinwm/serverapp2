﻿using System;
using System.Globalization;
using FORIS.TSS.BusinessLogic.Server;

namespace FORIS.TSS.BusinessLogic.Exceptions
{
	/// <summary> Объект не имеет прав </summary>
	[Serializable]
	public class SystemRightAccessException : ApplicationException
	{
		public SystemRight[] Rights  { get; private set; }
		public int           Id      { get; private set; }
		public IdType        IdType  { get; private set; }
		public CultureInfo   Culture { get; private set; }
		public override string Message
		{
			get { return _message; }
		}
		public SystemRightAccessException(string message, int id, IdType type, CultureInfo culture, params SystemRight[] rights)
		{
			Rights   = rights;
			Id       = id;
			IdType   = type;
			Culture  = culture;
			_message = message;
		}
		private readonly string _message;
	}
}