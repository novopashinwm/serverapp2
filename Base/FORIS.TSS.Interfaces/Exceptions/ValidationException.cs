﻿using System;

namespace FORIS.TSS.BusinessLogic.Exceptions
{
	[Serializable]
	public class ValidationException : ApplicationException
	{
		public Enum   Type;
		public object Value;
		public ValidationException(string message) : base(message)
		{
		}
	}
}