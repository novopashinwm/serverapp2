﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.Exceptions
{
	[Serializable]
	public class OperationBanException : Exception
	{
		public DateTime BanDate { get; private set; }

		public BanLimit Limit { get; private set; }

		public OperationBanException(DateTime banDate, BanLimit limit)
		{
			BanDate = banDate;
			Limit   = limit;
		}
	}
}