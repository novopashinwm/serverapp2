﻿using System;
using System.Runtime.Serialization;

namespace FORIS.TSS.BusinessLogic.Exceptions
{
    /// <summary>
    /// Объект наблюдения не найден по идентификатору
    /// </summary>
    [Serializable]
    public class VehicleNotFoundException : ApplicationException
    {
        //
        // For guidelines regarding the creation of new exception types, see
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
        // and
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
        //

        /// <summary>
        /// Конструктор по-умолчанию
        /// </summary>
        public VehicleNotFoundException()
        {
        }

        /// <summary>
        /// Конструктор с параметром message
        /// </summary>
        public VehicleNotFoundException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Конструктор с передачей внутреннего исключения
        /// </summary>
        public VehicleNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected VehicleNotFoundException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}
