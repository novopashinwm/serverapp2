﻿using System;

namespace FORIS.TSS.BusinessLogic.SchedulePassage
{
	[Serializable]
	public struct SchedulePointPassageInfo : ISchedulePointPassageInfo
	{
		private readonly int       idPoint;
		private readonly int       idSchedulePoint;
		private readonly DateTime  timeIn;
		private readonly DateTime  timeOut;
		private readonly DateTime? logTimeIn;
		private readonly DateTime? logTimeOut;

		#region ISchedulePointPassageInfo Members

		public int IdPoint
		{
			get { return idPoint; }
		}

		public int IdSchedulePoint
		{
			get { return idSchedulePoint; }
		}


		public DateTime TimeIn
		{
			get { return timeIn; }
		}

		public DateTime TimeOut
		{
			get { return timeOut; }
		}


		public DateTime? LogTimeIn
		{
			get { return logTimeIn; }
		}

		public DateTime? LogTimeOut
		{
			get { return logTimeOut; }
		}

		#endregion

		public SchedulePointPassageInfo(
			int idPoint,
			int idSchedulePoint,
			DateTime timeIn,
			DateTime timeOut,
			DateTime? logTimeIn,
			DateTime? logTimeOut
			)
		{
			this.idPoint = idPoint;
			this.idSchedulePoint = idSchedulePoint;

			this.timeIn = timeIn;
			this.timeOut = timeOut;

			if (logTimeIn != null)
			{
				Date Date = (Date)logTimeIn;

				double Seconds = ((DateTime)logTimeIn - Date).TotalSeconds;

				this.logTimeIn = Date.AddMinutes((int)Math.Ceiling(Seconds / 60));
			}
			else
			{
				this.logTimeIn = null;
			}

			if (logTimeOut != null)
			{
				Date Date = (Date)logTimeOut;

				double Seconds = ((DateTime)logTimeOut - Date).TotalSeconds;

				this.logTimeOut = Date.AddMinutes((int)Math.Floor(Seconds / 60));
			}
			else
			{
				this.logTimeOut = null;
			}

		}
	}
}