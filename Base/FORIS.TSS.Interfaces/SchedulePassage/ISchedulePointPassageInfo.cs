﻿using System;

namespace FORIS.TSS.BusinessLogic.SchedulePassage
{
	public interface ISchedulePointPassageInfo
	{
		int IdPoint          { get; }
		int IdSchedulePoint  { get; }
		DateTime  TimeIn     { get; }
		DateTime  TimeOut    { get; }
		DateTime? LogTimeIn  { get; }
		DateTime? LogTimeOut { get; }
	}
}