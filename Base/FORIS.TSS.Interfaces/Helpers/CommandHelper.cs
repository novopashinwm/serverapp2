﻿namespace FORIS.TSS.BusinessLogic.Helpers
{
	public sealed class CommandHelper
	{
		/// <summary> Интервал времени(сек) в течении которого позиция LBS затирается более точной координатой, если есть </summary>
		public const int LbsRewriteInterval = 5*60;
	}
}