﻿using System.Collections.Generic;

namespace FORIS.TSS.Common
{
	public class LogTimeComparer<T> : Comparer<T> where T : struct, ILogTimeContainer
	{
		public override int Compare(T x, T y)
		{
			return x.GetLogTime().CompareTo(y.GetLogTime());
		}

		public static readonly LogTimeComparer<T> Instance = new LogTimeComparer<T>();
	}
}