﻿using FORIS.TSS.BusinessLogic.DTO;

namespace FORIS.TSS.BusinessLogic.Helpers
{
	public static class ControllerTypeHelper
	{
		public static bool? SupportsDeviceId(string name)
		{
			if (name == null)
				return null;
			return name != ControllerType.Names.SoftTracker && name != ControllerType.Names.UltracomCollar;
		}
	}
}