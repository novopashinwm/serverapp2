﻿using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.Helpers
{
	public static class ContactTypeHelper
	{
		/// <summary> Перечень типов контактов, которые соответствуют мобильному приложению </summary>
		public static ContactType[] AppContactTypes = { ContactType.Android, ContactType.Apple, ContactType.iOsDebug };
		/// <summary> Перечень типов контактов, которые соответствуют мобильному приложению </summary>
		public static int[] AppContactTypeIds = AppContactTypes.Cast<int>().ToArray();
	}
}