﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;

namespace FORIS.TSS.BusinessLogic
{
	public static class TimeHelper
	{
		/// <summary> Время, принимаемое мной за нулевое </summary>
		public static readonly DateTime DateTimeNullValue = new DateTime(0);
		public static DateTime NULL
		{
			get
			{
				return DateTime.MinValue;
			}
		}
		/// <summary> Time base for calculations in UTC </summary>
		public static readonly DateTime year1970 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
		/// <summary> Returns date time from given base </summary>
		/// <param name="basedate"> Начало эпохи </param>
		/// <param name="time"> Количество секунд от начала эпохи </param>
		/// <returns></returns>
		public static DateTime GetDateTimeUTCFromBase(DateTime basedate, int time)
		{
			DateTime t = basedate.AddSeconds(time);
			return t;
		}
		/// <summary> Converts time into DateTime </summary>
		/// <param name="time"></param>
		/// <returns></returns>
		public static DateTime GetDateTimeUTC(int time)
		{
			DateTime t = GetDateTimeUTCFromBase(year1970, time);
			return t;
		}
		/// <summary> local time from seconds from midnight </summary>
		/// <param name="time"></param>
		/// <returns></returns>
		public static DateTime GetDateTimeUTCFromMidnight(int time)
		{
			DateTime globalNow = DateTime.Now.ToUniversalTime();
			DateTime t = GetDateTimeUTCFromBase(globalNow, time);
			return t;
		}
		/// <summary> Converts time to local time and then returns it as string </summary>
		/// <param name="time"></param>
		/// <param name="format"></param>
		/// <returns></returns>
		[Obsolete("Use override with TimeZoneInfo parameter")]
		public static string GetLocalTime(int time, string format)
		{
			DateTime t = GetDateTimeUTC(time);
			t = t.ToLocalTime();
			return t.ToString(format);
		}
		[Obsolete("Use override with TimeZoneInfo parameter")]
		public static DateTime GetLocalTime(int time)
		{
			var t = GetDateTimeUTC(time);
			t = t.ToLocalTime();
			return t;
		}
		public static DateTime GetLocalTime(int time, TimeZoneInfo timeZoneInfo)
		{
			return TimeZoneInfo.ConvertTimeFromUtc(GetDateTimeUTC(time), timeZoneInfo);
		}
		/// <summary> local time from seconds from midnight </summary>
		/// <param name="time"></param>
		/// <returns></returns>
		[Obsolete("Use override with TimeZoneInfo parameter")]
		public static string GetLocalTimeFromMidnight(int time)
		{
			DateTime t = GetDateTimeUTCFromBase(DateTime.Now.Date, time);
			return t.ToString("T");
		}
		public static int Round(int delta)
		{
			return (int)Math.Round(delta / 60.0);
		}
		/// <summary> Returns seconds from 1 January 1970 </summary>
		/// <returns></returns>
		public static int GetSecondsFromBase()
		{
			return GetSecondsFromBase(DateTime.UtcNow);
		}
		/// <summary> Returns seconds for given UTC time </summary>
		/// <returns></returns>
		public static int GetSecondsFromBase(DateTime dateUTC)
		{
			return GetSecondsFromBase(dateUTC, year1970);
		}
		/// <summary> Returns milliseconds for given UTC time </summary>
		/// <returns></returns>
		public static long GetMillisecondsFromBase(DateTime dateUTC)
		{
			return GetMillisecondsFromBase(dateUTC, year1970);
		}
		public static int GetSecondsFromBase(DateTime dateUTC, DateTime baseDate)
		{
			if (dateUTC.Kind != DateTimeKind.Utc)
				dateUTC = DateTime.SpecifyKind(dateUTC, DateTimeKind.Utc);

			var span = dateUTC - baseDate;
			return (int)span.TotalSeconds;
		}
		public static long GetMillisecondsFromBase(DateTime dateUTC, DateTime baseDate)
		{
			if (dateUTC.Kind != DateTimeKind.Utc)
				dateUTC = DateTime.SpecifyKind(dateUTC, DateTimeKind.Utc);

			var span = dateUTC - baseDate;
			return (long)span.TotalMilliseconds;
		}
		/// <summary> Возвращает для указанного LogTime первую секунду текущего дня с учетом текущей временной зоны </summary>
		public static int GetFirstSecondOfDay(int logTime, TimeZoneInfo timeZoneInfo)
		{
			var utcTime = GetDateTimeUTC(logTime);
			var localTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, timeZoneInfo);
			var utcUnixDate = GetSecondsFromBase(TimeZoneInfo.ConvertTimeToUtc(localTime.Date, timeZoneInfo));
			return utcUnixDate;
		}
		public static int ToLogTime(this DateTime dateTime)
		{
			return GetSecondsFromBase(dateTime);
		}
		public static int? ToLogTime(this DateTime? dateTime)
		{
			if (!dateTime.HasValue)
				return default(int?);
			return dateTime.Value.ToLogTime();
		}
		public static DateTime ToUtcDateTime(this int logTime)
		{
			return GetDateTimeUTC(logTime);
		}
		public static DateTime? ToUtcDateTime(this int? logTime)
		{
			if (!logTime.HasValue)
				return default(DateTime?);
			return logTime.Value.ToUtcDateTime();
		}
		public static long ToLogTimeMs(this DateTime dateTime)
		{
			return GetMillisecondsFromBase(dateTime);
		}
		/// <summary> Возвращает время, соответствующее переданному количеству миллисекунд от 1970 года </summary>
		/// <param name="msSince1970">Количество миллисекунд от 1970 года</param>
		public static DateTime FromLong(long msSince1970)
		{
			return year1970.AddMilliseconds(msSince1970);
		}
		public static DateTime ConvertToDateTime(object value)
		{
			var s = value as string;
			if (s != null)
				return ParseDateTime(s);

			return Convert.ToDateTime(value);
		}
		private static readonly string[] ExactDateTimeFormats =
		{
			"dd-MM-yyyy HH:mm:ss",
			"dd-MM-yyyy HH:mm",
			"dd-MM-yyyy",
			"dd.MM.yyyy HH:mm:ss",
			"dd.MM.yyyy HH:mm",
			"dd.MM.yyyy",
			"HH:mm:ss",
			"HH:mm"
		};
		private const DateTimeStyles DateTimeParsingStyles = DateTimeStyles.AllowWhiteSpaces;
		public static DateTime ParseDateTime(string s)
		{
			return DateTime.ParseExact(s?.Trim(), ExactDateTimeFormats, CultureInfo.InvariantCulture, DateTimeParsingStyles);
		}
		public static bool TryParseDateTime(string s, out DateTime result)
		{
			return DateTime.TryParseExact(s?.Trim(), ExactDateTimeFormats, CultureInfo.InvariantCulture, DateTimeParsingStyles, out result);
		}
		public static DateTime? GetDateTime(string text)
		{
			var parsedVal = default(DateTime);
			return TryParseDateTime(text?.Trim(), out parsedVal)
				? parsedVal
				: (DateTime?)null;
		}
		public static bool TryParseUtcDateTime(string s, string format, out DateTime result)
		{
			return DateTime.TryParseExact(
				s?.Trim(),
				format, 
				CultureInfo.InvariantCulture, 
				DateTimeParsingStyles | DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal, 
				out result);
		}
		public static DateTime GetUtcTime(DateTime time, TimeZoneInfo timeZoneInfo)
		{
			if (DateTimeKind.Utc != time.Kind)
				return TimeZoneInfo.ConvertTimeToUtc(time, timeZoneInfo);
			return time;
		}
		/// <summary> Получает список дат (время 00:00) от момента dateFrom до dateTo 
		/// с шагом в одни календарные сутки по локальному времени + конец последнего в цепочке дня (23:59)</summary>
		public static List<DateTime> GetDayList(DateTime dateFrom, DateTime dateTo)
		{
			var totalDays = (int)(dateTo - dateFrom).TotalDays;
			if (totalDays < 0)
				throw new ArgumentException("dateFrom " + dateFrom + " cannot be greater than dateTo " + dateTo);

			var result = new List<DateTime>(totalDays + 1);

			for (var currentLocalDate = dateFrom.Date;
				 currentLocalDate <= dateTo.Date;
				 currentLocalDate = currentLocalDate.AddDays(1))
			{
				result.Add(currentLocalDate);
			}

			return result;
		}
		public static string GetTimeSpanReadableShort(TimeSpan timeSpan)
		{
			return GetTimeSpanReadableShort(timeSpan, CultureInfo.CurrentCulture);
		}
		public static string GetTimeSpanReadableShort(TimeSpan timeSpan, CultureInfo cultureInfo)
		{
			return timeSpan.ToString();
		}

		public static readonly string FileCompatibleTimeFormat     = "dd-MM-yyyy_HH-mm-ss";
		public static readonly string DefaultTimeFormat            = "dd.MM.yyyy HH:mm:ss";
		public static readonly string DefaultTimeFormatUpToMinutes = "dd.MM.yyyy HH:mm";
		public static readonly string DefaultTimeFormatUpToDays    = "dd.MM.yyyy";
		public static readonly string ExcelTimeFormat              = "yyyy-MM-ddTHH:mm:ss.FFF";
		public static readonly string NotificationTimeFormat       = "HH:mm dd.MM.yy";

		public static string ToDaylessString(this TimeSpan timeSpan)
		{
			if (timeSpan < TimeSpan.Zero)
				throw new ArgumentOutOfRangeException("timeSpan", timeSpan, "Value cannot be less then TimeSpan.Zero");

			var hours = (int)timeSpan.TotalHours;
			var minutes = timeSpan.Minutes;
			var seconds = timeSpan.Seconds;

			return $"{hours:00}:{minutes:00}:{seconds:00}";
		}
		public static string ToXmlFormat(DateTime dateTime)
		{
			return XmlConvert.ToString(dateTime, XmlDateTimeSerializationMode.Utc);
		}
		public static DateTime? FromXmlFormat(string s)
		{
			try
			{
				return XmlConvert.ToDateTime(s, XmlDateTimeSerializationMode.Utc);
			}
			catch
			{
				return default(DateTime?);
			}
		}
	}
}