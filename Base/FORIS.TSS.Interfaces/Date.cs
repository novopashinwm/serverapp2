using System;

namespace FORIS.TSS.BusinessLogic
{
	[Serializable]
	public struct Date: IComparable
	{
		private DateTime value;

		public int Month
		{
			get { return this.value.Month; }
		}

		public int Year
		{
			get { return this.value.Year; }
		}

		public int Day
		{
			get { return this.value.Day; }
		}

		public Date( DateTime value )
		{
			this.value = value.Date;
		}

		public Date AddDays( int days )
		{
			return new Date( this.value.AddDays( days ) );
		}
		public Date AddMonths( int months )
		{
			return new Date( this.value.AddMonths( months ) );
		}

		public DateTime AddHours( int hours )
		{
			return ( (DateTime)this ).AddHours( hours );
		}
		public DateTime AddMinutes( int minutes )
		{
			return ( (DateTime)this ).AddMinutes( minutes );
		}
		public DateTime AddSeconds( int seconds )
		{
			return ( (DateTime)this ).AddSeconds( seconds );
		}

		public DateTime ToUniversalTime()
		{
			return ( (DateTime)this ).ToUniversalTime();
		}

		#region IComparable members

		public int CompareTo( object value )
		{
			Date Date = (Date)value;

			if( this > Date )
			{
				return +1;
			}
			else if( this < Date )
			{
				return -1;
			}
			else
				return 0;
		}

		#endregion // IComparable members

		public static explicit operator Date( DateTime value )
		{
			return new Date( value );
		}
		public static implicit operator DateTime( Date value )
		{
			return value.value;
		}


		public override bool Equals( object obj )
		{
			return this.value.Equals( ( (Date)obj ).value );
		}
		public override int GetHashCode()
		{
			return this.value.GetHashCode();
		}

		public string ToLongDateString()
		{
			return this.value.ToLongDateString();
		}
		public string ToShortDateString()
		{
			return this.value.ToShortDateString();
		}

		public override string ToString()
		{
			return this.value.ToLongDateString();
		}

		public static Date Today
		{
			get { return new Date( DateTime.Today ); }
		}

		public static bool operator ==( Date date1, Date date2 )
		{
			return Date.Equals( date1, date2 );
		}
		public static bool operator !=( Date date1, Date date2 )
		{
			return !( date1 == date2 );
		}

		public static DateSpan operator -( Date date1, Date date2 )
		{
			return new DateSpan( ( (DateTime)date1 - (DateTime)date2 ).Days );
		}
		public static DateTime operator +( Date date, TimeSpan time )
		{
			return date.value + time;
		}

		public static bool operator >( Date date1, Date date2 )
		{
			return (DateTime)date1 > (DateTime)date2;
		}
		public static bool operator <( Date date1, Date date2 )
		{
			return (DateTime)date1 < (DateTime)date2;
		}

		public static bool operator >=( Date date1, Date date2 )
		{
			return (DateTime)date1 >= (DateTime)date2;
		}
		public static bool operator <=( Date date1, Date date2 )
		{
			return (DateTime)date1 <= (DateTime)date2;
		}
	}

	public struct DateSpan
	{
		private int days;

		public int Days
		{
			get { return this.days; }
		}

		public DateSpan( int days )
		{
			this.days = days;
		}
	}
}
