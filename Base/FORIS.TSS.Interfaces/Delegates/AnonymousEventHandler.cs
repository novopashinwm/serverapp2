﻿using System;

namespace FORIS.TSS.Common
{
	public delegate void AnonymousEventHandler<TEventArgs>(TEventArgs args) where TEventArgs : EventArgs;
}