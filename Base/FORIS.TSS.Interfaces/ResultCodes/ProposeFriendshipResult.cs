﻿using System;

namespace FORIS.TSS.BusinessLogic.ResultCodes
{
	/// <summary> Результат попытки отправить приглашение другу </summary>
	[Serializable]
	public enum ProposeFriendshipResult
	{
		/// <summary> Приглашение отправлено </summary>
		InvitationSent = 1,
		/// <summary> Возвращается при попытке отправить приглашение дружбы самому себе </summary>
		UnableToInviteSelf = 3,
		/// <summary> Некорректный номер телефона </summary>
		InvalidMsisdn = 4,
		/// <summary> Оператор связи, обслуживающий указанный номер, не поддерживается системой </summary>
		UnsupportedNetworkProvider = 5,
		/// <summary>Не указан собственный номер телефона пользователя - друг не сможет идентифицировать пользователя, запрашивающего доступ</summary>
		OwnMsisdnIsNotSpecified = 6,
		/// <summary> Уже имеется доступ </summary>
		AlreadyHasAccess = 7,
		/// <summary> Приглашение отправлено, но нужно отправить ссылку для друга </summary>
		InvitationRegisteredAndWaitingForAppOnFriendDevice = 8
	}
}