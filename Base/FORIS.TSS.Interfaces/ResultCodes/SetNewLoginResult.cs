﻿namespace FORIS.TSS.BusinessLogic.Server
{
	/// <summary> Результат установки нового логина </summary>
	public enum SetNewLoginResult
	{
		/// <summary> Логин успешно изменен </summary>
		Success,
		/// <summary> Некорректное значение логина </summary>
		InvalidLogin,
		/// <summary> Такой логин уже существует </summary>
		LoginAlreadyExists,
		/// <summary> Запрещено </summary>
		Forbidden,
		/// <summary> Адрес уже назначен другому пользователю</summary>
		EmailBelongsToAnotherOperator,
		/// <summary> Неверный формат адреса электронной почты</summary>
		InvalidEmailFormat,
		/// <summary> Логин, по структуре похожий на email, не совпадает с email оператора </summary>
		LoginAsEmailDoesNotMatchToEmail
	}
}