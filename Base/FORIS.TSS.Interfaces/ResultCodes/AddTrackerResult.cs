﻿using System;

namespace FORIS.TSS.BusinessLogic.ResultCodes
{
	/// <summary> Результат добавления нового трекера пользователю </summary>
	[Serializable]
	public enum AddTrackerResult
	{
		/// <summary> Неверный номер трекера</summary>
		InvalidNumber               =  1,
		/// <summary> Неверный контрольный код</summary>
		InvalidControlCode          =  2,
		/// <summary> Запрещено</summary>
		Forbidden                   =  3,
		/// <summary> Успешно </summary>
		Success                     =  4,
		/// <summary> Ключ подтверждения неверный или устарел</summary>
		InvalidConfirmationKey      =  5,
		/// <summary> Нет такого оператора</summary>
		NoOperator                  =  6,
		/// <remarks> Общая ошибка для <see cname="AddTrackerResult.InvalidNumber" /> и <see cname="AddTrackerResult.InvalidControlCode" /> </remarks>
		InvalidNumberOrControlCode  =  7,
		/// <remarks> Превышено максимальное число попыток </remarks>
		MaxAttemptsExceeded         =  8,
		/// <summary>Недостаточно услуг в МТС</summary>
		InsufficientBillingServices =  9,
		/// <summary> Такой идентификатор терминала уже существует </summary>
		DeviceIDAlreadyExists       = 10,
		/// <summary> Номер телефона уже существует </summary>
		PhoneAlreadyExists          = 11,
		/// <summary> Некорректный номер телефона </summary>
		InvalidPhoneNumber          = 12
	}
}