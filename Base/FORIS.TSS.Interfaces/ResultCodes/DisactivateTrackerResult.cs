﻿using System;

namespace FORIS.TSS.BusinessLogic.ResultCodes
{
	/// <summary>Результат добавления нового трекера пользователю</summary>
	[Serializable]
	public enum DisactivateTrackerResult
	{
		/// <summary>Успешно</summary>
		Success = 0,
		/// <summary>Трекер не может быть активирован по номеру и коду</summary>
		TrackerIsNotActivatable = 1,
		/// <summary>Нет прав на трекер</summary>
		Forbidden = 3,
		
	}
}