﻿namespace FORIS.TSS.BusinessLogic.ResultCodes
{
	public enum SetAccessToVehicleResult
	{
		/// <summary> Доступ предоставлен </summary>
		AccessGranted                               = 1,
		/// <summary> Доступ предоставлен, требуется установка приложения </summary>
		AccessGrantedAndWaitingForAppOnFriendDevice = 8,
		/// <summary> Оператор связи, обслуживающий указанный номер, не поддерживается системой </summary>
		UnsupportedNetworkProvider                  = 5,
	}
}