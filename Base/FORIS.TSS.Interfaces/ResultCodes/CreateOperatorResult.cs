﻿namespace FORIS.TSS.BusinessLogic.Server
{
	public enum CreateOperatorResult
	{
		/// <summary> Логин успешно изменен </summary>
		Success,
		/// <summary> Некорректное значение логина </summary>
		InvalidLogin,
		/// <summary> Некорректное значение пароля </summary>
		InvalidPassword,
		/// <summary> Такой логин уже существует </summary>
		LoginAlreadyExists,
		/// <summary>Адрес электронной почты принадлежит другому оператору</summary>
		EmailBelongsToAnotherOperator,
		/// <summary> Такой телефонный номер уже зарегистрирован </summary>
		PhoneAlreadyRegistered,
		/// <summary> Логин как почта запрещен </summary>
		OperatorLoginNotAllowedAsEmail,
		/// <summary> Логин как телефон запрещен </summary>
		OperatorLoginNotAllowedAsPhone,
	}
}