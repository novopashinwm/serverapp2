﻿using System;

namespace FORIS.TSS.BusinessLogic.ResultCodes
{
	[Serializable]
	public enum CreateDepartmentResult
	{
		/// <summary> Успех </summary>
		Success                               = 0,
		/// <summary> Департамент уже существует </summary>
		DepartmentAlreadyExists               = 1,
		/// <summary> Логин уже существует </summary>
		DepartmentAdminAlreadyExists          = 2,
		/// <summary> Логин как почта, такая почта уже существует в контактах (более не проверяется, т.к. запрещено использование почты) </summary>
		DepartmentAdminEmailAlreadyExists     = 3,
		/// <summary> Логин как телефон, такой телефон уже существует в контактах (более не проверяется, т.к. запрещено использование телефона) </summary>
		DepartmentAdminPhoneAlreadyExists     = 4,
		/// <summary> Логин как почта запрещен </summary>
		DepartmentAdminLoginNotAllowedAsEmail = 5,
		/// <summary> Логин как телефон запрещен </summary>
		DepartmentAdminLoginNotAllowedAsPhone = 6,
	}
}