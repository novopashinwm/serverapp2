﻿namespace FORIS.TSS.BusinessLogic.ResultCodes
{
	/// <summary> Результат работы по разрешению/запрету видеть местоположение другому пользователю </summary>
	public enum ShareTrackerResult
	{
		Forbidden            = 1,
		UnknownTracker       = 2,
		Success              = 3,
		UnknownOperator      = 4,
		UnknownOtherOperator = 5
	}
}