﻿using System;

namespace FORIS.TSS.BusinessLogic.ResultCodes
{
	[Serializable]
	public enum SetNewContactResult
	{
		/// <summary> Успешно: адрес изменен </summary>
		Success = -1, //Значение по-умолчанию не должно быть успешным
		/// <summary> Ошибка </summary>
		Failed = 0,
		/// <summary> Адрес не подтвержден: код подтверждения неверен или устарел </summary>
		NotConfirmed = 1,
		/// <summary> Адрес уже назначен другому пользователю </summary>
		BelongsToAnotherOperator = 2,
		/// <summary> Неверный формат контакта </summary>
		InvalidFormat = 3,
		/// <summary> Ожидается подтверждение адреса </summary>
		WaitingForConfirmation = 4,
		/// <summary> Адрес электронной почты остался прежним </summary>
		RemainsTheSame = 5,
		/// <summary> Не удалось отправить сообщение для подтверждения </summary>
		UnableToSendConfirmationMessage = 6,
		/// <summary> Успешно удален Email </summary>
		SuccessfullyDeleted = 7,
		/// <summary> Произошла ошибка во время удаления </summary>
		ErrorDelete = 8,
		/// <summary> Неверный код подтверждения </summary>
		InvalidConfirmationCode = 9,
		/// <summary> Уже существует </summary>
		AlreadyExists = 10,
		/// <summary> Код подтверждения уже отправлен </summary>
		ConfirmationCodeIsAlreadySent = 11,
		/// <summary> Контакт нельзя удалять, т.к. он жёстко связан с маскированным номером </summary>
		ContactIsNotRemovable = 12,
		/// <summary> Контакт не существует </summary>
		ContactDoesNotExist = 13,
		/// <summary> У пользователя уже имеется подтверждённый контакт такого типа </summary>
		ConfirmedContactAlreadyExists = 14,
		/// <summary> Вышел срок жизни кода подтверждения </summary>
		ConfirmationCodeExpired = 15,
		/// <summary> Исчерпано количество попыток </summary>
		AttemptsQuantityIsExhausted = 16
	}
}