﻿namespace FORIS.TSS.BusinessLogic.ResultCodes
{
	public enum SchedulerExecutionResult
	{
		/// <summary> Успешно выполнено </summary>
		Success,
		/// <summary> Повторить через некоторое время </summary>
		Retry,
		/// <summary> Больше не выполнять задание </summary>
		Cancel,
		/// <summary> Отложить до перезагрузки </summary>
		WaitForReload
	}
}