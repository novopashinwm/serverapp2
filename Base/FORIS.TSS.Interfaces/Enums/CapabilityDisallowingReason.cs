﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> причина запрета использования услуги </summary>
	[Serializable]
	public enum CapabilityDisallowingReason
	{
		/// <summary> На сим-карте не подключена услуга </summary>
		NoServiceOnVehicle,
		/// <summary> Сим-карта заблокирована </summary>
		VehicleIsBlocked,
		/// <summary> Нет прав </summary>
		Forbidden,
		/// <summary> На сим-карте оператора не подключена услуга </summary>
		NoServiceOnOperator,
		/// <summary> Сим-карта оператора заблокирована </summary>
		OperatorIsBlocked,
		/// <summary> Возможность определения местоположения приостановлена </summary>
		PositionAskingIsPaused,
		/// <summary> команда старта/остановки двигателя в процессе выполнения </summary>
		CommandInProcess,
		/// <summary>Неизвестный номер телефона</summary>
		/// <remarks>По этому объекту наблюдения нет ни одного маскированного номера телефона, чтобы определять местоположение по данным сотовой сети</remarks>
		UnknownPhoneNumber,
		/// <summary>Нужно ожидать, запрос станет доступен после <see cref="BillingServiceStatus.NextTime"/></summary>
		WaitRequired
	}
}