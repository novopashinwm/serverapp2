﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary>Тип норматива расхода топлива</summary>
	public enum FuelSpendStandardType
	{
		/// <summary>Норматив по пробегу: литров на километр</summary>
		Run,
		/// <summary>Норматив по моточасам: литров на моточас</summary>
		EngineHours
	}
}