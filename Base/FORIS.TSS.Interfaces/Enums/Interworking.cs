﻿namespace FORIS.TSS.BusinessLogic.Enums
{

    /// <summary>
    /// Перечисления, которые применяются в интерворкинге
    /// </summary>
    public sealed class Interworking
    {
        public enum Result
        {
            /// <summary>
            /// Успешное определение местоположения
            /// </summary>
            OK = 0,

            /// <summary>
            /// Ошибка определения местоположения
            /// </summary>
            SYSTEM_FAILURE = 1,

            /// <summary>
            /// Абонент не существует
            /// </summary>
            UNKNOWN_SUBSCRIBER = 4,

            /// <summary>
            /// Абонент недоступен
            /// </summary>
            ABSENT_SUBSCRIBER = 5,

            /// <summary>
            /// Ошибка определения местоположения
            /// </summary>
            DISALLOWED_BY_LOCAL_REGULATIONS = 204,

            /// <summary>
            /// Абонент заблокирован
            /// </summary>
            REMOTE_FACILITY_BARRED = 504,

            /// <summary>
            /// Абонент находится в метро
            /// </summary>
            TEXTUAL_WHEREABOUT = 505,

            /// <summary>
            /// Абонент находится в роуминге
            /// </summary>
            ROAMING = 510,

            /// <summary>
            /// Прочие ошибки
            /// </summary>
            OTHER = 556,

            /// <summary>
            /// Нет разрешения на позиционирование
            /// </summary>
            POSITIONING_NOT_ALLOWED = 590,

            /// <summary>
            /// Превышено количество запросов на определяющего абонента, определяемого абонента в рамках данной Услуги за календарный месяц
            /// </summary>
            MONTHLY_LIMIT_EXCEEDED = 591,

            /// <summary>
            /// Превышено количество запросов на определяющего абонента, определяемого абонента в рамках данной Услуги за календарный месяц
            /// </summary>
            MONTHLY_LIMIT_EXCEEDED_EX = 592
        }
     
        public sealed class Mts
        {
            public enum Services
            {
                /// <summary>
                /// Локатор
                /// </summary>
                Locator = 1, 
                /// <summary>
                /// Мобильные Сотрудники
                /// </summary>
                MobileEmployees = 2,             
                
                /// <summary>
                /// Ребенок под присмотром
                /// </summary>
                Babysiyting = 11,

                /// <summary>
                /// Ufin
                /// </summary>
                Nika = 36,                
            }
        }

        public sealed class Megafon
        {      
            public enum Service
            {
                /// <summary>
                /// Навигатор
                /// </summary>
                Navigator = 37,
                
                /// <summary>
                /// Радар
                /// </summary>
                Radar = 38,

                /// <summary>
                /// Контроль кадров
                /// </summary>
                EmployeesControl = 39,

                /// <summary>
                /// Контроль автопарка
                /// </summary>
                FleetControl = 40
            }
        }
    }
}
