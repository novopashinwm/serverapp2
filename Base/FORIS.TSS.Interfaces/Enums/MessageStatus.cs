﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	public enum MessageStatus
	{
		Unknown  = 0,
		Received = 1,
		Read     = 2,
	}
}