﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	public enum ChangeType
	{
		Insert,
		Update,
		Delete,
	}
}