﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	[Serializable]
	public enum CmdStatus
	{
		/// <summary> Команда получена </summary>
		Received   = 0,
		/// <summary> Обрабатывается </summary>
		Processing = 1,
		/// <summary> Выполнена </summary>
		Completed  = 2,
	}
}