﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Состояние объекта наблюдения </summary>
	[Flags]
	public enum Status : int
	{
		/// <summary> Обычное или неопределенное состояние </summary>
		None             = 0x0000,
		/// <summary> Тревога (аварийная кнопка SOS) </summary>
		Alarm            = 0x0001,
		/// <summary> Припаркован, но движется (эвакуация или угон) </summary>
		ParcingMoving    = 0x0002,
		/// <summary> Парковка </summary>
		Parcing          = 0x0004,
		/// <summary> Статус скорой - "Свободен" </summary>
		AmbulanceFree    = 0x0008,
		/// <summary> Статус скорой - "На вызов" </summary>
		AmbulanceToOrder = 0x0010,
		/// <summary> Статус скорой - "На вызове" </summary>
		AmbulanceOrder   = 0x0020,
		/// <summary> Ремонт </summary>
		Repair           = 0x0040,
	}
}