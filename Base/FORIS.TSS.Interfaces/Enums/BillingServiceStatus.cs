﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Статус оказания услуги <see cref="RenderedServiceType"/> </summary>
	[Serializable]
	public class BillingServiceStatus
	{
		/// <summary> Оставшееся количество услуг </summary>
		public int?      QuantityLeft;
		/// <summary> Дата последнего оказания услуги </summary>
		public DateTime? LastTime;
		/// <summary> Дата, когда возможно следующее оказание услуги </summary>
		public DateTime? NextTime;
	}
}