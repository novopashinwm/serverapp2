﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Перечисление единиц измерения </summary>
	public enum UnitOfMeasure : short
	{
		/// <summary> Безразмерная величина </summary>
		None                           = 000,
		/// <summary> Километров на литр </summary>
		KmPerLitre                     = 001,
		/// <summary> Литров на 100 км </summary>
		LitrePer100Km                  = 002,
		/// <summary> Километров на м3 </summary>
		KmPerM3                        = 003,
		/// <summary> Литров на км </summary>
		LitrePerKm                     = 004,
		/// <summary> м3 на моточас </summary>
		M3PerMotohour                  = 005,
		/// <summary> л/ч </summary>
		LitrePerHour                   = 006,
		/// <summary> Литры </summary>
		Litre                          = 011,
		/// <summary> Кубические метры </summary>
		Cbm                            = 012,
		/// <summary> Вольты </summary>
		Volt                           = 021,
		/// <summary> Ваты </summary>
		Watt                           = 022,
		/// <summary> Амперы </summary>
		Ampere                         = 023,
		/// <summary> Градусы Цельсия </summary>
		Celsius                        = 031,
		/// <summary> Миллисекунды </summary>
		Millisecond                    = 041,
		/// <summary> Секунды </summary>
		Seconds                        = 042,
		/// <summary> Часы </summary>
		Hours                          = 043,
		/// <summary> Метры </summary>
		Meters                         = 051,
		/// <summary> Километры </summary>
		Kilometers                     = 052,
		/// <summary> 10 Километров </summary>
		TenKilometers                  = 053,
		/// <summary> 100 Километров </summary>
		Kilometers100                  = 054,
		/// <summary> Проценты </summary>
		Percentage                     = 061,
		/// <summary> Штуки </summary>
		Thing                          = 062,
		/// <summary> Количество </summary>
		Quantity                       = 063,
		/// <summary> Км/ч </summary>
		Kmph                           = 071,
		/// <summary> М/с </summary>
		Mps                            = 072,
		/// <summary> Оборотов в минуту </summary>
		RpmMinute                      = 073,
		/// <summary> кВт*ч </summary>
		KiloWattPerHours               = 081,
		/// <summary> Килограмм </summary>
		Kilogram                       = 082,
		/// <summary> Отн. Влажность в процентах </summary>
		RelHumidity                    = 083,
		/// <summary> Освещенность, Люкс https://ru.wikipedia.org/wiki/Люкс </summary>
		Lux                            = 084,
		/// <summary> мм </summary>
		Millimeter                     = 085,
		/// <summary> кПа </summary>
		Kilopascal                     = 086,
		/// <summary> мин </summary>
		Minute                         = 087,
		/// <summary> м² </summary>
		SquareMeter                    = 088,
		/// <summary> м²/ч </summary>
		SquareMetersPerHour            = 089,
		/// <summary> Баллы </summary>
		Score                          = 090,
		/// <summary> Тысячная доля ускорения свободного падения </summary>
		MilliGravitationalAcceleration = 091,
	}
}