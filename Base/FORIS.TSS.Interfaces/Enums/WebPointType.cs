﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	public enum WebPointType
	{
		/// <summary> Точка установленная пользователем (точка интереса) </summary>
		UserPoint     = 1,
		/// <summary> Точка экстренного вызова </summary>
		EmergencyCall = 2
	}
}