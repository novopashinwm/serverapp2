﻿using System;
using FORIS.TSS.BusinessLogic.Attributes;

namespace FORIS.TSS.BusinessLogic.Enums
{
	[Serializable]
	public enum SensorLegend
	{
		/// <summary> Напряжение питания </summary>
		[Measure(UnitOfMeasure.Volt)]
		PowerVoltage                       = 001,
		/// <summary> Датчик топлива </summary>
		[Measure(UnitOfMeasure.Litre)]
		Fuel                               = 002,
		/// <summary> Температура в салоне </summary>
		[Measure(UnitOfMeasure.Celsius)]
		TemperatureInCabin                 = 003,
		/// <summary> Зажигание </summary>
		Ignition                           = 004,
		/// <summary> Цифровой датчик температуры </summary>
		[Measure(UnitOfMeasure.Celsius)]
		DigitalTemperatureSensor           = 005,
		/// <summary> Тревога </summary>
		Alarm                              = 006,
		/// <summary> Значение уровня топлива в литрах согласно шине CAN </summary>
		[Measure(UnitOfMeasure.Litre)]
		CANFuel                            = 007,
		/// <summary> Значение общего пробега согласно шине CAN </summary>
		[Measure(UnitOfMeasure.Kilometers)]
		CANTotalRun                        = 008,
		/// <summary> Основное питание </summary>
		MainBattery                        = 009,
		/// <summary> Кондиционер вкл/выкл </summary>
		AirConditioner                     = 010,
		/// <summary> Уровень заряда батареи </summary>
		[Measure(UnitOfMeasure.Percentage)]
		BatteryLevel                       = 011,
		/// <summary> Автомобильная сигнализация </summary>
		CarAlarm                           = 012,
		/// <summary> Напряжение на аккумуляторе </summary>
		[Measure(UnitOfMeasure.Volt)]
		AccumulatorVoltage                 = 013,
		/// <summary> GPS включен </summary>
		GpsEnabled                         = 014,
		/// <summary> Подключен внешний источник питания </summary>
		IsPlugged                          = 015,
		/// <summary> Сканируется радио </summary>
		RadioScannerEnabled                = 016,
		/// <summary> Идет отправка данных </summary>
		DataSenderEnabled                  = 017,
		/// <summary> Объект движется </summary>
		IsMoving                           = 018,
		/// <summary> Аналоговый датчик 1 </summary>
		[Measure(UnitOfMeasure.None)]
		AnalogSensor1                      = 021,
		/// <summary> Аналоговый датчик 2 </summary>
		[Measure(UnitOfMeasure.None)]
		AnalogSensor2                      = 022,
		/// <summary> Аналоговый датчик 3 </summary>
		[Measure(UnitOfMeasure.None)]
		AnalogSensor3                      = 023,
		/// <summary> Аналоговый датчик 4 </summary>
		[Measure(UnitOfMeasure.None)]
		AnalogSensor4                      = 024,
		/// <summary> Аналоговый датчик 5 </summary>
		[Measure(UnitOfMeasure.None)]
		AnalogSensor5                      = 025,
		/// <summary> Аналоговый датчик 6 </summary>
		[Measure(UnitOfMeasure.None)]
		AnalogSensor6                      = 026,
		/// <summary> Аналоговый датчик 7 </summary>
		[Measure(UnitOfMeasure.None)]
		AnalogSensor7                      = 027,
		/// <summary> Аналоговый датчик 8 </summary>
		[Measure(UnitOfMeasure.None)]
		AnalogSensor8                      = 028,
		/// <summary> Цифровой датчик 1 </summary>
		DigitalSensor1                     = 031,
		/// <summary> Цифровой датчик 2 </summary>
		DigitalSensor2                     = 032,
		/// <summary> Цифровой датчик 3 </summary>
		DigitalSensor3                     = 033,
		/// <summary> Цифровой датчик 4 </summary>
		DigitalSensor4                     = 034,
		/// <summary> Цифровой датчик 5 </summary>
		DigitalSensor5                     = 035,
		/// <summary> Цифровой датчик 6 </summary>
		DigitalSensor6                     = 036,
		/// <summary> Цифровой датчик 7 </summary>
		DigitalSensor7                     = 037,
		/// <summary> Цифровой датчик 8 </summary>
		DigitalSensor8                     = 038,
		/// <summary> Виртуальный одометр </summary>
		[Measure(UnitOfMeasure.Kilometers)]
		VirtualOdometer                    = 039,
		/// <summary> Потребленная мощность, кВт*ч </summary>
		[Measure(UnitOfMeasure.KiloWattPerHours)]
		ConsumedPower                      = 040,
		/// <summary> Мощность, Вт </summary>
		[Measure(UnitOfMeasure.Watt)]
		Power                              = 041,
		/// <summary> Ток, А</summary>
		[Measure(UnitOfMeasure.Ampere)]
		Current                            = 042,
		/// <summary> Статус реле, вкл/выкл </summary>
		RelayStatus                        = 043,
		/// <summary> Количество вскрытий кожуха </summary>
		[Measure(UnitOfMeasure.Quantity)]
		CoverOpeningTimes                  = 044,
		/// <summary> Период записи, с </summary>
		[Measure(UnitOfMeasure.Seconds)]
		HeartBeatInterval                  = 045,
		/// <summary> Статус дверей </summary>
		Door                               = 046,
		/// <summary> Фары </summary>
		HeadLights                         = 047,
		/// <summary> Столкновение </summary>
		Accident                           = 048,
		/// <summary> Поднят </summary>
		Up                                 = 049,
		/// <summary> Опущен </summary>
		Down                               = 050,
		/// <summary> Метка </summary>
		Mark                               = 051,
		/// <summary> Номер метки </summary>
		MarkNumber                         = 052,
		/// <summary> Режим перемешивания бетона </summary>
		ConcreteMixing                     = 053,
		/// <summary> Режим слива бетона </summary>
		ConcreteDumping                    = 054,
		/// <summary> Детекция движения </summary>
		MotionAlarm                        = 055,
		/// <summary> Пересечение линии </summary>
		LineCrossed                        = 056,
		/// <summary> Детекция лица </summary>
		FaceDetected                       = 057,
		/// <summary> Оставлен предмет </summary>
		AbandonedAlarm                     = 058,
		/// <summary> Праздношатание </summary>
		LoiteringAlarm                     = 059,
		/// <summary> Всего пассажиров вошло </summary>
		[Measure(UnitOfMeasure.Quantity)]
		PassengersInput                    = 060,
		/// <summary> Всего пассажиров вышло </summary>
		[Measure(UnitOfMeasure.Quantity)]
		PassengersOutput                   = 061,
		/// <summary> Количество пассажиров в транспорте </summary>
		[Measure(UnitOfMeasure.Quantity)]
		PassengersCount                    = 062,
		/// <summary> Крановая установка (дискретный: вкл/выкл) </summary>
		Crane                              = 063,
		/// <summary> Отслеживание местоположения </summary>
		PositionTracking                   = 064,
		/// <summary> Наличие подключение к сети Интернет </summary>
		NetworkConnected                   = 065,
		/// <summary> Источник местоположения GPS </summary>
		GPSLocationProvider                = 066,
		/// <summary> Источник местоположения по данным сети </summary>
		NetworkLocationProvider            = 067,
		/// <summary> WiFi в настройках телефона </summary>
		WiFi                               = 068,
		/// <summary> Режим полёта </summary>
		AirplaneMode                       = 069,
		/// <summary> Кузов автомобиля </summary>
		/// <remarks> Например, для сыпучих грузов </remarks>
		TruckBody                          = 070,
		/// <summary> Тип используемого топлива: бензин / газ </summary>
		/// <remarks> В автомобилях, работающих на газу, есть переключатель "бензин/газ". </remarks>
		UsedFuelType                       = 071,
		/// <summary> АКБ терминала заряжается (Да/Нет) </summary>
		DeviceBatteryCharging              = 072,
		/// <summary> Питание от встроенной АКБ (Да/Нет) </summary>
		PoweredByInternalBattery           = 073,
		/// <summary> Статус двери 1 </summary>
		Door1                              = 081,
		/// <summary> Дверь 1, пассажиропоток, Вошло </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door1Entered                       = 0811,
		/// <summary> Дверь 1, пассажиропоток, Вышло </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door1Leaved                        = 0812,
		/// <summary> Дверь 1, пассажиропоток, включена/выключена </summary>
		Door1Offline                       = 0813,
		/// <summary> Дверь 1, пассажиропоток, выявлен саботаж </summary>
		Door1Sabotage                      = 0814,
		/// <summary> Дверь 1, пассажиропоток, Вошло за сегодня </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door1EnteredToday                  = 0815,
		/// <summary> Дверь 1, пассажиропоток, Вышло за сегодня </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door1LeavedToday                   = 0816,
		/// <summary> Статус двери 2 </summary>
		Door2                              = 082,
		/// <summary> Дверь 2, пассажиропоток, Вошло </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door2Entered                       = 0821,
		/// <summary> Дверь 2, пассажиропоток, Вышло </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door2Leaved                        = 0822,
		/// <summary> Дверь 2, пассажиропоток, включена/выключена </summary>
		Door2Offline                       = 0823,
		/// <summary> Дверь 2, пассажиропоток, выявлен саботаж </summary>
		Door2Sabotage                      = 0824,
		/// <summary> Дверь 2, пассажиропоток, Вошло за сегодня </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door2EnteredToday                  = 0825,
		/// <summary> Дверь 2, пассажиропоток, Вышло за сегодня </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door2LeavedToday                   = 0826,
		/// <summary> Статус двери 3 </summary>
		Door3                              = 083,
		/// <summary> Дверь 3, пассажиропоток, Вошло </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door3Entered                       = 0831,
		/// <summary> Дверь 3, пассажиропоток, Вышло </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door3Leaved                        = 0832,
		/// <summary> Дверь 3, пассажиропоток, включена/выключена </summary>
		Door3Offline                       = 0833,
		/// <summary> Дверь 3, пассажиропоток, выявлен саботаж </summary>
		Door3Sabotage                      = 0834,
		/// <summary> Дверь 3, пассажиропоток, Вошло за сегодня </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door3EnteredToday                  = 0835,
		/// <summary> Дверь 3, пассажиропоток, Вышло за сегодня </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door3LeavedToday                   = 0836,
		/// <summary> Статус двери 4 </summary>
		Door4                              = 084,
		/// <summary> Дверь 4, пассажиропоток, Вошло </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door4Entered                       = 0841,
		/// <summary> Дверь 4, пассажиропоток, Вышло </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door4Leaved                        = 0842,
		/// <summary> Дверь 4, пассажиропоток, включена/выключена </summary>
		Door4Offline                       = 0843,
		/// <summary> Дверь 4, пассажиропоток, выявлен саботаж </summary>
		Door4Sabotage                      = 0844,
		/// <summary> Дверь 4, пассажиропоток, Вошло за сегодня </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door4EnteredToday                  = 0845,
		/// <summary> Дверь 4, пассажиропоток, Вышло за сегодня </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door4LeavedToday                   = 0846,
		/// <summary> Статус двери 5 </summary>
		Door5                              = 085,
		/// <summary> Дверь 5, пассажиропоток, Вошло </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door5Entered                       = 0851,
		/// <summary> Дверь 5, пассажиропоток, Вышло </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door5Leaved                        = 0852,
		/// <summary> Дверь 5, пассажиропоток, включена/выключена </summary>
		Door5Offline                       = 0853,
		/// <summary> Дверь 5, пассажиропоток, выявлен саботаж </summary>
		Door5Sabotage                      = 0854,
		/// <summary> Дверь 5, пассажиропоток, Вошло за сегодня </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door5EnteredToday                  = 0855,
		/// <summary> Дверь 5, пассажиропоток, Вышло за сегодня </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door5LeavedToday                   = 0856,
		/// <summary> Статус двери 6 </summary>
		Door6                              = 086,
		/// <summary> Дверь 6, пассажиропоток, Вошло </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door6Entered                       = 0861,
		/// <summary> Дверь 6, пассажиропоток, Вышло </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door6Leaved                        = 0862,
		/// <summary> Дверь 6, пассажиропоток, включена/выключена </summary>
		Door6Offline                       = 0863,
		/// <summary> Дверь 6, пассажиропоток, выявлен саботаж </summary>
		Door6Sabotage                      = 0864,
		/// <summary> Дверь 6, пассажиропоток, Вошло за сегодня </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door6EnteredToday                  = 0865,
		/// <summary> Дверь 6, пассажиропоток, Вышло за сегодня </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door6LeavedToday                   = 0866,
		/// <summary> Статус двери 7 </summary>
		Door7                              = 087,
		/// <summary> Дверь 7, пассажиропоток, Вошло </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door7Entered                       = 0871,
		/// <summary> Дверь 7, пассажиропоток, Вышло </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door7Leaved                        = 0872,
		/// <summary> Дверь 7, пассажиропоток, включена/выключена </summary>
		Door7Offline                       = 0873,
		/// <summary> Дверь 7, пассажиропоток, выявлен саботаж </summary>
		Door7Sabotage                      = 0874,
		/// <summary> Дверь 7, пассажиропоток, Вошло за сегодня </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door7EnteredToday                  = 0875,
		/// <summary> Дверь 7, пассажиропоток, Вышло за сегодня </summary>
		[Measure(UnitOfMeasure.Quantity)]
		Door7LeavedToday                   = 0876,
		/// <summary> Время в миллисекундах, затраченное на предыдущую пересылку данных от блока на сервер </summary>
		[Measure(UnitOfMeasure.Millisecond)]
		LastPublishingDurationMilliseconds = 100,
		/// <summary> Код источника ошибки </summary>
		ErrorSource                        = 101,
		/// <summary> Код ошибки </summary>
		ErrorCode	                       = 102,
		/// <summary> Время получения первой позиции от GPS-приемника </summary>
		[Measure(UnitOfMeasure.Seconds)]
		TimeToFirstGPSFix                  = 103,
		/// <summary> Скорость (CAN) </summary>
		[Measure(UnitOfMeasure.Kmph)]
		CAN_Speed                          = 200,
		/// <summary> Круиз контроль </summary>
		CAN_CruiseControl                  = 201,
		/// <summary> Тормоз </summary>
		CAN_Brake                          = 202,
		/// <summary> Стояночный тормоз </summary>
		CAN_ParkingBrake                   = 203,
		/// <summary> Сцепление </summary>
		CAN_Clutch                         = 204,
		/// <summary> Педаль газа </summary>
		[Measure(UnitOfMeasure.Percentage)]
		CAN_Accelerator                    = 205,
		[Measure(UnitOfMeasure.Litre)]
		CAN_FuelRate                       = 206,
		/// <summary> Значение уровня топлива бака 1 в промилле согласно шине CAN </summary>
		[Measure(UnitOfMeasure.Percentage)]
		CAN_FuelLevel1                     = 207,
		/// <summary> Значение уровня топлива бака 2 в промилле согласно шине CAN </summary>
		[Measure(UnitOfMeasure.Percentage)]
		CAN_FuelLevel2                     = 208,
		/// <summary> Значение уровня топлива бака 3 в промилле согласно шине CAN </summary>
		[Measure(UnitOfMeasure.Percentage)]
		CAN_FuelLevel3                     = 209,
		/// <summary> Значение уровня топлива бака 4 в промилле согласно шине CAN </summary>
		[Measure(UnitOfMeasure.Percentage)]
		CAN_FuelLevel4                     = 210,
		/// <summary> Значение уровня топлива бака 5 в промилле согласно шине CAN </summary>
		[Measure(UnitOfMeasure.Percentage)]
		CAN_FuelLevel5                     = 211,
		/// <summary> Значение уровня топлива бака 6 в промилле согласно шине CAN </summary>
		[Measure(UnitOfMeasure.Percentage)]
		CAN_FuelLevel6                     = 212,
		/// <summary> Обороты двигателя </summary>
		[Measure(UnitOfMeasure.RpmMinute)]
		CAN_Revs                           = 213,
		/// <summary> Пробег до ТО </summary>
		[Measure(UnitOfMeasure.Kilometers)]
		CAN_RunToCarMaintenance            = 214,
		/// <summary> Моточасы по данным CAN </summary>
		[Measure(UnitOfMeasure.Hours)]
		CAN_EngHours                       = 215,
		/// <summary> Температура охлаждающей жидкости </summary>
		[Measure(UnitOfMeasure.Celsius)]
		CAN_CoolantT                       = 216,
		/// <summary> Температура масла в двигателе </summary>
		[Measure(UnitOfMeasure.Celsius)]
		CAN_EngOilT                        = 217,
		/// <summary> Температура топлива </summary>
		[Measure(UnitOfMeasure.Celsius)]
		CAN_FuelT                          = 218,
		/// <summary> Общий пробег по данным CAN </summary>
		[Measure(UnitOfMeasure.Kilometers)]
		CAN_TotalRun                       = 219,
		/// <summary> Суточный пробег по данным CAN </summary>
		[Measure(UnitOfMeasure.Kilometers)]
		CAN_DayRun                         = 220,
		/// <summary> Нагрузка на ось 1 </summary>
		[Measure(UnitOfMeasure.Kilogram)]
		CAN_AxleLoad1                      = 221,
		/// <summary> Нагрузка на ось 2 </summary>
		[Measure(UnitOfMeasure.Kilogram)]
		CAN_AxleLoad2                      = 222,
		/// <summary> Нагрузка на ось 3 </summary>
		[Measure(UnitOfMeasure.Kilogram)]
		CAN_AxleLoad3                      = 223,
		/// <summary> Нагрузка на ось 4 </summary>
		[Measure(UnitOfMeasure.Kilogram)]
		CAN_AxleLoad4                      = 224,
		/// <summary> Нагрузка на ось 5 </summary>
		[Measure(UnitOfMeasure.Kilogram)]
		CAN_AxleLoad5                      = 225,
		/// <summary> Нагрузка на ось 6 </summary>
		[Measure(UnitOfMeasure.Kilogram)]
		CAN_AxleLoad6                      = 226,
		/// <summary> Расход топлива с начала эксплуатации авто </summary>
		[Measure(UnitOfMeasure.Litre)]
		CAN_TotalFuelSpend                 = 227,
		/// <summary> Удельный расход топлива </summary>
		[Measure(UnitOfMeasure.LitrePerHour)]
		CAN_BrakeSpecificFuelConsumption   = 228,
		/// <summary> Режим работы устройства </summary>
		ControllerMode                     = 229,
		/// <summary> HDOP </summary>
		// HDOP                            = 230, Находится ниже со всеми DOPs
		/// <summary> Вышка (опущена / поднята) </summary>
		Tower                              = 231,
		/// <summary> Падение </summary>
		FallDown                           = 232,
		/// <summary> Движение </summary>
		Motion                             = 233,
		/// <summary> Перемещение </summary>
		Movement                           = 234,
		/// <summary> Иммобилизация - блокировка двигателя автомобиля </summary>
		Immobilized                        = 235,
		/// <summary> Уровень сигнала GSM </summary>
		[Measure(UnitOfMeasure.Percentage)]
		GsmSignalLevel                     = 236,
		/// <summary> Геолокация - включен ли модуль определения местоположения на устройстве </summary>
		LocationService                    = 237,
		/// <summary> Дополнительное оборудование автомобиля, например, дизель-генератор </summary>
		AdditionalEquipment                = 238,
		/// <summary> Работа двигателя: холостой ход/под нагрузкой </summary>
		EngineMode                         = 239,
		/// <summary> Цифровой выход 1 </summary>
		DigitalOutput1                     = 241,
		/// <summary> Цифровой выход 2 </summary>
		DigitalOutput2                     = 242,
		/// <summary> Цифровой выход 3 </summary>
		DigitalOutput3                     = 243,
		/// <summary> Режим "Невидимка" </summary>
		InvisibleMode                      = 250,
		WeakBlow                           = 251,
		StrongBlow                         = 252,
		Inclination                        = 253,
		#region DOPs (Dilution of Precision) — снижение точности
		/// <summary> HDOP (Horizontal Dilution of Precision) — снижение точности в горизонтальной плоскости </summary>
		/// <see cref="https://ru.wikipedia.org/wiki/DOP"/>
		[Measure(UnitOfMeasure.None)]
		HDOP                               = 230,
		/// <summary> VDOP (Vertical Dilution of Precision) — снижение точности в вертикальной плоскости </summary>
		/// <see cref="https://ru.wikipedia.org/wiki/DOP"/>
		[Measure(UnitOfMeasure.None)]
		VDOP                               = 254,
		/// <summary> PDOP (Position Dilution of Precision) — снижение точности по местоположению </summary>
		/// <remarks>PDOP^2 = HDOP^2 + VDOP^2</remarks>
		/// <see cref="https://ru.wikipedia.org/wiki/DOP"/>
		[Measure(UnitOfMeasure.None)]
		PDOP                               = 255,
		/// <summary> TDOP (Time Dilution of Precision) — снижение точности по времени </summary>
		/// <see cref="https://ru.wikipedia.org/wiki/DOP"/>
		[Measure(UnitOfMeasure.None)]
		TDOP                               = 256,
		/// <summary> GDOP (Geometric Dilution of Precision) — суммарное геометрическое снижение точности по местоположению и времени </summary>
		/// <remarks>GDOP^2 = PDOP^2 + TDOP^2</remarks>
		/// <see cref="https://ru.wikipedia.org/wiki/DOP"/>
		[Measure(UnitOfMeasure.None)]
		GDOP                               = 257,
		#endregion DOPs (Dilution of Precision) — снижение точности
		/// <summary> Температура 1 </summary>
		[Measure(UnitOfMeasure.Celsius)]
		TemperatureSensor1                 = 261,
		/// <summary> Температура 2 </summary>
		[Measure(UnitOfMeasure.Celsius)]
		TemperatureSensor2                 = 262,
		/// <summary> Температура 3 </summary>
		[Measure(UnitOfMeasure.Celsius)]
		TemperatureSensor3                 = 263,
		/// <summary> Температура 4 </summary>
		[Measure(UnitOfMeasure.Celsius)]
		TemperatureSensor4                 = 264,
		/// <summary> Уровень заряда батареи датчика 1 </summary>
		[Measure(UnitOfMeasure.Percentage)]
		BatteryExternalSensor1             = 271,
		/// <summary> Уровень заряда батареи датчика 2 </summary>
		[Measure(UnitOfMeasure.Percentage)]
		BatteryExternalSensor2             = 272,
		/// <summary> Уровень заряда батареи датчика 3 </summary>
		[Measure(UnitOfMeasure.Percentage)]
		BatteryExternalSensor3             = 273,
		/// <summary> Уровень заряда батареи датчика 4 </summary>
		[Measure(UnitOfMeasure.Percentage)]
		BatteryExternalSensor4             = 274,
		/// <summary> Влажность 1 </summary>
		[Measure(UnitOfMeasure.RelHumidity)]
		HumiditySensor1                    = 281,
		/// <summary> Влажность 2 </summary>
		[Measure(UnitOfMeasure.RelHumidity)]
		HumiditySensor2                    = 282,
		/// <summary> Влажность 3 </summary>
		[Measure(UnitOfMeasure.RelHumidity)]
		HumiditySensor3                    = 283,
		/// <summary> Влажность 4 </summary>
		[Measure(UnitOfMeasure.RelHumidity)]
		HumiditySensor4                    = 284,
		/// <summary> Освещенность 1 </summary>
		[Measure(UnitOfMeasure.Lux)]
		LuminositSensor1                   = 291,
		/// <summary> Освещенность 2 </summary>
		[Measure(UnitOfMeasure.Lux)]
		LuminositSensor2                   = 292,
		/// <summary> Освещенность 3 </summary>
		[Measure(UnitOfMeasure.Lux)]
		LuminositSensor3                   = 293,
		/// <summary> Освещенность 4 </summary>
		[Measure(UnitOfMeasure.Lux)]
		LuminositSensor4                   = 294,
		////////////////////////////////////////////////////////////
		/// <summary> Dallas Температура 1 </summary>
		[Measure(UnitOfMeasure.Celsius)]
		DallasTemperatureSensor1           = 295,
		/// <summary> Dallas Температура 2 </summary>
		[Measure(UnitOfMeasure.Celsius)]
		DallasTemperatureSensor2           = 296,
		/// <summary> Dallas Температура 3 </summary>
		[Measure(UnitOfMeasure.Celsius)]
		DallasTemperatureSensor3           = 297,
		/// <summary> Dallas Температура 4 </summary>
		[Measure(UnitOfMeasure.Celsius)]
		DallasTemperatureSensor4           = 298,
		/// <summary> Dallas Температура 5 </summary>
		[Measure(UnitOfMeasure.Celsius)]
		DallasTemperatureSensor5           = 299,
		/// <summary> Dallas Температура 6 </summary>
		[Measure(UnitOfMeasure.Celsius)]
		DallasTemperatureSensor6           = 300,
		/// <summary> LLS 1 Уровень топлива </summary>
		[Measure(UnitOfMeasure.Litre)]
		LLS1FuelLevel                      = 301,
		/// <summary> LLS 1 Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		LLS1Temperature                    = 302,
		/// <summary> LLS 2 Уровень топлива </summary>
		[Measure(UnitOfMeasure.Litre)]
		LLS2FuelLevel                      = 303,
		/// <summary> LLS 2 Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		LLS2Temperature                    = 304,
		/// <summary> LLS 3 Уровень топлива </summary>
		[Measure(UnitOfMeasure.Litre)]
		LLS3FuelLevel                      = 305,
		/// <summary> LLS 3 Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		LLS3Temperature                    = 306,
		/// <summary> LLS 4 Уровень топлива </summary>
		[Measure(UnitOfMeasure.Litre)]
		LLS4FuelLevel                      = 307,
		/// <summary> LLS 4 Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		LLS4Temperature                    = 308,
		/// <summary> LLS 5 Уровень топлива </summary>
		[Measure(UnitOfMeasure.Litre)]
		LLS5FuelLevel                      = 309,
		/// <summary> LLS 5 Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		LLS5Temperature                    = 310,
		/// <summary> Счетчик расхода топлива </summary>
		[Measure(UnitOfMeasure.Litre)]
		FuelCounter                        = 311,
		/// <summary> Уровень топлива 1 (%) </summary>
		[Measure(UnitOfMeasure.Percentage)]
		FuelLevel1                         = 312,
		/// <summary> Уровень топлива 2 (%) </summary>
		[Measure(UnitOfMeasure.Percentage)]
		FuelLevel2                         = 313,
		/// <summary> Уровень топлива 3 (%) </summary>
		[Measure(UnitOfMeasure.Percentage)]
		FuelLevel3                         = 314,
		/// <summary> Уровень топлива 4 (%) </summary>
		[Measure(UnitOfMeasure.Percentage)]
		FuelLevel4                         = 315,
		/// <summary> Уровень топлива 5 (%) </summary>
		[Measure(UnitOfMeasure.Percentage)]
		FuelLevel5                         = 316,
		/// <summary> Уровень топлива 6 (%) </summary>
		[Measure(UnitOfMeasure.Percentage)]
		FuelLevel6                         = 317,
		/// <summary> Уровень топлива 7 (%) </summary>
		[Measure(UnitOfMeasure.Percentage)]
		FuelLevel7                         = 318,
		/// <summary> Режим передачи данных </summary>
		[Measure(UnitOfMeasure.None)]
		DataMode                           = 319,
		/// <summary> Режим сна устройства </summary>
		SleepMode                          = 320,
		/// <summary> Активный GSM оператор </summary>
		[Measure(UnitOfMeasure.None)]
		ActiveGSMOperator                  = 321,
		/// <summary> Управляющий выход 4 </summary>
		DigitalOutput4                     = 322,
		/// <summary> U-S1 Уровень топлива </summary>
		[Measure(UnitOfMeasure.Millimeter)]
		UltrasonicFuelLevel1               = 323,
		/// <summary> U-S2 Уровень топлива </summary>
		[Measure(UnitOfMeasure.Millimeter)]
		UltrasonicFuelLevel2               = 324,
		/// <summary> Температура 0 </summary>
		[Measure(UnitOfMeasure.Celsius)]
		TemperatureSensor0                 = 325,
		/// <summary> Температура 5 </summary>
		[Measure(UnitOfMeasure.Celsius)]
		TemperatureSensor5                 = 326,
		/// <summary> Педаль тормоза </summary>
		BreakPedal                         = 327,
		/// <summary> Загрузка двигателя (%) </summary>
		[Measure(UnitOfMeasure.Percentage)]
		EngineLoad                         = 328,
		/// <summary> Нагрузка на ось 7 </summary>
		[Measure(UnitOfMeasure.Kilogram)]
		CAN_AxleLoad7                      = 329,
		/// <summary> Нагрузка на ось 8 </summary>
		[Measure(UnitOfMeasure.Kilogram)]
		CAN_AxleLoad8                      = 330,
		/// <summary> Нагрузка на ось 9 </summary>
		[Measure(UnitOfMeasure.Kilogram)]
		CAN_AxleLoad9                      = 331,
		/// <summary> Нагрузка на ось 10 </summary>
		[Measure(UnitOfMeasure.Kilogram)]
		CAN_AxleLoad10                     = 332,
		/// <summary> Нагрузка на ось 11 </summary>
		[Measure(UnitOfMeasure.Kilogram)]
		CAN_AxleLoad11                     = 333,
		/// <summary> Нагрузка на ось 12 </summary>
		[Measure(UnitOfMeasure.Kilogram)]
		CAN_AxleLoad12                     = 334,
		/// <summary> Нагрузка на ось 13 </summary>
		[Measure(UnitOfMeasure.Kilogram)]
		CAN_AxleLoad13                     = 335,
		/// <summary> Нагрузка на ось 14 </summary>
		[Measure(UnitOfMeasure.Kilogram)]
		CAN_AxleLoad14                     = 336,
		/// <summary> Нагрузка на ось 15 </summary>
		[Measure(UnitOfMeasure.Kilogram)]
		CAN_AxleLoad15                     = 337,
		/// <summary> Мгновенная экономия топлива </summary>
		[Measure(UnitOfMeasure.KmPerLitre)]
		InstantaneousFuelEconomy           = 338,
		/// <summary> Высокоточное значение общего потребленного топлива </summary>
		[Measure(UnitOfMeasure.Litre)]
		HighResolutionEngineTotalFuelUsed  = 339,
		/// <summary> Полный вес грузовика и прицепов </summary>
		[Measure(UnitOfMeasure.Kilogram)]
		GrossCombinationVehicleWeight      = 340,
		/// <summary> Шина 1. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire1pressure                      = 341,
		/// <summary> Шина 2. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire2pressure                      = 342,
		/// <summary> Шина 3. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire3pressure                      = 343,
		/// <summary> Шина 4. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire4pressure                      = 344,
		/// <summary> Шина 5. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire5pressure                      = 345,
		/// <summary> Шина 6. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire6pressure                      = 346,
		/// <summary> Шина 7. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire7pressure                      = 347,
		/// <summary> Шина 8. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire8pressure                      = 348,
		/// <summary> Шина 9. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire9pressure                      = 349,
		/// <summary> Шина 10. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire10pressure                     = 350,
		/// <summary> Шина 11. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire11pressure                     = 351,
		/// <summary> Шина 12. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire12pressure                     = 352,
		/// <summary> Шина 13. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire13pressure                     = 353,
		/// <summary> Шина 14. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire14pressure                     = 354,
		/// <summary> Шина 15. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire15pressure                     = 355,
		/// <summary> Шина 16. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire16pressure                     = 356,
		/// <summary> Шина 17. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire17pressure                     = 357,
		/// <summary> Шина 18. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire18pressure                     = 358,
		/// <summary> Шина 19. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire19pressure                     = 359,
		/// <summary> Шина 20. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire20pressure                     = 360,
		/// <summary> Шина 21. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire21pressure                     = 361,
		/// <summary> Шина 22. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire22pressure                     = 362,
		/// <summary> Шина 23. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire23pressure                     = 363,
		/// <summary> Шина 24. Давление </summary>
		[Measure(UnitOfMeasure.Kilopascal)]
		Tire24pressure                     = 364,
		/// <summary> Шина 1. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire1temperature                   = 365,
		/// <summary> Шина 2. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire2temperature                   = 366,
		/// <summary> Шина 3. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire3temperature                   = 367,
		/// <summary> Шина 4. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire4temperature                   = 368,
		/// <summary> Шина 5. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire5temperature                   = 369,
		/// <summary> Шина 6. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire6temperature                   = 370,
		/// <summary> Шина 7. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire7temperature                   = 371,
		/// <summary> Шина 8. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire8temperature                   = 372,
		/// <summary> Шина 9. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire9temperature                   = 373,
		/// <summary> Шина 10. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire10temperature                  = 374,
		/// <summary> Шина 11. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire11temperature                  = 375,
		/// <summary> Шина 12. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire12temperature                  = 376,
		/// <summary> Шина 13. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire13temperature                  = 377,
		/// <summary> Шина 14. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire14temperature                  = 378,
		/// <summary> Шина 15. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire15temperature                  = 379,
		/// <summary> Шина 16. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire16temperature                  = 380,
		/// <summary> Шина 17. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire17temperature                  = 381,
		/// <summary> Шина 18. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire18temperature                  = 382,
		/// <summary> Шина 19. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire19temperature                  = 383,
		/// <summary> Шина 20. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire20temperature                  = 384,
		/// <summary> Шина 21. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire21temperature                  = 385,
		/// <summary> Шина 22. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire22temperature                  = 386,
		/// <summary> Шина 23. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire23temperature                  = 387,
		/// <summary> Шина 24. Температура </summary>
		[Measure(UnitOfMeasure.Celsius)]
		Tire24temperature                  = 388,
		/// <summary> Шина 1. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire1warning                       = 389,
		/// <summary> Шина 2. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire2warning                       = 390,
		/// <summary> Шина 3. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire3warning                       = 391,
		/// <summary> Шина 4. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire4warning                       = 392,
		/// <summary> Шина 5. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire5warning                       = 393,
		/// <summary> Шина 6. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire6warning                       = 394,
		/// <summary> Шина 7. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire7warning                       = 395,
		/// <summary> Шина 8. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire8warning                       = 396,
		/// <summary> Шина 9. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire9warning                       = 397,
		/// <summary> Шина 10. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire10warning                      = 398,
		/// <summary> Шина 11. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire11warning                      = 399,
		/// <summary> Шина 12. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire12warning                      = 400,
		/// <summary> Шина 13. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire13warning                      = 401,
		/// <summary> Шина 14. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire14warning                      = 402,
		/// <summary> Шина 15. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire15warning                      = 403,
		/// <summary> Шина 16. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire16warning                      = 404,
		/// <summary> Шина 17. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire17warning                      = 405,
		/// <summary> Шина 18. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire18warning                      = 406,
		/// <summary> Шина 19. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire19warning                      = 407,
		/// <summary> Шина 20. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire20warning                      = 408,
		/// <summary> Шина 21. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire21warning                      = 409,
		/// <summary> Шина 22. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire22warning                      = 410,
		/// <summary> Шина 23. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire23warning                      = 411,
		/// <summary> Шина 24. Предупреждение </summary>
		[Measure(UnitOfMeasure.None)]
		Tire24warning                      = 412,
		/// <summary> Моточасы (пересчет) </summary>
		[Measure(UnitOfMeasure.Hours)]
		CAN_EngHours_CNTD                  = 413,
		/// <summary> Расход топлива с начала эксплуатации авто (пересчет) </summary>
		[Measure(UnitOfMeasure.Litre)]
		CAN_TotalFuelSpend_CNTD            = 414,
		/// <summary> Общий пробег (пересчет) </summary>
		[Measure(UnitOfMeasure.Kilometers)]
		CAN_TotalRun_CNTD                  = 415,
		/// <summary> Уровень AdBlue </summary>
		[Measure(UnitOfMeasure.Percentage)]
		AdBlueLevelPercent                 = 416,
		/// <summary> Объем AdBlue </summary>
		[Measure(UnitOfMeasure.Litre)]
		AdBlueLevelLiters                  = 417,
		/// <summary> Время сбора урожая </summary>
		[Measure(UnitOfMeasure.Minute)]
		HarvestingTime                     = 418,
		/// <summary> Площадь сбора урожая </summary>
		[Measure(UnitOfMeasure.SquareMeter)]
		AreaofHarvest                      = 419,
		/// <summary> Эффективность кошения </summary>
		[Measure(UnitOfMeasure.SquareMetersPerHour)]
		MowingEfficiency                   = 420,
		/// <summary> Объем скошенного зерна </summary>
		[Measure(UnitOfMeasure.Kilogram)]
		GrainMownVolume                    = 421,
		/// <summary> Влажность зерна </summary>
		[Measure(UnitOfMeasure.Percentage)]
		GrainMoisture                      = 422,
		/// <summary> Обороты уборочного барабана </summary>
		[Measure(UnitOfMeasure.RpmMinute)]
		HarvestingDrumRPM                  = 423,
		/// <summary> Зазор под уборочным барабаном </summary>
		[Measure(UnitOfMeasure.Millimeter)]
		GapUnderHarvestingDrum             = 424,
		/// <summary> Температура батареи </summary>
		[Measure(UnitOfMeasure.Celsius)]
		BatteryTemperature                 = 425,
		/// <summary> DTCErrors </summary>
		[Measure(UnitOfMeasure.None)]
		DTCErrors                          = 426,
		/// <summary> CNGStatus </summary>
		CNGStatus                          = 427,
		/// <summary> CNGUsed </summary>
		[Measure(UnitOfMeasure.Kilogram)]
		CNGUsed                            = 428,
		/// <summary> CNGLevel </summary>
		[Measure(UnitOfMeasure.Percentage)]
		CNGLevel                           = 429,
		/// <summary> Превышение скорости </summary>
		[Measure(UnitOfMeasure.Kmph)]
		OverSpeeding                       = 430,
		/// <summary> Эко вождение </summary>
		[Measure(UnitOfMeasure.None)]
		Greendrivingtype                   = 431,
		/// <summary> Буксировка </summary>
		Towing                             = 432,
		/// <summary> ДТП </summary>
		[Measure(UnitOfMeasure.None)]
		Crashdetection                     = 433,
		/// <summary> Джемминг </summary>
		Jamming                            = 434,
		/// <summary> Всего шин </summary>
		[Measure(UnitOfMeasure.Thing)]
		TotalTires                         = 435,
		/// <summary> Всего осей </summary>
		[Measure(UnitOfMeasure.Thing)]
		TotalAxles                         = 436,
		/// <summary> Drive Recognize </summary>
		DriveRecognize                     = 437,
		/// <summary> Водитель 1. Режим работы </summary>
		[Measure(UnitOfMeasure.None)]
		Driver1WorkingState                = 438,
		/// <summary> Водитель 2. Режим работы </summary>
		[Measure(UnitOfMeasure.None)]
		Driver2WorkingState                = 439,
		/// <summary> Тахограф. Превышение скорости </summary>
		TachographOverSpeed                = 440,
		/// <summary> Водитель 1. Карта вставлена </summary>
		Driver1CardPresence                = 441,
		/// <summary> Водитель 2. Карта вставлена </summary>
		Driver2CardPresence                = 442,
		/// <summary> Водитель 1. Временные пороги </summary>
		[Measure(UnitOfMeasure.None)]
		Driver1TimeRelatedStates           = 443,
		/// <summary> Водитель 2. Временные пороги </summary>
		[Measure(UnitOfMeasure.None)]
		Driver2TimeRelatedStates           = 444,
		/// <summary> Тахограф. Скорость </summary>
		[Measure(UnitOfMeasure.Kmph)]
		VehicleSpeed                       = 445,
		/// <summary> Тахограф. Одометер </summary>
		[Measure(UnitOfMeasure.Kilometers)]
		Odometer                           = 446,
		/// <summary> Тахограф. Поездка </summary>
		[Measure(UnitOfMeasure.Kilometers)]
		TripDistance                       = 447,
		/// <summary> Тахограф. Время </summary>
		[Measure(UnitOfMeasure.None)]
		Timestamp                          = 448,
		/// <summary> Карточка 1. Субъект выпуска </summary>
		[Measure(UnitOfMeasure.None)]
		Card1IssuingMemberState            = 449,
		/// <summary> Карточка 2. Субъект выпуска </summary>
		[Measure(UnitOfMeasure.None)]
		Card2IssuingMemberState            = 450,
		/// <summary> Водитель 1. Непрерывно за рулем </summary>
		[Measure(UnitOfMeasure.None)]
		Driver1ContinuousDrivingTime       = 451,
		/// <summary> Водитель 2. Непрерывно за рулем </summary>
		[Measure(UnitOfMeasure.None)]
		Driver2ContinuousDrivingTime       = 452,
		/// <summary> Водитель 1. Суммарно остановки </summary>
		[Measure(UnitOfMeasure.None)]
		Driver1CumulativeBreakTime         = 453,
		/// <summary> Водитель 2. Суммарно остановки </summary>
		[Measure(UnitOfMeasure.None)]
		Driver2CumulativeBreakTime         = 454,
		/// <summary> Водитель 1. Длительность выбранной активности </summary>
		[Measure(UnitOfMeasure.None)]
		Driver1SelectedActivityDuration    = 455,
		/// <summary> Водитель 2. Длительность выбранной активности </summary>
		[Measure(UnitOfMeasure.None)]
		Driver2SelectedActivityDuration    = 456,
		/// <summary> Водитель 1. Суммарное время вождения </summary>
		[Measure(UnitOfMeasure.None)]
		Driver1CumulativeDrivingTime       = 457,
		/// <summary> Водитель 2. Суммарное время вождения </summary>
		[Measure(UnitOfMeasure.None)]
		Driver2CumulativeDrivingTime       = 458,
		/// <summary> Тахо источника данных </summary>
		[Measure(UnitOfMeasure.None)]
		TachoDataSource                    = 459,
		/// <summary> Качество вождения </summary>
		[Measure(UnitOfMeasure.Score)]
		Ecodriving                         = 460,
		/// <summary> Работа холодильной установки </summary>
		[Measure(UnitOfMeasure.Hours)]
		RefrigerationUnitOperation         = 461,
		/// <summary> Осевое ускорение X </summary>
		[Measure(UnitOfMeasure.MilliGravitationalAcceleration)]
		AxisXAcceleration                  = 462,
		/// <summary> Осевое ускорение Y </summary>
		[Measure(UnitOfMeasure.MilliGravitationalAcceleration)]
		AxisYAcceleration                  = 463,
		/// <summary> Осевое ускорение Z </summary>
		[Measure(UnitOfMeasure.MilliGravitationalAcceleration)]
		AxisZAcceleration                  = 464,

		#region Ретрансляция в ЕГТС(EGTS) - Коды начинаются с 601

		/// <summary> Передача в ЕГТС, Цифровой датчик 1 </summary>
		ToEgtsDigitalSensor1               = 601,
		/// <summary> Передача в ЕГТС, Цифровой датчик 2 </summary>
		ToEgtsDigitalSensor2               = 602,
		/// <summary> Передача в ЕГТС, Цифровой датчик 3 </summary>
		ToEgtsDigitalSensor3               = 603,
		/// <summary> Передача в ЕГТС, Цифровой датчик 4 </summary>
		ToEgtsDigitalSensor4               = 604,
		/// <summary> Передача в ЕГТС, Цифровой датчик 5 </summary>
		ToEgtsDigitalSensor5               = 605,
		/// <summary> Передача в ЕГТС, Цифровой датчик 6 </summary>
		ToEgtsDigitalSensor6               = 606,
		/// <summary> Передача в ЕГТС, Цифровой датчик 7 </summary>
		ToEgtsDigitalSensor7               = 607,
		/// <summary> Передача в ЕГТС, Цифровой датчик 8 </summary>
		ToEgtsDigitalSensor8               = 608,
		/// <summary> Передача в ЕГТС, Зажигание </summary>
		ToEgtsIgnition                     = 609,
		/// <summary> Передача в ЕГТС, HDOP </summary>
		[Measure(UnitOfMeasure.None)]
		ToEgtsHDOP                         = 610,
		/// <summary> Передача в ЕГТС, VDOP </summary>
		[Measure(UnitOfMeasure.None)]
		ToEgtsVDOP                         = 611,
		/// <summary> Передача в ЕГТС, PDOP </summary>
		[Measure(UnitOfMeasure.None)]
		ToEgtsPDOP                         = 612,
		/// <summary> Передача в ЕГТС, Аналоговый датчик 1 </summary>
		[Measure(UnitOfMeasure.None)]
		ToEgtsAnalogSensor1                = 613,
		/// <summary> Передача в ЕГТС, Аналоговый датчик 2 </summary>
		[Measure(UnitOfMeasure.None)]
		ToEgtsAnalogSensor2                = 614,
		/// <summary> Передача в ЕГТС, Аналоговый датчик 3 </summary>
		[Measure(UnitOfMeasure.None)]
		ToEgtsAnalogSensor3                = 615,
		/// <summary> Передача в ЕГТС, Аналоговый датчик 4 </summary>
		[Measure(UnitOfMeasure.None)]
		ToEgtsAnalogSensor4                = 616,
		/// <summary> Передача в ЕГТС, Аналоговый датчик 5 </summary>
		[Measure(UnitOfMeasure.None)]
		ToEgtsAnalogSensor5                = 617,
		/// <summary> Передача в ЕГТС, Аналоговый датчик 6 </summary>
		[Measure(UnitOfMeasure.None)]
		ToEgtsAnalogSensor6                = 618,
		/// <summary> Передача в ЕГТС, Аналоговый датчик 7 </summary>
		[Measure(UnitOfMeasure.None)]
		ToEgtsAnalogSensor7                = 619,
		/// <summary> Передача в ЕГТС, Аналоговый датчик 8 </summary>
		[Measure(UnitOfMeasure.None)]
		ToEgtsAnalogSensor8                = 620,

		#endregion Ретрансляция в ЕГТС(EGTS) - Коды начинаются с 601
	}
}