﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	public enum VehicleControlDateTypes
	{
		/// <summary> закончилась страховка </summary>
		InsuranceExpiry = 0,
		/// <summary> закончилась лицензия </summary>
		LicenseExpiry   = 1,
		/// <summary> закончилось разрешение </summary>
		PermitExpiry    = 2
	}
}