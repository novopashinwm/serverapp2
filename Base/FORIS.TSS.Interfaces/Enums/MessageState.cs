namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> message state </summary>
	public enum MessageState
	{
		/// <summary> ������� ���������, ��� ��������� ��������� - ��������������� � �������� </summary>
		Wait    = 1,
		/// <summary> �������������� </summary>
		Pending = 2,
		/// <summary> ��������� ��������� </summary>
		Done    = 3,
	}
}