﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary>
	/// Перечисление возможных статусов команды. <see cref="CmdType"/>
	/// </summary>
	[Serializable]
	public enum CmdResult
	{
		/// <summary> Команда получена </summary>
		Received = 0,
		/// <summary> Обрабатывается </summary>
		Processing = 1,
		/// <summary> Выполнена </summary>
		Completed = 2,
		/// <summary> Ошибка при выполнении команды </summary>
		Failed = 3,
		/// <summary> Отменена </summary>
		Cancelled = 4,
		/// <summary> Доступ к устройству запрещен </summary>
		VehicleAccessDisallowed = 5,
		/// <summary> Администратор Asid временно приостановил разрешения на определение местоположения </summary>
		MLPTemporaryDisallowed = 6,
		/// <summary>Абонент не в сети</summary>
		AbsentSubscriber = 7,
		/// <summary>Превышено возможное время ожидания</summary>
		/// <remarks>Следует повторять запрос, пока он не будет выполнен</remarks>
		TimeoutExceeded = 8,
		/// <summary>Услуга определения местоположения по вышкам сотовой связи не подключена на целевое устройство</summary>
		NoTargetLBSService = 9,
		/// <summary>Услуга определения местоположения по вышкам сотовой связи заблокирована на целевом устройстве</summary>
		TargetLBSServiceIsBlocked = 10,
		/// <summary>Услуга определения местоположения по вышкам сотовой связи не подключена на устройство запросившего абонента</summary>
		NoCallerLBSService = 11,
		/// <summary>Услуга определения местоположения по вышкам сотовой связи заблокирована на устройстве запросившего абонента</summary>
		CallerLBSServiceIsBlocked = 12,
		/// <summary>Превышено максимальное количество запросов за период</summary>
		/// <remarks>Определение местоположения по вышкам сотовой связи не может производиться чаще, чем один раз в пять минут</remarks>
		MLPRequestCountPerTimespanExceeded = 13,
		/// <summary>Превышено количество запросов, допустимое для данного контент-провайдера</summary>
		/// <remarks>Следует отложить запрос на некоторое время</remarks>
		CapacityLimitReached = 14,
		/// <summary> Устройство не найдено в списке доступных устройств </summary>
		DeviceNotFoundInReceversList = 15,
		/// <summary> Менеджер устройств не определен </summary>
		TerminalManagerIsNull = 16,
		/// <summary> Устройство подключено, но во время отправки команды произошла ошибка </summary>
		SendCommandDeviceException = 17,
		/// <summary> Вышло время выполнения команды </summary>
		ExecutionTimePassed = 18,
		/// <summary> Недостаточно средств на счёте </summary>
		InsufficientFunds = 19,
		/// <summary> Услуга не подключена </summary>
		NoService = 20,
		/// <summary> Услуга заблокирована </summary>
		ServiceIsBlocked = 21,
		/// <summary> Отсутствует услуга для тарификации запроса </summary>
		NoTarifficationService = 22,
		/// <summary> Номер телефона устройства не известен, поэтому невозможно доставить команду </summary>
		PhoneNumberIsUnknown = 23,
		/// <summary> Сеть перегружена </summary>
		/// <remarks> Запрос ушел в GSM сеть (HLR, VLR), но ответа не поступило. Платформа считает, что это перегрузка сети. </remarks>
		NetworkIsOverloaded = 24,
		/// <summary> Неизвестная базовая станция </summary>
		/// <remarks> На момент позиционирования не было данных по БС, обновление занимает от суток до нескольких дней. </remarks>
		UnknownCell = 25,
		/// <summary> The location request is disallowed by local regulatory requirements. </summary>
		/// <remarks> Алексей Заврин (МТС): следует рассматривать как "Абонент в роуминге" </remarks>
		LbsDisallowedByLocalRegulations = 26,
		/// <summary> Данный тип команды недоступен для объекта наблюдения </summary>
		Inaccessible = 27,
		/// <summary> Команда не поддерживается устройством </summary>
		NotSupported = 28,
		/// <summary> Другая команда такого же типа выполняется в данный момент </summary>
		OtherCommandIsPending = 29,
		/// <summary> Команда отклонена из-за слишком частых вызовов </summary>
		Throttled = 30,
		/// <summary> Абонент неизвестен </summary>
		/// <remarks> После получения такого результата абонент должен быть удалён из БД, 
		/// т.к. номер находится в отстойнике и в дальнейшем будет назначен другому абоненту </remarks>
		UnknownSubscriber = 31,
		/// <summary> Для выполнения команды требуется пароль трекера </summary>
		PasswordIsNotSpecified = 32,
		/// <summary> Указанный пароль трекера некорректен </summary>
		IncorrectPassword = 33,
		/// <summary> Для выполнения команды требуется номер/IMEI трекера </summary>
		DeviceIdIsNotSpecified = 34,
		/// <summary> Указанный номер/IMEI трекера некорректен </summary>
		IncorrectDeviceId = 35,
		/// <summary> Оборудование для мониторинга не подключено </summary>
		NoDeviceInstalled = 36,
		/// <summary> Абонент находится в метро </summary>
		UndergroundRailwayPosition = 37,
		/// <summary> Превышено количество запросов на определяющего абонента, определяемого абонента в рамках данной Услуги за календарный месяц </summary>
		MonthlyLimitExceeded = 38,
		/// <summary> Абонент находится в роуминге </summary>
		Roaming = 39,
		/// <summary> Мобильное приложение не установлено </summary>
		MobileAppNotInstalled = 40,
		/// <summary> Не указан номер телефона пользователя, выполняющего команду </summary>
		OperatorPhoneIsNotSpecified = 41
	}
}