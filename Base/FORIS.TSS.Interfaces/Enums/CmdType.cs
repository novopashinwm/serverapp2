﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary>
	/// std command type
	/// </summary>
	[Serializable]
	public enum CmdType : int
	{
		Unspecified	= -1,
		// EXPLICIT, deprecated for external using, only internals
		UDP = 1,
		GSM = 2,
		SMS = 3,
		Monitor = 4,
		/// <summary>
		/// get pos every sec.
		/// pars: 1. switch = {-1 - force off, 0 - off, 1 - on, 2 - force on} - int
		/// </summary>
		Trace = 5,
		/// <summary>
		/// get history.
		/// pars: 1. from - UTC time, 2. to - UTC time, 3. switch = {1 - on} - int
		/// </summary>
		GetLog = 7,
		GetLogFromDB= 8,
		/// <summary>
		/// control the device.
		/// pars: 1. ignition, 2. engine, 3. pump, 4. headlight, 5. lights, 6. horn - int
		/// values: 1 - on, 0 - off
		/// </summary>
		Control = 9,
		/// <summary> get current device state. now not suitable for external use </summary>
		GetState = 10,
		/// <summary> set current device state. now not suitable for external use </summary>
		SetState = 11,
		/// <summary> set text to remote device </summary>
		SendText = 12,
		/// <summary> send confirm message pars: 1. MessageNo (int) </summary>
		Confirm = 13,
		/// <summary> Отсылка прошивки модулю </summary>
		SetFirmware = 14,
		/// <summary> Отсылка прошивки терминалу </summary>
		SendFirmware = 15,
		/// <summary> Получение настроек контроллера </summary>
		GetSettings = 16,
		/// <summary> Отсылка настроек контроллеру </summary>
		SetSettings = 17,
		/// <summary> Перезагрузка контроллера </summary>
		Restart = 18,
		/// <summary>Регистрация устройства в системе</summary>
		RegisterMobileUnit = 19,
		/// <summary> Запросить местоположение устройства</summary>
		AskPosition = 20,
		/// <summary> Заблокировать электросеть </summary>
		CutOffElectricity = 21,
		/// <summary> Разблокировать электросеть </summary>
		ReopenElectricity = 22,
		/// <summary> Отключить подачу топлива </summary>
		CutOffFuel = 23,
		/// <summary> Восстановить подачу топлива </summary>
		ReopenFuel = 24,
		#region Change controller configuration
		CancelAlarm = 25,
		ChangeMode = 26,
		ReloadDevice = 29,
		VibrateRequest = 30,
		/// <summary> Обратный вызов, трекер звонит на указанный в настройках или команде номер </summary>
		Callback = 31,
		ShutdownDevice = 32,
		#endregion
		/// <summary>Получить картинку со встроенной в устройство камеры</summary>
		/// <remarks>Реализовано в  <a href="https://tfsdoc.sts.sitronics.com/sites/TSS/DocLib/JB101/Tracker Communication Protocol V2.1.1.doc">JB101 V2</a></remarks>
		CapturePicture = 33,
		/// <summary> Запросить URL и дополнительную информацию для проигрывания видео с устройства </summary>
		CreateReplayUri = 34,
		/// <summary>Определение местоположения по протоколу MLP</summary>
		/// <remarks>Команда служебная, регистрироваться в очереди команд не должна, создаётся при определении местоположения с помощью команды <see cref="AskPosition"/></remarks>
		AskPositionOverMLP = 35,
		/// <summary> Настроить трекер для работы с системой</summary>
		/// <remarks><p>Прописать на трекере базовые настройки для начала отправки данных на сервер: APN, сервер и порт</p>
		/// <p>Остальные настройки должны управляться отдельно, либо команда должна брать необходимые настройки из БД и слать их в устройство</p>
		/// </remarks>
		Setup = 36,
		/// <summary> Заблокировать возможность движения автомобиля </summary>
		/// <remarks> Реализация блокировки остаётся на усмотрение монтажника, например, это может быть отключение стартера </remarks>
		Immobilize = 37,
		/// <summary> Снять блокировку с автомобиля </summary>
		Deimmobilize = 38,
		/// <summary> Включить режим постоянного слежения </summary>
		SetModeOnline = 39,
		/// <summary> Включить ждущий режим </summary>
		SetModeWaiting = 40,
		/// <summary> Включить спящий режим </summary>
		SetModeBeacon = 41,
		/// <summary> Включить датчик движения </summary>
		EnableMotionDetector = 42,
		/// <summary> Выключить датчик движения </summary>
		DisableMotionDetector = 43,
		/// <summary> Режим мгновенной блокировки </summary>
		/// <remarks> Двигатель будет блокироваться мгновенно после подачи команды </remarks>
		QuickImmobilizeON       = 44,
		/// <summary> Режим отложенной блокировки </summary>
		/// <remarks> Двигатель будет блокироваться после понижения скорости ТС </remarks>
		QuickImmobilizeOFF      = 45,
		/// <summary> Настроить APN </summary>
		/// <remarks> Настраивает APN контроллера </remarks>
		SetAPN                  = 46,
		/// <summary> Настроить интервал передачи данных </summary>
		/// <remarks> Устанавливает интервал передачи данных в секундах. Значение параметра секунд содержится в тексте команды </remarks>
		SetInterval             = 47,
		/// <summary> Статус </summary>
		/// <remarks> Получить статус контроллера </remarks>
		Status                  = 48,
		/// <summary> Установить номер SOS </summary>
		/// <remarks> Установить номер для получения вызова SOS от контроллера </remarks>
		SetSos                  = 49,
		/// <summary> Удалить номер SOS </summary>
		/// <remarks> Удалить SOS номер контроллера </remarks>
		DeleteSOS               = 50,
		/// <summary> Включить режим мониторинга </summary>
		/// <remarks> В режиме мониторинга появляется возможность позвонить на контроллер и услышать, что происходит вокруг </remarks>
		MonitorModeOn           = 51,
		/// <summary> Выключить режим мониторинга </summary>
		/// <remarks> Выключает режим мониторинга </remarks>
		MonitorModeOff          = 52,
		/// <summary> Найти контроллер </summary>
		/// <remarks> Найти устройство </remarks>
		FindDevice              = 53,
		/// <summary> Добавить контакт </summary>
		/// <remarks> Добавить контакт в телефонную книгу трекера </remarks>
		AddContact              = 54,
		/// <summary> Удалить контакт </summary>
		/// <remarks> Удалить контакт из телефонной книги трекера </remarks>
		DeleteContact           = 55,
		/// <summary> Включить световой маяк </summary>
		/// <remarks> Включить световой маяк на корпусе трекера </remarks>
		TurnLightOn             = 56,
		/// <summary> Отключить световой маяк </summary>
		/// <remarks> Отключить световой маяк на корпусе трекера </remarks>
		TurnLightOff            = 57,
		/// <summary> Включить режим смс оповещение при прекращении подачи внешнего питания </summary>
		PowerAlarmOn            = 58,
		/// <summary> Выключить режим смс оповещение при прекращении подачи внешнего питания </summary>
		PowerAlarmOff           = 59,
		/// <summary> Включить режим смс оповещение при срабатывании датчика вибрации </summary>
		ShakeAlarmOn            = 60,
		/// <summary> Выключить режим смс оповещение при  срабатывании датчика вибрации </summary>
		ShakeAlarmOff           = 61,
		/// <summary> Получить координаты и время </summary>
		AskGPSPosition          = 62,
		/// <summary> Запросить ссылку на Google карты </summary>
		AskGoogleLink           = 63,
		/// <summary> Состояние батареи </summary>
		AskBattery              = 64,
		/// <summary> Поиск bluetooth устройств </summary>
		BluetoothScan           = 65,
		/// <summary> Вывести список видимых bluetooth устройств </summary>
		BluetoothDiscoveredList = 66,
		/// <summary> Вывести список присоединенных bluetooth устройств </summary>
		BluetoothConnectedList  = 67,
		/// <summary> Установить значение одометра трекера </summary>
		SetOdometer             = 68,
		/// <summary> Получить значение одометра трекера </summary>
		GetOdometer             = 69,
		/// <summary> Компьютерная диагностика (OBD) </summary>
		GetOBDInfo              = 70,
		/// <summary> Получить коды ошибок (OBD) </summary>
		GetFaultCodes           = 71,
		/// <summary> Получить VIN код </summary>
		GetVin                  = 72,
		/// <summary> Очистить черный ящик </summary>
		BlackBoxClear           = 080,
		/// <summary> CAN: Активировать CAN-скрипт 1 </summary>
		RunCanScript01          = 081,
		/// <summary> CAN: Активировать CAN-скрипт 2 </summary>
		RunCanScript03          = 082,
		/// <summary> CAN: Активировать CAN-скрипт 3 </summary>
		RunCanScript02          = 083,
		/// <summary> Выход 1: включить </summary>
		SetOut01On              = 090,
		/// <summary> Выход 1: выключить </summary>
		SetOut01Off             = 091,
		/// <summary> Выход 2: включить </summary>
		SetOut02On              = 092,
		/// <summary> Выход 2: выключить </summary>
		SetOut02Off             = 093,
		/// <summary> Дополнительный выход 1: включить </summary>
		SetExtOut01On           = 100,
		/// <summary> Дополнительный выход 1: выключить </summary>
		SetExtOut01Off          = 101,
		/// <summary> Дополнительный выход 2: включить </summary>
		SetExtOut02On           = 102,
		/// <summary> Дополнительный выход 2: выключить </summary>
		SetExtOut02Off          = 103,
		/// <summary> Дополнительный выход 3: включить </summary>
		SetExtOut03On           = 104,
		/// <summary> Дополнительный выход 3: выключить </summary>
		SetExtOut03Off          = 105,
		/// <summary> Дополнительный выход 4: включить </summary>
		SetExtOut04On           = 106,
		/// <summary> Дополнительный выход 4: выключить </summary>
		SetExtOut04Off          = 107,
		/// <summary> Дополнительный выход 5: включить </summary>
		SetExtOut05On           = 108,
		/// <summary> Дополнительный выход 5: выключить </summary>
		SetExtOut05Off          = 109,
		/// <summary> Дополнительный выход 6: включить </summary>
		SetExtOut06On           = 110,
		/// <summary> Дополнительный выход 6: выключить </summary>
		SetExtOut06Off          = 111,
		/// <summary> Дополнительный выход 7: включить </summary>
		SetExtOut07On           = 112,
		/// <summary> Дополнительный выход 7: выключить </summary>
		SetExtOut07Off          = 113,
		/// <summary> Дополнительный выход 8: включить </summary>
		SetExtOut08On           = 114,
		/// <summary> Дополнительный выход 8: выключить </summary>
		SetExtOut08Off          = 115,
		/// <summary> Дополнительный выход 9: включить </summary>
		SetExtOut09On           = 116,
		/// <summary> Дополнительный выход 9: выключить </summary>
		SetExtOut09Off          = 117,
		/// <summary> Дополнительный выход 10: включить </summary>
		SetExtOut10On           = 118,
		/// <summary> Дополнительный выход 10: выключить </summary>
		SetExtOut10Off          = 119,
		/// <summary> Дополнительный выход 11: включить </summary>
		SetExtOut11On           = 120,
		/// <summary> Дополнительный выход 11: выключить </summary>
		SetExtOut11Off          = 121,
		/// <summary> Дополнительный выход 12: включить </summary>
		SetExtOut12On           = 122,
		/// <summary> Дополнительный выход 12: выключить </summary>
		SetExtOut12Off          = 123,
		/// <summary> Дополнительный выход 13: включить </summary>
		SetExtOut13On           = 124,
		/// <summary> Дополнительный выход 13: выключить </summary>
		SetExtOut13Off          = 125,
		/// <summary> Дополнительный выход 14: включить </summary>
		SetExtOut14On           = 126,
		/// <summary> Дополнительный выход 14: выключить </summary>
		SetExtOut14Off          = 127,
		/// <summary> Дополнительный выход 15: включить </summary>
		SetExtOut15On           = 128,
		/// <summary> Дополнительный выход 15: выключить </summary>
		SetExtOut15Off          = 129,
		#region Временно, команды специфичные для Teltonika
		/// <summary> Связь с FOTA </summary>
		Teltonika_ConnectFOTA            = 130,
		/// <summary> Отключить аналоговый вход </summary>
		Teltonika_SetAnInOff             = 131,
		/// <summary> Отключить передачу напряжения питания </summary>
		Teltonika_SetVoltageControlOff   = 132,
		/// <summary> Запись входа Analog Input 1 - раз в 5 минут </summary>
		Teltonika_SetAnInpRec1To1Per5Min = 133,
		/// <summary> Запись External Voltage - раз в 5 минут </summary>
		Teltonika_SetExtVltRecTo1Per5Min = 134,
		#endregion Временно, команды специфичные для Teltonika
	}
}