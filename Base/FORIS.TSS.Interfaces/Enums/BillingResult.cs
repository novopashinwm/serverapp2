﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Результат операции биллинга </summary>
	public enum BillingResult
	{
		/// <summary> Успешно </summary>
		Success,
		/// <summary> Услуга не подключена </summary>
		NoService,
		/// <summary> Превышено максимальное количество запросов за период </summary>
		LimitPerTimeSpanReached,
		/// <summary> Недостаточно средств на счёте </summary>
		InsufficientFunds,
		/// <summary> Услуга заблокирована </summary>
		ServiceIsBlocked,
		/// <summary> Услуга не подключена на приложении обслуживания вызывающего пользователя </summary>
		NoCallerService,
		/// <summary> Услуга не подключена на вызываемом приложении обслуживания </summary>
		NoTargetService,
		/// <summary> Приложение обслуживания вызывающего пользователя заблокировано </summary>
		CallerServiceIsBlocked,
		/// <summary> Вызываемое приложение обслуживания заблокировано </summary>
		TargetServiceIsBlocked
	}
}