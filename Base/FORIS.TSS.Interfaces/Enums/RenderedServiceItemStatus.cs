﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Статус предоставленной услуги </summary>
	[Serializable]
	public enum RenderedServiceItemStatus
	{
		/// <summary> Создано и не обработано </summary>
		Created = 0,
		/// <summary> Оплачено </summary>
		Payed   = 1,
		/// <summary> Ошибка обработки </summary>
		Error   = 2,
		/// <summary> Счёт заблокирован </summary>
		Barred  = 3,
	}
}