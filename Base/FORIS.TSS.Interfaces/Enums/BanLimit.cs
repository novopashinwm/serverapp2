﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	public enum BanLimit
	{
		NoLimit,
		SmsConfirmation,
		MsisdnVerification,
		CheckPassword,
		TryLogin
	}
}