﻿using System;
using System.Linq;

namespace FORIS.TSS.BusinessLogic.Enums
{
	[Serializable]
	public class UserRole
	{
		/// <summary> Права пользователя на работу в системе </summary>
		public SystemRight[] UserRights       { get; private set; } = DefaultNonRights;
		/// <summary> Права пользователя на работу с департаментом </summary>
		public SystemRight[] DepartmentRights { get; private set; } = DefaultNonRights;
		/// <summary> Права пользователя на работу с объектами наблюдения </summary>
		public SystemRight[] VehicleRights    { get; private set; } = DefaultNonRights;
		/// <summary> Пустой набор прав </summary>
		public static readonly SystemRight[] DefaultNonRights = new SystemRight[0];
		/// <summary> Права оператора по-умолчанию для пользователя </summary>
		/// <remarks> Права оператора для пользователя департамента, закладка "Права пользователя" </remarks>
		public static readonly SystemRight[] DefaultOprRights = DefaultNonRights
			.Union(new[]
			{
				SystemRight.VehicleAccess,
				SystemRight.PathAccess,
				SystemRight.ReportsAccess,
				SystemRight.SecurityMapGis,
				SystemRight.EditZone,
			})
			.ToArray();
		/// <summary> Права оператора по-умолчанию для администратора (пользователь + доп. права) </summary>
		/// <remarks> Права оператора для администратора департамента, закладка "Права пользователя" </remarks>
		public static readonly SystemRight[] DefaultAdmRights = DefaultOprRights
			.Union(new[]
			{
				SystemRight.SecurityAdministration,
			})
			.ToArray();
		/// <summary> Права на департамент по-умолчанию для пользователя </summary>
		/// <remarks> Права на департамент для пользователя департамента,
		/// закладки с этими правами нет в интерфейсе используется только внутри при создании пользователя </remarks>
		public static readonly SystemRight[] DefaultOprRightsOnDepartment = DefaultNonRights
			.Union(new[]
			{
				SystemRight.DepartmentsAccess,
			})
			.ToArray();
		/// <summary> Права на департаменты по-умолчанию для администратора (пользователь + доп. права) </summary>
		/// <remarks> Права на департамент для пользователя департамента,
		/// закладки с этими правами нет в интерфейсе используется только внутри при создании пользователя </remarks>
		public static readonly SystemRight[] DefaultAdmRightsOnDepartment = DefaultOprRightsOnDepartment
			.Union(new[]
			{
				SystemRight.SecurityAdministration,
			})
			.ToArray();
		/// <summary> Права на объект наблюдения по-умолчанию для пользователя </summary>
		public static readonly SystemRight[] DefaultOprRightsOnVehicle = DefaultNonRights
			.Union(new[]
			{
				SystemRight.VehicleAccess,
			})
			.ToArray();
		/// <summary> Права на объект наблюдения по-умолчанию для администратора (пользователь + доп. права) </summary>
		public static readonly SystemRight[] DefaultAdmRightsOnVehicle = DefaultOprRightsOnVehicle
			.Union(new[]
			{
				// Старые (VehicleAccess выдано в DefaultOprRightsOnVehicle)
				//SystemRight.ViewVehicleCurrentStatus, // Больше не нужно
				SystemRight.PathAccess,
				SystemRight.EditVehicles,
				SystemRight.Immobilization,
				SystemRight.ChangeDeviceConfigBySMS,
				//SystemRight.ControllerPasswordAccess, // Больше не нужно
				SystemRight.EditGroup,
				// Расширение
				SystemRight.ViewingTrackerAttributes,
				SystemRight.EditingTrackerAttributes,
				SystemRight.CommandAccess,
				SystemRight.ShareLinkAccess,
				SystemRight.ManageSensors,
				// Администрирование
				SystemRight.SecurityAdministration,
			})
			.ToArray();
		/// <summary> Константный набор прав владельца объекта </summary>
		public static readonly SystemRight[] DefaultOwnRightsOnVehicle = DefaultAdmRightsOnVehicle;
		/// <summary> Права на новые группы объектов наблюдения по-умолчанию для пользователя </summary>
		public static readonly SystemRight[] DefaultOprRightsOnVehicleGroup = DefaultNonRights
			.Union(new[]
			{
				SystemRight.VehicleAccess,
				SystemRight.EditGroup,
				SystemRight.GroupAdministration,
			})
			.ToArray();
		/// <summary> Права на новые группы объектов наблюдения по-умолчанию для администратора </summary>
		public static readonly SystemRight[] DefaultAdmRightsOnVehicleGroup = DefaultOprRightsOnVehicleGroup
			.Union(new[]
			{
				SystemRight.PathAccess,
				SystemRight.EditVehicles,
				SystemRight.Immobilization,
			})
			.ToArray();
		/// <summary> Константный набор прав выдаваемых создателю геозоны </summary>
		public static readonly SystemRight[] DefaultZoneCreatorRights = DefaultNonRights
			.Union(new[]
			{
				SystemRight.ZoneAccess,
				SystemRight.EditZone,
				SystemRight.SecurityAdministration
			})
			.ToArray();
		/// <summary> Константный набор прав выдаваемых для доступа к геозоне </summary>
		public static readonly SystemRight[] DefaultZoneAccessRights = DefaultNonRights
			.Union(new[]
			{
				SystemRight.ZoneAccess,
			})
			.ToArray();

		/// <summary> Константный набор прав выдаваемых создателю группы геозон </summary>
		public static readonly SystemRight[] DefaultZoneGroupCreatorRights = DefaultNonRights
			.Union(new[]
			{
				SystemRight.ZoneAccess,
				SystemRight.EditZone,
				SystemRight.AddZoneToGroup,
				SystemRight.EditGroup,
				SystemRight.GroupAdministration
			})
			.ToArray();
		/// <summary> Константный набор прав выдаваемых для доступа к группе геозон </summary>
		public static readonly SystemRight[] DefaultZoneGroupAccessRights = DefaultNonRights
			.Union(new[]
			{
				SystemRight.ZoneAccess,
			})
			.ToArray();
		/// <summary> Константный набор прав на геозоны для корпоративных клиентов </summary>
		public static readonly SystemRight[] CorporateZoneRights = DefaultNonRights
			.Union(new[]
			{
				SystemRight.ZoneAccess,
				SystemRight.EditZone,
				SystemRight.AddZoneToGroup,
				SystemRight.EditGroup, /// SystemRight.EditGroup не является правом на зону, это право на группу
			})
			.ToArray();
		/// <summary> Константный набор прав на геозоны для частных клиентов </summary>
		public static readonly SystemRight[] PhysicalZoneRights = DefaultNonRights
			.Union(new[]
			{
				SystemRight.ZoneAccess,
				SystemRight.EditZone,
			})
			.ToArray();
		/// <summary> Константный набор прав необходимый для просмотра значений датчиков (все из набора) </summary>
		public static readonly SystemRight[] SensorViewRightsAll = DefaultNonRights
			.Union(new[]
			{
				SystemRight.VehicleAccess,
				SystemRight.ViewingSensorValues
			})
			.ToArray();
		/// <summary> Константный набор прав необходимый для редактирования датчиков (все из набора) </summary>
		public static readonly SystemRight[] SensorEditRightsAll = DefaultNonRights
			.Union(new[]
			{
				SystemRight.VehicleAccess,
				SystemRight.ManageSensors,
			})
			.ToArray();
		/// <summary> Константный набор прав для просмотра свойств трекера (любое из набора) </summary>
		public static readonly SystemRight[] TrackerViewRightsAny = DefaultNonRights
			.Union(new[]
			{
				SystemRight.ViewingTrackerAttributes,
				SystemRight.EditingTrackerAttributes,
				SystemRight.Immobilization,
				SystemRight.CommandAccess,
				SystemRight.ChangeDeviceConfigBySMS,
			})
			.ToArray();
		/// <summary> Константный набор прав для редактирования свойств трекера (любое из набора) </summary>
		public static readonly SystemRight[] TrackerEditRightsAny = DefaultNonRights
			.Union(new[]
			{
				SystemRight.EditingTrackerAttributes,
			})
			.ToArray();
		/// <summary> Роль: Частный клиент (физическое лицо) </summary>
		public static UserRole UfinPrivUser = new UserRole
		{
			UserRights       = DefaultAdmRights,
			DepartmentRights = DefaultNonRights,
			VehicleRights    = DefaultAdmRightsOnVehicle,
		};
		/// <summary> Роль: Пользователь корпоративного клиента </summary>
		public static UserRole UfinCorpUser = new UserRole
		{
			UserRights       = DefaultOprRights,
			DepartmentRights = DefaultOprRightsOnDepartment,
			VehicleRights    = DefaultNonRights,
		};
		/// <summary> Роль: Администратор корпоративного клиента </summary>
		public static UserRole UfinCorpAdmin = new UserRole
		{
			UserRights       = DefaultAdmRights,
			DepartmentRights = DefaultOprRightsOnDepartment, // Теперь администратор не имеет административных прав на департамент
			VehicleRights    = DefaultNonRights,
		};
		/// <summary> Роль: Партнер, как корпоративный клиент </summary>
		public static UserRole UfinCorpPartner = new UserRole
		{
			UserRights       = UfinCorpAdmin.UserRights
				.Union(new[]
				{
					SystemRight.ServiceManagement
				})
				.ToArray(),
			DepartmentRights = UfinCorpAdmin.DepartmentRights,
			VehicleRights    = UfinCorpAdmin.VehicleRights,
		};
		/// <summary> Роль: Супер администратора </summary>
		public static UserRole UfinSuperAdmin = new UserRole
		{
			UserRights       = DefaultAdmRights
				.Union(new[]
				{
					SystemRight.ServiceManagement
				})
				.ToArray(),
			DepartmentRights = DefaultAdmRightsOnDepartment,
			VehicleRights    = DefaultNonRights,
		};
	}
}