﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	public enum VehicleAttributeName
	{
		/// <summary> Название </summary>
		Name,
		/// <summary> Идентификатор трекера </summary>
		DeviceId,
		/// <summary> Тип трекера </summary>
		TrackerType,
		/// <summary> Номер телефона трекера </summary>
		Phone,
		/// <summary> Пароль трекера </summary>
		Password,
		///<summary> Ip адрес </summary>
		IpAddress,
		///<summary> Тип объекта </summary>
		VehicleKind,
		///<summary> Расход (км/л) </summary>
		FuelSpentStandart,
		///<summary> Расход (л/моточас) </summary>
		FuelSpentStandartMH,
		///<summary> Расход (км/м3) </summary>
		FuelSpentStandartGas,
		///<summary> Максимальная разрешенная скорость, км/ч </summary>
		MaxAllowedSpeed,
		///<summary> Маршрут </summary>
		Route,
		///<summary> Госномер </summary>
		PublicNumber,
		///<summary> Марка объекта </summary>
		VehicleType,
		///<summary> Объём бака </summary>
		FuelTankVolume,
		///<summary> Объём бака в литрах </summary>
		FuelTankGasVolume,
		///<summary> Тип топлива </summary>
		FuelType,
		///<summary> Имя </summary>
		OwnerFirstName,
		///<summary> Фамилия </summary>
		OwnerLastName,
		///<summary> Отчество </summary>
		OwnerSecondName,
		///<summary> Телефоны владельца 1 </summary>
		OwnerPhoneNumber1,
		///<summary> Телефоны владельца 2 </summary>
		OwnerPhoneNumber2,
		///<summary> Телефоны владельца 3 </summary>
		OwnerPhoneNumber3,
		///<summary> Картинка </summary>
		Image,
		///<summary> Особые отметки </summary>
		Notes,
		///<summary> Иконка </summary>
		Icon,
		///<summary> закончилась страховка </summary>
		InsuranceExpiry,
		///<summary> закончилась лицензия </summary>
		LicenseExpiry,
		///<summary> закончилось разрешение </summary>
		PermitExpiry,
		/// <summary> протокол по которому принимаются данные от трекера </summary>
		Protocol,
		/// <summary> Название точки доступа для подключения к Интернету </summary>
		ApnName,
		/// <summary> Имя пользователя для подключения к Интернету через точку доступа </summary>
		ApnUser,
		/// <summary> Пароль для подключения к Интернету через точку доступа </summary>
		ApnPassword,
		/// <summary> Vehicle Identification Number </summary>
		VIN,
		/// <summary> OnlineGeocoding, свойство объекта, которое разрешает поиск адреса при любом изменении координат в WebPush </summary>
		/// <remarks> Разрешается видеть и изменять только суперадмину, если выставлен параметр конфигурации "OnlineGeocoding4All" со значением true, то атрибут становится неизменяемым, а значение true для всех </remarks>
		OnlineGeocoding,
	}
}