﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	[Flags]
	public enum ShowVehiclesMode
	{
		None    = 0,      // Сняты все фильтры, ничего не показываем
		Online  = 1 << 0, // <=15мин
		Old1    = 1 << 1, // > 1*15мин и <= 1*60мин
		Old2    = 1 << 2, // > 1*60мин и <= 3*60мин
		Old3    = 1 << 3, // > 3*60мин
		Unknown = 1 << 4, // Нет данных
		All     = Online | Old1 | Old2 | Old3 | Unknown,
	}
}