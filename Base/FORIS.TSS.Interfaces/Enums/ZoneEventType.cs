﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Тип события, привязанного к геозоне </summary>
	[Serializable]
	public enum ZoneEventType
	{
		/// <summary> Вход в зону </summary>
		/// <remarks> Событие срабатывает, если объект входит в зону</remarks>
		Incoming  = 0,
		/// <summary> Выход из зоны </summary>
		/// <remarks> Событие срабатывает, если объект выходит из зоны</remarks>
		Outgoing  = 1,
		/// <summary> Объект в зоне </summary>
		/// <remarks> Событие срабатывает, если объект находится внутри зоны</remarks>
		InZone    = 2,
		/// <summary> Объект вне зоны</summary>
		/// <remarks> Событие срабатывает, если объект находится вне зоны</remarks>
		OutOfZone = 3,
	}
}