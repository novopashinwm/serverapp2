﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	public enum AttributeType
	{
		Number,
		String,
		Boolean,
		Enum,
		DateTime
	}
}