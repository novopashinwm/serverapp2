﻿namespace FORIS.TSS.BusinessLogic.Enums
{
    public enum TryConnectServiceAppResult
    {
        ServiceAppDoesNotExist,
        NoService,
        ServiceIsAlreadyUsed,
        Success
    }
}
