﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Результат обработки сообщения </summary>
	[Serializable]
	public enum MessageProcessingResult
	{
		/// <summary> Сообщение находится в недопустимом состоянии </summary>
		Unknown = 0,
		Received = 1,
		Processing = 2,
		Sent = 3,
		/// <summary> Отправлено push-уведомление. Используется для отправки СМС, только если push не может быть доставлен </summary>
		PushSent = 27,
		/// <summary> Сообщение успешно обработано </summary>
		Success = 18,

		ContactDoesNotExist = 4,
		UnsuccessfulBillingResult = 5,
		Throttled = 6,
		MessageQueueFull = 7,

		InvalidMessageLength = 8,
		InvalidDestinationAddress = 9,
		NoConnectionWithGateway = 10,
		HttpError = 11,
		Timeout = 12,
		ServiceTurnedOff = 13,
		Error = 14,
		DestinationIsNotSpecified = 15,

		/// <summary> Поставлено в очередь на отправку </summary>
		Queued = 16,
		/// <summary>Контакт получателя заблокирован, срочно или навсегда (например, для SMS - при переполнении очереди)</summary>
		DestinationContactIsLocked = 17,
		/// <summary> Контакт получателя не валидный, можно удалить из БД </summary>
		DestinationContactIsNotValid = 19,

		/// <summary> Сообщение доставлено пользователю </summary>
		Delivered = 20,
		#region Статусы для SMPP
		/// <summary> Сообщение является не доставляемым. </summary>
		Undeliverable = 21,
		/// <summary> Сообщение было удалено. </summary>
		Deleted = 22,
		/// <summary> Сообщение находится в отклоненном состоянии. </summary>
		Rejected = 23,
		#endregion

		/// <summary> Прочитано пользователем </summary>
		Read = 25,
		/// <summary> Отменено при отмене команды </summary>
		CancelByCommand = 26,
		/// <summary> Отсутствует идентификатор клиентского приложения </summary>
		MissingAppId = 27,
		/// <summary> Некорректный идентификатор клиентского приложения </summary>
		MismatchAppId = 28,
		/// <summary> Нет подходящего обработчика </summary>
		NoApproriateProcessor = 29,
		/// <summary> Этот шаблон сообщений не поддерживается для отправки </summary>
		UnsupportedTemplate = 30
	}
}