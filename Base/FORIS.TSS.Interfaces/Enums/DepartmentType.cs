﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Тип абонента - корпоративный клиент, физическое лицо </summary>
	/// <remarks> Служит для разграничения функционала по обслуживанию абонентов </remarks>
	[Serializable]
	public enum DepartmentType
	{
		/// <summary> Корпоративный сектор - юридические лица </summary>
		Corporate = 0,
		/// <summary> Массовый сектор - физические лица </summary>
		Physical  = 1,
	}
}