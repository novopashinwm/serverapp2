﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	[Serializable]
	public enum EffectiveRightPriority
	{
		Direct                         = 1,
		Group                          = 2,
		OperatorGroup                  = 3,
		OperatorGroupToGroup           = 4,
		Department                     = 5,
		OperatorGroupDepartment        = 6,
		AdministratorToDepartment      = 7,
		AdministratorGroupToDepartment = 8,
		AdministratorDirect            = 9, //TODO: возможно, следует поднять этот приоритет
	}
}