﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Тип адресата сообщения, влияет на интерпретацию колонки MESSAGE.DESTINATION_ID </summary>
	public enum MessageDestinationType
	{
		Unknown          =  0,
		/// <summary> SMS-шлюз с API на HTTP </summary>
		RedHatSmsGateway =  1,
		/// <summary> SMS-шлюз с API на SMPP</summary>
		Smpp             =  2,
		/// <summary> Отправка через платформу MPX (МТС Россия) с использованием Asid - F(MSISDN) - маскированного номера телефона </summary>
		MpxSms           =  3,
		/// <summary> Электронная почта </summary>
		Email            =  6,
		/// <summary> Оператор системы (пользователь) </summary>
		Operator         =  7,
		/// <summary> Сервер - сообщение отправлено на сервер - для входящих сообщений </summary>
		Server           =  8,
		/// <summary> Клиентское приложение для Android </summary>
		Android          =  9,
		/// <summary> Клиентское приложение для iOS </summary>
		AppleiOS         = 10,
		/// <summary> Клиентское приложение для iOS отладка </summary>
		AppleiOSDebug    = 13,
		/// <summary> Отправка запросов на платформу MPX LBS с использованием Asid - F(MSISDN) - маскированного номера телефона </summary>
		/// <remarks> Должна использоваться для отправки запросов местоположения и CDR </remarks>
		MpxLbs           = 11,
		/// <summary> Модем </summary>
		Modem            = 12,
		/// <summary> Отправка уведомления через push или sms, если push не может быть доставлен </summary>
		Notification     = 14,
		/// <summary> Отправка push уведомления в веб </summary>
		Web              = 15
	}
}