﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	public enum SetNewPasswordResult
	{
		Success,
		NewPasswordIsInvalid,
		OldPasswordIsWrong,
		NoConfirmedContact
	}
}