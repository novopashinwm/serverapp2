﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Тип позиции: GPS (по умолчанию), LBS, Yandex.Locator etc </summary>
	[Serializable]
	public enum PositionType
	{
		/// <summary> Позиция по данным спутников </summary>
		GPS         = 0,
		/// <summary> Позиция по данным сотовой сети на основании биллинга </summary>
		LBS         = 1,
		/// <summary> Позиция по данным службы Яндекс.Локатор </summary>
		Yandex      = 2,
		/// <summary>Позиция по данным службы Яндекс.Локатор - GSM сети</summary>
		YandexGsm   = 3,
		/// <summary>Позиция по данным службы Яндекс.Локатор - WiFi сети</summary>
		YandexWifi  = 4,
		/// <summary>Позицию задал пользователь</summary>
		UserDefined = 5
	}
}