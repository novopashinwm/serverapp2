﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Роль контакта у сообщения </summary>
	[Serializable]
	public enum MessageContactType
	{
		/// <summary> Получатель сообщения </summary>
		Destination = 1,
		/// <summary> Отправитель сообщения </summary>
		Source      = 2,
		/// <summary> Получатель сообщения - в копии - только для email </summary>
		Copy        = 3,
		/// <summary> Получатель сообщения - в скрытой копии - только для email </summary>
		HiddenCopy  = 4,
		/// <summary> Значение поля reply-to: для email - поле Reply-To, для SMS - номер телефона, от имени которого происходит отправка SMS </summary>
		ReplyTo     = 5,
		/// <summary> Контакт в теле сообщения </summary>
		Reference   = 6
	}
}