﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	public enum HttpMethod
	{
		Unspecified,
		Get,
		Post,
		Delete
	}
}