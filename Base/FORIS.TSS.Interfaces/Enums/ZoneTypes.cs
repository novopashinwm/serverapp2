﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	public enum ZoneTypes
	{
		Circle   = 1,
		Circles  = 2,
		Polygon  = 3,
		Corridor = 4
	}
}