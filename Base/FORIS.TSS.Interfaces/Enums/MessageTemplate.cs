﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	public enum MessageTemplate
	{
		/// <summary> Запрос на подключение функционала для корпоративных клиентов </summary>
		SwitchToCorporativeRequest,
		/// <summary> Вопрос в техподдержку </summary>
		QuestionToSupport,
		/// <summary> Запрос на расчёт стоимости </summary>
		CalculationRequest,
		/// <summary> Уведомление о потери прав на объект </summary>
		LossRightsToObject
	}
}