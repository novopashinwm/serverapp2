﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Тип контакта </summary>
	[Serializable]
	public enum ContactType
	{
		/// <summary>Адрес электронной почты</summary>
		Email    = 1,
		/// <summary>Номер телефона в открытом виде - MSISDN</summary>
		Phone    = 2,
		/// <summary>Идентификатор объекта наблюдения - Vehicle.Vehicle_ID.ToString()</summary>
		Vehicle  = 4,
		/// <summary>Идентификатор пользователя - Operator.Operator_ID.ToString()</summary>
		Operator = 5,
		/// <summary>Экземпляр приложения пользователя, установленного на Android</summary>
		Android  = 6,
		/// <summary>Экземпляр приложения пользователя, установленного на iOS</summary>
		Apple    = 7,
		/// <summary> Экземпляр приложения пользователя, установленного на iOS для отладки </summary>
		// ReSharper disable once InconsistentNaming
		iOsDebug = 8,
		/// <summary> Произвольная текстовая информация </summary>
		Text     = 9
	}
}