﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Тип используемого хранилища </summary>
	public enum StorageType
	{
		MSSQL      = 1,
		// Перенесено в .unused
		//PostgreSQL = 2,
	}
}