﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Типы сравнения скалярных величин </summary>
	[Serializable]
	public enum ScalarComparisonType
	{
		Equal       = 0x01,
		Less        = 0x02,
		EqualOrLess = 0x03,
		More        = 0x04,
		EqualOrMore = 0x05,
		NotEqual    = 0x06
	}
}