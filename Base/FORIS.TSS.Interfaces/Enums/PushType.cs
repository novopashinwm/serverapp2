﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Тип push-уведомления </summary>
	[Serializable]
	public enum PushType
	{
		/// <summary> Уведомление о событии, происшедшее с объектом наблюдения </summary>
		/// <remarks> Дополнительное поле - идентификатор объекта наблюдения</remarks>
		Notification = 0,
		/// <summary> Запрос о местоположении объекта наблюдения </summary>
		PositionRequest = 1,
		/// <summary> Предложение дружбы </summary>
		/// <remarks> Дополнительное поле - номер телефона абонента, предлагающего дружбу</remarks>
		ProposeFriendship = 2,
		/// <summary> Тревога </summary>
		/// <remarks> Дополнительное поле - идентификатор объекта наблюдения</remarks>
		Alarm = 3,
		/// <summary> Друг был зарегистрирован в системе </summary>
		FriendWasRegistered = 4,
		/// <summary> Расписание запросов было изменено </summary>
		DataUpdate = 5,
		/// <summary> Результат запроса </summary>
		RequestResult = 6,
		/// <summary> Ping-push для активации приложения </summary>
		/// <remarks>Сервер предполагает, что приложение на целевом устройстве заснуло было выгружено и отправляет этот push, 
		/// чтобы "разбудить" приложение</remarks>
		WakeUp = 7,
		/// <summary>Исчерпание пакета услуг</summary>
		/// <remarks>По нажатию на пуш, клиент должен перевести пользователя в раздел, где он сможет пополнить баланс</remarks>
		BillingServiceExhaust = 8
	}
}