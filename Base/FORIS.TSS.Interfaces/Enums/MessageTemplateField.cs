﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	[Serializable]
	public enum MessageTemplateField
	{
		TrackerNumber,
		Contacts,
		Question
	}
}