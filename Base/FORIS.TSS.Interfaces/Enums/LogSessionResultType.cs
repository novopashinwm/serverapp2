namespace FORIS.TSS.BusinessLogic.Enums
{
    public enum LogSessionResultType
    {
        Success = 1,

        ChangeDepartment = 2
    }
}