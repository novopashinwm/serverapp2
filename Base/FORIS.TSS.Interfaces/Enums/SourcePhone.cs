﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Перечисление номеров телефонов для отправки СМС </summary>
	public enum SourcePhone
	{
		/// <summary> Номер телефона SMS-сервиса для приёма ответов от устройств в международном формате </summary>
		ServiceInternationalPhoneNumber = 1,
		/// <summary> Номер телефона SMS-сервиса для приёма ответов от устройств в коротком формате, например, 6452 </summary>
		ServiceShortNumber              = 2,
		/// <summary> Название услуги, например "Ufin" </summary>
		ServiceName                     = 3
	}
}