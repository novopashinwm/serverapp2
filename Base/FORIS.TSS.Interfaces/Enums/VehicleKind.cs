﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	[Serializable]
	public enum VehicleKind : int
	{
		/// <summary> автобус </summary>
		Bus           = 1,
		/// <summary> легковой </summary>
		Vehicle       = 2,
		/// <summary> Грузовой </summary>
		Truck         = 3,
		/// <summary> Прицеп </summary>
		Trailer       = 4,
		/// <summary> Персональный трекер </summary>
		Tracker       = 5,
		/// <summary> Мобильный телефон, у которого определение возможно только по LBS </summary>
		MobilePhone   = 7,
		/// <summary> Источник питания (дизель-генератор и т.п)</summary>
		PowerSource   = 8,
		/// <summary> Веб-камера </summary>
		WebCamera     = 9,
		/// <summary> Счетчик ресурсов (электроэнергии, воды, газа etc) </summary>
		ResourceMeter = 10,
		/// <summary> АЗС </summary>
		GasStation    = 11,
		/// <summary> Смартфон с установленным приложением Ufin</summary>
		Smartphone    = 12
	}
}