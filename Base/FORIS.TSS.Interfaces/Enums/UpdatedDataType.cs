﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
    [Serializable]
    public enum UpdatedDataType
    {
        Schedule = 1,
        BillingServices = 2
    }
}
