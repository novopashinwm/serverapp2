﻿using System;
using System.Runtime.Serialization;

namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Тип нарушения вождения </summary>
	[Serializable]
	[DataContract]
	public enum ViolationOfDrivingType
	{
		/// <summary> Курение </summary>
		[EnumMember] Smoking,
		/// <summary> Отвлечение </summary>
		[EnumMember] Distraction,
		/// <summary> Сонливость </summary>
		[EnumMember] Sleepiness,
	}
}