﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Блокировка биллинга </summary>
	public class BillingBlocking
	{
		public DateTime     BlockingDate;
		public BlockingType Type;
	}
}