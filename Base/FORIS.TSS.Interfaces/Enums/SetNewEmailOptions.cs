﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FORIS.TSS.BusinessLogic.Enums
{
    [Serializable, Flags]
    public enum SetNewEmailOptions
    {
        None = 0x0,
        CheckConfirmationKey = 0x1,
    }
}
