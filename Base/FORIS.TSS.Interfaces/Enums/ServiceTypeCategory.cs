﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Перечисление возможных типов услуг, оказываемых абоненту </summary>
	public enum ServiceTypeCategory
	{
		/// <summary> Определение местоположения по данным от бортовых терминалов ГЛОНАСС/GPS </summary>
		GNSS,
		/// <summary> Определение местоположения по данным от сотовых телефонов с номеронабирателем, оснащенных ГЛОНАСС/GPS </summary>
		SoftTracker,
		/// <summary> SMS-уведомления </summary>
		SMS,
		/// <summary> Определение местоположения по данным сотовой сети </summary>
		LBS,
		/// <summary> Общая услуга без каких-либо особенностей; настраивается администратором системы </summary>
		Generic
	}
}