﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	[Serializable]
	public class RenderedService
	{
		public RenderedServiceType Type;
		public string              Name;
		public int                 Quantity;
	}
}