﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Статус актуальности данных от устройства </summary>
	public enum OnlineStatus
	{
		/// <summary> Услуга заблокирована </summary>
		ServiceIsBlocked,
		/// <summary> Услуга не подключена или удалена </summary>
		NoService,
		/// <summary> Устройство никогда не передавало данных </summary>
		Never,
		/// <summary> Не передавало данные на сервер в течение более чем N минут </summary>
		Offline,
		/// <summary> Устройство онлайн, но передаёт старые данные (сливает чёрный ящик) </summary>
		Dumping,
		/// <summary> Устройство онлайн, передаёт данные, но без координат - вероятно, плохой сигнал GPS </summary>
		PositionIsOutOfDate,
		/// <summary> Устройство онлайн, передаёт данные и координаты </summary>
		Online
	}
}