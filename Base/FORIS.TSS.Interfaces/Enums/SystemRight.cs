﻿using System;
using System.ComponentModel;

namespace FORIS.TSS.BusinessLogic
{
	public class RightDescriptionAttribute : DescriptionAttribute
	{
		public RightDescriptionAttribute(string description) : base(description)
		{
		}
	}
	[Serializable]
	public enum SystemRight : int
	{
		[RightDescription("Задаёт право доступа к модулю \"Управление сервером\". Оператор ,имеющий данное право видит все объекты системы и имеет доступ ко всем модулям и операциям.")]
		ServerAdministration        =   1,
		/// <summary> 2: Управление правами на объекта </summary>
		SecurityAdministration      =   2,
		[Obsolete("Нет в коде")] EditRouteAndWaybill         =   3,
		[Obsolete("Нет в коде")] EditWaybill                 =   4,
		[Obsolete("Нет в коде")] ViewWaybillHistory          =   5,
		/// <summary> 6: принудительно запрашивать местоположение (платная услуга) </summary>
		[Obsolete("Нет в коде")] ViewVehicleCurrentStatus    =   6,
		/// <summary>7: Редактирование объекта наблюдения / добавление объектов наблюдения в департамент</summary>
		EditVehicles                =   7,
		[Obsolete("Нет в коде")] EditDrivers                 =   8,
		/// <summary>Редактирование личных точек интереса</summary>
		[Obsolete("Нет в коде")] EditPoints                  =   9,
		[Obsolete("Нет в коде")] ViewAnalytic                =  10,
		[Obsolete("Нет в коде")] DispatcherRoll              =  11,
		[Obsolete("Нет в коде")] AccessToTreeOnOperatorMail  =  12,
		[Obsolete("Нет в коде")] ReprintWaybill              =  13,
		[Obsolete("Нет в коде")] Roster                      =  14,
		[Obsolete("Нет в коде")] JournalAccess               =  15,
		SecurityMapGis              =  16,
		[Obsolete("Нет в коде")] EditStatusAndPenalty        =  17,
		[Obsolete("Нет в коде")] ReceiveMessages             =  18,
		/// <summary> Право на редактирование геозон </summary>
		EditZone                    =  19,
		/// <summary> 20: Доступ по чтению к номеру телефона сим-карты, установленной в бортовом терминале - поле Controller.Phone </summary>
		/// <remarks> Заменяется/объединяется <see cref="Immobilization"/> </remarks>
		[Obsolete("Объединено с Immobilization=112")]
		ViewControllerPhone         =  20,
		/// <summary> 21: Доступ по чтению к идентификатору устройства - поле Controller.Controller_Info.Device_ID </summary>
		/// <remarks> Заменяется/объединяется <see cref="Immobilization"/> </remarks>
		[Obsolete("Объединено с Immobilization=112")]
		ViewControllerDeviceID      =  21,
		/// <summary>Редактирование общедоступных точек интереса</summary>
		EditPublicPoints            =  22,
		/// <summary> редактирование телефона контроллера </summary>
		/// <remarks> Заменяется/объединяется <see cref="ChangeDeviceConfigBySMS"/> </remarks>
		[Obsolete("Объединено с ChangeDeviceConfigBySMS=116, которое должно быть потом переименовано в ChangeSettings")]
		EditControllerPhone         =  23,
		/// <summary> Редактирование идентификатора устройства </summary>
		/// <remarks> Заменяется/объединяется <see cref="ChangeDeviceConfigBySMS"/> </remarks>
		[Obsolete("Объединено с ChangeDeviceConfigBySMS=116, которое должно быть потом переименовано в ChangeSettings")]
		EditControllerDeviceID      =  24,
		/// <summary> Доступ к паролю устройства </summary>
		[Obsolete("Нет в коде")] ControllerPasswordAccess    =  25,
		#region Работа с трекером
		/// <summary> Разрешение на добавление трекеров </summary>
		AddingTracker               =  30,
		/// <summary> Просмотр атрибутов трекера, разрешение редактировать атрибуты объекта на закладке Трекер </summary>
		ViewingTrackerAttributes    =  31,
		/// <summary> Редактирование атрибутов трекера, разрешение редактировать атрибуты объекта на закладке Трекер </summary>
		EditingTrackerAttributes    =  32,
		#endregion Работа с трекером
		[Obsolete("Нет в коде")] DriverAccess                = 100,
		[Obsolete("Нет в коде")] RouteAccess                 = 101,
		/// <summary> 102: Доступ к объекту наблюдения </summary>
		VehicleAccess               = 102,
		/// <summary> 103: Доступ к отчетам </summary>
		ReportsAccess               = 103,
		/// <summary> 104: Доступ к департаментам </summary>
		DepartmentsAccess           = 104,
		/// <summary> 105: Доступ к геозонам/просмотр геозон </summary>
		ZoneAccess                  = 105,
		/// <summary> Изменение нормативов пробега между зонами </summary>
		[Obsolete("Нет в коде")] EditZoneDistanceStandart    = 106,
		/// <summary>Редактирование названия, описания и состава группы.</summary>
		EditGroup                   = 109,
		/// <summary> Право на добавление геозоны в группу геозон. </summary>
		AddZoneToGroup              = 113,
		/// <summary>Управление услугами: заливка информации по подключению/отключению услуг</summary>
		ServiceManagement           = 110,
		/// <summary>Определять местоположение за счет определяемого телефона</summary>
		/// <remarks>Если оператор обладает этим правом на объект наблюдения,
		/// то при выполнении команд определения местоположения объекта по вышкам сотовой связи
		/// со счета телефона списываются средства, связанные с номером телефона,
		/// чье местоположение определяется.
		/// Если такого права нет, то средства списываются с номера телефона оператора
		/// Право разрешено только для корпоративных абонентов, оно должно быть отключено при смене категории абонента
		/// </remarks>
		PayForCaller                = 111,
		/// <summary> Иммобилизация: выключение электричества или подачи топлива </summary>
		/// <remarks> Остается самим собой и заменяет/объединяет <see cref="ViewControllerPhone"/> и <see cref="ViewControllerDeviceID"/> </remarks>
		Immobilization              = 112,
		/// <summary> Право на выполнение команд помеченных данным правом </summary>
		CommandAccess               = 114,
		/// <summary> Доступ пользователя к возможности просмотра пути. По-умолчанию доступ есть </summary>
		PathAccess                  = 115,
		///<summary> Изменение настроек устройства с помощью отправки sms сообщений (возможно потом будет использована только для команды Setup) </summary>
		/// <remarks> Остается самим собой и заменяет <see cref="EditControllerPhone"/> и <see cref="EditControllerDeviceID"/> </remarks>
		/// TODO: Должно быть потом переименовано в ChangeSettings
		ChangeDeviceConfigBySMS     = 116,
		/// <summary>Управление настройками датчиков объекта наблюдения</summary>
		ManageSensors               = 117,
		/// <summary>Таким пользователям можно давать и забирать права на объекты в рамках своего клиента,
		/// но нельзя их удалять, менять атрибуты и т.п.</summary>
		Friendship                  = 118,
		/// <summary>Управление правами на группу</summary>
		/// <remarks>Проблема - права на группу транслируются в права на объекты, содержащиеся в группе.
		/// Тогда если пользователю дали право на объект, он может получить административный доступ к объекту,
		/// просто добавив объект в любую принадлежащую ему группу.
		/// Чтобы этого не произошло, пользователям разрешается управлять правами только тех групп,
		/// на которые есть право GroupAdministration (по сути Owner).
		/// Аналогично <see cref="EditGroup"/> не даёт права на редактирование объекта.
		/// </remarks>
		GroupAdministration         = 119,
		/// <summary>Право не видеть такой объект наблюдения в списке объектов</summary>
		/// <remarks>Это право может быть установлено любым пользователем только самому себе на произвольный объект наблюдения, в том числе чужой</remarks>
		Ignore                      = 120,
		/// <summary>Право управлять услугой Видеоконтроль в рамках данного клиента</summary>
		/// <remarks>Вьюга поддерживает только один аккаунт для услуги Видеонаблюдение, а на клиенте может быть несколько администраторов.
		/// Это право позволяет понимать, является ли пользователь администратором Вьюги</remarks>
		[Obsolete("Нет в коде")] ViugaAdmin                  = 121,
		#region Нужно только для совместимости и переноса
		/// <summary> Доступ к менеджеру файлов, скачиванию файлов и построению отчетов по файлам </summary>
		[Obsolete("Нет в коде")] FileAccess                  = 122,
		/// <summary> Разрешение вычислять ожидаемое время прибытия </summary>
		[Obsolete("Нет в коде")] CalcArrivalTime             = 123,
		/// <summary> Просмотр сотрудников </summary>
		[Obsolete("Нет в коде")] ViewEmployee                = 124,
		/// <summary> Редактирование сотрудников </summary>
		[Obsolete("Нет в коде")] EditEmployee                = 125,
		/// <summary> Просмотр идентификаторов сотрудников </summary>
		[Obsolete("Нет в коде")] ViewIdentifierEmployee      = 126,
		/// <summary> Редактирование идентификаторов сотрудников </summary>
		[Obsolete("Нет в коде")] EditIdentifierEmployee      = 127,
		#endregion Нужно только для совместимости и переноса
		/// <summary> Анонимный доступ по ссылке </summary>
		ShareLinkAccess             = 201,
		/// <summary> EN: "Data relay to other servers", RU: "Трансляция на другие сервера" </summary>
		TrackerDataRelay            = 202,
		/// <summary> EN: "Estimated time arrival",      RU: "Оценочное время прибытия" </summary>
		EstimatedTimeArrival        = 203,
		/// <summary> EN: "API access",                  RU: "API доступ" </summary>
		ApiObjectAccess             = 204,
		/// <summary> EN: "View sensor values",          RU: "Просмотр значений датчиков" </summary>
		ViewingSensorValues         = 205,
		/// <summary> Это право является составным для физ. лица. используется только для свертки и локализации </summary>
		[Obsolete("Нет в коде")] VehicleManagement           = 301,
	}
}