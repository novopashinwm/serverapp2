﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	public enum ReportTypeEnum
	{
		Acrobat        = 1,
		Excel          = 2,
		Html           = 3,
		RichTextFormat = 4,
		Word           = 5,
		Crystal        = 6,
		Xml            = 7
	}
}