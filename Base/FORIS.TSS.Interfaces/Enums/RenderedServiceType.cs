﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Тип оказанной разовой услуги </summary>
	public enum RenderedServiceType
	{
		/// <summary> Услуга определения местоположения по LBS </summary>
		LBS         = 0,
		/// <summary> Услуга SMS-уведомлений </summary>
		SMS         = 1,
		/// <summary> Услуга отправки SMS через MPX </summary>
		[Obsolete("Следует использовать SMS")]
		MPXSMS      = 2,
		/// <summary> Услуго-день </summary>
		ServiceDays = 3
	}
}