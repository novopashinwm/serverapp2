﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Способности устройства </summary>
	[Serializable]
	public enum DeviceCapability
	{
		[Obsolete("Do not use. It was left for compatibility with old serialized data")]
		None = 0,
		/// <summary> Определение местоположения по запросу </summary>
		AllowAskPosition = 1,
		[Obsolete("Теперь определяется наличием настроенных датчиков, оставлено для совместимости сериализации")]
		IgnitionMonitoring = 2,
		SpeedMonitoring = 3,
		/// <summary> Отправка текстовых сообщений на устройство </summary>
		AllowSendSms = 4,
		/// <summary> Управление бортовым электропитанием - включение и отключение </summary>
		[Obsolete("Определяется через список доступных команд")]
		ManageElectricity = 5,
		/// <summary> Управление подачей топлива - перекрыть подачу и возобновить </summary>
		[Obsolete("Определяется через список доступных команд")]
		ManageFuelSupply = 6,
		/// <summary> Изменение настроек устройства </summary>
		ChangeDeviceSettings = 7,
		/// <summary> Захват изображения с видео-камеры </summary>
		CapturePicture = 8,
		/// <summary> Просмотр видео в реальном времени </summary>
		LivePicture = 9,
		/// <summary> Просмотр архивного видео </summary>
		VideoArchive = 10,
		/// <summary> Получение обратного вызова от устройства </summary>
		PhoneCallBack = 11,
		/// <summary> Возможность слежения за объектом по расписанию </summary>
		ScheduledTracking = 12
	}
}