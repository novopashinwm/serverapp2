﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	/// <summary> Тип блокировки приложения обслуживания </summary>
	public enum BlockingType
	{
		/// <summary> Блокировка по данным системы биллинга </summary>
		Billing = 0,
		/// <summary> Ручная блокировка администратором </summary>
		Manual  = 1,
	}
}