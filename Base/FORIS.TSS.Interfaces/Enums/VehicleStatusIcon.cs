﻿namespace FORIS.TSS.BusinessLogic.Enums
{
	public enum VehicleStatusIcon
	{
		/// <summary> тревога </summary>
		Alarm,
		/// <summary> Отключен </summary>
		Off,
		/// <summary> данные LBS </summary>
		MLP,
		/// <summary> припаркован, но движется </summary>
		ParcingMoving,
		/// <summary> Alarm + ParcingMoving </summary>
		Park,
		/// <summary> припаркован </summary>
		Parking,
		/// <summary> Остальные </summary>
		Rest,
		RestAngle,
		/// <summary> Остановка </summary>
		Stop,
		/// <summary> Позиция определена с помощью сервиса Яндекс.Локатор </summary>
		Yandex,
		/// <summary> Иммобилизован - двигатель заблокирован </summary>
		Immobilized,
	}
}