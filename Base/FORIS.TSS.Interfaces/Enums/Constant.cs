﻿using System;

namespace FORIS.TSS.BusinessLogic.Enums
{
	[Serializable]
	public enum Constant
	{
		/// <summary>Название точки доступа для подключения к Интернету</summary>
		NetworkAPN,
		/// <summary>Имя пользователя для подключения к точке доступа</summary>
		NetworkAPNUser,
		NetworkAPNPassword,
		ServerIP,
		ServerPort,
		/// <summary>Номер телефона SMS-сервиса для приёма ответов от устройств в международном формате</summary>
		ServiceInternationalPhoneNumber,
		/// <summary> Название услуги, например "Ufin" </summary>
		ServiceName,
		/// <summary>Минимальный интервал времени, за пределами которого позиция считается остановкой</summary>
		StopPeriod,
		/// <summary>Период, в течении которого запрос дружбы считается устаревшим</summary>
		FriendRequestExpired,
		/// <summary> Время жизни подтверждения запроса </summary>
		ConfirmationLifetimeMinutes,
		/// <summary> Идентификатор "клиента", в которого попадают все удаленные объекты наблюдения </summary>
		RemovedDepartmentExtId,
		/// <summary> Нужно ли добавлять правила по умолчанию </summary>
		AddDefaultRules,
		/// <summary> Сайт веб-приложения </summary>
		AppDomain,
		/// <summary> Время устаревания push-сообщений, через которое нужно отправить смс-сообщение </summary>
		OutdateSeconds,
		/// <summary> Пользователи, которые могут отправлять любые SMS, без шаблона </summary>
		UsersAllowedToSendAnySms,
		/// <summary>Нужно ли отправлять уведомление вида: Вы запросили местоположение Г. Поттер</summary>
		SendNotificationAboutCommandStartToSender,
		/// <summary>Нужно ли отправлять уведомление вида: Лорд Вольдеморт ищет вас</summary>
		SendNotificationAboutCommandStartToTarget,
		/// <summary> Интервал между повторным срабатыванием одного и того же правила </summary>
		RuleEventIntervalSeconds,
		/// <summary> Если включено, то сервер хранит только последний адрес для доставки пушей </summary>
		SinglePushToAppDestinationPerUser,
		/// <summary> Количество попыток ввода контрольного кода для верификации контакта </summary>
		VerifyAttemptCount,
		/// <summary>Modulus публичного ключа API Android</summary>
		AndroidAppKeyModulus,
		/// <summary>Exponent публичного ключа API Android</summary>
		AndroidAppKeyExponent,
		/// <summary> Закрытый ключ учетной записи разработчика Android </summary>
		AndroidPrivateKey,
		/// <summary> Секрет для проверки платежей Apple, созданных в отладочной версии приложения </summary>
		AppleSandboxSharedSecret,
		/// <summary> Секрет для проверки платежей Apple, созданных в боевой версии приложения </summary>
		AppleSharedSecret,
		/// <summary> Список номеров телефонов технической поддержки </summary>
		TechSupportMsisdns,
		/// <summary> Последняя обработанная запись об оказанной услуге услуга </summary>
		LastRenderedServiceItemID,
		/// <summary> Наименование предприятия </summary>
		ESTABLISHMENT_NAME,
		/// <summary> Адрес предприятия </summary>
		ESTABLISHMENT_ADDRESS,
		/// <summary> Телефон предприятия </summary>
		ESTABLISHMENT_PHONE,
		/// <summary> Подпись создателя отчета </summary>
		REPORT_CREATOR_SIGNATURE,
		/// <summary> OnlineGeocoding4All Значение указывает, включен ли режим для всех объектов запрашивать адреса у поставщика услуг геокодинга </summary>
		OnlineGeocoding4All,
	}
}