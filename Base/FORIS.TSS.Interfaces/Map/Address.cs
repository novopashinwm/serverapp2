﻿using System;
using System.Diagnostics;
using System.Xml;

namespace FORIS.TSS.BusinessLogic.Map
{
	/// <summary>
	/// Содержит указание на точку вызова и комментарии
	/// </summary>
	[Serializable]
	public class Address
	{
		public readonly GeoPoint Location;

		public readonly string Note;

		public bool IsEmpty
		{
			get { return this.Location.IsEmpty; }
		}

		public Address(GeoPoint location, string note)
		{
			this.Location = location;
			this.Note = note;
		}

		public Address(string xml)
		{
			try
			{
				XmlDocument document = new XmlDocument();
				document.LoadXml(xml);

				XmlElement addressElement = (XmlElement)document.GetElementsByTagName("address")[0];

				XmlElement locationElement = (XmlElement)addressElement.GetElementsByTagName("location")[0];
				XmlElement noteElement = (XmlElement)addressElement.GetElementsByTagName("note")[0];

				this.Location = GeoPoint.CreateGeo(
					geoX: double.Parse(locationElement.Attributes["longitude"].Value),
					geoY: double.Parse(locationElement.Attributes["latitude"].Value));

				this.Note = noteElement.InnerText;
			}
			catch (Exception ex)
			{
				this.Location = GeoPoint.Empty;
				this.Note = string.Empty;

				Debug.WriteLine(this.GetType() + ".ctor() " + ex.Message);
			}
		}

		public string Xml
		{
			get
			{
				XmlDocument document = new XmlDocument();
				XmlElement addressElement = document.CreateElement("address");

				XmlElement locationElement = document.CreateElement("location");

				XmlAttribute longitudeAttribute = document.CreateAttribute("longitude");
				longitudeAttribute.Value = this.Location.X.ToString();

				XmlAttribute latitudeAttribute = document.CreateAttribute("latitude");
				latitudeAttribute.Value = this.Location.Y.ToString();

				locationElement.Attributes.Append(longitudeAttribute);
				locationElement.Attributes.Append(latitudeAttribute);

				XmlElement noteElement = document.CreateElement("note");
				noteElement.InnerText = this.Note;

				addressElement.AppendChild(locationElement);
				addressElement.AppendChild(noteElement);

				document.AppendChild(addressElement);


				//StringWriter writer = new StringWriter();

				//    XmlWriter xmlWriter = XmlTextWriter.Create( writer );

				//    xmlWriter.Close();

				//    writer.Close();

				//return writer.ToString();
				return document.InnerXml;
			}
		}

		public override string ToString()
		{
			return this.Note;
		}

		#region Empty

		public static readonly Address Empty;

		static Address()
		{
			Address.Empty = new Address(GeoPoint.Empty, string.Empty);
		}

		#endregion // Empty

		#region GetHashCode(), Equals(), operator ==, operator !=

		public override int GetHashCode()
		{
			return Location != null ? Location.GetHashCode() : 0;
		}

		public override bool Equals(object obj)
		{
			if (obj is Address)
			{
				Address address = (Address)obj;

				return this.Location == address.Location;
			}

			return false;
		}

		public static bool operator ==(Address address1, Address address2)
		{
			return object.Equals(address1, address2);
		}

		public static bool operator !=(Address address1, Address address2)
		{
			return !object.Equals(address1, address2);
		}

		#endregion // GetHashCode(), Equals(), operator ==, operator !=
	}
}