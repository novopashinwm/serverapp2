﻿using System;

namespace FORIS.TSS.BusinessLogic.Map
{
	/// <summary> Географическая точка хранит координаты в глобальной и локальной системах <summary>
	[Serializable]
	public class GeoPoint
	{
		private readonly double x;
		/// <summary> Долгота </summary>
		public double X
		{
			get { return x; }
		}
		private readonly double y;
		/// <summary> Широта </summary>
		public double Y
		{
			get { return y; }
		}
		public bool IsEmpty
		{
			get { return double.IsNaN(x) && double.IsNaN(y); }
		}
		[NonSerialized]
		private bool planarCalculated;
		[NonSerialized]
		private DPoint planar;
		public DPoint Planar
		{
			get
			{
				if (!planarCalculated)
				{
					Geo.geoToPlanar(x, y, out planar);

					planarCalculated = true;
				}

				return planar;
			}
		}
		/// <summary> Инициализирует точку с заданными координатами глобальной системы </summary>
		/// <param name="x"> Долгота </param>
		/// <param name="y"> Широта </param>
		private GeoPoint(double x, double y)
		{
			// Глобальные координаты
			this.x = x;
			this.y = y;
		}

		#region Equals(), GetHashCode(), operator ==, operator !=

		public override bool Equals(object obj)
		{
			if (obj is GeoPoint)
			{
				GeoPoint point = (GeoPoint)obj;

				return x == point.x && y == point.y;
			}

			return false;
		}
		public override int GetHashCode()
		{
			return x.GetHashCode() + 29 * y.GetHashCode();
		}
		public static bool operator !=(GeoPoint point1, GeoPoint point2)
		{
			return !object.Equals(point1, point2);
		}
		public static bool operator ==(GeoPoint point1, GeoPoint point2)
		{
			return object.Equals(point1, point2);
		}

		#endregion  Equals(), GetHashCode(), operator ==, operator !=

		#region static Create

		/// <summary> Конструктор </summary>
		/// <param name="geoX"> Долгота (Lng) </param>
		/// <param name="geoY"> Широта  (Lat) </param>
		/// <returns></returns>
		public static GeoPoint CreateGeo(double geoX, double geoY)
		{
			return new GeoPoint(geoX, geoY);
		}
		public static GeoPoint CreatePlanar(float planarX, float planarY)
		{
			double geoX;
			double geoY;

			Geo.planarToGeo(planarX, planarY, out geoX, out geoY);

			return new GeoPoint(geoX, geoY);
		}
		public static GeoPoint CreatePlanar(DPoint planarPoint)
		{
			return GeoPoint.CreatePlanar(planarPoint.x, planarPoint.y);
		}

		#endregion static Create

		public static readonly GeoPoint Empty = new GeoPoint(double.NaN, double.NaN);
	}
}