﻿using System;
using System.Collections;

namespace FORIS.TSS.BusinessLogic.Map
{
	/// <summary> Функции преобразования географических координат (WGS84) в локальные метровые координаты </summary>
	public class Geo
	{
		internal const double kx = 063166; //111324.72603;
		internal const double ky = 111411; //197104.85314;

		internal const double origCenterX = 38.000; // ( 41.00 + 35.00) / 2 = 38.000
		internal const double origCenterY = 55.665; // ( 57.33 + 54.00) / 2 = 55.665

		readonly float[] geo = new float[10]
		{
			2.89510866630289E+32f, 3.50205541649329E-12f,
			1.80114793122323E+25f, 3.38803551186428E-12f,
			4.84456568460734E+27f, 1.21406087226884E+33f,
			4.85854109611686E+30f, 1.75635012853238E+25f,
			6.13317441410025E+28f, 2.74870113605582E+20f
		};
		/// <summary> Преобразовать координаты точки из глобальных (WGS84) в локальные  в метрах </summary>
		/// <param name="geoX"> долгота </param>
		/// <param name="geoY"> широта </param>
		/// <param name="planar"> точка на карте </param>
		public static void geoToPlanar(double geoX, double geoY, out DPoint planar)
		{
			planar.x = (float)(kx * (geoX - origCenterX));
			planar.y = (float)(ky * (geoY - origCenterY));
		}
		/// <summary> Преобразовать координаты точки из локальных метровых в глобальные (WGS84) </summary>
		/// <param name="planar"> точка на карте </param>
		/// <param name="geoX"> долгота </param>
		/// <param name="geoY"> широта </param>
		public static void planarToGeo(DPoint planar, out double geoX, out double geoY)
		{
			Geo.planarToGeo(planar.x, planar.y, out geoX, out geoY);
		}
		public static void planarToGeo(double planarX, double planarY, out double geoX, out double geoY)
		{
			geoX = planarX / kx + origCenterX;
			geoY = planarY / ky + origCenterY;
		}
	}
	/// <summary> Геометрическая точка с действительными координатами </summary>
	public struct DPoint
	{
		/// <summary> Координата X точки </summary>
		public float x;
		/// <summary> Координата Y точки </summary>
		public float y;
		/// <summary> Инициализирует точку с заданными координатами </summary>
		public DPoint(float x, float y)
		{
			this.x = x;
			this.y = y;
		}
		/// <summary> Вычисляет расстояние между этой точкой и заданной </summary>
		/// <returns> Возвращает действительное расстояние до точки point </returns>
		public float distanceTo(DPoint point)
		{
			float dx = x - point.x;
			float dy = y - point.y;
			return (float)Math.Sqrt(dx * dx + dy * dy);
		}
		public bool IsEmpty
		{
			get { return float.IsNaN(x) && float.IsNaN(y); }
		}
		/// <summary> Устанавливает обе координаты точки в заданные значения </summary>
		public void setXY(float x, float y)
		{
			this.x = x;
			this.y = y;
		}
		/// <summary> Возвращается координаты точки </summary>
		public void getXY(ref float x, ref float y)
		{
			x = this.x;
			y = this.y;
		}
		#region Equals(), GetHashCode()
		public override bool Equals(object obj)
		{
			if (obj is DPoint)
			{
				DPoint point = (DPoint)obj;
				return x == point.x && y == point.y;
			}

			return false;
		}
		public override int GetHashCode()
		{
			return x.GetHashCode() + 29 * y.GetHashCode();
		}
		#endregion Equals(), GetHashCode()
		public static readonly DPoint Empty = new DPoint(float.NaN, float.NaN);
	}
	public static class DPointExtensions
	{
		public static float DistanceTo(this DPoint src, DPoint dst)
		{
			return src.distanceTo(dst);
		}
	}
	/// <summary> Массив точек с координатами </summary>
	public struct DPointList
	{
		/// <summary>
		/// Массив координат точек.
		/// </summary>
		public DPoint[] points;
		/// <summary>
		/// Задать кол-во точек.
		/// </summary>
		/// <param name="size">Новое кол-во точек</param>
		public void setSize(int size)
		{
			points = new DPoint[size];
		}
		public DPoint[] removeLast(int count)
		{
			DPoint[] points = new DPoint[this.points.Length - count];
			for (int i = 0; i < this.points.Length - count; i++)
				points[i] = this.points[i];
			return points;
		}
		/// <summary>
		/// Вычисляет координаты точки, заданной параметром t на этой полилинии.
		/// </summary>
		/// <param name="t">целая часть определяет номер отрезка в полилинии, дробная - точную позицию внутри отрезка; должно быть в пределах [0..points.Length]</param>
		/// <returns>Возвращает координаты точки</returns>
		public DPoint calcPointByT(float t)
		{
			int aIndex = (int)Math.Floor(t);
			DLineMath l = new DLineMath();
			l.create(ref points[aIndex], ref points[aIndex + 1]);
			return l.calcXY(t - aIndex);
		}
		/// <summary>
		/// Вычисляет координаты точки и её параметр (t), находящейся на расстоянии dist вдоль периметра 
		/// этой полилинии, начиная от точки, расположенной на этой же полилинии, заданной параметром fromT.
		/// </summary>
		/// <param name="fromT">параметр исходной точки, от которой откладывается заданное расстояние (dist)</param>
		/// <param name="dist">Расстояние вдоль периметра</param>
		/// <param name="t">Возвращает параметр найденной точки</param>
		/// <returns>Возвращает true, если искомая точка находится в рамках полилинии</returns>
		public bool calcPointTAtDistanceFrom(float fromT, float dist, out float t)
		{
			int fromP1Index = (int)Math.Floor(fromT);
			DLineMath l = new DLineMath();
			l.create(ref points[fromP1Index], ref points[fromP1Index + 1]);
			DPoint fromP = l.calcXY(fromT - (float)fromP1Index);
			dist += points[fromP1Index].distanceTo(fromP);
			do
			{
				l.create(ref points[fromP1Index], ref points[fromP1Index + 1]);
				float pl = l.pieceLength();
				if (pl < dist)
					dist -= pl;
				else
				{
					DPoint p;
					t = fromP1Index + l.calcPointAtDistFromA(dist, out p);
					return true;
				}
				fromP1Index++;
			}
			while (fromP1Index < points.Length - 1);
			t = fromP1Index;
			return false;
		}
		/// <summary>
		/// Вычислить расстояние вдоль полилинии (представленной этим списком точек) 
		/// между точками на ней, заданными параметрически.
		/// </summary>
		/// <param name="startT">Первая точка на полилинии</param>
		/// <param name="endT">Вторая точка на полилинии</param>
		/// <returns>Вычисленное расстояние</returns>
		public float calcDistanceBetweenTs(float startT, float endT)
		{
			int s = (int)Math.Ceiling(startT);
			int e = (int)Math.Floor(endT);

			if (e < s)
				return calcPointByT(startT).distanceTo(calcPointByT(endT));

			float dist = 0;
			for (int i = s; i < e; i++)
				dist += points[i].distanceTo(points[i + 1]);

			if (startT != (float)s)
				dist += calcPointByT(startT).distanceTo(points[s]);

			if (endT != (float)e)
				dist += calcPointByT(startT).distanceTo(points[s]);

			return dist;
		}
		/// <summary>
		/// Найти самый длинный отрезок в пределах части полилинии, заданной параметрически 
		/// границами startT и endT. И вернуть центр найденного отрезка в параметрическом представлении.
		/// </summary>
		public float findLargestPieceCenter(float startT, float endT)
		{
			int s = (int)Math.Ceiling(startT);
			int e = (int)Math.Floor(endT);

			if (e < s)
				return (startT + endT) / 2;

			float foundT = -1;
			float foundDist = -1;
			for (int i = s; i < e; i++)
			{
				float d = points[i].distanceTo(points[i + 1]);
				if (d > foundDist)
				{
					foundDist = d;
					foundT = 0.5f + i;
				}
			}

			if (startT != (float)s)
			{
				DPoint p = calcPointByT(startT);
				float d = points[s].distanceTo(p);
				if (d > foundDist)
				{
					foundDist = d;
					foundT = (s + startT) / 2;
				}
			}

			if (endT != (float)e)
			{
				DPoint p = calcPointByT(endT);
				float d = points[e].distanceTo(p);
				if (d > foundDist)
				{
					foundDist = d;
					foundT = (e + endT) / 2;
				}
			}
			return foundT;
		}
		/// <summary>
		/// Вычисляет периметр полилинии.
		/// </summary>
		/// <param name="closed">true - для указания замкнутости полилинии</param>
		/// <returns>Вычисленное значение периметра</returns>
		public float calcPerimeter(bool closed)
		{
			return Math2D.calcPerimeter(ref this, closed);
		}
		/// <summary>
		/// Найти все пересечения этой полилинии с заданной полилинией и вернуть список
		/// точек пересечения в параметрическом представлении.
		/// </summary>
		/// <param name="pp">Вторая полилиния</param>
		/// <param name="crossTs">Вычисленные точки пересечения</param>
		public void calcAllCrossesWith(ref DPointList pp, ArrayList crossTs)
		{
			DLineMath d = new DLineMath();
			DLineMath s = new DLineMath();
			for (int i = points.Length - 2; i >= 0; i--)
			{
				s.create(ref points[i], ref points[i + 1]);
				for (int j = pp.points.Length - 2; j >= 0; j--)
				{
					d.create(ref pp.points[j], ref pp.points[j + 1]);
					float t;
					if (s.crossPiece(ref d, out t) == Math2D.fOnBothPieces)
						crossTs.Add(t);
				}
			}
		}
	}
	/// <summary> Отрезок прямой </summary>
	public struct DLine
	{
		/// <summary> Первая точка, через которую проходит отрезок </summary>
		public DPoint a;
		/// <summary> Вторая точка, через которую проходит отрезок </summary>
		public DPoint b;
		/// <summary> Вычисляет длину отрезка </summary>
		public double length()
		{
			return a.distanceTo(b);
		}
		/// <summary> Вычисляет координаты середины отрезка и возвращает их в качестве результата </summary>
		public DPoint getCenter()
		{
			return new DPoint((a.x + b.x) / 2, (a.y + b.y) / 2);
		}
	}
	public struct DLineMath //: DLine
	{
		float deltaX, aX;
		float deltaY, aY;
		/// <summary>
		/// Создать объект отрезка по его начальной и конечной точкам.
		/// </summary>
		public void create(ref DPoint p1, ref DPoint p2)
		{
			deltaX = p2.x - p1.x;
			aX = p1.x;
			deltaY = p2.y - p1.y;
			aY = p1.y;
		}
		/// <summary>
		/// Создать объект отрезка по его начальной точке и приращениям до конечной точки по X и по Y.
		/// </summary>
		public void create(ref DPoint p1, float dX, float dY)
		{
			deltaX = dX;
			aX = p1.x;
			deltaY = dY;
			aY = p1.y;
		}
		/// <summary>
		/// Создать отрезок, параллельный данному исходящий из заданной точки
		/// </summary>
		public void createParallelOnBP(ref DLineMath l, ref DPoint p)
		{
			deltaX = l.deltaX;
			aX = p.x;
			deltaY = l.deltaY;
			aY = p.y;
		}
		/// <summary>
		/// Создать отрезок, перпендикулярный данному исходящий из заданной точки и в заданном направлении
		/// </summary>
		public void createPerpendOnBP(ref DLineMath l, ref DPoint p, bool ToRight)
		{
			if (ToRight) deltaX = +l.deltaY; else deltaX = -l.deltaY;
			aX = p.x;
			if (ToRight) deltaY = -l.deltaX; else deltaY = +l.deltaX;
			aY = p.y;
		}
		/// <summary>		
		/// Создать отрезок, который располагается под заданным углом к данному исходящий из заданной точки
		/// </summary>
		public void createCrossOnBP(ref DLineMath l, ref DPoint p, double angle)
		{
			deltaX = l.deltaX;
			aX = p.x;
			deltaY = l.deltaY;
			aY = p.y;

			rotate(angle);
		}
		/// <summary>		
		/// Создать отрезок, параллельный данному исходящий из точки, находящейся слева от вектора на расстоянии distance
		/// </summary>
		public void createParallelAtDistance(ref DLineMath l, float distance)
		{
			deltaX = -l.deltaY;
			deltaY = +l.deltaX;
			DPoint p;

			calcPointAtDistFromA(distance, out p);

			aX = p.x;
			aY = p.y;
			deltaX = l.deltaX;
			deltaY = l.deltaY;
		}
		private float DistancePrim(float dx, float dy)
		{
			return (float)Math.Sqrt(dx * dx + dy * dy);
		}
		/// <summary>		
		/// Расстояние от отрезка до точки (с учётом крайних точек отрезка)
		/// </summary>
		public float pieceDistanceTo(ref DPoint p)
		{
			float t;
			t = kABPerpend(p);

			return DistancePrim((deltaX * t + aX) - p.x, (deltaY * t + aY) - p.y);
		}
		/// <summary>
		/// Вычисляет расстояние от этого отрезка данной точки и параметр t - точки на отрезке, ближайшей к данной.
		/// </summary>
		/// <param name="p">Данная точка</param>
		/// <param name="t">Параметр t - точки на отрезке, ближайшей к данной точке</param>
		/// <returns>Вычисленное расстояние</returns>
		public float pieceDistanceToExt(ref DPoint p, out float t)
		{
			t = kABPerpend(p);
			return DistancePrim((deltaX * t + aX) - p.x, (deltaY * t + aY) - p.y);
		}
		/// <summary>		
		///Расстояние от прямой до точки
		/// </summary>
		public float lineDistanceTo(ref DPoint p)
		{
			float t;
			t = tPerpend(p);
			return DistancePrim((deltaX * t + aX) - p.x, (deltaY * t + aY) - p.y);
		}
		/// <summary>		
		/// Расстояние от прямой до точки со знаком, обозначающим их взаимное расположение (левое/правое).
		/// </summary>
		public float vectorDistanceTo(ref DPoint p)
		{
			float t, result;
			t = tPerpend(p);

			result = DistancePrim((deltaX * t + aX) - p.x, (deltaY * t + aY) - p.y);
			if (result > MathConst.NearlyZero)
			{
				DPoint tempPoint1 = new DPoint(aX, aY);
				DPoint tempPoint2 = new DPoint(aX + deltaX, aY + deltaY);
				if (Math2D.calcSignedTriangleArea(ref tempPoint1,
					ref tempPoint2, ref p) < 0) return -result;
			}
			return result;
		}
		/// <summary>		
		///Опустить перпендикуляр из точки на прямую, возвращает параметр t
		/// </summary>
		public float tPerpend(DPoint p)
		{
			float aCSqr;
			try
			{
				aCSqr = deltaX * deltaX + deltaY * deltaY;
				return (p.x - aX) * (deltaX / aCSqr) + (p.y - aY) * (deltaY / aCSqr);
			}
			catch
			{
				return (float)MathConst.Infinity;
			}
		}
		/// <summary>		
		/// Опустить перпендикуляр из точки на отрезок, возвращает параметр t, с ограничением пределами отрезка.
		/// </summary>
		/// <param name="p">Точка, из которой опускается перпендикуляр</param>
		/// <returns>Результат в диапазоне [0..1]</returns>
		public float kABPerpend(DPoint p)
		{
			float t;
			t = tPerpend(p);
			if (t < 0)
			{
				return 0;
			}
			else if (t > 1)
			{
				return 1;
			}
			else return t;
		}
		/// <summary>		
		/// Первая точка отрезка.
		/// </summary>
		public DPoint getFirstPoint()
		{
			DPoint result = new DPoint(aX, aY);
			result.x = aX;
			result.y = aY;

			return result;
		}
		/// <summary>		
		/// Вторая точка отрезка.
		/// </summary>
		public DPoint getSecondPoint()
		{
			DPoint result;
			result.x = deltaX + aX;
			result.y = deltaY + aY;

			return result;
		}
		/// <summary>		
		/// Вычисляет координаты точки, для заданного параметра t.
		/// </summary>
		public DPoint calcXY(float t)
		{
			DPoint result;
			result.x = deltaX * t + aX;
			result.y = deltaY * t + aY;

			return result;
		}
		float calcX(float t)
		{
			return deltaX * t + aX;
		}
		float calcY(float t)
		{
			return deltaY * t + aY;
		}
		/// <summary>		
		///Вычисляет пересечение двух прямых.
		/// </summary>
		public bool crossLine(DLineMath l, ref float t)
		{
			float lt, dd;

			if (Math.Abs(deltaX) < MathConst.AboutZero)
			{
				if (Math.Abs(l.deltaX) < MathConst.AboutZero) return false;
				if (Math.Abs(deltaY) < MathConst.AboutZero) return false;
				lt = (aX - l.aX) / l.deltaX;
				t = (l.deltaY * lt + l.aY - aY) / deltaY;
			}
			else if (Math.Abs(l.deltaX) < MathConst.AboutZero)
			{
				if (Math.Abs(deltaX) < MathConst.AboutZero) return false;
				t = (l.aX - aX) / deltaX;
			}
			else if (Math.Abs(deltaY) < MathConst.AboutZero)
			{
				if (Math.Abs(l.deltaY) < MathConst.AboutZero) return false;
				if (Math.Abs(deltaX) < MathConst.AboutZero) return false;
				lt = (aY - l.aY) / l.deltaY;
				t = (l.deltaX * lt + l.aX - aX) / deltaX;
			}
			else if (Math.Abs(l.deltaY) < MathConst.AboutZero)
			{
				if (Math.Abs(deltaY) < MathConst.AboutZero) return false;
				if (Math.Abs(l.deltaX) < MathConst.AboutZero) return false;
				t = (l.aY - aY) / deltaY;
			}
			else
			{
				dd = l.deltaX * deltaY - l.deltaY * deltaX;
				if (Math.Abs(dd) < MathConst.AboutZero) return false;
				t = (l.deltaY * (aX - l.aX) - l.deltaX * (aY - l.aY)) / dd;
			}

			return true;
		}
		/// <summary>		
		/// Вычисляет пересечение двух отрезков.
		/// </summary>
		/// <returns>0 - пересечения нет</returns>
		/// <returns>fCrossing - точка пересечения вне обоих отрезков</returns>
		/// <returns>fCrossing | fOnThisPiece - точка пересечения только вне второго отрезка</returns>
		/// <returns>fCrossing | fOnParamPiece - точка пересечения только вне первого отрезка</returns>
		/// <returns>fOnBothPieces - точка пересечения внутри обоих отрезков</returns>
		public int crossPiece(ref DLineMath l, out float t)
		{
			float lt, dd;
			int result = 0;
			t = 0;

			if (Math.Abs(deltaX) < MathConst.AboutZero)
			{
				if (Math.Abs(l.deltaX) < MathConst.AboutZero) return result;
				if (Math.Abs(deltaY) < MathConst.AboutZero) return result;
				lt = (aX - l.aX) / l.deltaX;
				t = (l.deltaY * lt + l.aY - aY) / deltaY;
			}
			else if (Math.Abs(l.deltaX) < MathConst.AboutZero)
			{
				if (Math.Abs(deltaX) < MathConst.AboutZero) return result;
				if (Math.Abs(l.deltaY) < MathConst.AboutZero) return result;
				t = (l.aX - aX) / deltaX;
				lt = (deltaY * t + aY - l.aY) / l.deltaY;
			}
			else if (Math.Abs(deltaY) < MathConst.AboutZero)
			{
				if (Math.Abs(l.deltaY) < MathConst.AboutZero) return result;
				if (Math.Abs(deltaX) < MathConst.AboutZero) return result;
				lt = (aY - l.aY) / l.deltaY;
				t = (l.deltaX * lt + l.aX - aX) / deltaX;
			}
			else if (Math.Abs(l.deltaY) < MathConst.AboutZero)
			{
				if (Math.Abs(deltaY) < MathConst.AboutZero) return result;
				if (Math.Abs(l.deltaX) < MathConst.AboutZero) return result;
				t = (l.aY - aY) / deltaY;
				lt = (deltaX * t + aX - l.aX) / l.deltaX;
			}
			else
			{
				dd = l.deltaX * deltaY - l.deltaY * deltaX;
				if (Math.Abs(dd) < MathConst.AboutZero) return result;
				t = (l.deltaY * (aX - l.aX) - l.deltaX * (aY - l.aY)) / dd;
				lt = (deltaY * (l.aX - aX) - deltaX * (l.aY - aY)) / (-dd);
			}

			result = result | Math2D.fCrossing;
			if ((t >= -MathConst.AboutZero) & (t <= 1 + MathConst.AboutZero))
				result = result | Math2D.fOnThisPiece;
			if ((lt >= -MathConst.AboutZero) & (lt <= 1 + MathConst.AboutZero))
				result = result | Math2D.fOnParamPiece;
			return result;
		}
		/// <summary>
		/// отличается от CrossPiece тем что условие "внутренности" точки пересечения является строгим
		/// </summary>
		public int crossPieceStrict(ref DLineMath l, ref float t)
		{
			DPoint cp;
			int result;
			result = crossPiece(ref l, out t);
			if ((result & (Math2D.fOnThisPiece | Math2D.fOnParamPiece)) != 0)
			{
				cp = calcXY(t);
				if ((Math2D.fOnThisPiece & result) != 0)
					if ((cp.distanceTo(getFirstPoint()) < MathConst.NearlyZero) || (cp.distanceTo(getSecondPoint()) < MathConst.NearlyZero))
						result = result & ~Math2D.fOnThisPiece;
				if ((Math2D.fOnParamPiece & result) != 0)
					if ((cp.distanceTo(l.getFirstPoint()) < MathConst.NearlyZero) || (cp.distanceTo(l.getSecondPoint()) < MathConst.NearlyZero))
						result = result & ~Math2D.fOnParamPiece;
			}
			return result;
		}
		/// <summary>		
		/// Вычисляет координаты точки, лежащей на прямой и находящейся на заданном расстоянии от первой точки отрезка
		/// </summary>
		public float calcPointAtDistFromA(float aDistance, out DPoint p)
		{
			double k, t;
			k = DistancePrim(deltaX, deltaY);
			if (k < MathConst.NearlyZero2) throw (new ApplicationException("Вычисления на отрезке нулевой длины!"));
			t = aDistance / k;
			p.x = (float)(deltaX * t + aX);
			p.y = (float)(deltaY * t + aY);
			return (float)t;
		}
		/// <summary>		
		/// Угол наклона вектора
		/// </summary>
		public double slopping()
		{
			return MathFunc.ArcTan2Ext(deltaY, deltaX);
		}
		/// <summary>		
		/// Возвращает угол между этим вектором и данным (L), положительный если L направлен левее чем этот
		/// </summary>
		public double angleWithVector(DLineMath l)
		{
			return MathFunc.normAngle(l.slopping() - slopping());
		}
		/// <summary>		
		/// Изменить (установить) угол наклона вектора (первая точка отрезка остаётся на месте)
		/// </summary>
		public void setSlopping(double angle)
		{
			float l;
			l = pieceLength();
			deltaX = (float)(Math.Cos(angle) * l);
			deltaY = (float)(Math.Sin(angle) * l);
		}
		/// <summary>		
		/// Повернуть вектор на заданный угол
		/// </summary>
		public void rotate(double angle)
		{
			double alfa;
			float l;
			alfa = MathFunc.normAngle(slopping() + angle);
			l = pieceLength();
			deltaX = (float)(Math.Cos(alfa) * l);
			deltaY = (float)(Math.Sin(alfa) * l);
		}
		/// <summary>		
		/// Вычисляет длину отрезка
		/// </summary>
		public float pieceLength()
		{
			return DistancePrim(deltaX, deltaY);
		}
	}
	/// <summary> Прямоугольная оболочка геометрического объекта </summary>
	public struct DExtents
	{
		/// <summary>
		/// Минимальные координаты оболочки.
		/// </summary>
		public DPoint min;
		/// <summary>
		/// Максимальные координаты оболочки.
		/// </summary>
		public DPoint max;
		public DExtents(
			DPoint min,
			DPoint max
			)
		{
			this.min = min;
			this.max = max;
		}
		/// <summary>
		/// вертикальный и горизонтальный центр оболочки
		/// </summary>
		public DPoint centerPoint
		{
			get
			{
				if (this.IsEmpty)
				{
					throw new InvalidOperationException();
				}

				return new DPoint((min.x + max.x) / 2, (min.y + max.y) / 2);
			}
		}
		/// <summary>
		/// ширина оболочки
		/// </summary>
		public double dX
		{
			get
			{
				return max.x - min.x;
			}
		}
		/// <summary>
		/// высота оболочки
		/// </summary>
		public double dY
		{
			get
			{
				return max.y - min.y;
			}
		}
		public bool IsEmpty
		{
			get { return this.min.IsEmpty && this.max.IsEmpty; }
		}
		/// <summary>
		/// Записывает вырожденную оболочку
		/// </summary>
		public void setEmpty()
		{
			this.min = DExtents.Empty.min;
			this.max = DExtents.Empty.max;
		}
		/// <summary>
		/// Записывает максимальную оболочку
		/// </summary>
		public void setMax()
		{
			min.x = (float)-MathConst.Infinity;
			min.y = (float)-MathConst.Infinity;
			max.x = (float)+MathConst.Infinity;
			max.y = (float)+MathConst.Infinity;
		}
		/// <summary>
		/// Проверяет, пересекается ли (или касается ли) эта оболочка с заданной.
		/// </summary>
		public bool isIntersects(ref DExtents extents)
		{
			return
				!this.IsEmpty
					? (extents.max.x >= min.x) &&
					  (extents.min.x <= max.x) &&
					  (extents.max.y >= min.y) &&
					  (extents.min.y <= max.y)
					: false;
		}
		public bool isIntersects(DExtents extent)
		{
			return
				!this.IsEmpty
					? (extent.max.x >= min.x) && (extent.min.x <= max.x) &&
					  (extent.max.y >= min.y) && (extent.min.y <= max.y)
					: false;
		}
		/// <summary>
		/// Расширяет оболочку так, чтобы заданная точка point оказалась включена в эту оболочку
		/// </summary>
		public void includePoint(ref DPoint point)
		{
			if (this.IsEmpty)
			{
				this.min = point;
				this.max = point;
			}
			else
			{
				if (point.x < min.x) min.x = point.x;
				if (point.x > max.x) max.x = point.x;
				if (point.y < min.y) min.y = point.y;
				if (point.y > max.y) max.y = point.y;
			}
		}
		public void includePoint(params DPoint[] points)
		{
			for (int i = 0; i < points.Length; i++)
			{
				this.includePoint(ref points[i]);
			}
		}
		/// <summary> Расширяет оболочку так, чтобы заданная оболочка (extent) оказалась включена в эту оболочку </summary>
		public void includeExtents(ref DExtents extent)
		{
			this.includePoint(extent.min);
			this.includePoint(extent.max);
		}
		/// <summary>
		/// Включить в оболочку заданный отрезок
		/// </summary>
		public void includeLine(ref DLine line)
		{
			this.includePoint(ref line.a);
			this.includePoint(ref line.b);
		}
		/// <summary>
		/// Включить в оболочку все точки заданного списка точек
		/// </summary>
		public void includePointList(ref DPointList pointList)
		{
			for (int i = 0; i < pointList.points.Length; i++)
				includePoint(ref pointList.points[i]);
		}
		public static readonly DExtents Empty = new DExtents(DPoint.Empty, DPoint.Empty);
	}
	/// <summary> Глобальные числовые константы </summary>
	public class MathConst
	{
		/// <summary> Точность сравнения координат </summary>
		public const float  NearlyZero  = 1E-01f;
		/// <summary> Точность сравнения углов, тангенсов и т.п. </summary>
		public const double NearlyZero2 = 1E-08;
		/// <summary> Условная бесконечность </summary>
		public const float Infinity     = 1E+20f;
		/// <summary> Условный ноль </summary>
		public const double AboutZero   = 1E-18;
		/// <summary> Число "Пи" </summary>
		public const double PI          = Math.PI;
		/// <summary> Число "2Пи" </summary>
		public const double twoPI       = Math.PI * 2;
	}
	/// <summary> Алгебраические функции </summary>
	public class MathFunc
	{
		/// <summary> Сравнение действительных чисел </summary>
		/// <returns> 0 - a и b равны </returns>
		/// <returns> 1 - a больше b </returns>
		/// <returns>-1 - a меньше b </returns>
		public static int CompareDouble(double a, double b)
		{
			if (a > b + MathConst.NearlyZero)
				return +1;
			else if (a < b - MathConst.NearlyZero)
				return -1;
			else
				return 0;
		}
		/// <summary> Нормализовать значение угла, угол будет находиться в пределах (-Pi..+Pi] </summary>
		public static double normAngle(double angle)
		{
			while (angle > +MathConst.PI)
				angle -= MathConst.twoPI;
			while (angle <= -MathConst.PI)
				angle += MathConst.twoPI;
			return angle;
		}
		/// <summary>
		/// Возвращает угол, соответствующий положению точки с координатами (dX,dY) 
		/// относительно точки начала координат в диапазоне (-PI..+PI].
		/// </summary>
		/// <param name="dY"></param>
		/// <param name="dX"></param>
		/// <returns>Угол в радианах, против часовой стрелки, 0 соответствует направлению оси X</returns>
		public static double ArcTan2Ext(double dY, double dX)
		{
			if (dY != 0)
				if (Math.Abs(dX) < MathConst.NearlyZero2)
					if (dY > 0)
						return +MathConst.PI / 2;
					else
						return -MathConst.PI / 2;
				else
					return Math.Atan2(dY, dX);
			else
				if (dX >= 0)
				return 0;
			else
				return +MathConst.PI;
		}
		/// <summary> Округляет число до указанного порядка </summary>
		/// <param name="aValue">Округляемое значение</param>
		/// <param name="aDigit">Количество знаков после запятой</param>
		public static double roundTo(double aValue, int aDigit)
		{
			double lFactor;
			lFactor = Math.Pow(10, aDigit);
			return Math.Round(aValue / lFactor) * lFactor;
		}
		/// <summary> Используется для поиска и сортировки действительных чисел в списках </summary>
		public class FloatComparer : IComparer
		{
			/// <summary> Сравнивает два действительных числа (float) </summary>
			public int Compare(object x, object y)
			{
				float first = (float)x;
				float second = (float)y;
				if (first < second)
					return -1;
				else
				if (first > second)
					return +1;
				else
					return 0;
			}
		}
		/// <summary> Используется для поиска и сортировки действительных чисел в списках </summary>
		public static FloatComparer floatComparer = new FloatComparer();
	}
	/// <summary> Функции обработки множеств точек как простых плоских фигур </summary>
	public class Math2D
	{
		/// <summary> Варианты размещения точки относительно полигона </summary>
		public enum PointInPolygonState
		{
			/// <summary> Полигон является вырожденным </summary>
			ppsNoPolygon,
			/// <summary> Точка лежит вне полигона </summary>
			ppsOutside,
			/// <summary> Точка лежит внутри полигона </summary>
			ppsInside,
			/// <summary> Точка лежит на границе полигона </summary>
			ppsOnEdge
		};

		private const int b0 = 0x001;   // AB никак не пересекается с l
		private const int b1 = 0x002;   // AB пересекается с l
		private const int b2 = 0x004;   // P == A
		private const int b3 = 0x008;   // P == B
		private const int b4 = 0x010;   // P лежит внутри AB, AB имеет ненулевую общую часть с l
		private const int b5 = 0x020;   // B лежит на l, остальной отрезок выше если нет b8
		private const int b6 = 0x040;   // A лежит на l, остальной отрезок выше если нет b8
		private const int b7 = 0x080;   // требуется вычисление пересечения
		private const int b8 = 0x100;   // остальной отрезок ниже

		private const int isOnEdgeMask   = b2 + b3 + b4;
		private const int isOnVertexMask = b2 + b3;
		private const int isExtTurnMask  = b5 + b6;
		private const int NeedCalcMask   = b7;

		/// <summary> Линии пересекаются </summary>
		public const int fCrossing     = 0x001;
		/// <summary> Точка пересечения лежит в пределах этого отрезка </summary>
		public const int fOnThisPiece  = 0x002;
		/// <summary> Точка пересечения лежит в пределах данного отрезка (переданного функции в качестве параметра) </summary>
		public const int fOnParamPiece = 0x004;
		/// <summary> Точка пересечения лежит в пределах обоих отрезков </summary>
		public const int fOnBothPieces = fCrossing | fOnThisPiece | fOnParamPiece;

		private static readonly int[,,,] m = new int[3, 3, 3, 3]
		{
			{
				{{001, 001, 001}, {001, 008, 032}, {001, 01, 128}},
				{{001, 001, 001}, {001, 008, 032}, {001, 16, 002}},
				{{001, 001, 001}, {001, 008, 032}, {128, 02, 002}},
			},
			{
				{{001, 001, 001}, {001, 008, 016}, {001, 001, 001}},
				{{004, 004, 004}, {004, 004, 004}, {004, 004, 004}},
				{{064, 064, 064}, {080, 072, 096}, {320, 320, 320}},
			},
			{
				{{001, 001, 128}, {001, 008, 288}, {001, 001, 001}},
				{{001, 016, 002}, {001, 008, 288}, {001, 001, 001}},
				{{128, 002, 002}, {001, 008, 288}, {001, 001, 001}},
			}
		};
		/// <summary> Вспомогательная функция, вызывается из функции findPointInPolygon </summary>
		protected static void calcLP(
			ref DLineMath sL, ref DPoint bP, bool rightDir, int pointCount, int iA, ref DPointList points, ref DPoint p, ref double r)
		{
			DLineMath pL = new DLineMath(), tL = new DLineMath();
			int jA, jB;
			int cK;
			DPoint pCur;
			float tMin, tCur, dCur, dTmp;

			pL.createPerpendOnBP(ref sL, ref bP, rightDir);

			tMin = MathConst.Infinity;
			for (jA = 0; jA < pointCount; jA++)
			{
				if (jA == iA) continue;

				jB = (jA + 1) % pointCount;

				tL.create(ref points.points[jA], ref points.points[jB]);
				if (tL.pieceLength() < MathConst.NearlyZero) continue;

				cK = pL.crossPiece(ref tL, out tCur);
				if ((Math2D.fCrossing & cK) != 0)
				{
					if (((Math2D.fOnParamPiece & cK) != 0) && (tCur > MathConst.NearlyZero2) && (tCur < tMin))
						tMin = tCur;
				}
				else
				{
					tCur = pL.lineDistanceTo(ref points.points[jA]) / pL.pieceLength();
					if ((tCur < tMin))
						tMin = tCur;
					tCur = pL.lineDistanceTo(ref points.points[jB]) / pL.pieceLength();
					if ((tCur < tMin))
						tMin = tCur;
				}
			}

			if (tMin < MathConst.Infinity)
			{
				tMin = tMin / 2;
				dCur = pL.pieceLength() * tMin;
				pCur = pL.calcXY(tMin);

				for (jA = 0; jA < pointCount; jA++)
				{
					jB = (jA + 1) % pointCount;
					tL.create(ref points.points[jA], ref points.points[jB]);
					if (tL.pieceLength() < MathConst.NearlyZero) continue;

					dTmp = tL.pieceDistanceTo(ref pCur);
					if (dTmp < dCur)
						dCur = dTmp;
				}

				if ((dCur > r) && (isPointInСontour(pCur, points) == PointInPolygonState.ppsInside))
				{
					r = dCur;
					p = pCur;
				}
			}
		}
		/// <summary> Вычисляет площадь треугольника со знаком </summary>
		public static float calcSignedTriangleArea(ref DPoint a, ref DPoint b, ref DPoint c)
		{
			return (float)0.5 * (a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y));
		}
		/// <summary> Определяет, является ли заданный контур правосторонним (т.е. обход точек этого контура идет в направлении по часовой стрелке) </summary>
		public static bool isContourRight(ref DPointList poly, int polySize)
		{
			int i, a, b, c;
			DPoint mx;

			b = 0;
			mx = poly.points[b];
			for (i = 1; i < polySize; i++)
				if ((poly.points[i].y > mx.y) || ((poly.points[i].y == mx.y) && (poly.points[i].x > mx.x)))
				{
					mx = poly.points[i];
					b = i;
				}

			c = (b + 1) % polySize;                 // следующая точка
			a = (b - 1 + polySize) % polySize;      // предыдущая точка
			return calcSignedTriangleArea(ref poly.points[a], ref poly.points[b], ref poly.points[c]) < 0;
		}
		/// <summary> Находит точку внутри полигона (как бы географический центр) </summary>
		public static void findPointInPolygon(ref DPointList points, out DPoint p, out double r)
		{
			int iA, iB;
			int pointCount = points.points.Length;
			DLineMath sL = new DLineMath();
			bool rightDir;

			if (pointCount < 3)
			{
				if (pointCount < 1)
				{
					r = MathConst.Infinity;
					p = new DPoint(MathConst.Infinity, MathConst.Infinity);
				}
				else
				{
					r = 0;
					p = points.points[0];
				}
				return;
			}

			p = points.points[0];
			rightDir = isContourRight(ref points, pointCount);
			r = -MathConst.Infinity;
			for (iA = 0; iA < pointCount; iA++)
			{
				iB = (iA + 1) % pointCount;
				sL.create(ref points.points[iA], ref points.points[iB]);

				if (sL.pieceLength() < MathConst.NearlyZero) continue;

				calcLP(ref sL, ref points.points[iA], rightDir, pointCount, iA, ref points, ref p, ref r);
				calcLP(ref sL, ref points.points[iB], rightDir, pointCount, iA, ref points, ref p, ref r);
				DPoint tempPoint = sL.calcXY(0.5f);
				calcLP(ref sL, ref tempPoint, rightDir, pointCount, iA, ref points, ref p, ref r);
				tempPoint = sL.calcXY(0.33f);
				calcLP(ref sL, ref tempPoint, rightDir, pointCount, iA, ref points, ref p, ref r);
				tempPoint = sL.calcXY(0.67f);
				calcLP(ref sL, ref tempPoint, rightDir, pointCount, iA, ref points, ref p, ref r);
			}
		}
		/// <summary> Проверка: находится ли точка внутри контура полигона </summary>
		public static PointInPolygonState isPointInСontour(DPoint p, DPointList poly)
		{
			return isPointInСontour(p, poly.points);
		}
		/// <summary> Проверка: находится ли точка внутри контура полигона </summary>
		public static PointInPolygonState isPointInСontour(DPoint p, DPoint[] poly)
		{
			int polySize = poly.Length;
			int v;
			double x;
			DPoint a, b;
			int turnCount, i;
			int a_on_X, b_on_X, a_on_Y, b_on_Y;

			if (polySize < 2)
				return PointInPolygonState.ppsNoPolygon;

			turnCount = 0;
			b = poly[polySize - 1];
			b_on_X = MathFunc.CompareDouble(b.x, p.x) + 1;
			b_on_Y = MathFunc.CompareDouble(b.y, p.y) + 1;
			for (i = 0; i <= polySize - 1; i++)
			{
				a = b;
				a_on_X = b_on_X;
				a_on_Y = b_on_Y;
				b = poly[i];
				b_on_X = MathFunc.CompareDouble(b.x, p.x) + 1;
				b_on_Y = MathFunc.CompareDouble(b.y, p.y) + 1;

				v = m[a_on_Y, a_on_X, b_on_Y, b_on_X];

				if (v != b0)
				{
					if ((v & isOnEdgeMask) != 0)
						return PointInPolygonState.ppsOnEdge;

					if ((v & b1) != 0)
						turnCount++;
					else if ((v & NeedCalcMask) != 0)
					{
						x = (b.x - a.x) * (p.y - a.y) / (b.y - a.y) + a.x;
						if (Math.Abs(x - p.x) < MathConst.NearlyZero)
							return PointInPolygonState.ppsOnEdge;

						if (x > p.x)
							turnCount++;
					}
					else if ((v & b8) != 0)
						turnCount++;
				}
			}

			if ((turnCount & 1) != 0)
				return PointInPolygonState.ppsInside;
			else
				return PointInPolygonState.ppsOutside;
		}
		private static DPoint iPoint(int i, ref DPointList polygon, int nPoints)
		{
			return polygon.points[(i + nPoints) / nPoints];
		}
		/// <summary>/ Вычислить периметр полигона/полилинии. Для полигонов и замкнутых полилиний </summary>
		public static float calcPerimeter(ref DPointList points, bool closed)
		{
			float s = 0;
			int nPoints = points.points.Length;
			for (int i = nPoints - 2; i >= 0; i--)
				s += points.points[i].distanceTo(points.points[i + 1]);
			if (closed)
				s += points.points[0].distanceTo(points.points[nPoints - 1]);
			return s;
		}
		/// <summary> Вычислить площадь полигона со знаком, для правого полигона площадь положительна </summary>
		public static float calcSignedPolygonArea(ref DPointList polygon, int nPoints)
		{
			double s, s1;
			int i;
			s = 0;
			for (i = 0; i <= nPoints - 1; i++)
			{
				s1 = iPoint(i, ref polygon, nPoints).x * (iPoint(i - 1, ref polygon, nPoints).y
					- iPoint(i + 1, ref polygon, nPoints).y);
				s = s + s1;
			}
			return (float)(s / 2);
		}
		/// <summary> Вычисляет расстояние от полилинии до точки </summary>
		public static float polylineToPointDistance(ref DPointList vPoints, bool closed, ref DPoint sample)
		{
			long pieceCount = vPoints.points.Length - 1;
			if (pieceCount < 0)
				return (float)MathConst.Infinity;
			if (pieceCount == 0)
				return vPoints.points[0].distanceTo(sample);

			DPoint P1;
			DPoint P2 = vPoints.points[0];
			DPoint P0;
			float MinDist = MathConst.Infinity, Dist;
			DLineMath l = new DLineMath();

			long StartPInd = 0;
			for (long i = 1; i <= pieceCount; i++)
			{
				P1 = P2;
				P2 = vPoints.points[i];
				l.create(ref P1, ref P2);
				float t;
				Dist = l.pieceDistanceToExt(ref sample, out t);
				if (Dist < MinDist - MathConst.NearlyZero)
					MinDist = Dist;
			}

			if (closed)
			{
				P1 = P2;
				P0 = vPoints.points[StartPInd];
				l.create(ref P1, ref P0);
				float t;
				Dist = l.pieceDistanceToExt(ref sample, out t);
				if (Dist < MinDist)
					MinDist = Dist;
			}

			return MinDist;
		}
	}
	public class MatrixNode
	{
		public long  Node;
		public int   Scale;
		public bool? IsFull;
	}
}