﻿namespace FORIS.TSS.BusinessLogic.Security
{
	public enum TssPrincipalType
	{
		Unknown,
		Operator,
		OperatorGroup
	}
}