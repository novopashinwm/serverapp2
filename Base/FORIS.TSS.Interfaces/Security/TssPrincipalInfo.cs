﻿using System;
using System.Security.Principal;

namespace FORIS.TSS.BusinessLogic.Security
{
	/// <summary> Описание объекта обладающего правами доступа к защищаемым объектам </summary>
	[Serializable]
	public struct TssPrincipalInfo : IPrincipal
	{
		#region IPrincipal members

		private readonly GenericIdentity identity;
		public IIdentity Identity
		{
			get { return identity; }
		}

		public bool IsInRole(string role)
		{
			throw new NotSupportedException();
		}

		#endregion IPrincipal Members

		private readonly string name;

		public string Name
		{
			get { return name; }
		}

		private readonly string tableName;
		public string TableName
		{
			get { return tableName; }
		}

		private readonly int id;
		public int Id
		{
			get { return id; }
		}

		private readonly TssPrincipalType principalType;
		public TssPrincipalType PrincipalType
		{
			get { return principalType; }
		}
		public TssPrincipalInfo(string name, string tableName, int id, TssPrincipalType principalType)
		{
			this.name          = name;
			this.tableName     = tableName;
			this.id            = id;
			this.principalType = principalType;

			if (this.principalType == TssPrincipalType.Operator)
			{
				identity = new GenericIdentity(this.name);
			}
			else
			{
				identity = null;
			}
		}
		public override bool Equals(object obj)
		{
			if (obj != null)
			{
				if (obj is TssPrincipalInfo)
				{
					TssPrincipalInfo Info = (TssPrincipalInfo)obj;

					return tableName == Info.tableName && id == Info.id;
				}
				else
					throw new ArgumentException();
			}
			else
				return false;
		}
		public override int GetHashCode()
		{
			// TODO: Может быть существует лучший алгоритм вычисления хэша по двум полям
			return id.GetHashCode();
		}
		public static bool operator ==(TssPrincipalInfo info1, TssPrincipalInfo info2)
		{
			return TssPrincipalInfo.Equals(info1, info2);
		}
		public static bool operator !=(TssPrincipalInfo info1, TssPrincipalInfo info2)
		{
			return !TssPrincipalInfo.Equals(info1, info2);
		}

		#region Nobody

		private static readonly TssPrincipalInfo s_nobody =
			new TssPrincipalInfo(string.Empty, string.Empty, 0, TssPrincipalType.Unknown);

		public static TssPrincipalInfo Nobody
		{
			get { return TssPrincipalInfo.s_nobody; }
		}

		#endregion Nobody
	}
}