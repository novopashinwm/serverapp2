﻿namespace FORIS.TSS.Common
{
	public interface ILogTimeContainer
	{
		int GetLogTime();
	}
}