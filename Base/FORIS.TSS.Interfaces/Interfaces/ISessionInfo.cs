﻿using System;
using System.Net;

namespace FORIS.TSS.BusinessLogic.Interfaces
{
	public interface ISessionInfo
	{
		int           SessionId     { get; }
		IOperatorInfo OperatorInfo  { get; set; }
		Guid          ClientId      { get; }
		string        ClientVersion { get; }
		DateTime      StartOn       { get; }
		DateTime?     StopOn        { get; set; }
		string        HostName      { get; }
		/// <summary> IP может быть изменен, например, при перемещении телефона между сотами </summary>
		IPAddress     IP            { get; set; }
	}
}