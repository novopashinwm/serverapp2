﻿using System;
using System.Net;

namespace FORIS.TSS.BusinessLogic.Interfaces
{
	[Serializable]
	public struct SessionInfo : ISessionInfo
	{
		#region Properties

		private readonly int sessionId;
		public int SessionId
		{
			get { return sessionId; }
		}
		IOperatorInfo operatorInfo;
		public IOperatorInfo OperatorInfo
		{
			get { return operatorInfo; }
			set { operatorInfo = value; }
		}
		private readonly Guid clientId;
		public Guid ClientId
		{
			get { return clientId; }
		}
		private readonly string clientVersion;
		public string ClientVersion
		{
			get { return clientVersion; }
		}
		private readonly string hostName;
		public string HostName
		{
			get { return hostName; }
		}
		private IPAddress ip;
		public IPAddress IP
		{
			get { return ip; }
			set { ip = value; }
		}
		private readonly DateTime startOn;
		public DateTime StartOn
		{
			get { return startOn; }
		}
		private DateTime? stopOn;
		public DateTime? StopOn
		{
			get { return stopOn; }
			set { stopOn = value; }
		}

		#endregion Properties

		#region Constructor

		public SessionInfo(int sessionId, IOperatorInfo operatorInfo, Guid clientId, string clientVersion, string hostName, IPAddress ip, DateTime startOn)
		{
			this.sessionId     = sessionId;
			this.operatorInfo  = operatorInfo;
			this.clientId      = clientId;
			this.clientVersion = clientVersion;
			this.hostName      = hostName;
			this.ip            = ip;
			this.startOn       = startOn;
			this.stopOn        = null;
		}

		#endregion Constructor
	}
}