﻿using System;

namespace FORIS.TSS.BusinessLogic.Interfaces
{
	[Serializable()]
	public class OperatorInfo : IOperatorInfo
	{
		private int operatorId;

		public int OperatorId
		{
			get { return operatorId; }
		}

		private string login;

		public string Login
		{
			get { return login; }
		}

		private string name;

		public string Name
		{
			get { return name; }
		}

		private string appId;

		public string AppId
		{
			get { return appId; }
		}

		public OperatorInfo(int operatorId, string login, string name)
		{
			this.operatorId = operatorId;
			this.login = login;
			this.name = name;
		}
		public override string ToString()
		{
			return operatorId.ToString() + (name == null ? "" : " - " + name);
		}
		public override bool Equals(object obj)
		{
			/* Использовать синтаксис OperatorInfo?, если требуется объявлять и
			 * сравнивать не инициализированные структуры OperatorInfo
			 */

			IOperatorInfo oi = (IOperatorInfo)obj;

			return operatorId == oi.OperatorId;
		}
		public override int GetHashCode()
		{
			return operatorId.GetHashCode();
		}
		public static bool operator ==(OperatorInfo operator1, OperatorInfo operator2)
		{
			return OperatorInfo.Equals(operator1, operator2);
		}
		public static bool operator !=(OperatorInfo operator1, OperatorInfo operator2)
		{
			return !(operator1 == operator2);
		}
	}
}