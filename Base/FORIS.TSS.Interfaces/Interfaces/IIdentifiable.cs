﻿namespace FORIS.TSS.BusinessLogic.Interfaces
{
	public interface IIdentifiable
	{
		int ID { get; }
	}
}