﻿using System;

namespace FORIS.TSS.BusinessLogic
{
	public interface IRepetitionClass
	{
		DateTime? GetNextTimeInUTC(DateTime taskStart);
		string GetComment();
	}
}