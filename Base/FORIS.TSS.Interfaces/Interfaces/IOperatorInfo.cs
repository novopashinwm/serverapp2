﻿namespace FORIS.TSS.BusinessLogic.Interfaces
{
	/// <summary> Интерфейс информации об операторе </summary>
	public interface IOperatorInfo
	{
		int    OperatorId { get; }
		string Login      { get; }
		string Name       { get; }
	}
}