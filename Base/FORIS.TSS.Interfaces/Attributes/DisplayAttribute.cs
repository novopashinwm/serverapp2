﻿using System;

namespace FORIS.TSS.BusinessLogic.Attributes
{
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	[Serializable]
	public class DisplayNameAttribute : Attribute
	{
		/// <summary>
		/// Отображаемое имя
		/// </summary>
		string _sText;

		/// <summary>
		/// Признак того, что текст атрибута является ключом в файле ресурса
		/// </summary>
		/// <remarks>true - ключ в файле ресурса, false - новое имя свойства</remarks>
		private bool hasResource = false;

		public string DisplayName
		{
			get
			{
				return _sText;
			}
		}

		public bool HasResource
		{
			get
			{
				return hasResource;
			}
		}

		#region .ctor

		public DisplayNameAttribute(string Text)
			: base()
		{
			this._sText = Text;
			this.hasResource = false;
		}

		public DisplayNameAttribute(string Text, bool hasResource)
			: this(Text)
		{
			this.hasResource = hasResource;
		}

		public DisplayNameAttribute(string Text, int index)
			: base()
		{
			this._sText = Text;
			this.index = index;
		}

		public DisplayNameAttribute(string Text, int index, bool hasResource)
			: this(Text, index)
		{
			this.hasResource = hasResource;
		}

		#endregion // .ctor


		public override string ToString()
		{
			return _sText;
		}

		private int index = 0;
		public int Index
		{
			get
			{
				return this.index;
			}
		}
	}
}