﻿using System;

namespace FORIS.TSS.BusinessLogic.Attributes
{
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	[Serializable]
	public sealed class ControlTypeAttribute : Attribute
	{
		public ControlTypeAttribute(string type, string description = null)
		{
			Type        = type;
			Description = description ?? type;
		}
		public string Type        { get; private set; }
		public string Description { get; private set; }
	}
}