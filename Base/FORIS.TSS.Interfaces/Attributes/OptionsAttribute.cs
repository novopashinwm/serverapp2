﻿using System;
using System.Collections.Generic;

namespace FORIS.TSS.BusinessLogic.Attributes
{
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	[Serializable]
	public class OptionsAttribute : Attribute
	{
		private OptionsAttribute()
		{
		}
		public OptionsAttribute(params SystemRight[] rights)
		{
			Rights                 = rights;
			KeyValuePairCollection = new Dictionary<string, string>();
		}
		public OptionsAttribute(string options, params SystemRight[] rights)
		{
			KeyValuePairCollection = new Dictionary<string, string>();
			Rights                 = rights;
			foreach (var group in options.Split(';'))
			{
				var keyValuePair = group.Split(':');
				if (keyValuePair.Length != 2)
					throw new ArgumentException(string.Format("No keyValue pair passed. string: {0}", options));
				KeyValuePairCollection.Add(keyValuePair[0], keyValuePair[1]);
			}
		}
		public IEnumerable<SystemRight>   Rights                 { get; private set; }
		public Dictionary<string, string> KeyValuePairCollection { get; private set; }
	}
}