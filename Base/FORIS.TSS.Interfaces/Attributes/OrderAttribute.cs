﻿using System;
using System.Globalization;

namespace FORIS.TSS.BusinessLogic.Attributes
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Property | AttributeTargets.Field)]
	[Serializable]
	public class OrderAttribute : Attribute
	{
		private readonly int _order;
		public OrderAttribute(int order)
		{
			_order = order;
		}
		public int Value
		{
			get { return _order; }
		}
		public override string ToString()
		{
			return _order.ToString(CultureInfo.InvariantCulture);
		}
	}
}