﻿using System;

namespace FORIS.TSS.BusinessLogic.Attributes
{
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	[Serializable]
	public class TypeAttribute : Attribute
	{
		public Type Type { get; private set; }
		public int Index { get; private set; }

		private TypeAttribute()
		{
		}

		public TypeAttribute(Type type)
		{
			Type = type;
		}

		public TypeAttribute(Type type, int index)
			: this(type)
		{
			Index = index;
		}
	}
}