﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.BusinessLogic.Attributes
{
	public class MeasureAttribute : Attribute
	{
		public UnitOfMeasure Unit { get; private set; }

		public MeasureAttribute(UnitOfMeasure unit)
		{
			Unit = unit;
		}
	}
}