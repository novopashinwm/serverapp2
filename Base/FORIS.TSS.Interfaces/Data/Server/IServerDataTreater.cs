﻿namespace FORIS.TSS.BusinessLogic.Data.Server
{
	public interface IServerDataTreater :
		IDataTreater
	{
		void SessionDelete(int sessionId);
	}
}