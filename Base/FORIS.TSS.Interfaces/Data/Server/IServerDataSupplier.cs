﻿namespace FORIS.TSS.BusinessLogic.Data.Server
{
	/// <summary> Интерфейс поставщика данных сервера </summary>
	/// <remarks> Данные сервера содержат информацию о сессиях, параметрах сервера и т.д. </remarks>
	public interface IServerDataSupplier :
		IDataSupplier<IServerDataTreater>
	{
	}
}