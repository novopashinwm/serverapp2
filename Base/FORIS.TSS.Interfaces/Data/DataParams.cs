using System;

namespace FORIS.TSS.BusinessLogic.Data
{
	[Serializable]
	public class DataParams
	{
		private readonly static DataParams s_empty = new DataParams();

		public static DataParams Empty
		{
			get { return s_empty; }
		}

		public override bool Equals( object obj )
		{
			if( obj is DataParams )
			{
				return true;
			}

			return false;
		}
		public override int GetHashCode()
		{
			return 0;
		}

		public static bool operator ==( DataParams params1, DataParams params2 )
		{
			return DataParams.Equals( params1, params2 );
		}
		public static bool operator !=( DataParams params1, DataParams params2 )
		{
			return !( params1 == params2 );
		}
	}
}