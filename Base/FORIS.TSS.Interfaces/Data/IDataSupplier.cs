﻿using System;
using System.Data;
using System.Runtime.Remoting.Messaging;
using FORIS.TSS.Common;

namespace FORIS.TSS.BusinessLogic.Data
{
	public interface IDataSupplier<TParams, TDataTreater> :
		IDataSupplier<TDataTreater>
		where TParams : DataParams
		where TDataTreater : IDataTreater
	{
		TParams Params { get; set; }
		bool IsReadOnly();
		event AnonymousEventHandler<EventArgs> ReadOnlyChanged;
	}
	public interface IDataSupplier<TDataTreater> :
		IDataSupplier
		where TDataTreater : IDataTreater
	{
		TDataTreater GetDataTreater();
	}
	public interface IDataSupplier :
		IDisposable
	{
		/// <summary> Возвращает основной датасет c версией данных и подписывается на событие изменения данных </summary>
		/// <param name="handler">обработчик</param>
		/// <returns>
		/// Очень важно, чтоб два этих действия 
		/// выполнялись в один этап.
		/// Имя метода такое хитрое, чтоб обмануть
		/// EventTranslatorClientChannelSink.SyncProcessMessage().
		/// Он подумает, что это стандартная подписка на событие.
		/// </returns>
		DataInfo add_Connection(AnonymousEventHandler<DataChangedEventArgs> handler);
		/// <summary> Отписывается от события изменения данных </summary>
		/// <param name="handler">обработчик</param>
		/// <remarks>
		/// Имя метода такое хитрое, чтоб обмануть
		/// EventTranslatorClientChannelSink.SyncProcessMessage()
		/// Он подумает, что это стандартная отписка от события.
		/// </remarks>
		void remove_Connection(AnonymousEventHandler<DataChangedEventArgs> handler);
		/// <summary> Пустой метод. Обращение к нему продлевает жизнь объекту </summary>
		[OneWay]
		void Tick();
		/// <summary> Событие об изменении конфигурации поставщика данных (параметры и т.д.) </summary>
		/// <remarks>
		/// Обычно потребитель поставщика должен
		/// загрузить данные целиком заново 
		/// из поставщика методом add_Connection
		/// при получении этого события
		/// </remarks>
		event AnonymousEventHandler<EventArgs> Changed;
	}
	/// <summary> Эта структура для того, чтоб в одном пакете (запросе, обращении) передавать информацию о текущем состоянии (версии) контейнера данных </summary>
	[Serializable]
	public struct DataInfo
	{
		private readonly DataSet dataSet;
		public DataSet DataSet
		{
			get { return this.dataSet; }
		}
		private readonly Guid version;
		public Guid Version
		{
			get { return this.version; }
		}
		public DataInfo(DataSet dataSet, Guid version)
		{
			this.dataSet = dataSet;
			this.version = version;
		}
		#region Empty
		private static readonly DataInfo s_empty = new DataInfo(null, Guid.Empty);
		public static DataInfo Empty
		{
			get { return DataInfo.s_empty; }
		}
		#endregion Empty
	}
	[Serializable]
	public class DataChangedEventArgs : EventArgs
	{
		private readonly Guid updatableVersion;
		public Guid UpdatableVersion
		{
			get { return this.updatableVersion; }
		}
		private readonly Guid currentVersion;
		public Guid CurrentVersion
		{
			get { return this.currentVersion; }
		}
		private readonly DataSet deleted;
		public DataSet Deleted
		{
			get { return this.deleted; }
		}
		private readonly DataSet added;
		public DataSet Added
		{
			get { return this.added; }
		}
		private readonly DataSet modified;
		public DataSet Modified
		{
			get { return this.modified; }
		}
		public DataChangedEventArgs(
			Guid updatableVersion, Guid currentVersion, DataSet deleted, DataSet added, DataSet modified)
		{
			this.updatableVersion = updatableVersion;
			this.currentVersion   = currentVersion;
			this.deleted          = deleted;
			this.added            = added;
			this.modified         = modified;
		}
	}
}