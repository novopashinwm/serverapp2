﻿using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO.OperatorRights;
using FORIS.TSS.BusinessLogic.DTO.Rules;

namespace FORIS.TSS.BusinessLogic.Server
{
	/// <summary> Функционал для управлениями правилами, связанных с объектами наблюдения </summary>
	/// <remarks> Правило применяется при поступлении очередной позиции от устройства.
	/// В случае срабатывания условия, указанного для правила, выполняется действие, указанное в правиле.</remarks>
	public interface IRuleServer
	{
		bool IsRuleServerAllowed { get; }
		/// <summary>Возвращает список правил, назначенных для указанного объекта (машина или группа машин)</summary>
		List<Rule> GetRules(IdType idType, int id);
		/// <summary> Удаляет правило по id </summary>
		void RemoveRule(int ruleID);
		/// <summary> Сохраняет правило в БД (новое или с изменением существующего) </summary>
		/// <returns> Возвращает идентификатор правила (нужно при создании новых правил)</returns>
		Rule SaveRule(Rule rule);
		/// <summary> Возвращает список операторов, у которых есть доступ к правилам данного объекта </summary>
		/// <param name="idType">Тип идентификатора объекта.
		/// Доступно: <see cref="IdType.Vehicle"/> и <see cref="IdType.VehicleGroup"/></param>
		/// <param name="id">Идентификатор объекта</param>
		List<Operator> GetOperators(IdType idType, int id);
	}
}