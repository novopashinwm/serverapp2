﻿using System;

namespace FORIS.TSS.BusinessLogic.Server
{
	/// <summary> Тип идентификатора </summary>
	[Serializable]
	public enum IdType
	{
		/// <summary> Идентификатор машины </summary>
		Vehicle,
		/// <summary> Идентификатор группы машин </summary>
		VehicleGroup,
		/// <summary> Идентификатор оператора </summary>
		Operator,
		/// <summary> Идентификатор группы операторов </summary>
		OperatorGroup,
		/// <summary> Идентификатор зоны </summary>
		Zone,
		/// <summary> Идентификатор группы зон </summary>
		ZoneGroup,
		/// <summary> Идентификатор клиента </summary>
		Department
	}
}