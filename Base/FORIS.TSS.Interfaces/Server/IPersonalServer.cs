﻿using System;
using System.Data;
using System.Runtime.Remoting.Lifetime;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.BusinessLogic.Security;

namespace FORIS.TSS.BusinessLogic.Server
{
	/// <summary> Основной интерфейс сессии пользователя </summary>
	/// <remarks>
	/// IPersonalServer реализует ISponsor, чтоб объекты, рожденные сессией на
	/// стороне сервера, продлевали свою лицензию именно у объекта сессии,
	/// и не требовали подтверждения лицензии на стороне клиента.
	/// </remarks>
	public interface IPersonalServer : ISponsor
	{
		string ApplicationName { get; }
		ISessionInfo SessionInfo { get; }
		/// <summary> Закрыть сессию </summary>
		void Close();
		/// <summary> Событие, происходящее перед закрытием </summary>
		event EventHandler Closing;
		/// <summary> Событие, происходящее после закрытия </summary>
		event EventHandler Closed;
		/// <summary> Время на которое обновляется лицензия при вызове метода объекта через инфраструктуру Remoting </summary>
		TimeSpan LeaseRenewOnCallTime { get; }
		/// <summary> Метод ничего не делает. Нужен только для проверки жизнеспособности </summary>
		bool IsAlive();
		/// <summary> Изменяет/задает собственный пароль оператора </summary>
		/// <param name="password"></param>
		/// <remarks>
		/// Может возникнуть исключение в случае
		/// если пользователю не разрешено менять
		/// собственный пароль
		/// </remarks>
		SetNewPasswordResult SetPassword(string password);
		/// <summary> Устанавливает новый логин оператора </summary>
		/// <param name="newLogin">Новое значение логина</param>
		/// <returns>Результат установки нового логина оператора</returns>
		SetNewLoginResult SetNewLogin(string newLogin);
		TssPrincipalInfo GetPrincipal();
	}
}