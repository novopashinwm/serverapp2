﻿using System;
using System.Net;

namespace FORIS.TSS.BusinessLogic.Server
{
	public interface ISessionManager
	{
		IPersonalServer CreateSession(Guid clientId, string clientVersion);
		IPersonalServer CreateSession(int operatorId, Guid clientId, string clientVersion, IPAddress address);
	}
}