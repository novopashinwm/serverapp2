﻿using FORIS.TSS.BusinessLogic.DTO;
using Res = FORIS.TSS.BusinessLogic.ResultCodes;
namespace FORIS.TSS.BusinessLogic.Server
{
	public interface ISalesManagerServer
	{
		/// <summary> Сбрасывает пароль и возвращает учетные данные для входа в систему администратора корпоративного абонента </summary>
		/// <returns> Если корпоративный абонент не заведен в системе, то возвращает null. </returns>
		/// <remarks> Можно было бы просто возвращать текущий логин и пароль, но тогда бы не удалось фиксировать все случаи
		/// получения учетных данных клиента персональным менеджером.</remarks>
		LoginInfo ResetAdministratorPassword(string login);
		/// <summary> Проверяет, может ли объект наблюдения быть добавлен </summary>
		/// <param name="trackerType"> Тип объекта наблюдения, одно из значений Controller_Type.Type_Name </param>
		/// <param name="trackerNumber"> Идентификатор терминала (IMEI или серийный номер) - Controller_Info.Device_ID </param>
		Res::AddTrackerResult CheckTrackerToAddByType(string trackerType, string trackerNumber);
		/// <summary> Добавляет объект наблюдения по его типу и номеру </summary>
		/// <param name="trackerType"> Тип объекта наблюдения, одно из значений Controller_Type.Type_Name </param>
		/// <param name="trackerNumber"> Идентификатор терминала (IMEI или серийный номер) - Controller_Info.Device_ID </param>
		/// <param name="vehicleId"> Идентификатор созданного объекта наблюдения </param>
		Res::AddTrackerResult AddTrackerByType(string trackerType, string trackerNumber, out int vehicleId);
		/// <summary> Проверяет, может ли объект наблюдения быть добавлен </summary>
		/// <param name="templateVehicleId"> Идентификатор объекта наблюдения, атрибуты которого должны быть скопированы в новый объект </param>
		/// <param name="trackerNumber"> Идентификатор терминала (IMEI или серийный номер) - Controller_Info.Device_ID </param>
		Res::AddTrackerResult CheckTrackerToAddByTemplate(int templateVehicleId, string trackerNumber);
		/// <summary> Добавляет объект наблюдения по его типу и номеру </summary>
		/// <param name="templateVehicleId"> Идентификатор объекта наблюдения, атрибуты которого будут скопированы в новый объект </param>
		/// <param name="trackerNumber"> Идентификатор терминала (IMEI или серийный номер) - Controller_Info.Device_ID </param>
		/// <param name="vehicleId"> Идентификатор созданного объекта наблюдения </param>
		Res::AddTrackerResult AddTrackerByTemplate(int templateVehicleId, string trackerNumber, out int vehicleId);
	}
}