﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Net.Sockets;
using System.Reflection;
using System.Security.Principal;
using System.Threading;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Infrastructure.Authentication;

namespace FORIS.TSS.WorkplaceSharnier
{
	/// <summary> Базовый класс объекта клиента </summary>
	/// <typeparam name="TPersonalServer"></typeparam>
	public abstract class ClientBase<TPersonalServer> :
		Component,
		IClientDataProvider
		where TPersonalServer : IPersonalServer
	{
		public event EventHandler NeedReConnect;
		private readonly Guid     clientId;
		private readonly string   clientVersion;

		#region Constructor & Dispose

		public ClientBase(NameValueCollection parameters)
		{
			clientId = Guid.NewGuid();

			AssemblyName LibraryName = GetType().Assembly.GetName();

			clientVersion = string.Format("{0}.{1}.{2}.{3}",
				LibraryName.Version.Major,
				LibraryName.Version.Minor,
				LibraryName.Version.Build,
				LibraryName.Version.Revision);

			var clientVersionConfig = parameters[GetType().FullName + ".ClientVersion"];
			if (!string.IsNullOrWhiteSpace(clientVersionConfig))
				clientVersion += ":" + clientVersionConfig;
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				lock (syncRoot)
				{
					if (pollingTimer != null)
					{
						pollingTimer.Dispose();
						pollingTimer = null;
					}
					var sessionCopy = session;
					if (!Equals(sessionCopy, default(TPersonalServer)))
					{
						try
						{
							sessionCopy.Close();
						}
						catch (Exception exception)
						{
							Trace.TraceError(exception.ToString());
						}
					}
					session = default(TPersonalServer);
					state   = ClientState.Disconnected;
				}
			}

			base.Dispose(disposing);
		}

		#endregion Constructor & Dispose

		#region Properties

		private TPersonalServer session;
		[Browsable(false)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public TPersonalServer Session
		{
			get { return session; }
		}
		public string ApplicationName
		{
			get { return Session.ApplicationName; }
		}
		public string ServerURL
		{
			get { return serverURL; }

			set { serverURL = value; }
		}

		#endregion Properties

		#region IClient Members

		protected readonly object syncRoot = new object();
		// TODO: Сделать управление состоянием клиента
		private ClientState state = ClientState.Undefined;
		protected string serverURL = string.Empty;
		public ClientState State
		{
			get { return state; }
		}
		IPersonalServer IClient.Session
		{
			get { return Session; }
		}
		void IClient.Connect(string sessionManagerUrl)
		{
			Connect(sessionManagerUrl, ConfigHelper.GetAppSettingAsBoolean("ClientBase.AllowUI", false));
		}
		void IClient.Start()
		{
			Start();
		}
		void IClient.Stop()
		{
			Stop();
		}

		#endregion IClient Members

		private Timer pollingTimer;

		private void pollingTimer_Callback(object state)
		{
			var time = DateTime.Now;
			try
			{
				lock (syncRoot)
				{
					if (this.state == ClientState.Connected)
					{
						Session.IsAlive();
						pollingTimer.Change((int)renewOnCallTime.TotalMilliseconds / 4, -1);
					}
				}
			}
			catch (ThreadAbortException)
			{
			}
			catch (Exception ex)
			{
				$"{GetType().Name} Started at {time} error"
					.WithException(ex)
					.CallTraceError();
				NeedReConnect?.Invoke(this, null);
			}
		}

		protected ISessionManager sessionManager;

		/// <summary> Создать объект сессии в контексте настройки объекта клиента </summary>
		/// <returns>
		/// Это почти синхронный метод. Конечно он создает 
		/// сторонний поток для выполнения работы, однако, 
		/// метод ожидает завершения этого потока при любом сценарии.
		/// </returns>
		/// <exception cref="ApplicationException">
		/// Invalid login name
		/// </exception>
		private IPersonalServer createSessionHandler()
		{
			#region Preconditions

			if (sessionManager == null)
				throw new ApplicationException("There is no sessionManager in client object");

			#endregion Preconditions

			IPersonalServer       sessionHolder          = null;
			SessionExistException createSessionException = null;

			Thread createSessionThread = new Thread(delegate ()
			{
				try
				{
					var identity = Thread.CurrentPrincipal.Identity;
					if (identity.Name == string.Empty)
						Thread.CurrentPrincipal = new GenericPrincipal(
							new GenericIdentity(LoginHelper.GetUserDomainName()), null);
					sessionHolder = sessionManager.CreateSession(clientId, clientVersion);
				}
				catch (SessionExistException ex)
				{
					sessionHolder = null;
					createSessionException = ex;
				}
				catch (ThreadAbortException)
				{
					sessionHolder = null;
				}
				catch (SocketException ex)
				{
					sessionHolder = null;
					$"{GetType().Name} Unexpected error"
						.WithException(ex)
						.CallTraceError();
				}
				catch (Exception ex)
				{
					sessionHolder = null;
					$"{GetType().Name} Unexpected error"
						.WithException(ex)
						.CallTraceError();
				}
			});

			createSessionThread.Start();
			if (!createSessionThread.Join(10000))
			{
				createSessionThread.Abort();
				createSessionThread.Join();
				throw new TimeoutException("Timeout get session object");
			}

			if (createSessionException != null)
				throw createSessionException;

			return sessionHolder;
		}
		public void Connect(string sessionManagerUrl, bool allowUI)
		{
			sessionManager = (ISessionManager)Activator.GetObject(typeof(ISessionManager), sessionManagerUrl);
			var sessionHolder = LoginHelper.Login(createSessionHandler, allowUI);
			if (sessionHolder == null)
				throw new ApplicationException($"SessionHolder is null when connecting to url {sessionManagerUrl}");
			if (!(sessionHolder is TPersonalServer))
				throw new ApplicationException($"Invalid type of server session object: {sessionHolder.GetType()} when expected: {typeof(TPersonalServer)}");
			////////////////////////////////////////
			Connect((TPersonalServer)sessionHolder);
			////////////////////////////////////////
		}
		/// <summary> Период опроса сервера - проверка доступности сервера </summary>
		TimeSpan renewOnCallTime;
		public void Connect(TPersonalServer personalServer)
		{
			lock (syncRoot)
			{
				session = personalServer;

				renewOnCallTime = session.LeaseRenewOnCallTime;

				pollingTimer = new Timer(
					new TimerCallback(pollingTimer_Callback),
					null,
					(int)renewOnCallTime.TotalMilliseconds / 4,
					-1);

				TssPrincipalInfo p = session.GetPrincipal();

				if (p != default(TssPrincipalInfo))
					Thread.CurrentPrincipal = p;

				state = ClientState.Connected;
			}
		}

		protected abstract void ReConnect();

		#region Start & Stop

		protected abstract void Start();
		protected abstract void Stop();

		#endregion Start & Stop
	}

	public interface IClient : IDisposable
	{
		string          ApplicationName { get; }
		ClientState     State           { get; }
		IPersonalServer Session         { get; }
		void Connect(string sessionManagerUrl);
		void Start();
		void Stop();
	}
	public enum ClientState
	{
		Undefined,
		Connected,
		Disconnected
	}
}