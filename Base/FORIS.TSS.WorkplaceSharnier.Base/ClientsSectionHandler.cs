﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Xml;

namespace FORIS.TSS.WorkplaceSharnier
{
	public class ClientsSectionHandler :
		System.Configuration.IConfigurationSectionHandler
	{
		/* Нет уверенности, что за время работы приложения этот метод вызывается лишь однажды */
		public object Create(object parent, object configContext, XmlNode section)
		{
			Dictionary<string, ClientApplicationConfiguration> Clients =
				new Dictionary<string, ClientApplicationConfiguration>();

			foreach (XmlNode node in section.ChildNodes)
			{
				switch (node.Name)
				{
					#region client

					case "client":

						Clients.Add(
							node.Attributes["name"].Value,
							new ClientApplicationConfiguration(node)
							);

						break;

					#endregion client

					#region other

					default:
						{
							// Если это не коммент, то выбрасываем исключение
							if (node.NodeType != XmlNodeType.Comment)
							{
								throw new ApplicationException(
									String.Format(
										"Unexpected node {0} in section 'clients'",
										node.Name
										)
									);
							}
							break;
						}

						#endregion other
				}
			}

			return Clients;
		}
	}

	public class ClientApplicationConfiguration
	{
		private readonly string name;
		private readonly Type type;
		private readonly string server;
		private readonly NameValueCollection properties;

		#region Properties

		public string Name
		{
			get { return name; }
		}

		public Type Type
		{
			get { return type; }
		}

		public string Server
		{
			get { return server; }
		}

		public NameValueCollection Properties
		{
			get { return properties; }
		}

		#endregion Properties

		public ClientApplicationConfiguration(XmlNode node)
		{
			try
			{
				this.name = node.Attributes["name"].Value;
				this.type = Type.GetType(node.Attributes["type"].Value, true);
				this.server = node.Attributes["server"].Value;

				#region properties

				this.properties = new NameValueCollection();
				foreach (XmlNode childNode in node.ChildNodes)
				{
					switch (childNode.Name)
					{
						#region add

						case "add":
							{
								this.properties.Set(
									childNode.Attributes["key"].Value,
									childNode.Attributes["value"].Value
									);
							}
							break;

						#endregion add
						#region #comment

						case "#comment":
							/* Комментарии не обрабатываются
							 */
							break;

						#endregion #comment
						#region other

						default:
							throw new ApplicationException(
								String.Format(
									"Unexpected node {0} in server section {1}",
									childNode.Name,
									this.name
									)
								);

							#endregion other
					}
				}
				#endregion properties
			}
			catch (Exception ex)
			{
				Trace.WriteLine(
					string.Format(
						"{0}.ctor( XmlNode ) Exception: {1}",
						this.GetType().Name,
						ex.Message
						)
					);
			}
		}

		/// <summary> Создает экземпляр объекта клиента и инициирует подключение к серверу согласно настройкам в файле конфигурации </summary>
		/// <returns></returns>
		public IClient CreateInstance()
		{
			IClient result =
				(IClient)Activator.CreateInstance(this.type, new object[] { this.properties });

			result.Connect(this.server);

			return result;
		}
	}
}