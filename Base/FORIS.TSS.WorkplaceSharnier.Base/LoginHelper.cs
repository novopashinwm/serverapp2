﻿using System;
using System.Diagnostics;
using System.Security.Principal;
using System.Threading;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Infrastructure.Authentication;

namespace FORIS.TSS.WorkplaceSharnier
{
	public static class LoginHelper
	{
		public static IPersonalServer Login(CreateSessionHandler createSessionHandler, bool allowUI)
		{
			IPersonalServer SessionHolder = null;
			{
				#region SSPI
				try
				{
					SessionHolder = createSessionHandler();
					if (LoginTraceSwitch.TraceInfo)
						$"SSPI authentication has been successfully."
							.CallTraceInformation();
				}
				catch (SessionExistException ex)
				{
					if (LoginTraceSwitch.TraceWarning)
						$"SSPI authentication failed."
							.WithException(ex)
							.CallTraceError();
					throw ex;
				}
				catch (Exception ex)
				{
					if (LoginTraceSwitch.TraceWarning)
						$"SSPI authentication failed."
							.WithException(ex)
							.CallTraceError();
				}

				#endregion SSPI

				#region Login only

				/*
				 * Если серверу не удалось проверить пользователя,
				 * то вряд ли он должен позволять подключение лишь
				 * по имени без запроса пароля.
				 */

				try
				{
					// login failed, switch to login-only authentication
					if (SessionHolder == null)
					{
						Thread.CurrentPrincipal = new GenericPrincipal(
							new GenericIdentity(WindowsIdentity.GetCurrent().Name), null);
						SessionHolder = createSessionHandler();
					}
				}
				catch (Exception ex)
				{
					if (LoginTraceSwitch.TraceError)
						$"Login only authentication failed."
							.WithException(ex)
							.CallTraceError();
				}

				#endregion Login only
			}

			/* Сюда мы доходим в случае, если создан объект 
			 * сессии на сервере, и ссылка возвращена клиенту, 
			 * а значит клиент идентифицирован и все хорошо.

			 * Однако в случае, если запрещено использование 
			 * UI для прохождения аутентификации, объект
			 * сессии пользователя может быть и не получен.
			 */

			return SessionHolder;
		}
		public static readonly TraceSwitch LoginTraceSwitch =
			new TraceSwitch("Login", "", "Warning"); // По умолчанию уровень Warning
		/// <summary> Возвращает доменное имя пользователя </summary>
		public static string GetUserDomainName()
		{
			var login = Environment.UserName;
			if (!string.IsNullOrEmpty(Environment.UserDomainName))
				login = $@"{Environment.UserDomainName}\{Environment.UserName}";
			return login;
		}
	}
	public delegate IPersonalServer CreateSessionHandler();
}