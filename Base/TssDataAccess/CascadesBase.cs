﻿namespace FORIS.DataAccess
{
	/// <summary> Summary description for CascadesBase </summary>
	public class CascadesBase
	{
		protected int trail_id;
		public int TrailID
		{
			get
			{
				return trail_id;
			}
			set
			{
				trail_id = value;
			}
		}
	}
}