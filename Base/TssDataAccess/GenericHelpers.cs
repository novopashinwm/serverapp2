﻿using System;
using System.Data;
using System.Diagnostics;
using System.Text;

namespace FORIS.DataAccess
{
	/// <summary> Summary description for GenericHelpers </summary>
	public class GenericHelpers : IDisposable
	{
		IDbConnection  conn = null;
		IDbTransaction tran = null;
		bool dispose_at_exit = true;
		public IDbConnection Connection
		{
			get
			{
				return conn;
			}
		}
		public IDbTransaction Transaction
		{
			get
			{
				return tran;
			}
		}
		public GenericHelpers(IDbConnection connection, IDbTransaction transaction)
		{
			conn = connection;
			tran = transaction;
			dispose_at_exit = false;
		}
		public GenericHelpers()
		{
			conn = Globals.TssDatabaseManager.DefaultDataBase.Connection();
			conn.Open();
			tran = null;
		}
		public void Dispose()
		{
			if (dispose_at_exit)
			{
				conn.Dispose();
			}
		}
		public DataSet SelectAllObjects(string tablename)
		{
			return SelectObjectsWithWhere(tablename, null);
		}
		public DataSet SelectLinksBetweenAliveObjects(string one, string another)
		{
			return SelectLinksBetweenAliveObjects(one, true, another, true);
		}
		public DataSet SelectLinksBetweenAliveObjects(string one, bool check_one, string another, bool check_another)
		{
			string tablename = one + "_" + another;
			StringBuilder where = new StringBuilder(1024);
			if (check_one)
			{
				where.AppendFormat("({0}_ID in (SELECT {0}_ID FROM {0}))", one);
			}
			if (check_one && check_another)
			{
				where.Append(" AND ");
			}
			if (check_another)
			{
				where.AppendFormat("({0}_ID in (SELECT {0}_ID FROM {0}))", another);
			}
			return SelectObjectsWithWhere(tablename, where.ToString());
		}
		public DataSet SelectAliveObjects(string tablename)
		{
			return SelectObjectsWithWhere(tablename, "1=1");
		}
		public DataSet SelectObjectsWithWhere(string tablename, string where)
		{
			string text = "select * from [" + tablename + "]";
			if (where != null && where.Trim().Length > 0)
			{
				text = text + " where " + where;
			}
			return SelectObjects(tablename, text);
		}
		public DataSet SelectObjects(string tablename, string query_text)
		{
			DataSet dataSet = new DataSet();
			using (IDbCommand selectCommand = Globals.TssDatabaseManager.DefaultDataBase.Command(query_text))
			{
				selectCommand.Connection  = conn;
				selectCommand.Transaction = tran;
				selectCommand.CommandType = CommandType.Text;
				Debug.WriteLine(selectCommand.CommandText);
				try
				{
					IDbDataAdapter adapter = Globals.TssDatabaseManager.DefaultDataBase.Adapter();
					adapter.SelectCommand = selectCommand;
					adapter.Fill(dataSet);
					dataSet.Tables[0].TableName = tablename;
				}
				finally
				{
					selectCommand.Parameters.Clear();
				}
			}
			return dataSet;
		}
		public void ExecuteNonQuery(string query_text)
		{
			TssDatabase database = Globals.TssDatabaseManager.DefaultDataBase;
			using (IDbCommand command = database.Command(query_text, null))
			{
				command.CommandType = CommandType.Text;
				command.Connection  = conn;
				command.Transaction = tran;
				command.ExecuteNonQuery();
			}
		}
		public static void SetConnectionTransaction(IDbDataAdapter da, IDbConnection connection, IDbTransaction transaction)
		{
			if (da.InsertCommand != null)
			{
				da.InsertCommand.Connection  = connection;
				da.InsertCommand.Transaction = transaction;
			}
			if (da.UpdateCommand != null)
			{
				da.UpdateCommand.Connection  = connection;
				da.UpdateCommand.Transaction = transaction;
			}
			if (da.DeleteCommand != null)
			{
				da.DeleteCommand.Connection  = connection;
				da.DeleteCommand.Transaction = transaction;
			}
		}
	}
}