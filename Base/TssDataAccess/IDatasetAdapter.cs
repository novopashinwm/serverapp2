﻿using System.Data;

namespace FORIS.DataAccess
{
	public delegate void BeforeDeleteHandler();
	public interface IDatasetAdapter
	{
		void Update(DataSet dataset, IDbConnection connection, IDbTransaction transaction);
		int CreateTransactionInDatabase(IDbConnection connection, IDbTransaction transaction);
		int SessionID
		{
			get;
			set;
		}
	}
}