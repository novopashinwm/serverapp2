﻿using System.Collections;
using System.Data;

namespace FORIS.DataAccess
{
	public class IDbDataAdapterCollection : CollectionBase
	{
		public IDbDataAdapter this[int index]
		{
			get
			{
				return (IDbDataAdapter)InnerList[index];
			}
			set
			{
				InnerList[index] = value;
			}
		}
		public void Add(IDbDataAdapter adapter)
		{
			InnerList.Add(adapter);
		}
	}
}