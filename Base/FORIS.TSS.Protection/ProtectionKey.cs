﻿using System;
using System.Diagnostics;
using System.Text;
#if (HASP)
using Aladdin.HASP;
#endif

namespace FORIS.TSS.Protection
{
	internal class VendorCode
	{
		private const string vendorCodeString = 
			"2i5vLMzAVbTslIvya90fTHng8Jv9orHCflgOKsf6pLW5cb7ULaIZC5E9YHm9aygm8+UPAGeveHKBjgAl"+
			"vkZQMORJk48xs8dKxWz7Pel3n75rBHhhSf+caF/Nh6x56M/HQb0t7a4jvsoDRNZpEzFnPRS5jlLbp0Tj"+
			"1Utpu3x1VZ5xFgkzwNQM/sNHixnRujHwkSTRYPrd0F/xXoxW49yOAU/ZOSwnnuIB+apyZYLl6RIdw5G2"+
			"DYlHyBMDi1sQ0uAml8XopWtNnybr87Py0+GxO3iX6tRmW3JVzeOc4RzNnjAjehIF5GZ7f6d/TJ4nmGv5"+
			"/8G91r1xQi9/rfL3JDGjJz8yUCtgCy4MFFrQY5q2A/+hHWp7y/Hd09X9hYIPC2ekJgJLuP0y5h1DEuQR"+
			"nG6MTJFmlxPlBm/ckD3WDzxK9oWx85q7cAVB2NarYKPu8LbugZ+o+Zrq1qoxvHRFppfrJA7Lz8FZBXQf"+
			"oG1Ys3OsXA9szhqT8CGXj2UfoEX4ZSsybox3IaoJovVdVZawU+jZahwiTm6PnYyPc2p0x6Htqhhm0ot7"+
			"KnturBiWqruxJvMUX8DJOBPWUBANXnbl3NK7evUbfuiSuHGuMLFx4WLJ1xYdK55ns6EqmHfAYvgJVsXl"+
			"BrQbmipY9JPmTVQpNZdECz8IdRFY0jjCcOKJUpoDlvzL0UUCAlw/MFAwZPhnQId2AnqMFibTC4JqHywZ"+
			"K84QAKn02M/7";
		static public byte[] Code
		{
			get
			{
				return Encoding.Default.GetBytes(vendorCodeString);
			}
		}
	}

	/// <summary>
	/// Класс ProtectionKey, инкапсулирующий некоторые (пока) возможности HASP API
	/// </summary>
	public class ProtectionKey
	{
		/// <summary>
		/// Массив для хранения пароля шифрования данных с помощью симметричных алгоритмов
		/// </summary>
		private static byte[] validateBytes;

		internal const int START_APP_FEAUTURE = 1;
		internal const int GET_DATA_FEAUTURE = 2;
		internal const int UPDATE_DATA_FEAUTURE = 3;


		internal static 
#if HASP
			Hasp Login(int nFeature)
#else
			object Login(int nFeature)
#endif
		{
#if (TOMSK)
			return null;
#elif (DEBUG || EMULATE)
			return null;
#elif (HASP)
			//if(Globals.AppSettings["HaspProtection"] == "disabled")
			//    return null;

			FeatureOptions options = FeatureOptions.Default;

			HaspFeature feature = HaspFeature.FromProgNum(nFeature);
		
			feature.SetOptions(options, FeatureOptions.Default);
		
			//Объект "Key"
			Hasp key = new Hasp(feature);
			HaspStatus status = key.Login(VendorCode.Code);

			if(status != HaspStatus.StatusOk)
			{
				return null;
			}
			return key;
#else
			return null;
#endif
		}
		internal static bool CanExecuteFeature(int nFeature)
		{

#if (TOMSK)
			//Для Томкса Лицензия по сроку только на карты
			/*
			if (DateTime.Now > new DateTime(2012, 5, 1))
			{
				return false;
			}
			*/
			return true;
#elif (DEBUG || EMULATE)
			return true;
#elif (HASP)
			Hasp key = null;
			try
			{
				//if (Globals.AppSettings["HaspProtection"] != null && Globals.AppSettings["HaspProtection"] == "disabled")
				//    return true;

				key = Login(nFeature);

				if (key == null)
					return false;

				return true;
			}
			catch (Exception ex)
			{
				Trace.TraceError("{0}", ex);
			}
			finally
			{
				if (key != null)
					key.Logout();
			}
			return false;
#elif (KOLPINO || MTSINDIA)
			return DateTime.Now < DeadLine;
#else
			return true;
#endif
		}

#if (MTSINDIA)
		private static readonly DateTime DeadLine = new DateTime(2014, 04, 28);
#endif

#if (KOLPINO)
		private static readonly DateTime DeadLine = DateTime.MaxValue;
#endif

		#region Методы проверяющие наличие лицензий на определенную функциональность
		/// <summary>
		/// Определяет, имеет ли User право запустить сервер
		/// </summary>
		/// <returns>В случае Debug, всегда возвращает true</returns>
		public static bool CanStartServer()
		{
			return CanExecuteFeature(START_APP_FEAUTURE);
		}
		/// <summary>
		/// Определяет, имеет ли User право получать данные с бд
		/// </summary>
		/// <returns>В случае Debug, всегда возвращает true</returns>
		public static bool CanGetData()
		{
			return CanExecuteFeature(GET_DATA_FEAUTURE);
		}
		/// <summary>
		/// Определяет, имеет ли User право обновлять данные в бд
		/// </summary>
		/// <returns>В случае Debug, всегда возвращает true</returns>
		public static bool CanUpdateData()
		{
			return CanExecuteFeature(UPDATE_DATA_FEAUTURE);
		}

		/// <summary>
		/// Определяет, имеет ли User право на работу с указанной картой
		/// </summary>
		/// <returns>Только для Томска ограничено по времени</returns>
		public static bool CanUseMap(Guid mapGuid)
		{
			//return CanExecuteFeature(GET_DATA_FEAUTURE);
			#if (TOMSK)
						if (DateTime.Now > new DateTime(2012, 5, 1))
						{
							return false;
						}
						return true;
			#else
						return true;
			#endif
		}
		#endregion

		/// <summary>
		/// количество машин
		/// </summary>

		public static int ValidVehiclesCount
		{
			get
			{
#if (TOMSK)
				return 1000;
#elif (DEBUG)
				return int.MaxValue;
#elif (EMULATE)
				return 5000000;
#elif (HASP)
				//if(Globals.AppSettings["HaspProtection"] == "disabled")
				//    return int.MaxValue;

				Hasp key = null;
				try
				{
					key = Login(0);
					if(key == 0)
						throw new Exception();

					HaspFile hfile = key.GetFile(HaspFiles.Main);
					if(hfile == 0)
						throw new Exception();

					int size = 0;
					int offset = 0;

					HaspStatus status = hfile.FileSize(ref size);

					if (HaspStatus.StatusOk != status || size == 0)
						throw new Exception();
					
					byte[] data = new byte[4]; //Читаем первые 4 байта - кв-о машин,
					status = hfile.Read(data, offset, data.Length);

					if (HaspStatus.StatusOk != status)
						throw new Exception();

					int VehiclesCount = BitConverter.ToInt32(data, 0);
					key.Logout();
					return VehiclesCount;
				}
				catch(Exception)
				{
					if(key != 0)
						if (key != null) key.Logout();
					return 0;
				}
#else
				return 5000000;
#endif
			}
		}
		/// <summary>
		/// Количество пользователей
		/// </summary>
		public static int ValidUsersCount
		{
			get
			{
#if (TOMSK)
				return 1000;
#elif DEBUG
				return int.MaxValue;
#elif EMULATE
				return 5000000;
#elif (HASP)
				//if(Globals.AppSettings["HaspProtection"] == "disabled")
				//    return int.MaxValue;

				Hasp key = null;
				try
				{
					key = Login(0);
					if(key == 0)
						throw new Exception();

					HaspFile hfile = key.GetFile(HaspFiles.Main);
					if(hfile == 0)
						throw new Exception();

					int size = 0;
					int offset = 0;

					HaspStatus status = hfile.FileSize(ref size);

					if (HaspStatus.StatusOk != status || size == 0)
						throw new Exception();
					
					byte[] data = new byte[8]; //Читаем вторые 4 байта- кв-о юзеров,
					status = hfile.Read(data, offset, data.Length);

					if (HaspStatus.StatusOk != status)
						throw new Exception();

					int UsersCount = BitConverter.ToInt32(data, 4);
					key.Logout();
					return UsersCount;
				}
				catch(Exception)
				{
					if(key != 0)
						if (key != null) key.Logout();
					return 0;
				}
#else
				return 5000000;
#endif
			}
		}

		///<summary>
		///</summary>
		public static byte[] ValidateBytes
		{
			get { return validateBytes; }
			set { validateBytes = value; }
		}

		/// <summary>
		/// Количество контроллеров
		/// </summary>
		public static int ValidControllersCount
		{
			get
			{
#if (TOMSK)
				return 1000;
#elif (DEBUG)
				return int.MaxValue;
#elif (EMULATE)
				return 5000000;
#elif (HASP)
				//if(Globals.AppSettings["HaspProtection"] == "disabled")
				//    return int.MaxValue;

				Hasp key = null;
				try
				{
					key = Login(0);
					if(key == 0)
						throw new Exception();

					HaspFile hfile = key.GetFile(HaspFiles.Main);
					if(hfile == 0)
						throw new Exception();

					int size = 0;
					int offset = 0;

					HaspStatus status = hfile.FileSize(ref size);

					if (HaspStatus.StatusOk != status || size == 0)
						throw new Exception();
					
					byte[] data = new byte[12]; //Читаем третьи 4 байта - кв-о контроллеров,
					status = hfile.Read(data, offset, data.Length);

					if (HaspStatus.StatusOk != status)
						throw new Exception();

					int ControllersCount = BitConverter.ToInt32(data, 8);
					key.Logout();
					return ControllersCount;
				}
				catch(Exception)
				{
					if(key != 0)
						if (key != null) key.Logout();
					return 0;
				}
#else
				return 5000000;
#endif
			}
		}
	}
}