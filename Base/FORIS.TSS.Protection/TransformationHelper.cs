﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Security.Cryptography;

namespace FORIS.TSS.Protection
{
	public class TransformationHelper
	{

		// key - набор байт, получаемый из картинки-ключа, которая полмещается в ресурсы программы
		// version - набор байт, получаемый из строки версии программы Major.Minor.Build.Revision
		// password - окончательный набор байт, который используется при шифровании

#if DEBUG
		/// <summary>
		/// 
		/// </summary>
		/// <param name="password">пароль</param>
		/// <param name="byteKey1">массив для шифрования (6 байт)</param>
		/// <param name="byteKey2">массив для шифрования (4 байта)</param>
		/// <returns></returns>
		public static byte[] Encrypt(byte[] password, byte[] byteKey1, byte[] byteKey2)
		{
			SymmetricAlgorithm alg = (SymmetricAlgorithm)DESCryptoServiceProvider.Create();

			//класс, позволяющий генерировать ключи на базе паролей
			PasswordDeriveBytes pdb = new PasswordDeriveBytes(password, null);
			pdb.HashName = "SHA512"; //будем использовать SHA512
			int keylen = 64;
			alg.KeySize = keylen; //устанавливаем размер ключа
			alg.Key = pdb.GetBytes(keylen >> 3); //получаем ключ из пароля
			alg.Mode = CipherMode.CBC; //используем режим CBC
			alg.IV = new Byte[alg.BlockSize >> 3]; //и пустой инициализационный вектор
			ICryptoTransform tr = alg.CreateEncryptor(); //создаем encryptor

			byte[] bytes = new byte[128]; //System.Text.Encoding.Default.GetBytes(encryptText);

			// зашифрованная дата
			// год 
			bytes[0] = byteKey1[0]; //(byte)(date.Year - 2000);
			// месяц
			bytes[10] = byteKey1[1]; //(byte)(date.Month);
			// день
			bytes[20] = byteKey1[2]; //(byte)(date.Day);
			// час
			bytes[30] = byteKey1[3]; //(byte)(date.Hour);
			// минуты
			bytes[40] = byteKey1[4]; //(byte)(date.Minute);
			// секунды
			bytes[50] = byteKey1[5]; //(byte)(date.Second);
			// зашифрованная версия
			// major
			bytes[60] = byteKey2[0]; //version[0];
			// minor
			bytes[70] = byteKey2[1]; //version[1];
			// build
			bytes[80] = byteKey2[2]; //version[2];
			// revision
			bytes[90] = byteKey2[3]; //version[3];
			
			int buflen = 128; // ((2 << 16) / alg.BlockSize) * alg.BlockSize;
			byte[] inbuf = new byte[buflen];
			byte[] outbuf = new byte[buflen];

			bytes.CopyTo(inbuf, 0);

			int enclen = tr.TransformBlock(inbuf, 0, buflen, outbuf, 0);

			alg.Clear();

			return outbuf; 
		}
#endif

		/// <summary>
		/// 
		/// </summary>
		/// <param name="bytes">зашифрованный массив байт</param>
		/// <param name="password"></param>
		/// <param name="byteKey1">первый расшифрованный ключ</param>
		/// <param name="byteKey2">второй расшифрованный ключ</param>
		public static void Decrypt(byte[] bytes, byte[] password, out byte[] byteKey1, out byte[] byteKey2)
		{
			// первый расшифрованный ключ
			byteKey1 = new byte[6];
			// второй расшифрованный ключ
			byteKey2 = new byte[4];

			SymmetricAlgorithm alg = (SymmetricAlgorithm)DESCryptoServiceProvider.Create();

			PasswordDeriveBytes pdb = new PasswordDeriveBytes(password, null);
			pdb.HashName = "SHA512";
			int keylen = 64;
			alg.KeySize = keylen;
			keylen >>= 3;
			alg.Key = pdb.GetBytes(keylen);
			alg.Mode = CipherMode.CBC;
			alg.IV = new Byte[alg.BlockSize >> 3];
			ICryptoTransform tr = alg.CreateDecryptor();

			int buflen = 128; //((2 << 16) / alg.BlockSize) * alg.BlockSize;
			byte[] inbuf = new byte[buflen];
			byte[] outbuf = new byte[buflen];

			bytes.CopyTo(inbuf, 0);
			tr.TransformBlock(inbuf, 0, buflen, outbuf, 0);
			alg.Clear();

			byteKey1[0] = outbuf[ 0]; // 1
			byteKey1[1] = outbuf[10]; // 2
			byteKey1[2] = outbuf[20]; // 3
			byteKey1[3] = outbuf[30]; // 4
			byteKey1[4] = outbuf[40]; // 5
			byteKey1[5] = outbuf[50]; // 6
			byteKey2[0] = outbuf[60]; // 7 
			byteKey2[1] = outbuf[70]; // 8
			byteKey2[2] = outbuf[80]; // 9
			byteKey2[3] = outbuf[90]; // 10
		}

		/// <summary>
		/// Формирование первичного ключа для создания пароля
		/// </summary>
		/// <param name="key">байты, полученные из картинки-ключа</param>
		/// <param name="version">байты, полученные из версии программы</param>
		/// <returns>первичный набор байт (ключ) из которого формируется пароль</returns>
		public static byte[] GetInitialKey(byte[] key, byte[] version)
		{
			int len = key.Length + version.Length;
			byte[] initialKey = new byte[len];
			key.CopyTo(initialKey, 0);
			version.CopyTo(initialKey, key.Length);

			return initialKey;
		}

		/// <summary>
		/// Преобразует текст вида "x.y.z" и "x-y-z" в массив байт
		/// </summary>
		/// <param name="text">исходная строка</param>
		/// <param name="delimiter">разделитель</param>
		/// <returns>искомый массив байт</returns>
		public static byte[] GetBytes(string text, char delimiter)
		{
			string[] s = text.Split(delimiter);
			byte[] b = new byte[s.Length];

			for (int i = 0; i < s.Length; i++)
			{
				b[i] = Convert.ToByte(String.Format("0x{0}", s[i]), 16);
			}
			return b;
		}

		/// <summary>
		/// Преобразует картинку в массив байт
		/// </summary>
		/// <param name="bitmap">исходная картинка</param>
		/// <returns>искомый массив байт</returns>
		public static byte[] GetBytes(Bitmap bitmap)
		{
			byte[] b;

			using (MemoryStream stream = new MemoryStream())
			{
				bitmap.Save(stream, ImageFormat.Bmp);
				b = stream.ToArray();
			}

			return b;
		}

		/// <summary>
		/// Преобразует массив байт в сроку вида "AA-BB-CC..."
		/// </summary>
		/// <param name="bytes">массив байт</param>
		/// <returns>искомая строка</returns>
		public static string GetString(byte[] bytes)
		{
			return BitConverter.ToString(bytes);
		}

		#region Набор методов для формирования пароля из первичного набора байт

		public static byte[] ShiftLeft(byte[] oldBytes)
		{
			byte[] newBytes = new byte[oldBytes.Length];
			oldBytes.CopyTo(newBytes, 0);
			for (int i = 0; i < newBytes.Length; i++)
				newBytes[i] <<= newBytes[i];
			return newBytes;
		}

		public static byte[] ShiftRight(byte[] oldBytes)
		{
			byte[] newBytes = new byte[oldBytes.Length];
			oldBytes.CopyTo(newBytes, 0);
			for (int i = 0; i < newBytes.Length; i++)
				newBytes[i] >>= newBytes[i];
			return newBytes;
		}

		public static byte[] Shift(byte[] oldBytes)
		{
			byte[] newBytes = new byte[oldBytes.Length];
			oldBytes.CopyTo(newBytes, 0);
		   
			for (int i = 0; i < newBytes.Length-1; i++)
			{
				byte x = newBytes[i];
				byte y = newBytes[i + 1];
				if (x != y)
				{
					x ^= y; y ^= x; x ^= y;
				}
				newBytes[i] = x;
				newBytes[i+1] = y;
			}
				
			return newBytes;
		}

		#endregion Набор методов для формирования пароля из первичного набора байт
	}
}