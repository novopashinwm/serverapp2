﻿using System;
using System.Runtime.Serialization;

namespace FORIS.TSS.Protection
{
	/// <summary> Исключение нарушения защиты HASP </summary>
	[Serializable]
	public class HaspProtectionException : Exception
	{
		public HaspProtectionException(string message)
			: base(message)
		{
		}
		protected HaspProtectionException(SerializationInfo serializationInfo, StreamingContext streamingContext)
			: base(serializationInfo, streamingContext)
		{
		}
	}
}