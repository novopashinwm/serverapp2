﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Compass.Ufin.Movireg.Provider.Models
{
	[Serializable]
	[DataContract]
	public class VehicleStatusResponse
	{
		[DataMember] public int                     result     { get; set; }
		[DataMember] public VehicleStatusPagination pagination { get; set; }
		[DataMember] public List<VehicleStatusInfo> infos      { get; set; }
	}
}