﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Compass.Ufin.Movireg.Provider.Models
{
	[Serializable]
	[DataContract]
	public class QueryUserVehicleVehicle
	{
		[DataMember] public int                      id   { get; set; }
		[DataMember] public List<QueryUserVehicleDl> dl   { get; set; }
		[DataMember] public string                   nm   { get; set; }
		[DataMember] public string                   pt   { get; set; }
		[DataMember] public int                      ic   { get; set; }
		[DataMember] public int                      pid  { get; set; }
	}
}