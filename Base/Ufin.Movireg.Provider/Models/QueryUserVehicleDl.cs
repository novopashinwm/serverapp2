﻿using System;
using System.Runtime.Serialization;

namespace Compass.Ufin.Movireg.Provider.Models
{
	[Serializable]
	[DataContract]
	public class QueryUserVehicleDl
	{
		[DataMember] public string id   { get; set; }
		[DataMember] public string cn   { get; set; }
		[DataMember] public string gps  { get; set; }
		[DataMember] public int?   tc   { get; set; }
		[DataMember] public string sim  { get; set; }
		[DataMember] public string srl  { get; set; }
		[DataMember] public int?   cc   { get; set; }
		[DataMember] public string io   { get; set; }
		[DataMember] public string tn   { get; set; }
		[DataMember] public int?   isb  { get; set; }
		[DataMember] public string vt   { get; set; }
		[DataMember] public string sdc  { get; set; }
		[DataMember] public string dt   { get; set; }
		[DataMember] public string ptt  { get; set; }
		[DataMember] public string dId  { get; set; }
		[DataMember] public int?   ic   { get; set; }
		[DataMember] public int?   us   { get; set; }
		[DataMember] public int?   md   { get; set; }
		[DataMember] public int?   pid  { get; set; }
		[DataMember] public int?   nflt { get; set; }
		[DataMember] public string st   { get; set; }
	}
}