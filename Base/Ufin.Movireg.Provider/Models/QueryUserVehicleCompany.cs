﻿using System;
using System.Runtime.Serialization;

namespace Compass.Ufin.Movireg.Provider.Models
{
	[Serializable]
	[DataContract]
	public class QueryUserVehicleCompany
	{
		[DataMember] public int    id  { get; set; }
		[DataMember] public string nm  { get; set; }
		[DataMember] public int    pId { get; set; }
	}
}