﻿using System;
using System.Runtime.Serialization;

namespace Compass.Ufin.Movireg.Provider.Models
{
	[Serializable]
	[DataContract]
	public class VehicleStatusPagination
	{
		[DataMember] public int    endRecord       { get; set; }
		[DataMember] public string primaryKey      { get; set; }
		[DataMember] public bool   hasNextPage     { get; set; }
		[DataMember] public bool   directQuery     { get; set; }
		[DataMember] public int    nextPage        { get; set; }
		[DataMember] public int    previousPage    { get; set; }
		[DataMember] public bool   hasPreviousPage { get; set; }
		[DataMember] public int    totalPages      { get; set; }
		[DataMember] public int    currentPage     { get; set; }
		[DataMember] public int    pageRecords     { get; set; }
		[DataMember] public int    totalRecords    { get; set; }
		[DataMember] public int    startRecord     { get; set; }
		[DataMember] public string sortParams      { get; set; }
	}
}