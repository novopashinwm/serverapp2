﻿using System;
using System.Runtime.Serialization;

namespace Compass.Ufin.Movireg.Provider.Models
{
	[Serializable]
	[DataContract]
	public class VehicleStatusInfo
	{
		[DataMember] public long   tm   { get; set; }
		[DataMember] public int    jd   { get; set; }
		[DataMember] public int    wd   { get; set; }
		[DataMember] public string vi   { get; set; }
		[DataMember] public string pos  { get; set; }
	}
}