﻿using System;
using System.Runtime.Serialization;

namespace Compass.Ufin.Movireg.Provider.Models
{
	[Serializable]
	[DataContract]
	public class LoginResponse
	{
		[DataMember] public int    result   { get; set; }
		[DataMember] public string message  { get; set; }
		[DataMember] public string jsession { get; set; }
	}
}