﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Compass.Ufin.Movireg.Provider.Models
{
	[Serializable]
	[DataContract]
	public class QueryUserVehicleResponse
	{
		[DataMember] public int                           result   { get; set; }
		[DataMember] public List<QueryUserVehicleCompany> companys { get; set; }
		[DataMember] public List<QueryUserVehicleVehicle> vehicles { get; set; }
	}
}