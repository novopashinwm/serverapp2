﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Compass.Ufin.Movireg.Provider.Models;
using FORIS.TSS.Common.Helpers;

namespace Compass.Ufin.Movireg.Provider
{
	public class MoviregApiProvider
	{
		#region Fields & Properties

		private readonly string _accountName;
		private readonly string _password;
		private readonly string _serverUrl;

		public string Session       { get; private set; }
		public bool   IsLoggedIn => !string.IsNullOrEmpty(Session);

		#endregion

		#region .ctor(s)
		/// <summary> Конструктор </summary>
		/// <param name="accountName"> Имя пользователя УЗ (учетная запись сервера Movireg) </param>
		/// <param name="password"> Пароль УЗ (учетная запись сервера Movireg) </param>
		/// <param name="serverUrl"> Адрес сервера Movireg </param>
		public MoviregApiProvider(string accountName, string password, string serverUrl)
		{
			_accountName = accountName;
			_password    = password;
			_serverUrl   = serverUrl;
		}

		#endregion

		#region StandardApiAction_login

		/// <summary> Войти (Получить сессию) </summary>
		/// <returns> Успех? Да/Нет </returns>
		public bool StandardApiAction_login()
		{
			const string methodAction = "/StandardApiAction_login.action";

			if (IsLoggedIn)
				return true;

			var methodResponse = GetPostResponse(
					$"{_serverUrl}{methodAction}",
					$"account={_accountName}&password={_password}")
				.FromJson<LoginResponse>();

			if (methodResponse.result == 0)
			{
				Session = methodResponse.jsession;
				return true;
			}
			else
				return false;
		}

		#endregion
		#region StandardApiAction_logout

		/// <summary> Выход (завершение сессии) </summary>
		/// <param name="resetSession"> Очистить внутреннюю переменную сессии независимо от результата выполнения метода </param>
		/// <returns> Успех? Да/Нет </returns>
		public bool StandardApiAction_logout(bool resetSession = false)
		{
			const string methodAction = "/StandardApiAction_logout.action";

			if (!IsLoggedIn)
				return true;

			var methodReponse = GetPostResponse($"{_serverUrl}{methodAction}", $"jsession={Session}")
				.FromJson<LogoutResponse>();
			if (methodReponse.result == 0 || resetSession)
				Session = string.Empty;
			return methodReponse.result == 0;
		}

		#endregion
		#region StandardApiAction_queryUserVehicle

		/// <summary> Получить список объектов наблюдения, указанной в конструкторе учетной записи (УЗ) </summary>
		/// <returns> Ответ метода Api с набором объектов наблюдения </returns>
		public QueryUserVehicleResponse StandardApiAction_queryUserVehicle()
		{
			const string methodAction = "/StandardApiAction_queryUserVehicle.action";

			if (!IsLoggedIn)
				throw new InvalidOperationException("Not logged in.");

			return GetPostResponse($"{_serverUrl}{methodAction}", $"jsession={Session}&language=en")
				.FromJson<QueryUserVehicleResponse>();
		}

		#endregion
		#region StandardApiAction_vehicleStatus

		/// <summary> Метод получения данных о местоположении и состоянии объектов наблюдения </summary>
		/// <param name="vehIds"> Массив строковых идентификаторов объектов наблюдения </param>
		/// <returns> Информация по всем запрошенным объектам наблюдения </returns>
		public VehicleStatusResponse StandardApiAction_vehicleStatus(string[] vehIds)
		{
			const string methodAction = "/StandardApiAction_vehicleStatus.action";

			if (!IsLoggedIn)
				throw new InvalidOperationException("Not logged in");

			if ((vehIds?.Length ?? 0) > 0)
				return GetPostResponse(
						$"{_serverUrl}{methodAction}",
						$"jsession={Session}&vehiIdno={string.Join(",", vehIds)}&toMap=1&currentPage=1&pageRecords={vehIds.Length}")
					.FromJson<VehicleStatusResponse>();
			else
				throw new ArgumentException("The length of the identifier array must be greater than zero. ", nameof(vehIds));
		}

		#endregion
		#region General

		/// <summary> Метод отправки POST запроса к серверу Movireg </summary>
		/// <param name="uri"> Адрес сервера Movireg </param>
		/// <param name="values"> Контент для отправки на сервер Movireg </param>
		/// <returns> Строка результата </returns>
		private string GetPostResponse(string uri, string values)
		{
			var requestUri        = uri;
			var requestBodyString = values;
			var contentType       = "application/x-www-form-urlencoded";
			var requestMethod     = "POST";

			var request = (HttpWebRequest)WebRequest.Create(requestUri);
			request.Method      = requestMethod;
			request.ContentType = contentType;

			var bytes = Encoding.UTF8.GetBytes(requestBodyString);
			var stream = request.GetRequestStream();
			stream.Write(bytes, 0, bytes.Length);
			stream.Close();

			using var response = request.GetResponse();
			using var sr = new StreamReader(response.GetResponseStream());
			return sr.ReadToEnd();
		}

		#endregion
	}
}