﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Resources;

namespace FORIS.TSS.Resources
{
	/// <summary> Класс контейнера строковых ресурсов </summary>
	public class ResourceStringContainer : IEnumerable
	{
		private readonly ResourceManager _resourceManager;
		public  readonly CultureInfo     Culture;
		public  readonly string          BaseName;
		public  readonly Assembly        Assembly;
		/// <summary> Создаёт новый экземпляр контейнера ресурсов </summary>
		/// <param name="culture">Культура</param>
		/// <param name="baseName">Название ресурса с точностью до пространства имен</param>
		/// <param name="assembly">Сборка, в которой хранится данный ресурс</param>
		public ResourceStringContainer(CultureInfo culture, string baseName, Assembly assembly)
		{
			if (assembly == null)
				throw new ArgumentNullException(nameof(assembly));

			_resourceManager = new ResourceManager(baseName, assembly);
			Culture          = culture;
			BaseName         = baseName;
			Assembly         = assembly;
		}
		/// <summary> Возвращает строковый ресурс по ключу. Если ресурс не найден, возвращает сам ключ. </summary>
		/// <param name="key"> Ключ ресурса </param>
		public string this[string key]
		{
			get
			{
				return GetStringOrDefault(key, key);
			}
		}
		/// <summary> Возвращает строковый ресурс по ключу. Если ресурс не найден, возвращает значение по умолчанию. </summary>
		/// <param name="key"> Ключ ресурса </param>
		/// <returns> Строковый ресурс или значение по умолчанию </returns>
		public string GetStringOrDefault(string stringKey, string defaultValue = null)
		{
			var result = defaultValue;
			try
			{
				result = _resourceManager.GetString(stringKey, Culture);
			}
			catch (MissingManifestResourceException e)
			{
				Trace.TraceError("Missing manifest when getting resource {0}|{1}|{2}|{3} at {4}", Culture, Assembly.FullName, BaseName, stringKey, e.StackTrace);
			}
			catch (MissingSatelliteAssemblyException e)
			{
				Trace.TraceError("Missing assembly when getting resource {0}|{1}|{2}|{3} at {4}", Culture, Assembly.FullName, BaseName, stringKey, e.Message + "\n" + e.StackTrace);
			}
			return result ?? defaultValue;
		}
		/// <summary> Получить перечислитель </summary>
		public IEnumerator GetEnumerator()
		{
			return _resourceManager
				.GetResourceSet(Culture, true, true)
				.GetEnumerator();
		}
	}
}