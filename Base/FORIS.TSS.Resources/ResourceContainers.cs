﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.BusinessLogic.Server;

namespace FORIS.TSS.Resources
{
	public class ResourceContainers
	{
		/// <summary> Словарь контейнеров ресурсов для языков </summary>
		private static readonly ConcurrentDictionary<CultureInfo, ResourceContainers> resourceContainersCache =
			new ConcurrentDictionary<CultureInfo, ResourceContainers>();

		/// <summary> Получить контейнер ресурсов по имени культуры </summary>
		/// <param name="cultureCode"> Имя культуры </param>
		/// <returns></returns>
		public static ResourceContainers Get(string cultureCode)
		{
			if (null == cultureCode)
				throw new ArgumentNullException("cultureCode");
			return Get(CultureInfo.GetCultureInfo(cultureCode));
		}
		/// <summary> Получить контейнер ресурсов </summary>
		/// <param name="cultureInfo"> Культура контейнера ресурсов </param>
		/// <returns></returns>
		public static ResourceContainers Get(CultureInfo cultureInfo)
		{
			if (null == cultureInfo)
				throw new ArgumentNullException(nameof(cultureInfo));
			return resourceContainersCache.GetOrAdd(cultureInfo, new ResourceContainers(cultureInfo));
		}

		public CultureInfo CultureInfo { get; }

		private readonly ConcurrentDictionary<Type, ResourceStringContainer> _typedContainers =
			new ConcurrentDictionary<Type, ResourceStringContainer>();

		private ResourceStringContainer _resStringContainer_CmdResult;
		private ResourceStringContainer _resStringContainer_SensorLegend;
		private ResourceStringContainer _resStringContainer_RenderedServiceType;
		private ResourceStringContainer _resStringContainer_CapabilityDisallowingReason;
		private ResourceStringContainer _resStringContainer_UnitOfMeasure;
		private ResourceStringContainer _resStringContainer_VehicleKind;
		private ResourceStringContainer _resStringContainer_SetNewContactResult;
		private ResourceStringContainer _resStringContainer_MessageState;
		private ResourceStringContainer _resStringContainer_MessageProcessingResult;
		private ResourceStringContainer _resStringContainer_IdType;
		private ResourceStringContainer _resStringContainer_SystemRight;
		private ResourceStringContainer _resStringContainer_VehicleAttributeName;
		private ResourceStringContainer _resStringContainer_VehicleCategory;
		private ResourceStringContainer _resStringContainer_Strings;
		private ResourceStringContainer _resStringContainer_MessageResources;
		private ResourceStringContainer _resStringContainer_ExceptionMessages;

		private ResourceContainers(CultureInfo cultureInfo)
		{
			CultureInfo = cultureInfo;
			_resStringContainer_CmdResult                   = GetContainer<CmdResult>                  (CultureInfo);
			_resStringContainer_SensorLegend                = GetContainer<SensorLegend>               (CultureInfo, "Sensors");
			_resStringContainer_RenderedServiceType         = GetContainer<RenderedServiceType>        (CultureInfo);
			_resStringContainer_CapabilityDisallowingReason = GetContainer<CapabilityDisallowingReason>(CultureInfo);
			_resStringContainer_UnitOfMeasure               = GetContainer<UnitOfMeasure>              (CultureInfo);
			_resStringContainer_VehicleKind                 = GetContainer<VehicleKind>                (CultureInfo);
			_resStringContainer_SetNewContactResult         = GetContainer<SetNewContactResult>        (CultureInfo);
			_resStringContainer_MessageState                = GetContainer<MessageState>               (CultureInfo);
			_resStringContainer_MessageProcessingResult     = GetContainer<MessageProcessingResult>    (CultureInfo);
			_resStringContainer_IdType                      = GetContainer<IdType>                     (CultureInfo);
			_resStringContainer_SystemRight                 = GetContainer<SystemRight>                (CultureInfo);
			_resStringContainer_VehicleAttributeName        = GetContainer<VehicleAttributeName>       (CultureInfo);
			_resStringContainer_VehicleCategory             = GetContainer<VehicleCategory>            (CultureInfo);
			_resStringContainer_Strings                     = GetContainer                             (CultureInfo, "Strings");
			_resStringContainer_MessageResources            = GetContainer                             (CultureInfo, "MessageResources");
			_resStringContainer_ExceptionMessages           = GetContainer                             (CultureInfo, "ExceptionMessages");
		}
		public string this[CapabilityDisallowingReason? disallowingReason, DeviceCapability? capability = null]
		{
			get
			{
				var reasonString = disallowingReason.ToString();
				var key = reasonString;
				if (disallowingReason != null)
					key += "." + capability.ToString();
				var result = _resStringContainer_CapabilityDisallowingReason[key];
				if (result == key)
					result = _resStringContainer_CapabilityDisallowingReason[reasonString];

				if (string.IsNullOrWhiteSpace(result))
					result = key;

				return result;
			}
		}
		public string this[CmdResult value, CmdType? cmdType = null]
		{
			get
			{
				string result;

				var key = value.ToString();
				if (cmdType != null)
				{
					var cmdTypeSpecificKey = cmdType.Value + "." + key;
					result = _resStringContainer_CmdResult[cmdTypeSpecificKey];

					if (result != cmdTypeSpecificKey)
						return result;
				}

				result = _resStringContainer_CmdResult[key];

				if (string.IsNullOrWhiteSpace(result))
					result = key;

				return result;
			}
		}
		public string this[CmdStatus value]
		{
			get
			{
				var key = value.ToString();

				var result = _resStringContainer_CmdResult[key];

				if (string.IsNullOrWhiteSpace(result))
					result = key;

				return result;
			}
		}
		public string this[RenderedServiceType value]
		{
			get
			{
				var key = value.ToString();

				var result = _resStringContainer_RenderedServiceType[key];

				if (string.IsNullOrWhiteSpace(result))
					result = key;

				return result;
			}
		}
		public string this[CmdResult? value]
		{
			get
			{
				if (value == null)
					return null;
				return this[value.Value];
			}
		}
		public string this[CmdType value]
		{
			get
			{
				var key = value.ToString();
				var result = _resStringContainer_RenderedServiceType[key];
				if (string.IsNullOrWhiteSpace(result))
					result = key;
				return result;
			}
		}
		public string this[SensorLegend sensorLegend]
		{
			get
			{
				var key = sensorLegend.ToString();
				var result = _resStringContainer_SensorLegend[key];
				if (string.IsNullOrWhiteSpace(key))
					result = key;
				return result;
			}
		}
		public string this[SetNewContactResult contactResult]
		{
			get
			{
				var key = contactResult.ToString();
				var result = _resStringContainer_SetNewContactResult[key];
				if (string.IsNullOrWhiteSpace(key))
					result = key;
				return result;
			}
		}
		public string this[UnitOfMeasure unitOfMeasure]
		{
			get
			{
				var key = unitOfMeasure.ToString();
				var result = _resStringContainer_UnitOfMeasure[key];
				return result ?? key;
			}
		}
		public string this[VehicleKind vehicleKind]
		{
			get
			{
				var key = vehicleKind.ToString();
				var result = _resStringContainer_VehicleKind[key];
				if (string.IsNullOrWhiteSpace(key))
					result = key;
				return result;
			}
		}
		public string this[MessageState state]
		{
			get
			{
				var key = state.ToString();
				var result = _resStringContainer_MessageState[key];
				if (string.IsNullOrEmpty(key)) 
					result = key;
				return result;
			}
		}
		public string this[SystemRight right]
		{
			get
			{
				var key = right.ToString();
				var result = _resStringContainer_SystemRight[key];
				if (string.IsNullOrEmpty(key))
					result = key;

				return result;
			}
		}
		public string this[MessageProcessingResult processingResult]
		{
			get
			{
				var key = processingResult.ToString();
				var result = _resStringContainer_MessageProcessingResult[key];
				if (string.IsNullOrEmpty(key))
					result = key;
				return result;
			}
		}
		public string this[IdType idType]
		{
			get
			{
				var key = idType.ToString();
				var result = _resStringContainer_IdType[key];
				if (string.IsNullOrEmpty(key))
					result = key;
				return result;
			}
		}
		public string this[VehicleAttributeName attribute]
		{
			get
			{
				var key = attribute.ToString();
				var result = _resStringContainer_VehicleAttributeName[key];
				if (string.IsNullOrEmpty(key))
					result = key;
				return result;
			}
		}
		public string this[VehicleCategory category]
		{
			get
			{
				var key = category.ToString();
				var result = _resStringContainer_VehicleCategory[key];
				if (string.IsNullOrEmpty(key))
					result = key;

				return result;
			}
		}
		public string GetLocalizedText<T>(string key)
		{
			return GetContainer<T>()[key];
		}
		public string GetLocalizedMessagePart(string key)
		{
			return _resStringContainer_MessageResources[key];
		}
		public string GetLocalizedString(string key)
		{
			return _resStringContainer_Strings[key];
		}
		public string GetLocalizedExceptionMessage(string key)
		{
			return _resStringContainer_ExceptionMessages[key];
		}
		public Dictionary<string, Bitmap> GetVehicleIcons()
		{
			return GetContainer<BusinessLogic.DTO.Icon>(CultureInfo)
				.OfType<DictionaryEntry>()
				.ToDictionary(d => (string)d.Key, d => (Bitmap)d.Value);
		}
		public string this[string key]
		{
			get
			{
				return GetLocalizedString(key);
			}
		}
		public ResourceStringContainer GetContainer<T>(string containerName = null)
		{
			return GetContainer<T>(CultureInfo, containerName);
		}
		private ResourceStringContainer GetContainer<T>(CultureInfo cultureInfo, string containerName = null)
		{
			return _typedContainers
				.GetOrAdd(typeof(T), (type) => GetContainer(cultureInfo, null == containerName ? type.Name : containerName));
		}
		private ResourceStringContainer GetContainer(CultureInfo cultureInfo, string containerName)
		{

			return new ResourceStringContainer(cultureInfo, $@"{GetType().Namespace}.{containerName}", GetType().Assembly);
		}

	}
}