﻿using System;
using System.Drawing;
using System.Reflection;
using System.ServiceProcess;
using System.Windows.Forms;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using TrayApp = System.Windows.Forms.Application;


namespace FORIS.TSS.ServerLauncher.Application
{
	public class TrayIconApplicationContext : ApplicationContext
	{
		public static void RunInteractive(string serverName, ServiceBase[] servicesToRun)
		{
			using (var appContext = new TrayIconApplicationContext(serverName))
			{
				var onStartMethod = typeof(ServiceBase).GetMethod("OnStart", BindingFlags.Instance | BindingFlags.NonPublic);
				var onStopMethod  = typeof(ServiceBase).GetMethod("OnStop",  BindingFlags.Instance | BindingFlags.NonPublic);
				foreach (ServiceBase service in servicesToRun)
				{
					using var logger = new Stopwatcher($"Start '{service.ServiceName}'", null, TimeSpan.Zero);
					try
					{
						onStartMethod?.Invoke(service, new object[] { new string[] { } });
					}
					catch (Exception ex)
					{
						$"Start '{service.ServiceName}'"
							.WithException(ex, true)
							.CallTraceError();
					}
				}
				////////////////////////
				// Тут очередь сообщений приложения с меню, что бы ловить сообщения меню и иконки
				TrayApp.Run(appContext);
				////////////////////////
				foreach (ServiceBase service in servicesToRun)
				{
					using var logger = new Stopwatcher($"Stop '{service.ServiceName}'", null, TimeSpan.Zero);
					try
					{
						onStopMethod?.Invoke(service, null);
					}
					catch (Exception ex)
					{
						$"Stop '{service.ServiceName}'"
							.WithException(ex, true)
							.CallTraceError();
					}
				}
			}
		}
		public NotifyIcon       TrayIcon    { get; }
		public TrayIconApplicationContext(string serverName)
		{
			var entryAssembly = Assembly.GetEntryAssembly();
			TrayIcon = new NotifyIcon
			{
				ContextMenuStrip = new ContextMenuStrip(),
				Text             = serverName,
				Visible          = true,
				Icon             = Icon.ExtractAssociatedIcon(entryAssembly.Location),
			};
			// Создаем меню
			TrayIcon.ContextMenuStrip.Items.Add("Остановить", null, (s, e) => ExitThread())
				.Font = new Font(TrayIcon.ContextMenuStrip.Font, FontStyle.Bold);
			TrayIcon.ContextMenuStrip.Items.Add("-");
			TrayIcon.ContextMenuStrip.Items.Add($"{serverName}({entryAssembly.VersionFull()}), {entryAssembly.Copyright()}")
				.Enabled = false;

			TrayIcon.MouseDoubleClick += TrayIconDoubleClickHandler;
			TrayIcon.MouseClick       += TrayIconSingleClickHandler;
			TrayApp.ApplicationExit   += ApplicationExitHandler;
		}
		protected virtual void OnApplicationExit(EventArgs e)
		{
			if (null != TrayIcon)
			{
				TrayIcon.Visible = false;
				if (null != TrayIcon.ContextMenuStrip)
					TrayIcon.ContextMenuStrip.Dispose();
				TrayIcon.Dispose();
			}
		}
		protected virtual void OnTrayIconSingleClick(MouseEventArgs e)
		{
		}
		protected virtual void OnTrayIconDoubleClick(MouseEventArgs e)
		{
		}
		private void ApplicationExitHandler(object sender, EventArgs e)
		{
			OnApplicationExit(e);
		}
		private void TrayIconSingleClickHandler(object sender, MouseEventArgs e)
		{
			OnTrayIconSingleClick(e);
		}
		private void TrayIconDoubleClickHandler(object sender, MouseEventArgs e)
		{
			OnTrayIconDoubleClick(e);
		}
	}
}