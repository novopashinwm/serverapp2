﻿using System;
using System.Runtime.Serialization;

namespace FORIS.DataAccess.Interfaces
{
	[Serializable]
	public class DataAccessException :
		Exception
	{
		public DataAccessException(string message, Exception innerException)
			: base(message, innerException)
		{
		}
		public DataAccessException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}