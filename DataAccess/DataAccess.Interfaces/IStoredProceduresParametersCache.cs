﻿using System.Collections.Generic;

namespace FORIS.DataAccess.Interfaces
{
	public interface IStoredProceduresParametersCache
	{
		IList<string> Names { get; }
		void Reset();
	}
}