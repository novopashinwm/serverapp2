using System;

namespace FORIS.TSS.BusinessLogic.Data
{
	/// <summary> ��������� ��������-�������� ��� ������ � �������� ������ �������� �������� </summary>
	[Serializable]
	public struct ParamValue
	{
		public string strParam;
		public object value;

		public ParamValue(string strParam, object value)
		{
			this.strParam = strParam;
			this.value    = value;
		}
	}
}