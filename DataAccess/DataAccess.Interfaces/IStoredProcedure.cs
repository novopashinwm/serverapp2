﻿using System;
using System.Data;

namespace FORIS.DataAccess.Interfaces
{
	public interface IStoredProcedure : IDisposable
	{
		void ExecuteNonQuery();
		object ExecuteScalar();
		DataSet ExecuteDataSet();
		IDbDataParameter this[string name] { get; }
	}
}