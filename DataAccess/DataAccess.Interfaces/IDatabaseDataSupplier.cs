﻿using System;
using System.Data;
using FORIS.DataAccess.Interfaces;
using FORIS.TSS.BusinessLogic.Data;

namespace FORIS.TSS.Helpers
{
	public interface IDatabaseDataSupplier : IDisposable
	{
		IStoredProceduresParametersCache ParametersCache { get; }
		IStoredProcedure Procedure(string procedureName);
		/// <summary> Получить данные из хранимой процедуры, возвращающей несколько наборов записей </summary>
		/// <param name="params"> параметры хранимой процедуры </param>
		/// <param name="procedureName"> имя хранимой процедуры </param>
		/// <param name="strTablesNames"> массив имен таблиц в том порядке в котором они возвращаются хранимой процедурой </param>
		/// <returns></returns>
		DataSet GetDataFromDB(ParamValue[] @params, string procedureName, string[] strTablesNames);
		DataSet GetRecords(string tableName, string whereClause);
		[Obsolete]
		void Update(DataSet dataSet);
		string GetConstant(string name);
		void SetConstant(string name, string value);
	}
}