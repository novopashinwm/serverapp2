using System;
using System.Diagnostics;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FORIS.DataAccess;

namespace FORIS.DataAccess
{
	public class SqlParameterInfo
	{
		//public int Size;
		public SqlDbType SqlType;
		public string ParameterName;
		public ParameterDirection Direction;
	}
}
