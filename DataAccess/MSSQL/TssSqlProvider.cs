﻿using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace FORIS.DataAccess
{
	public class TssSqlProvider : DbProvider
	{
		public string ServerName;
		public string DatabaseName;
		public bool   IsSQLAuthentication;
		public string Login;
		public string Password;
		public string ConnectionString;
		public override ICommandParameterAdapter PrepareParameters(string procedure, string connection)
		{
			return new SqlCommandParameterAdapter(procedure, connection);
		}
		public override ICommandParameterAdapter PrepareParameters(string procedure, IDbConnection connection, IDbTransaction tran)
		{
			return new SqlCommandParameterAdapter(procedure, connection, tran);
		}
		public override IDbConnection Connection(string connection_string)
		{
			return new SqlConnection(connection_string);
		}
		public override IDbDataParameter Parameter()
		{
			return new SqlParameter();
		}
		public override IDbCommand Command()
		{
			return new SqlCommand();
		}
		public override IDbDataAdapter Adapter()
		{
			return new SqlDataAdapter();
		}
		public string ConstructConnectionString()
		{
			StringBuilder sb = new StringBuilder(256);
			sb.AppendFormat("Data Source={0};", ServerName);
			sb.AppendFormat("Initial Catalog={0};", DatabaseName);
			if (IsSQLAuthentication)
			{
				sb.AppendFormat("User ID={0};Password={1}", Login, Password);
			}
			else
			{
				sb.AppendFormat("Integrated Security=SSPI;");
			}
			return sb.ToString();
		}
	}
}