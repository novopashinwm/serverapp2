﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace FORIS.DataAccess
{

	public class SqlCommandParameterAdapter : ICommandParameterAdapter
	{
		const string SIMPLE_PROC_ARGS = "select PARAMETER_NAME, PARAMETER_MODE, DATA_TYPE from INFORMATION_SCHEMA.PARAMETERS where SPECIFIC_NAME = '{0}' ORDER BY ORDINAL_POSITION";
		const string PACKAGE_PROC_ARGS = "select PARAMETER_NAME, PARAMETER_MODE, DATA_TYPE from INFORMATION_SCHEMA.PARAMETERS where SPECIFIC_NAME = '{1}' ORDER BY ORDINAL_POSITION";
		public static string FormatQuery(string procedure)
		{

			string[] name_parts = procedure.Split('.');
			if (name_parts.Length == 1)
			{
				string str = string.Format(SIMPLE_PROC_ARGS, procedure);
				return string.Format(SIMPLE_PROC_ARGS, procedure);
			}
			if (name_parts.Length == 2)
			{
				StringBuilder name0 = new StringBuilder(name_parts[0]);
				TrimBrackets(name0);
				StringBuilder name1 = new StringBuilder(name_parts[1]);
				TrimBrackets(name1);
				string query = string.Format(PACKAGE_PROC_ARGS, name0.ToString(), name1.ToString());
				return query;
			}
			throw new ArgumentException(string.Format("Invalid procedure name format - {0}", procedure));
		}
		static void TrimBrackets(StringBuilder name)
		{
			if (name[0] == '[' && name[name.Length - 1] == ']')
			{
				name.Remove(name.Length - 1, 1);
				name.Remove(0, 1);
			}
		}
		static SqlDbType FormatType(string type)
		{
			switch (type.ToLower())
			{
				case "float":
					return SqlDbType.Float;
				case "double":
					return SqlDbType.Float;
				case "numeric":
					return SqlDbType.Decimal;
				case "decimal":
					return SqlDbType.Decimal;
				case "char":
					return SqlDbType.Char;
				case "datetime":
					return SqlDbType.DateTime;
				case "smalldatetime":
					return SqlDbType.DateTime;
				case "image":
					return SqlDbType.Binary;
				case "int":
					return SqlDbType.Int;
				case "bigint":
					return SqlDbType.BigInt;
				case "nvarchar":
					return SqlDbType.NVarChar;
				case "real":
					return SqlDbType.Real;
				case "smallint":
					return SqlDbType.SmallInt;
				case "text":
					return SqlDbType.Text;
				case "ntext":
					return SqlDbType.NText;
				case "tinyint":
					return SqlDbType.TinyInt;
				case "varchar":
					return SqlDbType.VarChar;
				case "nchar":
					return SqlDbType.NChar;
				case "bit":
					return SqlDbType.Bit;
				case "uniqueidentifier":
					return SqlDbType.UniqueIdentifier;
				default:
					return SqlDbType.Binary;
			}
		}
		public static ParameterDirection FormatDirection(string name, string direction, string type_name)
		{
			if ((name == null) | (name == string.Empty))
			{
				return ParameterDirection.ReturnValue;
			}
			if (direction == "OUT")
				return ParameterDirection.Output;
			if (direction == "INOUT")
				return ParameterDirection.InputOutput;
			if (direction == "IN")
				return ParameterDirection.Input;
			throw new ApplicationException("Unexpected parameter mode");
		}
		public SqlParameterInfo[] _params_info;
		public SqlCommandParameterAdapter(string name, string connection)
			: this(name, CreateConnection(connection), null)
		{
		}
		private static IDbConnection CreateConnection(string connection)
		{
			return new SqlConnection(connection);
		}
		public SqlCommandParameterAdapter(string name, IDbConnection cnn, IDbTransaction tran)
		{
			using (SqlCommand cmd = new SqlCommand(FormatQuery(name), (SqlConnection)cnn, (SqlTransaction)tran))
			{
				CommandBehavior behavior = CommandBehavior.Default;
				if (cnn.State == ConnectionState.Closed)
				{
					cnn.Open();
					behavior = CommandBehavior.CloseConnection;
				}
				using (SqlDataReader reader = cmd.ExecuteReader(behavior))
				{
					ArrayList list = new ArrayList();
					while (reader.Read())
					{
						SqlParameterInfo param_info = new SqlParameterInfo();
						string param_name = null;
						if (!reader.IsDBNull(reader.GetOrdinal("PARAMETER_NAME")))
							param_name = reader.GetString(reader.GetOrdinal("PARAMETER_NAME"));
						string type = reader.GetString(reader.GetOrdinal("DATA_TYPE"));
						string direction = reader.GetString(reader.GetOrdinal("PARAMETER_MODE"));
						//string size = reader.GetString( reader.GetOrdinal( "LENGHT" ) );
						param_info.ParameterName = param_name;
						param_info.SqlType = FormatType(type);
						param_info.Direction = FormatDirection(param_name, direction, type);
						list.Add(param_info);
					}
					_params_info = (SqlParameterInfo[])list.ToArray(typeof(SqlParameterInfo));
				}
			}
		}
		public virtual void Fill(IDbCommand icommand)
		{
			foreach (SqlParameterInfo param_info in _params_info)
			{
				SqlParameter param = new SqlParameter(param_info.ParameterName, param_info.SqlType);
				param.Direction    = param_info.Direction;
				param.SourceColumn = param.ParameterName.Substring(1);

				icommand.Parameters.Add(param);
			}
		}
	}
}