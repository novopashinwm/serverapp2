﻿using System.Configuration;
using System.Xml;

namespace FORIS.DataAccess
{
	public class MsSqlSectionHandler : ISectionHandler
	{
		public object Create(object parent, object configContext, XmlNode node)
		{
			TssSqlProvider provider = new TssSqlProvider();
			PopulateSqlProvider(provider, node);
			TssDatabase database = new TssDatabase(provider.ConnectionString, provider);
			return database;
		}
		protected void PopulateSqlProvider(TssSqlProvider provider, XmlNode node)
		{
			ParseChilds(provider, node);
			if (node.Attributes["connectionString"] != null)
			{
				provider.ConnectionString = node.Attributes["connectionString"].Value;
			}
			else
			{
				provider.ConnectionString = provider.ConstructConnectionString();
			}
		}
		protected void ParseChilds(TssSqlProvider provider, XmlNode node)
		{
			foreach (XmlNode ch in node.ChildNodes)
			{
				if (ch.NodeType == XmlNodeType.Comment) continue;
				string name = ch.Attributes["key"].Value;
				switch (name)
				{
					case "ServerName":
						{
							provider.ServerName = ch.Attributes["value"].Value;
						}
						break;
					case "DBName":
						{
							provider.DatabaseName = ch.Attributes["value"].Value;
						}
						break;
					case "Integrated Security":
						{
							provider.IsSQLAuthentication = (ch.Attributes["value"].Value.ToUpper() == "TRUE");
						}
						break;
					case "User ID":
						{
							provider.Login = ch.Attributes["value"].Value;
						}
						break;
					case "Password":
						{
							provider.Password = ch.Attributes["value"].Value;
						}
						break;
					default:
						{
							throw new ConfigurationErrorsException("wrong key " + name);
						}
				}}
		}
	}
}