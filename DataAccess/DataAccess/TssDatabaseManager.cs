﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.DataAccess
{
	/// <summary>
	/// Summary description for TssDatabaseManager.
	/// </summary>
	public class TssDatabaseManager
	{
		protected Dictionary<string, TssDatabase> databases =
			new Dictionary<string, TssDatabase>();

		protected TssDatabase defaultDatabase = null;

		public void Add(string key, TssDatabase database)
		{
			if (databases.ContainsKey(key))
			{
				Debug.Assert(false);
				databases[key] = database;
			}
			else
			{
				databases.Add(key, database);
				if (defaultDatabase == null)
				{
					defaultDatabase = database;
				}
			}
		}

		public TssDatabase this[string instanceName]
		{
			get
			{
				if (string.IsNullOrEmpty(instanceName))
					return DefaultDataBase;

				if (!databases.ContainsKey(instanceName))
				{
					throw new ArgumentException(
						string.Format(
							"Database instance '{0}' not defined in config file",
							instanceName
							),
						"instanceName"
						);
				}

				return databases[instanceName];
			}
		}

		public List<StorageType> GetStorages()
		{
			return databases.Keys.Select(k => (StorageType)Enum.Parse(typeof(StorageType), k)).ToList();
		}

		public void SetDefaultDatabase(string key)
		{
			defaultDatabase = this[key];
		}
		/// <summary>
		/// Ссылка на объект доступа, который используется по умолчанию. 
		/// Это объект с ID = 0.( если такого нет, то компонентом по 
		/// умолчанию считается первый добавленный )
		/// </summary>
		public TssDatabase DefaultDataBase
		{
			get
			{
				return defaultDatabase;
			}
		}
	}
}