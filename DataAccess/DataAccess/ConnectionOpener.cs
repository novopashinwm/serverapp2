﻿using System;
using System.Data;

namespace FORIS.DataAccess
{
	/// <summary>
	/// Helper for temporary opening connection
	/// </summary>
	public class ConnectionOpener : IDisposable
	{
		protected IDbConnection connection;
		private CommandBehavior commandBehavior;
		public ConnectionOpener(IDbConnection connection)
		{
			if (connection.ConnectionString == string.Empty)
			{
				throw new ApplicationException("There is no connection");
			}
			this.connection = connection;
			if (connection.State == ConnectionState.Closed)
			{
				connection.Open();
				commandBehavior = CommandBehavior.CloseConnection;
			}
			else
			{
				commandBehavior = CommandBehavior.Default;
			}
		}
		public ConnectionOpener(StoredProcedure sp) : this(sp.Command.Connection)
		{
		}
		public void Dispose()
		{
			if (commandBehavior == CommandBehavior.CloseConnection)
			{
				connection.Close();
			}
		}
	}
}