﻿using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using FORIS.DataAccess.Interfaces;

namespace FORIS.DataAccess
{
	/// <summary> Класс для работы с базой данных </summary>
	public class TssDatabase :
		IDbConnectionProvider
	{
		#region IDbConnectionProvider Members

		public IDbConnection GetConnection()
		{
			return _provider.Connection(_connection);
		}

		#endregion IDbConnectionProvider Members

		/// <summary>
		/// Строка соединения с базой
		/// </summary>
		protected string _connection;

		/// <summary>
		/// timeout для команд по умолчанию
		/// </summary>
		protected int _default_cmd_timeout;

		/// <summary>
		/// timeout для соединения с базой данных
		/// </summary>
		protected int _connection_timout;

		/// <summary>
		/// Провайдер, используемого типа базы данных
		/// </summary>
		protected readonly DbProvider _provider;

		private readonly StoredProceduresParametersCache parametersCache;

		public IStoredProceduresParametersCache ParametersCache
		{
			get { return parametersCache; }
		}

		/// <summary>
		/// timeout для соединения с базой данных
		/// </summary>
		public int ConnectionTimeout
		{
			get { return _connection_timout; }
			set { _connection_timout = value; }
		}

		/// <summary>
		/// Строка соединения с базой даннях
		/// </summary>
		public string ConnectionString
		{
			get { return _connection; }
			set { _connection = value; }
		}

		/// <summary>
		/// timeout для команд по умолчанию
		/// </summary>
		public int DefaultTimeout
		{
			get { return _default_cmd_timeout; }
			set { _default_cmd_timeout = value; }
		}

		/// <summary> Провайдер, используемого типа базы данных </summary>
		public DbProvider DbProvider
		{
			get { return _provider; }
		}

		/// <summary>
		/// Экземпляр класса для работы с базой данных
		/// </summary>
		public TssDatabase(string connectionString, DbProvider provider)
		{
			_connection = connectionString;
			_provider = provider;

			parametersCache = new StoredProceduresParametersCache(_provider, this);
		}

		/// <summary>
		/// Создаёт адаптер для работы с отсоединённым набором данных
		/// </summary>
		/// <returns>адаптер для работы с отсоединённым набором данных</returns>
		public IDbDataAdapter Adapter()
		{
			return _provider.Adapter();
		}

		/// <summary>
		/// Создаёт соединения с базой данных
		/// </summary>
		/// <remarks>
		/// В возвращаемом коннекшене ConnectionString будет установлена
		/// </remarks>
		/// <returns>соединения с базой данных</returns>
		public IDbConnection Connection()
		{
			return _provider.Connection(_connection);
		}

		/// <summary>
		/// Формирует команду
		/// </summary>
		/// <param name="sql">имя хранимой процедуры</param>
		/// <param name="args">список параметров</param>
		/// <returns>команда</returns>
		public IDbCommand Command(string sql, params IDbDataParameter[] args)
		{
			return Command(null, sql, CommandType.StoredProcedure, 0, args);
		}

		/// <summary>
		/// Формирует команду
		/// </summary>
		/// <param name="sql">запрос SQL</param>
		/// <param name="type">тип команды</param>
		/// <param name="args">список параметров</param>
		/// <returns>команда</returns>
		public IDbCommand Command(string sql, CommandType type, params IDbDataParameter[] args)
		{
			return Command(null, sql, type, 0, args);
		}

		/// <summary>
		/// Формирует команду
		/// </summary>
		/// <param name="sql">запрос SQL</param>
		/// <param name="conn"></param>
		/// <param name="tran"></param>
		/// <returns>команда</returns>
		public IDbCommand Command(string sql, IDbConnection conn, IDbTransaction tran)
		{
			if (conn == null)
			{
				conn = Connection();
				//conn.Open();
			}
			IDbCommand command = _provider.Command();
			command.CommandText = sql;
			command.Connection = conn;
			command.Transaction = tran;
			return command;
		}

		/// <summary>
		/// Формаирование команды
		/// </summary>
		/// <param name="tran">транзакция</param>
		/// <param name="sql">текст команды</param>
		/// <param name="cmd_type">типа команды</param>
		/// <param name="timeout">таймаут</param>
		/// <param name="args">параметры команды</param>
		/// <returns></returns>
		public IDbCommand Command(IDbTransaction tran, string sql, CommandType cmd_type, int timeout, params IDbDataParameter[] args)
		{
			return Command2(tran, sql, cmd_type, timeout, args);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="tran"></param>
		/// <param name="sql"></param>
		/// <param name="cmd_type"></param>
		/// <param name="timeout"></param>
		/// <param name="arg_enum"></param>
		/// <returns></returns>
		private IDbCommand Command2(IDbTransaction tran, string sql, CommandType cmd_type, int timeout, IEnumerable arg_enum)
		{
			IDbCommand command = _provider.Command();

			if (tran != null)
			{
				command.Connection = tran.Connection;

				command.Transaction = tran;
			}
			else
			{
				command.Connection = Connection();
				//command.Connection.Open();
			}

			if (timeout != 0)
				command.CommandTimeout = timeout;
			else
				command.CommandTimeout = _default_cmd_timeout;

			command.CommandText = sql;
			command.CommandType = cmd_type;

			if (arg_enum != null)
			{
				foreach (IDbDataParameter parameter in arg_enum)
				{
					command.Parameters.Add(parameter);
				}
			}

			return command;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public StoredProcedure Procedure(string name)
		{
			IDbCommand cmd = ProcedureAsCommand(name);
			Debug.Assert(cmd.Connection.ConnectionString != String.Empty);
			return new StoredProcedure(cmd, this);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="name"></param>
		/// <param name="connection"></param>
		/// <param name="transaction"></param>
		/// <returns></returns>
		public StoredProcedure Procedure(string name, IDbConnection connection, IDbTransaction transaction)
		{
			IDbCommand cmd = ProcedureAsCommand(name, connection, transaction);
			StoredProcedure sp = new StoredProcedure(cmd, this);
			return sp;
		}

		/// <summary>
		/// Shortcut to <see cref="ProcedureAsCommand(string, IDbConnection, IDbTransaction)"/>
		/// </summary>
		/// <remarks>
		/// Name of stored procedure will be converted to upper case
		/// </remarks>
		/// <param name="name">name of stored procedure</param>
		/// <returns>Command which corresponds to stored procedure</returns>
		public IDbCommand ProcedureAsCommand(string name)
		{
			string procedure_name = name.ToUpper();
			IDbConnection conn = Connection();
			using (new ConnectionOpener(conn))
			{
				IDbCommand res = ProcedureAsCommand(procedure_name, conn, null);
				Debug.Assert(res.Connection.ConnectionString != String.Empty);
				return res;
			}
		}

		/// <summary>
		/// Loads information about procedure and it's parameters
		/// </summary>
		/// <remarks>
		/// This procedure searches for exact matches of procedure's name.
		/// There should be only one procedure with given name for different owners
		/// Otherwise results are unpredictable (choosing owner is not implemented)
		/// </remarks>
		/// <param name="procedure_name"></param>
		/// <param name="conn">connection to use (shouln't be null)</param>
		/// <param name="tran">transaction to use (may be null)</param>
		/// <returns>Command object with initialized parameters 
		/// and initialized connection/transaction properties</returns>
		public IDbCommand ProcedureAsCommand(string procedure_name, IDbConnection conn, IDbTransaction tran)
		{
			if (conn == null)
			{
				conn = Connection();
			}
			IDbCommand command = _provider.Command();
			command.Connection = conn;
			command.Transaction = tran;
			command.CommandTimeout = _default_cmd_timeout;
			command.CommandText = procedure_name;
			command.CommandType = CommandType.StoredProcedure;

			ICommandParameterAdapter param_adapter =
				parametersCache[procedure_name];

			param_adapter.Fill(command);

			return command;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sql"></param>
		public void ExecuteNonQuery(string sql)
		{
			ExecuteNonQuery(sql, CommandType.Text);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="cmdtype"></param>
		public void ExecuteNonQuery(string sql, CommandType cmdtype)
		{
			ExecuteNonQuery(sql, cmdtype, null);
		}

		/// <summary>
		/// Выполнить запрос без возврата результатов
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="args"></param>
		public void ExecuteNonQuery(string sql, params IDbDataParameter[] args)
		{
			ExecuteNonQuery(null, sql, CommandType.StoredProcedure, 0, args);
		}

		/// <summary>
		/// Выполнить запрос без возврата результатов
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="type"></param>
		/// <param name="args"></param>
		public void ExecuteNonQuery(string sql, CommandType type, params IDbDataParameter[] args)
		{
			ExecuteNonQuery(null, sql, type, 0, args);
		}

		/// <summary>
		/// Выполнить запрос без возврата результатов
		/// </summary>
		/// <param name="tran"></param>
		/// <param name="sql"></param>
		/// <param name="cmd_type"></param>
		/// <param name="timeout"></param>
		/// <param name="args"></param>
		public void ExecuteNonQuery(IDbTransaction tran, string sql, CommandType cmd_type, int timeout, params IDbDataParameter[] args)
		{
			using (IDbCommand command = Command(tran, sql, cmd_type, timeout, args))
			{
				ExecuteNonQuery(command);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="command"></param>
		public void ExecuteNonQuery(IDbCommand command)
		{
			IDbConnection connection = command.Connection;

			CommandBehavior commandBehavior = CommandBehavior.Default;
			if (connection.State == ConnectionState.Closed)
			{
				connection.Open();
				commandBehavior = CommandBehavior.CloseConnection;
			}

			try
			{
				int rowsAffected = command.ExecuteNonQuery();
			}
			finally
			{
				if (commandBehavior == CommandBehavior.CloseConnection)
				{
					connection.Close();
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public DataTable ExecuteDataTable(string sql, params IDbDataParameter[] args)
		{
			return ExecuteDataTable(null, sql, CommandType.StoredProcedure, 0, args);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="cmd_type"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public DataTable ExecuteDataTable(string sql, CommandType cmd_type, params IDbDataParameter[] args)
		{
			return ExecuteDataTable(null, sql, cmd_type, 0, args);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="tran"></param>
		/// <param name="sql"></param>
		/// <param name="cmd_type"></param>
		/// <param name="time_out"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public DataTable ExecuteDataTable(IDbTransaction tran, string sql, CommandType cmd_type, int time_out, params IDbDataParameter[] args)
		{
			DataTable table = new DataTable();
			FillDataTable(table, tran, sql, cmd_type, time_out, args);
			return table;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="table"></param>
		/// <param name="sql"></param>
		/// <param name="args"></param>
		public void FillDataTable(DataTable table, string sql, params IDbDataParameter[] args)
		{
			FillDataTable(table, null, sql, CommandType.StoredProcedure, 0, args);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="table"></param>
		/// <param name="sql"></param>
		/// <param name="cmd_type"></param>
		/// <param name="args"></param>
		public void FillDataTable(DataTable table, string sql, CommandType cmd_type, params IDbDataParameter[] args)
		{
			FillDataTable(table, null, sql, cmd_type, 0, args);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="table"></param>
		/// <param name="tran"></param>
		/// <param name="sql"></param>
		/// <param name="cmd_type"></param>
		/// <param name="time_out"></param>
		/// <param name="args"></param>
		public void FillDataTable(DataTable table, IDbTransaction tran, string sql, CommandType cmd_type, int time_out, params IDbDataParameter[] args)
		{
			try
			{
				IDbCommand comamnd = Command(tran, sql, cmd_type, time_out, args);
				FillDataTable(table, comamnd);
			}
			catch (Exception e)
			{
				Trace.WriteLine(e.ToString());
				throw;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="table"></param>
		/// <param name="command"></param>
		public void FillDataTable(DataTable table, IDbCommand command)
		{
			IDbDataAdapter iadapter = _provider.Adapter();
			iadapter.SelectCommand = command;
			Trace.WriteLine(command.ToString());
			DbDataAdapter adapter = (DbDataAdapter)iadapter;

			adapter.Fill(table);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public IDataReader ExecuteReader(string sql, params IDbDataParameter[] args)
		{
			return ExecuteReader(null, sql, CommandType.StoredProcedure, 0, args);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="type"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public IDataReader ExecuteReader(string sql, CommandType type, params IDbDataParameter[] args)
		{
			return ExecuteReader(null, sql, type, 0, args);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="type"></param>
		/// <param name="timeout"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public IDataReader ExecuteReader(string sql, CommandType type, int timeout, params IDbDataParameter[] args)
		{
			return ExecuteReader(null, sql, type, timeout, args);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="tran"></param>
		/// <param name="sql"></param>
		/// <param name="cmd_type"></param>
		/// <param name="timeout"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public IDataReader ExecuteReader(IDbTransaction tran, string sql, CommandType cmd_type, int timeout, params IDbDataParameter[] args)
		{
			try
			{
				IDbCommand command = Command(tran, sql, cmd_type, timeout, args);
				return ExecuteReader(command);
			}
			catch (Exception e)
			{
				Trace.WriteLine(e.ToString());
				throw;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		public IDataReader ExecuteReader(IDbCommand command)
		{
			if (command.Connection.State == ConnectionState.Closed)
			{
				command.Connection.Open();
			}

			return command.ExecuteReader(CommandBehavior.CloseConnection);
		}
	}
}