﻿using System.Data;

namespace FORIS.DataAccess
{
	public interface ICommandParameterAdapter
	{
		void Fill(IDbCommand icommand);
	}
}