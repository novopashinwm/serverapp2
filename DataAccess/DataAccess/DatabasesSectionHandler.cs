﻿using System;
using System.Collections;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.Remoting;
using System.Xml;

namespace FORIS.DataAccess
{
	/// <summary> Summary description for DatabasesSectionHandler </summary>
	public class DatabasesSectionHandler : IConfigurationSectionHandler
	{
		/// <summary>
		/// Maps provider name to config section handler
		/// </summary>
		Hashtable providers = new Hashtable();
		public object Create(object parent, object configContext, XmlNode section)
		{
			TssDatabaseManager manager = new TssDatabaseManager();
			foreach (XmlNode node in section.ChildNodes)
			{
				switch (node.Name.ToUpper())
				{
					case "PROVIDERS":
						foreach (XmlNode prov in node.ChildNodes)
						{
							if (prov.Name.ToUpper() == "PROVIDER")
							{
								object handler = LoadProvider(parent, configContext, prov);
								string provname = prov.Attributes["name"].Value;

								if (providers.ContainsKey(provname))
									continue;

								this.providers.Add(provname, handler);
							}
						}
						break;
					case "INSTANCES":
						foreach (XmlNode dbinstance in node.ChildNodes)
						{
							string name = dbinstance.LocalName;
							if (providers.ContainsKey(name) == false)
							{
								continue;
							}
							object o = providers[name];
							ISectionHandler sh = o as ISectionHandler;
							object db = sh.Create(parent, configContext, dbinstance);
							if (db != null)
							{
								TssDatabase database = (TssDatabase)db;
								string dbiname = dbinstance.Attributes["name"].Value;
								manager.Add(dbiname, database);
							}
							else
							{
								Trace.WriteLine("Not implemented handler");
							}
						}
						break;
					default:
						{
						}
						break;
				}
			}
			return manager;
		}
		private object LoadProvider(object parent, object configContext, XmlNode node)
		{
			string type = node.Attributes["type"].Value;
			int commaindex = type.IndexOf(",");
			if (commaindex < 0)
			{
				commaindex = type.Length;
			}
			string typename = type.Substring(0, commaindex);
			string assembly = type.Substring(commaindex + 1).Trim();
			ObjectHandle oh = Activator.CreateInstance(assembly, typename);
			object handler = oh.Unwrap();
			if (handler == null)
			{
				throw new ConfigurationErrorsException("Unknown type: " + type.ToString());
			}
			return handler;
		}
	}
}