using System;
using System.Text;

namespace FORIS.DataAccess
{
	/// <summary>
	/// Summary description for NameHelper.
	/// </summary>
	public class NameHelper
	{
		public static string ToPascal(string name)
		{
			StringBuilder builder = new StringBuilder(name.Trim().ToLower());
			if (builder.Length > 0)
			{
				for (int i = 1; i < builder.Length; ++i)
				{
					if (builder[i - 1] == '_')
					{
						builder[i] = builder[i].ToString().ToUpper()[0];
					}
				}
				builder.Replace("_", "");
				builder[0] = builder[0].ToString().ToUpper()[0];
			}
			return builder.ToString();
		}
		public static string ToOracle(string name)
		{
			if (name.Length >= 30)
			{
				StringBuilder sb = new StringBuilder();
				int overrun = (name.Length + 29) / 30;
				for (int i = 0; i < name.Length; i += overrun)
				{
					sb.Append(name[i]);
				}
				return sb.ToString();
			}
			else
			{
				return name;
			}
		}
		public static string QuoteValue(string type, string field)
		{
			StringBuilder quote = new StringBuilder();
			switch (type)
			{
				case "SqlBoolean":
					quote.AppendFormat("\"(\"+{0}+\"?true:false)\"", field);
					break;
				case "SqlString": case "SqlMoney": 
					quote.AppendFormat("\"'\"+{0}+\"'\"", field);
					break;
				default:
					quote.AppendFormat("{0}", field);
					break;
			}
			return quote.ToString();
		}
	}
}
