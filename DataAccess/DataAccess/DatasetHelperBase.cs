﻿using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text;
using FORIS.DataAccess;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;

namespace FORIS.TSS.BusinessLogic
{
	public class DatasetHelperBase
	{
		public static DataRow GetRow(DataSet dataset, string tableName, params object[] keys)
		{
			DataTable table = dataset.Tables[tableName];
			if (table == null)
			{
				StringBuilder message = new StringBuilder();
				message.AppendFormat("no table with name {0} in dataset", tableName);
				throw new ArgumentException(message.ToString(), nameof(tableName));
			}
			DataRow row = table.Rows.Find(keys);
			if (row == null)
			{
				// check for deleted rows
				StringBuilder filterExpression = new StringBuilder(100);
				for (int index = 0; index < table.PrimaryKey.Length; ++index)
				{
					if (index > 0)
					{
						filterExpression.Append(" AND ");
					}
					DataColumn c = table.PrimaryKey[index];
					filterExpression.AppendFormat("{0}={1}", c.ColumnName, keys[index]);
				}
				StringBuilder sortExpression = new StringBuilder(100);
				for (int index2 = 0; index2 < table.PrimaryKey.Length; ++index2)
				{
					if (index2 > 0)
					{
						sortExpression.Append(", ");
					}
					DataColumn c = table.PrimaryKey[index2];
					sortExpression.AppendFormat("{0}", c.ColumnName);
				}
				DataRow[] rows = table.Select(filterExpression.ToString(), sortExpression.ToString(), DataViewRowState.Deleted);
				if (rows.Length > 0)
				{
					// Deleted row was founded
					row = rows[0];
				}
			}
			return row;
		}
		public static void Dump(DataSet ds, IndentedTextWriter tw)
		{
			if (Globals.TraceDataSets == false) return;
			if (ds == null)
			{
				Trace.WriteLine("dataset is null");
				return;
			}
			foreach (DataTable dt in ds.Tables)
			{
				tw.WriteLine("Table: " + dt.TableName);
				tw.Indent++;
				try
				{
					// dump column names
					foreach (DataColumn col in dt.Columns)
					{
						tw.Write(col.ColumnName + "\t");
					}
					tw.WriteLine("");
					// dump rows
					foreach (DataRow row in dt.Rows)
					{
						if (row.RowState != DataRowState.Deleted)
						{
							foreach (DataColumn col in dt.Columns)
							{
								tw.Write(row[col, DataRowVersion.Current] + "\t");
							}
						}
						else
						{
							foreach (DataColumn col in dt.Columns)
							{
								tw.Write(row[col, DataRowVersion.Original] + "\t");
							}
						}
						tw.WriteLine(" " + row.RowState.ToString());
					}
				}
				finally
				{
					tw.Indent--;
				}
			}
		}
		public static void Dump(DataSet ds)
		{
			if (Globals.TraceDataSets == false) return;
			if (ds == null)
			{
				Trace.WriteLine("dataset is null");
				return;
			}
			using (MemoryStream stmMemory = new MemoryStream())
			{
				using (TextWriter w = new StreamWriter(stmMemory, Encoding.Unicode))
				{
					using (IndentedTextWriter tw = new IndentedTextWriter(w))
					{
						Dump(ds, tw);
						tw.Flush();
					}
					w.Flush();
				}
				Debug.WriteLine(Encoding.Unicode.GetString(stmMemory.GetBuffer()));
			}
		}
		public static void DumpDiff(DataSet ds, IndentedTextWriter tw)
		{
			if (Globals.TraceDataSets == false) return;
			if (ds == null)
			{
				Trace.WriteLine("dataset is null");
				return;
			}
			foreach (DataTable dt in ds.Tables)
			{
				tw.WriteLine("Table: " + dt.TableName);
				tw.Indent++;
				try
				{
					// dump column names
					foreach (DataColumn col in dt.Columns)
					{
						tw.Write(col.ColumnName + "\t");
					}
					tw.WriteLine("");
					// dump rows
					foreach (DataRow row in dt.Rows)
					{
						if (row.HasVersion(DataRowVersion.Original))
						{
							tw.Write("(old) ");
							foreach (DataColumn col in dt.Columns)
							{
								tw.Write(row[col, DataRowVersion.Original] + "\t");
							}
							tw.WriteLine("");
						}
						else
						{
							tw.WriteLine("no old version");
						}
					}
					foreach (DataRow row in dt.Rows)
					{
						if (row.HasVersion(DataRowVersion.Current))
						{
							tw.Write("(new) ");
							foreach (DataColumn col in dt.Columns)
							{
								tw.Write(row[col, DataRowVersion.Current] + "\t");
							}
							tw.WriteLine("");
						}
						else
						{
							tw.WriteLine("no current version");
						}
					}
				}
				finally
				{
					tw.Indent--;
				}
			}
		}
		public static void DumpTraceDiff(DataSet ds)
		{
			if (Globals.TraceDataSets == false) return;
			if (ds == null)
			{
				Trace.WriteLine("dataset is null");
				return;
			}
			using (MemoryStream stmMemory = new MemoryStream())
			{
				using (TextWriter w = new StreamWriter(stmMemory, Encoding.Unicode))
				{
					using (IndentedTextWriter tw = new IndentedTextWriter(w))
					{
						DumpDiff(ds, tw);
						tw.Flush();
					}
					w.Flush();
				}
				Trace.WriteLine(Encoding.Unicode.GetString(stmMemory.GetBuffer()));
			}
		}
		public static void RenameTables(DataSet ds, string[] table_names)
		{
			#region Preconditions 

			/* Так как метод работает и на стороне сервера тоже,
			 * то из-за невозможности взаимодействия с рабочим столом
			 * служба зависает при попытке вызова Debug.Assert()
			 * с не выполняющимся условием. 
			 * Лучше выкинуть исключение без зависания.
			 */

			if (ds.Tables.Count != table_names.Length)
			{
				string tableNames = string.Empty;

				foreach (string tableName in table_names)
				{
					tableNames += string.Format("[{0}]", tableName);
				}

				StackTrace trace = new StackTrace(true);
				StackFrame frame = trace.GetFrame(1);

				throw new Exception(
					"DataSet tables count doesn't match speculative count" + "\r\n\r\n" +
					"method: " + frame.GetMethod().Name + "\r\n\r\n" +
					"tables:  " + tableNames
					);
			}

			#endregion // Preconditions

			for (int i = 0; i < ds.Tables.Count; ++i)
			{
				ds.Tables[i].TableName = table_names[i];
			}
		}
		/// <summary> Сжимает набор данных перед обновлением в БД </summary>
		/// <param name="ds"></param>
		/// <param name="databaseSchema"></param>
		public static void CompactTables(DataSet ds, IDatabaseSchema databaseSchema)
		{
			// remove relations
			ds.EnforceConstraints = false;
			ds.Relations.Clear();

			// удаляем Foreign Key Constraint из таблиц 
			foreach (DataTable t in ds.Tables)
			{
				ArrayList fk = new ArrayList();
				foreach (Constraint dc in t.Constraints)
				{
					if (dc is ForeignKeyConstraint)
					{
						fk.Add(dc);
					}
				}
				foreach (Constraint dc in fk)
				{
					t.Constraints.Remove(dc);
				}
			}

			foreach (DataTable t in ds.Tables)
			{
				t.Constraints.Clear();
			}

			// Compact unmodified records
			foreach (DataTable t in ds.Tables)
			{
				ArrayList rowsToDelete = new ArrayList();
				foreach (DataRow r in t.Rows)
				{
					if (r.RowState == DataRowState.Unchanged)
					{
						rowsToDelete.Add(r);
					}
				}
				foreach (DataRow r in rowsToDelete)
				{
					//r.Delete();
					t.Rows.Remove(r);
				}
			}
			// Compact empty tables
			ArrayList tables = new ArrayList();
			foreach (DataTable t in ds.Tables)
			{
				if (t.Rows.Count == 0)
				{
					tables.Add(t);
				}
			}
			foreach (DataTable t in tables)
			{
				ds.Tables.Remove(t);
			}
			// Восстанавливаем ограничения если схема задана
			databaseSchema?.MakeSchema(ds);
		}
	}
}