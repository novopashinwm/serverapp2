﻿using System;
using System.Data;

namespace FORIS.DataAccess
{
	public abstract class DbProvider
	{
		public DbProvider()
		{
		}
		public abstract IDbConnection Connection(string connection_string);
		public abstract IDbDataParameter Parameter();
		public abstract IDbCommand Command();
		public abstract IDbDataAdapter Adapter();
		public virtual ICommandParameterAdapter PrepareParameters(string procedure, string connection)
		{
			throw new NotSupportedException(string.Format("PrepareParameters method for {0} not supported", GetType().Name));
		}
		public virtual ICommandParameterAdapter PrepareParameters(string procedure, IDbConnection conn, IDbTransaction tran)
		{
			throw new NotSupportedException(string.Format("PrepareParameters method for {0} not supported", GetType().Name));
		}
	}
}