﻿using System;
using System.Configuration;
using System.Diagnostics;
using FORIS.TSS.Config;

namespace FORIS.DataAccess
{
	/// <summary> Summary description for Globals. </summary>
	public static class Globals
	{
		#region TssDatabaseManager

		private static readonly Lazy<TssDatabaseManager> _databaseManagerLazy = new Lazy<TssDatabaseManager>(GetTssDatabaseManagerValue);
		private static TssDatabaseManager GetTssDatabaseManagerValue()
		{
			try
			{
				var config = ConfigurationManager.GetSection("databases");
				if (config == null)
				{
					Trace.TraceWarning("No databases section found");
					return null;
				}

				var databaseManager = (TssDatabaseManager)config;
				var databaseName = GlobalsConfig.AppSettings["Database"];
				if (databaseName == null)
				{
					Trace.TraceWarning("Database name not specified in AppSettings");
					return null;
				}
				databaseManager.SetDefaultDatabase(databaseName);
				return databaseManager;
			}
			catch (Exception ex)
			{
				Trace.TraceError("{0}", ex);
			}
			return default(TssDatabaseManager);
		}
		public static TssDatabaseManager TssDatabaseManager
		{
			get
			{
				return _databaseManagerLazy.Value;
			}
		}

		#endregion TssDatabaseManager
		#region TraceDataSets

		private static readonly Lazy<bool> _tracingInUpdateLazy = new Lazy<bool>(GetTracingInUpdateValue);
		private static bool GetTracingInUpdateValue()
		{
			return GlobalsConfig.AppSettings["TraceDataSets"]?.ToUpper() == "TRUE";
		}
		public static bool TraceDataSets
		{
			get
			{
				return _tracingInUpdateLazy.Value;
			}
		}

		#endregion TraceDataSets
	}
}