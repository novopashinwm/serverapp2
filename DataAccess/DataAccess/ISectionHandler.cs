﻿namespace FORIS.DataAccess
{
	public interface ISectionHandler
	{
		object Create(object parent, object configContext, System.Xml.XmlNode section);
	}
}