﻿using System.Collections.Generic;
using System.Data;
using FORIS.DataAccess.Interfaces;

namespace FORIS.DataAccess
{
	public class StoredProceduresParametersCache : IStoredProceduresParametersCache
	{
		public StoredProceduresParametersCache(DbProvider provider, IDbConnectionProvider connectionProvider)
		{
			this.provider           = provider;
			this.connectionProvider = connectionProvider;
		}
		private readonly DbProvider provider;
		private readonly IDbConnectionProvider connectionProvider;
		private readonly Dictionary<string, ICommandParameterAdapter> cache =
			new Dictionary<string, ICommandParameterAdapter>();
		public ICommandParameterAdapter this[string procedureName]
		{
			get
			{
				lock (this.cache)
				{
					if (!this.cache.ContainsKey(procedureName))
					{
						using (IDbConnection connection = this.connectionProvider.GetConnection())
						{
							connection.Open();

							this.cache.Add(
								procedureName,
								this.provider.PrepareParameters(procedureName, connection, null)
								);
						}
					}
				}

				return this.cache[procedureName];
			}
		}
		public IList<string> Names
		{
			get
			{
				lock (this.cache)
				{
					return new List<string>(this.cache.Keys);
				}
			}
		}
		public void Reset()
		{
			lock (this.cache)
			{
				this.cache.Clear();
			}
		}
	}
}