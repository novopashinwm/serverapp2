﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;
using FORIS.DataAccess.Interfaces;

namespace FORIS.DataAccess
{
	public class StoredProcedure : IStoredProcedure
	{
		/// <summary> команда </summary>
		protected IDbCommand _command;
		/// <summary> ссылка на БД </summary>
		protected TssDatabase _db;
		/// <summary> Команда </summary>
		public IDbCommand Command
		{
			get
			{
				return _command;
			}
		}
		/// <summary> Набор параметров процедуры </summary>
		public IDataParameterCollection Parameters
		{
			get
			{
				return _command.Parameters;
			}
		}
		public StoredProcedure(IDbCommand command, TssDatabase database)
		{
			_command = command;
			_db = database;
		}
		public void ExecuteNonQuery()
		{
			_db.ExecuteNonQuery(_command);
		}
		public object ExecuteScalar()
		{
			using (new ConnectionOpener(_command.Connection))
			{
				if (_command.Connection.State == ConnectionState.Open)
				{
					object res = _command.ExecuteScalar();
					return res;
				}
				else
					throw new ApplicationException("ConnectionState = " + _command.Connection.State.ToString());
			}
		}
		public IDataReader ExecuteReader()
		{
			using (new ConnectionOpener(_command.Connection))
			{
				return _command.ExecuteReader();
			}
		}
		public IDataReader ExecuteReader(CancellationToken cancellationToken)
		{
			using (new ConnectionOpener(_command.Connection))
			{
				var sqlCommand = _command as SqlCommand;
				if (sqlCommand == null)
					return _command.ExecuteReader();

				return sqlCommand.ExecuteReaderAsync(cancellationToken).Result;
			}
		}
		public void Fill(DataSet dataSet)
		{
			using (new ConnectionOpener(_command.Connection))
			{
				try
				{
					IDbDataAdapter adapter = _db.DbProvider.Adapter();
					adapter.SelectCommand = _command;
					adapter.Fill(dataSet);
				}
				catch (SqlException ex)
				{
					Trace.TraceError(GetType().Name + ": Fill(DataSet dataSet) SqlException {0}", ex);
					for (int i = 0; i < ex.Errors.Count; i++)
						Trace.WriteLine($"\tIndex #{i}: Proc={ex.Errors[i].Procedure}, Line={ex.Errors[i].LineNumber}\n{ex.Errors[i].Message}\n");
				}
			}
		}
		public DataSet ExecuteDataSet()
		{
			DataSet ds = new DataSet();
			try
			{
				Fill(ds);
			}
			catch (SqlException ex)
			{
				Trace.TraceError(GetType().Name + ": ExecuteDataSet() SqlException {0}", ex);
				for (int i = 0; i < ex.Errors.Count; i++)
					Trace.WriteLine($"\tIndex #{i}: Proc={ex.Errors[i].Procedure}, Line={ex.Errors[i].LineNumber}\n{ex.Errors[i].Message}\n");
			}
			return ds;
		}
		public DataSet ExecuteDataSet(params string[] names)
		{
			DataSet ds = new DataSet();
			try
			{
				Fill(ds);
				if (names.Length != ds.Tables.Count)
				{
					throw new ApplicationException("database to code mismatch");
				}
				for (int i = 0; i < names.Length; ++i)
				{
					ds.Tables[i].TableName = names[i];
				}
			}
			catch (SqlException ex)
			{
				Trace.TraceError(GetType().Name + ": ExecuteDataSet(params string[] names) SqlException {0}", ex);
				for (int i = 0; i < ex.Errors.Count; i++)
					Trace.WriteLine($"\tIndex #{i}: Proc={ex.Errors[i].Procedure}, Line={ex.Errors[i].LineNumber}\n{ex.Errors[i].Message}\n");
			}
			return ds;
		}
		public IDbDataParameter this[string name]
		{
			get
			{
				try
				{
					IDbDataParameter parameter = _command.Parameters[name] as IDbDataParameter;
					if (parameter == null)
						throw new ArgumentOutOfRangeException("name", name,
															  string.Format("There are no parameter {0} in Stored procedure {1}",
																			name, _command.CommandText));
					return parameter;
				}
				catch (Exception ex)
				{
					throw
						new DataAccessException(
							string.Format("Stored procedure {0}: {1}",
								_command.CommandText, ex.Message),
							ex);
				}
			}
		}
		public IDbDataParameter this[int i]
		{
			get
			{
				try
				{
					return _command.Parameters[i] as IDbDataParameter;
				}
				catch (Exception ex)
				{
					throw
						new DataAccessException(
							string.Format("Stored procedure {0}: {1}",
								_command.CommandText, ex.Message),
							ex);
				}
			}
		}
		public void Dispose()
		{
			if (_command != null)
			{
				_command.Dispose();
			}
		}
		public void SetTableValuedParameter(string name, DataTable value)
		{
			var parameter = this[name];
			if (parameter == null)
				throw new ArgumentOutOfRangeException("name", name, "Unknown parameter name");
			var sqlParameter = parameter as SqlParameter;
			if (sqlParameter != null)
			{
				sqlParameter.SqlDbType = SqlDbType.Structured;
			}

			parameter.Value = value;
		}
		public void SetTableValuedParameter(string name, int[] ids)
		{
			SetTableValuedParameter(name, GetIdTable(ids));
		}
		private static DataTable GetIdTable(IEnumerable<int> vehicleIDs)
		{
			var vehicleIDsTable = new DataTable("Ids");
			vehicleIDsTable.Columns.Add("Id", typeof(int));
			foreach (var id in vehicleIDs)
				vehicleIDsTable.Rows.Add(id);
			return vehicleIDsTable;
		}
	}
}