﻿namespace FORIS.TSS.ServerApplication.Geo
{
	public class GeoSessionManager : SessionManager<GeoServer>
	{
		private readonly GeoServerDispatcher _serverDispatcher = new GeoServerDispatcher();
		protected override ServerDispatcher<GeoServer> GetServerDispatcher()
		{
			return _serverDispatcher;
		}
		/// <summary> Метод создает объект сессии на сервере </summary>
		/// <returns> объект сессии - персональный сервер </returns>
		/// <remarks>
		/// Возможно следует передавать какие-нибудь параметры
		/// в этот метод, чтоб можно было создавать объекты
		/// сессий различных типов в зависимости от запроса клиента
		/// </remarks>
		protected override PersonalServerBase<GeoServer> OnCreatePersonalServer()
		{
			return new GeoPersonalServer();
		}
	}
}