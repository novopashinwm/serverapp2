﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using FORIS.DataAccess;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;
using FORIS.TSS.ServerApplication.Geo.Address;
using FORIS.TSS.ServerApplication.Geo.Address.Suppliers;
using FORIS.TSS.ServerApplication.Geo.Distances;
using Interfaces.Geo;

namespace FORIS.TSS.ServerApplication.Geo
{
	public class GeoServer : ServerBase<GeoServer>, IGeoAddressProvider, IGeoArrivalProvider
	{
		#region Controls & Components

		private IContainer                                _components;
		private GeoServerDispatcher                       _serverDispatcher;
		private GeoServerDatabaseManager                  _databaseManager;
		private Dictionary<Guid, ICachingAddressResolver> _cachingAddressResolvers;

		#endregion Controls & Components

		#region Constructor & Dispose

		public GeoServer(SessionManager<GeoServer> sessionManager, NameValueCollection properties)
			: base(sessionManager, properties)
		{
			InitializeComponent();
			Init();
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
				_components?.Dispose();
			base.Dispose(disposing);
		}

		#endregion Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			_components       = new Container();
			_serverDispatcher = new GeoServerDispatcher(_components);
			_databaseManager  = new GeoServerDatabaseManager(_components) { Database = Globals.TssDatabaseManager.DefaultDataBase };

			_cachingAddressResolvers = new Dictionary<Guid, ICachingAddressResolver>();
			var openStreetMap = new PrimaryCacheWrapper(new OpenStreetMapAddressResolver());
			if (openStreetMap.IsConfigured)
				_cachingAddressResolvers.Add(openStreetMap.MapId, openStreetMap);

			var googleMap = new TableCacheWrapper(new GoogleMapsAddressResolver(), _databaseManager);
			if (googleMap.IsConfigured)
				_cachingAddressResolvers.Add(googleMap.MapId, googleMap);

			var yandexMap = new TableCacheWrapper(new YandexAddressResolver(), _databaseManager);
			if (yandexMap.IsConfigured)
				_cachingAddressResolvers.Add(yandexMap.MapId, yandexMap);

			var googleDst = new GoogleMapsDistanceResolver();
			if (googleDst.IsConfigured)
			{
				// Нужно для создания счетчиков производительности
			}
		}

		#endregion Component Designer generated code

		protected override ServerDispatcher<GeoServer> GetServerDispatcher()
		{
			return _serverDispatcher;
		}

		protected override IDatabaseDataSupplier GetDatabaseDataSupplier()
		{
			return _databaseManager;
		}

		#region Start & Stop

		protected override void Start()
		{
		}
		protected override void Stop()
		{
		}

		#endregion Start & Stop

		#region Find Address & Route

		public GetLatLngByAddressResult GetLatLngByAddress(string address, out double? lat, out double? lng, Guid? mapGuid = null)
		{
			lat = lng = null;
			var cachingAddressResolver = default(ICachingAddressResolver);
			if (!mapGuid.HasValue)
				mapGuid = AddressHelper.DefaultMapGuid;
			if (!(mapGuid.HasValue && _cachingAddressResolvers.TryGetValue(mapGuid.Value, out cachingAddressResolver)))
				cachingAddressResolver = _cachingAddressResolvers.Values.OfType<PrimaryCacheWrapper>().FirstOrDefault();
			return cachingAddressResolver?.GetLatLngByAddress(address, lat: out lat, lng: out lng) ?? GetLatLngByAddressResult.FailedAddressNotFound;
		}
		public IEnumerable<AddressResponse> GetAddressesByText(string searchText, string language, Guid? mapGuid = null)
		{
			var cachingAddressResolver = default(ICachingAddressResolver);
			if (!mapGuid.HasValue)
				mapGuid = AddressHelper.DefaultMapGuid;
			if (!(mapGuid.HasValue && _cachingAddressResolvers.TryGetValue(mapGuid.Value, out cachingAddressResolver)))
				cachingAddressResolver = _cachingAddressResolvers.Values.OfType<PrimaryCacheWrapper>().FirstOrDefault();
			return cachingAddressResolver
				?.GetAddressesByText(searchText, language)
				?.Select(ar => new AddressResponse(
					id:       ar.Id,
					lat:      ar.Lat,
					lng:      ar.Lng,
					address:  AddressHelper.CleanupAddress(ar.Address, mapGuid),
					distance: ar.Distance,
					size:     ar.Size))
				?? EmptyCollection;
		}
		public string GetAddressByPoint(double lat, double lng, string language, Guid? mapGuid = null, bool cacheOnly = false)
		{
			if (!GeoHelper.ValidLocation(lat: lat, lng: lng))
				return null;

			var cachingAddressResolver = default(ICachingAddressResolver);
			if (!mapGuid.HasValue)
				mapGuid = AddressHelper.DefaultMapGuid;
			if (!(mapGuid.HasValue && _cachingAddressResolvers.TryGetValue(mapGuid.Value, out cachingAddressResolver)))
				cachingAddressResolver = _cachingAddressResolvers.Values.OfType<PrimaryCacheWrapper>().FirstOrDefault();
			if (null == cachingAddressResolver)
				return null;

			return AddressHelper.CleanupAddress(
				cachingAddressResolver.GetAddressByPoint(lat: lat, lng: lng, language, cacheOnly)
					?.Address,
				cachingAddressResolver.MapId);
		}
		private static readonly IEnumerable<AddressResponse> EmptyCollection = new AddressResponse[0];
		private struct LatLngTuple
		{
			public readonly double Lat;
			public readonly double Lng;
			public LatLngTuple(double lat, double lng)
			{
				Lat = lat;
				Lng = lng;
			}
			public override bool Equals(object obj)
			{
				var x = obj as LatLngTuple?;
				if (x == null)
					return false;

				return Lat == x.Value.Lat && Lng == x.Value.Lng;
			}
			public override int GetHashCode()
			{
				return Lat.GetHashCode() ^ Lng.GetHashCode();
			}
		}
		public IEnumerable<AddressResponse> GetAddresses(IEnumerable<AddressRequest> requests, CultureInfo culture, Guid? mapGuid = null)
		{
			var cachingAddressResolver = default(ICachingAddressResolver);
			if (!mapGuid.HasValue)
				mapGuid = AddressHelper.DefaultMapGuid;
			if (!(mapGuid.HasValue && _cachingAddressResolvers.TryGetValue(mapGuid.Value, out cachingAddressResolver)))
				cachingAddressResolver = _cachingAddressResolvers.Values.OfType<PrimaryCacheWrapper>().FirstOrDefault();
			if (null == cachingAddressResolver)
				return EmptyCollection;

			try
			{
				return cachingAddressResolver.GetAddresses(requests, culture);
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
			}
			return EmptyCollection;
		}
		public DataSet FillAddressColumn(DataSet dataSet, string tableName, string lngColumnName, string latColumnName, string addressColumnName, string language, Guid? mapGuid = null)
		{
			var cachingAddressResolver = default(ICachingAddressResolver);
			if (!mapGuid.HasValue)
				mapGuid = AddressHelper.DefaultMapGuid;
			if (!(mapGuid.HasValue && _cachingAddressResolvers.TryGetValue(mapGuid.Value, out cachingAddressResolver)))
				cachingAddressResolver = _cachingAddressResolvers.Values.OfType<PrimaryCacheWrapper>().FirstOrDefault();
			if (null == cachingAddressResolver)
				return dataSet;
			try
			{
				var culture = CultureInfo.GetCultureInfoByIetfLanguageTag(language);
				return AddressHelper.FillAddressColumnWrapper(
					cachingAddressResolver.MapId,
					dataSet,
					tableName,
					lngColumnName,
					latColumnName,
					addressColumnName,
					requests => cachingAddressResolver.GetAddresses(requests, culture));
			}
			catch (NotSupportedException)
			{
			}
			return dataSet;
		}
		#endregion Find Address & Route

		public ArrivalTimesToPoint GetArrivalTimesToPoint(LatLng destination, ArrivalTimeForObject[] origins, string language)
		{
			return new GoogleMapsDistanceResolver()
				.GetArrivalTimesToPoint(destination, origins, language);
		}
	}
}