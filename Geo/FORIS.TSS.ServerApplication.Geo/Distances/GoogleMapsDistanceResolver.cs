﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;
using FORIS.TSS.ServerApplication.Geo.Address;
using GoogleApi;
using GoogleApi.Entities.Common;
using GoogleApi.Entities.Common.Enums;
using GoogleApi.Entities.Common.Enums.Extensions;
using GoogleApi.Entities.Maps.Common.Enums;
using GoogleApi.Entities.Maps.DistanceMatrix.Request;
using GoogleApi.Entities.Translate.Common.Enums.Extensions;
using Interfaces.Geo;

namespace FORIS.TSS.ServerApplication.Geo.Distances
{
	public class GoogleMapsDistanceResolver : IArrivalResolver
	{
		private const int POINTS_LIMIT = 100;
		private string                       _apiKey;
		private string                       _channel;
		private Dictionary<string, Language> _langs;

		private static DistanceResolverPerfCounters _perfs;
		static GoogleMapsDistanceResolver()
		{
			_perfs = new DistanceResolverPerfCounters($"{typeof(GoogleMapsDistanceResolver).Name}");
		}
		public GoogleMapsDistanceResolver()
		{
			_apiKey  = this.GetAppSettingAsString("ApiKey");
			_channel = this.GetAppSettingAsString("Channel");
			_langs   = Enum.GetValues(typeof(Language))
				.OfType<Language>()
				.ToDictionary(k => k.ToCode());
		}
		/// <summary> Сконфигурирован </summary>
		public bool IsConfigured
		{
			get
			{
				return !string.IsNullOrEmpty(_apiKey);
			}
		}
		public ArrivalTimesToPoint GetArrivalTimesToPoint(LatLng destination, ArrivalTimeForObject[] origins, string language)
		{
			var result = new ArrivalTimesToPoint { PointLatLng = destination };
			var originsCnt = origins?.Count() ?? 0;
			var originsIdx = 0;
			do
			{
				var reqOrigins = origins
					.Skip(originsIdx)
					.Take(POINTS_LIMIT)
					.ToList();
				originsIdx += reqOrigins?.Count() ?? 0;
				if (0 < (reqOrigins?.Count() ?? 0))
				{
					_perfs?.Increment(DistanceResolverPerfCounters.DISTANCE_MATRIX_REQUESTS_COUNT);
					_perfs?.Increment(DistanceResolverPerfCounters.DISTANCE_MATRIX_ELEMENTS_COUNT, reqOrigins.Count);
					var req = new DistanceMatrixRequest
					{
						Units         = Units.Metric,
						TravelMode    = TravelMode.Driving,
						TrafficModel  = TrafficModel.Best_Guess,
						Origins       = reqOrigins.Select(o => new Location((double)o.ObjectLatLng.Lat, (double)o.ObjectLatLng.Lng)),
						Destinations  = new[] { new Location((double)destination.Lat, (double)destination.Lng) },
						Key           = _apiKey,
						Channel       = _channel,
						Language      = _langs.GetValueOrDefault(language, Language.English),
					};
					///////////////////////////////////////////////////////////////////////////////////////////////////////////
					var res = GoogleMaps.DistanceMatrix.Query(req);
					$"{CallStackHelper.GetCallerMethodFullName()}\nREQUEST:\n{res?.RawQueryString?.UnescapeDataString()}\nRESPONSE:\n{res?.RawJson}"
						.TraceInformation();
					///////////////////////////////////////////////////////////////////////////////////////////////////////////
					if (res?.Status == Status.Ok)
					{
						if (string.IsNullOrWhiteSpace(result.PointAddress))
							result.PointAddress = res.DestinationAddresses.FirstOrDefault();
						if (null == result.ArrivalTimes)
							result.ArrivalTimes = new List<ArrivalTimeForObject>();
						result.ArrivalTimes.AddRange(reqOrigins
							?.Zip(res.OriginAddresses, (v, o) => new
							{
								ObjectId      = v.ObjectId,
								ObjectLatLng  = v.ObjectLatLng,
								ObjectAddress = o
							})
							?.Zip(res.Rows, (v, r) => new
							{
								ObjectId      = v.ObjectId,
								ObjectLatLng  = v.ObjectLatLng,
								ObjectAddress = v.ObjectAddress,
								ArrivalInfo   = r.Elements.Zip(res.DestinationAddresses, (element, Address) => new
								{
									element,
									Address
								})
								?.FirstOrDefault(e =>
									e.Address        == result.PointAddress &&
									e.element.Status == Status.Ok)
							})
							?.Where(a => null != a.ArrivalInfo)
							?.Select(a => new ArrivalTimeForObject
							{
								ObjectId                                 = a.ObjectId,
								ObjectLatLng                             = a.ObjectLatLng,
								ObjectAddress                            = a.ObjectAddress,
								DistanceInMeters                         = (int?)a.ArrivalInfo?.element?.Distance?.Value,
								EstimatedArrivalTimeInSeconds            = (int?)a.ArrivalInfo?.element?.Duration?.Value,
								EstimatedArrivalTimeInSecondsWithTraffic = (int?)a.ArrivalInfo?.element?.DurationInTraffic?.Value
							})
						);
					}
					else
						_perfs?.Increment(DistanceResolverPerfCounters.DISTANCE_MATRIX_REQUESTS_ERRORS);
				}
			} while (originsIdx < originsCnt);
			return result;
		}
	}
}