﻿using System;
using System.Diagnostics;
using System.Linq;

namespace FORIS.TSS.ServerApplication.Geo.Address
{
	public sealed class DistanceResolverPerfCounters : IDisposable
	{
		public const string PERF_COUNTER_CATEGORY_NAME = "Ufin Distance Matrix";
		public const string PERF_COUNTER_CATEGORY_HELP = "Ufin Distance Matrix performance counters";

		#region Distance Matrix counters

		public const string DISTANCE_MATRIX_REQUESTS_COUNT  = "Distance Matrix requests count";
		public const string DISTANCE_MATRIX_REQUESTS_ERRORS = "Distance Matrix requests with errors";
		public const string DISTANCE_MATRIX_ELEMENTS_COUNT  = "Distance Matrix elements count";

		#endregion Distance Matrix counters

		/// <summary> Ссылки на сами счетчики </summary>
		private PerformanceCounter[] _counters;

		public static void CreateCategory()
		{
			if (PerformanceCounterCategory.Exists(PERF_COUNTER_CATEGORY_NAME))
				PerformanceCounterCategory.Delete(PERF_COUNTER_CATEGORY_NAME);

			PerformanceCounterCategory.Create(
				PERF_COUNTER_CATEGORY_NAME,
				PERF_COUNTER_CATEGORY_HELP,
				PerformanceCounterCategoryType.MultiInstance,
				new CounterCreationDataCollection(new[]
					{
						new CounterCreationData(DISTANCE_MATRIX_REQUESTS_COUNT,  DISTANCE_MATRIX_REQUESTS_COUNT,  PerformanceCounterType.NumberOfItems32),
						new CounterCreationData(DISTANCE_MATRIX_REQUESTS_ERRORS, DISTANCE_MATRIX_REQUESTS_ERRORS, PerformanceCounterType.NumberOfItems32),
						new CounterCreationData(DISTANCE_MATRIX_ELEMENTS_COUNT,  DISTANCE_MATRIX_ELEMENTS_COUNT,  PerformanceCounterType.NumberOfItems64),
					}));
		}

		public DistanceResolverPerfCounters(string resolverName)
		{
			if (null == resolverName)
				throw new ArgumentNullException(nameof(resolverName));

			InitDisposableMembers(resolverName);
		}
		public void Dispose()
		{
			_counters
				?.ToList()
				?.ForEach(perfCounter =>
				{
					try
					{
						if (null != perfCounter)
						{
							if (0 < perfCounter.RawValue)
								perfCounter.RawValue = 0;
							perfCounter.Dispose();
						}
					}
					catch (Exception ex)
					{
						Trace.TraceError("Can not destroy performance counter Category: '{0}', Counter: '{1}', Instance '{2}'.\nMessage:\n{3}",
							perfCounter?.CategoryName,
							perfCounter?.CounterName,
							perfCounter?.InstanceName,
							ex.Message);
					}
				});
		}
		private void InitDisposableMembers(string resolverName)
		{
			if (!PerformanceCounterCategory.Exists(PERF_COUNTER_CATEGORY_NAME))
				return;
			try
			{
				_counters = new[]
				{
					new PerformanceCounter(PERF_COUNTER_CATEGORY_NAME, DISTANCE_MATRIX_REQUESTS_COUNT,  resolverName, false),
					new PerformanceCounter(PERF_COUNTER_CATEGORY_NAME, DISTANCE_MATRIX_REQUESTS_ERRORS, resolverName, false),
					new PerformanceCounter(PERF_COUNTER_CATEGORY_NAME, DISTANCE_MATRIX_ELEMENTS_COUNT,  resolverName, false),
				};
				// Т.к. Dispose вызывается не всегда, сбрасываем значения при старте
				_counters
					.ToList()
					.ForEach(perfCounter =>
					{
						if (0 < perfCounter.RawValue)
							perfCounter.RawValue = 0;
					});
			}
			catch(Exception ex)
			{
				Trace.TraceError("Can not create performance counters for cache {0}.\nMessage:\n{1}", resolverName, ex.Message);
				Dispose();
			}
		}
		public void Increment(string name, long value)
		{
			_counters?.FirstOrDefault(c => c.CounterName.Equals(name))?.IncrementBy(value);
		}
		public void Increment(string name)
		{
			Increment(name, +1);
		}
		public void Decrement(string name)
		{
			Increment(name, -1);
		}
	}
}