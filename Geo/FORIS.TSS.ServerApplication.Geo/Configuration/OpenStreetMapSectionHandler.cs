﻿using System.Configuration;

namespace FORIS.TSS.ServerApplication.Geo.Configuration
{
	/// <summary> Секция для конфигурации поставщика карт OpenStreetMap </summary>
	public class OpenStreetMapSectionHandler : ConfigurationSection
	{
		private const string EnabledKey = "Enabled";
		/// <summary> Управляет состоянием: включено или выключено </summary>
		[ConfigurationProperty(EnabledKey, IsRequired = true)]
		public bool Enabled
		{
			get { return (bool)this[EnabledKey]; }
			set { this[EnabledKey] = value; }
		}

		private const string DataBaseKey = "DataBase";
		/// <summary> Ключ на экземпляр БД </summary>
		[ConfigurationProperty(DataBaseKey, IsRequired = true)]
		public string DataBase
		{
			get { return (string)this[DataBaseKey]; }
			set { this[DataBaseKey] = value; }
		}
	}
}