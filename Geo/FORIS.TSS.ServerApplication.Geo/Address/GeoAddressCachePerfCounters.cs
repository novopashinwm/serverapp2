﻿using System;
using System.Diagnostics;
using System.Linq;

namespace FORIS.TSS.ServerApplication.Geo.Address
{
	public sealed class GeoAddressCachePerfCounters : IDisposable
	{
		public const string PERF_COUNTER_CATEGORY_NAME  = "Ufin Geo Cache Two Levels";
		public const string PERF_COUNTER_CATEGORY_HELP  = "Ufin Geo server two levels cache performance counters";
		/// Level0 Уровень кэша запросов
		public const string CACHE_LEVEL0_HITS           = "Cache level0 Hits";
		/// Level1 Уровень кэша запросов
		public const string CACHE_LEVEL1_HITS           = "Cache level1 Hits";
		public const string CACHE_LEVEL1_HIT_RATIO      = "Cache level1 Hit Ratio";
		public const string CACHE_LEVEL1_HIT_RATIO_BASE = "Cache level1 Hit Ratio Base";
		public const string CACHE_LEVEL1_MISSES         = "Cache level1 Misses";
		/// Level2 Уровень кэша запросов
		public const string CACHE_LEVEL2_HITS           = "Cache level2 Hits";
		public const string CACHE_LEVEL2_HIT_RATIO      = "Cache level2 Hit Ratio";
		public const string CACHE_LEVEL2_HIT_RATIO_BASE = "Cache level2 Hit Ratio Base";
		public const string CACHE_LEVEL2_MISSES         = "Cache level2 Misses";
		/// Ссылки на сами счетчики
		private PerformanceCounter[] _counters;
	
		public static void CreateCategory()
		{
			if (PerformanceCounterCategory.Exists(PERF_COUNTER_CATEGORY_NAME))
				PerformanceCounterCategory.Delete(PERF_COUNTER_CATEGORY_NAME);

			PerformanceCounterCategory.Create(
				PERF_COUNTER_CATEGORY_NAME,
				PERF_COUNTER_CATEGORY_HELP,
				PerformanceCounterCategoryType.MultiInstance,
				new CounterCreationDataCollection(new[]
					{
						new CounterCreationData(CACHE_LEVEL0_HITS,           CACHE_LEVEL0_HITS,           PerformanceCounterType.NumberOfItems32),
						new CounterCreationData(CACHE_LEVEL1_HITS,           CACHE_LEVEL1_HITS,           PerformanceCounterType.NumberOfItems32),
						new CounterCreationData(CACHE_LEVEL1_HIT_RATIO,      CACHE_LEVEL1_HIT_RATIO,      PerformanceCounterType.RawFraction),
						new CounterCreationData(CACHE_LEVEL1_HIT_RATIO_BASE, CACHE_LEVEL1_HIT_RATIO_BASE, PerformanceCounterType.RawBase),
						new CounterCreationData(CACHE_LEVEL1_MISSES,         CACHE_LEVEL1_MISSES,         PerformanceCounterType.NumberOfItems32),
						new CounterCreationData(CACHE_LEVEL2_HITS,           CACHE_LEVEL2_HITS,           PerformanceCounterType.NumberOfItems32),
						new CounterCreationData(CACHE_LEVEL2_HIT_RATIO,      CACHE_LEVEL2_HIT_RATIO,      PerformanceCounterType.RawFraction),
						new CounterCreationData(CACHE_LEVEL2_HIT_RATIO_BASE, CACHE_LEVEL2_HIT_RATIO_BASE, PerformanceCounterType.RawBase),
						new CounterCreationData(CACHE_LEVEL2_MISSES,         CACHE_LEVEL2_MISSES,         PerformanceCounterType.NumberOfItems32),
					}));
		}

		public GeoAddressCachePerfCounters(string cacheName)
		{
			if (null == cacheName)
				throw new ArgumentNullException(nameof(cacheName));

			InitDisposableMembers(cacheName);
		}
		public void Dispose()
		{
			_counters
				?.ToList()
				?.ForEach(perfCounter =>
				{
					try
					{
						if (null != perfCounter)
						{
							if (0 < perfCounter.RawValue)
								perfCounter.RawValue = 0;
							perfCounter.Dispose();
						}
					}
					catch (Exception ex)
					{
						Trace.TraceError("Can not destroy performance counter Category: '{0}', Counter: '{1}', Instance '{2}'.\nMessage:\n{3}",
							perfCounter?.CategoryName,
							perfCounter?.CounterName,
							perfCounter?.InstanceName,
							ex.Message);
					}
				});
		}
		private void InitDisposableMembers(string cacheName)
		{
			if (!PerformanceCounterCategory.Exists(PERF_COUNTER_CATEGORY_NAME))
				return;
			try
			{
				_counters = new[]
				{
					new PerformanceCounter(PERF_COUNTER_CATEGORY_NAME, CACHE_LEVEL0_HITS,           cacheName, false),
					new PerformanceCounter(PERF_COUNTER_CATEGORY_NAME, CACHE_LEVEL1_HITS,           cacheName, false),
					new PerformanceCounter(PERF_COUNTER_CATEGORY_NAME, CACHE_LEVEL1_HIT_RATIO,      cacheName, false),
					new PerformanceCounter(PERF_COUNTER_CATEGORY_NAME, CACHE_LEVEL1_HIT_RATIO_BASE, cacheName, false),
					new PerformanceCounter(PERF_COUNTER_CATEGORY_NAME, CACHE_LEVEL1_MISSES,         cacheName, false),
					new PerformanceCounter(PERF_COUNTER_CATEGORY_NAME, CACHE_LEVEL2_HITS,           cacheName, false),
					new PerformanceCounter(PERF_COUNTER_CATEGORY_NAME, CACHE_LEVEL2_HIT_RATIO,      cacheName, false),
					new PerformanceCounter(PERF_COUNTER_CATEGORY_NAME, CACHE_LEVEL2_HIT_RATIO_BASE, cacheName, false),
					new PerformanceCounter(PERF_COUNTER_CATEGORY_NAME, CACHE_LEVEL2_MISSES,         cacheName, false),
				};
				// Т.к. Dispose вызывается не всегда, сбрасываем значения при старте
				_counters
					.ToList()
					.ForEach(perfCounter =>
					{
						if (0 < perfCounter.RawValue)
							perfCounter.RawValue = 0;
					});
			}
			catch(Exception ex)
			{
				Trace.TraceError("Can not create performance counters for cache {0}.\nMessage:\n{1}", cacheName, ex.Message);
				Dispose();
			}
		}
		private void Decrement(string name)
		{
			_counters?.FirstOrDefault(c => c.CounterName.Equals(name))?.Decrement();
		}
		private void Increment(string name)
		{
			_counters?.FirstOrDefault(c => c.CounterName.Equals(name))?.Increment();
		}
		private void IncrementBy(string name, long value)
		{
			_counters?.FirstOrDefault(c => c.CounterName.Equals(name))?.IncrementBy(value);
		}
		public void IncrementByLevel0Hits(long value = 1)
		{
			IncrementBy(CACHE_LEVEL0_HITS, value);
		}
		public void IncrementByLevel1Hits(long value = 1)
		{
			IncrementBy(CACHE_LEVEL1_HITS,           value);
			IncrementBy(CACHE_LEVEL1_HIT_RATIO,      value);
			IncrementBy(CACHE_LEVEL1_HIT_RATIO_BASE, value);
		}
		public void IncrementByLevel1Miss(long value = 1)
		{
			IncrementBy(CACHE_LEVEL1_MISSES,         value);
			IncrementBy(CACHE_LEVEL1_HIT_RATIO_BASE, value);
		}
		public void IncrementByLevel2Hits(long value = 1)
		{
			IncrementBy(CACHE_LEVEL2_HITS,           value);
			IncrementBy(CACHE_LEVEL2_HIT_RATIO,      value);
			IncrementBy(CACHE_LEVEL2_HIT_RATIO_BASE, value);
		}
		public void IncrementByLevel2Miss(long value = 1)
		{
			IncrementBy(CACHE_LEVEL2_MISSES,         value);
			IncrementBy(CACHE_LEVEL2_HIT_RATIO_BASE, value);
		}
	}
}