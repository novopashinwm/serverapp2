﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FORIS.TSS.Common.Caching;
using Interfaces.Geo;

namespace FORIS.TSS.ServerApplication.Geo.Address
{
	public class PrimaryCacheWrapper : ICachingAddressResolver
	{
		private readonly IBatchAddressResolver             _resolver;
		private readonly GeoPointTagCache<AddressResponse> _cache;
		private readonly GeoAddressCachePerfCounters       _perfs;

		public PrimaryCacheWrapper(IBatchAddressResolver resolver)
		{
			_resolver = resolver;
			_cache    = new GeoPointTagCache<AddressResponse>(GetAddressByPoint4Cache, 500000, false);
			_perfs    = new GeoAddressCachePerfCounters($"{_resolver.GetType().Name}");
		}
		private AddressResponse GetAddressByPoint4Cache(double lat, double lng, string language)
		{
			var addressRequest = new AddressRequest(id: default(int), lat: lat, lng: lng, false/*всегда false*/);
			var addressCulture = CultureInfo.GetCultureInfoByIetfLanguageTag(language);
			return _resolver.GetAddresses(new[] { addressRequest }, addressCulture)
				?.FirstOrDefault();
		}
		/// <summary> Идентификатор карты </summary>
		public Guid MapId        => _resolver.MapId;
		public bool IsConfigured => _resolver.IsConfigured;
		public IEnumerable<AddressResponse> GetAddressesByText(string searchText, string language)
		{
			throw new NotImplementedException();
		}
		public GetLatLngByAddressResult GetLatLngByAddress(string address, out double? lat, out double? lng)
		{
			throw new NotImplementedException();
		}
		public AddressResponse GetAddressByPoint(double lat, double lng, string language, bool cacheOnly = false)
		{
			var addressRequest = new AddressRequest(id: default(int), lat: lat, lng: lng, cacheOnly: cacheOnly);
			var addressCulture = CultureInfo.GetCultureInfoByIetfLanguageTag(language);
			return _resolver.GetAddresses(new[] { addressRequest }, addressCulture)
				?.FirstOrDefault();
		}
		/// <summary> Загружает список адресов из кэша, а которых нет в кэше resolver'a и помещает в кэш </summary>
		/// <param name="requests"></param>
		/// <param name="culture"></param>
		/// <returns></returns>
		public IEnumerable<AddressResponse> GetAddresses(IEnumerable<AddressRequest> requests, CultureInfo culture)
		{
			var responses         = new List<AddressResponse>();
			var notCachedRequests = new Dictionary<int, AddressRequest>();
			var language          = culture.TwoLetterISOLanguageName;
			var addressRequests   = requests.ToArray();
			// Полное количество запросов
			_perfs?.IncrementByLevel0Hits(addressRequests.LongLength);
			// Загружаем значения из кэша
			foreach (var addressRequest in addressRequests)
			{
				var addressResponse = default(AddressResponse);
				if (_cache.TryGetValue(lat: addressRequest.Lat, lng: addressRequest.Lng, tag: language, value: out addressResponse))
					responses.Add(new AddressResponse(
						id:       addressRequest.Id,
						lat:      addressResponse?.Lat ?? addressRequest.Lat,
						lng:      addressResponse?.Lng ?? addressRequest.Lng,
						address:  addressResponse?.Address,
						distance: addressResponse?.Distance ?? 0,
						size:     addressResponse?.Size     ?? 0));
				else
					notCachedRequests.Add(addressRequest.Id, addressRequest);
			}
			// Адреса полученные из кэш
			_perfs?.IncrementByLevel1Hits(responses.Count);
			_perfs?.IncrementByLevel1Miss(notCachedRequests.Count);
			// Загружаем значения, которых нет в кэше
			if (notCachedRequests.Any())
			{
				// Загружаем данные из провайдера
				var addressResponses = _resolver.GetAddresses(notCachedRequests.Values, culture).ToList();
				// Помещаем полученное в кэш
				addressResponses.ForEach(addressResponse =>
				{
					responses.Add(addressResponse);
					var addressRequest = notCachedRequests[addressResponse.Id];
					_cache[lat: addressResponse.Lat, lng: addressResponse.Lng, tag: language] = addressResponse;
					notCachedRequests.Remove(addressResponse.Id);
				});
				// Сколько было получено
				_perfs?.IncrementByLevel2Hits(addressResponses.Count);
				// Сколько осталось
				_perfs?.IncrementByLevel2Miss(notCachedRequests.Count);
			}
			return responses;
		}
	}
}