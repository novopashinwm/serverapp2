﻿using System.Globalization;
using System.Net;
using System.Text;

namespace FORIS.TSS.ServerApplication.Geo.Address.Yandex
{
	/// <summary>
	/// Клиент сервиса Яндекс:Геокодирование.
	/// </summary>
	public class YandexGeoLocatorClient
	{
		private const string ApiUri = "http://geocode-maps.yandex.ru/1.x/";
		private readonly WebClient _client;
		private readonly YandexGeoLocatorSettings _settings;
		public YandexGeoLocatorClient(YandexGeoLocatorSettings settings = null)
		{
			_settings = settings ?? YandexGeoLocatorSettings.LoadFromConfiguration();
			_client = new WebClient { Encoding = Encoding.UTF8 };
		}
		public YandexGeoLocatorResponse SendRequest(YandexGeoLocatorRequest request)
		{
			var uri            = GetRequestUri(request);
			var responseString = _client.DownloadString(uri);
			var response       = YandexGeoLocatorResponse.Parse(responseString);
			response.MetaData.RequestUri = uri;
			return response;
		}
		public YandexGeoLocatorResponse SendRequest(string address, CultureInfo culture = null)
		{
			var request = YandexGeoLocatorRequest.CreateRequest(address);
			if (_settings != null)
			{
				request.ApiKey       = _settings.ApiKey;
				request.Culture      = _settings.Culture;
				request.IsRestricted = _settings.IsRestricted;
				request.MaxCount     = _settings.MaxCount;
				request.Skip         = _settings.Skip;
				request.ZonePoint    = _settings.ZonePoint;
				request.ZoneRadius   = _settings.ZoneRadius;
			}

			if (culture != null)
			{
				request.Culture = culture;
			}

			return SendRequest(request);
		}
		public YandexGeoLocatorResponse SendRequest(double lat, double lng, CultureInfo culture = null)
		{
			var request = YandexGeoLocatorRequest.CreateRequest(lat, lng);
			if (_settings != null)
			{
				request.ApiKey               = _settings.ApiKey;
				request.Culture              = _settings.Culture;
				request.IsRestricted         = _settings.IsRestricted;
				request.MaxCount             = _settings.MaxCount;
				request.Skip                 = _settings.Skip;
				request.ZoneRadius           = _settings.ZoneRadius;
				request.AddressApproximation = _settings.AddressApproximation;
			}

			if (culture != null)
			{
				request.Culture = culture;
			}

			return SendRequest(request);
		}
		private string GetRequestUri(YandexGeoLocatorRequest request)
		{
			var uri = new StringBuilder(ApiUri);
			uri.Append("?");

			switch (request.Type)
			{
				case GeoLocatorRequestType.Direct:
					uri.AppendDirectRequestQueryString((YandexByAddressRequest)request);
					break;
				case GeoLocatorRequestType.Reverse:
					uri.AppendReverseRequestQueryString((YandexByLatLongRequest)request);
					break;
			}
			return uri.ToString();
		}
	}
}