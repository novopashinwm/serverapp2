﻿using System.Collections.Generic;
using System.Globalization;
using FORIS.TSS.BusinessLogic.Map;

namespace FORIS.TSS.ServerApplication.Geo.Address.Yandex
{
	/// <summary> Базовый объект запроса к Яндекс:Геокодированию </summary>
	public abstract class YandexGeoLocatorRequest
	{
		/// <summary> Список поддерживаемых языковых локалей </summary>
		public static IEnumerable<string> AcceptedLangs = new[] { "ru", "uk", "be", "en" };
		/// <summary> Ключ Api </summary>
		public string      ApiKey       { get; set; }
		/// <summary> Протяженность (в градусах) </summary>
		public GeoPoint    ZoneRadius   { get; set; }
		/// <summary> Поиск ограничен зоной поиска </summary>
		public bool        IsRestricted { get; set; }
		/// <summary> Максимальное количество результатов </summary>
		public int?        MaxCount     { get; set; }
		/// <summary> Количество пропущенных элементов с начала </summary>
		public int?        Skip         { get; set; }
		/// <summary> Культура для запроса данных </summary>
		public CultureInfo Culture      { get; set; }
		/// <summary> Тип запроса геокодирования </summary>
		public abstract GeoLocatorRequestType Type { get; }
		public static YandexByAddressRequest CreateRequest(string address)
		{
			return new YandexByAddressRequest(address);
		}
		public static YandexByLatLongRequest CreateRequest(double lat, double lng)
		{
			return new YandexByLatLongRequest(lat, lng);
		}
	}
	/// <summary> Запрос прямого геокодирования к сервису Яндекс:Геокодирование </summary>
	public class YandexByAddressRequest : YandexGeoLocatorRequest
	{
		private readonly string _address;
		/// <summary> Запрашиваемый адрес </summary>
		public string Address
		{
			get { return _address; }
		}
		/// <summary> Долготу и широту центра области (в градусах) </summary>
		public GeoPoint ZonePoint { get; set; }
		/// <summary> Тип запроса геокодирования </summary>
		public override GeoLocatorRequestType Type
		{
			get { return GeoLocatorRequestType.Direct; }
		}
		public YandexByAddressRequest(string address)
		{
			_address = address;
		}
	}
	/// <summary> Запрос обратного геокодирования к сервису Яндекс:Геокодирование </summary>
	public class YandexByLatLongRequest : YandexGeoLocatorRequest
	{
		private readonly GeoPoint _point;
		/// <summary> Широта </summary>
		public double Lat
		{
			get { return _point.Y; }
		}
		/// <summary> Долгота </summary>
		public double Lng
		{
			get { return _point.X; }
		}
		/// <summary> Вид топонима </summary>
		public AddressApproximation? AddressApproximation { get; set; }
		/// <summary> Тип запроса геокодирования </summary>
		public override GeoLocatorRequestType Type
		{
			get { return GeoLocatorRequestType.Reverse; }
		}
		public YandexByLatLongRequest(double lat, double lng)
		{
			_point = GeoPoint.CreateGeo(geoX: lng, geoY: lat);
		}
	}
}