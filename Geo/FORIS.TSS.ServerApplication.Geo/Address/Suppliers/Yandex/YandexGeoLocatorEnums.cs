﻿namespace FORIS.TSS.ServerApplication.Geo.Address.Yandex
{
	/// <summary> Значение до которого обрезается адрес распознавания объекта </summary>
	public enum AddressApproximation
	{
		/// <summary> Неизвестно </summary>
		Unknown,
		/// <summary> Дом </summary>
		House,
		/// <summary> Улица </summary>
		Street,
		/// <summary> Метро </summary>
		Metro,
		/// <summary> Район </summary>
		District,
		/// <summary> Населенный пункт </summary>
		Locality
	}
	/// <summary> Тип геокодирования </summary>
	public enum GeoLocatorRequestType
	{
		/// <summary> Прямое геокодирование </summary>
		Direct,
		/// <summary> Обратное геокодирование </summary>
		Reverse
	}
}