﻿using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using FORIS.TSS.BusinessLogic.Map;
using FORIS.TSS.Common;

namespace FORIS.TSS.ServerApplication.Geo.Address.Yandex
{
	public class YandexGeoLocatorSection : ConfigurationSection
	{
		[ConfigurationProperty(nameof(ApiKey))]
		public string ApiKey
		{
			get
			{
				return this[nameof(ApiKey)] as string;
			}
		}
		/// <summary> Протяженность (в градусах) </summary>
		[ConfigurationProperty(nameof(ZoneRadius))]
		public string ZoneRadiusString
		{
			get { return this[nameof(ZoneRadius)] as string; }
		}
		public GeoPoint ZoneRadius
		{
			get
			{
				GeoPoint zoneRadius = null;
				if (!string.IsNullOrEmpty(ZoneRadiusString))
				{
					var coords = ZoneRadiusString.Split(' ');
					var lng = double.Parse(coords[0], NumberStyles.Any, NumberHelper.FormatInfo); // X - долгота
					var lat = double.Parse(coords[1], NumberStyles.Any, NumberHelper.FormatInfo); // Y - широта
					zoneRadius = GeoPoint.CreateGeo(geoX: lng, geoY: lat);
				}

				return zoneRadius;
			}
		}
		/// <summary> Поиск ограничен зоной поиска </summary>
		[ConfigurationProperty(nameof(IsRestricted))]
		public bool? IsRestricted
		{
			get
			{
				return this[nameof(IsRestricted)] as bool?;
			}
		}
		/// <summary> Максимальное количество результатов </summary>
		[ConfigurationProperty(nameof(MaxCount))]
		public int? MaxCount
		{
			get
			{
				return this[nameof(MaxCount)] as int?;
			}
		}
		/// <summary> Количество пропущенных элементов с начала </summary>
		[ConfigurationProperty(nameof(Skip))]
		public int? Skip
		{
			get
			{
				var result = (int?) null;
				var value = this[nameof(Skip)] as string;
				if (!string.IsNullOrEmpty(value))
				{
					int parsed;
					if (int.TryParse(value, out parsed))
					{
						result = parsed;
					}
				}

				return result;
			}
		}
		[ConfigurationProperty(nameof(Culture))]
		public string CultureString
		{
			get
			{
				return this[nameof(Culture)] as string;
			}
		}
		/// <summary> Культура для запроса данных </summary>
		public CultureInfo Culture
		{
			get
			{
				CultureInfo culture = null;
				if (!string.IsNullOrEmpty(CultureString))
				{
					var parsedCulture = CultureInfo.GetCultures(CultureTypes.SpecificCultures).FirstOrDefault(item => item.Name == CultureString);
					culture = parsedCulture;
				}

				return culture;
			}
		}
		[ConfigurationProperty(nameof(ZonePoint))]
		public string ZonePointString
		{
			get { return this[nameof(ZonePoint)] as string; }
		}
		/// <summary> Долготу и широту центра области (в градусах) </summary>
		public GeoPoint ZonePoint
		{
			get
			{
				GeoPoint zonePoint = null;
				if (!string.IsNullOrEmpty(ZonePointString))
				{
					var coords = ZonePointString.Split(' ');
					var lng = double.Parse(coords[0], NumberStyles.Any, NumberHelper.FormatInfo); // X - долгота
					var lat = double.Parse(coords[1], NumberStyles.Any, NumberHelper.FormatInfo); // Y - широта
					zonePoint = GeoPoint.CreateGeo(geoX: lng, geoY: lat);
				}

				return zonePoint;
			}
		}
		[ConfigurationProperty(nameof(AddressApproximation))]
		public string AddressApproximationString
		{
			get
			{
				return this[nameof(AddressApproximation)] as string;
			}
		}
		/// <summary> Вид топонима </summary>
		public AddressApproximation? AddressApproximation
		{
			get
			{
				AddressApproximation? approximation = null;
				if (!string.IsNullOrEmpty(AddressApproximationString))
				{
					AddressApproximation parsed;
					if (Enum.TryParse(AddressApproximationString, true, out parsed))
					{
						approximation = parsed;
					}
				}
				return approximation;
			}
		}
		public void OverrideSettings(YandexGeoLocatorSettings settings)
		{
			settings.ApiKey               = ApiKey                        ?? settings.ApiKey;
			settings.AddressApproximation = AddressApproximation.HasValue ? AddressApproximation : settings.AddressApproximation;
			settings.Culture              = Culture                       ?? settings.Culture;
			settings.IsRestricted         = IsRestricted.HasValue         ? IsRestricted.Value   : settings.IsRestricted;
			settings.MaxCount             = MaxCount.HasValue             ? MaxCount             : settings.MaxCount;
			settings.Skip                 = Skip.HasValue                 ? Skip                 : settings.Skip;
			settings.ZonePoint            = ZonePoint                     ?? settings.ZonePoint;
			settings.ZoneRadius           = ZoneRadius                    ?? settings.ZoneRadius;
		}
	}
}