﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FORIS.TSS.BusinessLogic.Map;
using FORIS.TSS.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FORIS.TSS.ServerApplication.Geo.Address.Yandex
{
	/// <summary> Метаданные ответа геокодера </summary>
	public class YandexResponseMetaData
	{
		public string   RequestUri { get; internal set; }
		/// <summary> Строка запроса </summary>
		public string   Request    { get; internal set; }
		/// <summary> Количество найденных результатов </summary>
		public int      Found      { get; internal set; }
		/// <summary> Количество возвращенных результатов </summary>
		public int      Results    { get; internal set; }
		/// <summary> Точка геокодирования </summary>
		public GeoPoint Point      { get; internal set; }
	}
	/// <summary> Ответ сервиса геокодера </summary>
	public class YandexFeatureMember
	{
		/// <summary> Название </summary>
		public string               Name                  { get; internal set; }
		/// <summary> Описание </summary>
		public string               Description           { get; internal set; }
		/// <summary> Аппроксимация адреса </summary>
		public AddressApproximation Kind                  { get; internal set; }
		/// <summary> Распознанный адрес </summary>
		public string               Text                  { get; internal set; }
		/// <summary> Строка адреса без страны </summary>
		public string               AddressLine           { get; internal set; }
		/// <summary> Название страны </summary>
		public string               CountryName           { get; internal set; }
		/// <summary> Код страны </summary>
		public string               CountryNameCode       { get; internal set; }
		/// <summary> Округ </summary>
		public string               AdministrativeArea    { get; internal set; }
		/// <summary> Под округ </summary>
		public string               SubAdministrativeArea { get; internal set; }
		/// <summary> Местность </summary>
		public string               Locality              { get; internal set; }
		/// <summary> Проезд </summary>
		public string               Thoroughfare          { get; internal set; }
		/// <summary> Помещение </summary>
		public string               Premise               { get; internal set; }
		/// <summary> Левый верхний угол </summary>
		public GeoPoint             LowerCorner           { get; internal set; }
		/// <summary> Правый верхний угол </summary>
		public GeoPoint             UpperCorner           { get; internal set; }
		/// <summary> Geo позиция адреса </summary>
		public GeoPoint             Point                 { get; internal set; }
	}
	/// <summary> Ответ сервиса на запрос прямого геокодирования </summary>
	public class YandexGeoLocatorResponse
	{
		private readonly string _response;
		public YandexGeoLocatorResponse(string response)
		{
			_response = response;
		}
		/// <summary> Мета данные ответа </summary>
		public YandexResponseMetaData MetaData { get; internal set; }
		/// <summary> Данные геокодирования </summary>
		public IEnumerable<YandexFeatureMember> FeatureMembers { get; internal set; }
		/// <summary> Строка ответа </summary>
		public string Response
		{
			get { return _response; }
		}
		/// <summary> Парсит объект json, ответа </summary>
		/// <param name="responseString"></param>
		/// <returns></returns>
		public static YandexGeoLocatorResponse Parse(string responseString)
		{
			var response = new YandexGeoLocatorResponse(responseString)
			{
				MetaData       = new YandexResponseMetaData(),
				FeatureMembers = Enumerable.Empty<YandexFeatureMember>()
			};

			var jsonObject = (JObject)JsonConvert.DeserializeObject(responseString);
			var jsonResponse = jsonObject["response"] as JObject;
			if (jsonResponse == null) throw new ArgumentException("response");

			var getObjectCollection = jsonResponse["GeoObjectCollection"] as JObject;
			if (getObjectCollection == null) throw new ArgumentException("GeoObjectCollection");

			var metaDataProperty = getObjectCollection["metaDataProperty"] as JObject;
			if (metaDataProperty != null)
			{
				var responseMetaData = metaDataProperty["GeocoderResponseMetaData"] as JObject;
				if (responseMetaData != null)
				{
					var metadata = response.MetaData;
					metadata.Request = responseMetaData["request"] != null
						? responseMetaData["request"].ToString()
						: string.Empty;
					metadata.Found = responseMetaData["found"] != null
						? int.Parse(responseMetaData["found"].ToString())
						: default(int);
					metadata.Results = responseMetaData["results"] != null
						? int.Parse(responseMetaData["results"].ToString())
						: default(int);

					var point = responseMetaData["Point"] as JObject;
					if (point != null)
					{
						var coords = point["pos"].ToString().Split(' ');
						var lng = double.Parse(coords[0], NumberStyles.Any, NumberHelper.FormatInfo); // X - долгота
						var lat = double.Parse(coords[1], NumberStyles.Any, NumberHelper.FormatInfo); // Y - широта
						metadata.Point = GeoPoint.CreateGeo(geoX: lng, geoY: lat);
					}
				}
			}
			else
			{
				throw new ArgumentException("metaDataProperty");
			}

			var features = getObjectCollection["featureMember"] as JArray;
			if (features != null)
			{
				var featureMembers = new YandexFeatureMember[features.Count];
				var index = 0;
				foreach (JObject feature in features)
				{
					var geoObject = feature["GeoObject"] as JObject;
					if (geoObject == null) continue;

					var featureMember = new YandexFeatureMember
					{
						Name = geoObject["name"] != null ? geoObject["name"].ToString() : string.Empty,
						Description =
							geoObject["description"] != null ? geoObject["description"].ToString() : string.Empty
					};

					var boundedBy = geoObject["boundedBy"] as JObject;
					if (boundedBy != null)
					{
						var envelope = boundedBy["Envelope"] as JObject;
						if (envelope != null)
						{
							if (envelope["lowerCorner"] != null)
							{
								//долгота широта
								var coords = envelope["lowerCorner"].ToString().Split(' ');
								var lng = double.Parse(coords[0], NumberStyles.Any, NumberHelper.FormatInfo); // X - долгота
								var lat = double.Parse(coords[1], NumberStyles.Any, NumberHelper.FormatInfo); // Y - широта
								featureMember.LowerCorner = GeoPoint.CreateGeo(geoX: lng, geoY: lat);
							}

							if (envelope["upperCorner"] != null)
							{
								//долгота широта
								var coords = envelope["upperCorner"].ToString().Split(' ');
								var lng = double.Parse(coords[0], NumberStyles.Any, NumberHelper.FormatInfo); // X - долгота
								var lat = double.Parse(coords[1], NumberStyles.Any, NumberHelper.FormatInfo); // Y - широта
								featureMember.UpperCorner = GeoPoint.CreateGeo(geoX: lng, geoY: lat);
							}
						}
					}

					var point = geoObject["Point"] as JObject;
					if (point != null)
					{
						if (point["pos"] != null)
						{
							//долгота широта
							var coords = point["pos"].ToString().Split(' ');
							var lng = double.Parse(coords[0], NumberStyles.Any, NumberHelper.FormatInfo); // X - долгота
							var lat = double.Parse(coords[1], NumberStyles.Any, NumberHelper.FormatInfo); // Y - широта
							featureMember.Point = GeoPoint.CreateGeo(geoX: lng, geoY: lat);
						}
					}

					var featureMetaDataProperty = geoObject["metaDataProperty"] as JObject;
					if (featureMetaDataProperty != null)
					{
						var featureMetaData = featureMetaDataProperty["GeocoderMetaData"] as JObject;
						if (featureMetaData != null)
						{
							if (featureMetaData["kind"] != null)
							{
								AddressApproximation kind;
								featureMember.Kind = Enum.TryParse(featureMetaData["kind"].ToString(), true, out kind)
									? kind
									: AddressApproximation.Unknown;
							}

							featureMember.Text = featureMetaData["text"] != null
								? featureMetaData["text"].ToString()
								: string.Empty;
							//precision
							var addressDetails = featureMetaData["AddressDetails"] as JObject;
							if (addressDetails != null)
							{
								var country = addressDetails["Country"] as JObject;
								if (country != null)
								{
									featureMember.AddressLine = country["AddressLine"] != null
										? country["AddressLine"].ToString()
										: string.Empty;
									featureMember.CountryName = country["CountryName"] != null
										? country["CountryName"].ToString()
										: string.Empty;
									featureMember.CountryNameCode = country["CountryNameCode"] != null
										? country["CountryNameCode"].ToString()
										: string.Empty;
									var administrativeArea = country["AdministrativeArea"] as JObject;
									if (administrativeArea != null)
									{
										featureMember.AdministrativeArea =
											administrativeArea["AdministrativeAreaName"] != null
												? administrativeArea["AdministrativeAreaName"].ToString()
												: string.Empty;
										var subAdministrativeArea =
											administrativeArea["SubAdministrativeArea"] as JObject;
										if (subAdministrativeArea != null)
										{
											featureMember.SubAdministrativeArea =
												subAdministrativeArea["SubAdministrativeAreaName"] != null
													? subAdministrativeArea["SubAdministrativeAreaName"].ToString()
													: string.Empty;
											var locality = subAdministrativeArea["Locality"] as JObject;
											if (locality != null)
											{
												featureMember.Locality = locality["LocalityName"] != null
													? locality["LocalityName"].ToString()
													: string.Empty;
												var thoroughfare = locality["Thoroughfare"] as JObject;
												if (thoroughfare != null)
												{
													featureMember.Thoroughfare = thoroughfare["ThoroughfareName"] !=
																				 null
														? thoroughfare["ThoroughfareName"].ToString()
														: string.Empty;
													var premise = thoroughfare["Premise"] as JObject;
													if (premise != null)
													{
														featureMember.Premise = premise["PremiseNumber"] != null
															? premise["PremiseNumber"].ToString()
															: string.Empty;
													}
												}
											}
										}
									}
								}
							}
						}
					}

					featureMembers[index] = featureMember;
					index++;
				}

				response.FeatureMembers = featureMembers;
			}

			return response;
		}
	}
}