﻿using System.Configuration;
using System.Globalization;
using FORIS.TSS.BusinessLogic.Map;

namespace FORIS.TSS.ServerApplication.Geo.Address.Yandex
{
	/// <summary> Настройки клиента сервиса Яндекс:Геолокатор </summary>
	public class YandexGeoLocatorSettings
	{
		public string                ApiKey               { get; set; }
		/// <summary> Протяженность (в градусах) </summary>
		public GeoPoint              ZoneRadius           { get; set; }
		/// <summary> Поиск ограничен зоной поиска </summary>
		public bool                  IsRestricted         { get; set; }
		/// <summary> Максимальное количество результатов </summary>
		public int?                  MaxCount             { get; set; }
		/// <summary> Количество пропущенных элементов с начала </summary>
		public int?                  Skip                 { get; set; }
		/// <summary> Культура для запроса данных </summary>
		public CultureInfo           Culture              { get; set; }
		/// <summary> Долготу и широту центра области (в градусах) </summary>
		public GeoPoint              ZonePoint            { get; set; }
		/// <summary> Вид топонима </summary>
		public AddressApproximation? AddressApproximation { get; set; }
		/// <summary> Конструктор с настройками по умолчанию </summary>
		public YandexGeoLocatorSettings()
		{
			Culture = CultureInfo.CurrentCulture;
		}
		/// <summary> Создает настройки на основе файла конфигурации </summary>
		/// <returns></returns>
		public static YandexGeoLocatorSettings LoadFromConfiguration()
		{
			var settings = new YandexGeoLocatorSettings();
			var section = ConfigurationManager.GetSection("YandexGeoLocator") as YandexGeoLocatorSection;
			if (section != null)
				section.OverrideSettings(settings);
			return settings;
		}
	}
}