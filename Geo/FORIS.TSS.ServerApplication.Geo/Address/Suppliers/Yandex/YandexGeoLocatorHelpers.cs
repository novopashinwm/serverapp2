﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;

namespace FORIS.TSS.ServerApplication.Geo.Address.Yandex
{
	/// <summary> Класс хелперов для сервиса Яндекс:Геокодирование </summary>
	static class YandexGeoLocatorHelpers
	{
		public static void AppendDirectRequestQueryString(this StringBuilder sb, YandexByAddressRequest request)
		{
			sb.Append(string.Format(CultureInfo.CurrentCulture, "geocode={0}", request.Address));
			if (request.ZonePoint != null && request.IsRestricted)
			{
				sb.Append(string.Format(CultureInfo.InvariantCulture, "&ll={0},{1}", request.ZonePoint.X, request.ZonePoint.Y));
			}

			sb.AppendRequestQueryString(request);
		}
		public static void AppendReverseRequestQueryString(this StringBuilder sb, YandexByLatLongRequest request)
		{
			sb.Append(string.Format(CultureInfo.InvariantCulture, "geocode={0},{1}", request.Lat, request.Lng));
			sb.Append("&sco=latlong");
			if (request.AddressApproximation.HasValue && request.AddressApproximation.Value != AddressApproximation.Unknown)
			{
				sb.Append(string.Format(CultureInfo.InvariantCulture, "&kind={0}", request.AddressApproximation.ToString().ToLower()));
			}

			sb.AppendRequestQueryString(request);
		}
		public static void AppendRequestQueryString(this StringBuilder sb, YandexGeoLocatorRequest request)
		{
			sb.Append("&format=json");
			if (request.MaxCount.HasValue)
			{
				sb.Append(string.Format(CultureInfo.InvariantCulture, "&results={0}", request.MaxCount));
			}

			if (request.Skip.HasValue)
			{
				sb.Append(string.Format(CultureInfo.InvariantCulture, "&skip={0}", request.Skip));
			}

			sb.Append(string.Format(CultureInfo.InvariantCulture, "&rspn={0}", request.IsRestricted ? 1 : 0));

			if (request.ZoneRadius != null && request.IsRestricted)
			{
				sb.Append(string.Format(CultureInfo.InvariantCulture, "&spn={0},{1}", request.ZoneRadius.X, request.ZoneRadius.Y));
			}

			if (request.Culture != null)
			{
				var lang = YandexGeoLocatorRequest.AcceptedLangs.FirstOrDefault(item => request.Culture.Name.StartsWith(item, StringComparison.InvariantCultureIgnoreCase)) ?? "en";
				sb.Append(string.Format(CultureInfo.InvariantCulture, "&lang={0}", lang));
			}
			if (!string.IsNullOrWhiteSpace(request.ApiKey))
				sb.Append($"&{nameof(request.ApiKey).ToLowerInvariant()}={request.ApiKey}");
		}
	}
}