﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;
using FORIS.TSS.ServerApplication.Geo.Address.Yandex;
using Interfaces.Geo;

namespace FORIS.TSS.ServerApplication.Geo.Address.Suppliers
{
	/// <summary> Источник адресов Яндекс: Геокодера </summary>
	public class YandexAddressResolver : ISingleAddressResolver
	{
		public static readonly Guid MapGuid = MapGuids.YandexMapGuid;

		private readonly YandexGeoLocatorSettings    _settings;

		private static GeocodingResolverPerfCounters _perfs;
		static YandexAddressResolver()
		{
			_perfs = new GeocodingResolverPerfCounters($"{typeof(YandexAddressResolver).Name}");
		}
		public YandexAddressResolver()
		{
			_settings = YandexGeoLocatorSettings.LoadFromConfiguration();
		}
		public Guid MapId
		{
			get { return MapGuid; }
		}
		public bool IsConfigured
		{
			get { return true; }
		}
		public GetLatLngByAddressResult GetLatLngByAddress(string address, out double? lat, out double? lng)
		{
			lat = GeoHelper.InvalidLatitude;
			lng = GeoHelper.InvalidLongitude;

			var result = GetLatLngByAddressResult.FailedGeocoderTooBusy;
			try
			{
				_perfs?.Increment(GeocodingResolverPerfCounters.DIRECT__GEOCODING_REQUESTS_COUNT);
				var client   = new YandexGeoLocatorClient(_settings);
				///////////////////////////////////////////////////////////////////////////////////////////////////////////
				var response = client.SendRequest(address);
				$"\nREQUEST:\n{response?.MetaData?.Request?.UnescapeDataString().Trim()}\nRESPONSE:\n{response?.Response?.Trim()}"
					.CallTraceInformation();
				///////////////////////////////////////////////////////////////////////////////////////////////////////////
				var responseCount = response.FeatureMembers?.Count() ?? 0;
				result = responseCount > 0
					? responseCount == 1
						? GetLatLngByAddressResult.SuccessSingleResult
						: GetLatLngByAddressResult.SuccessMultipleResult
					: GetLatLngByAddressResult.FailedAddressNotFound;
				var validResponse = response.FeatureMembers?.FirstOrDefault();
				if (validResponse != null)
				{
					lat = validResponse.Point.Y;
					lng = validResponse.Point.X;
				}
				else
					_perfs?.Increment(GeocodingResolverPerfCounters.DIRECT__GEOCODING_REQUESTS_MISSES);
			}
			catch (Exception ex)
			{
				_perfs?.Increment(GeocodingResolverPerfCounters.DIRECT__GEOCODING_REQUESTS_ERRORS);
				default(string)
					.WithException(ex, true)
					.CallTraceError();
			}
			return result;
		}
		public IEnumerable<AddressResponse> GetAddressesByText(string searchText, string language)
		{
			var result  = new List<AddressResponse>();
			try
			{
				_perfs?.Increment(GeocodingResolverPerfCounters.DIRECT__GEOCODING_REQUESTS_COUNT);
				var culture  = CultureInfo.GetCultureInfoByIetfLanguageTag(language);
				var client   = new YandexGeoLocatorClient(_settings);
				///////////////////////////////////////////////////////////////////////////////////////////////////////////
				var response = client.SendRequest(searchText, culture);
				$"\nREQUEST:\n{response?.MetaData?.RequestUri?.UnescapeDataString().Trim()}\nRESPONSE:\n{response?.Response?.Trim()}"
					.CallTraceInformation();
				///////////////////////////////////////////////////////////////////////////////////////////////////////////
				response.FeatureMembers
					?.Select ((fm, idx) => new AddressResponse(
						id:       idx,
						lat:      fm.Point.Y,
						lng:      fm.Point.X,
						address:  fm.AddressLine,
						size:     (int)GeoHelper.DistanceBetweenLocations(
							lat1Degrees: fm.LowerCorner.Y,
							lng1Degrees: fm.LowerCorner.X,
							lat2Degrees: fm.UpperCorner.Y,
							lng2Degrees: fm.UpperCorner.X)))
					?.ForEach(result.Add);
			}
			catch (Exception ex)
			{
				_perfs?.Increment(GeocodingResolverPerfCounters.DIRECT__GEOCODING_REQUESTS_ERRORS);
				default(string)
					.WithException(ex, true)
					.CallTraceError();
			}
			if (0 == result.Count)
				_perfs?.Increment(GeocodingResolverPerfCounters.DIRECT__GEOCODING_REQUESTS_MISSES);
			return result;
		}
		public AddressResponse GetAddressByPoint(double lat, double lng, string language)
		{
			var result = default(AddressResponse);
			try
			{
				_perfs?.Increment(GeocodingResolverPerfCounters.REVERSE_GEOCODING_REQUESTS_COUNT);
				var culture  = CultureInfo.GetCultureInfoByIetfLanguageTag(language);
				var client   = new YandexGeoLocatorClient(_settings);
				///////////////////////////////////////////////////////////////////////////////////////////////////////////
				var response = client.SendRequest(lat, lng, culture);
				$"\nREQUEST:\n{response?.MetaData?.RequestUri?.UnescapeDataString().Trim()}\nRESPONSE:\n{response?.Response?.Trim()}"
					.CallTraceInformation();
				///////////////////////////////////////////////////////////////////////////////////////////////////////////
				result = response.FeatureMembers
					?.Select(fm => new AddressResponse(
						id:       default(int),
						lat:      fm.Point.Y,
						lng:      fm.Point.X,
						address:  fm.AddressLine,
						distance: (int)GeoHelper.DistanceBetweenLocations(
							lat1Degrees: lat,
							lng1Degrees: lng,
							lat2Degrees: fm.Point.Y,
							lng2Degrees: fm.Point.X),
						size: (int)GeoHelper.DistanceBetweenLocations(
							lat1Degrees: fm.LowerCorner.Y,
							lng1Degrees: fm.LowerCorner.X,
							lat2Degrees: fm.UpperCorner.Y,
							lng2Degrees: fm.UpperCorner.X)))
					?.OrderBy(ar => ar.Distance)
					?.FirstOrDefault();
			}
			catch (Exception ex)
			{
				_perfs?.Increment(GeocodingResolverPerfCounters.REVERSE_GEOCODING_REQUESTS_ERRORS);
				default(string)
					.WithException(ex, true)
					.CallTraceError();
			}
			if (string.IsNullOrWhiteSpace(result?.Address))
				_perfs?.Increment(GeocodingResolverPerfCounters.REVERSE_GEOCODING_REQUESTS_ERRORS);
			return result;
		}
	}
}