﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;
using GoogleApi;
using GoogleApi.Entities.Common;
using GoogleApi.Entities.Common.Enums;
using GoogleApi.Entities.Common.Enums.Extensions;
using GoogleApi.Entities.Maps.Geocoding.Address.Request;
using GoogleApi.Entities.Maps.Geocoding.Common.Enums;
using GoogleApi.Entities.Maps.Geocoding.Location.Request;
using Interfaces.Geo;

namespace FORIS.TSS.ServerApplication.Geo.Address.Suppliers
{
	public class GoogleMapsAddressResolver : ISingleAddressResolver
	{
		public static readonly Guid MapGuid = MapGuids.GoogleMapGuid;

		private string                       _apiKey;
		private string                       _channel;
		private Dictionary<string, Language> _langs;

		private static GeocodingResolverPerfCounters _perfs;
		static GoogleMapsAddressResolver()
		{
			_perfs = new GeocodingResolverPerfCounters($"{typeof(GoogleMapsAddressResolver).Name}");
		}
		public GoogleMapsAddressResolver()
		{
			_apiKey  = this.GetAppSettingAsString("ApiKey");
			_channel = this.GetAppSettingAsString("Channel");
			_langs   = Enum.GetValues(typeof(Language))
				.OfType<Language>()
				.ToDictionary(k => k.ToCode());
		}
		public Guid MapId
		{
			get { return MapGuid; }
		}
		public bool IsConfigured
		{
			get
			{
				return !string.IsNullOrEmpty(_apiKey);
			}
		}
		public GetLatLngByAddressResult GetLatLngByAddress(string address, out double? lat, out double? lng)
		{
			lat = lng = null;
			try
			{
				_perfs?.Increment(GeocodingResolverPerfCounters.DIRECT__GEOCODING_REQUESTS_COUNT);
				var req = new AddressGeocodeRequest
				{
					Address = address,
					Key     = _apiKey,
					Channel = _channel,
				};
				///////////////////////////////////////////////////////////////////////////////////////////////////////////
				var res = GoogleMaps.AddressGeocode.Query(req);
				$"\nREQUEST:\n{res?.RawQueryString?.UnescapeDataString().Trim()}\nRESPONSE:\n{res?.RawJson?.Trim()}"
					.CallTraceInformation();
				///////////////////////////////////////////////////////////////////////////////////////////////////////////
				if (res?.Status == null)
					return GetLatLngByAddressResult.FailedGeocoderTooBusy;
				switch (res.Status)
				{
					case Status.Ok:
						var fstRes = res.Results.FirstOrDefault();
						if (fstRes == null)
							return GetLatLngByAddressResult.FailedAddressNotFound;
						lat = fstRes.Geometry?.Location?.Latitude;
						if (lat == null)
							return GetLatLngByAddressResult.FailedAddressNotFound;
						lng = fstRes.Geometry?.Location?.Longitude;
						if (lng == null)
							return GetLatLngByAddressResult.FailedAddressNotFound;
						return 1 == res.Results.Count()
							? GetLatLngByAddressResult.SuccessSingleResult
							: GetLatLngByAddressResult.SuccessMultipleResult;
					case Status.NotFound:
					case Status.ZeroResults:
						_perfs?.Increment(GeocodingResolverPerfCounters.DIRECT__GEOCODING_REQUESTS_MISSES);
						return GetLatLngByAddressResult.FailedAddressNotFound;
					case Status.OverQueryLimit:
						_perfs?.Increment(GeocodingResolverPerfCounters.DIRECT__GEOCODING_REQUESTS_MISSES);
						return GetLatLngByAddressResult.FailedGeocoderTooBusy;
					case Status.RequestDenied:
					case Status.InvalidRequest:
					case Status.MaxElementsExceeded:
					case Status.MaxWaypointsExceeded:
					case Status.UnknownError:
					case Status.HttpError:
					case Status.InvalidKey:
					default:
						_perfs?.Increment(GeocodingResolverPerfCounters.DIRECT__GEOCODING_REQUESTS_ERRORS);
						return GetLatLngByAddressResult.UnknownError;
				}
			}
			catch (Exception ex)
			{
				_perfs?.Increment(GeocodingResolverPerfCounters.DIRECT__GEOCODING_REQUESTS_ERRORS);
				default(string)
					.WithException(ex, true)
					.CallTraceError();
			}
			return GetLatLngByAddressResult.UnknownError;
		}
		public IEnumerable<AddressResponse> GetAddressesByText(string searchText, string language)
		{
			var result  = new List<AddressResponse>();
			try
			{
				_perfs?.Increment(GeocodingResolverPerfCounters.DIRECT__GEOCODING_REQUESTS_COUNT);
				var req = new AddressGeocodeRequest
				{
					Address  = searchText,
					Key      = _apiKey,
					Channel  = _channel,
					Language = _langs.GetValueOrDefault(language, Language.English),
				};
				///////////////////////////////////////////////////////////////////////////////////////////////////////////
				var res = GoogleMaps.AddressGeocode.Query(req);
				$"\nREQUEST:\n{res?.RawQueryString?.UnescapeDataString().Trim()}\nRESPONSE:\n{res?.RawJson?.Trim()}"
					.CallTraceInformation();
				///////////////////////////////////////////////////////////////////////////////////////////////////////////
				res
					?.Results
					?.Where(rlt => !string.IsNullOrWhiteSpace(rlt.FormattedAddress) && rlt.Geometry != null)
					?.Select((rlt, idx) => new AddressResponse(
						id:       idx,
						lat:      rlt.Geometry.Location.Latitude,
						lng:      rlt.Geometry.Location.Longitude,
						address:  rlt.FormattedAddress,
						size:     (int)GeoHelper.DistanceBetweenLocations(
								lat1Degrees: rlt?.Geometry?.Bounds?.SouthWest?.Latitude  ?? (rlt?.Geometry?.ViewPort?.SouthWest?.Latitude  ?? 0),
								lng1Degrees: rlt?.Geometry?.Bounds?.SouthWest?.Longitude ?? (rlt?.Geometry?.ViewPort?.SouthWest?.Longitude ?? 0),
								lat2Degrees: rlt?.Geometry?.Bounds?.NorthEast?.Latitude  ?? (rlt?.Geometry?.ViewPort?.NorthEast?.Latitude  ?? 0),
								lng2Degrees: rlt?.Geometry?.Bounds?.NorthEast?.Longitude ?? (rlt?.Geometry?.ViewPort?.NorthEast?.Longitude ?? 0))))
					?.ForEach(result.Add);
			}
			catch (Exception ex)
			{
				_perfs?.Increment(GeocodingResolverPerfCounters.DIRECT__GEOCODING_REQUESTS_ERRORS);
				default(string)
					.WithException(ex, true)
					.CallTraceError();
			}
			if (0 == result.Count)
				_perfs?.Increment(GeocodingResolverPerfCounters.DIRECT__GEOCODING_REQUESTS_MISSES);
			return result;
		}
		public AddressResponse GetAddressByPoint(double lat, double lng, string language)
		{
			var result = default(AddressResponse);
			try
			{
				_perfs?.Increment(GeocodingResolverPerfCounters.REVERSE_GEOCODING_REQUESTS_COUNT);
				var req = new LocationGeocodeRequest
				{
					Location = new Location(latitude: lat, longitude: lng),
					Key      = _apiKey,
					Channel  = _channel,
					Language = _langs.GetValueOrDefault(language, Language.English),
				};
				///////////////////////////////////////////////////////////////////////////////////////////////////////////
				var res = GoogleMaps.LocationGeocode.Query(req);
				$"\nREQUEST:\n{res?.RawQueryString?.UnescapeDataString().Trim()}\nRESPONSE:\n{res?.RawJson?.Trim()}"
					.CallTraceInformation();
				///////////////////////////////////////////////////////////////////////////////////////////////////////////
				var providerResults = res
					?.Results
					?.Where(rlt => !string.IsNullOrWhiteSpace(rlt.FormattedAddress) && rlt.Geometry != null)
					?.Select((rlt, idx) => new
					{
						Type = rlt.Geometry.LocationType,
						Addr = new AddressResponse(
							id:       idx,
							lat:      rlt.Geometry.Location.Latitude,
							lng:      rlt.Geometry.Location.Longitude,
							address:  CropAddress(rlt.FormattedAddress),
							distance: (int)GeoHelper.DistanceBetweenLocations(
								lat1Degrees: req.Location.Latitude,
								lng1Degrees: req.Location.Longitude,
								lat2Degrees: rlt.Geometry.Location.Latitude,
								lng2Degrees: rlt.Geometry.Location.Longitude),
							size:     (int)GeoHelper.DistanceBetweenLocations(
								lat1Degrees: rlt?.Geometry?.Bounds?.SouthWest?.Latitude  ?? (rlt?.Geometry?.ViewPort?.SouthWest?.Latitude  ?? 0),
								lng1Degrees: rlt?.Geometry?.Bounds?.SouthWest?.Longitude ?? (rlt?.Geometry?.ViewPort?.SouthWest?.Longitude ?? 0),
								lat2Degrees: rlt?.Geometry?.Bounds?.NorthEast?.Latitude  ?? (rlt?.Geometry?.ViewPort?.NorthEast?.Latitude  ?? 0),
								lng2Degrees: rlt?.Geometry?.Bounds?.NorthEast?.Longitude ?? (rlt?.Geometry?.ViewPort?.NorthEast?.Longitude ?? 0)))
					});
				///////////////////////////////////////////////////////////////////////////////////////////////////////////
				/// Сортируем результаты по расстоянию от необходимой точки
				var orederedByDstn = providerResults
					?.OrderBy(rlt => rlt.Addr.Distance);
				///////////////////////////////////////////////////////////////////////////////////////////////////////////
				/// Ближайший объект с типом позиции Rooftop
				result = orederedByDstn
					?.Where(rlt => rlt.Type == GeometryLocationType.Rooftop)
					?.FirstOrDefault()
					?.Addr;
				if (result != null)
					return result;
				///////////////////////////////////////////////////////////////////////////////////////////////////////////
				/// Ближайший объект с типом позиции Range_Interpolated
				result = orederedByDstn
					?.Where(rlt => rlt.Type == GeometryLocationType.Range_Interpolated)
					?.FirstOrDefault()
					?.Addr;
				if (result != null)
					return result;
				///////////////////////////////////////////////////////////////////////////////////////////////////////////
				/// Сортируем результаты по размеру диагонали и расстоянию и выбираем первый
				result = providerResults
					?.OrderBy(rlt => rlt.Addr.Size)
					?.ThenBy(rlt  => rlt.Addr.Distance)
					?.FirstOrDefault()
					?.Addr;
				///////////////////////////////////////////////////////////////////////////////////////////////////////////
			}
			catch (Exception ex)
			{
				_perfs?.Increment(GeocodingResolverPerfCounters.REVERSE_GEOCODING_REQUESTS_ERRORS);
				default(string)
					.WithException(ex, true)
					.CallTraceError();
			}
			if (string.IsNullOrWhiteSpace(result?.Address))
				_perfs?.Increment(GeocodingResolverPerfCounters.REVERSE_GEOCODING_REQUESTS_ERRORS);
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			return result;
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
		}
		/// <summary> Костыль для урезания строки </summary>
		internal static string CropAddress(string address)
		{
			int index;
			var addressParts = address.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
			var lastIsIndex = int.TryParse(addressParts.Last(), out index);
			var cropCount = lastIsIndex && addressParts.Length > 2 ? 2 : 1;
			return string.Join(",", addressParts, 0, addressParts.Length > 3 ? addressParts.Length - cropCount : addressParts.Length);
		}
	}
}