﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using FORIS.DataAccess;
using FORIS.TSS.ServerApplication.Geo.Configuration;
using Interfaces.Geo;

namespace FORIS.TSS.ServerApplication.Geo.Address.Suppliers
{
	public class OpenStreetMapAddressResolver : IBatchAddressResolver
	{
		public static readonly Guid MapGuid = MapGuids.OsmMapGuid;

		private readonly OpenStreetMapSectionHandler   _configuration;
		private readonly TssDatabase                   _database;
		private readonly ResourceManager               _resourceManager;

		public OpenStreetMapAddressResolver()
		{
			_configuration = (OpenStreetMapSectionHandler)ConfigurationManager.GetSection("OpenStreetMap");
			if (_configuration == null)
			{
				Trace.TraceWarning("OpenStreetMap configuration section not found");
				return;
			}

			_database = Globals.TssDatabaseManager[_configuration.DataBase];
			if (_database == null)
				Trace.TraceError("Database not found by key: " + _configuration.DataBase);
			_resourceManager = new ResourceManager("FORIS.TSS.ServerApplication.Geo.Address.OpenStreetMap", GetType().Assembly);
		}

		public Guid MapId
		{
			get { return MapGuid; }
		}

		public bool IsConfigured
		{
			get { return _configuration != null && _configuration.Enabled; }
		}
		public IEnumerable<AddressResponse> GetAddresses(IEnumerable<AddressRequest> requests, CultureInfo culture)
		{
			if (_database == null)
				return EmptyResult;

			if (requests == null)
				return EmptyResult;

			var result = new List<AddressResponse>();

			var addressRequests = requests.ToArray();
			var pointDataTable = new DataTable();
			pointDataTable.Columns.Add("Id",  typeof(int));
			pointDataTable.Columns.Add("Lat", typeof(double));
			pointDataTable.Columns.Add("Lng", typeof(double));

			foreach (var request in addressRequests)
				pointDataTable.Rows.Add(request.Id, request.Lat, request.Lng);

			if (pointDataTable.Rows.Count == 0) return result;
			var accurateAddresses = GetAccurateAddresses(pointDataTable).ToDictionary(p => p.Id);
			var administrativeAddresses = GetAdministrativeAddresses(pointDataTable);
			result.AddRange(Merge(addressRequests, culture, accurateAddresses, administrativeAddresses));
			return result;
		}
		private class AccurateAddress
		{
			public readonly int    Id;
			public readonly string Name;
			public readonly string City;
			public readonly string Street;
			public readonly string HouseNumber;
			public readonly int    Distance;

			public AccurateAddress(int id, string name, string city, string street, string houseNumber, int distance)
			{
				Id          = id;
				Name        = name;
				City        = city;
				Street      = street;
				HouseNumber = houseNumber;
				Distance    = distance;
			}
		}
		private class AdministrativeArea
		{
			public readonly int    Level;
			public readonly string Name;
			/// <summary>Площадь</summary>
			public double          Area; //TODO: убрать после поддержки в osm2mssql внутренних границ территорий

			public AdministrativeArea(int level, string name, double area)
			{
				Level = level;
				Name  = name;
				Area  = area;
			}
		}
		private static readonly AddressResponse[] EmptyResult = { };
		private IEnumerable<AddressResponse> Merge(
			IEnumerable<AddressRequest> addressRequests, CultureInfo culture,
			Dictionary<int, AccurateAddress> accurateAddresses,
			Dictionary<int, List<AdministrativeArea>> administrativeAddresses)
		{
			var localizedDistanceMeasure = _resourceManager.GetString("DistanceMeasure", culture);
			foreach (var addressRequest in addressRequests)
			{
				AccurateAddress accurateAddress;
				if (!accurateAddresses.TryGetValue(addressRequest.Id, out accurateAddress))
					accurateAddress = null;

				List<AdministrativeArea> administrativeAreas;
				if (!administrativeAddresses.TryGetValue(addressRequest.Id, out administrativeAreas))
					administrativeAreas = null;

				if (accurateAddress == null && administrativeAreas == null)
					continue;

				var sb = new StringBuilder(255);
				if (accurateAddress != null)
				{
					if (!string.IsNullOrWhiteSpace(accurateAddress.Street))
					{
						if (sb.Length != 0)
							sb.Append(' ');
						sb.Append(accurateAddress.Street.Trim());
					}

					if (!string.IsNullOrWhiteSpace(accurateAddress.HouseNumber))
					{
						if (sb.Length != 0)
							sb.Append(' ');
						sb.Append(accurateAddress.HouseNumber.Trim());
					}

					if (!string.IsNullOrWhiteSpace(accurateAddress.Name))
					{
						if (sb.Length != 0)
							sb.Append(' ');
						sb.Append('[');
						sb.Append(accurateAddress.Name.Trim());
						sb.Append(' ');
						sb.Append(']');
					}

					if ((administrativeAreas == null || administrativeAreas.Count == 0) &&
						!string.IsNullOrWhiteSpace(accurateAddress.City))
					{
						if (sb.Length != 0)
							sb.Append(", ");
						sb.Append(accurateAddress.City.Trim());
					}
				}

				if (administrativeAreas != null)
				{
					int? prevLevel = null;
					foreach (var area in administrativeAreas.OrderByDescending(a => a.Level).ThenBy(a => a.Area))
					{
						// Избавляемся от округов.
						if (area.Level == 5 || area.Level == 3)
							continue;
						if (string.IsNullOrWhiteSpace(area.Name))
							continue;
						if (area.Level == prevLevel)
							continue;
						prevLevel = area.Level;

						if (sb.Length != 0)
							sb.Append(", ");
						sb.Append(area.Name);
					}
				}

				var result = sb.ToString();
				if (accurateAddress != null && 20 <= accurateAddress.Distance && localizedDistanceMeasure != null)
					result = string.Format(localizedDistanceMeasure, result, accurateAddress.Distance);

				yield return new AddressResponse(
					id:       addressRequest.Id,
					lat:      addressRequest.Lat,
					lng:      addressRequest.Lng,
					address:  result,
					distance: accurateAddress?.Distance ?? 0);
			}
		}
		private Dictionary<int, List<AdministrativeArea>> GetAdministrativeAddresses(DataTable pointDataTable)
		{
			using (var sp = _database.Procedure("dbo.GetAdministrativeAreas"))
			using (new ConnectionOpener(sp))
			{
				sp.SetTableValuedParameter("@point", pointDataTable);
				using (var reader = sp.ExecuteReader())
				{
					var pointIdOrdinal = reader.GetOrdinal("pointId");
					var adminLevelOrdinal = reader.GetOrdinal("adminLevel");
					var nameOrdinal = reader.GetOrdinal("name");
					var areaOrdinal = reader.GetOrdinal("area");

					var result = new Dictionary<int, List<AdministrativeArea>>(pointDataTable.Rows.Count);
					while (reader.Read())
					{
						var pointId = reader.GetInt32(pointIdOrdinal);
						var levelString = reader.GetString(adminLevelOrdinal);
						int level;
						if (!int.TryParse(levelString, out level))
							continue;
						var name = reader.GetString(nameOrdinal);
						if (string.IsNullOrWhiteSpace(name))
							continue;
						var area = reader.GetDouble(areaOrdinal);


						List<AdministrativeArea> areas;
						if (!result.TryGetValue(pointId, out areas))
							result.Add(pointId, areas = new List<AdministrativeArea>());

						areas.Add(new AdministrativeArea(level, name, area));
					}

					return result;
				}
			}
		}
		private IEnumerable<AccurateAddress> GetAccurateAddresses(DataTable pointDataTable)
		{
			using (var sp = _database.Procedure("dbo.GetAddresses"))
			using (new ConnectionOpener(sp))
			{
				sp.SetTableValuedParameter("@point", pointDataTable);
				using (var reader = sp.ExecuteReader())
				{
					var pointIdOrdinal     = reader.GetOrdinal("pointId");
					var nameOrdinal        = reader.GetOrdinal("name");
					var cityOrdinal        = reader.GetOrdinal("city");
					var streetOrdinal      = reader.GetOrdinal("street");
					var houseNumberOrdinal = reader.GetOrdinal("houseNumber");
					var distanceOrdinal    = reader.GetOrdinal("distance");

					while (reader.Read())
					{
						if (reader.IsDBNull(distanceOrdinal))
							continue;

						yield return new AccurateAddress(
							reader.GetInt32(pointIdOrdinal),
							reader.IsDBNull(nameOrdinal) ? null : reader.GetString(nameOrdinal),
							reader.IsDBNull(cityOrdinal) ? null : reader.GetString(cityOrdinal),
							reader.IsDBNull(streetOrdinal) ? null : reader.GetString(streetOrdinal),
							reader.IsDBNull(houseNumberOrdinal) ? null : reader.GetString(houseNumberOrdinal),
							reader.GetInt32(distanceOrdinal));
					}
				}
			}
		}
	}
}