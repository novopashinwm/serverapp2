﻿using System;
using System.Diagnostics;
using System.Linq;

namespace FORIS.TSS.ServerApplication.Geo.Address
{
	public sealed class GeocodingResolverPerfCounters : IDisposable
	{
		public const string PERF_COUNTER_CATEGORY_NAME = "Ufin Geocoding";
		public const string PERF_COUNTER_CATEGORY_HELP = "Ufin Geocoding performance counters";
		#region Direct performance counters

		public const string DIRECT__GEOCODING_REQUESTS_COUNT  = "Direct geocoding requests count";
		public const string DIRECT__GEOCODING_REQUESTS_ERRORS = "Direct geocoding requests with errors";
		public const string DIRECT__GEOCODING_REQUESTS_MISSES = "Direct geocoding requests without result";

		#endregion Direct performance counters
		#region Reverse performance counters

		public const string REVERSE_GEOCODING_REQUESTS_COUNT  = "Reverse geocoding requests count";
		public const string REVERSE_GEOCODING_REQUESTS_ERRORS = "Reverse geocoding requests with errors";
		public const string REVERSE_GEOCODING_REQUESTS_MISSES = "Reverse geocoding requests without result";

		#endregion Reverse performance counters

		/// <summary> Ссылки на сами счетчики </summary>
		private PerformanceCounter[] _counters;

		public static void CreateCategory()
		{
			if (PerformanceCounterCategory.Exists(PERF_COUNTER_CATEGORY_NAME))
				PerformanceCounterCategory.Delete(PERF_COUNTER_CATEGORY_NAME);

			PerformanceCounterCategory.Create(
				PERF_COUNTER_CATEGORY_NAME,
				PERF_COUNTER_CATEGORY_HELP,
				PerformanceCounterCategoryType.MultiInstance,
				new CounterCreationDataCollection(new[]
				{
					#region Direct performance counters

					new CounterCreationData(DIRECT__GEOCODING_REQUESTS_COUNT,  DIRECT__GEOCODING_REQUESTS_COUNT,  PerformanceCounterType.NumberOfItems32),
					new CounterCreationData(DIRECT__GEOCODING_REQUESTS_ERRORS, DIRECT__GEOCODING_REQUESTS_ERRORS, PerformanceCounterType.NumberOfItems32),
					new CounterCreationData(DIRECT__GEOCODING_REQUESTS_MISSES, DIRECT__GEOCODING_REQUESTS_MISSES, PerformanceCounterType.NumberOfItems32),

					#endregion Direct performance counters
					#region Reverse performance counters

					new CounterCreationData(REVERSE_GEOCODING_REQUESTS_COUNT,  REVERSE_GEOCODING_REQUESTS_COUNT,  PerformanceCounterType.NumberOfItems32),
					new CounterCreationData(REVERSE_GEOCODING_REQUESTS_ERRORS, REVERSE_GEOCODING_REQUESTS_ERRORS, PerformanceCounterType.NumberOfItems32),
					new CounterCreationData(REVERSE_GEOCODING_REQUESTS_MISSES, REVERSE_GEOCODING_REQUESTS_MISSES, PerformanceCounterType.NumberOfItems32),

					#endregion Reverse performance counters
				}));
		}

		public GeocodingResolverPerfCounters(string resolverName)
		{
			if (null == resolverName)
				throw new ArgumentNullException(nameof(resolverName));

			InitDisposableMembers(resolverName);
		}
		public void Dispose()
		{
			_counters
				?.ToList()
				?.ForEach(perfCounter =>
				{
					try
					{
						if (null != perfCounter)
						{
							if (0 < perfCounter.RawValue)
								perfCounter.RawValue = 0;
							perfCounter.Dispose();
						}
					}
					catch (Exception ex)
					{
						Trace.TraceError("Can not destroy performance counter Category: '{0}', Counter: '{1}', Instance '{2}'.\nMessage:\n{3}",
							perfCounter?.CategoryName,
							perfCounter?.CounterName,
							perfCounter?.InstanceName,
							ex.Message);
					}
				});
		}
		private void InitDisposableMembers(string resolverName)
		{
			if (!PerformanceCounterCategory.Exists(PERF_COUNTER_CATEGORY_NAME))
				return;
			try
			{
				_counters = new[]
				{
					#region Direct performance counters

					new PerformanceCounter(PERF_COUNTER_CATEGORY_NAME, DIRECT__GEOCODING_REQUESTS_COUNT,  resolverName, false),
					new PerformanceCounter(PERF_COUNTER_CATEGORY_NAME, DIRECT__GEOCODING_REQUESTS_ERRORS, resolverName, false),
					new PerformanceCounter(PERF_COUNTER_CATEGORY_NAME, DIRECT__GEOCODING_REQUESTS_MISSES, resolverName, false),

					#endregion Direct performance counters
					#region Reverse performance counters

					new PerformanceCounter(PERF_COUNTER_CATEGORY_NAME, REVERSE_GEOCODING_REQUESTS_COUNT,  resolverName, false),
					new PerformanceCounter(PERF_COUNTER_CATEGORY_NAME, REVERSE_GEOCODING_REQUESTS_ERRORS, resolverName, false),
					new PerformanceCounter(PERF_COUNTER_CATEGORY_NAME, REVERSE_GEOCODING_REQUESTS_MISSES, resolverName, false),

					#endregion Reverse performance counters
				};
				// Т.к. Dispose вызывается не всегда, сбрасываем значения при старте
				_counters
					.ToList()
					.ForEach(perfCounter =>
					{
						if (0 < perfCounter.RawValue)
							perfCounter.RawValue = 0;
					});
			}
			catch(Exception ex)
			{
				Trace.TraceError("Can not create performance counters for cache {0}.\nMessage:\n{1}", resolverName, ex.Message);
				Dispose();
			}
		}
		public void IncrementBy(string name, long value)
		{
			_counters?.FirstOrDefault(c => c.CounterName.Equals(name))?.IncrementBy(value);
		}
		public void Increment(string name)
		{
			IncrementBy(name, +1);
		}
		public void Decrement(string name)
		{
			IncrementBy(name, -1);
		}
	}
}