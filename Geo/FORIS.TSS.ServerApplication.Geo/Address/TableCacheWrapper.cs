﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Caching;
using Interfaces.Geo;

namespace FORIS.TSS.ServerApplication.Geo.Address
{
	public class TableCacheWrapper : ICachingAddressResolver
	{
		private readonly ISingleAddressResolver            _resolver;
		private readonly GeoPointTagCache<AddressResponse> _cache;
		private readonly GeoAddressCachePerfCounters       _perfs;
		private readonly GeoServerDatabaseManager          _db;

		public TableCacheWrapper(ISingleAddressResolver resolver, GeoServerDatabaseManager db)
		{
			_resolver = resolver;
			_cache    = new GeoPointTagCache<AddressResponse>(GetAddressByPoint4Cache, 500000, true);
			_perfs    = new GeoAddressCachePerfCounters($"{_resolver.GetType().Name}");
			_db       = db;
		}
		private AddressResponse GetAddressByPoint4Cache(double lat, double lng, string language)
		{
			return _resolver.GetAddressByPoint(lat, lng, language);
		}
		/// <summary> Идентификатор карты </summary>
		public Guid MapId        => _resolver.MapId;
		public bool IsConfigured => _resolver.IsConfigured;
		public GetLatLngByAddressResult GetLatLngByAddress(string address, out double? lat, out double? lng)
		{
			lat = default(double?);
			lng = default(double?);
			return _resolver?.GetLatLngByAddress(address, out lat, out lng) ?? GetLatLngByAddressResult.UnknownError;
		}
		public IEnumerable<AddressResponse> GetAddressesByText(string searchText, string language)
		{
			return _resolver?.GetAddressesByText(searchText, language) ?? new List<AddressResponse>();
		}
		public AddressResponse GetAddressByPoint(double lat, double lng, string language, bool cacheOnly = false)
		{
			var addressRequest = new AddressRequest(id: default(int), lat: lat, lng: lng, cacheOnly: cacheOnly);
			var addressCulture = CultureInfo.GetCultureInfoByIetfLanguageTag(language);
			return GetAddresses(new[] { addressRequest }, addressCulture)
				?.FirstOrDefault();
		}
		/// <summary> Загружает список адресов из кэша, а которых нет в кэше resolver'a и помещает в кэш </summary>
		/// <param name="requests"></param>
		/// <param name="culture"></param>
		/// <returns></returns>
		public IEnumerable<AddressResponse> GetAddresses(IEnumerable<AddressRequest> requests, CultureInfo culture)
		{
			var responses         = new List<AddressResponse>();
			if (0 == (requests?.Count() ?? 0))
				return responses;
			var notCachedRequests = new Dictionary<int, AddressRequest>();
			var language          = culture.TwoLetterISOLanguageName;
			var addressRequests   = requests.ToArray();
			_perfs?.IncrementByLevel0Hits(addressRequests.LongLength);
			// Загружаем значения из кэша
			foreach (var addressRequest in addressRequests)
			{
				var addressResponse = default(AddressResponse);
				if (_cache.TryGetValue(lat: addressRequest.Lat, lng: addressRequest.Lng, tag: language, value: out addressResponse))
					responses.Add(new AddressResponse(
						id:       addressRequest.Id,
						lat:      addressResponse?.Lat ?? addressRequest.Lat,
						lng:      addressResponse?.Lng ?? addressRequest.Lng,
						address:  addressResponse?.Address,
						distance: addressResponse?.Distance ?? 0,
						size:     addressResponse?.Size     ?? 0));
				else
					notCachedRequests.Add(addressRequest.Id, addressRequest);
			}
			_perfs?.IncrementByLevel1Hits(responses.Count);
			_perfs?.IncrementByLevel1Miss(notCachedRequests.Count);
			// Загружаем значения, которых нет в кэше
			if (notCachedRequests.Any())
			{
				// Загружаем данные из таблицы
				var addressResponses = _db.FindAddresses(MapId, language, _cache.CellSizeMeters, notCachedRequests.Values.ToArray());
				// Помещаем полученное в кэш
				addressResponses?.ForEach(addressResponse =>
				{
					responses.Add(addressResponse);
					var addressRequest = notCachedRequests[addressResponse.Id];
					_cache[lat: addressResponse.Lat, lng: addressResponse.Lng, tag: language] = addressResponse;
					notCachedRequests.Remove(addressResponse.Id);
				});
				// Сколько было получено
				_perfs?.IncrementByLevel2Hits(addressResponses?.Count ?? 0);
			}
			// Загружаем данные через кэш (в случае отсутствия обращаемся к провайдеру, если повтор, то уже нет) и помещаем в таблицу
			if (notCachedRequests.Any())
			{
				foreach (var addressRequest in notCachedRequests.Values)
				{
					//////////////////////////////////////////
					var addressResponse = default(AddressResponse);
					// Повторно проверяем наличие в кэше, т.к. на предыдущей итерации уже могли добавить
					if (!_cache.TryGetValue(lat: addressRequest.Lat, lng: addressRequest.Lng, tag: language, value: out addressResponse))
					{
						// Если повторно нет в кэше, то загружаем с использованием провайдера, если в запросе не указано не обращаться
						addressResponse = !addressRequest.CacheOnly
							? _cache[lat: addressRequest.Lat, lng: addressRequest.Lng, tag: language]
							: default(AddressResponse);
						// Если удачно нашли, получили через провайдера, то сохраняем в БД
						if (!string.IsNullOrWhiteSpace(addressResponse?.Address))
						{
							var savingLat = addressResponse.Lat; // Если попали в точность сохраняем координаты ответа
							var savingLng = addressResponse.Lng; // Если попали в точность сохраняем координаты ответа
							// Если нужно меняем адрес и сохраняемые координаты
							if ((addressResponse?.Distance ?? 0) > _cache.CellSizeMeters)
							{
								savingLat = addressRequest.Lat; // Если не попали в точность сохраняем координаты запроса
								savingLng = addressRequest.Lng; // Если не попали в точность сохраняем координаты запроса
								addressResponse = new AddressResponse(
									addressResponse.Id,
									addressResponse.Lat,
									addressResponse.Lng,
									GeoHelper.GetInaccurateAddressString(addressRequest, addressResponse, language),
									addressResponse.Distance,
									addressResponse.Size);
								// Заменяем адрес и в кэше, т.к. выдаем пользователю уже измененный
								_cache[lat: addressRequest.Lat, lng: addressRequest.Lng, tag: language] = addressResponse;
							}
							_db.SaveAddress(
								mapId:    MapId,
								lat:      savingLat,  // Если не попали в точность сохраняем координаты запроса
								lng:      savingLng,  // Если не попали в точность сохраняем координаты запроса
								language: language,
								address:  addressResponse.Address,
								radius:   _cache.CellSizeMeters);
						}
						// Т.к. в кэше не было ни в табличном ни в памяти, добавляем пропуск
						_perfs?.IncrementByLevel2Miss(+1);
					}
					else
					{
						// Т.к. в итоге взяли из кэша, то убираем один пропуск
						_perfs?.IncrementByLevel1Miss(-1);
						// Т.к. в итоге взяли из кэша, то добавляем одно попадание
						_perfs?.IncrementByLevel1Hits(+1);
					}
					//////////////////////////////////////////
					// Если не удалось никак найти, то переходим к следующему запросу
					if (string.IsNullOrWhiteSpace(addressResponse?.Address))
						continue;
					//////////////////////////////////////////
					// Добавляем полученный ответ
					responses.Add(new AddressResponse(
						id:       addressRequest.Id,
						lat:      addressResponse?.Lat ?? addressRequest.Lat,
						lng:      addressResponse?.Lng ?? addressRequest.Lng,
						address:  addressResponse?.Address,
						distance: addressResponse?.Distance ?? 0,
						size:     addressResponse?.Size     ?? 0));
					//////////////////////////////////////////
				}
			}
			return responses;
		}
	}
}