﻿using System;
using System.Collections.Generic;
using System.Data;
using FORIS.TSS.Config;
using Interfaces.Geo;

namespace FORIS.TSS.ServerApplication.Geo.Address
{
	internal static class AddressHelper
	{
		internal static DataSet FillAddressColumnWrapper(
			Guid mapGuid,
			DataSet dataSet,
			string tableName,
			string lngColumnName,
			string latColumnName,
			string addressColumnName,
			Func<IEnumerable<AddressRequest>, IEnumerable<AddressResponse>> getAddresses)
		{
			if (dataSet == null)
				return null;

			var dataTable = dataSet.Tables[tableName];
			if (dataTable == null)
				return dataSet;

			var lngColumn = dataTable.Columns[lngColumnName];
			var latColumn = dataTable.Columns[latColumnName];

			if (dataTable.Rows.Count == 0)
				return dataSet;

			var requests = new List<AddressRequest>(dataTable.Rows.Count);

			for (int i = 0, count = dataTable.Rows.Count; i != count; ++i)
			{
				var row = dataTable.Rows[i];
				if (row.IsNull(lngColumn) || row.IsNull(latColumn))
					continue;

				requests.Add(
					new AddressRequest(
						i,
						Convert.ToDouble(row[latColumn]),
						Convert.ToDouble(row[lngColumn])));
			}

			if (requests.Count == 0)
				return dataSet;

			var addresses = getAddresses(requests);
			var addressColumn = dataTable.Columns[addressColumnName];
			foreach (var address in addresses)
			{
				dataTable.Rows[address.Id][addressColumn] = CleanupAddress(address.Address, mapGuid);
			}

			return dataSet;
		}
		internal static string CleanupAddress(string preciseAddress, Guid? mapGuid)
		{
			if (string.IsNullOrWhiteSpace(preciseAddress))
				return preciseAddress;

			preciseAddress = preciseAddress.Replace("город ", "");
			preciseAddress = preciseAddress.Replace("муниципальный ", "");
			preciseAddress = preciseAddress.Replace("городское поселение", @"г/п");
			preciseAddress = preciseAddress.Replace("сельское поселение", @"с/п");

			return preciseAddress;
		}
		internal static Guid? DefaultMapGuid
		{
			get
			{
				var defaultMapGuidRes = default(Guid?);
				var defaultMapGuidVal = default(Guid);
				var defaultMapGuidTxt = GlobalsConfig.AppSettings[nameof(DefaultMapGuid)];
				if (!string.IsNullOrWhiteSpace(defaultMapGuidTxt) && Guid.TryParse(defaultMapGuidTxt, out defaultMapGuidVal))
					defaultMapGuidRes = defaultMapGuidVal;
				return defaultMapGuidRes;
			}
		}
	}
}