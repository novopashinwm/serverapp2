﻿using System.ComponentModel;
using FORIS.TSS.Common;

namespace FORIS.TSS.ServerApplication.Geo
{
	public class GeoServerDispatcher
		: ServerDispatcher<GeoServer>
	{
		#region Constructor & 

		public GeoServerDispatcher()
		{
		}
		public GeoServerDispatcher(IContainer container)
			: this()
		{
			container.Add(this);
		}

		#endregion // Constructor &

		private readonly GeoServerAmbassadorCollection ambassadors =
			new GeoServerAmbassadorCollection();

		protected override AmbassadorCollection<IServerItem<GeoServer>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public new GeoServerAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}
	}

	public class GeoServerAmbassadorCollection
		: ServerAmbassadorCollection<GeoServer>
	{
		public new GeoServerAmbassador this[int index]
		{
			get { return (GeoServerAmbassador)base[index]; }
		}
	}

	public class GeoServerAmbassador
		: ServerAmbassador<GeoServer>
	{
	}
}