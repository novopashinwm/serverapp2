﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using FORIS.DataAccess;
using FORIS.TSS.ServerApplication.Database;
using Interfaces.Geo;

namespace FORIS.TSS.ServerApplication.Geo
{
	public class GeoServerDatabaseManager : DatabaseManager
	{
		public GeoServerDatabaseManager()
			: base()
		{
		}
		public GeoServerDatabaseManager(IContainer container)
			: base(container)
		{
		}
		public void SaveAddress(Guid mapId, double lat, double lng, string language, string address, int radius)
		{
			using (var sp = Database.Procedure("AddAddress"))
			using (new ConnectionOpener(sp))
			{
				sp["@Lat"].Value      = lat;
				sp["@Lng"].Value      = lng;
				sp["@MapId"].Value    = mapId;
				sp["@Language"].Value = language;
				sp["@Value"].Value    = address;
				sp["@Radius"].Value   = radius;

				sp.ExecuteNonQuery();
			}
		}
		public List<AddressResponse> FindAddresses(Guid mapId, string language, int addressAccuracy, params AddressRequest[] addressRequests)
		{
			if (addressRequests == null || addressRequests.Length == 0)
				return null;

			var pointDataTable = new DataTable();
			pointDataTable.Columns.Add("Id",  typeof(int));
			pointDataTable.Columns.Add("Lat", typeof(double));
			pointDataTable.Columns.Add("Lng", typeof(double));

			foreach (var request in addressRequests)
				pointDataTable.Rows.Add(
					request.Id,
					request.Lat,
					request.Lng);

			using (var sp = Database.Procedure("GetAddresses"))
			using (new ConnectionOpener(sp))
			{
				sp.SetTableValuedParameter("@point", pointDataTable);
				sp["@language"].Value = language;
				sp["@MapId"].Value    = mapId;
				sp["@radius"].Value   = addressAccuracy;

				using (var reader = sp.ExecuteReader())
				{
					var ordinalId       = reader.GetOrdinal("id");
					var ordinalLat      = reader.GetOrdinal("lat");
					var ordinalLng      = reader.GetOrdinal("lng");
					var ordinalValue    = reader.GetOrdinal("value");
					var ordinalDistance = reader.GetOrdinal("distance");

					var result = new List<AddressResponse>();

					while (reader.Read())
					{
						if (reader.IsDBNull(ordinalDistance))
							continue;

						var id       = reader.GetInt32(ordinalId);
						var lat      = reader.GetDouble(ordinalLat);
						var lng      = reader.GetDouble(ordinalLng);
						var address  = reader.GetString(ordinalValue);
						var distance = reader.GetDouble(ordinalDistance);
						result.Add(new AddressResponse(
							id:       id,
							lat:      lat,
							lng:      lng,
							address:  address,
							distance: (int)distance));
					}

					return result;
				}
			}
		}
	}
}