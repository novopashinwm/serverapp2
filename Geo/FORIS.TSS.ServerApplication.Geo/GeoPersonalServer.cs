﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.BusinessLogic.Server;
using Interfaces.Geo;

namespace FORIS.TSS.ServerApplication.Geo
{
	public class GeoPersonalServer :
		PersonalServerBase<GeoServer>,
		IGeoPersonalServer
	{
		public override string ApplicationName
		{
			get { return "GEO"; }
		}
		protected override SetNewLoginResult SetNewLogin(string newLogin)
		{
			throw new NotImplementedException();
		}
		protected override bool CheckPassword( string password )
		{
			throw new NotImplementedException();
		}
		protected override SetNewPasswordResult SetPassword( string password, int idOperator )
		{
			throw new NotImplementedException();
		}

		#region IGeoPersonalServer Members
		public GetLatLngByAddressResult GetLatLngByAddress(string address, out double? lat, out double? lng, Guid? mapGuid = null)
		{
			return serverInstance.GetLatLngByAddress(address: address, lat: out lat, lng: out lng, mapGuid: mapGuid);
		}
		public IEnumerable<AddressResponse> GetAddressesByText(string searchText, string language, Guid? mapGuid = null)
		{
			return serverInstance.GetAddressesByText(searchText, language, mapGuid)?.ToArray() ?? new AddressResponse[0];
		}
		public string GetAddressByPoint(double lat, double lng, string language, Guid? mapGuid = null, bool cacheOnly = false)
		{
			return serverInstance.GetAddressByPoint(lat: lat, lng: lng, language: language, mapGuid: mapGuid);
		}
		public IEnumerable<AddressResponse> GetAddresses(IEnumerable<AddressRequest> requests, CultureInfo culture, Guid? mapGuid = null)
		{
			return serverInstance.GetAddresses(requests, culture, mapGuid)?.ToArray() ?? new AddressResponse[0];
		}
		public DataSet FillAddressColumn(DataSet dataSet, string tableName, string lngColumnName, string latColumnName, string addressColumnName, string language, Guid? mapGuid = null)
		{
			try
			{
				return serverInstance.FillAddressColumn(dataSet, tableName, lngColumnName, latColumnName, addressColumnName, language, mapGuid);
			}
			catch (Exception ex)
			{
				Trace.TraceError("Unable to fill address column: " + ex);
				return dataSet;
			}
		}
		#endregion IGeoPersonalServer Members

		public override TssPrincipalInfo GetPrincipal()
		{
			return
				new TssPrincipalInfo(
					SessionInfo.OperatorInfo.Login,
					"OPERATOR",
					SessionInfo.OperatorInfo.OperatorId,
					TssPrincipalType.Operator);
		}
		public ArrivalTimesToPoint GetArrivalTimesToPoint(LatLng destination, ArrivalTimeForObject[] origins, string language)
		{
			return serverInstance.GetArrivalTimesToPoint(destination, origins, language);
		}
	}
}