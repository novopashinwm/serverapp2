﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Globalization;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Config;
using FORIS.TSS.WorkplaceSharnier;
using Interfaces.Geo;

namespace FORIS.TSS.WorkplaceShadow.Geo
{
	public class GeoClient
		: ClientBase<IGeoPersonalServer>, IGeoAddressProvider
	{
		#region Default GeoClient
		private const           string    _defaultGeoServerName = "MapServer";
		private static readonly object    _defaultGeoClientLock = new object();
		private static volatile GeoClient _defaultGeoClient;
		public static GeoClient Default
		{
			get
			{
				// Первая проверка
				if (null == _defaultGeoClient)
					// Ожидание блокировки и блокировка
					lock (_defaultGeoClientLock)
						// Вторая проверка после блокировки (во время ожидания блокировки, могли уже присвоить)
						if (null == _defaultGeoClient)
							_defaultGeoClient = GetConnectedGeoClient();
				if (null == _defaultGeoClient)
					$"No connection to map server"
						.CallTraceWarning(2);
				return _defaultGeoClient;
			}
		}
		private static GeoClient GetConnectedGeoClient()
		{
			var geoClientUri = GlobalsConfig.AppSettings[_defaultGeoServerName];
			if (!string.IsNullOrWhiteSpace(geoClientUri))
			{
				try
				{
					// Создаем экземпляр
					var geoClientObj = new GeoClient();
					// Подключаемся
					geoClientObj.Connect(geoClientUri, false);
					// Подписываемся на событие после подключения
					geoClientObj.NeedReConnect += (o, e) =>
					{
						if (null != _defaultGeoClient)
							lock (_defaultGeoClientLock)
							{
								_defaultGeoClient?.Dispose();
								_defaultGeoClient = null;
							}
					};
					return geoClientObj;
				}
				catch (Exception ex)
				{
					$"Error connecting to map server '{geoClientUri}'"
						.WithException(ex)
						.CallTraceError();
				}
			}
			else
				$"{_defaultGeoServerName} not configured in configuration file"
					.CallTraceError();
			return default(GeoClient);
		}
		#endregion Default GeoClient
		public GeoClient()
			: base(new NameValueCollection(0))
		{
		}
		protected override void Start()
		{
		}
		protected override void Stop()
		{
		}
		public GetLatLngByAddressResult GetLatLngByAddress(string address, out double? lat, out double? lng, Guid? mapGuid = null)
		{
			lat = lng = null;
			return Session?.GetLatLngByAddress(address, out lat, out lng, mapGuid) ?? GetLatLngByAddressResult.UnknownError;
		}
		public IEnumerable<AddressResponse> GetAddressesByText(string searchText, string language, Guid? mapGuid = null)
		{
			return Session?.GetAddressesByText(searchText, language, mapGuid) ?? new AddressResponse[0];
		}
		public string GetAddressByPoint(double lat, double lng, string language, Guid? mapGuid = null, bool cacheOnly = false)
		{
			return Session?.GetAddressByPoint(lat: lat, lng: lng, language, mapGuid, cacheOnly);
		}
		public IEnumerable<AddressResponse> GetAddresses(IEnumerable<AddressRequest> requests, CultureInfo culture, Guid? mapGuid = null)
		{
			return Session?.GetAddresses(requests, culture, mapGuid) ?? new AddressResponse[0];
		}
		public DataSet FillAddressColumn(DataSet dataSet, string tableName, string lngColumnName, string latColumnName, string addressColumnName, string language, Guid? mapGuid = null)
		{
			return Session?.FillAddressColumn(dataSet, tableName, lngColumnName, latColumnName, addressColumnName, language, mapGuid) ?? dataSet;
		}
		protected override void ReConnect()
		{
			throw new NotImplementedException();
		}
		/// <summary> Получить оценочные времена прибытия в указанную точку объектов наблюдения </summary>
		/// <param name="destination"> Координаты точки назначения </param>
		/// <param name="origins"> Объекты наблюдения с координатами </param>
		/// <param name="language"> Язык пользователя </param>
		/// <param name="mapGuid"> Идентификатор карты </param>
		/// <returns></returns>
		public ArrivalTimesToPoint GetArrivalTimesToPoint(LatLng destination, ArrivalTimeForObject[] origins, string language)
		{
			return Session?.GetArrivalTimesToPoint(destination, origins, language)
				?? new ArrivalTimesToPoint { PointLatLng = destination };
		}
	}
}