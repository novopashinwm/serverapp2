﻿using System;
using System.Runtime.Remoting;
using System.ServiceProcess;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Config;

namespace FORIS.TSS.ServerLauncher.Service.Geo
{
	public partial class GeoService : ServiceBase
	{
		public GeoService()
		{
			InitializeComponent();
		}
		private AppDomain   _serverAppDomain  = null;
		private IEntryPoint _serverEntryPoint = null;
		protected override void OnStart(string[] args)
		{
			try
			{
				var nameOfNewDomain    = GlobalsConfig.AppSettings["nameOfNewDomain"];
				var nameOfAssemblyFile = GlobalsConfig.AppSettings["nameOfAssemblyFile"];
				var nameOfConfigFile   = GlobalsConfig.AppSettings["nameOfConfigFile"];
				var nameOfType         = GlobalsConfig.AppSettings["nameOfType"];
				var appBase            = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;

				_serverEntryPoint = (IEntryPoint)DomainHelper
					.CreateDomainAndGetEntryPoint(
						nameOfNewDomain,
						nameOfConfigFile,
						nameOfAssemblyFile,
						nameOfType,
						out _serverAppDomain,
						appBase);

				_serverEntryPoint?.Start(true, true, true);
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();
				throw;
			}
		}
		protected override void OnStop()
		{
			try
			{
				_serverEntryPoint?.Stop();
			}
			catch (RemotingException)
			{
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();
			}
			finally
			{
				_serverEntryPoint = null;
			}
			try
			{
				DomainHelper.UnloadDomainOrStopProcess(_serverAppDomain);
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();
			}
			finally
			{
				_serverAppDomain = null;
			}
		}
	}
}