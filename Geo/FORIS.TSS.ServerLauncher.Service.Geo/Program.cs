﻿using System;
using System.ServiceProcess;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.ServerLauncher.Application;

namespace FORIS.TSS.ServerLauncher.Service.Geo
{
	static class Program
	{
		static void Main()
		{
			try
			{
				var servicesToRun = new ServiceBase[]
				{
					new GeoService(),
				};
				if (Environment.UserInteractive)
					TrayIconApplicationContext.RunInteractive("Geo Server", servicesToRun);
				else
					ServiceBase.Run(servicesToRun);
			}
			catch (Exception ex)
			{
				$"Application start"
					.WithException(ex, true)
					.TraceError();
			}
		}
	}
}