﻿using System.Collections.Generic;
using System.Globalization;

namespace Interfaces.Geo
{
	public interface IBatchAddressResolver : IAddressResolver
	{
		/// <summary> Получение информации по адресам </summary>
		/// <param name="requests"> Список запросов на геокодирование </param>
		/// <param name="culture"> Культура для локализации </param>
		/// <returns></returns>
		IEnumerable<AddressResponse> GetAddresses(IEnumerable<AddressRequest> requests, CultureInfo culture);
	}
}