﻿using System;

namespace Interfaces.Geo
{
	[Serializable]
	public struct AddressRequest
	{
		public AddressRequest(int id, double lat, double lng, bool cacheOnly = false)
		{
			Id        = id;
			Lat       = lat;
			Lng       = lng;
			CacheOnly = cacheOnly;
		}
		/// <summary> Идентификатор запроса адреса </summary>
		public readonly int    Id;
		/// <summary> Широта точки для запроса адреса </summary>
		public readonly double Lat;
		/// <summary> Долгота точки для запроса адреса </summary>
		public readonly double Lng;
		public readonly bool   CacheOnly;
	}
}