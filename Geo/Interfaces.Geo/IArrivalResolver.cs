﻿namespace Interfaces.Geo
{
	/// <summary> Интерфейс клиента к различным сервисам расчета времени прибытия </summary>
	public interface IArrivalResolver
	{
		/// <summary> Сконфигурирован? </summary>
		bool IsConfigured { get; }
		/// <summary> Получить оценочные времена прибытия в указанную точку объектов наблюдения </summary>
		/// <param name="destination"> Координаты точки назначения </param>
		/// <param name="origins"> Объекты наблюдения с координатами </param>
		/// <param name="language"> Язык пользователя </param>
		/// <returns></returns>
		public ArrivalTimesToPoint GetArrivalTimesToPoint(LatLng destination, ArrivalTimeForObject[] origins, string language);
	}
}