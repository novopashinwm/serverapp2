﻿using System;
using System.Globalization;

namespace FORIS.TSS.Common
{
	public struct GeoPointTag : IEquatable<GeoPointTag>
	{
		public const    double EarthCircleInMeters = 40000000;
		public const    double MaxLat              = 360;
		public const    double MaxLng              = 180;
		public const    int    CellSize            = 500; // Для сокращения количества запросов адресов уменьшаем точность до радиуса 500м

		public readonly int    CellSizeInMeters;
		public readonly double Lat;
		public readonly double Lng;
		public readonly string Tag;
		public readonly int    LngCell;
		public readonly int    LatCell;

		public GeoPointTag(double lat, double lng, int cellSizeInMeters = CellSize)
			: this(lat, lng, string.Empty, cellSizeInMeters)
		{
		}
		public GeoPointTag(double lat, double lng, string tag, int cellSizeInMeters = CellSize)
		{
			Lat              = lat;
			Lng              = lng;
			Tag              = tag;
			CellSizeInMeters = cellSizeInMeters;
			LngCell          = GetLngCell(Lng, CellSizeInMeters);
			LatCell          = GetLatCell(Lat, CellSizeInMeters);
		}
		public static int GetLngCell(double lng, int cellSizeInMeters = CellSize)
		{
			var lngStep = MaxLng / ((EarthCircleInMeters / 2) / cellSizeInMeters);
			lng = FitAngle(lng, MaxLng);
			var cellx = (int)Math.Floor(lng / lngStep);
			return cellx;
		}
		public static int GetLatCell(double lat, int cellSizeInMeters = CellSize)
		{
			var latStep = MaxLat / (EarthCircleInMeters / cellSizeInMeters);
			lat = FitAngle(lat, MaxLat);
			var cellY = (int)Math.Floor(lat / latStep);
			return cellY;
		}
		public GeoPointTag AddMeters(double metersAlongLat, double metersAlongLng)
		{
			const double earthRadiusInMeters = 6378800.0;
			var radiansAlongLat = Math.PI * metersAlongLat / earthRadiusInMeters;

			var latRadians = Lat * Math.PI / 180.0;
			var lngRadians = Lng * Math.PI / 180.0;

			var littleCircleRadius = earthRadiusInMeters * Math.Cos(latRadians);
			var radiansAlongLng = Math.PI * metersAlongLng / littleCircleRadius;

			return new GeoPointTag(
				(latRadians + radiansAlongLat) * 180.0 / Math.PI,
				(lngRadians + radiansAlongLng) * 180.0 / Math.PI,
				Tag,
				CellSizeInMeters);
		}

		#region Overrides

		public override string ToString()
		{
			return string.Format(CultureInfo.CurrentCulture, "({0},{1}) - ({2}, {3})", Lat, Lng, LngCell, LatCell);
		}
		private static double FitAngle(double v, double d)
		{
			if (v < 0)
				v += d * (Math.Round(-v / d) + 1);
			if (v >= d)
				v -= d;
			return v;
		}
		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = CellSizeInMeters.GetHashCode();
				hashCode = (hashCode * 397) ^ (Tag != null ? Tag.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ LngCell;
				hashCode = (hashCode * 397) ^ LatCell;
				return hashCode;
			}
		}
		public bool Equals(GeoPointTag other)
		{
			return CellSizeInMeters.Equals(other.CellSizeInMeters)
				&& string.Equals(Tag, other.Tag)
				&& LngCell == other.LngCell
				&& LatCell == other.LatCell;
		}
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is GeoPointTag && Equals((GeoPointTag)obj);
		}
		public static bool operator ==(GeoPointTag left, GeoPointTag right)
		{
			return left.Equals(right);
		}
		public static bool operator !=(GeoPointTag left, GeoPointTag right)
		{
			return !left.Equals(right);
		}

		#endregion
	}
}