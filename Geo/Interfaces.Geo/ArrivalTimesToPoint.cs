﻿using System;
using System.Collections.Generic;

namespace Interfaces.Geo
{
	/// <summary>
	/// Класс времен прибытия различных объектов в указанную точку.
	/// </summary>
	[Serializable]
	public class ArrivalTimesToPoint
	{
		/// <summary> Координаты точки </summary>
		public LatLng                     PointLatLng  { get; set; }
		/// <summary> Адрес точки </summary>
		public string                     PointAddress { get; set; }
		/// <summary> Времена прибытия объектов в точку </summary>
		public List<ArrivalTimeForObject> ArrivalTimes { get; set; }
	}
}