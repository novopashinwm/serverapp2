﻿using System;

namespace Interfaces.Geo
{
	/// <summary> Интерфейс клиента к различным сервисам geo-кодирования </summary>
	public interface IAddressResolver
	{
		/// <summary> Идентификатор карты поставщика </summary>
		Guid MapId        { get; }
		/// <summary> Сконфигурирован? </summary>
		bool IsConfigured { get; }
	}
}