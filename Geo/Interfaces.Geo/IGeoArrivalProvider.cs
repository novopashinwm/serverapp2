﻿using System;

namespace Interfaces.Geo
{
	public interface IGeoArrivalProvider
	{
		/// <summary> Получить оценочные времена прибытия в указанную точку объектов наблюдения </summary>
		/// <param name="destination"> Координаты точки назначения </param>
		/// <param name="origins"> Объекты наблюдения с координатами </param>
		/// <param name="language"> Язык пользователя </param>
		public ArrivalTimesToPoint GetArrivalTimesToPoint(LatLng destination, ArrivalTimeForObject[] origins, string language);
	}
}