﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;

namespace Interfaces.Geo
{
	public interface IGeoAddressProvider
	{
		#region Find Address and Route
		/// <summary> Вычисляет широту и долготу по адресу </summary>
		/// <param name="address"> Адрес </param>
		/// <param name="lat"> Широта, если адрес найден </param>
		/// <param name="lng"> Долгота, если адрес найден </param>
		/// <param name="mapGuid"> Идентификатор карты </param>
		/// <remarks> Если адрес не найден на карте, устанавливает lat и lng в null </remarks>
		GetLatLngByAddressResult GetLatLngByAddress(string address, out double? lat, out double? lng, Guid? mapGuid = null);
		/// <summary> Получение адресов по тексту </summary>
		IEnumerable<AddressResponse> GetAddressesByText(string searchText, string language, Guid? mapGuid = null);
		/// <summary> Вычисляет ближайший к указанной координате адрес </summary>
		/// <param name="lat"> широта </param>
		/// <param name="lng"> долгота </param>
		/// <param name="language"> Язык, на котором возвращать адрес </param>
		/// <param name="mapGuid"> Guid карты </param>
		/// <param name="cacheOnly">Брать только из cache, не обращаясь к поставщику</param>
		/// <returns> адрес </returns>
		/// <remarks>
		/// Возможно, этот метод, опираясь на возможности 
		/// пользователя (права доступа к картам и данные о 
		/// территории обслуживания) мог бы сам выбирать 
		/// подходящую карту (файл) и по ней определять адрес
		/// </remarks>
		string GetAddressByPoint(double lat, double lng, string language, Guid? mapGuid = null, bool cacheOnly = false);
		/// <summary> Получение адресов по координатам </summary>
		IEnumerable<AddressResponse> GetAddresses(IEnumerable<AddressRequest> requests, CultureInfo culture, Guid? mapGuid = null);
		/// <summary> Заполнение колонки адрес в таблице </summary>
		/// <param name="mapGuid"> GUID карты для поиска адреса </param>
		/// <param name="dataSet"> Датасет для заполнения адресами </param>
		/// <param name="tableName"> Название таблицы в которой необходимо заполнить адрес </param>
		/// <param name="lngColumnName"> Название колонки долготы </param>
		/// <param name="latColumnName"> Название колонки широты </param>
		/// <param name="addressColumnName"> Название колонки адреса </param>
		/// <param name="language"> Язык, на котором возвращать адрес </param>
		/// <returns> Входящий датасет с заполненными адресами </returns>
		DataSet FillAddressColumn(DataSet dataSet, string tableName, string lngColumnName, string latColumnName, string addressColumnName, string language, Guid? mapGuid = null);
		#endregion Find Address and Route
	}
}