﻿using System.Collections.Generic;

namespace Interfaces.Geo
{
	public interface ISingleAddressResolver : IAddressResolver
	{
		/// <summary> Возвращает координаты по адресу </summary>
		/// <param name="address"> Адрес </param>
		/// <param name="lat"> Широта </param>
		/// <param name="lng"> Долгота </param>
		/// <returns></returns>
		GetLatLngByAddressResult GetLatLngByAddress(string address, out double? lat, out double? lng);
		/// <summary> Возвращает набор адресов по части адреса </summary>
		/// <param name="searchText"> Текст, часть адреса </param>
		/// <param name="language"> Язык результата </param>
		/// <returns></returns>
		IEnumerable<AddressResponse> GetAddressesByText(string searchText, string language);
		/// <summary> Возвращает адрес по координатам </summary>
		/// <param name="latitude"> Широта </param>
		/// <param name="longitude"> Долгота </param>
		/// <param name="language"> Язык </param>
		/// <returns> Адрес </returns>
		AddressResponse GetAddressByPoint(double lat, double lng, string language);
	}
}