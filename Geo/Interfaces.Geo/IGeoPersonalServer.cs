﻿using FORIS.TSS.BusinessLogic.Server;

namespace Interfaces.Geo
{
	public interface IGeoPersonalServer
		: IPersonalServer, IGeoAddressProvider, IGeoArrivalProvider
	{
	}
}