﻿using System;
using System.Globalization;

namespace Interfaces.Geo
{
	/// <summary> Класс представляющий точку с координатами </summary>
	[Serializable]
	public class LatLng
	{
		public decimal Lat;
		public decimal Lng;
		public override string ToString()
		{
			return Lat.ToString("#.000000#", CultureInfo.InvariantCulture) + "," + Lng.ToString("#.000000#", CultureInfo.InvariantCulture);
		}
	};
}