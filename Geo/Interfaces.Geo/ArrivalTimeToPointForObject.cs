﻿using System;

namespace Interfaces.Geo
{
	/// <summary> Класс получения ориентировочного времени прибытия объекта наблюдения </summary>
	[Serializable]
	public class ArrivalTimeForObject
	{
		public int    ObjectId                                 { get; set; }
		public LatLng ObjectLatLng                             { get; set; }
		public string ObjectAddress                            { get; set; }
		public int?   DistanceInMeters                         { get; set; }
		public int?   EstimatedArrivalTimeInSeconds            { get; set; }
		public int?   EstimatedArrivalTimeInSecondsWithTraffic { get; set; }
	}
}