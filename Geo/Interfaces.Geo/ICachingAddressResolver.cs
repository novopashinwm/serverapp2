﻿using System.Collections.Generic;
using System.Globalization;

namespace Interfaces.Geo
{
	public interface ICachingAddressResolver : IAddressResolver
	{
		/// <summary> Возвращает координаты по адресу </summary>
		/// <param name="address"> Адрес </param>
		/// <param name="lat"> Широта </param>
		/// <param name="lng"> Долгота </param>
		/// <returns></returns>
		GetLatLngByAddressResult GetLatLngByAddress(string address, out double? lat, out double? lng);
		/// <summary> Возвращает набор адресов по части адреса </summary>
		/// <param name="searchText"> Текст, часть адреса </param>
		/// <param name="language"> Язык результата </param>
		/// <returns></returns>
		IEnumerable<AddressResponse> GetAddressesByText(string searchText, string language);
		/// <summary> Возвращает адрес по координатам </summary>
		/// <param name="lat"> Широта </param>
		/// <param name="lng"> Долгота </param>
		/// <param name="language"> Язык </param>
		/// <param name="cacheOnly">Брать только из cache, не обращаясь к поставщику</param>
		/// <returns> Адрес </returns>
		public AddressResponse GetAddressByPoint(double lat, double lng, string language, bool cacheOnly = false);
		/// <summary> Получение информации по адресам </summary>
		/// <param name="requests"> Список запросов на геокодирование </param>
		/// <param name="culture"> Культура для локализации </param>
		/// <returns></returns>
		IEnumerable<AddressResponse> GetAddresses(IEnumerable<AddressRequest> requests, CultureInfo culture);
	}
}