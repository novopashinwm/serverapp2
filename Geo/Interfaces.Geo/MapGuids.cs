﻿using System;

namespace Interfaces.Geo
{
	public static class MapGuids
	{
		public static readonly Guid GoogleMapGuid = Guid.Empty;
		public static readonly Guid YandexMapGuid = new Guid("8EAD71AB-CDCD-42AF-8AF6-6A7B3FCBB68D");
		public static readonly Guid OsmMapGuid    = new Guid("054FDB02-EC91-4309-A0AA-DBFEC56E4EE4");
	}
}