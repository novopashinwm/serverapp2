﻿using System;

namespace Interfaces.Geo
{
	[Serializable]
	public enum GetLatLngByAddressResult
	{
		/// <summary> Адрес успешно распознан, обнаружен ровно 1 адрес </summary>
		SuccessSingleResult,
		/// <summary> Адрес успешно распознан, но обнаружен более одного адреса </summary>
		SuccessMultipleResult,
		/// <summary> Не удалось найти местоположение адреса </summary>
		FailedAddressNotFound,
		/// <summary> Служба распознавания адресов перегружена </summary>
		/// <remarks> Следует повторить запрос позже </remarks>
		FailedGeocoderTooBusy,
		/// <summary> Не распознанная ошибка </summary>
		UnknownError
	}
}