﻿using System;

namespace Interfaces.Geo
{
	[Serializable]
	public class AddressResponse
	{
		/// <summary> Идентификатор запроса адреса </summary>
		public int    Id       { get; private set; }
		/// <summary> Широта найденного адреса </summary>
		public double Lat      { get; private set; }
		/// <summary> Долгота найденного адреса </summary>
		public double Lng      { get; private set; }
		/// <summary> Найденный адрес </summary>
		public string Address  { get; private set; }
		/// <summary> Дистанция до запрашиваемых координат </summary>
		public int    Distance { get; private set; }
		/// <summary> Размер диагонали прямоугольника в который вписан объект </summary>
		public int    Size     { get; private set; }
		/// <summary> Конструктор </summary>
		/// <param name="id"> Идентификатор запроса адреса </param>
		/// <param name="lat"> Широта найденного адреса </param>
		/// <param name="lng"> Долгота найденного адреса </param>
		/// <param name="address"> Найденный адрес </param>
		/// <param name="distance"> Дистанция до запрашиваемых координат </param>
		/// <param name="size"> Размер диагонали прямоугольника в который вписан объект </param>
		public AddressResponse(int id, double lat, double lng, string address, int distance = 0, int size = 0)
		{
			Id       = id;
			Lat      = lat;
			Lng      = lng;
			Address  = address;
			Distance = distance;
			Size     = size;
		}
	}
}