using System;
using System.Runtime.Serialization;

namespace FORIS.TSS.Infrastructure.Authentication
{
	// ----------------------------------- ** -----------------------------------
	//
	//                   ServerStateReceivedUnexpectedMessageException 
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class ServerStateReceivedUnexpectedMessageException : SecuritySinkException
	{
		// construction/destruction
		public ServerStateReceivedUnexpectedMessageException(State serverState, Messages.Message receivedMessage, string expectedMessageName) : base()
		{
			this.stateName = serverState.GetType().Name;
			this.receivedMessageName = receivedMessage.GetType().Name;
			this.expectedMessageName = expectedMessageName;
		}
	
		protected ServerStateReceivedUnexpectedMessageException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	
		// public properties
		public override string Message
		{
			get
			{
				string msg = string.Format("Server state '{0}' received unexpected message '{1}' but expected '{2}'", new Object[] {this.stateName, this.receivedMessageName, this.expectedMessageName});
				return msg;
			}
		}

		public string ReceivedMessageName
		{
			get
			{
				return this.receivedMessageName;
			}
		}

		public string ExpectedMessageName
		{
			get
			{
				return this.expectedMessageName;
			}
		}



		// private data members
		string stateName;
		string receivedMessageName;
		string expectedMessageName;
	}

}
