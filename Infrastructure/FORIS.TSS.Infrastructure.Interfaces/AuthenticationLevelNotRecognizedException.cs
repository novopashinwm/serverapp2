using System;
using System.Runtime.Serialization;
using System.Collections;

namespace FORIS.TSS.Infrastructure.Authentication
{
	// ----------------------------------- ** -----------------------------------
	//
	//                     AuthenticationLevelNotRecognizedException 
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class AuthenticationLevelNotRecognizedException : SecuritySinkException
	{
		// construction/destruction
		public AuthenticationLevelNotRecognizedException(string authenticationLevelName) : base()
		{
			this.authenticationLevelName = authenticationLevelName;
		}
	
		protected AuthenticationLevelNotRecognizedException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}

	
		// public properties
		public override string Message
		{
			get
			{
				string msg = string.Format("Unknown authentication level name '{0}' in configuration file", new Object[] {this.authenticationLevelName});
				return msg;
			}
		}

		public string AuthenticationLevelName
		{
			get
			{
				return this.authenticationLevelName;
			}
		}



		// private data members
		string authenticationLevelName;
	}

}
