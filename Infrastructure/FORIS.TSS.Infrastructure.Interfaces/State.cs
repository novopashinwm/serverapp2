using System;

namespace FORIS.TSS.Infrastructure.Authentication
{
	// ----------------------------------- ** -----------------------------------
	//
	//                                    State
	//
	// ----------------------------------- ** -----------------------------------

	public abstract class State
	{
		/// <summary>
		/// Each state belongs to some state machine
		/// </summary>
		protected StateMachine stateMachine = null;

		/// <summary>
		/// Creates new state and sets it's state machine
		/// </summary>
		/// <param name="stateMachine"></param>
		public State(StateMachine stateMachine)
		{
			this.stateMachine = stateMachine;
		}


		/// <summary>
		/// List of valid next states
		/// </summary>
		private State[] nextStates = null;

		/// <summary>
		/// Initializes list of next states
		/// </summary>
		/// <param name="states"></param>
		public void SetTransitions(State[] states)
		{
			this.nextStates = states;

			for (int i = 0; i < states.Length; ++i)
			{
				if (states[i] == null)
				{
					throw new ApplicationException("Invalid state");
				}
			}
		}

		/// <summary>
		/// looks through each valid next state of this state 
		/// and checks to see if its type
		/// matches the state type passed to this method.
		/// </summary>
		/// <param name="stateType"></param>
		protected void SetNextState(System.Type stateType)
		{
			// reset the next state
			foreach (State validNextState in this.nextStates)
			{
				if ( validNextState.GetType() == stateType )
				{
					// if we have a match, then set the next state
					this.stateMachine.NextState = validNextState;
					return;
				}
			}

			this.stateMachine.NextState = null;
			throw new UnknownNextStateException(this, stateType);
		}
	}
}
