﻿using System;


namespace FORIS.TSS.BusinessLogic.Interfaces.Events
{
	[Serializable]
	public class EventMessage
	{
		public Guid HandlerID { get; }
		public EventArgs Args { get; }
		/// <summary> Штамп времени, когда возникло событие и было создано сообщение о нем </summary>
		public DateTime TimeStamp { get; }

		public EventMessage(Guid handlerID, EventArgs args)
		{
			TimeStamp = DateTime.Now;
			HandlerID = handlerID;
			Args      = args;
		}
	}
}