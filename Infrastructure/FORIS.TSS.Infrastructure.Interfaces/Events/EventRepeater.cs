﻿using System;
using System.Diagnostics;
using System.Reflection;
using FORIS.TSS.Common;
using FORIS.TSS.Infrastructure.Interfaces.EventTranslator;

namespace FORIS.TSS.BusinessLogic.Interfaces.Events
{
	public interface IRepeater
	{
		IWeakDelegate GenuineHandler { get; }
		MulticastDelegate Handler { get; }

		Type EventArgsType { get; }
	}

	public class EventRepeater<TEventArgs> : MarshalByRefObject, IRepeater
		where TEventArgs : EventArgs
	{
		#region Implement IRepeater

		IWeakDelegate IRepeater.GenuineHandler
		{
			get { return this.genuineHandler; }
		}

		MulticastDelegate IRepeater.Handler
		{
			get { return this.handler; }
		}

		Type IRepeater.EventArgsType
		{
			get { return this.GetType().GetGenericArguments()[0]; }
		}

		#endregion // Implement IRepeater

		private readonly WeakAnonymousDelegate<TEventArgs> genuineHandler;

		public WeakAnonymousDelegate<TEventArgs> GenuineHandler
		{
			get { return this.genuineHandler; }
		}

		private readonly AnonymousEventHandler<TEventArgs> handler;
		public AnonymousEventHandler<TEventArgs> Handler
		{
			get { return this.handler; }
		}

		private EventRepeater(AnonymousEventHandler<TEventArgs> genuineHandler)
		{
			this.genuineHandler =
				new WeakAnonymousDelegate<TEventArgs>(genuineHandler);

			/* Репитер держит делегат со ссылкой на объект, с методом 
			 * обработчиком события, в слабой ссылке, это позволяет 
			 * объекту, подписавшемуся на событие дистанцированного 
			 * объекта быть независимым от реестра репитеров, который 
			 * держит репитеры крепко.
			 */

			this.handler = new AnonymousEventHandler<TEventArgs>(this.Callback);
		}

		public override object InitializeLifetimeService()
		{
			return null;
		}

		public void Callback(TEventArgs e)
		{
			this.genuineHandler.Invoke(e);
		}

		/* Не будем переопределять InitializeLifeеimeService,
		 * а то еще вдруг на самом деле репитеры станут вечными
		 */

		/// <summary>
		/// Создает объект репитера.
		/// </summary>
		/// <param name="handler"></param>
		/// <returns> Непосредственно объект репитера. НЕ ДЕЛЕГАТ</returns>
		/// <remarks>
		/// Используем этот метод, так как его проще 
		/// получить рефлексией чем конструктор
		/// </remarks>
		public static EventRepeater<TEventArgs> Create(AnonymousEventHandler<TEventArgs> handler)
		{
			return new EventRepeater<TEventArgs>(handler);
		}
	}

	public interface IWeakDelegate
	{
		bool IsAlive { get; }

		void Invoke(object[] args);
	}

	public class WeakAnonymousDelegate<TEventArgs> : IWeakDelegate
		where TEventArgs : EventArgs
	{
		#region Implement IWeakHandler

		bool IWeakDelegate.IsAlive
		{
			get { return this.targetReference.IsAlive; }
		}

		void IWeakDelegate.Invoke(object[] args)
		{
			object Target = this.targetReference.Target;

			if (Target != null)
			{
				this.method.Invoke(Target, args);
			}
			else
			{
				Debug.WriteLine("((IWeakDelegate)WeakAnonymousEventHandler.Invoke(). TARGET DIED!!!", "CLIENT");
			}

		}

		#endregion // IWeakHandler

		private readonly WeakReference targetReference;
		private readonly MethodInfo method;
		private readonly Type targetType;

		public WeakAnonymousDelegate(AnonymousEventHandler<TEventArgs> genuineHandler)
		{
			this.targetType = genuineHandler.GetType();
			this.targetReference = new WeakReference(genuineHandler.Target);
			this.method = genuineHandler.Method;
		}

		public void Invoke(TEventArgs args)
		{
			object target = this.targetReference.Target;

			try
			{
				if (target != null)
				{
					this.method.Invoke(target, new object[] { args });
				}
				else
				{
					Trace.WriteLineIf(
						EventTranslatorSink.TraceSwitch.TraceWarning,
						string.Format(
							"{0}.Invoke(); Target {1}.{2}(); DIED!!!",
							this.GetType().Name,
							this.targetType.Name,
							this.method.Name
							),
						"CLIENT ET"
						);
				}
			}
			catch (Exception ex)
			{
				Trace.WriteLineIf(
					EventTranslatorSink.TraceSwitch.TraceError,
					string.Format(
						"{0}.Invoke(); Target {1}.{2}(); Exception: {3}",
						this.GetType().Name,
						this.targetType.Name,
						this.method.Name,
						ex.Message
						),
					"CLIENT ET"
					);

				Trace.WriteLine(ex.StackTrace);

				if (ex.InnerException != null)
				{
					Trace.WriteLineIf(
						EventTranslatorSink.TraceSwitch.TraceError,
						string.Format(
							"{0}.Invoke(); Target {1}.{2}(); InnerException: {3}",
							this.GetType().Name,
							this.targetType.Name,
							this.method.Name,
							ex.InnerException.Message
							),
						"CLIENT ET"
						);

					Trace.WriteLine(ex.InnerException.StackTrace);
				}
			}
		}

		public override bool Equals(object obj)
		{
			Delegate Delegate = (Delegate)obj;

			object Target = this.targetReference.Target;

			if (Target != null)
			{
				return
					object.Equals(Delegate.Target, Target) &&
					object.Equals(Delegate.Method, this.method);
			}

			return false;
		}

		public override int GetHashCode()
		{
			return (targetReference != null && targetReference.Target != null ? targetReference.Target.GetHashCode() : 0)
				   ^
				   (method != null ? method.GetHashCode() : 0);
		}

		public override string ToString()
		{
			object TargetHolder = this.targetReference.Target;

			if (TargetHolder != null)
			{
				return TargetHolder.GetType().Name + " " + this.method.Name;
			}
			else
				return "dead " + this.method.Name;
		}
	}

}