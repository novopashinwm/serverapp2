﻿namespace FORIS.TSS.BusinessLogic.Interfaces.Events
{
	public enum HandlerAction
	{
		None   = 0,
		Add    = 1,
		Remove = 2
	}
}