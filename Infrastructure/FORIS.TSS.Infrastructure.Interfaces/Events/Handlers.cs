﻿using System;
using System.Collections;
using System.Data;
using System.Diagnostics;

namespace FORIS.TSS.BusinessLogic
{
	[Serializable]
	public class TableEventArgs : EventArgs
	{
		public DataTable Table { get; }

		public TableEventArgs(DataTable Table)
		{
			this.Table = Table;
		}
	}

	public delegate void TableEventHandler(int Session, TableEventArgs e);

	public class TableEventRepeater : MarshalByRefObject
	{
		private TableEventHandler target;

		private TableEventRepeater(TableEventHandler target)
		{
			this.target += target;
		}

		public void Callback(int SessionID, TableEventArgs args)
		{
			target(SessionID, args);
		}

		public static TableEventHandler Create(TableEventHandler target)
		{
			TableEventRepeater repeater = new TableEventRepeater(target);
			return new TableEventHandler(repeater.Callback);
		}

		public override object InitializeLifetimeService()
		{
			return null;
		}
	}


	// TODO: Класс HandlerHash клиенту видеть совсем не нужно

	public class HandlerHash : IEnumerable
	{
		private Hashtable m_Hash = new Hashtable();

		public System.Delegate[] this[string Table]
		{
			get
			{
				if (m_Hash.ContainsKey(Table))
					return (m_Hash[Table] as MulticastDelegate).GetInvocationList();
				else
					return new System.Delegate[] { };
			}
		}

		public void Add(string[] Tables, TableEventHandler Handler)
		{
			foreach (string table in Tables)
			{
				if (m_Hash.ContainsKey(table))
				{
					m_Hash[table] = MulticastDelegate.Combine(m_Hash[table] as MulticastDelegate, Handler);
				}
				else
					m_Hash.Add(table, Handler);
			}
		}
		public void Remove(TableEventHandler Handler)
		{
			foreach (MulticastDelegate md in m_Hash.Values)
				MulticastDelegate.Remove(md, Handler);
		}

		public IEnumerator GetEnumerator()
		{
			return m_Hash.GetEnumerator();
		}

		public void Invoke(int SessionID, DataSet DataSet)
		{
			// TODO: Порядок таблиц
			/* О порядке таблиц можно сказать, что он такой же как 
			 * в хп GetDataFor...
			 * Будем считать, что нас это пока вполне устраивает
			 */
			foreach (DataTable table in DataSet.Tables)
			{
				foreach (TableEventHandler handler in this[table.TableName])
				{
					handler.BeginInvoke(SessionID, new TableEventArgs(table), new AsyncCallback(CallbackTableEvent), handler);
				}
			}
		}

		private void CallbackTableEvent(IAsyncResult ar)
		{
			TableEventHandler handler = (TableEventHandler)ar.AsyncState;
			try
			{
				handler.EndInvoke(ar);
			}
			catch (Exception e)
			{
				Trace.TraceError(e.ToString());
			}
		}
	}
}