using System;
using System.IO;
using System.Threading;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Serialization;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization.Formatters.Binary;

namespace FORIS.TSS.Infrastructure.Authentication
{
	// ----------------------------------- ** -----------------------------------
	//
	//             ServerAsyncProcessResponseExpectedToSendMessageException 
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class ServerAsyncProcessResponseExpectedToSendMessageException : SecuritySinkException
	{
		// construction/destruction
		public ServerAsyncProcessResponseExpectedToSendMessageException() : base()
		{
		}
	
		protected ServerAsyncProcessResponseExpectedToSendMessageException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}

		// public properties
		public override string Message
		{
			get
			{
				string msg = "Server side AsyncProcessResponse() has transitioned the state machine but it is not ready to send a message back to the client as expected.";
				return msg;
			}
		}
	}
}
