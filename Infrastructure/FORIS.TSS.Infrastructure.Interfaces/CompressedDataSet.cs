using System;

namespace FORIS.TSS.Infrastructure.Compactor
{
	[Serializable]
	public class CompressedDataSet
	{
		/// <summary>
		/// Compressed array
		/// </summary>
		public byte[] array;
	}
}
