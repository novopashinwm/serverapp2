/*===================================================================

SUMMARY - 

This file contains the base class for the typed exceptions found in this sample.

=====================================================================*/

using System;
using System.Runtime.Serialization;

namespace FORIS.TSS.Infrastructure.Authentication
{
	// ----------------------------------- ** -----------------------------------
	//
	//                            SecuritySinkException 
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public abstract class SecuritySinkException : System.ApplicationException
	{
		// construction/destruction
		public SecuritySinkException() : base()
		{
		}
		protected SecuritySinkException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}

		public SecuritySinkException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}
