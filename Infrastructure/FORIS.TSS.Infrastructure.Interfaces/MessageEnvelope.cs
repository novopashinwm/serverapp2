using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace FORIS.TSS.Infrastructure.Authentication
{
	// ----------------------------------- ** -----------------------------------
	//
	//                                MessageEnvelope
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class MessageEnvelope
	{
		// ----------------------------------- ** -----------------------------------
		//
		//                            Construction/Destruction
		//
		// ----------------------------------- ** -----------------------------------

		public MessageEnvelope(Guid clientId, Messages.Message message)
		{
			this.clientId = clientId;
			this.message = message;
		}





		// ----------------------------------- ** -----------------------------------
		//
		//                              Public Properties
		//
		// ----------------------------------- ** -----------------------------------

		public Guid ClientId
		{
			get
			{
				return this.clientId;
			}
		}

		public Messages.Message Message
		{
			get
			{
				return this.message;
			}
		}





		// ----------------------------------- ** -----------------------------------
		//
		//                               Public Methods
		//
		// ----------------------------------- ** -----------------------------------

		public static string MessageEnvelopeToString(MessageEnvelope messageEnvelope)
		{
			// serialize the message into a stream
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			MemoryStream memoryStream = new MemoryStream();
			binaryFormatter.Serialize(memoryStream, messageEnvelope);


			// convert the stream into a string
			return Convert.ToBase64String(memoryStream.ToArray());
		}

		public static MessageEnvelope StringToMessageEnvelope(string serializedMessageEnvelope)
		{
			// convert the string into a stream
			MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(serializedMessageEnvelope));


			// deserialize the stream into a message
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			object objForDebug = binaryFormatter.Deserialize(memoryStream);
			return (MessageEnvelope)objForDebug;
		}





		// ----------------------------------- ** -----------------------------------
		//
		//                             Private Data Members
		//
		// ----------------------------------- ** -----------------------------------

		private Guid clientId;
		private Messages.Message message;
	}
}
