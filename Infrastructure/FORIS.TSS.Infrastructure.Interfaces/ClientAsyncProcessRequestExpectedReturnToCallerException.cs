using System;
using System.IO;
using System.Threading;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Serialization;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization.Formatters.Binary;

namespace FORIS.TSS.Infrastructure.Authentication
{
	// ----------------------------------- ** -----------------------------------
	//
	//            ClientAsyncProcessRequestExpectedReturnToCallerException 
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class ClientAsyncProcessRequestExpectedReturnToCallerException : SecuritySinkException
	{
		// construction/destruction
		public ClientAsyncProcessRequestExpectedReturnToCallerException() : base()
		{
		}
	
		protected ClientAsyncProcessRequestExpectedReturnToCallerException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	
		// public properties
		public override string Message
		{
			get
			{
				string msg = "Client side AsyncProcessRequest() is processing a one-way call. The method has been called and the state machine transitioned but it is not ready to return to caller as expected.";
				return msg;
			}
		}
	}


}