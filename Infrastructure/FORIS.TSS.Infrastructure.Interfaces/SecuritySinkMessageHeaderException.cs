using System;
using System.IO;
using System.Threading;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Serialization;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization.Formatters.Binary;

namespace FORIS.TSS.Infrastructure.Authentication
{
	// ----------------------------------- ** -----------------------------------
	//
	//                      SecuritySinkMessageHeaderException 
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class SecuritySinkMessageHeaderException : SecuritySinkException
	{
		// construction/destruction
		public SecuritySinkMessageHeaderException() : base()
		{
		}

		protected SecuritySinkMessageHeaderException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}

		// public properties
		public override string Message
		{
			get
			{
				string msg = "An attempt was made to retrieve the message envelope from the header array but it does not exist.  The client or server channel sink may not be installed.";
				return msg;
			}
		}

	}

}
