﻿using System.Net;
using System.Security.Principal;

namespace FORIS.TSS.Infrastructure.Interfaces.UserName
{
	public class CustomPrincipal : IPrincipal
	{
		protected IIdentity identity = null;
		protected IPAddress addr     = null;

		public IPAddress IPAddress
		{
			get
			{
				return addr;
			}
		}
		public CustomPrincipal(IIdentity identity, IPAddress addr)
		{
			this.identity = identity;
			this.addr = addr;
		}
		public IIdentity Identity
		{
			get
			{
				return this.identity;
			}
		}
		public bool IsInRole(string role)
		{
			bool isInRole = false;
			if (string.Equals("Employee", role) == true)
				isInRole = true;
			return isInRole;
		}
	}
}