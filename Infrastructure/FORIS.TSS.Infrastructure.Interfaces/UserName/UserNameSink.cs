using System.Diagnostics;

namespace FORIS.TSS.Infrastructure.Interfaces.UserName
{
	public class UserNameSink
	{
		public static readonly TraceSwitch TraceSwitch =
			new TraceSwitch( "UserNameSink", "", "Warning" );
	}
}