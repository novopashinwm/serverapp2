﻿using System;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace FORIS.TSS.Infrastructure.Compactor
{
	class DataSerializer
	{
		static readonly DataRowVersion[] _aryVer = new DataRowVersion[2] 
				{ DataRowVersion.Original, DataRowVersion.Current };

		// Массив для перемапливания TypeCode на Type
		// TypeCode это перечисление включающее константы для базовых типов.
		static readonly Type[] _TypeMap;

		// Статический конструктор. Нужен для инициализации _TypeMap.
		static DataSerializer()
		{
			// TypeCode.DBNull и TypeCode.Empty пропущены, так как они 
			// по сути, не являются типами.
			_TypeMap = new Type[(int)TypeCode.String + 1];
			_TypeMap[(int)TypeCode.Object]   = typeof(Object);
			_TypeMap[(int)TypeCode.Boolean]  = typeof(Boolean);
			_TypeMap[(int)TypeCode.Char]     = typeof(Char);
			_TypeMap[(int)TypeCode.SByte]    = typeof(SByte);
			_TypeMap[(int)TypeCode.Byte]     = typeof(Byte);
			_TypeMap[(int)TypeCode.Int16]    = typeof(Int16);
			_TypeMap[(int)TypeCode.UInt16]   = typeof(UInt16);
			_TypeMap[(int)TypeCode.Int32]    = typeof(Int32);
			_TypeMap[(int)TypeCode.UInt32]   = typeof(UInt32);
			_TypeMap[(int)TypeCode.Int64]    = typeof(Int64);
			_TypeMap[(int)TypeCode.UInt64]   = typeof(UInt64);
			_TypeMap[(int)TypeCode.Single]   = typeof(Single);
			_TypeMap[(int)TypeCode.Double]   = typeof(Double);
			_TypeMap[(int)TypeCode.Decimal]  = typeof(Decimal);
			_TypeMap[(int)TypeCode.DateTime] = typeof(DateTime);
			_TypeMap[(int)TypeCode.String]   = typeof(String);
		}
		public static void SerializeDataSet(Stream stream, DataSet ds)
		{
			BinaryFormatter formatter = new BinaryFormatter();
			formatter.Serialize(stream, ds);
		}
		public static DataSet DeserializeDataSet(Stream stream)
		{
			BinaryFormatter formatter = new BinaryFormatter();
			DataSet res = (DataSet)formatter.Deserialize(stream);
			return res;
		}
	}
}