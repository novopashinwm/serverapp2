﻿using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Runtime.Remoting.Messaging;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;

namespace FORIS.TSS.Infrastructure.Compactor
{
	public class Compactor
	{
		public static void Compress(ref IMessage msg)
		{
			IDictionary dict = msg.Properties;
			// Enumerate the contents of the IDictionary interface.
			IDictionaryEnumerator eDict = dict.GetEnumerator();
			while (eDict.MoveNext())
			{
				// Get object's key.
				Object oKey = eDict.Key;
				if ((oKey as string) == "__Return")
				{
					// Get value of the key.
					Object oValue = eDict.Value;
					if (oValue is DataSet)
					{
						CompressedDataSet v = CompressInternal((DataSet)oValue);
						IMethodReturnMessage mrm = (IMethodReturnMessage)msg;
						msg = new ReturnMessage(v, mrm.OutArgs, mrm.OutArgCount, mrm.LogicalCallContext, null);
						break;
					}
				}
			}
		}
		public static void Decompress(ref IMessage msg)
		{

			IDictionary dict = msg.Properties;
			// Enumerate the contents of the IDictionary interface.
			IDictionaryEnumerator eDict = dict.GetEnumerator();
			while (eDict.MoveNext())
			{
				// Get object's key.
				Object oKey = eDict.Key;
				if ((oKey as string) == "__Return")
				{
					// Get value of the key.
					Object oValue = eDict.Value;
					if (oValue is CompressedDataSet)
					{
						DataSet v = DecompressInternal((CompressedDataSet)oValue);
						IMethodReturnMessage mrm = (IMethodReturnMessage)msg;
						msg = new ReturnMessage(v, mrm.OutArgs, mrm.OutArgCount, mrm.LogicalCallContext, null);
						break;
					}
				}
			}
		}
		protected static CompressedDataSet CompressInternal(DataSet dataset)
		{
			// Стрим для ручной сериализации через DataSetHandsSerializate.
			MemoryStream ms = new MemoryStream(1024);
			DataSerializer.SerializeDataSet(ms, dataset);
			long length = ms.Length;
			// Зипуем данные
			using (MemoryStream compressedData = new MemoryStream((int)length))
			{
				using (DeflaterOutputStream zipStream = new DeflaterOutputStream(compressedData))
				{
					zipStream.Write(ms.ToArray(), 0, (int)length);
					zipStream.Finish();
				}
				byte[] aryZip = compressedData.ToArray();
				// Полученный массив - в канал
				CompressedDataSet cds = new CompressedDataSet();
				cds.array = aryZip;
				return cds;
			}
		}
		public static DataSet DecompressInternal(CompressedDataSet cds)
		{
			// unpack array
			byte[] aryZip = cds.array;
			// unpack stream
			using (MemoryStream ms = new MemoryStream(aryZip))
			{
				// Perform unpack
				using (InflaterInputStream input = new InflaterInputStream(ms))
				{
					// Deserialize dataset
					DataSet dsLoaded = DataSerializer.DeserializeDataSet(input);
					return dsLoaded;
				}
			}
		}
	}
}