using System;
using System.Collections;
using System.Runtime.Serialization;

namespace FORIS.TSS.Infrastructure.Authentication
{
	// ----------------------------------- ** -----------------------------------
	//
	//               MutualAuthenticationSettingNotRecognizedException 
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class MutualAuthenticationSettingNotRecognizedException : SecuritySinkException
	{
		// construction/destruction
		public MutualAuthenticationSettingNotRecognizedException(string mutualAuthenticationSetting) : base()
		{
			this.mutualAuthenticationSetting = mutualAuthenticationSetting;
		}
	
		protected MutualAuthenticationSettingNotRecognizedException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	
		// public properties
		public override string Message
		{
			get
			{
				string msg = string.Format("Unknown mutual authentication setting '{0}' in configuration file", new Object[] {this.mutualAuthenticationSetting});
				return msg;
			}
		}

		public string MutualAuthenticationSetting
		{
			get
			{
				return this.mutualAuthenticationSetting;
			}
		}



		// private data members
		string mutualAuthenticationSetting;
	}
}
