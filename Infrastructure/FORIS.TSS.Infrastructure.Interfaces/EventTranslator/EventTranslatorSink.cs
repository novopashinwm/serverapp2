using System.Diagnostics;

namespace FORIS.TSS.Infrastructure.Interfaces.EventTranslator
{
	public static class EventTranslatorSink
	{
		public static readonly TraceSwitch TraceSwitch =
			new TraceSwitch( "EventTranlatorSink", "", "Warning" );
	}
}
