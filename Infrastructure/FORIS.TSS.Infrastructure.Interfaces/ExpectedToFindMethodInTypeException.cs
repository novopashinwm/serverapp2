using System;
using System.IO;
using System.Threading;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Serialization;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization.Formatters.Binary;

namespace FORIS.TSS.Infrastructure.Authentication
{
	// ----------------------------------- ** -----------------------------------
	//
	//                      ExpectedToFindMethodInTypeException 
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class ExpectedToFindMethodInTypeException : SecuritySinkException
	{
		// construction/destruction
		public ExpectedToFindMethodInTypeException(string methodName, string typeName) : base()
		{
			this.methodName = methodName;
			this.typeName = typeName;
		}
	
		protected ExpectedToFindMethodInTypeException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}

	
		// public properties
		public override string Message
		{
			get
			{
				string msg = string.Format("Attempting to find method '{0}' in type '{1}' as part of determining if the method is one-way but the method couldn't be found.", new Object[] {this.methodName, this.typeName});
				return msg;
			}
		}

		public string MethodName
		{
			get
			{
				return this.methodName;
			}
		}

		public string TypeName
		{
			get
			{
				return this.typeName;
			}
		}



		// private data members
		string methodName;
		string typeName;
	}

}
