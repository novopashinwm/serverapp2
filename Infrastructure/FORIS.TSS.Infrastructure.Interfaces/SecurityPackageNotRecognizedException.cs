using System;
using System.Runtime.Serialization;

namespace FORIS.TSS.Infrastructure.Authentication
{
	// ----------------------------------- ** -----------------------------------
	//
	//                     SecurityPackageNotRecognizedException 
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class SecurityPackageNotRecognizedException : SecuritySinkException
	{
		// construction/destruction
		public SecurityPackageNotRecognizedException(string securityPackageName) : base()
		{
			this.securityPackageName = securityPackageName;
		}
	
		protected SecurityPackageNotRecognizedException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	
		// public properties
		public override string Message
		{
			get
			{
				string msg = string.Format("Unknown security package name '{0}' in configuration file", new Object[] {this.securityPackageName});
				return msg;
			}
		}

		public string SecurityPackageName
		{
			get
			{
				return this.securityPackageName;
			}
		}



		// private data members
		string securityPackageName;
	}

}
