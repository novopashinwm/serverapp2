﻿using System;

namespace FORIS.TSS.Infrastructure.Authentication.Messages
{
	// ----------------------------------- ** -----------------------------------
	//
	//                                  Message
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public abstract class Message
	{
		// ----------------------------------- ** -----------------------------------
		//
		//                            Construction/Destruction
		//
		// ----------------------------------- ** -----------------------------------

		public Message()
		{
		}
	}





	// ----------------------------------- ** -----------------------------------
	//
	//                                 SPNRequest
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class SPNRequest : Message
	{
		// ----------------------------------- ** -----------------------------------
		//
		//                            Construction/Destruction
		//
		// ----------------------------------- ** -----------------------------------

		public SPNRequest() : base()
		{
		}
	}





	// ----------------------------------- ** -----------------------------------
	//
	//                                 SPNResponse
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class SPNResponse : Message
	{
		// ----------------------------------- ** -----------------------------------
		//
		//                            Construction/Destruction
		//
		// ----------------------------------- ** -----------------------------------

		public SPNResponse(string serverPrincipalName) : base()
		{
			this.serverPrincipalName = serverPrincipalName;
		}





		// ----------------------------------- ** -----------------------------------
		//
		//                             Public Properties
		//
		// ----------------------------------- ** -----------------------------------

		public string ServerPrincipalName
		{
			get
			{
				return this.serverPrincipalName;
			}
		}





		// ----------------------------------- ** -----------------------------------
		//
		//                             Private Data Members
		//
		// ----------------------------------- ** -----------------------------------

		private string serverPrincipalName = null;
	}





	// ----------------------------------- ** -----------------------------------
	//
	//                                 ClientToken
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class ClientToken : Message
	{
		// ----------------------------------- ** -----------------------------------
		//
		//                            Construction/Destruction
		//
		// ----------------------------------- ** -----------------------------------

		public ClientToken(byte[] token, SecuritySink.AuthenticationLevel clientAuthenticationLevel, SecuritySink.ImpersonationLevel clientImpersonationLevel) : base()
		{
			this.token = token;
			this.clientAuthenticationLevel = clientAuthenticationLevel;
			this.clientImpersonationLevel = clientImpersonationLevel;
		}





		// ----------------------------------- ** -----------------------------------
		//
		//                             Public Properties
		//
		// ----------------------------------- ** -----------------------------------

		public byte[] Token
		{
			get
			{
				return this.token;
			}
		}

		public SecuritySink.AuthenticationLevel ClientAuthenticationLevel
		{
			get
			{
				return this.clientAuthenticationLevel;
			}
		}

		public SecuritySink.ImpersonationLevel ClientImpersonationLevel
		{
			get
			{
				return this.clientImpersonationLevel;
			}
		}





		// ----------------------------------- ** -----------------------------------
		//
		//                             Private Data Members
		//
		// ----------------------------------- ** -----------------------------------

		private byte[] token = null;
		private SecuritySink.AuthenticationLevel clientAuthenticationLevel;
		private SecuritySink.ImpersonationLevel clientImpersonationLevel;
	}





	// ----------------------------------- ** -----------------------------------
	//
	//                                 ServerToken
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class ServerToken : Message
	{
		// ----------------------------------- ** -----------------------------------
		//
		//                            Construction/Destruction
		//
		// ----------------------------------- ** -----------------------------------

		public ServerToken(byte[] token) : base()
		{
			this.token = token;
		}





		// ----------------------------------- ** -----------------------------------
		//
		//                             Public Properties
		//
		// ----------------------------------- ** -----------------------------------

		public byte[] Token
		{
			get
			{
				return this.token;
			}
		}





		// ----------------------------------- ** -----------------------------------
		//
		//                             Private Data Members
		//
		// ----------------------------------- ** -----------------------------------

		private byte[] token = null;
	}





	// ----------------------------------- ** -----------------------------------
	//
	//                                  MethodCall
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class MethodCall : Message
	{
		// ----------------------------------- ** -----------------------------------
		//
		//                            Construction/Destruction
		//
		// ----------------------------------- ** -----------------------------------

		public MethodCall(byte[] token, SecuritySink.AuthenticationLevel clientAuthenticationLevel, SecuritySink.ImpersonationLevel clientImpersonationLevel) : base()
		{
			this.token = token;
			this.clientAuthenticationLevel = clientAuthenticationLevel;
			this.clientImpersonationLevel = clientImpersonationLevel;
		}





		// ----------------------------------- ** -----------------------------------
		//
		//                             Public Properties
		//
		// ----------------------------------- ** -----------------------------------

		public byte[] Token
		{
			get
			{
				return this.token;
			}
		}

		public SecuritySink.AuthenticationLevel ClientAuthenticationLevel
		{
			get
			{
				return this.clientAuthenticationLevel;
			}
		}

		public SecuritySink.ImpersonationLevel ClientImpersonationLevel
		{
			get
			{
				return this.clientImpersonationLevel;
			}
		}





		// ----------------------------------- ** -----------------------------------
		//
		//                             Private Data Members
		//
		// ----------------------------------- ** -----------------------------------

		private byte[] token = null;
		private SecuritySink.AuthenticationLevel clientAuthenticationLevel;
		private SecuritySink.ImpersonationLevel clientImpersonationLevel;
	}





	// ----------------------------------- ** -----------------------------------
	//
	//                               MethodResponse
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class MethodResponse : Message
	{
		// ----------------------------------- ** -----------------------------------
		//
		//                            Construction/Destruction
		//
		// ----------------------------------- ** -----------------------------------

		public MethodResponse() : base()
		{
		}
	}
}