using System;

namespace FORIS.TSS.Infrastructure.Authentication
{
	// ----------------------------------- ** -----------------------------------
	//
	//                                StateMachine
	//
	// ----------------------------------- ** -----------------------------------

	public abstract class StateMachine : IDisposable
	{
		public StateMachine()
		{
		}

		/// <remarks>
		/// Do not re-create Dispose clean-up code here.
		/// Calling Dispose(false) is optimal in terms of
		/// readability and maintainability.
		/// </remarks>
		~StateMachine()
		{
			Dispose(false);
		}

		/// <summary>
		/// Implement IDisposable*
		/// </summary>
		/// <remarks>
		/// Do not make this method virtual.
		/// A derived class should not be able to call this method.
		/// </remarks>
		public void Dispose() 
		{
			Dispose(true);
			// Take ourselves off the Finalization queue 
			// to prevent finalization code for this object
			// from executing a second time.
			GC.SuppressFinalize(this);
		}

		private bool disposed = false;

		/// <summary>
		/// </summary>
		/// <remarks>
		/// Dispose(bool disposing) executes in two distinct scenarios.
		/// If disposing equals true, the method has been called directly
		/// or indirectly by a user's code. Managed and unmanaged resources
		/// can be disposed.
		/// If disposing equals false, the method has been called by the
		/// runtime from inside the finalizer and you should not reference
		/// other objects. Only unmanaged resources can be disposed.
		/// </remarks>
		/// <param name="disposing"></param>
		protected virtual void Dispose(bool disposing) 
		{
			// Check to see if Dispose has already been called.
			if (!this.disposed) 
			{
				// If disposing equals true, dispose all managed
				// and unmanaged resources.
				if (disposing) 
				{
					// Dispose managed resources

					// (there are no managed resources here)
				}


				// Call the appropriate methods to clean up
				// unmanaged resources here.
				// If disposing is false,
				// only the following code is executed.

				// (there are no unmanaged resources here>
			}
			this.disposed = true;
		}


		/// <summary>
		/// All states of this state machine
		/// </summary>
		protected State[] states;

		/// <summary>
		/// Current state of state machine
		/// </summary>
		protected State currentState;

		/// <summary>
		/// Proposed state of state machine
		/// </summary>
		protected State nextState;


		// ----------------------------------- ** -----------------------------------
		//
		//                               Public Properties
		//
		// ----------------------------------- ** -----------------------------------

		public State NextState
		{
			get
			{
				return this.nextState;
			}

			set
			{
				this.nextState = value;
			}
		}


		protected void SetCurrentStateToNextState()
		{
			// if nextState is null then something has gone wrong.  there's no way to set the next state
			if (this.nextState == null)
			{
				throw new NextStateNotSetException(this.currentState);
			}

			// set the current state to the 'next state'
			this.currentState = this.nextState;

			// reset the next state
			this.nextState = null;
		}

	}
}
