using System;
using System.Runtime.Serialization;

namespace FORIS.TSS.Infrastructure.Authentication.ServerStates
{
	// ----------------------------------- ** -----------------------------------
	//
	//          ClientAuthenticationLevelMustBeGreaterThanServerException 
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class ClientAuthenticationLevelMustBeGreaterThanServerException : SecuritySinkException
	{
		// construction/destruction
		public ClientAuthenticationLevelMustBeGreaterThanServerException(SecuritySink.AuthenticationLevel clientAuthenticationLevel, SecuritySink.AuthenticationLevel serverAuthenticationLevel) : base()
		{
			this.clientAuthenticationLevel = clientAuthenticationLevel;
			this.serverAuthenticationLevel = serverAuthenticationLevel;
		}
	
		protected ClientAuthenticationLevelMustBeGreaterThanServerException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	
		// public properties
		public override string Message
		{
			get
			{
				string msg = string.Format("Client side authentication level '{0}' must be greater than or equal to server side authentication level '{1}'", new Object[] {this.clientAuthenticationLevel.ToString(), this.serverAuthenticationLevel.ToString()});
				return msg;
			}
		}

		public SecuritySink.AuthenticationLevel ClientAuthenticationLevel
		{
			get
			{
				return this.clientAuthenticationLevel;
			}
		}

		public SecuritySink.AuthenticationLevel ServerAuthenticationLevel
		{
			get
			{
				return this.serverAuthenticationLevel;
			}
		}



		// private data members
		SecuritySink.AuthenticationLevel clientAuthenticationLevel;
		SecuritySink.AuthenticationLevel serverAuthenticationLevel;
	}

}
