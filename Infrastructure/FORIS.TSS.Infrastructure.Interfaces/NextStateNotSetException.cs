using System;
using System.Runtime.Serialization;

namespace FORIS.TSS.Infrastructure.Authentication
{
	// ----------------------------------- ** -----------------------------------
	//
	//                          NextStateNotSetException 
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class NextStateNotSetException : SecuritySinkException
	{
		// construction/destruction
		public NextStateNotSetException(State currentState) : base()
		{
			this.currentStateName = currentState.GetType().Name;
		}
	
		protected NextStateNotSetException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	
		// public properties
		public override string Message
		{
			get
			{
				string msg = string.Format("The current state is '{0}' and an attempt is being made to set the next state but it is not set", new Object[] {currentStateName});
				return msg;
			}
		}

		public string CurrentStateName
		{
			get
			{
				return this.currentStateName;
			}
		}



		// private data members
		string currentStateName;
	}

}
