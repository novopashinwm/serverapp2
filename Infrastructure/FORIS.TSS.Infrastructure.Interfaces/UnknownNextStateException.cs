using System;
using System.Runtime.Serialization;

namespace FORIS.TSS.Infrastructure.Authentication
{
	// ----------------------------------- ** -----------------------------------
	//
	//                          UnknownNextStateException 
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class UnknownNextStateException : SecuritySinkException
	{
		// construction/destruction
		public UnknownNextStateException(State currentState, Type nextState) : base()
		{
			this.currentStateName = currentState.GetType().Name;
			this.nextStateName = nextState.GetType().Name;
		}
	
		public UnknownNextStateException(SerializationInfo info, StreamingContext context) 
			: base(info, context)
		{
		}
		
		// public properties
		public override string Message
		{
			get
			{
				string msg = string.Format("The current state is '{0}' and an attempt was made to set an invalid next state to '{1}'", new Object[] {currentStateName, nextStateName});
				return msg;
			}
		}

		public string CurrentStateName
		{
			get
			{
				return this.currentStateName;
			}
		}

		public string NextStateName
		{
			get
			{
				return this.nextStateName;
			}
		}



		// private data members
		string currentStateName;
		string nextStateName;
	}

}
