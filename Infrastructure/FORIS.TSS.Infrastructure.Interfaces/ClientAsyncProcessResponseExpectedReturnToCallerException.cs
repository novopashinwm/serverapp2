using System;
using System.IO;
using System.Threading;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Serialization;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization.Formatters.Binary;

namespace FORIS.TSS.Infrastructure.Authentication
{
	// ----------------------------------- ** -----------------------------------
	//
	//          ClientAsyncProcessResponseExpectedReturnToCallerException 
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class ClientAsyncProcessResponseExpectedReturnToCallerException : SecuritySinkException
	{
		// construction/destruction
		public ClientAsyncProcessResponseExpectedReturnToCallerException() : base()
		{
		}
	
		protected ClientAsyncProcessResponseExpectedReturnToCallerException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	
		// public properties
		public override string Message
		{
			get
			{
				string msg = "Client side AsyncProcessResponse() has transitioned the state machine but it is not ready to return to caller as expected.";
				return msg;
			}
		}
	}

}
