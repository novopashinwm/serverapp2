using System;
using System.IO;
using System.Collections;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;
using System.Security.Principal;

namespace FORIS.TSS.Infrastructure.Authentication
{
	// ----------------------------------- ** -----------------------------------
	//
	//                                SecuritySink
	//
	// ----------------------------------- ** -----------------------------------

	public class SecuritySink
	{
		// type definitions
		public enum AuthenticationLevel
		{
			Call,
			PacketIntegrity,
			PacketPrivacy
		};

		public enum ImpersonationLevel
		{
			Identify,
			Impersonate,
			Delegate
		};



		// constructor/destructor
		private SecuritySink()
		{
		}
	}

}
