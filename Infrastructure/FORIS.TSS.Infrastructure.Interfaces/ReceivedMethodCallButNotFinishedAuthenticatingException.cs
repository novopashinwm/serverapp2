using System;
using System.Runtime.Serialization;

namespace FORIS.TSS.Infrastructure.Authentication.ServerStates
{
	// ----------------------------------- ** -----------------------------------
	//
	//            ReceivedMethodCallButNotFinishedAuthenticatingException 
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class ReceivedMethodCallButNotFinishedAuthenticatingException : SecuritySinkException
	{
		// construction/destruction
		public ReceivedMethodCallButNotFinishedAuthenticatingException() : base()
		{
		}
	
		protected ReceivedMethodCallButNotFinishedAuthenticatingException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	
		// public properties
		public override string Message
		{
			get
			{
				string msg = "Server state machine received a method call without a security token but server context is not finished authenticating";
				return msg;
			}
		}
	}

}
