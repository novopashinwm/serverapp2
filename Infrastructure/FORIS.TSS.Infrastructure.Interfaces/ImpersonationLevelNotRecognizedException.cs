using System;
using System.Collections;
using System.Runtime.Serialization;

namespace FORIS.TSS.Infrastructure.Authentication
{
	// ----------------------------------- ** -----------------------------------
	//
	//                     ImpersonationLevelNotRecognizedException 
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class ImpersonationLevelNotRecognizedException : SecuritySinkException
	{
		// construction/destruction
		public ImpersonationLevelNotRecognizedException(string impersonationLevelName) : base()
		{
			this.impersonationLevelName = impersonationLevelName;
		}
	
		protected ImpersonationLevelNotRecognizedException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	
		// public properties
		public override string Message
		{
			get
			{
				string msg = string.Format("Unknown impersonation level name '{0}' in configuration file", new Object[] {this.impersonationLevelName});
				return msg;
			}
		}

		public string ImpersonationLevelName
		{
			get
			{
				return this.impersonationLevelName;
			}
		}



		// private data members
		string impersonationLevelName;
	}

}
