using System;
using System.Runtime.Serialization;

namespace FORIS.TSS.Infrastructure.Authentication
{
	/// <summary>
	/// Summary description for SessionExistException.
	/// </summary>
	[Serializable]
	public class SessionExistException : ApplicationException
	{
		public SessionExistException() : base("���������� ��������� ����������, �.�. ������ � ����� ������� ��� �������� �� ����" +
			"���!\n\n���������� ��������� ��������� ��� ���. ���� �������� ����������, ���������� � ���������� ��������������.")
		{
		}

		public SessionExistException(SerializationInfo info, StreamingContext context) 
			: base(info, context)
		{
		}
	}
}
