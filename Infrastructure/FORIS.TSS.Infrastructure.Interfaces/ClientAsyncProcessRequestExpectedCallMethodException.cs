using System;
using System.IO;
using System.Threading;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Serialization;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization.Formatters.Binary;


namespace FORIS.TSS.Infrastructure.Authentication
{
	// ----------------------------------- ** -----------------------------------
	//
	//               ClientAsyncProcessRequestExpectedCallMethodException 
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class ClientAsyncProcessRequestExpectedCallMethodException : SecuritySinkException
	{
		// construction/destruction
		public ClientAsyncProcessRequestExpectedCallMethodException() : base()
		{
		}
	
		protected ClientAsyncProcessRequestExpectedCallMethodException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	
		// public properties
		public override string Message
		{
			get
			{
				string msg = "Client side AsyncProcessRequest() is processing a one-way call and has transitioned the state machine but it is not ready to make the method call as expected.  One-way calls are only supported by authentication protocols that can authenticate in a single leg.";
				return msg;
			}
		}
	}
}
