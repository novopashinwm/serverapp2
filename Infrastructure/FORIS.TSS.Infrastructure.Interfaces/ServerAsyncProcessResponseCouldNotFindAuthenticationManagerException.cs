using System;
using System.IO;
using System.Threading;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Serialization;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization.Formatters.Binary;

namespace FORIS.TSS.Infrastructure.Authentication
{
	// ----------------------------------- ** -----------------------------------
	//
	//    ServerAsyncProcessResponseCouldNotFindAuthenticationManagerException 
	//
	// ----------------------------------- ** -----------------------------------

	[Serializable]
	public class ServerAsyncProcessResponseCouldNotFindAuthenticationManagerException : SecuritySinkException
	{
		// construction/destruction
		public ServerAsyncProcessResponseCouldNotFindAuthenticationManagerException() : base()
		{
		}
	
		protected ServerAsyncProcessResponseCouldNotFindAuthenticationManagerException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	
		// public properties
		public override string Message
		{
			get
			{
				string msg = "Server side AsyncProcessResponse() is attempting to find the authentication manager for the incoming message but it could not be found";
				return msg;
			}
		}
	}
}
