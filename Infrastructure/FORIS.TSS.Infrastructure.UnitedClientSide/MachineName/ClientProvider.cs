﻿using System.Collections;
using System.Runtime.Remoting.Channels;

namespace FORIS.TSS.Infrastructure.MachineName
{
	public class ClientProvider : IClientChannelSinkProvider
	{
		IClientChannelSinkProvider nextProvider = null;
		public IClientChannelSink CreateSink(IChannelSender channel, string url, object remoteChannelData)
		{
			IClientChannelSink next = nextProvider.CreateSink
				(channel, url, remoteChannelData);

			return new ClientSink(next);
		}

		public IClientChannelSinkProvider Next
		{
			get
			{
				return nextProvider;
			}
			set
			{
				nextProvider = value;
			}
		}
		public ClientProvider(IDictionary properties, ICollection providerData)
		{
		}
	}
}