﻿using System.Collections;
using System.Runtime.Remoting.Channels;

namespace FORIS.TSS.Infrastructure.MachineName
{
	public class ClientSink : IClientChannelSink
	{
		protected IClientChannelSink nextClient = null;
		public ClientSink(IClientChannelSink nextClient)
		{
			this.nextClient = nextClient;
		}

		public void ProcessMessage(
			System.Runtime.Remoting.Messaging.IMessage msg,
			ITransportHeaders requestHeaders,
			System.IO.Stream requestStream,
			out ITransportHeaders responseHeaders,
			out System.IO.Stream responseStream)
		{
			// push header
			requestHeaders["MachineName"] = System.Environment.MachineName;
			nextClient.ProcessMessage(msg, requestHeaders, requestStream, out responseHeaders, out responseStream);
		}

		public IClientChannelSink NextChannelSink
		{
			get
			{
				return nextClient;
			}
		}

		public void AsyncProcessRequest(IClientChannelSinkStack sinkStack, System.Runtime.Remoting.Messaging.IMessage msg, ITransportHeaders headers, System.IO.Stream stream)
		{
			// TODO:  Add ClientSink.AsyncProcessRequest implementation
		}

		public void AsyncProcessResponse(IClientResponseChannelSinkStack sinkStack, object state, ITransportHeaders headers, System.IO.Stream stream)
		{
			// TODO:  Add ClientSink.AsyncProcessResponse implementation
		}

		public System.IO.Stream GetRequestStream(System.Runtime.Remoting.Messaging.IMessage msg, ITransportHeaders headers)
		{
			// TODO:  Add ClientSink.GetRequestStream implementation
			return null;
		}

		public IDictionary Properties
		{
			get
			{
				// TODO:  Add ClientSink.Properties getter implementation
				return null;
			}
		}
	}

}