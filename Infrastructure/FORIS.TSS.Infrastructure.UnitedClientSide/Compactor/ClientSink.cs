﻿using System;
using System.Collections;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;

namespace FORIS.TSS.Infrastructure.Compactor
{
	public class ClientSink : IClientChannelSink, IMessageSink
	{
		protected IClientChannelSink nextChannelSink = null;
		protected IMessageSink nextSink = null;

		public ClientSink(IClientChannelSink nextChannelSink)
		{
			this.nextChannelSink = nextChannelSink;
		}

		public ClientSink(IMessageSink nextClient)
		{
			this.nextSink = nextClient;
		}

		public void ProcessMessage(
			System.Runtime.Remoting.Messaging.IMessage msg,
			ITransportHeaders requestHeaders,
			System.IO.Stream requestStream,
			out ITransportHeaders responseHeaders,
			out System.IO.Stream responseStream)
		{
			throw new NotSupportedException("Please move this sink into appropriate position in the chain");
		}

		public IClientChannelSink NextChannelSink
		{
			get { return this.nextChannelSink; }
		}

		public void AsyncProcessRequest(IClientChannelSinkStack sinkStack, System.Runtime.Remoting.Messaging.IMessage msg, ITransportHeaders headers, System.IO.Stream stream)
		{
			throw new NotSupportedException("Please move this sink into appropriate position in the chain");
		}

		public void AsyncProcessResponse(IClientResponseChannelSinkStack sinkStack, object state, ITransportHeaders headers, System.IO.Stream stream)
		{
			throw new NotSupportedException("Please move this sink into appropriate position in the chain");
		}

		public System.IO.Stream GetRequestStream(System.Runtime.Remoting.Messaging.IMessage msg, ITransportHeaders headers)
		{
			throw new NotSupportedException("Please move this sink into appropriate position in the chain");
		}

		private readonly IDictionary properties = new Hashtable(0);

		public IDictionary Properties
		{
			get { return this.properties; }
		}

		public IMessage SyncProcessMessage(IMessage msg)
		{
			IMessage res = nextSink.SyncProcessMessage(msg);
			Compactor.Decompress(ref res);
			return res;
		}

		public IMessageSink NextSink
		{
			get
			{
				return nextSink;
			}
		}

		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			return this.nextSink.AsyncProcessMessage(msg, replySink);
		}
	}
}