﻿using System.Collections;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;

namespace FORIS.TSS.Infrastructure.Compactor
{
	public class ClientProvider : IClientChannelSinkProvider
	{
		IClientChannelSinkProvider nextProvider = null;
		public IClientChannelSink CreateSink(IChannelSender channel, string url, object remoteChannelData)
		{
			IClientChannelSink next = nextProvider.CreateSink
				(channel, url, remoteChannelData);

			return new ClientSink((IMessageSink)next);
		}

		public IClientChannelSinkProvider Next
		{
			get
			{
				return nextProvider;
			}
			set
			{
				nextProvider = value;
			}
		}
		public ClientProvider(IDictionary properties, ICollection providerData)
		{
		}
	}
}