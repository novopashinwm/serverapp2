﻿using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using FORIS.TSS.Infrastructure.Interfaces.UserName;

namespace FORIS.TSS.Infrastructure.UserName
{
	public class ClientSink : IClientChannelSink, IMessageSink
	{
		protected IClientChannelSink nextChannelSink = null;
		public ClientSink(IClientChannelSink nextClient)
		{
			nextChannelSink = nextClient;
		}

		public void ProcessMessage(
			System.Runtime.Remoting.Messaging.IMessage msg,
			ITransportHeaders requestHeaders,
			System.IO.Stream requestStream,
			out ITransportHeaders responseHeaders,
			out System.IO.Stream responseStream)
		{
			// push header
			if (Thread.CurrentPrincipal != null)
			{
				Trace.WriteLineIf(
					UserNameSink.TraceSwitch.TraceInfo,
					string.Format(
						"{0}.ProcessMessage() Thread.CurrentPrincipal.Identity.Name={1}",
						GetType().Name,
						Thread.CurrentPrincipal.Identity.Name
						),
					"CLIENT UN"
					);

				requestHeaders["login"] = Thread.CurrentPrincipal.Identity.Name;
			}

			nextChannelSink.ProcessMessage(msg, requestHeaders, requestStream, out responseHeaders, out responseStream);
		}

		public IClientChannelSink NextChannelSink
		{
			get { return nextChannelSink; }
		}

		public void AsyncProcessRequest(
			IClientChannelSinkStack sinkStack,
			IMessage msg, ITransportHeaders headers, Stream stream
			)
		{
			nextChannelSink.AsyncProcessRequest(sinkStack, msg, headers, stream);
		}

		public void AsyncProcessResponse(
			IClientResponseChannelSinkStack sinkStack,
			object state, ITransportHeaders headers, Stream stream
			)
		{
			nextChannelSink.AsyncProcessResponse(sinkStack, state, headers, stream);
		}

		public System.IO.Stream GetRequestStream(System.Runtime.Remoting.Messaging.IMessage msg, ITransportHeaders headers)
		{
			// TODO:  Add ClientSink.GetRequestStream implementation
			return null;
		}

		public IDictionary Properties
		{
			get
			{
				// TODO:  Add ClientSink.Properties getter implementation
				return null;
			}
		}
		#region IMessageSink Members

		protected internal IMessageSink nextSink = null;
		public IMessage SyncProcessMessage(IMessage msg)
		{
			IMessage result = nextSink.SyncProcessMessage(msg);

			return result;
		}

		public IMessageSink NextSink
		{
			get
			{
				return nextSink;
			}
		}

		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			return nextSink.AsyncProcessMessage(msg, replySink);
		}

		#endregion
	}
}