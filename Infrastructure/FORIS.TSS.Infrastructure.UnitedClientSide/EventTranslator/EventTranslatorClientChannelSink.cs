﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;
using FORIS.TSS.BusinessLogic.Interfaces.Events;

namespace FORIS.TSS.Infrastructure.EventTranslator
{
	class EventTranslatorClientChannelSink : IClientChannelSink, IMessageSink
	{
		private static int countInstances;

		private readonly int instanceNumber = countInstances++;

		private readonly bool trace;

		/// <summary>
		///
		/// </summary>
		/// <param name="nextChannelSink"></param>
		/// <param name="polling"></param>
		/// <param name="opportunitySpectator">наблюдатель за окказиями</param>
		/// <param name="eventRepeaterRegistry"></param>
		/// <param name="trace"></param>
		public EventTranslatorClientChannelSink(
			IClientChannelSink nextChannelSink,
			bool polling,
			IOpportunitySpectator opportunitySpectator,
			EventRepeaterRegistry eventRepeaterRegistry,
			bool trace
			)
		{
			this.trace = trace;

			Debug.WriteLineIf(
				this.trace,
				"Construction",
				string.Format("ET Client {0}", this.instanceNumber)
				);

			this.polling = polling;
			this.opportunitySpectator = opportunitySpectator;

			this.eventRepeaterRegistry = eventRepeaterRegistry;

			this.nextChannelSink = nextChannelSink;
			this.nextSink = (IMessageSink)nextChannelSink;
		}

		~EventTranslatorClientChannelSink()
		{
			Debug.WriteLineIf(
				this.trace,
				"Destruction",
				string.Format("ET Client {0}", this.instanceNumber)
				);
		}

		private readonly bool polling;
		/// <summary>
		/// наблюдатель за возникновениями
		/// окказий доставки сообщений событий
		/// </summary>
		private readonly IOpportunitySpectator opportunitySpectator;

		private readonly EventRepeaterRegistry eventRepeaterRegistry;

		private readonly System.Threading.Timer pollingTimer;

		private string lastUri;

		#region Implement IClientChannelSink

		private readonly IClientChannelSink nextChannelSink;
		///<summary>
		///Gets the next client channel sink in the client sink chain.
		///</summary>
		///
		///<returns>
		///The next client channel sink in the client sink chain.
		///</returns>
		///
		///<exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public IClientChannelSink NextChannelSink
		{
			get { return nextChannelSink; }
		}

		private readonly IDictionary properties = new Hashtable(0);
		///<summary>
		///Gets a dictionary through which properties on the sink can be accessed.
		///</summary>
		///
		///<returns>
		///A dictionary through which properties on the sink can be accessed, or null if the channel sink does not support properties.
		///</returns>
		///
		///<exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public IDictionary Properties
		{
			get { return properties; }
		}

		///<summary>
		///Requests message processing from the current sink.
		///</summary>
		///
		///<param name="requestStream">The stream headed to the transport sink. </param>
		///<param name="responseHeaders">When this method returns, contains a <see cref="T:System.Runtime.Remoting.Channels.ITransportHeaders"></see> interface that holds the headers that the server returned. This parameter is passed uninitialized. </param>
		///<param name="requestHeaders">The headers to add to the outgoing message heading to the server. </param>
		///<param name="msg">The message to process. </param>
		///<param name="responseStream">When this method returns, contains a <see cref="T:System.IO.Stream"></see> coming back from the transport sink. This parameter is passed uninitialized. </param>
		///<exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception>
		public void ProcessMessage(IMessage msg, ITransportHeaders requestHeaders, Stream requestStream,
									out ITransportHeaders responseHeaders, out Stream responseStream)
		{
			throw new NotImplementedException();
		}

		///<summary>
		///Requests asynchronous processing of a method call on the current sink.
		///</summary>
		///
		///<param name="sinkStack">A stack of channel sinks that called this sink. </param>
		///<param name="stream">The stream headed to the transport sink. </param>
		///<param name="msg">The message to process. </param>
		///<param name="headers">The headers to add to the outgoing message heading to the server. </param>
		///<exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception>
		public void AsyncProcessRequest(IClientChannelSinkStack sinkStack, IMessage msg, ITransportHeaders headers,
										 Stream stream)
		{
			throw new NotImplementedException();
		}

		///<summary>
		///Requests asynchronous processing of a response to a method call on the current sink.
		///</summary>
		///
		///<param name="sinkStack">A stack of sinks that called this sink. </param>
		///<param name="state">Information generated on the request side that is associated with this sink. </param>
		///<param name="stream">The stream coming back from the transport sink. </param>
		///<param name="headers">The headers retrieved from the server response stream. </param>
		///<exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception>
		public void AsyncProcessResponse(IClientResponseChannelSinkStack sinkStack, object state, ITransportHeaders headers,
										  Stream stream)
		{
			throw new NotImplementedException();
		}

		///<summary>
		///Returns the <see cref="T:System.IO.Stream"></see> onto which the provided message is to be serialized.
		///</summary>
		///
		///<returns>
		///The <see cref="T:System.IO.Stream"></see> onto which the provided message is to be serialized.
		///</returns>
		///
		///<param name="headers">The headers to add to the outgoing message heading to the server. </param>
		///<param name="msg">The <see cref="T:System.Runtime.Remoting.Messaging.IMethodCallMessage"></see> containing details about the method call. </param>
		///<exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception>
		public Stream GetRequestStream(IMessage msg, ITransportHeaders headers)
		{
			throw new NotImplementedException();
		}


		#endregion // Implement IClientChannelSink

		#region Implement IMessageSink

		private readonly IMessageSink nextSink;
		///<summary>
		///Gets the next message sink in the sink chain.
		///</summary>
		///
		///<returns>
		///The next message sink in the sink chain.
		///</returns>
		///
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public IMessageSink NextSink
		{
			get { return nextSink; }
		}

		///<summary>
		///Synchronously processes the given message.
		///</summary>
		///
		///<returns>
		///A reply message in response to the request.
		///</returns>
		///
		///<param name="msg">The message to process. </param>
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception>
		public IMessage SyncProcessMessage(IMessage msg)
		{
			this.lastUri = (string)msg.Properties["__Uri"];

			/* Если это сообщение полинга, то нет нужды
			 * анализировать какие же там методы вызываются
			 */
			IMethodCallMessage MCM = msg as IMethodCallMessage;

			if (!msg.Properties.Contains("Polling") && MCM != null)
			{
				#region Debug

				Debug.WriteLineIf(
					this.trace,
					String.Format(
						"SyncProcessMessage() MethodName: {0}",
						MCM.MethodName
						),
					string.Format("ET Client {0}", this.instanceNumber)
					);

				#endregion // Debug

				if (MCM.InArgs.Length == 1 &&
					MCM.InArgs[0] is MulticastDelegate &&
					(
					MCM.MethodName.StartsWith("add_") ||
					MCM.MethodName.StartsWith("remove_")
					)
					)
				{
					MulticastDelegate Handler = (MulticastDelegate)MCM.InArgs[0];

					#region HandlerAction

					HandlerAction HandlerAction = HandlerAction.None;

					if (MCM.MethodName.StartsWith("add_"))
					{
						HandlerAction = HandlerAction.Add;
					}
					else if (MCM.MethodName.StartsWith("remove_"))
					{
						HandlerAction = HandlerAction.Remove;
					}

					#endregion // HandlerAction

					#region Get HandlerID & Repeater

					/* Чтоб определить идентификатор делегата
					 * сначала поищем нет ли уже такого делегата в реестре,
					 * а потом уже будем создавать и регистрировать новый
					 * идентификатор
					 */

					Guid HandlerID = this.eventRepeaterRegistry.GetHandlerID(Handler);

					if (HandlerID == Guid.Empty)
					{
						HandlerID = Guid.NewGuid();

						this.eventRepeaterRegistry.Add(HandlerID, new EventRepeaterRegistryItem(Handler));
					}

					/* Не может быть ситуации, чтоб вернулся мертвый препитер
					 */

					IRepeater Repeater = this.eventRepeaterRegistry[HandlerID].Repeater;

					#endregion // Get HandlerID & Repeater

					#region Replace handler by repeater

					/* А ведь обработчик можно прямо тут и обернуть.
					 * Создать по известному типу аргумента объект
					 * EventRepeater<TEventArgs> и уже его посылать на сервер.
					 */

					#region Change method call Message args

					/* Говорят, что это можно сделать только через
					 * специальный класс MethodCallMessageWrapper
					 */

					MethodCallMessageWrapper MCMWrapper = new MethodCallMessageWrapper(MCM);

					MCMWrapper.Args = new object[] { Repeater.Handler };

					msg = MCMWrapper;

					#endregion // Change method call Message args

					#endregion // Replace handler by repeater

					/* В свойства сообщения нельзя добавлять исходный Handler,
					 * а то он как-то там путается с аргументом метода,
					 * и все перестает работать, на сервер в аргументах
					 * прилетает исходный Handler, а не репитер.
					 *
					 * Ну на самом деле он там не то чтоб путается,
					 * а просто не может быть восстановлен, именно в
					 * свойствах сообщения, потому что сервер не знает ничего
					 * о методе-обработчике на который указывает Handler
					 * Поэтому возникает TargetInvocationException
					 */

					msg.Properties.Add("HandlerID", HandlerID);
					msg.Properties.Add("HandlerAction", HandlerAction);

					if (this.polling)
					{
						msg.Properties.Add("EventArgsType", Repeater.EventArgsType);
					}

					Guid[] Failed = this.eventRepeaterRegistry.GetFailed();

					if (Failed.Length > 0)
					{
						msg.Properties.Add("HandlerFailed", Failed);
					}
				}
			}

			msg.Properties.Add("RegistryID", this.eventRepeaterRegistry.RegistryID);

			IMessage Result = this.nextSink.SyncProcessMessage(msg);

			this.opportunitySpectator.OpportunityHasHappened();

			if (Result.Properties.Contains("EventMessages"))
			{
				EventMessage[] messages = (EventMessage[])Result.Properties["EventMessages"];
				Result.Properties.Remove("EventMessages");

				Trace.WriteLineIf(
					this.trace,
					string.Format("Delivered {0} event messages", messages.Length),
					string.Format("ET Client {0}", this.instanceNumber)
					);

				RaiseEventsDelegate RaiseEventsHandler = new RaiseEventsDelegate(this.RaiseEvents);

				RaiseEventsHandler.BeginInvoke(
					messages,
					new AsyncCallback(RaiseEvents_Callback),
					RaiseEventsHandler
					);
			}

			return Result;
		}

		private void RaiseEvents_Callback(IAsyncResult asyncResult)
		{
			RaiseEventsDelegate RaiseEventsHandler = (RaiseEventsDelegate)asyncResult.AsyncState;

			RaiseEventsHandler.EndInvoke(asyncResult);
		}

		///<summary>
		///Asynchronously processes the given message.
		///</summary>
		///
		///<returns>
		///Returns an <see cref="T:System.Runtime.Remoting.Messaging.IMessageCtrl"></see> interface that provides a way to control asynchronous messages after they have been dispatched.
		///</returns>
		///
		///<param name="msg">The message to process. </param>
		///<param name="replySink">The reply sink for the reply message. </param>
		///<exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception>
		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			/* Не надо ничего здесь реализовывать,
			 * прямой вызов обработчика события с
			 * сервера пройдет прямо к цели
			 * (репитеры обработчиков событий),
			 * и все что нужно будет выполнено,
			 */

			return this.nextSink.AsyncProcessMessage(msg, replySink);
		}


		#endregion // Implement IMessageSink

		private void RaiseEvents(EventMessage[] eventMessages)
		{
			Debug.WriteLineIf(
				this.trace,
				String.Format(
					"RaiseEvents() Count messages: {0}, Current registries count: {1}",
					eventMessages.Length,
					this.eventRepeaterRegistry.Count
					),
				string.Format("ET Client {0}", this.instanceNumber)
				);

			foreach (EventMessage eventMessage in eventMessages)
			{
				/* Возможна ситуация в которой объект (модуль), подписавшийся
				 * на событие дистанцируемого объекта закрыт и подобран
				 * сборщиком мусора не отписавшись от события.
				 *
				 * Разработчикам: отписывайтесь вовремя по возможности
				 *
				 * Следом за ним подметальщик реестра репитеров
				 * вычистил репитер, однако объект сервера все равно пришлет
				 * сообщение о событии, но репитера с соответствующим
				 * идентификатором уже не будет.
				 *
				 * При отсутствии репитера в реестре необходимо послать на сервер
				 * уведомление, что идентификатор события уже не рабочий,
				 * чтоб сервер пометил соответствующую заглушку как бракованую.
				 */

				if (this.eventRepeaterRegistry.ContainsKey(eventMessage.HandlerID))
				{
					this.eventRepeaterRegistry[eventMessage.HandlerID].Repeater.Handler.DynamicInvoke(eventMessage.Args);
				}
				else
				{
					this.eventRepeaterRegistry.AddFailed(eventMessage.HandlerID);
				}
			}
		}

		private delegate void RaiseEventsDelegate(EventMessage[] eventMessages);

		public void ProcessPollingMessage()
		{
			// TODO: Почему же без __Uri нельзя и зачем он?
			// TODO: Возможно негативное влияние на время жизни объекта на стороне сервера

			/* Нельзя посылать сообщения если в нем не задан __Uri.
			 * __Uri вроде как необходим для поиска объекта на
			 * стороне сервера. Однако мы хотели чтоб сообщение
			 * поллинга сообщений событий не выходило из стека
			 * канальных приемников. Зачем же тогда нужен
			 * валидный __Uri. Дело в том что валидность __Uri
			 * проверяется еще в канальном приемнике транспортного
			 * уровня (TcpServerTransportSink), еще до того как
			 * сообщение дойдет до нашего канального приемника и его
			 * можно будет перехватить и не вызывать объект.
			 * Может там проверяется обращение по ремотингу к объекту
			 * и обновляется его лицензия на RenewOnCallTimе, тогда
			 * возможно негативное влияние поллинга сообщений
			 * событий на время жизни объектов на стороне сервера.
			 */

			if (this.lastUri != null)
			{
				Trace.WriteLineIf(
					this.trace,
					"Polling...",
					string.Format("ET Client {0}", this.instanceNumber)
					);

				MethodCall msg = new MethodCall(
					new Header[]
						{
							new Header( "Polling", true ),
							new Header( "__Args", new object[] { } ),
							new Header( "__TypeName", typeof(object).FullName ),
							new Header( "__MethodName", "GetType" ),
							new Header( "__Uri", this.lastUri )
						}
					);

				this.SyncProcessMessage(msg);
			}

		}
	}
}