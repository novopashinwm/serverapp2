﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Remoting.Channels;
using System.Threading;
using FORIS.TSS.BusinessLogic.Interfaces.Events;

namespace FORIS.TSS.Infrastructure.EventTranslator
{
	/// <summary>
	/// Поставщик канальных приемников передачи сообщений о событиях
	/// </summary>
	/// <remarks>
	/// Если не включение получение сообщений о событиях,
	/// то при вызове метода подписки на событие через ремотниг
	/// этот канальный приемник создает объект EventRepeater,
	/// и подставляет его в аргументы вместо самого подписавщегося
	/// на события объекта клиентской стороны, это позволяет не 
	/// иметь на стороне сервера сборку с метаданными подписавшегося
	/// на событие класса и не делать метод обрабатывающий событие 
	/// публичным и скрывает от разработчика механизм создания 
	/// повторителей событий.
	/// При включении получения сообщений о событиях канальник 
	/// приемник делает все то же самое но и еще вместе с сообщением
	/// о подписке на событие извещает сторону сервера, что 
	/// возникновение этого события на стороне сервера не приведет 
	/// к вызову по ремотингу, а сообщение о его возникновении будет 
	/// помещенно в специальную очередь, и клиент заберет его с оказией
	/// Наверное клиенту достаточно иметь именно такое представление
	/// </remarks>
	class EventTranslatorClientChannelSinkProvider :
		IClientChannelSinkProvider,
		IOpportunitySpectator
	{
		private readonly bool polling;

		private readonly bool trace;

		/// <summary>
		/// Интервал с которым события забираются 
		/// с сервера при отсутствии активности в канале
		/// </summary>
		private readonly TimeSpan pollingInterval;

		/// <summary>
		/// Интервал с которым происходит подметание 
		/// в реестре репитеров с умершими целями вызова
		/// </summary>
		private readonly TimeSpan sweepInterval;

		private readonly IDictionary properties;
		private readonly ICollection providerData;

		private readonly System.Threading.Timer pollingTimer;

		/// <summary>
		/// Список созданных синков
		/// </summary>
		/// <remarks>
		/// В этом списке будем хранить все созданные канальные приемники.
		/// Для полинга будем использовать первый из них. 
		/// Если канальный приемник отпадает или совершает ошибку при 
		/// обработке сообщения поллинга сообщений событий, то его 
		/// удаляем из этого списка и т.д. и т.п.
		///  </remarks>
		private readonly List<Common.WeakReference<EventTranslatorClientChannelSink>> sinks =
			new List<Common.WeakReference<EventTranslatorClientChannelSink>>();

		public EventTranslatorClientChannelSinkProvider(IDictionary properties, ICollection providerData)
		{
			this.properties = properties;
			this.providerData = providerData;

			#region Get configuration properties

			if (
				!this.properties.Contains("polling") ||
				!bool.TryParse((string)this.properties["polling"], out this.polling)
				)
			{
				this.polling = false;
			}

			if (
				!this.properties.Contains("pollingInterval") ||
				!TimeSpan.TryParse((string)this.properties["pollingInterval"], out this.pollingInterval)
				)
			{
				this.pollingInterval = TimeSpan.FromSeconds(5);
			}

			if (
				!this.properties.Contains("sweepInterval") ||
				!TimeSpan.TryParse((string)this.properties["sweepInterval"], out this.sweepInterval)
				)
			{
				this.sweepInterval = TimeSpan.FromSeconds(30);
			}

			if (
				!this.properties.Contains("trace") ||
				!bool.TryParse((string)this.properties["trace"], out this.trace)
				)
			{
				this.trace = false;
			}

			#endregion // Get configuration properties

			this.eventRepeaterRegistry = new EventRepeaterRegistry(this.sweepInterval);

			if (this.polling)
			{
				this.pollingTimer = new Timer(
					new TimerCallback(pollingTimer_Callback),
					null,
					this.pollingInterval,
					this.pollingInterval
					);
			}
		}

		private IClientChannelSinkProvider nextProvider;
		///<summary>
		///Gets or sets the next sink provider in the channel sink provider chain.
		///</summary>
		///
		///<returns>
		///The next sink provider in the channel sink provider chain.
		///</returns>
		///
		///<exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public IClientChannelSinkProvider Next
		{
			get { return this.nextProvider; }
			set { this.nextProvider = value; }
		}

		private readonly EventRepeaterRegistry eventRepeaterRegistry;

		///<summary>
		///Creates a sink chain.
		///</summary>
		///
		///<returns>
		///The first sink of the newly formed channel sink chain, or null, which indicates that this provider will not or cannot provide a connection for this endpoint.
		///</returns>
		///
		///<param name="url">The URL of the object to connect to. This parameter can be null if the connection is based entirely on the information contained in the remoteChannelData parameter. </param>
		///<param name="channel">Channel for which the current sink chain is being constructed. </param>
		///<param name="remoteChannelData">A channel data object that describes a channel on the remote server. </param>
		///<exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception>
		public IClientChannelSink CreateSink(IChannelSender channel, string url, object remoteChannelData)
		{
			IClientChannelSink NextSink = null;

			if (this.nextProvider != null)
			{
				// Create the next sink in the chain.
				NextSink = this.nextProvider.CreateSink(channel, url, remoteChannelData);

				if (NextSink == null) return null;
			}

			EventTranslatorClientChannelSink sink =
				new EventTranslatorClientChannelSink(
					NextSink,
					this.polling,
					this,
					this.eventRepeaterRegistry,
					this.trace
					);

			this.sinks.Add(
				new Common.WeakReference<EventTranslatorClientChannelSink>(sink)
				);

			return sink;
		}

		#region IOpportunitySpectator Members

		void IOpportunitySpectator.OpportunityHasHappened()
		{
			if (this.polling)
			{
				this.pollingTimer.Change(
					this.pollingInterval,
					this.pollingInterval
					);
			}
		}

		#endregion // IOpportunitySpectator Members

		#region Polling

		private void pollingTimer_Callback(object state)
		{
			bool processed = false;

			while (this.sinks.Count > 0 && !processed)
			{
				EventTranslatorClientChannelSink sinkHolder = this.sinks[0].Target;

				if (sinkHolder != null)
				{
					try
					{
						this.sinks[0].Target.ProcessPollingMessage();

						processed = true;
					}
					catch (Exception)
					{
						processed = false;
					}
				}

				if (!processed)
				{
					this.sinks.RemoveAt(0);
				}
			}
		}

		#endregion // Polling
	}

	/// <summary>
	/// Интерфейс наблюдателя за возникновением 
	/// окказий доставки сообщений событий
	/// </summary>
	public interface IOpportunitySpectator
	{
		/// <summary>
		/// Известить наблюдателя, что окказия случилась
		/// </summary>
		void OpportunityHasHappened();
	}

	public class EventRepeaterRegistry
	{
		private readonly ConcurrentDictionary<Guid, EventRepeaterRegistryItem> _dictionary =
			new ConcurrentDictionary<Guid, EventRepeaterRegistryItem>();

		private readonly Guid _registryID = Guid.NewGuid();

		public Guid RegistryID { get { return _registryID; } }

		public Guid GetHandlerID(MulticastDelegate handler)
		{
			Guid result = Guid.Empty;

			foreach (KeyValuePair<Guid, EventRepeaterRegistryItem> entry in _dictionary)
			{
				if (entry.Value.EqualsHandler(handler))
				{
					result = entry.Key;
				}
			}

			return result;
		}

		private readonly System.Threading.Timer _sweepTimer;

		public EventRepeaterRegistry(TimeSpan sweepInterval)
		{
			_sweepInterval = sweepInterval;
			this._sweepTimer = new Timer(
				sweepTimer_Callback,
				null,
				_sweepInterval,
				TimeSpan.FromMilliseconds(-1)
				);
		}

		private void sweepTimer_Callback(object state)
		{
			var dead = new List<Guid>(_dictionary.Count);

			foreach (KeyValuePair<Guid, EventRepeaterRegistryItem> entry in _dictionary)
			{
				if (!entry.Value.Repeater.GenuineHandler.IsAlive)
				{
					dead.Add(entry.Key);
				}
			}

			foreach (Guid handlerID in dead)
			{
				EventRepeaterRegistryItem item;
				_dictionary.TryRemove(handlerID, out item);
			}

			_sweepTimer.Change(_sweepInterval, TimeSpan.FromMilliseconds(-1));
		}

		#region Failed

		private readonly List<Guid> _failed = new List<Guid>();
		private readonly object _failedLock = new object();
		private readonly TimeSpan _sweepInterval;

		public void AddFailed(Guid handlerID)
		{
			lock (_failedLock)
			{
				if (!this._failed.Contains(handlerID))
				{
					this._failed.Add(handlerID);
				}
			}
		}

		public Guid[] GetFailed()
		{
			lock (_failedLock)
			{
				Guid[] Result = this._failed.ToArray();

				this._failed.Clear();

				return Result;
			}
		}

		#endregion // Failed

		public void Add(Guid key, EventRepeaterRegistryItem item)
		{
			_dictionary.TryAdd(key, item);
		}

		public bool ContainsKey(Guid key)
		{
			return _dictionary.ContainsKey(key);
		}

		public int Count
		{
			get
			{
				return _dictionary.Count;
			}
		}

		public EventRepeaterRegistryItem this[Guid key]
		{
			get
			{
				return _dictionary[key];
			}
		}
	}

	public class GuidEventArgs : EventArgs
	{
		private readonly Guid[] guids;

		public Guid[] Guids
		{
			get { return this.guids; }
		}

		public GuidEventArgs(params Guid[] guids)
		{
			this.guids = guids;
		}
	}

	public class EventRepeaterRegistryItem
	{
		/* Ссылка на репитер в реестре должна быть жесткой,
		 * иначе будет наблюдаться преждевременная смерть репитеров,
		 * 
		 * Если ссылка на репитер будет только передаваться по ремотингу 
		 * и не удерживаться в реестре, то по истечении времени 
		 * лицензии у репитера объект умрет
		 */

		private readonly IRepeater repeater;

		public IRepeater Repeater
		{
			get { return this.repeater; }
		}

		public EventRepeaterRegistryItem(MulticastDelegate handler)
		{
			#region Design repeater

			Type HandlerType = handler.GetType();

			Type[] GenericArguments = HandlerType.GetGenericArguments();

			#region Check

			if (!HandlerType.IsGenericType ||
				GenericArguments.Length != 1 ||
				!typeof(EventArgs).IsAssignableFrom(GenericArguments[0])
				)
			{
				throw new NotSupportedException(
					"Only 'AnonymousEventHandler<TEventArgs> where TEventArgs: EventArgs' delegates are supported"
					);
			}

			#endregion Check

			Type RepeaterType = typeof(EventRepeater<>).MakeGenericType(GenericArguments);

			MethodInfo RepeaterCreatorInfo = RepeaterType.GetMethod("Create");

			this.repeater =
				(IRepeater)RepeaterCreatorInfo.Invoke(
							null,
							new object[] { handler }
							);

			#endregion // Design repeater
		}

		public bool EqualsHandler(MulticastDelegate handler)
		{
			IWeakDelegate Handler = this.repeater.GenuineHandler;

			return Handler.Equals(handler);
		}
	}
}