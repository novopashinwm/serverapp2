﻿using System.Collections;
using System.Runtime.Remoting.Channels;
using System.Security.Principal;
using System.Threading;

namespace FORIS.TSS.Infrastructure.MachineName
{
	public class ServerSink : IServerChannelSink
	{
		protected IServerChannelSink nextSink = null;

		public ServerSink(IServerChannelSink nextSink)
		{
			this.nextSink = nextSink;
		}

		public System.Runtime.Remoting.Channels.ServerProcessing ProcessMessage(
			IServerChannelSinkStack sinkStack,
			System.Runtime.Remoting.Messaging.IMessage requestMsg,
			ITransportHeaders requestHeaders,
			System.IO.Stream requestStream,
			out System.Runtime.Remoting.Messaging.IMessage responseMsg,
			out ITransportHeaders responseHeaders,
			out System.IO.Stream responseStream)
		{
			responseMsg = null;
			responseHeaders = null;
			responseStream = null;

			sinkStack.Push(this, null);

			ServerProcessing myServerProcessing = ServerProcessing.Complete;

			if (nextSink != null)
			{
				// extract header and set principal
				string machineName = requestHeaders["MachineName"] as string;
				if (machineName != null)
				{
					Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(machineName), null);
				}
				myServerProcessing = nextSink.ProcessMessage(sinkStack, requestMsg, requestHeaders, requestStream, out responseMsg, out responseHeaders, out responseStream);
			}

			switch (myServerProcessing)
			{
				case ServerProcessing.Complete:
					{
						sinkStack.Pop(this);
						break;
					}

				case ServerProcessing.OneWay:
					{
						sinkStack.Pop(this);
						break;
					}

				case ServerProcessing.Async:
					{
						sinkStack.Store(this, null);
						break;
					}
			}
			return myServerProcessing;
		}

		public System.IO.Stream GetResponseStream(IServerResponseChannelSinkStack sinkStack, object state, System.Runtime.Remoting.Messaging.IMessage msg, ITransportHeaders headers)
		{
			// TODO:  Add ServerSink.GetResponseStream implementation
			return null;
		}

		public void AsyncProcessResponse(IServerResponseChannelSinkStack sinkStack, object state, System.Runtime.Remoting.Messaging.IMessage msg, ITransportHeaders headers, System.IO.Stream stream)
		{
			// TODO:  Add ServerSink.AsyncProcessResponse implementation
		}

		public IServerChannelSink NextChannelSink
		{
			get
			{
				return nextSink;
			}
		}

		public IDictionary Properties
		{
			get
			{
				// TODO:  Add ServerSink.Properties getter implementation
				return null;
			}
		}
	}
}