﻿using System.Collections;
using System.Runtime.Remoting.Channels;

namespace FORIS.TSS.Infrastructure.MachineName
{
	public class ServerProvider : IServerChannelSinkProvider
	{
		IServerChannelSinkProvider provider = null;
		public IServerChannelSink CreateSink(IChannelReceiver channel)
		{
			IServerChannelSink next = provider.CreateSink(channel);
			return new ServerSink(next);
		}
		public IServerChannelSinkProvider Next
		{
			get
			{
				return provider;
			}
			set
			{
				provider = value;
			}
		}
		public void GetChannelData(IChannelDataStore channelData)
		{
			// TODO:  Add ServerProvider.GetChannelData implementation
		}
		public ServerProvider(IDictionary properties, ICollection providerData)
		{
		}
	}
}