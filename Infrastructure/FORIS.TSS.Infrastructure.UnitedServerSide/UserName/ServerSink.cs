﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Net;
using System.Runtime.Remoting.Channels;
using System.Security.Principal;
using System.Threading;
using FORIS.TSS.Infrastructure.Interfaces.UserName;

namespace FORIS.TSS.Infrastructure.UserName
{
	public class ServerSink : IServerChannelSink
	{
		protected IServerChannelSink nextSink = null;

		public ServerSink(IServerChannelSink nextSink)
		{
			this.nextSink = nextSink;
		}

		public System.Runtime.Remoting.Channels.ServerProcessing ProcessMessage(
			IServerChannelSinkStack sinkStack,
			System.Runtime.Remoting.Messaging.IMessage requestMsg,
			ITransportHeaders requestHeaders,
			System.IO.Stream requestStream,
			out System.Runtime.Remoting.Messaging.IMessage responseMsg,
			out ITransportHeaders responseHeaders,
			out System.IO.Stream responseStream)
		{
			responseMsg = null;
			responseHeaders = null;
			responseStream = null;

			sinkStack.Push(this, null);

			ServerProcessing myServerProcessing = ServerProcessing.Complete;

			if (nextSink != null)
			{
				try
				{
					// extract header and set principal
					string userName = requestHeaders["login"] as string;
					if (userName != null)
					{
						IPAddress addr = null;
						try
						{
							addr = (IPAddress)requestHeaders[CommonTransportKeys.IPAddress];
						}
						catch (Exception ex)
						{
							// suppress the exception
							Trace.WriteLine(ex.ToString());
						}
						Thread.CurrentPrincipal = new CustomPrincipal(new GenericIdentity(userName), addr);
					}
				}
				catch (Exception ex)
				{
					Trace.WriteLine(ex.ToString());
					throw;
				}
				try
				{
					myServerProcessing = nextSink.ProcessMessage(
						sinkStack,
						requestMsg,
						requestHeaders,
						requestStream,
						out responseMsg,
						out responseHeaders,
						out responseStream);
				}
				catch (Exception ex)
				{
					Trace.WriteLine(ex.ToString());
					throw;
				}
			}

			switch (myServerProcessing)
			{
				case ServerProcessing.Complete:
					{
						sinkStack.Pop(this);
						break;
					}

				case ServerProcessing.OneWay:
					{
						sinkStack.Pop(this);
						break;
					}

				case ServerProcessing.Async:
					{
						sinkStack.Store(this, null);
						break;
					}
			}
			return myServerProcessing;
		}

		public System.IO.Stream GetResponseStream(IServerResponseChannelSinkStack sinkStack, object state, System.Runtime.Remoting.Messaging.IMessage msg, ITransportHeaders headers)
		{
			// TODO:  Add ServerSink.GetResponseStream implementation
			return null;
		}

		public void AsyncProcessResponse(IServerResponseChannelSinkStack sinkStack, object state, System.Runtime.Remoting.Messaging.IMessage msg, ITransportHeaders headers, System.IO.Stream stream)
		{
			// TODO:  Add ServerSink.AsyncProcessResponse implementation
		}

		public IServerChannelSink NextChannelSink
		{
			get
			{
				return nextSink;
			}
		}

		public IDictionary Properties
		{
			get
			{
				// TODO:  Add ServerSink.Properties getter implementation
				return null;
			}
		}
	}
}