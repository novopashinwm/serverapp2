﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using FORIS.TSS.BusinessLogic.Interfaces.Events;
using FORIS.TSS.Infrastructure.Interfaces.EventTranslator;

namespace FORIS.TSS.Infrastructure.EventTranslator
{
	class EventTranslatorServerChannelSink : IServerChannelSink
	{
		public EventTranslatorServerChannelSink(
			IServerChannelSink nextChannelSink,
			ServerEventRegistry serverEventRegistry
			)
		{
			this.serverEventRegistry = serverEventRegistry;

			this.nextChannelSink = nextChannelSink;

			//			this.nextSink = (IMessageSink)nextChannelSink;
		}

		private readonly ServerEventRegistry serverEventRegistry;

		#region Implement IServerChannelSink

		private readonly IServerChannelSink nextChannelSink;
		private IDictionary properties;

		///<summary>
		///Requests message processing from the current sink.
		///</summary>
		///
		///<returns>
		///A <see cref="T:System.Runtime.Remoting.Channels.ServerProcessing"></see> status value that provides information about how message was processed.
		///</returns>
		///
		///<param name="responseHeaders">When this method returns, contains a <see cref="T:System.Runtime.Remoting.Channels.ITransportHeaders"></see> that holds the headers that are to be added to return message heading to the client. This parameter is passed uninitialized. </param>
		///<param name="responseMsg">When this method returns, contains a <see cref="T:System.Runtime.Remoting.Messaging.IMessage"></see> that holds the response message. This parameter is passed uninitialized. </param>
		///<param name="sinkStack">A stack of channel sinks that called the current sink. </param>
		///<param name="requestMsg">The message that contains the request. </param>
		///<param name="responseStream">When this method returns, contains a <see cref="T:System.IO.Stream"></see> that is heading back to the transport sink. This parameter is passed uninitialized. </param>
		///<param name="requestHeaders">Headers retrieved from the incoming message from the client. </param>
		///<param name="requestStream">The stream that needs to be to processed and passed on to the deserialization sink. </param>
		///<exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception>
		public ServerProcessing ProcessMessage(
			IServerChannelSinkStack sinkStack,
			IMessage requestMsg, ITransportHeaders requestHeaders, Stream requestStream,
			out IMessage responseMsg, out ITransportHeaders responseHeaders, out Stream responseStream
			)
		{
			ServerProcessing Result;

			Guid RegistryID = Guid.Empty;

			if (requestMsg.Properties.Contains("Polling"))
			{
				Trace.WriteLine(
					string.Format("Polling requested from {0}", requestMsg.Properties["__Uri"]),
					"SERVER"
					);

				if (requestMsg.Properties.Contains("RegistryID"))
				{
					RegistryID = (Guid)requestMsg.Properties["RegistryID"];
				}

				responseHeaders = new TransportHeaders();
				responseMsg = new MethodResponse(new Header[] { }, (IMethodCallMessage)requestMsg);
				responseStream = null;

				Result = ServerProcessing.Complete;
			}
			else
			{
				IMethodCallMessage MCM = requestMsg as IMethodCallMessage;

				if (MCM != null)
				{
					if (MCM.Properties.Contains("RegistryID"))
					{
						RegistryID = (Guid)MCM.Properties["RegistryID"];

						#region Get StubRegistry & MessageCollector

						EventStubRegistry StubRegistry =
							this.serverEventRegistry[RegistryID].EventStubRegistry;

						IEventMessageCollector MessageCollector =
							this.serverEventRegistry[RegistryID].EventMessageQueue;

						#endregion // Get StubRegistry & MessageCollector

						#region Mark failed stubs

						if (MCM.Properties.Contains("HandlerFailed"))
						{
							Guid[] Failed = (Guid[])MCM.Properties["HandlerFailed"];

							foreach (Guid handlerID in Failed)
							{
								/* Есть подозрение, что тут могут возникать исключения
								 * из-за того, что подметальщик уже подмел заглушку
								 * с таким идентификатором
								 */

								StubRegistry[handlerID].Stub.MarkFailed();
							}
						}

						#endregion // Mark failed stubs

						if (MCM.Properties.Contains("HandlerID"))
						{
							Guid HandlerID = (Guid)MCM.Properties["HandlerID"];

							Type EventArgsType = (Type)MCM.Properties["EventArgsType"];

							if (EventArgsType != null)
							{
								/* Необходимо подменить делегат в
								 * в аргументах сообщения на заглушку
								 */

								#region Replace handler by stub

								lock (StubRegistry)
								{
									#region Get or Create Stub

									IStub Stub = null;

									if (StubRegistry.ContainsKey(HandlerID))
									{
										Stub = StubRegistry[HandlerID].Stub;
									}

									if (Stub == null)
									{
										Stub = EventStubRegistryItem.CreateStub(
											EventArgsType,
											HandlerID,
											MessageCollector
											);

										if (StubRegistry.ContainsKey(HandlerID))
										{
											StubRegistry.Remove(HandlerID);
										}

										StubRegistry.Add(
											HandlerID,
											new EventStubRegistryItem(Stub)
											);
									}

									#endregion // Get or Create Stub

									MethodCallMessageWrapper MCMWrapper = new MethodCallMessageWrapper(MCM);

									MCMWrapper.Args[0] = Stub.Handler;

									requestMsg = MCMWrapper;
								}

								#endregion // Replace Handler by stub
							}
						}
					}
				}

				/* Создадим объекты-заглушки, обработчики которых будут
				 * вызываться событиями и помещать сообщения в очередь.
				 *
				 * Положим что обработчики поступающие на сервер только
				 * класса AnonymousEventHandler<TEventArgs>
				 */

				Result = this.nextChannelSink.ProcessMessage(
					sinkStack,
					requestMsg, requestHeaders, requestStream,
					out responseMsg, out responseHeaders, out responseStream
					);
			}

			if (RegistryID != Guid.Empty)
			{
				lock (this.serverEventRegistry)
					if (this.serverEventRegistry.ContainsKey(RegistryID))
					{
						EventMessage[] messages =
							this.serverEventRegistry[RegistryID].EventMessageQueue.Pop();

						if (messages.Length > 0)
						{
							Trace.WriteLineIf(
								EventTranslatorSink.TraceSwitch.TraceInfo,
								string.Format(
									"{0}.ProcessMessage(); send {1} messages; Thread.CurrentPrincipalIdentity.Name={2}",
									this.GetType().Name,
									messages.Length,
									Thread.CurrentPrincipal.Identity.Name
									),
								"SERVER ET"
								);

							responseMsg.Properties.Add("EventMessages", messages);
						}
					}
			}

			return Result;
		}

		///<summary>
		///Requests processing from the current sink of the response from a method call sent asynchronously.
		///</summary>
		///
		///<param name="sinkStack">A stack of sinks leading back to the server transport sink. </param>
		///<param name="state">Information generated on the request side that is associated with this sink. </param>
		///<param name="stream">The stream heading back to the transport sink. </param>
		///<param name="msg">The response message. </param>
		///<param name="headers">The headers to add to the return message heading to the client. </param>
		///<exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception>
		public void AsyncProcessResponse(IServerResponseChannelSinkStack sinkStack, object state, IMessage msg,
										  ITransportHeaders headers, Stream stream)
		{
			throw new NotImplementedException();
		}

		///<summary>
		///Returns the <see cref="T:System.IO.Stream"></see> onto which the provided response message is to be serialized.
		///</summary>
		///
		///<returns>
		///The <see cref="T:System.IO.Stream"></see> onto which the provided response message is to be serialized.
		///</returns>
		///
		///<param name="sinkStack">A stack of sinks leading back to the server transport sink. </param>
		///<param name="state">The state that has been pushed to the stack by this sink. </param>
		///<param name="msg">The response message to serialize. </param>
		///<param name="headers">The headers to put in the response stream to the client. </param>
		///<exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception>
		public Stream GetResponseStream(IServerResponseChannelSinkStack sinkStack, object state, IMessage msg,
										 ITransportHeaders headers)
		{
			throw new NotImplementedException();
		}

		///<summary>
		///Gets the next server channel sink in the server sink chain.
		///</summary>
		///
		///<returns>
		///The next server channel sink in the server sink chain.
		///</returns>
		///
		///<exception cref="T:System.Security.SecurityException">The immediate caller does not have the required <see cref="F:System.Security.Permissions.SecurityPermissionFlag.Infrastructure"></see> permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public IServerChannelSink NextChannelSink
		{
			get { return nextChannelSink; }
		}

		///<summary>
		///Gets a dictionary through which properties on the sink can be accessed.
		///</summary>
		///
		///<returns>
		///A dictionary through which properties on the sink can be accessed, or null if the channel sink does not support properties.
		///</returns>
		///
		///<exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public IDictionary Properties
		{
			get { return properties; }
		}

		#endregion // Implement IServerChannelSink
	}
}