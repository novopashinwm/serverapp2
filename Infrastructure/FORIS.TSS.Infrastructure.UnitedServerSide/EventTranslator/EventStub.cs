﻿using System;
using FORIS.TSS.Common;
using FORIS.TSS.BusinessLogic.Interfaces.Events;

namespace FORIS.TSS.Infrastructure.EventTranslator
{
	public interface IStub
	{
		MulticastDelegate Handler { get; }

		void MarkFailed();
	}

	public class EventStub<TEventArgs> : IStub
		where TEventArgs : EventArgs
	{
		#region Implement IStub

		MulticastDelegate IStub.Handler
		{
			get { return this.handler; }
		}

		void IStub.MarkFailed()
		{
			this.failed = true;
		}

		#endregion // Implement IStub

		private bool failed = false;

		private Guid handlerID;

		private IEventMessageCollector eventMessageCollector;

		private AnonymousEventHandler<TEventArgs> handler;

		private EventStub(
			Guid handlerID,
			IEventMessageCollector eventMessageCollector
			)
		{
			this.handlerID = handlerID;
			this.eventMessageCollector = eventMessageCollector;

			this.handler = new AnonymousEventHandler<TEventArgs>(this.Callback);
		}

		private void Callback(TEventArgs e)
		{
			if (!this.failed)
			{
				EventMessage EventMessage = new EventMessage(this.handlerID, e);

				this.eventMessageCollector.Push(EventMessage);
			}
			else
			{
				throw new FailedEventStubException();
			}
		}

		/// <summary>
		/// Создает объект заглушки события, 
		/// перенаправляещего события в очередь сообщений о событиях
		/// </summary>
		/// <param name="handlerID">идентификатор события</param>
		/// <param name="eventMessageCollector">очередь сообщений о событиях</param>
		/// <returns>Непосредственно объект заглушки. НЕ ДЕЛЕГАТ</returns>
		public static EventStub<TEventArgs> Create(
			Guid handlerID,
			IEventMessageCollector eventMessageCollector
			)
		{
			return
				new EventStub<TEventArgs>(
					handlerID,
					eventMessageCollector
					);
		}
	}

	public interface IEventMessageCollector
	{
		void Push(EventMessage eventMessage);
	}

	public class FailedEventStubException : ApplicationException
	{
		public FailedEventStubException()
			: base()
		{

		}
	}
}