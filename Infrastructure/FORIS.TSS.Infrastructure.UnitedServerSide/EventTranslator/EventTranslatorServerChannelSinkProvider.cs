﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.Remoting.Channels;

using FORIS.TSS.BusinessLogic.Interfaces.Events;

namespace FORIS.TSS.Infrastructure.EventTranslator
{
	public class EventTranslatorServerChannelSinkProvider : IServerChannelSinkProvider
	{
		private readonly IDictionary properties;
		private ICollection providerData;

		private readonly TimeSpan eventMessageLifeTime;
		private readonly TimeSpan sweepInterval;

		public EventTranslatorServerChannelSinkProvider(IDictionary properties, ICollection providerData)
		{
			this.properties = properties;
			this.providerData = providerData;

			if (!this.properties.Contains("eventMessageLifeTime") ||
				!TimeSpan.TryParse((string)this.properties["eventMessageLifeTime"], out this.eventMessageLifeTime)
				)
			{
				this.eventMessageLifeTime = TimeSpan.FromSeconds(20);
			}

			if (!this.properties.Contains("sweepInterval") ||
				!TimeSpan.TryParse((string)this.properties["sweepInterval"], out this.sweepInterval)
				)
			{
				this.sweepInterval = TimeSpan.FromSeconds(5);
			}

			this.serverEventRegistry = new ServerEventRegistry(this.eventMessageLifeTime, this.sweepInterval);
		}

		private IServerChannelSinkProvider nextProvider;
		///<summary>
		///Gets or sets the next sink provider in the channel sink provider chain.
		///</summary>
		///
		///<returns>
		///The next sink provider in the channel sink provider chain.
		///</returns>
		///
		///<exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="Infrastructure" /></PermissionSet>
		public IServerChannelSinkProvider Next
		{
			get { return this.nextProvider; }
			set { this.nextProvider = value; }
		}


		///<summary>
		///Returns the channel data for the channel that the current sink is associated with.
		///</summary>
		///
		///<param name="channelData">A <see cref="T:System.Runtime.Remoting.Channels.IChannelDataStore"></see> object in which the channel data is to be returned. </param>
		///<exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception>
		public void GetChannelData(IChannelDataStore channelData)
		{
			/* Совсем нечего добавить
			 */
		}

		private readonly ServerEventRegistry serverEventRegistry;

		///<summary>
		///Creates a sink chain.
		///</summary>
		///
		///<returns>
		///The first sink of the newly formed channel sink chain, or null, which indicates that this provider will not or cannot provide a connection for this endpoint.
		///</returns>
		///
		///<param name="channel">The channel for which to create the channel sink chain. </param>
		///<exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception>
		public IServerChannelSink CreateSink(IChannelReceiver channel)
		{
			// Create the next sink in the chain.
			IServerChannelSink NextSink =
				this.nextProvider.CreateSink(channel);

			return new EventTranslatorServerChannelSink(NextSink, this.serverEventRegistry);
		}
	}

	public class ServerEventRegistryItem
	{
		private readonly EventMessageQueue eventMessageQueue;

		public EventMessageQueue EventMessageQueue
		{
			get { return this.eventMessageQueue; }
		}

		private readonly EventStubRegistry eventStubRegistry;

		public EventStubRegistry EventStubRegistry
		{
			get { return this.eventStubRegistry; }
		}

		public ServerEventRegistryItem()
		{
			this.eventMessageQueue = new EventMessageQueue();

			this.eventStubRegistry = new EventStubRegistry();
		}
	}

	public class EventMessageQueue : IEventMessageCollector
	{
		private readonly List<EventMessage> eventMessages = new List<EventMessage>();

		public void Push(EventMessage eventMessage)
		{
			lock (this)
			{
				this.eventMessages.Add(eventMessage);
			}
		}

		public bool Empty
		{
			get
			{
				lock (this)
				{
					return this.eventMessages.Count == 0;
				}
			}
		}

		public int Count
		{
			get
			{
				lock (this)
				{
					return this.eventMessages.Count;
				}
			}
		}

		public EventMessage[] Pop()
		{
			lock (this)
			{
				EventMessage[] Result = this.eventMessages.ToArray();

				this.eventMessages.Clear();

				return Result;
			}
		}

		/// <summary>
		/// Подметает в очереди устаревшие сообщения о событиях
		/// </summary>
		/// <param name="eventMessageLifeTime">
		/// Порог времени жизни сообщения о событии
		/// </param>
		/// <returns>
		/// Идентификаторы событий (заглушек), для которых 
		/// найдены устаревшие сообщения о событиях
		/// </returns>
		public IList<Guid> SweepArchaic(TimeSpan eventMessageLifeTime)
		{
			if (eventMessages.Count == 0)
				return null;

			lock (this)
			{
				Trace.TraceInformation("EventMessageQueue: sweeping archaic eventMessages from " + eventMessages.Count);

				List<Guid> Result = new List<Guid>(this.eventMessages.Count);

				List<EventMessage> archaic = new List<EventMessage>(this.eventMessages.Count);

				foreach (EventMessage eventMessage in this.eventMessages)
				{
					if (eventMessage.TimeStamp + eventMessageLifeTime < DateTime.Now)
					{
						archaic.Add(eventMessage);

						if (Result.Contains(eventMessage.HandlerID))
						{
							Result.Add(eventMessage.HandlerID);
						}
					}
				}

				foreach (EventMessage eventMessage in archaic)
				{
					this.eventMessages.Remove(eventMessage);
				}

				Trace.TraceInformation("EventMessageQueue: archaic eventMessages sweeped: " + archaic.Count);

				return Result;
			}
		}
	}

	public class EventStubRegistry : Dictionary<Guid, EventStubRegistryItem>
	{
		public bool Empty
		{
			get
			{
				lock (this)
				{
					return this.Count == 0;
				}
			}
		}

	}

	public class EventStubRegistryItem
	{
		private class StubWeakReference : Common.WeakReference<IStub>
		{
			public StubWeakReference(IStub stub)
				: base(stub)
			{

			}
		}

		private readonly StubWeakReference stubReference;

		public IStub Stub
		{
			get { return this.stubReference.Target; }
		}

		public EventStubRegistryItem(IStub stub)
		{
			this.stubReference = new StubWeakReference(stub);
		}

		public static IStub CreateStub(
			Type eventArgsType,
			Guid handlerID,
			IEventMessageCollector eventMessageCollector
			)
		{
			Type StubType = typeof(EventStub<>).MakeGenericType(eventArgsType);

			MethodInfo StubCreatorInfo = StubType.GetMethod("Create");

			return
				(IStub)StubCreatorInfo.Invoke(
						null,
						new object[]
							{
								handlerID,
								eventMessageCollector
							}
						);
		}
	}
}