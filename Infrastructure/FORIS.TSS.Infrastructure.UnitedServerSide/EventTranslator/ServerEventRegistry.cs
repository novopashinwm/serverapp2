﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace FORIS.TSS.Infrastructure.EventTranslator
{
	/// <summary>
	/// Реестр обработчиков событий с очередью сообщений о событиях
	/// </summary>
	/// <remarks>
	/// Для каждого клиентского подключения (ключ) используется 
	/// собственный реестр обработчиков событий и собственная
	/// очередь сообщений о событиях.
	/// </remarks>
	public class ServerEventRegistry
	{
		private readonly ConcurrentDictionary<Guid, ServerEventRegistryItem> _hash =
			new ConcurrentDictionary<Guid, ServerEventRegistryItem>();

		private readonly TimeSpan eventMessageLifeTime;

		private readonly System.Threading.Timer sweepTimer;

		public ServerEventRegistry(TimeSpan eventMessageLifeTime, TimeSpan sweepInterval)
		{
			this.eventMessageLifeTime = eventMessageLifeTime;

			this.sweepTimer = new System.Threading.Timer(
				sweepTimer_Callback,
				null,
				120000,
				(int)sweepInterval.TotalMilliseconds
				);
		}

		private void sweepTimer_Callback(object state)
		{
			int EventStubCount = 0;
			int EventMessageCount = 0;

			List<Guid> empty = new List<Guid>(this._hash.Count);

			foreach (KeyValuePair<Guid, ServerEventRegistryItem> serverEventRegistryEntry in _hash)
			{
				IList<Guid> archaic =
					serverEventRegistryEntry.Value.EventMessageQueue.SweepArchaic(
						this.eventMessageLifeTime
						);
				if (archaic != null)
				{
					foreach (Guid handlerID in archaic)
					{
						serverEventRegistryEntry.Value.EventStubRegistry[handlerID].Stub.MarkFailed();
					}
				}

				List<Guid> dead = new List<Guid>(serverEventRegistryEntry.Value.EventStubRegistry.Count);

				#region Collect dead stubs

				foreach (
					KeyValuePair<Guid, EventStubRegistryItem> entry in serverEventRegistryEntry.Value.EventStubRegistry)
				{
					if (entry.Value.Stub == null)
					{
						dead.Add(entry.Key);
					}
				}

				#endregion // Collect dead stubs

				#region Remove dead stubs

				foreach (Guid handlerID in dead)
				{
					serverEventRegistryEntry.Value.EventStubRegistry.Remove(handlerID);
				}

				#endregion Remove dead stubs

				#region Collect empty registries

				if (serverEventRegistryEntry.Value.EventMessageQueue.Empty &&
					serverEventRegistryEntry.Value.EventStubRegistry.Empty)
				{
					empty.Add(serverEventRegistryEntry.Key);
				}

				#endregion // Collect Empty registries

				EventStubCount += serverEventRegistryEntry.Value.EventStubRegistry.Count;
				EventMessageCount += serverEventRegistryEntry.Value.EventMessageQueue.Count;
			}

			#region Remove empty registries

			foreach (Guid registryID in empty)
			{
				ServerEventRegistryItem value;
				_hash.TryRemove(registryID, out value);
			}

			#endregion // Remove empty registries

			// TODO: Придумать сбор статистики
			//StatisticCollector.Instance.EventTranslatorServerSinkRegistryCounter.RawValue = this.hash.Count;
			//StatisticCollector.Instance.EventTranslatorServerSinkEventStubCounter.RawValue = EventStubCount;
			//StatisticCollector.Instance.EventTranslatorServerSinkEventMessageCounter.RawValue = EventMessageCount;
		}

		#region Indexer

		public ServerEventRegistryItem this[Guid Key]
		{
			get
			{
				ServerEventRegistryItem result;
				if (!_hash.TryGetValue(Key, out result))
					_hash.TryAdd(Key, result = new ServerEventRegistryItem());

				return result;
			}
		}

		/// <summary>
		/// Имеется ли для клиентского соединения 
		/// реестр обработчиков и очередь сообщений
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		/// <remarks>
		/// Внутренний подметальщик может подмести данные для 
		/// клиентского соединения ( если реестр обработчиков 
		/// и очередь сообщений пусты ) даже если клиент не 
		/// отключен.
		/// </remarks>
		public bool ContainsKey(Guid key)
		{
			return this._hash.ContainsKey(key);
		}

		#endregion // Indexer
	}
}