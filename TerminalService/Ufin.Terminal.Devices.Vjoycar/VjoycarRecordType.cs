﻿namespace Compass.Ufin.Terminal.Devices.Vjoycar
{
	public enum VjoycarRecordType
	{
		ZC01, // 1.01 Registration Content: (ZC01)
		BP05, // 1.02 LLC format: (BP05)
		ZC02, // 1.03 Heartbeat Packet: (ZC02)
		ZC03, // 1.04 Return SMS: (ZC03)
		ZC04, // 1.05 Power On: (ZC04)
		ZC07, // 1.06 Check format: (ZC07)
		ZC11, // 1.07 Motion Alert with Location: (ZC11)
		ZC12, // 1.08 Low Battery Alert with Location: (ZC12)
		ZC13, // 1.09 Power Cut Alert with Location: (ZC13)
		ZC17, // 1.10 Dismounting Alert with Location: (ZC17)
		ZC20, // 1.11 Battery Level: (ZC20)
	}
}