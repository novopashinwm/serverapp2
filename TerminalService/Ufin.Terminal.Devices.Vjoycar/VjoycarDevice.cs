﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace Compass.Ufin.Terminal.Devices.Vjoycar
{
	public partial class VjoycarDevice : Device
	{
		internal static class DeviceNames
		{
			internal const string VjoyCarTK10SE = "Vjoy Car TK10SE";
			internal const string VjoyCarT580   = "Vjoy Car T580";
		}
		public VjoycarDevice() : base(typeof(VjoycarSensors), new[]
		{
			DeviceNames.VjoyCarTK10SE,
			DeviceNames.VjoyCarT580,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			if (data == null)
				return false;
			/*
			| Head       | Tracker ID | Command | Message Body         | Trail      |
			| 1 byte '(' | number,    | 4 char, | data comma separated | 1 byte ')' |
			(864768011640874,ZC11,181218,A,5552.72143N,03743.58847E,0.395,145627,000.0,134.70,12,0)
			(864768011640874,ZC20,181218,145627,6,408,65535,255)
			(864768011640874,BP05,181218,A,5552.72121N,03743.58749E,0.598,145633,000.0,134.60,12,0)
			*/
			var strVal = Encoding.ASCII.GetString(data);

			if (strVal.Length < 18)
				return false;

			if (!strVal.StartsWith("(") || !strVal.EndsWith(")") || !strVal.Contains(","))
				return false;

			return VjoycarRecord.IsMatch(strVal);
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			var results = new List<object>();
			var records = VjoycarRecord.GetRecords(data, out bufferRest);
			foreach (var record in records)
			{
				switch (record.RecType)
				{
					case "ZC01": // 1.01 Registration Content (ZC01)
						results.AddRange(ParseRecordFormat01(record));
						break;
					case "ZC02": // 1.03 Heartbeat Packet (ZC02)
					case "ZC04": // 1.05 Power On (ZC04)
						results.AddRange(ParseRecordFormat02(record));
						break;
					case "ZC03": // 1.04 Return SMS (ZC03)
						results.AddRange(ParseRecordFormat03(record));
						break;
					case "BP05": // 1.02 LLC format: (BP05)
					case "ZC07": // 1.06 Check format: (ZC07)
					case "ZC11": // 1.07 Motion Alert with Location: (ZC11)
					case "ZC12": // 1.08 Low Battery Alert with Location: (ZC12)
					case "ZC13": // 1.09 Power Cut Alert with Location: (ZC13)
					case "ZC17": // 1.10 Dismounting Alert with Location: (ZC17)
						results.AddRange(ParseRecordFormat04(record));
						break;
					case "ZC20": // 1.11 Battery Level(ZC20)
						results.AddRange(ParseRecordFormat05(record));
						break;
					default:
						results.Add(new ConfirmPacket("ok"));
						break;
				}
			}
			return results;
		}
		public IEnumerable<object> ParseRecordFormat01(VjoycarRecord record)
		{
			var results = new List<object>()
			{
				new ConfirmPacket("ok"),
			};
			return results;
		}
		public IEnumerable<object> ParseRecordFormat02(VjoycarRecord record)
		{
			var results = new List<object>()
			{
				new ConfirmPacket("ok"),
			};
			var pars = VjoycarRecordFormat02.Parse(record.RecBody);
			if (null != pars)
			{
			}
			return results;
		}
		public IEnumerable<object> ParseRecordFormat03(VjoycarRecord record)
		{
			var results = new List<object>()
			{
				new ConfirmPacket("ok"),
			};
			var pars = VjoycarRecordFormat03.Parse(record.RecBody);
			if (null != pars)
			{
			}
			return results;
		}
		public IEnumerable<object> ParseRecordFormat04(VjoycarRecord record)
		{
			var results = new List<object>()
			{
				new ConfirmPacket("ok"),
			};
			var pars = VjoycarRecordFormat04.Parse(record.RecBody);
			if (null != pars)
			{
				var mu = UnitReceived();
				mu.DeviceID     = record.RecCode;
				mu.cmdType      = CmdType.Trace;
				mu.Time         = TimeHelper.GetSecondsFromBase(pars.Timestamp);
				mu.Latitude     = MobilUnit.InvalidLatitude;
				mu.Longitude    = MobilUnit.InvalidLongitude;
				mu.CorrectGPS   = pars.Valid;
				if (mu.CorrectGPS)
				{
					mu.Latitude  = (double)pars.Lat;
					mu.Longitude = (double)pars.Lng;
					// Заплняем пришедшее из трекера
					mu.Course    = (int?)pars.Course;
					mu.Speed     = (int?)pars.Speed;
					// Заполняем данными для вычисления скорости и направления по координатам
					if ((Manager?.FillUnitInfo(mu) ?? false) && mu.Unique > 0)
					{
						// Получаем предыдущую позицию
						var lastPosition = Manager?.GetLastPosition(mu.Unique);
						if (lastPosition?.Time < mu.Time)
						{
							var metersPerSecond = GeoHelper.DistanceBetweenLocations(
								lastPosition.Latitude, lastPosition.Longitude, mu.Latitude, mu.Longitude) / (mu.Time - lastPosition.Time);
							// Расчитываем и перезаписываем значения скорости и направления движения не доверяем показаниям трекера
							mu.Speed  = (int)MeasureUnitHelper.MpsToKmph(metersPerSecond);
							mu.Course = mu.CalcCourse(lastPosition.Latitude, lastPosition.Longitude);
						}
					}
				}
				results.Add(mu);
			}
			return results;
		}
		public IEnumerable<object> ParseRecordFormat05(VjoycarRecord record)
		{
			var results = new List<object>()
			{
				new ConfirmPacket("ok"),
			};
			var pars = VjoycarRecordFormat05.Parse(record.RecBody);
			if (null != pars)
			{
				var mu = UnitReceived();
				mu.DeviceID     = record.RecCode;
				mu.cmdType      = CmdType.Trace;
				mu.Time         = TimeHelper.GetSecondsFromBase(pars.Timestamp);
				mu.Latitude     = MobilUnit.InvalidLatitude;
				mu.Longitude    = MobilUnit.InvalidLongitude;
				mu.CorrectGPS   = false;
				var sensors = new Dictionary<int, long>();
				if (pars.BatteryLevel.HasValue)
					sensors.Add((int)VjoycarSensors.BatteryLevel, pars.BatteryLevel.Value);
				if (pars.BatteryVoltage.HasValue)
					sensors.Add((int)VjoycarSensors.BatteryVoltage, pars.BatteryVoltage.Value);
				if (pars.ExternalVoltage.HasValue)
					sensors.Add((int)VjoycarSensors.ExternalVoltage, pars.ExternalVoltage.Value);
				mu.SensorValues = sensors;
				results.Add(mu);
			}
			return results;
		}
	}
}