﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Compass.Ufin.Terminal.Devices.Vjoycar
{
	/// <summary> Запись в формате Vjoycar </summary>
	public sealed class VjoycarRecord
	{
		private VjoycarRecord() { }
		/// <summary> Идентификатор трекера </summary>
		public string   RecCode { get; private set; }
		/// <summary> Тип записи </summary>
		public string   RecType { get; private set; }
		/// <summary> Тело записи после типа и до завершающего символа </summary>
		public string   RecBody { get; private set; }
		/// <summary> Массив значений параметров из RecBody разделитель запятая </summary>
		public string[] RecVals { get { return null != RecBody ? RecBody.Split(',') : new string[0]; } }
		/// <summary> Returns a <see cref="string" /> that represents this instance. </summary>
		/// <returns> A <see cref="string" /> that represents this instance. </returns>
		public override string ToString()
		{
			return $"({RecCode},{RecType},{RecBody})";
		}
		/// <summary> Регулярное выражение разбора строки(записи) </summary>
		private static readonly Regex VjoycarRecordAllRegex = new Regex(string.Empty
			+ $@"\("
			+ $@"(?<{nameof(RecCode)}>\d+),"    // DeviceId (любое число десятичных цифр)
			+ $@"(?<{nameof(RecType)}>.{{4}})," // Command  (четыре любых символа)
			+ $@"(?<{nameof(RecBody)}>[^\)]*?)" // Body     (любые символы до скобки)
			+ $@"\)",
			RegexOptions.Multiline | RegexOptions.Compiled);
		/// <summary> Проверить, содержит ли входной текст записи формата Vjoycar </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public static bool IsMatch(string data)
		{
			return VjoycarRecordAllRegex.IsMatch(data);
		}
		/// <summary> Разобрать входную строку на записи формата Vjoycar </summary>
		/// <remarks> В качестве набора парсеров используется набор по умолчанию, полученный из потомков в этой сборке </remarks>
		/// <param name="text"> Входная строка </param>
		/// <returns> Набор классов парсеров с результатами</returns>
		public static IEnumerable<VjoycarRecord> GetRecords(byte[] data, out byte[] bufferRest)
		{
			var text = Encoding.ASCII.GetString(data);
			var matches = VjoycarRecordAllRegex
				.Matches(text)
				.OfType<Match>()
				.OrderBy(m => m.Index);
			var lastMatch = matches.LastOrDefault();
			if (null != lastMatch)
				bufferRest = Encoding.ASCII.GetBytes(
					text.Substring(lastMatch.Index + lastMatch.Length));
			else
				bufferRest = Encoding.ASCII.GetBytes(text);
			return matches
				.Select(m => new VjoycarRecord
				{
					RecCode = m.Groups[nameof(RecCode)].Value,
					RecType = m.Groups[nameof(RecType)].Value,
					RecBody = m.Groups[nameof(RecBody)].Value,
				});
		}
	}
}