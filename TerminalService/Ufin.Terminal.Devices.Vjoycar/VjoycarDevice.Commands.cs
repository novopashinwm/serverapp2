﻿using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.Common;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace Compass.Ufin.Terminal.Devices.Vjoycar
{
	public partial class VjoycarDevice
	{
		public override List<CommandStep> GetCommandSteps(IStdCommand command)
		{
			var devPsw = GetPassword(command);
			var apnCfg = GetInternetApnConfig(command);
			var srvAdd = Manager.GetConstant(Constant.ServerIP);
			var srvPrt = Manager.GetConstant(Constant.ServerPort);
			var rplPhn = ContactHelper.GetNormalizedPhone(command.GetParamValue(PARAMS.Keys.ReplyToPhone));
			switch (command.Target.DeviceType)
			{
				case DeviceNames.VjoyCarTK10SE:
				case DeviceNames.VjoyCarT580:
					return new[]
					{
						new { Cmd = CmdType.Setup, Steps = new []
							{
								new { Wait = false, Text = $@"*apn*{apnCfg.Name}*{apnCfg.User}*{apnCfg.Pass}*", },
								new { Wait = false, Text = $@"*master*{devPsw}*+{rplPhn}*",                     },
								new { Wait = false, Text = $@"*multiquery*",                                    },
								new { Wait = false, Text = $@"*sleepoff*",                                      },
								new { Wait = false, Text = $@"*rupload*60*",                                    },
								new { Wait = false, Text = $@"*alertoff*",                                      },
								new { Wait = false, Text = $@"*checkoff*",                                      },
								new { Wait = false, Text = $@"*setip*{srvAdd.Replace('.','*')}*{srvPrt}*",      },
							}},
						new { Cmd = CmdType.SetInterval,    Steps = new [] { new { Wait = false, Text = $@"*rupload*60*",    }, }, },
						new { Cmd = CmdType.Status,         Steps = new [] { new { Wait = false, Text = $@"*status*",        }, }, },
						new { Cmd = CmdType.AskPosition,    Steps = new [] { new { Wait = false, Text = $@"*locateaddress*", }, }, },
						new { Cmd = CmdType.SetModeOnline,  Steps = new [] { new { Wait = false, Text = $@"*sleepoff*",      }, }, },
						new { Cmd = CmdType.SetModeWaiting, Steps = new [] { new { Wait = false, Text = $@"*sleepv*",        }, }, },
						new { Cmd = CmdType.MonitorModeOn,  Steps = new [] { new { Wait = false, Text = $@"*callin*",        }, }, },
					}
						?.FirstOrDefault(x => x.Cmd == command.Type)
						?.Steps
						?.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						?.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						?.ToList<CommandStep>();
				default:
					return base.GetCommandSteps(command);
			}
		}
		private string GetPassword(IStdCommand command)
		{
			var password = command.Target.Password;
			if (string.IsNullOrWhiteSpace(password))
				password = "123456";
			return password;
		}
	}
}