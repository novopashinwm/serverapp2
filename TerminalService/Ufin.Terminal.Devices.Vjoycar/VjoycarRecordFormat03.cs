﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Compass.Ufin.Terminal.Devices.Vjoycar
{
	/// <summary> Запись в формате Vjoycar (ZC03) </summary>
	public sealed class VjoycarRecordFormat03
	{
		/// <summary> Временная метка </summary>
		public DateTime Timestamp { get; private set; }
		/// <summary> Контент </summary>
		public string   Content   { get; private set; }
		private VjoycarRecordFormat03() { }
		/// <summary> Регулярное выражение разбора строки команды </summary>
		private static readonly Regex VjoycarRecordFormat03Regex = new Regex(string.Empty
			+ $@"^"
			+ $@"(\d{{6}}),"    // Дата  в формате: ddMMyy, UTC time
			+ $@"(\d{{6}}),"    // Время в формате: HHmmss, UTC time
			+ $@"(\$[^\$]*?\$)" // Контент "$…$"
			+ $@"$",
			RegexOptions.Multiline | RegexOptions.Compiled);
		public static VjoycarRecordFormat03 Parse(string data)
		{
			var result = default(VjoycarRecordFormat03);
			var match = VjoycarRecordFormat03Regex
				.Match(data);
			if (!match.Success)
				return result;
			var values = match
				.Groups
				.OfType<Group>()
				.Skip(1)
				.Select(g => g.Value)
				.ToArray();
			if (null != values && 3 == values.Length)
			{
				var dateTimeStyles = DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal;
				var date = DateTime.MinValue;
				if (!DateTime.TryParseExact(values[0], "ddMMyy", CultureInfo.InvariantCulture, dateTimeStyles, out date))
					return result;
				var time = DateTime.MinValue;
				if (!DateTime.TryParseExact(values[1], "HHmmss", CultureInfo.InvariantCulture, dateTimeStyles, out time))
					return result;
				result = new VjoycarRecordFormat03
				{
					Timestamp = date.Add(time - time.Date),
					Content   = values[2],
				};
			}
			return result;
		}
	}
}