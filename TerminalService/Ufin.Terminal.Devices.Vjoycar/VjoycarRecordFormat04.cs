﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using FORIS.TSS.Common;

namespace Compass.Ufin.Terminal.Devices.Vjoycar
{
	/// <summary> Запись в формате Vjoycar </summary>
	public sealed class VjoycarRecordFormat04
	{
		/// <summary> Временная метка </summary>
		public DateTime Timestamp { get; private set; }
		/// <summary> Географическая широта мобильного объекта (градусы) </summary>
		public decimal  Lat       { get; private set; }
		/// <summary> Географическая долгота мобильного объекта (градусы) </summary>
		public decimal  Lng       { get; private set; }
		/// <summary> Скорость мобильного объекта (км/ч) </summary>
		public decimal  Speed     { get; private set; }
		/// <summary> Курс мобильного объекта (градусы) </summary>
		public decimal  Course    { get; private set; }
		/// <summary> Признак валидной (правильной) позиции </summary>
		public bool     Valid     { get; private set; }
		private VjoycarRecordFormat04() { }
		/// <summary> Регулярное выражение разбора строки команды </summary>
		private static readonly Regex VjoycarRecordFormat04Regex = new Regex(string.Empty
			+ $@"^"
			+ $@"(\d{{6}}),"       // Дата в формате: ddMMyy, UTC time
			+ $@"([AV]),"          // Validity
			// Lat: ddmm.mmmm[NS]
			+ $@"(\d{{2}})"        // Lat: dd
			+ $@"(\d{{2}}\.\d+)"   // Lat: mm.mmmm
			+ $@"([NS]),"          // Lat: [NS]
			// Lng: dddmm.mmmm[EW]
			+ $@"(\d{{3}})"        // Lng: ddd
			+ $@"(\d{{2}}\.\d+)"   // Lng: mm.mmmm
			+ $@"([EW]),"          // Lng: [EW]
			+ $@"(\d+\.\d+),"      // Speed, unit is km/h
			+ $@"(\d{{6}}),"       // Время в формате: HHmmss, UTC time
			+ $@"(\d+\.\d+)"       // Cource
			,//+ $@"$",
			RegexOptions.Multiline | RegexOptions.Compiled);
		public static VjoycarRecordFormat04 Parse(string data)
		{
			var result = default(VjoycarRecordFormat04);
			var match = VjoycarRecordFormat04Regex
				.Match(data);
			if (!match.Success)
				return result;
			var values = match
				.Groups
				.OfType<Group>()
				.Skip(1)
				.Select(g => g.Value)
				.ToArray();
			if (null != values && 11 == values.Length)
			{
				var dateTimeStyles = DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal;
				var date = DateTime.MinValue;
				if (!DateTime.TryParseExact(values[0], "ddMMyy", CultureInfo.InvariantCulture, dateTimeStyles, out date))
					return result;

				var valid  = values[1] == "A";

				var latD   = decimal.Parse(values[2], NumberStyles.Number, NumberHelper.FormatInfo);
				var latM   = decimal.Parse(values[3], NumberStyles.Number, NumberHelper.FormatInfo);
				var latS   = values[4] == "N" ? +1 : -1;

				var lngD   = decimal.Parse(values[5], NumberStyles.Number, NumberHelper.FormatInfo);
				var lngM   = decimal.Parse(values[6], NumberStyles.Number, NumberHelper.FormatInfo);
				var lngS   = values[7] == "E" ? +1 : -1;
				var speed  = decimal.Parse(values[8], NumberStyles.Number, NumberHelper.FormatInfo);
				var time = DateTime.MinValue;
				if (!DateTime.TryParseExact(values[9], "HHmmss", CultureInfo.InvariantCulture, dateTimeStyles, out time))
					return result;
				var cource = decimal.Parse(values[10], NumberStyles.Number, NumberHelper.FormatInfo);
				result = new VjoycarRecordFormat04
				{
					Timestamp = date.Add(time - time.Date),
					Valid     = valid,
					Lat       = (latD + latM / 60m) * latS,
					Lng       = (lngD + lngM / 60m) * lngS,
					Speed     = speed,
					Course    = cource,
				};
			}
			return result;
		}
	}
}