﻿namespace Compass.Ufin.Terminal.Devices.Vjoycar
{
	public enum VjoycarSensors
	{
		BatteryLevel    = 01,
		BatteryVoltage  = 02,
		ExternalVoltage = 03,
	}
}