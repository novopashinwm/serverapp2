﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Compass.Ufin.Terminal.Devices.Vjoycar
{
	/// <summary> Запись в формате Vjoycar </summary>
	public sealed class VjoycarRecordFormat05
	{
		/// <summary> Временная метка </summary>
		public DateTime Timestamp       { get; private set; }
		/// <summary> Уровень заряда батареи </summary>
		public long?    BatteryLevel    { get; private set; }
		/// <summary> Напряжение батареи (в 100*В, сотых вольта) </summary>
		public long?    BatteryVoltage  { get; private set; }
		/// <summary> Внешнее напряжение (в 10*В, десятых вольта)</summary>
		public long?    ExternalVoltage { get; private set; }
		/// <summary> Что-то установлено </summary>
		public long?    Installed       { get; private set; }
		private VjoycarRecordFormat05() { }
		/// <summary> Регулярное выражение разбора строки команды </summary>
		private static readonly Regex VjoycarRecordFormat05Regex = new Regex(string.Empty
			+ $@"^"
			+ $@"(\d{{6}})," // Дата  в формате: ddMMyy
			+ $@"(\d{{6}})," // Время в формате: HHmmss
			+ $@"(\d+),"     // "6" stands for battery level, range is 0 - 6，255 refers to invalid rate
			+ $@"(\d+),"     // "421" stands for battery voltage, eg 4.21V，65535 refers to invalid rate
			+ $@"(\d+),"     // "112" stands for battery input voltage, eg11.2V，65535 refers to invalid rate
			+ $@"(\d+)"      // "0" stands for not installed(0) or installed(1), 255 refers to invalid rate
			+ $@"$",
			RegexOptions.Multiline | RegexOptions.Compiled);
		public static VjoycarRecordFormat05 Parse(string data)
		{
			var result = default(VjoycarRecordFormat05);
			var match = VjoycarRecordFormat05Regex
				.Match(data);
			if (!match.Success)
				return result;
			var values = match
				.Groups
				.OfType<Group>()
				.Skip(1)
				.Select(g => g.Value)
				.ToArray();
			if (null != values && 6 == values.Length)
			{
				var dateTimeStyles = DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal;
				var date = DateTime.MinValue;
				if (!DateTime.TryParseExact(values[0], "ddMMyy", CultureInfo.InvariantCulture, dateTimeStyles, out date))
					return result;
				var time = DateTime.MinValue;
				if (!DateTime.TryParseExact(values[1], "HHmmss", CultureInfo.InvariantCulture, dateTimeStyles, out time))
					return result;

				var blev = long.Parse(values[2]);
				var bvol = long.Parse(values[3]);
				var evol = long.Parse(values[4]);
				var inst = long.Parse(values[5]);
				result = new VjoycarRecordFormat05
				{
					Timestamp       = date.Add(time - time.Date),
					BatteryLevel    = 00255 != blev ? blev         : (long?)null,
					BatteryVoltage  = 65535 != bvol ? bvol         : (long?)null,
					ExternalVoltage = 65535 != evol ? evol         : (long?)null,
					Installed       = 00255 != inst ? (inst & 0x1) : (long?)null,
				};
			}
			return result;
		}
	}
}