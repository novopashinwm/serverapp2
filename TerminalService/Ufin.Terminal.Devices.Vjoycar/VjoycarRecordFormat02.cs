﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Compass.Ufin.Terminal.Devices.Vjoycar
{
	/// <summary> Запись в формате Vjoycar </summary>
	public sealed class VjoycarRecordFormat02
	{
		/// <summary> Временная метка </summary>
		public DateTime Timestamp { get; private set; }
		private VjoycarRecordFormat02() { }
		/// <summary> Регулярное выражение разбора строки команды </summary>
		private static readonly Regex VjoycarRecordFormat02Regex = new Regex(string.Empty
			+ $@"^"
			+ $@"(\d{{6}})," // Дата  в формате: ddMMyy, UTC time
			+ $@"(\d{{6}})"  // Время в формате: HHmmss, UTC time
			+ $@"$",
			RegexOptions.Multiline | RegexOptions.Compiled);
		public static VjoycarRecordFormat02 Parse(string data)
		{
			var result = default(VjoycarRecordFormat02);
			var match = VjoycarRecordFormat02Regex
				.Match(data);
			if (!match.Success)
				return result;
			var values = match
				.Groups
				.OfType<Group>()
				.Skip(1)
				.Select(g => g.Value)
				.ToArray();
			if (null != values && 2 == values.Length)
			{
				var dateTimeStyles = DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal;
				var date = DateTime.MinValue;
				if (!DateTime.TryParseExact(values[0], "ddMMyy", CultureInfo.InvariantCulture, dateTimeStyles, out date))
					return result;
				var time = DateTime.MinValue;
				if (!DateTime.TryParseExact(values[1], "HHmmss", CultureInfo.InvariantCulture, dateTimeStyles, out time))
					return result;
				result = new VjoycarRecordFormat02
				{
					Timestamp = date.Add(time - time.Date),
				};
			}
			return result;
		}
	}
}