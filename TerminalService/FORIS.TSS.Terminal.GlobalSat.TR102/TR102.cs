﻿using System.Collections;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.GlobalSat.TR102
{
	public class TR102 : Device
	{
		internal const string ProtocolName = "GlobalSat TR-102";
		internal static class DeviceNames
		{
			internal const string GlobalSatTR102 = "GlobalSat TR-102";
		}
		public TR102() : base(null, new[]
		{
			DeviceNames.GlobalSatTR102,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			if (data[0] == '$' && data[data.Length - 1] == '!')
			{
				string strVal = Encoding.ASCII.GetString(data);
				if (strVal.Split(',').Length == 11)
					return true;
			}
			return false;
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;
			if (!SupportData(data))
				return null;

			TR102Datagram dg = TR102Datagram.Create(data);
			IMobilUnit[] mu_ar = new IMobilUnit[1];

			var mu = UnitReceived(ProtocolName);
			mu.DeviceID   = dg.IMEI;
			mu.cmdType    = CmdType.Trace;
			mu.Time       = dg.Time;
			mu.Latitude   = dg.Latitude;
			mu.Longitude  = dg.Longitude;
			mu.Speed      = dg.Speed;
			mu.Satellites = dg.Satellites;
			mu.CorrectGPS = dg.Satellites >= 3;

			mu_ar[0] = mu;

			return mu_ar;
		}
	}
}