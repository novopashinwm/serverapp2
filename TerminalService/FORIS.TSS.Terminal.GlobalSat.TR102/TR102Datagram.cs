﻿using System;
using System.Text;
using FORIS.TSS.Common;
using FORIS.TSS.Communication;

namespace FORIS.TSS.Terminal.GlobalSat.TR102
{
	class TR102Datagram : Datagram
	{
		internal static TR102Datagram Create(byte[] data)
		{
			TR102Datagram dt = new TR102Datagram();
			dt.Data = ASCIIEncoding.ASCII.GetString(data);

			return dt;
		}

		string data;
		/// <summary>
		/// Полученная от устройства строчка с данными
		/// </summary>
		string Data
		{
			get { return this.data; }
			set { data = value; }
		}

		string[] Params
		{
			get
			{
				return Data.Split(new char[] { ',', '*', '$' });
			}
		}

		public string IMEI
		{
			get
			{
				return Params[1];
			}
		}

		public time_t Time
		{
			get
			{
				DateTime date = DateTime.ParseExact(Params[4], "ddMMyy", null);
				DateTime time = DateTime.ParseExact(Params[5], "HHmmss", null);

				return date.Add(time.TimeOfDay);
			}
		}

		static readonly string point = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;

		public float longitude
		{
			get
			{
				string str = Params[6].Substring(1);

				str = str.Replace(".", point);

				float l = float.Parse(str.Substring(0, 3));
				float m = float.Parse(str.Substring(3));

				return l + (m * 100) / 6000;
			}
		}

		public float latitude
		{
			get
			{
				string str = Params[7].Substring(1);

				str = str.Replace(".", point);

				float l = float.Parse(str.Substring(0, 2));
				float m = float.Parse(str.Substring(2));

				return l + (m * 100) / 6000;
			}
		}

		public bool North
		{
			get
			{
				return Params[7][0] == 'N';
			}
		}

		public bool East
		{
			get
			{
				return Params[6][0] == 'E';
			}
		}

		/// <summary>
		/// unit latitude. negative - south latitude
		/// </summary>
		public float Latitude
		{
			get
			{
				return North ? latitude : -latitude;
			}
		}

		/// <summary>
		/// unit longitude. negative - south longitude
		/// </summary>
		public float Longitude
		{
			get
			{
				return East ? longitude : -longitude;
			}
		}

		public int Speed
		{
			get
			{
				string str = Params[9];

				str = str.Replace(".", point);

				float speed = (float)(float.Parse(str) * 1.852);

				return (int)speed;
			}
		}

		public int Satellites
		{
			get
			{
				return int.Parse(Params[11]);
			}
		}
	}
}