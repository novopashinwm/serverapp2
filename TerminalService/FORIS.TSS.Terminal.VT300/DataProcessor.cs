﻿using System;
using System.Collections;
using System.Collections.Generic;
using FORIS.TSS.Communication;

namespace FORIS.TSS.Terminal.Meiligao.VT300
{
	public class DataProcessor
	{
		public static bool SupportCommand(byte[] data)
		{
			/*
			 * Поддерживаемые команды: 
			 * 9955 - приход координат
			*/
			short command = BitConverter.ToInt16(data, 11);
			if (command == 0x5599)
			{
				return true;
			}

			return false;
		}
		private static List<byte[]> SplitDataToPackets(byte[] data, out byte[] bufferRest)
		{
			bufferRest = null;
			var res = new List<byte[]>();
			if (data == null || data.Length == 0)
				return null;

			while (data.Length > 0)
			{

				if (data.Length < 4)
				{
					bufferRest = data;
					break;
				}

				//Длина пакета
				int packetLength = Datagram.IntFromBytesBE(data, 2, 2);

				if (data.Length < packetLength)
				{
					bufferRest = data;
					break;
				}

				var recArr = new byte[packetLength];
				Array.Copy(data, 0, recArr, 0, packetLength);
				res.Add(recArr);

				var newData = new byte[data.Length - packetLength];
				Array.Copy(data, packetLength, newData, 0, data.Length - packetLength);
				data = newData;
			}


			return res;
		}
		public static IList Process(byte[] data, out byte[] bufferRest)
		{
			var packets = SplitDataToPackets(data, out bufferRest);

			if (packets == null || packets.Count == 0)
				return null;

			var alRes = new ArrayList();

			foreach (var packet in packets)
			{
				var responses = ProcessPacket(packet);
				if (responses != null && responses.Count > 0)
				{
					alRes.AddRange(responses);
				}
			}

			return alRes;
		}
		private static IList ProcessPacket(byte[] packet)
		{
			//Если поддерживаемая команда, обрабатываем ее
			if (SupportCommand(packet))
			{
				short command = BitConverter.ToInt16(packet, 11);
				BaseDatagram datagram = null;
				switch(command)
				{
					case 0x5599: //Координаты
						datagram = new PositionDatagram(packet);
						break;
				}
				if (datagram != null)
					return datagram.GetResponses();

			}

			return null;
		}
	}
}