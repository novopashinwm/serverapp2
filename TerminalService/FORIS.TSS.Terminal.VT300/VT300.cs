﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Communication;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Meiligao.VT300
{
	public class VT300 : Device
	{
		internal const string ProtocolName = "Meiligao VT300";
		internal static class DeviceNames
		{
			internal const string MeiligaoVT300 = "Meiligao VT300";
		}
		public VT300() : base(null, new[]
		{
			DeviceNames.MeiligaoVT300,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			if (data == null) return false;

			/*
			$$<L (2 bytes)><ID (7 bytes)><command (2 bytes)><data><checksum (2 bytes)>\r\n
			*/
			string strVal = Encoding.ASCII.GetString(data);
			if (strVal.Length < 17 || !strVal.StartsWith("$$") || strVal.IndexOf("\r\n") < 0)
				return false;

			if (Datagram.IntFromBytesBE(data, 2, 2) == strVal.IndexOf("\r\n") + 2)
				return true;

			/*
			if (DataProcessor.SupportCommand(data))
			{
				return true;
			}
			*/
			return false;
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			return Parse(data, out bufferRest);
		}
		public IList Parse(byte[] data, out byte[] bufferRest)
		{
			return DataProcessor.Process(data, out bufferRest);
		}
	}
}