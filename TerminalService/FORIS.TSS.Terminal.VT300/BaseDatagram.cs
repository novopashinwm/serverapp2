﻿using System;
using System.Collections;
using System.Diagnostics;

namespace FORIS.TSS.Terminal.Meiligao.VT300
{
	public abstract class BaseDatagram
	{
		protected byte[] Data;
		protected short  Command;
		protected bool   IsCorrectCRC;
		protected string ID;
		public BaseDatagram(byte[] data)
		{
			Data = data;
			Command = BitConverter.ToInt16(data, 11);
			ParseID();
			CheckCRC();
		}
		protected void ParseID()
		{
			string s = BitConverter.ToString(Data, 4, 7);
			s = s.Replace("F", "");
			s = s.Replace("-", "");
			ID = s;
		}
		protected void CheckCRC()
		{
			var crc = BitConverter.ToUInt16(new byte[] { Data[Data.Length - 3], Data[Data.Length - 4] }, 0);
			var calculatedCRC = CalculateCRC();
			IsCorrectCRC = calculatedCRC==crc;
			if (!IsCorrectCRC)
			{
				Trace.WriteLine(string.Format(this.GetType() + ".CheckCRC - wrong CRC in packet: \r\n {0}", BitConverter.ToString(Data)));
			}
		}
		protected ushort CalculateCRC()
		{
			Crc16Ccitt crc = new Crc16Ccitt(InitialCrcValue.NonZero1);
			byte[] toCalc = new byte[Data.Length - 4];
			Array.Copy(Data, toCalc, Data.Length - 4);
			var crcVal = crc.ComputeChecksum(toCalc);
			return crcVal;
		}
		public abstract IList GetResponses();
	}
}