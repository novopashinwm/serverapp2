﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Meiligao.VT300
{
	public class PositionDatagram: BaseDatagram
	{
		public DateTime? Time;
		public bool GPSCorrect;
		public double? Lat;
		public double? Lng;
		public int? Speed;
		public int? Course;
		public byte? HDOP;
		/// <summary>
		/// Высота в сантиметрах
		/// </summary>
		public int? Altitude;
		public int? AN1;
		public int? AN2;
		public int? DS;

		private string[] _GPRMCParts;

		public PositionDatagram (byte[] data):base(data)
		{
			if (IsCorrectCRC)
			{
				string strData = Encoding.ASCII.GetString(data, 13, data.Length - 13 - 4);
				var dataParts = strData.Split('|');

				if (!string.IsNullOrEmpty(dataParts[0]))
				{
					//GPRMC формат:  hhmmss.dd,S,xxmm.dddd,<N|S>,yyymm.dddd,<E|W>,s.s,h.h,ddmmyy,d.d,D*HH
					_GPRMCParts = dataParts[0].Split(',');
					//Дата Время
					DateTime time;
					if (DateTime.TryParseExact(GPRMCPart(GPRMC_Parts.Date) + GPRMCPart(GPRMC_Parts.Time).Split('.')[0],
											   "ddMMyyHHmmss", CultureInfo.InvariantCulture,
											   DateTimeStyles.AdjustToUniversal, out time))
					{
						Time = time;
					}

					if (Time != null && Time.HasValue)
					{

						GPSCorrect = GPRMCPart(GPRMC_Parts.GPSStatus) == "A";

						if (GPSCorrect)
						{
							//xxmm.dddd xx = degrees;mm = minutes;dddd = decimal part of minutes
							var latStr = GPRMCPart(GPRMC_Parts.Latitude);
							Lat = double.Parse(latStr.Substring(0, 2), CultureInfo.InvariantCulture);
							Lat += double.Parse(latStr.Substring(2, 2) + latStr.Substring(5, 4), CultureInfo.InvariantCulture) / 600000;
							Lat *= GPRMCPart(GPRMC_Parts.NorthSouth) == "S" ? -1 : 1;

							//yyymm.dddd yyy = degrees;mm = minutes;dddd = decimal part of minutes
							var lngStr = GPRMCPart(GPRMC_Parts.Longitude);
							Lng = double.Parse(lngStr.Substring(0, 3), CultureInfo.InvariantCulture);
							Lng += double.Parse(lngStr.Substring(3, 2) + lngStr.Substring(6, 4), CultureInfo.InvariantCulture) / 600000;
							Lng *= GPRMCPart(GPRMC_Parts.EastWest) == "W" ? -1 : 1;

							if (!string.IsNullOrEmpty(GPRMCPart(GPRMC_Parts.Speed)))
							{
								Speed =
									(int)
									(double.Parse(GPRMCPart(GPRMC_Parts.Speed), CultureInfo.InvariantCulture)*1.852);
							}

							if (!string.IsNullOrEmpty(GPRMCPart(GPRMC_Parts.Course)))
							{
								Course = (int) double.Parse(GPRMCPart(GPRMC_Parts.Course), CultureInfo.InvariantCulture);
							}
						}
					}
				}

				if (dataParts.Length > 1)
				{
					if (!string.IsNullOrEmpty(dataParts[1]))
					{
						double hdop;
						if (double.TryParse(dataParts[1], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture,
											out hdop))
						{
							HDOP = (byte) (hdop);
						}
					}
				}

				if (dataParts.Length > 2)
				{
					if (!string.IsNullOrEmpty(dataParts[2]))
					{
						double alt;
						if (double.TryParse(dataParts[2], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture,
											out alt))
						{
							Altitude = (int) (alt*100);
						}
					}
				}

				if (dataParts.Length > 3)
				{
					if (!string.IsNullOrEmpty(dataParts[3]))
					{
						int ds;

						if (int.TryParse(dataParts[3], NumberStyles.HexNumber, CultureInfo.InvariantCulture, out ds))
						{
							DS = ds;
						}
					}
				}

				if (dataParts.Length > 4)
				{
					if (!string.IsNullOrEmpty(dataParts[4]))
					{
						var ans = dataParts[4].Split(',');
						if (ans.Length == 2)
						{
							int an;
							if (int.TryParse(ans[0].Trim(), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out an))
							{
								AN1 = an;
							}
							if (int.TryParse(ans[1].Trim(), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out an))
							{
								AN2 = an;
							}
						}
					}
				}
			}

		}

		private string GPRMCPart(GPRMC_Parts part)
		{
			return _GPRMCParts[(int) part];
		}

		//GPRMC формат:  hhmmss.dd,S,xxmm.dddd,<N|S>,yyymm.dddd,<E|W>,s.s,h.h,ddmmyy,d.d,D*HH
		private enum GPRMC_Parts
		{
			Time=0,
			GPSStatus=1,
			Latitude=2,
			NorthSouth=3,
			Longitude=4,
			EastWest=5,
			Speed=6,
			Course=7,
			Date=8,
		}

		public override IList GetResponses()
		{
			if (Time == null || !Time.HasValue)
				return null;
			
			var res = new ArrayList();

			var mu = new MobilUnit.Unit.MobilUnit
			{
				cmdType = CmdType.Trace,
				DeviceID = ID,
				Time = BusinessLogic.TimeHelper.GetSecondsFromBase(Time.Value),
				CorrectGPS = GPSCorrect
			};
			mu.Properties.Add(DeviceProperty.Protocol, VT300.ProtocolName);

			if (GPSCorrect)
			{
				mu.Latitude = Lat.Value;
				mu.Longitude = Lng.Value;
				mu.Satellites = HDOP.HasValue?HDOP.Value:(byte)10;
				if (Speed.HasValue) mu.Speed = Speed.Value;
				if (Course.HasValue) mu.Course = Course.Value;
				if (Altitude.HasValue) mu.Height = Altitude.Value;
			}

			//if (AN1.HasValue) mu.VoltageAN1 = AN1.Value;
			//if (AN2.HasValue) mu.VoltageAN2 = AN2.Value;

			if (AN1.HasValue || AN2.HasValue || DS.HasValue)
			{
				//Заполняем массив показаний датчиков
				mu.SensorValues = new Dictionary<int, long>();

				if (AN1.HasValue) mu.SensorValues.Add((int) Sensors.AN1, AN1.Value);
				if (AN2.HasValue) mu.SensorValues.Add((int)Sensors.AN2, AN2.Value);

				if (DS.HasValue)
				{
					mu.SensorValues.Add((int)Sensors.D1, (DS.Value & 0x01) == 0x01? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D2, (DS.Value & 0x02) == 0x02 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D3, (DS.Value & 0x04) == 0x04 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D4, (DS.Value & 0x08) == 0x08 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D5, (DS.Value & 0x10) == 0x10 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D9, (DS.Value & 0x100) == 0x100 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D10, (DS.Value & 0x200) == 0x200 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D11, (DS.Value & 0x400) == 0x400 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D12, (DS.Value & 0x800) == 0x800 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D13, (DS.Value & 0x1000) == 0x1000 ? 1 : 0);
				}
			}

			res.Add(mu);
			return res;
		}

		public enum Sensors
		{
			AN1 =  1,
			AN2 =  2,
			D1  = 11,
			D2  = 12,
			D3  = 13,
			D4  = 14,
			D5  = 15,
			D9  = 19,
			D10 = 20,
			D11 = 21,
			D12 = 22,
			D13 = 23,
		}
	}
}