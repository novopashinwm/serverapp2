﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FORIS.TSS.Terninal.FORT300
{
    public enum SensorValues
    {
        
        /// <summary>
        /// аналлоговый вход 1
        /// </summary>
        Analog1 = 12,
        /// <summary>
        /// аналлоговый вход 2
        /// </summary>
        Analog2 = 13,
        /// <summary>
        /// аналлоговый вход 3
        /// </summary>
        Analog3 = 14,
        /// <summary>
        /// аналлоговый вход 4
        /// </summary>
        Analog4 = 15,

        //цифровые входы
        INDigital1 = 16,
        INDigital2 = 17,
        INDigital3 = 18,
        INDigital4 = 19,
        INDigital5 = 20,
        INDigital6 = 21,
        INDigital7 = 22,
        INDigital8 = 23,
        INDigital9 = 24,
        INDigital10 = 25,

        //цыфровые выходы
        OUTDigital1 = 26,
        OUTDigital2 = 27,
        OUTDigital3 = 28,
        OUTDigital4 = 29,
        OUTDigital5 = 30,
        OUTDigital6 = 31,
        OUTDigital7 = 32,
        OUTDigital8 = 33,
        OUTDigital9 = 34,
        OUTDigital10 = 35,

        //внутренний и внешний аккумуляторы
        InternalBattery = 36,
        ExternalBattery = 37,

        //CAN
        CANSpeed = 38,
        CANAccelerator = 39,
        CANTotalFuelSpend = 40,
        CANFuelLevel = 41,
        CANrPM = 42,
        CANAxisNumber = 43,
        CANNumberOfTires = 44,
        CANTotalNumberOfHours = 45,
        CANTotalRun = 46,
        CANRunToService = 47,
        CANTemperatureEngine = 48,
        CANAxisHeight = 49,

        FirstCounter = 50,
        SecondCounter = 51,

        LLSTemperature = 52,
        LLSCounter = 53,
        LLSFrequency = 54

    }
}
