﻿using System;
using System.Text;
using System.Collections;
using FORIS.TSS.Terninal.FORT300.Datagram;

namespace FORIS.TSS.Terninal.FORT300
{
    public class DataProcessor
    {
        public static IList Process(byte[] data, out byte[] bufferRest)
        {
            bufferRest = null;

            if (data == null || data.Length == 0)
                return null;
            var alRes = new ArrayList();
            var responses = ProcessPacket(data);
            if (responses != null && responses.Count > 0)
                alRes.AddRange(responses);
            return alRes;
        }

        private static IList ProcessPacket(byte[] packet)
        {
            BaseDatagram datagramm = BaseDatagram.Init(packet);
            if (datagramm == null) return null;
            return datagramm.GetResponses();
        }
    }
}
