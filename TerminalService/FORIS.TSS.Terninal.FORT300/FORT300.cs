﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FORIS.TSS.Terminal;

namespace FORIS.TSS.Terninal.FORT300
{
    public class FORT300: Device
    {
        /// <summary>
        /// обработка пришедших данных
        /// </summary>
        /// <param name="data"></param>
        /// <param name="count"></param>
        /// <param name="stateData"></param>
        /// <param name="bufferRest"></param>
        /// <returns></returns>
        public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
        {
            return DataProcessor.Process(data, out bufferRest);
        }

        /// <summary>
        /// проверка поддержки формата
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override bool SupportData(byte[] data)
        {
            return Datagram.BaseDatagram.SuportData(data);
        }
    }
}
