﻿using System.Collections;
using System.Reflection.Emit;

namespace FORIS.TSS.Terninal.FORT300.Datagram
{
    public abstract class BaseDatagram
    {
        public byte[] Paket { get; protected set; }
        public string DeviceId { get; protected set; }

        public static BaseDatagram Init(byte[] paket)
        {
            if (!SuportData(paket)) return null;
            return new PositionDatagram(paket);
        }

        public static bool SuportData(byte[] paket)
        {
            if (paket == null)
                return false;

            if (paket[0] != 0x1D && paket[0] != 0x2D)
                return false;

            var additionalDataIndex = 55;
            if (paket.Length > additionalDataIndex)
            {
                if (paket[54] != 0x00)
                    return false;

                var length = paket[additionalDataIndex + 1];
                return length > 0;
            }

            return true;
        }

        public abstract IList GetResponses();
    }
}
