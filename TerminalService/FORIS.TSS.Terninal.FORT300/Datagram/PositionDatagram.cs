﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Helpers;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terninal.FORT300.Datagram
{
    public sealed class PositionDatagram: BaseDatagram
    {
        public PositionDatagram(ITerminalManager manager)
        {
            _manager = manager;
        }

        #region fields
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Time { get; set; }
        public int Speed { get; set; }
        public int Course { get; set; }
        public int Height { get; set; }
        public int Satellites { get; set; }

        byte messageType, messageNumber;
        private readonly ITerminalManager _manager;

        #endregion


        public Dictionary<int, long> Sensors { get; set; }

        int BCDToInt(int bcds)
        {
            return int.Parse(bcds.ToString("X"));
        }

        internal PositionDatagram(byte[] paket)
        {
            messageType = paket[0];
            messageNumber = paket[9];

            base.Paket = paket;
            DeviceId = string.Join("", paket.AsEnumerable().TakeAfter(1).Take(8).Select(d => d.ToString("X")));
            //данные навигации
            var dataNavigationArray = new byte[27];
            Array.Copy(paket,10,dataNavigationArray,0,27);
            Latitude = BCDToInt(dataNavigationArray[1]) * (dataNavigationArray[0] == 1 ? -1 : 1)
                + (BCDToInt(dataNavigationArray[2]) + BCDToInt(dataNavigationArray[3] * 0x100 + dataNavigationArray[4]) / 10000.0) / 60.0;
            Longitude = BCDToInt(dataNavigationArray[6]) * (dataNavigationArray[5] == 1 ? -1 : 1) 
                + (BCDToInt(dataNavigationArray[7])  + BCDToInt(dataNavigationArray[8] * 0x100 + dataNavigationArray[9]) / 10000.0) / 60.0;
            var date = new DateTime(BCDToInt(dataNavigationArray[12]) + 2000, BCDToInt(dataNavigationArray[11]),
                BCDToInt(dataNavigationArray[10]), BCDToInt(dataNavigationArray[13]), BCDToInt(dataNavigationArray[14]), BCDToInt(dataNavigationArray[15]));
            Time = TimeHelper.GetSecondsFromBase(date);
            Speed = BCDToInt(dataNavigationArray[17] * 0x100 + dataNavigationArray[18]) / 10;
            Course = BCDToInt(dataNavigationArray[18] * 0x100 + dataNavigationArray[19]) / 10;
            Height = BCDToInt(dataNavigationArray[20] * 0x10000 + dataNavigationArray[21] * 0x100 + dataNavigationArray[22]);
            Satellites = BCDToInt(dataNavigationArray[24]);

            if (Paket.Length>38)
            {
            if (Sensors==null)Sensors = new Dictionary<int, long>();
            //входы и выходы
            #region
            var arSensorValues = new byte[12];
            Array.Copy(paket, 37, arSensorValues, 0, 12);
            var mask = 0x1;
            Sensors.Add((int)SensorValues.INDigital9, (arSensorValues[0] & mask) > 0 ? 1 : 0);
            mask = mask << 1;
            Sensors.Add((int)SensorValues.INDigital10, (arSensorValues[0] & mask) > 0 ? 1 : 0);
            mask = 0x1;
            Sensors.Add((int)SensorValues.INDigital1, (arSensorValues[1] & mask) > 0 ? 1 : 0);
            mask = mask << 1;
            Sensors.Add((int)SensorValues.INDigital2, (arSensorValues[1] & mask) > 0 ? 1 : 0);
            mask = mask << 1;
            Sensors.Add((int)SensorValues.INDigital3, (arSensorValues[1] & mask) > 0 ? 1 : 0);
            mask = mask << 1;
            Sensors.Add((int)SensorValues.INDigital4, (arSensorValues[1] & mask) > 0 ? 1 : 0);
            mask = mask << 1;
            Sensors.Add((int)SensorValues.INDigital5, (arSensorValues[1] & mask) > 0 ? 1 : 0);
            mask = mask << 1;
            Sensors.Add((int)SensorValues.INDigital6, (arSensorValues[1] & mask) > 0 ? 1 : 0);
            mask = mask << 1;
            Sensors.Add((int)SensorValues.INDigital7, (arSensorValues[1] & mask) > 0 ? 1 : 0);
            mask = mask << 1;
            Sensors.Add((int)SensorValues.INDigital8, (arSensorValues[1] & mask) > 0 ? 1 : 0);


            Sensors.Add((int)SensorValues.Analog1, arSensorValues[2] * 0x100 + arSensorValues[3]);
            Sensors.Add((int)SensorValues.Analog2, arSensorValues[4] * 0x100 + arSensorValues[5]);
            Sensors.Add((int)SensorValues.Analog3, arSensorValues[6] * 0x100 + arSensorValues[7]);
            Sensors.Add((int)SensorValues.Analog4, arSensorValues[8] * 0x100 + arSensorValues[9]);

            mask = 0x1;
            Sensors.Add((int)SensorValues.OUTDigital9, (arSensorValues[10] & mask) > 0 ? 1 : 0);
            mask = mask << 1;
            Sensors.Add((int)SensorValues.OUTDigital10, (arSensorValues[10] & mask) > 0 ? 1 : 0);
            mask = 0x1;
            Sensors.Add((int)SensorValues.OUTDigital1, (arSensorValues[11] & mask) > 0 ? 1 : 0);
            mask = mask << 1;
            Sensors.Add((int)SensorValues.OUTDigital2, (arSensorValues[11] & mask) > 0 ? 1 : 0);
            mask = mask << 1;
            Sensors.Add((int)SensorValues.OUTDigital3, (arSensorValues[11] & mask) > 0 ? 1 : 0);
            mask = mask << 1;
            Sensors.Add((int)SensorValues.OUTDigital4, (arSensorValues[11] & mask) > 0 ? 1 : 0);
            mask = mask << 1;
            Sensors.Add((int)SensorValues.OUTDigital5, (arSensorValues[11] & mask) > 0 ? 1 : 0);
            mask = mask << 1;
            Sensors.Add((int)SensorValues.OUTDigital6, (arSensorValues[11] & mask) > 0 ? 1 : 0);
            mask = mask << 1;
            Sensors.Add((int)SensorValues.OUTDigital7, (arSensorValues[11] & mask) > 0 ? 1 : 0);
            mask = mask << 1;
            Sensors.Add((int)SensorValues.OUTDigital8, (arSensorValues[11] & mask) > 0 ? 1 : 0);
            #endregion
            
            //батареи
            Sensors.Add((int)SensorValues.InternalBattery, (paket[49] * 0x100 + paket[50]) / 100);
            Sensors.Add((int)SensorValues.ExternalBattery, (paket[51] * 0x100 + paket[52]) / 100);
            }

            //paket[51] - номер сообщения
            if (paket.Length > 55)//есть дополнительные данные
            {
                //paket[54] 0x00
                
                var from = 55;
                while (from < paket.Length)
                {
                    var length = paket[from + 1];
                    var d = new byte[length + 3];
                    Array.Copy(paket, from, d, 0, length);

                    switch (d[2])
                    {
                        case 1://данные CAN
                            Sensors.Add((int)SensorValues.CANSpeed, d[3] * 0x100 + d[4]);
                            Sensors.Add((int)SensorValues.CANAccelerator, d[5] * 0x100 + d[6]);
                            Sensors.Add((int)SensorValues.CANTotalFuelSpend, d[7] * 0x10000000000 + d[8] * 0x100000000 + d[9] * 0x1000000 + d[10] * 0x10000 + d[11] * 0x100 + (d[12] - 0xf) / 0x10);
                            Sensors.Add((int)SensorValues.CANFuelLevel, d[13] * 0x100 + d[14]);
                            Sensors.Add((int)SensorValues.CANrPM, d[15] * 0x100 + d[16]);
                            Sensors.Add((int)SensorValues.CANAxisNumber, d[17]);
                            Sensors.Add((int)SensorValues.CANNumberOfTires, d[18]);
                            Sensors.Add((int)SensorValues.CANAxisHeight, d[19] + d[20] + (d[21] - 0xf) / 100);
                            Sensors.Add((int)SensorValues.CANTotalNumberOfHours, d[22] * 0x100000000 + d[23] * 0x1000000 + d[24] * 0x10000 + d[25] * 0x100 + d[26] - 0xf + d[27] / 100);
                            Sensors.Add((int)SensorValues.CANTotalRun, d[28] * 0x1000000 + d[29] * 0x10000 + d[30] * 0x100 + d[31] + d[32] - 0xf0 / 10 + d[33] / 1000);
                            Sensors.Add((int)SensorValues.CANRunToService, ((d[34] == 0xe) ? -1 : 1) * (d[35] * 0x10000 + d[36] * 0x100 + d[37]));
                            Sensors.Add((int)SensorValues.CANTemperatureEngine, ((d[38] * 0x100 + d[39] & 0xFFFF) == 0xE0 ? -1 : 1) * ((d[39] & 0X0F)*0x10000 +d[40]*0x100+d[41]));
                            break;
                        case 2:
                            Sensors.Add((int)SensorValues.FirstCounter, d[3] * 0x10000000000 + d[4] * 0x100000000 + d[5] * 0x1000000 + d[6] * 0x10000 + d[7] * 0x100 + d[8]);
                            break;
                        case 3:
                            Sensors.Add((int)SensorValues.SecondCounter, d[3] * 0x10000000000 + d[4] * 0x100000000 + d[5] * 0x1000000 + d[6] * 0x10000 + d[7] * 0x100 + d[8]);
                            break;
                        case 4:
                            break;
                        case 5:
                            Sensors.Add((int)SensorValues.LLSTemperature, d[3]);
                            Sensors.Add((int)SensorValues.LLSCounter, d[4]*0x100 + d[5]);
                            Sensors.Add((int)SensorValues.LLSFrequency, d[6] * 0x100 + d[7]);
                            break;
                        case 6:
                            Sensors.Add((int)SensorValues.CANFuelLevel, d[3]*0x100+d[4]);
                            Sensors.Add((int)SensorValues.CANAxisNumber, d[5]);
                            Sensors.Add((int)SensorValues.CANNumberOfTires, d[6]);
                            Sensors.Add((int)SensorValues.CANAxisHeight, d[7] + d[8] + (d[9] - 0xf0) / 100);
                            break;
                    }

                    from = from + d.Length;
                }
            }
        }

        public override IList GetResponses()
        {
            var result = new ArrayList();

            var mu = new MobilUnit.Unit.MobilUnit
            {
                DeviceID = base.DeviceId,
                cmdType = CmdType.Trace,
                Longitude = Longitude,
                Latitude = Latitude,
                Time = Time,
                Speed = Speed,
                SensorValues = Sensors,
                Course = Course,
                Satellites = Satellites
            };

            mu.CorrectGPS = mu.Satellites >= MobilUnit.Unit.MobilUnit.MinSatellites;
            mu.Properties.Add(DeviceProperty.Protocol, ControllerType.Names.FORT300);
            result.Add(mu);

            var unitInfo = UnitInfo.Factory.Create(UnitInfoPurpose.Firmware);
            unitInfo.DeviceID = DeviceId;
            _manager.FillUnitInfo(unitInfo);

            //подтверждение

            if (unitInfo.Password != null)
            {
                var confirm = new byte[11];
                var pass = Encoding.ASCII.GetBytes(unitInfo.Password).ToList();
                while (pass.Count < 8)
                    pass.Add(0xff);
                Array.Copy(pass.ToArray(), 0, confirm, 0, 8);
                confirm[8] = messageType;
                confirm[9] = messageNumber;
                confirm[10] = 0x00;

                result.Add(new ConfirmPacket(confirm));
            }

            return result;
        }

    }
}
