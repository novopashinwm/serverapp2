﻿namespace RU.NVG.NIKA.Terminal.Zenith
{
	public enum ZenithSensor
	{
		Positive1    = 1,
		Positive2    = 2,
		Negative3    = 3,
		Negative4    = 4,
		Positive5    = 5,
		Positive6    = 6,
		Positive7    = 7,
		Positive8    = 8,
		Alarm        = 9,
		Reserved     = 10,
		Negative1    = 11,
		Negative2    = 12,
		Analogue1    = 21,
		Analogue2    = 22,
		Analogue3    = 23,
		Analogue4    = 24,
		Analogue5    = 25,
		Analogue6    = 26,
		ReservePower = 27,
		OuterPower   = 28,
		/// <summary> Идентификатор RFID-метки </summary>
		RFID         = 29,
	}
}