﻿using System;
using System.Collections.Generic;
using System.Text;
using FORIS.TSS.Common;

namespace RU.NVG.NIKA.Terminal.Zenith
{
	class ZenithReader
	{
		private readonly DataReader _reader;

		public ZenithReader(byte[] data)
		{
			_reader = new DataReader(data);
		}

		public bool CheckControlSum(int length)
		{
			/* Код на Delphi
				for b1:=0 to 22 do
					 i1:=i1 xor ((ord(str1[pstr+b1*2+1]))*256 + (ord(str1[pstr+b1*2+2])));
				
				receivedControlSum:=(ord(str1[pstr+47])-32)*10000+(ord(str1[pstr+48])-32)*100+(ord(str1[pstr+49])-32);
			 */

			ushort calculated = 0;
			for (var i = 0; i != length / 2; ++i)
			{
				var b1 = _reader.ReadByte();
				var b2 = _reader.ReadByte();

				calculated ^= (ushort)((b1 << 8) + b2);
			}

			const int controlSumFieldLength = 3;
			var received = (ushort) ReadNumber(controlSumFieldLength);

			_reader.Rewind(length+controlSumFieldLength);

			return received == calculated;
		}

		public void Skip(int count)
		{
			_reader.Skip(count);
		}
		
		public void SkipFrameType()
		{
			_reader.Skip(1); //Тип кадра
		}

		public DateTime ReadDateTime()
		{
			var hours = ReadByte();
			var minutes = ReadByte();
			var seconds = ReadByte();

			var day = ReadByte();
			var month = ReadByte();
			var year = ReadByte();
			
			return new DateTime(2000 + year, month, day, hours, minutes, seconds, DateTimeKind.Utc);
		}

		public enum CoordinateStatus
		{
			None,
			NorthEast = 5,
			SouthEast = 6,
			NorthWest = 7,
			SouthWest = 8
		}

		public CoordinateStatus ReadCoordinateStatus()
		{
			return (CoordinateStatus) ReadByte();
		}

		public decimal ReadLatitude()
		{
			var degrees = ReadByte();
			var minutes = ReadByte();
			var minuteParts = ReadNumber(2);

			return degrees + minutes/60.0m + minuteParts/60m/10000m;
		}

		public decimal ReadLongitude()
		{
			var degrees = ReadNumber(2);
			var minutes = ReadByte();
			var minuteParts = ReadNumber(2);

			return degrees + minutes / 60.0m + minuteParts / 60m / 10000m;
		}

		public decimal ReadSpeedMph()
		{
			return ReadNumber(2) / 10m;
		}

		public int ReadAzimuth()
		{
			return ReadNumber(2);
		}

		public string ReadDeviceId()
		{
			var sb = new StringBuilder();
			for (var i = 0; i != 5; ++i)
			{
				var b = ReadByte();
				sb.Append(b / 10);
				sb.Append(b%10);
			}
			return sb.ToString();
		}

		private const byte B0 = 0x01;
		private const byte B1 = B0 << 1;
		private const byte B2 = B1 << 1;
		private const byte B3 = B2 << 1;

		public IEnumerable<KeyValuePair<ZenithSensor, long>> ReadSensors78PartOne()
		{
			var b = ReadByte();

			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Positive1, (b & B0) != 0 ? 1 : 0);
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Positive2, (b & B1) != 0 ? 1 : 0);
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Negative1, (b & B2) != 0 ? 1 : 0);
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Negative2, (b & B3) != 0 ? 1 : 0);

			b = ReadByte();

			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Alarm, (b & B0) != 0 ? 1 : 0);
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Reserved, (b & B1) != 0 ? 1 : 0);

			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.OuterPower, ReadNumber(2));

			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Analogue3, ReadNumber(2));
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Analogue4, ReadNumber(2));

			//Идентификатор RFID-метки совмещён с 3 и 4 аналоговыми датчиками
			_reader.Rewind(4);

			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.RFID, ReadNumber(4));
		}

		public IEnumerable<KeyValuePair<ZenithSensor, long>> ReadSensors78PartTwo()
		{
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Analogue1, ReadNumber(2));
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Analogue2, ReadNumber(2));
		}

		public IEnumerable<KeyValuePair<ZenithSensor, long>> ReadSensors56()
		{
			var b = ReadByte();

			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Positive1, (b & B0) != 0 ? 1 : 0);
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Positive2, (b & B1) != 0 ? 1 : 0);
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Negative3, (b & B2) != 0 ? 1 : 0);
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Negative4, (b & B3) != 0 ? 1 : 0);

			b = ReadByte();

			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Positive5, (b & B0) != 0 ? 1 : 0);
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Positive6, (b & B1) != 0 ? 1 : 0);
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Positive7, (b & B2) != 0 ? 1 : 0);
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Positive8, (b & B3) != 0 ? 1 : 0);

			b = ReadByte();

			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Alarm, (b & B0) != 0 ? 1 : 0);
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Reserved, (b & B1) != 0 ? 1 : 0);
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Negative1, (b & B2) != 0 ? 1 : 0);
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Negative2, (b & B3) != 0 ? 1 : 0);

			_reader.Skip(); //outputs

			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Analogue1, ReadNumber(2));
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Analogue2, ReadNumber(2));
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Analogue3, ReadNumber(2));
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Analogue4, ReadNumber(2));
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Analogue5, ReadNumber(2));
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.Analogue6, ReadNumber(2));
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.ReservePower, ReadNumber(2));
			yield return new KeyValuePair<ZenithSensor, long>(ZenithSensor.OuterPower, ReadNumber(2));
		}

		public int ReadOdometer()
		{
			return ReadNumber(4);
		}

		public byte ReadVersion()
		{
			return ReadByte();
		}

		public void SkipFrameTailer()
		{
			_reader.Skip(2);
		}

		public int RemainingBytesCount
		{
			get
			{
				return _reader.RemainingBytesCount;
			}
		}

		public byte[] GetRemainingBytes()
		{
			if (!_reader.Any())
				return null;
			return _reader.ReadBytes(_reader.RemainingBytesCount);
		}

		private byte ReadByte()
		{
			return (byte) (_reader.ReadByte() - 0x20);
		}

		public int ReadNumber(int count)
		{
			int result = 0;
			while (count != 0)
			{
				--count;
				result *= 100;
				result += ReadByte();
			}
			return result;
		}

		public void SkipControlSum()
		{
			_reader.Skip(3);
		}

		public void Rewind(int count)
		{
			_reader.Rewind(count);
		}
	}
}