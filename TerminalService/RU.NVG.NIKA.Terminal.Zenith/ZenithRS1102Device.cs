﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace RU.NVG.NIKA.Terminal.Zenith
{
	public class ZenithRS1102Device : Device
	{
		internal const string ProtocolName = "RS-1102 \"Zenith\"";
		internal static class DeviceNames
		{
			internal const string ZenithRs1102 = "RS-1102 \"Zenith\"";
		}
		public ZenithRS1102Device() : base(typeof(ZenithSensor), new[]
		{
			DeviceNames.ZenithRs1102,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			if (data.Length < MinFrameLength)
				return false;

			if (data[0] != 0xAF ||
				data[1] != 0x84)
				return false;

			var frameId = data[2] - 0x20;
			var expectedPacketLength = GetExpectedFrameLength(frameId);

			if (data[expectedPacketLength - 2] != 0x0D ||
				data[expectedPacketLength - 1] != 0x0A)
				return false;

			return true;
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			var reader = new ZenithReader(data);
			var result = new ArrayList();

			while (MinFrameLength <= reader.RemainingBytesCount)
			{
				//TODO: расчёт контрольной суммы зависит от типа фрейма
				reader.Skip(2); //Заголовок
				var frameId = reader.ReadNumber(1); //Идентификатор фрейма
				int expectedFrameLength = GetExpectedFrameLength(frameId);

				const int headerWithFrameIdLength = 3;

				reader.Rewind(headerWithFrameIdLength);
				if (reader.RemainingBytesCount < expectedFrameLength)
					break;

				if (!reader.CheckControlSum(expectedFrameLength - 3 /*Длина контрольной суммы*/- 2 /*Конец кадра*/))
				{
					bufferRest = null;
					return null;
				}

				reader.Skip(headerWithFrameIdLength); //Заголовок и идентификатор фрейма
				reader.SkipFrameType();

				switch (frameId)
				{
					case 5:
					case 6:
						result.Add(ReadFrame56(reader));
						if (frameId == 6)
							result.Add(СonfirmPacket);
						break;
					case 7:
					case 8:
						result.Add(ReadFrame78(reader));
						if (frameId == 8)
							result.Add(СonfirmPacket);
						break;
				}

				reader.SkipControlSum();
				reader.SkipFrameTailer();
			}

			bufferRest = reader.GetRemainingBytes();

			return result;
		}
		private const int MinFrameLength = 51;
		private static readonly ConfirmPacket СonfirmPacket = new ConfirmPacket { Data = new byte[] { 0x54, 0x53, 0x41, 0x0D, 0x0A } };
		private int GetExpectedFrameLength(int frameId)
		{
			switch (frameId)
			{
				case 5:
				case 6:
					return 59;
				case 7:
				case 8:
					return 51;
				default:
					throw new ArgumentOutOfRangeException("frameId", frameId, @"Frame id is not supported");
			}
		}
		private IMobilUnit ReadFrame78(ZenithReader reader)
		{
			var mu = UnitReceived(ProtocolName);
			mu.Time = TimeHelper.GetSecondsFromBase(reader.ReadDateTime());

			var coordinateStatus = reader.ReadCoordinateStatus();

			mu.Latitude = (double)reader.ReadLatitude();
			mu.Longitude = (double)reader.ReadLongitude();

			if (coordinateStatus == ZenithReader.CoordinateStatus.NorthWest ||
				coordinateStatus == ZenithReader.CoordinateStatus.SouthWest)
			{
				mu.Longitude = -mu.Longitude;
			}

			if (coordinateStatus == ZenithReader.CoordinateStatus.SouthEast ||
				coordinateStatus == ZenithReader.CoordinateStatus.SouthWest)
			{
				mu.Latitude = -mu.Latitude;
			}

			mu.CorrectGPS = coordinateStatus != ZenithReader.CoordinateStatus.None;

			var speedMph = reader.ReadSpeedMph();
			mu.Speed = (int)(speedMph * 1.609344m);
			mu.Course = reader.ReadAzimuth();

			var deviceIdString = reader.ReadDeviceId();
			mu.DeviceID = deviceIdString;

			mu.SensorValues = new Dictionary<int, long>();
			foreach (var pair in reader.ReadSensors78PartOne())
			{
				mu.SensorValues.Add((int)pair.Key, pair.Value);
			}

			mu.Run = reader.ReadOdometer();

			foreach (var pair in reader.ReadSensors78PartTwo())
			{
				mu.SensorValues.Add((int)pair.Key, pair.Value);
			}

			mu.Firmware = reader.ReadVersion().ToString(CultureInfo.InvariantCulture);

			return mu;
		}
		private IMobilUnit ReadFrame56(ZenithReader reader)
		{
			var mu = UnitReceived(ProtocolName);
			mu.Time = TimeHelper.GetSecondsFromBase(reader.ReadDateTime());

			var coordinateStatus = reader.ReadCoordinateStatus();

			mu.Latitude = (double)reader.ReadLatitude();
			mu.Longitude = (double)reader.ReadLongitude();

			if (coordinateStatus == ZenithReader.CoordinateStatus.NorthWest ||
				coordinateStatus == ZenithReader.CoordinateStatus.SouthWest)
			{
				mu.Longitude = -mu.Longitude;
			}

			if (coordinateStatus == ZenithReader.CoordinateStatus.SouthEast ||
				coordinateStatus == ZenithReader.CoordinateStatus.SouthWest)
			{
				mu.Latitude = -mu.Latitude;
			}

			mu.CorrectGPS = coordinateStatus != ZenithReader.CoordinateStatus.None;

			var speedMph = reader.ReadSpeedMph();
			mu.Speed = (int)(speedMph * 1.609344m);
			mu.Course = reader.ReadAzimuth();

			var deviceIdString = reader.ReadDeviceId();
			mu.DeviceID = deviceIdString;

			mu.SensorValues = new Dictionary<int, long>();
			foreach (var pair in reader.ReadSensors56())
			{
				mu.SensorValues.Add((int)pair.Key, pair.Value);
			}

			mu.Run = reader.ReadOdometer();
			mu.Firmware = reader.ReadVersion().ToString(CultureInfo.InvariantCulture);

			return mu;
		}
	}
}