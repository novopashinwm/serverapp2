﻿using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO.Historical;

namespace FORIS.TSS.BusinessLogic.Terminal.Data
{
	public class DataForLocator
	{
		public int                     VehicleId;
		public List<WlanRecord>        Wlans;
		public List<CellNetworkRecord> Cells;
	}
}