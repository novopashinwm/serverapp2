﻿using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.BusinessLogic.Terminal.Data
{
	public struct Position
	{
		public Position(IMobilUnit mu)
		{
			VehicleID = mu.Unique;
			Time      = mu.Time;
			Longitude = mu.Longitude;
			Latitude  = mu.Latitude;
			Speed     = mu.Speed;
			Course    = mu.Course;
		}

		public Position(int vehicleID, int time, double longitude, double latitude, int? speed, int? course)
		{
			VehicleID = vehicleID;
			Time      = time;
			Longitude = longitude;
			Latitude  = latitude;
			Speed     = speed;
			Course    = course;
		}

		public readonly int    VehicleID;
		public readonly double Latitude;
		public readonly double Longitude;
		public readonly int    Time;
		public readonly int?   Speed;
		public readonly int?   Course;
	}
}