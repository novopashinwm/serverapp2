﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using FORIS.TSS.BusinessLogic.DTO.Historical;

namespace FORIS.TSS.BusinessLogic.Terminal.Data
{
	public class DataForNetmonitoring
	{
		/// <remarks>Без разделителей, в верхнем регистре</remarks>
		public static readonly string UuidFormat = "N";

		public Guid Uuid;
		public ILookup<int, CellNetworkRecord> Cells;
		public ILookup<int, WlanRecord> Wlans;
		public List<Position> Positions;

		public struct Position
		{
			public int LogTime;
			public decimal Lat;
			public decimal Lng;
			public int Speed;
			public int Course;
		}

		public XDocument GetXmlForYandex(string yandexApiKey)
		{
			return new XDocument(
				new XElement(
					"wifipool",
					new XAttribute("uuid", Uuid.ToString(UuidFormat)),
					new XElement("api_key", yandexApiKey),
					Positions.Select(ToChunkXElement)));
		}

		private XElement ToChunkXElement(Position position)
		{
			var chunk = new XElement(
				"chunk",
				new XAttribute("type", "normal"),
				new XAttribute("time",
							   position.LogTime.ToString(CultureInfo.InvariantCulture)),
				new XElement("gps",
							 new XAttribute("lat", position.Lat),
							 new XAttribute("lng", position.Lng),
							 new XAttribute("speed", position.Speed),
							 new XAttribute("course", position.Course)));

			var wlans = Wlans[position.LogTime].ToArray();
			if (wlans.Any())
			{
				chunk.Add(
					new XElement(
						"bssids",
						wlans.Select(wlan => ToXElement(wlan, position))));
			}

			var cells = Cells[position.LogTime].ToArray();
			if (cells.Any())
			{
				chunk.Add(
					new XElement(
						"cellinfos",
						cells.Select(ToXElement)));
			}

			return chunk;
		}

		private static XElement ToXElement(WlanRecord wlan, Position position)
		{
			var bssid = new XElement(
				"bssid",
				new XAttribute("name", wlan.WlanSSID),
				new XAttribute("sigstr",
							   "-" +
							   XmlConvert.ToString(
								   wlan.SignalStrength)),
				new XAttribute("age",
							   XmlConvert.ToString((wlan.LogTime -
													position.
														LogTime) *
												   1000)));

			if (wlan.ChannelNumber != null)
				bssid.Add(new XAttribute("channel",
										 wlan.ChannelNumber));

			bssid.Value = wlan.WlanMacAddress;

			return bssid;
		}

		private static XElement ToXElement(CellNetworkRecord cell)
		{
			var cellInfo = new XElement("cellinfo", new XAttribute("cellid", cell.CellID), new XAttribute("lac", cell.LAC),
										new XAttribute("operatorid", cell.NetworkCode),
										new XAttribute("countrycode", cell.CountryCode)
										);

			if (cell.SignalStrength > 0)
				cellInfo.Add(new XAttribute("sigstr", "-" + XmlConvert.ToString(cell.SignalStrength)));

			return cellInfo;
		}
	}
}