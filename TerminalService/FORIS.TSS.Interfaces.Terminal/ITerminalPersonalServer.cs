﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.BusinessLogic.Terminal
{
	public interface ITerminalPersonalServer :
		IPersonalServer
	{
		/// <summary> Событие готовности данных для клиента, после получения этого сообщения, клиент может забрать предназначенные для него данные </summary>
		event AnonymousEventHandler<EventArgs> DataReadyEvent;
		/// <summary> Забирает позиции накопленные в TerminalPersonalServer </summary>
		/// <returns> Список позиций </returns>
		IMobilUnit[] GetPositions();
		/// <summary> Возвращает отсортированный по времени список последних позиций из БД за указанный период времени </summary>
		/// <param name="begTime"> начало периода. UTC </param>
		/// <param name="endTime"> конец периода. UTC </param>
		/// <param name="controllerId"> идентификатор контроллера </param>
		/// <param name="interval"> интервал между позициями в секундах </param>
		/// <param name="count"> максимальное количество возвращаемых позиций </param>
		/// <returns> отсортированный по времени список последних позиций </returns>
		SortedList<int, IMobilUnit> GetLogFromDB(DateTime begTime, DateTime endTime, int controllerId, int interval, int count);
		/// <summary> Доступ к интерфейсу менеджера терминалов, работающего внутри сервера терминалов </summary>
		/// <returns></returns>
		/// <remarks> доступ к этому интерфейсу для работы TaskProcessor и FirmwareTaskProcessor </remarks>
		[Obsolete("Наличие доступа к ядру сервера терминалов - это ужасно", false)]
		ITerminalManager GetTerminalManager();
		/// <summary> Отсылка СМС </summary>
		/// <param name="target"></param>
		/// <param name="text"></param>
		void SendSMS(string target, string text);
		/// <summary> Отсылка СМС </summary>
		/// <param name="target"></param>
		/// <param name="data"></param>
		void SendSMS(string target, byte[] data);
		/// <summary> Перенаправление команды конкретному терминалу </summary>
		/// <param name="command"> команда для мобильного объекта </param>
		void SendCommand(IStdCommand command);
		/// <summary> Событие наличия сообщений у терминала </summary>
		event AnonymousEventHandler<BulkNotifyEventArgs> NotifyEvent;
	}
}