﻿using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.Terminal.Data;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.BusinessLogic.Terminal
{
	public interface ITerminalDatabaseManager
	{
		void AddMonitoreeLog(IList<IMobilUnit> mobilUnits, int storageId);
		DataForNetmonitoring GetDataForNetmonitoring();
		DataForLocator GetDataForLocator();
		IEnumerable<Position> GetLastPositions();
		string GetConstant(string constant);
		SortedList<int, IMobilUnit> GetLogForController(int from, int to, int id, int interval, int count);
	}
}