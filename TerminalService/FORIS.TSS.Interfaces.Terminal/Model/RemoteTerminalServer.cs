﻿namespace FORIS.TSS.BusinessLogic.Terminal.Model
{
	public class RemoteTerminalServer
	{
		public int    Id;
		public string Name;
		public bool   Enabled;
		public string Config;
		public string DeviceId;
	}
}