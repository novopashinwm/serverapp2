﻿using System.Data;

namespace FORIS.TSS.BusinessLogic.Terminal.Schema
{
	///<summary>
	/// Class for creating table schema on client
	///</summary>
	public class DatabaseSchema : DatabaseSchemaBase
	{
		/// <summary>
		/// Initialises list of tables
		/// </summary>
		static DatabaseSchema()
		{
			tables.Add("CONTROLLER_TYPE");
			tables.Add("MEDIA_TYPE");
			tables.Add("MEDIA");
			tables.Add("MEDIA_ACCEPTORS");
			tables.Add("MONITOREE_LOG");
			tables.Add("OPERATOR");
			tables.Add("SESSION");
			tables.Add("sysdiagrams");
			tables.Add("TRAIL");
			tables.Add("VEHICLE");
			tables.Add("CONTROLLER");
			tables.Add("CONTROLLER_INFO");
			h_tables.Add("H_CONTROLLER_TYPE");
			h_tables.Add("H_MEDIA_TYPE");
			h_tables.Add("H_MEDIA");
			h_tables.Add("H_MEDIA_ACCEPTORS");
			h_tables.Add("H_OPERATOR");
			h_tables.Add("H_sysdiagrams");
			h_tables.Add("H_VEHICLE");
			h_tables.Add("H_CONTROLLER");
			h_tables.Add("H_CONTROLLER_INFO");
		}
		///<summary>
		/// [CONTROLLER]
		///</summary>
		public static void MakeTable_Controller(DataSet dataset)
		{
			if (dataset.Tables.Contains("CONTROLLER")) return;
			DataTable table = dataset.Tables.Add("CONTROLLER");
			table.Columns.Add("CONTROLLER_ID", typeof(System.Int32)); // int
			table.Columns.Add("CONTROLLER_TYPE_ID", typeof(System.Int32)); // int
			table.Columns.Add("VEHICLE_ID", typeof(System.Int32)); // int
			table.Columns.Add("PHONE", typeof(System.String)); // varchar
			table.Columns.Add("NUMBER", typeof(System.Int32)); // int
			table.Columns.Add("FIRMWARE", typeof(System.String)); // varchar
		}
		///<summary>
		/// PK_CONTROLLER
		///</summary>
		public static void MakePK_Controller(DataSet dataset)
		{
			DataTable table = dataset.Tables["CONTROLLER"];
			if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
			table.PrimaryKey = new DataColumn[]{
				table.Columns["CONTROLLER_ID"]
			}
			;
		}
		///<summary>
		/// CONTROLLER
		///</summary>
		public static void MakeIdentities_Controller(DataSet dataset)
		{
			DataTable table = dataset.Tables["CONTROLLER"];
			table.Columns["CONTROLLER_ID"].AutoIncrement = true;
			table.Columns["CONTROLLER_ID"].AutoIncrementSeed = -1;
			table.Columns["CONTROLLER_ID"].AutoIncrementStep = -1;
		}
		///<summary>
		/// [CONTROLLER_INFO]
		///</summary>
		public static void MakeTable_ControllerInfo(DataSet dataset)
		{
			if (dataset.Tables.Contains("CONTROLLER_INFO")) return;
			DataTable table = dataset.Tables.Add("CONTROLLER_INFO");
			table.Columns.Add("CONTROLLER_ID", typeof(System.Int32)); // int
			table.Columns.Add("TIME", typeof(System.DateTime)); // smalldatetime
			table.Columns.Add("DEVICE_ID", typeof(System.Byte[])); // varbinary
			table.Columns.Add("FIRMWARE", typeof(System.Byte[])); // varbinary
			table.Columns.Add("FUEL_LVAL", typeof(System.Int32)); // int
			table.Columns.Add("FUEL_UVAL", typeof(System.Int32)); // int
			table.Columns.Add("FUEL_FACTOR", typeof(System.Single)); // real
			table.Columns.Add("SERVER_IP", typeof(System.String)); // varchar
			table.Columns.Add("SERVER_PORT", typeof(System.Int32)); // int
			table.Columns.Add("TIME_ZONE", typeof(System.Byte)); // tinyint
			table.Columns.Add("VOICE_PHONE", typeof(System.String)); // varchar
			table.Columns.Add("VOICE_PHONE1", typeof(System.String)); // varchar
			table.Columns.Add("VOICE_PHONE2", typeof(System.String)); // varchar
			table.Columns.Add("DATA_PHONE", typeof(System.String)); // varchar
			table.Columns.Add("DATA_PHONE1", typeof(System.String)); // varchar
			table.Columns.Add("INTERVAL", typeof(System.Byte)); // tinyint
			table.Columns.Add("TCP", typeof(System.Boolean)); // bit
			table.Columns.Add("INET_ENTRY_POINT", typeof(System.String)); // varchar
			table.Columns.Add("LOGIN", typeof(System.String)); // varchar
			table.Columns.Add("PASSWORD", typeof(System.String)); // varchar
		}
		///<summary>
		/// PK_CONTROLLER_INFO
		///</summary>
		public static void MakePK_ControllerInfo(DataSet dataset)
		{
			DataTable table = dataset.Tables["CONTROLLER_INFO"];
			if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
			table.PrimaryKey = new DataColumn[]{
				table.Columns["CONTROLLER_ID"]
			}
			;
		}
		///<summary>
		/// CONTROLLER_INFO
		///</summary>
		public static void MakeIdentities_ControllerInfo(DataSet dataset)
		{
			DataTable table = dataset.Tables["CONTROLLER_INFO"];
			table.Columns["CONTROLLER_ID"].AutoIncrement = true;
			table.Columns["CONTROLLER_ID"].AutoIncrementSeed = -1;
			table.Columns["CONTROLLER_ID"].AutoIncrementStep = -1;
		}
		///<summary>
		/// [CONTROLLER_TYPE]
		///</summary>
		public static void MakeTable_ControllerType(DataSet dataset)
		{
			if (dataset.Tables.Contains("CONTROLLER_TYPE")) return;
			DataTable table = dataset.Tables.Add("CONTROLLER_TYPE");
			table.Columns.Add("CONTROLLER_TYPE_ID", typeof(System.Int32)); // int
			table.Columns.Add("TYPE_NAME", typeof(System.String)); // nvarchar
		}
		///<summary>
		/// PK_CONTROLLER_TYPE
		///</summary>
		public static void MakePK_ControllerType(DataSet dataset)
		{
			DataTable table = dataset.Tables["CONTROLLER_TYPE"];
			if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
			table.PrimaryKey = new DataColumn[]{
				table.Columns["CONTROLLER_TYPE_ID"]
			}
			;
		}
		///<summary>
		/// CONTROLLER_TYPE
		///</summary>
		public static void MakeIdentities_ControllerType(DataSet dataset)
		{
			DataTable table = dataset.Tables["CONTROLLER_TYPE"];
			table.Columns["CONTROLLER_TYPE_ID"].AutoIncrement = true;
			table.Columns["CONTROLLER_TYPE_ID"].AutoIncrementSeed = -1;
			table.Columns["CONTROLLER_TYPE_ID"].AutoIncrementStep = -1;
		}
		///<summary>
		/// [MEDIA]
		///</summary>
		public static void MakeTable_Media(DataSet dataset)
		{
			if (dataset.Tables.Contains("MEDIA")) return;
			DataTable table = dataset.Tables.Add("MEDIA");
			table.Columns.Add("MEDIA_ID", typeof(System.Int32)); // int
			table.Columns.Add("NAME", typeof(System.String)); // nvarchar
			table.Columns.Add("TYPE", typeof(System.Byte)); // tinyint
		}
		///<summary>
		/// PK_MEDIA
		///</summary>
		public static void MakePK_Media(DataSet dataset)
		{
			DataTable table = dataset.Tables["MEDIA"];
			if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
			table.PrimaryKey = new DataColumn[]{
				table.Columns["MEDIA_ID"]
			}
			;
		}
		///<summary>
		/// MEDIA
		///</summary>
		public static void MakeIdentities_Media(DataSet dataset)
		{
			DataTable table = dataset.Tables["MEDIA"];
			table.Columns["MEDIA_ID"].AutoIncrement = true;
			table.Columns["MEDIA_ID"].AutoIncrementSeed = -1;
			table.Columns["MEDIA_ID"].AutoIncrementStep = -1;
		}
		///<summary>
		/// [MEDIA_ACCEPTORS]
		///</summary>
		public static void MakeTable_MediaAcceptors(DataSet dataset)
		{
			if (dataset.Tables.Contains("MEDIA_ACCEPTORS")) return;
			DataTable table = dataset.Tables.Add("MEDIA_ACCEPTORS");
			table.Columns.Add("MA_ID", typeof(System.Int32)); // int
			table.Columns.Add("MEDIA_ID", typeof(System.Int32)); // int
			table.Columns.Add("INITIALIZATION", typeof(System.String)); // nvarchar
			table.Columns.Add("DESCRIPTION", typeof(System.String)); // nvarchar
		}
		///<summary>
		/// PK_MEDIA_ACCEPTORS
		///</summary>
		public static void MakePK_MediaAcceptors(DataSet dataset)
		{
			DataTable table = dataset.Tables["MEDIA_ACCEPTORS"];
			if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
			table.PrimaryKey = new DataColumn[]{
				table.Columns["MA_ID"]
			}
			;
		}
		///<summary>
		/// MEDIA_ACCEPTORS
		///</summary>
		public static void MakeIdentities_MediaAcceptors(DataSet dataset)
		{
			DataTable table = dataset.Tables["MEDIA_ACCEPTORS"];
			table.Columns["MA_ID"].AutoIncrement = true;
			table.Columns["MA_ID"].AutoIncrementSeed = -1;
			table.Columns["MA_ID"].AutoIncrementStep = -1;
		}
		///<summary>
		/// [MEDIA_TYPE]
		///</summary>
		public static void MakeTable_MediaType(DataSet dataset)
		{
			if (dataset.Tables.Contains("MEDIA_TYPE")) return;
			DataTable table = dataset.Tables.Add("MEDIA_TYPE");
			table.Columns.Add("ID", typeof(System.Byte)); // tinyint
			table.Columns.Add("Name", typeof(System.String)); // nvarchar
		}
		///<summary>
		/// PK_MEDIA_TYPE
		///</summary>
		public static void MakePK_MediaType(DataSet dataset)
		{
			DataTable table = dataset.Tables["MEDIA_TYPE"];
			if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
			table.PrimaryKey = new DataColumn[]{
				table.Columns["ID"]
			}
			;
		}
		///<summary>
		/// MEDIA_TYPE
		///</summary>
		public static void MakeIdentities_MediaType(DataSet dataset)
		{
			DataTable table = dataset.Tables["MEDIA_TYPE"];
			table.Columns["ID"].AutoIncrement = true;
			table.Columns["ID"].AutoIncrementSeed = -1;
			table.Columns["ID"].AutoIncrementStep = -1;
		}
		///<summary>
		/// [MONITOREE_LOG]
		///</summary>
		public static void MakeTable_MonitoreeLog(DataSet dataset)
		{
			if (dataset.Tables.Contains("MONITOREE_LOG")) return;
			DataTable table = dataset.Tables.Add("MONITOREE_LOG");
			table.Columns.Add("MONITOREE_ID", typeof(System.Int32)); // int
			table.Columns.Add("LOG_TIME", typeof(System.Int32)); // int
			table.Columns.Add("X", typeof(System.Single)); // real
			table.Columns.Add("Y", typeof(System.Single)); // real
			table.Columns.Add("SPEED", typeof(System.Byte)); // tinyint
			table.Columns.Add("MEDIA", typeof(System.Byte)); // tinyint
			table.Columns.Add("V", typeof(System.Byte)); // tinyint
			table.Columns.Add("V1", typeof(System.Byte)); // tinyint
			table.Columns.Add("V2", typeof(System.Byte)); // tinyint
			table.Columns.Add("V3", typeof(System.Byte)); // tinyint
			table.Columns.Add("V4", typeof(System.Byte)); // tinyint
			table.Columns.Add("SATELLITES", typeof(System.Byte)); // tinyint
			table.Columns.Add("FIRMWARE", typeof(System.Int16)); // smallint
		}
		///<summary>
		/// [OPERATOR]
		///</summary>
		public static void MakeTable_Operator(DataSet dataset)
		{
			if (dataset.Tables.Contains("OPERATOR")) return;
			DataTable table = dataset.Tables.Add("OPERATOR");
			table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
			table.Columns.Add("NAME", typeof(System.String)); // nvarchar
			table.Columns.Add("LOGIN", typeof(System.String)); // nvarchar
			table.Columns.Add("PASSWORD", typeof(System.String)); // nvarchar
			table.Columns.Add("PHONE", typeof(System.String)); // nvarchar
			table.Columns.Add("EMAIL", typeof(System.String)); // nvarchar
		}
		///<summary>
		/// PK_OPERATOR
		///</summary>
		public static void MakePK_Operator(DataSet dataset)
		{
			DataTable table = dataset.Tables["OPERATOR"];
			if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
			table.PrimaryKey = new DataColumn[]{
				table.Columns["OPERATOR_ID"]
			}
			;
		}
		///<summary>
		/// OPERATOR
		///</summary>
		public static void MakeIdentities_Operator(DataSet dataset)
		{
			DataTable table = dataset.Tables["OPERATOR"];
			table.Columns["OPERATOR_ID"].AutoIncrement = true;
			table.Columns["OPERATOR_ID"].AutoIncrementSeed = -1;
			table.Columns["OPERATOR_ID"].AutoIncrementStep = -1;
		}
		///<summary>
		/// [SESSION]
		///</summary>
		public static void MakeTable_Session(DataSet dataset)
		{
			if (dataset.Tables.Contains("SESSION")) return;
			DataTable table = dataset.Tables.Add("SESSION");
			table.Columns.Add("SESSION_ID", typeof(System.Int32)); // int
			table.Columns.Add("OPERATOR_ID", typeof(System.Int32)); // int
			table.Columns.Add("HOST_IP", typeof(System.String)); // nvarchar
			table.Columns.Add("HOST_NAME", typeof(System.String)); // nvarchar
			table.Columns.Add("SESSION_START", typeof(System.DateTime)); // datetime
			table.Columns.Add("SESSION_END", typeof(System.DateTime)); // datetime
			table.Columns.Add("CLIENT_VERSION", typeof(System.String)); // nvarchar
		}
		///<summary>
		/// PK_SESSION
		///</summary>
		public static void MakePK_Session(DataSet dataset)
		{
			DataTable table = dataset.Tables["SESSION"];
			if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
			table.PrimaryKey = new DataColumn[]{
				table.Columns["SESSION_ID"]
			}
			;
		}
		///<summary>
		/// SESSION
		///</summary>
		public static void MakeIdentities_Session(DataSet dataset)
		{
			DataTable table = dataset.Tables["SESSION"];
			table.Columns["SESSION_ID"].AutoIncrement = true;
			table.Columns["SESSION_ID"].AutoIncrementSeed = -1;
			table.Columns["SESSION_ID"].AutoIncrementStep = -1;
		}
		///<summary>
		/// [sysdiagrams]
		///</summary>
		public static void MakeTable_Sysdiagrams(DataSet dataset)
		{
			if (dataset.Tables.Contains("sysdiagrams")) return;
			DataTable table = dataset.Tables.Add("sysdiagrams");
			table.Columns.Add("name", typeof(System.String)); // nvarchar
			table.Columns.Add("principal_id", typeof(System.Int32)); // int
			table.Columns.Add("diagram_id", typeof(System.Int32)); // int
			table.Columns.Add("version", typeof(System.Int32)); // int
			table.Columns.Add("definition", typeof(System.Byte[])); // varbinary
		}
		///<summary>
		/// PK__sysdiagrams__440B1D61
		///</summary>
		public static void MakePK_Sysdiagrams(DataSet dataset)
		{
			DataTable table = dataset.Tables["sysdiagrams"];
			if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
			table.PrimaryKey = new DataColumn[]{
				table.Columns["diagram_id"]
			}
			;
		}
		///<summary>
		/// sysdiagrams
		///</summary>
		public static void MakeIdentities_Sysdiagrams(DataSet dataset)
		{
			DataTable table = dataset.Tables["sysdiagrams"];
			table.Columns["diagram_id"].AutoIncrement = true;
			table.Columns["diagram_id"].AutoIncrementSeed = -1;
			table.Columns["diagram_id"].AutoIncrementStep = -1;
		}
		///<summary>
		/// [TRAIL]
		///</summary>
		public static void MakeTable_Trail(DataSet dataset)
		{
			if (dataset.Tables.Contains("TRAIL")) return;
			DataTable table = dataset.Tables.Add("TRAIL");
			table.Columns.Add("TRAIL_ID", typeof(System.Int32)); // int
			table.Columns.Add("SESSION_ID", typeof(System.Int32)); // int
			table.Columns.Add("TRAIL_TIME", typeof(System.DateTime)); // datetime
		}
		///<summary>
		/// PK_TRAIL
		///</summary>
		public static void MakePK_Trail(DataSet dataset)
		{
			DataTable table = dataset.Tables["TRAIL"];
			if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
			table.PrimaryKey = new DataColumn[]{
				table.Columns["TRAIL_ID"]
			}
			;
		}
		///<summary>
		/// TRAIL
		///</summary>
		public static void MakeIdentities_Trail(DataSet dataset)
		{
			DataTable table = dataset.Tables["TRAIL"];
			table.Columns["TRAIL_ID"].AutoIncrement = true;
			table.Columns["TRAIL_ID"].AutoIncrementSeed = -1;
			table.Columns["TRAIL_ID"].AutoIncrementStep = -1;
		}
		///<summary>
		/// [VEHICLE]
		///</summary>
		public static void MakeTable_Vehicle(DataSet dataset)
		{
			if (dataset.Tables.Contains("VEHICLE")) return;
			DataTable table = dataset.Tables.Add("VEHICLE");
			table.Columns.Add("VEHICLE_ID", typeof(System.Int32)); // int
			table.Columns.Add("DEPARTMENT_ID", typeof(System.Int32)); // int
			table.Columns.Add("GARAGE_NUMBER", typeof(System.String)); // nvarchar
			table.Columns.Add("PUBLIC_NUMBER", typeof(System.String)); // nvarchar
			table.Columns.Add("VEHICLE_MAKE_ID", typeof(System.Int32)); // int
			table.Columns.Add("PRODUCE_DATE", typeof(System.DateTime)); // datetime
		}
		///<summary>
		/// PK_VEHICLE
		///</summary>
		public static void MakePK_Vehicle(DataSet dataset)
		{
			DataTable table = dataset.Tables["VEHICLE"];
			if (table.PrimaryKey != null && table.PrimaryKey.Length > 0) return;
			table.PrimaryKey = new DataColumn[]{
				table.Columns["VEHICLE_ID"]
			}
			;
		}
		///<summary>
		/// VEHICLE
		///</summary>
		public static void MakeIdentities_Vehicle(DataSet dataset)
		{
			DataTable table = dataset.Tables["VEHICLE"];
			table.Columns["VEHICLE_ID"].AutoIncrement = true;
			table.Columns["VEHICLE_ID"].AutoIncrementSeed = -1;
			table.Columns["VEHICLE_ID"].AutoIncrementStep = -1;
		}
		///<summary>
		/// FK_CONTROLLER_CONTROLLER_TYPE
		///</summary>
		public static void MakeRelation_FK_CONTROLLER_CONTROLLER_TYPE(DataSet dataset)
		{
			if (dataset.Relations.Contains("FK_CONTROLLER_CONTROLLER_TYPE")) return;
			DataColumn parent = dataset.Tables["CONTROLLER_TYPE"].Columns["CONTROLLER_TYPE_ID"];
			DataColumn child = dataset.Tables["CONTROLLER"].Columns["CONTROLLER_TYPE_ID"];
			dataset.Relations.Add("FK_CONTROLLER_CONTROLLER_TYPE", parent, child);
		}
		///<summary>
		/// FK_CONTROLLER_INFO_CONTROLLER
		///</summary>
		public static void MakeRelation_FK_CONTROLLER_INFO_CONTROLLER(DataSet dataset)
		{
			if (dataset.Relations.Contains("FK_CONTROLLER_INFO_CONTROLLER")) return;
			DataColumn parent = dataset.Tables["CONTROLLER"].Columns["CONTROLLER_ID"];
			DataColumn child = dataset.Tables["CONTROLLER_INFO"].Columns["CONTROLLER_ID"];
			dataset.Relations.Add("FK_CONTROLLER_INFO_CONTROLLER", parent, child);
		}
		///<summary>
		/// FK_CONTROLLER_VEHICLE
		///</summary>
		public static void MakeRelation_FK_CONTROLLER_VEHICLE(DataSet dataset)
		{
			if (dataset.Relations.Contains("FK_CONTROLLER_VEHICLE")) return;
			DataColumn parent = dataset.Tables["VEHICLE"].Columns["VEHICLE_ID"];
			DataColumn child = dataset.Tables["CONTROLLER"].Columns["VEHICLE_ID"];
			dataset.Relations.Add("FK_CONTROLLER_VEHICLE", parent, child);
		}
		///<summary>
		/// FK_MEDIA_ACCEPTORS_MEDIA
		///</summary>
		public static void MakeRelation_FK_MEDIA_ACCEPTORS_MEDIA(DataSet dataset)
		{
			if (dataset.Relations.Contains("FK_MEDIA_ACCEPTORS_MEDIA")) return;
			DataColumn parent = dataset.Tables["MEDIA"].Columns["MEDIA_ID"];
			DataColumn child = dataset.Tables["MEDIA_ACCEPTORS"].Columns["MEDIA_ID"];
			dataset.Relations.Add("FK_MEDIA_ACCEPTORS_MEDIA", parent, child);
		}
		///<summary>
		/// FK_MEDIA_MEDIA_TYPE
		///</summary>
		public static void MakeRelation_FK_MEDIA_MEDIA_TYPE(DataSet dataset)
		{
			if (dataset.Relations.Contains("FK_MEDIA_MEDIA_TYPE")) return;
			DataColumn parent = dataset.Tables["MEDIA_TYPE"].Columns["ID"];
			DataColumn child = dataset.Tables["MEDIA"].Columns["TYPE"];
			dataset.Relations.Add("FK_MEDIA_MEDIA_TYPE", parent, child);
		}
		///<summary>
		/// FK_SESSION_OPERATOR
		///</summary>
		public static void MakeRelation_FK_SESSION_OPERATOR(DataSet dataset)
		{
			if (dataset.Relations.Contains("FK_SESSION_OPERATOR")) return;
			DataColumn parent = dataset.Tables["OPERATOR"].Columns["OPERATOR_ID"];
			DataColumn child = dataset.Tables["SESSION"].Columns["OPERATOR_ID"];
			dataset.Relations.Add("FK_SESSION_OPERATOR", parent, child);
		}
		///<summary>
		/// FK_TRAIL_SESSION
		///</summary>
		public static void MakeRelation_FK_TRAIL_SESSION(DataSet dataset)
		{
			if (dataset.Relations.Contains("FK_TRAIL_SESSION")) return;
			DataColumn parent = dataset.Tables["SESSION"].Columns["SESSION_ID"];
			DataColumn child = dataset.Tables["TRAIL"].Columns["SESSION_ID"];
			dataset.Relations.Add("FK_TRAIL_SESSION", parent, child);
		}

		public static void MakeSchema(DataSet dataset)
		{
			if (dataset.Tables.Contains("CONTROLLER"))
			{
				MakePK_Controller(dataset);
				MakeIdentities_Controller(dataset);
			}
			if (dataset.Tables.Contains("CONTROLLER_INFO"))
			{
				MakePK_ControllerInfo(dataset);
				MakeIdentities_ControllerInfo(dataset);
			}
			if (dataset.Tables.Contains("CONTROLLER_TYPE"))
			{
				MakePK_ControllerType(dataset);
				MakeIdentities_ControllerType(dataset);
			}
			if (dataset.Tables.Contains("MEDIA"))
			{
				MakePK_Media(dataset);
				MakeIdentities_Media(dataset);
			}
			if (dataset.Tables.Contains("MEDIA_ACCEPTORS"))
			{
				MakePK_MediaAcceptors(dataset);
				MakeIdentities_MediaAcceptors(dataset);
			}
			if (dataset.Tables.Contains("MEDIA_TYPE"))
			{
				MakePK_MediaType(dataset);
				MakeIdentities_MediaType(dataset);
			}
			if (dataset.Tables.Contains("OPERATOR"))
			{
				MakePK_Operator(dataset);
				MakeIdentities_Operator(dataset);
			}
			if (dataset.Tables.Contains("SESSION"))
			{
				MakePK_Session(dataset);
				MakeIdentities_Session(dataset);
			}
			if (dataset.Tables.Contains("sysdiagrams"))
			{
				MakePK_Sysdiagrams(dataset);
				MakeIdentities_Sysdiagrams(dataset);
			}
			if (dataset.Tables.Contains("TRAIL"))
			{
				MakePK_Trail(dataset);
				MakeIdentities_Trail(dataset);
			}
			if (dataset.Tables.Contains("VEHICLE"))
			{
				MakePK_Vehicle(dataset);
				MakeIdentities_Vehicle(dataset);
			}
			if (dataset.Tables.Contains("CONTROLLER") && dataset.Tables.Contains("CONTROLLER_TYPE"))
			{
				MakeRelation_FK_CONTROLLER_CONTROLLER_TYPE(dataset);
			}
			if (dataset.Tables.Contains("CONTROLLER_INFO") && dataset.Tables.Contains("CONTROLLER"))
			{
				MakeRelation_FK_CONTROLLER_INFO_CONTROLLER(dataset);
			}
			if (dataset.Tables.Contains("CONTROLLER") && dataset.Tables.Contains("VEHICLE"))
			{
				MakeRelation_FK_CONTROLLER_VEHICLE(dataset);
			}
			if (dataset.Tables.Contains("MEDIA_ACCEPTORS") && dataset.Tables.Contains("MEDIA"))
			{
				MakeRelation_FK_MEDIA_ACCEPTORS_MEDIA(dataset);
			}
			if (dataset.Tables.Contains("MEDIA") && dataset.Tables.Contains("MEDIA_TYPE"))
			{
				MakeRelation_FK_MEDIA_MEDIA_TYPE(dataset);
			}
			if (dataset.Tables.Contains("SESSION") && dataset.Tables.Contains("OPERATOR"))
			{
				MakeRelation_FK_SESSION_OPERATOR(dataset);
			}
			if (dataset.Tables.Contains("TRAIL") && dataset.Tables.Contains("SESSION"))
			{
				MakeRelation_FK_TRAIL_SESSION(dataset);
			}
		}
	}
}