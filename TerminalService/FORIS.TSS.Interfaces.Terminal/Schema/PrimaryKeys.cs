﻿using System;

namespace FORIS.TSS.BusinessLogic.Terminal.Schema
{
	[Serializable]
	public struct ControllerPrimaryKey
	{
		public System.Int32 ControllerId;
		public ControllerPrimaryKey(System.Int32 ControllerId)
		{
			this.ControllerId = ControllerId;
		}
		public object[] GetObjects()
		{
			return new object[] { ControllerId };
		}
	}
	[Serializable]
	public struct ControllerInfoPrimaryKey
	{
		public System.Int32 ControllerId;
		public ControllerInfoPrimaryKey(System.Int32 ControllerId)
		{
			this.ControllerId = ControllerId;
		}
		public object[] GetObjects()
		{
			return new object[] { ControllerId };
		}
	}
	[Serializable]
	public struct ControllerTypePrimaryKey
	{
		public System.Int32 ControllerTypeId;
		public ControllerTypePrimaryKey(System.Int32 ControllerTypeId)
		{
			this.ControllerTypeId = ControllerTypeId;
		}
		public object[] GetObjects()
		{
			return new object[] { ControllerTypeId };
		}
	}
	[Serializable]
	public struct MediaPrimaryKey
	{
		public System.Int32 MediaId;
		public MediaPrimaryKey(System.Int32 MediaId)
		{
			this.MediaId = MediaId;
		}
		public object[] GetObjects()
		{
			return new object[] { MediaId };
		}
	}
	[Serializable]
	public struct MediaAcceptorsPrimaryKey
	{
		public System.Int32 MaId;
		public MediaAcceptorsPrimaryKey(System.Int32 MaId)
		{
			this.MaId = MaId;
		}
		public object[] GetObjects()
		{
			return new object[] { MaId };
		}
	}
	[Serializable]
	public struct MediaTypePrimaryKey
	{
		public System.Byte Id;
		public MediaTypePrimaryKey(System.Byte Id)
		{
			this.Id = Id;
		}
		public object[] GetObjects()
		{
			return new object[] { Id };
		}
	}
	[Serializable]
	public struct OperatorPrimaryKey
	{
		public System.Int32 OperatorId;
		public OperatorPrimaryKey(System.Int32 OperatorId)
		{
			this.OperatorId = OperatorId;
		}
		public object[] GetObjects()
		{
			return new object[] { OperatorId };
		}
	}
	[Serializable]
	public struct SessionPrimaryKey
	{
		public System.Int32 SessionId;
		public SessionPrimaryKey(System.Int32 SessionId)
		{
			this.SessionId = SessionId;
		}
		public object[] GetObjects()
		{
			return new object[] { SessionId };
		}
	}
	[Serializable]
	public struct SysdiagramsPrimaryKey
	{
		public System.Int32 DiagramId;
		public SysdiagramsPrimaryKey(System.Int32 DiagramId)
		{
			this.DiagramId = DiagramId;
		}
		public object[] GetObjects()
		{
			return new object[] { DiagramId };
		}
	}
	[Serializable]
	public struct TrailPrimaryKey
	{
		public System.Int32 TrailId;
		public TrailPrimaryKey(System.Int32 TrailId)
		{
			this.TrailId = TrailId;
		}
		public object[] GetObjects()
		{
			return new object[] { TrailId };
		}
	}
	[Serializable]
	public struct VehiclePrimaryKey
	{
		public System.Int32 VehicleId;
		public VehiclePrimaryKey(System.Int32 VehicleId)
		{
			this.VehicleId = VehicleId;
		}
		public object[] GetObjects()
		{
			return new object[] { VehicleId };
		}
	}
}