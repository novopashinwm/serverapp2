﻿using System;
using System.Globalization;

namespace FORIS.TSS.Terminal.IntelliTrac
{
    public sealed class PositionReader : SplittedStringReader
    {
        public PositionReader(string s, char delimeter) 
            : base(s, delimeter)
        {

        }

        public int ReadInt()
        {
            var s = ReadString();
            return int.Parse(s);
        }

        private static readonly NumberFormatInfo NumberFormatInfo = new NumberFormatInfo { NumberDecimalSeparator = "." };
        public double ReadDouble()
        {
            var s = ReadString();
            return  Double.Parse(s, NumberFormatInfo);
        }

        public decimal ReadDecimal()
        {
            var s = ReadString();
            return Decimal.Parse(s, NumberFormatInfo);
        }

        public DateTime ReadDateTime()
        {
            var s = ReadString();
            return DateTime.ParseExact(s, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
        }
    }
}
