﻿namespace FORIS.TSS.Terminal.IntelliTrac
{
    public enum Sensors
    {
        Input1,
        Input2,
        Input3,
        Input4,
        Input5,
        Input6,
        Input7,
        Input8,

        Output1,
        Output2,
        Output3,
        Output4,
        Output5,
        Output6,
        Output7,
        Output8,

        AnalogInput9,
        AnalogInput10
    }
}
