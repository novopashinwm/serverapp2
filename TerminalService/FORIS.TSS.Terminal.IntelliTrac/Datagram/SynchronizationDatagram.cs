﻿using System;
using System.Collections;
using System.Globalization;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.IntelliTrac.Datagram
{
    public class SynchronizationDatagram: BaseDatagram
    {

        public SynchronizationDatagram(byte[] packet) 
            : base(packet)
        {
            var deviceId = new byte[Packet.Length - 4];
            Array.Copy(packet, Packet.Length - 4, deviceId, 0, Packet.Length - 4);

            int intDeviceId = 0, s = 1;
            foreach (var b in deviceId)
            {
                intDeviceId = intDeviceId + b * s;
                s = s << 8;
            }
            DeviceId = intDeviceId.ToString(CultureInfo.InvariantCulture);
        }

        public override IList GetResponses()
        {
            //вернем пакет подтверждения
            return new ArrayList
            {
                new MobilUnit.Unit.MobilUnit
                {
                    Time = TimeHelper.GetSecondsFromBase(),
                    DeviceID = DeviceId,
                    CorrectGPS = false
                },
                new ConfirmPacket(Packet)
            };
        }

        public static bool SupportData(byte[] packet)
        {
            return
                packet.Length >= 5 && (
                    (packet[0] == 0xfa && packet[1] == 0xf8) ||
                    (packet[0] == 0x0a && packet[1] == 0xfa && packet[2] == 0xf8)
                    );
        }
    }
}
