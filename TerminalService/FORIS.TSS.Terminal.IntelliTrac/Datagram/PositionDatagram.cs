﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.IntelliTrac.Datagram
{
    public class PositionDatagram: BaseDatagram
    {
        readonly string[] _positionsPackets;

        public PositionDatagram(byte[] packet) 
            : base(packet)
        {
            var dataString = Encoding.ASCII.GetString(packet);
            _positionsPackets = dataString.Replace("\n", "").Split('\r');
        }

        public override IList GetResponses()
        {
            var res = new ArrayList();

            foreach (var packet in _positionsPackets)
            {
                if (!string.IsNullOrEmpty(packet))
                    res.Add(GetUnitFromPacket(packet));
            }
            
            return res;
        }

        static readonly Dictionary<Sensors, int> SensorsMask = new Dictionary<Sensors, int>()
                                                          {
                                                              { Sensors.Input1, 0x1 },
                                                              { Sensors.Input2, 0x2 },
                                                              { Sensors.Input3, 0x4 },
                                                              { Sensors.Input4, 0x8 },
                                                              { Sensors.Input5, 0x10 },
                                                              { Sensors.Input6, 0x20 },
                                                              { Sensors.Input7, 0x40 },
                                                              { Sensors.Input8, 0x80 },

                                                              { Sensors.Output1, 0x1 },
                                                              { Sensors.Output2, 0x2 },
                                                              { Sensors.Output3, 0x4 },
                                                              { Sensors.Output4, 0x8 },
                                                              { Sensors.Output5, 0x10 },
                                                              { Sensors.Output6, 0x20 },
                                                              { Sensors.Output7, 0x40 },
                                                              { Sensors.Output8, 0x80 }
                                                          }; 

        static MobilUnit.Unit.MobilUnit GetUnitFromPacket(string packet)
        {
            var positionReader = new PositionReader(packet, ',');
            if (positionReader.Length < 11) return null;
            var mu = new MobilUnit.Unit.MobilUnit();
            mu.DeviceID = positionReader.ReadString();
            var time = positionReader.ReadDateTime();
            mu.cmdType = CmdType.Trace;
            
            mu.Time = TimeHelper.GetSecondsFromBase(time);
            mu.Longitude = positionReader.ReadDouble();
            mu.Latitude = positionReader.ReadDouble();
            mu.Speed = positionReader.ReadInt();
            mu.Course = positionReader.ReadInt();
            mu.Height = positionReader.ReadInt() * 100;
            mu.Satellites = positionReader.ReadInt();
            positionReader.Skip(1);
            mu.CorrectGPS = mu.Satellites>=MobilUnit.Unit.MobilUnit.MinSatellites;

            var intInputs = positionReader.ReadInt();
            var intOutputs = positionReader.ReadInt();

            mu.SensorValues = new Dictionary<int, long>();
            mu.SensorValues.Add((int)Sensors.Input1, (SensorsMask[Sensors.Input1] & intInputs) > 0? 1:0);
            mu.SensorValues.Add((int)Sensors.Input2, (SensorsMask[Sensors.Input2] & intInputs) > 0? 1:0);
            mu.SensorValues.Add((int)Sensors.Input3, (SensorsMask[Sensors.Input3] & intInputs) > 0? 1:0);
            mu.SensorValues.Add((int)Sensors.Input4, (SensorsMask[Sensors.Input4] & intInputs) > 0? 1:0);
            mu.SensorValues.Add((int)Sensors.Input5, (SensorsMask[Sensors.Input5] & intInputs) > 0? 1:0);
            mu.SensorValues.Add((int)Sensors.Input6, (SensorsMask[Sensors.Input6] & intInputs) > 0? 1:0);
            mu.SensorValues.Add((int)Sensors.Input7, (SensorsMask[Sensors.Input7] & intInputs) > 0? 1:0);
            mu.SensorValues.Add((int)Sensors.Input8, (SensorsMask[Sensors.Input8] & intInputs) > 0? 1:0);

            mu.SensorValues.Add((int)Sensors.Output1, (SensorsMask[Sensors.Output1] & intOutputs) > 0 ? 1 : 0);
            mu.SensorValues.Add((int)Sensors.Output2, (SensorsMask[Sensors.Output2] & intOutputs) > 0 ? 1 : 0);
            mu.SensorValues.Add((int)Sensors.Output3, (SensorsMask[Sensors.Output3] & intOutputs) > 0 ? 1 : 0);
            mu.SensorValues.Add((int)Sensors.Output4, (SensorsMask[Sensors.Output4] & intOutputs) > 0 ? 1 : 0);
            mu.SensorValues.Add((int)Sensors.Output5, (SensorsMask[Sensors.Output6] & intOutputs) > 0 ? 1 : 0);
            mu.SensorValues.Add((int)Sensors.Output6, (SensorsMask[Sensors.Output6] & intOutputs) > 0 ? 1 : 0);
            mu.SensorValues.Add((int)Sensors.Output7, (SensorsMask[Sensors.Output7] & intOutputs) > 0 ? 1 : 0);
            mu.SensorValues.Add((int)Sensors.Output8, (SensorsMask[Sensors.Output8] & intOutputs) > 0 ? 1 : 0);
            mu.Properties.Add(DeviceProperty.Protocol, ControllerType.Names.IntelliTrac);
            
            if (positionReader.HasMoreData())
            {
                mu.SensorValues.Add((int)Sensors.AnalogInput9, (long) (positionReader.ReadDecimal() * 1000m));
                mu.SensorValues.Add((int)Sensors.AnalogInput10, (long) (positionReader.ReadDecimal() * 1000m));
            }

            return mu;
        }
    }
}
