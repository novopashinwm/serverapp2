﻿using System.Collections;
using System.Text;

namespace FORIS.TSS.Terminal.IntelliTrac.Datagram
{
    public abstract class BaseDatagram
    {
        protected readonly byte[] Packet;
        protected string DeviceId;

        protected BaseDatagram(byte[] packet)
        {
            Packet = packet;
        }

        protected static byte[] Trim(byte[] data)
        {
            if (data == null) return null;
            string dataString = Encoding.ASCII.GetString(data).Trim();
            return Encoding.ASCII.GetBytes(dataString);
        }

        public static BaseDatagram Init(byte[] packet)
        {
            if (SynchronizationDatagram.SupportData(packet))
                return new SynchronizationDatagram(packet);
            packet = Trim(packet);
            return new PositionDatagram(packet);
        }

        public abstract IList GetResponses();
    }
}
