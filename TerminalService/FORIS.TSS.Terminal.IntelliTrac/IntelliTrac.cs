﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.IntelliTrac
{
	public class IntelliTrac: Device
	{
		public IntelliTrac() : base(typeof(Sensors), new[] { "IntelliTrac" })
		{
			MaxIdleTime = TimeSpan.FromDays(1);
		}

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;

			if (data.SequenceEqual(Encoding.ASCII.GetBytes("$OK:TRACKING\r\n")))
			{
				var state = stateData as ReceiverState;

				return new ArrayList
				{
					CreateCommandCompletedNotification(
						state != null ? state.DeviceID : null, 
						CmdType.SetSettings)
				};
			}
			
			var dtgram = Datagram.BaseDatagram.Init(data);
			if (dtgram == null) return null;
			var responses = dtgram.GetResponses();

			if (stateData == null)
			{
				var deviceId = responses.OfType<IMobilUnit>().Select(x => x.DeviceID).FirstOrDefault(x => x != null);
				if (!string.IsNullOrWhiteSpace(deviceId))
					responses.Add(new ReceiverStoreToStateMessage {StateData = new ReceiverState {DeviceID = deviceId}});
			}

			return responses;
		}

		public override bool SupportData(byte[] data)
		{
			return Datagram.SynchronizationDatagram.SupportData(data);
		}

		public override byte[] GetCmd(IStdCommand cmd)
		{
			switch (cmd.Type)
			{
				case CmdType.Setup:
					return Encoding.ASCII.GetBytes("$ST+TRACKING=0000,2,0,100,0,0");
				default:
					return base.GetCmd(cmd);
			}
		}
	}
}