﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;
using FORIS.TSS.Terminal;

namespace RU.NVG.Terminal.SadSpFoxIn
{
	public class SadSpFoxInDevice : Device
	{
		internal const string ProtocolName = "SADSP FOX-IN";
		internal static class DeviceNames
		{
			internal const string SadSpFoxIn = "SADSP FOX-IN";
		}
		public SadSpFoxInDevice() : base(typeof(SadSpFoxInDeviceSensor), new[]
		{
			DeviceNames.SadSpFoxIn,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			if (data.StartsWith(HistoryPrefix))
				return true;
			return data.StartsWith(PrefixBytes);
		}
		private const string Prefix = "SA200";
		private static readonly byte[] PrefixBytes = Encoding.ASCII.GetBytes(Prefix);
		private static readonly byte[] HistoryPrefix = Encoding.ASCII.GetBytes("$<GPS.History.Read>");
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			var s = Encoding.ASCII.GetString(data, 0, count);

			var lineReader = new SplittedStringReader(s, '\r', '\n');

			var result = new ArrayList();
			while (lineReader.HasMoreData())
			{
				var line = lineReader.ReadString();
				if (string.IsNullOrWhiteSpace(line) || line.StartsWith("$"))
					continue;

				if (!line.StartsWith(Prefix))
					continue;

				var reader = new SplittedStringReader(line, ';');
				if (reader.Length < 19 && !lineReader.HasMoreData())
				{
					bufferRest = Encoding.ASCII.GetBytes(line);
					return result;
				}

				var mu = UnitReceived(ProtocolName);

				reader.Skip(1); //Prefix + packet type
				var imei = reader.ReadString();
				mu.DeviceID = imei;

				var firmwareString = reader.ReadString();
				mu.Firmware = firmwareString;

				var dateString = reader.ReadString();
				var timeString = reader.ReadString();

				DateTime date;
				if (!DateTime.TryParseExact(dateString, "yyyyMMdd", CultureInfo.InvariantCulture,
											DateTimeStyles.AdjustToUniversal, out date))
				{
					Trace.TraceWarning("{0}: Unable to parse date {1} as DateTime, all data was: {2}",
									   this, dateString, StringHelper.SmartFormatBytes(data));
					bufferRest = null;
					return null;
				}

				TimeSpan timeOfDay;
				if (!TimeSpan.TryParseExact(timeString, @"hh\:mm\:ss", CultureInfo.InvariantCulture, out timeOfDay))
				{
					Trace.TraceWarning("{0}: Unable to parse time of day {1} as time span, all data was: {2}",
									   this, timeString, StringHelper.SmartFormatBytes(data));
					bufferRest = null;
					return null;
				}
				mu.Time = TimeHelper.GetSecondsFromBase(date) + (int) timeOfDay.TotalSeconds;

				reader.Skip(1); //Location Code ID(3 digits hex) + Serving Cell BSIC(2 digits decimal)

				var latString = reader.ReadString();
				mu.Latitude = double.Parse(latString, NumberStyles.Number, NumberHelper.FormatInfo);

				var lngString = reader.ReadString();
				mu.Longitude = double.Parse(lngString, NumberStyles.Number, NumberHelper.FormatInfo);

				var speedMetersPerSecondString = reader.ReadString();
				// В протоколе написано, что скорость в метрах в секунду, но в таком случае скорость оказывается слишком высокой, ровно в 3.6 раза, поэтому считаем, что скорость в км/ч
				mu.Speed =
					(int)
					(Math.Round(double.Parse(speedMetersPerSecondString, NumberStyles.Number, NumberHelper.FormatInfo)
						/** 3.6 - protocol error*/));

				var courseDegreesString = reader.ReadString();
				mu.Course =
					(int) (Math.Round(double.Parse(courseDegreesString, NumberStyles.Number, NumberHelper.FormatInfo)));

				var satellitesString = reader.ReadString();
				mu.Satellites = int.Parse(satellitesString);
				var gpsFixed = reader.ReadString() == "1";
				mu.CorrectGPS = gpsFixed;
				reader.Skip(1); //Travelled distance in meters (GPS Based)

				mu.SensorValues = new Dictionary<int, long>();
				var mainPowerVoltageString = reader.ReadString();
				mu.SensorValues.Add(
					(int) SadSpFoxInDeviceSensor.MainPowerVoltage,
					(long) (decimal.Parse(mainPowerVoltageString, NumberStyles.Number, NumberHelper.FormatInfo)*1000m));

				var waterLevelSensorVoltageString = reader.ReadString();
				mu.SensorValues.Add(
					(int) SadSpFoxInDeviceSensor.WaterLevelSensorVoltage,
					(long)
					(decimal.Parse(waterLevelSensorVoltageString, NumberStyles.Number, NumberHelper.FormatInfo)*1000m));

				var inputsAndOutputsString = reader.ReadString();
				mu.SensorValues.Add(
					(int) SadSpFoxInDeviceSensor.Ignition,
					inputsAndOutputsString[0] == '1' ? 1 : 0);
				mu.SensorValues.Add(
					(int) SadSpFoxInDeviceSensor.Input1TripStartKey,
					inputsAndOutputsString[1] == '1' ? 1 : 0);
				mu.SensorValues.Add(
					(int) SadSpFoxInDeviceSensor.Input2WaterDispenseKey,
					inputsAndOutputsString[2] == '1' ? 1 : 0);
				mu.SensorValues.Add(
					(int) SadSpFoxInDeviceSensor.Input3SOSKey,
					inputsAndOutputsString[3] == '1' ? 1 : 0);
				mu.SensorValues.Add(
					(int) SadSpFoxInDeviceSensor.Output1,
					inputsAndOutputsString[4] == '1' ? 1 : 0);
				mu.SensorValues.Add(
					(int) SadSpFoxInDeviceSensor.Output2,
					inputsAndOutputsString[5] == '1' ? 1 : 0);

				reader.Skip(1); //Parking/Driving/Off
				var trip = reader.ReadString() == "1";
				mu.SensorValues.Add(
					(int) SadSpFoxInDeviceSensor.Trip,
					trip ? 1 : 0);

				var panic = reader.ReadString() == "1";
				mu.SensorValues.Add(
					(int) SadSpFoxInDeviceSensor.Panic,
					panic ? 1 : 0);

				result.Add(mu);
			}

			bufferRest = null;
			return result;
		}
	}
}