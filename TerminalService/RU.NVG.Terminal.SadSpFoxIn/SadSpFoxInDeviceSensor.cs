﻿namespace RU.NVG.Terminal.SadSpFoxIn
{
	public enum SadSpFoxInDeviceSensor
	{
		/// <summary> Voltage value of main power </summary>
		MainPowerVoltage = 1,
		/// <summary> Voltage level of Water level Sensor </summary>
		WaterLevelSensorVoltage = 2,
		/// <summary> For Ignition ‘1’ (ON), ‘0’ (OFF) </summary>
		Ignition = 3,
		/// <summary> Input 1 : Trip Start Key Function status - ‘1’ (Trip Start ON), ‘0’ (Trip Start OFF) </summary>
		Input1TripStartKey = 4,
		/// <summary> Input 2 : Water Dispense Function Key Status - ‘1’ (Water Dispense ON), ‘0’ (Water Dispense OFF) </summary>
		Input2WaterDispenseKey = 5,
		/// <summary> Input 3 : SOS Key Function Status – ‘1’ (SOS Key ON), ‘0’ (SOS Key OFF) </summary>
		Input3SOSKey = 6,
		Output1 = 7,
		Output2 = 8,
		/// <summary> ‘1’ = Trip On (Active Mode) (Depends on Serial interface), ‘0’ = Trip Off (Normal Mode) </summary>
		Trip = 9,
		/// <summary> ‘1’ = Panic Status On (Depends on Serial interface), ‘0’ = Panic Status Off </summary>
		Panic = 10,
	}
}