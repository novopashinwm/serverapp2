﻿using System;

namespace FORIS.TSS.Terminal.GlobalOrient
{
	public class NPHResultDatagram : NPHDatagram
	{
		public NPHResultDatagram(NPH_SERVICE_TYPE serviceType, UInt16 requestID, NPH_RESULT result)
			: base(NPH_PACKET_HEADER_SIZE + 4)
		{
			PacketType      = NPH_PACKET_TYPE.NPH_RESULT;
			ServiceType     = serviceType;
			RequestID       = requestID;
			IsNeedToConfirm = false;
			Data            = BitConverter.GetBytes((uint)result);
		}
	}
}