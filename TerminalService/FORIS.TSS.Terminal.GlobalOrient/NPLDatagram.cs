﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using FORIS.TSS.Communication;
// ReSharper disable InconsistentNaming

namespace FORIS.TSS.Terminal.GlobalOrient
{
	/// <summary>
	/// Пакеты сеансовго уровня (нижнего уровня)
	/// </summary>
	public class NPLDatagram : GlobalOrientDatagramBase
	{
		#region Const
		/*NPL packet definitions*/
		protected const Int16 NPL_PACKET_SIGNATURE = 0x7E7E;
		protected const int NPL_PACKET_HEADER_SIZE = 15;
		protected const int NPL_PACKET_MAX_SIZE = 1024;
		protected const int NPL_PACKET_MAX_DATA_SIZE = (NPL_PACKET_MAX_SIZE - NPL_PACKET_HEADER_SIZE);

		/*NPL packet flags (short int)*/
		protected const Int16 NPL_FLAG_ENCRYPTION = 0x0001;
		protected const Int16 NPL_FLAG_CRC = 0x0002;

		/// <summary>
		/// NPL packet types
		/// </summary>
		public enum NPL_TYPE : byte
		{
			ERROR = 0x01,
			NPH = 0x02,
			DEBUG = 0x03,
		}

		/// <summary>
		/// NPL address definitions
		/// </summary>
		public enum NPL_ADDRESS : uint
		{
			SERVER = 0x00000000,
			BROAD_DISPATHERS = 0x00000001,
			BROAD_DEVICES = 0x00000002,
			RESERVED_MIN = 0x00000003,
			RESERVED_MAX = 0x000003FF,
			CLIENT_MIN = 0x00000400,
			CLIENT_MAX = 0xFFFFFFFE,
			DEVICE_MIN = 0x00000400,
			DEVICE_MAX = 0x7FFFFFFF,
			DISPATHCER_MIN = 0x80000000,
			DISPATHCER_MAX = 0xFFFFFFFE,
			INVALID = 0xFFFFFFFF,
			OUTPUT_DEVICE = 0x80000001
		}

		/// <summary>
		/// Response message data
		/// </summary>
		public enum NPL_ERR : uint
		{
			/*general errors*/
			OK = 0,
			DECRYPTION_FAILED = 1,
			CRC_FAILED = 2,
			UNDEFINED = 0xFFFFFFFF,

			/*routing errors*/
			INVALID_PEER_ADDRESS = 100,
			PEER_NOT_AVAILABLE = 101,
			PEER_PERM_DENIED = 102,
		}

		#endregion Const

		#region .ctor

		public NPLDatagram(uint capacity)
			: base(capacity)
		{
			if (capacity > 1)
			{
				bytes[0] = 0x7E;
				bytes[1] = 0x7E;
			}
		}

		public NPLDatagram()
			: this(NPL_PACKET_HEADER_SIZE)
		{
		}

		public NPLDatagram(byte[] data)
			: this((uint)data.Length)
		{
			if (data == null || data.Length < NPL_PACKET_HEADER_SIZE || data[0] != 0x7E || data[1] != 0x7E)
				throw new DatagramNotFoundException(
					"Datagram not found or too short", GetType(), data, 0);

			Array.Copy(data, bytes, data.Length);

			Trace.WriteLineIf(ts.TraceVerbose, BitConverter.ToString(bytes), "GlobalOrient");
		}

		#endregion // .ctor

		#region CRC

		/// <summary>
		/// флаг необходимости расчета контрольной суммы
		/// </summary>
		protected bool IsCalcCRC
		{
			get
			{
				if (bytes == null || bytes.Length < NPL_PACKET_HEADER_SIZE) return false;
				return (Flags & NPL_FLAG_CRC) > 0;
			}
			set
			{
				if (value)
				{
					Flags = (short)(Flags | NPL_FLAG_CRC);
				}
				else
				{
					Flags = (short)(Flags & ~NPL_FLAG_CRC);
				}
			}
		}

		public override ushort CRC16
		{
			get
			{
				if (bytes == null || bytes.Length < NPL_PACKET_HEADER_SIZE || !IsCalcCRC) return 0;

				return BitConverter.ToUInt16(bytes, 6);
			}
			set
			{
				if (bytes == null || bytes.Length < NPL_PACKET_HEADER_SIZE) return;

				byte[] crcB = BitConverter.GetBytes(value);
				bytes[6] = crcB[0];
				bytes[7] = crcB[1];
			}
		}

		/// <summary>
		/// CRC in datagram
		/// </summary>
		protected override int CRC
		{
			get
			{
				return CRC16;
			}
			set
			{
				CRC16 = (ushort)value;
			}
		}

		/// <summary>
		/// CRC поля data
		/// </summary>
		/// <returns>контрольная сумма 2</returns>
		protected override int CalcCRC()
		{
			byte[] data = Data;
			int crc = CRCCalc.crc16_compute(data, (ushort)data.Length);
			return crc;
			/*
			if (bytes == null || bytes.Length < NPL_PACKET_HEADER_SIZE || !IsCalcCRC) return 0;

			//ushort crc = CRC_16_12_5_0(NPL_PACKET_HEADER_SIZE, bytes.Length - NPL_PACKET_HEADER_SIZE - 1);
			
			int crc = 0;
			for (int i = NPL_PACKET_HEADER_SIZE; i < bytes.Length; ++i) crc += bytes[i];
			//Нам нужны только 2 младших байта...
			crc &= 0xffff;
			
			//int crc = (int) G.CRC32(bytes, NPL_PACKET_HEADER_SIZE, bytes.Length - NPL_PACKET_HEADER_SIZE);
			//crc &= 0xffff;

			return crc;
			*/
		}

		public override bool CheckCRC()
		{
			//return true;
			if (!IsCalcCRC) return true;

			return base.CheckCRC();
		}

		#endregion CRC

		#region атрибуты пакета
		protected UInt16 Data_size
		{
			get
			{
				return BitConverter.ToUInt16(bytes, 2);
			}
			set
			{
				byte[] b = BitConverter.GetBytes(value);
				bytes[2] = b[0];
				bytes[3] = b[1];
			}
		}

		protected short Flags
		{
			get
			{
				return BitConverter.ToInt16(bytes, 4);
			}
			set
			{
				byte[] b = BitConverter.GetBytes(value);
				bytes[4] = b[0];
				bytes[5] = b[1];
			}
		}

		public NPL_TYPE PacketType
		{
			get
			{
				return (NPL_TYPE) bytes[8];
			}
			set
			{
				bytes[8] = (byte)value;
			}
		}

		protected NPL_ADDRESS Peer_address
		{
			get
			{
				var res = (NPL_ADDRESS)BitConverter.ToUInt32(bytes, 9);
				return res;
			}
			set
			{
				byte[] b = BitConverter.GetBytes((uint)value);
				bytes[9] = b[0];
				bytes[10] = b[1];
				bytes[11] = b[2];
				bytes[12] = b[3];
			}
		}

		public UInt16 RequestID
		{
			get
			{
				return BitConverter.ToUInt16(bytes, 13);
			}
			set
			{
				byte[] b = BitConverter.GetBytes(value);
				bytes[13] = b[0];
				bytes[14] = b[1];
			}
		}

		protected byte[] Data
		{
			get
			{
				int data_size = bytes.Length - NPL_PACKET_HEADER_SIZE;
				var res = new byte[data_size];
				Array.Copy(bytes, NPL_PACKET_HEADER_SIZE, res, 0, data_size);
				return res;
			}
			set
			{
				int data_size = bytes.Length - NPL_PACKET_HEADER_SIZE;
				if (data_size != value.Length)
				{
					Array.Resize(ref bytes, NPL_PACKET_HEADER_SIZE + value.Length);
				}

				Array.Copy(value, 0, bytes, NPL_PACKET_HEADER_SIZE, value.Length);
			}
		}

		#endregion атрибуты пакета

		protected NPHDatagram _NPHDatagram = null;
		public NPHDatagram NPHDatagram
		{
			get
			{
				if (PacketType == NPL_TYPE.NPH)
				{
					if (_NPHDatagram==null)
					{
						_NPHDatagram = NPHDatagramFactory.CreateNPHDatagram(Data, RequestID, Peer_address);
					}
					return _NPHDatagram;
				}
				else
				{
					return null;
				}
			}
		}

		public NPL_ERR Error
		{
			get
			{
				if (PacketType != NPL_TYPE.ERROR)
				{
					return NPL_ERR.OK;
				}
				
				var res = (NPL_ERR) BitConverter.ToUInt32(bytes, NPL_PACKET_HEADER_SIZE);
				return res;
			}
		}

		#region static creators

		/// <summary>
		/// create datagram from byte sequence
		/// </summary>
		/// <param name="data">bytes</param>
		/// <param name="pos">in - pos to search datagram from, out - pos after dg, -1 - not found</param>
		/// <param name="bufferRest">Часть неполного покета (возвращается только в последней записи)</param>
		/// <returns>datagram if found whole or null if not</returns>
		public static NPLDatagram Create(byte[] data, ref int pos, out byte[] bufferRest)
		{
			bufferRest = null;
			try
			{
				Debug.Assert(pos > -1 && pos < g_MaxDatagramLength, "pos out of range");
				int originalPos = pos;
				pos = FindPacketBeginPos(data, pos);
				// not found or too short
				if (pos == -1)
				{
					bufferRest = GetRestOfBuffer(data, originalPos);
					return null;
				}

				// full size
				int size = GetPacketSize(data, pos);
				// too short or accidental 0xAA not the beginning 
				if (pos + size > data.Length)
				{
					bufferRest = GetRestOfBuffer(data, pos);
					pos += size;
					return null;
				}

				// copy full datagram
				var bytes = new byte[size];
				Array.Copy(data, pos, bytes, 0, size);

				NPLDatagram dg = new NPLDatagram(bytes);

				pos += size;
				
				if (!dg.Valid) 
					return null;
				else
					return dg;
			}
			catch (Exception ex)
			{
				Trace.WriteLine("Error GlobalOrientNPLDatagram Create(...) " + ex.Message +
					"\r\nStack:\r\n" + ex.StackTrace);
				return null;
			}
		}


		public static List<NPLDatagram> CreateDatagramList(byte[] data, out byte[] bufferRest)
		{
			bufferRest = null;
			// list of output dg
			var alOut = new List<NPLDatagram>();
			if (data.Length == 0) return alOut;

			byte[] buf = data;

			int prev, pos = 0, len = buf.Length;
			do
			{
				prev = pos;
				// make attempt create output dg
				NPLDatagram dg = NPLDatagram.Create(buf, ref pos, out bufferRest);
				// succeeded
				if (dg != null)
				{
					//Debug.Assert(!dg.Valid || pos > 0 && pos <= len, "Incorrect pos of next dg");
					alOut.Add(dg);
				}
			}
			while (pos > -1 && pos < len); // && prev < pos && alOut.Count < 256);	// pause parsing at 256 dg
			
			// another try. look if it's SMS and just sub data received
			/*
			if (LinkType == CmdType.SMS)
			{
				GlobalOrientSubData sub = GlobalOrientSubData.Create(buf, 0);
				if (sub == null)	// no more data expected
				{
					return null;
				}
				Debug.Assert(sub.Size <= data.Length,
					"byte count in sub data > input bytes");
				alOut.Add(GlobalOrientOutputDatagram.Create(sub));
			}
			*/

			return alOut;
		}



		/// <summary>
		/// create datagram for response to device
		/// </summary>
		/// <param name="data">NPH Datagram</param>
		/// <param name="NPL_requestID">NPL packet requestID</param>
		/// <param name="NPL_address">NPL packet Peer_address</param>
		/// <returns>datagram</returns>
		public static NPLDatagram CreateResponse(NPHDatagram data, ushort NPL_requestID, NPLDatagram.NPL_ADDRESS NPL_address)
		{
			try
			{
				byte[] dataBytes = data.GetBytes();
				
				int size = NPL_PACKET_HEADER_SIZE + dataBytes.Length;
				var dg = new NPLDatagram((uint)size);
				dg.Data = dataBytes;
				dg.Data_size = (ushort)dataBytes.Length;
				dg.IsCalcCRC = true;
				dg.PacketType = NPL_TYPE.NPH;
				dg.Peer_address = NPL_address;
				dg.RequestID = NPL_requestID;
				dg.BuildCRC();

				return dg;
			}
			catch (Exception ex)
			{
				Trace.WriteLine("Error GlobalOrientNPLDatagram CreateResponse(...) " + ex.Message +
					"\r\nStack:\r\n" + ex.StackTrace);
				return null;
			}
		}
		#endregion static creators

		#region static methods

		protected static int FindPacketBeginPos(byte[] data, int pos)
		{
			//Ищем два байта начала пакета GlobalOrient 0x7E7E
			do
			{
				// find start point 0x7E - начало пакета данных
				pos = Array.IndexOf(data, (byte)0x7E, pos);

				if (pos < 0) return -1;

				// not found or too short
				if (pos >= 0 && (pos + 1) < data.Length && data[pos + 1] == 0x7E && pos + NPL_PACKET_HEADER_SIZE <= data.Length) 
				{
					return pos;
				}
				else
				{
					pos++;
				}
			} while (pos >= 0 && pos < data.Length);

			return -1;
		}

		protected static int GetPacketSize(byte[] data, int pos)
		{
			return NPL_PACKET_HEADER_SIZE + BitConverter.ToUInt16(data, pos + 2);
		}
		
		public static bool IsDataSupported(byte[] data, int pos)
		{
			if ((data.Length - pos) >= NPL_PACKET_HEADER_SIZE && data[pos] == 0x7E && data[pos + 1] == 0x7E)
				return true;
			else
				return false;
		}

		#endregion static methods
	}
}