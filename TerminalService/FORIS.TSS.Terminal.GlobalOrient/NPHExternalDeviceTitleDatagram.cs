using System;
using System.Collections;
using System.Text;

namespace FORIS.TSS.Terminal.GlobalOrient
{
    public class NPHExternalDeviceTitleDatagram : NPHExternalDeviceDatagram
    {
        #region .ctor

        public NPHExternalDeviceTitleDatagram()
            : base(NPH_SRV_EXTERNAL_DEVICE_TITLE_DATA_OFFSET)
        {
            PacketType = NPH_PACKET_TYPE.NPH_SED_TITLE_DATA;
        }

        public NPHExternalDeviceTitleDatagram(ushort requestId, string message)
            : this()
        {
            RequestID = requestId;
            MessageID = GlobalOrient.GetNewMessageID();
            PacketNum = 0x8000;
            DeviceFrom = DEVICE_ADDR.DISPLAY_DEVICE;
            DeviceNumFrom = 0;
            DeviceTo = DEVICE_ADDR.DISPLAY_DEVICE;
            DeviceNumTo = 0;
            DataType = DATA_TYPE.MSG_SCRIPT_DISPLAY;
            
            //����������
            message = message.Replace("<", "{").Replace(">", "}").Replace("&", "&amp;").Replace("\"", "&quot;").Replace("'", "&apos;");
            //.Replace("\"", "&quot;").Replace("'", "&apos;");
            
            /*
            string script = string.Format("<NAVSCR ver=1.0><ID>{0}</ID>" + 
                "<FROM>SERVER</FROM><TO>USER</TO><TYPE>QUERY</TYPE>" +
                "<MSG id={1}>{2}</MSG></NAVSCR>", MessageID, 8, message);
            */

            string script = string.Format("<NAVSCR ver=1.0><ID>{0}</ID>" +
                                          "<FROM>SERVER</FROM><TO>USER</TO><TYPE>QUERY</TYPE>" +
                                          "<MSG id={1} beep=1>{2}</MSG></NAVSCR>", MessageID, 8, message);

            byte[] msgData = Encoding.GetEncoding(1251).GetBytes(script);
            PacketData = msgData;
        }

        public NPHExternalDeviceTitleDatagram(byte[] data, ushort NPL_requestID, NPLDatagram.NPL_ADDRESS NPL_address)
            : base(data, NPL_requestID, NPL_address)
        {

        }

        #endregion // .ctor

        public byte[] PacketData
        {
            get
            {
                var val = new byte[bytes.Length - NPH_SRV_EXTERNAL_DEVICE_TITLE_DATA_OFFSET];
                Array.Copy(bytes, NPH_SRV_EXTERNAL_DEVICE_TITLE_DATA_OFFSET, val, 0, bytes.Length - NPH_SRV_EXTERNAL_DEVICE_TITLE_DATA_OFFSET);
                return val;
            }
            set
            {
                int data_size = bytes.Length - NPH_SRV_EXTERNAL_DEVICE_TITLE_DATA_OFFSET;
                if (data_size != value.Length)
                {
                    Array.Resize(ref bytes, NPH_SRV_EXTERNAL_DEVICE_TITLE_DATA_OFFSET + value.Length);
                }

                Array.Copy(value, 0, bytes, NPH_SRV_EXTERNAL_DEVICE_TITLE_DATA_OFFSET, value.Length);
            }
        }

        public ushort MessageID
        {
            get
            {
                var res = BitConverter.ToUInt16(bytes, NPH_PACKET_HEADER_SIZE + 0);
                return res;
            }
            set
            {
                byte[] b = BitConverter.GetBytes(value);
                bytes[NPH_PACKET_HEADER_SIZE + 0] = b[0];
                bytes[NPH_PACKET_HEADER_SIZE + 1] = b[1];
            }
        }

        public ushort PacketNum
        {
            get
            {
                var res = BitConverter.ToUInt16(bytes, NPH_PACKET_HEADER_SIZE + 2);
                return res;
            }
            set
            {
                byte[] b = BitConverter.GetBytes(value);
                bytes[NPH_PACKET_HEADER_SIZE + 2] = b[0];
                bytes[NPH_PACKET_HEADER_SIZE + 3] = b[1];
            }
        }

        public ushort AddrFrom
        {
            get
            {
                var res = BitConverter.ToUInt16(bytes, NPH_PACKET_HEADER_SIZE + 4);
                return res;
            }
            set
            {
                byte[] b = BitConverter.GetBytes(value);
                bytes[NPH_PACKET_HEADER_SIZE + 4] = b[0];
                bytes[NPH_PACKET_HEADER_SIZE + 5] = b[1];
            }
        }

        public ushort AddrTo
        {
            get
            {
                var res = BitConverter.ToUInt16(bytes, NPH_PACKET_HEADER_SIZE + 6);
                return res;
            }
            set
            {
                byte[] b = BitConverter.GetBytes(value);
                bytes[NPH_PACKET_HEADER_SIZE + 6] = b[0];
                bytes[NPH_PACKET_HEADER_SIZE + 7] = b[1];
            }
        }

        public DATA_TYPE DataType
        {
            get
            {
                var res = (DATA_TYPE)BitConverter.ToUInt32(bytes, NPH_PACKET_HEADER_SIZE + 8);
                return res;
            }
            set
            {
                byte[] b = BitConverter.GetBytes((uint)value);
                bytes[NPH_PACKET_HEADER_SIZE + 8] = b[0];
                bytes[NPH_PACKET_HEADER_SIZE + 9] = b[1];
                bytes[NPH_PACKET_HEADER_SIZE + 10] = b[2];
                bytes[NPH_PACKET_HEADER_SIZE + 11] = b[3];
            }
        }

        public DEVICE_ADDR DeviceTo
        {
            get
            {
                return (DEVICE_ADDR)(AddrTo >> 4);
            }
            set
            {
                AddrTo = (ushort)(((ushort)value << 4) + DeviceNumTo);
            }
        }
        public byte DeviceNumTo
        {
            get
            {
                return (byte)(AddrTo & 0xF);
            }
            set
            {
                AddrTo = (ushort)(value + ((ushort)DeviceTo << 4));
            }
        }

        public DEVICE_ADDR DeviceFrom
        {
            get
            {
                return (DEVICE_ADDR)(AddrFrom >> 4);
            }
            set
            {
                AddrFrom = (ushort)(((ushort)value << 4) + DeviceNumFrom);
            }
        }

        public byte DeviceNumFrom
        {
            get
            {
                return (byte)(AddrFrom & 0xF);
            }
            set
            {
                AddrFrom = (ushort)(value + ((ushort)DeviceFrom << 4));
            }
        }

        public string GetDisplayMessage()
        {
            if (DataType != DATA_TYPE.MSG_SCRIPT_DISPLAY) return "";

            /*<NAVSCR ver=1.0><ID>1256903099</ID><FROM>USER</FROM><TO>SERVER</TO>
             * <TYPE>QUERY</TYPE>
             * <MSG id=8>����������� �������������</MSG>
             * </NAVSCR>
             */
            string msg = "";

            string script = Encoding.GetEncoding(1251).GetString(PacketData);
            if (string.IsNullOrEmpty(script) || !script.Contains("</MSG>")) return "";
            //���������� XML - "ver=1.0" �� ��������
            //var xmlDoc = new XmlDocument();
            //xmlDoc.LoadXml(script);
            msg = script.Substring(0, script.IndexOf("</MSG>"));
            msg = msg.Substring(msg.LastIndexOf(">") + 1);

            return msg;
        }

        
        public override ArrayList GetMessages()
        {
            var msg = GetDisplayMessage();
            
            var res = new ArrayList();

            if (!string.IsNullOrEmpty(msg))
            {
                //������� ��������� ��� ����������
                res.Add(new NPHExternalDeviceTextMessage {Text = msg, MessageID = MessageID});
            }

            var confirmMessagePacket = CreateConfirmPacket(new NPHExternalDeviceResultDatagram(RequestID, PacketNum, MessageID, 0), NPL_RequestID, NPL_Address);
            res.Add(confirmMessagePacket);

            //������������� ������ ������ �������� ������
            /*
            if (IsNeedToConfirm)
            {
                res.Add(CreateResultOKMessage());
            }
            */

            return res;
        }
    }
}