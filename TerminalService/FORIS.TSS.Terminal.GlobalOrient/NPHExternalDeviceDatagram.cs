﻿using System;
// ReSharper disable InconsistentNaming

namespace FORIS.TSS.Terminal.GlobalOrient
{
    public class NPHExternalDeviceDatagram : NPHDatagram
    {

        /*NPH_SRV_EXTERNAL_DEVICE packets*/
        public enum PACKET_TYPE : ushort
        {
            TITLE_DATA = 100
            ,
            DEVICE_DATA = 101
                , DEVICE_RESULT = 102
        }

        /*NPH_SRV_EXTERNAL_DEVICE device address*/
        public enum DEVICE_ADDR : ushort
        {
            DISPLAY_DEVICE = 0x0
        ,
            VOICE_DRIVER_DEVICE = 0x10
                ,
            VOICE_DRIVER_PASSANGERS = 0x20
                ,
            INTERNAL_DISPLAY = 0x30
                ,
            EXTERNAL_DISPLAY = 0x40
                ,
            IRMA_DEVICE = 0x50
                ,
            BPKRD_DEVICE = 0x60
                ,
            TEMPERATURE_ON_BOARD = 0x70
                , JPG_CAMERA = 0x80
        }

        /*NPH_SRV_EXTERNAL_DEVICE type packet*/
        public enum DATA_TYPE : ushort
        {
            MSG_SCRIPT_DISPLAY = 0x1
        ,
            JPG_FOTO = 0x2
                ,
            VECTOR_MAP = 0x3
                ,
            VOICE_LPC10 = 0x4
                ,
            VOICE_GSM630 = 0x5
                , VOICE_SPEEX = 0x6
        }

        protected const int NPH_SRV_EXTERNAL_DEVICE_TITLE_INSIDE_HEADER_SIZE = 12;
        //Начало данных пакета NPH_SRV_EXTERNAL_DEVICE_TITLE
        protected const int NPH_SRV_EXTERNAL_DEVICE_TITLE_DATA_OFFSET = NPH_PACKET_HEADER_SIZE + NPH_SRV_EXTERNAL_DEVICE_TITLE_INSIDE_HEADER_SIZE;

        
        #region .ctor

        public NPHExternalDeviceDatagram()
            : this((NPH_PACKET_HEADER_SIZE))
        {

        }

        public NPHExternalDeviceDatagram(uint capacity)
            : base(capacity)
        {
            ServiceType = NPH_SERVICE_TYPE.EXTERNAL_DEVICE;
            IsNeedToConfirm = false;
        }

        public NPHExternalDeviceDatagram(UInt16 requestID, PACKET_TYPE packetType)
            : this()
        {
            PacketType = (NPH_PACKET_TYPE)packetType;
            RequestID = requestID;
        }

        public NPHExternalDeviceDatagram(byte[] data, ushort NPL_requestID, NPLDatagram.NPL_ADDRESS NPL_address)
            : base(data, NPL_requestID, NPL_address)
        {

        }

        #endregion // .ctor
    }
    /*
    public class NPH_SRV_EXTERNAL_DEVICE_DISPLAY_TEXT: NPHExternalDeviceDatagram
    {
        public NPH_SRV_EXTERNAL_DEVICE_DISPLAY_TEXT(UInt16 requestID, string text)
            : base(requestID, NPH_PACKET_TYPE.NPH_SED_ADDR_DISPLAY_DEVICE)
        {
            Data = Encoding.ASCII.GetBytes(text);
        }
    }
    */
}
