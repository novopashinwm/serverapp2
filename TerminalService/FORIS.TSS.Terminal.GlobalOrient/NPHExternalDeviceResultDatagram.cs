using System;
// ReSharper disable InconsistentNaming

namespace FORIS.TSS.Terminal.GlobalOrient
{
    public class NPHExternalDeviceResultDatagram : NPHDatagram
    {
        public NPHExternalDeviceResultDatagram(byte[] data, ushort NPL_requestID, NPLDatagram.NPL_ADDRESS NPL_address)
            : base(data, NPL_requestID, NPL_address)
        {
        }

        public NPHExternalDeviceResultDatagram(UInt16 requestID, ushort packetNum, ushort messageID, uint error)
            : base(NPH_PACKET_HEADER_SIZE + 8)
        {
            PacketType = NPH_PACKET_TYPE.NPH_SED_DEVICE_RESULT;
            ServiceType = NPH_SERVICE_TYPE.EXTERNAL_DEVICE;
            RequestID = requestID; // (ushort)(requestID == ushort.MaxValue ? 1 : (requestID + 1));
            IsNeedToConfirm = false;

            PacketNum = packetNum;
            Error = error;
            MessageID = messageID;
        }


        public ushort PacketNum
        {
            get
            {
                var res = BitConverter.ToUInt16(bytes, NPH_PACKET_HEADER_SIZE + 0);
                return res;
            }
            set
            {
                byte[] b = BitConverter.GetBytes(value);
                bytes[NPH_PACKET_HEADER_SIZE + 0] = b[0];
                bytes[NPH_PACKET_HEADER_SIZE + 1] = b[1];
            }
        }

        public uint Error
        {
            get
            {
                var res = BitConverter.ToUInt32(bytes, NPH_PACKET_HEADER_SIZE + 2);
                return res;
            }
            set
            {
                byte[] b = BitConverter.GetBytes(value);
                bytes[NPH_PACKET_HEADER_SIZE + 2] = b[0];
                bytes[NPH_PACKET_HEADER_SIZE + 3] = b[1];
                bytes[NPH_PACKET_HEADER_SIZE + 4] = b[2];
                bytes[NPH_PACKET_HEADER_SIZE + 5] = b[3];
            }
        }
        
        public ushort MessageID
        {
            get
            {
                var res = BitConverter.ToUInt16(bytes, NPH_PACKET_HEADER_SIZE + 6);
                return res;
            }
            set
            {
                byte[] b = BitConverter.GetBytes(value);
                bytes[NPH_PACKET_HEADER_SIZE + 6] = b[0];
                bytes[NPH_PACKET_HEADER_SIZE + 7] = b[1];
            }
        }
        
    }
}