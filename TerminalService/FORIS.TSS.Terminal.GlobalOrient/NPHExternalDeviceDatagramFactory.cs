namespace FORIS.TSS.Terminal.GlobalOrient
{
    public class NPHExternalDeviceDatagramFactory
    {
        public static NPHDatagram CreateDatagram(byte[] data, ushort NPL_requestID, NPLDatagram.NPL_ADDRESS NPL_address)
        {
            NPHDatagram dg = null;

            var packet_type = (NPHExternalDeviceDatagram.PACKET_TYPE)NPHDatagram.GetPacketType(data);

            switch (packet_type)
            {
                case NPHExternalDeviceDatagram.PACKET_TYPE.TITLE_DATA:
                    dg = new NPHExternalDeviceTitleDatagram(data, NPL_requestID, NPL_address);
                    break;

                case NPHExternalDeviceDatagram.PACKET_TYPE.DEVICE_RESULT:
                    dg = new NPHExternalDeviceResultDatagram(data, NPL_requestID, NPL_address);
                    break;
                
                /*
                case NPHExternalDeviceDatagram.PACKET_TYPE.DEVICE_DATA:
                        
                    break;

                    
                case NPHExternalDeviceDatagram.PACKET_TYPE.DEVICE_RESULT:
                        
                    break;
                */
                default:
                    dg = null;// new NPHExternalDeviceDatagram(data);
                    break;
            }

            return dg;
        }
    }
}