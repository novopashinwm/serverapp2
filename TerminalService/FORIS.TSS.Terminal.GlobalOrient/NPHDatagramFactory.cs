﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FORIS.TSS.Terminal.GlobalOrient
{
    public class NPHDatagramFactory
    {
        public static NPHDatagram CreateNPHDatagram(byte[] data, ushort NPL_requestID, NPLDatagram.NPL_ADDRESS NPL_address)
        {
            NPHDatagram dg = null;
            if (data != null && data.Length >= NPHDatagram.NPH_PACKET_HEADER_SIZE)
            {
                NPHDatagram.NPH_SERVICE_TYPE serviceType = NPHDatagram.GetServiceType(data);
                switch(serviceType)
                {
                    case NPHDatagram.NPH_SERVICE_TYPE.GENERIC_CONTROLS:
                        dg = new NPHGenericControlDatagram(data, NPL_requestID, NPL_address);
                        break;
                    case NPHDatagram.NPH_SERVICE_TYPE.NAVDATA:
                        dg = new NPHNavDataDatagram(data, NPL_requestID, NPL_address);
                        break;
                    case NPHDatagram.NPH_SERVICE_TYPE.EXTERNAL_DEVICE:
                        dg = NPHExternalDeviceDatagramFactory.CreateDatagram(data, NPL_requestID, NPL_address);
                        break;
                    default:
                        //Создаем базовый класс, который умеет только отправлять пакеты подтверждения
                        dg = new NPHDatagram(data, NPL_requestID, NPL_address);
                        break;
                }
            }
            return dg;
        }
    }
}
