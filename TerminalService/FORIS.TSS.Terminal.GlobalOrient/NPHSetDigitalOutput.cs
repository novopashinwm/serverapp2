namespace FORIS.TSS.Terminal.GlobalOrient
{
	public class NPHSetDigitalOutput : NPHDatagram
	{
		public NPHSetDigitalOutput(ushort requestId, bool enabled)
			: base(NPH_PACKET_HEADER_SIZE + 2 /*������ � ������, 2 ����� - ����� ����� � ��������*/)
		{
			ServiceType = NPH_SERVICE_TYPE.GENERIC_CONTROLS;
			PacketType = NPH_PACKET_TYPE.NPH_SET_PRDO;
			Flags = NPH_FLAG_REQUEST;
			RequestID = requestId;
			Data = new byte[]
			{
				//����� ������
				0,
				//�������� �� ������
				(byte) (enabled ? 1 : 0)
			};
		}
	}

	public class NPHGetDigitalOutput : NPHDatagram
	{
		public NPHGetDigitalOutput(ushort requestId)
			: base(NPH_PACKET_HEADER_SIZE)
		{
			ServiceType = NPH_SERVICE_TYPE.GENERIC_CONTROLS;
			PacketType  = NPH_PACKET_TYPE.NPH_GET_PRDO;
			Flags       = NPH_FLAG_REQUEST;
			RequestID   = requestId;
			Data        = new byte[0];
		}
	}
}