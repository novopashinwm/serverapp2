﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.Communication;
using FORIS.TSS.Config;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.Terminal.GlobalOrient
{
	public class GlobalOrient : Device
	{
		#region Static counters

		private static int _outRequestID;
		private static int _outMessageID;
		private ConcurrentDictionary<int, IStdCommand> _messageToCommand = new ConcurrentDictionary<int, IStdCommand>(); 

		public static ushort GetNewRequestID()
		{
			return (ushort) (Interlocked.Increment(ref _outRequestID)%ushort.MaxValue);
		}

		public static ushort GetNewMessageID()
		{
			return (ushort) (Interlocked.Increment(ref _outMessageID)%ushort.MaxValue);
		}

		private ushort GetNewRequestID(IStdCommand command)
		{
			var requestId = GetNewRequestID();
			_messageToCommand[requestId] = command;
			return requestId;
		}

		#endregion Static counters

		#region Fields

		/// <summary>
		/// current command
		/// </summary>
		private IStdCommand _cmdTask;

		/// <summary>
		/// open connection with device
		/// </summary>
		/// <remarks>can be cached in OnLinkOpen and used till OnLinkClose</remarks>
		ILink _lnLink;

		readonly static TraceSwitch Ts = new TraceSwitch("GlobalOrient", "GlobalOrient datagram related info");

		#endregion

		#region Properties

		private CmdType LinkType { get; set; }

		#endregion

		public GlobalOrient() : base(typeof (GlobalOrientSensor))
		{
		}
	   
		#region IDevice Members

		public override void Init(CmdType link, CmdType mode)
		{
			OnLinkClose();
			_cmdTask = null;

			LinkType = link;
			Mode = mode;
		}

		
		public override void Init(CmdType link, IStdCommand cmd)
		{
			Init(link, cmd.Type);
			_cmdTask = cmd;
		}

		public override bool Initialized
		{
			get
			{
				return LinkType != CmdType.Unspecified;
			}
		}

	   
		public override byte[] GetCmd(IStdCommand cmd)
		{
			var requestId = GetNewRequestID(cmd);

			NPHDatagram data;
			NPLDatagram dg;
			switch (cmd.Type)
			{
					/*
				case CmdType.Confirm:
					data = new NPHResultDatagram((NPHDatagram.NPH_SERVICE_TYPE) cmd.Params["serviceType"],
											 (ushort) cmd.Params["requestID"],
											 (NPHDatagram.NPH_RESULT) cmd.Params["result"]);

					break;
				*/
				case CmdType.SendText:
					data = new NPHExternalDeviceTitleDatagram(requestId, cmd.Params["Text"].ToString());
					dg = NPLDatagram.CreateResponse(data, GetNewRequestID(), NPLDatagram.NPL_ADDRESS.BROAD_DEVICES);
					break;
				case CmdType.Immobilize:
					data = new NPHSetDigitalOutput(requestId, true);
					dg = NPLDatagram.CreateResponse(data, 0, NPLDatagram.NPL_ADDRESS.DISPATHCER_MIN + 1);
					break;
				case CmdType.Deimmobilize:
					data = new NPHSetDigitalOutput(requestId, false);
					dg = NPLDatagram.CreateResponse(data, 0, NPLDatagram.NPL_ADDRESS.DISPATHCER_MIN + 1);
					break;
				case CmdType.GetState:
					data = new NPHGetDigitalOutput(requestId);
					dg = NPLDatagram.CreateResponse(data, 0, NPLDatagram.NPL_ADDRESS.DISPATHCER_MIN + 1);
					break;

				default:
					Trace.WriteLineIf(Ts.TraceError, "GlobalOrient: Command not supported yet! Command is: " + cmd,
									  "GlobalOrient");
					return null;
			}

			return dg.GetBytes();

			//return this.LinkType == CmdType.SMS ? Postprocess(dg.GetBytes()) : dg.GetBytes();
		}
		
		/*
		private byte[] Postprocess(byte[] data)
		{
			// sms data in special format. post processing.
			// every byte divided into 2.
			// ex. res[2 * i] = 0x60 | HIBYTE(data[i]); res[2 * i + 1] = 0x60 | LOBYTE(data[i])
			if (LinkType == CmdType.SMS)
			{
				byte[] res = new byte[data.Length * 2];
				for (int i = 0, len = data.Length; i < len; ++i)
				{
					res[2 * i] = (byte)(0x40 | data[i] >> 4);
					res[2 * i + 1] = (byte)(0x40 | data[i] & 0xf);
				}
				return res;
			}
			return data;
		}
		*/

		public override void OnLinkOpen(ILink lnk)
		{
			_lnLink = lnk;

			byte[] data = GetCmd(_cmdTask);
			if (data == null || data.Length == 0) return;

			Trace.WriteLineIf(Ts.TraceInfo,
				"Sending command:\r\n\t" + BitConverter.ToString(data), "GlobalOrient");
			_lnLink.Write(data);
		}

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;
			if (!Initialized)
				throw new ApplicationException("Device not initialized");

			// list of wrapped responses
			var alRes = new ArrayList(1);

			//if (this.LinkType == CmdType.SMS)
			//    data = Preprocess(data, count_);

			try
			{
				List<NPLDatagram> datagrams = NPLDatagram.CreateDatagramList(data, out bufferRest);
				if (datagrams.Count == 0)
					return null;

				foreach (NPLDatagram dg in datagrams)
				{
					switch (dg.PacketType)
					{
						/*
						case NPLDatagram.NPL_TYPE.DEBUG:
							break;
						case NPLDatagram.NPL_TYPE.ERROR:
							break;
						*/
						case NPLDatagram.NPL_TYPE.NPH:
							if (dg.NPHDatagram != null)
							{
								ArrayList msgs = dg.NPHDatagram.GetMessages();
								if (msgs != null && msgs.Count > 0)
								{
									alRes.AddRange(msgs);
								}
							}
							break;
					}
				}

				alRes = PostprocessResultData(alRes, stateData);
			}
			catch(Exception ex)
			{
				Trace.WriteLine(string.Format("{0}: GlobalOrient OnData \r\n {1} \r\n Error: \r\n{2}",
											  DateTime.Now,
											  BitConverter.ToString(data),
											  ex
									));
				throw;
			}

			return alRes;
		}

		private ArrayList PostprocessResultData(ArrayList result, object state)
		{
			var list = new ArrayList();
			var orientState = state as GlobalOrientState;
			var deviceId = orientState != null ? orientState.DeviceID : null;
			MobilUnit.Unit.MobilUnit unit = null;
			foreach (var o in result)
			{
				//Проставляем DeviceID
				var navData = o as NPHNavDataRow;
				var sensors = o as NPHSensorDataRow;
				var fuelSensors = o as NPHFuelDataRow;
				var deviceTextMessage = o as NPHExternalDeviceTextMessage;
				var commandResult = o as GlobalOrientCommandResult;
				if (commandResult != null)
				{
					IStdCommand command;

					if (orientState != null && 
						_messageToCommand.TryRemove(commandResult.RequestID, out command) &&
						command.Target.DeviceID == orientState.DeviceID)
					{
						list.Add(CreateCommandCompletedNotification(orientState.DeviceID, command.Type));
					}
					continue;
				}
				if (navData != null)
				{
					var mu = UnitReceived();
					mu.DeviceID = deviceId;
					mu.cmdType = CmdType.Trace;
					mu.Latitude = navData.Lat;
					mu.Longitude = navData.Lng;
					mu.Speed = navData.Speed;
					mu.Time = (int) navData.Time_Stamp;
					mu.Satellites = navData.Satellites;
					mu.CorrectGPS = navData.CorrectGPS;
					mu.Height = navData.Altitude;

					mu.SetSensorValue((int) GlobalOrientSensor.Alarm, navData.Alarm ? 1 : 0);
					mu.SetSensorValue((int) GlobalOrientSensor.Sos, navData.SOS ? 1 : 0);
					mu.SetSensorValue((int) GlobalOrientSensor.BatteryVoltage, navData.Voltage);
					mu.SetSensorValue((int) GlobalOrientSensor.FromBattery, navData.FromBattery ? 1 : 0);

					list.Add(mu);
					unit = mu;
					continue;
				}
				
				if (sensors != null && unit != null)
				{
					unit.Run = (int) sensors.Odometer;

					unit.SetSensorValue((int) GlobalOrientSensor.AnalogueInput0, sensors.AnalogueInput0);
					unit.SetSensorValue((int) GlobalOrientSensor.AnalogueInput1, sensors.AnalogueInput1);
					unit.SetSensorValue((int) GlobalOrientSensor.AnalogueInput2, sensors.AnalogueInput2);
					unit.SetSensorValue((int) GlobalOrientSensor.AnalogueInput3, sensors.AnalogueInput3);

					unit.SetSensorValue((int) GlobalOrientSensor.DigitalInput0, sensors.DigitalInput0);
					unit.SetSensorValue((int) GlobalOrientSensor.DigitalInput1, sensors.DigitalInput1);
					unit.SetSensorValue((int) GlobalOrientSensor.DigitalInput2, sensors.DigitalInput2);
					unit.SetSensorValue((int) GlobalOrientSensor.DigitalInput3, sensors.DigitalInput3);

					unit.SetSensorValue((int) GlobalOrientSensor.DigitalInput, sensors.DigitalInput);
					unit.SetSensorValue((int) GlobalOrientSensor.DigitalOutput, sensors.DigitalOutput);

					unit.SetSensorValue((int) GlobalOrientSensor.GprsSignal, sensors.GprsSignal);
					unit.SetSensorValue((int) GlobalOrientSensor.SignalStrength, sensors.SignalStrength);
					unit.SetSensorValue((int) GlobalOrientSensor.AccelerometerAccel, sensors.AccelerometerAccel);
					unit.SetSensorValue((int) GlobalOrientSensor.AccelerometerEnergy, sensors.AccelerometerEnergy);
					continue;
				}
				
				if (fuelSensors != null && unit != null)
				{
					var fuelStatusKey = "FuelStatus" + fuelSensors.Number;
					GlobalOrientSensor fuelStatus;
					if (Enum.TryParse(fuelStatusKey, true, out fuelStatus) &&
						Enum.IsDefined(typeof (GlobalOrientSensor), fuelStatus))
						unit.SetSensorValue((int) fuelStatus, fuelSensors.Status);

					var fuelLevelKey = "FuelLevel" + fuelSensors.Number;
					GlobalOrientSensor fuelLevel;
					if (Enum.TryParse(fuelLevelKey, true, out fuelLevel) &&
						Enum.IsDefined(typeof (GlobalOrientSensor), fuelLevel))
						unit.SetSensorValue((int) fuelLevel, fuelSensors.Level);

					var fuelHeightKey = "FuelHeight" + fuelSensors.Number;
					GlobalOrientSensor fuelHeight;
					if (Enum.TryParse(fuelHeightKey, true, out fuelHeight) &&
						Enum.IsDefined(typeof (GlobalOrientSensor), fuelHeight))
						unit.SetSensorValue((int) fuelHeight, fuelSensors.Height);

					var fuelTemperatureKey = "FuelTemperature" + fuelSensors.Number;
					GlobalOrientSensor fuelTemperature;
					if (Enum.TryParse(fuelTemperatureKey, true, out fuelTemperature) &&
						Enum.IsDefined(typeof (GlobalOrientSensor), fuelTemperature))
						unit.SetSensorValue((int) fuelTemperature, fuelSensors.Temperature);
					continue;
				}

				if (deviceTextMessage != null)
				{
					//Обработка текстовых сообщений
					var ui = UnitInfo.Factory.Create(UnitInfoPurpose.MobilUnit);
					ui.DeviceID = deviceId;

					var par = new object[4];
					par[0] = ui;
					par[1] = -1;
					par[2] = deviceTextMessage.Text;
					par[3] = 0; //(int)deviceTextMessage.MessageID;
					var notify = new NotifyEventArgs(TerminalEventMessage.NE_MESSAGE, null, Severity.Lvl3,
						NotifyCategory.Controller, deviceTextMessage.Text, null, par);

					list.Add(notify);
					continue;
				}

				list.Add(o);
			}

			return list;
		}

		public override void OnNoData()
		{
			
		}

		public override void OnLinkClose()
		{
			// clear controller link
			_lnLink = null;
		}

		public override bool SupportData(byte[] data)
		{
			return NPLDatagram.IsDataSupported(data, 0);
		}

		public override IEnumerable<ITerminal> CreateTerminal(CmdType type)
		{
			string terminalName;
			switch (type)
			{
				case CmdType.UDP:
				case CmdType.Monitor:
				case CmdType.SendText:
				case CmdType.SetFirmware:
				case CmdType.GetSettings:
				case CmdType.SetSettings:
				case CmdType.Restart:
				case CmdType.Control:
					terminalName = "TCP";
					/*
					terminalName = Globals.AppSettings["Terminal"];
					if (string.IsNullOrEmpty(terminalName)) terminalName = "GPRS";
					*/ 
					break;

				case CmdType.GSM:
				case CmdType.Trace:
					terminalName = "GSM";
					break;

				case CmdType.GetLog:
					if (GlobalsConfig.AppSettings["HistoryByUDP"] != null &&
						GlobalsConfig.AppSettings["HistoryByUDP"].ToLower() == "true") goto case CmdType.UDP;
					terminalName = "Log";
					break;

				case CmdType.SMS:
				case CmdType.AskPosition:
					terminalName = "MLP";
					break;

				default: throw new NotSupportedException("Тип команды не известен - " + type);
			}
			Manager.CreateTerminal(terminalName);

			//Trace.WriteLine("GlobalOrient: CreateTerminal " + terminalName);

			yield return Manager.CreateTerminal(terminalName);
		}

		#endregion
	}
   
}
