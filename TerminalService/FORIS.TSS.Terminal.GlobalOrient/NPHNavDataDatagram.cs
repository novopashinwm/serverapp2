﻿using System;
using System.Collections;
using System.Collections.Generic;
using FORIS.TSS.Common;

namespace FORIS.TSS.Terminal.GlobalOrient
{
	public class NPHNavDataDatagram : NPHDatagram
	{
		public NPHNavDataDatagram(byte[] data, ushort NPL_requestID, NPLDatagram.NPL_ADDRESS NPL_address)
			: base(data, NPL_requestID, NPL_address) // (uint)data.Length)
		{
			ParseData();
		}

		protected IList<NPHNavDataRowBase> Rows;

		protected void ParseData()
		{
			Rows = NPHNavDataRowFactory.GetRows(bytes, NPH_PACKET_HEADER_SIZE);
		}

		public override ArrayList GetMessages()
		{
			ArrayList res = new ArrayList();

			foreach(NPHNavDataRowBase row in Rows)
			{
				ArrayList msgs = row.GetMessages();
				if (msgs != null && msgs.Count > 0)
				{
					res.AddRange(msgs);
				}
			}

			res.Add(CreateResultOKMessage());
			return res;
		}
	}

	public class NPHNavDataRowFactory
	{
		public static IList<NPHNavDataRowBase> GetRows(byte[] data, int pos)
		{
			IList<NPHNavDataRowBase> result = new List<NPHNavDataRowBase>();
			while (pos < data.Length)
			{
				byte type = data[pos];
				NPHNavDataRowBase row = null;
				switch (type)
				{
					case 0:
						row = new NPHNavDataRow(data, pos);
						break;
					case 2:
						row = new NPHSensorDataRow(data, pos);
						break;
					case 8:
						row = new NPHFuelDataRow(data, pos);
						break;
				}

				if (row == null)
					break;

				result.Add(row);
				
				pos += row.RowDataLen + NPHNavDataRowBase.ROW_HEADER_LEN;
			}

			return result;
		}
	}

	public class NPHFuelDataRow : NPHNavDataRowBase
	{
		public byte Status;
		public ushort Height;
		public ushort Level;
		public byte Temperature;

		public NPHFuelDataRow(byte[] data, int pos) : base(data, pos)
		{
			var dataReader = new DataReader(Data, 0);
			Status = dataReader.ReadByte();
			Height = (ushort) dataReader.ReadLittleEndian32(2);
			Level = (ushort) dataReader.ReadLittleEndian32(2);
			Temperature = dataReader.ReadByte();
		}

		public override ArrayList GetMessages()
		{
			return new ArrayList(1) { this };
		}
	}

	public class NPHSensorDataRow : NPHNavDataRowBase
	{
		public ushort AnalogueInput0;
		public ushort AnalogueInput1;
		public ushort AnalogueInput2;
		public ushort AnalogueInput3;
		public byte DigitalInput;
		public byte DigitalOutput;
		public ushort DigitalInput0;
		public ushort DigitalInput1;
		public ushort DigitalInput2;
		public ushort DigitalInput3;
		public uint Odometer;
		public byte SignalStrength;
		public byte GprsSignal;
		public byte AccelerometerEnergy;
		public byte AccelerometerAccel;

		public NPHSensorDataRow(byte[] data, int pos) : base(data, pos)
		{
			var dataReader = new DataReader(Data, 0);
			AnalogueInput0 = (ushort) dataReader.ReadLittleEndian32(2);
			AnalogueInput1 = (ushort)dataReader.ReadLittleEndian32(2);
			AnalogueInput2 = (ushort)dataReader.ReadLittleEndian32(2);
			AnalogueInput3 = (ushort)dataReader.ReadLittleEndian32(2);
			DigitalInput = dataReader.ReadByte();
			DigitalOutput = dataReader.ReadByte();
			DigitalInput0 = (ushort)dataReader.ReadLittleEndian32(2);
			DigitalInput1 = (ushort)dataReader.ReadLittleEndian32(2);
			DigitalInput2 = (ushort)dataReader.ReadLittleEndian32(2);
			DigitalInput3 = (ushort)dataReader.ReadLittleEndian32(2);
			Odometer = dataReader.ReadLittleEndian32(4);
			SignalStrength = dataReader.ReadByte();
			GprsSignal = dataReader.ReadByte();
			AccelerometerEnergy = dataReader.ReadByte();
			AccelerometerAccel = dataReader.ReadByte();
		}

		public override ArrayList GetMessages()
		{
			return new ArrayList(1) { this };
		}
	}

	public class NPHNavDataRow: NPHNavDataRowBase
	{
		public uint Time_Stamp;
		public float Lat;
		public float Lng;
		public byte Voltage;
		public bool FromBattery;
		public byte Flags;
		public bool CorrectGPS;
		public bool Alarm;
		public bool SOS;
		public int Speed;
		public int Course;
		public int Altitude;
		public int Satellites;

		public NPHNavDataRow(byte[] data, int pos): base(data, pos)
		{
			//TODO: parse data;
			Time_Stamp = BitConverter.ToUInt32(Data, 0);
			Lng = ((float)BitConverter.ToUInt32(Data, 4)) / 10000000;
			Lat = ((float)BitConverter.ToUInt32(Data, 8)) / 10000000;
			Flags = Data[12];
			Voltage = Data[13];

			FromBattery = (Flags & 0x10) > 0;
			CorrectGPS = (Flags & 0x80) > 0;
			Alarm = (Flags & 0x2) > 0;
			SOS = (Flags & 0x4) > 0;
			Speed = BitConverter.ToUInt16(Data, 14);
			Course = BitConverter.ToUInt16(Data, 18);
			Altitude = BitConverter.ToInt16(Data, 22);
			Satellites = Data[24];

			if (Speed < 3) Speed = 0;
		}

		
		public override ArrayList GetMessages()
		{
			return new ArrayList {this};
		}
	}

	public class NPHNavDataRowBase
	{
		public const int ROW_HEADER_LEN = 2;
		public byte Type;
		public byte Number;
		public byte[] Data;
		public int RowDataLen;

		public NPHNavDataRowBase(byte[] data, int pos)
		{
			Type = data[pos];
			Number= data[pos+1];
			RowDataLen = GetRowDataLen(Type);
			Data = new byte[RowDataLen];
			Array.Copy(data, pos + 2, Data, 0, RowDataLen);
		}

		public virtual ArrayList GetMessages()
		{
			return null;
		}

		public static int GetRowDataLen(byte type)
		{
			switch(type)
			{
				case 0:
					return 26;
				case 2:
					return 26;
				case 3:
					return 14;
				case 4:
					return 15;
				case 8:
					return 6;
				default:
					throw new ArgumentOutOfRangeException("Unknown NavData row type " + type);
			}
		}
	}
}
