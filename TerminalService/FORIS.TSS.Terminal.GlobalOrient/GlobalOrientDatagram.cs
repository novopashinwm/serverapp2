﻿using System;
using System.Diagnostics;
using FORIS.TSS.Communication;

namespace FORIS.TSS.Terminal.GlobalOrient
{
    public class GlobalOrientDatagramBase : Datagram
    {
        protected static TraceSwitch ts =
            new TraceSwitch("GlobalOrient", "GlobalOrient datagram related info");

        #region .ctor

        public GlobalOrientDatagramBase()
        {
        }

        public GlobalOrientDatagramBase(uint capacity)
            : base(capacity)
        {
        }

        #endregion // .ctor

        protected static byte[] GetRestOfBuffer(byte[] buffer, int pos)
        {
            if (buffer == null || buffer.Length <= pos) return null;

            var rest = new byte[buffer.Length - pos];
            Array.Copy(buffer, pos, rest, 0, buffer.Length - pos);
            return rest;
        }
    }
}
