﻿using System;
using System.Collections;
using System.Globalization;
using FORIS.TSS.Common;
using FORIS.TSS.Terminal.Terminal;

// ReSharper disable InconsistentNaming

namespace FORIS.TSS.Terminal.GlobalOrient
{
	public class NPHGenericControlDatagram : NPHDatagram
	{
		public uint DeviceID;
		public uint MaxPacketSize;

		public NPHGenericControlDatagram(uint capacity)
			: base(capacity)
		{
			if (PacketType == NPH_PACKET_TYPE.NPH_RESULT)
				return; DeviceID = BitConverter.ToUInt32(bytes, NPH_PACKET_HEADER_SIZE + 6);
			MaxPacketSize = BitConverter.ToUInt32(bytes, NPH_PACKET_HEADER_SIZE + 10);
		}


		public NPHGenericControlDatagram(byte[] data, ushort NPL_requestID, NPLDatagram.NPL_ADDRESS NPL_address)
			: base(data, NPL_requestID, NPL_address)
		{
			if (PacketType == NPH_PACKET_TYPE.NPH_RESULT)
				return;
			DeviceID = BitConverter.ToUInt32(bytes, NPH_PACKET_HEADER_SIZE + 6);
			MaxPacketSize = BitConverter.ToUInt32(bytes, NPH_PACKET_HEADER_SIZE + 10);
		}

		public override ArrayList GetMessages()
		{
			var res = new ArrayList();

			if (DeviceID != 0)
			{
				//Сохраняем в стейт ресивера ID устройства так как оно не приходит с другими пакетами
				var state = new GlobalOrientState
				{
					DeviceID = DeviceID.ToString(CultureInfo.InvariantCulture),
					MaxPacketSize = MaxPacketSize
				};

				res.Add(new ReceiverStoreToStateMessage {StateData = state});
			}

			//Ответ OK
			if ((Flags & NPH_FLAG_REQUEST) != 0)
			{
				var confirm = CreateResultOKMessage();
				UnitInfo ui = UnitInfo.Factory.Create(UnitInfoPurpose.MobilUnit);
				if (DeviceID != 0)
					ui.DeviceID = DeviceID.ToString(CultureInfo.InvariantCulture);
				confirm.UnitInfo = ui;
				res.Add(confirm);
			}

			if (RequestID != 0 && PacketType == NPH_PACKET_TYPE.NPH_RESULT)
			{
				res.Add(new GlobalOrientCommandResult(RequestID, Data.FromLE2UInt32(0, 4 >= Data.Length ? Data.Length : 4)));
			}

			return res;
		}
	}

	public class GlobalOrientCommandResult
	{
		public ushort RequestID { get; set; }
		public uint Error { get; set; }

		public GlobalOrientCommandResult(ushort requestID, uint error)
		{
			RequestID = requestID;
			Error     = error;
		}
	}
}