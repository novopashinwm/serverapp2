﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using FORIS.TSS.Terminal.Messaging;
using FORIS.TSS.TerminalService.Interfaces;
using FORIS.TSS.Terminal.Terminal;

namespace FORIS.TSS.Terminal.GlobalOrient
{
    public class GlobalOrientTcpReceiver : TCPReceiver
    {
        public int DeviceID = -1;

        public GlobalOrientTcpReceiver(TcpClient client): base(client)
        {
        }
    }
    /*
    class GlobalOrientTcpReceiver : TCPReceiver
    {
        public override event EventHandler OnData;
        public override event EventHandler OnProcessedData;

        public GlobalOrientTcpReceiver(TcpClient client)
            : base(client)
        {
            this.lastReceiveTime = DateTime.Now;

            this.OnData += GlobalOrientTcpReceiver_OnData;
            //this.Start();
        }

        void SendConfirmPacket(IList res)
        {
            if (res != null)
            {
                foreach (object o in res)
                {
                    if (o is NPLDatagram)
                    {
                        byte[] data = (o as NPLDatagram).GetBytes();

                        this.Send(data, data.Length);
                    }
                }
            }
        }

        void GlobalOrientTcpReceiver_OnData(object sender, EventArgs e)
        {
            try
            {
                if (this.Device != null)
                {
                    this.aRes = null;
                    byte[] bufferRest;
                    this.aRes = this.Device.OnData(((TcpOnDataEventArgs)e).Data, ((TcpOnDataEventArgs)e).Data.Length, out bufferRest);
                    if (this.OnProcessedData != null)
                    {
                        TcpOnProcessedDataEventArgs args = new TcpOnProcessedDataEventArgs();
                        args.aRes = this.aRes;
                        args.Address = m_client.Client.RemoteEndPoint as IPEndPoint;
                        foreach (Delegate dlgt in OnProcessedData.GetInvocationList())
                        {
                            //((EventHandler)dlgt).BeginInvoke(this, args, OnData_AsyncCallback, dlgt);
                            ((EventHandler)dlgt).Invoke(this, args);
                        }
                    }

                    SendConfirmPacket(this.aRes);
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine("TcpReceiver_OnData: " + ex.ToString());
            }
        }

        
        protected override void ReceiveData_Callback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the client socket 
                // from the asynchronous state object.
                StateObject state = (StateObject)ar.AsyncState;
                Socket client = state.workSocket;

                // Read data from the remote device.
                int bytesRead = 0;
                if (client.Connected)
                {
                    bytesRead = client.EndReceive(ar);
                }

                if (bytesRead > 0)
                {
                    byte[] d = new byte[bytesRead];
                    for (int n = 0; n < bytesRead; n++)
                    {
                        d[n] = state.buffer[n];
                    }

                    if (OnData != null)
                    {
                        TcpOnDataEventArgs args = new TcpOnDataEventArgs();
                        args.Address = m_client.Client.RemoteEndPoint as IPEndPoint;

                        args.Data = d;// state.data.ToArray();
                        //state.data.Clear();

                        foreach (Delegate dlgt in OnData.GetInvocationList())
                        {
                            ((EventHandler)dlgt).BeginInvoke(this, args, OnData_AsyncCallback, dlgt);
                        }
                        receive_counter++;
                    }
                    lastReceiveTime = DateTime.Now;

                    //state.data.AddRange(d);
                    // There might be more data, so store the data received so far.
                    //state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));

                    // Get the rest of the data.
                    client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReceiveData_Callback), state);
                }
                else
                {
                    // All the data has arrived; put it in response.
                    if (state.data.Count > 1)
                    {
                        //string response = state.sb.ToString();

                        if (OnData != null)
                        {
                            TcpOnDataEventArgs args = new TcpOnDataEventArgs();
                            args.Address = m_client.Client.RemoteEndPoint as IPEndPoint;

                            args.Data = state.data.ToArray();
                            state.data.Clear();

                            foreach (Delegate dlgt in OnData.GetInvocationList())
                            {
                                ((EventHandler)dlgt).BeginInvoke(this, args, OnData_AsyncCallback, dlgt);
                            }
                            receive_counter++;
                        }
                        lastReceiveTime = DateTime.Now;
                    }
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.ToString());
            }
        }
    }
    */
}
