﻿using System;
using System.Collections;
using FORIS.TSS.Communication;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.GlobalOrient
{
	/// <summary>
	/// Пакеты уровня представления(верхнего уровня)
	/// </summary>
	public class NPHDatagram : GlobalOrientDatagramBase
	{
		#region Const
		
		public const int NPH_PACKET_HEADER_SIZE = 10;

		/// <summary>
		/// NPH service types
		/// </summary>
		public enum NPH_SERVICE_TYPE: ushort
		{
			GENERIC_CONTROLS = 0,
			NAVDATA          = 1,
			FILE_TRANSFER    = 3,
			CLIENT_LIST      = 4,
			EXTERNAL_DEVICE  = 5,
			DEBUG            = 6
		}

		protected ushort NPH_FLAG_REQUEST = 0x0001;

		public enum NPH_PACKET_TYPE: ushort
		{
			NPH_RESULT   =                           0

			/*NPH_SRV_GENERIC_CONTROLS packets*/
			, NPH_SGC_RESULT              =            NPH_RESULT
			, NPH_SGC_CONN_REQUEST            =        100
			, NPH_SGC_CONN_AUTH_STRING       =         101
			, NPH_SGC_SERVICE_REQUEST         =        110
			, NPH_SGC_SERVICES_REQUEST        =        111
			, NPH_SGC_SERVICES                =        112
			, NPH_SGC_PEER_DESC_REQUEST       =        120
			, NPH_SGC_PEER_DESC               =        121
			, NPH_SET_PRIA				=			   130
			, NPH_SET_PRNAV			=				   131
			// PHOTO
			, NPH_GET_PAR_PHOTO      =                 141
			, NPH_SET_PAR_PHOTO      =                 142
			, NPH_PAR_PHOTO_SD       =                 143
			, NPH_PAR_PHOTO_GPRS     =                 144

			, NPH_PAR_PHOTO_MASK_MODE_SD      =        0x3
			, NPH_PAR_PHOTO_MASK_MODE_GPRS    =        0xC

			// DIGITAL OUTPUT
			, NPH_SET_PRDO          =                  150
			, NPH_GET_PRDO           =                 151
			, NPH_PRDO                =                152
			// MODE ALARM
			, NPH_SET_MODALARM            =            160
			, NPH_GET_MODALARM             =           161
			, NPH_MODALARM                  =          162

			/*NPH_SRV_NAVDATA packets*/
			, NPH_SND_RESULT                =          NPH_RESULT
			, NPH_SND_HISTORY                =         100
			, NPH_SND_REALTIME                =        101

			/*NPH_SRV_CLIENT_LIST packets*/
			, NPH_SCL_RESULT                   =       NPH_RESULT
			, NPH_SCL_CLIENT_LIST_REQUEST       =      100
			, NPH_SCL_CLIENT_LIST                =     101
			, NPH_SCL_CLIENT_STATUS_REQUEST       =    102

					/*NPH_SRV_EXTERNAL_DEVICE packets*/
			, NPH_SED_TITLE_DATA = 100
			, NPH_SED_DEVICE_DATA = 101
			, NPH_SED_DEVICE_RESULT = 102
			}

		public enum NPH_RESULT: uint
		{
			  OK            =               0x00000000
			, UNDEFINED      =              0xFFFFFFFF
			, BUSY            =             1

			/*service errors*/
			, SERVICE_NOT_SUPPORTED =       100
			, SERVICE_NOT_ALLOWED    =      101
			, SERVICE_NOT_AVIALABLE   =     102
			//, SERVICE_BUSY           =    103
			, SERVICE_NOT_CONSECUTION   =   104
			, SERVICE_NOT_DEVICE_ADDRESS = 	105

			/*packet errors*/
			, PACKET_NOT_SUPPORTED  =       200
			, PACKET_INVALID_SIZE    =      201
			, PACKET_INVALID_FORMAT   =     202
			, PACKET_INVALID_PARAMETER =    203
			, PACKET_UNEXPECTED         =   204

			/*connection errors*/
			, PROTO_VER_NOT_SUPPORTED =     300
			, CLIENT_NOT_REGISTERED    =    301
			, CLIENT_TYPE_NOT_SUPPORTED =   302
			, CLIENT_AUTH_FAILED         =  303
			, INVALID_ADDRESS             = 304
			, CLIENT_ALREADY_REGISTERED    =305

			/*database errors*/
			, DATABASE_QUERY_FAILED    =    400
			, DATABASE_DOWN             =   401

			/*message errors*/
			, CODEC_NOT_SUPPORTED       =  	500
		 }

		#endregion Const

		#region .ctor

		public NPHDatagram(uint capacity)
			: base(capacity)
		{
		}

		public NPHDatagram()
			: this(NPH_PACKET_HEADER_SIZE)
		{
		}

		protected ushort NPL_RequestID;
		protected NPLDatagram.NPL_ADDRESS NPL_Address;

		public NPHDatagram(byte[] data, ushort NPL_requestID, NPLDatagram.NPL_ADDRESS NPL_address)
			: this((uint)data.Length)
		{
			if (data == null || data.Length < NPH_PACKET_HEADER_SIZE)
			{
				throw new DatagramNotFoundException(
					"Datagram not found or too short", GetType(), data, 0);
			}

			NPL_RequestID = NPL_requestID;
			NPL_Address = NPL_address;
			Array.Copy(data, bytes, data.Length);
		}

		#endregion // .ctor

		#region атрибуты пакета

		public NPH_SERVICE_TYPE ServiceType
		{
			get
			{
				var res = (NPH_SERVICE_TYPE) BitConverter.ToUInt16(bytes, 0);
				return res;
			}
			set
			{
				byte[] b = BitConverter.GetBytes((ushort)value);
				bytes[0] = b[0];
				bytes[1] = b[1];
			}
		}

		public NPH_PACKET_TYPE PacketType
		{
			get
			{
				var res = (NPH_PACKET_TYPE) BitConverter.ToUInt16(bytes, 2);
				return res;
			}
			set
			{
				byte[] b = BitConverter.GetBytes((ushort)value);
				bytes[2] = b[0];
				bytes[3] = b[1];
			}
		}

		protected ushort Flags
		{
			get
			{
				return BitConverter.ToUInt16(bytes, 4);
			}
			set
			{
				byte[] b = BitConverter.GetBytes(value);
				bytes[4] = b[0];
				bytes[5] = b[1];
			}
		}

		/// <summary>
		/// флаг необходимости подтверждения запроса
		/// </summary>
		public bool IsNeedToConfirm
		{
			get
			{
				if (bytes == null || bytes.Length < NPH_PACKET_HEADER_SIZE) return false;
				return (Flags & NPH_FLAG_REQUEST) > 0;
			}
			set
			{
				if (value)
				{
					Flags = (ushort)(Flags | NPH_FLAG_REQUEST);
				}
				else
				{
					Flags = (ushort)(Flags & ~NPH_FLAG_REQUEST);
				}
			}
		}

		public UInt16 RequestID
		{
			get
			{
				return BitConverter.ToUInt16(bytes, 6);
			}
			set
			{
				byte[] b = BitConverter.GetBytes(value);
				bytes[6] = b[0];
				bytes[7] = b[1];
			}
		}


		protected byte[] Data
		{
			set
			{
				int dataSize = bytes.Length - NPH_PACKET_HEADER_SIZE;
				if (dataSize != value.Length)
				{
					Array.Resize(ref bytes, NPH_PACKET_HEADER_SIZE + value.Length);
				}

				Array.Copy(value, 0, bytes, NPH_PACKET_HEADER_SIZE, value.Length);
			}
			get
			{
				int dataSize = bytes.Length - NPH_PACKET_HEADER_SIZE;
				var result = new byte[dataSize];
				Array.Copy(bytes, NPH_PACKET_HEADER_SIZE, result, 0, dataSize);
				return result;
			}
		}

		#endregion атрибуты пакета


		#region методы
		public virtual ArrayList GetMessages()
		{
			if (!IsNeedToConfirm) return null;

			//Формируем пакет подтверждения даже если не знаем что за сообщение, чтобы больше не слал

			var res = new ArrayList {CreateResultOKMessage()};
			return res;
		}

		protected ConfirmPacket CreateResultOKMessage()
		{
			var resultDG = new NPHResultDatagram(ServiceType, RequestID, NPH_RESULT.OK);
			var nplDG = NPLDatagram.CreateResponse(resultDG, NPL_RequestID, NPL_Address);

			return new ConfirmPacket(nplDG.GetBytes());
		}

		protected static ConfirmPacket CreateConfirmPacket(NPHDatagram dg, ushort NPL_requestID, NPLDatagram.NPL_ADDRESS NPL_address)
		{
			var nplDG = NPLDatagram.CreateResponse(dg, NPL_requestID, NPL_address);
			return new ConfirmPacket(nplDG.GetBytes());
		}

		#endregion методы

		#region static methods
		public static NPH_SERVICE_TYPE GetServiceType(byte[] data)
		{
			var res = (NPH_SERVICE_TYPE) BitConverter.ToUInt16(data, 0);
			return res;
		}

		public static NPH_PACKET_TYPE GetPacketType(byte[] data)
		{
			var res = (NPH_PACKET_TYPE) BitConverter.ToUInt16(data, 2);
			return res;
		}

		#endregion static methods
	}
}