﻿namespace FORIS.TSS.Terminal.GlobalOrient
{
    public enum GlobalOrientSensor
    {
        Alarm = 0,
        Sos = 1,
        FromBattery = 2,
        BatteryVoltage = 3,

        AnalogueInput0 = 11,
        AnalogueInput1 = 12,
        AnalogueInput2 = 13,
        AnalogueInput3 = 14,
        DigitalInput = 20,
        DigitalInput0 = 21,
        DigitalInput1 = 22,
        DigitalInput2 = 23,
        DigitalInput3 = 24,
        DigitalOutput = 25,
        Odometer = 31,
        SignalStrength = 41,
        GprsSignal = 42,
        AccelerometerEnergy = 43,
        AccelerometerAccel = 44,

        FuelStatus0 = 51,
        FuelHeight0 = 52,
        FuelLevel0 = 53,
        FuelTemperature0 = 54,

        FuelStatus1 = 61,
        FuelHeight1 = 62,
        FuelLevel1 = 63,
        FuelTemperature1 = 64
    }
}
