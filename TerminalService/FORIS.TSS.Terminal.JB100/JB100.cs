﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using FORIS.TSS.BusinessLogic;

namespace FORIS.TSS.Terminal.JB100
{
	public class JB100 : Device
	{
		internal const string ProtocolName = "STAR TRACKER JB-100";
		internal static class DeviceNames
		{
			internal const string JB100 = "STAR TRACKER JB-100";
		}
		public JB100() : base(typeof(JB100Sensor), new[]
		{
			DeviceNames.JB100,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			if (data.Length < 16)
				return false;

			if (data[0] != '#' || data[data.Length - 2] != '#' || data[data.Length - 1] != '#')
				return false;

			int i;
			for (i = 1; i != data.Length && data[i] != '#'; ++i)
			{
				if (data[i] < '0' || '9' < data[i])
					return false;
			}
			var digitsCount = i - 1;

			if (digitsCount < 15 || 17 < digitsCount)
				return false;

			return true;
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;
			var reader = new Reader(Encoding.ASCII.GetString(data));

			reader.Skip(1);
			var deviceID = reader.ReadDeviceID();
			reader.Skip(3);
			var packetType = reader.ReadString(); //AUT - normal, SOS - alarm

			var packetCount = reader.ReadInt();
			var result = new ArrayList(packetCount);
			for (var i = 0; i != packetCount; ++i)
			{
				reader.Skip(1);
				var positionString = reader.ReadString();
				var positionParts = positionString.Split(',');

				var longitude = ParseDegrees(positionParts[0]);

				var longitudeSign = positionParts[1] == "E" ? 1 : -1;
				longitude *= longitudeSign;

				var latitude = ParseDegrees(positionParts[2]);
				var latitudeSign = positionParts[3] == "N" ? 1 : 0;
				latitude *= latitudeSign;

				var speed = double.Parse(positionParts[4], NumberFormatInfo);
				var course = double.Parse(positionParts[5], NumberFormatInfo);
				var logTime = reader.ReadLogTime();

				var mu = UnitReceived();
				mu.DeviceID = deviceID;
				mu.Longitude = (double)longitude;
				mu.Latitude = (double)latitude;
				mu.Speed = (int)speed;
				mu.Course = (int)course;
				mu.Time = logTime;
				mu.Satellites = 4;
				mu.CorrectGPS = true;
				mu.SensorValues = new Dictionary<int, long>
				{{(int) JB100Sensor.Alarm, packetType == "SOS" ? 1 : 0}};
				result.Add(mu);
			}

			return result;
		}
		class Reader : SplittedStringReader
		{
			public Reader(string s) : base(s, '#')
			{
			}

			public string ReadDeviceID()
			{
				var imei = ReadString();
				return imei;
			}

			public int ReadInt()
			{
				return int.Parse(ReadString());
			}

			public int ReadLogTime()
			{
				var dateString = ReadString();
				var timeString = ReadString();
				var dateTime = new DateTime(
					2000 + int.Parse(dateString.Substring(4, 2)),
					int.Parse(dateString.Substring(2, 2)),
					int.Parse(dateString.Substring(0, 2)),
					int.Parse(timeString.Substring(0, 2)),
					int.Parse(timeString.Substring(2, 2)),
					int.Parse(timeString.Substring(4, 2)),
					DateTimeKind.Utc);
				return TimeHelper.GetSecondsFromBase(dateTime);
			}
		}
		private static readonly IFormatProvider NumberFormatInfo = new NumberFormatInfo { NumberDecimalSeparator = "." };
		private static decimal ParseDegrees(string s)
		{
			var raw = decimal.Parse(s, NumberFormatInfo);
			var degrees = (int)(raw / 100m);
			var minutes = raw - degrees * 100m;
			return degrees + minutes / 60;
		}
	}
}