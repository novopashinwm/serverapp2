﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.TerminalService.Interfaces;
using FORIS.TSS.TerminalService.Interfaces.Exceptions;

namespace FORIS.TSS.Terminal.GPS103A
{
	public class GPS103ADevice : Device
	{
		private static readonly ConfirmPacket LogonConfirmPacket     = new ConfirmPacket(Encoding.ASCII.GetBytes("LOAD"));
		private static readonly ConfirmPacket HeartbeatConfirmPacket = new ConfirmPacket(Encoding.ASCII.GetBytes("ON"));
		private const string ImmobilizeCommand   = "stop";
		private const string DeimmobilizeCommand = "resume";
		private const string CantDeimmobilizeBySpeed = "it will be executed after speed less than 20km/h";
		private static readonly char[] PacketPartSeparator = { ';' };

		private const string LogonPrefix = "##,imei:";
		private const string CommandPrefix = "imei:";
		internal static class DeviceNames
		{
			internal const string AnywhereTK106 = "Anywhere TK106";
			internal const string AnywhereTK108 = "Anywhere TK108";
			internal const string CobanGPS304B  = "Coban GPS304B";
			internal const string CobanGPS305   = "Coban GPS305";
			internal const string CobanGPS306A  = "Coban GPS306A";
			internal const string CobanTK102    = "Coban TK102";
			internal const string CobanTK1022   = "Coban TK102-2";
			internal const string CobanTK103    = "Coban TK103";
			internal const string CobanTK1042   = "Coban TK104-2";
			internal const string CobanTK104SD  = "Coban TK104 SD";
			internal const string CobanTK106    = "Coban TK106";
			internal const string CobanTK1062   = "Coban TK106-2";
			internal const string XexunTK101    = "Xexun TK101";
			internal const string XexunTK102    = "Xexun TK102";
			internal const string XexunTK1022   = "Xexun TK102-2";
			internal const string XexunTK103    = "Xexun TK103";
			internal const string XexunTK1032   = "Xexun TK103-2";
			internal const string XexunTK1042   = "Xexun TK104-2";
			internal const string XexunTK104SD  = "Xexun TK104 SD";
			internal const string XexunTK201    = "Xexun TK201";
			internal const string XexunTK2012   = "Xexun TK201-2";
			internal const string XexunTK2013   = "Xexun TK201-3";
			internal const string XexunXT008    = "Xexun XT008";
			internal const string XexunXT009    = "Xexun XT009";
			internal const string XexunXT107    = "Xexun XT107";
		}
		public GPS103ADevice() : base(null, new[] {
			DeviceNames.AnywhereTK106,
			DeviceNames.AnywhereTK108,
			DeviceNames.CobanGPS304B,
			DeviceNames.CobanGPS305,
			DeviceNames.CobanGPS306A,
			DeviceNames.CobanTK102,
			DeviceNames.CobanTK1022,
			DeviceNames.CobanTK103,
			DeviceNames.CobanTK1042,
			DeviceNames.CobanTK104SD,
			DeviceNames.CobanTK106,
			DeviceNames.CobanTK1062,
			DeviceNames.XexunTK101,
			DeviceNames.XexunTK102,
			DeviceNames.XexunTK1022,
			DeviceNames.XexunTK103,
			DeviceNames.XexunTK1032,
			DeviceNames.XexunTK1042,
			DeviceNames.XexunTK104SD,
			DeviceNames.XexunTK201,
			DeviceNames.XexunTK2012,
			DeviceNames.XexunTK2013,
			DeviceNames.XexunXT008,
			DeviceNames.XexunXT009,
			DeviceNames.XexunXT107,
		})
		{
		}

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;
			var s = Encoding.ASCII.GetString(data);
			var parts = s.Split(PacketPartSeparator, StringSplitOptions.RemoveEmptyEntries);

			var result = new List<object>();
			foreach (var part in parts)
			{
				if (part.StartsWith(LogonPrefix))
				{
					result.Add(LogonConfirmPacket);

					var imei = part.Substring(LogonPrefix.Length, ImeiHelper.ImeiLength);

					result.Add(new ConfirmPacket(Encoding.ASCII.GetBytes("**,imei:" + imei + ",C,10s")));
					continue;
				}

				if (part.Length == 15 && part.All(Char.IsDigit))
				{
					result.Add(HeartbeatConfirmPacket);
					var imei = part;
					result.Add(new ConfirmPacket(Encoding.ASCII.GetBytes("**,imei:" + imei + ",B")));

					continue;
				}

				if (part.StartsWith(CommandPrefix))
				{
					var mu = ParseCommand(part);
					if (mu != null)
						result.Add(mu);
					continue;
				}

				Trace.TraceWarning("Unable to parse GPS103A packet: {0}", part);
			}
			
			return result;
		}
		private MobilUnit.Unit.MobilUnit ParseCommand(string s)
		{
			var reader = new GPS103AReader(s);
			var prefix = reader.ReadString(); //prefix
			var imei = prefix.Substring(CommandPrefix.Length);
			reader.Skip(1); //command, e.g. "tracker"
			var dateTime = reader.ReadDateTime(); //date time with accuracy to minutes
			reader.Skip(1); //??? e.g. 13554900601
			var readString = reader.ReadString();
			if (readString == "L")
			{
				var mu = UnitReceived(); //NO GPS signal
				mu.DeviceID = imei;
				mu.Time = TimeHelper.GetSecondsFromBase(DateTime.UtcNow);
				mu.CorrectGPS = false;
				return mu;
			}

			var utcTimeOfDay = reader.ReadUtcTimeOfDay(); //UTC time of day 112909.397
			reader.Skip(1); //??? e.g. A
			var lat = reader.ReadLatitude();
			var lng = reader.ReadLongitude();
			var speedMps = reader.ReadDecimal();

			var time = dateTime.HasValue && utcTimeOfDay.HasValue
				? dateTime.Value.Date + utcTimeOfDay.Value
				: DateTime.UtcNow;

			var speed = speedMps != null ? (int) Math.Round(speedMps.Value) : (int?) null;
			reader.Skip(1); //??? empty string

			var unit = UnitReceived();
			unit.DeviceID = imei;
			unit.Time = TimeHelper.GetSecondsFromBase(time);
			unit.Longitude = (double)lng;
			unit.Latitude = (double)lat;
			unit.Speed = speed;
			unit.Course = null;
			unit.Satellites = MobilUnit.Unit.MobilUnit.MinSatellites;
			unit.CorrectGPS = true;
			return unit;
		}
		public override bool SupportData(byte[] data)
		{
			var s = Encoding.ASCII.GetString(data);
			return s.StartsWith(LogonPrefix) || s.StartsWith(CommandPrefix);
		}
		public override List<CommandStep> GetCommandSteps(IStdCommand command)
		{
			switch (command.Type)
			{
				case CmdType.Setup:
					return GetSetupCommandSteps(command);
				case CmdType.Immobilize:
					return GetImmobilizeCommandSteps(command);
				case CmdType.Deimmobilize:
					return GetDeimmobilizeCommandSteps(command);
				case CmdType.QuickImmobilizeON:
					return GetQuickImmobilizeONCommandSteps(command);
				case CmdType.QuickImmobilizeOFF:
					return GetQuickImmobilizeOFFCommandSteps(command);
				case CmdType.SetInterval:
					return GetSetIntervalCommandSteps(command);
				case CmdType.AskPosition:
					return GetAskPositionCommandSteps(command);
				case CmdType.ReloadDevice:
					return GetReloadDeviceCommandSteps(command);
				case CmdType.SetModeOnline:
					return GetSetModeOnlineCommandSteps(command);
				case CmdType.SetModeWaiting:
					return GetSetModeWaitingCommandSteps(command);
				case CmdType.CancelAlarm:
					return GetCancelAlarmCommandSteps(command);
				case CmdType.MonitorModeOn:
					return GetMonitorModeOnCommandSteps(command);
				case CmdType.MonitorModeOff:
					return GetMonitorModeOffCommandSteps(command);
				default:
					return base.GetCommandSteps(command);
			}
		}
		public override IList<object> ParseSms(IUnitInfo ui, string text)
		{
			IList<object> result;
			if (TryParseImmobilizeDeimmobilizeCommands(ui, text, out result))
				return result;
			if (TryParseSetupCommand(ui, text, out result))
				return result;
			return result;
		}
		private string GetPassword(IStdCommand command)
		{
			var password = command.Target.Password;
			if (string.IsNullOrWhiteSpace(password))
				password = "123456";
			return password;
		}
		private List<CommandStep> GetSetupCommandSteps(IStdCommand command)
		{
			if (CmdType.Setup != command.Type)
				throw new CommandSendingFailureException(CmdResult.Failed);

			var devPsw = GetPassword(command);
			var apnCfg = GetInternetApnConfig(command);
			var srvAdd = Manager.GetConstant(Constant.ServerIP);
			var srvPrt = Manager.GetConstant(Constant.ServerPort);
			switch (command.Target.DeviceType)
			{
				case DeviceNames.CobanGPS304B:
				case DeviceNames.CobanGPS305:
					return new[]
					{
						new { Wait = false, Text = $@"begin{devPsw}"                          },
						new { Wait = false, Text = $@"apn{devPsw} {apnCfg.Name}"              },
						new { Wait = false, Text = $@"up{devPsw} {apnCfg.User} {apnCfg.Pass}" },
						new { Wait = false, Text = $@"tracker{devPsw}"                        },
						new { Wait = false, Text = $@"save060s***n{devPsw}"                   },
						new { Wait = false, Text = $@"time zone{devPsw} 0"                    },
						new { Wait = false, Text = $@"gprs{devPsw} on"                        },
						new { Wait = false, Text = $@"adminip{devPsw} {srvAdd} {srvPrt}"      },
					}
						.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						.ToList<CommandStep>();
				case DeviceNames.CobanTK1042:
				case DeviceNames.CobanTK104SD:
					return new[]
					{
						new { Wait = true, Text = $@"begin{devPsw}"                          },
						new { Wait = true, Text = $@"apn{devPsw} {apnCfg.Name}"              },
						new { Wait = true, Text = $@"up{devPsw} {apnCfg.User} {apnCfg.Pass}" },
						new { Wait = true, Text = $@"tracker{devPsw}"                        },
						new { Wait = true, Text = $@"sleep{devPsw} off"                      },
						new { Wait = true, Text = $@"save030s***n{devPsw}"                   },
						new { Wait = true, Text = $@"time zone{devPsw} 0"                    },
						new { Wait = true, Text = $@"gprs{devPsw} on"                        },
						new { Wait = true, Text = $@"adminip{devPsw} {srvAdd} {srvPrt}"      },
					}
						.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						.ToList<CommandStep>();
				case DeviceNames.XexunTK103:
					return new[]
					{
						new { Wait = true, Text = $@"begin{devPsw}"                          },
						new { Wait = true, Text = $@"apn{devPsw} {apnCfg.Name}"              },
						new { Wait = true, Text = $@"apnuser{devPsw} {apnCfg.User}"          },
						new { Wait = true, Text = $@"apnpasswd{devPsw} {apnCfg.Pass}"        },
						new { Wait = true, Text = $@"tracker{devPsw}"                        },
						new { Wait = true, Text = $@"save030s***n{devPsw}"                   },
						new { Wait = true, Text = $@"time zone{devPsw} 0"                    },
						new { Wait = true, Text = $@"gprs{devPsw} on"                        },
						new { Wait = true, Text = $@"adminip{devPsw} {srvAdd} {srvPrt}"      },
					}
						.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						.ToList<CommandStep>();
				case DeviceNames.XexunTK102:
					return new[]
					{
						new { Wait = true, Text = $@"begin{devPsw}"                          },
						new { Wait = true, Text = $@"tracker{devPsw}"                        },
						new { Wait = true, Text = $@"imei{devPsw}"                           },
						new { Wait = true, Text = $@"time zone{devPsw} 0"                    },
						new { Wait = true, Text = $@"apn{devPsw} {apnCfg.Name}"              },
						new { Wait = true, Text = $@"up{devPsw} {apnCfg.User} {apnCfg.Pass}" },
						new { Wait = true, Text = $@"adminip{devPsw} {srvAdd} {srvPrt}"      },
						new { Wait = true, Text = $@"gprs{devPsw}"                           },
					}
						.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						.ToList<CommandStep>();
				default:
					return new[]
					{
						new { Wait = true, Text = $@"begin{devPsw}"                          },
						new { Wait = true, Text = $@"tracker{devPsw}"                        },
						new { Wait = true, Text = $@"imei{devPsw}"                           },
						new { Wait = true, Text = $@"time zone{devPsw} 0"                    },
						new { Wait = true, Text = $@"apn{devPsw} {apnCfg.Name}"              },
						new { Wait = true, Text = $@"adminip{devPsw} {srvAdd} {srvPrt}"      },
						new { Wait = true, Text = $@"gprs{devPsw}"                           },
					}
						.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						.ToList<CommandStep>();
			}
		}
		private bool TryParseSetupCommand(IUnitInfo ui, string text, out IList<object> result)
		{
			result = new List<object>();

			if (text.EndsWith("ok!", StringComparison.OrdinalIgnoreCase) ||
				ui.DeviceType == DeviceNames.XexunTK102 && text.EndsWith(" ok", StringComparison.OrdinalIgnoreCase))
			{
				result.Add(CommandCompleted(ui, CmdResult.Completed, CmdType.Setup));
			}
			else if (text.StartsWith("IMEI:"))
			{
				result.Add(ImeiReceived(ui, text.Substring("IMEI:".Length)));
				result.Add(CommandCompleted(ui, CmdResult.Completed, CmdType.Setup));
			}
			else if (ui.DeviceType == DeviceNames.XexunTK102 && text.All(char.IsDigit) && ImeiHelper.IsImeiCorrect(text))
			{
				result.Add(ImeiReceived(ui, text));
				result.Add(CommandCompleted(ui, CmdResult.Completed, CmdType.Setup));
			}
			return result.Any();
		}
		private List<CommandStep> GetImmobilizeCommandSteps(IStdCommand command)
		{
			if (CmdType.Immobilize != command.Type)
				throw new CommandSendingFailureException(CmdResult.Failed);

			var devPsw = GetPassword(command);
			switch (command.Target.DeviceType)
			{
				case DeviceNames.CobanTK1042:
				case DeviceNames.CobanTK104SD:
				case DeviceNames.XexunTK103:
					return new[]
					{
						new { Wait = true, Text = $@"{ImmobilizeCommand}{devPsw}" },
					}
						.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						.ToList<CommandStep>();
				default:
					return Enumerable.Empty<CommandStep>().ToList();
			}
		}
		private List<CommandStep> GetDeimmobilizeCommandSteps(IStdCommand command)
		{
			if (CmdType.Deimmobilize != command.Type)
				throw new CommandSendingFailureException(CmdResult.Failed);

			var devPsw = GetPassword(command);
			switch (command.Target.DeviceType)
			{
				case DeviceNames.CobanTK1042:
				case DeviceNames.CobanTK104SD:
				case DeviceNames.XexunTK103:
					return new[]
					{
						new { Wait = true, Text = $@"{DeimmobilizeCommand}{devPsw}" },
					}
						.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						.ToList<CommandStep>();
				default:
					return Enumerable.Empty<CommandStep>().ToList();
			}
		}
		private bool TryParseImmobilizeDeimmobilizeCommands(IUnitInfo ui, string text, out IList<object> result)
		{
			result = new List<object>(1);
			if (text.ToLower() == CantDeimmobilizeBySpeed)
				return true;
			if (text.ToLower().StartsWith(ImmobilizeCommand))
				result.Add(CommandCompleted(ui, CmdResult.Completed, CmdType.Immobilize));
			if (text.ToLower().StartsWith(DeimmobilizeCommand))
				result.Add(CommandCompleted(ui, CmdResult.Completed, CmdType.Deimmobilize));
			return result.Any();
		}
		private List<CommandStep> GetQuickImmobilizeONCommandSteps(IStdCommand command)
		{
			if (CmdType.QuickImmobilizeON != command.Type)
				throw new CommandSendingFailureException(CmdResult.Failed);

			var devPsw = GetPassword(command);
			switch (command.Target.DeviceType)
			{
				case DeviceNames.CobanTK1042:
				case DeviceNames.CobanTK104SD:
				case DeviceNames.XexunTK103:
					return new[]
					{
						new { Wait = false, Text = $@"quickstop{devPsw}" },
					}
						.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						.ToList<CommandStep>();
				default:
					return Enumerable.Empty<CommandStep>().ToList();
			}
		}
		private List<CommandStep> GetQuickImmobilizeOFFCommandSteps(IStdCommand command)
		{
			if (CmdType.QuickImmobilizeOFF != command.Type)
				throw new CommandSendingFailureException(CmdResult.Failed);

			var devPsw = GetPassword(command);
			switch (command.Target.DeviceType)
			{
				case DeviceNames.CobanTK1042:
				case DeviceNames.CobanTK104SD:
				case DeviceNames.XexunTK103:
					return new[]
					{
						new { Wait = false, Text = $@"noquickstop{devPsw}" },
					}
						.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						.ToList<CommandStep>();
				default:
					return Enumerable.Empty<CommandStep>().ToList();
			}
		}
		private List<CommandStep> GetSetIntervalCommandSteps(IStdCommand command)
		{
			if (CmdType.SetInterval != command.Type)
				throw new CommandSendingFailureException(CmdResult.Failed);

			var devPsw = GetPassword(command);
			switch (command.Target.DeviceType)
			{
				case DeviceNames.CobanGPS304B:
				case DeviceNames.CobanGPS305:
				case DeviceNames.CobanTK1042:
				case DeviceNames.CobanTK104SD:
				case DeviceNames.XexunTK103:
					return new[]
					{
						new { Wait = false, Text = $@"save060s***n{devPsw}" },
					}
						.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						.ToList<CommandStep>();
				default:
					return Enumerable.Empty<CommandStep>().ToList();
			}
		}
		private List<CommandStep> GetAskPositionCommandSteps(IStdCommand command)
		{
			if (CmdType.AskPosition != command.Type)
				throw new CommandSendingFailureException(CmdResult.Failed);

			var devPsw = GetPassword(command);
			switch (command.Target.DeviceType)
			{
				case DeviceNames.CobanGPS304B:
				case DeviceNames.CobanGPS305:
				case DeviceNames.CobanTK1042:
				case DeviceNames.CobanTK104SD:
				case DeviceNames.XexunTK103:
					return new[]
					{
						new { Wait = false, Text = $@"address{devPsw}" },
					}
						.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						.ToList<CommandStep>();
				default:
					return Enumerable.Empty<CommandStep>().ToList();
			}
		}
		private List<CommandStep> GetReloadDeviceCommandSteps(IStdCommand command)
		{
			if (CmdType.ReloadDevice != command.Type)
				throw new CommandSendingFailureException(CmdResult.Failed);

			var devPsw = GetPassword(command);
			switch (command.Target.DeviceType)
			{
				case DeviceNames.CobanGPS304B:
				case DeviceNames.CobanGPS305:
				case DeviceNames.CobanTK1042:
				case DeviceNames.CobanTK104SD:
				case DeviceNames.XexunTK103:
					return new[]
					{
						new { Wait = false, Text = $@"reset{devPsw}" },
					}
						.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						.ToList<CommandStep>();
				default:
					return Enumerable.Empty<CommandStep>().ToList();
			}
		}
		private List<CommandStep> GetSetModeOnlineCommandSteps(IStdCommand command)
		{
			if (CmdType.SetModeOnline != command.Type)
				throw new CommandSendingFailureException(CmdResult.Failed);

			var devPsw = GetPassword(command);
			switch (command.Target.DeviceType)
			{
				case DeviceNames.CobanTK1042:
				case DeviceNames.CobanTK104SD:
					return new[]
					{
						new { Wait = false, Text = $@"sleep{devPsw} off" },
					}
						.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						.ToList<CommandStep>();
				default:
					return Enumerable.Empty<CommandStep>().ToList();
			}
		}
		private List<CommandStep> GetSetModeWaitingCommandSteps(IStdCommand command)
		{
			if (CmdType.SetModeWaiting != command.Type)
				throw new CommandSendingFailureException(CmdResult.Failed);

			var devPsw = GetPassword(command);
			switch (command.Target.DeviceType)
			{
				case DeviceNames.CobanTK1042:
				case DeviceNames.CobanTK104SD:
					return new[]
					{
						new { Wait = false, Text = $@"sleep{devPsw} shock" },
					}
						.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						.ToList<CommandStep>();
				default:
					return Enumerable.Empty<CommandStep>().ToList();
			}
		}
		private List<CommandStep> GetCancelAlarmCommandSteps(IStdCommand command)
		{
			if (CmdType.CancelAlarm != command.Type)
				throw new CommandSendingFailureException(CmdResult.Failed);

			var devPsw = GetPassword(command);
			switch (command.Target.DeviceType)
			{
				case DeviceNames.CobanTK1042:
				case DeviceNames.CobanTK104SD:
				case DeviceNames.XexunTK103:
					return new[]
					{
						new { Wait = false, Text = $@"help me!" },
					}
						.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						.ToList<CommandStep>();
				default:
					return Enumerable.Empty<CommandStep>().ToList();
			}
		}
		private List<CommandStep> GetMonitorModeOnCommandSteps(IStdCommand command)
		{
			if (CmdType.MonitorModeOn != command.Type)
				throw new CommandSendingFailureException(CmdResult.Failed);

			var devPsw = GetPassword(command);
			switch (command.Target.DeviceType)
			{
				case DeviceNames.CobanTK1042:
				case DeviceNames.CobanTK104SD:
				case DeviceNames.XexunTK103:
					return new[]
					{
						new { Wait = false, Text = $@"monitor{devPsw}" },
					}
						.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						.ToList<CommandStep>();
				default:
					return Enumerable.Empty<CommandStep>().ToList();
			}
		}
		private List<CommandStep> GetMonitorModeOffCommandSteps(IStdCommand command)
		{
			if (CmdType.MonitorModeOff != command.Type)
				throw new CommandSendingFailureException(CmdResult.Failed);

			var devPsw = GetPassword(command);
			switch (command.Target.DeviceType)
			{
				case DeviceNames.CobanTK1042:
				case DeviceNames.CobanTK104SD:
				case DeviceNames.XexunTK103:
					return new[]
					{
						new { Wait = false, Text = $@"tracker{devPsw}" },
					}
						.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						.ToList<CommandStep>();
				default:
					return Enumerable.Empty<CommandStep>().ToList();
			}
		}
	}
}