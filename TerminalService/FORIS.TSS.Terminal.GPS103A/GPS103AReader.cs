﻿using System;
using System.Linq;

namespace FORIS.TSS.Terminal.GPS103A
{
	class GPS103AReader : SplittedStringReader
	{
		public GPS103AReader(string s) : base(s, ',')
		{
		}

		public DateTime? ReadDateTime()
		{
			var dateString = ReadString();

			if (dateString.All(c => c == '0'))
				return null;

			var year = int.Parse(dateString.Substring(0, 2));
			var month = int.Parse(dateString.Substring(2, 2));
			var day = int.Parse(dateString.Substring(4, 2));
			var hours = int.Parse(dateString.Substring(6, 2));
			var minutes = int.Parse(dateString.Substring(8, 2));
			if (month == 0 || day == 0)
			{
				return null;
			}

			var dateTime = new DateTime(2000 + year, month, day, hours, minutes, 0, DateTimeKind.Utc);
			return dateTime;
		}

		public decimal ReadLatitude()
		{
			var s = ReadString();
			var reader = new StringReader(s);
			var degrees = reader.ReadDecimal(2);
			var minutes = reader.ReadDecimal(7);
			var result = degrees + minutes/60;
			var hemisphere = ReadString();
			return (hemisphere == "S") ? -result : result;
		}

		public decimal ReadLongitude()
		{
			var s = ReadString();
			var reader = new StringReader(s);
			var degrees = reader.ReadDecimal(3);
			var minutes = reader.ReadDecimal(6);
			var result = degrees + minutes / 60;
			var hemisphere = ReadString();
			return (hemisphere == "W") ? -result : result;
		}

		public TimeSpan? ReadUtcTimeOfDay()
		{
			var s = ReadString();
			if (string.IsNullOrEmpty(s)) return null;

			var reader = new StringReader(s);
			var hours = reader.ReadDecimalInt(2);
			var minutes = reader.ReadDecimalInt(2);
			var seconds = reader.ReadDecimalInt(2);

			return new TimeSpan(hours, minutes, seconds);
		}
	}
}