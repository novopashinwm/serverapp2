﻿using System;
using System.Collections;
using System.Linq;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace Compass.Ufin.Terminal.Devices.Arnavi
{
	public class ArnaviDevice : Device
	{
		public ArnaviDevice() : base(typeof(ArnaviSensor))
		{
		}

		private const byte HEADER_VERSION_1 = 0x22;
		private const byte HEADER_VERSION_2 = 0x23;

		enum ContentType
		{
			Data       = 0x01,
			Text       = 0x03,
			File       = 0x04,
			DataBinary = 0x06
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;
			if (stateData == null)
				return ProcessHeader(data);

			var imei = ((ReceiverState) stateData).DeviceID;

			if (data[0] == 0x5B)
			{
				return ProcessPackage(data, imei);
			}
			return null;
		}
		private IList ProcessPackage(byte[] data, string imei)
		{
			var r = new DataReader(data);
			// Header 5B
			r.Skip();
			//parcel number from 0x01 to 0xFB
			var parcelNumber = r.ReadByte();

			var result = new ArrayList();

			while (r.Any())
			{
				var contentType = (ContentType)r.ReadByte();
				var length      = (int)r.ReadLittleEndian32(2);
				var time        = r.ReadLittleEndian32(4);

				switch (contentType)
				{
					case ContentType.Data:
						var mu = ReadMobilUnit(new DataReader(r.ReadBytes(length)));
						if (mu != null)
						{
							mu.Time     = (int)time;
							mu.DeviceID = imei;
							result.Add(mu);
						}
						// TODO checksum
						r.Skip(1);
						break;
					default:
						var notSupportedPacket = r.ReadBytes(length);
						// not supported yet
						$"{this.GetTypeName()}.{nameof(ProcessPackage)} error: Content type (packet type) not supported yet\r\nData:\r\n{BitConverter.ToString(notSupportedPacket)}"
							.TraceWarning();
						return null;
				}

				var final = r.ReadByte();
				if (final != 0x5D)
					r.Rewind(1);
			}

			result.Add(new ConfirmPacket(new byte[] { 0x7b, 0x00, parcelNumber, 0x7d }));

			return result;
		}
		enum VarNum
		{
			Latitude             = 3,
			Longitude            = 4,
			SpeedSatHeightCourse = 5,
			LacCid               = 7,
			SignalLevelMccMnc    = 8,
			Ds                   = 9,
			DilutionOfPrecision  = 151
		}
		private IMobilUnit ReadMobilUnit(DataReader r)
		{
			var mu = new MobilUnit();
			var gpsDataParts = 0;
			var totalCount   = 0;
			var defaultCount = 0;
			while (r.Any())
			{
				++totalCount;

				var n = (VarNum) r.ReadByte();

				switch (n)
				{
					case VarNum.Latitude:
						mu.Latitude = r.ReadSingle();
						++gpsDataParts;
						break;
					case VarNum.Longitude:
						mu.Longitude = r.ReadSingle();
						++gpsDataParts;
						break;
					case VarNum.SpeedSatHeightCourse:
						mu.Course = r.ReadByte() * 2;
						mu.Height = r.ReadByte() * 10;
						var sat   = r.ReadByte();
						mu.Satellites = (sat >> 4) + (sat & 0xf);
						mu.Speed  = (int) Math.Round(r.ReadByte() * 1.852);
						++gpsDataParts;
						break;
					case VarNum.DilutionOfPrecision:
						mu.SetSensorValue((int) ArnaviSensor.Hdop, r.ReadLittleEndian32(2));
						r.Skip(2);
						break;
					case VarNum.Ds:
						ParseDs(mu, r.ReadLittleEndian32(4));
						break;
					case VarNum.LacCid:
					{
						var cellNetworks = mu.CellNetworks ?? (mu.CellNetworks = new CellNetworkRecord[1]);
						cellNetworks[0].LAC = (int) r.ReadLittleEndian32(2);
						cellNetworks[0].CellID = (int) r.ReadLittleEndian32(2);
					}
						break;
					case VarNum.SignalLevelMccMnc:
					{
						var cellNetworks = mu.CellNetworks ?? new CellNetworkRecord[1];
						cellNetworks[0].SignalStrength = r.ReadByte();
						cellNetworks[0].CountryCode = r.ReadLittleEndian32(2).ToString();
						cellNetworks[0].NetworkCode = r.ReadByte().ToString();
					}
						break;
					default:
						++defaultCount;
						mu.SetSensorValue((int)n, r.ReadLittleEndian32(4));
						break;
				}
			}

			if (defaultCount == totalCount)
				return null;

			mu.CorrectGPS = gpsDataParts == 3;
			return mu;
		}
		private void ParseDs(MobilUnit mu, uint value)
		{
			for (var i = 0; i != 7; ++i)
			{
				mu.SetSensorValue((int) ArnaviSensor.InputIN0 + i, (value & (1 << 0)) != 0 ? 1 : 0);
			}

			for (var i = 8; i != 8 + 4; ++i)
			{
				mu.SetSensorValue((int) ArnaviSensor.OutputOUT0 + i-8, (value & (1 << 0)) != 0 ? 1 : 0);
			}
		}
		private static IList ProcessHeader(byte[] data)
		{
			var imei = GetImei(data);
			var results = new ArrayList();
			switch (data[1])
			{
				case HEADER_VERSION_2:
					var confirmData = BitConverter.GetBytes((uint)DateTime.UtcNow
						.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))
						.TotalSeconds);
					results.Add(new ConfirmPacket((new byte[] { 0x7B, 0x04, 0x00 })
						.Union(new byte[] { (byte)confirmData.Aggregate<byte, uint>(0, (crc, val) => (crc + val) & 0xFF) })
						.Union(confirmData)
						.Union(new byte[] { 0x7D })
						.ToArray()));
					break;
				case HEADER_VERSION_1:
				default:
					results.Add(new ConfirmPacket(new byte[] { 0x7B, 0x00, 0x00, 0x7D }));
					break;
			}
			results.Add(new ReceiverStoreToStateMessage
			{
				StateData = new ReceiverState { DeviceID = imei }
			});
			return results;
		}
		public override bool SupportData(byte[] data)
		{
			if (data.Length != 10)
				return false;

			if (data[0] != 0xff)
				return false;

			var imei = GetImei(data);
			return ImeiHelper.IsImeiCorrect(imei);
		}
		private static string GetImei(byte[] data)
		{
			var r = new DataReader(data, 2);
			var imei = r.ReadLittleEndian64(8).ToString();
			return imei;
		}
	}
}