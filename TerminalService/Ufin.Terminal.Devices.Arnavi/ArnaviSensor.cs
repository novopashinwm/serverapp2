namespace Compass.Ufin.Terminal.Devices.Arnavi
{
	public enum ArnaviSensor
	{
		Hdop       = 0151,
		InputIN0   = 1000,
		InputIN1   = 1001,
		InputIN2   = 1002,
		InputIN3   = 1003,
		InputIN4   = 1004,
		InputIN5   = 1005,
		InputIN6   = 1006,
		InputIN7   = 1007,
		OutputOUT0 = 1010,
		OutputOUT1 = 1011,
		OutputOUT2 = 1012,
		OutputOUT3 = 1013,
	}
}