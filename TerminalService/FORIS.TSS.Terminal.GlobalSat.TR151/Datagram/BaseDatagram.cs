﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.GlobalSat.TR151.Datagram
{
	/// <summary>
	/// устройство возвращает только один вид сообщений - месторасположения
	/// поэтому нет смысла плодить дейтаграммы
	/// </summary>
	public class BaseDatagram
	{
		protected static readonly Regex DeviceMessageRegex = new Regex(@"\$([0-9.EN]{1,},?){12}!"),
			ArrayParametersRegex = new Regex(@"[0-9.EN]{1,}(?=,|!)");

		public int Status { get; private set; }
		public string DeviceId { get; private set; }
		public DateTime Date { get; private set; }
		public double Longitude { get; private set; }
		public double Latitude { get; private set; }
		public double Altitude { get; private set; }
		public int Speed { get; private set; }
		public int Satellites { get; private set; }
		/// <summary>
		/// проверка валидности сообщения
		/// </summary>
		/// <param name="paket"></param>
		/// <returns></returns>
		public static bool IsValid(string paket)
		{
			if (string.IsNullOrEmpty(paket)) return false;
			return DeviceMessageRegex.IsMatch(paket);
		}

		public static BaseDatagram Init(string packet)
		{
			if (!IsValid(packet)) return null;

			var dataArray = (from Match match in ArrayParametersRegex.Matches(packet) select match.Value).ToArray();
			var result = new BaseDatagram
			{
				DeviceId = dataArray[0]
			};

			DateTime dateTime;
			if (!DateTime.TryParseExact(dataArray[3] + dataArray[4], "ddMMyyHHmmss", null, DateTimeStyles.AdjustToUniversal, out dateTime))
				return null;
			result.Date = dateTime;


			var ci = CultureInfo.InvariantCulture.Clone() as CultureInfo;
			ci.NumberFormat.NumberDecimalSeparator = ".";

			result.Longitude = Convert.ToInt32(dataArray[5].Substring(1, 3)) + Convert.ToDouble(dataArray[5].Substring(4, dataArray[5].Length - 4), ci) / 60.0
				* (dataArray[5][0] == 'E' ? 1 : -1);

			result.Latitude = Convert.ToInt32(dataArray[6].Substring(1, 2)) + Convert.ToDouble(dataArray[6].Substring(3, dataArray[6].Length - 3), ci) / 60.0
				* (dataArray[6][0] == 'N' ? 1 : -1);


			//dddmm.mmmm
			//ddmm.mmmm

			result.Altitude = Convert.ToDouble(dataArray[7], ci);
			result.Speed = (int)Convert.ToDouble(dataArray[8], ci);
			result.Satellites = Convert.ToInt32(dataArray[10]);
			result.Status = Convert.ToInt32(dataArray[1]);
			return result;
		}

		public IList GetResponses()
		{
			var result = new ArrayList();
			var munit = new MobilUnit.Unit.MobilUnit
			{
				DeviceID = DeviceId,
				cmdType = CmdType.Trace,
				Longitude = Longitude,
				Latitude = Latitude,
				Time = TimeHelper.GetSecondsFromBase(Date),
				Speed = Speed,
				Status = Status == 5 ? BusinessLogic.Enums.Status.Alarm : BusinessLogic.Enums.Status.None,
				Satellites = Satellites,
				CorrectGPS = true,
				SensorValues = new Dictionary<int, long>
				{
					{ (int)DeviceStatus.Aalarm,    (Status == 5  ? 1 : 0) },
					{ (int)DeviceStatus.BataryLow, (Status == 14 ? 1 : 0) }
				}
			};
			munit.Properties.Add(DeviceProperty.Protocol, GlobalSatTR151.ProtocolName);

			result.Add(munit);
			return result;
		}

		/// <summary> Статус устройства </summary>
		enum DeviceStatus
		{
			Aalarm    = 1,
			BataryLow = 2
		}
	}
}