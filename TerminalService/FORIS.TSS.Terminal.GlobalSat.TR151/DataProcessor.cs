﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FORIS.TSS.Terminal.GlobalSat.TR151.Datagram;


namespace FORIS.TSS.Terminal.GlobalSat.TR151
{
    public class DataProcessor
    {
        public static IList Process(byte[] data, out byte[] bufferRest)
        {
            bufferRest = null;

            if (data == null || data.Length == 0)
                return null;
            var alRes = new ArrayList();
            var dataString = Encoding.ASCII.GetString(data);
            var responses = ProcessPacket(dataString);
            if (responses != null && responses.Count > 0)
                alRes.AddRange(responses);
            return alRes;
        }

        private static IList ProcessPacket(string packet)
        {
            BaseDatagram datagramm = BaseDatagram.Init(packet);
            if (datagramm == null) return null;
            return datagramm.GetResponses();
        }
    }
}
