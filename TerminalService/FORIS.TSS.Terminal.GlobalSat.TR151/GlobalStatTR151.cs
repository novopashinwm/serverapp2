﻿using System.Collections;
using System.Text;

namespace FORIS.TSS.Terminal.GlobalSat.TR151
{
	public class GlobalSatTR151 : Device
	{
		internal const string ProtocolName = "GlobalSat TR-151";
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			return DataProcessor.Process(data, out bufferRest);
		}

		/// <summary>
		/// check format, example $355632000166323,1,1,040202,093633,E12129.2252,N2459.8891,00161,0.0100,147,07,2.4!
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public override bool SupportData(byte[] data)
		{
			if (data == null) return false;
			var paket = Encoding.ASCII.GetString(data);
			return Datagram.BaseDatagram.IsValid(paket);
		}
	}
}