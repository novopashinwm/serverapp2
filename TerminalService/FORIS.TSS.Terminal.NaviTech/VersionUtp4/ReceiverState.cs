﻿using System;

namespace FORIS.TSS.Terminal.NaviTech.VersionUtp4
{
    [Serializable]
    public class ReceiverState : TSS.Terminal.ReceiverState
    {
        public bool ReplyEnabled;

        public override string ToString()
        {
            return base.ToString() + ", ReplyEnabled:" + ReplyEnabled;
        }
    }
}
