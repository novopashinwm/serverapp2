﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.NaviTech.VersionUtp4
{
	public class NaviTechDevice : Device
	{
		public override bool SupportData(byte[] data)
		{
			const int imeiPacketSize = 66;
			if (data.Length < imeiPacketSize) return false;
			if (data[2] != imeiPacketSize) return false;
			if (data[3] != 1 && data[3] != 3) return false;

			byte computedCrc = 0;
			for (var i = 0; i != imeiPacketSize - 1; ++i)
				computedCrc ^= data[i];

			var receivedCrc = data[imeiPacketSize - 1];
			if (computedCrc != receivedCrc)
				return false;

			const int imeiLength = 15;
			for (var i = 4; i != 4 + imeiLength; ++i)
			{
				if (data[i] < '0' || '9' < data[i])
					return false;
			}

			return true;
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			var result = new ArrayList(count / 66);

			var reader = new NaviTechReader(data, 0, count);
			var packetParsedCount = 0;
			var receiverState = stateData as ReceiverState;
			string deviceId = receiverState != null ? receiverState.DeviceID : null;
			bool replyEnabled = receiverState != null && receiverState.ReplyEnabled;
			
			while (reader.Any())
			{
				if (reader.RemainingBytesCount < 3)
					break;

				reader.Skip(2); //Номер устройства

				var length = reader.ReadByte(); //Длина пакета

				var packetStartIndex = reader.Position - 3;
				var packetEndIndex = packetStartIndex + length;

				if (count < packetEndIndex)
				{
					reader.Rewind(3);
					break;
				}

				if (length == 0x84)
				{
					//Message packet
					reader.Skip(length - 3);
					continue;
				}

				var packetType = (NaviTechPacketType)reader.ReadByte(); //Тип пакета

				if (packetType == NaviTechPacketType.DataPacket ||
					packetType == NaviTechPacketType.ImeiPacket)
				{
					var validCrc = CheckCrc(data, packetStartIndex, packetEndIndex);

					if (!validCrc)
					{
						Trace.TraceWarning("NaviTech.VersionUtp4: invalid crc, data = " + BitConverter.ToString(data));
						bufferRest = null;
						return null;
					}
				}
				
				switch (packetType)
				{
					case NaviTechPacketType.DataPacket:
						result.Add(ReadDataPacket(reader, deviceId));
						break;
					case NaviTechPacketType.ImeiPacket:
						var imei = reader.ReadAsciiString(ImeiLength);
						deviceId = imei;
						reader.Skip(); //hardware type
						var replyEnabledValue = reader.ReadByte();
						replyEnabled = replyEnabledValue == 0x02;
						result.Add(new ReceiverStoreToStateMessage
							{
								StateData = new ReceiverState
									{
										DeviceID = deviceId,
										ReplyEnabled = replyEnabled
									}
							});
						reader.Skip(ImeiPacketLength - reader.BytesRead);
						break;
					default:
						//Пакет ASC-6
						result.Add(ParseAsc6Device(reader, deviceId, packetType));
						break;
				}

				//Дочитать пакет до конца
				if (packetEndIndex > reader.Position)
				{
					reader.Skip(packetEndIndex - reader.Position);
				}

				++packetParsedCount;
			}

			bufferRest = reader.Any() ? reader.ReadBytes(reader.RemainingBytesCount) : null;
			
			if (replyEnabled)
				result.Add(new ConfirmPacket {Data = new byte[] {0x2A, 0x2A, 0x2A, (byte) packetParsedCount, 0x2A}});

			return result;
		}
		private const int ImeiPacketLength = 0x42;
		private const int ImeiLength       = 15;
		private static bool CheckCrc(byte[] data, int packetStartIndex, int packetEndIndex)
		{
			var crcReader = new NaviTechReader(data, packetStartIndex, packetEndIndex);
			byte computedCrc = 0;
			while (1 < crcReader.RemainingBytesCount)
				computedCrc ^= crcReader.ReadByte();

			var receivedCrc = crcReader.ReadByte();

			return receivedCrc == computedCrc;
		}
		private IMobilUnit ParseAsc6Device(NaviTechReader reader, string deviceId, NaviTechPacketType type)
		{
			var mu = UnitReceived();
			mu.CorrectGPS = false;
			mu.SensorValues = new Dictionary<int, long>();
			mu.DeviceID = deviceId;

			mu.FirmwareVersion = reader.ReadByte().ToString(CultureInfo.InvariantCulture);
			reader.Skip(2); //GPS_PNTR - номер пакета по порядку
			var status = reader.ReadLittleEndian32(2);
			mu.SensorValues.Add((int)Asc6Sensor.Reboot, status & 0x01);
			mu.SensorValues.Add((int)Asc6Sensor.Sim, (status >> 1) & 0x01);
			mu.SensorValues.Add((int)Asc6Sensor.Connection, 1 - ((status >> 2) & 0x01));
			mu.SensorValues.Add((int)Asc6Sensor.Guard, ((status >> 3) & 0x01));
			mu.SensorValues.Add((int)Asc6Sensor.LowVoltage, ((status >> 4) & 0x01));
			mu.CorrectGPS = ((status >> 5) & 0x01) == 0;
			mu.SensorValues.Add((int)Asc6Sensor.Motion, 1 - ((status >> 7) & 0x01));
			mu.SensorValues.Add((int)Asc6Sensor.OuterPower, 1 - ((status >> 7) & 0x01));
			mu.SensorValues.Add((int)Asc6Sensor.Alarm, ((status >> 8) & 0x01));
			mu.SensorValues.Add((int)Asc6Sensor.GpsAntennaCut, ((status >> 9) & 0x01));
			mu.SensorValues.Add((int)Asc6Sensor.GpsAntennaShortCircuit, ((status >> 10) & 0x01));

			mu.Latitude = reader.ReadSingle();
			mu.Longitude = reader.ReadSingle();

			var courseValue = (ushort)reader.ReadLittleEndian32(2);
			mu.Course = (int)Math.Round(courseValue * 0.1);

			var speedValue = (ushort)reader.ReadLittleEndian32(2);
			mu.Speed = (int)Math.Round(speedValue * 0.1);

			mu.SensorValues.Add((int)Asc6Sensor.Acc, reader.ReadByte());

			mu.Height = (int)reader.ReadLittleEndian32(2);

			mu.SensorValues.Add((int)Asc6Sensor.Hdop, reader.ReadByte());

			var satellitesCount = reader.ReadByte();
			mu.Satellites = satellitesCount & 0xF;
			mu.SensorValues.Add((int)Asc6Sensor.GlonassSatellites, (satellitesCount >> 4) & 0xF);

			mu.Time = GetCorrectedTime((int) reader.ReadLittleEndian32(4));

			mu.SensorValues.Add((int)Asc6Sensor.PowerVoltage, reader.ReadLittleEndian32(2));
			mu.SensorValues.Add((int)Asc6Sensor.BatteryVoltage, reader.ReadLittleEndian32(2));

			if ((type & NaviTechPacketType.Acc) != 0)
			{
				mu.SensorValues.Add((int)Asc6Sensor.Vibration, reader.ReadByte());
				mu.SensorValues.Add((int)Asc6Sensor.VibrationCount, reader.ReadByte());
				var @out = reader.ReadByte();
				mu.SensorValues.Add((int)Asc6Sensor.Out0, @out & 0x01);
				mu.SensorValues.Add((int)Asc6Sensor.Out1, (@out >> 1) & 0x01);
				mu.SensorValues.Add((int)Asc6Sensor.Out2, (@out >> 2) & 0x01);
				mu.SensorValues.Add((int)Asc6Sensor.Out3, (@out >> 3) & 0x01);

				var inAlarm = reader.ReadByte();
				mu.SensorValues.Add((int)Asc6Sensor.InputAlarm0, inAlarm & 0x01);
				mu.SensorValues.Add((int)Asc6Sensor.InputAlarm1, (inAlarm >> 1) & 0x01);
				mu.SensorValues.Add((int)Asc6Sensor.InputAlarm2, (inAlarm >> 2) & 0x01);
				mu.SensorValues.Add((int)Asc6Sensor.InputAlarm3, (inAlarm >> 3) & 0x01);
				mu.SensorValues.Add((int)Asc6Sensor.InputAlarm4, (inAlarm >> 4) & 0x01);
				mu.SensorValues.Add((int)Asc6Sensor.InputAlarm5, (inAlarm >> 5) & 0x01);
				mu.SensorValues.Add((int)Asc6Sensor.InputAlarm6, (inAlarm >> 6) & 0x01);
				mu.SensorValues.Add((int)Asc6Sensor.InputAlarm7, (inAlarm >> 7) & 0x01);
			}

			if ((type & NaviTechPacketType.AnalogInputs) != 0)
			{
				mu.SensorValues.Add((int)Asc6Sensor.In0, (short)reader.ReadLittleEndian32(2));
				mu.SensorValues.Add((int)Asc6Sensor.In1, (short)reader.ReadLittleEndian32(2));
				mu.SensorValues.Add((int)Asc6Sensor.In2, (short)reader.ReadLittleEndian32(2));
				mu.SensorValues.Add((int)Asc6Sensor.In3, (short)reader.ReadLittleEndian32(2));
				mu.SensorValues.Add((int)Asc6Sensor.In4, (short)reader.ReadLittleEndian32(2));
				mu.SensorValues.Add((int)Asc6Sensor.In5, (short)reader.ReadLittleEndian32(2));
			}

			if ((type & NaviTechPacketType.Impulse) != 0)
			{
				mu.SensorValues.Add((int)Asc6Sensor.Counter0, reader.ReadLittleEndian32(4));
				mu.SensorValues.Add((int)Asc6Sensor.Counter1, reader.ReadLittleEndian32(4));
			}

			if ((type & NaviTechPacketType.FlsTemp) != 0)
			{
				mu.SensorValues.Add((int)Asc6Sensor.FuelLevel0, reader.ReadLittleEndian32(2));
				mu.SensorValues.Add((int)Asc6Sensor.FuelLevel1, reader.ReadLittleEndian32(2));
				mu.SensorValues.Add((int)Asc6Sensor.FuelLevel2, reader.ReadLittleEndian32(2));
				mu.SensorValues.Add((int)Asc6Sensor.Temperature0, (sbyte)reader.ReadByte());
				mu.SensorValues.Add((int)Asc6Sensor.Temperature1, (sbyte)reader.ReadByte());
				mu.SensorValues.Add((int)Asc6Sensor.Temperature2, (sbyte)reader.ReadByte());
			}

			if ((type & NaviTechPacketType.Can) != 0)
			{
				var canSize = reader.ReadByte();
				var remainingCanBytes = canSize - 1;
				while (0 < remainingCanBytes)
				{
					var key = reader.ReadByte();
					--remainingCanBytes;
					var canTag = key & 0x3F;
					var bytes = 0x01 >> ((key & 0xC0) >> 6);
					var canValue = reader.ReadLittleEndian64(bytes);
					remainingCanBytes -= canSize;
					mu.SensorValues.Add((int)Asc6Sensor.CAN0 + canTag, (long)canValue);
				}
			}

			return mu;
		}
		private int GetCorrectedTime(int time)
		{
			var dateTime = TimeHelper.GetDateTimeUTC(time);
			if (dateTime - DateTime.UtcNow < TimeSpan.FromDays(1))
				return time;
			return TimeHelper.GetSecondsFromBase(dateTime.AddMonths(-1));
		}
		private IMobilUnit ReadDataPacket(NaviTechReader reader, string deviceId)
		{
			reader.Skip(1); //Cycl - служебное
			reader.Skip(2); //Номер пакета в памяти устройства
			reader.Skip(2); //Status
			var mu = UnitReceived();
			mu.SensorValues = new Dictionary<int, long>();
			mu.DeviceID = deviceId;
			mu.Firmware = reader.ReadByte().ToString(CultureInfo.InvariantCulture);
			reader.Skip(4); //Данные с акселерометра
			for (var i = 0; i != 6; ++i)
				mu.SensorValues.Add((int) (NaviTechSensor.FirstInput + i), reader.ReadUint(2));
			reader.Skip(1); //Значения выходов (?)
			mu.SensorValues.Add((int) NaviTechSensor.AccumulatorVoltage, reader.ReadUint(2));
			mu.SensorValues.Add((int) NaviTechSensor.PowerVoltage, reader.ReadUint(2));
			var validPosition = reader.ReadByte() == 0;
			var secondsFromMidnight = reader.ReadUint(4);
			var dateValue = reader.ReadUint(4);
			mu.Time = TimeHelper.GetSecondsFromBase(
				new DateTime((int) (dateValue%100 + 2000), (int) ((dateValue%10000)/100),
							 (int) (dateValue/10000))
					.AddSeconds(secondsFromMidnight));
			mu.Latitude = reader.ReadSingle();
			mu.Longitude = reader.ReadSingle();
			mu.Height = (int) reader.ReadSingle();
			mu.Speed = (int) reader.ReadSingle();
			mu.Course = (int) reader.ReadSingle();
			reader.Skip(4); //HDOP, single
			mu.Satellites = reader.ReadByte();
			reader.Skip(1); //CRC
			mu.CorrectGPS = validPosition;
			
			return mu;
		}
	}
}