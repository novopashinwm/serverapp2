﻿using System.Text;
using FORIS.TSS.Common;

namespace FORIS.TSS.Terminal.NaviTech.VersionUtp4
{
	class NaviTechReader : DataReader
	{
		public NaviTechReader(byte[] data, int startIndex, int endIndex) : base(data, startIndex, endIndex)
		{
		}
		public uint ReadUint(int length)
		{
			uint result = 0;
			for (var i = 0; i != length; ++i)
				result |= (uint)(ReadByte() << (i * 8));
			return result;
		}
		/// <summary>
		/// Читает из массива строку, в формате [LENGTH, STRING]
		/// LENGTH - длина строки, 1 байт
		/// STRING - содержимое строки длиной LENGTH в формате ASCII
		/// </summary>
		public string ReadAsciiString8()
		{
			var length = ReadByte();
			var result = Encoding.ASCII.GetString(ReadBytes(length));
			return result;
		}
		public string ReadBytesAsHexString(int count)
		{
			const string hexTable = "0123456789ABCDEF";

			var result = new StringBuilder(count * 2);
			for (var i = 0; i != count; ++i)
			{
				byte b = ReadByte();
				result.Append(hexTable[b / 16]);
				result.Append(hexTable[b % 16]);
			}

			return result.ToString();
		}
		public int ReadInt(int count)
		{
			var result = ReadUint(count);

			if ((result >> (count * 8 - 1)) == 0)
				return (int)result;

			for (var i = 4; i > count; --i)
				result |= (uint)(0xFF << ((i - 1) * 8));

			return (int)result;
		}
	}
}