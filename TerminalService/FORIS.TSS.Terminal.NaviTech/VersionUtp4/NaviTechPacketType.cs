﻿using System;

namespace FORIS.TSS.Terminal.NaviTech.VersionUtp4
{
	[Flags]
	enum NaviTechPacketType
	{
		/// <summary> Пакет с данными, ASC5 </summary>
		DataPacket   = 1,
		/// <summary> Пакет с IMEI </summary>
		ImeiPacket   = 3,
		/// <summary>акселерометр, выходы, события по входам</summary>
		Acc          = 0x04,
		/// <summary>аналоговые входы</summary>
		AnalogInputs = 0x08,
		/// <summary>импульсные/дискретные входы</summary>
		Impulse      = 0x10,
		/// <summary>датчики уровня топлива и температуры</summary>
		FlsTemp      = 0x20,
		/// <summary>данные с CAN шины</summary>
		Can          = 0x40
	}
}