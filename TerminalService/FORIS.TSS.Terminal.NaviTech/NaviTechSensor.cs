﻿namespace FORIS.TSS.Terminal.NaviTech
{
    enum NaviTechSensor
    {
        PowerVoltage = 0,
        AccumulatorVoltage = 1,
        Temperature = 3,
        Acceleration0 = 4,
        Acceleration1 = 5,
        Acceleration2 = 6,
        FirstInput = 30
    }
}
