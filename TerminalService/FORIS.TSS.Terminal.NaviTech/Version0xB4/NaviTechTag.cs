﻿namespace FORIS.TSS.Terminal.NaviTech.Version0xB4
{
	enum NaviTechTag
	{
		/// <summary> Модель устройства – целое 1 байт </summary>
		DeviceModel = 0x01,
		/// <summary> Версия прошивки – целое 1 байт </summary>
		FirmwareVersion = 0x02,
		/// <summary> IMEI – 15 байт </summary>
		Imei = 0x03,
		/// <summary> Номер устройства – целое 2 байта </summary>
		DeviceNumber = 0x04,
		/// <summary> Номер пакета – целое 2 байта </summary>
		PacketNumber = 0x10,
		/// <summary> Следующий пакет – без параметров </summary>
		NextPacket = 0x11,
		/// <summary> Дата/время – целое 4 байта, в формате UTC (секунды с 01.01.1970 00:00:00) </summary>
		DateTime = 0x20,
		/// <summary> Изменение даты/времени – целое 1 байт </summary>
		DateTimeChange = 0x21,
		/// <summary> Координаты в движении – запись </summary>
		/// <remarks>
		/// •	кол-во спутников/признак валидности – целое 1 байт; кол-во спутников в младшей тетраде, валидность – в старшей
		/// •	широта – целое 4 байта (6 знаков после точки), градусы
		/// •	долгота – целое 4 байта (6 знаков после точки), градусы 
		/// </remarks>
		CoordinatesInMotion = 0x30,
		/// <summary> Скорость и направление движения – запись </summary>
		/// <remarks>
		/// •	скорость – целое 2 байта (1 знак после точки), км/ч
		/// •	направление – целое 2 байта (1 знак после точки), градусы
		/// </remarks>
		SpeedAndDirection = 0x33,
		/// <summary> Высота - целое 2 байта, м </summary>
		Altitude = 0x34,
		/// <summary> HDOP – целое 1 байт (1 знак после точки) </summary>
		HDOP = 0x35,
		/// <summary> Статус – целое 2 байта </summary>
		Status = 0x40,
		/// <summary> Напряжение питания – целое 2 байта (3 знака после точки) </summary>
		PowerVoltage = 0x41,
		/// <summary> Напряжение на аккумуляторе – целое 2 байта (3 знака после точки) </summary>
		AccumulatorVoltage = 0x42,
		/// <summary> Температура – целое 1 байт </summary>
		Temperature = 0x43,
		/// <summary> Данные с акселерометра – запись, 10 бит по каждой оси – целое 4 байта </summary>
		Acceleration = 0x44,
		/// <summary> Состояние выходов - целое 2 байта </summary>
		OutputStates = 0x45,
		/// <summary> Состояние входов - целое 2 байта </summary>
		InputStates = 0x46,
		/// <summary>
		/// 50..5F – Данные со входов – 50..5F в зависимости от номера входа (50..57 – аналоговые входя IN0..IN7, далее цифровые входы)
		/// Примеры:
		/// 	аналоговый вход 50 35 14 – 5.173 В
		/// 	вход типа счетчик 50 D2 04 – 1234
		/// 	цифровой вход 50 0B 03 – 779
		/// </summary>
		FirstInput = 0x50,
		/// <summary> Номер запроса – целое 4 байта </summary>
		RequestNumber = 0xE0,
		/// <summary> Текст запроса/ответа – запись </summary>
		/// <remarks>длина строки – 1 байт
		/// строка – последовательность символов; кол-во символов определяется полем длина строки
		/// </remarks>
		Text = 0xE1
	}
}