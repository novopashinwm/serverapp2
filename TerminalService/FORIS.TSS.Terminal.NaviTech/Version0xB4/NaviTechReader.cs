﻿using System;
using System.Text;

namespace FORIS.TSS.Terminal.NaviTech.Version0xB4
{
	class NaviTechReader
	{
		private readonly byte[] _data;
		private readonly int _startIndex;
		private readonly int _endIndex;
		private int _index;

		public NaviTechReader(byte[] data, int startIndex, int endIndex)
		{
			_data = data;
			_index = startIndex;
			_startIndex = startIndex;
			_endIndex = endIndex;
		}

		public int Position
		{
			get { return _index; }
		}

		public void Rewind()
		{
			_index = _startIndex;
		}

		public uint ReadUint(int length)
		{
			uint result = 0;
			for (var i = 0; i != length; ++i)
			{
				result |= (uint)(_data[_index] << (i*8));
				++_index;
			}
			return result;
		}

		public byte ReadByte()
		{
			return _data[_index++];
		}

		/// <summary>
		/// Читает из массива строку, в формате [LENGTH, STRING]
		/// LENGTH - длина строки, 1 байт
		/// STRING - содержимое строки длиной LENGTH в формате ASCII
		/// </summary>
		public string ReadAsciiString8()
		{
			var length = ReadByte();
			var result = Encoding.ASCII.GetString(_data, _index, length);
			_index += length;
			return result;
		}

		public bool Any()
		{
			return _index != _endIndex;
		}

		public string ReadBytesAsHexString(int count)
		{
			const string hexTable = "0123456789ABCDEF"; 
			
			var result = new StringBuilder(count * 2);
			for (var i = 0; i != count; ++i)
			{
				++_index;
				result.Append(hexTable[_data[_index]/16]);
				result.Append(hexTable[_data[_index]%16]);
			}

			return result.ToString();
		}

		public void SkipBytes(int count)
		{
			_index += count;
		}

		public byte[] ReadArray(int count)
		{
			var result = new byte[count];
			Array.Copy(_data, _index, result, 0, count);
			_index += count;
			return result;
		}
	}
}