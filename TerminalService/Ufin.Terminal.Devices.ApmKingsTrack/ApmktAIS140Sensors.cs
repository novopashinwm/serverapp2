﻿namespace Compass.Ufin.Terminal.Devices.ApmKingsTrack
{
	public enum ApmktAIS140Sensors
	{
		Sos            = 01,
		Ignition       = 02,
		GsmSignalLevel = 03,
		DigitalInput1  = 04,
		DigitalInput2  = 05,
		DigitalInput3  = 06,
		DigitalInput4  = 07,
		DigitalOutput1 = 08,
		DigitalOutput2 = 09,
		AnalogInput1   = 10,
		AnalogInput2   = 11,
	}
}