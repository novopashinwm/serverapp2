﻿using System;
using System.Globalization;
using System.Linq;
using FORIS.TSS.Common;

namespace Compass.Ufin.Terminal.Devices.ApmKingsTrack
{
	/// <summary> Запись в формате ApmktAIS140 (10 NORMAL или 200 HISTORY) </summary>
	public sealed class ApmktAIS140RecordGeneral
	{
		/// <summary> Уникальный идентификатор vendor </summary>
		public string   Vendor          { get; private set; } // 00
		/// <summary> Прошивка </summary>
		public string   Firmware        { get; private set; } // 01
		/// <summary> Тип пакета </summary>
		public string   PacketType      { get; private set; } // 02
		/// <summary> Идентификатор ???тревоги???, ???события?? </summary>
		public int?     AlertId         { get; private set; } // 03
		/// <summary> Вид пакета (L = Live or H = History) </summary>
		public string   PacketKind      { get; private set; } // 04
		/// <summary> Идентификатор устройства </summary>
		public string   DeviceId        { get; private set; } // 05
		/// <summary> Номер машины прописанный в трекере </summary>
		public string   VehicleNo       { get; private set; } // 06
		/// <summary> Корректность позиции (1 = GPS fix OR 0 = GPS invalid) </summary>
		public bool     Valid           { get; private set; } // 07
		/// <summary> Временная метка </summary>
		public DateTime Timestamp       { get; private set; } // 08, 09
		/// <summary> Географическая широта мобильного объекта (градусы) </summary>
		public decimal? Lat             { get; private set; } // 10, 11
		/// <summary> Географическая долгота мобильного объекта (градусы) </summary>
		public decimal? Lng             { get; private set; } // 12, 13
		/// <summary> Скорость мобильного объекта (км/ч) </summary>
		public decimal? Speed           { get; private set; } // 14
		/// <summary> Курс мобильного объекта (градусы) </summary>
		public decimal? Course          { get; private set; } // 15
		/// <summary> Количество спутников </summary>
		public int?     Satellites      { get; private set; } // 16
		/// <summary> Высота над уровнем моря (м) </summary>
		public decimal? Altitude        { get; private set; } // 17
		/// <summary> Позиционная точность </summary>
		public decimal? PDOP            { get; private set; } // 18
		/// <summary> Горизонтальная точность </summary>
		public decimal? HDOP            { get; private set; } // 19
		/// <summary> Оператор мобильной связи </summary>
		public string   MobileOperator  { get; private set; } // 20
		/// <summary> Зажигание (1 = Ignition On, 0 = Ignition Off) </summary>
		public int?     Ignition        { get; private set; } // 21
		// Пропускаем 22, 23, 24
		/// <summary> Emergency Status (1 = On, 0 = Off) </summary>
		public int?     EmergencyStatus { get; private set; } // 25
		/// <summary> GSM Signal Strength (Value Ranging from 0 - 31) </summary>
		public int?     GsmSignalLevel  { get; private set; } // 26
		// Пропускаем 27 - 42
		/// <summary> Digital Input Status  DIN1: 0/1 (0 = Off; 1 = On) </summary>
		public int?     DigitalInput1   { get; private set; } // 43 bit 0 DIN1: 0/1
		/// <summary> Digital Input Status  DIN2: 0/1 (0 = Off; 1 = On) </summary>
		public int?     DigitalInput2   { get; private set; } // 43 bit 1 DIN2: 0/1
		/// <summary> Digital Input Status  DIN3: 0/1 (0 = Off; 1 = On) </summary>
		public int?     DigitalInput3   { get; private set; } // 43 bit 2 DIN3: 0/1
		/// <summary> Digital Input Status  DIN4: 0/1 (0 = Off; 1 = On) </summary>
		public int?     DigitalInput4   { get; private set; } // 43 bit 3 DIN4: 0/1
		/// <summary> Digital Output Status DOU1: 0/1 (0 = Off; 1 = On) </summary>
		public int?     DigitalOutput1  { get; private set; } // 44 bit 0 DOU1: 0/1
		/// <summary> Digital Output Status DOU2: 0/1 (0 = Off; 1 = On) </summary>
		public int?     DigitalOutput2  { get; private set; } // 44 bit 1 DOU2: 0/1
		///// <summary> Analog Input (External voltage input 0.0-30.0V) </summary>
		//public decimal? AnalogInput     { get; private set; } // 45 (float)
		public static ApmktAIS140RecordGeneral Parse(ApmktAIS140Record record)
		{
			var result = default(ApmktAIS140RecordGeneral);
			var length = record?.RecVals?.Length ?? 0;
			if (20 < length)
			{
				var parsedInt = default(int);
				var parsedDec = default(decimal);
				var recordVal = default(string);
				///////////////////////////////////
				// 00, 01, 02
				result = new ApmktAIS140RecordGeneral
				{
					Vendor     = record.RecVals[00],
					Firmware   = record.RecVals[01],
					PacketType = record.RecVals[02],
				};
				///////////////////////////////////
				// 03
				if (int.TryParse(record.RecVals[03], out parsedInt))
					result.AlertId = parsedInt;
				///////////////////////////////////
				// 04, 05, 06
				result.PacketKind = record.RecVals[04];
				result.DeviceId   = record.RecVals[05];
				result.VehicleNo  = record.RecVals[06];
				///////////////////////////////////
				// 07
				result.Valid = false;
				if (int.TryParse(record.RecVals[07], out parsedInt))
					result.Valid = 1 == parsedInt;
				///////////////////////////////////
				// 08, 09
				var date   = DateTime.ParseExact(record.RecVals[08], "ddMMyyyy",
					CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal);
				var time   = DateTime.ParseExact(record.RecVals[09], "HHmmss",
					CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal);
				result.Timestamp = date.Add(time - time.Date);
				///////////////////////////////////
				// 10, 11, 12, 13, 14, 15, 16
				if (decimal.TryParse(record.RecVals[10], NumberStyles.Number, NumberHelper.FormatInfo, out parsedDec))
					result.Lat = (record.RecVals[11] == "N" ? +1 : -1) * parsedDec;
				if (decimal.TryParse(record.RecVals[12], NumberStyles.Number, NumberHelper.FormatInfo, out parsedDec))
					result.Lng = (record.RecVals[13] == "E" ? +1 : -1) * parsedDec;
				if (decimal.TryParse(record.RecVals[14], NumberStyles.Number, NumberHelper.FormatInfo, out parsedDec))
					result.Speed = parsedDec;
				if (decimal.TryParse(record.RecVals[15], NumberStyles.Number, NumberHelper.FormatInfo, out parsedDec))
					result.Course = parsedDec;
				if (int.TryParse(record.RecVals[16], NumberStyles.Number, NumberHelper.FormatInfo, out parsedInt))
					result.Satellites = parsedInt;
				///////////////////////////////////
				// 17, 18, 19
				if (decimal.TryParse(record.RecVals[17], NumberStyles.Number, NumberHelper.FormatInfo, out parsedDec))
					result.Altitude = parsedDec;
				if (decimal.TryParse(record.RecVals[18], NumberStyles.Number, NumberHelper.FormatInfo, out parsedDec))
					result.PDOP = parsedDec;
				if (decimal.TryParse(record.RecVals[19], NumberStyles.Number, NumberHelper.FormatInfo, out parsedDec))
					result.HDOP = parsedDec;
				///////////////////////////////////
				// 20
				result.MobileOperator = record.RecVals[20];
				///////////////////////////////////
				// 21
				if (int.TryParse(record.RecVals[21], NumberStyles.Number, NumberHelper.FormatInfo, out parsedInt))
					result.Ignition = parsedInt;
				///////////////////////////////////
				// 22, - 24
				///////////////////////////////////
				// 25
				if (int.TryParse(record.RecVals[25], NumberStyles.Number, NumberHelper.FormatInfo, out parsedInt))
					result.EmergencyStatus = parsedInt;
				///////////////////////////////////
				// 26
				if (int.TryParse(record.RecVals[26], NumberStyles.Number, NumberHelper.FormatInfo, out parsedInt))
					result.GsmSignalLevel = parsedInt;
				///////////////////////////////////
				// 27 - 42
				///////////////////////////////////
				// 43
				recordVal = record.RecVals[43];
				if (4 == recordVal.Length && recordVal.All(c => ('1' == c || '0' == c)))
				{
					// Переводим строку вида 1100 в целое число соответствующее этому двоичному коду
					parsedInt = recordVal.Reverse().Select((c, i) => '1' == c ? 1 << i : 0).Sum();
					result.DigitalInput1 = (parsedInt >> 3) & 0x000001;
					result.DigitalInput2 = (parsedInt >> 2) & 0x000001;
					result.DigitalInput3 = (parsedInt >> 1) & 0x000001;
					result.DigitalInput4 = (parsedInt >> 0) & 0x000001;
				}
				///////////////////////////////////
				// 44
				recordVal = record.RecVals[44];
				if (2 == recordVal.Length && recordVal.All(c => ('1' == c || '0' == c)))
				{
					// Переводим строку вида 10 в целое число соответствующее этому двоичному коду
					parsedInt = recordVal.Reverse().Select((c, i) => '1' == c ? 1 << i : 0).Sum();
					result.DigitalOutput1 = (parsedInt >> 1) & 0x000001;
					result.DigitalOutput2 = (parsedInt >> 0) & 0x000001;
				}
			}
			return result;
		}
	}
}