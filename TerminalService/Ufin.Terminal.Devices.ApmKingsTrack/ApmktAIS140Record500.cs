﻿using System;
using System.Globalization;

namespace Compass.Ufin.Terminal.Devices.ApmKingsTrack
{
	/// <summary> Запись в формате ApmktAIS140 (500 DIAGNOSIS DATA) </summary>
	public sealed class ApmktAIS140Record500
	{
		/// <summary> Идентификатор устройства </summary>
		public string   DeviceId  { get; private set; } // 00
		/// <summary> Временная метка </summary>
		public DateTime Timestamp { get; private set; } // 02 -03
		/// <summary> Приватный конструктор, блокировка создания извне </summary>
		private ApmktAIS140Record500() { }
		public static ApmktAIS140Record500 Parse(ApmktAIS140Record record)
		{
			var result = default(ApmktAIS140Record500);
			var length = record?.RecVals?.Length ?? 0;
			if (1 < length)
			{
				///////////////////////////////////
				// 00
				result = new ApmktAIS140Record500
				{
					DeviceId = record.RecVals[00],
				};
				///////////////////////////////////
				// 01
				///////////////////////////////////
				// 02 - 03
				var date = DateTime.ParseExact(record.RecVals[02], "ddMMyyyy",
					CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal);
				var time = DateTime.ParseExact(record.RecVals[03], "HHmmss",
					CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal);
				result.Timestamp = date.Add(time - time.Date);
			}
			return result;
		}
	}
}