﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;

namespace Compass.Ufin.Terminal.Devices.ApmKingsTrack
{
	public partial class ApmktAIS140Device : Device
	{
		public const string ProtocolName = "APM KingsTrack-AIS140";
		internal static class DeviceNames
		{
			internal const string Apmkt1819001A = "APMKT 1819001A (AIS-140)";
		}
		public ApmktAIS140Device() : base(typeof(ApmktAIS140Sensors), new[]
		{
			DeviceNames.Apmkt1819001A,
		})
		{
		}
		internal static readonly byte[] Prefix = Encoding.ASCII.GetBytes("$,");
		internal static readonly byte[] Suffix = Encoding.ASCII.GetBytes(",*\r\n");

		public override bool SupportData(byte[] data)
		{
			if (data == null)
				return false;
			// Начало пакета
			if (!data.Take(Prefix.Length).SequenceEqual(Prefix))
				return false;
			// Конец пакета(-ов)
			if (!data.EndsWith(Suffix))
				return false;
			// Проверяем подходит ли строка
			return ApmktAIS140Record.IsMatch(Encoding.ASCII.GetString(data));
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			var results = new List<object>();
			var records = ApmktAIS140Record.GetRecords(data, out bufferRest);
			foreach (var record in records)
			{
				// TODO: Добавить известные пакеты 10, 200, 500, 101...
				switch (record.RecType)
				{
					case "10":  // General NORMAL
					case "200": // General HISTORY
						results.AddRange(ParseRecordGeneral(record));
						break;
					case "101": // HEALTH
						results.AddRange(ParseRecord101(record));
						break;
					case "500": // DIAGNOSIS
						results.AddRange(ParseRecord500(record));
						break;
					default:
						$"{GetType()}.{nameof(OnData)} error: Record type '{record.RecType}' not supported yet\r\nData:\r\n{record}".TraceWarning();
						break;
				}
			}
			return results;
		}
		public IEnumerable<object> ParseRecordGeneral(ApmktAIS140Record record)
		{
			var results = new List<object>();
			var pars = ApmktAIS140RecordGeneral.Parse(record);
			if (null != pars)
			{
				var mu = UnitReceived(ProtocolName);
				mu.DeviceID     = pars.DeviceId;
				mu.cmdType      = CmdType.Trace;
				mu.Time         = TimeHelper.GetSecondsFromBase(pars.Timestamp);
				mu.Latitude     = MobilUnit.InvalidLatitude;
				mu.Longitude    = MobilUnit.InvalidLongitude;
				mu.CorrectGPS   = pars.Valid;
				if (pars.Valid)
				{
					mu.Latitude  = (double)pars.Lat;
					mu.Longitude = (double)pars.Lng;
					mu.Speed     = (int?)pars.Speed;
					mu.Course    = (int?)pars.Course;
					if (pars.Altitude.HasValue)
						mu.Height = (int)(pars.Altitude.Value * 100.0M);
					if (pars.Satellites.HasValue)
						mu.Satellites = pars.Satellites.Value;
				}
				mu.SetSensorValue((int)ApmktAIS140Sensors.Sos,            pars.EmergencyStatus);
				mu.SetSensorValue((int)ApmktAIS140Sensors.Ignition,       pars.Ignition);
				mu.SetSensorValue((int)ApmktAIS140Sensors.GsmSignalLevel, pars.GsmSignalLevel);
				mu.SetSensorValue((int)ApmktAIS140Sensors.DigitalInput1,  pars.DigitalInput1);
				mu.SetSensorValue((int)ApmktAIS140Sensors.DigitalInput2,  pars.DigitalInput2);
				mu.SetSensorValue((int)ApmktAIS140Sensors.DigitalInput3,  pars.DigitalInput3);
				mu.SetSensorValue((int)ApmktAIS140Sensors.DigitalInput4,  pars.DigitalInput4);
				mu.SetSensorValue((int)ApmktAIS140Sensors.DigitalOutput1, pars.DigitalOutput1);
				mu.SetSensorValue((int)ApmktAIS140Sensors.DigitalOutput2, pars.DigitalOutput2);
				results.Add(mu);
			}
			return results;
		}
		public IEnumerable<object> ParseRecord101(ApmktAIS140Record record)
		{
			var results = new List<object>();
			var pars = ApmktAIS140Record101.Parse(record);
			if (null != pars)
			{
				var mu = UnitReceived(ProtocolName);
				mu.DeviceID     = pars.DeviceId;
				mu.cmdType      = CmdType.Trace;
				mu.Time         = TimeHelper.GetSecondsFromBase(pars.Timestamp);
				mu.Latitude     = MobilUnit.InvalidLatitude;
				mu.Longitude    = MobilUnit.InvalidLongitude;
				mu.CorrectGPS   = false;
				mu.SetSensorValue((int)ApmktAIS140Sensors.DigitalInput1, pars.DigitalInput1);
				mu.SetSensorValue((int)ApmktAIS140Sensors.DigitalInput2, pars.DigitalInput2);
				mu.SetSensorValue((int)ApmktAIS140Sensors.DigitalInput3, pars.DigitalInput3);
				mu.SetSensorValue((int)ApmktAIS140Sensors.DigitalInput4, pars.DigitalInput4);
				if (pars.AnalogInput1.HasValue)
					mu.SetSensorValue((int)ApmktAIS140Sensors.AnalogInput1, (long)(pars.AnalogInput1.Value * 100M));
				if (pars.AnalogInput2.HasValue)
					mu.SetSensorValue((int)ApmktAIS140Sensors.AnalogInput2, (long)(pars.AnalogInput2.Value * 100M));
				results.Add(mu);
			}
			return results;

		}
		public IEnumerable<object> ParseRecord500(ApmktAIS140Record record)
		{
			var results = new List<object>();
			var pars = ApmktAIS140Record500.Parse(record);
			if (null != pars)
			{
				var mu = UnitReceived(ProtocolName);
				mu.DeviceID     = pars.DeviceId;
				mu.cmdType      = CmdType.Trace;
				mu.Time         = TimeHelper.GetSecondsFromBase(pars.Timestamp);
				mu.Latitude     = MobilUnit.InvalidLatitude;
				mu.Longitude    = MobilUnit.InvalidLongitude;
				mu.CorrectGPS   = false;
				results.Add(mu);
			}
			return results;
		}
	}
}