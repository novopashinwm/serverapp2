﻿using System;
using System.Globalization;
using System.Linq;
using FORIS.TSS.Common;

namespace Compass.Ufin.Terminal.Devices.ApmKingsTrack
{
	/// <summary> Запись в формате ApmktAIS140 (101 HEALTH DATA) </summary>
	public sealed class ApmktAIS140Record101
	{
		/// <summary> Уникальный идентификатор vendor </summary>
		public string   Vendor        { get; private set; } // 00
		/// <summary> Прошивка </summary>
		public string   Firmware      { get; private set; } // 01
		/// <summary> Идентификатор устройства </summary>
		public string   DeviceId      { get; private set; } // 02
		/// <summary> Временная метка </summary>
		public DateTime Timestamp     { get; private set; } // 03, 04
		/// <summary> Indicates internal battery charge percentage </summary>
		public int?     BatteryLevel  { get; private set; } // 05
		/// <summary> Digital Input Status  DIN1: 0/1 (0 = Off; 1 = On) </summary>
		public int?     DigitalInput1 { get; private set; } // 10 bit 0 DIN1: 0/1
		/// <summary> Digital Input Status  DIN2: 0/1 (0 = Off; 1 = On) </summary>
		public int?     DigitalInput2 { get; private set; } // 10 bit 1 DIN2: 0/1
		/// <summary> Digital Input Status  DIN3: 0/1 (0 = Off; 1 = On) </summary>
		public int?     DigitalInput3 { get; private set; } // 10 bit 2 DIN3: 0/1
		/// <summary> Digital Input Status  DIN4: 0/1 (0 = Off; 1 = On) </summary>
		public int?     DigitalInput4 { get; private set; } // 10- bit 3 DIN4: 0/1
		/// <summary> Analog Input 1 </summary>
		public decimal? AnalogInput1  { get; private set; } // 11 (float)
		/// <summary> Analog Input 2 </summary>
		public decimal? AnalogInput2  { get; private set; } // 12 (float)

		/// <summary> Приватный конструктор, блокировка создания извне </summary>
		private ApmktAIS140Record101() { }
		public static ApmktAIS140Record101 Parse(ApmktAIS140Record record)
		{
			var result = default(ApmktAIS140Record101);
			var length = record?.RecVals?.Length ?? 0;
			if (4 < length)
			{
				///////////////////////////////////
				// 00, 01, 02
				result = new ApmktAIS140Record101
				{
					Vendor   = record.RecVals[00],
					Firmware = record.RecVals[01],
					DeviceId = record.RecVals[02],
				};
				///////////////////////////////////
				// 03, 04
				var date = DateTime.ParseExact(record.RecVals[03], "ddMMyyyy",
					CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal);
				var time = DateTime.ParseExact(record.RecVals[04], "HHmmss",
					CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal);
				result.Timestamp = date.Add(time - time.Date);
				///////////////////////////////////
				var parsedInt = default(int);
				var parsedDec = default(decimal);
				var recordVal = default(string);
				///////////////////////////////////
				// 05
				if (int.TryParse(record.RecVals[05], NumberStyles.Number, NumberHelper.FormatInfo, out parsedInt))
					result.BatteryLevel = parsedInt;
				///////////////////////////////////
				// 06, 07, 08, 09 пропускаем
				///////////////////////////////////
				// 10
				recordVal = record.RecVals[10];
				if (4 == recordVal.Length && recordVal.All(c => ('1' == c || '0' == c)))
				{
					// Переводим строку вида 1100 в целое число соответствующее этому двоичному коду
					parsedInt = recordVal.Reverse().Select((c, i) => '1' == c ? 1 << i : 0).Sum();
					result.DigitalInput1 = (parsedInt >> 3) & 0x000001;
					result.DigitalInput2 = (parsedInt >> 2) & 0x000001;
					result.DigitalInput3 = (parsedInt >> 1) & 0x000001;
					result.DigitalInput4 = (parsedInt >> 0) & 0x000001;
				}
				///////////////////////////////////
				// 11
				if (decimal.TryParse(record.RecVals[11], NumberStyles.Number, NumberHelper.FormatInfo, out parsedDec))
					result.AnalogInput1 = parsedDec;
				///////////////////////////////////
				// 12
				if (decimal.TryParse(record.RecVals[12], NumberStyles.Number, NumberHelper.FormatInfo, out parsedDec))
					result.AnalogInput2 = parsedDec;
			}
			return result;
		}
	}
}