﻿using System;

namespace FORIS.TSS.Terminal.Symbian
{
	public class ServiceStatusResult
	{
		public ServiceStatus service_status;
		public DateTime?     expiration_date;
		public DateTime?     start_date;
		public int           vehicle_id;
	}
}