﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Common.Http;
using FORIS.TSS.Helpers;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Symbian
{
	/// <summary> Реализация текстового JSON RESTful протокола для взаимодействия с Ника.Трекер </summary>
	public class NikaTrackerV2 : Device, IHttpDevice
	{
		public NikaTrackerV2()
		{
			_storeAppLogsInDatabase =
				"true".Equals(ConfigurationManager.AppSettings["NikaTrackerV2.StoreAppLogsInDatabase"],
					StringComparison.OrdinalIgnoreCase);            
			_traceLogs =
				"true".Equals(ConfigurationManager.AppSettings["NikaTrackerV2.TraceLogs"],
					StringComparison.OrdinalIgnoreCase);
		}

		[Obsolete("Убрать, когда все клиенты перейдут на новую версию")]
		const string NikaTrackerPrefix = "NikaTrackerV2";
		const string SoftTrackerPrefix = "SoftTracker";
		const string CommonDevicePrefix = "Device";

		private readonly bool _storeAppLogsInDatabase;
		private readonly bool _traceLogs;
		private readonly int _maxIncomingDataAgeSeconds = (int) TimeSpan.FromDays(7).TotalSeconds;

		public bool CanProcess(IHttpRequest request)
		{
			
			return
				request.RequestUrl.Segments.Any(
					s => string.Equals(s, SoftTrackerPrefix  + '/', StringComparison.OrdinalIgnoreCase) 
					  || string.Equals(s, CommonDevicePrefix + '/', StringComparison.OrdinalIgnoreCase)
					  || string.Equals(s, NikaTrackerPrefix  + '/', StringComparison.OrdinalIgnoreCase)
					  );
		}

		public IList<object> OnData(IHttpRequest request)
		{
			var urlSegments = Array.ConvertAll(request.RequestUrl.Segments, s => s.TrimEnd('/'));



			bool simIdentification;
			int? prefixIndex;

			if (urlSegments.Any(segment => segment == SoftTrackerPrefix))
			{
				simIdentification = true;
				prefixIndex = urlSegments.IndexOf(SoftTrackerPrefix, StringComparer.OrdinalIgnoreCase.Equals);
			}
			else if (urlSegments.Any(segment => segment == NikaTrackerPrefix))
			{
				simIdentification = true;
				prefixIndex = urlSegments.IndexOf(NikaTrackerPrefix, StringComparer.OrdinalIgnoreCase.Equals);
			}
			else
			{
				simIdentification = false;
				prefixIndex = urlSegments.IndexOf(CommonDevicePrefix, StringComparer.OrdinalIgnoreCase.Equals);
			}

			if (prefixIndex == null || prefixIndex.Value == urlSegments.Length - 1)
				return BadRequest("Empty request");

			var segments = urlSegments.TakeAfter(prefixIndex.Value).ToArray();
			IList<object> result;
			if (simIdentification)
			{
				switch (segments[0])
				{
					case "sim_id":
						return ReturnJson(GetSimId(request), DateTime.UtcNow.AddDays(1));
					case "sim":
						result = ProcessRequest(
							request.Method,
							segments.TakeAfter(0).ToArray(),
							request.Text,
							info => info.SimId = segments[1],
							true);
						break;
					default:
						return NotFound(segments[1]);
				}
			}
			else
			{
				result = ProcessRequest(
					request.Method,
					segments,
					request.Text,
					info => info.DeviceID = segments[0],
					false);
			}

			var mobilUnits = result.OfType<MobilUnit.Unit.MobilUnit>().ToArray();
			foreach (var mobilUnit in mobilUnits)
				mobilUnit.Properties.Add(DeviceProperty.Protocol, request.RequestUrl.Segments.Take(prefixIndex.Value).Join(string.Empty));

			return result;
		}

		private IList<object> ProcessRequest(
			HttpMethod       method,
			string[]         urlSegments,
			string           text,
			Action<UnitInfo> fillInfoAction,
			bool             simIdentification)
		{
			if (urlSegments.Length != 2)
				return BadRequest("2 segments after base are expected");

			var secondLevelSegment = urlSegments[1];

			var ui = UnitInfo.Factory.Create(UnitInfoPurpose.Undefined);
			fillInfoAction(ui);

			switch (secondLevelSegment)
			{
				case "service_status":
				{
					switch (method)
					{
						case HttpMethod.Get:
							return ReturnJson(GetServiceStatus(fillInfoAction));
						default:
							throw new NotSupportedException("Http method is not supported: " + method);
					}
				}
				case "data":
				{
					switch (method)
					{
						case HttpMethod.Post:
						{
							if (!Manager.FillUnitInfo(ui))
							{
								if (!string.IsNullOrEmpty(ui.SimId))
								{
									const string message = "Service is not provided for specified SIM - no associated object";
									Trace.TraceWarning("{0} sim_id = {1}: {2}", GetType().Name, ui.SimId, message);
									return Error(510, message);
								}

								if (!string.IsNullOrEmpty(ui.DeviceID))
								{
									const string message = "Service is not provided for specified DeviceId - no associated object";
									Trace.TraceWarning("{0} deviceId = {1}: {2}", GetType().Name, ui.DeviceID, message);
									return Error(510, message);
								}
							}

							if (simIdentification && ui.DeviceType != ControllerType.Names.SoftTracker)
								return new List<object> { HttpResponse.Ok() };

							List<object> data;
							try
							{
								data = ParseData(text, ui);
							}
							catch (ArgumentException ex)
							{
								return Error(400, "Data is not valid: " + ex.Message);
							}
							catch(Exception ex)
							{
								return Error(400, "Unable to parse input: " + ex.Message);
							}
							data.Add(HttpResponse.Ok());
							return data;
						}
						case HttpMethod.Delete:
							{
								if (!Manager.FillUnitInfo(ui) || !ui.AllowDeleteLog)
									return new List<object> { HttpResponse.Forbidden() };
								Manager.DeleteLog(ui.Unique);
								return new List<object> { HttpResponse.Ok() };
							}
						default:
							throw new NotSupportedException("Http method is not supported: " + method);
					}
				}
				case "schedule":
					if (!Manager.FillUnitInfo(ui))
						return new List<object> { HttpResponse.NotFound() };
					return ReturnJson(Manager.GetTrackingSchedule(ui.Unique));
				default:
					return NotFound(secondLevelSegment);
			}
		}

		private List<object> ParseData(string text, UnitInfo ui)
		{
			var data       = JsonHelper.DeserializeObjectFromJson<NikaTrackerV2Data>(text);
			var mobilUnits = ParseData(data, ui);
			return mobilUnits;
		}

		private List<object> ParseData(NikaTrackerV2Data data, UnitInfo ui)
		{
			var allLogTimes = new SortedSet<int>();

			ILookup<int, NikaTrackerV2Data.Location> locations;
			if (data.Locations != null)
			{
				locations = data.Locations.ToLookup(x => TimeHelper.GetSecondsFromBase(x.Date));
				foreach (var locationLogTime in locations)
					allLogTimes.Add(locationLogTime.Key);
			}
			else locations = null;

			ILookup<int, NikaTrackerV2Data.Sensor> sensors;
			if (data.Sensors != null)
			{
				sensors = data.Sensors.ToLookup(x => TimeHelper.GetSecondsFromBase(x.Date));
				foreach (var sensorGroup in sensors)
					allLogTimes.Add(sensorGroup.Key);
			}
			else sensors = null;
			ILookup<int, NikaTrackerV2Data.Wlan> wlans;
			if (data.Wlans != null)
			{
				wlans = data.Wlans.ToLookup(x => TimeHelper.GetSecondsFromBase(x.Date));
				foreach (var wlanGroup in wlans)
					allLogTimes.Add(wlanGroup.Key);
			}
			else wlans = null;
			ILookup<int, NikaTrackerV2Data.Cell> cells;
			if (data.Cells != null)
			{
				cells = data.Cells.ToLookup(x => TimeHelper.GetSecondsFromBase(x.Date));
				foreach (var cellGroup in cells)
					allLogTimes.Add(cellGroup.Key);

			}
			else cells = null;
			ILookup<int, NikaTrackerV2Data.Version> versions;
			if (data.Versions != null)
			{
				versions = data.Versions.ToLookup(x => TimeHelper.GetSecondsFromBase(x.Date));
				foreach (var logTime in versions)
					allLogTimes.Add(logTime.Key);
			}
			else versions = null;

			var ageThreshold = TimeHelper.GetSecondsFromBase() - _maxIncomingDataAgeSeconds;
			var result = new List<object>();
			foreach (var logTime in allLogTimes)
			{
				if (logTime < ageThreshold)
					continue;

				var mu = new MobilUnit.Unit.MobilUnit(ui)
				{
					Time  = logTime, CorrectGPS = false, 
					AppId = data.AppId
				};
				result.Add(mu);

				if (locations != null)
				{
					NikaTrackerV2Data.Location location = locations[logTime].FirstOrDefault();
					if (location != null)
					{
						mu.Latitude   = location.Lat;
						mu.Longitude  = location.Lng;
						mu.Radius     = location.Accuracy;
						mu.Speed      = location.Speed;
						mu.Course     = location.Direction;
						mu.Satellites = location.Satellites ?? MobilUnit.Unit.MobilUnit.MinSatellites;
						mu.Height     = location.Altitude * 100;
						mu.CorrectGPS = true;
					}
				}
				else
				{
					mu.CorrectGPS = false;
				}

				if (sensors != null && sensors[logTime].Any())
				{
					mu.SensorValues = new Dictionary<int, long>();
					foreach (var sensor in sensors[logTime])
					{
						S60V3FPSensor sensorKey;
						if (!Enum.TryParse(sensor.Key, true, out sensorKey))
							continue;
						mu.SensorValues[(int) sensorKey] = sensor.Value;
					}
				}

				if (wlans != null && wlans[logTime].Any())
				{
					var wlanRecords = new List<WlanRecord>();
					foreach (var wlan in wlans[logTime])
					{
						wlanRecords.Add(
							new WlanRecord
							{
								Number         = wlanRecords.Count,
								LogTime        = logTime,
								WlanMacAddress = wlan.mac,
								WlanSSID       = wlan.ssid,
								ChannelNumber  = wlan.channel != null ? (byte)wlan.channel.Value : (byte?)null,
								SignalStrength = wlan.signal_strength ?? 0
							});
					}
					mu.Wlans = wlanRecords.ToArray();
				}

				if (cells != null && cells[logTime].Any())
				{
					var cellRecords = new List<CellNetworkRecord>();
					foreach (var cell in cells[logTime])
					{
						if (cell.mcc == null)
							throw new ArgumentException("Cell.mcc could not be empty");
						if (4 < cell.mcc.Length)
							throw new ArgumentException("Cell.mcc is too long, max length is 4");

						if (cell.mnc == null)
							throw new ArgumentException("Cell mnc could not be empty");
						if (8 < cell.mnc.Length)
							throw new ArgumentException("Cell.mnc is too long, max length is 8");

						cellRecords.Add(
							new CellNetworkRecord
								{
									Number = cellRecords.Count,
									LogTime = logTime,
									CountryCode = cell.mcc,
									NetworkCode = cell.mnc,
									LAC = cell.lac,
									CellID = cell.cell_id,
									SignalStrength = cell.signal_strength
								});
					}
					mu.CellNetworks = cellRecords.ToArray();
				}

				if (versions != null)
				{
					NikaTrackerV2Data.Version version = versions[logTime].FirstOrDefault();
					if (version != null)
						mu.Firmware = version.Code;
				}
			}

			if (data.App_logs != null)
			{
				foreach (var entry in data.App_logs)
				{
					if (_storeAppLogsInDatabase)
					{
						result.Add(new NotifyEventArgs(
							entry.Date,
							TerminalEventMessage.AppLog, 
							null, 
							Severity.Lvl7,
							NotifyCategory.Controller, 
							entry.Text, 
							null, 
							ui));
					}
					else if (_traceLogs)
					{
						Trace.TraceInformation(
							"{0}.AppLog: {1}: {2}: {3}",
							this, ui.SimId, entry.Date, entry.Text);
					}
				}
			}

			return result;
		}

		private ServiceStatusResult GetServiceStatus(Action<UnitInfo> fillInfoAction)
		{
			var ui = UnitInfo.Factory.Create(UnitInfoPurpose.Undefined);
			fillInfoAction(ui);

			if (!string.IsNullOrEmpty(ui.SimId))
			{
				var blockingDate = Manager.GetBlockingDate(ui.SimId);
				if (blockingDate != null)
					return new ServiceStatusResult
					{
						service_status = ServiceStatus.Blocked,
						start_date = blockingDate.Value
					};
			}

			if (!Manager.FillUnitInfo(ui) || ui.AsidId == null)
				return null;

			var result = new ServiceStatusResult
			{
				start_date     = DateTime.UtcNow,
				vehicle_id     = ui.Unique,
				service_status = ServiceStatus.Active
			};

			return result;
		}

		private static List<object> NotFound(string firstLevelSegment)
		{
			return new List<object>
			{
				new HttpResponse
				{
					StatusCode        = 404,
					StatusDescription = "Not Found",
					Text              = "Unable to dispatch " + firstLevelSegment
				}
			};
		}

		private static List<object> Error(int code, string description)
		{
			return new List<object>
			{
				new HttpResponse
				{
					StatusCode        = code,
					StatusDescription = description
				}
			};
		}

		private List<object> ReturnJson(object jsonObject, DateTime? expires = null)
		{
			return new List<object>
			{
				new HttpResponse
				{
					StatusCode        = 200,
					StatusDescription = "OK",
					Expires           = expires,
					Text              = JsonHelper.SerializeObjectToJson(jsonObject),
					Type              = DataType.Json
				}
			};
		}

		private IList<object> BadRequest(string text)
		{
			return new List<object>
			{
				new HttpResponse
				{
					StatusCode        = 400,
					StatusDescription = "Bad Request",
					Text              = text
				}
			};
		}

		private Dictionary<string, string> GetSimId(IHttpRequest request)
		{
			var result = new Dictionary<string, string>();
			if (string.IsNullOrWhiteSpace(request.Requestor))
				return result;
			result.Add("id", Manager.GetOrCreateSimIdByAsid(request.Requestor));
			return result;
		}

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			throw new NotSupportedException();
		}

		public override bool SupportData(byte[] data)
		{
			return false;
		}
	}
}