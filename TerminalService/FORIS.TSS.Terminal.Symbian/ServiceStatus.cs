﻿namespace FORIS.TSS.Terminal.Symbian
{
	public enum ServiceStatus
	{
		NoService,
		WrongService,
		Blocked,
		Active,
		Trial,
		TrialPeriodExpired
	}
}