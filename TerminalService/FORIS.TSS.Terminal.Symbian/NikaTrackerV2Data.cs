﻿using System;

namespace FORIS.TSS.Terminal.Symbian
{
	public class NikaTrackerV2Data
	{
		public string     AppId;
		public Location[] Locations;
		public Sensor[]   Sensors;
		public Wlan[]     Wlans;
		public Cell[]     Cells;
		public AppLog[]   App_logs;
		public Version[]  Versions;

		public class Location
		{
			public DateTime Date;
			public double   Lat;
			public double   Lng;
			public int?     Accuracy;
			public int?     Speed;
			public int?     Direction;
			public int?     Satellites;
			/// <summary> Высота относительно уровня моря, м </summary>
			public int? Altitude;
		}

		public class Sensor
		{
			public DateTime Date;
			public string   Key;
			public int      Value;
		}

		public class Wlan
		{
			public DateTime Date;
			public string   mac;
			public int?     signal_strength;
			public string   ssid;
			public int?     channel;
		}

		public class Cell
		{
			public DateTime Date;
			public string   mcc;
			public string   mnc;
			public int      cell_id;
			public int      lac;
			public int      signal_strength;

		}

		public class AppLog
		{
			public DateTime Date;
			public string   Text;
		}

		public class Version
		{
			public DateTime Date;
			public string   Code;
		}
	}
}