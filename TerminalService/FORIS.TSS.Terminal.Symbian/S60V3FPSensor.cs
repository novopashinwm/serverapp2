﻿namespace FORIS.TSS.Terminal.Symbian
{
	public enum S60V3FPSensor
	{
		LastPublishingDurationMilliseconds = 0,
		ErrorSource         =  1,
		ErrorCode           =  2,
		TimeToFirstGPSFix   =  3,
		BatteryLevel        =  5,
		SatellitesInView    =  6,
		GpsActivePercentage =  7,
		GpsEnabled          = 11,
		IsPlugged           = 12,
		RadioScannerEnabled = 13,
		DataSenderEnabled   = 14,
		/// <summary>Отслеживание местоположения (вкл/выкл)</summary>
		PositionTracking    = 32,
		LocationService     = 39,
		AnalogInput1        = 40,
		AnalogInput2        = 41,
		AnalogInput3        = 42,
		AnalogInput4        = 43,
		AnalogInput5        = 44,
		AnalogInput6        = 45,
		AnalogInput7        = 46,
		AnalogInput8        = 47,
		AnalogInput9        = 48,
		AnalogInput10       = 49,
		/// <summary> Режим "Невидимка" </summary>
		InvisibleMode       = 50
	}
}