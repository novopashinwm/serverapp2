// Static Model

namespace FORIS.TSS.Terminal.GSM
{/*
	public enum Code
	{
		ok,
		can_not_access_server,
		can_not_create_server,
		server_configuration_file_missing,
		wrong_username_or_password,
		database_not_existing,
		can_not_create_database,
		server_cannot_connect_to_database,
		terminal_status_can_not_be_logged_in_database,
		terminal_position_can_not_be_logged_in_database,
		terminals_maximum_exceeded,
		can_not_fetch_group_hierarchy_from_database,
		can_not_fetch_monitoree_history_from_database,
		can_not_fetch_device_capabilities_from_database,
		can_not_fetch_device_acceptors_from_database,
		terminal_manager_not_existing,
		terminal_manager_can_not_connect_to_monitoree,
		monitoree_not_found_in_database,
		unknown_terminal_type_for_monitoree,
		monitoree_last_position_not_available,
		monitoree_last_status_not_available,
		terminal_device_driver_not_available,
		operator_info_not_found_in_database,
		operator_has_no_rights_on_monitoree,
		operator_already_connected,
		sms_gateway_not_existing,
		sms_link_not_set,
		sms_cannot_open_sms_link,
		sms_gateway_cannot_send_sms,
		sms_reply_time_out,
		live_driver_not_existing,
		log_driver_not_existing,
		log_in_progress,
		tcp_link_not_set,
		unknown_message,
		dcom_error
	}

	public enum DriverType
	{
		unknown_driver_type = 0,
		pos_driver_type = 1,
		log_driver_type = 2,
		sms_driver_type = 3,
		styx_driver_type = 4
	}
*/
	public enum StatusCode 
	{
		unknown					= 0,

		journey_start			= 1, // From logging capable devices
		journey_stop			= 2, // From logging capable devices

		driver_busy				= 3, // Driver notifies server that it is busy
		driver_free				= 4, // Driver notifies server that it is free for new tasks
		driver_not_available	= 5, // Driver notifies server that it is not available (pause, car defect etc.)

		driver_accepted		= 6, // Driver accepted task given by operator	
		driver_rejected		= 7, // Driver rejected task given by operator	
		driver_not_responded	= 8, // Driver not responded to task given by operator

	}
}