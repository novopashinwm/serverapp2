﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Communication;
using FORIS.TSS.Terminal.Messaging;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using Link = FORIS.TSS.Terminal.GSM.TAPIModemImpl;
using PositionData = FORIS.TSS.MobilUnit.Unit.MobilUnit;

namespace FORIS.TSS.Terminal.GSM
{
	/// <summary>
	/// terminal to communicate with controller via GSM data link
	/// support for CmdType.Trace, CmdType.GetLog
	/// mostly from MIREO code
	/// Info: need optimization
	/// </summary>
	public class GSMTerminal: _Terminal
	{
		/// <summary>
		/// main flag about using terminal. and lock for it.
		/// </summary>
		static bool busy;
		static object busy_cs = new object();

		// device commands
		const string MJ2016_INIT = "\u0003\u0003\r\n\r\n";
		const string MJ2016_LIVE_START_MSG = "psta65535\r\n";
		const string MJ2016_LOG_START_MSG = "skt {0:x}\r\nlgp ffff 1 1\r\n";
		const string MJ2016_STOP_MSG = "\u0003\r\nexit\r\n";

		// named param list
		const string fromTimePar = "FromTime";
		const string toTimePar = "ToTime";
		const string switchPar = "Switch";

		/// <summary>
		/// cached info
		/// </summary>
		IUnitInfo unitInfo;
		/// <summary>
		/// time when link opened
		/// </summary>
		DateTime dtLinkBegin = new DateTime(0);
		/// <summary>
		/// keep data from device
		/// </summary>
		ArrayList alData = new ArrayList();
		/// <summary>
		/// mean CmdType.GetLog
		/// </summary>
		bool fetch_log;
		/// <summary>
		/// from time of log to get
		/// </summary>
		int from_time;
		/// <summary>
		/// to time of log to get
		/// </summary>
		int to_time;
		/// <summary>
		/// last history pos
		/// </summary>
		IMobilUnit imuLastHistory = new PositionData();
		/// <summary>
		/// modem name
		/// </summary>
		string modem = "TC35";
		int replyTO = 60000;
		int ioTO = 30000;

		static IOperatorInfo operatorInfo;
		static IOperatorInfo ioiInitiator;

		/// <summary>
		/// stop event
		/// </summary>
		static AutoResetEvent evt = new AutoResetEvent(false);
		/// <summary>
		/// stop done event
		/// </summary>
		static AutoResetEvent doneevt = new AutoResetEvent(false);
		/// <summary>
		/// stop flag
		/// </summary>
		static bool should_stop;

		int retry_count = 5;
		int retry_pause = 2000;
		DateTime fire_tm;
		float speed = 1;

		/// <summary>
		/// modem for outcoming calls
		/// </summary>
		Link src_;
		/// <summary>
		/// modem for incoming calls
		/// </summary>
		Link lnkWaiter;

		TraceSwitch tsTerm = new TraceSwitch("Terminal", "GSM");
		public GSMTerminal(ITerminalManager tmmgr)
			: this("GSMTerminal", tmmgr)
		{
		}
		public GSMTerminal(string name, ITerminalManager tmmgr)
			: base(name, tmmgr)
		{
			Trace.TraceInformation(this.GetTypeName() + "({0}): Terminal created", Name);
		}
		protected override void Dispose(bool disposing)
		{
			if(disposing)
			{
				Stop(null);
				if(null != Link)
				{
					while(!Link.Closed) Thread.Sleep(100);	// poor decision
					Link.Dispose();
				}
				if(lnkWaiter != null)
					lnkWaiter.Dispose();

				$"Terminal {Name} disposed"
					.CallTraceInformation();
			}
			base.Dispose(disposing);
		}
		/// <summary>
		/// fetching log
		/// started from SendCommand in separated thread
		/// </summary>
		/// <param name="subj">command</param>
		public void FetchLog(object subj)
		{
			$"Prepare and start"
				.CallTraceInformation();

			IStdCommand cmd = subj as IStdCommand;
			try
			{
				lock(this)
				{
					fetch_log = true;
					from_time = (time_t)(DateTime)cmd.Params[fromTimePar];
					to_time = (time_t)(DateTime)cmd.Params[toTimePar];
					imuLastHistory = new PositionData();

					// ensure right bound 
					DateTime now = DateTime.UtcNow.AddMinutes(-5), rbound = (DateTime)cmd.Params[toTimePar];
					cmd.Params[toTimePar] = rbound < now? now: rbound;
				}
				_Start(subj);
			}
			catch(Exception ex)
			{
				OnNotify(new NotifyEventArgs(TerminalEventMessage.TM_EXCEPTION, cmd.Sender, Severity.Lvl0, 
					NotifyCategory.Terminal, ex.Message, ex, LinkStatus.Error, cmd.Target));
			}
			finally
			{
				Busy = fetch_log = false;
			}
		}
		/// <summary>
		/// make init after getting connected
		/// </summary>
		protected void OnStart()
		{
			$"{this.GetTypeName()}({Name}) Sending start sequence to unit"
				.CallTraceInformation();
			lock(this)
			{
				alData.Clear();
				Thread.Sleep(1000);
				Link.Write(MJ2016_INIT);
				Thread.Sleep(500);
				if(fetch_log)
				{
					string msg = String.Format(MJ2016_LOG_START_MSG, from_time);
					Link.Write(msg);
				}
				else Link.Write(MJ2016_LIVE_START_MSG);
			}
		}
		/// <summary>
		/// link opened but no data
		/// </summary>
		protected void OnNoData()
		{
			OnStart();
		}
		/// <summary>
		/// stopping data transmission
		/// </summary>
		protected void OnStop()
		{
			$"{this.GetTypeName()}({Name}) Sending stop sequence to unit"
				.CallTraceInformation();
			lock(this)
				Link.Write(MJ2016_STOP_MSG);
		}
		/// <summary>
		/// full data processing
		/// </summary>
		/// <param name="data">new data fragment</param>
		/// <param name="len">data length</param>
		/// <param name="sender">phone no</param>
		protected void OnData(byte[] data, int len, string sender)
		{
			$"{this.GetTypeName(false)}({Name}) Process data from unit"
				.CallTraceInformation();

			byte[] dst = new byte[len];
			System.Array.Copy(data, dst, len);

			alData.AddRange(dst);

			PositionData[] p;
			while(MJ2016Parse(alData, out p))
				foreach(PositionData pos in p)
				{
					try
					{
						if(fetch_log && to_time < pos.Time)
						{
							$"{this.GetTypeName(false)}({Name}) Stopping cause of time int"
								.CallTraceInformation();

							Stop(null);
							return;
						}
						if(fetch_log && from_time > pos.Time) continue;
					}
					catch
					{
						throw;
					}

					if(ShouldFire())
					{
						pos.TermID = id;
						pos.Unique = pos.ID = 0;
						pos.Telephone = sender;
						pos.cmdType = !fetch_log? CmdType.Trace: CmdType.GetLog;

						$"Sending pos data to subscribers"
							.CallDebugInformation();

						OnReceive(new ReceiveEventArgs(pos));
					}
				}
			
			if(alData.Count == 0 || !fetch_log) return;

			byte[] arr = new byte[alData.Count];
			alData.CopyTo(arr);
			if(Encoding.ASCII.GetString(arr).ToUpper().IndexOf("END") != -1) Stop(null);
		}
		/// <summary>
		/// data parser. redirect to specific device object
		/// </summary>
		/// <param name="device">device</param>
		/// <param name="data">bytes to parse</param>
		/// <param name="len">significant bytes count</param>
		/// <param name="sender">sender phone</param>
		/// <remarks>existing code extend.
		/// old OnData will be eliminated after creating MJ device</remarks>
		void OnData(IDevice device, byte[] data, int len, string sender)
		{
			// device not implemented, e. g. MJ. go to old OnData
			if(device.GetType().ToString() == "FORIS.TSS.Terminal.MJ.MJ")
			{
				OnData(data, len, sender);
				return;
			}

			Trace.WriteLineIf(tsTerm.TraceVerbose, "Process data from unit", "GSM");

			try
			{
				// redirecting and wait
				IList ilRes;
				lock (device)
				{
					device.Init(CmdType.GSM, CmdType.Unspecified);
					byte[] bufferRest;
					ilRes = device.OnData(data, len, null, out bufferRest);
				}
				if(ilRes == null || ilRes.Count == 0) return;

				foreach(object o in ilRes)
				{
					if(o is IPositionMessage)
					{
						IPositionMessage msg = (IPositionMessage)o;
						IMobilUnit pos = msg.MobilUnit;
						if(pos.cmdType == CmdType.Unspecified) 
							pos.cmdType = fetch_log? CmdType.GetLog: CmdType.Trace;

						Debug.Assert(pos.cmdType == CmdType.GetLog || pos.cmdType == CmdType.Trace);

						// it's log
						if(fetch_log && pos.cmdType == CmdType.GetLog)
						{
							// log pos after time interval
							if(to_time < pos.Time)
							{
								Debug.WriteLine(string.Format("To time {0} < pos.Time {1}", (time_t)to_time, 
									(time_t)pos.Time), "GSM");
								Trace.WriteLineIf(tsTerm.TraceWarning, "Stopping cause of time interval", "GSM");
								Stop(null);
								continue;
							}
							// before interval or last pos
							if(from_time > pos.Time || pos.Time < imuLastHistory.Time) continue;
							
							if(pos.Time > imuLastHistory.Time) imuLastHistory = pos;
						}

						if(ShouldFire())
						{
							pos.TermID = id;
							// to avoid misunderstanding and ambiguity
							pos.Unique = pos.ID = 0;
							pos.Telephone = sender;

							Trace.WriteLineIf(tsTerm.TraceVerbose, "Sending pos data to subscribers", "GSM");
#if !DEBUG
							if(msg.Valid)
#endif
							OnReceive( new ReceiveEventArgs( pos ) );
						}
						continue;
					}
					if(o is NotifyEventArgs)
					{
						NotifyEventArgs e = (NotifyEventArgs)o;
						if(e.Event == TerminalEventMessage.NE_MESSAGE) Trace.WriteLineIf(tsTerm.TraceWarning, 
							string.Format("Message received from {0}\r\n\t{1}", ((object[])e.Tag)[0], e.Message), 
							"GSM");
						if(e.Event == TerminalEventMessage.NE_ALARM) Trace.WriteLineIf(tsTerm.TraceWarning, 
							string.Format("Alarm received from {0}", ((object[])e.Tag)[0]), "GSM");
						OnNotify((NotifyEventArgs)o);
					}
				}
			}
			catch
			{
				throw;
			}
		}
		/// <summary>
		/// main work function. started in separated thread
		/// </summary>
		/// <param name="subj">command</param>
		void _Start(object subj)
		{
#if DEBUG
			StreamWriter sw = new StreamWriter(@"c:\GSM.txt", true);
			sw.AutoFlush = true;
#endif
			$"Working thread"
				.CallTraceInformation();

			IStdCommand cmd = subj as IStdCommand;
			try
			{
				string target = cmd.Target.Telephone;
				if(target == null || target == "") throw new ArgumentException("Номер не указан", "Phone");

				if(LineBusy)
					if(Link.Number != target)
					{
						$"Interrupting existing communication"
							.CallTraceWarning();
						Stop(null);
						Thread.Sleep(5000);
					}
					else
						$"Using already open connection"
							.CallTraceWarning();

				IDevice dev;
				try
				{
					dev = itmManager.GetDevice(cmd.Target);
					if(dev != null) 
						dev.Init(CmdType.GSM, cmd);
				}
				catch(Exception ex)
				{
					default(string)
						.WithException(ex)
						.CallTraceError();
					// not return just reset device
					dev = null;
				}

				int num_retry = 0;
				evt.Reset();

				
				lock(this)
				{
					if(Link == null || Link.Number != target)
					{
						Link = new Link(replyTO, ioTO);
						Link.Number = target;
					}
					unitInfo = cmd.Target;
					SetStop(false);
					fire_tm = DateTime.MinValue;
				}

				$"First try to open connection"
					.CallTraceInformation();

				if(Link.Open())
					OnLinkOpened(dev);


				$"Link.Valid: {Link.Valid}"
					.CallTraceInformation();
				$"Link.Closed: {Link.Closed}"
					.CallTraceInformation();

				byte[] data = new byte[256];

				$"Starting main cycle"
					.CallTraceInformation();

				while(!WaitStop(1000))
				{
					if(Link.Error == ErrorCode.OK)
					{
						int rb = Link.Read(data);
						if(rb > 0)
						{
#if DEBUG
							sw.Write(BitConverter.ToString(data, 0, rb) + " ");
#endif
							OnData(dev, data, rb, target);
							num_retry = 0;
						}
						continue;
					}

					if(Link.Error == ErrorCode.Timeout && num_retry != retry_count && LineBusy)
					{
						++num_retry;

						$"Retry connect - {num_retry}"
							.CallTraceWarning();
						$"Link.Error: {Link.Error}"
							.CallTraceInformation();
						$"Line is Busy: {LineBusy}"
							.CallTraceInformation();

						OnNoData();
						continue;
					}

					if(Link.Error != ErrorCode.OK && LineBusy)
					{
						Link.Close();
						OnLinkClosed();
					}
					if(!LineBusy && num_retry != retry_count)
					{
						++num_retry;

						$"Retry connect - {num_retry}"
							.CallTraceWarning();
						$"Link.Error: {Link.Error}"
							.CallTraceInformation();
						$"Line is Busy: {LineBusy}"
							.CallTraceInformation();

						if(LineBusy || !Link.Open()) WaitStop(retry_pause);
						else
						{
							if(from_time < imuLastHistory.Time) from_time = imuLastHistory.Time;
							if(dev != null)
							{
								cmd.Params[fromTimePar] = (DateTime)(time_t)from_time;
								dev.Init(CmdType.GSM, cmd);
							}
							OnLinkOpened(dev);
						}
						continue;
					}

					$"Exit main cycle due to retry count exceeded"
						.CallTraceInformation();

					ioiInitiator = operatorInfo;
					throw new ApplicationException("Соединение установить не удалось");
				}

				$"Exit main cycle"
					.CallTraceInformation();
			}
			catch(Exception ex)
			{
				ioiInitiator = operatorInfo;
				default(string)
					.WithException(ex)
					.CallTraceError();

				OnNotify(new NotifyEventArgs(TerminalEventMessage.TM_EXCEPTION, cmd.Sender, Severity.Lvl0, 
					NotifyCategory.Terminal, ex.Message, ex, LinkStatus.Error, cmd.Target));
			}
			finally
			{
#if DEBUG
				sw.Close();
#endif
				$"Do finalization"
					.CallTraceInformation();

				if(Link != null && Link.Error == ErrorCode.OK && WaitStop(0))
					OnStop();
				if(Link != null)
					Link.Close();
				OnLinkClosed();

				Busy = false;
				doneevt.Set();

				if(this.dtLinkBegin > TimeHelper.DateTimeNullValue)
				{
					if((int)DateTime.Now.Subtract(this.dtLinkBegin).TotalSeconds > 0)
						OnReceiveCounting(new ReceiveCountingEventArgs(CountingMessageType.GSM,
							operatorInfo.OperatorId, this.unitInfo.Telephone, this.dtLinkBegin,
							(int)DateTime.Now.Subtract(this.dtLinkBegin).TotalSeconds, 1));

					this.dtLinkBegin = TimeHelper.DateTimeNullValue;
				}
				string sMessage = "Соединение разорвано. Инициатор - " + ioiInitiator.Name + ".";
				OnNotify(new NotifyEventArgs(TerminalEventMessage.TM_LINKSTATUS, cmd.Sender, Severity.Lvl3, 
					NotifyCategory.Terminal, sMessage, null, LinkStatus.Closed, cmd.Target, operatorInfo, 
					cmd.Type));
			}
			$"End working thread"
				.CallTraceInformation();
		}
		/// <summary>
		/// stop communication
		/// </summary>
		public void Stop(object obj)
		{
			$"Stop signal received"
				.CallTraceInformation();

			if(!Busy) return;

			IStdCommand cmd = obj as IStdCommand;
			if(cmd != null) ioiInitiator = cmd.Sender;
			else ioiInitiator = operatorInfo;
			
			doneevt.Reset();
			// set stop flag
			SetStop(true);
			// return if self stop or caller and interrupter are the same
			if(cmd == null || cmd.Sender == operatorInfo) return;

			// wait for stop
			doneevt.WaitOne();
//			if (this.dtLinkBegin > TimeHelper.DateTimeNullValue)
//			{
//				if ((int)DateTime.Now.Subtract(this.dtLinkBegin).TotalSeconds > 0)
//					OnReceiveCounting(new ReceiveCountingEventArgs(CountingMessageType.GSM,
//						operatorInfo.OperatorID, this.unitInfo.Telephone, this.dtLinkBegin,
//						(int)DateTime.Now.Subtract(this.dtLinkBegin).TotalSeconds, 1));
//
//				this.dtLinkBegin = TimeHelper.DateTimeNullValue;
//			}

			// send success notify
			string sMessage = "Соединение разорвано. Инициатор - " + ioiInitiator.Name + ".";
			OnNotify(new NotifyEventArgs(TerminalEventMessage.TM_LINKSTATUS, cmd.Sender,
				Severity.Lvl3, NotifyCategory.Terminal, sMessage, null,
				LinkStatus.Closed, cmd.Target, operatorInfo, cmd.Type));
		}
		/// <summary>
		/// call init func
		/// </summary>
		void OnLinkOpened()
		{
			this.dtLinkBegin = DateTime.Now;

			OnReceiveCounting(new ReceiveCountingEventArgs(CountingMessageType.GSM, operatorInfo.OperatorId, 
				this.unitInfo.Telephone, this.dtLinkBegin, 0, 1));

			$"Link open"
				.CallTraceInformation();

			OnStart();
			OnNotify(new NotifyEventArgs(TerminalEventMessage.TM_LINKSTATUS, operatorInfo, 
				Severity.Lvl2, NotifyCategory.Terminal, "Соединение установлено", null, 
				LinkStatus.Open, unitInfo.Clone(), operatorInfo, 
				fetch_log? CmdType.GetLog: CmdType.Trace));
		}
		void OnLinkOpened(IDevice device)
		{
			if(device == null)
			{
				OnLinkOpened();
				return;
			}

			this.dtLinkBegin = DateTime.Now;

			OnReceiveCounting(new ReceiveCountingEventArgs(CountingMessageType.GSM, operatorInfo.OperatorId, 
				this.unitInfo.Telephone, this.dtLinkBegin, 0, 1));

			$"Link open"
				.CallTraceInformation();

			device.OnLinkOpen(Link);
			OnNotify(new NotifyEventArgs(TerminalEventMessage.TM_LINKSTATUS, operatorInfo, Severity.Lvl2, 
				NotifyCategory.Terminal, "Соединение установлено", null,  LinkStatus.Open, unitInfo.Clone(), 
				operatorInfo, fetch_log? CmdType.GetLog: CmdType.Trace));
		}
		/// <summary>
		/// Wait for stop signal for specified amount of time(in milliseconds)
		/// </summary>
		/// <param name="to">timeout</param>
		/// <returns>should stop</returns>
		bool WaitStop(int to)
		{
			lock(typeof(GSMTerminal))
			{
				if(should_stop) return true;
			}
			evt.WaitOne(to, false);
			lock(typeof(GSMTerminal))
			{
				return should_stop;
			}
		}
		void OnLinkClosed()
		{
			$"Link closed"
				.CallTraceInformation();
		}

		// Get current Link.
		public Link Link
		{
			get
			{
				lock(this)
				{
					return src_;
				}
			}
			set
			{
				lock(this)
				{
					if(src_ != null) src_.Dispose();
					src_ = value;
				}
			}
		}

		/// <summary>
		/// Driver should call this method before it notifies his clients
		/// </summary>
		/// <returns></returns>
		bool ShouldFire()
		{
			bool ret;
			int wt = 0;
			if(speed > 0.0f && !fetch_log)
			{
				if(fire_tm != DateTime.MinValue)
					wt = (int)(1000.0 / speed) -(DateTime.Now - fire_tm).Milliseconds;
				else wt = -1;
				if(wt < 0)
					wt = 0;
			}
			ret = !WaitStop(wt);
			fire_tm = DateTime.Now;
			return ret;
		}

		/// <summary>
		/// Function for parsing of MJ2016GSM data. It can be sta messages or log
		/// data. If message is completed true is returned and parsed data is
		/// cleared from input buffer. On success output parameter PositionData
		/// pos if filled.
		/// </summary>
		/// <param name="buf">data</param>
		/// <param name="pos">object to fill</param>
		/// <returns>success</returns>
		bool MJ2016Parse(ArrayList buf, out PositionData[] pos)
		{
			pos = null;
			int typ;
			string s = "";

			// SGGB. starts from 0xAA
			int beg = buf.IndexOf((byte)0xAA), eol = 0;
			Debug.Assert(beg == -1, "SGGB found");
			byte[] res;
			if(beg == -1)
			{
				char[] c = new char[buf.Count];
				for(int i = 0; i < c.Length; ++i) c[i] = (char)(byte)buf[i];
				s = new string(c);

				beg = s.IndexOf(">A");
				if(beg == -1)
				{
					beg = s.IndexOf("01");
					if(beg == -1)
						return false;
					typ = 2;
				}
				else
					typ = 1;
			}
			else
			{
				typ = 3;
				if(beg + 18 > buf.Count) return false;

				if((eol =
					beg + 16 +
					(byte)buf[beg + 13] +
					((byte)buf[beg + 12] << 8) +
					((byte)buf[beg + 11] << 16) +
					2
					) > buf.Count) return false;

				res = new byte[eol - beg];
				for(int i = 0, cnt = res.Length; i < cnt; ++i) res[i] = (byte)buf[beg + i];
			}

			buf.RemoveRange(0, beg);

			if(typ < 3)
			{
				s = s.Substring(beg);
				if((eol = s.IndexOf("\r\n")) == -1) return false;
			}
			else eol -= beg;

			bool ret = false;
			PositionData p;
			switch(typ)
			{
			case 1:
				ret = ParseA(s.Substring(0, eol), out p);
				if(ret) pos = new PositionData[] { p };
				buf.RemoveRange(0, eol + 2);
				break;

			case 2:
				ret = Parse01(s.Substring(0, eol), out p);
				if(ret) pos = new PositionData[] { p };
				buf.RemoveRange(0, eol + 2);
				break;

			case 3:
				break;
			}
			return ret;
		}

		// Latitude and longitude factors for conversion from MJ2016GSM.
		const double LATITUDE_FACTOR = 1.0728837339e-05f;
		const double LONGITUDE_FACTOR = 2.1457672119e-05f;

		// Function for parsing log message '01' form MJ2016GSM device. Buf
		// contains raw data. Parsed position data is returned in data 
		// parameter. Function returns true on success and false otherwise.
		//
		// Message format:
		//           1         2         3 
		// 0123456789x123456789x123456789x1
		// 01ffBBBBBBLLLLLLttttttttVVrrRRcc
		// 01804695ff0a69f33d7c958200440625
		// 010046971a0a68d43d7c95a300451577
		// 01ffBBBBBBLLLLLLttttttttVVrrRRcc
		//
		//	ff      : flags(we are not handling this yet)
		//	BBBBBB  : latitude(multiply with 1.0728837339e-05 for conversion)
		//	LLLLLL  : longitude(multiply with 2.1457672119e-05 for conversion)
		//	tttttttt: UTC time
		//	VV      : speed(0-254), 255 is unknown speed
		//	rr      : GPS receiver status. Contains 2 characters 
		//             (0x40 * nav + nsat), where nsat is number of acquired
		//             GPS satellites and nav is position determination status 
		//             (0 .. position not determined, 1 .. position determined, 
		//             2 .. DGPS position). 
		//	RR      : Driven distance from journey beginning.
		//	cc      : Check sum. Sum of all message bytes cc modulo 0x100 is zero.
		bool Parse01(string buf, out PositionData data)
		{
			data = null;
			if(buf.Length != 32)
				return false;

			data = new PositionData();
			try
			{
				data.Time = int.Parse(buf.Substring(16, 8), NumberStyles.AllowHexSpecifier);
				data.Longitude = LONGITUDE_FACTOR *
					int.Parse(buf.Substring(10, 6), NumberStyles.AllowHexSpecifier);
				data.Latitude = LATITUDE_FACTOR *
					int.Parse(buf.Substring(4, 6), NumberStyles.AllowHexSpecifier);
				data.Speed = int.Parse(buf.Substring(24, 2), NumberStyles.AllowHexSpecifier);
				
				#region unused
				/*
				int stat = (int)int.Parse(buf.Substring(2, 2), NumberStyles.AllowHexSpecifier);
				if((stat & 0x40) != 0) {
					status = StatusCode.journey_stop;
				}
				else if(0 != (stat & 0x80)) {
					status = StatusCode.journey_start;
				}

				stat = int.Parse(buf.Substring(26, 2), NumberStyles.AllowHexSpecifier);
				if(stat > 0x80) {
					data.fHDOP = 1.0f;
					data.nSatellites = (PositionData.Satellite)(stat%0x80);
				}
				else if(stat > 0x40) {
					data.fHDOP = 1.0f;
					data.nSatellites = (PositionData.Satellite)(stat%0x40);
				}
				else {
					data.fHDOP = (float)PositionData.HDOP.BadQuality;
					data.nSatellites = (PositionData.Satellite)(stat&0x0f);
				}
				*/
				#endregion unused

				data.Satellites = 
					int.Parse(buf.Substring(26, 2), NumberStyles.AllowHexSpecifier) % 0x40;

				return true;
			}
			catch(Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();

				return false;
			}
		}

		// Function for parsing sta message from MJ2016GSM device. Buf contains 
		// raw data. Parsed position data is returned in data parameter. 
		// Function returns true on success and false otherwise.
		//
		// Message format:
		//           1         2         3         4         5         6         7         8
		// 0123456789x123456789x123456789x123456789x123456789x123456789x123456789x123456789x1234
		// >AttttttttBBBBBBLLLLLL,VVHHrrxxxx TT,AASSRGgx,xxWWA0A1A2A3A4C1C2C3C4TRxxxxxxVVVV*CCCC
		// >A3db3c98b4730da0a39b2,00ff000000ff,00014900,0001b8515051510000000000002427*768e

		// >AttttttttBBBBBBLLLLLL,VVHHrrxxxxTT,AASSRGgx,xxA0A1A2A3A4C1C2C3C4TRxxxxVVVV*CCCC
		// >A42130d854f9e081a6faa,00ff04000005,00035900,00ff6e6d6e6f00000000000000302c*46ea
		// >A42130d864f9e0d1a6fac,04ff43000000,00035900,00ff6e6d6e6f00000000000000302c*4f34
		// >A42130d874f9e0e1a6fac,06ff43000000,00035900,00ff6e6d6e6f00000000000000302c*3002
		// >A42130d884f9e0c1a6fae,0aff44000000,00035900,00ff6e6d6e6f00000000000000302c*c444
		// >A42130d894f9e0c1a6fae,0aff04000001,00035900,00ff6e6d6e6f00000000000000302c*abdc

		//	tttttttt: UTC time
		//	BBBBBB  : latitude(multiply with 1.0728837339e-05 for conversion)
		//	LLLLLL  : longitude(multiply with 2.1457672119e-05 for conversion)
		//	VV      : speed(0-254), 255 is unknown speed
		//	HH      : heading(0-179) value of 255 is invalid heading
		//	rr      : GPS receiver status. Contains 2 characters 
		//             (0x40 * nav + nsat), where nsat is number of acquired
		//             GPS satellites and nav is position determination status 
		//             (0 .. position not determined, 1 .. position determined, 
		//             2 .. DGPS position). 
		bool ParseA(string buf, out PositionData data)
		{
			data = null;
			int ast = buf.IndexOf("*");
			if(ast != 75)
				return false;

			try
			{
				ushort crc = 0,
					o_crc = ushort.Parse(buf.Substring(76, 4), NumberStyles.AllowHexSpecifier);
				for(int i = 0; buf[i] != '*'; ++i)
				{
					ushort a = (ushort)(buf[i] ^(crc >> 8));
					crc <<= 8;
					crc ^= a;
					crc ^= (ushort)(a << 12);
					crc ^= (ushort)(a << 5);
					crc ^= a >>= 4;
					crc ^= a <<= 5;
					crc ^= (ushort)(a << 7);
				}
				if(crc != o_crc)
					return false;

				data = new PositionData();
				data.Time = int.Parse(buf.Substring(2, 8), NumberStyles.AllowHexSpecifier);
				data.Longitude = LONGITUDE_FACTOR *
					int.Parse(buf.Substring(16, 6), NumberStyles.AllowHexSpecifier);
				data.Latitude = LATITUDE_FACTOR *
					int.Parse(buf.Substring(10, 6), NumberStyles.AllowHexSpecifier);
				data.Speed =
					int.Parse(buf.Substring(23, 2), NumberStyles.AllowHexSpecifier);
				data.VoltageAN1 = 
					int.Parse(buf.Substring(49, 2), NumberStyles.AllowHexSpecifier);
				data.VoltageAN2 = 
					int.Parse(buf.Substring(51, 2), NumberStyles.AllowHexSpecifier);
				data.VoltageAN3 = 
					int.Parse(buf.Substring(53, 2), NumberStyles.AllowHexSpecifier);
				data.VoltageAN4 = 
					int.Parse(buf.Substring(55, 2), NumberStyles.AllowHexSpecifier);
				data.Satellites = 
					int.Parse(buf.Substring(27, 2), NumberStyles.AllowHexSpecifier) % 0x40;

				#region unused
				/*int stat = int.Parse(buf.Substring(27, 2), NumberStyles.AllowHexSpecifier);
				if(stat > 0x80) {
					data.fHDOP = 1.0f;
					data.nSatellites = (PositionData.Satellite)(stat%0x80);
				}
				else if(stat > 0x40) {
					data.fHDOP = 1.0f;
					data.nSatellites = (PositionData.Satellite)(stat%0x40);
				}
				else
					data.fHDOP = (float)PositionData.HDOP.BadQuality;*/
				#endregion unused

				data.Firmware = buf.Substring(71, 4);
				return true;
			}
			catch(Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
				return false;
			}
		}

		/// <summary>
		/// command interpreter
		/// </summary>
		/// <param name="cmd">command</param>
		public override bool SendCommand(IStdCommand cmd)
		{
			$"Command received:\n{cmd}"
				.CallTraceInformation();

			switch(cmd.Type)
			{
			case CmdType.Trace:
				switch((int)cmd.Params[switchPar])
				{
					// force connect. not implemented
				case 2:
					// attempt to connect
				case 1:
					if(!Acquire(cmd.Sender))
					{
						// reflect to busy state
						OnLinkBusy(cmd.Sender, cmd.Target);
						return false;
					}

					ThreadPool.QueueUserWorkItem(new WaitCallback(_Start), cmd);
					break;
					// attempt to disconnect
				case 0:
					IOperatorInfo oi = operatorInfo;
						if (oi != null && oi.OperatorId != (int) ObjectID.Unknown &&
							cmd.Sender.OperatorId != oi.OperatorId) return true;

					goto case -1;
					// force disconnect
				case -1:
					if(Busy) ThreadPool.QueueUserWorkItem(new WaitCallback(Stop), cmd);
					break;

				default:
					throw new ArgumentException("Некорректное значение параметра '" +
						switchPar + "' - " + cmd.Params[switchPar]);
				}
				break;

			case CmdType.GetLog:
				switch((int)cmd.Params[switchPar])
				{
				case 1:
					if(!Acquire(cmd.Sender))
					{
						OnLinkBusy(cmd.Sender, cmd.Target);
						return false;
					}

					ThreadPool.QueueUserWorkItem(new WaitCallback(FetchLog), cmd);
					break;

				case 0:
					IOperatorInfo oi = operatorInfo;
					if(oi != null && oi.OperatorId != (int)ObjectID.Unknown &&
						cmd.Sender.OperatorId != oi.OperatorId) return false;

					goto case -1;

				case -1:
					ThreadPool.QueueUserWorkItem(new WaitCallback(Stop), cmd);
					break;

				default:
					throw new ArgumentException("Некорректное значение параметра '" +
						switchPar + "' - " + cmd.Params[switchPar]);
				}
				break;

			case CmdType.Control:
			case CmdType.SendText:
			case CmdType.CutOffElectricity:
			case CmdType.ReopenElectricity:
			case CmdType.CutOffFuel:
			case CmdType.ReopenFuel:

				if(!Acquire(cmd.Sender))
				{
					OnLinkBusy(cmd.Sender, cmd.Target);
					return false;
				}

				ThreadPool.QueueUserWorkItem(new WaitCallback(Control), cmd);
				break;

			default:
				throw new ArgumentException(
					"Неизвестный тип команды - " + cmd.Type.ToString());
			}

			return true;
		}

		/// <summary>
		/// Инициализация терминала через список строк
		/// </summary>
		public override void Initialization(params string[] initStrings)
		{
			base.Initialization(initStrings);

			if(initStrings[1] != "TAPI" || initStrings[2] != modem)
				throw new ArgumentException("Unknown init parameters");

			if(initStrings.Length >= 5)
			{
				replyTO = int.Parse(initStrings[3]);
				ioTO = int.Parse(initStrings[4]);
			}

			if(initStrings.Length < 6 || initStrings[5].ToLower() != "monitor") return;

			lnkWaiter = new Link(replyTO, ioTO);
			lnkWaiter.OfferingEvent +=
				new TAPIModemImpl.OfferingEventHandler(lnkWaiter_OfferingEvent);
			lnkWaiter.StartMonitor();
		}

		/// <summary>
		/// line is really busy
		/// </summary>
		bool LineBusy
		{
			get
			{
				lock(this)
				{
					return src_ != null && !src_.Closed;
				}
			}
		}

		/// <summary>
		/// terminal is busy. more common than LineBusy
		/// </summary>
		bool Busy
		{
			get
			{
				lock(busy_cs)
				{
					return busy;
				}
			}
			set
			{
				lock(busy_cs)
				{
					busy = value;
				}
			}
		}

		/// <summary>
		/// try to get exclusive access to terminal
		/// </summary>
		/// <returns>true if success</returns>
		bool Acquire(IOperatorInfo oi)
		{
			if(busy) 
				return false;

			lock(busy_cs)
			{
				if(busy) 
					return false;
				operatorInfo = oi;
				return busy = true;
			}
		}

		/// <summary>
		/// link is busy. log this and notify operator
		/// </summary>
		/// <param name="oi">info about operator that must be notified</param>
		/// <param name="ui">info about busy unit</param>
		void OnLinkBusy(IOperatorInfo oi, IUnitInfo ui)
		{
			var info = operatorInfo;
			$"Line is busy.{(info != null ? " Operator - " + info.Name : "")}"
				.CallTraceWarning();
			OnNotify(new NotifyEventArgs(TerminalEventMessage.TM_LINKSTATUS, oi,
				Severity.Lvl2, NotifyCategory.Terminal,
				"Линия занята." +(info != null? " Оператор - " + info.Name: ""), null,
				LinkStatus.Busy, ui, info, fetch_log? CmdType.GetLog: CmdType.Trace));
		}

		/// <summary>
		/// send control commands
		/// </summary>
		/// <param name="command">IStdCommand a must</param>
		void Control(object command)
		{
			IStdCommand cmd = (IStdCommand)command;
			IDevice device = null;

			try
			{
				device = itmManager.GetDevice(cmd.Target);
				device.Init(CmdType.GSM, cmd);
				Debug.Assert(!LineBusy);

				string target = cmd.Target.Telephone;
				if(null == target)
					throw new ArgumentException("Неправильно набран номер");

				int num_retry = 0;
				lock(this)
				{
					if(Link == null || Link.Number != target)
					{
						Link = new Link(replyTO, ioTO);
						Link.Number = target;
					}
					unitInfo = cmd.Target;
					SetStop(false);
				}

				$"Starting main cycle"
					.CallTraceInformation();

				while(!WaitStop(1000))
				{
					if(Link.Error == ErrorCode.Timeout || Link.Open())
					{
						device.OnLinkOpen(Link);
						if(Link.Error == ErrorCode.OK) break;
					}

					if(Link.Error != ErrorCode.Timeout)
					{
						Link.Close();
						OnLinkClosed();
					}
					if(num_retry != retry_count)
					{
						num_retry++;
						$"Retry connect - {num_retry}"
							.CallTraceWarning();
						continue;
					}

					$"Exit connect cycle due to retry count exceeded"
						.CallTraceInformation();

					throw new Exception("Соединение установить не удалось");
				}
				$"After connect cycle"
					.CallTraceInformation();

				if(WaitStop(0))
					return;

				byte[] data = device.GetCmd(cmd);
				Thread.Sleep(5000);
				if (tsTerm.TraceInfo)
					$"Sending control command:\n{BitConverter.ToString(data)}"
						.CallTraceInformation();

				Link.Write(data);
				
				string text = "Сообщение";
				if (cmd.Params["text"] != null)
					text = (string)cmd.Params["text"];

				OnNotify(new NotifyEventArgs(TerminalEventMessage.NE_MESSAGE, cmd.Sender, Severity.Lvl3,
					NotifyCategory.Terminal, $"{text} - отправлено (по GSM)", null, cmd.Target, -1, 
					"Сообщение отправлено"));
			}
			catch(Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();

				OnNotify(new NotifyEventArgs(TerminalEventMessage.TM_EXCEPTION, cmd.Sender, Severity.Lvl0, 
					NotifyCategory.Terminal, ex.Message, ex, LinkStatus.Error, cmd.Target));
			}
			finally
			{
				$"Do finalization"
					.CallTraceInformation();

				if(Link != null)
				{
					if(Link.Error == ErrorCode.OK && device.UninitSeq != null)
						Link.Write(device.UninitSeq);
					Link.Close();
					OnLinkClosed();
				}

				$"Link closed"
					.CallTraceInformation();

				Busy = false;

				OnNotify(new NotifyEventArgs(TerminalEventMessage.TM_LINKSTATUS, cmd.Sender,
					Severity.Lvl3, NotifyCategory.Terminal, "Соединение разорвано", null,
					LinkStatus.Closed, cmd.Target, operatorInfo, cmd.Type));

				$"End working thread"
					.CallTraceInformation();
			}
		}

		bool lnkWaiter_OfferingEvent(Object sender)
		{
			if(!Acquire(null)) return false;

			ThreadPool.QueueUserWorkItem(new WaitCallback(ProceedInCall));
			return true;
		}

		/// <summary>
		/// incoming call processing
		/// </summary>
		/// <param name="pars">state</param>
		void ProceedInCall(object pars)
		{
			if(!lnkWaiter.Open(true)) return;

			try
			{
				SetStop(false);
				byte[] data = new byte[256];
				bool alerted = false;
				while(!WaitStop(0) && lnkWaiter.Error == ErrorCode.OK)
				{
					int rb = lnkWaiter.Read(data);
					if(rb == 0) continue;

					if(!alerted && System.Array.IndexOf(data,(byte)0xAA) != -1)
					{
						byte[] dst = new byte[rb];
						System.Array.Copy(data, dst, rb);
						ArrayList al = new ArrayList(256);
						al.AddRange(dst);
						PositionData[] pds;
						bool res = MJ2016Parse(al, out pds);
						if(res && pds != null && pds.Length != 0)
						{
							alerted = true;
							IUnitInfo ui = UnitInfo.Factory.Create( UnitInfoPurpose.GSMTerminal );
							ui.Telephone = "";
							OnNotify(new NotifyEventArgs(TerminalEventMessage.NE_ALARM, null, 
								Severity.Lvl1, NotifyCategory.Terminal, "Тревога", null, ui, 
								"", 1));
						}
					}
					OnData(data, rb, "");
				}
			}
			finally
			{
				doneevt.Set();
				Busy = false;
			}
		}

		void SetStop(bool stop)
		{
			if(should_stop == stop) return;
			lock(typeof(GSMTerminal))
			{
				if(should_stop = stop) evt.Set();
			}
		}
	}
}