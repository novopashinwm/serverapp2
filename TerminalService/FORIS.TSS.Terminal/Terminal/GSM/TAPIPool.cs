// Static Model

using System;
using System.Threading;
using System.Windows.Forms;

using FORIS.TSS.IO.TAPI;

using HLINEAPP	= System.IntPtr;
using HCALL		= System.IntPtr;
using HANDLE	= System.IntPtr;
using HLINE		= System.IntPtr;

namespace FORIS.TSS.Terminal.GSM
{
	/// <summary>
	/// modem pool impl. counterpart TAPIModemImpl
	/// ported from mireo code
	/// Info: need optimization
	/// !! epte !!
	/// </summary>
	public class TAPIPool : IDisposable
	{
		public CTapi tapi;

		public static TAPIPool my_ptr;

		private AutoResetEvent e_stop = new AutoResetEvent(false);

		private bool init_ok;

		Thread thd;

		TAPIPool()
		{
			lock(this)
			{
				thd = new Thread(new ThreadStart(Start));
				thd.Name = "TAPIPool";
				thd.IsBackground = true;
				thd.Start();
				e_stop.WaitOne();
			}
		}

		~TAPIPool()
		{
			Stop();
		}

		public void Dispose()
		{
			Stop();
			GC.SuppressFinalize(this);
		}

		public bool GetHandle(HCALL t_call, out HANDLE h)
		{
			return CTapi.LineErrReturn.LINEERR_OK == tapi.GetID(t_call, out h);
		}

		public int Call(string name, string number, ref CLine t_line, ref HCALL t_call)
		{
			t_line = null;
			t_call = IntPtr.Zero;
			lock(this)
			{
				if (!init_ok)
					return -1;

				CLine line = tapi.GetLineByFilter("Universal Modem Driver", false, // ?????
					CTapi.LineCallPrivilege.LINECALLPRIVILEGE_MONITOR);
				t_line = line;
				if (null == line) return -1;
				
				CTapi.linecallparams call_params = new CTapi.linecallparams();
				call_params.dwTotalSize = (uint)
					System.Runtime.InteropServices.Marshal.SizeOf(call_params);
				call_params.dwMinRate = 2400;
				call_params.dwMaxRate = 57600;
				call_params.dwMediaMode = (uint)CTapi.LineMediaMode.LINEMEDIAMODE_DATAMODEM;

				return tapi.DialNumber(line, number, out t_call, call_params);
			}
		}

		public static TAPIPool Inst()
		{
			lock(typeof(TAPIPool))
			{
				if (null == TAPIPool.my_ptr) 
					TAPIPool.my_ptr = new TAPIPool();
				return TAPIPool.my_ptr;
			}
		}

		private void Start()
		{
			tapi = new CTapi("CTapi", 0x00020000, IntPtr.Zero);
			if(null == tapi)
			{
				e_stop.Set();
				return;
			}
			init_ok = true;
			e_stop.Set();

			Application.Run();
			tapi.ShutDown();
		}

		private void Stop()
		{
			Application.Exit();
		}
	}// END CLASS DEFINITION TAPIPool
}
