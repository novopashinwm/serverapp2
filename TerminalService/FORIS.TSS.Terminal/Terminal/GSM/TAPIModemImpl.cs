﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Communication;
using FORIS.TSS.IO;
using FORIS.TSS.IO.TAPI;
using CriticalSection = System.Object;
using HANDLE          = System.IntPtr;
using HCALL           = System.IntPtr;

namespace FORIS.TSS.Terminal.GSM
{
	/// <summary>
	/// modem implementation
	/// ported from mireo code
	/// Info: need optimization
	/// </summary>
	public class TAPIModemImpl : Link, IDisposable
	{
		ErrorCode err = ErrorCode.Unknown;
		CriticalSection mtx = new CriticalSection();
		public ErrorCode Error
		{
			get
			{
				lock(mtx)
				{
					return err;
				}
			}
			set
			{
				lock(mtx)
				{
					err = value;
				}
			}
		}

		private TAPIPool pool;

		private string name;
		private string modem;
		private string number;

		private CLine t_line;

		private HCALL t_call;

		private AutoResetEvent e_conn = new AutoResetEvent(false);
		/// <summary>
		/// LineReply timeout
		/// </summary>
		int ReplyTimeout	= 60000;

		SerialStream ss;
		BinaryReader br;
		BinaryWriter bw;

		int ReqID = -1;

		/// <summary>
		/// delegate for offering  event
		/// </summary>
		public delegate bool OfferingEventHandler(object sender);
		/// <summary>
		/// event of incoming call
		/// </summary>
		public event OfferingEventHandler OfferingEvent;

		public TAPIModemImpl(int replyto, int ioto)
		{
			ss = new SerialStream(ioto);
			br = new BinaryReader(ss);
			bw = new BinaryWriter(ss);

			pool = TAPIPool.Inst();
			ReplyTimeout = replyto;
		}

		public TAPIModemImpl(string n, int ioto)
		{
			ss = new SerialStream(ioto);
			br = new BinaryReader(ss);
			bw = new BinaryWriter(ss);

			pool = TAPIPool.Inst();
			name = n.Trim();
			modem = name.Substring(0, name.IndexOf(',')).Trim();
			number = name.Substring(name.IndexOf(',') + 1).Trim();
		}

		public TAPIModemImpl(string n) : this(n, 30000)
		{
		}
		public TAPIModemImpl(string n, int replyto, int ioto) : this(n, ioto)
		{
			ReplyTimeout = replyto;
		}

		~TAPIModemImpl()
		{
			Close();
		}

		public void Dispose()
		{
			Close();
			GC.SuppressFinalize(this);
		}

		public string Number
		{
			get
			{
				return number;
			}
			set
			{
				number = value != null? value.Trim(): "";
			}
		}

		public bool Open(bool answer)
		{
			if(!answer) return Open();

			if (!ss.Closed)
				Close();
			lock(this)
			{
				e_conn.Reset();
				pool.tapi.LineReplyEvent += new CTapi.LineReplyEventHandler(OnLineReply);
			}

			if (e_conn.WaitOne(ReplyTimeout, false))
			{
				if (err != ErrorCode.OK)
					return false;
			}
			else
			{
				Close();
				Error = ErrorCode.SysError;
				return false;
			}

			HANDLE h_;
			if (!pool.GetHandle(t_call, out h_))
			{
				Close();
				Error = ErrorCode.SysError;
				return false;
			}
			ss.Handle = h_;
			Thread.Sleep(1000);
			ss.SetTimeouts(200, 0, 0, 0, 1000);

			Error = ErrorCode.OK;
			return true;
		}

		public bool Open()
		{
			return Open(Number);
		}

		public bool Open(string phone)
		{
			if (!ss.Closed)
				Close();
			if (phone == null || phone == "") {
				Error = ErrorCode.WrongParam;
				return false;
			}
			lock(this)
			{
				ReqID = pool.Call(modem, phone, ref t_line, ref t_call);
				Trace.WriteLine("GSM Open link: modem: " + modem + " Phone: " + phone);
				if (ReqID < 0) {
					Close();
					Error = ErrorCode.Unavailable;
					return false;
				}
				e_conn.Reset();
				pool.tapi.LineReplyEvent += new CTapi.LineReplyEventHandler(OnLineReply);
			}

			if (e_conn.WaitOne(ReplyTimeout, false)) {
				if (err != ErrorCode.OK)
					return false;
			}
			else {
				Close();
				Error = ErrorCode.SysError;
				return false;
			}

			HANDLE h_;
			if (!pool.GetHandle(t_call, out h_)) {
				Close();
				Error = ErrorCode.SysError;
				return false;
			}
			ss.Handle = h_;
			Thread.Sleep(1000);
			ss.SetTimeouts(200, 0, 0, 0, 1000);

			Error = ErrorCode.OK;
			return true;
		}

		public void Close()
		{
			if (!ss.Closed) {
				ss.Close();
			}
			if (t_call != IntPtr.Zero) {
				pool.tapi.HangUp(t_call);
				t_call = IntPtr.Zero;
			}
			if (t_line != null) {
				t_line.LineClose();
				t_line = null;
			}
			pool.tapi.CallStateEvent -= new CTapi.CallStateEventHandler(OnCallState);
			pool.tapi.LineReplyEvent -= new CTapi.LineReplyEventHandler(OnLineReply);
			Error = ErrorCode.OK;
		}

		/// <summary>
		/// read chars from serial stream
		/// </summary>
		/// <param name="buf">buf to fill</param>
		/// <returns>number of chars read</returns>
		public override int Read(char[] buf)
		{
			if(ss.Closed)
			{
				Error = ErrorCode.Unavailable;
				return -1;
			}

			try
			{
				int rb = br.Read(buf, 0, buf.Length);
				Error = rb > 0? ErrorCode.OK: ErrorCode.Timeout;
				return rb;
			}
			catch(Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
				Error = ErrorCode.Unknown;
				return -2;
			}
		}

		/// <summary>
		/// read bytes from serial stream
		/// </summary>
		/// <param name="buf">buf to fill</param>
		/// <returns>number of bytes read</returns>
		public override int Read(byte[] buf)
		{
			if(ss.Closed)
			{
				Error = ErrorCode.Unavailable;
				return -1;
			}

			try
			{
				int rb = br.Read(buf, 0, buf.Length);
				Error = rb > 0? ErrorCode.OK: ErrorCode.Timeout;
				return rb;
			}
			catch(Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();

				Error = ErrorCode.Unknown;
				return -2;
			}
		}

		/// <summary>
		/// write string to stream
		/// </summary>
		/// <param name="data">string</param>
		public override void Write(string data)
		{
			try
			{
				bw.Write(data); bw.Flush();
				Error = ErrorCode.OK;
			}
			catch(Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();

				Error = ErrorCode.Unknown;
			}
		}

		/// <summary>
		/// write data to stream
		/// </summary>
		/// <param name="buf">bytes to write</param>
		public override void Write(byte[] buf)
		{
			try
			{
				bw.Write(buf, 0, buf.Length); bw.Flush();
				Error = ErrorCode.OK;
			}
			catch(Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();

				Error = ErrorCode.Unknown;
			}
		}

		public void OnCallState(object sender, CTapi.CallStateEventArgs args)
		{
			if(args.hcall != t_call &&
				CTapi.LineCallState.LINECALLSTATE_OFFERING != args.CallState) return;

			switch(args.CallState)
			{
				case CTapi.LineCallState.LINECALLSTATE_CONNECTED:
					Error = ErrorCode.OK;
					e_conn.Set();
					break;

				case CTapi.LineCallState.LINECALLSTATE_DISCONNECTED:
					Error = ErrorCode.Disconnected;
					e_conn.Set();
					break;

				case CTapi.LineCallState.LINECALLSTATE_BUSY:
					Error = ErrorCode.Busy;
					e_conn.Set();
					break;

				case CTapi.LineCallState.LINECALLSTATE_OFFERING:
					if((uint)CTapi.LineOfferingMode.LINEOFFERINGMODE_INACTIVE == args.CallStateDetail ||
						OfferingEvent == null || !OfferingEvent(this))
						return;

					t_call = args.hcall;
					pool.tapi.Answer(args.hcall);
					break;
			}
		}

		public void OnLineReply(object sender, CTapi.LineReplyEventArgs args)
		{
			if(args.IDRequest != ReqID) return;

			if ((int)args.status < 0) {
				Close();
				Error = ErrorCode.Unavailable;
				e_conn.Set();
			}
			else
				pool.tapi.CallStateEvent += new CTapi.CallStateEventHandler(OnCallState);
		}

		public bool Closed
		{
			get
			{
				return ss.Closed;
			}
		}

		public bool StartMonitor()
		{
			pool.tapi.CallStateEvent += new CTapi.CallStateEventHandler(OnCallState);
			CLine line = pool.tapi.GetLineByFilter("Universal Modem Driver", false, // ?????
				CTapi.LineCallPrivilege.LINECALLPRIVILEGE_OWNER |
				CTapi.LineCallPrivilege.LINECALLPRIVILEGE_MONITOR);
			t_line = line;
			return null != line;
		}

		public override bool Valid
		{
			get
			{
				return !Closed;
			}
		}
	}
}