﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Xml.Linq;
using FORIS.DataAccess;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Terminal.Emulator
{
	/// <summary> Класс наследник фонового потока работы эмулятора </summary>
	public class Emulator : BackgroundProcessor
	{
		/// <summary> Номер терминала </summary>
		private readonly int                  _terminalId;
		private readonly ITerminalManager     _terminalManager;
		private readonly Dictionary<int, int> _lastFakeVehicleLogTime = new Dictionary<int, int>();

		/// <summary> Конструктор </summary>
		/// <param name="terminalId">Номер терминала</param>
		public Emulator(int terminalId, ITerminalManager terminalManager = null)
		{
			_terminalId      = terminalId;
			_terminalManager = terminalManager;
		}
		private class EmulatedVehicle
		{
			public int      RealVehicleId   { get; set; }
			public int      FakeVehicleId   { get; set; }
			/// <summary> Начало периода, из которого берутся данные </summary>
			public int      DateFrom        { get; set; }
			/// <summary> Окончание периода, из которого берутся данные </summary>
			public int      DateTo          { get; set; }
			/// <summary> Время "запуска" эмулятора: данные по эмулятору должны оказаться в Log-таблицах начиная с указанного в этом поле времени </summary>
			public int      FakeLogTimeFrom { get; set; }
			/// <summary> Время последних записанных данных для эмулятора </summary>
			public int?     LastLogTime     { get; set; }
			/// <summary> Эмулируемая широта для статических объектов </summary>
			public decimal? FakeLat         { get; set; }
			/// <summary> Эмулируемая долгота для статических объектов </summary>
			public decimal? FakeLng         { get; set; }
		}
		private struct VehicleRequest
		{
			public VehicleRequest(int vehicleId, int logTimeFrom, int logTimeTo)
			{
				VehicleId   = vehicleId;
				LogTimeFrom = logTimeFrom;
				LogTimeTo   = logTimeTo;
			}

			public readonly int VehicleId;
			public readonly int LogTimeFrom;
			public readonly int LogTimeTo;
			public override bool Equals(object obj)
			{
				if (obj == null)
					return false;
				if (GetType() != obj.GetType())
					return false;
				var other = (VehicleRequest)obj;
				return VehicleId   == other.VehicleId
					&& LogTimeFrom == other.LogTimeFrom
					&& LogTimeTo   == other.LogTimeTo;
			}
			public override int GetHashCode()
			{
				return VehicleId.GetHashCode() ^ LogTimeFrom.GetHashCode() ^ LogTimeTo.GetHashCode();
			}
		}
		private class Emulation
		{
			public EmulatedVehicle Vehicle;
			public int             DateOffset;
		}
		private void AddEmulation(
			Dictionary<VehicleRequest, List<Emulation>> requestToEmulations,
			VehicleRequest vehicleRequest,
			Emulation emulation)
		{
			if (!requestToEmulations.TryGetValue(vehicleRequest, out List<Emulation> emulations))
				requestToEmulations.Add(vehicleRequest, emulations = new List<Emulation>(1));
			emulations.Add(emulation);
		}
		/// <summary> Подготовка эмулируемых данных </summary>
		/// <returns> Эмулируемые данные </returns>
		private List<IMobilUnit> GetLogForEmulator()
		{
			// Получаем список эмулируемых объектов
			var emulatedVehicles = GetEmulatedVehicles().ToDictionary(ev => ev.FakeVehicleId);
			// Получаем текущее время
			var currLogTime = TimeHelper.GetSecondsFromBase(DateTime.UtcNow);
			// Создаем и заполняем словарь запросов на эмуляцию
			var vehicleRequestToEmulation = new Dictionary<VehicleRequest, List<Emulation>>();
			foreach (var ev in emulatedVehicles.Values)
			{
				if (currLogTime < ev.FakeLogTimeFrom)
					continue; // Время начала эмуляции еще не наступило

				// Если последнее время совсем старое корректируем его на интервал сохранения
				// TODO: Взять его из БД или кода, сейчас и там и там 180 дней
				if (currLogTime - ev.LastLogTime > TimeSpan.FromDays(180).TotalSeconds)
					ev.LastLogTime = currLogTime - (int)TimeSpan.FromDays(180).TotalSeconds;

				if (currLogTime < ev.LastLogTime)
					continue; //Данные каким-то образом уже попали в БД, значит, не нужно эмулировать

				// Длина периода, из которого берутся данные с реального объекта наблюдения
				var periodLength = ev.DateTo - ev.DateFrom + 1;

				if (periodLength <= 0)
					continue; // Длина периода меньше либо равна 0

				// Коррекция времени начала либо предыдущее+1 либо начальная дата эмуляции
				var logTimeFrom = (ev.LastLogTime + 1) ?? ev.FakeLogTimeFrom;
				if (logTimeFrom < ev.FakeLogTimeFrom)
					logTimeFrom = ev.FakeLogTimeFrom;

				// Относительное время начала (относительное от начальной даты эмуляции)
				var relativeLogTimeFrom = logTimeFrom - ev.FakeLogTimeFrom;
				// Относительное время окончания (текущее время) (относительное от начальной даты эмуляции)
				var relativeLogTimeTo   = currLogTime - ev.FakeLogTimeFrom;

				// Разные алгоритмы, сколько периодов эмуляции потребуется
				if (relativeLogTimeTo / periodLength == relativeLogTimeFrom / periodLength)
				{ // Один период эмуляции
					var sourceLogTimeFrom = (relativeLogTimeFrom % periodLength) + ev.DateFrom;
					// В рамках одного периода эмуляции, значит, достаточно одного запроса к БД
					AddEmulation(vehicleRequestToEmulation,
						new VehicleRequest(ev.RealVehicleId, sourceLogTimeFrom, (relativeLogTimeTo % periodLength) + ev.DateFrom),
						new Emulation     { Vehicle = ev, DateOffset = logTimeFrom - sourceLogTimeFrom });
				}
				else
				{ // Несколько периодов эмуляции
					// В рамках двух и более периодов эмуляции
					if (1 < relativeLogTimeTo / periodLength - relativeLogTimeFrom / periodLength) // Более, чем 2 периода,
						relativeLogTimeTo = periodLength - 1; //значит, ограничиваем 2 периодами эмуляции

					var firstSourceLogTimeFrom = (relativeLogTimeFrom % periodLength) + ev.DateFrom;
					// Первый период - до конца периода
					var firstOffset = logTimeFrom - firstSourceLogTimeFrom;
					AddEmulation(vehicleRequestToEmulation,
						new VehicleRequest(
							ev.RealVehicleId,
							firstSourceLogTimeFrom,
							ev.DateTo),
						new Emulation
						{
							Vehicle    = ev,
							DateOffset = firstOffset
						});

					//Второй период - с начала по "relativeLogTimeTo"
					AddEmulation(vehicleRequestToEmulation,
						new VehicleRequest(
							ev.RealVehicleId,
							ev.DateFrom,
							(relativeLogTimeTo % periodLength) + ev.DateFrom),
						new Emulation
						{
							Vehicle    = ev,
							DateOffset = firstOffset + periodLength
						});
				}
			}
			// Результаты эмуляции
			var result = new List<IMobilUnit>();
			// Получить данные эмуляции (словарь: запрос, набор элементов)
			var sourceMobileUnits = GetMobileUnits(vehicleRequestToEmulation.Keys);
			foreach (var pair in sourceMobileUnits)
			{
				var emulations = vehicleRequestToEmulation[pair.Key];
				for (var i = 0; i != emulations.Count; ++i)
				{
					var emulation = emulations[i];
					foreach (var mobilUnit in pair.Value)
					{
						var fakeMobilUnit = (i == emulations.Count - 1) ? mobilUnit : (MobilUnit.Unit.MobilUnit)mobilUnit.Clone();
						result.Add(fakeMobilUnit);

						fakeMobilUnit.Unique = emulation.Vehicle.FakeVehicleId;
						if (emulation.Vehicle.FakeLat != null && emulation.Vehicle.FakeLng != null)
						{
							fakeMobilUnit.Latitude  = (double)emulation.Vehicle.FakeLat.Value;
							fakeMobilUnit.Longitude = (double)emulation.Vehicle.FakeLng.Value;
							fakeMobilUnit.Speed     = 0;
						}
						else
						{
							if (fakeMobilUnit.ValidPosition && null != _terminalManager)
							{
								var prevUnitPosition = _terminalManager.GetLastPosition(fakeMobilUnit.Unique);
								if (null != prevUnitPosition)
									fakeMobilUnit.CalcCourse(prevUnitPosition);
							}
						}
						fakeMobilUnit.Time += emulation.DateOffset;
					}
				}
			}
			foreach (var mu in result)
			{
				if (_lastFakeVehicleLogTime.TryGetValue(mu.Unique, out int lastLogTime) &&
					mu.Time < lastLogTime)
					continue;
				_lastFakeVehicleLogTime[mu.Unique] = mu.Time;
			}
			return result;
		}
		/// <summary> Получение из БД логов для эмуляции </summary>
		/// <param name="vehicleRequests"></param>
		/// <returns></returns>
		private Dictionary<VehicleRequest, List<MobilUnit.Unit.MobilUnit>> GetMobileUnits(IEnumerable<VehicleRequest> vehicleRequests)
		{
			var result = new Dictionary<VehicleRequest, List<MobilUnit.Unit.MobilUnit>>();

			var dt = new DataTable();
			dt.Columns.Add("Vehicle_ID",    typeof(int));
			dt.Columns.Add("Log_Time_From", typeof(int));
			dt.Columns.Add("Log_Time_To",   typeof(int));

			foreach (var vehicleRequest in vehicleRequests)
				dt.Rows.Add(vehicleRequest.VehicleId, vehicleRequest.LogTimeFrom, vehicleRequest.LogTimeTo);

			using (var sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetLogForEmulator"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp.SetTableValuedParameter("@logParams", dt);
				using (var dr = sp.ExecuteReader())
				{
					while (dr.Read())
					{
						var vehicleRequest = new VehicleRequest(
							dr.GetInt32(dr.GetOrdinal("Vehicle_ID")),
							dr.GetInt32(dr.GetOrdinal("Log_Time_From")),
							dr.GetInt32(dr.GetOrdinal("Log_Time_To")));
						var mu = new MobilUnit.Unit.MobilUnit
						{
							CorrectGPS = false,
							Media      = Media.Emulator,
							cmdType    = CmdType.Trace,
							Firmware   = "Emulator",
							TermID     = _terminalId,
							Unique     = vehicleRequest.VehicleId,
							Time       = dr.GetInt32(dr.GetOrdinal("Log_Time"))
						};
						mu.Properties.Add(DeviceProperty.Protocol, "Emulator");

						if (!result.TryGetValue(vehicleRequest, out List<MobilUnit.Unit.MobilUnit> mobilUnits))
							result.Add(vehicleRequest, mobilUnits = new List<MobilUnit.Unit.MobilUnit>());
						mobilUnits.Add(mu);

						// Получаем данные xml
						var dataXmlString = dr.GetStringOrNull(dr.GetOrdinal("Data"));
						if (dataXmlString != null)
						{
							var data = XDocument.Parse(dataXmlString).Root;
							if (null != data)
							{
								mu.Latitude   = data.GetDouble("Lat") ?? MobilUnit.Unit.MobilUnit.InvalidLatitude;
								mu.Longitude  = data.GetDouble("Lng") ?? MobilUnit.Unit.MobilUnit.InvalidLongitude;
								mu.Speed      = data.GetInt32("SPEED");
								mu.Radius     = data.GetInt32("Radius");
								mu.Satellites = data.GetInt32("SATELLITES") ?? 7;
								mu.CorrectGPS = mu.Latitude > -90 && mu.Longitude > -180 && mu.Latitude < 90 && mu.Longitude < 180;
							}
						}

						var sensorsXmlString = dr.GetStringOrNull(dr.GetOrdinal("Sensors"));
						if (sensorsXmlString != null)
						{
							var sensorsXml = XDocument.Parse("<root>" + sensorsXmlString + "</root>").Root;
							if (sensorsXml == null)
								continue;
							if (mu.SensorValues == null)
								mu.SensorValues = new Dictionary<int, long>();
							foreach (var element in sensorsXml.Descendants())
							{
								var number = element.GetInt32("Number");
								var value  = element.GetInt64("Value");
								if (number == null || value == null)
									continue;
								mu.SensorValues[number.Value] = value.Value;
							}
						}
					}
				}
			}

			return result;
		}
		/// <summary> Получение списка эмулируемых объектов (машин) </summary>
		private List<EmulatedVehicle> GetEmulatedVehicles()
		{
			var emulatedVehicles = new List<EmulatedVehicle>();
			using (var sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetEmulators"))
			{
				using (new ConnectionOpener(sp.Command.Connection))
				{
					using (var reader = sp.ExecuteReader())
					{
						var realVehicleIdOrdinal   = reader.GetOrdinal("RealVehicleID");
						var fakeVehicleIdOrdinal   = reader.GetOrdinal("FakeVehicleID");
						var dateFromOrdinal        = reader.GetOrdinal("DateFrom");
						var dateToOrdinal          = reader.GetOrdinal("DateTo");
						var lastLogTimeOrdinal     = reader.GetOrdinal("LastLogTime");
						var fakeLogTimeFromOrdinal = reader.GetOrdinal("FakeLogTimeFrom");
						var fakeLatOrdinal         = reader.GetOrdinal("FakeLat");
						var fakeLngOrdinal         = reader.GetOrdinal("FakeLng");

						while (reader.Read())
						{
							emulatedVehicles.Add(
								new EmulatedVehicle
								{
									RealVehicleId   = reader.GetInt32(realVehicleIdOrdinal),
									FakeVehicleId   = reader.GetInt32(fakeVehicleIdOrdinal),
									DateFrom        = reader.GetInt32(dateFromOrdinal),
									DateTo          = reader.GetInt32(dateToOrdinal),
									LastLogTime     = reader.IsDBNull(lastLogTimeOrdinal) ? (int?)null : reader.GetInt32(lastLogTimeOrdinal),
									FakeLogTimeFrom = reader.GetInt32(fakeLogTimeFromOrdinal),
									FakeLat         = reader.IsDBNull(fakeLatOrdinal) ? (decimal?)null : reader.GetDecimal(fakeLatOrdinal),
									FakeLng         = reader.IsDBNull(fakeLngOrdinal) ? (decimal?)null : reader.GetDecimal(fakeLngOrdinal)
								});
						}
					}
				}
			}

			// Если БД опаздывает от локальных данных более, чем на час, данные не эмулируются, пока сохранение не завершит работу
			emulatedVehicles.RemoveAll(vehicle =>
			{
				if (!_lastFakeVehicleLogTime.TryGetValue(vehicle.FakeVehicleId, out int appLastLogTime))
					return false;
				return 3600 < appLastLogTime - vehicle.LastLogTime;
			});

			foreach (var vehicle in emulatedVehicles)
			{
				if (!_lastFakeVehicleLogTime.TryGetValue(vehicle.FakeVehicleId, out int appLastLogTime))
					continue;

				if (vehicle.LastLogTime == null || vehicle.LastLogTime.Value < appLastLogTime)
					vehicle.LastLogTime = appLastLogTime;
			}

			return emulatedVehicles;
		}

		private          DateTime? _lastRunningDateTime;
		private readonly TimeSpan  _intervalBetweenRunnings = TimeSpan.FromSeconds(10);
		private readonly TimeSpan  _minimalSleepingTime     = TimeSpan.FromSeconds(1);

		/// <summary> Метод выполняемый в цикле BackgroundProcessor.ThreadLoop </summary>
		/// <returns></returns>
		protected override bool Do()
		{
			if (_lastRunningDateTime == null)
				_lastRunningDateTime = DateTime.UtcNow;
			else
			{
				var timePassedSinceLastRunning = DateTime.UtcNow - _lastRunningDateTime.Value;
				var timeToSleepLeft = _intervalBetweenRunnings - timePassedSinceLastRunning;
				if (_minimalSleepingTime < timeToSleepLeft)
					Thread.Sleep(timeToSleepLeft);
			}
			// Проверка, есть ли куда отправлять
			var onSendEvent = OnSendEvent;
			if (onSendEvent == null)
				return false;
			// Отправка
			var emulatedPositions = GetLogForEmulator();
			foreach(var emulatedPositionsByUnique in emulatedPositions.GroupBy(u => u.Unique))
				onSendEvent(emulatedPositionsByUnique.OrderBy(u => u.Time));
			// Если даже нет элементов, то вызываем событие, что оно было
			if (0 == emulatedPositions.Count)
				onSendEvent(emulatedPositions);
			return emulatedPositions.Count != 0;
		}
		/// <summary> Эмулятор активирует событие, когда у него появляются новые данные </summary>
		public event Action<IEnumerable<IMobilUnit>> OnSendEvent;
	}
}