﻿using System;
using System.Diagnostics;
using System.Linq;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Emulator
{
	public class EmulatorTerminal : _Terminal
	{
		protected Terminal.Emulator.Emulator Emulator;

		public EmulatorTerminal(ITerminalManager mgr)
			: base("EmulatorTerminal", mgr)
		{
		}
		public EmulatorTerminal(string name, ITerminalManager mgr)
			: base(name, mgr)
		{
		}

		#region ITerminal Members

		/// <summary> Инициализация терминала через список строк </summary>
		/// <param name="initStrings">
		/// <p>initSrings[0] - название терминала (не используется),</p> 
		/// <p>initSrings[1] - IP адрес сервера,</p> 
		/// <p>initSrings[2] - порт сервера,</p>
		/// <p>initSrings[3] - тип очереди (0-нет, 1-MSMQ, 2-программная)</p>
		/// </param>
		public override void Initialization(params string[] initStrings)
		{
			try
			{
				base.Initialization(initStrings);
				Emulator = new Terminal.Emulator.Emulator(id, itmManager);
				Emulator.OnSendEvent += mobilUnits => PublishMobileUnit(mobilUnits.ToArray());
				Emulator.Start();
			}
			catch (Exception ex)
			{
				Trace.TraceError(ex.ToString());
			}
		}

		#endregion ITerminal Members

		#region IDisposable Members

		/// <summary> Очистка ресурсов </summary>
		/// <param name="disposing"> Флаг явной/неявной очистки ресурсов </param>
		protected override void Dispose(bool disposing)
		{
			lock (this)
			{
				// здесь выполняется явная очистка ресурсов (управляемые ресурсы)
				if (disposing)
				{
					// стоп
					Emulator.Stop();
				}
				// здесь выполняется неявная очистка (неуправляемые ресурсы)
			}
		}
		protected override void OnReceiveErrors(Exception ex, ReceiveEventArgs args)
		{
			lock (ReceiveErrorsRepairTimerLock)
			{
				Trace.TraceError(string.Format("{0}\r\n{1}", ex, args.MobilUnits));
			}
		}

		#endregion IDisposable Members

		private void PublishMobileUnit(params IMobilUnit[] mobilUnits)
		{
			OnReceive(new ReceiveEventArgs(mobilUnits));
		}
	}
}