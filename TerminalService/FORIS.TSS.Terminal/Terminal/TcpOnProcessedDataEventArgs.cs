﻿using System;
using System.Collections;
using System.Net;

namespace FORIS.TSS.Terminal.Terminal
{
	public class TcpOnProcessedDataEventArgs : EventArgs
	{
		public IPEndPoint Address { get; set; }
		public IList      Res     { get; set; }
	}
}