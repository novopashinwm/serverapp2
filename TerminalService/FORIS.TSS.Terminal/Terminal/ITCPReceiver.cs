﻿using System.Net;
using System.Net.Sockets;

namespace FORIS.TSS.Terminal.Terminal
{
	public interface ITCPReceiver : ITerminalReceiver
	{
		IPEndPoint EndPoint      { get; }
		IPEndPoint LocalEndPoint { get; }
		TcpClient Client         { get; }
		void CloseClient();
		void PrepareToProcessData();
	}
}