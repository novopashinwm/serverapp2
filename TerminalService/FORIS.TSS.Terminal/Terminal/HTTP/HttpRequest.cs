﻿using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TerminalService.Interfaces;
using System.Net;

namespace FORIS.TSS.Terminal.Terminal.HTTP
{
    class HttpRequest : IHttpRequest
    {
        private readonly string _text;

        private readonly string[] _allowedRemoteHostsForXRequestor =
                {
                    "10.241.11.25"
#if DEBUG                    
                    , "127.0.0.1"
                    , "::1"
#endif
                };

        private readonly Uri _requestUrl;
        private readonly HttpMethod _requestMethod;
        private readonly IPEndPoint _remoteEndPoint;
        private readonly string _headerXRequestor;
        private readonly Encoding _encoding;
        private readonly string _xNikaDevice;
        private readonly string _xNikaDeviceID;

        public HttpRequest(HttpListenerRequest request, string entityBody)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            _text = entityBody;
            _requestUrl = request.Url;
            _requestMethod = ToHttpMethod(request.HttpMethod);
            _remoteEndPoint = request.RemoteEndPoint;
            _headerXRequestor = request.Headers["X-Requestor"];
            _xNikaDevice = request.Headers["X-Nika-Device"];
            _xNikaDeviceID = request.Headers["X-Nika-DeviceID"];

            _encoding = request.ContentEncoding.Equals(Encoding.Unicode)? Encoding.BigEndianUnicode : request.ContentEncoding;

            TimeReceived = DateTime.UtcNow;

            var allowedRemoteHostsForXRequestorConfig =
                ConfigurationManager.AppSettings["HttpTerminal.HttpRequest.AllowedRemoteHostsForXRequestor"];

            if (allowedRemoteHostsForXRequestorConfig != null)
            {
                _allowedRemoteHostsForXRequestor = allowedRemoteHostsForXRequestorConfig.Split(
                    new[] {';'}, StringSplitOptions.RemoveEmptyEntries).Select(s => s.Trim()).ToArray();
            }
        }

        public Uri RequestUrl
        {
            get { return _requestUrl; }
        }

        public HttpMethod Method
        {
            get { return _requestMethod; }
        }

        private static HttpMethod ToHttpMethod(string value)
        {
            switch (value)
            {
                case "GET":
                    return HttpMethod.Get;
                case "POST":
                    return HttpMethod.Post;
                case "DELETE":
                    return HttpMethod.Delete;
                default:
                    throw new InvalidOperationException("Unsupported http-method: " + value);
            }
        }

        public DateTime TimeReceived
        {
            get;
            private set;
        }

        public string Requestor
        {
            get
            {
                //Заголовок X-Requestor имеет смысл читать только для запросов, полученных VPN-канал с МТС

                var remoteEndPoint = _remoteEndPoint;
                if (remoteEndPoint == null || remoteEndPoint.Address == null)
                    return null;
                var ip = remoteEndPoint.Address.ToString();

                return
                    _allowedRemoteHostsForXRequestor.Any(
                        s => s.Equals(ip, StringComparison.OrdinalIgnoreCase))
                        ? _headerXRequestor
                        : null;
            }
        }


        public Encoding Encoding
        {
            get { return _encoding; }
        }

        public string Text
        {
            get
            {
                return _text;
            }
        }

        public string XNikaDevice
        {
            get { return _xNikaDevice; }
        }

        public string XNikaDeviceID
        {
            get { return _xNikaDeviceID; }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.Append("Incoming http request ");
            sb.AppendLine(RequestUrl.ToString());
            sb.Append("TimeReceived: ");
            sb.AppendLine(TimeReceived.ToString(CultureInfo.InvariantCulture));
            if (string.IsNullOrEmpty(Requestor))
            {
                sb.Append("Requestor: ");
                sb.AppendLine(Requestor);
            }
            sb.Append("Text: ");
            sb.AppendLine(Text);

            return sb.ToString();
        }
    }
}
