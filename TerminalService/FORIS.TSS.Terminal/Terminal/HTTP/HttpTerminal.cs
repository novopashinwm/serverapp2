﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using FORIS.TSS.Common.Http;
using FORIS.TSS.ServerApplication.Diagnostic;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Terminal.HTTP
{
	/// <summary> Терминал для приёма данных от устройств по протоколу HTTP </summary>
	[Serializable]
	public class HttpTerminal : _Terminal
	{
		private readonly bool _logRequests;
		private HttpReceiver _httpReceiver;

		private class HttpReceiver : BasicHttpReceiver
		{
			private readonly HttpTerminal _httpTerminal;

			public HttpReceiver(HttpTerminal httpTerminal, string[] httpPrefixes) : base(httpPrefixes)
			{
				_httpTerminal = httpTerminal;
			}

			protected override HttpResponse Process(HttpListenerRequest request, string entityBody)
			{
				return _httpTerminal.Process(request, entityBody);
			}
		}

		public HttpTerminal(ITerminalManager mgr)
			: base("HTTP", mgr)
		{
			Trace.TraceInformation("Create HTTP terminal");

			_logRequests = "true".Equals(ConfigurationManager.AppSettings["HttpTerminal._logRequests"], StringComparison.OrdinalIgnoreCase);

			Trace.TraceInformation("{0}._logRequests = {1}", this, _logRequests);
		}

		public HttpTerminal(string name, ITerminalManager mgr)
			: base(name, mgr)
		{
		}

		/// <summary>
		/// Инициализация терминала через список строк
		/// </summary>
		public override void Initialization(params string[] initStrings)
		{
			base.Initialization(initStrings);

			if (initStrings.Length < 2)
				return;

			var httpPrefixes = new string[initStrings.Length - 1];

			for (var i = 1; i != initStrings.Length; ++i)
				httpPrefixes[i - 1] = initStrings[i];

			if (!HttpListener.IsSupported)
			{
				Trace.TraceError("HttpTerminal: HttpListener class is not supported");
				return;
			}

			if (httpPrefixes.Length == 0)
			{
				Trace.TraceWarning("HttpTerminal: URI prefixes are not set");
				return;
			}

			_httpReceiver = new HttpReceiver(this, httpPrefixes);
			_httpReceiver.Start();
		}

		private HttpResponse Process(HttpListenerRequest request, string entityText)
		{
			var httpRequest = new HttpRequest(request, entityText);

			if (_logRequests)
				Trace.TraceInformation(
					"{0}.Process {1} {2}\n{3}",
					this, httpRequest.Method, httpRequest.RequestUrl, entityText);

			var device = itmManager.GetHttpDevice(httpRequest);
			if (device == null)
			{
				Trace.TraceInformation(
					"{0}: request {1} {2} result is 404 Not Found",
					this, httpRequest.Method, httpRequest.RequestUrl);

				return HttpResponse.NotFound();
			}

			IList<object> list;
			try
			{
				list = device.OnData(httpRequest);
			}
			catch (FormatException e)
			{
				Trace.TraceWarning("Error when processing incoming http request:\n{0},\nexception: {1}", httpRequest, e);
				return HttpResponse.BadRequest(e.Message);
			}
			catch (Exception e)
			{
				Trace.TraceError("Error when processing incoming http request:\n{0},\nexception: {1}", httpRequest, e);
				return HttpResponse.InternalServerError();
			}

			if (list == null)
				return HttpResponse.NotFound();

			var mobilUnits = new List<IMobilUnit>();

			var asid = httpRequest.Requestor;

			HttpResponse responseToSend = null;
			foreach (var item in list)
			{
				var httpResponse = item as HttpResponse;
				if (httpResponse != null)
				{
					if (responseToSend != null)
					{
						Trace.TraceWarning("{0}.Process - duplicate http response is ignored", this);
						continue;
					}

					responseToSend = httpResponse;
				}

				var mu = item as IMobilUnit;
				if (mu != null)
				{
					mu.Asid = asid;
					mu.TermID = ID;
					if (request.RemoteEndPoint != null)
						mu.IP = request.RemoteEndPoint;
					mobilUnits.Add(mu);
					Counters.Instance.Increment(CounterGroup.CountPerSecond, Counters.Instances.Http);
				}

				var notification = item as NotifyEventArgs;
				if (notification != null)
				{
					OnNotify(notification, false, true);
				}
			}

			if (responseToSend == null)
			{
				Trace.TraceWarning(
					"{0}.Process: no http response was sent, sending default 500 Internal Server Error, request was {1} {2}:\n{3}",
					this, httpRequest.Method, httpRequest.RequestUrl, entityText);
				responseToSend = HttpResponse.InternalServerError();
			}

			OnReceive(new ReceiveEventArgs(mobilUnits.ToArray()));

			return responseToSend;
		}

		protected override void Dispose(bool disposing)
		{
			if (_httpReceiver != null && _httpReceiver.IsListening)
			{
				Trace.TraceInformation("{0}: Stopping http listener", this);
				_httpReceiver.Stop();
			}

			base.Dispose(disposing);
		}
	}
}