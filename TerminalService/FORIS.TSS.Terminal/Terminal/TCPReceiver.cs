﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using FORIS.TSS.ServerApplication.Diagnostic;

namespace FORIS.TSS.Terminal.Terminal
{
	public class TCPReceiver : ITCPReceiver
	{
		[field: NonSerialized]
		private readonly TraceSwitch _traceSwitch = new TraceSwitch("Terminal", "TCP");

		private readonly TcpClient   _client;
		private readonly EndPoint    _remoteEndPoint;
		private readonly StateObject _stateObject;
		private object               _stateDataForDevice;
		public void CloseClient()
		{
			_client.Close();
		}
		public IPEndPoint LocalEndPoint { get; private set; }
		public TcpClient Client
		{
			get
			{
				return _client;
			}
		}
		public object Tag { get; set; }
		/// <summary> Информация об объекте слежения </summary>
		public IUnitInfo UnitInfo { get; set; }
		private DateTime _lastReceiveTime = DateTime.MinValue;
		public DateTime LastReceiveTime
		{
			get
			{
				return _lastReceiveTime;
			}
		}
		public IPEndPoint EndPoint
		{
			get
			{
				return _client.Client.RemoteEndPoint as IPEndPoint;
			}
		}
		/// <summary> Список сообщений полученных от устройства </summary>
		public IList Res { get; set; }
		/// <summary> Устройство </summary>
		public IDevice Device { get; set; }
		private long _receiveCounter;
		public long ReceiveCounter
		{
			get
			{
				return _receiveCounter;
			}
		}
		protected class StateObject
		{
			/// <summary> Client socket </summary>
			public Socket WorkSocket;
			/// <summary> Size of receive buffer </summary>
			public const int BufferSize = 2048;
			/// <summary> Receive buffer </summary>
			public byte[] Buffer = new byte[BufferSize];
			/// <summary> Received data string </summary>
			public StringBuilder Sb = new StringBuilder();
			public byte[] CollectedBuffer;
			public List<byte> Data = new List<byte>();
		}
		public TCPReceiver(TcpClient client, IPEndPoint localEndpoint = null)
		{
			if (client == null)
				throw new ArgumentNullException("client");

			_client = client;
			LocalEndPoint = localEndpoint;
			_remoteEndPoint = client.Client != null ? client.Client.RemoteEndPoint : null;
			_stateObject = new StateObject { WorkSocket = _client.Client };
			_lastReceiveTime = DateTime.UtcNow;

			#if DEBUG
				Trace.WriteLine(GetType() + " Created. Client: " + _client.Client.RemoteEndPoint);
			#endif
		}
		public void PrepareToProcessData()
		{
			OnData += Receiver_OnData;
		}
		public void Start()
		{
			PrepareToProcessData();
			_client.Client.BeginReceive(_stateObject.Buffer, 0, StateObject.BufferSize, 0,
										 ReceiveData_Callback, _stateObject);
		}
		protected virtual void PostProcessReceiverStoreToStateMessage()
		{
			var listToCleanup = Res;

			if (listToCleanup == null || listToCleanup.Count <= 0) return;

			for (int i = 0; i < listToCleanup.Count;)
			{
				object o = listToCleanup[i];
				var message = o as ReceiverStoreToStateMessage;
				if (message != null)
				{
					_stateDataForDevice = message.StateData;
					listToCleanup.RemoveAt(i);
				}
				else
				{
					++i;
				}
			}
		}
		protected virtual void PostProcessing()
		{
			PostProcessReceiverStoreToStateMessage();
		}
		protected virtual void Receiver_OnData(object sender, EventArgs e)
		{
			if (Device == null)
				return;

			byte[] receivedData = ((TcpOnDataEventArgs)e).Data;
			// обрабатываем данные
			byte[] bufferRest;
			byte[] totalData;
			// Если что-то оставалось от предыдущей посылки, то все складываем
			if (_stateObject.CollectedBuffer != null && _stateObject.CollectedBuffer.Length > 0)
			{
				totalData = new byte[receivedData.Length + _stateObject.CollectedBuffer.Length];
				Array.Copy(_stateObject.CollectedBuffer, totalData, _stateObject.CollectedBuffer.Length);
				Array.Copy(receivedData, 0, totalData, _stateObject.CollectedBuffer.Length, receivedData.Length);
			}
			else
			{
				totalData = receivedData;
			}

			var sw = new Stopwatch();
			sw.Start();
			Res = Device.OnData(totalData, totalData.Length, _stateDataForDevice, out bufferRest);

			if (bufferRest != null && 10 * 1024 < bufferRest.Length)
			{
				Trace.TraceWarning("{0}'s buffer rest is too big: {1} bytes: {2}", Device.GetType().Name, bufferRest.Length, bufferRest);
			}

			sw.Stop();
			if (1000 < sw.ElapsedMilliseconds)
			{
				Trace.TraceWarning("{0}.OnData is too slow, action has taken {2}, data ({3}) is {1}",
					Device.GetType(), BitConverter.ToString(totalData), sw.Elapsed, totalData.Length);
			}

			PostProcessing();

			if (bufferRest != null && bufferRest.Length > 0)
			{
				_stateObject.CollectedBuffer = bufferRest;
			}
			else
			{
				_stateObject.CollectedBuffer = null;
			}

			if (Res == null && _stateObject.CollectedBuffer == null)
				CloseClient();

			var onOnProcessedData = OnProcessedData;
			if (onOnProcessedData != null)
			{
				var args = new TcpOnProcessedDataEventArgs
				{
					Res     = Res,
					Address = _remoteEndPoint as IPEndPoint
				};

				foreach (Delegate dlgt in onOnProcessedData.GetInvocationList())
				{
					((EventHandler)dlgt).Invoke(this, args);
				}
			}
		}
		/// <summary> Метод асинхронно вызывает подписчиков на событие OnData, а также читает оставшиеся данные из сокета </summary>
		/// <param name="data"> Принятые данные в необработанном виде </param>
		public virtual void ProcessReceivedData(byte[] data)
		{
			Socket client = _stateObject.WorkSocket;
			try
			{
				if (OnData != null)
				{
					TcpOnDataEventArgs args = new TcpOnDataEventArgs
					{
						Address = _client.Client.RemoteEndPoint as IPEndPoint,
						Data    = data
					};

					foreach (Delegate dlgt in OnData.GetInvocationList())
					{
						((EventHandler)dlgt).Invoke(this, args);
					}
					_receiveCounter++;
				}
			}
			catch (Exception ex)
			{
				Trace.TraceError(GetType() + ".ProcessReceivedData error: \r\n {0} \r\n Data: \r\n{1}",
					ex,
					BitConverter.ToString(data));
				//StringHelper.SmartFormatBytes(data)));
				CloseClient();
			}

			// Get the rest of the data.
			if (!disposed && client.Connected)
			{
				client.BeginReceive(_stateObject.Buffer, 0, StateObject.BufferSize, 0, ReceiveData_Callback, _stateObject);
			}
		}
		protected virtual void ReceiveData_Callback(IAsyncResult ar)
		{
			// Установили соединение увеличиваем оба счетчика
			Counters.Instance.Increment(CounterGroup.Count,          Counters.Instances.Tcp);
			Counters.Instance.Increment(CounterGroup.CountPerSecond, Counters.Instances.Tcp);
			try
			{
				// Retrieve the state object and the client socket 
				// from the asynchronous state object.
				StateObject state = (StateObject)ar.AsyncState;
				Socket client = state.WorkSocket;

				// Read data from the remote device.
				int bytesRead = 0;
				if (client.Connected)
					bytesRead = client.EndReceive(ar);

				if (bytesRead > 0)
				{
					byte[] d = new byte[bytesRead];
					for (int n = 0; n < bytesRead; n++)
					{
						d[n] = state.Buffer[n];
					}

					_lastReceiveTime = DateTime.UtcNow;
					ProcessReceivedData(d); //"Get the rest of the data" перенесено в ProcessReceivedData
				}

				Counters.Instance.Decrement(CounterGroup.Count, Counters.Instances.Tcp);
			}
			catch (SocketException)
			{
				Counters.Instance.Decrement(CounterGroup.Count, Counters.Instances.Tcp);
				//TODO: обработать потерю связи, например, закрыть сокет
			}
			catch (Exception e)
			{
				Counters.Instance.Decrement(CounterGroup.Count, Counters.Instances.Tcp);
				Trace.TraceError(e.ToString());
			}
		}
		public virtual event EventHandler OnData;
		public virtual event EventHandler OnProcessedData;
		protected void OnData_AsyncCallback(IAsyncResult result)
		{
			try
			{
				var eventHandler = result.AsyncState as EventHandler;
				if (eventHandler != null)
					eventHandler.EndInvoke(result);
			}
			catch (Exception ex)
			{
				Trace.WriteLine(GetType() + ".OnData_AsyncCallback: " + ex);
			}
		}
		protected void Send(byte[] data)
		{
			Send(data, data.Length);
		}
		public bool Send(byte[] data, int length)
		{

			var tcpClient = _client;
			if (tcpClient == null)
			{
				Trace.WriteLine("Unexpected null value in " + Environment.StackTrace);
				return false;
			}

			if (!tcpClient.Connected)
				return false;

			try
			{
				NetworkStream stream = tcpClient.GetStream();

				// ReSharper disable once ConditionIsAlwaysTrueOrFalse
				if (stream == null)
				{
					Trace.TraceError("Unexpected tcpClient.GetStream() null return value");
					return false;
				}

				stream.Write(data, 0, length);
				stream.Flush();

				CalcOutBitrate(length);

				if (_traceSwitch != null)
					Trace.WriteLineIf(_traceSwitch.TraceInfo,
						GetType() + ": Sending text: " + Encoding.ASCII.GetString(data, 0, length) +
						", binary: " + BitConverter.ToString(data));

				return true;
			}
			catch (SocketException socketException)
			{
				if (socketException.ErrorCode == 10053)
					return false; //TODO: probably, cleanup
				Trace.TraceError("Unable to send data to device, SocketException Code = {0}: {1}",
								 socketException.ErrorCode, socketException);
				return false;
			}
			catch (IOException ex)
			{
				var socketException = ex.InnerException as SocketException;
				if (socketException != null)
				{
					if (socketException.ErrorCode == 10053)
						return false; //TODO: probably, cleanup

					Trace.TraceError("Unable to send data to device, SocketException Code = {0}: {1}",
									 socketException.ErrorCode, socketException);
				}
				else
				{
					Trace.TraceError("Unable to send data to device: {0}", ex);
				}
				return false;
			}
			catch (Exception ex)
			{
				Trace.TraceError("Unable to send data to device {0}", ex);
				return false;
			}
		}
		#region Расчет битрейта
		private int _totalBytesReceived;
		private DateTime _dtTmp = DateTime.MinValue;
		[Conditional("DEBUG")]
		protected void CalcInBitrate(int bytesReceived)
		{
			if (_dtTmp == DateTime.MinValue)
				_dtTmp = DateTime.Now;

			_totalBytesReceived += (bytesReceived + 20);

			double totalSeconds = (DateTime.Now - _dtTmp).TotalSeconds;
			if (totalSeconds > 0)
			{
				Trace.WriteLine(GetType() + " Input bitrate: " +
								((double)_totalBytesReceived * 8) / totalSeconds + " bits per second");
			}
		}
		private int _totalBytesSent;
		private DateTime _dtTmp1 = DateTime.MinValue;
		[Conditional("DEBUG")]
		protected void CalcOutBitrate(int bytesSent)
		{
			if (_dtTmp1 == DateTime.MinValue)
				_dtTmp1 = DateTime.Now;

			_totalBytesSent += (bytesSent + 20);

			double totalSeconds = (DateTime.Now - _dtTmp1).TotalSeconds;
			if (totalSeconds > 0)
			{
				Trace.WriteLine(GetType() + " Output bitrate: " +
								((double)_totalBytesSent * 8) / totalSeconds + " bits per second");
			}
		}
		#endregion Расчет битрейта
		protected bool disposed;
		public bool Disposed
		{
			get
			{
				return disposed;
			}
		}

		#region IDisposable Members

		public void Dispose()
		{
			if (!disposed)
			{
				disposed = true;
			}
		}

		#endregion
	}
}