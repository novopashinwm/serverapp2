﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Xml;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.ServerApplication.Diagnostic;
using FORIS.TSS.Terminal.Messaging;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Terminal
{
	/// <summary> Терминал для работы с контролером через TCP </summary>
	[Serializable]
	public class TCPTerminal : _Terminal
	{
		[field: NonSerialized] readonly TraceSwitch _tsTerm = new TraceSwitch("Terminal", "TCP");

		private TimeSpan _maxIdleTime = TimeSpan.FromMinutes(5);
		private int      _defaultPort;
		private int[]    _ports;
		private bool     _closeReceiverWhenDeviceIsUnknown = true;

		/// <summary> IP адрес сервера </summary>
		private IPAddress _address;

		public Timer CollectTimer
		{
			get { return _collectTimer; }
		}

		readonly IDictionary<EndPoint, ITCPReceiver> _receiversList = new Dictionary<EndPoint, ITCPReceiver>();

		/// <summary> Терминал для работы с контролером через TCP </summary>
		public TCPTerminal(ITerminalManager itm)
			: this("TCPTerminal", itm)
		{
		}
		/// <summary> Терминал для работы с контролером через TCP </summary>
		public TCPTerminal(string name, ITerminalManager itm)
			: base(name, itm)
		{
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
				Stop();
			base.Dispose(disposing);
		}
		/// <summary> Инициализация терминала через список строк </summary>
		/// <param name="initStrings">
		/// <p>initSrings[0] - название терминала (не используется),</p> 
		/// <p>initSrings[1] - IP адрес сервера,</p> 
		/// <p>initSrings[2] - порт сервера,</p>
		/// <p>initSrings[3] - тип очереди (0-нет, 1-MSMQ, 2-программная)</p>
		/// </param>
		public override void Initialization(params string[] initStrings)
		{
			try
			{
				id = int.Parse(initStrings[0]);

				_address = IPAddress.Parse(initStrings[2]);
				var ports = ParsePortSpecification(initStrings[3]);
				// ReSharper disable PossibleMultipleEnumeration
				_defaultPort = ports.First();
				_ports = ports.Distinct().ToArray();
				// ReSharper restore PossibleMultipleEnumeration

				Trace.TraceInformation("{0}: ports - got {1}, parsed to {2}", this, initStrings[3], _ports.Join(","));

				if (initStrings.Length > 4) queueType = (QueueType)int.Parse(initStrings[4]);
				if (initStrings.Length > 5) queueSize = int.Parse(initStrings[5]);
				if (initStrings.Length > 6) timerInterval = int.Parse(initStrings[6]);
				if (initStrings.Length > 7) _closeReceiverWhenDeviceIsUnknown = XmlConvert.ToBoolean(initStrings[7]);

				_maxIdleTime = ConfigHelper.GetAppSettingAsTimeSpan("TCPTerminal.MaxIdleTime") ?? TimeSpan.FromMinutes(5);

				if (queueType == QueueType.Program)
				{
					QMsg = Queue.Synchronized(new Queue(queueSize));
					timer = new Timer(ListenerTimerCallBack, null, timerInterval, 0);
				}

				Start();
			}
			catch (FormatException ex)
			{
				throw new FormatException(
					"Неверные аргументы инициализации. Один из параметров имеет неверный формат.", ex);
			}
		}
		private IEnumerable<int> ParsePortSpecification(string initString)
		{
			var parts = initString.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries);
			foreach (var part in parts)
			{
				var subParts = part.Split(new[] {'-'}, StringSplitOptions.RemoveEmptyEntries);
				if (subParts.Length == 0)
					continue;
				int first;
				if (!int.TryParse(subParts[0].Trim(), out first))
					continue;
				if (subParts.Length == 1)
				{
					yield return first;
					continue;
				}

				int second;
				if (!int.TryParse(subParts[1].Trim(), out second))
					continue;
				if (second < first)
					continue;

				for (var port = first; port <= second; ++port)
					yield return port;
			}
		}

		private Timer _collectTimer;
		protected void Start()
		{
			_tcpListeners = _ports
				.OrderBy(port => port)
				.Select(port => new TcpListener(new IPEndPoint(_address, port)))
				.ToArray();

			foreach (var tcpListener in _tcpListeners)
			{
				try
				{
					tcpListener.Start();
					$"'{(IPEndPoint)tcpListener.LocalEndpoint}' LISTENING"
						.CallTraceInformation();
					AcceptNextTcpClient(tcpListener);
				}
				catch (Exception ex)
				{
					$"'{(IPEndPoint)tcpListener.LocalEndpoint}' NOT LISTENING"
						.WithException(ex, true)
						.CallTraceError();
				}
			}

			_collectTimer = new Timer(OnCollect, null, 0, 60000);
		}
		private void DisposeAllReceivers()
		{
			lock (_receiversList)
			{
				foreach (KeyValuePair<EndPoint, ITCPReceiver> pair in _receiversList)
				{
					Counters.Instance.Decrement(CounterGroup.Count, Counters.Instances.TcpConnections);
					ITCPReceiver r = pair.Value;
					try
					{
						r.CloseClient();
						r.Dispose();
					}
					catch (Exception ex)
					{
						Exception exception = ex;
						while (exception != null)
						{
							Trace.WriteLine(ex.ToString());
							exception = exception.InnerException;
						}
					}
				}
				_receiversList.Clear();
				Trace.TraceInformation($@"{this.GetTypeName()}({Name}): All receivers have been disconnected");
			}
		}
		private void OnCollect(object o)
		{
			try
			{
				lock (_receiversList)
				{
					IDictionary<EndPoint, ITCPReceiver> deadReceivers = new Dictionary<EndPoint, ITCPReceiver>();

					foreach (KeyValuePair<EndPoint, ITCPReceiver> pair in _receiversList)
					{
						ITCPReceiver r = pair.Value;
						if (IsAlive(r))
							continue;
						deadReceivers[pair.Key] = pair.Value;
					}

					Trace.TraceInformation($@"{this.GetTypeName()}({Name}): Dead receivers to be closed: {deadReceivers.Count}/{_receiversList.Count}");

					foreach (KeyValuePair<EndPoint, ITCPReceiver> pair in deadReceivers)
					{
						_receiversList.Remove(pair);
						Counters.Instance.Decrement(CounterGroup.Count, Counters.Instances.TcpConnections);
						ITCPReceiver tcpReceiver = pair.Value;
						if (tcpReceiver == null)
							continue;
						try
						{
							tcpReceiver.CloseClient();
							tcpReceiver.Dispose();
						} 
						catch (Exception ex)
						{
							Trace.TraceError($@"{this.GetTypeName()}({Name}): Unable to close dead receiver: {ex}");
						}
					}
				}
			}
			catch (ThreadAbortException)
			{
			}
			catch (Exception ex)
			{
				Trace.TraceError($@"{this.GetTypeName()}({Name}): OnCollect error: {ex}");
			}
		}
		private bool IsAlive(ITCPReceiver r)
		{
			if (r == null)
				return false;
			var client = r.Client;
			if (client == null)
				return false;

			var lastReceiveTime = r.LastReceiveTime;
			
			var maxIdleTime = _maxIdleTime;
			var device = r.Device;
			if (device != null && device.MaxIdleTime != null)
				maxIdleTime = device.MaxIdleTime.Value;

			if (maxIdleTime < DateTime.UtcNow - lastReceiveTime)
				return false;

			return true;
		}

		private TcpListener[] _tcpListeners;
		private void TcpClientAccepted(IAsyncResult ar)
		{
			var tcpListener = (TcpListener)ar.AsyncState;

			try
			{
				var tcpClient = tcpListener.EndAcceptTcpClient(ar);
				var receiver = new TCPReceiver(tcpClient, tcpListener.LocalEndpoint as IPEndPoint);
				RegisterReceiver(receiver);
				receiver.OnData += Receiver_OnFirstData;
				receiver.Start();
			}
			catch (ObjectDisposedException)
			{
				return;
			}
			catch (Exception ex)
			{
				Trace.TraceError(GetType() + ".ListenerThread(): " + ex);
			}

			if (_tcpListeners == null)
				return;

			AcceptNextTcpClient(tcpListener);
		}

		private void AcceptNextTcpClient(TcpListener tcpListener)
		{
			try
			{
				tcpListener.BeginAcceptTcpClient(TcpClientAccepted, tcpListener);
			}
			catch (Exception ex)
			{
				Trace.TraceInformation("{0}", ex);
			}
		}

		/// <summary> Метод для определения обрабатывающего ресивера </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void Receiver_OnFirstData(object sender, EventArgs e)
		{
			var receiver = sender as ITCPReceiver;
			if (!(e is TcpOnDataEventArgs) || receiver == null)
				return;

			try
			{
				byte[] data = ((TcpOnDataEventArgs) e).Data;
				
				
				var dev = itmManager.GetDevice(data);

				if (dev == null)
				{
					if (_closeReceiverWhenDeviceIsUnknown)
					{
						try
						{
							receiver.CloseClient();
						}
						catch (Exception exception)
						{
							Trace.TraceError("Error when closing receiver: {0}", exception);
						}
					}
					return;
				}

				//Отписываемся от этого события - оно только для первых данных
				receiver.OnData -= Receiver_OnFirstData;

				if (receiver.Disposed) return;

				receiver.Device = dev;
				receiver.OnProcessedData += Receiver_OnProcessedData;
			}
			catch (Exception ex)
			{
				Trace.TraceError("TCPTerminal Receiver_OnFirstData: {0}", ex);
			}
		}

		private void RegisterReceiver(ITCPReceiver receiver)
		{
			/*
						Если в списке приемников уже есть приемник для этой удаленной точки, то закрываем его.
						Получаем приемник от device-а, добавляем его в список приемников и передаем ему полученную информацию.
					*/

			//TODO: убрать блокировку
			lock (_receiversList)
			{
				if (_receiversList.ContainsKey(receiver.Client.Client.RemoteEndPoint))
				{
					var oldReceiver = _receiversList[receiver.Client.Client.RemoteEndPoint];
					if (oldReceiver != receiver)
					{
						oldReceiver.CloseClient();
						oldReceiver.Dispose();
						_receiversList.Remove(receiver.Client.Client.RemoteEndPoint);
						Counters.Instance.Decrement(CounterGroup.Count, Counters.Instances.TcpConnections);
					}
				}

				_receiversList[receiver.Client.Client.RemoteEndPoint] = receiver;
				Counters.Instance.Increment(CounterGroup.Count, Counters.Instances.TcpConnections);
			}
		}

		#region OnReceiveErrors

		protected Timer ReceiveErrorsRepairTimer;

		protected void CheckReceiveErrorsRepair(object o)
		{
			try
			{
				var state = (ReceiveErrorsRepairTimerState)o;

				if (DateTime.UtcNow - state.TimerStarted < ReceiveErrorsRepairTimeout)
				{
					OnReceive(state.Args);
					//Если в этот раз все прошло хорошо, то восстанавливаем работу слушателей
					Trace.TraceInformation("TCPTerminal repaired!");
				}
				else
				{
					Trace.TraceWarning("Timeout exceeded to repair");
				}
				DisposeRepairTimer();
				Start();
			}
			catch (Exception ex)
			{
				Trace.TraceError("Unable to repair TCPTerminal: " + ex);
				DisposeRepairTimer();
				ReceiveErrorsRepairTimer = new Timer(
					CheckReceiveErrorsRepair,
					o,
					RepairWaitTime,
					TimeSpan.FromMilliseconds(-1));

			}
		}

		private void DisposeRepairTimer()
		{
			try
			{
				lock (ReceiveErrorsRepairTimerLock)
				{
					if (ReceiveErrorsRepairTimer != null)
					{
						ReceiveErrorsRepairTimer.Dispose();
						ReceiveErrorsRepairTimer = null;
					}
				}
			}
			catch (Exception ex)
			{
				Trace.TraceError("Unable to dispose ReceiveErrorsRepairTimer: " + ex);
			}
		}

		class ReceiveErrorsRepairTimerState
		{
			public DateTime TimerStarted;
			public ReceiveEventArgs Args;
		}

		/// <summary> Обработка ошибок записи в БД, принятой терминалом информации </summary>
		protected override void OnReceiveErrors(Exception ex, ReceiveEventArgs args)
		{
			try
			{
				lock (ReceiveErrorsRepairTimerLock)
				{
					++ContinuousErrorCounter;
					var errorsCount = ContinuousErrorCounter;
					const int errorCountThreshold = 7;
					if (errorsCount == errorCountThreshold && ReceiveErrorsRepairTimer == null)
					{
						Trace.TraceError("TCPTerminal catch " + errorsCount + " Continuous Errors and stop receiving messages!");

						//Закрываем socket и все ресиверы
						try
						{
							Stop();
						}
						catch (Exception exception)
						{
							Trace.TraceError("Unable to stop TCPTerminal: " + exception);
						}
						//Начинаем проверку восстановления работоспособности
						ReceiveErrorsRepairTimer = new Timer(
							CheckReceiveErrorsRepair, 
							new ReceiveErrorsRepairTimerState
							{
								TimerStarted = DateTime.UtcNow,
								Args         = args,
							},
							RepairWaitTime, 
							TimeSpan.FromMilliseconds(-1));
					}
					else
					{
						//Пока лезут ошибки, откладываем запуск проверки TCPTerminal'а
						//Если этого не делать, возникает следующая ситуация:
						//1. Данные не удается записать из-за перегрузки сервера БД
						//2. Несмотря на то, что слушатель закрывается, в ThreadPool'е по-прежнему висят вызовы сохранения данных
						//от уже разорванных соединений
						//3. Как только БД слегка освобождается и таймеру удается успешно записать данные БД,
						//таймер открывает входящий сокет и все устройства одновременно подключаются к службе и 
						//создают новую нагрузку на базу, в то время как запросы от предыдущего сокета еще висят
						//4. Всё снова повторяется и база никогда не освободится от нагрузки, помогает только 
						//принудительное закрытие процесса TerminalService, после которого SQL Server прибивает 
						//подключения из-за закрытия связанных с процессом TCP/IP соединений.
						if (ReceiveErrorsRepairTimer != null && errorsCount > errorCountThreshold)
						{
							ReceiveErrorsRepairTimer.Change(RepairWaitTime, TimeSpan.FromMilliseconds(-1));
							Trace.TraceWarning("Repair is delayed for " + RepairWaitTime);
						}
					}
				}
			}
			catch(Exception e)
			{
				Trace.TraceError("TCPTerminal OnReceiveErrors: " + e);
			}
		}


		#endregion OnReceiveErrors

		void Receiver_OnProcessedData(object sender, EventArgs e)
		{
			
			if (!(e is TcpOnProcessedDataEventArgs))
				return;

			IList aRes = ((TcpOnProcessedDataEventArgs)e).Res;
			ITCPReceiver receiver = sender as ITCPReceiver;

			if (aRes == null || receiver== null)
				return;

			List<IMobilUnit> mobilUnits = null;

			foreach (object o in aRes)
			{
				if (o is IPositionMessage || o is IMobilUnit)
				{
					IPositionMessage msg = o as IPositionMessage;
					IMobilUnit mu;
					if (msg != null)
					{
						mu = msg.MobilUnit;
					}
					else mu = (IMobilUnit)o;
					mu.TermID = id;
					mu.IP = ((TcpOnProcessedDataEventArgs)e).Address;
					mu.Media = Media.TCP;
					if (receiver.LocalEndPoint != null && receiver.LocalEndPoint.Port != _defaultPort)
						mu.Port = receiver.LocalEndPoint.Port;

					if (!mu.Properties.ContainsKey(DeviceProperty.Protocol) ||
						string.IsNullOrEmpty(mu.Properties[DeviceProperty.Protocol]))
					{
						var protocol = receiver.Device?.GetType().Name;

						if (protocol != null)
							mu.Properties[DeviceProperty.Protocol] = protocol;
					}

					if (mobilUnits == null)
						mobilUnits = new List<IMobilUnit>(1);

					mobilUnits.Add(mu);

					receiver.UnitInfo = mu.UnitInfo;
					Counters.Instance.Increment(CounterGroup.CountPerSecond, Counters.Instances.Tcp);
				}
				var notifyEventArgs = o as NotifyEventArgs;
				if (notifyEventArgs != null)
				{
					IUnitInfo ui = null;
					string deviceID = null;
					NotifyEventArgs arg = notifyEventArgs;
					int confirm = 0;
					object[] par = arg.Tag;
					switch (arg.Event)
					{
						case TerminalEventMessage.NE_MESSAGE:
							ui = par[0] as IUnitInfo;
							if (ui != null)
							{
								if (ui.DeviceID != null)
								{
									deviceID = ui.DeviceID;
								}
							}
							Trace.WriteLineIf(
								_tsTerm.TraceWarning,
								string.Format("Message received from {0}\r\n\t{1}", ui, arg.Message),
								_tsTerm.Description);
							if (par.Length > 3)
								confirm = (int)par[3];
							OnNotify(arg, confirm > 0, true);
							break;

						case TerminalEventMessage.NE_ALARM:
							ui = par[0] as IUnitInfo;
							if (ui != null)
								ui = (IUnitInfo)ui.Clone();
							Trace.WriteLineIf(
								_tsTerm.TraceWarning,
								string.Format("Alarm received from {0}", ui),
								_tsTerm.Description);
							if (par.Length > 3)
								confirm = (int)par[3];
							OnNotify(arg, confirm > 0, true);
							break;

						case TerminalEventMessage.NE_CONFIRM:
							OnNotify(arg, false, true);
							break;

						default:
							OnNotify(arg);
							break;
					}
					if (ui != null)
					{
						ui.DeviceID = deviceID;

						if (deviceID != null &&
							(receiver.UnitInfo == null || 
							 receiver.UnitInfo.DeviceID == null ||
							 receiver.UnitInfo.DeviceID != ui.DeviceID))
							Trace.TraceInformation("Device ID {0} is resolved for IP {1} have got device id", deviceID, receiver.EndPoint.Address);

						receiver.UnitInfo = ui;
					}
				}
				if (o is ConfirmPacket)
				{
					var cfrm = o as ConfirmPacket;
					receiver.Send(cfrm.Data, cfrm.Data.Length);

					//Заполняем UnitInfo. В случае если это первый пакет
					if ((receiver.UnitInfo==null || receiver.UnitInfo.ID == 0) && cfrm.UnitInfo != null)
					{
						itmManager.FillUnitInfo(cfrm.UnitInfo);
						receiver.UnitInfo = cfrm.UnitInfo;
					}
				}
			}

			if (mobilUnits != null)
			{
				switch (queueType)
				{
					case QueueType.Program:
						foreach (var mu in mobilUnits)
							AddToQueryUDP(mu);
						break;
					default:
						OnReceive(new ReceiveEventArgs(mobilUnits.ToArray()));
						break;
				}
			}
		}

		#region AddToQuery(IMobilUnit mu)
		protected Queue QMsg;
		private static readonly TimeSpan ReceiveErrorsRepairTimeout = TimeSpan.FromMinutes(2);

		/// <summary>
		/// Время, которое дается системе на восстановление после сбоя.
		/// Должно коррелировать с таймаутом SQL-соединений
		/// </summary>
		private static TimeSpan RepairWaitTime
		{
			get
			{
				var s = ConfigurationManager.AppSettings["TCPTerminal.RepairWaitTime"];
				TimeSpan result;
				if (TimeSpan.TryParse(s, out result))
					return result;
				return TimeSpan.FromMinutes(1);
			}
		}

		protected void AddToQueryUDP(IMobilUnit mu)
		{
			try
			{
				QMsg.Enqueue(mu);
				if (QMsg.Count >= queueSize) SendReceivedMessage();
			}
			catch (Exception ex)
			{
				Trace.TraceError(ex.ToString());
			}
		}
		#endregion // AddToQuery(IMobilUnit mu)
		#region ListenerTimerCallBack(object o)
		private void ListenerTimerCallBack(object syncRoot)
		{
			SendReceivedMessage();
		}
		#endregion // ListenerTimerCallBack(object o)

		#region SendReceivedMessage()
		protected void SendReceivedMessage()
		{
			lock (QMsg.SyncRoot)
			{
				try
				{
					if (QMsg.Count == 0) return;

					Trace.WriteLineIf(_tsTerm.TraceVerbose, "SendReceivedMessage ... " + QMsg.Count,
									  _tsTerm.Description);
					IMobilUnit[] mus = new IMobilUnit[QMsg.Count];
					QMsg.CopyTo(mus, 0);
					OnReceive(new ReceiveEventArgs(mus));
					QMsg.Clear();
				}
				catch (Exception ex)
				{
					Trace.TraceError(ex.ToString());
				}
				finally
				{
					timer.Change(timerInterval, 0);
				}
			}
		}
		#endregion SendReceivedMessage()
		
		private void Stop()
		{
			var tcpListeners = _tcpListeners;
			if (tcpListeners != null)
			{
				foreach (var tcpListener in tcpListeners)
				{
					try
					{
						tcpListener.Stop();
					}
					catch (SocketException ex)
					{
						Trace.TraceError("Error stopping tcpListener with error code " + ex.ErrorCode + " " + ex);
					}
				}
				_tcpListeners = null;
			}

			var collectTimeCopy = _collectTimer;
			if (collectTimeCopy != null)
			{
				collectTimeCopy.Dispose();
				_collectTimer = null;
			}

			DisposeAllReceivers();
		}

		public override bool SendCommand(IStdCommand cmd)
		{
			if (cmd == null)
				throw new ArgumentNullException("cmd");

			IUnitInfo ui = cmd.Target;
			if (ui == null)
				throw new ArgumentException("cmd.Target cannot be null");

			var uiID = ui.ID;
			var uiDeviceID = ui.DeviceID;

			var receivers = new List<ITCPReceiver>();

			//Вызов receiver.Send блокирует поток, поэтому вначале ищем доступные ресиверы, потом отправляем
			lock (_receiversList)
			{
				foreach (KeyValuePair<EndPoint, ITCPReceiver> pair in _receiversList)
				{
					ITCPReceiver receiver = pair.Value;

					if (receiver.UnitInfo == null || receiver.UnitInfo.DeviceID == null)
						continue;

					if ((uiID != 0 && uiID == receiver.UnitInfo.ID) ||
						(uiDeviceID != null && uiDeviceID.SequenceEqual(receiver.UnitInfo.DeviceID)))
					{
						receivers.Add(receiver);
					}
				}
			}

			if (receivers.Count == 0)
				return false;

			IDevice dev = itmManager.GetDevice(ui);

			if (dev == null)
			{
				Trace.TraceWarning("Unable to find Device for IUnitInfo " + ui);
				return false;
			}

			byte[] data = dev.GetCmd(cmd);
			if (data == null || data.Length == 0)
			{
				Trace.TraceWarning("Empty bytes array for command " + cmd);
				return false;
			}
			
			foreach (var receiver in receivers)
			{
				if (receiver.Send(data, data.Length))
				{
					if (cmd.Sender.OperatorId != (int) ObjectID.Unknown)
					{
						OnNotify(
							new NotifyEventArgs(
								TerminalEventMessage.NE_MESSAGE,
								cmd.Sender,
								Severity.Lvl3,
								NotifyCategory.Terminal,
								"",
								null,
								cmd.Target,
								-1,
								""
							)
						);
					}
					return true;
				}
			}

			return false;
		}
	}
}