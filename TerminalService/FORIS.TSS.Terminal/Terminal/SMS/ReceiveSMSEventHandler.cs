﻿namespace FORIS.TSS.Terminal.SMS
{
	/// <summary>
	/// event with received sms
	/// </summary>
	public delegate void ReceiveSMSEventHandler(object sender, ReceiveSMSEventArgs args);
}