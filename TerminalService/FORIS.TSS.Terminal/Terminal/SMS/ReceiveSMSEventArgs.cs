﻿using System;

namespace FORIS.TSS.Terminal.SMS
{
	public class ReceiveSMSEventArgs : EventArgs
	{
		/// <summary>
		/// sender phone
		/// </summary>
		public readonly string target;
		/// <summary>
		/// received as text
		/// </summary>
		public readonly string text;
		/// <summary>
		/// received as data. not alphabet conversion
		/// </summary>
		public readonly byte[] data;

		public ReceiveSMSEventArgs(string _target, string _text, byte[] _data)
		{
			target = _target;
			text   = _text;
			data   = _data;
		}
	}
}