﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Communication;
using FORIS.TSS.IO;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using CriticalSection = System.Object;
using PositionData = FORIS.TSS.MobilUnit.Unit.MobilUnit;

namespace FORIS.TSS.Terminal.SMS
{
	/// <summary>
	/// terminal to communicate with controller via SMS
	/// mostly from MIREO code
	/// Info: need optimization
	/// </summary>
	public class SMSTerminal : _Terminal
	{
		/// <summary>
		/// SMS gateway link
		/// </summary>
		SiemensSMSGateway link;

		/// <summary>
		/// sms ask pos command
		/// </summary>
		const string MJ2016_ASK_POSITION_STRING = "$U:Q0U!";

		const string MJAlarm = "alarm";
		const string MJAlarmStop = "konec alarmu";

		const string TextPar = "text";

		/// <summary>
		/// cs objects
		/// </summary>
		CriticalSection obj_cs = new CriticalSection();
		CriticalSection r_cs = new CriticalSection();
		CriticalSection w_cs = new CriticalSection();

		/// <summary>
		/// stop flag
		/// </summary>
		volatile bool should_stop;

		/// <summary>
		/// types of SMS-messages
		/// </summary>
		private enum SMSType
		{
			Unknown = -1,
			RPVSimple = 0,
			RPVEnchanced = 1,
			MultiPosition = 2,

			Alarm = 3,
			AlarmStop = 4,
		}

		TraceSwitch tsTerm = new TraceSwitch("Terminal", "SMS");

		/// <summary>
		/// get type of SMS-message
		/// </summary>
		/// <param name="sMessage">SMS-message text</param>
		/// <returns>SMSType value</returns>
		SMSType GetSMSType(string sMessage)
		{
			if (sMessage.ToLower().IndexOf(MJAlarmStop) != -1) return SMSType.AlarmStop;
			if (sMessage.ToLower().IndexOf(MJAlarm) != -1) return SMSType.Alarm;

			if (sMessage.Length == 160 && sMessage.IndexOf("$U2", 0, 3) == 0) return SMSType.MultiPosition;

			if (sMessage.Length > 72 && sMessage.IndexOf(">RPV", 0, 4) == 0 &&
				sMessage.IndexOf("<", sMessage.Length - 1, 1) != -1)
			{
				if (sMessage.Length == 73) return SMSType.RPVSimple;
				if (sMessage.Length == 105) return SMSType.RPVEnchanced;
			}
			return SMSType.Unknown;
		}

		/// <summary>
		/// convert char in ASCII to byte in ETSI
		/// </summary>
		/// <param name="c">char in ASCII encoding</param>
		/// <returns>byte in ETSI encoding</returns>
		byte AsciiToEtsi(char c)
		{
			if (c >= '0' && c <= '9')
				return (byte)(c - '0');

			if (c == ':')
				return 10;

			if (c == ';')
				return 11;

			if (c >= 'A' && c <= 'Z')
				return (byte)(c - 'A' + 12);

			if (c >= 'a' && c <= 'z')
				return (byte)(c - 'a' + 38);

			return 255;
		}

		/// <summary>
		/// convert Multi Position SMS-message to MobilUnit array
		/// </summary>
		/// <param name="sMessage">SMS-message text in $U encoding</param>
		/// <returns>MobilUnit array</returns>
		PositionData[] ParseMP(string sMessage)
		{
			int i;

			// convert input string to byte array
			byte[] bArrayEtsiMess = new byte[sMessage.Length - 2];

			for (i = 0; i < bArrayEtsiMess.Length; i++)
				bArrayEtsiMess[i] = AsciiToEtsi(sMessage[i + 2]);

			// transform input string's byte array to ETSI byte array
			byte[] bArrayEtsiBytes = new byte[116]; // [(int)(bArrayEtsiMess.Length/4*3)];

			for (i = 0; i < bArrayEtsiBytes.Length - 2; i += 3)
			{
				bArrayEtsiBytes[i] =
					(byte)((bArrayEtsiMess[i / 3 * 4] & 0x3f) + (bArrayEtsiMess[i / 3 * 4 + 1] << 6));
				bArrayEtsiBytes[i + 1] =
					(byte)(((bArrayEtsiMess[i / 3 * 4 + 1] >> 2) & 0xf) + (bArrayEtsiMess[i / 3 * 4 + 2] << 4));
				bArrayEtsiBytes[i + 2] =
					(byte)(((bArrayEtsiMess[i / 3 * 4 + 2] >> 4) & 0x3) + (bArrayEtsiMess[i / 3 * 4 + 3] << 2));
			}

			bArrayEtsiBytes[114] = (byte)((bArrayEtsiMess[152] & 0x3f) + (bArrayEtsiMess[153] << 6));
			bArrayEtsiBytes[115] = (byte)(((bArrayEtsiMess[153] >> 2) & 0x0f) + (bArrayEtsiMess[154] << 4));

			// gaining position data from ETSI byte array
			int iPositionsCount = bArrayEtsiBytes[1];
			PositionData[] pdArray = new PositionData[iPositionsCount];

			uint uiReferenceTime = ((uint)bArrayEtsiBytes[12] << 24) +
				((uint)bArrayEtsiBytes[13] << 16) +
				((uint)bArrayEtsiBytes[14] << 8) +
				bArrayEtsiBytes[15];

			uint uiTempLatitude;
			uint uiTempLongitude;
			uint uiTempSpeed;
			uint uiTempTime;

			for (i = 0; i < pdArray.Length; i++)
			{
				uiTempLatitude = ((uint)bArrayEtsiBytes[10 * i + 16] << 16) +
					((uint)bArrayEtsiBytes[10 * i + 17] << 8) +
					bArrayEtsiBytes[10 * i + 18];

				if (bArrayEtsiBytes[10 * i + 16] > 0x7f)
					uiTempLatitude |= 0xff000000;

				uiTempLongitude = ((uint)bArrayEtsiBytes[10 * i + 19] << 16) +
					((uint)bArrayEtsiBytes[10 * i + 20] << 8) +
					bArrayEtsiBytes[10 * i + 21];

				if (bArrayEtsiBytes[10 * i + 19] > 0x7f)
					uiTempLongitude |= 0xff000000;

				uiTempTime = ((uint)bArrayEtsiBytes[10 * i + 22] << 14) +
					((uint)bArrayEtsiBytes[10 * i + 23] << 6) +
					((uint)bArrayEtsiBytes[10 * i + 24] >> 2);

				uiTempSpeed = ((uint)(bArrayEtsiBytes[10 * i + 24] & 0x03) << 8) +
					bArrayEtsiBytes[10 * i + 25];

				pdArray[i] = new PositionData();
				pdArray[i].Latitude = ((int)uiTempLatitude) * .0000512;
				pdArray[i].Longitude = ((int)uiTempLongitude) * .0000512;
				pdArray[i].Speed = (int)(((int)uiTempSpeed) * 1.6);
				pdArray[i].Time = (int)(uiReferenceTime + uiTempTime);
			}

			return pdArray;
		}

		/// <summary>
		/// Convert SMS containing position(retrieved from MJ2016 device)
		/// into MobilUnit
		/// >RPVTTTTTBBBBBBBBLLLLLLLLLVVV00012;ID=IIIISSDDA0A1A2A3A4C1C2C3aaaavvvvcc
		/// </summary>
		/// <param name="rpv">data from device in RPV format</param>
		/// <returns>MobilUnit</returns>
		PositionData ParseRPV(string rpv)
		{
			PositionData data = new PositionData();
			TimeSpan ts = new TimeSpan(0, 0, int.Parse(rpv.Substring(4, 5)));

			DateTime mydatetime = DateTime.UtcNow;
			TimeSpan mytime = mydatetime.TimeOfDay;//mydatetime %(24*60*60);
			DateTime mydate = mydatetime.Date;
			TimeSpan diff = mytime - ts;
			if (diff < TimeSpan.Zero)
			{
				if (-diff > TimeSpan.FromHours(12)) mydate -= TimeSpan.FromDays(1);
			}
			else
			{
				if (diff > TimeSpan.FromHours(12)) mydate += TimeSpan.FromDays(1);
			}
			data.Time = (int)(mydate.Add(ts) - (new DateTime(1970, 1, 1))).TotalSeconds;

			data.Latitude = Double.Parse(rpv.Substring(9, 8)) / 100000;
			data.Longitude = Double.Parse(rpv.Substring(17, 9)) / 100000;
			data.Speed = (int)(int.Parse(rpv.Substring(26, 3)) / 1.6);
			data.PowerVoltage = int.Parse(rpv.Substring(46, 2), NumberStyles.AllowHexSpecifier);
			data.VoltageAN1 = int.Parse(rpv.Substring(48, 2), NumberStyles.AllowHexSpecifier);
			data.VoltageAN2 = int.Parse(rpv.Substring(50, 2), NumberStyles.AllowHexSpecifier);
			data.VoltageAN3 = int.Parse(rpv.Substring(52, 2), NumberStyles.AllowHexSpecifier);
			data.VoltageAN4 = int.Parse(rpv.Substring(54, 2), NumberStyles.AllowHexSpecifier);
			data.Firmware = rpv.Substring(66, 4);
			return data;
		}

		public SMSTerminal(ITerminalManager itm) : base("SMSTerminal", itm)
		{
		}

		/// <summary>
		/// asking position via sms. started in separated thread
		/// </summary>
		/// <param name="tar">command</param>
		void FindPosition(object tar)
		{
			IStdCommand cmd = (IStdCommand)tar;
			string _target = cmd.Target.Telephone;
			if (null == _target) return;

			if (Write(_target, MJ2016_ASK_POSITION_STRING) != MJ2016_ASK_POSITION_STRING.Length)
			{
				$"Unable to send SMS"
					.CallTraceError();
				OnNotify(new NotifyEventArgs(TerminalEventMessage.TM_EXCEPTION, cmd.Sender, Severity.Lvl0,
					NotifyCategory.Terminal, "Не удалось отправить SMS", null, LinkStatus.Error, cmd.Target));
			}
		}

		/// <summary>
		/// Посылка СМС
		/// </summary>
		/// <param name="_target"></param>
		/// <param name="text"></param>
		/// <returns></returns>
		public int Write(string _target, string text)
		{
			return link.SendMessage(_target, text, 30000);
		}

		/// <summary>
		/// Посылка СМС
		/// </summary>
		/// <param name="_target"></param>
		/// <param name="data"></param>
		/// <returns></returns>
		public bool Write(string _target, byte[] data)
		{
			return link.SendMessage(_target, data);
		}

		/// <summary>
		/// send text via sms. started in separate thread
		/// </summary>
		/// <param name="mess">command</param>
		void SendMessage(object mess)
		{
			if (link == null)
			{
				if (tsTerm.TraceError)
					$"SMS terminal is not properly initialized."
						.CallTraceError();
				return;
			}
			IStdCommand cmd = (IStdCommand)mess;
			string message = cmd.Params[TextPar] as string;
			if (null == message) return;

			IDevice dev;
			byte[] data = null;
			try
			{
				dev = itmManager.GetDevice(cmd.Target);
				if (dev != null)
				{
					lock (dev)
					{
						dev.Init(CmdType.SMS, cmd);
						data = dev.GetCmd(cmd);
					}
				}
			}
			catch (Exception ex)
			{
				if (tsTerm.TraceError)
					default(string)
						.WithException(ex, true)
						.CallTraceError();
				dev  = null; // not return try to send as is
				data = null;
			}
			string _target = cmd.Target.Telephone;
			bool res;
			// no device or bytes -> send as is
			if (data == null) res = Write(_target, message) == message.Length;
			else res = link.SendMessage(_target, data);
			if (res)
			{
				string text = "Сообщение";
				if (cmd.Params["text"] != null)
					text = (string)cmd.Params["text"];

				OnNotify(new NotifyEventArgs(TerminalEventMessage.NE_MESSAGE, cmd.Sender, Severity.Lvl4,
					NotifyCategory.Terminal, $"{text} - отправлено (по SMS)", null, cmd.Target, -1, "", -1));
			}
			else
			{
				$"Unable to send SMS"
					.CallTraceError();
				OnNotify(new NotifyEventArgs(TerminalEventMessage.TM_EXCEPTION, cmd.Sender, Severity.Lvl0,
					NotifyCategory.Terminal, "Не удалось отправить SMS", null, LinkStatus.Error, cmd.Target));
			}
		}

		public void CancelActions()
		{
			should_stop = true;
		}

		bool ShouldStop()
		{
			return should_stop;
		}

		/// <summary> command interpreter </summary>
		/// <param name="cmd">command. must be IStdCommand</param>
		public override bool SendCommand(IStdCommand cmd)
		{
			if (cmd.Target.Telephone == null)
				cmd.Target.Telephone = cmd.Text;

			$"Sending command:\n{cmd}"
				.CallTraceInformation();

			switch (cmd.Type)
			{
				case CmdType.SetSettings:
				case CmdType.SendText:
					ThreadPool.QueueUserWorkItem(new WaitCallback(SendMessage), cmd);
					return true;
				case CmdType.AskPosition:
					ThreadPool.QueueUserWorkItem(new WaitCallback(FindPosition), cmd);
					return true;
				default:
					Trace.WriteLine("SMS Terminal->SendCommand: Unknown command type: " + cmd.Type);
					throw new ArgumentException("SMS Terminal->SendCommand: Unknown command type!");
			}
		}

		// init form "MEDIA_ID|SiemensSMS|+70957699100,5000,L|serial|COM1,57600,8N1,none", delimiter == '|'
		public override void Initialization(params string[] initStrings)
		{
			base.Initialization(initStrings);

			link = SMSGateway.Inst(initStrings) as SiemensSMSGateway; // !!!
			if (link != null)
				link.ReceiveSMSEvent += link_ReceiveSMSEvent;
		}

		/// <summary>
		/// receive data from gateway
		/// </summary>
		/// <param name="sender">sender</param>
		/// <param name="e">args</param>
		private void link_ReceiveSMSEvent(object sender, ReceiveSMSEventArgs e)
		{
			Trace.TraceInformation("{0} link_ReceiveSMSEvent from {1}: {2}", this, e.target, e.text);

			var time = DateTime.UtcNow;

			var phoneNumber = e.target.TrimStart('+');

			OnReceiveCounting(
				new ReceiveCountingEventArgs(CountingMessageType.SMS, 0, phoneNumber, time, 86400, 1));
			Process(new IncomingSms
			{
				Requestor    = new Contact { Type = ContactType.Phone, Value = phoneNumber },
				Text         = e.text,
				TimeReceived = time
			});
		}

		/// <summary>
		/// Признак возможности обработки команды
		/// </summary>
		/// <param name="cmd">команда</param>
		public override bool CanHandle(IStdCommand cmd)
		{
			return false;
		}
	}
}