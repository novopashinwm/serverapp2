﻿using System;
using System.Diagnostics;
using FORIS.TSS.IO;

namespace FORIS.TSS.Terminal.SMS
{
	public abstract class SMSGateway : IDisposable
	{
		/// <summary>
		/// event notification
		/// </summary>
		public event ReceiveSMSEventHandler ReceiveSMSEvent;
		protected virtual void NotifySMSEvent(ReceiveSMSEventArgs e)
		{
			ReceiveSMSEvent?.Invoke(this, e);
		}

		// the only gateway
		static SMSGateway inst;

		public abstract int SendMessage(string target, string msg, int timeout);

		public static SMSGateway Inst(params string[] initSrings)
		{
			// init form "M_ID|SiemensSMS|+70957699100,5000,L|serial|COM1,57600,8N1,none|shared"
			Debug.Assert(initSrings.Length > 0, "SMSGateway type not defined");

			// already created
			if (null != inst)
				if (initSrings[1] == "SiemensSMS") return inst;
				else throw new ArgumentException("Incompatible SMSGateway specified");

			switch (initSrings[1])
			{
				case "SiemensSMS":
					return InitSiemensSMSGateway(initSrings);
			}
			return null;
		}

		private static SMSGateway InitSiemensSMSGateway(string[] initSrings)
		{
			// длина массива строк инициализации
			if (initSrings.Length < 5)
				throw new ArgumentException(
					"Неверное количество аргументов инициализации", "initSrings");

			// не пустые параметры массива строк инициализации
			if (initSrings[2] == "" || initSrings[3] == "" || initSrings[4] == "")
				throw new ArgumentException("Неверные аргументы инициализации. " +
											"Один из параметров не указан");

			// SMSC info. "SMSC No.,timeout"
			string[] ss = initSrings[2].Split(',');
			if (0 == ss.Length) throw new ArgumentException("Invalid SMS center info", initSrings[1]);

			string SMSC = ss[0];
			int pool_int = 1 < ss.Length ? (int)ushort.Parse(ss[1]) : 5000;
			// ss[2] == "L"(ock) -> hold till death, default
			// ss[2] == "S"(hared) -> shared use only with physical port
			bool hold = ss.Length < 3 || ss[2] != "S";

			// device connection interface type
			if ("serial" != initSrings[3].ToLower())
				throw new ArgumentException("Unknown device link type", initSrings[2]);

			// serial port info
			ss = initSrings[4].Split(',');

			lock (typeof(SiemensSMSGateway))
			{
				if (null != inst) return inst;

				string port = "" != ss[0] ? ss[0] : "COM1";
				try
				{
					SerialStream stream = new SerialStream(port);
					if (ss.Length > 1) stream.SetPortSettings(uint.Parse(ss[1]));
					return inst = new SiemensSMSGateway(stream, pool_int, SMSC, hold);
				}
				catch (Exception e)
				{
					Trace.WriteLine("SMS Gateway: Can't create SerialStream!");
					Trace.WriteLine(e.ToString());
					return null;
				}
			}
		}

		#region IDisposable Members

		public void Dispose()
		{
			Dispose(true);
		}

		#endregion

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
				lock (typeof(SiemensSMSGateway))
					inst = null;
		}
	}
}