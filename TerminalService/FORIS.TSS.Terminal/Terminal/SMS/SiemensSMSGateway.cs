﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

using FORIS.TSS.Communication;

using Link = FORIS.TSS.IO.SerialStream;

namespace FORIS.TSS.Terminal.SMS
{
	/// <summary>
	/// SMS gateway for Siemens MC35i device
	/// TODO: merge with CommGateway
	/// </summary>
	public class SiemensSMSGateway : SMSGateway
	{
		protected override void Dispose(bool disposing)
		{
			try
			{
				if(disposing)
				{
					areStop.Set();
					if(worker.IsAlive) worker.Join();
				}
			}
			catch
			{
			}
			LinkClose();
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// message container
		/// </summary>
		class _rw_lnk
		{
			public readonly string phone;
			public readonly AutoResetEvent evt = new AutoResetEvent(false);
			public readonly string text;
			public readonly byte[] bytes;

			private readonly DateTime dtCreationTime;

			private int retryCount = 0;
			
			public _rw_lnk(string num, string txt, byte[] arr)
			{
				if(num == null) throw new ArgumentNullException("num", "Phone must be not null");
				if(txt == null && arr == null) throw new ArgumentNullException("txt and arr is null");
				Debug.Assert(txt == null || arr == null, "don't use them together");
				
				phone = num;
				text = txt;
				bytes = arr;

				dtCreationTime = DateTime.Now;
			}

			public DateTime CreationTime
			{
				get { return this.dtCreationTime; }
			}

			public int RetryCount
			{
				get { return this.retryCount;}
				set { this.retryCount = value;}
			}
			
			public override string ToString()
			{
				return bytes != null? BitConverter.ToString(bytes): text;
			}
		}

		readonly Link link;
		readonly StreamWriter sWriter;
		readonly StreamReader sReader;
		readonly string sPort;

		int iPoolInterval = 5000;
		string SMSC;
		Thread worker;
		AutoResetEvent areStop = new AutoResetEvent(false);
		/// <summary>
		/// hold port till total annihilation
		/// </summary>
		bool bHoldPort = true;
		
		/// <summary>
		/// synchronized outbound message list
		/// </summary>
		ArrayList alSendList = ArrayList.Synchronized(new ArrayList());

		Encoding encASCII = Encoding.ASCII;
		
		public SiemensSMSGateway(Link _link, int poolInterval, string _SMSC, bool hold)
		{
			sPort = (link = _link).Port;
			sReader = new StreamReader(link, encASCII);
			sWriter = new StreamWriter(link, encASCII);
			sWriter.AutoFlush = true;

			iPoolInterval = poolInterval;
			SMSC = _SMSC;
			bHoldPort = hold;

			Trace.WriteLine(string.Format("Starting SMSGW in {0} mode", hold? "exclusive": "shared"));
			
			worker = new Thread(_Start);
			worker.IsBackground = true;
			worker.Name = "SiemensSMSGateway";
			worker.Start();
		}
				
		~SiemensSMSGateway()
		{
			Dispose(false);
		}

		public override int SendMessage(string target, string msg, int timeout)
		{
			_rw_lnk p = new _rw_lnk(target, msg, null);
			lock(alSendList)
				alSendList.Add(p);
			if(!p.evt.WaitOne(timeout, false)) return 0;
			return msg.Length;
		}

		bool _InitSequence()
		{
			SendCommand(sEcho, 0);
			if(!Succeeded())
			{
				Trace.WriteLine("Unable to remove echo. No response", "SMSGW");
				return false;
			}

			// select SMS memory; ignore answer since tc35 does not have this command (but s45 does)
			SendCommand(sSelectSMSMem);
			string res = ReadResponse();
			if(res == "")
			{
				Trace.WriteLine("Unable to select memory" + (res != ""? ". Response - " + res: ". No response"), 
					"SMSGW");
				return false;
			}


			if (!TrySetModemMode())
				return false;

			SendCommand(sSMSArrivalEvt);
			res = ReadResponse();
			if(!Succeeded(res, sOK))
			{
				Trace.WriteLine(
					"Unable to set arrival event" + (res != ""? ". Response - " + res: ". No response"), "SMSGW");
				return false;
			}
	
			return true;
		}

		private bool TrySetModemMode()
		{
			SendCommand(sSetModemMode);
			var res = ReadResponse();
			if (!Succeeded(res, sOK))
			{
				Trace.TraceError(
					"{0}: Unable to set modem mode, {1}",
					this,
					(res != "" ? ". Response - " + res : ". No response"));
				return false;
			}
			return true;
		}

		void _Start()
		{
			// 1. try to open link
			// 2. if link opens, perform initialization procedure
			// 3. if initialization succeeds, read messages from terminal
			// 4. send SMSes in the queue
			link.SetTimeouts(500, 0, 0, 0, 5000);
			Trace.WriteLine(GetType().Name + ": Starting");
			if(!_InitSequence()) LinkClose();
			while(!areStop.WaitOne(iPoolInterval, false))
			{
				try
				{
					if(!LinkOpen())
					{
						Trace.WriteLine(DateTime.Now + ": Unable to init modem");
						LinkClose();
					}

					if(!link.Closed)
					{
						bool res = _SendMessages();
						Debug.Assert(res, "error during send");
						Trace.WriteLineIf(!res, DateTime.Now + ": Unable to send messages");
						if(!res) LinkClose();

						res = _ReadMessages();
						Debug.Assert(res, "error during read");
						Trace.WriteLineIf(!res, DateTime.Now + ": Unable to read messages");
						if(!res) LinkClose();
					}

					ArrayList cpy = new ArrayList(alSendList);
					foreach (_rw_lnk rw_lnk in cpy)
					{
						if( rw_lnk.RetryCount >= 2)
							lock(alSendList)
								alSendList.Remove(rw_lnk);
					}
				}
				catch(Exception ex)
				{
					Trace.WriteLine(ex.ToString(), "SMSGW");
					LinkClose();
				}
				finally
				{
					if(!bHoldPort) LinkClose();
				}
			}
			Trace.WriteLine(GetType().Name + ": Stopped");
			LinkClose();
		}

		bool _ReadMessages()
		{
			// get all messages in buffer
			SendCommand(sListMsgs);
			
			StringBuilder sb = new StringBuilder(BUFF_SIZE);
			// read all response till OK or ERROR
			while(true)
			{
				string res = ReadResponse();
				if(res == "")
				{
					Trace.WriteLine("No response during read", "SMSGW");
					return false;
				}

				if(Succeeded(sb.Append(res).ToString(), sOK, sError)) break;
			}
			
			int end = 0;
			string data = sb.ToString();
			// parsing cycle
			while(true)
			{
				int? msgid = null;
				try
				{
					int cmgl_beg = data.IndexOf("+CMGL:", end);
					if(cmgl_beg == -1 || end == -1) return true; // answer is OK, so we are done

					Trace.WriteLine("Incoming messages\n" + data, "SMSGW");
					msgid = int.Parse(data.Substring(cmgl_beg + 6, data.IndexOf(',', cmgl_beg + 6) - cmgl_beg - 6));
					int beg = data.IndexOf(sCRLF, cmgl_beg) + 2;
					end = data.IndexOf(sCRLF, beg);
					string pdu = data.Substring(beg, end - beg);
					string sender, msg;
					byte[] arr;

					if(0 != PDU.pdu_decode(pdu, out sender, out msg, out arr))
					{
						Trace.WriteLine(string.Format("Unable to decode message {0}: {1}", msgid, pdu), "SMSGW");
						// delete this message
						SendCommand(sDelMsg, msgid);
						if(!Succeeded()) return false;
						continue;
					}

					DispatchMsg(sender, msg, arr);
				}
				catch(Exception ex)
				{
					if(msgid == null)
					{
						Trace.WriteLine(string.Format("!!! Unable to parse messages - {0}", ex.ToString()), "SMSGW");
						return false;
					}
					Trace.WriteLine(string.Format("Error during parsing message {0} - {1}", msgid, ex.ToString()), 
						"SMSGW");
				}

				// delete this message
				SendCommand(sDelMsg, msgid.Value);
				if(!Succeeded()) return false;
			}
		}

		/// <summary>
		/// send messages in buffer to recipients
		/// </summary>
		/// <returns>false - critical error (no response), true - non-ctitical error or success</returns>
		bool _SendMessages()
		{
			// list empty -> quit
			if(alSendList.Count == 0) return true;

			// copy list
			ArrayList cpy = new ArrayList(alSendList);

			string dst;
			int smsc_length = 0;
			// find out SMSC len
			if(PDU.pdu_encode_smsc(out dst, SMSC) == 0) smsc_length = dst.Length;

			// encode and send every message
			// message removed only if send succeeded
			foreach(_rw_lnk msg in cpy)
			{
				string[] pdus = null;
				// try to encode
				try
				{
					int res = -1;
					if(msg.bytes != null)
						res = PDU.pdu_encode(SMSC, msg.phone, encASCII.GetString(msg.bytes), false, out pdus);
					else if(msg.text != null)
						res = PDU.pdu_encode(SMSC, msg.phone, msg.text, true, out pdus);
					if(res != 0) throw new ApplicationException("Unable to encode - " + msg);
					msg.RetryCount++;
				}
				catch(Exception ex)
				{
					Trace.WriteLine(ex.ToString(), "SMSGW");
					
					// if once failed to encode so ever. remove from list but without complete flag set
					lock (alSendList)
						alSendList.Remove(msg);
					continue;
				}

				Trace.WriteLine(string.Format("{0}: Sending message '{1}' to {2} in {3} part(s)", DateTime.Now, 
					msg, msg.phone, pdus.Length));
			
				int i = 0;
				for(int cnt = pdus.Length; i < cnt; ++i)
				{
					string pdu = pdus[i];
					SendCommand(sSendMsg, (pdu.Length - smsc_length) / 2);
					string res = ReadResponse();
					if(res == "")
					{
						Trace.WriteLine("No response before send", "SMSGW");
						return false;
					}

					if(!Succeeded(res, ">"))
					{
						Trace.WriteLine("Error starting SMS: - " + res, "SMSGW");
						break;
					}

					Write(pdu + '\x1A');
					res = ReadResponse();
					if(res == "")
					{
						Trace.WriteLine("No response during send", "SMSGW");
						return false;
					}

					if(!Succeeded(res, sOK))
					{
						Trace.WriteLine("Error sending SMS: - " + res, "SMSGW");
						break;
					}
					else Trace.WriteLine(res, "SMSGW");
				}
				if(i != pdus.Length) continue;
				// sent well
				msg.evt.Set();
				lock (alSendList)
					alSendList.Remove(msg);
			}
			return true;
		}

		void DispatchMsg(string sender, string text, byte[] data)
		{
			try
			{
				Trace.WriteLine(GetType().Name + ": Message from " + sender, "SMSGW");
				if(text != null) Trace.WriteLine("\ttext: " + text);
				if(data != null) Trace.WriteLine("\tdata: " + BitConverter.ToString(data));
				NotifySMSEvent(new ReceiveSMSEventArgs(sender, text, data));
			}
			catch(Exception ex)
			{
				Trace.WriteLine(ex.ToString(), "SMSGW");
			}
		}

		/// <summary>
		/// send data message
		/// </summary>
		/// <param name="target">phone</param>
		/// <param name="data">bytes to send</param>
		public bool SendMessage(string target, byte[] data)
		{
			_rw_lnk p = new _rw_lnk(target, null, data);
			lock (alSendList)
				alSendList.Add(p);
			return p.evt.WaitOne(30000, false);
		}
		
		/// <summary>
		/// open serial stream if not already
		/// </summary>
		/// <returns>true - open and inited, false - open and not inited</returns>
		bool LinkOpen()
		{
			bool res = true;
			if(link.Closed)
			{
				link.Open(sPort);
				link.SetTimeouts(500, 0, 0, 0, 5000);
				res = _InitSequence();
			}
			return res;
		}
		
		void LinkClose()
		{
			if(link.Closed) return;
			lock(link)
				if(!link.Closed) link.Close();
		}

		const string sOK = "OK";
		const string sError = "ERROR";
		
		/************************************************************************/
		/* commands list                                                        */
		/************************************************************************/
		const string sCRLF = "\r\n";
		const string sRestart = "AT+CFUN=1,1" + sCRLF;
		const string sEcho = "ATE{0}" + sCRLF;
		/// <summary>
		/// Выбор хранилища сообщений, SM=чтение производится с SIM-карты
		/// </summary>
		const string sSelectSMSMem = "AT+CPMS=\"SM\"" + sCRLF; //TODO: вынести в настройку или читать из обоих хранилищ, SIM-карты (SM) и внутреннего хранилища устройства (ME)
		const string sSMSArrivalEvt = "AT+CNMI=1,0" + sCRLF;
		const string sListMsgs = "AT+CMGL=4" + sCRLF;
		const string sDelMsg = "AT+CMGD={0}" + sCRLF;
		const string sSendMsg = "AT+CMGS={0}" + sCRLF;
		private const string sSetModemMode = "AT+CMGF=0" + sCRLF;
		
		bool ResetModem()
		{
			try
			{
				Trace.WriteLine("Restarting modem", "SMSGW");
				SendCommand(sRestart);

				bool res = Succeeded();
				Debug.Assert(res, "Unable to restart modem");
				if(!res) Trace.WriteLine("Unable to restart modem", "SMSGW");
				Thread.Sleep(5000);
				link.Dtr = true;
				return res;
			}
			catch
			{
				throw;
			}
		}
		

		/// <summary>
		/// read from stream no more than BUFF_SIZE bytes
		/// </summary>
		/// <returns>data read</returns>
		string ReadResponse()
		{
			try
			{
				LinkOpen();
				return new string(acResp, 0, Read(acResp));
			}
			catch
			{
				return "";
			}
		}

		// buf for text responses from port
		const int BUFF_SIZE = 256;
		char[] acResp = new char[BUFF_SIZE];

		/// <summary>
		/// read from stream till at least one of conditions found
		/// </summary>
		/// <param name="checks">list of condition strings</param>
		/// <returns>data read</returns>
		string ReadResponse(params string[] checks)
		{
			try
			{
				LinkOpen();
				string res;
				StringBuilder sb = new StringBuilder(BUFF_SIZE);
				do 
				{
					res = sb.Append(acResp, 0, Read(acResp)).ToString();
				}
				while(!Succeeded(res, checks));
				Trace.WriteLine("Response: " + res, "SMSGW");
				return res;
			}
			catch
			{
				return "";
			}
		}

		/// <summary>
		/// test res for containing of checks
		/// </summary>
		/// <param name="res">string to test</param>
		/// <param name="checks">check conditions</param>
		/// <returns>contain or not</returns>
		bool Succeeded(string res, params string[] checks)
		{
			foreach(string check in checks)
				if(res.IndexOf(check) != -1) return true;
			return false;
		}
		/// <summary>
		/// get response and test for OK
		/// </summary>
		/// <returns>contain or not</returns>
		bool Succeeded()
		{
			return Succeeded(sOK);
		}
		/// <summary>
		/// get response and test for check
		/// </summary>
		/// <param name="check">check to seek in response</param>
		/// <returns>contain or not</returns>
		bool Succeeded(string check)
		{
			return Succeeded(ReadResponse(), check);
		}

		public void Write(string data)
		{
			sWriter.Write(data);
		}
		
		public int Read(char[] data)
		{
			return sReader.Read(data, 0, data.Length);
		}
		
		/// <summary>
		/// prepare and write command to com port
		/// </summary>
		/// <param name="cmd">cmd template</param>
		/// <param name="par">params</param>
		void SendCommand(string cmd, params object[] par)
		{
			Write(string.Format(cmd, par));
		}
	}
}