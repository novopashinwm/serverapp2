﻿using System;
using System.Collections.Generic;
using Assemblies.Ftp.FileSystem;

namespace FORIS.TSS.Terminal.Terminal.Ftp
{
	class FtpInMemoryFile : IFile
	{
		private readonly string _fileName;
		private readonly List<byte> _bytes;
		public class WriteCompletedEventArgs : EventArgs
		{
			public string FileName { get; private set; }
			public byte[] Bytes { get; private set; }
			public WriteCompletedEventArgs(string fileName, byte[] bytes)
			{
				FileName = fileName;
				Bytes = bytes;
			}
		}
		public event EventHandler<WriteCompletedEventArgs> WriteCompleted;
		public FtpInMemoryFile(string fileName)
		{
			_fileName = fileName;
			_bytes = new List<byte>();
		}
		public int Read(byte[] abData, int nDataSize)
		{
			throw new NotSupportedException();
		}
		public int Write(byte[] abData, int nDataSize)
		{
			if (abData.Length < nDataSize)
				nDataSize = abData.Length;

			for (var i = 0; i != nDataSize; ++i)
				_bytes.Add(abData[i]);

			return nDataSize;
		}
		public void Close()
		{
			WriteCompleted?.Invoke(this, new WriteCompletedEventArgs(_fileName, _bytes.ToArray()));
		}
	}
}