﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Assemblies.Ftp;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Terminal.Ftp
{
	///<summary>
	/// Терминал для получения данных от терминалов по ftp (терминал выступает в роли ftp-сервера)
	///</summary>
	[Serializable]
	public class FtpTerminal : _Terminal
	{
		private FtpServer _ftpServer;

		///<summary>Создает экземпляр терминала</summary>
		///<param name="mgr">Менеджер терминалов</param>
		public FtpTerminal(ITerminalManager mgr)
			: base("Ftp Terminal", mgr)
		{
		}

		public override void Initialization(params string[] initStrings)
		{
			base.Initialization(initStrings);

			IPAddress ipAddress;
			if (initStrings.Length < 2 || !IPAddress.TryParse(initStrings[1], out ipAddress))
				ipAddress = IPAddress.Any;

			int passivePortFrom;
			int passivePortTo;
			if (initStrings.Length < 3 || !int.TryParse(initStrings[2], out passivePortFrom))
				passivePortFrom = 49152;

			if (initStrings.Length < 4 || !int.TryParse(initStrings[3], out passivePortTo))
				passivePortTo = 65534;

			Start(ipAddress, passivePortFrom, passivePortTo);
		}

		private void Start(IPAddress ipAddress, int passivePortFrom, int passivePortTo)
		{
			var fileSystemFactory = new FtpInMemoryFileSystemFactory();
			fileSystemFactory.WriteCompleted +=
				delegate(object sender, FtpInMemoryFileSystemFactory.WriteCompletedEventArgs e)
					{
						DateTime dateTime;
						var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(e.FileName);
						var dateTimeStringBuilder = new StringBuilder(17);
						if (fileNameWithoutExtension != null)
						{
							foreach (var c in fileNameWithoutExtension.Where(c => '0' <= c && c <= '9'))
								dateTimeStringBuilder.Append(c);
						}
						if (!DateTime.TryParseExact(
							dateTimeStringBuilder.ToString(),
							"yyyyMMddHHmmssFFF",
							CultureInfo.InvariantCulture, 
							DateTimeStyles.None, 
							out dateTime))
							dateTime = DateTime.UtcNow;

						OnReceive(
							new ReceiveEventArgs(
								new MobilUnit.Unit.PictureUnit
									{
										DeviceID = e.User,
										Time = TimeHelper.GetSecondsFromBase(dateTime),
										Bytes = e.Bytes,
										CorrectGPS = false
									}));
					};
			_ftpServer = new FtpServer(fileSystemFactory);
			_ftpServer.Start(ipAddress, 21, passivePortFrom, passivePortTo);
		}

		private void Stop()
		{
			_ftpServer.Stop();
		}

		public override void Dispose()
		{
			Stop();
			base.Dispose();
		}
	}
}
