﻿using System;
using System.IO;
using Assemblies.Ftp.FileSystem;

namespace FORIS.TSS.Terminal.Terminal.Ftp
{
	class FtpInMemoryFileSystem : IFileSystem
	{
		private readonly string _user;
		private static readonly string[] EmptyStringArray = new string [0];
		public event EventHandler<FtpInMemoryFile.WriteCompletedEventArgs> WriteCompleted;
		public FtpInMemoryFileSystem(string user)
		{
			_user = user;
		}
		public IFile OpenFile(string sPath, bool fWrite)
		{
			if (!fWrite)
				return null;

			var ftpInMemoryFile = new FtpInMemoryFile(Path.GetFileName(sPath));

			ftpInMemoryFile.WriteCompleted += OnWriteCompleted;

			return ftpInMemoryFile;
		}
		private void OnWriteCompleted(object sender, FtpInMemoryFile.WriteCompletedEventArgs e)
		{
			WriteCompleted?.Invoke(this, e);
		}
		public IFileInfo GetFileInfo(string sPath)
		{
			return null;
		}
		public string[] GetFiles(string sPath)
		{
			return EmptyStringArray;
		}
		public string[] GetFiles(string sPath, string sWildcard)
		{
			return EmptyStringArray;
		}
		public string[] GetDirectories(string sPath)
		{
			return new[] { _user };
		}
		public string[] GetDirectories(string sPath, string sWildcard)
		{
			return GetDirectories(sPath);
		}
		public bool DirectoryExists(string sPath)
		{
			return true;
		}
		public bool FileExists(string sPath)
		{
			return false;
		}
		public bool CreateDirectory(string sPath)
		{
			return true;
		}
		public bool Move(string sOldPath, string sNewPath)
		{
			return false;
		}
		public bool Delete(string sPath)
		{
			return false;
		}
	}
}