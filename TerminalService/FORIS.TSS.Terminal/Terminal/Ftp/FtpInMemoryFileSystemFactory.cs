﻿using System;
using Assemblies.Ftp.FileSystem;

namespace FORIS.TSS.Terminal.Terminal.Ftp
{
	class FtpInMemoryFileSystemFactory : IFileSystemClassFactory
	{
		public class WriteCompletedEventArgs : FtpInMemoryFile.WriteCompletedEventArgs
		{
			public string User { get; private set; }

			public WriteCompletedEventArgs(string user, string fileName, byte[] bytes)
				: base(fileName, bytes)
			{
				User = user;
			}
		}

		public event EventHandler<WriteCompletedEventArgs> WriteCompleted;

		public IFileSystem Create(string sUser, string sPassword)
		{
			var fileSystem = new FtpInMemoryFileSystem(sUser);

			fileSystem.WriteCompleted +=
				delegate (object sender, FtpInMemoryFile.WriteCompletedEventArgs e)
					{
						if (WriteCompleted == null)
							return;
						WriteCompleted(this, new WriteCompletedEventArgs(sUser, e.FileName, e.Bytes));
					};

			return fileSystem;
		}
	}
}