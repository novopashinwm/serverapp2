﻿using System;
using System.Collections;

namespace FORIS.TSS.Terminal.Terminal
{
	public interface ITerminalReceiver : IDisposable
	{
		/// <summary> Информация об объекте слежения </summary>
		IUnitInfo UnitInfo        { get; set; }
		DateTime  LastReceiveTime { get; }
		IList     Res             { get; set; }
		long      ReceiveCounter  { get; }
		bool      Disposed        { get; }
		IDevice   Device          { get; set; }
		object    Tag             { get; set; }
		event EventHandler OnData;
		event EventHandler OnProcessedData;
		bool Send(byte[] data, int length);
		void ProcessReceivedData(byte[] data);
	}
}