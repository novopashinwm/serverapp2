using System;
using System.Diagnostics;
using System.Net;

using FORIS.TSS.Communication;

namespace FORIS.TSS.Tools.UDPTranslator
{
	/// <summary>
	/// ���������� UDP �����������
	/// extended (0xff) datagram wraps original, size = source_size + 16
	/// bytes[0]				- 0xff, code
	/// bytes[1..2]				- size of original, N
	/// bytes[3..N + 2]			- original bytes
	/// bytes[N + 3]			- 0xff
	/// bytes[N + 4..N + 7]		- source ip bytes, ipv4
	/// bytes[N + 8..N + 9]		- source port
	/// bytes[N + 10..N + 13]	- target ip bytes, ipv4
	/// bytes[N + 14..N + 15]	- target port
	/// </summary>
	public class UDPTranslatorDatagram : Datagram
	{
		byte[] arbWrapped, arbUnwrapped;
		IPEndPoint epTarget, epSource;
		
		// whole size of wrapper-added bytes, wrapper prefix, wrapper postfix
		const int extSize = 16, preSize = 3, postSize = 13, ID = 0xff;
		
		public UDPTranslatorDatagram(byte[] data) : base(data, 0, data.Length)
		{
			Unwrap();
		}
		
		public IPEndPoint Source
		{
			get
			{
				return epSource;
			}
		}
		
		public IPEndPoint Target
		{
			get
			{
				return epTarget;
			}
		}
		
		/// <summary>
		/// unwrap data and bytes and end point
		/// </summary>
		/// <returns>unwrapped bytes</returns>
		public byte[] Unwrap()
		{
			if(arbUnwrapped != null) return arbUnwrapped;
			if(bytes[0] != ID) return arbUnwrapped = bytes;
			
			// data pos, size
			int size = IntFromBytesLE(bytes[1], bytes[2]);
			// if any unexpected case use initial as unwrapped
			if(size > g_MaxDatagramLength) return arbUnwrapped = bytes;
			if(size + extSize != bytes.Length) return arbUnwrapped = bytes;
			if(bytes[size + preSize] != ID) return arbUnwrapped = bytes;
				
			// pos after second ID, point to source IP
			int extpos = size + preSize + 1;
			epSource = new IPEndPoint(new IPAddress((uint)IntFromBytesLE(bytes[extpos], bytes[extpos + 1], 
				bytes[extpos + 2], bytes[extpos + 3])), IntFromBytesLE(bytes[extpos + 4], bytes[extpos + 5]));
			if(epSource.Address == IPAddress.Any || epSource.Port == 0) epSource = null;
			epTarget = new IPEndPoint(new IPAddress((uint)IntFromBytesLE(bytes[extpos + 6], bytes[extpos + 7], 
				bytes[extpos + 8], bytes[extpos + 9])), IntFromBytesLE(bytes[extpos + 10], bytes[extpos + 11]));
			if(epTarget.Address == IPAddress.Any || epTarget.Port == 0) epTarget = null;
			
			arbUnwrapped = new byte[size];
            System.Array.Copy(bytes, preSize, arbUnwrapped, 0, size);
			return arbUnwrapped;
		}
		
		/// <summary>
		/// extended send data. can use proxy
		/// </summary>
		/// <param name="target">target end point</param>
		public byte[] Wrap(IPEndPoint source, IPEndPoint target)
		{
			Debug.Assert(source != target, "source and target same");
			
			// for sure unwrap
			if(arbUnwrapped == null) Unwrap();

			// wrapping the same
			if(source == Source && target == Target)
			{
				if(arbWrapped != null) return arbWrapped;
			}
			else
			{
				epSource = source;
				epTarget = target;
			}
			
			int len = arbUnwrapped.Length;
			
			arbWrapped = new byte[len + extSize];
			arbWrapped[0] = arbWrapped[len + preSize] = ID;
			Datagram.IntToBytes(len, arbWrapped, 1, 2);
            System.Array.Copy(arbUnwrapped, 0, arbWrapped, 3, len);
			
			// pos after second ID, point to source IP
			int extpos = preSize + len + 1;
			byte[] addr;
			if(source != null)
			{
				addr = source.Address.GetAddressBytes();
				if(addr.Length != 4) throw new ArgumentOutOfRangeException("source", target, "ipv4 expected");
                System.Array.Copy(addr, 0, arbWrapped, extpos, addr.Length);
				Datagram.IntToBytes(source.Port, arbWrapped, extpos + 4, 2);
			}
			if(target != null)
			{
				addr = target.Address.GetAddressBytes();
				if(addr.Length != 4) throw new ArgumentOutOfRangeException("target", target, "ipv4 expected");
                System.Array.Copy(addr, 0, arbWrapped, extpos + 6, addr.Length);
				Datagram.IntToBytes(target.Port, arbWrapped, extpos + 10, 2);
			}
			return bytes = arbWrapped;
		}
	}
}