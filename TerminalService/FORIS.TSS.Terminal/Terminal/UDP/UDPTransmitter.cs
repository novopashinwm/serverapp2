using System;
using System.Collections;
using System.Net;
using System.Threading;
using System.Diagnostics;

using FORIS.TSS.Communication;
using FORIS.TSS.Config;
using FORIS.TSS.IO.UDP;
using FORIS.TSS.TerminalService.Interfaces;
using FORIS.TSS.Tools.UDPTranslator;

namespace FORIS.TSS.Terminal.UDP
{
	#region class UDPTransmitterEventArgs 
	/// <summary>
	/// �������� ������� ������� ��� ������������� ������ �� UDP ���������
	/// </summary>
	public abstract class UDPTransmitterEventArgs : UDPListenerEventArgs
	{
		/// <summary>
		/// ��������� ���������� �� ���������� �������
		/// </summary>
		protected Datagram datagram;
		/// <summary>
		/// ��������� ���������� �� ���������� �������
		/// </summary>
		public Datagram Datagram
		{
			get { return datagram; }
		}


		/// <summary>
		/// IP ������ ���������� �������
		/// </summary>
		protected IPEndPoint ip;
		
		/// <summary>
		/// IP ������ ���������� �������
		/// </summary>
		public IPEndPoint IP
		{
			get { return ip; }
		}

		/// <summary>
		/// true, if datagram format is correct
		/// </summary>
		bool bIsCorrect;
		
		/// <summary>
		/// true, if datagram format is correct
		/// </summary>
		public bool IsCorrect
		{
			get
			{
				return bIsCorrect;
			}
		}

		#region .ctor
		public UDPTransmitterEventArgs(Datagram datagram, IPEndPoint ip, bool bIsCorrect)
		{
			this.datagram = datagram;
			this.ip = ip;
			this.bIsCorrect = bIsCorrect;
		}
		#endregion .ctor

		public override byte[] Data
		{
			get
			{
				return datagram.GetBytes();
			}
			set
			{
				throw new NotSupportedException("Data is read only");
			}
		}
		
		public abstract IMobilUnit[] GetMobilUnits();
	}
	#endregion // class UDPTransmitterEventArgs 

	/// <summary>
	/// Summary description for UDPTransmitter.
	/// </summary>
	[Serializable]
	public class UDPTransmitter : UDPListener
	{
		/// <summary>
		/// terminal related switch
		/// </summary>
		TraceSwitch tsTerm = new TraceSwitch("Terminal", "UDP");

		/// <summary>
		/// ������� ��������� ���������� �� UDP ���������
		/// </summary>
		public new event UDPListenerEventHandler ReceiveEvent;

		/// <summary>
		/// ������� ��������� ������ �������������
		/// </summary>
		public new event EventHandler StopEvent;

		/// <summary>
		/// ������� ������� ������ �������������
		/// </summary>
		public new event EventHandler StartEvent;

		/// <summary>
		/// ������ ��� ������������� ������ �� UDP ���������
		/// </summary>
		/// <param name="ipEndPoint">IP ����� � ���� ��� �������������</param>
		public UDPTransmitter(IPEndPoint ipEndPoint) : base(ipEndPoint)
		{
			string ep = GlobalsConfig.AppSettings["UDPProxy"];
			
			if(string.IsNullOrEmpty(ep)) 
				return;
			
			string[] ss = ep.Split(':');
			try
			{
				if (ss != null)
				{
					var hostEntry = Dns.GetHostEntry(ss[0]);
					var ipAddress = hostEntry.AddressList[0];
					epProxy = new IPEndPoint(ipAddress, int.Parse(ss[1]));
				}
			}
			catch
			{
				try
				{
					if (ss != null) epProxy = new IPEndPoint(IPAddress.Parse(ss[0]), int.Parse(ss[1]));
				}
				catch(Exception ex)
				{
					Trace.WriteLine("Error during parsing UDPProxy: - " + ex.ToString(), tsTerm.Description);
				}
			}
			Trace.WriteLineIf(tsTerm.TraceWarning, "Using UDPProxy - " + ep, tsTerm.Description);
		}
		protected override void Dispose(bool disposing)
		{
			Debug.WriteLine("UDPTransmitter.Dispose()");

			base.Dispose(disposing);
		}

		#region void ListenerThread()
		
		/// <summary>
		/// UDP listener thread
		/// </summary>
		protected override void ListenerThread()
		{
			Trace.WriteLineIf(tsTerm.TraceWarning, "UDPTransmitter thread started", tsTerm.Description);
			while(listenerThread.IsAlive) 
			{
				try
				{
					byte[] bytes = Unwrap(Receive(ref ipClientEndPoint), ref ipClientEndPoint);
					if(bytes.Length != 0) OnReceive(new UDPListenerEventArgs(bytes, ipClientEndPoint));
					else Debug.Fail("empty data");
				}
				catch(ThreadAbortException ex)
				{
					Trace.WriteLineIf(tsTerm.TraceError, ex.ToString(), tsTerm.Description);
					break;
				}
				catch(Exception ex)
				{
					Trace.WriteLineIf(tsTerm.TraceError, ex.ToString(), tsTerm.Description);
				}
			}
			OnStop();
			Trace.WriteLineIf(tsTerm.TraceWarning, "UDPTransmitter thread stopped", tsTerm.Description);
		}
		#endregion // void ListenerThread()

		#region void OnStop()
		// ��������� ������ �������������
		protected override void OnStop()
		{
			Close();

			//
			if(listenerThread != null)
				if(listenerThread.IsAlive)
					listenerThread.Abort();
			//
			if(StopEvent != null)
				StopEvent(this, EventArgs.Empty);
		}
		#endregion // void OnStop()

		#region void OnStart()
		// ������ ������ �������������
		protected override void OnStart()
		{
			try
			{
				//
				listenerThread = new Thread(ListenerThread);
				listenerThread.Name = "UDP Listener Thread";
				listenerThread.IsBackground = true;
				listenerThread.Start();

				//
				if(StartEvent != null)
					StartEvent(this, EventArgs.Empty);
			}
			catch
			{
				throw;
			}
		}
		#endregion // void OnStart()
		
		IPEndPoint epProxy;
		/// <summary>
		/// extended send data. can use proxy
		/// </summary>
		/// <param name="data">bytes to send</param>
		/// <param name="target">target end point</param>
		public void Send(byte[] data, IPEndPoint target)
		{
			int len = data.Length;
			if(epProxy == null)
			{
				Send(data, len, target);
				return;
			}
			
			data = new UDPTranslatorDatagram(data).Wrap(null, target);
			if(Send(data, data.Length, epProxy) != data.Length) 
				throw new ApplicationException("�� ������� ��������� ���������");
		}

		static byte[] Unwrap(byte[] data, ref IPEndPoint source)
		{
			UDPTranslatorDatagram dg = new UDPTranslatorDatagram(data);
			byte[] res = dg.Unwrap();
			if(dg.Source != null) source = dg.Source;
			return res;
		}
		

		#region OnReceive(ReceiveEventArgs args)
		/// <summary>
		/// ����� ������ ������� ������� �������� ���������� ������ 
		/// </summary>
		protected virtual void OnReceive(UDPListenerEventArgs args)
		{
			NotifyReceiveEvents(args);
		}
		#endregion // OnReceive(ReceiveEventArgs args)
		

		#region NotifyReceiveEvents(ReceiveEventArgs args)
		/// <summary>
		/// ����������� �������� � ������� ������� �������� ���������� ������
		/// </summary>
		/// <param name="args">�������� ������</param>
		private void NotifyReceiveEvents(UDPListenerEventArgs args)
		{
			if(ReceiveEvent == null)
				return;

			Delegate[] invkList = ReceiveEvent.GetInvocationList();
			IEnumerator ie = invkList.GetEnumerator();
			//
			while(ie.MoveNext())
			{
				UDPListenerEventHandler handler = (UDPListenerEventHandler)ie.Current;
				
				try
				{
					handler.BeginInvoke(this, args, new AsyncCallback(CallBackReceiveEvents), handler);
				}
				catch
				{
					throw;
				}
			}
		}

		/// <summary>
		/// �������� ������������ ������ 
		/// </summary>
		/// <param name="ar"></param>
		void CallBackReceiveEvents(IAsyncResult ar)
		{
			// ������� ��������� ����� ���������
			UDPListenerEventHandler handler = (UDPListenerEventHandler)ar.AsyncState;
			try
			{
				handler.EndInvoke(ar);
			}
			catch(Exception ex)
			{
				Trace.WriteLineIf(tsTerm.TraceError,
					"UDPTransmitter -> CallBackReceiveEvents Exception !!!\n" + ex.ToString(), tsTerm.Description);
				ReceiveEvent -= handler;
			}
		}

		#endregion // NotifyReceiveEvents(ReceiveEventArgs args)
	}
}