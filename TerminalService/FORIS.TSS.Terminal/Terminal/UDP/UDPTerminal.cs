﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Config;
using FORIS.TSS.IO.UDP;
using FORIS.TSS.Terminal.Messaging;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.UDP
{
	/// <summary>
	/// Терминал для работы с контролером через
	/// </summary>
	[Serializable]
	public class UDPTerminal : _Terminal
	{
		[field:NonSerialized]
		readonly TraceSwitch tsTerm = new TraceSwitch("Terminal", "UDP");

		protected IPAddress address = IPAddress.Any;
		protected int port;
		protected UDPTransmitter listener;
		protected Queue qMsg;

		// named par list
		const string sConfirmNoPar = "ConfirmNo";
		const string ConfirmMessTypePar = "ConfirmMessType";

		struct UnitIpTime
		{
			private readonly IPEndPoint unitIP;
			private readonly DateTime connectionTime;

			public UnitIpTime(IPEndPoint unitIP, DateTime connectionTime)
			{
				this.unitIP = unitIP;
				this.connectionTime = connectionTime;
			}

			public IPEndPoint UnitIP
			{
				get { return unitIP; }
			}

			public DateTime ConnectionTime
			{
				get { return connectionTime; }
			}
		}

		private Dictionary<int, UnitIpTime> UintsIpAddresses = new Dictionary<int, UnitIpTime>();

		/// <summary>
		/// life time of remote end point. all of them older than iLifeTime seconds treated are invalid
		/// </summary>
		int iLifeTime = 600;

		/// <summary>
		/// IP адрес сервера
		/// </summary>
		public IPAddress Address
		{
			get { return address; }
			set { address = value; }
		}

		/// <summary>
		/// Сетевой порт сервера
		/// </summary>
		public int Port
		{
			get { return port; }
			set
			{
				if(IPEndPoint.MinPort > value || value > IPEndPoint.MaxPort)
					throw new ArgumentOutOfRangeException("Port", "Неверное значение сетевого порта сервера.");
				port = value;
			}
		}

		readonly System.Timers.Timer cleanerTimer;

		#region .ctor

		/// <summary>
		/// Терминал для работы с контролером через UDP
		/// </summary>
		public UDPTerminal(ITerminalManager itm) : base("UDPTerminal", itm)
		{
			string sLogLoc = GlobalsConfig.AppSettings["LogLocation"];
			if (string.IsNullOrWhiteSpace(sLogLoc))
			{
				if (!sLogLoc.EndsWith(@"\")) sLogLoc += @"\";
				swInvalidData = new StreamWriter(sLogLoc + "mjdata.log", true);
				swInvalidData.AutoFlush = true;
			}

//#if DEBUG
//            swByteLog.AutoFlush = true;
//#endif

			try
			{
				string eplf = GlobalsConfig.AppSettings["EndPointLifeTime"];
				if(eplf != null && eplf != "") iLifeTime = ushort.Parse(eplf);
				if(iLifeTime == 0) iLifeTime = int.MaxValue;
			}
			catch
			{
				Trace.WriteLineIf(
					tsTerm.TraceError, "Incorrect config value, key = EndPointLifeTime", tsTerm.Description);
			}

			double TimeToClean = 3;//Время запуска очистки, в часах после начала суток


			if (GlobalsConfig.AppSettings["TimeToClean"] != null)
				TimeToClean = TimeSpan.Parse(GlobalsConfig.AppSettings["TimeToClean"]).TotalHours;

			cleanerTimer = new System.Timers.Timer();

			cleanerTimer.Elapsed += new System.Timers.ElapsedEventHandler(cleanerTimer_Elapsed);
			DateTime dt3 = DateTime.Now.TimeOfDay < TimeSpan.FromHours(TimeToClean) ?
				DateTime.Now.Date.AddHours(TimeToClean) :
				DateTime.Now.Date.AddDays(1).AddHours(TimeToClean);
			cleanerTimer.Interval = (dt3 - DateTime.Now).TotalMilliseconds;
#if DEBUG
			cleanerTimer.Interval = TimeSpan.FromMinutes(5).TotalMilliseconds;
#endif
			cleanerTimer.Enabled = true;
		}



		#endregion // .ctor

		void cleanerTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			cleanerTimer.Enabled = false;
			double PeriodToClean = 24 * 60; //Периодичность запуска очистки, в минутах
			if (GlobalsConfig.AppSettings["PeriodToClean"] != null)
				PeriodToClean = double.Parse(GlobalsConfig.AppSettings["PeriodToClean"]);
			cleanerTimer.Interval = TimeSpan.FromMinutes(PeriodToClean).TotalMilliseconds;
			cleanerTimer.Enabled = true;

			lock(this.UintsIpAddresses)
				this.UintsIpAddresses.Clear();

			GC.Collect();
			GC.WaitForPendingFinalizers();
			GC.Collect();
		}

		// named param list
		const string fromTimePar = "FromTime";
		const string toTimePar = "ToTime";

		#region ITerminal Members
		/// <summary>
		/// Передача команды от терминала мобильному объекту с контроллером по UDP
		/// </summary>
		/// <param name="cmd">команда для мобильного объекта с контроллером</param>
		public override bool SendCommand(IStdCommand cmd)
		{
			try
			{
				IDevice dev = this.GetDevice(cmd.Target);
				if (dev == null)
				{
					dev = itmManager.GetDevice(cmd.Target);
					if(dev != null)
						this.AddDevice(dev);
				}

				if(dev == null && cmd.Target.Telephone != string.Empty)
				{
					return itmManager.CreateTerminal("GSM").SendCommand(cmd);
				}
				// find end point for our target
				IPEndPoint ipep = null;
				//lock(slUnitInfo.SyncRoot)
				//{
				//    // check in list of remote end points
				//    int i = slUnitInfo.IndexOfKey(cmd.Target);
				//    if(i > -1)	// here it is
				//    {
				//        IUnitInfo ui = (IUnitInfo)slUnitInfo.GetKey(i);
				//        // !! WARN !!
				//        Debug.Assert(ui.CompareTo(cmd.Target) == 0, "Unexpected problem: comparisons failed");
				//        Debug.Assert(slUnitInfo.IndexOfKey(cmd.Target) == slUnitInfo.IndexOfKey(ui),
				//            "Unexpected problem: comparisons failed");

				//        // end point is valid and not old
				//        if(ui.IP.Address != IPAddress.Any)
				//            // end point valid or no explicit specified
				//            if(DateTime.Now.AddSeconds(-iLifeTime) < ((DateTime)slUnitInfo.GetByIndex(i))
				//                //((DateTime)slUnitInfo[ui]) &&
				//                || cmd.Target.IP == null || cmd.Target.IP.Address == IPAddress.Any)
				//                ipep = ui.IP;
				//    }
				//}
				lock(this.UintsIpAddresses)
				{
					if(this.UintsIpAddresses.ContainsKey(cmd.Target.ID))
					{
						if(this.UintsIpAddresses[cmd.Target.ID].UnitIP.Address != IPAddress.Any)
						{
							if(DateTime.Now.AddSeconds(-iLifeTime) <
								this.UintsIpAddresses[cmd.Target.ID].ConnectionTime ||
								cmd.Target.IP == null ||
								cmd.Target.IP.Address == IPAddress.Any)
								ipep = this.UintsIpAddresses[cmd.Target.ID].UnitIP;
						}
					}
				}

				if(ipep == null) ipep = cmd.Target.IP;
				if(ipep == null || ipep.Address == IPAddress.Any)
					throw new ArgumentException("End point incorrect");


				if(dev == null)
				{
					Trace.WriteLineIf(tsTerm.TraceVerbose,
						"Unable to send message due to unknown device - " + cmd.Target,
						tsTerm.Description);

					if(cmd.Sender.OperatorId != (int)ObjectID.Unknown)
						OnNotify(new NotifyEventArgs(TerminalEventMessage.NE_MESSAGE, cmd.Sender, Severity.Lvl1,
							NotifyCategory.Terminal,
							"Не удалось создать устройство для отправки сообщения по UDP",
							null, cmd.Target, -1, ""));

					return false;
				}

				byte[] data;
				lock (dev)
				{
					dev.Init(CmdType.UDP, cmd);
					data = dev.GetCmd(cmd);
				}
				if(data == null)
				{
					//throw new Exception("Can't send comand.");
					Trace.WriteLineIf(tsTerm.TraceVerbose,
						"Can't send comand to - " + cmd.Target,
						tsTerm.Description);
					return false;
				}
				Trace.WriteLineIf(tsTerm.TraceVerbose, string.Format("Sending {2} command to {0}\r\n{1}", ipep,
					BitConverter.ToString(data), cmd.Type), tsTerm.Description);
				listener.Send(data, ipep);

				if (cmd.Sender.OperatorId != (int)ObjectID.Unknown)
				{
					string text = "Сообщение";
					if(cmd.Params["text"] != null)
						text = (string)cmd.Params["text"];

					OnNotify(new NotifyEventArgs(TerminalEventMessage.NE_MESSAGE, cmd.Sender, Severity.Lvl3,
												 NotifyCategory.Terminal, text + " - отправлено (по UDP)", null, cmd.Target, -1, ""));
				}
			}
			catch(Exception ex)
			{
				// !! WARN !! in release SocketException through remoting terminates client
				throw new ApplicationException(ex.Message, ex);
			}

			return true;
		}

		// TODO: конфигурирование конкретного терминала через файл
		/// <summary>
		/// Инициализация терминала через список строк
		/// </summary>
		/// <param name="initStrings">
		/// <p>initSrings[0] - название терминала (не используется),</p>
		/// <p>initSrings[1] - IP адрес сервера,</p>
		/// <p>initSrings[2] - порт сервера,</p>
		/// <p>initSrings[3] - тип очереди (0-нет, 1-MSMQ, 2-программная)</p>
		/// </param>
		public override void Initialization(params string[] initStrings)
		{
			try
			{
				base.Initialization(initStrings);

				// длина массива строк инициализации
				if(initStrings.Length < 4) throw new ArgumentException(
					"Неверное количество аргументов инициализации терминала " + Name,
					"Строка" + " инициализации");

				// не пустые параметры массива строк инициализации
				if(initStrings[2] == null || initStrings[3] == null) throw new ArgumentNullException(
					"Неверные аргументы инициализации." + " Один из параметров равен null.");

				try
				{
					Port = int.Parse(initStrings[3]);
					// дополнительные параметры
					if(initStrings.Length > 4) queueType = (QueueType)int.Parse(initStrings[4]);
					if(initStrings.Length > 5) queueSize = int.Parse(initStrings[5]);
					if(initStrings.Length > 6) timerInterval = int.Parse(initStrings[6]);
				}
				catch(FormatException ex)
				{
					throw new FormatException("Неверные аргументы инициализации. " +
						"Один из параметров имеет неверный формат.", ex);
				}

				//
				IPEndPoint ipLocalEndPoint = new IPEndPoint(IPAddress.Any, Port);
				// TODO: check endpoint
				listener = new UDPTransmitter(ipLocalEndPoint);
				listener.ReceiveEvent += new UDPListenerEventHandler(listener_ReceiveEvent);

				//
				if(queueType == QueueType.Program)
				{
					qMsg = Queue.Synchronized(new Queue(queueSize));
					timer = new Timer(ListenerTimerCallBack, null, timerInterval, 0);
				}
				//
				listener.Start();
			}
			catch(Exception ex)
			{
				Trace.WriteLineIf(tsTerm.TraceError, ex.ToString(), tsTerm.Description);
				throw;
			}
		}
		#endregion // ITerminal Members

		#region IDisposable Members

		/// <summary>
		/// Очистка ресурсов
		/// </summary>
		/// <param name="disposing">флаг явной/неявной очистки ресурсов</param>
		protected override void Dispose(bool disposing)
		{
			lock (this)
			{
				// здесь выполняется явная очистка ресурсов (управляемые ресурсы)
				if(disposing)
				{
					listener.Stop();
//#if DEBUG
//                    swByteLog.Close();
//#endif
				}
				// здесь выпоняется неявная очистка (неуправляемые ресурсы)
			}
		}

		#endregion // IDisposable Members

		//int count = 0;
		//int countBad = 0;

		/// <summary>
		/// logger
		/// </summary>
		private readonly StreamWriter swInvalidData;

		List<IDevice> Devices = new List<IDevice>();

		IDevice GetDevice(byte[] data)
		{
			IDevice res = null;
			List<IDevice> devices = new List<IDevice>();
			lock (this.Devices)
			{
				foreach (IDevice dev in this.Devices)
				{
					try
					{
						if (dev.SupportData(data))
						{
							devices.Add(dev);
							res = dev;
						}
					}
					catch(Exception ex)
					{
						//Если парсер сломался - логируем, но не мешаем другим девайсам проверить не их ли сигнал
						Trace.WriteLine("Error during check SupportData - " + ex.ToString());
						Trace.WriteLine(ex.StackTrace);
						Trace.WriteLine(ex.Source);
						Trace.WriteLine(ex.InnerException);
					}
				}
			}

			if (devices.Count > 1)
			{
				Trace.WriteLine("Several devices supports the same data:");
				Trace.WriteLine(BitConverter.ToString(data));
				Trace.WriteLine("devices:");
				foreach (var dev in devices)
				{
					Trace.WriteLine(dev.GetType().FullName);
				}
			}

			if (res == null)
			{
				Trace.WriteLineIf(tsTerm.TraceWarning, "UDP. Unable to create device. Unknown packet format:\n" +
					BitConverter.ToString(data), tsTerm.Description);
			}

			return res;
		}

		public IDevice GetDevice(IUnitInfo ui)
		{
			string objectType = this.itmManager.GetDeviceType(ui);
			if(objectType == null)
				return null;
			lock (this.Devices)
			{
				foreach (IDevice dev in this.Devices)
				{
					if(dev.GetType().ToString().Contains(objectType))
						return dev;
				}
			}
			return null;
		}

		void AddDevice(IDevice dev)
		{
			lock (this.Devices)
			{
				foreach (IDevice device in this.Devices)
				{
					if (device.GetType() == dev.GetType())
						return;
				}
				this.Devices.Add(dev);
			}
		}

		#region listener_ReceiveEvent(object sender, UDPListenerEventArgs e)

		/// <summary>
		/// Делегат события наличия принятых терминалом данных через TCP/IP
		/// </summary>
		/// <param name="sender">объект сервера TCP</param>
		/// <param name="e">датаграмма с информацией о мобильном устройстве</param>
		private void listener_ReceiveEvent(object sender, UDPListenerEventArgs e)
		{
			try
			{
				IList aRes = null;

				byte[] data = e.Data;
				if(data == null || data.Length == 0)
					return;
				IDevice dev;
				try
				{
					////////////////
					//for (int i = 0; i < data.Length; i++ )
					//{
					//    if(data[i]==0xAA && i+16 < data.Length && data[i+16]==0x05 && data[i+11]==1)
					//    {
					//        int aaa = 0;
					//    }
					//}
					//////////////
					dev = this.GetDevice(data);
					if (dev == null)
					{
						dev = itmManager.GetDevice(data);

						if( dev != null )
						{
							this.AddDevice( dev );
						}
					}

					if(dev != null)
					{
						lock(dev)
						{
							dev.Init(CmdType.UDP, CmdType.Unspecified);
							byte[] bufferRest;
							aRes = dev.OnData(data, data.Length, null, out bufferRest);
						}
					}
				}
				catch(Exception ex)
				{
					Trace.WriteLineIf(tsTerm.TraceError, ex.ToString(), tsTerm.Description);
					dev = null;
				}


				if(aRes == null)
					return;

				foreach(object o in aRes)
				{
					IUnitInfo ui = null;
					if(o is IPositionMessage || o is IMobilUnit)
					{
						IPositionMessage msg = o as IPositionMessage;
						IMobilUnit mu;
						if(msg != null)
						{
							mu = msg.MobilUnit;

						}
						else mu = (IMobilUnit)o;
						mu.TermID = id;
						mu.IP = e.EndPoint;

						// CLONE #1
						ui = (IUnitInfo)mu.UnitInfo.Clone();

						OnReceiveCounting(new ReceiveCountingEventArgs(CountingMessageType.GPRS, 0, mu.ID,
							DateTime.Now.Date, 86400, e.Data.Length));

						switch(queueType)
						{
							case QueueType.MSMQ:
								#region not used
								/*
								using(messagequeuetransaction transaction = new messagequeuetransaction())
								{
									transaction.begin();
									using (message message = new message(mu))
									{
										receivedqueue.send(mu, transaction);
									}
									transaction.commit();
								}*/
								#endregion not used
								break;
							case QueueType.Program:
								AddToQueryUDP(mu);
								break;
							default:
								OnReceive(new ReceiveEventArgs(mu));
								break;
						}

						if (ui != null && (ui.Unique != 0 || ui.ID != 0 || ui.Telephone != "") &&
							ui.IP != null && ui.IP.Address != IPAddress.Any)
						{
							lock(this.UintsIpAddresses)
							{
								if (!this.UintsIpAddresses.ContainsKey(ui.ID))
									this.UintsIpAddresses[ui.ID] = new UnitIpTime(ui.IP, DateTime.Now);
								else
									this.UintsIpAddresses[ui.ID] = new UnitIpTime(ui.IP, DateTime.Now);
							}
						}

						#region Отсылка подтверждения
						if(GlobalsConfig.AppSettings["SendConfirmation"] != null
							&& GlobalsConfig.AppSettings["SendConfirmation"].ToLower() == "true")
						{
							IPositionMessage pm = o as IPositionMessage;
							int confirm = 0;
							if(pm != null) confirm = pm.Num;
							if(confirm > 0 && dev != null)
							{
								try
								{
									IStdCommand cmd =
										CommandCreator.CreateCommand(CmdType.Confirm) as IStdCommand;
									cmd.Params[sConfirmNoPar] = confirm;
									if(pm.MobilUnit.cmdType == CmdType.Trace)
										cmd.Params[ConfirmMessTypePar] = "position";
									else
										cmd.Params[ConfirmMessTypePar] = "log";
									cmd.Params["time"] = pm.MobilUnit.Time;
									ui.IP = e.EndPoint;
									cmd.Target = ui;
									SendCommand(cmd);
								}
								catch(Exception ex)
								{
									Trace.WriteLine(ex.ToString());
								}
							}
						}
						#endregion Отсылка подтверждения
					}
					if(o is NotifyEventArgs)
					{
						NotifyEventArgs arg = (NotifyEventArgs)o;
						int confirm = 0;
						object[] par = (object[])arg.Tag;
						switch(arg.Event)
						{
						case TerminalEventMessage.NE_MESSAGE:
						case TerminalEventMessage.NE_FREE:
						case TerminalEventMessage.NE_TO_ORDER:
						case TerminalEventMessage.NE_ORDER:
							ui = par[0] as IUnitInfo;
							if(ui != null) ui = (IUnitInfo)ui.Clone();
							Trace.WriteLineIf(tsTerm.TraceWarning,
								string.Format("Message received from {0}\r\n\t{1}", ui, arg.Message),
								tsTerm.Description);
							Debug.Assert(par.Length == 4, "unexpected param count");
							if(par.Length > 3) confirm = (int)par[3];
							OnNotify(arg, confirm > 0, true);
							#region Отсылка подтверждения
							if (GlobalsConfig.AppSettings["SendConfirmation"] != null
								&& GlobalsConfig.AppSettings["SendConfirmation"].ToLower() == "true")
							{
								IPositionMessage pm = o as IPositionMessage;
								if (pm != null) confirm = pm.Num;
								if (confirm > 0 && dev != null)
								{
									try
									{
										IStdCommand cmd =
											CommandCreator.CreateCommand(CmdType.Confirm) as IStdCommand;
										cmd.Params[sConfirmNoPar] = confirm;
										cmd.Params[ConfirmMessTypePar] = "notify";
										ui.IP = e.EndPoint;
										cmd.Target = ui;
										SendCommand(cmd);
									}
									catch (Exception ex)
									{
										Trace.WriteLine(ex.ToString());
									}
								}
							}
							#endregion Отсылка подтверждения
							break;

						case TerminalEventMessage.NE_ALARM:
							ui = par[0] as IUnitInfo;
							if(ui != null) ui = (IUnitInfo)ui.Clone();
							Trace.WriteLineIf(tsTerm.TraceWarning, String.Format("Alarm received from {0}", ui),
								tsTerm.Description);
							Debug.Assert(par.Length == 4, "unexpected param count");
							if(par.Length > 3) confirm = (int)par[3];
							OnNotify(arg, confirm > 0, true);
							break;

						case TerminalEventMessage.NE_CONFIRM:
							OnNotify(arg, false, true);
							break;

						default:
							OnNotify(arg);
							break;
						}

						if (ui != null && (ui.Unique != 0 || ui.ID != 0 || ui.Telephone != "") &&
							ui.IP != null && ui.IP.Address != IPAddress.Any)
						{
							lock (this.UintsIpAddresses)
							{
								if (!this.UintsIpAddresses.ContainsKey(ui.ID))
									//this.UintsIpAddresses.Add(ui.ID, new UnitIpTime(ui.IP, DateTime.Now));
									this.UintsIpAddresses[ui.ID] = new UnitIpTime(ui.IP, DateTime.Now);
								else
									this.UintsIpAddresses[ui.ID] = new UnitIpTime(ui.IP, DateTime.Now);
							}
						}

						if(confirm > 0 && dev != null)
						{
							try
							{
								IStdCommand cmd = CommandCreator.CreateCommand(CmdType.Confirm) as IStdCommand;
								cmd.Params[sConfirmNoPar] = confirm;
								ui.IP = e.EndPoint;
								cmd.Target = ui;
								SendCommand((ICommand)cmd);
							}
							catch(Exception ex)
							{
								Trace.WriteLine("Try confirm SendCommsnd error: " + ex.ToString());
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				Trace.WriteLine("..Error during translate and dispatch messages - " + ex.ToString());
				Trace.WriteLine(ex.StackTrace);
				Trace.WriteLine(ex.Source);
				Trace.WriteLine(ex.InnerException);
			}
		}
		#endregion // listener_ReceiveEvent(object sender, UDPListenerEventArgs e)

		#region AddToQuery(IMobilUnit mu)
		protected void AddToQueryUDP(IMobilUnit mu)
		{
			try
			{
				qMsg.Enqueue(mu);
				if(qMsg.Count >= queueSize)
					SendReceivedMessage();
			}
			catch (Exception ex)
			{
				Trace.WriteLine(ex.ToString());
				Trace.WriteLine(ex.StackTrace);
			}
		}
		#endregion // AddToQuery(IMobilUnit mu)

		#region ListenerTimerCallBack(object o)
		private void ListenerTimerCallBack(object syncRoot)
		{
			try
			{
				SendReceivedMessage();
			}
			catch (ThreadAbortException)
			{
			}
			catch (Exception e)
			{
				Trace.TraceError("Unexpected error: {0}", e);
			}
		}
		#endregion // ListenerTimerCallBack(object o)

		#region SendReceivedMessage()
		protected void SendReceivedMessage()
		{
			lock(qMsg.SyncRoot)
			{
				try
				{
					if(qMsg.Count == 0) return;

					Trace.WriteLineIf(tsTerm.TraceVerbose, "SendReceivedMessage ... " + qMsg.Count,
						tsTerm.Description);
					IMobilUnit[] mus = new IMobilUnit[qMsg.Count];
					qMsg.CopyTo(mus, 0);
					OnReceive(new ReceiveEventArgs(mus));
					qMsg.Clear();
				}
				catch(Exception ex)
				{
					Trace.WriteLine(ex.ToString());
					Trace.WriteLine(ex.StackTrace);
				}
				finally
				{
					timer.Change(timerInterval, 0);
				}
			}
		}
		#endregion // SendReceivedMessage()

		public override bool CanHandle(IStdCommand cmd)
		{
			lock(this.UintsIpAddresses)
			{
				if( !this.UintsIpAddresses.ContainsKey(cmd.Target.ID))
					return false;

				return this.UintsIpAddresses[cmd.Target.ID].UnitIP.Address != IPAddress.Any &&
				DateTime.Now.AddSeconds(-iLifeTime) < this.UintsIpAddresses[cmd.Target.ID].ConnectionTime;
			}
		}
	}
}