﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Net;
using System.Text;
using System.Threading;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Communication;
using FORIS.TSS.Terminal.Messaging;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Terminal
{
	/// <summary> Базовый класс для всех типов терминалов </summary>
	[Serializable]
	public abstract class _Terminal : Link, ITerminal
	{
		/// <summary> Максимальная скорость, при которой GPS данные считаются некорректными </summary>
		public static int GPSLimitMaxSpeed = int.MaxValue;
		/// <summary> Минимально допустимое кол-во спутников, при котором GPS-позиция считается корректной </summary>
		public static int MinSatellites;
		/// <summary> Объект блокировки ошибок обработки исходящих сообщений </summary>
		protected object ReceiveErrorsRepairTimerLock = new object();
		#region ITerminal Members
		/// <summary> Событие наличия данных у терминала </summary>
		private      ReceiveEventHandler ReceiveEventStorage;
		public event ReceiveEventHandler ReceiveEvent
		{
			add    { ReceiveEventStorage -= value; ReceiveEventStorage += value; }
			remove { ReceiveEventStorage -= value; }
		}
		/// <summary> Событие наличия сообщений у терминала </summary>
		private      NotifyEventHandler NotifyEventStorage;
		public event NotifyEventHandler NotifyEvent
		{
			add    { NotifyEventStorage -= value; NotifyEventStorage += value; }
			remove { NotifyEventStorage -= value; }
		}
		/// <summary> Event of receiving a message needed to count </summary>
		private      ReceiveCountingEventHandler ReceiveCountingEventStorage;
		public event ReceiveCountingEventHandler ReceiveCountingEvent
		{
			add    { ReceiveCountingEventStorage -= value; ReceiveCountingEventStorage += value; }
			remove { ReceiveCountingEventStorage -= value; }
		}

		/// <summary> ID терминала </summary>
		protected int id;
		/// <summary> Название терминала </summary>
		protected string name;
		/// <summary> Очередь для отсылки сообщений </summary>
		protected MessageQueue sendingQueue;
		/// <summary> Очередь принятых сообщений </summary>
		protected MessageQueue receivedQueue;
		/// <summary> Тип очереди для передачи сообщений </summary>
		protected QueueType queueType = QueueType.None;
		/// <summary> Максимальный размер очереди сообщений </summary>
		protected int queueSize = 10;
		/// <summary> Интервал времени, по истечении которого происходит принудительная очистка очереди сообщений </summary>
		protected int timerInterval = 250;
		/// <summary> Таймер для отсылки очереди сообщений клиенту </summary>
		protected System.Threading.Timer timer;
		/// <summary> Terminal manager link </summary>
		protected ITerminalManager itmManager;
		/// <summary> Идентификатор терминала, соответствует Media_Acceptors.MA_ID в БД </summary>
		public virtual int ID
		{
			get { return id; }
		}
		/// <summary> Название терминала </summary>
		public virtual string Name
		{
			get { return name; }
		}
		/// <summary> Очередь для отсылки сообщений </summary>
		public virtual MessageQueue SendingQueue
		{
			get { return sendingQueue; }
		}
		/// <summary> Очередь принятых сообщений </summary>
		public virtual MessageQueue ReceivedQueue
		{
			get { return receivedQueue; }
		}
		/// <summary> Тип очереди для передачи сообщений </summary>
		public QueueType QueueType
		{
			get { return queueType; }
		}
		/// <summary> Максимальный размер очереди сообщений </summary>
		/// <remarks> По достижении этого размера очередь принудительно очищается </remarks>
		public int QueueSize
		{
			get { return queueSize; }
		}
		/// <summary> Интервал времени, по истечении которого происходит принудительная очистка очереди сообщений </summary>
		public int TimerInterval
		{
			get { return timerInterval; }
		}
		/// <summary> Передача команды от терминала мобильному объекту </summary>
		/// <param name="command"> Команда для мобильного объекта </param>
		public bool SendCommand(ICommand command)
		{
			IStdCommand cmd = command as IStdCommand;
			if (cmd == null)
				throw new ArgumentException("Неверный тип параметра", "command");

			// create command message-wrapper for registration in manager
			IMessage msg = MessageCreator.Create(cmd);
			if (msg != null)
			{
				msg.TermID = ID;
				itmManager.AssignMessageNumber(msg);
				cmd.Params["MessageNo"] = msg.Num;
			}
			var result = SendCommand(cmd);
			try
			{
				itmManager.RegisterMessage(msg);
			}
			catch (Exception ex)
			{
				Trace.TraceError(ex.ToString(), "Terminal");
			}

			return result;
		}
		public virtual bool SendCommand(IStdCommand cmd)
		{
			switch (cmd.Type)
			{
				case CmdType.RegisterMobileUnit:
					var mobilUnit = (IMobilUnit)cmd.Target;
					mobilUnit.TermID = ID;
					OnReceive(new ReceiveEventArgs(mobilUnit));
					return true;
				default:
					return false;
			}
		}
		/// <summary> Инициализация терминала через список строк </summary>
		public virtual void Initialization(params string[] initStrings)
		{
			Debug.Assert(initStrings.Length > 0, "Terminal ID not specified");

			if (initStrings.Length == 0)
			{
				return;
			}

			id = Int32.Parse(initStrings[0]);
		}
		#endregion ITerminal Members

		#region IDisposable Members

		/// <summary> Очистка ресурсов </summary>
		public virtual void Dispose()
		{
			GC.SuppressFinalize(this);

			// явная очистка ресурсов
			Dispose(true);
		}

		/// <summary> Очистка ресурсов </summary>
		/// <param name="disposing"> Флаг явной/неявной очистки ресурсов </param>
		protected virtual void Dispose(bool disposing)
		{
		}

		#endregion IDisposable Members

		#region OnReceive(ReceiveEventArgs args)
		/// <summary> Метод вызова события наличия принятых терминалом данных </summary>
		protected virtual void OnReceive(ReceiveEventArgs args)
		{
			if (ReceiveEventStorage == null)
			{
				Trace.TraceWarning("OnReceive execution skipped because of no event listeners attached");
				return;
			}

			var invs = ReceiveEventStorage.GetInvocationList().GetEnumerator();
			try
			{
				while (invs.MoveNext())
				{
					var handler = (ReceiveEventHandler)invs.Current;
					handler(args);
				}
				ContinuousErrorCounter = 0;
			}
			catch (ThreadAbortException)
			{
				throw;
			}
			catch (Exception ex)
			{
				Trace.TraceError("Exception in ReceiveEventHandler: {0}", ex);
				OnReceiveErrors(ex, args);
				throw;
			}
		}
		protected int ContinuousErrorCounter = 0;
		/// <summary> Ошибка обработки (в том числе записи в БД), принятых терминалом данных </summary>
		protected virtual void OnReceiveErrors(Exception ex, ReceiveEventArgs args)
		{
		}
		#endregion OnReceive(ReceiveEventArgs args)

		#region OnNotify(NotifyEventArgs args)

		/// <summary> Метод вызова события наличия сообщений у терминала </summary>
		protected virtual void OnNotify(NotifyEventArgs args)
		{
			if (args != null)
				args.TermID = ID;
			NotifyEventStorage?.Invoke(args);
		}
		protected virtual void OnNotify(NotifyEventArgs args, bool confirm, bool log)
		{
			try
			{
				if (args != null)
					args.TermID = ID;

				if (!(confirm || log))
					return;

				// wrap notification
				IMessage msg = MessageCreator.Create(args);

				if (msg != null)
				{
					msg.TermID  = ID;
					msg.Confirm = confirm;
					msg.Log     = log;
					itmManager.RegisterMessage(msg);
					if (msg.Target        != null &&
						msg.Target.Object != null &&
						msg.Target.Type   == ObjectType.Operator &&
						args.Operator     == null)
						args.Operator = (IOperatorInfo)msg.Target.Object;
				}
			}
			finally
			{
				// if unknown confirm drop it
				if (args.Event              != TerminalEventMessage.NE_CONFIRM ||
					args.Operator           != null                            &&
					args.Operator.OperatorId > 0)
					OnNotify(args);
			}
		}

		#endregion OnNotify(NotifyEventArgs args)

		#region OnReceiveCounting(ReceiveCountingEventArgs rcea)
		/// <summary> method calling when terminal is receiving a message needed to count </summary>
		/// <param name="rcea">event arguments</param>
		protected virtual void OnReceiveCounting(ReceiveCountingEventArgs rcea)
		{
			if (ReceiveCountingEventStorage == null)
				return;

			Delegate[] dInvocationArray = ReceiveCountingEventStorage.GetInvocationList();
			IEnumerator ie = dInvocationArray.GetEnumerator();

			while (ie.MoveNext())
			{
				ReceiveCountingEventHandler rceh = (ReceiveCountingEventHandler)ie.Current;

				rceh.BeginInvoke(rcea, CallBackReceiveCountingEvents, rceh);
			}
		}
		/// <summary> Проверка асинхронного вызова </summary>
		/// <param name="iar"></param>
		void CallBackReceiveCountingEvents(IAsyncResult iar)
		{
			// попытка завершить метод синхронно
			ReceiveCountingEventHandler rceh = (ReceiveCountingEventHandler)iar.AsyncState;
			try
			{
				rceh.EndInvoke(iar);
			}
			catch
			{
				ReceiveCountingEvent -= rceh;
			}
		}
		#endregion OnReceiveCounting(ReceiveCountingEventArgs rcea)

		#region .ctor

		public _Terminal(string name, ITerminalManager mgr)
		{
			this.name = name;
			itmManager = mgr;
		}

		#endregion .ctor

		#region Finalize
		~_Terminal()
		{
			// неявная очистка (неуправляемые ресурсы)
			Dispose(false);
		}
		#endregion Finalize

		/// <summary> Признак возможности обработки команды </summary>
		/// <param name="cmd"> Команда </param>
		/// <returns></returns>
		public virtual bool CanHandle(IStdCommand cmd)
		{
			return true;
		}
		private bool TryForwardSms(IncomingSms sms)
		{
			var smsForwardingUrl = ConfigurationManager.AppSettings["Terminal.Process.SmsForwardingUrl"];
			if (string.IsNullOrWhiteSpace(smsForwardingUrl))
				return false;

			Uri uri;
			if (!Uri.TryCreate(smsForwardingUrl, UriKind.RelativeOrAbsolute, out uri))
			{
				Trace.TraceWarning("{0}.TryForwardSms: unable to parse uri {1}", this, smsForwardingUrl);
				return false;
			}

			try
			{
				var httpRequest = (HttpWebRequest)WebRequest.CreateDefault(uri);
				httpRequest.Method = "POST";
				httpRequest.KeepAlive = true;
				httpRequest.ContentType = "text/plain";

				if (sms.Requestor != null)
				{
					switch (sms.Requestor.Type)
					{
						case ContactType.Phone:
							httpRequest.Headers["X-Phone-Requestor"] = sms.Requestor.Value;
							break;
						default:
							throw new ArgumentException("Wrong sms.Requestor.Type: " + sms.Requestor.Type);
					}
				}

				var requestStream = httpRequest.GetRequestStream();
				byte[] buffer = Encoding.ASCII.GetBytes(sms.Text);
				requestStream.Write(buffer, 0, buffer.Length);
				requestStream.Close();
				var loWebResponse = (HttpWebResponse)httpRequest.GetResponse();

				var responseStream = loWebResponse.GetResponseStream();
				if (responseStream == null)
					return false;

				string answer;
				using (var stringReader = new StreamReader(responseStream))
					answer = stringReader.ReadToEnd();

				return answer == "OK";
			}
			catch (Exception e)
			{
				Trace.TraceError("{0}.TryForwardSms: {1}", this, e);
				return false;
			}
		}
		protected void Process(IncomingSms sms)
		{
			if (TryForwardSms(sms))
				return; //Обработка SMS производится на другом сервере

			//1. Определяет по маскированному номеру тип устройства
			//2. Если устройство не найдено, регистрирует SMS как необработанную входящую.
			//3. Если найдено - пытается разобрать SMS парсером данного устройства.

			if (TryProcessSms(sms))
			{
				RegisterProcessedSms(sms);
			}
			else
			{
				RegisterUnprocessedSms(sms);
			}
		}
		private bool TryProcessSms(IncomingSms sms)
		{
			var ui = UnitInfo.Factory.Create(UnitInfoPurpose.Undefined);

			if (sms.Requestor == null ||
				sms.Requestor.Type == null ||
				sms.Requestor.Value == null)
				return false;

			switch (sms.Requestor.Type.Value)
			{
				case ContactType.Phone:
					ui.Telephone = sms.Requestor.Value;
					break;
				default:
					return false;
			}

			if (!itmManager.FillUnitInfo(ui))
				return false;

			IDevice device;
			try
			{
				device = itmManager.GetDevice(ui);
			}
			catch (Exception exception)
			{
				Trace.TraceError("{0}: GetDevice for {1} failed: {2}", this, sms, exception);
				return false;
			}

			if (device == null)
			{
				Trace.TraceInformation("{0}: No device found for sms: {1}", this, sms);
				return false;
			}

			Trace.TraceInformation("{0}: trying to parse sms {1} using device {2}", this, sms.Text, device);

			IList<object> results;
			try
			{
				results = device.ParseSms(ui, sms.Text);
			}
			catch (Exception e)
			{
				Trace.TraceError("{0}: unable to parse sms {1}, error is: {2}", this, sms, e);
				results = null;
			}
			if (results == null)
				return false;

			//TODO: объединить этот код и код TCPTerminal'а
			var mobilUnits = results.OfType<IMobilUnit>().ToArray();
			foreach (var mu in mobilUnits)
			{
				mu.TermID = ID;
				mu.Media = Media.SMS;
			}

			if (mobilUnits.Length != 0)
				OnReceive(new ReceiveEventArgs(mobilUnits));

			var notifyEvents = results.OfType<NotifyEventArgs>();
			foreach (var item in notifyEvents)
				OnNotify(item);

			return true;
		}
		protected void RegisterProcessedSms(IncomingSms sms)
		{
			RegisterSms(sms.TimeReceived, sms.Requestor, sms.Text, MessageState.Done);
		}
		protected void RegisterUnprocessedSms(IncomingSms sms)
		{
			RegisterSms(sms.TimeReceived, sms.Requestor, sms.Text, MessageState.Wait);
		}
		private void RegisterSms(DateTime time, Contact contact, string text, MessageState state)
		{
			var sms = (ITextMessage)MessageCreator.CreateText();
			if (contact != null && contact.Type != null)
				sms.Sender = new MessageObject(GetObjectType(contact.Type.Value), contact.Value, Direction.Outgoing);
			sms.Target = new MessageObject(ObjectType.Terminal, this, Direction.Incoming);
			sms.Text = text;
			sms.Time = time;
			sms.Log = true;
			sms.TermID = ID;
			sms.State = state;
			itmManager.RegisterMessage(sms);
		}
		private ObjectType GetObjectType(ContactType type)
		{
			switch (type)
			{
				case ContactType.Phone:
					return ObjectType.Phone;
				default:
					throw new NotSupportedException(type.ToString());
			}
		}
	}
}