﻿using FORIS.TSS.TerminalService.Interfaces;
using System;
using System.Diagnostics;
using System.Linq;

namespace FORIS.TSS.Terminal.Terminal.Movireg
{
	public class MoviregTerminal : _Terminal
	{
		protected Terminal.Movireg.Movireg Movireg;

		public MoviregTerminal(ITerminalManager mgr)
			: base("MoviregTerminal", mgr)
		{
		}

		public MoviregTerminal(string name, ITerminalManager mgr)
			: base(name, mgr)
		{
		}

		public override void Initialization(params string[] initStrings)
		{
			try
			{
				base.Initialization(initStrings);
				Movireg = new Movireg(id, itmManager);
				Movireg.OnSendEvent += mobilUnits => PublishMobileUnit(mobilUnits.ToArray());
				Movireg.Start();
			}
			catch (Exception ex)
			{
				Trace.TraceError(ex.ToString());
			}
		}

		protected override void Dispose(bool disposing)
		{
			lock (this)
			{
				// здесь выполняется явная очистка ресурсов (управляемые ресурсы)
				if (disposing)
				{
					// стоп
					Movireg.Stop();
				}
				// здесь выполняется неявная очистка (неуправляемые ресурсы)
			}
		}

		private void PublishMobileUnit(params IMobilUnit[] mobilUnits)
		{
			OnReceive(new ReceiveEventArgs(mobilUnits));
		}
	}
}
