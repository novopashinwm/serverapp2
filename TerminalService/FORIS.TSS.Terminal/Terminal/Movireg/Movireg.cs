﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Compass.Ufin.Movireg.Provider;
using FORIS.DataAccess;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Terminal.Movireg
{
	public class Movireg : BackgroundProcessor
	{
		/// <summary> Номер терминала </summary>
		private readonly int              _terminalId;
		private readonly ITerminalManager _terminalManager;

		public event Action<IEnumerable<IMobilUnit>> OnSendEvent;

		#region General
		private class DEPARTMENT_MOVIREG_ACCOUNT
		{
			public string MOVIREG_SERVER_URL;
			public string MOVIREG_ACCOUNT;
			public string MOVIREG_PASSWORD;
			public int    DEPARTMENT_ID;
			public int    MOVIREG_SERVER_TIMEOFFSET;
		}

		private class VehicleInfo
		{
			public string   devid; // Идентификатор устройства
			public DateTime tm;    // время
			public double   wd;    // широта
			public double   jd;    // долгота
		}

		#endregion
		#region .ctor(s)
		/// <summary> Конструктор </summary>
		/// <param name="terminalId"> Номер терминала </param>
		public Movireg(int terminalId, ITerminalManager terminalManager = null)
		{
			_terminalId      = terminalId;
			_terminalManager = terminalManager;
		}
		#endregion
		#region Overrides
		protected override bool Do()
		{
			var accsInfo = GetMoviregAccountsInfo();
			if (accsInfo == null)
				return false; // Всегда возвращаем false, чтобы между итерациями была пауза

			var vehicles = QueryUserVehicleDetails(accsInfo);

			var mus = new List<MobilUnit.Unit.MobilUnit>();

			foreach (var item in vehicles)
			{
				var mu = new MobilUnit.Unit.MobilUnit
				{
					DeviceID  = item.devid,
					Latitude  = item.wd,
					Longitude = item.jd,
					Time      = item.tm.ToLogTime()
				};

				mu.Properties.Add(DeviceProperty.Protocol, "Movireg");

				mus.Add(mu);
			}

			OnSendEvent?.Invoke(mus);

			return false; // Всегда возвращаем false, чтобы между итерациями была пауза
		}
		#endregion

		#region Base logic
		private List<VehicleInfo> QueryUserVehicleDetails(List<DEPARTMENT_MOVIREG_ACCOUNT> accounts)
		{
			var vehInfoItems = new List<VehicleInfo>();

			foreach (var account in accounts)
			{
				var provider = new MoviregApiProvider(account.MOVIREG_ACCOUNT, account.MOVIREG_PASSWORD, account.MOVIREG_SERVER_URL);

				try
				{
					if (provider.IsLoggedIn || provider.StandardApiAction_login())
					{
						var quvr = provider.StandardApiAction_queryUserVehicle();

						if (quvr.result == 0)
						{
							var vs = provider.StandardApiAction_vehicleStatus(quvr.vehicles.Select(v => v.nm).ToArray());
							if (vs.result == 0)
							{
								foreach (var vehicleItem in quvr.vehicles)
								{
									foreach (var deviceItem in vehicleItem.dl)
									{
										var vsItem = vs.infos.FirstOrDefault(i => i.vi.Equals(vehicleItem.nm));
										if (vsItem == null)
											continue;
										vehInfoItems.Add(new VehicleInfo
										{
											devid = deviceItem.id,
											jd    = vsItem.jd * 0.000001,
											wd    = vsItem.wd * 0.000001,
											tm    = TimeHelper.year1970
												.AddMilliseconds(vsItem.tm)
												//.AddMinutes(-account.MOVIREG_SERVER_TIMEOFFSET)
										});
									}
								}
							}
						}

						if (provider.IsLoggedIn)
							provider.StandardApiAction_logout();
					}
				}
				catch (Exception ex)
				{
					$"{this.GetTypeName(false)} not started"
						.WithException(ex, true)
						.CallTraceError();
				}
			}

			return vehInfoItems;
		}

		private List<DEPARTMENT_MOVIREG_ACCOUNT> GetMoviregAccountsInfo()
		{
			var result = new List<DEPARTMENT_MOVIREG_ACCOUNT>();
			if (Globals.TssDatabaseManager == null)
				return null;

			var sqlAccountsReq = @"
				select
					MOVIREG_SERVER_URL,
					MOVIREG_ACCOUNT,
					MOVIREG_PASSWORD,
					DEPARTMENT_ID,
					MOVIREG_SERVER_TIMEOFFSET
				from dbo.[DEPARTMENT_MOVIREG_ACCOUNT] with(nolock)";

			using var dr = Globals.TssDatabaseManager.DefaultDataBase.ExecuteReader(sqlAccountsReq, CommandType.Text);
			while (dr.Read())
			{
				result.Add(new DEPARTMENT_MOVIREG_ACCOUNT()
				{
					MOVIREG_ACCOUNT           = (string)dr["MOVIREG_ACCOUNT"],
					MOVIREG_PASSWORD          = (string)dr["MOVIREG_PASSWORD"],
					MOVIREG_SERVER_URL        = (string)dr["MOVIREG_SERVER_URL"],
					MOVIREG_SERVER_TIMEOFFSET = (int)dr["MOVIREG_SERVER_TIMEOFFSET"],
					DEPARTMENT_ID             = (int)dr["DEPARTMENT_ID"]
				});
			}

			return result;
		}
		#endregion
	}
}