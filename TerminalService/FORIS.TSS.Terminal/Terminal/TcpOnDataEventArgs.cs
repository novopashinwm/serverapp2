﻿using System;
using System.Net;

namespace FORIS.TSS.Terminal.Terminal
{
	public class TcpOnDataEventArgs : EventArgs
	{
		public IPEndPoint Address { get; set; }
		public byte[]     Data    { get; set; }
	}
}