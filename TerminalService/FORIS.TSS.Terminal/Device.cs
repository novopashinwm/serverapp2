﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Communication;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal
{
	public abstract class Device : IDevice
	{
		private readonly Type _sensorEnumType;

		protected Device(Type sensorEnumType = null, IEnumerable<string> deviceTypeNames = null)
		{
			_sensorEnumType = sensorEnumType;
			if (deviceTypeNames != null)
				_deviceTypeNames.AddRange(deviceTypeNames);
			SupportsShortNumber = true;
		}

		public virtual void Init(CmdType link, CmdType mode)
		{
			Mode = link;
		}

		public virtual void Init(CmdType link, IStdCommand cmd)
		{
		}

		public virtual bool Initialized
		{
			get { return false; }
		}

		public virtual Datagram InputDatagram
		{
			get { throw new NotImplementedException(); }
		}

		public virtual Datagram OutputDatagram(byte[] data, out int begin)
		{
			begin = 0;
			return null;
		}

		public virtual CmdType Mode { get; set; }

		public virtual byte[] InitSeq
		{
			get { return null; }
		}

		public virtual byte[] UninitSeq
		{
			get { return null; }
		}
		public virtual byte[] GetCmd(IStdCommand command)
		{
			return null;
		}

		public virtual void OnLinkOpen(ILink lnk)
		{
		}

		public abstract IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest);

		public virtual void OnNoData()
		{
			throw new NotImplementedException();
		}

		public virtual void OnError()
		{
			throw new NotImplementedException();
		}

		public virtual void OnLinkClose()
		{
			throw new NotImplementedException();
		}

		public abstract bool SupportData(byte[] data);

		public virtual bool SupportData(Datagram dg)
		{
			return false;
		}

		public ITerminalManager Manager { get; set; }

		public virtual IEnumerable<ITerminal> CreateTerminal(CmdType type)
		{
			switch (type)
			{
				case CmdType.AskPosition:
					return Manager.GetLiveTerminals(Media.LBS);
				default:
					return null;
			}
		}

		public virtual byte[] AdjustSettings()
		{
			throw new NotImplementedException();
		}

		public virtual ITerminal CreateTerminal(Media type)
		{
			string terminalName;

			switch (type)
			{
				case Media.SMS:
					terminalName = "SMS";
					break;
				case Media.GPRS:
					terminalName = "TCP";
					break;
				case Media.GSM:
					terminalName = "GSM";
					break;
				default:
					return null;
			}
			return Manager.CreateTerminal(terminalName);
		}

		Type IDevice.SensorEnumType
		{
			get { return _sensorEnumType; }
		}

		private readonly List<string> _deviceTypeNames = new List<string>();

		public IList<string> DeviceTypeNames
		{
			get { return _deviceTypeNames; }
		}

		public virtual IList<object> ParseSms(IUnitInfo ui, string text)
		{
			return null;
		}

		protected virtual IEnumerable<string> GetSetupCommands(
			string ip, string port,
			string apnName, string apnUser, string apnPassword,
			string devicePassword)
		{
			yield break;
		}

		public virtual List<CommandStep> GetCommandSteps(IStdCommand command)
		{
			switch (command.Type)
			{
				case CmdType.Setup:
					{
						var apnCfg = GetInternetApnConfig(command);
						var srvAdd = Manager.GetConstant(Constant.ServerIP);
						var srvPrt = Manager.GetConstant(Constant.ServerPort);

						return GetSetupCommands(srvAdd, srvPrt, apnCfg.Name, apnCfg.User, apnCfg.Pass, command.Target.Password)
							?.Select(x => new SmsCommandStep(x))
							?.ToList<CommandStep>();
					}
				default:
					{
						var commandDataBytes = GetCmd(command);
						if (commandDataBytes == null)
							return null;
						// commandDataBytes не используются, т.к. GetCmd будет вызвана еще раз при исполнении TcpCommandStep
						return new List<CommandStep>
						{
							new TcpCommandStep
							{
								WaitAnswer = command.Type
							}
						};
					}
			}
		}

		protected NotifyEventArgs CommandCompleted(IUnitInfo ui, CmdResult result, CmdType cmdType)
		{
			return new NotifyEventArgs(
				TerminalEventMessage.CommandCompleted, null, Severity.Lvl7, NotifyCategory.Controller, null,
				null, new CommandResult
				{
					CmdType   = cmdType,
					CmdResult = result,
					Tag       = ui
				});
		}

		protected NotifyEventArgs CreateCommandCompletedNotification(string deviceID, CmdType cmdType)
		{
			var ui = UnitInfo.Factory.Create(UnitInfoPurpose.Undefined);
			ui.DeviceID = deviceID;
			if (!Manager.FillUnitInfo(ui))
				return null;

			var notification =
				new NotifyEventArgs(
					TerminalEventMessage.CommandCompleted,
					new CommandResult
					{
						CmdResult = CmdResult.Completed,
						CmdType   = cmdType,
						Tag       = ui
					});

			return notification;
		}

		protected static MobilUnit.Unit.MobilUnit DeviceIdReceived(IUnitInfo ui, string deviceId)
		{
			var mu = new MobilUnit.Unit.MobilUnit(ui)
			{
				CorrectGPS = false,
				Time       = TimeHelper.GetSecondsFromBase()
			};
			mu.Properties.Add(DeviceProperty.IMEI, deviceId);
			return mu;
		}

		protected static MobilUnit.Unit.MobilUnit ImeiReceived(IUnitInfo ui, string imei)
		{
			if (!ImeiHelper.IsImeiCorrect(imei))
				throw new ArgumentOutOfRangeException("imei", imei, "Provided imei is incorrect");

			var mu = new MobilUnit.Unit.MobilUnit(ui)
			{
				CorrectGPS = false,
				Time       = TimeHelper.GetSecondsFromBase()
			};
			mu.Properties.Add(DeviceProperty.IMEI, imei);
			return mu;
		}

		protected MobilUnit.Unit.MobilUnit UnitReceived(string protocol = null)
		{
			var typeName = string.IsNullOrEmpty(protocol) ? GetType().Name : protocol;
			var mobilUnit = new MobilUnit.Unit.MobilUnit();
			mobilUnit.Properties.Add(DeviceProperty.Protocol, typeName);

			return mobilUnit;
		}

		public bool SupportsShortNumber { get; protected set; }

		/// <summary> Возвращает параметры точки доступа для подключения к Интернету </summary>
		/// <param name="command">Команда, чьи параметры используются</param>
		protected InternetApnConfig GetInternetApnConfig(IStdCommand command)
		{
			var result = new InternetApnConfig();

			if (command.Params.ContainsKey(PARAMS.Keys.InternetApn.Name))
			{
				result.Name = (string) command.Params[PARAMS.Keys.InternetApn.Name];
				if (command.Params.ContainsKey(PARAMS.Keys.InternetApn.User))
					result.User = (string) command.Params[PARAMS.Keys.InternetApn.User];
				if (command.Params.ContainsKey(PARAMS.Keys.InternetApn.Password))
					result.Pass = (string) command.Params[PARAMS.Keys.InternetApn.Password];
			}
			else
			{
				result.Name = Manager.GetConstant(Constant.NetworkAPN);
				result.User = Manager.GetConstant(Constant.NetworkAPNUser);
				result.Pass = Manager.GetConstant(Constant.NetworkAPNPassword);
			}
			return result;
		}

		public TimeSpan? MaxIdleTime { get; protected set; }
	}
}