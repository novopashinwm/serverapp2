﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Xml;
using FORIS.DataAccess;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Terminal;
using FORIS.TSS.BusinessLogic.Terminal.Data;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;
using FORIS.TSS.ServerApplication.Diagnostic;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal
{
	/// <summary> Обеспечивает доступ к данным сервера терминалов </summary>
	public class TerminalDatabaseManager : ITerminalDatabaseManager
	{
		public class AddMonitoreeLogContext : IDisposable
		{
			private readonly string _databaseName;
			StoredProcedure  _addSensorValues;
			ConnectionOpener _addSensorValuesConnectionOpener;
			StoredProcedure  _addMonitoreeeLog;
			ConnectionOpener _addMonitoreeLogConnectionOpener;
			StoredProcedure  _spPositionRadius;
			ConnectionOpener _addPositionRadiusConnectionOpener;
			StoredProcedure  _spAddCellNetworkLog;
			ConnectionOpener _spAddCellNetworkLogConnectionOpener;
			StoredProcedure  _spAddWlanLog;
			ConnectionOpener _spAddWlanLogConnectionOpener;
			StoredProcedure  _spAddPictureLog;
			ConnectionOpener _spAddPictureLogConnectionOpener;
			StoredProcedure  _spUpdateInsertTime;
			ConnectionOpener _spUpdateInsertTimeConnectionOpener;

			private string _prefix;

			public AddMonitoreeLogContext(int storageId)
			{
				_databaseName = ((StorageType)storageId).ToString();
			}
			private DataTable _sensorValuesTable;
			public void AddAll(IList<IMobilUnit> mobilUnits)
			{
				if (0 == (mobilUnits?.Count ?? 0))
					return;

				ConnectToDatabase(mobilUnits);

				var minAllowedLogTime = TimeHelper.GetSecondsFromBase(DateTime.UtcNow.AddDays(-180));
				var maxAllowedLogTime = TimeHelper.GetSecondsFromBase(DateTime.UtcNow.AddMinutes(15));

				var vehicleIds = new HashSet<int>();
				var mobilUnitByTimeGroups = mobilUnits
					.Where(mu => mu != null)
					.GroupBy(mu => mu.Time)
					.OrderBy(g => g.Key);
				foreach (var mobilUnitByTimeGroup in mobilUnitByTimeGroups)
				{
					var logTime = mobilUnitByTimeGroup.Key;
					// Пропускаем выходящие за пределы сохранения записи (меняем только счетчик)
					if (logTime < minAllowedLogTime || maxAllowedLogTime < logTime)
					{
						// Уменьшаем счетчик очереди у конкретного обработчика Count, а CountPerSecond не увеличиваем, т.к. обработки не было
						Counters.Instance.Decrement(CounterGroup.Count, Counters.Instances.SaverTask, Thread.CurrentThread.Name, mobilUnitByTimeGroup.Count());
						continue;
					}
					// На данной итерации очищаем сохраненные VehicleIds
					vehicleIds.Clear();
					// Сохраняем по одному объекту наблюдения
					foreach (var mobilUnit in mobilUnitByTimeGroup)
					{
						if (AddMobilUnit(mobilUnit))
							vehicleIds.Add(mobilUnit.Unique);
						// Увеличиваем счетчик производительности общей очереди сохранения CountPerSecond
						Counters.Instance.Increment(CounterGroup.CountPerSecond, Counters.Instances.SaveQueue);
						// Увеличиваем счетчик производительности очереди конкретного обработчика CountPerSecond
						Counters.Instance.Increment(CounterGroup.CountPerSecond, Counters.Instances.SaverTask, Thread.CurrentThread.Name);
						// Уменьшаем счетчик очереди у конкретного обработчика
						Counters.Instance.Decrement(CounterGroup.Count,          Counters.Instances.SaverTask, Thread.CurrentThread.Name);
					}
					// Помечаем завершение записи пакета
					// В одном вызове несколько объектов наблюдения - это нормально, например, от эмулятора
					foreach (var vehicleId in vehicleIds)
						UpdateInsertTime(vehicleId, logTime);
				}
			}
			private bool AddMobilUnit(IMobilUnit mu)
			{
				_prefix = mu.StorageId == (int)StorageType.MSSQL ? "@" : "_";
				bool anythingAdded = false;
				try
				{
					// Сохраняем данные датчиков
					if (AddSensorValues(mu))
						anythingAdded = true;
					// Сохраняем общую информацию
					if (AddCommonLog(mu))
						anythingAdded = true;
					// Сохраняем информацию о сотовых вышках
					// Временно отключаем, т.к. вызывает deadlock
					//if (AddCellNetworkLog(mu))
					//	anythingAdded = true;
					// Сохраняем информацию о сетях WiFi
					// Временно отключаем, т.к. не используется
					//if (AddWlanLog(mu))
					//	anythingAdded = true;
					// Сохраняем радиус (для MLP-координат)
					if (AddPositionRadius(mu))
						anythingAdded = true;
					// Сохраняем изображение
					var pictureDevice = mu as IPictureDevice;
					if (pictureDevice != null)
					{
						if (AddPictureLog(pictureDevice, mu))
							anythingAdded = true;
					}
				}
				catch (Exception ex)
				{
					var muJson = default(string);
					try
					{
						muJson = mu?.ToJson() ?? "Mobile unit is null";
					}
					catch
					{
						muJson = "Mobile unit, serialize exception";
					}
					Trace.TraceError(this.GetTypeName() + ": {0} error for mobile unit '{1}':\n'{2}'\nMessage:\n{3}\nStackTrace:\n{4}",
						nameof(AddMobilUnit),
						mu?.ToString() ?? "Unknown",
						muJson ?? string.Empty,
						ex.Message,
						ex.StackTrace);
				}
				return anythingAdded;
			}
			private readonly Dictionary<int, HashSet<int>> _id2inputs = new Dictionary<int, HashSet<int>>();
			private HashSet<int> GetInputs(IMobilUnit mu)
			{
				HashSet<int> result;
				if (!_id2inputs.TryGetValue(mu.Unique, out result))
				{
					_id2inputs.Add(mu.Unique, result = new HashSet<int>());
					result.UnionWith(mu.SensorMap.Values.SelectMany(x => x).Select(x => x.InputNumber));
				}
				return result;
			}
			private bool AddSensorValues(IMobilUnit mu)
			{
				if (mu.SensorValues == null || mu.SensorValues.Count == 0) 
					return false;

				if (SaveOnlyMappedSensors(mu) && (mu.SensorMap == null || mu.SensorMap.Count == 0))
					return false;

				//TODO: можно вынести за пределы функции
				KeyValuePair<int, long>[] sensorValues;
				if (SaveOnlyMappedSensors(mu))
				{
					var inputs = GetInputs(mu);
					sensorValues = mu.SensorValues.Where(pair => inputs.Contains(pair.Key)).ToArray();
				}
				else
				{
					sensorValues = mu.SensorValues.ToArray();
				}

				if (sensorValues.Length == 0)
					return false;
				
				_addSensorValues[_prefix + "vehicle_id"].Value = mu.Unique;
				_addSensorValues[_prefix + "log_time"].Value = mu.Time;
				if (mu.StorageId == (int) StorageType.MSSQL)
				{
					if (_sensorValuesTable == null)
					{
						_sensorValuesTable = new DataTable("SensorValues");
						_sensorValuesTable.Columns.Add("Number", typeof (int));
						_sensorValuesTable.Columns.Add("Value", typeof (long));
					}
					else
					{
						_sensorValuesTable.Clear();
					}

					foreach (var pair in sensorValues)
						_sensorValuesTable.Rows.Add(pair.Key, pair.Value);

					_addSensorValues.SetTableValuedParameter(_prefix + "sensorValues", _sensorValuesTable);
				}
				else
				{
					_addSensorValues[_prefix + "sensorValues"].Value = sensorValues.ToArray();
				}

				var result = _addSensorValues.ExecuteScalar();

				return (int)result != 0;
			}
			private bool SaveOnlyMappedSensors(IMobilUnit mu)
			{
				return mu.DeviceType != ControllerType.Names.SoftTracker;
			}
			private void UpdateInsertTime(int vehicleId, int logTime)
			{
				_spUpdateInsertTime[_prefix + "vehicle_id"].Value = vehicleId;
				_spUpdateInsertTime[_prefix + "log_time"].Value = logTime;
				_spUpdateInsertTime.ExecuteNonQuery();
			}
			private bool AddPictureLog(IPictureDevice pictureDevice, IMobilUnit mu)
			{
				_spAddPictureLog[_prefix + "vehicle_id"].Value = mu.Unique;
				_spAddPictureLog[_prefix + "log_time"].Value = mu.Time;
				_spAddPictureLog[_prefix + "camera_number"].Value = pictureDevice.CameraNumber;
				_spAddPictureLog[_prefix + "photo_number"].Value = pictureDevice.PhotoNumber;
				_spAddPictureLog[_prefix + "picture"].Value = pictureDevice.Bytes;
				_spAddPictureLog[_prefix + "url"].Value = pictureDevice.Url;
				_spAddPictureLog.ExecuteNonQuery();

				return true;
			}
			private bool AddPositionRadius(IMobilUnit mu)
			{
				if (mu.Radius == null) 
					return false;

				_spPositionRadius[_prefix + "log_time"].Value   = mu.Time;
				_spPositionRadius[_prefix + "vehicle_id"].Value = mu.Unique;
				_spPositionRadius[_prefix + "radius"].Value     = mu.Radius;
				_spPositionRadius[_prefix + "type"].Value       = mu.Type;

				var rowsAffected = _spPositionRadius.ExecuteScalar();

				return (int)rowsAffected != 0;
			}
			private bool AddWlanLog(IMobilUnit mu)
			{
				if (mu.Wlans == null || mu.Wlans.Length == 0) 
					return false;

				var totalRowsAffected = 0;

				foreach (var wlan in mu.Wlans)
				{
					_spAddWlanLog[_prefix + "Vehicle_ID"].Value = mu.Unique;
					_spAddWlanLog[_prefix + "Log_Time"].Value = mu.Time;
					_spAddWlanLog[_prefix + "Number"].Value = (byte) wlan.Number;
					string wlanMacAddress = wlan.WlanMacAddress;
					if (wlanMacAddress != null)
					{
						wlanMacAddress = wlanMacAddress.Replace(":", "");
					}
					_spAddWlanLog[_prefix + "WLAN_MAC_Address"].Value = wlanMacAddress;
					_spAddWlanLog[_prefix + "SignalStrength"].Value = (byte) wlan.SignalStrength;
					_spAddWlanLog[_prefix + "WLAN_SSID"].Value = wlan.WlanSSID;
					_spAddWlanLog[_prefix + "Channel_Number"].Value = wlan.ChannelNumber;
					var rowsAffected = (int)_spAddWlanLog.ExecuteScalar();

					totalRowsAffected += rowsAffected;
				}

				return totalRowsAffected != 0;
			}
			private bool AddCellNetworkLog(IMobilUnit mu)
			{
				if (mu.CellNetworks == null || mu.CellNetworks.Length == 0) 
					return false;

				var rowsAffected = 0;
				foreach (var cellNetwork in mu.CellNetworks)
				{
					_spAddCellNetworkLog[_prefix + "Vehicle_ID"].Value = mu.Unique;
					_spAddCellNetworkLog[_prefix + "Log_Time"].Value = cellNetwork.LogTime;
					_spAddCellNetworkLog[_prefix + "Number"].Value = (byte)cellNetwork.Number;
					_spAddCellNetworkLog[_prefix + "CountryCode"].Value = cellNetwork.CountryCode;
					_spAddCellNetworkLog[_prefix + "NetworkCode"].Value = cellNetwork.NetworkCode;
					_spAddCellNetworkLog[_prefix + "Cell_ID"].Value = cellNetwork.CellID;
					_spAddCellNetworkLog[_prefix + "LAC"].Value = cellNetwork.LAC;
					_spAddCellNetworkLog[_prefix + "SAC"].Value = cellNetwork.SAC;
					_spAddCellNetworkLog[_prefix + "ECI"].Value = cellNetwork.ECI;
					_spAddCellNetworkLog[_prefix + "SignalStrength"].Value =
						(Int16) cellNetwork.SignalStrength;
					var result = (int)_spAddCellNetworkLog.ExecuteScalar();

					rowsAffected += result;
				}

				return rowsAffected != 0;
			}
			private static void AppendByteAsDecimal(StringBuilder sb, byte b)
			{
				if (b < 100)
					sb.Append('0');
				if (b < 10)
					sb.Append('0');
				sb.Append(b);
			}
			private static string Canonize(IPAddress ip)
			{
				var bytes = ip.GetAddressBytes();
				if (bytes.Length == 4)
				{
					var sb = new StringBuilder();
					AppendByteAsDecimal(sb, bytes[0]);
					sb.Append('.');
					AppendByteAsDecimal(sb, bytes[1]);
					sb.Append('.');
					AppendByteAsDecimal(sb, bytes[2]);
					sb.Append('.');
					AppendByteAsDecimal(sb, bytes[3]);
					return sb.ToString();
				}

				//TODO: implement
				return ip.ToString();
			}
			private bool AddCommonLog(IMobilUnit mu)
			{
				_addMonitoreeeLog[_prefix + "uniqueID"].Value = mu.Unique;
				_addMonitoreeeLog[_prefix + "log_time"].Value = mu.Time;
				_addMonitoreeeLog[_prefix + "long"].Value = mu.Longitude;
				_addMonitoreeeLog[_prefix + "lat"].Value = mu.Latitude;
				_addMonitoreeeLog[_prefix + "speed"].Value = mu.Speed;
				_addMonitoreeeLog[_prefix + "course"].Value = mu.Course;
				_addMonitoreeeLog[_prefix + "media"].Value = mu.TermID;

				_addMonitoreeeLog[_prefix + "satellites"].Value = mu.Satellites;
				_addMonitoreeeLog[_prefix + "firmware"].Value = mu.Firmware;

				_addMonitoreeeLog[_prefix + "height"].Value = mu.Height;

				if (mu.Type == PositionType.GPS &&
					mu.SensorMap.ContainsKey(SensorLegend.HDOP))
				{
					var hdop = mu.GetSensorValue(SensorLegend.HDOP);
					if (hdop == null || 99.99m < hdop.Value)
						hdop = 99.99m;
					_addMonitoreeeLog[_prefix + "hdop"].Value = hdop;
				}
				else
				{
					_addMonitoreeeLog[_prefix + "hdop"].Value = null;
				}

				_addMonitoreeeLog[_prefix + "validPosition"].Value = mu.ValidPosition;
				_addMonitoreeeLog[_prefix + "ip"].Value = mu.IP != null ? Canonize(mu.IP.Address) : null;
				if (mu.Properties.ContainsKey(DeviceProperty.Protocol))
					_addMonitoreeeLog[_prefix + "protocol"].Value = mu.Properties[DeviceProperty.Protocol];
				else
					_addMonitoreeeLog[_prefix + "protocol"].Value = null;

				var affected = (int)_addMonitoreeeLog.ExecuteScalar();
				return affected > 0;
			}
			private void ConnectToDatabase(IList<IMobilUnit> mobilUnits)
			{
				if (mobilUnits.Any(mu => mu != null))
				{
					_addMonitoreeeLog = Globals.TssDatabaseManager[_databaseName].Procedure("AddMonitoreeLog");
					_addMonitoreeLogConnectionOpener = new ConnectionOpener(_addMonitoreeeLog.Command.Connection);

					_spUpdateInsertTime = Globals.TssDatabaseManager[_databaseName].Procedure("UpdateInsertTime");
					_spUpdateInsertTimeConnectionOpener = new ConnectionOpener(_spUpdateInsertTime.Command.Connection);
				}
				
				if (mobilUnits.Any(mu => mu != null && mu.SensorValues != null && mu.SensorValues.Count != 0))
				{
					_addSensorValues = Globals.TssDatabaseManager[_databaseName].Procedure("AddSensorValues");
					_addSensorValuesConnectionOpener = new ConnectionOpener(_addSensorValues.Command.Connection);
				}


				if (mobilUnits.Any(mu => mu != null && mu.Radius != null))
				{
					_spPositionRadius = Globals.TssDatabaseManager[_databaseName].Procedure("AddPositionRadius");
					_addPositionRadiusConnectionOpener = new ConnectionOpener(_spPositionRadius.Command.Connection);
				}

				if (mobilUnits.Any(mu => mu != null && mu.CellNetworks != null && mu.CellNetworks.Length != 0))
				{
					_spAddCellNetworkLog = Globals.TssDatabaseManager[_databaseName].Procedure("AddCell_Network_Log");
					_spAddCellNetworkLogConnectionOpener = new ConnectionOpener(_spAddCellNetworkLog.Command.Connection);
				}

				if (mobilUnits.Any(mu => mu != null && mu.Wlans != null && mu.Wlans.Length != 0))
				{
					_spAddWlanLog = Globals.TssDatabaseManager[_databaseName].Procedure("AddWLAN_Log");
					_spAddWlanLogConnectionOpener = new ConnectionOpener(_spAddWlanLog.Command.Connection);
				}

				if (mobilUnits.Any(mu => mu is IPictureDevice))
				{
					_spAddPictureLog = Globals.TssDatabaseManager[_databaseName].Procedure("AddPictureLog");
					_spAddPictureLogConnectionOpener = new ConnectionOpener(_spAddPictureLog.Command.Connection);
				}
			}
			public void Dispose()
			{
				_spAddPictureLogConnectionOpener?.Dispose();
				_spAddPictureLog?.Dispose();

				_spAddWlanLogConnectionOpener?.Dispose();
				_spAddWlanLog?.Dispose();

				_spAddCellNetworkLogConnectionOpener?.Dispose();
				_spAddCellNetworkLog?.Dispose();

				_addPositionRadiusConnectionOpener?.Dispose();
				_spPositionRadius?.Dispose();

				_addSensorValues?.Dispose();
				_addSensorValuesConnectionOpener?.Dispose();

				_addMonitoreeLogConnectionOpener?.Dispose();
				_addMonitoreeeLog?.Dispose();

				_spUpdateInsertTime?.Dispose();
				_spUpdateInsertTimeConnectionOpener?.Dispose();
			}
		}
		public void AddMonitoreeLog(IList<IMobilUnit> mobilUnits, int storageId = 1)
		{
			using (var context = new AddMonitoreeLogContext(storageId))
			using (new Stopwatcher("Saving mobile units".CallTraceMessage(),
				elapsed => $@"{Counters.Instances.SaverTask}_{Thread.CurrentThread.Name}: Saving of {mobilUnits.Count} mobile units speed {mobilUnits.Count / elapsed.TotalSeconds} ups"))
			{
				context.AddAll(mobilUnits);
			}
		}
		public SortedList<int, IMobilUnit> GetLogForController(int from, int to, int id, int interval, int count)
		{
			var sl = new SortedList<int, IMobilUnit>();
			IMobilUnit lastMU = null;
			try
			{
				using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetLogForController"))
				{
					sp["@count"].Value         = count;
					sp["@interval"].Value      = interval;
					sp["@from"].Value          = from;
					sp["@to"].Value            = to;
					sp["@controller_id"].Value = id;
					using (new ConnectionOpener(sp.Command.Connection))
					{
						IDataReader dr = sp.ExecuteReader();
						try
						{
							for (int i = 0; dr.Read(); i++)
							{
								var mu = new MobilUnit.Unit.MobilUnit();
								mu.Unique    = (int)dr["MONITOREE_ID"];
								mu.Time      = (int)dr["LOG_TIME"];
								mu.Speed     = dr["SPEED"] as byte? ?? 0;
								mu.Longitude = Convert.ToDouble(dr["X"]);
								mu.Latitude  = Convert.ToDouble(dr["Y"]);
								// температура
								mu.VoltageAN1 = dr["T"] != DBNull.Value ? (int)dr["T"] : int.MaxValue;
								// расход топлива
								mu.VoltageAN4 = dr["F"] != DBNull.Value ? (int)dr["F"] : 0;

								if (!(mu.Latitude < 090f && mu.Longitude < 180f))
									mu.CorrectGPS = false;
								else
									mu.CorrectGPS = true;

								// считаем курс
								if (lastMU != null && mu.ValidPosition)
								{
									if (mu.Speed > 0 &&
										Math.Abs(lastMU.Longitude - mu.Longitude) < 0.0000001 &&
										Math.Abs(lastMU.Latitude  - mu.Latitude)  < 0.0000001 &&
										lastMU.Speed == mu.Speed)
										mu.CorrectGPS = false;
									else
										mu.CalcCourse(lastMU);
								}
								lastMU      = mu;
								sl[mu.Time] = mu;
							}
						}
						finally
						{
							dr.Close();
						}
					}
				}
			}
			catch (Exception ex)
			{
				sl.Clear();

				Trace.TraceError(
					GetType().Name + ".GetLogForController(controllerId={0}) exception: {1} \r\n {2}",
					id,
					ex.Message,
					ex.StackTrace);
				throw;
			}
			return sl;
		}
		public DataForLocator GetDataForLocator()
		{
			using (var sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForLocator"))
			using (new ConnectionOpener(sp))
			{
				var reader = sp.ExecuteReader();

				var result = new DataForLocator();

				var vehicleIdOrdinal   = reader.GetOrdinal("Vehicle_ID");
				var cellLogTimeOrdinal = reader.GetOrdinal("Cell_Log_Time");
				var wlanLogTimeOrdinal = reader.GetOrdinal("Wlan_Log_Time");

				if (!reader.Read())
					return null;

				result.VehicleId = reader.GetInt32(vehicleIdOrdinal);
				var cellLogTime  = reader.IsDBNull(cellLogTimeOrdinal)
					? (int?)null
					: reader.GetInt32(cellLogTimeOrdinal);
				var wlanLogTime  = reader.IsDBNull(wlanLogTimeOrdinal)
					? (int?)null
					: reader.GetInt32(wlanLogTimeOrdinal);

				if (!reader.NextResult())
					return result;

				result.Cells = ReadCellsForLocator(reader, cellLogTime);
				if (!reader.NextResult())
					return result;
				result.Wlans = ReadWlansForLocator(reader, wlanLogTime);

				return result;
			}
		}
		public IEnumerable<Position> GetLastPositions()
		{
			if (Globals.TssDatabaseManager == null)
				yield break;

			using (var sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetLastPositions"))
			using (new ConnectionOpener(sp))
			using (var reader = sp.ExecuteReader())
			{
				var ordinalVehicleId = reader.GetOrdinal("MONITOREE_ID");
				var ordinalLogTime   = reader.GetOrdinal("LOG_TIME");
				var ordinalLat       = reader.GetOrdinal("Y");
				var ordinalLng       = reader.GetOrdinal("X");
				var ordinalSpeed     = reader.GetOrdinal("SPEED");
				var ordinalCourse    = reader.GetOrdinal("Course");

				while (reader.Read())
				{
					int vehicleId = reader.GetInt32(ordinalVehicleId);
					int time      = reader.GetInt32(ordinalLogTime);
					var latitude  = (double)reader.GetDecimal(ordinalLat);
					var longitude = (double)reader.GetDecimal(ordinalLng);
					var speed     = reader.IsDBNull(ordinalSpeed)  ? (int?)null : reader.GetByte(ordinalSpeed);
					var course    = reader.IsDBNull(ordinalCourse) ? (int?)null : reader.GetInt32(ordinalCourse);

					yield return new Position(vehicleId, time, longitude, latitude, speed, course);
				}
			}
		}
		public DataForNetmonitoring GetDataForNetmonitoring()
		{
			using (var sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetDataForNetmonitoring"))
			using (new ConnectionOpener(sp))
			{
				var reader = sp.ExecuteReader();

				if (!reader.Read())
					return null;

				var uuid = reader.GetGuid(reader.GetOrdinal("Uuid"));
				var result = new DataForNetmonitoring { Uuid = uuid };

				if (!reader.NextResult())
					return result;

				result.Positions = ReadPositionsForNetMonitoring(reader);

				if (!reader.NextResult())
					return result;

				result.Cells = ReadCellsForNetmonitoring(reader).ToLookup(p => p.Key, p => p.Value);

				if (!reader.NextResult())
					return result;

				result.Wlans = ReadWlansForNetmonitoring(reader).ToLookup(p => p.Key, p => p.Value);

				return result;
			}
		}
		public string GetConstant(string constant)
		{
			var tssManager = Globals.TssDatabaseManager;
			if (tssManager == null)
				return null;
			using (var sp = tssManager.DefaultDataBase.Procedure("dbo.GetConstant"))
			using (new ConnectionOpener(sp))
			{
				sp["@NAME"].Value = constant;
				using (var reader = sp.ExecuteReader())
				{
					if (!reader.Read() || reader.IsDBNull(0))
						return null;
					return reader.GetString(0);
				}
			}
		}
		private List<WlanRecord> ReadWlansForLocator(IDataReader reader, int? wlanLogTime)
		{
			if (wlanLogTime == null)
				return null;

			var ssidOrdinal = reader.GetOrdinal("SSID");
			var macAddressOrdinal = reader.GetOrdinal("Mac_Address");
			var channelNumberOrdinal = reader.GetOrdinal("Channel_Number");
			var signalStrengthOrdinal = reader.GetOrdinal("SignalStrength");

			var result = new List<WlanRecord>();

			while (reader.Read())
			{
				var wlanSsid       = reader.GetString(ssidOrdinal);
				var wlanMacAddress = reader.GetString(macAddressOrdinal);
				var channelNumber  = reader.GetByte(channelNumberOrdinal);
				var signalStrength = reader.GetByte(signalStrengthOrdinal);
				result.Add(new WlanRecord
				{
					LogTime        = wlanLogTime.Value,
					WlanSSID       = wlanSsid,
					WlanMacAddress = wlanMacAddress,
					ChannelNumber  = channelNumber,
					SignalStrength = signalStrength
				});
			}

			return result;
		}
		private List<CellNetworkRecord> ReadCellsForLocator(IDataReader reader, int? logTime)
		{
			if (logTime == null)
				return null;

			var countryCodeOrdinal    = reader.GetOrdinal("Country_Code");
			var networkCodeOrdinal    = reader.GetOrdinal("Network_Code");
			var lacOrdinal            = reader.GetOrdinal("LAC");
			var cellIdOrdinal         = reader.GetOrdinal("Cell_ID");
			var signalStrengthOrdinal = reader.GetOrdinal("SignalStrength");

			var result = new List<CellNetworkRecord>();

			while (reader.Read())
			{
				var countryCode    = reader.GetString(countryCodeOrdinal);
				var networkCode    = reader.GetString(networkCodeOrdinal);
				var lac            = reader.GetInt32(lacOrdinal);
				var cellID         = reader.GetInt32(cellIdOrdinal);
				var signalStrength = reader.GetInt16(signalStrengthOrdinal);

				result.Add(new CellNetworkRecord
				{
					CountryCode    = countryCode,
					NetworkCode    = networkCode,
					LAC            = lac,
					CellID         = cellID,
					SignalStrength = signalStrength,
					LogTime        = logTime.Value
				});
			}

			if (result.Count == 0)
				return null;

			return result;
		}
		private List<DataForNetmonitoring.Position> ReadPositionsForNetMonitoring(IDataReader reader)
		{
			var logTimeOrdinal = reader.GetOrdinal("Log_Time");
			var latOrdinal = reader.GetOrdinal("Lat");
			var lngOrdinal = reader.GetOrdinal("Lng");
			var speedOrdinal = reader.GetOrdinal("Speed");
			var courseOrdinal = reader.GetOrdinal("Course");

			var result = new List<DataForNetmonitoring.Position>();

			while (reader.Read())
			{
				var logTime = reader.GetInt32(logTimeOrdinal);
				var lat = reader.GetDecimal(latOrdinal);
				var lng = reader.GetDecimal(lngOrdinal);
				var speed = reader.GetInt32(speedOrdinal);
				var course = reader.GetInt32(courseOrdinal);

				result.Add(new DataForNetmonitoring.Position
				{
					LogTime = logTime,
					Lat     = lat,
					Lng     = lng,
					Speed   = speed,
					Course  = course
				});
			}

			return result;
		}
		private IEnumerable<KeyValuePair<int, WlanRecord>> ReadWlansForNetmonitoring(IDataReader reader)
		{
			var geoLogTimeOrdinal = reader.GetOrdinal("Geo_Log_Time");
			var logTimeOrdinal = reader.GetOrdinal("Log_Time");
			var ssidOrdinal = reader.GetOrdinal("SSID");
			var macAddressOrdinal = reader.GetOrdinal("Mac_Address");
			var channelNumberOrdinal = reader.GetOrdinal("Channel_Number");
			var signalStrengthOrdinal = reader.GetOrdinal("SignalStrength");

			while (reader.Read())
			{
				var geoLogTime = reader.GetInt32(geoLogTimeOrdinal);
				var logTime = reader.GetInt32(logTimeOrdinal);
				var wlanSsid = reader.GetString(ssidOrdinal);
				var wlanMacAddress = reader.GetString(macAddressOrdinal);
				var channelNumber = reader.GetByte(channelNumberOrdinal);
				var signalStrength = reader.GetByte(signalStrengthOrdinal);

				yield return new KeyValuePair<int, WlanRecord>(
					geoLogTime,
					new WlanRecord
					{
						LogTime        = logTime,
						WlanSSID       = wlanSsid,
						WlanMacAddress = wlanMacAddress,
						ChannelNumber  = channelNumber,
						SignalStrength = signalStrength
					});
			}
		}
		private IEnumerable<KeyValuePair<int, CellNetworkRecord>> ReadCellsForNetmonitoring(IDataReader reader)
		{
			var geoLogTimeOrdinal = reader.GetOrdinal("Geo_Log_Time");
			var logTimeOrdinal = reader.GetOrdinal("Log_Time");
			var countryCodeOrdinal = reader.GetOrdinal("Country_Code");
			var networkCodeOrdinal = reader.GetOrdinal("Network_Code");
			var lacOrdinal = reader.GetOrdinal("LAC");
			var cellIdOrdinal = reader.GetOrdinal("Cell_ID");
			var signalStrengthOrdinal = reader.GetOrdinal("SignalStrength");
			
			while (reader.Read())
			{
				var geoLogTime = reader.GetInt32(geoLogTimeOrdinal);
				var logTime = reader.GetInt32(logTimeOrdinal);
				var countryCode = reader.GetString(countryCodeOrdinal);
				var networkCode = reader.GetString(networkCodeOrdinal);
				var lac = reader.GetInt32(lacOrdinal);
				var cellID = reader.GetInt32(cellIdOrdinal);
				var signalStrength = reader.GetInt16(signalStrengthOrdinal);
			
				yield return new KeyValuePair<int, CellNetworkRecord>(
					geoLogTime,
					new CellNetworkRecord
					{
						LogTime        = logTime,
						CountryCode    = countryCode,
						NetworkCode    = networkCode,
						LAC            = lac,
						CellID         = cellID,
						SignalStrength = signalStrength,
					});
			}
		}
		private static readonly DataContractSerializer TrackingScheduleSerializer = new DataContractSerializer(typeof(TrackingSchedule));
		public List<TrackingSchedule> GetTrackingSchedule(int vehicleId)
		{
			using (var sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetTrackingSchedule"))
			using (new ConnectionOpener(sp))
			{
				sp["@vehicle_id"].Value = vehicleId;
				using (var reader = sp.ExecuteReader())
				{
					var idOrdinal = reader.GetOrdinal("id");
					var configOrdinal = reader.GetOrdinal("config");
					
					var result = new List<TrackingSchedule>();

					while (reader.Read())
					{
						var config = reader.GetString(configOrdinal);
						var xmlReader = new XmlTextReader(new System.IO.StringReader(config));
						var trackingSchedule = (TrackingSchedule)TrackingScheduleSerializer.ReadObject(xmlReader);
						trackingSchedule.Id = reader.GetInt32(idOrdinal);
						trackingSchedule.Enabled = true;
						result.Add(trackingSchedule);
					}

					return result;
				}
			}
		}
		/// <summary> Пытается продлить действие услуги на объекте наблюдения до указанной даты </summary>
		/// <remarks> В случае возможности и необходимости, происходит списание услуги
		/// Возможность = наличие доступного остатка у владельца объекта
		/// Необходимость = услуга отсутствует или срок действия оплаты истек
		/// </remarks>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <param name="paidTill"> Предлагаемая новая дата, на которую начнет действовать оплата </param>
		/// <returns> В случае успешной оплаты (в этот момент или ранее) возвращает дату, по которую действует оплата </returns>
		public DateTime? TryContinueVehicleService(int vehicleId, DateTime paidTill)
		{
			using (var sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.TryContinueVehicleService"))
			using (new ConnectionOpener(sp))
			{
				sp["@vehicle_id"].Value = vehicleId;
				sp["@paidTill"].Value = paidTill;
				using (var reader = sp.ExecuteReader())
				{
					if (!reader.Read())
						return null;
					return reader.GetDateTimeOrNull(0);
				}
			}
		}
	}
}