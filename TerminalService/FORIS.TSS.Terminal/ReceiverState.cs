﻿using System;

namespace FORIS.TSS.Terminal
{
	[Serializable]
	public class ReceiverState
	{
		public string DeviceID;

		public override string ToString()
		{
			return "DeviceID: " + (DeviceID ?? "null");
		}
	}
}