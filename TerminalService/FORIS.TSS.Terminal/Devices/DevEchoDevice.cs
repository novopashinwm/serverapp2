﻿using System.Collections;
using System.Linq;
using System.Text;
using FORIS.TSS.Helpers;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Devices
{
	public class DevEchoDevice : Device
	{
		private readonly byte[] _deviceSign = Encoding.ASCII.GetBytes("/dev/echo");
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;
			return new ArrayList { new ConfirmPacket(data.Take(count).ToArray()) };
		}
		public override bool SupportData(byte[] data)
		{
			return data.StartsWith(_deviceSign);
		}
	}
}