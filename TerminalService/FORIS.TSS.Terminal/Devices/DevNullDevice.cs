﻿using System.Collections;
using System.Text;
using FORIS.TSS.Helpers;

namespace FORIS.TSS.Terminal.Devices
{
	public class DevNullDevice : Device
	{
		private readonly byte[] _deviceSign = Encoding.ASCII.GetBytes("/dev/null");
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;
			return new ArrayList();
		}
		public override bool SupportData(byte[] data)
		{
			return data.StartsWith(_deviceSign);
		}
	}
}