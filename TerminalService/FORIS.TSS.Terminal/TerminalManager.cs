﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Remoting;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FORIS.DataAccess;
using FORIS.Extension;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.BusinessLogic.Terminal.Data;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Caching;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Config;
using FORIS.TSS.ServerApplication.Diagnostic;
using FORIS.TSS.Terminal.Emulator;
using FORIS.TSS.Terminal.GSM;
using FORIS.TSS.Terminal.Messaging;
using FORIS.TSS.Terminal.SMS;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.Terminal.Terminal.Ftp;
using FORIS.TSS.Terminal.Terminal.HTTP;
using FORIS.TSS.Terminal.Terminal.Movireg;
using FORIS.TSS.Terminal.Terminal.Publish;
using FORIS.TSS.Terminal.UDP;
using FORIS.TSS.TerminalService.Interfaces;
using FORIS.TSS.TerminalService.Interfaces.Exceptions;

namespace FORIS.TSS.Terminal
{
	/// <summary> Менеджер терминалов </summary>
	public class TerminalManager : MarshalByRefObject, ITerminalManager, IDisposable
	{
		readonly TraceSwitch _tsTerm = new TraceSwitch("Terminal", "TmMgr");

		/// <summary> Информация о конфигурациях терминалов сервера </summary>
		private Dictionary<string, TerminalConfig> Terminals { get; set; }
		/// <summary> Список запущенных терминалов </summary>
		readonly Dictionary<string, ITerminal> _liveTerminals = new Dictionary<string, ITerminal>();

		/// <summary>
		/// list of information about received messages needed to count;
		/// keys - ReceiveCountingEventArgs object,
		/// values - number of messages received from senders
		/// </summary>
		readonly Hashtable _hCounting = new Hashtable();

		/// <summary> timer for sending counting information to db </summary>
		Timer _tCounting;

		/// <summary> Менеджер базы данных </summary>
		public TerminalDatabaseManager DbManager
		{
			get { return _dbManager; }
		}

		/// <summary> event of receiving a message needed to count </summary>
		private      AnonymousEventHandler<SaveCountingListEventArgs> saveCountingListEventHandler;
		public event AnonymousEventHandler<SaveCountingListEventArgs> SaveCountingListEvent
		{
			add    { saveCountingListEventHandler -= value; saveCountingListEventHandler += value; }
			remove { saveCountingListEventHandler -= value; }
		}

		/// <summary> Возвращает отсортированный по времени список последних позиций ТС из БД за указанный период времени </summary>
		/// <param name="from"> начало периода </param>
		/// <param name="to"> конец периода </param>
		/// <param name="id"> идентификатор контроллера </param>
		/// <param name="interval"> интервал между позициями в секундах </param>
		/// <param name="count"> количество запрашиваемых позиций </param>
		/// <returns> отсортированный по времени список последних позиций ТС </returns>
		public SortedList<int, IMobilUnit> GetLogFromDB(int from, int to, int id, int interval, int count)
		{
			return DbManager.GetLogForController(from, to, id, interval, count);
		}

		#region .ctor

		/// <summary> Менеджер терминалов </summary>
		public TerminalManager()
		{
			InitializeComponent();

			_checkVehiclePayment      = ConfigHelper.GetAppSettingAsBoolean("TerminalManager.CheckVehiclePayment");
			_savingMobilUnitsBulkSize = ConfigHelper.GetAppSettingAsInt32("TerminalManager.SaveMobilUnits.BulkSize", 1000);
			_mobilUnitSavers          = new Task[ConfigHelper.GetAppSettingAsInt32("TerminalManager.MobilUnitSaverCount", 4)];

			_onReceiveInvoker = new BulkProcessor<IMobilUnit>(BulkProcessReceiveEvents, _savingMobilUnitsBulkSize);
			_onReceiveInvoker.Start();

			_onNotifyInvoker = new BulkProcessor<NotifyEventArgs>(BulkProcessNotifyEvents, _savingMobilUnitsBulkSize);
			_onNotifyInvoker.Start();

			_dbManager = new TerminalDatabaseManager();

			MobilUnit.Unit.MobilUnit.SetLimits();

			// Загрузить последние позиции объектов из БД
			LoadLastPositions();
			// Загрузить конфигурации терминалов из БД
			Terminals = LoadTerminals();
			// Загрузить сборки парсеров(плагинов) устройств
			LoadDevices();
			// Если нужно, то обновить время вставки
			if (ConfigHelper.GetAppSettingAsBoolean("TerminalManager.Start.UpdateInsertTime"))
				UpdateInsertTime();
			// Запустить таймер (TODO: разобраться для чего он нужен)
			_tmMessage = new Timer(CheckMessages, null, IMsgChkPeriod * 1000, IMsgChkPeriod * 1000);
		}
		public void AddSavingMobileQueue(int storageId)
		{
			_storageQueues.Add(storageId, new BlockingCollection<List<IMobilUnit>>());
			for (var i = 0; i != _mobilUnitSavers.Length; ++i)
			{
				_mobilUnitSavers[i] = new Task(SaveMobilUnits, new Tuple<int, string>(storageId, $@"{i+1:00}"), TaskCreationOptions.LongRunning);
				_mobilUnitSavers[i].Start();
			}
		}
		private bool SaveMobilUnits(int storageId, List<IMobilUnit> mobilUnits)
		{
			// Уменьшаем счетчик общей очереди сохранения
			Counters.Instance.Decrement(CounterGroup.Count, Counters.Instances.SaveQueue,                           mobilUnits.Count);
			// Увеличиваем счетчик очереди у конкретного обработчика
			Counters.Instance.Increment(CounterGroup.Count, Counters.Instances.SaverTask, Thread.CurrentThread.Name, mobilUnits.Count);
			if (_dbManager == null)
				return false;

			var result = true;
			try
			{
				_dbManager.AddMonitoreeLog(mobilUnits, storageId);
			}
			catch (Exception ex)
			{
				Trace.TraceError("{0} AddMonitoreeLog[storageId := {1}, count := {2}, uniques := {3}] exception: {4}",
					this,
					storageId,
					mobilUnits.Count,
					string.Join(";", mobilUnits.Select(mu => mu.Unique).Distinct()),
					ex);
				result = !ex.IsSqlTimeoutException();
			}
			return result;
		}
		private void SaveMobilUnits(object state)
		{
			var stateTuple = state as Tuple<int, string>;
			if (null == stateTuple)
				return;

			var storageId             = stateTuple.Item1;
			Thread.CurrentThread.Name = stateTuple.Item2;
			//////////////////////////////////////////////////////////
			Counters.Instance.ResetCounter(CounterGroup.CountPerSecond, Counters.Instances.SaverTask, Thread.CurrentThread.Name);
			Counters.Instance.ResetCounter(CounterGroup.Count,          Counters.Instances.SaverTask, Thread.CurrentThread.Name);
			//////////////////////////////////////////////////////////

			// Объявляем список объектов для сохранения в БД с вместимостью в _savingMobilUnitsBulkSize элементов
			var bulkSaveMobilUnits = new List<IMobilUnit>(_savingMobilUnitsBulkSize);
			var storageQueue       = _storageQueues[storageId];
			// Пока saveStorageMobilUnits.IsAddingCompleted == false поток работает
			while (!storageQueue.IsAddingCompleted)
			{
				// Объявляем список объектов для сохранения в БД
				var dequeuedMobilUnits = default(List<IMobilUnit>);
				// Получить если возможно список объектов для сохранения в БД
				var muTaken = storageQueue.TryTake(out dequeuedMobilUnits);
				if (muTaken)
				{
					// Удалось получить список объектов для сохранения БД
					bulkSaveMobilUnits.AddRange(dequeuedMobilUnits);
				}
				else
				{
					// Не удалось получить список объектов для сохранения БД, делаем вторую попытку
					if (0 == bulkSaveMobilUnits.Count)
					{
						muTaken = storageQueue.TryTake(out dequeuedMobilUnits, TimeSpan.FromSeconds(1));
						if (muTaken)
							bulkSaveMobilUnits.AddRange(dequeuedMobilUnits);
						else
							continue;
					}
				}
				// Если количество элементов меньше чем размер пакета, то пропускаем сохранение
				if (muTaken && bulkSaveMobilUnits.Count < bulkSaveMobilUnits.Capacity)
					continue;

				if (!SaveMobilUnits(storageId, bulkSaveMobilUnits))
				{
					// Если сохранить не удалось, немного ждем до возврата в очередь
					Thread.Sleep(TimeSpan.FromSeconds(1));
					// Возвращаем в очередь, и если удачно увеличиваем счетчик
					if (storageQueue.TryAdd(bulkSaveMobilUnits))
					{
						// Увеличиваем счетчик общей очереди сохранения
						Counters.Instance.Increment(CounterGroup.Count, Counters.Instances.SaveQueue,                            bulkSaveMobilUnits.Count);
						// Уменьшаем счетчик очереди у конкретного обработчика
						Counters.Instance.Decrement(CounterGroup.Count, Counters.Instances.SaverTask, Thread.CurrentThread.Name, bulkSaveMobilUnits.Count);
					}
				}
				bulkSaveMobilUnits.Clear();
			}
			// Выполняем сохранение в процессе завершения потока, если что осталось
			if (0 < bulkSaveMobilUnits.Count)
				SaveMobilUnits(storageId, bulkSaveMobilUnits);
		}
		private readonly Dictionary<int, BlockingCollection<List<IMobilUnit>>> _storageQueues =
			new Dictionary<int, BlockingCollection<List<IMobilUnit>>>();
		private readonly int    _savingMobilUnitsBulkSize;
		private readonly Task[] _mobilUnitSavers;

		/// <summary> Загрузка последний позиций объектов из БД </summary>
		private void LoadLastPositions()
		{
			if (DbManager == null)
				return;
			foreach (var position in DbManager.GetLastPositions())
				_lastGoodPositions[position.VehicleID] = position;
		}

		private void UpdateInsertTime()
		{
			try
			{
				if (Globals.TssDatabaseManager == null)
					return;

				Globals.TssDatabaseManager.DefaultDataBase.ExecuteNonQuery(
					"dbo.UpdateAllInsertTime", System.Data.CommandType.StoredProcedure);
			}
			catch (Exception e)
			{
				Trace.TraceError("Error when updating insert time: {0}", e);
			}
		}

		#endregion .ctor

		#region Finalize
		~TerminalManager()
		{
			// неявная очистка (неуправляемые ресурсы)
			Dispose(false);
		}
		#endregion Finalize

		#region InitializeComponent()
		void InitializeComponent()
		{

		}
		#endregion InitializeComponent()

		#region ITerminalManager Members

		#region CreateTerminal
		/// <summary> Создание экземпляра терминала </summary>
		/// <param name="terminalName"> Идентификатор терминала </param>
		/// <returns> Ссылка на экземпляр конкретного терминала </returns>
		public ITerminal CreateTerminal(string terminalName)
		{
			lock (this)
			{
				try
				{
					_Terminal term;
					if (_liveTerminals.ContainsKey(terminalName))
					{
						term = (_Terminal)_liveTerminals[terminalName];
					}
					else
					{
						if (Terminals == null || Terminals.Count == 0)
							throw new ApplicationException("Не установлена база данных терминалов сервера");

						var config = GetInitializationStrings(terminalName);

						if (config == null)
						{
							Trace.TraceWarning("Unable to find terminal config by name {0}", terminalName);
							return null;
						}

						switch (config.MediaName)
						{
							case "GPRS":
							case "GPRS0":
							case "GPRS1":
							case "GPRS2":
							case "GPRS3":
							case "GPRS4":
							case "GPRS5":
							case "GPRS6":
							case "GPRS7":
							case "GPRS8":
							case "GPRS9":
							case "GPRS10":
							case "GPRS11":
							case "GPRS12":
							case "GPRS13":
							case "GPRS14":
							case "GPRS15":
							case "GPRS16":
								term = new UDPTerminal(this);
								break;

							case "SMS":
								term = new SMSTerminal(this);
								break;

							case "GSM":
							case "Log":
							case "Info":
								term = new GSMTerminal(terminalName, this);
								break;
							case "TCP":
								term = new TCPTerminal(terminalName, this);
								break;
							case "Emulator":
								term = new EmulatorTerminal(terminalName, this);
								break;
							case "HTTP":
								term = new HttpTerminal(terminalName, this);
								break;
							case "FTP":
								term = new FtpTerminal(this);
								break;
							case "Order285":
								term = new Order285Terminal(terminalName, this);
								break;
							case "Movireg":
								term = new MoviregTerminal(terminalName, this);
								break;
							default:
								throw new ArgumentOutOfRangeException(
									nameof(terminalName), terminalName, @"Неверное значение идентификатора терминала");
						}

						term.Initialization(config.Initialization);
						if (!_liveTerminals.ContainsKey(terminalName))
							_liveTerminals[terminalName] = term;

						term.ReceiveCountingEvent += Terminal_ReceiveCountingEvent;
						term.NotifyEvent          += Terminal_NotifyEvent;
						term.ReceiveEvent         += Terminal_ReceiveEvent;

						terminalName
							.CallTraceInformation();
					}

					return term;
				}
				catch (Exception ex)
				{
					$"Exception during CreateTerminal({terminalName})"
						.WithException(ex, true)
						.CallTraceError();
					return null;
				}
			}
		}
		public IEnumerable<ITerminal> GetLiveTerminals(Media media)
		{
			if (media == Media.Unspecified)
				yield break;

			foreach (var terminalConfig in Terminals.Values.Where(t => t.Media == media))
			{
				ITerminal terminal;
				if (_liveTerminals.TryGetValue(terminalConfig.Name, out terminal))
					yield return terminal;
			}
		}
		public void CreateMainTerminal(string terminalName)
		{
			CreateTerminal(terminalName);
		}
		/// <summary> Обработчик события получения объектов от экземпляров терминалов </summary>
		/// <param name="args"> Данные объектов </param>
		private void Terminal_ReceiveEvent(ReceiveEventArgs args)
		{
			// Проверяем наличие данных, если нет выходим
			if (0 == (args?.MobilUnits?.Length ?? 0))
				return;
			// Дополняем данными из БД (из кэша)
			FillUnitInfo(args.MobilUnits);
			// Обновляем коллекцию последних позиций по объектам пришедшими позициями
			UpdateLastGoodPositions(args.MobilUnits);
			// Обновляем коллекцию последних данных по объектам пришедшими данными
			UpdateLastMobilUnits(args.MobilUnits);
			// Если доступен менеджер БД, то пытаемся сохранить
			if (_dbManager != null)
			{
				// Фильтруем объекты, которые нужно сохранять
				var savingMobilUnits = args.MobilUnits
					.Where(mu => mu.StoreLog/*Значение загружается в FillUnitInfo из кэша БД*/)
					.ToList();
				// Фильтруем объекты, которые оплачены (убираем объекты без оплаты)
				savingMobilUnits = CheckPaymentStatus(savingMobilUnits);
				// Распределяем пришедшие объекты по хранилищам (группируя по идентификатору хранилища)
				foreach (var mobileUnitsToStorage in savingMobilUnits.GroupBy(mu => mu.StorageId))
				{
					var storageQueue = default(BlockingCollection<List<IMobilUnit>>);
					if (_storageQueues.TryGetValue(mobileUnitsToStorage.Key, out storageQueue))
					{
						var bulkPartition = mobileUnitsToStorage
							.OrderBy(mu => mu.Time)
							.ToList();
						// Добавляем в очередь
						storageQueue.Add(bulkPartition);
						// Увеличиваем счетчик
						Counters.Instance.Increment(CounterGroup.Count, Counters.Instances.SaveQueue, bulkPartition.Count);
					}
					else
						Trace.TraceError("Unsupported StorageId: {0}", mobileUnitsToStorage.Key);
				}
			}
			// Добавляем в очередь, увеличиваем счетчик
			if (_onReceiveInvoker.Running)
			{
				foreach (var mobilUnit in args.MobilUnits)
				{
					_onReceiveInvoker.Enqueue(mobilUnit);
					Counters.Instance.Increment(CounterGroup.Count, Counters.Instances.ReceiveQueue);
				}
			}
		}
		/// <summary> Поток ретранслирующий подписчикам менеджера терминалов объекты полученные от экземпляров терминалов </summary>
		private readonly BulkProcessor<IMobilUnit> _onReceiveInvoker;
		/// <summary> Событие ретранслятор объектов полученных от экземпляров терминалов </summary>
		private readonly object                              receiveEventLock = new object();
		private      AnonymousEventHandler<ReceiveEventArgs> receiveEventHandler;
		public event AnonymousEventHandler<ReceiveEventArgs> ReceiveEvent
		{
			add    { lock (receiveEventLock) { receiveEventHandler -= value; receiveEventHandler += value; } }
			remove { lock (receiveEventLock) { receiveEventHandler -= value; } }
		}
		/// <summary> Метод отправки подписчикам менеджера терминалов массива объектов </summary>
		/// <param name="mobilUnits"></param>
		private void BulkProcessReceiveEvents(IMobilUnit[] mobilUnits)
		{
			// Сохраняем длину массива аргументов
			var argsArrayLength = mobilUnits?.Length ?? 0;
			if (0 == argsArrayLength)
				return;

			if (receiveEventHandler != null)
			{
				var args = new ReceiveEventArgs(mobilUnits);
				var invs = receiveEventHandler.GetInvocationList().GetEnumerator();

				while (invs.MoveNext())
				{
					var handler = (AnonymousEventHandler<ReceiveEventArgs>)invs.Current;
					try
					{
						handler(args);
					}
					catch (RemotingException)
					{
						UnsubscribeReceiveEvent(handler, "remoting");
					}
					catch (ThreadInterruptedException)
					{
						UnsubscribeReceiveEvent(handler, "thread interrupted");
					}
					catch (ThreadAbortException)
					{
						UnsubscribeReceiveEvent(handler, "thread abort");
					}
					catch (SocketException e)
					{
						UnsubscribeReceiveEvent(handler, "socket exception - " + e);
					}
					catch (Exception e)
					{
						Trace.TraceError("TerminalManager.ReceiveEvent_Callback: {0}", e);
					}
				}
			}
			Counters.Instance.Decrement(CounterGroup.Count,          Counters.Instances.ReceiveQueue, argsArrayLength);
			Counters.Instance.Increment(CounterGroup.CountPerSecond, Counters.Instances.ReceiveQueue, argsArrayLength);
		}
		private void UnsubscribeReceiveEvent(AnonymousEventHandler<ReceiveEventArgs> handler, string reason)
		{
			Trace.TraceWarning("Unsubscribing TerminalManager.ReceiveEvent because of {0}", reason);
			try
			{
				ReceiveEvent -= handler;
			}
			catch (Exception ex)
			{
				Trace.TraceError("TerminalManager.ReceiveEvent - unable to unsubscribe: {0}", ex);
			}
		}
		/// <summary> Обработчик события получения сообщений от экземпляров терминалов </summary>
		/// <param name="args"> Данные сообщения </param>
		private void Terminal_NotifyEvent(NotifyEventArgs args)
		{
			if (args != null &&
				args.Event == TerminalEventMessage.CommandCompleted)
			{
				var tags = args.Tag as object[];
				if (tags != null && tags.Length != 0)
				{
					var commandResult = tags[0] as CommandResult;
					if (commandResult != null)
					{
						if (TryProcessCommandResult(commandResult))
							return;
					}
				}
			}
			// Отправляем сообщение в очередь отправки подписчикам менеджера терминалов
			if (_onNotifyInvoker.Running)
				_onNotifyInvoker.Enqueue(args);
		}
		/// <summary> Поток ретранслирующий подписчикам менеджера терминалов сообщения полученные от экземпляров терминалов </summary>
		private readonly BulkProcessor<NotifyEventArgs> _onNotifyInvoker;
		/// <summary> Событие ретранслятор сообщений полученных от экземпляров терминалов </summary>
		private readonly object                                 notifyEventLock = new object();
		private      AnonymousEventHandler<BulkNotifyEventArgs> notifyEventHandler;
		public event AnonymousEventHandler<BulkNotifyEventArgs> NotifyEvent
		{
			add    { lock (notifyEventLock) { notifyEventHandler -= value; notifyEventHandler += value; } }
			remove { lock (notifyEventLock) { notifyEventHandler -= value; } }
		}
		/// <summary> Метод отправки подписчикам события сообщений терминала </summary>
		/// <param name="notifyEventArgsArray"></param>
		private void BulkProcessNotifyEvents(NotifyEventArgs[] notifyEventArgsArray)
		{
			// Сохраняем длину массива аргументов
			var argsArrayLength = notifyEventArgsArray?.Length ?? 0;
			if (0 == argsArrayLength)
				return;

			if (notifyEventHandler == null)
				return;

			var args = new BulkNotifyEventArgs { SingleEventArgs = notifyEventArgsArray };
			var invs = notifyEventHandler.GetInvocationList().GetEnumerator();

			while (invs.MoveNext())
			{
				var handler = (AnonymousEventHandler<BulkNotifyEventArgs>)invs.Current;
				try
				{
					handler(args);
				}
				catch (RemotingException)
				{
					Trace.TraceWarning("Unsubscribing {0}.Notify because of remoting error", GetType().Name);
					try
					{
						NotifyEvent -= handler;
					}
					catch (Exception ex)
					{
						Trace.TraceError(GetType().Name + ".Notify - unable to unsubscribe: " + ex);
					}
				}
				catch (ThreadAbortException)
				{
					Trace.TraceInformation("Unsubscribing {0}.Notify because of thread abort", GetType().Name);
					try
					{
						NotifyEvent -= handler;
					}
					catch (Exception ex)
					{
						Trace.TraceError(GetType().Name + ".Notify - unable to unsubscribe: " + ex);
					}
				}
				catch (SocketException)
				{
					Trace.TraceWarning("Unsubscribing {0}.Notify because of socket error", GetType().Name);
					try
					{
						NotifyEvent -= handler;
					}
					catch (Exception ex)
					{
						Trace.TraceError(GetType().Name + ".Notify - unable to unsubscribe: " + ex);
					}
				}
				catch (Exception ex)
				{
					Trace.TraceError(GetType().Name + ".Notify: " + ex);
				}
			}
		}

		private readonly ConcurrentDictionary<int, Position> _lastGoodPositions =
			new ConcurrentDictionary<int, Position>();
		private readonly ConcurrentDictionary<int, IMobilUnit> _lastMobilUnits =
			new ConcurrentDictionary<int, IMobilUnit>();
		private readonly bool _checkVehiclePayment;

		private class PaymentStatus
		{
			public bool     IsPaid;
			public DateTime NextCheck;
		}
		private readonly ConcurrentDictionary<int, PaymentStatus> _vehiclePaymentStatuses =
			new ConcurrentDictionary<int, PaymentStatus>();
		private bool IsPayed(IMobilUnit mu)
		{
			//TODO: проверять не для типа передачи данных, а по другому критерию, например, были ли данные идентифицированы через SimID или DeviceID
			//Оплату не нужно требовать для LBS-запросов, мобильных приложений, эмулятора
			if (mu.Media != Media.TCP)
				return true;

			// Кэш проверки
			PaymentStatus paymentStatus;
			while (!_vehiclePaymentStatuses.TryGetValue(mu.Unique, out paymentStatus))
			{
				if (_vehiclePaymentStatuses.TryAdd(mu.Unique, paymentStatus = new PaymentStatus()))
					break;
			}

			if (DateTime.UtcNow < paymentStatus.NextCheck)
				return paymentStatus.IsPaid;

			//TODO: убрать блокировку, организовав очередь обработки данных с одного и того же объекта наблюдения, т.е. каждый объект - в свою собственную очередь
			lock (paymentStatus)
			{
				//в блокировке, чтобы не вызывать параллельно большое количество обращений к БД по одному и тому же устройству

				var payedToDate = DbManager.TryContinueVehicleService(mu.Unique, DateTime.UtcNow.AddDays(3));

				if (payedToDate == null || payedToDate.Value < DateTime.UtcNow)
				{
					paymentStatus.IsPaid    = false;
					paymentStatus.NextCheck = DateTime.UtcNow.AddMinutes(5);
					return false;
				}

				paymentStatus.IsPaid = true;
				paymentStatus.NextCheck = payedToDate.Value;
			}

			return true;
		}
		private List<IMobilUnit> CheckPaymentStatus(List<IMobilUnit> mobilUnits)
		{
			// Если проверка оплаты не нужна, то возвращаем исходный набор
			if (!_checkVehiclePayment)
				return mobilUnits;

			var result           = new List<IMobilUnit>(mobilUnits.Count);
			var unpaidVehicleIds = new HashSet<int>();
			foreach (var mu in mobilUnits)
			{
				// Проверяем оплату
				if (!IsPayed(mu))
					unpaidVehicleIds.Add(mu.Unique);
				else
					result.Add(mu);
			}
			// Для всех объектов без оплаты отправляем уведомление
			foreach (var unpaidVehicleId in unpaidVehicleIds)
				Terminal_NotifyEvent(new NotifyEventArgs(TerminalEventMessage.VehicleIsUnpaid, unpaidVehicleId));
			// Возвращаем набор объектов с оплатой
			return result;
		}
		private void FillUnitInfo(IEnumerable<IMobilUnit> mobilUnits)
		{
			foreach (IMobilUnit mobilUnit in mobilUnits)
				FillUnitInfo(mobilUnit.UnitInfo);
		}
		private void UpdateLastMobilUnits(IEnumerable<IMobilUnit> mobilUnits)
		{
			foreach (IMobilUnit mobilUnit in mobilUnits)
			{
				if (mobilUnit.Unique < 1)
				{
					//TODO: в кэше обновить актуальную информацию по объектам, для которых нет VehicleId, но известен DeviceID
					continue;
				}

				if (!mobilUnit.StoreLog)
					continue;

				IMobilUnit existing;
				if (_lastMobilUnits.TryGetValue(mobilUnit.Unique, out existing) && mobilUnit.Time <= existing.Time)
					continue;

				_lastMobilUnits[mobilUnit.Unique] = mobilUnit;
			}
		}
		private void UpdateLastGoodPositions(IEnumerable<IMobilUnit> mobilUnits)
		{
			foreach (IMobilUnit mobilUnit in mobilUnits)
			{
				if (mobilUnit.Unique < 1)
					continue;

				if (!mobilUnit.StoreLog)
					continue;

				if (mobilUnit.ValidPosition)
				{
					Position lastGoodPosition;
					if (_lastGoodPositions.TryGetValue(mobilUnit.Unique, out lastGoodPosition) || lastGoodPosition.Time < mobilUnit.Time)
					{
						_lastGoodPositions[mobilUnit.Unique] = new Position(mobilUnit);
					}
				}
				else
				{
					Position lastGoodPosition;
					if (_lastGoodPositions.TryGetValue(mobilUnit.Unique, out lastGoodPosition) &&
						lastGoodPosition.Time < mobilUnit.Time)
					{
						mobilUnit.Longitude   = lastGoodPosition.Longitude;
						mobilUnit.Latitude    = lastGoodPosition.Latitude;
						mobilUnit.CorrectTime = lastGoodPosition.Time;
					}
				}
			}
		}
		/// <summary> Создание объекта конкретного терминала </summary>
		/// <param name="type">type of command to handle</param>
		/// <returns>ссылка на объект конкретного терминала</returns>
		public ITerminal CreateTerminal(CmdType type)
		{
			string terminalName;
			switch (type)
			{
				case CmdType.UDP:
				case CmdType.Monitor:
				case CmdType.SendText:
				case CmdType.SetFirmware:
				case CmdType.GetSettings:
				case CmdType.SetSettings:
				case CmdType.Restart:
					terminalName = GlobalsConfig.AppSettings["Terminal"];
					if (!terminalName.StartsWith("GPRS")) terminalName = "GPRS";
					break;

				case CmdType.GSM:
				case CmdType.Trace:
				case CmdType.Control:
					terminalName = "GSM";
					break;

				case CmdType.GetLog:
					if (GlobalsConfig.AppSettings["HistoryByUDP"] != null &&
						GlobalsConfig.AppSettings["HistoryByUDP"].ToLower() == "true") goto case CmdType.UDP;
					terminalName = "Log";
					break;

				case CmdType.SMS:
				case CmdType.AskPosition:
					terminalName = "MLP";
					break;

				default: throw new NotSupportedException("Тип команды не известен - " + type);
			}
			//return
			CreateTerminal(terminalName);

			return CreateTerminal(terminalName);
		}

		public ITerminal CreateTerminal(ICommand command)
		{
			var cmd = command as IStdCommand;
			if(cmd == null) throw new NotSupportedException("Only IStdCommand supported");
			// create primary terminal
			ITerminal term = CreateTerminal(cmd.Type);
			// if UDP unavailable use SMS /* для посылки сообщений - заплатка */
			if(term is UDPTerminal && !((_Terminal)term).CanHandle(cmd))
				term = CreateTerminal(CmdType.SMS);
			return term;
		}

		#region GetInitializationStrings(int id)
		/// <summary>
		/// Расшифровка строки инициализации
		/// </summary>
		/// <param name="terminalName">название режима работы</param>
		/// <returns>массив строк инициализации</returns>
		private TerminalConfig GetInitializationStrings(string terminalName)
		{
			TerminalConfig terminalConfig;

			if (!Terminals.TryGetValue(terminalName, out terminalConfig))
				return null;

			return terminalConfig;
		}
		#endregion // GetInitializationStrings(int id)

		#endregion // CreateTerminal(int id)

		#region SendCommand(ICommand command)

		/// <summary> Перенаправление команды конкретному терминалу </summary>
		/// <param name="command">команда для мобильного объекта</param>
		private void SendCommandWithDevice(IStdCommand command)
		{
			if (command == null)
				throw new ArgumentNullException("command");

			var unitInfo = command.Target;
			if (!FillUnitInfo(command.Target))
				throw new DeviceDisconnectedException("Unable to fill IUnitInfo by command.Target = " + command.Target);

			if (command.Type == CmdType.CreateReplayUri)
			{
				//TODO: проверить
				CreateReplayUri(command);
				return;
			}

			var dev = GetDevice(unitInfo);
			if (dev == null)
			{
				if (command.Type == CmdType.AskPosition)
					return;

				throw new DeviceDisconnectedException("Unable to find associated device by unitInfo = " + unitInfo);
			}
			List<CommandStep> commandSteps;
			try
			{
				commandSteps = dev.GetCommandSteps(command);
			}
			catch (CommandSendingFailureException e)
			{
				Fail(command, e.CmdResult ?? CmdResult.Failed);
				return;
			}
			if (0 == (commandSteps?.Count ?? 0))
			{
				if (command.Type == CmdType.AskPosition)
					return;

				Fail(command, CmdResult.NotSupported);
				return;
			}
			AssignReplyToPhone(command, commandSteps);

			ContinueCommand(new ExecutingCommand { Command = command, Steps = commandSteps });
		}

		public void SendCommand(IStdCommand command)
		{
			switch (command.MediaType)
			{
				case Media.Unspecified:
					SendCommandWithDevice(command);
					break;
			}
		}

		private void ContinueCommand(ExecutingCommand executingCommand)
		{
			executingCommand.MoveNext();
			if (executingCommand.CurrentStep == null)
			{
				// Команда Setup особенная - должна быть завершена только после поступления первого пакета о местоположении устройства через GPRS
				if (executingCommand.Command.Type == CmdType.Setup)
					return;
				Complete(executingCommand.Command);
				return;
			}

			var smsCommandStep = executingCommand.CurrentStep as SmsCommandStep;
			if (smsCommandStep != null)
			{
				var sms = (ITextMessage)MessageCreator.CreateText();

				if (!string.IsNullOrWhiteSpace(executingCommand.Command.Target.Telephone))
				{
					sms.Target = new MessageObject(
						ObjectType.Phone, executingCommand.Command.Target.Telephone, Direction.Incoming);
				}
				else if (!string.IsNullOrWhiteSpace(executingCommand.Command.Target.Asid))
				{
					sms.Target = new MessageObject(
						ObjectType.Asid, executingCommand.Command.Target.Asid, Direction.Incoming);
				}
				else
				{
					Fail(executingCommand.Command, CmdResult.PhoneNumberIsUnknown);
					return;
				}

				if (smsCommandStep.WaitAnswer != null && smsCommandStep.ReplyTo == null)
				{
					//После подключения нового трекера по номеру телефона не происходит автоматического маскирования его номера,
					//т.к. эта операция ставит под удар конфиденциальность местоположения третьих лиц.
					//Кроме того, технически маскирование номера - длительная операция, поэтому нельзя полагаться, что номер будет маскирован сразу после добавления трекера
					//А также соображение тарификации: получать SMS от трекеров на короткий номер предпочтительнее, т.к. такие SMS бесплатны для абонента
					//Пока услуга не подключена, наших обязательств нулевого тарифа для абонента нет

					if (executingCommand.Command.Target.AsidId == null)
						smsCommandStep.ReplyTo = GetConstant(Constant.ServiceInternationalPhoneNumber);
				}

				if (smsCommandStep.ReplyTo != null)
					sms.Sender = new MessageObject(ObjectType.Phone, smsCommandStep.ReplyTo, Direction.Outgoing);
				sms.Text = smsCommandStep.Text;
				sms.State = MessageState.Wait;
				sms.Params = new PARAMS(false)
				{
					{ CommandParameter.CommandID, executingCommand.Command.Params[CommandParameter.CommandID] }
				};

				//Далее нужно поместить команду в список команд, ожидающих доставку сообщения
				//Если по сценарию требуется ответ - нужно поместить команду в список команд, ожидающих подтверждения
				//При получении от устройства ответа о выполнении команды, ждать доставку сообщения нет смысла
				//- очевидно, что сигнал о доставке сообщения просто запоздал

				if (null != smsCommandStep.WaitAnswer)
					WaitForAnswer(executingCommand, smsCommandStep.WaitAnswer.Value);

				var messageId = RegisterMessage(sms);
				if (messageId == null)
				{
					Fail(executingCommand.Command, CmdResult.Failed);
					return;
				}

				//TODO: устранить гонку, когда сообщение доставляется раньше, чем начинается ожидание доставки
				if (null == smsCommandStep.WaitAnswer)
					WaitForDelivery(executingCommand, messageId.Value);

				return;
			}

			var tcpCommandStep = executingCommand.CurrentStep as TcpCommandStep;
			if (tcpCommandStep != null)
			{
				foreach (var terminal in GetLiveTerminals(Media.TCP))
				{
					//TODO: вначале найти терминал с живым подключением, а затем отправить туда байты из команды
					if (terminal.SendCommand(executingCommand.Command))
						return;
				}

				throw new DeviceDisconnectedException("Unable to find TCPTerminal for command " + executingCommand.Command);

				//TODO: добавить поддержку более одного шага в сценарии
			}
		}

		private void WaitForAnswer(ExecutingCommand executingCommand, CmdType resultCmdType)
		{
			var vehicleId = executingCommand.Command.Target.Unique;

			if (!_executingCommands.TryAdd(new Tuple<int, CmdType>(vehicleId, resultCmdType), executingCommand))
			{
				//TODO: обработать случай, когда TryAdd вернёт false
				Trace.TraceWarning(
					"Unable to subscribe to command waiting for vehicle {0} result command type {1}, executing command is {2}",
					vehicleId, resultCmdType, executingCommand.Command.Type);
			}
		}

		private readonly ConcurrentDictionary<Tuple<int, CmdType>, ExecutingCommand> _executingCommands           =
				new ConcurrentDictionary<Tuple<int, CmdType>, ExecutingCommand>();
		private readonly ConcurrentDictionary<int, ExecutingCommand>                 _messageIdToExecutingCommand =
			new ConcurrentDictionary<int, ExecutingCommand>();

		private void WaitForDelivery(ExecutingCommand executingCommand, int messageId)
		{
			if (_messageIdToExecutingCommand.TryAdd(messageId, executingCommand))
				return;
			ExecutingCommand dummy;
			if (!_messageIdToExecutingCommand.TryRemove(messageId, out dummy))
			{
				Trace.TraceWarning("{0}: Unexpected problem when removing messageId {1} from _messageIdToExecutingCommand",
					this, messageId);
			}
		}

		class ExecutingCommand
		{
			private int _index = -1;
			public List<CommandStep> Steps;
			public CommandStep       CurrentStep
			{
				get
				{
					if (_index < 0 || Steps.Count == _index)
						return null;
					return Steps[_index];
				}
			}
			public IStdCommand       Command;
			public void MoveNext()
			{
				if (_index != Steps.Count)
					++_index;
			}
		}
		private void CreateReplayUri(IStdCommand command)
		{
			var terminal = CreateTerminal("Onvif");
			if (terminal == null)
				throw new DeviceDisconnectedException("Ovif terminal is off");

			terminal.SendCommand(command);
		}
		#endregion SendCommand(ICommand command)

		public void PublishMobileUnits(params IMobilUnit[] mobileUnits)
		{
			Terminal_ReceiveEvent(new ReceiveEventArgs(mobileUnits));
		}
		#endregion ITerminalManager Members

		#region IDisposable Members

		/// <summary>
		/// Очистка ресурсов
		/// </summary>
		/// <param name="disposing">флаг явной/неявной очистки ресурсов</param>
		protected void Dispose(bool disposing)
		{
			// здесь выполняется явная очистка ресурсов (управляемые ресурсы)
			if(disposing)
			{
				lock(this)
				{
					foreach(var liveTerminal in _liveTerminals.Values)
						liveTerminal.Dispose();
				}

				foreach (var storageQueue in _storageQueues.Values)
					storageQueue.CompleteAdding();
				Task.WaitAll(_mobilUnitSavers);

				_onReceiveInvoker.Stop();
				_onNotifyInvoker.Stop();
				if (_tmMessage != null)
					_tmMessage.Dispose();

				GC.SuppressFinalize(this);
			}
		}

		public void Dispose()
		{
			Dispose(true);
		}

		#endregion IDisposable Members

		void Terminal_ReceiveCountingEvent(ReceiveCountingEventArgs rcea)
		{
			if(_tCounting == null)
				_tCounting = new Timer(CountingTimerHandler, this, 60000, 60000);

			lock(_hCounting.SyncRoot)
				if (_hCounting.ContainsKey(GetKeyForHCounting(rcea.TimeBegin, rcea.Duration)))
					((ReceiveCountingEventArgs)_hCounting[GetKeyForHCounting(rcea.TimeBegin, rcea.Duration)]).Count += rcea.Count;
				else
					_hCounting[GetKeyForHCounting(rcea.TimeBegin, rcea.Duration)] = rcea;
		}
		/// <summary>
		/// return string implementation of begin time and duration
		/// to use as key in hashtable
		/// </summary>
		/// <param name="dtTimeBegin">begin time</param>
		/// <param name="iDuration">duration</param>
		/// <returns></returns>
		string GetKeyForHCounting(DateTime dtTimeBegin, int iDuration)
		{
			return dtTimeBegin.ToString(CultureInfo.InvariantCulture) + iDuration.ToString(CultureInfo.InvariantCulture);
		}
		/// <summary> handler for tCounting timer </summary>
		/// <param name="oState">object</param>
		void CountingTimerHandler(object oState)
		{
			try
			{
				lock(_hCounting.SyncRoot)
					if (_hCounting.Count > 0)
					{
						OnSaveCounting(new SaveCountingListEventArgs(_hCounting));
						_hCounting.Clear();
					}
			}
			catch (ThreadAbortException)
			{
			}
			catch (Exception e)
			{
				Trace.TraceError("Unexpected error: {0}", e);
			}
		}
		/// <summary> method calling when terminal is saving messages list needed to count </summary>
		/// <param name="sclea">event arguments</param>
		protected virtual void OnSaveCounting(SaveCountingListEventArgs sclea)
		{
			if (saveCountingListEventHandler != null)
			{
				Delegate[] handlers = saveCountingListEventHandler.GetInvocationList();

				foreach (AnonymousEventHandler<SaveCountingListEventArgs> handler in handlers)
				{
					handler.BeginInvoke(sclea, SaveCounting_Callback, handler);
				}
			}
		}
		private void SaveCounting_Callback(IAsyncResult asyncResult)
		{
			var handler = (AnonymousEventHandler<SaveCountingListEventArgs>)asyncResult.AsyncState;
			try
			{
				handler.EndInvoke(asyncResult);
			}
			catch (Exception)
			{
				saveCountingListEventHandler -= handler;
			}
		}
		private class UnitInfoQuery
		{
			public readonly int    VehicleID;
			public readonly int    ID;
			public readonly string DeviceID;
			public readonly string Phone;
			public readonly string Asid;
			public readonly string SimId;
			public readonly int    Port;
			public UnitInfoQuery(int vehicleID, int id, string deviceID, string phone, string asid, string simId, int port)
			{
				VehicleID = vehicleID;
				ID        = id;
				DeviceID  = deviceID;
				Phone     = phone;
				Asid      = asid;
				SimId     = simId;
				Port      = port;
			}
			public override bool Equals(object obj)
			{
				//
				// See the full list of guidelines at
				// http://go.microsoft.com/fwlink/?LinkID=85237
				// and also the guidance for operator== at
				// http://go.microsoft.com/fwlink/?LinkId=85238
				//

				if (obj == null || GetType() != obj.GetType())
				{
					return false;
				}

				var other = (UnitInfoQuery) obj;
				return VehicleID == other.VehicleID &&
					   ID == other.ID &&
					   string.Equals(DeviceID, other.DeviceID, StringComparison.Ordinal) &&
					   Phone == other.Phone &&
					   Asid == other.Asid &&
					   SimId == other.SimId &&
					   Port == other.Port;
			}
			public override int GetHashCode()
			{
				return VehicleID ^
					   ID ^
					   (DeviceID ?? string.Empty).GetHashCode() ^
					   (Phone ?? string.Empty).GetHashCode() ^
					   (Asid ?? string.Empty).GetHashCode() ^
					   (SimId ?? string.Empty).GetHashCode() ^
					   Port.GetHashCode();
			}
			public override string ToString()
			{
				return string.Format(
					"VehicleID = {0}, ID = {1}, DeviceID = {2}, Phone = {3}, Asid = {4}, SimId = {5}, Port = {6}",
					VehicleID, ID, DeviceID, Phone, Asid, SimId, Port);

			}
			public bool IsEmpty
			{
				get
				{
					return
						VehicleID == default(int) &&
						ID == default(int) &&
						string.IsNullOrWhiteSpace(DeviceID) &&
						string.IsNullOrWhiteSpace(Phone) &&
						string.IsNullOrWhiteSpace(Asid) &&
						string.IsNullOrWhiteSpace(SimId);
				}
			}
		}
		private class UnitInfoFromDb
		{
			public int VehicleID;
			public short? StorageId;
			public int ControllerID;
			public string Phone;
			public string Password;
			public string DeviceID;
			public DateTime? BlockingDate;
			public bool StoreLog;
			public bool AllowDeleteLog;
			public string FirmwareVersion;
			public int UnitType;
			public string TypeName;
			public string ObjectType;
			public int ID;
			public int? AsidId;
			public string Asid;
			public string SimId;
		}
		private static readonly ConcurrentDictionary<UnitInfoQuery, bool> UnresolvedUnitInfoQueries = new ConcurrentDictionary<UnitInfoQuery, bool>();
		private readonly TimingCache<UnitInfoQuery, UnitInfoFromDb> _unitInfoCache =
			new TimingCache<UnitInfoQuery, UnitInfoFromDb>(
				MobilUnitInfoCacheAge,
				p =>
				{
					var unitInfoFromDb = GetUnitInfoFromDb(p.VehicleID, p.ID, p.DeviceID, p.Phone, p.Asid, p.SimId, p.Port);

					if (unitInfoFromDb == null && UnresolvedUnitInfoQueries.TryAdd(p, true))
						Trace.TraceInformation("FillUnitInfo: unable to resolve unit info by query: {0}", p);

					return unitInfoFromDb;
				},
				int.MaxValue);
		private static UnitInfoFromDb GetUnitInfoFromDb(int vehicleID, int id, string deviceID, string phone, string asid, string simId, int port)
		{
			if (Globals.TssDatabaseManager == null)
				return null;

			if (vehicleID == 0 &&
				id == 0 &&
				string.IsNullOrEmpty(deviceID) &&
				string.IsNullOrWhiteSpace(phone) &&
				string.IsNullOrWhiteSpace(asid) &&
				string.IsNullOrWhiteSpace(simId))
				return null;

			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetUnitInfo"))
			{
				sp["@uniqueID"].Value = vehicleID;

				if (id == 0 &&
					(phone != string.Empty || deviceID != null))
					sp["@id"].Value = -1; // DBNull.Value;
				else
					sp["@id"].Value = id;

				if (port != default(int))
					deviceID += "P" + port;

				sp["@device_id"].Value = deviceID != null ? Encoding.ASCII.GetBytes(deviceID) : null;
				sp["@phone"].Value = phone;
				sp["@asid"].Value = asid;
				sp["@simId"].Value = simId;

				try
				{
					using (new ConnectionOpener(sp.Command.Connection))
					using (IDataReader dr = sp.ExecuteReader())
					{
						if (!dr.Read())
							return null;

						if (dr["Unique"] == DBNull.Value)
							return null;

						var result = new UnitInfoFromDb();
						var firmwareBytes = dr["Firmware"] as byte[];
						result.FirmwareVersion = firmwareBytes != null ? Encoding.ASCII.GetString(firmwareBytes) : null;

						result.VehicleID = (int) dr["Unique"];
						result.StorageId = dr["StorageId"] as short?;
						result.ControllerID = (int) dr["UnitID"];
						result.ID = (int) dr["ID"];
						result.Phone = dr["Phone"] as string;
						result.Password = dr["UnitPassword"] as string;
						var deviceIdBytes = dr["Device_ID"] as byte[];
						result.DeviceID = deviceIdBytes != null ? Encoding.ASCII.GetString(deviceIdBytes) : null;
						result.BlockingDate = dr["BlockingDate"] as DateTime?;
						result.StoreLog = (bool) dr["StoreLog"];
						result.AllowDeleteLog = (bool) dr["AllowDeleteLog"];
						result.UnitType = (int) dr["UnitType"];
						result.TypeName = dr["TypeName"] as string;
						result.ObjectType = dr["ObjectType"] as string;
						result.AsidId = dr["AsidId"] as int?;
						result.Asid = dr["Asid"] as string;
						result.SimId = dr["SimId"] as string;

						return result;
					}
				}
				catch (SqlException e)
				{
					Trace.TraceError("GetUnitInfo sql error when calling with parameters: {0}. {1}, {2}, {3}, {4}: {5}",
						vehicleID, id, deviceID, phone, asid, e);
					return null;
				}
			}
		}
		/// <summary> get unit info from db </summary>
		/// <param name="ui"> partly filled info </param>
		/// <returns> unit type </returns>
		public bool FillUnitInfo(IUnitInfo ui)
		{
			var unitInfoQuery = new UnitInfoQuery(ui.Unique, ui.ID, ui.DeviceID, ui.Telephone, ui.Asid, ui.SimId, ui.Port);

			if (unitInfoQuery.IsEmpty)
				return false;

			var unitInfoFromDb = _unitInfoCache[unitInfoQuery];
			if (unitInfoFromDb == null)
				return false;

			ui.Unique         = unitInfoFromDb.VehicleID;
			ui.StorageId      = unitInfoFromDb.StorageId ?? (short)StorageType.MSSQL;
			ui.ID             = unitInfoFromDb.ID;
			ui.Telephone      = unitInfoFromDb.Phone;
			ui.Password       = unitInfoFromDb.Password;
			ui.DeviceType     = unitInfoFromDb.TypeName;

			ui.DeviceID       = unitInfoFromDb.DeviceID;
			ui.BlockingDate   = unitInfoFromDb.BlockingDate;
			ui.StoreLog       = unitInfoFromDb.StoreLog;
			ui.AllowDeleteLog = unitInfoFromDb.AllowDeleteLog;
			ui.Asid           = unitInfoFromDb.Asid;
			ui.AsidId         = unitInfoFromDb.AsidId;
			ui.SimId          = unitInfoFromDb.SimId;

			ui.SensorMap      = _sensorMapCache[ui.Unique];

			return true;
		}
		private static string GetOrCreateSimIdByAsidFromDb(string asid)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetOrCreateSimID"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@asid"].Value = asid;

				using (var reader = sp.ExecuteReader())
				{
					if (!reader.Read())
						return null;

					return reader.GetString(0);
				}
			}
		}
		private static string GetOrCreateSimIdByDeviceIdFromDb(string deviceId)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetOrCreateSimIDByDeviceId"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@deviceId"].Value = deviceId;

				using (var reader = sp.ExecuteReader())
				{
					if (!reader.Read())
						return null;

					return reader.GetString(0);
				}
			}
		}
		public string GetOrCreateSimIdByAsid(string asid)
		{
			return GetOrCreateSimIdByAsidFromDb(asid);
		}
		public string GetOrCreateSimIdByDeviceId(string deviceId)
		{
			return GetOrCreateSimIdByDeviceIdFromDb(deviceId);
		}
		private readonly TimingCache<string, DateTime?> _blockingDateBySimId =
			new TimingCache<string, DateTime?>(
				MobilUnitInfoCacheAge,
				GetBlockingDateFromDb,
				int.MaxValue);
		public DateTime? GetBlockingDate(string simId)
		{
			return _blockingDateBySimId[simId];
		}
		private static DateTime? GetBlockingDateFromDb(string simId)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetBlocking"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@simId"].Value = simId;

				using (var reader = sp.ExecuteReader())
				{
					if (!reader.Read())
						return null;
					return reader.GetDateTime(0);
				}
			}
		}
		private readonly TimingCache<int, List<BillingService>> _servicesByAsidId =
			new TimingCache<int, List<BillingService>>(
				MobilUnitInfoCacheAge,
				GetServicesFromDb,
				int.MaxValue);
		public List<BillingService> GetServices(int asidId)
		{
			return _servicesByAsidId[asidId];
		}
		private static List<BillingService> GetServicesFromDb(int asidId)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetServices"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@asidId"].Value = asidId;

				var result = new List<BillingService>();
				using (var reader = sp.ExecuteReader())
				{
					if (reader.FieldCount == 0)
						return null;

					var idOrdinal = reader.GetOrdinal("id");
					var nameOrdinal = reader.GetOrdinal("name");
					var typeOrdinal = reader.GetOrdinal("type");
					var startDateOrdinal = reader.GetOrdinal("startDate");
					var endDateOrdinal = reader.GetOrdinal("endDate");
					var isTrialOrdinal = reader.GetOrdinal("isTrial");
					var isActivatedOrdinal = reader.GetOrdinal("isActivated");

					while (reader.Read())
					{
						var billingService = new BillingService();
						result.Add(billingService);

						billingService.Id = reader.GetInt32(idOrdinal);
						billingService.Name = reader.GetString(nameOrdinal);
						billingService.Type =
							(ServiceTypeCategory)
							Enum.Parse(typeof (ServiceTypeCategory), reader.GetString(typeOrdinal));
						billingService.StartDate = reader.GetDateTime(startDateOrdinal);
						billingService.EndDate = reader.IsDBNull(endDateOrdinal)
													 ? (DateTime?) null
													 : reader.GetDateTime(endDateOrdinal);
						billingService.IsTrial = reader.GetBoolean(isTrialOrdinal);
						billingService.IsActivated = reader.GetBoolean(isActivatedOrdinal);
					}

					return result;
				}
			}
		}
		public void DeleteLog(int vehicleId)
		{
			var ui = UnitInfo.Factory.Create(UnitInfoPurpose.Undefined);
			ui.Unique = vehicleId;
			if (!FillUnitInfo(ui) || !ui.AllowDeleteLog)
				return;

			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.DeleteLog"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@vehicle_id"].Value = vehicleId;

				sp.ExecuteNonQuery();
			}
		}
		public void NotifyMessageState(IEnumerable<MessageStateItem> items)
		{
			if (items == null)
				return;

			foreach (var item in items)
			{
				if (item.State != MessageState.Done)
					continue;

				if (item.ProcessingResult == MessageProcessingResult.Received ||
					item.ProcessingResult == MessageProcessingResult.Queued   ||
					item.ProcessingResult == MessageProcessingResult.Processing)
					continue;

				ExecutingCommand executingCommand;
				if (!_messageIdToExecutingCommand.TryRemove(item.MessageId, out executingCommand))
					continue;

				if (item.ProcessingResult == MessageProcessingResult.Sent)
				{
					var currentStep = executingCommand.CurrentStep;
					if (currentStep != null && currentStep.WaitAnswer == null)
						ContinueCommand(executingCommand);
				}
				else
				{
					Fail(executingCommand.Command, TranslateToCmdResult(item.ProcessingResult));
				}
			}
		}
		public string GetConstant(Constant constant)
		{
			return DbManager.GetConstant(constant.ToString());
		}
		public IStdCommand GetWaitingCommand(IUnitInfo ui, CmdType cmdType)
		{
			ExecutingCommand waitingCommand;
			if (!_executingCommands.TryGetValue(new Tuple<int, CmdType>(ui.Unique, cmdType), out waitingCommand))
				return null;
			return waitingCommand.Command;
		}
		public CommandScenario GetScenario(IStdCommand command)
		{
			if (command == null)
				throw new ArgumentNullException("command");

			var unitInfo = command.Target;
			if (!FillUnitInfo(command.Target))
				throw new DeviceDisconnectedException("Unable to fill IUnitInfo by command.Target = " + command.Target);

			var dev = GetDevice(unitInfo);
			if (dev == null)
				return new CommandScenario { Result = CmdResult.NotSupported };

			if (!command.Params.ContainsKey(PARAMS.Keys.ReplyToPhone))
			{
				var replyToPhone = command.Params[PARAMS.Keys.ReplyToPhone] as string;
				// Если номер заявлен, но не указан подставляем федеральный номер сервера
				if (string.IsNullOrWhiteSpace(replyToPhone))
				{
					command.Params[PARAMS.Keys.ReplyToPhone] = GetConstant(Constant.ServiceInternationalPhoneNumber);
				}
			}

			List<CommandStep> steps;
			try
			{
				steps = dev.GetCommandSteps(command);
			}
			catch (CommandSendingFailureException e)
			{
				Trace.TraceWarning("{0}.GetScenario({1}): {2}", this, command, e);
				return new CommandScenario { Result = e.CmdResult ?? CmdResult.Failed };
			}

			if (steps == null || steps.Count == 0)
				return new CommandScenario { Result = CmdResult.NotSupported };

			AssignReplyToPhone(command, steps);

			var commandSmses = steps.Where(s => s is SmsCommandStep)
				.Cast<SmsCommandStep>()
				.Select(s => new CommandScenario.CommandSms { Text = s.Text, WaitAnswer = s.WaitAnswer != null })
				.ToArray();

			if (commandSmses.Length == 0)
				return new CommandScenario { Result = CmdResult.NotSupported };

			return new CommandScenario
			{
				Result = CmdResult.Completed,
				Sms    = commandSmses
			};
		}
		public List<TrackingSchedule> GetTrackingSchedule(int vehicleId)
		{
			return DbManager.GetTrackingSchedule(vehicleId);
		}
		private static void AssignReplyToPhone(IStdCommand command, List<CommandStep> steps)
		{
			var replyTo = command.GetParamValue(PARAMS.Keys.ReplyToPhone);
			if (replyTo != null)
			{
				foreach (var step in steps)
				{
					var smsCommandStep = step as SmsCommandStep;
					if (smsCommandStep != null)
					{
						smsCommandStep.ReplyTo = replyTo;
					}
				}
			}
		}
		private void Complete(IStdCommand command)
		{
			NotifyCommandCompletion(command, CmdResult.Completed);
		}
		private void Fail(IStdCommand command, CmdResult cmdResult)
		{
			NotifyCommandCompletion(command, cmdResult);
		}
		private void NotifyCommandCompletion(IStdCommand command, CmdResult cmdResult)
		{
			Terminal_NotifyEvent(new NotifyEventArgs(TerminalEventMessage.CommandCompleted, new CommandResult
			{
				CmdType   = command.Type,
				CmdResult = cmdResult,
				Tag       = command.Target
			}));
		}
		private bool TryProcessCommandResult(CommandResult commandResult)
		{
			var tag = commandResult.Tag as IUnitInfo;
			if (tag == null)
			{
				return false;
			}
			ExecutingCommand executingCommand;
			var cmdType = commandResult.CmdType;
			if (!_executingCommands.TryRemove(new Tuple<int, CmdType>(tag.Unique, cmdType), out executingCommand))
				return false;

			if (commandResult.CmdResult == CmdResult.Completed)
				ContinueCommand(executingCommand);
			else
				Fail(executingCommand.Command, commandResult.CmdResult);

			return true;
		}
		private CmdResult TranslateToCmdResult(MessageProcessingResult processingResult)
		{
			switch (processingResult)
			{
				case MessageProcessingResult.ContactDoesNotExist:
					return CmdResult.UnknownSubscriber;
				case MessageProcessingResult.UnsuccessfulBillingResult:
					return CmdResult.InsufficientFunds;
				case MessageProcessingResult.Throttled:
					return CmdResult.Throttled;
				case MessageProcessingResult.MessageQueueFull:
					return CmdResult.Throttled;
				default:
					return CmdResult.Failed;
			}
		}
		private readonly TimingCache<int, Dictionary<SensorLegend, SensorMap[]>> _sensorMapCache =
			new TimingCache<int, Dictionary<SensorLegend, SensorMap[]>>(
				MobilUnitInfoCacheAge,
				vehicleID =>
				GetSensorMaps(vehicleID)
					.ToLookup(m => m.SensorLegend)
					.ToDictionary(g => g.Key, g => g.ToArray()), int.MaxValue);
		private static IEnumerable<SensorMap> GetSensorMaps(int vehicleId)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.GetSensorMap"))
			using (new ConnectionOpener(sp.Command.Connection))
			{
				sp["@vehicleID"].Value = vehicleId;

				using (var reader = sp.ExecuteReader())
				{
					var ordinalLegendNumber = reader.GetOrdinal("Sensor_Legend");
					var ordinalSensorNumber = reader.GetOrdinal("Sensor_Number");
					var ordinalMultiplier   = reader.GetOrdinal("Multiplier");
					var ordinalConstant     = reader.GetOrdinal("Constant");
					var ordinalMinValue     = reader.GetOrdinal("Min_Value");
					var ordinalMaxValue     = reader.GetOrdinal("Max_Value");
					var ordinalValueExpired = reader.GetOrdinal("VALUE_EXPIRED");
					while (reader.Read())
					{
						var map = new SensorMap();
						map.SensorLegend = (SensorLegend) reader.GetInt32(ordinalLegendNumber);
						map.InputNumber  = reader.GetInt32(ordinalSensorNumber);
						map.Multiplier   = !reader.IsDBNull(ordinalMultiplier)   ? reader.GetDecimal(ordinalMultiplier) : 1;
						map.Constant     = !reader.IsDBNull(ordinalConstant)     ? reader.GetDecimal(ordinalConstant)   : 0;
						map.MinValue     = !reader.IsDBNull(ordinalMinValue)     ? reader.GetInt64(ordinalMinValue)     : (long?)null;
						map.MaxValue     = !reader.IsDBNull(ordinalMaxValue)     ? reader.GetInt64(ordinalMaxValue)     : (long?)null;
						map.ValueExpired = !reader.IsDBNull(ordinalValueExpired) ? reader.GetInt32(ordinalValueExpired) : (int?) null;
						yield return map;
					}
				}
			}
		}
		/// <summary> extended unit info </summary>
		public class UnitInfoEx
		{
			public int    UnitID;
			public int    UnitNo;
			public int    UnitType;
			public string TypeName;

			public int    VehicleID;
			public string Phone;
			public string ObjectType;
			public string FirmwareVersion;

			public IUnitInfo UnitInfo;

			public UnitInfoEx(int id, IUnitInfo ui, int type, string typeName, string objectType)
			{
				UnitID     = id;
				UnitNo     = ui.ID;
				UnitType   = type;
				TypeName   = typeName;
				VehicleID  = ui.Unique;
				Phone      = ui.Telephone;
				ObjectType = objectType;
				UnitInfo   = ui;
			}
		}
		/// <summary> get device info according to unit info </summary>
		/// <param name="ui"> unit info. filled here </param>
		/// <returns> object with device specific info </returns>
		public IDevice GetDevice(IUnitInfo ui)
		{
			var uix = GetUnitInfo(ui);

			if (null == uix/* || uix.UnitType == 1*/)
				return null;

			var objectType = uix.ObjectType;

			// Поиск в массиве устройств
			var dev = _arDevice.FirstOrDefault(d =>
				(d.DeviceTypeNames?.Contains(objectType)      ?? false)
				||
				(d.GetType().ToString()?.Contains(objectType) ?? false));
			// Поиск в массиве плагинов если в устройствах нет
			if (null == dev)
			{
				dev = _arPluginInfo.FirstOrDefault(p =>
					p.PluginType.Name                  == objectType ||
					p.PluginType.FullName              == objectType ||
					p.PluginType.AssemblyQualifiedName == objectType)?.GetPlugin() as IDevice;
				// Если нашли тут, то указываем Manager
				if (null != dev)
					dev.Manager = this;
			}

			if (null == dev)
				Trace.TraceWarning("Unable to create device. Unknown unit type: {0}", ui);

			return dev;
		}
		public string GetDeviceType(IUnitInfo ui)
		{
			UnitInfoEx uix = GetUnitInfo(ui);
			if(uix == null)
				return null;
			return uix.ObjectType;
		}
		private UnitInfoEx GetUnitInfo(IUnitInfo ui)
		{
			var unitInfoFromDb = _unitInfoCache[new UnitInfoQuery(ui.Unique, ui.ID, ui.DeviceID, ui.Telephone, ui.Asid, ui.SimId, ui.Port)];

			if (unitInfoFromDb == null)
				return null;

			var result = new UnitInfoEx(
				unitInfoFromDb.ControllerID,
				ui,
				unitInfoFromDb.UnitType,
				unitInfoFromDb.TypeName,
				unitInfoFromDb.ObjectType);

			result.FirmwareVersion = unitInfoFromDb.FirmwareVersion;
			return result;
		}
		public List<IDevice> GetDevices(byte[] data)
		{
			var devices = new List<IDevice>();
			foreach (IDevice dev in _arDevice)
			{
				try
				{
					if (!dev.SupportData(data))
						continue;
					devices.Add(dev);
				}
				catch (Exception ex)
				{
					// Если парсер сломался - логируем, но не мешаем другим девайсам проверить не их ли сигнал
					Trace.TraceError("Error during check SupportData: {0}\ndata:{1}", ex, BitConverter.ToString(data));
				}
			}
			return devices;
		}
		/// <summary> Получить первое устройство распознавшее пакет, как свой </summary>
		/// <param name="packet"> Байты пакета </param>
		/// <returns> Объект с информацией об устройстве </returns>
		public IDevice GetDevice(byte[] packet)
		{
			var sw = new Stopwatch();
			sw.Start();
			var devices = GetDevices(packet);
			sw.Stop();
			if (100 < sw.ElapsedMilliseconds)
			{
				Trace.TraceWarning(
					"Finding device by using SupportData is too slow {0}, data: {1}",
					sw.Elapsed,
					BitConverter.ToString(packet));
			}

			if (devices.Count > 1)
			{
				Trace.TraceWarning(
					"Several devices supports the same data: {0}\n {1}",
					StringHelper.SmartFormatBytes(packet),
					devices.Select(dev => dev.GetType().FullName).Join(", "));
			}

			var res = devices.FirstOrDefault();
			if (null == res)
			{
				Trace.TraceWarning(
					"TerminalManager. Unable to create device. Unknown packet format:\n{0}",
					BitConverter.ToString(packet));
				//StringHelper.SmartFormatBytes(packet));
			}
			return res;
		}
		public IHttpDevice GetHttpDevice(IHttpRequest request)
		{
			var devices = new List<IHttpDevice>(1);
			foreach (var device in _arDevice)
			{
				var httpDevice = device as IHttpDevice;
				if (httpDevice == null)
					continue;
				try
				{
					if (httpDevice.CanProcess(request))
						devices.Add(httpDevice);
				}
				catch (Exception exception)
				{
					Trace.TraceError("Error in {0}.GetProcess on checking packet {1}: {2}",
									 httpDevice.GetType().Name, request, exception);
				}
			}

			switch (devices.Count)
			{
				case 0:
					Trace.WriteLineIf(_tsTerm.TraceWarning, "TerminalManager. Unable to create http device. Unknown request format:\n" +
						request,
						_tsTerm.Description);
					return null;
				case 1:
					return devices.First();
				default:
					Trace.WriteLine("Several devices supports the same data:");
					Trace.WriteLine(request.ToString());
					Trace.WriteLine("devices:");
					foreach (var dev in devices)
						Trace.WriteLine(dev.GetType().FullName);
					return null;
			}
		}
		/// <summary> list of messages to confirm </summary>
		readonly ArrayList _alConfirm = ArrayList.Synchronized(new ArrayList(256));
		class MESSAGE : IComparable
		{
			public readonly IMessage Msg;
			public readonly ManualResetEvent Complete = new ManualResetEvent(false);
			public int Group;
			private readonly DateTime _time = DateTime.Now;
			public MESSAGE(IMessage msg)
			{
				Msg = msg;
			}

			#region IComparable Members

			public int CompareTo(object obj)
			{
				int num = obj is int? (int)obj: -1;
				var a = obj as IMessage;
				var b = obj as MESSAGE;
				if(a != null) num = a.Num;
				if(b != null) num = b.Msg.Num;

				if(num == -1) throw new ArgumentException("Unexpected argument type - " + obj.GetType());

				return Msg.Num.CompareTo(num);
			}

			#endregion IComparable Members

			public class TimeComparer : IComparer
			{
				#region IComparer Members

				public int Compare(object x, object y)
				{
					DateTime a = x is DateTime? (DateTime)x: ((MESSAGE)x)._time,
						b = y is DateTime? (DateTime)y: ((MESSAGE)y)._time;
					return a.CompareTo(b);
				}

				#endregion
			}
		}
		public int? RegisterMessage(IMessage msg)
		{
			try
			{
				// assign number
				if (msg.Num == -1)
					AssignMessageNumber(msg);

				Trace.WriteLineIf(
					_tsTerm.TraceVerbose, "Registering " + msg.Direction + " message " + msg.Num, _tsTerm.Description);

				MESSAGE orig = null;
				// need confirm
				if (msg.Confirm)
				{
					Trace.WriteLineIf(
						_tsTerm.TraceVerbose, "Adding message " + msg.Num + " to confirm list", _tsTerm.Description);
					lock (_alConfirm.SyncRoot)
					{
						int i = _alConfirm.BinarySearch(msg);
						if (i >= 0)
							_alConfirm.RemoveAt(i);
						if (i != 0)
							_alConfirm.Insert(~i, orig = new MESSAGE(msg));
					}
					if (!msg.Log && orig != null)
						orig.Complete.Set();
				}
				// message group in db
				int group = 0;
				// confirm received
				if ((msg.Category & MessageCategory.Confirm) != 0)
				{
					var conf = (IConfirmMessage) msg;
					Trace.WriteLineIf(_tsTerm.TraceVerbose, "Confirming message " + conf.ConfirmNo,
									  _tsTerm.Description);
					lock (_alConfirm.SyncRoot)
					{
						int i = _alConfirm.BinarySearch(conf.ConfirmNo);
						if (i < 0)
						{
							Trace.WriteLineIf(_tsTerm.TraceWarning, "Original message not found",
											  _tsTerm.Description);
							return null;
						}

						orig = (MESSAGE)_alConfirm[i];
					}
					if (orig.Complete.WaitOne(30000, false)) group = orig.Group;
					else
					{
						Trace.WriteLineIf(_tsTerm.TraceWarning, "Original message not completed",
										  _tsTerm.Description);
						Debug.Fail("Original message not completed");
					}
					// target not specified so it's incoming message
					if (msg.Target.Object == null)
					{
						Debug.Assert(orig.Msg.Direction == Direction.Outgoing &&
									 msg.Direction == Direction.Incoming &&
									 msg.Target.Type == ObjectType.Operator, "Only incoming can be without target");
						msg.Target = orig.Msg.Sender;
					}
					if (!orig.Msg.Log) return null;
				}
				// need log
				if (!msg.Log)
				{
					return null;
				}

				Trace.WriteLineIf(
					_tsTerm.TraceVerbose, "Logging message " + msg.Num + " from " + msg.Sender + " to " + msg.Target, _tsTerm.Description);
				// check message type
				var txtMsg = msg as ITextMessage;
				if (txtMsg == null)
				{
					Debug.Fail("TerminalManager.RegisterMessage. Nothing to log");
					return null;
				}

				// type bit mask. 1 - predefined, 4 - delivered,
				// 8 - accepted, 0x100 - to controller,
				// 0x200 - from controller, 0x400 - from operator,
				// 0x800 - to operator
				var type = (int) txtMsg.State;
				string src = null;
				if (msg.Sender != null)
				{
					switch (msg.Sender.Type)
					{
						case ObjectType.Controller:
							type |= 0x200;
							if (msg.Sender.Object != null)
							{
								var ui = (IUnitInfo) msg.Sender.Object;
								if (FillUnitInfo(ui))
									src = ui.Unique.ToString(CultureInfo.InvariantCulture);
							}
							break;
						case ObjectType.Operator:
							type |= 0x400;
							if (msg.Sender.Object != null)
								src = ((IOperatorInfo) msg.Sender.Object).OperatorId.ToString(CultureInfo.InvariantCulture);
							break;
						case ObjectType.Phone:
						case ObjectType.Asid:
							src = msg.Sender.Object as string;
							break;
					}
				}

				string dst = null;
				if (msg.Target != null)
				{
					switch (msg.Target.Type)
					{
						case ObjectType.Controller:
							type |= 0x0100;
							if (msg.Target.Object != null)
							{
								var ui = (IUnitInfo) msg.Target.Object;
								FillUnitInfo(ui);
								dst = ui.Unique.ToString(CultureInfo.InvariantCulture);
							}
							break;
						case ObjectType.Operator:
							type |= 0x0800;
							if (msg.Target.Object != null)
								dst = ((IOperatorInfo) msg.Target.Object).OperatorId.ToString(CultureInfo.InvariantCulture);
							break;
						case ObjectType.Asid:
							type |= (int) ObjectType.Asid;
							if (msg.Target.Object != null)
								dst = msg.Target.Object as string;
							break;
						case ObjectType.Phone:
							type |= (int)ObjectType.Phone;
							if (msg.Target.Object != null)
								dst = msg.Target.Object as string;
							break;
					}
				}

				var @params = msg.Params;
				int id = AddMessage(
					txtMsg.TermID,
					txtMsg.Code,
					type,
					msg.Sender != null ? msg.Sender.Type : (ObjectType?)null,
					src,
					dst,
					ref @group,
					msg.Time,
					txtMsg.Text,
					txtMsg.State,
					(int)msg.Severity,
					msg.Target != null ? msg.Target.Type : (ObjectType?)null,
					(int?)(@params != null && @params.ContainsKey(CommandParameter.CommandID) ? @params[CommandParameter.CommandID] : null));

				Trace.WriteLineIf(
					_tsTerm.TraceVerbose, "Message " + msg.Num + " saved with id = " + id +
														" in group " + @group, _tsTerm.Description);
				if (orig != null)
				{
					if (orig.Msg != msg)
					{
						Debug.Assert(orig.Group == @group, "Message and confirm in different groups");
					}
					orig.Group = @group;
					orig.Complete.Set();
				}

				return id;
			}
			catch(Exception ex)
			{
				Trace.TraceError("{0}", ex.ToString());
				return null;
			}
		}
		/// <summary> add message to message log </summary>
		/// <param name="termID"><see cref="ITerminal.ID"/></param>
		/// <param name="code">message code if predefined</param>
		/// <param name="type">bit mask. MessageState | MessageObject</param>
		/// <param name="sourceType">Тип отправителя сообщения</param>
		/// <param name="src">if type &amp; 0x100 - operator, type &amp; 0x200 - controller</param>
		/// <param name="dst">if type &amp; 0x100 - controller, type &amp; 0x200 - operator</param>
		/// <param name="group">message group</param>
		/// <param name="tm">time stamp</param>
		/// <param name="text">text</param>
		/// <param name="status">status</param>
		/// <param name="severity"></param>
		public static int AddMessage(int? termID, string code, int type, ObjectType? sourceType, string src, string dst, ref int @group, DateTime tm, string text, MessageState status, int severity, ObjectType? destinationType, int? commandId)
		{
			using (StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("dbo.AddMessage"))
			{
				if (!string.IsNullOrWhiteSpace(code))
					sp["@code"].Value = code;
				sp["@ma_id"].Value = termID;

				if(commandId.HasValue)
					sp["@commandId"].Value = commandId;

				if (sourceType != null)
				{
					var sourceContactType = GetContactType(sourceType.Value);
					if (sourceContactType != null)
					{
						sp["@sourceTypeID"].Value = (int)sourceContactType.Value;
						sp["@sourceValue"].Value = src;
					}
				}

				if (destinationType != null)
				{
					var destinationContactType = GetContactType(destinationType.Value);
					if (destinationContactType != null)
					{
						sp["@destinationTypeID"].Value = (int)destinationContactType.Value;
						sp["@destinationValue"].Value = dst;
					}
				}

				if (group > 0)
					sp["@group"].Value = group;
				if (tm != TimeHelper.NULL)
					sp["@time"].Value = tm;

				sp["@text"].Value = text;
				sp["@status"].Value = (int)status;
				if (severity > -1)
					sp["@severity"].Value = severity;

				using (new ConnectionOpener(sp.Command.Connection))
				using (IDataReader dr = sp.ExecuteReader())
				{
					if (dr == null || dr.IsClosed || !dr.Read())
						throw new ApplicationException("dbo.AddMessage not returned info");

					group = (int) dr["GROUP"];
					return (int) dr["ID"];
				}
			}
		}
		private static ContactType? GetContactType(ObjectType objectType)
		{
			switch (objectType)
			{
				case ObjectType.Controller:
					return ContactType.Vehicle;
				case ObjectType.Email:
					return ContactType.Email;
				case ObjectType.Phone:
					return ContactType.Phone;
				default:
					return null;
			}
		}

		/// <summary>
		/// running manager message counter
		/// </summary>
		int _iMsgNo;
		int NextMsgNo()
		{
			if (_iMsgNo >= ((1 << 16) - 1))
				Interlocked.Exchange(ref _iMsgNo, 0);
			return Interlocked.Increment(ref _iMsgNo);
		}

		public int AssignMessageNumber(IMessage msg)
		{
			if(msg == null) return NextMsgNo();

			return msg.Num = NextMsgNo();
		}
		/// <summary> get previously registered message </summary>
		/// <param name="num">message no.</param>
		/// <returns>message object or null if not found</returns>
		public IMessage GetRegisteredMessage(int num)
		{
			lock(_alConfirm.SyncRoot)
			{
				int i = _alConfirm.BinarySearch(num);
				return i > -1? ((MESSAGE)_alConfirm[i]).Msg: null;
			}
		}

		readonly string _currentDirectoryDevicePath = Environment.CurrentDirectory + "\\Devices";
		readonly string _applicationBaseDevicePath =
			AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "Devices";

		readonly PluginManager _pmDevicer = new PluginManager();
		/// <summary> Массив доступных устройств </summary>
		IDevice[]    _arDevice;
		/// <summary> Массив плагинов устройств </summary>
		PluginInfo[] _arPluginInfo;
		/// <summary> Загрузка плагинов устройств </summary>
		void LoadDevices()
		{
			_pmDevicer.AddDirectory(_applicationBaseDevicePath);
			_pmDevicer.AddDirectory(_currentDirectoryDevicePath);

			_pmDevicer.PluginType = typeof(IDevice);
			_arPluginInfo         = _pmDevicer.Plugins;

			Trace.TraceInformation($"LoadDevices: arPluginInfo.Length = {_arPluginInfo.Length}");

			_arDevice = new IDevice[_arPluginInfo.Length];

			for (int i = 0, cnt = _arPluginInfo.Length; i < cnt; i++)
			{
				try
				{
					_arDevice[i] = (IDevice)_arPluginInfo[i].GetPlugin();
					if (null != _arDevice[i])
					{
						_arDevice[i].Manager = this;
						Trace.WriteLineIf(
							_tsTerm.TraceWarning,
							$"Device {_arDevice[i].GetType().FullName} loaded from {_arPluginInfo[i].PluginFile}",
							_tsTerm.Description);
					}
				}
				catch (Exception ex)
				{
					Trace.TraceError($@"Unable to create plugin from file: {_arPluginInfo[i].PluginFile}, error: {ex}");
				}
			}
		}

		/// <summary> check messages timer. kill old and weak </summary>
		readonly Timer _tmMessage;

		private readonly TerminalDatabaseManager _dbManager;
		internal static readonly TimeSpan MobilUnitInfoCacheAge = TimeSpan.FromMinutes(3);

		/// <summary> message life time, e. g. timeout for confirm(s) wait </summary>
		const int IMsgLt        = 300;
		const int IMsgChkPeriod = 600;
		void CheckMessages(object state)
		{
			try
			{
				if(_alConfirm.Count == 0)
					return;

				Trace.TraceInformation("{0}: {1} message(s) to process", this, _alConfirm.Count);

				var timeComparer = new MESSAGE.TimeComparer();
				var al = new ArrayList(_alConfirm);
				al.Sort(timeComparer);
				int pos = al.BinarySearch(DateTime.Now.AddSeconds(-IMsgLt), timeComparer);
				for(int i = 0, n = pos > -1? pos: ~pos; i < n; ++i)
					_alConfirm.Remove(al[i]);

				Trace.TraceInformation("{0}: message check done, {1} message(s) left", this, _alConfirm.Count);
			}
			catch (ThreadAbortException)
			{
			}
			catch (Exception e)
			{
				Trace.TraceError("{0}", e);
			}
		}
		public void SendSMS(string target, string text)
		{
			try
			{
				((SMSTerminal)CreateTerminal("SMS")).Write(target, text);
			}
			catch (Exception ex)
			{
				Trace.WriteLine(ex.ToString(), "SendSMS");
			}

		}
		public void SendSMS(string target, byte[] data)
		{
			try
			{
				((SMSTerminal)CreateTerminal("SMS")).Write(target, data);
			}
			catch (Exception ex)
			{
				Trace.WriteLine(ex.ToString(), "SendSMS");
			}

		}
		public override object InitializeLifetimeService()
		{
			return null;
		}
		private class TerminalConfig
		{
			public string   Name;
			public Media    Media;
			public string   MediaName;
			public string[] Initialization;
		}

		const string SqlMediaList = "exec dbo.GetMedia";
		/// <summary> Загрузка из БД конфигураций терминалов </summary>
		/// <returns> Словарь конфигураций терминалов с именем в качестве ключа </returns>
		private static Dictionary<string, TerminalConfig> LoadTerminals()
		{
			if (Globals.TssDatabaseManager == null)
				return null;

			using (IDataReader dr = Globals.TssDatabaseManager.DefaultDataBase.ExecuteReader(SqlMediaList, System.Data.CommandType.Text))
			{
				var terminals = new Dictionary<string, TerminalConfig>();
				while (dr.Read())
				{
					var name = ((string) dr["NAME"]).Trim();

					var terminalConfig = new TerminalConfig();
					terminals[name] = terminalConfig;
					terminalConfig.Name = name;

					var mediaTypeName = ((string) dr["Media_Type"]).Trim();
					if (!Enum.TryParse(mediaTypeName, true, out terminalConfig.Media))
						terminalConfig.Media = Media.Unspecified;

					terminalConfig.MediaName = ((string)dr["Media_Name"]).Trim();
					terminalConfig.Initialization = ((string) dr["INITIALIZATION"]).Trim().Split('|');
				}
				return terminals;
			}
		}
		public IUnitPosition GetLastPosition(int vehicleId)
		{
			if (!_lastGoodPositions.TryGetValue(vehicleId, out Position position))
				return default;

			return new UnitPosition
			{
				Time      = position.Time,
				Latitude  = position.Latitude,
				Longitude = position.Longitude,
				Speed     = position.Speed,
				Course    = position.Course
			};
		}
		public IMobilUnit GetLastMobilUnit(int vehicleId)
		{
			if (!_lastMobilUnits.TryGetValue(vehicleId, out IMobilUnit result))
				return default;
			return result;
		}
	}
}