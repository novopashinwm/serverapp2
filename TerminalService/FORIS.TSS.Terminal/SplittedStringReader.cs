﻿using System;
using System.Globalization;
using FORIS.TSS.Common;

namespace FORIS.TSS.Terminal
{
	public class SplittedStringReader
	{
		private readonly string[] _parts;
		private          int      _index;
		public SplittedStringReader(string s, params char[] delimeter)
		{
			_parts = s.Split(delimeter);
			_index = 0;
		}
		public string ReadString()
		{
			return _parts[_index++];
		}
		public void Skip(int count)
		{
			_index += count;
		}
		public bool HasMoreData()
		{
			return _parts.Length > _index;
		}
		public int Length
		{
			get { return _parts?.Length ?? 0; }
		}
		/// <summary> Читает очередной элемент как целое число в десятичном представлении </summary>
		public int ReadInt()
		{
			var s = ReadString();
			return int.Parse(s);
		}
		/// <summary> Читает очередной элемент как целое число в десятичном представлении </summary>
		public int? ReadNullableInt(bool zeroToNull = false)
		{
			var s = ReadString();
			if (!int.TryParse(s, out int result))
				return null;
			return zeroToNull ? result == 0 ? (int?)null : result : result;
		}
		/// <summary> Читает очередной элемент как дробное число в десятичном представлении </summary>
		public double? ReadDouble()
		{
			var s = ReadString();
			if (!double.TryParse(s, NumberStyles.Any, NumberHelper.FormatInfo, out double result))
				return null;
			return result;
		}
		public bool ReadBoolean()
		{
			var b = ReadInt();
			return b == 1;
		}
		public bool? ReadNullableBoolean()
		{
			var b = ReadNullableInt();
			return b.HasValue ? b == 1 : (bool?)null;
		}
		public DateTime ReadDate(string format)
		{
			var s = ReadString();
			return DateTime.ParseExact(s, format, CultureInfo.InvariantCulture);
		}
		public DateTime? TryReadDate(string format)
		{
			var s = ReadString();
			if (!DateTime.TryParseExact(s, format, CultureInfo.InvariantCulture,
					DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal, out DateTime result))
				return null;
			return result;
		}
		public int ReadHexInt()
		{
			var s = ReadString();
			return int.Parse(s, NumberStyles.HexNumber);
		}
		public bool TrySkip(string s)
		{
			if (s == null)
				throw new ArgumentNullException(nameof(s));

			if (!HasMoreData())
				return false;
			var read = ReadString();
			return read == s;
		}
		public decimal? ReadDecimal()
		{
			var s = ReadString();
			if (!decimal.TryParse(s, NumberStyles.Any, NumberHelper.FormatInfo, out decimal result))
				return null;
			return result;

		}
		public int? ReadDecimalAsNullableInt()
		{
			var value = ReadDecimal();
			return value != null ? (int)Math.Round(value.Value) : (int?)null;
		}
		public TimeSpan? ReadTime(string format)
		{
			var s = ReadString();
			if (!TimeSpan.TryParseExact(s, format, CultureInfo.InvariantCulture, out TimeSpan result))
				return null;
			return result;
		}
	}
}