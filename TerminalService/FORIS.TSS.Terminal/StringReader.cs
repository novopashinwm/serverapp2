﻿using System;
using System.Globalization;
using System.Linq;

namespace FORIS.TSS.Terminal
{
	public class StringReader
	{
		private readonly string _string;
		private          int    _index;
		public StringReader(string input)
		{
			if (input == null)
				throw new ArgumentNullException(nameof(input));

			_string = input;
			_index  = 0;
		}
		public char ReadChar()
		{
			return _string[_index++];
		}
		public string ReadString(int count)
		{
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count), count, @"Value should be greater than zero");

			if (_string.Length < _index + count)
				throw new ArgumentOutOfRangeException(nameof(count), count, @"Value too big, maximum value allowed: " + (_index + count));

			var result = _string.Substring(_index, count);
			
			_index += count;
			
			return result;
		}
		public string ReadBefore(params char[] c)
		{
			var index = _string.IndexOfAny(c, _index);
			if (index == -1)
				return null;
			if (index == _index)
				return string.Empty;
			var result = _string.Substring(_index, index - _index);
			_index = index;
			return result;
		}
		public void Skip(int count)
		{
			if (count <= 0 || _index + _string.Length < count)
				throw new ArgumentOutOfRangeException(nameof(count), count, @"Value should be positive and not to follow to outgoing of range");

			_index += count;
		}
		public void Skip(string expectedString)
		{
			if (expectedString == null)
				throw new ArgumentNullException(nameof(expectedString));

			foreach (var c in expectedString)
				Skip(c);
		}
		public void Skip(char expectedChar)
		{
			var actualChar = _string[_index];
			if (actualChar != expectedChar)
				throw new InvalidOperationException("Unexpected char at position " + _index + ": " + actualChar + ", expected: " + expectedChar);
			++_index;
		}
		public void SkipBeforeAndIncluding(string s)
		{
			var index = _string.IndexOf(s, _index);
			if (index == -1)
				throw new InvalidOperationException("String not found: " + s);
			_index = index + s.Length;
		}
		public char ReadOneOf(params char[] chars)
		{
			if (chars == null)
				throw new ArgumentNullException(nameof(chars));
			if (chars.Length == 0)
				throw new ArgumentException(@"Input collection should be non-empty", nameof(chars));

			var c = _string[_index];
			if (!chars.Contains(c))
				throw new InvalidOperationException("Unable to read char " + c + " as one of " + String.Join(", ", chars));
			++_index;
			return c;
		}
		public decimal ReadDecimal(int count)
		{
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count), count, @"Value should be greater than zero");

			if (_string.Length < _index + count)
				throw new ArgumentOutOfRangeException(nameof(count), count, @"Value too big, maximum value allowed: " + (_index + count));

			var s = _string.Substring(_index, count);

			var result = decimal.Parse(s, CultureInfo.InvariantCulture);
			
			_index += count;
			
			return result;
		}
		/// <summary> Читает count шестнадцатеричных цифр как целое число </summary>
		/// <param name="count">Количество цифр</param>
		public int ReadHexInt(int count)
		{
			return ReadInt(count, NumberStyles.HexNumber);
		}
		private int ReadInt(int count, NumberStyles numberStyles)
		{
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count), count, @"Value should be greater than zero");

			if (_string.Length < _index + count)
				throw new ArgumentOutOfRangeException(nameof(count), count,
													  @"Value too big, maximum value allowed: " + (_index + count));

			var s = _string.Substring(_index, count);
			_index += count;

			//Удаляем лидирующие нули
			s = s.TrimStart('0');
			if (s.Length == 0)
				return 0; //все нули

			var result = int.Parse(s, numberStyles);


			return result;
		}
		/// <summary>
		/// Отматывает на count символов назад
		/// </summary>
		/// <param name="count">Количество символов, на которое нужно отмотать</param>
		public void Rewind(int count)
		{
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count), count, @"Value could not be negative");
			if (_index - count < 0)
				throw new ArgumentOutOfRangeException(nameof(count), count, @"Value is too high");
			_index -= count;
		}
		public int ReadDecimalInt(int count)
		{
			return ReadInt(count, NumberStyles.Integer | NumberStyles.AllowLeadingSign);
		}
		/// <summary> Читает дату и время в указанном формате </summary>
		/// <param name="format">Формат даты и времени для передачи в <see cref="DateTime.ParseExact(string,string,System.IFormatProvider)"/></param>
		public DateTime ReadDateTime(string format)
		{
			var s = ReadString(format.Length);
			return DateTime.ParseExact(s, format, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal);
		}
	}
}