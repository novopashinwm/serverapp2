﻿using System;
using System.Collections;
using System.Text;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.Common;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.NavisetGT20
{
	public class NavisetGT20Reader
	{
		private readonly DataReader _reader;

		private readonly byte _packetType;

		private readonly byte[] _confirmationBytes;

		public NavisetGT20Reader(byte[] data)
		{
			_reader = new DataReader(data);
			var lengthPacketType = _reader.ReadLittleEndian32(2);
			var length = lengthPacketType & 0xFFF;

			_packetType = (byte)((lengthPacketType & 0xC000) >> 14);
			_confirmationBytes = new byte[] {0x01, data[data.Length - 2], data[data.Length - 1]};
		}

		public IList ReadUnits(NavisetGT20State state)
		{
			IList result;
			switch (_packetType)
			{
				case 0:
					result = ReadHeader();
					break;
				case 1:
					if (state == null || string.IsNullOrEmpty(state.DeviceId))
						return null;
					result = state.Modified
						? ReadPositionUnitsModified(state.DeviceId)
						: ReadPositionUnits(state.DeviceId);
					break;
				default:
					result = new ArrayList();
					break;
			}

			result.Add(new ConfirmPacket(_confirmationBytes));
			return result;
		}

		private IList ReadPositionUnits(string deviceId)
		{
			var result = new ArrayList();
			var number = _reader.ReadLittleEndian32(2);
			var additionalStructure = new BitArray(_reader.ReadBytes(2));

			while (_reader.RemainingBytesCount > 2)
			{
				var mobilUnit = ReadPositionUnit(additionalStructure);
				mobilUnit.DeviceID = deviceId;
				result.Add(mobilUnit);
			}

			return result;
		}

		private IList ReadPositionUnitsModified(string deviceId)
		{
			var result = new ArrayList();
			_reader.Skip(3);

			while (_reader.RemainingBytesCount > 2)
			{
				var mobilUnit = ReadPositionUnitModified();
				mobilUnit.DeviceID = deviceId;
				result.Add(mobilUnit);
			}

			//checksum
			_reader.Skip(2);

			return result;
		}

		private MobilUnit.Unit.MobilUnit ReadPositionUnit(BitArray additionalStructure)
		{
			var packetNumber = _reader.ReadLittleEndian32(2);
			var datetime = _reader.ReadLittleEndian32(4);
			var validitySatelites = _reader.ReadByte();
			var validity = validitySatelites & 0xF0 >> 4;
			var satelites = validitySatelites & 0xF;
			var lat = _reader.ReadLittleEndian32(4);
			var lon = _reader.ReadLittleEndian32(4);
			var speed = _reader.ReadLittleEndian32(2) / 10.0;
			var cource = _reader.ReadLittleEndian32(2) / 10.0;
			var altitude = _reader.ReadLittleEndian32(2);
			var hdop = _reader.ReadByte();

			var mobilUnit = new MobilUnit.Unit.MobilUnit
			{
				CorrectGPS = validity == 0,
				Time = (int)datetime,
				Satellites = satelites,
				Latitude = lat / 1000000.0,
				Longitude = lon / 1000000.0,
				Speed = (int)Math.Ceiling(speed),
				Course = (int)Math.Ceiling(cource),
				Height = (int)altitude
			};
			mobilUnit.Properties.Add(DeviceProperty.Protocol, ControllerType.Names.NavisetGT20);


			#region Sensors
			for (var i = 0; i < additionalStructure.Length; i++)
			{
				if (!additionalStructure[i])
					continue;

				switch (i)
				{
					case 0:
						var value = _reader.ReadByte();
						var bitArray = new BitArray(new[] { value });
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.NormalVoltage, bitArray[0] ? 1 : 0);
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.SecurityStatus, bitArray[2] ? 1 : 0);
						break;
					case 1:
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.VoltageSource, _reader.ReadLittleEndian32(2));
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.VoltageBattery, _reader.ReadLittleEndian32(2));
						break;
					case 2:
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.InternalTemperature, _reader.ReadByte());
						break;
					case 3:
						//входы
						var inputsValue = _reader.ReadByte();
						var inputs = new BitArray(new[] { inputsValue });
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.INDigital1, inputs[0] ? 1 : 0);
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.INDigital2, inputs[1] ? 1 : 0);
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.INDigital3, inputs[2] ? 1 : 0);
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.INDigital4, inputs[3] ? 1 : 0);
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.INDigital5, inputs[4] ? 1 : 0);
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.INDigital6, inputs[5] ? 1 : 0);
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.INDigital7, inputs[6] ? 1 : 0);
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.INDigital8, inputs[7] ? 1 : 0);
						//выходы
						var outputsValue = _reader.ReadByte();
						var outputs = new BitArray(new[] { outputsValue });
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.OUTDigital1, outputs[0] ? 1 : 0);
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.OUTDigital2, outputs[1] ? 1 : 0);
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.OUTDigital3, outputs[2] ? 1 : 0);
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.OUTDigital4, outputs[3] ? 1 : 0);
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.OUTDigital5, outputs[4] ? 1 : 0);
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.OUTDigital6, outputs[5] ? 1 : 0);
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.OUTDigital7, outputs[6] ? 1 : 0);
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.OUTDigital8, outputs[7] ? 1 : 0);
						break;
					case 4:
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.VoltageIN1, _reader.ReadLittleEndian32(2));
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.VoltageIN2, _reader.ReadLittleEndian32(2));
						break;
					case 5:
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.VoltageIN3, _reader.ReadLittleEndian32(2));
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.VoltageIN4, _reader.ReadLittleEndian32(2));
						break;
					case 6:
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.VoltageIN5, _reader.ReadLittleEndian32(2));
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.VoltageIN6, _reader.ReadLittleEndian32(2));
						break;
					case 7:
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.VoltageIN7, _reader.ReadLittleEndian32(2));
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.VoltageIN8, _reader.ReadLittleEndian32(2));
						break;
					case 8:
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.ExternalTemperature1, _reader.ReadByte());
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.ExternalTemperature2, _reader.ReadByte());
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.ExternalTemperature3, _reader.ReadByte());
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.ExternalTemperature4, _reader.ReadByte());
						break;
					case 9:
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.ExternalTemperature5, _reader.ReadByte());
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.ExternalTemperature6, _reader.ReadByte());
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.ExternalTemperature7, _reader.ReadByte());
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.ExternalTemperature8, _reader.ReadByte());
						break;
					case 10:
						_reader.Skip(6);
						break;
					case 11:
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.INFrequency1, _reader.ReadLittleEndian32(2));
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.INFrequency2, _reader.ReadLittleEndian32(2));
						break;
					case 12:
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.OmnicommFuelLevel, _reader.ReadLittleEndian32(2));
						_reader.Skip(2);
						break;
					case 13:
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.OmnicommTemperature, _reader.ReadByte());
						_reader.Skip(1);
						break;
					case 14:
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.CANFuelLevel, _reader.ReadByte());
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.CANRpM, _reader.ReadLittleEndian32(2));
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.CANCoolantTemperature, _reader.ReadByte());
						break;
					case 15:
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.CANFuelSpend, _reader.ReadLittleEndian32(4));
						mobilUnit.SetSensorValue((int)NavisetGT20Sensors.CANTotalrun, _reader.ReadLittleEndian32(4));
						break;
				}
			}
			#endregion

			return mobilUnit;
		}

		private MobilUnit.Unit.MobilUnit ReadPositionUnitModified()
		{
			var packetNumber = _reader.ReadLittleEndian32(2);
			var validitySatelites = _reader.ReadByte();
			var datetime = _reader.ReadLittleEndian32(4);
			var validity = validitySatelites & 0x80;
			var satelites = validitySatelites & 0xF;
			var lat = _reader.ReadLittleEndian32(4);
			var lon = _reader.ReadLittleEndian32(4);
			var speed = _reader.ReadLittleEndian32(2) / 10.0;
			var cource = _reader.ReadLittleEndian32(2) / 10.0;
			var altitude = _reader.ReadLittleEndian32(2);
			var hdop = _reader.ReadByte();
			var skiped = SkipToPacketNumber(packetNumber + 1);

			var mobilUnit = new MobilUnit.Unit.MobilUnit
			{
				CorrectGPS = validity > 0,
				Time = (int) datetime,
				Satellites = satelites,
				Latitude = lat / 1000000.0,
				Longitude = lon / 1000000.0,
				Speed = (int) Math.Ceiling(speed),
				Course = (int) Math.Ceiling(cource),
				Height = (int) altitude
			};

			mobilUnit.Properties.Add(DeviceProperty.Protocol, ControllerType.Names.NavisetGT20);
			return mobilUnit;
		}

		private int SkipToPacketNumber(uint packetNumber)
		{
			uint nextPacketNumber = 0;
			var skiped = 0;
			while (nextPacketNumber != packetNumber && _reader.RemainingBytesCount > 1)
			{
				nextPacketNumber = _reader.ReadLittleEndian32(2);
				_reader.Rewind(1);
				skiped ++;
			}

			_reader.Rewind(1);
			return skiped;
		}

		private IList ReadHeader()
		{
			_reader.Skip(2);
			var result = new ArrayList
			{
				new ReceiverStoreToStateMessage
				{
					StateData = new NavisetGT20State
					{
						DeviceId = Encoding.ASCII.GetString(_reader.ReadBytes(15)),
						Modified = _reader.TotalBytes == NavisetGT20.ModifiedLength
					}
				}
			};
			_reader.Skip(1);

			return result;
		}
	}
}