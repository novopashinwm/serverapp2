﻿namespace FORIS.TSS.Terminal.NavisetGT20
{
    public enum NavisetGT20Sensors
    {
        //в норме ли напряжение
        NormalVoltage = 0,
        //стоит ли на охране
        SecurityStatus = 1,


        VoltageSource = 2,
        VoltageBattery = 3,
        InternalTemperature = 4,

        INDigital1 = 5,
        INDigital2 = 6,
        INDigital3 = 7,
        INDigital4 = 8,
        INDigital5 = 9,
        INDigital6 = 10,
        INDigital7 = 11,
        INDigital8 = 12,

        OUTDigital1 = 13,
        OUTDigital2 = 14,
        OUTDigital3 = 15,
        OUTDigital4 = 16,
        OUTDigital5 = 17,
        OUTDigital6 = 18,
        OUTDigital7 = 19,
        OUTDigital8 = 20,

        VoltageIN1 = 21,
        VoltageIN2 = 22,
        VoltageIN3 = 23,
        VoltageIN4 = 24,
        VoltageIN5 = 25,
        VoltageIN6 = 26,
        VoltageIN7 = 27,
        VoltageIN8 = 28,

        ExternalTemperature1 = 29,
        ExternalTemperature2 = 30,
        ExternalTemperature3 = 31,
        ExternalTemperature4 = 32,
        ExternalTemperature5 = 33,
        ExternalTemperature6 = 34,
        ExternalTemperature7 = 35,
        ExternalTemperature8 = 36,

        INFrequency1 = 37,
        INFrequency2 = 38,

        OmnicommFuelLevel = 39,
        OmnicommTemperature = 40,

        CANFuelLevel = 41,
        CANRpM = 42,
        CANCoolantTemperature = 43,

        CANFuelSpend = 44,
        CANTotalrun = 45

    }
}
