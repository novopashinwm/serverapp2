﻿using System.Collections;

namespace FORIS.TSS.Terminal.NavisetGT20
{
    public class NavisetGT20:Device
    {
        public const int HeaderLength = 22;

        public const int ModifiedLength = 23;

        public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
        {
            bufferRest = null;
            if (data.Length < 22 || !NavisetGT20Helper.CheckSumm(data)) 
                return null;

            var reader = new NavisetGT20Reader(data);
            var state = stateData as NavisetGT20State;
            var units = reader.ReadUnits(state);

            return units;
        }

        public override bool SupportData(byte[] data)
        {
            if (data.Length < 22)
                return false;

            int dataLength;
            switch (data.Length)
            {
                case HeaderLength:
                    dataLength = ((data[1] & 0x3F) << 8) + data[0];
                    break;
                case ModifiedLength:
                    dataLength = ((data[1] << 8) + data[0]) & 0xFFF;
                    break;
                default:
                    return false;
            }

            if (dataLength != data.Length - 2 /*байта длины*/ - 2 /*байта CRC*/)
                return false;
            
            var crcIsCorrect = NavisetGT20Helper.CheckSumm(data);
            return crcIsCorrect;
        }
    }
}
