﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.GPS608
{
	public class AlarmDatagram : PositionDatagram
	{
		private int AlarmType;
		/*
		BO01X+GPS data
		 * 
			Alarm information：
			0：Vehicle power off 
			1：Accident 
			2：Vehicle robbery（SOS help）
			3：Vehicle anti-theft and alarming 
			4：Lowerspeed Alert
			5：Overspeed Alert 
			6:Alarm when out of Geo-fence
		 * 
		 
		(
		009136079285
		BO01 - команда
		0 - тип alarm
		120918V2254.4383N11407.9875E000.0022021000.0010000000L00000000
		)
		*/

		public AlarmDatagram(string data): base(data)
		{
			Parse();
		}

		protected override void Parse()
		{
			int gpsDataIndex = HeaderWithCommandLength + 1;
			GPSData = Packet.Substring(gpsDataIndex, Packet.Length - gpsDataIndex - 1);
			ParseGPS();

			AlarmType = int.Parse(Packet.Substring(HeaderWithCommandLength, 1));
		}

		public override IList GetResponses()
		{
			var res = base.GetResponses();
			if (res != null && res.Count > 0 && res[0] is MobilUnit.Unit.MobilUnit)
			{
				/*
				Alarm information：
				0：Vehicle power off 
				1：Accident 
				2：Vehicle robbery（SOS help）
				3：Vehicle anti-theft and alarming 
				4：Lowerspeed Alert
				5：Overspeed Alert 
				6:Alarm when out of Geo-fence
				*/
				var mu = (MobilUnit.Unit.MobilUnit)res[0];
				mu.SensorValues[50 + AlarmType] = 1;

				//Ответ прибору
				res.Add(new ConfirmPacket(Encoding.ASCII.GetBytes("(" + GetIDForResponse() + "AS01" + AlarmType.ToString() + ")")));
			}

			return res;
		}
	}
}