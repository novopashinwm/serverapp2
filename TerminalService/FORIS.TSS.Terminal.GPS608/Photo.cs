﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic;

namespace FORIS.TSS.Terminal.GPS608
{
	[Serializable]
	public class Photo
	{
		public readonly int LogTime;
		public readonly int CameraNumber;
		public readonly int PhotoNumber;
		public readonly int TotalSize;
		public readonly SortedList<int, byte[]> Parts;

		public Photo(int cameraNumber, int photoNumber, int totalSize)
		{
			CameraNumber = cameraNumber;
			PhotoNumber = photoNumber;
			LogTime = TimeHelper.GetSecondsFromBase();
			TotalSize = totalSize;
			Parts = new SortedList<int, byte[]>();
		}
	}
}