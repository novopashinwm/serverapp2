﻿using System.Collections;

namespace FORIS.TSS.Terminal.GPS608
{
	public abstract class BaseDatagram
	{
		protected int HeaderLength = 13;
		protected int HeaderWithCommandLength = 13 + 4;

		protected string Packet;
		protected string Command;
		protected bool IsCorrectCRC;
		protected string ID;

		public static string GetCommand(string packet)
		{
			if (string.IsNullOrEmpty(packet) || packet.Length < 18)
				return string.Empty;

			int packetHeaderLenght = GetHeaderLength(packet);
			if (packetHeaderLenght > 0)
				return packet.Substring(packetHeaderLenght, 4);
			return "";
		}

		public static int GetHeaderLength(string packet)
		{
			int packetHeaderLength = packet.IndexOf('B');
			if (packetHeaderLength >= 12 && packetHeaderLength <= 13)
				return packetHeaderLength;

			return -1;
		}

		protected BaseDatagram(string packet)
		{
			Packet = packet;
			HeaderLength = GetHeaderLength(packet);
			HeaderWithCommandLength = HeaderLength + 4;

			Command = GetCommand(packet);
			ParseID();
		}

		protected void ParseID()
		{
			string id = Packet.Substring(1, HeaderLength - 1);
			id = id.TrimStart('0');
			id = id.TrimEnd((char) 0x00);
			ID = id;
		}

		protected string GetIDForResponse()
		{
			return Packet.Substring(1, HeaderLength - 1);
		}

		public abstract IList GetResponses();

		public enum Sensors
		{
			D1                = 11,
			D2                = 12,
			VehiclePowerOff   = 50,
			Accident          = 51,
			SOS               = 52,
			AntiTheftAlarming = 53,
			LowerspeedAlert   = 54,
			OverspeedAlert    = 55,
			OutOfGeoFence     = 56,
			AN1               = 60,
			Temperature       = 61,
		}
	}
}