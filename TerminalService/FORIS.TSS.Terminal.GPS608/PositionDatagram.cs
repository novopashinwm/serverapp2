﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.GPS608
{
	public class PositionDatagram: BaseDatagram
	{
		public DateTime? Time;
		public bool GPSCorrect;
		public double? Lat;
		public double? Lng;
		public int? Speed;
		public int? Course;
		public bool Ignition;
		public bool MainPower;
		public int AN1;
		public bool? D2; 
		/// <summary>
		/// Температура в 0.25 градусах цельсия (4bit = 1degree)
		/// </summary>
		public int Temperature; 

		public PositionDatagram(string data): base(data)
		{
			Parse();
		}

		protected virtual void Parse()
		{
			int gpsDataIndex = -1;
			switch (Command)
			{ 
				case "BP05":
					//15 terminal ID ＋ GPS data 
					gpsDataIndex = HeaderWithCommandLength + (HeaderLength + 2);
					break;
				case "BP04": //Answer Calling Message
				case "BR00": //Isochronous for continues feedback message
				case "BR01": //Isometry continous feedback message
				case "BR02": //Continues feedback ending message
					gpsDataIndex = HeaderWithCommandLength;
					break;

			}
			GPSData = Packet.Substring(gpsDataIndex, Packet.Length - gpsDataIndex - 1);
			ParseGPS();
		 }

		protected string GPSData;

		protected string GPSPart(GpsParts part)
		{

			int dataLen = 0;
			switch (part)
			{
				case GpsParts.Date:
					dataLen = 6;
					break;
				case GpsParts.GPSCorrect:
					dataLen = 1;
					break;
				case GpsParts.Lat:
					dataLen = 9;
					break;
				case GpsParts.LatInd:
					dataLen = 1;
					break;
				case GpsParts.Lng:
					dataLen = 10;
					break;
				case GpsParts.LngInd:
					dataLen = 1;
					break;
				case GpsParts.Speed:
					dataLen = 5;
					break;
				case GpsParts.Time:
					dataLen = 6;
					break;
				case GpsParts.Orientation:
					dataLen = 6;
					break;
				case GpsParts.IOState:
					dataLen = 8;
					break;
				case GpsParts.Milepost:
					dataLen = 1;
					break;
				case GpsParts.MileData:
					dataLen = 8;
					break;
			}

			if (((int)part + dataLen) > GPSData.Length)
				return "";

			return GPSData.Substring((int)part, dataLen);
		}

		protected enum GpsParts: int
		{ 
			Date=0,
			GPSCorrect=Date+6,
			Lat = GPSCorrect+1,
			LatInd = Lat +9,
			Lng = LatInd + 1,
			LngInd = Lng + 10,
			Speed = LngInd + 1,
			Time = Speed+5,
			Orientation = Time+6,
			IOState = Orientation+6,
			Milepost = IOState + 8,
			MileData = Milepost + 1,
		}

		/// <summary>
		/// Для приборов JB 101 оранжевый провод передает свое состояние 0 или 1 в третий символ строки IOState
		/// </summary>
		protected void ParseD2()
		{
			string strD2 = GPSPart(GpsParts.IOState).Substring(2, 1);

			//Там еще могут быть 8 - это приборы с датчиками температуры так передают - тогда нет состояния у D2
			if (strD2 != "0" && strD2 != "1")
			{
				D2 = null;
				return;
			}

			D2 = strD2 == "1";
		}

		protected void ParseTemperature()
		{
			//3 цифры перед L - это входное напряжение на AN1 вход в HEX формате
			var startIndex = Packet.IndexOf("L");
			if (startIndex <= 6) 
				return;

			uint temperatureRawValue;
			if (!uint.TryParse(
				Packet.Substring(startIndex - 6, 3), 
				NumberStyles.HexNumber, 
				CultureInfo.InvariantCulture,
				out temperatureRawValue)) 
				return;

			Temperature = (int) (temperatureRawValue & 0x7ff); //Отбрасываем 0x800 - бит знака температуры
			if ((temperatureRawValue & 0x800) != 0)
				Temperature = -Temperature;
		}


		protected void ParseAN1()
		{
			//3 цифры перед L - это входное напряжение на AN1 вход в HEX формате
			if (Packet.IndexOf("L") > 3)
			{
				int.TryParse(Packet.Substring(Packet.IndexOf("L") - 3, 3), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out AN1);
			}
		}

		protected void ParseGPS()
		{
			
			/*
			(008800293731BP05000008800293731101207A2829.9881N07704.7801E000.0092955000.0000000000L00000000)
			* 
			GPS data 
			101207A2829.9881N07704.7801E000.0092955000.0000000000L00000000
			101207 - date yymmdd
			A - GPSCorrect
			2829.9881 - The unit is degree for he front two bytes，from 0～90；the unit is cent for later seven bytes
			N - “N” means north latitude ， ”S” means south latitude
			07704.7801 - The unit is degree for he front three bytes, from 0～180；the unit is cent for later seven bytes
			E - “E” means east longitude，”W” means west longitude
			000.0 - speed
			092955 - time hhmmss
			000.00 - Course
			00000000 - IO
			L00000000 - пробег
			*/

			//Дата Время
			DateTime time;
			if (DateTime.TryParseExact(GPSPart(GpsParts.Date) + GPSPart(GpsParts.Time),
									   "yyMMddHHmmss", CultureInfo.InvariantCulture,
									   DateTimeStyles.AdjustToUniversal, out time))
			{
				Time = time;
			}

			if (Time != null && Time.HasValue)
			{
				GPSCorrect = GPSPart(GpsParts.GPSCorrect) == "A";

				if (GPSCorrect)
				{
					//2254.4390 - The unit is degree for he front two bytes，from 0～90；the unit is cent for later seven bytes
					var latStr = GPSPart(GpsParts.Lat);
					Lat = double.Parse(latStr.Substring(0, 2), CultureInfo.InvariantCulture);
					Lat += double.Parse(latStr.Substring(2, 7), CultureInfo.InvariantCulture) / 60;
					Lat *= GPSPart(GpsParts.LatInd) == "S" ? -1 : 1;

					//11407.9872 - The unit is degree for he front three bytes, from 0～180；the unit is cent for later seven bytes
					var lngStr = GPSPart(GpsParts.Lng);
					Lng = double.Parse(lngStr.Substring(0, 3), CultureInfo.InvariantCulture);
					Lng += double.Parse(lngStr.Substring(3, 7), CultureInfo.InvariantCulture) / 60;
					Lng *= GPSPart(GpsParts.LngInd) == "W" ? -1 : 1;

					//speed - 000.00 - The unit is km/h
					Speed = (int)double.Parse(GPSPart(GpsParts.Speed), CultureInfo.InvariantCulture);

					
					//Orientation (Course)
					double dblCourse;
					if (double.TryParse(GPSPart(GpsParts.Orientation), NumberStyles.Number, CultureInfo.InvariantCulture,
									out dblCourse))
					{
						Course = (int)dblCourse;
					}
				}

				/*
				IO
					The 8 bits of IO
					The first bit representative of the main power switch, "0" means the main power-on, 
					"1", means the main power-off.
					The second bit on behalf of the ACC (ignition), "0" means ACC off,
					"1" means ACC on.
					
					Other reservations
				*/
				Ignition = GPSPart(GpsParts.IOState).Substring(1, 1) == "1";
				MainPower = GPSPart(GpsParts.IOState).Substring(0, 1) == "0";

				ParseAN1();
				ParseTemperature();
				ParseD2();
			}
		}

		public override IList GetResponses()
		{
			if (Time == null || !Time.HasValue)
				return null;

			var res = new ArrayList();

			var mu = new MobilUnit.Unit.MobilUnit
			{
				cmdType = CmdType.Trace,
				DeviceID = ID,
				Time = BusinessLogic.TimeHelper.GetSecondsFromBase(Time.Value),
				CorrectGPS = GPSCorrect
			};

			mu.Properties.Add(DeviceProperty.Protocol, JB101.ProtocolName);
			if (GPSCorrect)
			{
				mu.Latitude = Lat.Value;
				mu.Longitude = Lng.Value;
				mu.Satellites = 10;
				if (Speed.HasValue) mu.Speed = Speed.Value;
				if (Course.HasValue) mu.Course = Course.Value;
			}

			//Заполняем массив показаний датчиков
			mu.SensorValues = new Dictionary<int, long>
								  {
									  {(int) Sensors.D1, Ignition ? 1 : 0},
									  {(int) Sensors.VehiclePowerOff, MainPower ? 0 : 1},
									  {(int) Sensors.AN1, AN1},
									  {(int) Sensors.Temperature, Temperature}
								  };

			for (int alarmType = 0; alarmType <= 6; ++alarmType)
				mu.SensorValues[50 + alarmType] = 0;

			if (D2.HasValue) mu.SensorValues.Add((int)Sensors.D2, D2.Value?1:0);

			res.Add(mu);

			//Ответ прибору
			switch (Command)
			{
				case "BP05":
					res.Add(new ConfirmPacket(Encoding.ASCII.GetBytes("(" + GetIDForResponse() + "AP05"  + ")")));
					break;
			}
			return res;
		}
	}
}