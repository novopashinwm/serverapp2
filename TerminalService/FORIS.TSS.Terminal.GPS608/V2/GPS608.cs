﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.GPS608.V2
{
	public class GPS608 : Device
	{
		private static readonly char[] GpsValiditySign = {'A', 'V'};
		private const char MileageSign = 'L';

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			return Parse(data, out bufferRest);
		}

		public override bool SupportData(byte[] data)
		{
			if (data == null)
				return false;

			/*
			Head 	Serial number/ Time	Command 	    Message Body 	                                                                                            Trail
			1 byte 	<=12 byte 		    4 byte 	        N byte (N≤1K) 	                                                                                            1byte
														Device ID    YYMMDD GPS Validity Latitude   Longitude   Speed Time   Direction   Alarm   Sensors
																			A = valid,
																			V = invalid
old         (        09639014685        BP05    000     09639014685  120121 A            2831.2262N 07712.1900E 019.2 120921 282.60              01000000 L 017E98DF) 
new         (       008130691969        BP05    000     008130691969 120119 A            2830.0068N 07704.7693E 000.0 145804 134.58      00      00000000 L 00000037)
			 */
			string strVal = Encoding.ASCII.GetString(data).Trim();
			if (strVal.Length < 18)
				return false;
			
			if (!strVal.StartsWith("(") || !strVal.EndsWith(")"))
				return false;

			// Отделение этого протокола от протокола Vjoycar
			if (strVal.Contains(","))
				return false;

			if (!strVal.Contains("B"))
				return false;

			//(008130691969BP05000008130691969120119A2830.0068N07704.7693E000.0145804134.580000000000L00000037)
			var indexOfGpsValiditySign = strVal.IndexOfAny(GpsValiditySign);
			if (indexOfGpsValiditySign == -1)
				return false;
			var indexOfMileageSign = strVal.IndexOf(MileageSign);
			if (indexOfMileageSign == -1)
				return false;

			if (indexOfMileageSign - indexOfGpsValiditySign != 49)
				return false;

			return true;
		}

		public IList Parse(byte[] data, out byte[] bufferRest)
		{
			bufferRest = null;

			if (data == null || data.Length == 0)
				return null;

			var alRes = new ArrayList();

			string strVal = Encoding.ASCII.GetString(data).Trim();

			//TODO: как оказалось, в данных могут быть бинарные вставки, поэтому следует использовать DataReader
			var reader = new StringReader(strVal);

			reader.Skip('(');
			var deviceId = reader.ReadBefore('B');
			var command = reader.ReadString(4);
			var sensors = new Dictionary<int, long>();
			string dateString;
			switch (command)
			{
				case "BP05": // Answer device login response message (Периодическая посылка координат)
					reader.SkipBeforeAndIncluding(deviceId);
					//Ответ прибору
					alRes.Add(new ConfirmPacket(Encoding.ASCII.GetBytes("(" + deviceId + "AP05" + ")")));

					dateString = reader.ReadString(6);
					break;
				case "BP04": //Answer Calling Message
				case "BR00": //Isochronous for continues feedback message
				case "BR01": //Isometry continous feedback message
				case "BR02": //Continues feedback ending message
					{
						var s = reader.ReadBefore(GpsValiditySign);
						if (string.IsNullOrEmpty(s) || s.Length < 6)
						{
							Trace.TraceWarning("Invalid GPS608 packet: " + strVal);
							return null;
						}

						dateString = s.Substring(s.Length - 6);
					}
					break;
				default:
					Trace.TraceWarning("GPS608: Command {0} is not supported in message {1}", command, strVal);
					return null; // Неподдерживаемая команда
			}

			var date = DateTime.ParseExact(dateString, "yyMMdd",
				CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
			var validGps = reader.ReadOneOf('A', 'V') == 'A';
			var latDegrees = reader.ReadDecimal(2);
			var latMinutes = reader.ReadDecimal(7);
			var latitude = latDegrees + latMinutes/60;
			if (reader.ReadOneOf('N', 'S') == 'S')
				latitude = -latitude;
			var lngDegrees = reader.ReadDecimal(3);
			var lngMinutes = reader.ReadDecimal(7);
			var longitude = lngDegrees + lngMinutes/60;
			if (reader.ReadOneOf('E', 'W') == 'W')
				longitude = -longitude;
			var speed = (int) reader.ReadDecimal(5);
			var timeString = reader.ReadString(6);
			var time =
				DateTime.ParseExact(timeString, "HHmmss", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal)
					.TimeOfDay;
			var direction = reader.ReadDecimal(6);
			var alarmAndIO = reader.ReadBefore('L');
			if (alarmAndIO.Length != 10)
			{
				Trace.TraceWarning("GPS608: Invalid count of symbols before L delimeter: " + strVal);
				return null;
			}
			var sensorsReader = new StringReader(alarmAndIO);
			
			var light = sensorsReader.ReadHexInt(1);
			sensors[(int) GPS608Sensors.Pin03LightsOrange] = ((light & 0x01) != 0) ? 1 : 0;
			var alarm = sensorsReader.ReadHexInt(1);
			foreach (var pair in AlarmValues)
				sensors[(int) pair.Key] = (alarm == pair.Value) ? 1 : 0;

			var ioStateByte = sensorsReader.ReadHexInt(2);

			//Bit 0
			var power = (ioStateByte & 0x01) != 0;
			sensors[(int) GPS608Sensors.Pin16PowerOn] = power ? 1 : 0;

			//Bit 1
			var ignition = (ioStateByte & 0x02) != 0;
			sensors[(int) GPS608Sensors.Pin14GreenIgnition] = ignition ? 1 : 0;

			//Bit 2
			var door = (ioStateByte & 0x04) != 0;
			sensors[(int) GPS608Sensors.Pin13BrownDoorControl] = door ? 1 : 0;

			//Bit 3
			var airconditioner = (ioStateByte & 0x08) != 0;
			sensors[(int)GPS608Sensors.Pin15PurpleAirConditioner] = airconditioner ? 1 : 0;

			//Неизвестно, для чего используется bit 4
			//var airCondition = (ioStateByte & 0x10) != 0;
			//sensors[(int) GPS608Sensors.Pin15PurpleAirConditioner] = airCondition ? 1 : 0;

			var temperatureRawValue = sensorsReader.ReadHexInt(3);
			var temperatureValue = temperatureRawValue & 0x7ff;
			if ((temperatureRawValue & 0x800) != 0)
				temperatureValue = -temperatureValue;
			sensors[(int) GPS608Sensors.Pin10GreenBlackTemperature] = temperatureValue;

			var fuelValue = sensorsReader.ReadHexInt(3);
			sensors[(int) GPS608Sensors.Pin11YellowBlackFuel] = fuelValue;

			var mu = UnitReceived();
			mu.DeviceID = deviceId.TrimStart('0');
			mu.cmdType = CmdType.Trace;
			mu.Time = BusinessLogic.TimeHelper.GetSecondsFromBase(date + time);
			mu.CorrectGPS = validGps;
			mu.SensorValues = sensors;
			if (validGps)
			{
				mu.Latitude = (double) latitude;
				mu.Longitude = (double) longitude;
				mu.Speed = speed;
				mu.Course = (int?) direction;
			}

			alRes.Add(mu);

			return alRes;
		}

		private static readonly Dictionary<GPS608Sensors, int> AlarmValues =
			new Dictionary<GPS608Sensors, int>
				{
					{ GPS608Sensors.Pin06BlueSOS,      0x01 },
					{ GPS608Sensors.Pin02AccidentGray, 0x02 },
					{ GPS608Sensors.Movement,          0x03 },
					{ GPS608Sensors.OverSpeedAlert,    0x04 },
					{ GPS608Sensors.OutOfGeoFence,     0x05 },
				};
	}
}