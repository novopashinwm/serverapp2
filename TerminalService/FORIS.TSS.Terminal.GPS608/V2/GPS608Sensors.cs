﻿namespace FORIS.TSS.Terminal.GPS608.V2
{
	public enum GPS608Sensors
	{
		Pin02AccidentGray =             02,
		Pin03LightsOrange =             03,
		Pin06BlueSOS =                  06,
		Pin10GreenBlackTemperature =    10,
		Pin11YellowBlackFuel =          11,
		Pin13BrownDoorControl =         13,
		Pin14GreenIgnition =            14,
		Pin15PurpleAirConditioner =     15,
		Pin16PowerOn =                  16,
		OverSpeedAlert =                55,
		OutOfGeoFence =                 56,
		Movement =                      57,
	}
}