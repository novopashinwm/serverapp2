﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FORIS.TSS.Terminal.GPS608
{
	class MessageBuilder
	{
		private readonly List<byte> _bytes;
		private State _state;

		private enum State
		{
			Command,
			Parameters,
			Finished,
		}

		public MessageBuilder(byte[] deviceId)
		{
			_bytes = new List<byte>(64) { (byte)'(' };

			const int deviceIdLength = 12;

			for (int ind = 0; ind < deviceIdLength - deviceId.Length; ind++)
				_bytes.Add((byte)'0');
			_bytes.AddRange(deviceId);

			_state = State.Command;
		}

		public MessageBuilder(string deviceId)
			: this(Encoding.ASCII.GetBytes(deviceId))
		{
		}

		public void AddCommand(string command)
		{
			if (_state != State.Command)
				throw new InvalidOperationException("Command cannot be placed here, state is " + _state);

			_bytes.AddRange(Encoding.ASCII.GetBytes(command));

			_state = State.Parameters;
		}

		public void AddByte(byte b)
		{
			if (_state != State.Parameters)
				throw new InvalidOperationException("Byte cannot be placed here, state is " + _state);

			_bytes.Add(b);
		}

		public byte[] Finish()
		{
			_bytes.Add((byte)')');
			return _bytes.ToArray();
		}

		/// <summary> Добавляет число как последовательность байт в Big Endian (первым идёт самый старший байт) </summary>
		/// <param name="bytesCount"> Количество младших байт для добавления </param>
		/// <param name="value"> Значение </param>
		public void AddIntBE(int bytesCount, int value)
		{
			var shift = (bytesCount - 1) * 8;
			while (bytesCount != 0)
			{
				_bytes.Add((byte)((value >> shift) & 0xff));
				shift -= 8;
				bytesCount--;
			}
		}
	}
}