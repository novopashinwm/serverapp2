﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.GPS608
{
	public class JB101 : Device
	{
		internal const string ProtocolName = "SkyTrack JB101";

		private static readonly char[] GpsValiditySign = { 'A', 'V' };

		private readonly ConcurrentDictionary<string, List<Photo>> _photos =
			new ConcurrentDictionary<string, List<Photo>>();
		internal static class DeviceNames
		{
			internal const string SkyTrackJB101 = "SkyTrack JB101";
		}
		public JB101() : base(null, new[]
		{
			DeviceNames.SkyTrackJB101,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			if (data == null)
				return false;

			/*
			| Head       | Serial number/Time | Command | Message Body  | Trail      |
			| 1 byte '(' | 12 byte            | 4 byte  | N byte (N≤1K) | 1 byte ')' |
			(009136079285BP05000009136079285120918A2254.4390N11407.9872E000.00208440.000000000000L00000000)
			*/

			if (data.Length < 18)
				return false;

			var i = 0;
			while (data[i] == '\n' || data[i] == '\r' || data[i] == ' ')
			{
				++i;
				if (i == data.Length)
					return false;
			}

			if (data[i] != '(' || data[data.Length - 1] != ')')
				return false;

			// Отделение этого протокола от протокола Vjoycar
			var cIndex = Array.IndexOf(data, (byte)',', i + 1);
			if (cIndex != -1)
				return false;

			var bIndex = Array.IndexOf(data, (byte)'B', i + 1);
			if (bIndex == -1)
				return false;

			if (bIndex - i < 9 || 20 < bIndex - i)
				return false;

			return GpsValiditySign.Any(gpsValiditySign => Array.IndexOf(data, (byte)gpsValiditySign) != -1);
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			var state = stateData as ReceiverState ?? new ReceiverState();

			var result = Parse(data, out bufferRest, state);

			result.Add(new ReceiverStoreToStateMessage {StateData = state});

			return result;
		}
		private IList Parse(byte[] data, out byte[] bufferRest, ReceiverState state)
		{
			return Process(data, out bufferRest, state);
		}
		public override byte[] GetCmd(IStdCommand cmd)
		{
			if (cmd.Target.DeviceID == null)
				return null;

			var mb = new MessageBuilder(cmd.Target.DeviceID);

			string command, cmdType, commandWord;
			switch (cmd.Type)
			{
				case CmdType.CutOffElectricity:
					commandWord = "AV0";
					command = "0";
					cmdType = "0";
					break;
				case CmdType.ReopenElectricity:
					commandWord = "AV0";
					command = "1";
					cmdType = "0";
					break;
				case CmdType.CutOffFuel:
					commandWord = "AV0";
					command = "0";
					cmdType = "1";
					break;
				case CmdType.ReopenFuel:
					commandWord = "AV0";
					command = "1";
					cmdType = "1";
					break;
				case CmdType.CapturePicture:
					{
						mb.AddCommand("AY01");
						//TODO: брать значения полей Camera Number, Photo Number и Photo Quality из параметров команды
						mb.AddByte(0);
						mb.AddByte(0);
						mb.AddByte(1);
						return mb.Finish();
					}
				default:
					return null;
			}

			//TODO: use OutgoingMessageBuilder
			command =
				string.Format("({0}{1}{2}{3})", GetDeviceIdForSendingToTracker(cmd.Target.DeviceID), commandWord, cmdType, command);
			return Encoding.ASCII.GetBytes(command);
		}
		/// <summary> Получить deviceId для отправки команды </summary>
		/// <param name="deviceId"></param>
		/// <returns></returns>
		private static string GetDeviceIdForSendingToTracker(string deviceId)
		{
			const int deviceIdLength = 12;
			var result = new StringBuilder();
			for (int ind = 0; ind < deviceIdLength - deviceId.Length; ind++)
				result.Append("0");
			foreach (var b in deviceId)
				result.Append(b);
			return result.ToString();
		}
		public IList Process(byte[] data, out byte[] bufferRest, ReceiverState state)
		{
			bufferRest = null;

			if (data == null || data.Length == 0)
				return null;

			var alRes = new ArrayList();

			var packetStart = Array.IndexOf(data, (byte)'(');
			if (packetStart != 0)
			{
				var newData = new byte[data.Length - packetStart];
				Array.Copy(data, packetStart, newData, 0, newData.Length);
				data = newData;
			}

			var responses = ProcessPacket(data);
			if (responses != null && responses.Count > 0)
			{
				alRes.AddRange(responses);
			}

			return alRes;
		}
		private IList ProcessPacket(byte[] packet)
		{
			BaseDatagram datagram;

			var strVal = Encoding.ASCII.GetString(packet);
			string command = BaseDatagram.GetCommand(strVal);
			switch (command)
			{
				case "BP05": // Answer device login response message (Периодическая посылка координат)
				case "BP04": // Answer Calling Message
				case "BR00": // Isochronous for continues feedback message
				case "BR01": // Isometry continous feedback message
				case "BR02": // Continues feedback ending message
					datagram = new PositionDatagram(strVal);
					break;
				case "BO01": // BO01 - Alarm message
					datagram = new AlarmDatagram(strVal);
					break;
				case "BV00": // control circuit
					return ProcessControlCircuitAnswer(strVal);
				case "BV01":
					return ProcessControlFuelAnswer(strVal);
				case "BY01": // answer request photo
					return GetAnswerRequestPhoneResponses(packet);
				case "BY02": // send photo packet to server
					return GetSendPhotoPacketToServerResponses(packet);
				default:
					Trace.TraceWarning("{0}: command \"{1}\" is not supported, packet: {2}",
						"GPS608", command, BitConverter.ToString(packet));
					return null;
			}
			return datagram.GetResponses();
		}
		private IList ProcessControlCircuitAnswer(string packet)
		{
			return ProcessControlAnswer(packet, CmdType.ReopenElectricity, CmdType.CutOffElectricity, "BV00");
		}
		private IList ProcessControlFuelAnswer(string packet)
		{
			return ProcessControlAnswer(packet, CmdType.ReopenFuel, CmdType.CutOffFuel, "BV01");
		}
		private IList ProcessControlAnswer(string packet, CmdType positiveCmdType, CmdType negativeCmdType, string type)
		{
			var cmdType = packet.Substring(packet.Length - 2, 1).Contains("1")
				? positiveCmdType
				: negativeCmdType;

			var deviceId = GetNormalizedDeviceId(packet.Substring(1, packet.IndexOf(type, StringComparison.Ordinal) - 1));
			return new List<object> {CreateCommandCompletedNotification(deviceId, cmdType)};
		}
		private IList GetSendPhotoPacketToServerResponses(byte[] packet)
		{
			var r = new DataReader(packet);
			r.SkipChar('(');
			var deviceId = r.ReadAsciiString(12);
			r.SkipAsciiString("BY02");
			var cameraNumber = r.ReadByte();
			var photoNumber = r.ReadByte();
			var photoPacketNumber = (int) r.ReadBigEndian32(2);
			var photoPacketContentLength = (int) r.ReadBigEndian32(2);
			var photoPartData = r.ReadBytes(photoPacketContentLength);
			var receivedCrc = r.ReadBigEndian32(2);

			var calculatedCrc = receivedCrc; //TODO: рассчитать CRC
			if (calculatedCrc != receivedCrc)
			{
				var mb = new MessageBuilder(deviceId);
				mb.AddCommand("AY02");
				mb.AddByte(cameraNumber);
				mb.AddByte(photoNumber);
				mb.AddIntBE(2, photoPacketNumber);
					//photo data packet number, two bytes, hex format, value from 0x0001 start
				return new ArrayList {new ConfirmPacket(mb.Finish())};
			}

			r.SkipChar(')');


			var deviceIdString = deviceId;
			var photos = _photos.GetOrAdd(deviceIdString, delegate { return new List<Photo>(); });
			
			Photo photo = photos.FirstOrDefault(
				p => p.CameraNumber == cameraNumber && p.PhotoNumber == photoNumber);
			if (photo == null)
				return null;

			photo.Parts[photoPacketNumber] = photoPartData;

			var readyContentLength = photo.Parts.Values.Sum(part => part.Length);
#if DEBUG
			Trace.TraceInformation(
				"{0} / {1} bytes of photo uploaded (part {2}) from {3}.{4}.{5}",
				readyContentLength, photo.TotalSize, photoPacketNumber, deviceIdString, cameraNumber, photoNumber);
#endif
			if (readyContentLength < photo.TotalSize)
			{
				var ui = UnitInfo.Factory.Create(UnitInfoPurpose.Undefined);
				ui.DeviceID = GetNormalizedDeviceId(deviceId);
				if (!Manager.FillUnitInfo(ui))
					return null;

				var result = new ArrayList
				{
					new NotifyEventArgs(
						TerminalEventMessage.Picture,
						null, Severity.Lvl7,
						NotifyCategory.Controller,
						null, null, ui, cameraNumber, photoNumber,
						photo.TotalSize, readyContentLength)
				};

				//Проверка на пропущенные части
				var partAfterLost = photo.Parts.Keys.FirstOrDefault(p => 1 < p && !photo.Parts.ContainsKey(p - 1));
				if (partAfterLost != default(int))
				{
					var lostPart = partAfterLost - 1;
#if DEBUG
					Trace.TraceInformation("Requesting lost part {0} from  {1}.{2}.{3}", lostPart, deviceIdString, cameraNumber, photoNumber);
#endif
					var mb = new MessageBuilder(deviceId);
					mb.AddCommand("AY02");
					mb.AddByte(cameraNumber);
					mb.AddByte(photoNumber);
					mb.AddIntBE(2, lostPart);
					
					result.Add(new ConfirmPacket(mb.Finish()));
				}
				else
				{
					var lastPacketNumber = photo.Parts.Keys[photo.Parts.Count-1];

					//Устройство стабильно не досылает ровно последний пакет
					if (photoPacketNumber == lastPacketNumber && photo.TotalSize - readyContentLength < photoPartData.Length)
					{
						var mb = new MessageBuilder(deviceId);
						mb.AddCommand("AY02");
						mb.AddByte(cameraNumber);
						mb.AddByte(photoNumber);
						mb.AddIntBE(2, photoPacketNumber+1);

						result.Add(new ConfirmPacket(mb.Finish()));
					}
				}

				return result;
			}


#if DEBUG
			Trace.TraceInformation("Photo is ready {0}.{1}.{2}", deviceIdString, cameraNumber, photoNumber);
#endif

			var bytes = new byte[readyContentLength];
			var i = 0;
			foreach (var part in photo.Parts.Values)
			{
				Array.Copy(part, 0, bytes, i, part.Length);
				i += part.Length;
			}

			//Завершение приёма фото
			var pictureUnit = new MobilUnit.Unit.PictureUnit
				{
					DeviceID = GetNormalizedDeviceId(deviceId),
					Time = photo.LogTime,
					cmdType = CmdType.CapturePicture,
					CameraNumber = cameraNumber,
					PhotoNumber = photoNumber,
					Bytes = bytes,
					Satellites = 0,
					CorrectGPS = false,
				};

			photos.Remove(photo);

			var resultList = new ArrayList
				{
					pictureUnit, 
					CreateCommandCompletedNotification(pictureUnit.DeviceID, CmdType.CapturePicture)
				};
			return resultList;
		}
		private IList GetAnswerRequestPhoneResponses(byte[] packet)
		{
			var r = new DataReader(packet);
			r.SkipChar('(');
			var deviceId = r.ReadAsciiString(12); //deviceId
			r.SkipAsciiString("BY01");
			var cameraNumber = r.ReadByte();
			var photoNumber = r.ReadByte();
			var photoPacketLength = (int) r.ReadBigEndian32(3);
			r.SkipChar(')');

			if (photoPacketLength != 0)
			{
				var photos = _photos.GetOrAdd(deviceId, delegate { return new List<Photo>(); });
				photos.RemoveAll(p => p.CameraNumber == cameraNumber && p.PhotoNumber == photoNumber);
				photos.Add(new Photo(cameraNumber, photoNumber, photoPacketLength));
			}
			else
			{
				Trace.TraceWarning(
					"Device {0} reported that it was unable to capture the picture",
					deviceId);
			}

			var ui = UnitInfo.Factory.Create(UnitInfoPurpose.Undefined);
			ui.DeviceID = GetNormalizedDeviceId(deviceId);
			if (!Manager.FillUnitInfo(ui))
				return null;

			return new ArrayList
				{
					new NotifyEventArgs(
						TerminalEventMessage.Picture,
						null,
						Severity.Lvl7,
						NotifyCategory.Controller,
						null,
						null,
						ui, cameraNumber, photoNumber, photoPacketLength)
				};
		}
		private static string GetNormalizedDeviceId(string deviceId)
		{
			return deviceId.TrimStart('0');
		}
		public override IEnumerable<ITerminal> CreateTerminal(CmdType type)
		{
			switch (type)
			{
				case CmdType.CutOffElectricity:
				case CmdType.ReopenElectricity:
				case CmdType.CutOffFuel:
				case CmdType.ReopenFuel:
				case CmdType.CapturePicture:
					return Manager.GetLiveTerminals(Media.TCP);
				default:
					return base.CreateTerminal(type);
			}
		}
	}
}