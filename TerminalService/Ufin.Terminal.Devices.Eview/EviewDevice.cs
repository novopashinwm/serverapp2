﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace Compass.Ufin.Terminal.Devices.Eview
{
	public partial class EviewDevice : Device
	{
		internal const string ProtocolName = "Eview Communication Protocol";
		internal static class DeviceNames
		{
			internal const string EviewEV_07B = "Eview MultiScan EV-07B";
			internal const string EviewEV_201 = "Eview Smartx EV-201";
		}
		private readonly byte[] Prefix = { EviewDeviceConsts.Cmd_Head };

		public EviewDevice() : base(typeof(EviewSensor), new[]
		{
			DeviceNames.EviewEV_07B,
			DeviceNames.EviewEV_201,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			if (data == null)
				return false;
			/*
			| Head          | properties | length               | Check sum   | Sequence id | Message body           |
			| 1 byte '0xAB' | 1 byte     | 2 byte (LE) max=1024 | 2 byte (LE) | 2 byte (LE) | N byte (N≤1K) = length |
			"ab:10:15:00:a9:84:8f:03:03:10:01:38:36:39:32:37:30:30:34:38:35:32:33:36:38:31:02:10:5a"
			FullSize=length+8
			*/
			// Самый маленький пакет с однобайтовым телом и 8 байт заголовка
			if (data.Length < 9)
				return false;
			// Пакет начинается с префикса текущего протокола
			if (!data.StartsWith(Prefix))
				return false;
			var packetLength = data.FromLE2UInt16(2, 2);
			if (1024 < packetLength)
				return false;
			if (data.Length < (packetLength + 8))
				return false;
			return true;
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			var results = new ArrayList();
			bufferRest = null;
			var dataReader = new DataReader(data, 0, count);
			// Обрабатываем пока еще остался хотябы один заголовок
			while(EviewDataHead.HeadSize < dataReader.RemainingBytesCount)
			{
				// Читаем байты заголовка пакета
				var dataHeadRaw = dataReader.ReadBytes(EviewDataHead.HeadSize);
				// Разбираем байты заголовка пакета
				var dataHeadObj = new EviewDataHead(dataHeadRaw);
				if (!dataHeadObj.IsParsed)
					throw new DataException("Header is not valid");
				if (dataHeadObj.Properties.Encryption != EviewDataHead.EncriptionType.NotEncrypted)
					throw new NotSupportedException("Encryption not supported");
				// Проверяем достаточность байт в теле пакета, если нет возвращаем вместе с заголовком
				if (dataReader.RemainingBytesCount < (int)dataHeadObj.Length)
				{
					// Откатываем позицию в потоке на размер заголовка
					dataReader.Rewind(EviewDataHead.HeadSize);
					break;
				}
				// Читаем байты тела пакета
				var dataBodyRaw = dataReader.ReadBytes((int)dataHeadObj.Length);
				// Считаем контрольную сумму
				var calcedChkSum = dataBodyRaw.Crc16Ccitt(0x0000);
				// Проверяем контрольную сумму
				if (calcedChkSum != dataHeadObj.CheckCrc)
					throw new DataException("Checksum is not valid");
				var dataBodyObj = new EviewDataBody(dataBodyRaw);
				// Разбираем данные пакета
				if (!dataBodyObj.IsParsed)
					throw new DataException("Body is not valid");
				if (dataHeadObj.Properties.Flag_ACK)
					results.Add(GetConfirmPacket(dataHeadObj, dataBodyObj));
				//$"EviewCmdType.{((EviewCmdType)dataBodyObj.CmdType).ToString()}".TraceInformation();
				switch ((EviewCmdType)dataBodyObj.CmdType)
				{
					case EviewCmdType.Data:
						results.AddRange(ParseCmdTypeData(dataBodyObj.Cmds));
						break;
					case EviewCmdType.Config:
					case EviewCmdType.Service:
					case EviewCmdType.System:
					case EviewCmdType.Update:
					case EviewCmdType.Response:
					case EviewCmdType.FactoryTest:
					default:
						break;
				}
			}
			// Очистка пустых элементов
			for (int i = results.Count - 1; i >= 0; i--)
				if (null == results[i])
					results.RemoveAt(i);
			bufferRest = dataReader.GetRemainingBytes();
			return results;
		}
		#region ParseCmdTypeData
		private ConfirmPacket GetConfirmPacket(EviewDataHead dataHead, EviewDataBody dataBody)
		{
			var resDataBody = new byte[]
			{
				(byte)EviewCmdType.Response,
				(byte)1,
				(byte)EviewCmdResponseType.Success
			};
			var resDataHead = new List<byte>();
			resDataHead.Add((byte)EviewDeviceConsts.Cmd_Head);
			resDataHead.Add((byte)0);
			resDataHead.AddRange(((ushort)resDataBody.Length).ToLE());
			resDataHead.AddRange(((ushort)resDataBody.Crc16Ccitt(0x0000)).ToLE());
			resDataHead.AddRange(((ushort)dataHead.SequenceId).ToLE());
			resDataHead.AddRange(resDataBody);
			return new ConfirmPacket(resDataHead.ToArray());
		}
		/// <summary> Разбор данных трекера </summary>
		/// <param name="cmds"></param>
		/// <returns></returns>
		/// <remarks>
		/// Каждый новый статус передает метку времени и все данные без времени привязываются к последнему статусу.
		/// Данные со временем, должны организовывать свой MobilUnit и добавлять его в результаты сразу, без упора на статус
		/// </remarks>
		private MobilUnit[] ParseCmdTypeData(EviewDataBody.Cmd[] cmds)
		{
			var results = new List<MobilUnit>();
			if (null == cmds)
				return results.ToArray();
			var devId = default(string);
			foreach (var cmd in cmds)
			{
				//$"\tEviewCmdDataType.{((EviewCmdDataType)cmd.CmdKey).ToString()}".TraceInformation();
				var munit = default(MobilUnit);
				switch ((EviewCmdDataType)cmd.CmdKey)
				{
					// Эта часть всегда первая и обязательная, поэтому тут создаем объект
					case EviewCmdDataType.IMEI:
						// Заполняем идентификатор устройства
						devId = Encoding.ASCII.GetString(cmd.CmdVal);
						break;
					case EviewCmdDataType.Status:
						// Ищем последний с таким же временем, и если его нет создаем новый
						munit = results.LastOrDefault(mu => mu.Time == (int)ParseCmdTypeDataStatusGetTimestamp(cmd.CmdVal));
						if (null == munit)
							results.Add(munit = UnitReceived(ProtocolName));
						// Заполняем координаты, т.к. в пакете их может не быть, а 0 это нормальные координаты
						munit.Latitude  = MobilUnit.InvalidLatitude;
						munit.Longitude = MobilUnit.InvalidLongitude;
						// Разбираем и заполняем статус, тут приходит метка времени
						ParseCmdTypeDataStatus(munit, cmd.CmdVal);
						break;
					/// Данные без времени
					case EviewCmdDataType.GPS:
						// Ищем последний
						munit = results.LastOrDefault();
						// Разбираем и заполняем информацию GPS
						if (null != munit)
							ParseCmdTypeDataGps(munit, cmd.CmdVal);
						break;
					case EviewCmdDataType.AlarmCode:
					case EviewCmdDataType.HistoricalCompleted:
					case EviewCmdDataType.Historical:
					case EviewCmdDataType.SingleLocating:
					case EviewCmdDataType.ContinueLocating:
					case EviewCmdDataType.GSM:
					case EviewCmdDataType.WIFI:
					case EviewCmdDataType.BLE:
					case EviewCmdDataType.BLE2:
					case EviewCmdDataType.Smart:
						break;
					/// Данные со временем
					case EviewCmdDataType.CallRecords:
					case EviewCmdDataType.STEP:
					case EviewCmdDataType.Active:
					case EviewCmdDataType.HeartRate:
						break;
					default:
						break;
				}
			}
			for (int i = results.Count - 1; i >= 0; i--)
			{
				var result = results[i];
				// Если есть время, то при наличии добавляем идентификатор, если нет, удаляем запись
				if (!string.IsNullOrWhiteSpace(devId) && 0 < result.Time)
				{
					result.DeviceID = devId;
					// Проверяем и устанавливаем флаг корректности параметров движения
					result.CorrectGPS =
						result.ValidPosition &&
						result.Satellites >= MobilUnit.MinSatellites;
				}
				else
					results.RemoveAt(i);
			}
			//results.ForEach(mu => (string.Empty
			//	+ $"{mu.Time.ToUtcDateTime().ToString("\t\tyyMMdd-HHmmss")}"
			//	+ $" {(mu.SensorValues?.ContainsKey((int)EviewSensor.BatteryLevel)   ?? false ? mu.SensorValues[(int)EviewSensor.BatteryLevel]   : -1)}"
			//	+ $" {(mu.SensorValues?.ContainsKey((int)EviewSensor.GsmSignalLevel) ?? false ? mu.SensorValues[(int)EviewSensor.GsmSignalLevel] : -1)}"
			//	+ $" {mu.Latitude}"
			//	+ $" {mu.Longitude}")
			//		.TraceInformation());
			return results
				.OrderBy(mu => mu.Time)
				.ToArray();
		}
		private uint ParseCmdTypeDataStatusGetTimestamp(byte[] cmdVal)
		{
			// 4 bytes. The UTC timestamp is the total number of seconds since January 01, 1970, from 00:00 to 00 seconds.
			return cmdVal.FromLE2UInt32(0, 4);
		}
		private void ParseCmdTypeDataStatus(MobilUnit mu, byte[] cmdVal)
		{
			// 4 bytes. The UTC timestamp is the total number of seconds since January 01, 1970, from 00:00 to 00 seconds.
			var tim = ParseCmdTypeDataStatusGetTimestamp(cmdVal);
			// 4 bytes. Status of device.
			var sts = cmdVal.Skip(4).Take(4).ToArray();
			mu.Time = (int)tim;
			mu.SetSensorValue((int)EviewSensor.BatteryLevel,   (sts[3] & 0xFF));
			mu.SetSensorValue((int)EviewSensor.GsmSignalLevel, (sts[2] & 0xF8) >> 3);
		}
		private void ParseCmdTypeDataGps(MobilUnit mu, byte[] cmdVal)
		{
			if (null == mu)
				return;
			if (0 == (cmdVal?.Length ?? 0))
				return;
			var lat = (int)cmdVal.FromLE2UInt32(00, 4); // 4 bytes. Unit: ten millionth of a degree, Signed number
			var lng = (int)cmdVal.FromLE2UInt32(04, 4); // 4 bytes. Unit: ten millionth of a degree, Signed number
			var spd = (int)cmdVal.FromLE2UInt16(08, 2); // 2 bytes. Unit: km/h
			var dir = (int)cmdVal.FromLE2UInt16(10, 2); // 2 bytes. Indicates the driving direction. The unit is degree. When the value is 0, the direction is north.The value ranges from 0 to 359.
			var hgt = (int)cmdVal.FromLE2UInt16(12, 2); // 2 bytes. Unit: m, Signed number
			var acc = (int)cmdVal.FromLE2UInt16(14, 2); // 2 bytes. Unit: 1/10. The smaller the value is, the more the accuracy is. The value ranges from 0.5 to 99.9. When the accuracy value is 0, the signal is invalid.
			var run = (int)cmdVal.FromLE2UInt32(16, 4); // 4 bytes. Unit: m, Only GPS positioning need calculate mileage, and the value can be setting.
			var sat = (int)cmdVal.FromLE2UInt16(20, 1); // 1 bytes. Indicates the number of received GPS satellites
			mu.Latitude   = (double)lat / 10000000;
			mu.Longitude  = (double)lng / 10000000;
			mu.Speed      = spd;
			mu.Course     = dir;
			mu.Height     = hgt * 10;
			mu.Radius     = acc;
			mu.Satellites = sat;
			mu.Run        = run;
			mu.Type       = PositionType.GPS;
		}
		#endregion ParseCmdTypeData
	}
}