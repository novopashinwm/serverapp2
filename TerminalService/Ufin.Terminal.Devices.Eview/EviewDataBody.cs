﻿using System.Collections.Generic;
using FORIS.TSS.Common;

namespace Compass.Ufin.Terminal.Devices.Eview
{
	public class EviewDataBody
	{
		public           bool      IsParsed { get; } = false;
		public           byte      CmdType  { get; }
		public           Cmd[]     Cmds     => cmds.ToArray();
		private readonly List<Cmd> cmds     = new List<Cmd>();
		public EviewDataBody(byte[] data)
		{
			var dataReader = new DataReader(data);
			if (1 > dataReader.RemainingBytesCount)
				return;
			CmdType = dataReader.ReadByte();
			while (0 < dataReader.RemainingBytesCount)
			{
				var cmd = new Cmd(dataReader);
				if (!cmd.IsParsed)
					break;
				cmds.Add(cmd);
			}
			IsParsed = true;
		}
		public class Cmd
		{
			public byte   CmdLen   { get; }
			public byte   CmdKey   { get; }
			public byte[] CmdVal   { get; }
			public bool   IsParsed { get; } = false;
			public Cmd(DataReader dataReader)
			{
				if (null == dataReader)
					return;
				if (2 > dataReader.RemainingBytesCount)
					return;
				CmdLen = dataReader.ReadByte();
				if (CmdLen > dataReader.RemainingBytesCount)
					return;
				CmdKey = dataReader.ReadByte();
				CmdVal = dataReader.ReadBytes(CmdLen - 1 /*sizeof(CmdKey)*/);
				IsParsed = true;
			}
		}
	}
}