﻿namespace Compass.Ufin.Terminal.Devices.Eview
{
	public enum EviewSensor
	{
		BatteryLevel   = 01,
		GsmSignalLevel = 02,
	}
}