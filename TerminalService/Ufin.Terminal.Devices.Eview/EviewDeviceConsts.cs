﻿namespace Compass.Ufin.Terminal.Devices.Eview
{
	public enum EviewCmdType : byte
	{
		Data                = 0x01, // Data Command
		Config              = 0x02, // Configuration
		Service             = 0x03, // Services
		System              = 0x04, // System Control
		Update              = 0x7E, // Firmware Update
		Response            = 0x7F, // Negative Response
		FactoryTest         = 0xFE, // Factory Test
	}
	public enum EviewCmdDataType : byte
	{
		IMEI                = 0x01, // IMEI
		AlarmCode           = 0x02, // Alarm Code
		HistoricalCompleted = 0x10, // Historical data completed
		Historical          = 0x11, // Historical data
		SingleLocating      = 0x12, // Single locating
		ContinueLocating    = 0x13, // Continue locating
		GPS                 = 0x20, // GPS location
		GSM                 = 0x21, // GSM towers
		WIFI                = 0x22, // Wifi towers
		BLE                 = 0x23, // BLE
		Status              = 0x24, // General Status data
		CallRecords         = 0x25, // Call Records
		BLE2                = 0x26, // BLE
		Smart               = 0x27, // Smart Location
		STEP                = 0x30, // G-sensor data(Pedometer)
		Active              = 0x31, // Active
		HeartRate           = 0x40, // HeartRate
	}
	public enum EviewCmdConfigType : byte
	{
		Module              = 0x01, // Module Number
		Version             = 0x02, // Firmware Version
		IMEI                = 0x03, // IMEI
		ICCID               = 0x04, // ICCID
		MAC                 = 0x05, // MAC
		DATATIME            = 0x06, // Setting Time
		RUNTIME             = 0x07, // Run Times
		Firmware            = 0x08, // Firmware Information
	}
	public enum EviewCmdServicesType : byte
	{
		IMEI            = 0x01, // IMEI
		HeartBeat       = 0x10, // Heart beat
		GetAddresses    = 0x11, // Get Addresses
		GetTimestamp    = 0x12, // Get Timestamp
		GetLocationGSM  = 0x21, // Get GSM Location
		GetLocationWIFI = 0x22, // Get WIFI Location
	}
	public enum EviewCmdResponseType : byte
	{
		Success         = 0x00, // Success
		VersionError    = 0x11, // Version Invalid
		EncryptError    = 0x12, // Encryption Invalid
		LengthError     = 0x13, // Length error
		CRCError        = 0x14, // Check CRC error
		CommandError    = 0x15, // Command Invalid
		KeyError        = 0x16, // Key Invalid
		KeyLengthError  = 0x17, // Key length error
		DataFormatError = 0x21, // Data Format Invalid
		DataSizeError   = 0x22, // Data Size Error
		StateError      = 0x23, // Invalid State
		ParameterError  = 0x24, // Invalid Parameter
		NoMemoryError   = 0x25, // No Memory
		funNoSuported   = 0x26, // function not supported
		GPSNoLocation   = 0x27, // GPS not Location
		AddressError    = 0x28, // Address resolution Error
		ServiceFeeError = 0x30, // Service default fee
		LowBattery      = 0xF0, // Battery Power Low
	}
	public class EviewDeviceConsts
	{
		// Head
		public const int Cmd_Head                     = 0xAB; // Command Head
		// Type см. EviewCmdType
		//public const int Cmd_Type_Data                = 0x01; // Data Command
		//public const int Cmd_Type_Config              = 0x02; // Configuration
		//public const int Cmd_Type_Service             = 0x03; // Services
		//public const int Cmd_Type_System              = 0x04; // System Control
		//public const int Cmd_Type_Update              = 0x7E; // Firmware Update
		//public const int Cmd_Type_Response            = 0x7F; // Negative Response
		// Data см. EviewCmdDataType
		//public const int Cmd_Data_IMEI                = 0x01; // IMEI
		//public const int Cmd_Data_AlarmCode           = 0x02; // Alarm Code
		//public const int Cmd_Data_Historical          = 0x11; // Historical data
		//public const int Cmd_Data_SingleLocating      = 0x12; // Single locating
		//public const int Cmd_Data_ContinueLocating    = 0x13; // Continue locating
		//public const int Cmd_Data_GPS                 = 0x20; // GPS location
		//public const int Cmd_Data_GSM                 = 0x21; // GSM towers
		//public const int Cmd_Data_WIFI                = 0x22; // Wifi towers
		//public const int Cmd_Data_BLE                 = 0x23; // BLE
		//public const int Cmd_Data_Status              = 0x24; // General Status data
		//public const int Cmd_Data_CallRecords         = 0x25; // Call Records
		//public const int Cmd_Data_BLE2                = 0x26; // BLE
		//public const int Cmd_Data_Smart               = 0x27; // Smart Location
		//public const int Cmd_Data_STEP                = 0x30; // G-sensor data(Pedometer)
		//public const int Cmd_Data_Active              = 0x31; // Active
		//public const int Cmd_Data_HeartRate           = 0x40; // HeartRate
		// Config см. EviewCmdConfigType
		//public const int Cmd_Config_Module            = 0x01; // Module Number
		//public const int Cmd_Config_Version           = 0x02; // Firmware Version
		//public const int Cmd_Config_IMEI              = 0x03; // IMEI
		//public const int Cmd_Config_ICCID             = 0x04; // ICCID
		//public const int Cmd_Config_MAC               = 0x05; // MAC
		//public const int Cmd_Config_DATATIME          = 0x06; // Setting Time
		//public const int Cmd_Config_RUNTIME           = 0x07; // Run Times
		//public const int Cmd_Config_Firmware          = 0x08; // Firmware Information
		// System Setting
		public const int Cmd_Config_Mileage           = 0x09; // Initialize Mileage
		public const int Cmd_Config_WorkMode          = 0x0A; // Work Mode
		public const int Cmd_Config_AlarmClock        = 0x0B; // Alarm Clock
		public const int Cmd_Config_NoDisturb         = 0x0C; // No Disturb
		public const int Cmd_Config_Password          = 0x0D; // Password Protect
		public const int Cmd_Config_TimeZone          = 0x0E; // Time Zone
		public const int Cmd_Config_EnableControl     = 0x0F; // Enable control
		public const int Cmd_Config_RingtoneVolume    = 0x10; // Ring-Tone Volume
		public const int Cmd_Config_MicVolume         = 0x11; // Mic Volume
		public const int Cmd_Config_SpeakerVolume     = 0x12; // Speaker Volume
		public const int Cmd_Config_DeviceName        = 0x13; // Device Name
		public const int Cmd_Config_Battery           = 0x14; // Battery
		public const int Cmd_Config_BleLoc            = 0x15; // BLE loc
		public const int Cmd_Config_BleWhiteList      = 0x16; // BLE whitelist
		public const int Cmd_Config_Music             = 0x19; // Music
		public const int Cmd_Config_FWVersion         = 0x1a; // FW Version
		public const int Cmd_Config_GSMModule         = 0x1b; // GSM Module
		// Button Setting
		public const int Cmd_Config_SOSButton         = 0x20; // SOS Button
		public const int Cmd_Config_Call1Button       = 0x21; // Call 1 Button
		public const int Cmd_Config_Call2Button       = 0x22; // Call 2 Button
		// Phone Settings
		public const int Cmd_Config_Number            = 0x30; // Set Authorized Number
		public const int Cmd_Config_SMSOption         = 0x31; // SMS Reply Prefix Text
		public const int Cmd_Config_SOSOption         = 0x32; // SOS Option
		public const int Cmd_Config_PhoneOption       = 0x33; // Phone Switches
		// GPRS Setting
		public const int Cmd_Config_APN               = 0x40; // APN
		public const int Cmd_Config_ApnUserName       = 0x41; // Apn user name
		public const int Cmd_Config_ApnPassword       = 0x42; // Apn password
		public const int Cmd_Config_SeverIPPort       = 0x43; // Sever IP &Port
		public const int Cmd_Config_TimeInterval      = 0x44; // Time interval
		public const int Cmd_Config_ContinueLocate    = 0x45; // Continue Locate
		// Alert Settings
		public const int Cmd_Config_AlertPowerLow     = 0x50; // Power Alert
		public const int Cmd_Config_AlertGEO          = 0x51; // GEO Alert
		public const int Cmd_Config_AlertMotion       = 0x52; // Motion Alert
		public const int Cmd_Config_AlertNoMotion     = 0x53; // No-motion Alert
		public const int Cmd_Config_AlertOverSpeed    = 0x54; // Over speed Alert
		public const int Cmd_Config_AlertTilt         = 0x55; // Tilt Alert
		public const int Cmd_Config_AlertFallDown     = 0x56; // Fall Down Alert
		public const int Cmd_Config_Read              = 0xF0; // Read
		// Services
		//public const int Cmd_Services_IMEI            = 0x01; // IMEI
		//public const int Cmd_Services_HeartBeat       = 0x10; // Heart beat
		//public const int Cmd_Services_getAddresses    = 0x11; // Get Addresses
		//public const int Cmd_Services_getTimestamp    = 0x12; // Get Timestamp
		//public const int Cmd_Services_getLocationGSM  = 0x21; // Get GSM Location
		//public const int Cmd_Services_getLocationWIFI = 0x22; // Get WIFI Location
		// Firmware Update
		public const int Cmd_Update_InitialData       = 0x10; // Initial Data
		public const int Cmd_Update_FirmwareData      = 0x11; // Firmware Data
		public const int Cmd_Update_Validate          = 0x12; // Validate
		public const int Cmd_Update_State             = 0x13; // Update State
		public const int Cmd_Update_PackSize          = 0x15; // Update Pack Max len
		// Response Data
		//public const int Cmd_Response_Success         = 0x00; // Success
		//public const int Cmd_Response_VersionError    = 0x11; // Version Invalid
		//public const int Cmd_Response_EncryptError    = 0x12; // Encryption Invalid
		//public const int Cmd_Response_LengthError     = 0x13; // Length error
		//public const int Cmd_Response_CRCError        = 0x14; // Check CRC error
		//public const int Cmd_Response_CommandError    = 0x15; // Command Invalid
		//public const int Cmd_Response_KeyError        = 0x16; // Key Invalid
		//public const int Cmd_Response_KeyLengthError  = 0x17; // Key length error
		//public const int Cmd_Response_DataFormatError = 0x21; // Data Format Invalid
		//public const int Cmd_Response_DataSizeError   = 0x22; // Data Size Error
		//public const int Cmd_Response_StateError      = 0x23; // Invalid State
		//public const int Cmd_Response_ParameterError  = 0x24; // Invalid Parameter
		//public const int Cmd_Response_NoMemoryError   = 0x25; // No Memory
		//public const int Cmd_Response_funNoSuported   = 0x26; // functiont not suported
		//public const int Cmd_Response_GPSNoLocation   = 0x27; // GPS not Location
		//public const int Cmd_Response_AddressError    = 0x28; // Address resolution Error
		//public const int Cmd_Response_ServiceFeeError = 0x30; // Service default fee
		//public const int Cmd_Response_LowBattery      = 0xF0; // Battery Power Low
	}
}