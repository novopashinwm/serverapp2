﻿using FORIS.TSS.Common;

namespace Compass.Ufin.Terminal.Devices.Eview
{
	public class EviewDataHead
	{
		/// <summary> Длина заголовка в байтах </summary>
		public const int      HeadSize            = 8;
		/// <summary> Признак, что заголовок пакета был разобран </summary>
		public bool           IsParsed   { get; } = false;
		/// <summary> Заголовок заголовка пакета </summary>
		public uint           Header     { get; }
		/// <summary> Свойства пакета </summary>
		public HeadProperties Properties { get; }
		/// <summary> Длина данных пакета без заголовка </summary>
		public uint           Length     { get; }
		/// <summary> Контрольная сумма данных пакета без заголовка </summary>
		public uint           CheckCrc   { get; }
		/// <summary> Последовательный номер пакета </summary>
		public uint           SequenceId { get; }
		public EviewDataHead(byte[] data)
		{
			var dataReader = new DataReader(data);
			if (HeadSize > dataReader.RemainingBytesCount)
				return;
			Header     = dataReader.ReadByte();
			if (EviewDeviceConsts.Cmd_Head != Header)
				return;
			Properties = new HeadProperties(dataReader.ReadByte());
			Length     = dataReader.ReadLittleEndian32(2);
			CheckCrc   = dataReader.ReadLittleEndian32(2);
			SequenceId = dataReader.ReadLittleEndian32(2);
			IsParsed   = true;
		}
		/// <summary> Перечисление, типов шифрования тела пакета (исходно 2 бита, т.е. 4 значения) </summary>
		/// <remarks>00 is not encrypted, 01 means that the message body is encrypted by RSA, and the other is reserved</remarks>
		public enum EncriptionType
		{
			NotEncrypted = 0,
			Rsa          = 1,
			Reserved2    = 2,
			Reserved3    = 3,
		}
		public class HeadProperties
		{
			/// <summary> Версия протокола, по умолчанию 0 </summary>
			public int            Version    { get; }
			/// <summary> Ответ требуется? </summary>
			public bool           Flag_ACK   { get; }
			/// <summary> Ошибка есть? </summary>
			public bool           Flag_ERR   { get; }
			/// <summary> Тип шифрования тела пакета </summary>
			public EncriptionType Encryption { get; }

			public HeadProperties(byte properties)
			{
				Version    =                  (properties & 0x0F);
				Flag_ACK   =                  (properties & 0x10) > 0;
				Flag_ERR   =                  (properties & 0x20) > 0;
				Encryption = (EncriptionType)((properties & 0xC0) >> 6);
			}
		}
	}
}