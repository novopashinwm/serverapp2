﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace Compass.Ufin.Terminal.Devices.Eview
{
	public partial class EviewDevice
	{
		#region Commands
		private IEnumerable<Tuple<string, CmdType, string>> GetCommandMap(IStdCommand command)
		{
			var apnCfg = GetInternetApnConfig(command);
			var srvAdd = Manager.GetConstant(Constant.ServerIP);
			var srvPrt = Manager.GetConstant(Constant.ServerPort);
			var devTyp = command.Target.DeviceType;
			switch (devTyp)
			{
				case DeviceNames.EviewEV_07B:
				case DeviceNames.EviewEV_201:
					///////////////////////////////////////////////////////
					yield return new Tuple<string, CmdType, string>(devTyp, CmdType.Setup,          $@"s1,{apnCfg.Name},{apnCfg.User},{apnCfg.Pass}");
					yield return new Tuple<string, CmdType, string>(devTyp, CmdType.Setup,          $@"ti60s"                                       );
					yield return new Tuple<string, CmdType, string>(devTyp, CmdType.Setup,          $@"tz+00"                                       );
					yield return new Tuple<string, CmdType, string>(devTyp, CmdType.Setup,          $@"ps0"                                         );
					yield return new Tuple<string, CmdType, string>(devTyp, CmdType.Setup,          $@"ds0"                                         );
					yield return new Tuple<string, CmdType, string>(devTyp, CmdType.Setup,          $@"s2"                                          );
					yield return new Tuple<string, CmdType, string>(devTyp, CmdType.Setup,          $@"ip1,{srvAdd},{srvPrt}"                       );
					///////////////////////////////////////////////////////
					yield return new Tuple<string, CmdType, string>(devTyp, CmdType.SetInterval,    $@"ti60s"                                       );
					yield return new Tuple<string, CmdType, string>(devTyp, CmdType.Status,         $@"status"                                      );
					yield return new Tuple<string, CmdType, string>(devTyp, CmdType.SetSos,         $@"a1,+79991112233"                             );
					yield return new Tuple<string, CmdType, string>(devTyp, CmdType.DeleteSOS,      $@"a0"                                          );
					yield return new Tuple<string, CmdType, string>(devTyp, CmdType.AskPosition,    $@"loc"                                         );
					yield return new Tuple<string, CmdType, string>(devTyp, CmdType.SetModeOnline,  $@"ps0"                                         );
					yield return new Tuple<string, CmdType, string>(devTyp, CmdType.SetModeWaiting, $@"ps1"                                         );
					///////////////////////////////////////////////////////
					break;
				default:
					break;
			}
		}
		public override List<CommandStep> GetCommandSteps(IStdCommand command)
		{
			return GetCommandMap(command)
				.Where(m =>
					m.Item1 == command.Target.DeviceType &&
					m.Item2 == command.Type              &&
					!string.IsNullOrWhiteSpace(m.Item3))
				.Select(m => new SmsCommandStep(m.Item3) { WaitAnswer = null })
				.ToList<CommandStep>();
			;
		}
		public override IList<object> ParseSms(IUnitInfo ui, string text)
		{
			// TODO: Реализовать, когда будет понятно реализовано ожидание команд
			return base.ParseSms(ui, text);
		}
		#endregion Commands
	}
}