﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Net;
using System.Text;
using FORIS.TSS.Common;
using FORIS.TSS.Communication;

namespace FORIS.TSS.Terminal.SGGB2
{
	public abstract class SGGB2Datagram : Datagram
	{
		protected static TraceSwitch tsSGGB =
			new TraceSwitch("SGGB2", "SGGB2 datagram related info");

		#region .ctor

		public SGGB2Datagram()
		{
		}

		public SGGB2Datagram(uint capacity)
			: base(capacity)
		{
		}

		#endregion // .ctor

		#region CRC

		/// <summary>
		/// CRC_ID – контрольная сумма полей ID отправителя, ID пакета и ДП
		/// </summary>
		public int CRC_ID
		{
			get
			{
				return IntFromBytesBE(bytes[14], bytes[15]);
			}
			set
			{
				IntToBytesBE(value, bytes, 14, 2);
			}
		}

		/// <summary>
		/// CRC_ID – контрольная сумма полей ID отправителя, ID пакета и ДП.
		/// </summary>
		/// <returns>контрольная сумма 1</returns>
		protected int CalcCRC_ID()
		{
			int crc = 0;
			for (int i = 1; i < 14; i++) crc += bytes[i];

			return crc;
		}

		/// <summary>
		/// build and set CRC_ID
		/// </summary>
		/// <returns>crc</returns>
		protected int BuildCRC_ID()
		{
			return CRC_ID = CalcCRC_ID();
		}

		/// <summary>
		/// CRC in datagram
		/// </summary>
		protected override int CRC
		{
			get
			{
				return IntFromBytesBE(bytes[Size - 2], bytes[Size - 1]);
			}
			set
			{
				IntToBytesBE(value, bytes, Size - 2, 2);
			}
		}

		/// <summary>
		/// CRC – контрольная сумма вычисляется сложением всех полей за исключением: ИНП и CRC
		/// </summary>
		/// <returns>контрольная сумма 2</returns>
		protected override int CalcCRC()
		{
			int crc = 0;
			for (int i = 1, cnt = Size - 2; i < cnt; ++i) crc += bytes[i];
			//Нам нужны только 2 младших байта...
			crc &= 0xffff;
			return crc;
		}

		public override bool CheckCRC()
		{
			return CRC_ID == CalcCRC_ID() && base.CheckCRC();
		}

		#endregion CRC

		/// <summary>
		/// unit no. applied by user. 2 bytes
		/// </summary>
		public int UnitNo
		{
			get
			{
				return IntFromBytesBE(bytes[7], bytes[8]);
			}
		}

		/// <summary>
		/// id of packet. 2 bytes
		/// </summary>
		public int PacketID
		{
			get
			{
				return IntFromBytesBE(bytes[9], bytes[10]);
			}
		}

		protected void SetPacketID(int id)
		{
			//if(id < 0 || id > 0xffff) throw new ArgumentOutOfRangeException(
			//    "id", id, "PacketID must be [0..0xffff]");
			IntToBytesBE(id, bytes, 9, 2);
		}

		/// <summary>
		/// start position of data block in datagram
		/// </summary>
		protected int DataPos
		{
			get
			{
				return 16;
			}
		}

		/// <summary>
		/// data block size in datagram
		/// </summary>
		public int DataSize
		{
			get
			{
				return IntFromBytesBE(bytes[12], bytes[13]);
			}
		}

		/// <summary>
		/// Информационное поле - список объектов SGGBSubData
		/// </summary>
		protected ArrayList subs = null;

		/// <summary>
		/// Информационное поле данных, список объектов SGGBSubData
		/// </summary>
		public ArrayList DataField
		{
			get { return subs; }
		}
	}

	/// <summary>
	/// Блок данных датаграммы для обмена данными с мобильным устройством SGGB
	/// </summary>
	/// <remarks>Структура блока данных
	/// <p>
	/// bytes[0]			- identifier of datagram (SGGB2DataType)
	/// bytes[1..2]			- len - 3
	/// bytes[3..(len - 1)] - type specific
	/// </p>
	/// </remarks>
	public abstract class SGGB2SubData : IComparable
	{
		protected static TraceSwitch tsSGGB2 =
			new TraceSwitch("SGGB2", "SGGB sub data related info");

		/// <summary>
		/// массив данных
		/// </summary>
		protected byte[] bytes;
		public byte[] Bytes
		{
			get
			{
				return bytes;
			}
		}

		/// <summary>
		/// начало блока данных
		/// </summary>
		protected int begin;

		/// <summary>
		/// sizeof(ID) + sizeof(Size)
		/// </summary>
		protected const int hdrsize = 3;

		/// <summary>
		/// pos in byte sequence after the header
		/// </summary>
		protected int DataPos
		{
			get
			{
				return begin + hdrsize;
			}
		}

		/// <summary>
		/// ID of sub data block. use abstract prop for not forget to change val
		/// </summary>
		/// <remarks>each sub data type has unique ID.
		/// commonly it's theoretical id and may differ from real byte values.
		/// for real val use GetID</remarks>
		public abstract int ID { get;}

		/// <summary>
		/// sub data size
		/// </summary>
		/// <remarks>full sub data size.
		/// commonly it's theoretical size and may differ from real byte values.
		/// for real size use GetSize</remarks>
		public int Size
		{
			get
			{
				return hdrsize + DataSize;
			}
		}

		/// <summary>
		/// data size of block. size without header. 
		/// use abstract prop for not forget to change val
		/// </summary>
		protected abstract int DataSize { get;}

		/// <summary>
		/// very useful
		/// </summary>
		protected Encoding encoding = Encoding.GetEncoding(1251);

		/// <summary>
		/// get ID of sub data in byte sequence
		/// </summary>
		/// <param name="data">bytes where to seek sub data ID</param>
		/// <param name="begin">begin pos of sub data in byte sequence</param>
		/// <returns>ID, 0 - bad</returns>
		public static int GetID(byte[] data, int begin)
		{
			return data[begin];
		}

		public static void SetID(byte[] data, int begin, SGGB2DataType id)
		{
			data[begin] = (byte)id;
		}

		/// <summary>
		/// get size of sub data in byte sequence
		/// </summary>
		/// <param name="data">bytes where to seek sub data size</param>
		/// <param name="begin">begin pos of sub data in byte sequence</param>
		/// <returns>size</returns>
		public static int GetSize(byte[] data, int begin)
		{
			try
			{
				return Datagram.IntFromBytesBE(data[begin + 1], data[begin + 2]) + hdrsize;
			}
			catch
			{
				return 0;
			}
		}

		public static void SetSize(byte[] data, int begin, int size)
		{
			if (size < 0 || size > 0xffff) throw new 
				ArgumentOutOfRangeException("SGGB sub data size must" + " be [0..0xffff]");

			Datagram.IntToBytesBE(size - hdrsize, data, begin + 1, 2);
		}

		/// <summary>
		/// check packet header (ID + Size)
		/// </summary>
		/// <returns></returns>
		protected virtual bool CheckHdr()
		{
			return ID == GetID(bytes, begin) && Size == GetSize(bytes, begin);
		}

		#region .ctor
		/// <summary>
		/// for data from controller
		/// </summary>
		public SGGB2SubData(byte[] bytes, int begin)
		{
			this.bytes = bytes;
			this.begin = begin;
		}

		public SGGB2SubData(bool init)
		{
			if (init) Init();
		}

		#endregion // .ctor

		protected void Init()
		{
			bytes = new byte[Size];
			SetID(bytes, begin, (SGGB2DataType)ID);
			SetSize(bytes, begin, Size);
		}

		#region IComparable Members

		public virtual int CompareTo(object obj)
		{
			if (obj is int) return ID.CompareTo(obj);

			SGGB2SubData sub = obj as SGGB2SubData;
			if (sub == null) throw new ArgumentException(
				 "Incorrect type of object - " + obj.GetType(), "obj");

			return ID.CompareTo(sub.ID);
		}

		#endregion

		public virtual bool Valid
		{
			get
			{
				return CheckHdr();
			}
		}

		public static SGGB2SubData Create(byte[] data, int posBegin)
		{
			switch ((SGGB2DataType)GetID(data, posBegin))
			{
				case SGGB2DataType.GPS:
					return null;// new SGGB2SubDataGPS(data, posBegin);

				case SGGB2DataType.Settings:
					return new SGGB2SubDataSettings(data, posBegin);

				case SGGB2DataType.Log:
					return null;//new SGGB2SubDataLog(data, posBegin);

				case SGGB2DataType.Text:
					return new SGGB2SubDataText(data, posBegin);

				case SGGB2DataType.Alarm:
					return null;

				case SGGB2DataType.Confirm:
					return new SGGB2SubDataConfirm(data, posBegin);
				case SGGB2DataType.Sggb2Confirm:
					{
						switch (data[posBegin + 5])
						{
							case (byte)SGGB2DataType.GetLog:
								return new SGGB2SubDataGetLogReply(data, posBegin);
							default:
								break;
						}
						break;
					}
				default:
					if (!tsSGGB2.TraceWarning)
						return null;

					Trace.WriteLineIf(tsSGGB2.TraceWarning, "UNEXPECTED SUB DATA TYPE - " + (SGGB2DataType)GetID(data, posBegin), "SGGB");
					int subN = GetSize(data, posBegin);
					byte[] usd = new byte[subN];
					Array.Copy(data, posBegin, usd, 0, subN);
					Trace.WriteLineIf(tsSGGB2.TraceWarning, "\t" + BitConverter.ToString(usd));
					return null;
			}
			return null;
		}

		
		public override string ToString()
		{
			return bytes == null ? base.ToString() + " empty" :
				BitConverter.ToString(bytes, begin, GetSize(bytes, begin));
		}
	}

	/// <summary>
	/// SGGB2SubDataSetFirmware - отсылка перепрошивки модулю
	/// </summary>
	/// <remarks>
	/// bytes[0]		- ID = SGGBDataType.SetFirmware = 0x26
	/// bytes[1..2]		- Размер пакета
	/// bytes[3..4]		- Порядковый номер пакета
	/// bytes[5..n]		- Байты прошивки
	/// </remarks>
	public class SGGB2SubDataSetFirmware : SGGB2SubData
	{
		#region .ctor
		public SGGB2SubDataSetFirmware(int SizeOfData)
			: base(true)
		{
			datasize = SizeOfData;
			Init();
		}
		#endregion
		public override int ID
		{
			get
			{
				return (int)SGGB2DataType.SetFirmware;
			}
		}

		readonly int datasize;
		protected override int DataSize
		{
			get
			{
				return datasize;
			}
		}

		/// <summary>
		/// Данные прошивки
		/// </summary>
		public byte[] FirmwareData
		{
			set
			{
				value.CopyTo(bytes, DataPos + 2);
			}
		}

		/// <summary>
		/// Номер пакета
		/// </summary>
		public int Index
		{
			set
			{
				Datagram.IntToBytesBE(value, bytes, DataPos, 2);
			}
			get
			{
				return Datagram.IntFromBytesBE(bytes, DataPos, 2);
			}
		}
	}

	/// <summary>
	/// Ответ на запрос на выдачу архива
	/// </summary>
	internal class SGGB2SubDataGetLogReply : SGGB2SubData
	{

		public SGGB2SubDataGetLogReply(byte[] bytes, int begin) : base(bytes, begin)
		{
		}

		/// <summary>
		/// ID of sub data block. use abstract prop for not forget to change val
		/// </summary>
		/// <remarks>each sub data type has unique ID.
		/// commonly it's theoretical id and may differ from real byte values.
		/// for real val use GetID</remarks>
		public override int ID
		{
			get { return GetID(bytes, begin); }
		}

		/// <summary>
		/// data size of block. size without header. 
		/// use abstract prop for not forget to change val
		/// </summary>
		protected override int DataSize
		{
			get { return 12; }
		}

		public DateTime From
		{
			get 
			{
				return (time_t)Datagram.IntFromBytesBE(bytes[DataPos + 4],
					bytes[DataPos + 5], bytes[DataPos + 6], bytes[DataPos + 7]); 
			}
		}

		public time_t To
		{
			get
			{
				return (time_t)Datagram.IntFromBytesBE(bytes[DataPos + 8],
				  bytes[DataPos + 9], bytes[DataPos + 10], bytes[DataPos + 11]);
			}
		}
	}

	internal class SGGB2SubDataConfirm : SGGB2SubData
	{
		public SGGB2SubDataConfirm(byte[] data, int begin)
			: base(data, begin)
		{
			
		}

		public SGGB2SubDataConfirm(int packetID, SGGB2DataType blockID)
			: base(false)
		{
			bytes = new byte[Size];
			SetID(bytes, begin, SGGB2DataType.Confirm);
			SetSize(bytes, begin, Size);
			PacketID = packetID;
			BlockID = blockID;
		}

		public int PacketID
		{
			get 
			{
				return Datagram.IntFromBytesBE(bytes[DataPos], bytes[DataPos + 1], bytes[DataPos + 2], bytes[DataPos + 3]); 
			}
			set 
			{

				Datagram.IntToBytesBE(value, bytes, DataPos, 4); ; 
			}
		}

		/// <summary>
		/// Идентификатор подтверждаемого блока
		/// </summary>
		public SGGB2DataType BlockID
		{
			get
			{
				try
				{
					return (SGGB2DataType) bytes[DataPos + 4];
				}
				catch(Exception ex)
				{
					Trace.WriteLineIf(tsSGGB2.TraceError, this.GetType()+" "+ ex.Message);
				}
				return SGGB2DataType.Undefined;
			}
			set
			{
				bytes[DataPos + 4] = (byte) value;
			}
		}


		public int State
		{
			get
			{
				return bytes[DataPos + 5];
			}
			set
			{
				bytes[DataPos + 5] = (byte)value;
			}
		}

		
		/// <summary>
		/// ID of sub data block. use abstract prop for not forget to change val
		/// </summary>
		/// <remarks>each sub data type has unique ID.
		/// commonly it's theoretical id and may differ from real byte values.
		/// for real val use GetID</remarks>
		public override int ID
		{
			get { return GetID(bytes, begin); }
		}

		/// <summary>
		/// data size of block. size without header. 
		/// use abstract prop for not forget to change val
		/// </summary>
		protected override int DataSize
		{
			get { return 6; }
		}
	}

	internal class SGGB2SubDataText : SGGB2SubData
	{
		public SGGB2SubDataText(byte[] data, int begin)
			: base(data, begin)
		{
		}

		public SGGB2SubDataText(string text) : base(false)
		{
			text = text.Trim();
			if (text.Length > 240) Trace.WriteLineIf(tsSGGB2.TraceWarning,
				 "Text too long. Max length - 240", "SGGB2");
			bytes = new byte[hdrsize + text.Length];
			SetID(bytes, begin, SGGB2DataType.SendText);
			SetSize(bytes, begin, bytes.Length);
			encoding.GetBytes(text, 0, text.Length, bytes, DataPos);
		}

		/// <summary>
		/// ID of sub data block. use abstract prop for not forget to change val
		/// </summary>
		/// <remarks>each sub data type has unique ID.
		/// commonly it's theoretical id and may differ from real byte values.
		/// for real val use GetID</remarks>
		public override int ID
		{
			get
			{
				return GetID(bytes, begin);
			}
		}

		/// <summary>
		/// data size of block. size without header. 
		/// use abstract prop for not forget to change val
		/// </summary>
		protected override int DataSize
		{
			get
			{
				return GetSize(bytes, begin) - hdrsize;
			}
		}

		protected override bool CheckHdr()
		{
			return ID == (int)SGGB2DataType.SendText || ID == (int)SGGB2DataType.Text;
		}

		public string Text
		{
			get
			{
				return encoding.GetString(bytes, DataPos, DataSize);
			}
		}
	}

	/// <summary>
	/// controller settings
	/// </summary>
	/// <remarks>
	/// bytes[0]		- ID = SGGB2DataType.Settings = 0x0B
	/// bytes[1..2]		- Size = 346
	/// bytes[3..6]		- dispatcher IP
	/// bytes[7..8]		- dispatcher port
	///	bytes[9..20]	- reserved
	/// byte [21]       - часовой пояс
	///	bytes[22..37]	- dispatcher voice phone, with city code
	///	bytes[38..53]	- dispatcher data phone
	///	bytes[54]		- send interval. 10-60
	///	bytes[55]		- 0 - udp, 1 - tcp. char
	///	bytes[56..87]	- точка входа в интернет
	/// bytes[88..119]  - логин (точка входа в интернет)
	/// bytes[120..151] - пароль (точка входа в интернет)
	/// bytes[152..183] - версия прошивки модуля
	/// bytes[184..199] - время компиляции прошивки модуля
	/// bytes[200..215] - дата компиляции прошивки модуля
	/// bytes[216..247] - версия прошивки загрузчика
	/// bytes[248..263] - время компиляции прошивки загрузчика
	/// bytes[264..279] - дата компиляции прошивки загрузчика
	/// bytes[280..283] - IP адрес FTP сервера
	///	bytes[284..315]	- Логин FTP
	/// bytes[316..347]	- Пароль FTP
	///	bytes[348..349]	- Интервал выдачи пакетов в режиме энергосбережения
	/// </remarks>
	internal class SGGB2SubDataSettings : SGGB2SubData
	{
		public SGGB2SubDataSettings(byte[] data, int begin) : base(data, begin)
		{
			//Debug.Fail("not properly implemented");
		}

		public override int ID
		{
			get
			{
				return (byte)SGGB2DataType.Settings;
			}
		}

		protected override int DataSize
		{
			get
			{
				return 346;
			}
		}

		/// <summary>
		/// dispatcher IP
		/// </summary>
		public IPAddress IP
		{
			get
			{
				return new IPAddress(new byte[4]{bytes[DataPos], bytes[DataPos + 1], 
					bytes[DataPos + 2], bytes[DataPos + 3]});
			}
			set
			{
				Array.Copy(value.GetAddressBytes(), 0, bytes, DataPos, 4);
			}
		}

		/// <summary>
		/// dispatcher port
		/// </summary>
		public int Port
		{
			get
			{
				return Datagram.IntFromBytesBE(bytes[DataPos + 4], bytes[DataPos + 5]);
			}
			set
			{
				Datagram.IntToBytesBE(value, bytes, DataPos + 4, 2);
			}
		}

		/// <summary>
		/// Часовой пояс
		/// </summary>
		public int TimeZone
		{
			get
			{
				return (int)bytes[DataPos + 18];
			}
		}

		/// <summary>
		/// Voice phone
		/// </summary>
		public string VoicePhone
		{
			get
			{
				return encoding.GetString(bytes, DataPos + 19, 16).TrimEnd();
			}
			set
			{
				if(value == null) value = "";
				if(value.Length > 16) 
					throw new ArgumentException("Voice phone number too long");

				encoding.GetBytes(value, 0, value.Length, bytes, DataPos + 19);
			}
		}

		/// <summary>
		/// Data phone
		/// </summary>
		public string DataPhone
		{
			get
			{
				return encoding.GetString(bytes, DataPos + 35, 16).TrimEnd();
			}
			set
			{
				if(value == null) value = "";
				if(value.Length > 16) 
					throw new ArgumentException("Data phone number too long");

				encoding.GetBytes(value, 0, value.Length, bytes, DataPos + 35);
			}
		}

		/// <summary>
		/// data send interval
		/// </summary>
		public int Interval
		{
			get
			{
				return bytes[DataPos + 51];
			}
			set
			{
				bytes[DataPos + 51] = (byte)value;
			}
		}

		/// <summary>
		/// true - TCP, false - UDP
		/// </summary>
		public bool TCP
		{
			get
			{
				return bytes[DataPos + 52] != '0';
			}
			set
			{
				bytes[DataPos + 52] = (byte)(value? '1': '0');
			}
		}

		/// <summary>
		/// Точка входа в интернет
		/// </summary>
		public string InetEntryPoint
		{
			get
			{
				string result = encoding.GetString(bytes, DataPos + 53, 32);

				int n = result.IndexOf("\0".ToCharArray()[0]);

				if (n >= 0)
					result = result.Remove(n);

				return result;
			}
		}

		/// <summary>
		/// Логин, для доступа в интернет
		/// </summary>
		public string Login
		{
			get
			{
				string result = encoding.GetString(bytes, DataPos + 85, 32);

				int n = result.IndexOf("\0".ToCharArray()[0]);

				if (n >= 0)
					result = result.Remove(n);

				return result;
			}
		}

		/// <summary>
		/// Пароль, для доступа в интернет
		/// </summary>
		public string Password
		{
			get
			{
				string result = encoding.GetString(bytes, DataPos + 117, 32);

				int n = result.IndexOf("\0".ToCharArray()[0]);

				if (n >= 0)
					result = result.Remove(n);

				return result;
			}
		}

		/// <summary>
		/// module (unit) firmware
		/// </summary>
		public string ModuleFW
		{
			get
			{
				return encoding.GetString(bytes, DataPos + 149, 32);
			}
		}

		/// <summary>
		/// IP адрес FTP сервера
		/// </summary>
		public IPAddress FTPIP
		{
			get
			{
				return new IPAddress(new byte[4]{bytes[DataPos+277], bytes[DataPos + 278], 
					bytes[DataPos + 279], bytes[DataPos + 280]});
			}
		 }

		/// <summary>
		/// Логин FTP
		/// </summary>
		public string LoginFTP
		{
			get
			{
				string result = encoding.GetString(bytes, DataPos + 281, 32);

				int n = result.IndexOf("\0".ToCharArray()[0]);

				if (n >= 0)
					result = result.Remove(n);

				return result;
			}
		}

		/// <summary>
		/// Пароль FTP
		/// </summary>
		public string PasswordFTP
		{
			get
			{
				string result = encoding.GetString(bytes, DataPos + 313, 32);

				int n = result.IndexOf("\0".ToCharArray()[0]);

				if (n >= 0)
					result = result.Remove(n);

				return result;
			}
		}

		/// <summary>
		/// Интервал выдачи пакетов в сек. в режиме энергосбережения
		/// </summary>
		public int IntervalSleep
		{
			get
			{
				return Datagram.IntFromBytesBE(bytes[DataPos + 345], bytes[DataPos + 346]);
			}
		}
	}

	/// <summary>
	/// Датаграмма для обмена данными с мобильным устройством SGGB. 
	/// Данные передаются на устройство.
	/// </summary>
	public class SGGB2InputDatagram : SGGB2Datagram
	{
		#region .ctor

		/// <summary>
		/// Датаграмма для обмена данными с мобильным устройством SGGB. Данные передаются на устройство.
		/// </summary>
		public SGGB2InputDatagram(uint capacity)
			: base(capacity)
		{
			// ID
			bytes[0] = (byte)SGGB2DataType.SGGB;
		}

		#endregion // .ctor

		/// <summary>
		/// create input datagram from sub data list
		/// </summary>
		/// <param name="packetID">list of sub data</param>
		/// <param name="subs">list of sub data</param>
		/// <returns>datagram</returns>
		public static SGGB2InputDatagram Create(int packetID, params SGGB2SubData[] subs)
		{
			// add all subs' bytes to al
			ArrayList al = new ArrayList(256);
			foreach (SGGB2SubData sub in subs)
			{
				if (!sub.Valid)
					throw new DatagramException("Sub data not properly initialized");
				al.AddRange(sub.Bytes);
			}

			// create dg with empty bytes
			SGGB2InputDatagram dg = new SGGB2InputDatagram(18 + (uint)al.Count);
			dg.SetPacketID(packetID);
			//параметры протокола
			dg.bytes[11] = 0x00;
			// data length
			IntToBytesBE(al.Count, dg.bytes, 12, 2);
			// create CRC_ID
			dg.BuildCRC_ID();
			// copy subs' bytes to dg
			Debug.Assert(dg.GetBytes().Length == 18 + al.Count);
			al.CopyTo(dg.GetBytes(), 16);
			// create CRC
			dg.BuildCRC();

			return dg;
		}

		public static SGGB2InputDatagram Create(params SGGB2SubData[] subs)
		{
			return Create(0, subs);
		}
	}

	/// <summary>
	/// Датаграмма для обмена данными с мобильным устройством SGGB. Данные поступают от устройства.
	/// </summary>
	/// <remarks>
	/// <p>
	/// byte[0] = version of protocol = 0xAA
	/// byte[1..6] = identifier of mobile unit (6 bytes)
	/// bytes[7..8] = ID отправителя
	/// bytes[9..10] = ID пакета
	/// byte[11] = параметры протокола
	/// bytes[12..13] = N length of data N
	/// bytes[14..15] = CRC_1
	/// bytes[16..16+N] = data
	/// bytes[17+N..18+N] = CRC_2
	/// </p>
	/// </remarks>
	public class SGGB2OutputDatagram : SGGB2Datagram
	{
		#region **************************** Атрибуты *******************************

		/// <summary>
		/// identifier of datagram
		/// </summary>
		public int DataID
		{
			get
			{
				return IntFromBytesBE(bytes[9], bytes[10]);
			}
			set
			{
				IntToBytesBE(value, bytes, 9, 2);
			}
		}

		#endregion // **************************** Атрибуты *******************************

		#region .ctor
		/// <summary>
		/// Датаграмма для обмена данными с мобильным устройством SGGB. 
		/// Данные поступают от устройства.
		/// </summary>
		SGGB2OutputDatagram(byte[] data, int pos)
		{
			// find start point 0xAA
			int i = Array.IndexOf(data, (byte)SGGB2DataType.SGGB, pos);
			// not found or too short
			if (i == -1 || i + 18 > data.Length) throw new DatagramNotFoundException(
				 "Datagram not found or too short", GetType(), data, 0);

			// full size
			int size = 18 + IntFromBytesBE(data[i + 12], data[i + 13]);
			if (i + size > data.Length) throw new DatagramNotFoundException(
				 "Datagram not found or too short", GetType(), data, 0);

			// copy full datagram
			bytes = new byte[size];
			Array.Copy(data, i, bytes, 0, size);

			Debug.WriteLineIf(tsSGGB.TraceVerbose, BitConverter.ToString(bytes), "SGGB");
		}

		/// <summary>
		/// Датаграмма для обмена данными с мобильным устройством SGGB. Данные поступают от устройства.
		/// </summary>
		public SGGB2OutputDatagram(uint capacity)
			: base(capacity)
		{
		}

		#endregion // .ctor

		/// <summary>
		/// create output datagram from sub data
		/// </summary>
		/// <param name="subs">list of sub data</param>
		/// <returns>output datagram</returns>
		public static SGGB2OutputDatagram Create(params SGGB2SubData[] subs)
		{
			// add all subs' bytes to al
			ArrayList al = new ArrayList(256);
			foreach (SGGB2SubData sub in subs) al.AddRange(sub.Bytes);

			// create dg with empty bytes
			SGGB2OutputDatagram dg = new SGGB2OutputDatagram(18 + (uint)al.Count);
			dg.subs = new ArrayList(subs);
			// ID
			dg.bytes[0] = 0xAA;
			// data length
			IntToBytesBE(al.Count, dg.bytes, 11, 3);
			// create CRC_ID
			dg.BuildCRC_ID();
			// copy subs' bytes to dg
			Debug.Assert(dg.GetBytes().Length == 18 + al.Count);
			al.CopyTo(dg.GetBytes(), 16);
			// create CRC
			dg.BuildCRC();

			return dg;
		}

		public static SGGB2OutputDatagram Create(byte[] data)
		{
			int begin = 0;
			SGGB2OutputDatagram dg = Create(data, ref begin);
			if (dg != null && dg.Valid && begin != dg.Size)
				throw new DatagramException("Pos must be after datagram");
			return dg;
		}

		/// <summary>
		/// create datagram from byte sequence
		/// </summary>
		/// <param name="data">bytes</param>
		/// <param name="pos">in - pos to search datagram from, out - pos after dg, -1 - not found</param>
		/// <returns>datagram if found whole or null if not</returns>
		public static SGGB2OutputDatagram Create(byte[] data, ref int pos)
		{
			try
			{
				Debug.Assert(pos > -1 && pos < g_MaxDatagramLength, "pos out of range");

				// find start point 0xAA
				pos = Array.IndexOf(data, (byte)SGGB2DataType.SGGB, pos);
				// not found or too short
				if (pos == -1 || pos + 18 > data.Length || data[11] != 0x01) return null;

				// full size
				int size = 18 + IntFromBytesBE(data[pos + 12], data[pos + 13]);
				if (size >= g_MaxDatagramLength)	// data too big so move pos to next 0xAA
				{	// invalid datagram or wrong 0xAA
					Trace.WriteLineIf(tsSGGB.TraceVerbose, "Size in datagram too big", "SGGB");
					pos = Array.IndexOf(data, (byte)SGGB2DataType.SGGB, pos + 1);
					return null;
				}
				// too short or accidental 0xAA not the beginning 
				if (pos + size > data.Length) return null;

				SGGB2OutputDatagram dg = new SGGB2OutputDatagram(data, pos);
				if (dg.Valid) pos += dg.Size;
				else
				{
					pos = Array.IndexOf(data, (byte)SGGB2DataType.SGGB, pos + 1);
					return dg;
				}

				// заполняем блоки данных
				int n = dg.DataSize;
				int posBegin = dg.DataPos; // начало блока данных
				int posEnd = posBegin + n;

				dg.subs = new ArrayList(1);
				while (posBegin < posEnd)
				{
					// sub data real size
					int subN = SGGB2SubData.GetSize(dg.bytes, posBegin);
					if (subN < 1)
					{
						pos = Array.IndexOf(data, (byte)SGGB2DataType.SGGB, pos + 1);
						if (pos == -1) pos = data.Length;
						return dg;
					}

					try
					{
						SGGB2SubData sub = SGGB2SubData.Create(dg.bytes, posBegin);
						if (sub != null) dg.subs.Add(sub);
					}
					catch (DatagramFieldException ex)
					{
						Trace.WriteLineIf(tsSGGB.TraceError, ex.Message + "\n\t" + string.Format(
							"Field = {0}, Type = {1}, Data =\n\t{2}", ex.Field, ex.DatagramType,
							BitConverter.ToString(ex.Data)), "SGGB");
					}
					catch (Exception ex)
					{
						Trace.WriteLineIf(tsSGGB.TraceError, ex.Message, "SGGB");
					}
					finally
					{
						posBegin += subN;
					}
				}
				return dg;
			}
			catch (Exception ex)
			{
				Trace.WriteLine("SGGB2OutputDatagram Create(...) " + ex.Message +
					"\r\nStack:\r\n" + ex.StackTrace);
				return null;
			}
		}
	}

	/// <summary>
	/// request for getting settings
	/// </summary>
	/// <remarks>
	/// bytes[0]	- ID = SGGB2DataType.GetSettings = 0x0E
	/// bytes[1..2]	- Size = 0
	/// </remarks>
	public class SGGB2SubDataGetSettings : SGGB2SubData
	{
		public SGGB2SubDataGetSettings()
			: base(true)
		{
		}

		public override int ID
		{
			get
			{
				return (int)SGGB2DataType.GetSettings;
			}
		}

		protected override int DataSize
		{
			get
			{
				return 0;
			}
		}
	}

	/// <summary>
	/// Запрос на выдачу архива (диспетчер -> контролер). ID = 0x06
	/// </summary>
	public class SGGB2SubDataGetLog : SGGB2SubData
	{
		#region .ctor

		/// <summary>
		/// Запрос на выдачу архива (диспетчер -> контролер)
		/// </summary>
		public SGGB2SubDataGetLog()
			: base(true)
		{
		}

		#endregion // .ctor

		public override int ID
		{
			get
			{
				return (int)SGGB2DataType.GetLog;
			}
		}

		protected override int DataSize
		{
			get
			{
				return 9;
			}
		}

		public int QueryType
		{
			get { return bytes[DataPos]; }
			set { bytes[DataPos] = (byte) value;}
		}

		/// <summary>
		/// controller memory from time
		/// </summary>
		public DateTime From
		{
			get
			{
				return (time_t)Datagram.IntFromBytesBE(bytes[DataPos+1],
					bytes[DataPos + 2], bytes[DataPos + 3], bytes[DataPos + 4]);
			}
			set
			{
				int tm = (time_t)value;
				if (tm < 0) throw new ArgumentOutOfRangeException("value out of range");

				Datagram.IntToBytesBE(tm, bytes, DataPos+1, 4);
			}
		}

		/// <summary>
		/// controller memory to time
		/// </summary>
		public DateTime To
		{
			get
			{
				return (time_t)Datagram.IntFromBytesBE(bytes[DataPos + 5],
					bytes[DataPos + 6], bytes[DataPos + 7], bytes[DataPos + 8]);
			}
			set
			{
				int tm = (time_t)value;
				if (tm < 0) throw new ArgumentOutOfRangeException("value out of range");

				Datagram.IntToBytesBE(tm, bytes, DataPos + 5, 4);
			}
		}
	}

	/// <summary>
	/// sub data to set controller settings
	/// </summary>
	/// <remarks>
	/// bytes[0]		- ID = SGGB2DataType.SetSettings = 0x0A
	/// bytes[1..2]		- Size = 40 (???). actually 55
	/// bytes[3..6]		- dispatcher IP
	/// bytes[7..8]		- dispatcher port
	/// bytes[9..21]	- reserved
	///	bytes[22..37]	- dispatcher phone for voice, with city code
	///	bytes[38..53]	- dispatcher phone for data
	///	bytes[54]		- send interval. 10-60
	///	bytes[55]		- '0' - udp, '1' - tcp. char
	///	bytes[56..57]	- unit no.
	/// </remarks>
	public class SGGB2SubDataSetSettings : SGGB2SubData
	{
		public override int ID
		{
			get
			{
				return (int)SGGB2DataType.SetSettings;
			}
		}

		protected override int DataSize
		{
			get
			{
				return 221;
			}
		}

		/// <summary>
		/// ctor for empty object. initialized
		/// </summary>
		public SGGB2SubDataSetSettings()
			: base(true)
		{
			//Debug.Fail("not properly implemented");
		}

		/// <summary>
		/// dispatcher IP
		/// </summary>
		public IPAddress IP
		{
			get
			{
				return new IPAddress(
					new byte[4] { bytes[DataPos], bytes[DataPos + 1], bytes[DataPos + 2], bytes[DataPos + 3] });
			}
			set
			{
				Array.Copy(value.GetAddressBytes(), 0, bytes, DataPos, 4);
			}
		}

		/// <summary>
		/// dispatcher port
		/// </summary>
		public int Port
		{
			get
			{
				return Datagram.IntFromBytesBE(bytes[DataPos + 4], bytes[DataPos + 5]);
			}
			set
			{
				Datagram.IntToBytesBE(value, bytes, DataPos + 4, 2);
			}
		}

		public int TimeZone
		{
			get { return bytes[DataPos + 18]; }
			set { bytes[DataPos + 18] = (byte)value; }
		}

		/// <summary>
		/// Voice phone
		/// </summary>
		public string VoicePhone
		{
			get
			{
				return encoding.GetString(bytes, DataPos + 19, 16).TrimEnd();
			}
			set
			{
				if (value == null) value = "";
				if (value.Length > 16)
					throw new ArgumentException("Voice phone number too long");

				encoding.GetBytes(value, 0, value.Length, bytes, DataPos + 19);
			}
		}

		/// <summary>
		/// Data phone
		/// </summary>
		public string DataPhone
		{
			get
			{
				return encoding.GetString(bytes, DataPos + 35, 16).TrimEnd();
			}
			set
			{
				if (value == null) value = "";
				if (value.Length > 16)
					throw new ArgumentException("Data phone number too long");

				encoding.GetBytes(value, 0, value.Length, bytes, DataPos + 35);
			}
		}

		/// <summary>
		/// send interval
		/// </summary>
		public int Interval
		{
			get
			{
				return bytes[DataPos + 51];
			}
			set
			{
				bytes[DataPos + 51] = (byte)value;
			}
		}

		/// <summary>
		/// true - TCP, false - UDP
		/// </summary>
		public bool TCP
		{
			get
			{
				return bytes[DataPos + 52] != '0';
			}
			set
			{
				bytes[DataPos + 52] = (byte)(value ? '1' : '0');
			}
		}

		/// <summary>
		/// assignable unit no
		/// </summary>
		public int UnitNo
		{
			get
			{
				return Datagram.IntFromBytesBE(bytes[DataPos + 53], bytes[DataPos + 54]);
			}
			set
			{
				Datagram.IntToBytesBE(value, bytes, DataPos + 53, 2);
			}
		}

		/// <summary>
		/// Internet Entry Point
		/// </summary>
		public string EntryPoint
		{
			get
			{
				return encoding.GetString(bytes, DataPos + 55, 32).TrimEnd();
			}
			set
			{
				if (value == null) value = "";
				if (value.Length > 32)
					throw new ArgumentException("Entry point is too long");

				encoding.GetBytes(value, 0, value.Length, bytes, DataPos + 55);
			}
		}

		/// <summary>
		/// Internet Login
		/// </summary>
		public string Login
		{
			get
			{
				return encoding.GetString(bytes, DataPos + 87, 32).TrimEnd();
			}
			set
			{
				if (value == null) value = "";
				if (value.Length > 32)
					throw new ArgumentException("Login is too long");

				encoding.GetBytes(value, 0, value.Length, bytes, DataPos + 87);
			}
		}

		// <summary>
		/// Internet Password
		/// </summary>
		public string Password
		{
			get
			{
				return encoding.GetString(bytes, DataPos + 119, 32).TrimEnd();
			}
			set
			{
				if (value == null) value = "";
				if (value.Length > 32)
					throw new ArgumentException("Password is too long");

				encoding.GetBytes(value, 0, value.Length, bytes, DataPos + 119);
			}
		}

		public IPAddress IpFtp
		{
			get
			{
				return new IPAddress(
					new byte[4] { bytes[DataPos+151], bytes[DataPos + 152], bytes[DataPos + 153], bytes[DataPos + 154] });
			}
			set
			{
				Array.Copy(value.GetAddressBytes(), 0, bytes, DataPos+151, 4);
			}
		}

		public string LoginFtp
		{
			get
			{
				return encoding.GetString(bytes, DataPos + 155, 32).TrimEnd();
			}
			set
			{
				if (value == null) value = "";
				if (value.Length > 32)
					throw new ArgumentException("LoginFtp is too long");

				encoding.GetBytes(value, 0, value.Length, bytes, DataPos + 155);
			}
		}

		// <summary>
		/// Ftp Password
		/// </summary>
		public string PasswordFtp
		{
			get
			{
				return encoding.GetString(bytes, DataPos + 187, 32).TrimEnd();
			}
			set
			{
				if (value == null) value = "";
				if (value.Length > 32)
					throw new ArgumentException("PasswordFtp is too long");

				encoding.GetBytes(value, 0, value.Length, bytes, DataPos + 187);
			}
		}

		/// <summary>
		/// Интервал выдачи данных в режиме энергосбереженния
		/// </summary>
		public int SleepPeriod
		{
			get
			{
				return Datagram.IntFromBytesBE(bytes[DataPos + 219], bytes[DataPos + 220]);
			}
			set
			{
				Datagram.IntToBytesBE(value, bytes, DataPos + 219, 2);
			}
		}
	}

	class SGGB2SubDataControl : SGGB2SubData
	{

		public SGGB2SubDataControl()
			: base(true)
		{
		}

		public override int ID
		{
			get
			{
				return (int)SGGB2DataType.Control;
			}
		}

		protected override int DataSize
		{
			get
			{
				return 5;
			}
		}

		public BitArray Digits
		{
			get
			{
				return new BitArray(new byte[]{bytes[DataPos]});
			}
			set
			{
				value.CopyTo(bytes, DataPos);
			}
		}

		public int Channel1
		{
			get
			{
				return Datagram.IntFromBytesBE(bytes[DataPos + 1], bytes[DataPos + 2]);
			}
			set
			{
				Datagram.IntToBytesBE(value, bytes, DataPos + 1, 2);
			}
		}

		public int Channel2
		{
			get
			{
				return Datagram.IntFromBytesBE(bytes[DataPos + 3], bytes[DataPos + 4]);
			}
			set
			{
				Datagram.IntToBytesBE(value, bytes, DataPos + 3, 2);
			}
		}

	}

	internal class SGGB2SubDataRestart : SGGB2SubData
	{
		public SGGB2SubDataRestart()
			: base(true)
		{
		}

		public override int ID
		{
			get
			{
				return (int)SGGB2DataType.Restart;
			}
		}

		protected override int DataSize
		{
			get
			{
				return 0;
			}
		}
	}
}