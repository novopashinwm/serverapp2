﻿namespace FORIS.TSS.Terminal.SGGB2
{
	public class FieldInfo
	{
		public static readonly int PosSeconds         = 4;  // 4
		public static readonly int PosLatitude        = 8; // 6
		public static readonly int PosNorth           = 14; // 1
		public static readonly int PosLongitude       = 15; // 7
		public static readonly int PosEast            = 22; // 1
		public static readonly int PosCorrectGPS      = 23; // 1
		public static readonly int PosDigits          = 24; // 2
		public static readonly int PosAnalogSensor1   = 25; // 2
		public static readonly int PosAnalogSensor2   = 27; // 2
		public static readonly int PosVehicleVoltage  = 29; // 2
		public static readonly int PosControlPos      = 32; // 5
		public static readonly int PosSpeed           = 33; // 4
		public static readonly int PosTemperature     = 31; // 1
		public static readonly int PosSOS             = 37;

		public static readonly int SizeSeconds        = 4;
		public static readonly int SizeLatitude       = 6;
		public static readonly int SizeNorth          = 1;
		public static readonly int SizeLongitude      = 7;
		public static readonly int SizeEast           = 1;
		public static readonly int SizeCorrectGPS     = 1;
		public static readonly int SizeDigits         = 2;
		public static readonly int SizeAnalogSensor1  = 2;
		public static readonly int SizeAnalogSensor2  = 2;
		public static readonly int SizeVehicleVoltage = 2;
		public static readonly int SizeControlPos     = 5;
		public static readonly int SizeSpeed          = 4;
		public static readonly int SizeTemperature    = 1;
	}
}