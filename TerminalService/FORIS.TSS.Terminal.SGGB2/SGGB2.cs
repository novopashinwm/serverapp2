﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Communication;
using FORIS.TSS.Terminal.Messaging;

namespace FORIS.TSS.Terminal.SGGB2
{
	public class SGGB2 : Device
	{
		#region Fields

		private CmdType linkType;
		/// <summary>
		/// current command
		/// </summary>
		private IStdCommand cmdTask;

		/// <summary>
		/// device work mode
		/// </summary>
		CmdType ctMode = CmdType.Unspecified;

		/// <summary>
		/// open connection with device
		/// </summary>
		/// <remarks>can be cached in OnLinkOpen and used till OnLinkClose</remarks>
		ILink lnLink;

		const string sMsgNoPar = "MessageNo";

		readonly static TraceSwitch tsSGGB = new TraceSwitch("TraceSGGB", "Info from SGGB device");

		const string sFromTimePar = "FromTime";
		const string sToTimePar = "ToTime";

		// send text response
		const string sDelivered = "Сообщение доставлено";
		const string sAccepted = "Сообщение прочитано";
		const string sRejected = "Сообщение не прочитано";
		const string sFull = "Нет места для новых сообщений";
		static readonly string[] arTextRes = new[]
			{
				null, sDelivered, sAccepted, sRejected, sFull
			};

		#endregion

		#region Properties

		private CmdType LinkType
		{
			get { return linkType; }
			set { linkType = value; }
		}

		#endregion

		#region IDevice Members
		internal static class DeviceNames
		{
			internal const string SureGuardSGGB2 = "SureGuard SGGB2";
		}
		public SGGB2() : base(null, new[]
		{
			DeviceNames.SureGuardSGGB2,
		})
		{
		}
		ArrayList OutputDatagram(byte[] data)
		{
			// list of output dg
			ArrayList alOut = new ArrayList(1);
			if (data.Length == 0) return alOut;

			byte[] buf = data;

			int prev, pos = 0, len = buf.Length;
			do
			{
				prev = pos;
				// make attempt create output dg
				SGGB2OutputDatagram dg = SGGB2OutputDatagram.Create(buf, ref pos);
				// succeeded
				if (dg != null)
				{
					Debug.Assert(!dg.Valid || pos > 0 && pos <= len, "Incorrect pos of next dg");
					alOut.Add(dg);
				}
			}
			while (pos > -1 && pos < len && prev < pos && alOut.Count < 256);	// pause parsing at 256 dg
			// another try. look if it's SMS and just sub data received
			if (LinkType == CmdType.SMS)
			{
				SGGB2SubData sub = SGGB2SubData.Create(buf, 0);
				if (sub == null)	// no more data expected
				{
					return null;
				}
				Debug.Assert(sub.Size <= data.Length,
					"byte count in sub data > input bytes");
				alOut.Add(SGGB2OutputDatagram.Create(sub));
			}

			return alOut;
		}

		public override byte[] GetCmd(IStdCommand cmd)
		{
			SGGB2SubData data;
			switch (cmd.Type)
			{
				case CmdType.Confirm:
					switch ((string)cmd.Params["ConfirmMessType"])
					{
						case "position":
							data = new SGGB2SubDataConfirm((int)cmd.Params["time"], 
														   SGGB2DataType.GPS);
							break;
						case "log":
							data = new SGGB2SubDataConfirm((int)cmd.Params["time"],
														   SGGB2DataType.Log);
							break;
						case "notify":
							data = new SGGB2SubDataConfirm((int)cmd.Params["ConfirmNo"],
														   SGGB2DataType.Text);
							break;
						default:
							data = null;
							break;
					}
					break;
				case CmdType.SendText:
					data = new SGGB2SubDataText(cmd.Params["Text"].ToString());
					break;
				case CmdType.GetSettings:
					data = new SGGB2SubDataGetSettings();
					
					break;
				case CmdType.GetLog:
					{
						SGGB2SubDataGetLog sub = new SGGB2SubDataGetLog();
						sub.From = (DateTime)cmd.Params[sFromTimePar];
						sub.To = (DateTime)cmd.Params[sToTimePar];
						sub.QueryType = 2;
						data = sub;
					}
					break;
				case CmdType.SetSettings:
					{
						Debug.WriteLine("SGGB: SetSettings...");
						SGGB2SubDataSetSettings sub = new SGGB2SubDataSetSettings();

						sub.IP = (IPAddress)cmd.Params["IP"];
						sub.Port = (int)cmd.Params["Port"];
						sub.TCP = (bool)cmd.Params["Tcp"];
						sub.Interval = (int)cmd.Params["Interval"];
						sub.VoicePhone = (string)cmd.Params["VoicePhone"];
						sub.DataPhone = (string)cmd.Params["DataPhone"];
						sub.TimeZone = (int)cmd.Params["TimeZone"];
						sub.EntryPoint = (string)cmd.Params["EntryPoint"];
						sub.Login = (string)cmd.Params["Login"];
						sub.Password = (string)cmd.Params["Password"];
						sub.UnitNo = (int)cmd.Params["UnitNo"];
						sub.IpFtp = IPAddress.Any;
						sub.LoginFtp = "";
						sub.PasswordFtp = "";
						sub.SleepPeriod = 60;

						data = sub;
					}
					break;
				case CmdType.Control:
					{
						SGGB2SubDataControl sub = new SGGB2SubDataControl();
						sub.Digits = (BitArray) cmd.Params["Digits"];
						sub.Channel1 = (int) cmd.Params["Channel1"];
						sub.Channel2 = (int) cmd.Params["Channel2"];

						data = sub;
					}
					break;
				case CmdType.Restart:
					{
						SGGB2SubDataRestart sub = new SGGB2SubDataRestart();
						data = sub;
					}
					break;
				case CmdType.SetFirmware:
					{
					   SGGB2SubDataSetFirmware sub = new SGGB2SubDataSetFirmware(((byte[])cmd.Params["Data"]).Length + 2);

						sub.Index = (int)cmd.Params["Index"];
						sub.FirmwareData = (byte[])cmd.Params["Data"];

						data = sub;
					}
					break;
				default:
					Trace.WriteLineIf(tsSGGB.TraceError, "SGGB2: Command not supported yet! Command is: "+cmd, "SGGB2");
					return null;
			}
			if(data == null)
				return null;
			SGGB2Datagram dg;
			if (cmd.Params[sMsgNoPar] is int)
				dg = SGGB2InputDatagram.Create((int)cmd.Params[sMsgNoPar], data);
			else dg = SGGB2InputDatagram.Create(data);

			Debug.Assert(dg.Valid, "Datagram invalid");

			return this.LinkType == CmdType.SMS ? Postprocess(dg.GetBytes()) : dg.GetBytes();
		}

		private byte[] Postprocess(byte[] data)
		{
			// sms data in special format. post processing.
			// every byte divided into 2.
			// ex. res[2 * i] = 0x60 | HIBYTE(data[i]); res[2 * i + 1] = 0x60 | LOBYTE(data[i])
			if (LinkType == CmdType.SMS)
			{
				byte[] res = new byte[data.Length * 2];
				for (int i = 0, len = data.Length; i < len; ++i)
				{
					res[2 * i] = (byte)(0x40 | data[i] >> 4);
					res[2 * i + 1] = (byte)(0x40 | data[i] & 0xf);
				}
				return res;
			}
			return data;
		}

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;
			if (!Initialized)
				throw new ApplicationException("Device not initialized");

			// list of wrapped responses
			ArrayList alRes = new ArrayList(1);

			if (this.LinkType == CmdType.SMS)
				data = Preprocess(data, count);
			try
			{
				List<int> datagrams;
				DatagramasShiftsInPacket(data, out datagrams);
				if (datagrams.Count == 0)
					return null;

				foreach (int offset in datagrams)
				{
					if (DataIsGpsOrLog(data, offset))
					{
						int PacketID;
						bool bAlarm;
						MobilUnit.Unit.MobilUnit mu = ParseGPSData(data, offset, out PacketID, out bAlarm);
						if (mu != null)
						{
							IMessage msg = MessageCreator.CreatePosition(mu, mu.ValidPosition);
							msg.Num = PacketID;
							alRes.Add(msg);

							if (bAlarm)
							{
								object[] par = new object[4];
								par[0] = mu.UnitInfo;
								par[1] = "";
								par[2] = 1;
								par[3] = PacketID;
								alRes.Add(new NotifyEventArgs((time_t) mu.Time, TerminalEventMessage.NE_ALARM, null,
															  Severity.Lvl1, NotifyCategory.Controller, "Тревога", null,
															  par));
							}
						}
					}
					else if (IsDataText(data, offset))
					{
						int PacketID;
						IUnitInfo iui = ParseHeader(data, offset, out PacketID);
						string text = ParseTextMessage(data, offset + 16);

						object[] par = new object[4];
						par[0] = iui;
						par[1] = -1;
						par[2] = text;
						par[3] = PacketID;
						alRes.Add(new NotifyEventArgs(TerminalEventMessage.NE_MESSAGE, null, Severity.Lvl3,
							NotifyCategory.Controller, text, null, par));
						if(text.Contains("ремонт"))
							alRes.Add(new NotifyEventArgs(TerminalEventMessage.NE_FREE, null,
															  Severity.Lvl1, NotifyCategory.Controller, "Тревога", null,
															  par));
						if (text.Contains("болезн"))
							alRes.Add(new NotifyEventArgs(TerminalEventMessage.NE_TO_ORDER, null,
															  Severity.Lvl1, NotifyCategory.Controller, "Тревога", null,
															  par));
						if (text.Contains("ДТП"))
							alRes.Add(new NotifyEventArgs(TerminalEventMessage.NE_ORDER, null,
															  Severity.Lvl1, NotifyCategory.Controller, "Тревога", null,
															  par));
					}
					else if (IsDataConfirm(data, offset))
					{
						int PacketID;
						int replyPacketID;
						IUnitInfo iui = ParseHeader(data, offset, out PacketID);
						int state;
						CmdType type;

						if (ParseConfirmData(data, offset, out type, out state, out replyPacketID))
						{
							ICommandMessage msg =
									Manager.GetRegisteredMessage(replyPacketID) as ICommandMessage;

							if (msg != null)
							{
								IStdCommand cmd = msg.Command as IStdCommand;
								if (cmd != null)
								{
									switch (type)
									{
										case CmdType.SendText:
											{
												string txt = arTextRes[state];
												alRes.Add(
													new NotifyEventArgs(TerminalEventMessage.NE_CONFIRM, null,
																		Severity.Lvl3,
																		NotifyCategory.Controller, txt, null, iui, "",
																		replyPacketID, state,
																		PacketID, Mode));
											}
											break;
									}
								}
							}
						}
					}
					else if(IsDataConfirmFirmware(data, offset))
					{
						int PacketID;
						IUnitInfo iui = ParseHeader(data, offset, out PacketID);
						int dataPartNo;
						bool result = ParseFirmwareConfirm(data, offset, out dataPartNo);
						object[] par = new object[4];
						par[0] = iui;
						par[1] = dataPartNo;
						par[2] = result;
						par[3] = "";
						alRes.Add(new NotifyEventArgs(TerminalEventMessage.NE_CONFIRM_FIRMWARE, null,
							Severity.Lvl2, NotifyCategory.Controller, "Прошивка", null, par));
					}
				}
				if (alRes.Count > 0)
					return alRes;

				foreach(SGGB2OutputDatagram dg in OutputDatagram(data))
				{
					if (!dg.Valid)
					{
						// !! WARN !!. if dg is bad we must reject all buf
						// but we can suggest it's possible to find next dg in buf
						//Debug.Fail("Invalid datagram");
						Debug.WriteLineIf(tsSGGB.TraceWarning, "Invalid datagram", "SGGB2");
						Debug.Indent();
						Debug.WriteLineIf(tsSGGB.TraceWarning, dg, "SGGB2");
						Debug.Unindent();

						continue;
					}
					IUnitInfo ui = UnitInfo.Factory.Create(UnitInfoPurpose.SGGB);
					ui.ID = dg.UnitNo;

					foreach (SGGB2SubData sub in dg.DataField)
					{
						if (!sub.Valid)
						{
							if (tsSGGB.TraceVerbose)
							{
								Debug.WriteLine(DateTime.Now);
								Debug.WriteLine(string.Format("Invalid sub data {0:X2}", sub.ID), "SGGB2");
								Debug.Indent();
								Debug.WriteLine(sub);
								Debug.WriteLine("in datagram");
								Debug.WriteLine(dg);
								Debug.Unindent();
							}
						}
						if (sub.ID == (int)SGGB2DataType.Text)
						{
							string txt = ((SGGB2SubDataText)sub).Text;
							object[] par = new object[4];
							par[0] = ui;
							par[1] = -1;
							par[2] = txt;
							par[3] = dg.PacketID;
							alRes.Add(new NotifyEventArgs(TerminalEventMessage.NE_MESSAGE, null, Severity.Lvl3,
								NotifyCategory.Controller, txt, null, par));
						}
						else if (sub.ID == (int)SGGB2DataType.Settings)
						{
							Debug.WriteLine(DateTime.Now + " SGGB2 Settings Received");
							SGGB2SubDataSettings settings = (SGGB2SubDataSettings)sub;
							object[] par = new object[13];
							par[0] = ui;
							par[1] = settings.VoicePhone;
							par[2] = settings.ModuleFW;
							par[3] = settings.FTPIP;
							par[4] = settings.TCP;
							par[5] = settings.Port;
							par[6] = settings.IP;
							par[7] = settings.Interval;
							par[8] = settings.DataPhone;
							par[9] = settings.InetEntryPoint;
							par[10] = settings.Login;
							par[11] = settings.Password;
							par[12] = settings.TimeZone;
							alRes.Add(new NotifyEventArgs(TerminalEventMessage.NE_SETTINGS, null,
								Severity.Lvl2, NotifyCategory.Controller, "Настройки контроллера", null, par));
						}
						else if (sub.ID == (int)SGGB2DataType.Sggb2Confirm)
						{
							SGGB2SubDataGetLogReply getLogReply = (SGGB2SubDataGetLogReply)sub;
						}
					}
				}
			}
			catch(Exception)
			{
				return null;
			}

			return alRes;
		}

		

		private static IUnitInfo ParseHeader(byte[] bytes, int offset, out int PacketID)
		{
			int pos = 9;

			PacketID = Datagram.IntFromBytesBE(bytes[offset + pos], bytes[offset + pos + 1]);

			int ControllerNum = Datagram.IntFromBytesBE(bytes[offset + 7], bytes[offset + 8]);

			UnitInfo ui = UnitInfo.Factory.Create(UnitInfoPurpose.MobilUnit);
			ui.ID = ControllerNum;

			return ui;
		}

		private static readonly Encoding encoding = Encoding.GetEncoding(1251);

		private static string ParseTextMessage(byte[] data, int offset)
		{
			try
			{
				byte id = data[offset];
				if (id != (byte)SGGB2DataType.Text)
					throw new ArgumentException("There is no text in data.", "data");
				int size = Datagram.IntFromBytesBE(data[offset + 1], data[offset + 2]);

				return encoding.GetString(data, offset + 3, size);
			}
			catch (Exception)
			{
				return string.Empty;
			}
		}

		private static bool ParseFirmwareConfirm(byte[] data, int offset, out int partNo)
		{
			partNo = -1;
			try
			{
				int blockID = data[offset + 21];

				if (blockID == 0x26)
				{
					partNo = Datagram.IntFromBytesBE(data, offset + 22, 2);
					if(partNo == 0)
					{
						return data[offset + 24] == 0x01;
					}
				}
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		private static bool ParseConfirmData(byte[] data, int offset, out CmdType type, out int state, out int replyPacketID)
		{
			state = -1;
			replyPacketID = -1;
			type = CmdType.Unspecified;
			try
			{
				replyPacketID = Datagram.IntFromBytesBE(data, offset + 19, 2);
				int blockID = data[offset + 21];
				if(blockID == (int) SGGB2DataType.SendText)
				{
					type = CmdType.SendText;
					state = data[offset + 22];
				}
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}



		private MobilUnit.Unit.MobilUnit ParseGPSData(byte[] bytes, int offset, out int PacketID, out bool Alarm)
		{
			Alarm = false;

			const int headerSize = 19 - 3;

			const float MarineKnot = (float)1.852;

			const int AbsoluteZero = -273;

			var pos = 9;

			PacketID = Datagram.IntFromBytesBE(bytes[offset + pos], bytes[offset + pos + 1]);

			#region longitude

			pos = headerSize + FieldInfo.PosLongitude;
			float longitude = Datagram.IntFromBytesBE(bytes[offset + pos], bytes[offset + pos + 1]);
			// минуты 
			longitude += Datagram.IntFromBytesBE(bytes[offset + pos + 2]) / 60.0f;
			// доли минут 
			longitude += Datagram.FloatFromBytesBE(bytes[offset + pos + 3], bytes[offset + pos + 4],
				bytes[offset + pos + 5], bytes[offset + pos + 6]) / 60;

			#endregion //longitude

			#region latitude
			pos = headerSize + FieldInfo.PosLatitude;
			// градусы
			float latitude = Datagram.IntFromBytesBE(bytes[offset + pos]);
			// минуты 
			latitude += Datagram.IntFromBytesBE(bytes[offset + pos + 1]) / 60.0f;
			// доли минут 
			latitude += Datagram.FloatFromBytesBE(bytes[offset + pos + 2], bytes[offset + pos + 3],
				bytes[offset + pos + 4], bytes[offset + pos + 5]) / 60;
			#endregion //latitude

			#region north

			bool north = (char)bytes[offset + headerSize + FieldInfo.PosNorth] == 'N';

			#endregion //north

			#region east

			bool east = (char)bytes[offset + headerSize + FieldInfo.PosEast] == 'E';

			#endregion //east

			#region seconds

			int seconds = 0;

			pos = headerSize + FieldInfo.PosSeconds;
			int size = FieldInfo.SizeSeconds;

			if (size == 4)
				seconds = Datagram.IntFromBytesBE(
					 bytes[offset + pos], bytes[offset + pos + 1], bytes[offset + pos + 2], bytes[offset + pos + 3]);

			#endregion //seconds

			#region satellites

			int satellites = bytes[offset + headerSize + FieldInfo.PosCorrectGPS];

			satellites = satellites > 0 ? satellites : -1;

			#endregion //satellites

			#region speed

			pos = headerSize + FieldInfo.PosSpeed;
			float speed =
				Datagram.FloatFromBytesBE(bytes[offset + pos], bytes[offset + pos + 1], bytes[offset + pos + 2], bytes[offset + pos + 3]);
			speed *= MarineKnot;

			#endregion //speeed

			#region correctGPS

			bool correctGPS =
				satellites >= MobilUnit.Unit.MobilUnit.MinSatellites &&
				longitude > 0 && longitude < 180 &&
				latitude > 0 && latitude < 90 &&
				(byte)speed <= MobilUnit.Unit.MobilUnit.GPSLimitMaxSpeed;

			#endregion //correctGPS

			#region analogSensor1

			pos = headerSize + FieldInfo.PosAnalogSensor1;
			int analogSensor1 = Datagram.IntFromBytesBE(bytes[offset + pos], bytes[offset + pos + 1]);

			#endregion analogSensor1

			#region analogSensor2

			pos = headerSize + FieldInfo.PosAnalogSensor2;
			int analogSensor2 = Datagram.IntFromBytesBE(bytes[offset + pos], bytes[offset + pos + 1]);

			#endregion analogSensor2

			#region VehicleVoltage

			pos = headerSize + FieldInfo.PosVehicleVoltage;

			int vehicleVoltage = Datagram.IntFromBytesBE(bytes[offset + pos], bytes[offset + pos + 1]);

			#endregion //VehicleVoltage


			#region temperature

			int temperature;

			temperature = Datagram.IntFromBytesBE(bytes, headerSize + FieldInfo.PosTemperature, 1);

			#endregion temperature

			
			#region digits

			pos = headerSize + FieldInfo.PosDigits;
			int digits = Datagram.IntFromBytesBE(bytes[offset + pos], bytes[offset + pos + 1]);

			#endregion

			#region SOS

			bool SOS = bytes[offset + headerSize + FieldInfo.PosSOS] == 0x01;

			#endregion //SOS

			var mu = UnitReceived();
			mu.ID = Datagram.IntFromBytesBE(bytes[offset + 7], bytes[offset + 8]);

			var ID = (SGGB2DataType)bytes[16 + offset];
			switch (ID)
			{
				case SGGB2DataType.GPS:
					mu.cmdType = CmdType.Trace;
					break;
				case SGGB2DataType.Log:
					mu.cmdType = CmdType.GetLog;
					break;
				default:
					Debug.Assert(false, "Позиция и не обычная, и не историческая. По идее, этого не может быть.");
					return null;
			}

			mu.Latitude = north ? latitude : -1 * latitude;
			mu.Longitude = east ? longitude : -1 * longitude;
			mu.Speed = (int)speed;
			mu.Time = seconds;
			mu.PowerVoltage = vehicleVoltage;

			if (temperature != AbsoluteZero)
				mu.VoltageAN1 = temperature;
			else mu.VoltageAN1 = int.MaxValue;

			mu.VoltageAN2 = int.MaxValue;
			mu.VoltageAN3 = analogSensor1;
			mu.VoltageAN4 = analogSensor2;

			mu.CorrectGPS = correctGPS;

			if (mu.cmdType == CmdType.Trace)
				Alarm = SOS;
#if DEBUG
			if (SOS && mu.cmdType == CmdType.GetLog)
			{
				Trace.WriteLine("SOS in the archive! Datagram: " + BitConverter.ToString(bytes, offset, 56));
			}
#endif

			return mu;
		}

	   
		private static bool IsDataText(byte[] data, int offset)
		{
			try
			{
				if (data.Length < offset + 16)
					return false;

				if (data[offset + 0] != (byte)SGGB2DataType.SGGB)
					return false;

				var ID = (SGGB2DataType)data[offset + 16];
				return ID == SGGB2DataType.Text;
			}
			catch (Exception)
			{
				return false;
			}
		}

		private static bool IsDataConfirm(byte[] data, int offset)
		{
			try
			{
				if (data.Length < offset + 22)
					return false;

				if (data[offset + 0] != (byte)SGGB2DataType.SGGB)
					return false;

				var ID = (SGGB2DataType)data[offset + 16];
				if (ID == SGGB2DataType.Sggb2Confirm)
				{
					int blockID = data[offset + 21];
					if(blockID == 0x26)
					{
						return false;
					}
					return true;
				}

				return false;
			}
			catch (Exception)
			{
				return false;
			}
		}

		private static bool IsDataConfirmFirmware(byte[] data, int offset)
		{
			try
			{
				if(data.Length < offset + 24)
					return false;
				if (data[offset + 0] != (byte)SGGB2DataType.SGGB)
					return false;

				var ID = (SGGB2DataType)data[offset + 16];
				if (ID == SGGB2DataType.Sggb2Confirm)
				{
					int blockID = data[offset + 21];
					if (blockID == 0x26)
					{
						return true;
					}
				}

				return false;
			}
			catch (Exception)
			{
				return false;
			}
		}

		private static bool DataIsGpsOrLog(byte[] data, int offset)
		{
			if (data[offset + 0] != 0xAA)
				return false;

			if (offset + 56 > data.Length)
				return false;

			var size = 18 + Datagram.IntFromBytesBE(data[offset + 12], data[offset + 13]);
			if (size != 56)
				return false;
			var ID = (SGGB2DataType)data[offset + 16];
			return ID == SGGB2DataType.GPS || ID == SGGB2DataType.Log;
		}

		/// <summary>
		/// Возвращает начала всех датаграмм в пакете
		/// </summary>
		/// <param name="data">пакет с данными</param>
		/// <param name="list">список смещений датаграмм в пакете</param>
		private static void DatagramasShiftsInPacket(byte[] data, out List<int> list)
		{
			list = new List<int>();
			try
			{
				int begin = 0;

				for (;;)
				{
					if (begin >= data.Length)
						break;
					int pos = Array.IndexOf(data, (byte) 0xAA, begin);
					if (pos >= 0 && pos + 11 < data.Length && data[pos+11] == 1)
					{
						list.Add(pos);

						int size = 18 + Datagram.IntFromBytesBE(data[pos + 12], data[pos + 13]);
						begin = pos + size;
					}
					else
						break;
				}
			}
			catch(Exception)
			{
				Trace.WriteLineIf(tsSGGB.TraceError, "Оборванный пакет", "SGGB2");
			}
		}

		private byte[] Preprocess(byte[] data, int count)
		{
			byte[] res;

			// SMS data in special format. preprocessing.
			// every byte divided into 2.
			// ex. res[i] = LOBYTE(data[2 * i]) << 4 | LOBYTE(data[2 * i + 1])
			if (LinkType == CmdType.SMS)
			{
				res = new byte[count / 2];
				for (int i = 0, len = res.Length; i < len; ++i)
					res[i] = (byte)((data[2 * i] & 0xf) << 4 | data[2 * i + 1] & 0xf);
				return res;
			}
			res = new byte[count];
			Array.Copy(data, res, count);
			return res;
		}

		public override bool SupportData(byte[] data)
		{
			int pos = Array.IndexOf(data, (byte)0xAA, 0);
			if(pos == 0 && data.Length > pos+11 && data[pos+11] == 1)
				return true;

			return false;
		}

		#endregion
	}
}