﻿namespace FORIS.TSS.Terminal.SGGB2
{
	public enum SGGB2DataType : byte
	{
		/// <summary> Инф. блок содержит  состояния модуля на момент времени стоящего в поле «время в сек от начала суток» </summary>
		GPS          = 0x15,
		Log          = 0x19,
		Settings     = 0x0B,
		Text         = 0x05,
		Alarm,
		Confirm      = 0x22,
		ConfirmFirmware,
		SendConfirm,
		Undefined    = 0xFF,
		SGGB         = 0xAA,
		SendText     = 0x04,
		GetSettings  = 0x0E,
		GetLog       = 0x17,
		Sggb2Confirm = 0x11,
		SetSettings  = 0x0A,
		Control      = 0x02,
		Restart      = 0xEE,
		SetFirmware  = 0x26
	}
}