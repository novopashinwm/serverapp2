﻿using System.Collections.Generic;
using FORIS.TSS.Common;

namespace RU.NVG.NIKA.Terminal.SatSolRu
{
	internal class BinaryWriter
	{
		private readonly List<byte> _bytes = new List<byte>();

		public void WriteBytes(byte[] bytes)
		{
			_bytes.AddRange(bytes);
		}
		public void Write(byte value, int count)
		{
			for (var i = 0; i != count; ++i)
				_bytes.Add(value);
		}
		public byte[] Result
		{
			get { return _bytes.ToArray(); }
		}
		public void WriteLE(ushort value)
		{
			_bytes.AddRange(value.ToLE());
		}
	}
}