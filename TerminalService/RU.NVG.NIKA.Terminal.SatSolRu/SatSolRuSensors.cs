﻿// ReSharper disable InconsistentNaming
namespace RU.NVG.NIKA.Terminal.SatSolRu
{
	public enum SatSolRuSensors
	{
		mode_acc_stop  =  1,
		Ignition       =  2,
		Alarm          =  3,
		DiscreteInput0 = 10,
		DiscreteInput1 = 11,
		DiscreteInput2 = 12,
		DiscreteInput3 = 13,
		DiscreteInput4 = 14,
		DiscreteInput5 = 15,
		DiscreteInput6 = 16,
		DiscreteInput7 = 17
	}
}