﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace RU.NVG.NIKA.Terminal.SatSolRu
{
	public class SatSolRuDevice : Device
	{
		public SatSolRuDevice() : base(typeof(SatSolRuSensors))
		{
		}

		private static readonly byte[] PreambleBytes = new byte[] { 0x2c, 0x8A };

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			var reader = new DataReader(data, 0, count);

			var receivedCrc = (ushort) reader.ReadLittleEndian32(2);
			if (receivedCrc != Crc16(data, 2, count - 2))
			{
				bufferRest = null;
				return null;
			}

			//Preamble
			reader.Skip(2);

			var deviceId = reader.ReadLittleEndian32(4).ToString();

			//Length
			reader.Skip(2);

			var result = new ArrayList();
			while (reader.Any())
			{
				//skip crc
				reader.Skip(2);
				//serial_id - серийный номер пакета
				reader.Skip(2);
				
				var mu = new MobilUnit { DeviceID = deviceId, CorrectGPS = false, SensorValues = new Dictionary<int, long>()};
				result.Add(mu);

				mu.Time = (int) reader.ReadLittleEndian32(4);
				var packetType = reader.ReadLittleEndian32(2);
				var packetLength = reader.ReadLittleEndian32(2);
				var xCoord = reader.ReadLittleEndian32(4);
				var yCoord = reader.ReadLittleEndian32(4);
				if (xCoord != 0xffffffff)
					mu.Latitude = ((double) xCoord/0xFFFFFFFF)*90;
				if (yCoord != 0xffffffff)
					mu.Longitude = ((double)yCoord / 0xFFFFFFFF) * 180;
				var sf = reader.ReadByte();
				var correctGPS = xCoord != 0xffffffff && yCoord != 0xffffffff;

				if (correctGPS)
				{
					if (((1 << 6) & sf) != 0)
						mu.Latitude = -mu.Latitude;
					if (((1 << 7) & sf) != 0)
						mu.Longitude = -mu.Longitude;
				}
				mu.CorrectGPS = correctGPS;

				switch (packetType)
				{
					case 0x0040:
						ReadGpsPacket0040(mu, reader);
						break;
					case 0x0003:
						ReadGpsPacket0003(mu, reader);
						break;
					case 0x000A:
						ReadAlarmPacket(mu, reader);
						break;
					default:
						LogNotImplementedPacketType(packetType, data);
						reader.Skip((int)packetLength);
						break;
				}
				
			}

			result.Add(new ConfirmPacket(GetConfirmBytes(deviceId)));

			//TODO: реализовать работу с bufferRest
			bufferRest = null;
			return result;
		}

		private void ReadAlarmPacket(MobilUnit mu, DataReader reader)
		{
			//PACKET_TYPE_ALARM_DATA 0x000A - данные тревожной кнопки
			//1 typedef struct {
			//2  uint16_t gps_speed;  // Скорость в момент возникновения тревожного события
			mu.Speed = (int)(reader.ReadLittleEndian32(2) / 100);
			//3  uint16_t gps_angle;  // Направление в момент возникновения тревожного события
			mu.Course = (int)reader.ReadLittleEndian32(2);
			//4  uint32_t x_coord;  // Широта - милионные доли градуса
			reader.Skip(4);
			//5  uint32_t y_coord;  // Долгота - милионные доли градуса
			reader.Skip(4);
			//6  uint8_t alarm_state;  // 1 - есть тревога; 0 - нет тревоги
			var alarmState = reader.ReadByte();
			mu.SensorValues[(int) SatSolRuSensors.Alarm] = alarmState;
			//7 } t_alarm_data_v1;
		}

		private void ReadGpsPacket0003(MobilUnit mu, DataReader reader)
		{
//uint16_t v_in; // входное напряжение, сотые доли вольта
			reader.Skip(2);

//uint16_t v_bat; // напряжение на акумуляторе, сотые доли вольта
			reader.Skip(2);

			//int16_t fs_data; // данные топливного датчика
			reader.Skip(2);

			//uint8_t mode_acc_stop:4; // состояние остановки по акселерометру. 0 - остановк ане зафиксирована; 1 - зафиксирована остановка;
			mu.SensorValues.Add((int)SatSolRuSensors.mode_acc_stop, reader.ReadByte());
			Trace.TraceInformation(
				"{0}: {1}.mode_acc_stop = {2}",
				this, mu.DeviceID, mu.SensorValues[(int) SatSolRuSensors.mode_acc_stop]);

			//uint8_t ignition_on:4; // состояние датчика зажигания. 0 - зажигание выключено; 1 - зажигание включено;
			reader.Skip(1);

			//uint8_t d_state; // состояние дискретныз датчиков - см. выше
			reader.Skip(1);

			//uint8_t fix_type; // тип фиксации GPS/GLONASS;
			reader.Skip(1);

			//uint8_t sat_count; // количество отслеживаемых спутников
			reader.Skip(1);

			//uint16_t hdop; // hdop - сотые доли
			reader.Skip(2);

			//uint16_t altitude; // Высота антенны приёмника над/ниже уровня моря, м;
			reader.Skip(2);

			//uint16_t geoid_height; // Геоидальное различие - различие между земным эллипсоидом WGS-84 и уровнем моря(геоидом), ”-” = уровень моря ниже эллипсоида.;
			reader.Skip(2);

			//uint16_t speed; // скорость - сотые доли км-ч
			reader.Skip(2);

			//uint16_t azimuth; // вектор перемещения - сотые доли градусов
			reader.Skip(2);

			//int32_t x_coord; // широта - кодирование в соответсвии с правилами ЕГТС; (4294967295 - координата невалидна)
			reader.Skip(4);

			//int32_t y_coord; // долгота - кодирование в соответсвии с правилами ЕГТС; (4294967295 - координата невалидна)
			reader.Skip(4);

			//uint8_t ant_state; // состояние GPS антенны. 0 - OK, 1 - короткое замыкание, 2 - отсутсвует или обрыв.
			reader.Skip(1);

			//uint8_t egts_flags; // флаги ЕГТС для передачи на сервер
			reader.Skip(1);

			//uint8_t egts_src; // Источник данных ЕГТС для передачи на сервер
			reader.Skip(1);
		}

		private readonly ConcurrentDictionary<uint, bool> _loggedPacketTypes = new ConcurrentDictionary<uint, bool>(); 
		private void LogNotImplementedPacketType(uint packetType, byte[] data)
		{
			if (!_loggedPacketTypes.TryAdd(packetType, true))
				return;
			Trace.TraceInformation(
				"{0}: packet type {1:X} is not implemented: {2}",
				this, packetType, data.ToHexString());
		}

		private byte[] GetConfirmBytes(string deviceId)
		{
			var payload = GetConfirmPayload();
			var container = new BinaryWriter();
			container.Write(0, 2); //crc - будет заполнено позднее
			container.WriteBytes(PreambleBytes);
			container.WriteBytes(uint.Parse(deviceId).ToLE());
			container.WriteLE((ushort)payload.Length); //data_len - будет заполнено позднее
			container.WriteBytes(payload);

			// Заполняем контрольную сумму и отправляем результат
			var bytes = container.Result;
			Array.Copy(Crc16(bytes, 2, bytes.Length - 2).ToLE(), 0, bytes, 0, 2);
			
			return bytes;
		}

		private static byte[] GetConfirmPayload()
		{
			var payload = new BinaryWriter();
			payload.Write(0, 2);    //crc - будет заполнено позднее
			payload.Write(0, 2);    //serial_id
			payload.Write(0, 4);    //timestamp
			payload.Write(0xff, 2); //packet_type
			payload.Write(0, 2);    //packet_len
			payload.Write(0xff, 4); //x_coord invalid
			payload.Write(0xff, 4); //y_coord invalid
			payload.Write(0, 1);    //sf
			// Заполняем контрольную сумму и отправляем результат
			var bytes = payload.Result;
			Array.Copy(Crc16(bytes, 2, bytes.Length - 2).ToLE(), 0, bytes, 0, 2);
			return bytes;
		}

		private void ReadGpsPacket0040(MobilUnit mu, DataReader reader)
		{

			//uint16_t v1224; - напряжение на аккумуляторе автомобиля, сотые доли вольта 
			reader.Skip(2);

			//uint16_t v_bat; - напряжение на аккумуляторе трекера - сотые доли вольта; 
			reader.Skip(2);

			//uint16_t fs_data; - данные топливного датчка 1 на канале RS232 или RS485 
			reader.Skip(2);

			//uint8_t stop_state; - 1 если автомобиль стоит на месте; 0 - если едет; 
			reader.Skip(1);

			//uint8_t ign_state; - 1 если зажигание включено; 0 - если зажигание выключено; 
			var ignition = reader.ReadByte();
			mu.SensorValues[(int) SatSolRuSensors.Ignition] = ignition;

			//uint8_t d_state; - состояние дискретных входов - битовая маска; 
			var discreteInputs = reader.ReadByte();
			for (var i = 0; i != 8; ++i)
			{
				mu.SensorValues[(int) SatSolRuSensors.DiscreteInput0+i] = (discreteInputs >> i) & 0x01;
			}

			//uint16_t freq1; - частота на дискретном входе 1; 
			reader.Skip(2);

			//uint16_t c_counter1; - счетчик импульсов на дискретном входе 1; 
			reader.Skip(2);

			//uint16_t freq2; - частота на дискретом входе 2; 
			reader.Skip(2);

			//uint16_t c_counter2; - счетчик импульсов на дискретном входе 2; 
			reader.Skip(2);

			//uint8_t ant_state; - состояние GPS-антенны; 
			reader.Skip(1);

			//uint8_t fix_type; - тип фиксации спутников; 
			reader.Skip(1);

			//uint8_t sat_count; - количество отслеживаемых спутников; 
			mu.Satellites = reader.ReadByte();

			//uint16_t altitude; - высота; 
			mu.Height = (int)reader.ReadLittleEndian32(2);

			//uint16_t geoid_height; - высота геоида; 
			reader.Skip(2);

			//uint32_t x_coord; - широта в системе шифрования ЕГТС; 
			reader.Skip(4);

			//uint32_t y_coord; - долгота в системе шифрования ЕГТС; 
			reader.Skip(4);

			//uint16_t speed; - сотые км-час; 
			mu.Speed = (int)(reader.ReadLittleEndian32(2)/100);

			//uint16_t course; - азимут, градусы; 
			mu.Course = (int)reader.ReadLittleEndian32(2);

			//uint16_t adc1; - данные АЦП1 - слтые доли вольта; 
			reader.Skip(2);

			//uint16_t c_counter3; - счетчик дискретного входа 3; 
			reader.Skip(2);

			//int16_t ow_data; - данные датчика 1-wire N1, сотые доли градуса; 
			reader.Skip(2);

			//uint8_t egts_flags; - флаги ЕГТС; 
			reader.Skip(1);

			//uint8_t egts_src; - источник данных ЕГТС;
			reader.Skip(1);
		}

		public override bool SupportData(byte[] data)
		{
			const int binaryContainerLength = 10;
			const int commonHeaderLength = 21;

			if (data.Length < binaryContainerLength + commonHeaderLength)
				return false;

			//preamble
			for (var i = 0; i != PreambleBytes.Length; ++i)
			{
				if (data[i + 2] != PreambleBytes[i])
					return false;
			}

			//crc
			var receivedCrc   = data.FromLE2UInt16(0, 2);
			var calculatedCrc = Crc16(data, 2, data.Length - 2);
			if (receivedCrc != calculatedCrc)
				return false;

			//TODO: проверить из списка поддерживаемых протоколов, также можно проверить
			var packetLength = data.FromLE2UInt16(8, 2);
			//Максимальная длина бинарного контейнера - 1400байт, включая заголовок.
			if (1400 - binaryContainerLength - commonHeaderLength < packetLength)
				return false;

			return true;
		}

		public static ushort Crc16(byte[] data, int startIndex, int length)
		{
			ushort crc = 0xFFFF;
			
			for (var k = startIndex; k != startIndex + length; ++k)
			{
				crc ^= (ushort)(data[k] << 8);

				for (var i = 0; i < 8; i++)
					crc = (ushort) (((crc & 0x8000) != 0) ? (crc << 1) ^ 0x1021 : crc << 1);
			}
			return crc;
		}
	}
}