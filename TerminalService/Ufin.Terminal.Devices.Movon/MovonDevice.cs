﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Terminal;

namespace Compass.Ufin.Terminal.Devices.Movon
{
	public partial class MovonDevice : Device
	{
		internal const string ProtocolName = "Movon";
		internal static class DeviceNames
		{
			internal const string MovonMDSM7 = "Movon MDSM-7";
		}
		public MovonDevice() : base(null, new[]
		{
			DeviceNames.MovonMDSM7,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			return
				data[0]               == 0x1A &&
				data[data.Length - 1] == 0x1F &&
				data.Length           == 52;
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;
			var results = new List<object>();
			new ArraySegment<byte>(data)
				.ReadNext<byte>          (01, (curr) => {}) // Пропускаем STX, т.к. уже проверено в SupportData;
				.ReadNext<byte, string>  (15, (curr) => { return Encoding.ASCII.GetString(curr.TakeWhile(b => b != 0x00).ToArray()); }, out var muDeviceId)
				.ReadNext<byte, int?>    (01, (curr) => { return (int?)curr.FirstOrDefault(); },                                        out var muSpeed)
				.ReadNext<byte>          (03, (curr) => {}) // Пропускаем Dummy_CARINFO
				.ReadNext<byte, BitArray>(01, (curr) => { return new BitArray(curr.ToArray()); },                                       out var muDsmEvents)
				.ReadNext<byte, string>  (10, (curr) => { return Encoding.ASCII.GetString(curr.TakeWhile(b => b != 0x00).ToArray()); }, out var muDriverName)
				// Record status 0x00 – None, 0x01 – Recording(Mic off), 0x02 – Recording(Mic On)
				.ReadNext<byte, byte>    (01, (curr) => { return (byte)(curr.ElementAt(0) & 0x03); },                                   out var muRecStatus)
				// Пропускаем GPS status 0x00 – None, 0x01 – Connected & Data None, 0x02 – Connected & Data Valuable
				.ReadNext<byte, byte>    (01, (curr) => { return (byte)(curr.ElementAt(0) & 0x03); },                                   out var muGpsStatus)
				.ReadNext<byte, decimal> (04, (curr) => { return curr.ToArray().FromBE2UInt32(0, 4) / 10000m; },                        out var muLat)
				.ReadNext<byte, decimal> (04, (curr) => { return curr.ToArray().FromBE2UInt32(0, 4) / 10000m; },                        out var muLng)
				.ReadNext<byte, BitArray>(01, (curr) => { return new BitArray(curr.ToArray()); },                                       out var errCodes)
				.ReadNext<byte, DateTime>(07, (curr) =>
				{
					var currArray = curr.ToArray();
					return new DateTime(
						currArray.FromBE2UInt16(0, 2),
						currArray[2],
						currArray[3],
						currArray[4],
						currArray[5],
						currArray[6],
						DateTimeKind.Utc);
				}, out var timestmp)
				.ReadNext<byte, bool>   (01, (curr) => { return (curr.ElementAt(0) & 0x01) == 1; }, out var gSensorEvent)
				.ReadNext<byte, byte>   (01, (curr) => { return curr.FirstOrDefault(); },           out var crc)
				;
			var mu        = UnitReceived(ProtocolName);
			mu.DeviceID   = muDeviceId;
			mu.Time       = timestmp.ToLogTime();
			mu.Speed      = muSpeed;
			mu.Latitude   = (double)muLat;
			mu.Longitude  = (double)muLng;
			mu.CorrectGPS = muGpsStatus == 0x02;
			//////////////////////////////////////////////////////////
			return new List<object>(new[] { mu });
			//////////////////////////////////////////////////////////
		}
	}
}