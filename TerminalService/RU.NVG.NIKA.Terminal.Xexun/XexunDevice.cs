﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace RU.NVG.NIKA.Terminal.Xexun
{
	public class XexunDevice : Device
	{
		public XexunDevice() : base(null, new[]
		{
			"Meitrack MT90",
			"Meitrack MT90S",
			"Meitrack MVT100",
			"Meitrack MVT380",
			"Meitrack MVT800",
			"Meitrack T1",
			"Meitrack T622",
			"Meitrack TC68",
			"Meitrack TC68S",
			"Meitrack T355",
			"Meitrack Trackids Bear",
			"Meitrack Trackids Turtle",
		})
		{
		}

		private static readonly byte[] GprmcHeader = Encoding.ASCII.GetBytes(",GPRMC");

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;

			var reader = new SplittedStringReader(Encoding.ASCII.GetString(data, 0, count), ',');

			reader.Skip(2);
			if (reader.ReadString() != "GPRMC")
				return null;
			var timeString = reader.ReadString();
			var timeStringDotIndex = timeString.IndexOf('.');
			if (timeStringDotIndex != -1)
				timeString = timeString.Substring(0, timeStringDotIndex);
			TimeSpan time;
			if (!TimeSpan.TryParseExact(timeString, "hhmmss", CultureInfo.InvariantCulture, out time))
				return null;
			var validGps = reader.ReadString() == "A";
			var latitudeString = reader.ReadString();
			var latitudeHemisphere = reader.ReadString();
			var longitudeString = reader.ReadString();
			var longitudeHemisphere = reader.ReadString();
			var speedKnots = reader.ReadDouble();
			var azimuth = reader.ReadDouble();
			var date = reader.ReadDate("ddMMyy");
			reader.Skip(4);
			var imeiString = reader.ReadString();
			if (!imeiString.StartsWith("imei:"))
				return null;
			var imei = imeiString.Substring("imei:".Length);

			var result = new ArrayList();
			if (validGps)
			{
				var mu = UnitReceived();
				result.Add(mu);
				mu.Latitude = ParseCoordinate(latitudeString);
				if (latitudeHemisphere == "S")
					mu.Latitude = -mu.Latitude;
				mu.Longitude = ParseCoordinate(longitudeString);
				if (longitudeHemisphere == "W")
					mu.Longitude = -mu.Longitude;
				mu.Speed = speedKnots != null ? MeasureUnitHelper.KnotsToKMPH(speedKnots.Value) : (int?) null;
				mu.Course = azimuth != null ? (int) azimuth.Value : (int?) null;
				mu.Time = TimeHelper.GetSecondsFromBase(date.Add(time));
				mu.DeviceID = imei;
			}

			return result;
		}
		private static double ParseCoordinate(string s)
		{
			var rawValue = decimal.Parse(s, NumberHelper.FormatInfo);
			var degrees = Math.Truncate(rawValue / 100);
			var minutes = (rawValue - degrees * 100);
			return (double)(degrees + minutes / 60);
		}
		public override bool SupportData(byte[] data)
		{
			var gprmcHeaderIndex = data.IndexOf(GprmcHeader);
			if (gprmcHeaderIndex == null)
				return false;

			if (gprmcHeaderIndex.Value < 4)
				return false;

			return true;
		}
		public override List<CommandStep> GetCommandSteps(IStdCommand command)
		{
			switch (command.Type)
			{
				case CmdType.Setup:
				{
					switch (command.Target.DeviceType)
					{
						default:
							return CreateSetupSteps(command.Target, command);
					}
				}
				default:
					return base.GetCommandSteps(command);
			}
		}
		private List<CommandStep> CreateSetupSteps(IUnitInfo unitInfo, IStdCommand command)
		{
			var password = unitInfo.Password;
			if (string.IsNullOrWhiteSpace(password))
				password = "0000";

			var ip = Manager.GetConstant(Constant.ServerIP);
			var port = Manager.GetConstant(Constant.ServerPort);

			var internetApnConfig = GetInternetApnConfig(command);
			var apn = internetApnConfig.Name;
			var apnUser = internetApnConfig.User;
			var apnPassword = internetApnConfig.Pass;

			var setupSteps = new List<CommandStep>
			{
				new SmsCommandStep(password + ",A22,8.8.8.8")
				{
					WaitAnswer = CmdType.Setup
				},
				new SmsCommandStep(password + ",A21,1," + ip + "," + port + "," + apn + "," + apnUser + "," + apnPassword)
				{
					WaitAnswer = CmdType.Setup
				},
				new SmsCommandStep(password + ",A12,6,0")
				{
					WaitAnswer = CmdType.Setup
				},
				new SmsCommandStep(password + ",A73,1")
				{
					WaitAnswer = CmdType.Setup
				},
			};
			return setupSteps;
		}
		public override IList<object> ParseSms(IUnitInfo ui, string text)
		{
			var result = new List<object>(2);

			var parts = text.Split(',');
			var imei = parts[0];
			result.Add(ImeiReceived(ui, imei));

			var command = parts[1];
			var commandResult = parts[2];

			var cmdType = GetCmdType(command);
			var cmdResult = GetCmdResult(commandResult);

			result.Add(CommandCompleted(ui, cmdResult, cmdType));

			return result;
		}
		private CmdResult GetCmdResult(string commandResult)
		{
			switch (commandResult)
			{
				case "OK":
					return CmdResult.Completed;
				default:
					return CmdResult.Failed;
			}
		}
		private CmdType GetCmdType(string command)
		{
			switch (command)
			{
				case "A22":
				case "A21":
				case "A12":
				case "A73":
					return CmdType.Setup;
				default:
					throw new ArgumentOutOfRangeException("command", command, "Unexpected command type");
			}
		}
	}
}