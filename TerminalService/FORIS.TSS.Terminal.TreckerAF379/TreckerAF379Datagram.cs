﻿using System;
using System.Globalization;
using System.Text;
using FORIS.TSS.BusinessLogic;

namespace FORIS.TSS.Terminal.TreckerAF379
{
	public class TreckerAF379Datagram
	{
		public string IMEI;
		public int Time;
		public float Lat;
		public float Lng;
		public int Speed;
		public bool IsValid = false;

		public TreckerAF379Datagram(byte[] data)
		{
			/* #353327020214052,
			 * CMD-F,
			 * A,
			 * DATE:091103,
			 * TIME:124956,
			 * LAT:55.7789633N,
			 * LOT:037.5980383E,
			 * Speed:000.0,,#
			*/
			if (data == null) return;
			string strVal = Encoding.ASCII.GetString(data).Replace("#","");
			string[] parts = strVal.Split(',');
			if (parts.Length < 8) return;

			IMEI = parts[0];
			DateTime time = DateTime.ParseExact(parts[3].Split(':')[1] + parts[4].Split(':')[1], "yyMMddHHmmss", CultureInfo.InvariantCulture);

			Time = TimeHelper.GetSecondsFromBase(time);
			Lat = float.Parse(parts[5].Split(':')[1].Replace("N", ""), CultureInfo.InvariantCulture);
			Lng = float.Parse(parts[6].Split(':')[1].Replace("E", ""), CultureInfo.InvariantCulture);
			Speed = (int)float.Parse(parts[7].Split(':')[1], CultureInfo.InvariantCulture);

			IsValid = true;
		}
	}
}