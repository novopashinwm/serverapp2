﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Communication;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.TreckerAF379
{
	public class TreckerAF379 : Device
	{
		#region Fields

		private ITerminalManager manager;
		private CmdType linkType;
		/// <summary>
		/// current command
		/// </summary>
		private IStdCommand cmdTask;

		/// <summary>
		/// device work mode
		/// </summary>
		CmdType ctMode = CmdType.Unspecified;

		/// <summary>
		/// open connection with device
		/// </summary>
		/// <remarks>can be cached in OnLinkOpen and used till OnLinkClose</remarks>
		ILink lnLink;

		const string sMsgNoPar = "MessageNo";

		readonly static TraceSwitch tsTreckerAF379 = new TraceSwitch("TraceTreckerAF379", "Info from TreckerAF379 device");

		const string sFromTimePar = "FromTime";
		const string sToTimePar = "ToTime";

		// send text response
		const string sDelivered = "Сообщение доставлено";
		const string sAccepted = "Сообщение прочитано";
		const string sRejected = "Сообщение не прочитано";
		const string sFull = "Нет места для новых сообщений";
		static readonly string[] arTextRes = new[]
			{
				null, sDelivered, sAccepted, sRejected, sFull
			};

		#endregion Fields

		#region Properties

		private CmdType LinkType
		{
			get { return linkType; }
			set { linkType = value; }
		}

		#endregion Properties

		#region IDevice Members

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;
			if (!Initialized)
				throw new ApplicationException("Device not initialized");

			// list of wrapped responses
			ArrayList alRes = new ArrayList(1);
			TreckerAF379Datagram datagram = new TreckerAF379Datagram(data);

			if (datagram.IsValid)
			{
				MobilUnit.Unit.MobilUnit mu = UnitReceived();
				mu.cmdType = CmdType.Trace;
				mu.DeviceID = datagram.IMEI;
				//mu.ID = int.Parse(datagram.ID);
				mu.Latitude = datagram.Lat;
				mu.Longitude = datagram.Lng;
				mu.Speed = datagram.Speed;
				mu.Time = datagram.Time;
				mu.Satellites = 10;
				mu.CorrectGPS = true;
				mu.Height = 0;

				alRes.Add(mu);
			}

			if (alRes.Count > 0)
				return alRes;


			return alRes;
		}

		public override bool SupportData(byte[] data)
		{
			if (data == null) return false;

			string strVal = Encoding.ASCII.GetString(data);
			string[] parts = strVal.Split(',');
			if (parts.Length < 8 || !strVal.StartsWith("#") || !strVal.EndsWith("#"))
			{
				return false;
			}

			return 16 <= parts[0].Length && parts[0].All(c => '0' <= c  && c <= '9');
		}

		#endregion
	}
}