﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Globalization;
using FORIS.TSS.MobilUnit.Unit;

namespace FORIS.TSS.Terminal.AvtoGRAF
{
    public struct RecParts
    {
        public RecParts(string recString)
        {
            d1 = ConvertHexStringToUInt32(recString.Substring(0, 8));
            d2 = ConvertHexStringToUInt32(recString.Substring(8, 8));
            d3 = ConvertHexStringToUInt32(recString.Substring(16, 8));
            Bytes = recString;
        }

        internal static UInt32 ConvertHexStringToUInt32(string hexString)
        {
            UInt32 res = 0;
            res = BitConverter.ToUInt32(new byte[] { byte.Parse(hexString.Substring(0, 2), NumberStyles.HexNumber), byte.Parse(hexString.Substring(2, 2), NumberStyles.HexNumber), byte.Parse(hexString.Substring(4, 2), NumberStyles.HexNumber), byte.Parse(hexString.Substring(6, 2), NumberStyles.HexNumber) }, 0);
            return res;
        }

        public UInt32 d1;
        public UInt32 d2;
        public UInt32 d3;
        public string Bytes;
    }

    internal class RecFactory
    {
        protected static int GetRecType(RecParts recParts)
        {
            int recType = 0;
            if ((recParts.d1 & (1 << 27)) != 0x08000000)
            {
                recType = (int)(recParts.d2 & 0xFF);
            }

            return recType;
        }

        public static Rec GetRec(string recString)
        {
            Rec rec = null;
            try
            {
                RecParts recParts = new RecParts(recString);
                int recType = GetRecType(recParts);

                switch (recType)
                {
                    case (int)RecTypes.Analogue:
                        rec = new RecAnalogue(recParts, recType);
                        break;
                    case (int)RecTypes.RecPosition:
                        rec = new RecPosition(recParts, recType);
                        break;
                    case (int)RecTypes.RecMotion:
                        rec = new RecMotion(recParts, recType);
                        break;
                    case (int)RecTypes.RecDigSensors1_2:
                        rec = new RecDigSensors1_2(recParts, recType);
                        break;
                    case (int)RecTypes.CAN1:
                        rec = new CAN1(recParts, recType);
                        break;
                    case (int)RecTypes.CAN2:
                        rec = new CAN2(recParts, recType);
                        break;
                    case (int)RecTypes.CAN3:
                        rec = new CAN3(recParts, recType);
                        break;
                    case (int)RecTypes.CAN4:
                        rec = new CAN4(recParts, recType);
                        break;
                    case (int)RecTypes.CAN5:
                        rec = new CAN5(recParts, recType);
                        break;
                    default:
                        if (recType >= (int)RecTypes.CAN6_start && recType <= (int)RecTypes.CAN6_end)
                        {
                            rec = new CAN6(recParts, recType);
                        }
                        else
                        {
                            rec = new Rec(recParts, recType);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(
                    "AvtoGraf16Datagram GetRec Error parsing record: \r\n {0} \r\n Error: \r\n{1}",
                    recString, ex);
                rec = null;
                //throw;
            }

            return rec;
        }
    }

    public class CAN1 : Rec
    {
        public byte Speed;
        public bool CruiseControl;
        public bool Brake;
        public bool ParkingBrake;
        public bool Clutch;
        public byte Accelerator;
        public int FuelRate;

        public CAN1(RecParts recParts, int recType)
            : base(recParts, recType)
        {
            ParseSpeed();
            ParseBoolValues();
            ParseАccelerator();
            ParseFuelRate();
        }

        protected void ParseSpeed()
        {
            Speed = (byte)((_recParts.d2 >> 8) & 0xFF);
            if (Speed == 255)
            {
                Speed = 0;
            }
        }

        protected void ParseBoolValues()
        {
            CruiseControl = (_recParts.d2 & (1 << 17)) > 0;
            Brake = (_recParts.d2 & (1 << 19)) > 0;
            Clutch = (_recParts.d2 & (1 << 21)) > 0;
            ParkingBrake = (_recParts.d2 & (1 << 23)) > 0;
        }

        protected void ParseАccelerator()
        {
            //(0.4% хода педали /1)
            Accelerator = (byte)((_recParts.d2 >> 24) * 0.4);
            if (Accelerator > 100)
            {
                Accelerator = 0;
            }
        }

        protected void ParseFuelRate()
        { 
            //показания приходят в (0.5 литров /1), а храним в десятых долях литра = X * 0.5 * 10 = X * 5
            FuelRate = (int) ((_recParts.d3 >> 8) * 5) ;
        }
    }

    public class CAN2 : Rec
    {
        public ushort FuelLevel1;
        public ushort FuelLevel2;
        public ushort FuelLevel3;
        public ushort FuelLevel4;
        public ushort FuelLevel5;
        public ushort FuelLevel6;

        public CAN2(RecParts recParts, int recType)
            : base(recParts, recType)
        {
            Parse();
        }

        protected void Parse()
        {
            FuelLevel1 = ParseFuelLevel(_recParts.d2, 8);
            FuelLevel2 = ParseFuelLevel(_recParts.d2, 16);
            FuelLevel3 = ParseFuelLevel(_recParts.d2, 24);

            FuelLevel4 = ParseFuelLevel(_recParts.d3, 8);
            FuelLevel5 = ParseFuelLevel(_recParts.d3, 16);
            FuelLevel6 = ParseFuelLevel(_recParts.d3, 24);
        }

        protected ushort ParseFuelLevel(uint part, int startBit)
        {
            //все показания уровня топлива приходят как 0.4% на бит, а храним десятые доли процента
            ushort res = (ushort)(((part >> startBit) & 0xFF) * 4);
            if (res > 1000) { res = 0; }
            return res;
        }
    }

    public class CAN3 : Rec
    {
        public byte Revs;
        public ushort RunToCarMaintenance;
        public int EngHours;

        public CAN3(RecParts recParts, int recType)
            : base(recParts, recType)
        {
            /// Обороты двигателя (32 об/мин /1), а храним в 100об/мин
            Revs = (byte)((_recParts.d2 >> 8) & 0xFF);
            Revs = (byte)(Revs == 255 ? 0 : (Revs * 32 / 100));
            /// Пробег до ТО (5км /1, сдвиг 160635км), а храним в 10км/ 1
            RunToCarMaintenance = (ushort)(((_recParts.d2 >> 16) * 5 - 160635) / 10);
            /// Моточасы (0.05 ч. /1 ), а храним в часах
            EngHours = (int)((_recParts.d3 >> 8) * 0.05);
        }


    }

    public class CAN4 : Rec
    {
        public short CoolantT { get; set; }
        public short EngOilT { get; set; }
        public short FuelT { get; set; }

        public CAN4(RecParts recParts, int recType)
            : base(recParts, recType)
        {
            /// Температура охлаждающей жидкости 1 градус /1 (сдвиг -40 градусов)
            CoolantT = (short)(((_recParts.d2 >> 8) & 0xFF) - 40);
            if (CoolantT == 215)
            {
                CoolantT = 0;
            }
            /// Температура масла в двигателе 0.03125 градус /1 (сдвиг -272 градусов)
            EngOilT = (short)(((_recParts.d2 >> 16) * 0.03125) - 272);
            /// Температура топлива 1 градус /1 (сдвиг -40 градусов)
            FuelT = (short)(((_recParts.d3 >> 8) & 0xFF) - 40);
            if (FuelT == 215)
            {
                FuelT = 0;
            }
        }
    }

    public class CAN5 : Rec
    {
        public int TotalRun;
        public ushort? DayRun;

        public CAN5(RecParts recParts, int recType)
            : base(recParts, recType)
        {
            //Общий пробег (5м /1), храним в км
            TotalRun = (int)(((_recParts.d2 >> 8) + (((_recParts.d3 >> 8) & 0xFF) << 24)) * 5 / 1000);
            
            // Суточный пробег (40м /1), храним в км
            var dayRunRawValue = _recParts.d2 >> 16;
            if (dayRunRawValue != 0xFFFF)
                DayRun = (ushort)(dayRunRawValue * 40 / 1000);
        }
    }

    public class CAN6 : Rec
    {
        public int AxleLoad;
        public byte AxleNumber;

        public CAN6(RecParts recParts, int recType)
            : base(recParts, recType)
        {
            //Суммируем нагрузку на каждое колесо и умножаем на (128кг на бит).
            var wheelLoads = GetWheelLoads();

            AxleLoad = wheelLoads.Sum(w => (int)w);

            AxleNumber = (byte)((recType & 0xF) + 1);
        }

        protected ushort ParseOneWheelLoad(uint part, int startBit)
        {
            //все показания уровня топлива приходят как (128кг на бит), а храним кг
            var rawValue = ((part >> startBit) & 0xFF);

            if (rawValue == 0xFF)
                return 0;

            return (ushort)(rawValue * 128);
        }

        private IEnumerable<ushort> GetWheelLoads()
        {
            yield return ParseOneWheelLoad(_recParts.d2, 8);
            yield return ParseOneWheelLoad(_recParts.d2, 16);
            yield return ParseOneWheelLoad(_recParts.d2, 24);
            yield return ParseOneWheelLoad(_recParts.d3, 8);
            yield return ParseOneWheelLoad(_recParts.d3, 16);
            yield return ParseOneWheelLoad(_recParts.d3, 24);
        }

    }

    public class RecMotion : Rec
    {
        public Int16 Speed;
        public Int16 Course;
        public bool Input3;
        public bool Input4;
        public bool Input5;
        public bool Input6;

        /// <summary>
        /// 1 marine knot in km/h
        /// </summary>
        const double MarineKnot = 1.852;	// 50. / 27 = 1.(851)

        public RecMotion(RecParts recParts, int recType)
            : base(recParts, recType)
        {
            ParseSpeed();
            ParseCourse();
            ParseInputs();
        }

        public override string ToString()
        {
            return base.ToString() + string.Format(", speed={0}, course={1}, input3={2}, input4={3}, input5={4}, input6={5}", Speed, Course, Input3, Input4, Input5, Input6);
        }

        protected void ParseSpeed()
        {
            Speed = (Int16)(((_recParts.d2 >> 8) & 0x3FFF) / 10 * MarineKnot);
            if (Speed < 3)
            {
                Speed = 0;
            }
        }

        protected void ParseCourse()
        {
            Course = (Int16)(_recParts.d2 >> 23);
        }

        protected void ParseInputs()
        {
            Input3 = (_recParts.d1 & (1 << 31)) > 0;
            Input4 = (_recParts.d1 & (1 << 30)) > 0;
            Input5 = (_recParts.d1 & (1 << 29)) > 0;
            Input6 = (_recParts.d1 & (1 << 28)) > 0;
        }
    }

    public class RecAnalogue : Rec
    {
        public RecAnalogue(RecParts recParts, int recType)
            : base(recParts, recType)
        {
            ParseSensor1();
            ParseSensor2();
            ParsePowerVoltage();
            ParseBackupBattery();
        }

        public override string ToString()
        {
            return base.ToString() + ", Sensor1=" + Sensor1.ToString() + ", Sensor2=" + Sensor2.ToString();
        }

        /// <summary>
        /// Аналоговый вход 1, 0.0098В на бит
        /// </summary>
        public Int32 Sensor1;

        /// <summary>
        /// Аналоговый вход 2, 0.0235В на бит
        /// </summary>
        public Int32 Sensor2;

        /// <summary>
        /// Напряжение питания, ~0.017В на бит
        /// </summary>
        public Int32 PowerVoltage;

        /// <summary>
        /// Резервный АКБ, ~0.07В на бит
        /// </summary>
        public Int32 BackupBattery;

        protected void ParseSensor1()
        {
            Sensor1 = (int) ((_recParts.d2 >> 8) & 0x3f);
        }

        protected void ParseSensor2()
        {
            Sensor2 = (int)((_recParts.d3 >> 8) & 0x3f);
        }

        private void ParsePowerVoltage()
        {
            PowerVoltage = (int)((_recParts.d2 >> 18) & 0x3f);
        }

        private void ParseBackupBattery()
        {
            BackupBattery = (int)((_recParts.d3) & 0xf);
        }
    }

    public class RecDigSensors1_2 : Rec
    {
        public RecDigSensors1_2(RecParts recParts, int recType)
            : base(recParts, recType)
        {
            ParseSensor1();
            ParseSensor2();
        }

        public override string ToString()
        {
            return base.ToString() + ", Sensor1=" + Sensor1.ToString() + ", Sensor2=" + Sensor2.ToString();
        }

        public Int32 Sensor1;
        public Int32 Sensor2;

        protected void ParseSensor1()
        {
            Sensor1 = (Int32)(_recParts.d2 >> 8);
        }

        protected void ParseSensor2()
        {
            Sensor2 = (Int32)(_recParts.d3 >> 8);
        }
    }

    public class RecPosition : Rec
    {
        public double Lat;
        public double Lng;
        public bool Input1;
        public bool Input2;
        public bool GPSCorrect;

        protected double ParseCoord(UInt32 value)
        {
            double res = 0.0;
            //градусы
            res = Math.Floor((double)(value / 10000000));
            //минуты
            res += (double)((value % 10000000) * 100) / 60 / 10000000; //Math.Floor((double)((value % 10000000) / 100000)) / 60;
            //доли минут
            //res += Math.Floor((double)((value % 100000))) / 60 / 1000000;
            return res;
        }

        protected void ParseLat()
        {
            Lat = ParseCoord(_recParts.d2); //Math.Floor((double)(_recParts.d2 / 10000000)) + (double)((_recParts.d2 % 10000000) * 100) / 60 / 10000000;
        }
        protected void ParseLng()
        {
            Lng = ParseCoord(_recParts.d3); //Math.Floor((double)(_recParts.d3 / 10000000)) + (double)((_recParts.d3 % 10000000) * 100) / 60 / 10000000;
        }

        protected void ParseGPSCorrect()
        {
            GPSCorrect = (_recParts.d1 & (1<<27))==0x08000000 && (_recParts.d1 & (1<<30))==0x40000000;
        }

        protected void ParseInputs()
        {
            Input1 = ((_recParts.d1 & (1<<29))!=0);
            Input2 = ((_recParts.d1 & (1 << 28)) == 0x10000000);
        }

        public RecPosition(RecParts recParts, int recType)
            : base(recParts, recType)
        {
            ParseLat();
            ParseLng();
            ParseGPSCorrect();
            ParseInputs();
        }

        public override string ToString()
        {
            return base.ToString() + ", lat=" + Lat.ToString() + ", lng=" + Lng.ToString();
        }
    }

    public class Rec
    {
        protected void ParseTime()
        {
            int sec = (int)(_recParts.d1 & 0x1FFFF);
            int month = (int)((_recParts.d1 >> 17) & 0xF);
            int day = (int)((_recParts.d1 >> 21) & 0x1F);
            Time = new DateTime(DateTime.Now.Year, month, day).AddSeconds(sec);

            if (Time > DateTime.Now.AddMonths(1)
                || Time < DateTime.Now.AddMonths(-6))
            {
                throw new ArgumentOutOfRangeException(string.Format("Avtograf Rec.ParseTime Error invalid record time: {0} \r\nRecord: {1}", Time, _recParts.Bytes));
            }
        }

        public override string ToString()
        {
            return "Дата:" + Time.ToString() + ", Тип:" + RecType.ToString();
        }

        public Rec(RecParts recParts, int recType)
        {
            _recParts = recParts;
            RecType = recType;
            ParseTime();
        }

        public DateTime Time;
        public int RecType;

        protected RecParts _recParts;


    }

    public class TracePoint
    {
        public DateTime Time;
        public double Lat;
        public double Lng;
        public Int16 Speed;
        public Int16 Course;
        public bool GPSCorrect;
        public bool DigitalSensor1;
        public bool DigitalSensor2;
        public bool DigitalSensor3;
        public bool DigitalSensor4;
        public bool DigitalSensor5;
        public bool DigitalSensor6;
        public int  DigitalSensor1Counter;
        public int DigitalSensor2Counter;
        public int DigitalSensor3Counter;
        public int DigitalSensor4Counter;
        public CANInfo CAN;

        /// <summary>
        /// Аналоговый вход 1, мВ
        /// </summary>
        public int? Analogue1 { get; set; }
        /// <summary>
        /// Аналоговый вход 2, мВ
        /// </summary>
        public int? Analogue2 { get; set; }
        /// <summary>
        /// Напряжение питания, мВ
        /// </summary>
        public int? PowerVoltage { get; set; }
        /// <summary>
        /// Резервный АКБ, мВ
        /// </summary>
        public int? BackupBattery { get; set; }
    }

    public enum RecTypes: int
    {
        RecPosition = 0,
        Analogue = 1,
        RecDigSensors1_2 = 2,
        RecMotion = 4,
        CAN1 = 10,
        CAN2 = 11,
        CAN3 = 12,
        CAN4 = 13,
        CAN5 = 14,
        CAN6_start = 16,
        CAN6_end = 31
    }

    public class AvtoGRAFDatagram
    {
        protected List<Rec> _records = new List<Rec>();
        protected List<TracePoint> _tracePoints = new List<TracePoint>();
        protected string _id;
        protected RecOrderType sensors1_2RecOrder = RecOrderType.undefined;
        protected RecOrderType CANRecOrder = RecOrderType.undefined;


        protected enum RecOrderType
        { 
            undefined = 0,
            before = 1,
            after = 2
        }

        public List<Rec> Records
        { get { return _records; } }

        public List<TracePoint> TracePoints
        { get { return _tracePoints; } }

        public string ID
        { get { return _id; } }
        
        public AvtoGRAFDatagram(byte[] bytes)
        {
            string dataString = Encoding.ASCII.GetString(bytes);
            if (!dataString.StartsWith("##") || !dataString.Contains(":") || !dataString.Contains("@"))
            {
                return;
            }

            string[] parts = dataString.Split(new char[] {':'});
            string recordsString = parts[parts.Length - 1];
            _id = dataString.Split(new char[] { '@' })[0].Substring(2);
            _records = new List<Rec>();
            for (int i = 0; i < recordsString.Length-24; i += 24)
            {
                string recString = recordsString.Substring(i, 24);
                Rec rec = RecFactory.GetRec(recString);
                if (rec != null)
                {
                    _records.Add(rec);

                    //определяем идет ли запись с показаниями датчиков 1 и 2 перед записью с позициями или после
                    if (sensors1_2RecOrder == RecOrderType.undefined)
                    {
                        if (rec.RecType == (int) RecTypes.RecPosition)
                        {
                            sensors1_2RecOrder = RecOrderType.after;
                        }
                        if (rec.RecType == (int) RecTypes.RecDigSensors1_2)
                        {
                            sensors1_2RecOrder = RecOrderType.before;
                        }
                    }

                    //определяем идет ли запись с данными с CAN перед записью с позициями или после
                    if (CANRecOrder == RecOrderType.undefined)
                    {
                        if (rec.RecType == (int) RecTypes.RecPosition)
                        {
                            CANRecOrder = RecOrderType.after;
                        }
                        if (isCANRecord(rec))
                        {
                            CANRecOrder = RecOrderType.before;
                        }
                    }
                }
            }

            FillTracePoints();
        }

        protected bool isCANRecord(Rec rec)
        {
            return (rec.RecType >= (int)RecTypes.CAN1 && rec.RecType <= (int)RecTypes.CAN6_end && rec.RecType != 15);
        }

        public static UInt32 ConvertHexStringToUInt32(string hexString)
        {
            UInt32 res = 0;
            res = BitConverter.ToUInt32(new byte[] { byte.Parse(hexString.Substring(0, 2), NumberStyles.HexNumber), byte.Parse(hexString.Substring(2, 2), NumberStyles.HexNumber), byte.Parse(hexString.Substring(4, 2), NumberStyles.HexNumber), byte.Parse(hexString.Substring(6, 2), NumberStyles.HexNumber) }, 0);
            return res;
        }

        public static int GetPacketLenght(byte[] bytes, int startIndex)
        {
            if (bytes.Length < startIndex + 26)
            {
                return -1;
            }

            var dataString = Encoding.ASCII.GetString(bytes);

            var length = (int)ConvertHexStringToUInt32(dataString.Substring(startIndex + 17, 8));

            return length*2 + 26;
        }

        protected void FillTracePoints()
        {
            var times = _records.Select(r => r.Time);

            _tracePoints = new List<TracePoint>();
            int index = 0;
            foreach (var time in times)
            {
                var point = new TracePoint { Time = time };

                var posRec = FindPosRec(time);

                if (posRec != null)
                {
                    point.Lat = posRec.Lat;
                    point.Lng = posRec.Lng;
                    point.Time = posRec.Time;
                    point.GPSCorrect = posRec.GPSCorrect;
                    point.DigitalSensor1 = posRec.Input1;
                    point.DigitalSensor2 = posRec.Input2;
                }
                
                var motionRec = FindMotionRec(time) as RecMotion;
                if (motionRec != null)
                {
                    point.Speed = motionRec.Speed;
                    point.Course = motionRec.Course;
                    point.DigitalSensor3 = motionRec.Input3;
                    point.DigitalSensor4 = motionRec.Input4;
                    point.DigitalSensor5 = motionRec.Input5;
                    point.DigitalSensor6 = motionRec.Input6;
                }

                var analogueRec = FindAnalogueRec(time);
                if (analogueRec != null)
                {
                    point.Analogue1 = (int) ((analogueRec.Sensor1 * 0.0098m) * 1000m);
                    point.Analogue2 = (int)((analogueRec.Sensor2 * 0.0098m) * 1000m);
                    point.PowerVoltage = (int)((analogueRec.PowerVoltage * 0.017m) * 1000m);
                    point.BackupBattery = (int)((analogueRec.BackupBattery * 0.07m) * 1000m);
                }

                var digSensors12 = FindDigSensors1_2Rec(time, index) as RecDigSensors1_2;
                if (digSensors12 != null)
                {
                    point.DigitalSensor1Counter = digSensors12.Sensor1;
                    point.DigitalSensor2Counter = digSensors12.Sensor2;
                }

                point.CAN = GetCANInfo(time, index);

                _tracePoints.Add(point);
                index++;
            }
        }

        private RecAnalogue FindAnalogueRec(DateTime time)
        {
            return FindRec(time, RecTypes.Analogue) as RecAnalogue;
        }


        protected CANInfo GetCANInfo(DateTime positionTime, int posIndex)
        {
            CANInfo res = null;
            //Ищем записи с показаниями CAN, 
            //но чтобы между ними не было других записей позиций 
            //(расчитываем, на то что все записи отсортированы по времени)
            int i = posIndex + ((CANRecOrder == RecOrderType.after) ? 1 : -1);

            while (i >= 0 && i < _records.Count)
            {
                Rec rec = _records[i];

                //Если есть запись после обрабатываемой в данный момент, 
                //то запись с датчиками будет принадлежать ей, а не текущей
                if (rec.RecType == (int)RecTypes.RecPosition)
                {
                    return res;
                }

                if (isCANRecord(rec))
                {
                    if (res == null)
                    {
                        res = new CANInfo();
                        res.Time = TSS.BusinessLogic.TimeHelper.GetSecondsFromBase(rec.Time);
                    }

                    switch (rec.RecType)
                    {
                        case (int)RecTypes.CAN1:
                            CAN1 c = (CAN1)rec;
                            res.Brake= c.Brake;
                            res.Clutch = c.Clutch;
                            res.CruiseControl = c.CruiseControl;
                            res.FuelRate = c.FuelRate;
                            res.ParkingBrake = c.ParkingBrake;
                            res.Speed = c.Speed;
                            res.Accelerator = c.Accelerator;
                            break;
                        case (int)RecTypes.CAN2:
                            CAN2 c2 = (CAN2)rec;
                            res.FuelLevel1 = c2.FuelLevel1;
                            res.FuelLevel2 = c2.FuelLevel2;
                            res.FuelLevel3 = c2.FuelLevel3;
                            res.FuelLevel4 = c2.FuelLevel4;
                            res.FuelLevel5 = c2.FuelLevel5;
                            res.FuelLevel6 = c2.FuelLevel6;
                            break;
                        case (int)RecTypes.CAN3:
                            CAN3 c3 = (CAN3)rec;
                            res.EngHours= c3.EngHours;
                            res.Revs= c3.Revs;
                            res.RunToCarMaintenance = c3.RunToCarMaintenance;
                            break;
                        case (int)RecTypes.CAN4:
                            CAN4 c4 = (CAN4)rec;
                            res.CoolantT = c4.CoolantT;
                            res.EngOilT = c4.EngOilT;
                            res.FuelT = c4.FuelT;
                            break;
                        case (int)RecTypes.CAN5:
                            CAN5 c5 = (CAN5)rec;
                            res.DayRun= c5.DayRun;
                            res.TotalRun = c5.TotalRun;
                            break;
                        default:
                            if (rec.RecType >= (int)RecTypes.CAN6_start && rec.RecType <= (int)RecTypes.CAN6_end)
                            {
                                CAN6 c6 = (CAN6)rec;
                                switch (c6.AxleNumber)
                                {
                                    case 1:
                                        res.AxleLoad1 = c6.AxleLoad;
                                        break;
                                    case 2:
                                        res.AxleLoad2 = c6.AxleLoad;
                                        break;
                                    case 3:
                                        res.AxleLoad3 = c6.AxleLoad;
                                        break;
                                    case 4:
                                        res.AxleLoad4 = c6.AxleLoad;
                                        break;
                                    case 5:
                                        res.AxleLoad5 = c6.AxleLoad;
                                        break;
                                    case 6:
                                        res.AxleLoad6 = c6.AxleLoad;
                                        break;
                                }
                            }
                            break;
                    }
                    
                }

                i = i + ((CANRecOrder == RecOrderType.after) ? 1 : -1);
            }

            return res;
        }

        protected Rec FindDigSensors1_2Rec(DateTime positionTime, int posIndex)
        {
            //Ищем первую запись с показаниями датчиков перед/после записи позиции, 
            //но чтобы между ними не было других записей позиций 
            //(расчитываем, на то что все записи отсортированы по времени)
            int i = posIndex + ((sensors1_2RecOrder == RecOrderType.after)?1:-1);

            while (i >= 0 && i < _records.Count)
            {
                Rec rec = _records[i];

                //Если есть запись после обрабатываемой в данный момент, 
                //то запись с датчиками будет принадлежать ей, а не текущей
                if (rec.RecType == (int)RecTypes.RecPosition)
                {
                    return null;
                }

                if (rec.RecType == (int)RecTypes.RecDigSensors1_2)
                {
                    return rec;
                }

                i = i + ((sensors1_2RecOrder == RecOrderType.after) ? 1 : -1);
            }
            return null;
        }
        


        protected RecPosition FindPosRec(DateTime positionTime)
        {
            return FindRec(positionTime, RecTypes.RecPosition) as RecPosition;
        }

        private Rec FindRec(DateTime positionTime, RecTypes recType)
        {
            return
                _records.FirstOrDefault(
                    rec =>
                    rec.RecType == (int) recType &&
                    (rec.Time == positionTime || (rec.Time > positionTime && (rec.Time - positionTime).TotalSeconds < 2)));
        }

        protected Rec FindMotionRec(DateTime positionTime)
        {
            foreach (Rec rec in _records)
            {
                if (rec.RecType == (int)RecTypes.RecMotion
                    && (
                        rec.Time == positionTime || 
                        (rec.Time > positionTime && (rec.Time - positionTime).TotalSeconds < 2))
                    )
                {
                    return rec;
                }
            }
            return null;
        }
    }
}
