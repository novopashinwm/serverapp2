﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Communication;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.AvtoGRAF
{
    public class AvtoGRAF : Device
    {
        public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
        {
            bufferRest = null;

            // list of wrapped responses
            ArrayList alRes = new ArrayList();

            try
            {
                //Разбиваем на пакеты
                var packets = Datagram.SplitDataPackets(data, AvtoGRAFDatagram.GetPacketLenght, out bufferRest);

                if (packets != null)
                {
                    foreach (var packet in packets)
                    {
                        AvtoGRAFDatagram datagram = new AvtoGRAFDatagram(packet);
                        //if (datagram.TracePoints.Count == 0)
                        //    return null;

                        foreach (TracePoint point in datagram.TracePoints)
                        {
                            MobilUnit.Unit.MobilUnit mu = UnitReceived();
                            mu.cmdType = CmdType.Trace;
                            mu.DeviceID = Encoding.ASCII.GetString(Encoding.ASCII.GetBytes(datagram.ID));
                            mu.Latitude = point.Lat;
                            mu.Longitude = point.Lng;
                            mu.Speed = point.Speed;
                            mu.Time = TimeHelper.GetSecondsFromBase(point.Time);
                            mu.Satellites = point.GPSCorrect ? 10 : 0;
                            mu.CorrectGPS = point.GPSCorrect;
                            //mu.ID = int.Parse(datagram.ID);


                            //=========Датчики=============

                            if (point.Analogue1 != null)
                                AddSensor(mu, AvtoGrafSensors.AN1, point.Analogue1.Value);
                            if (point.Analogue2 != null)
                                AddSensor(mu, AvtoGrafSensors.AN2, point.Analogue2.Value);
                            if (point.PowerVoltage != null)
                                AddSensor(mu, AvtoGrafSensors.PowerVoltage, point.PowerVoltage.Value);
                            if (point.BackupBattery != null)
                                AddSensor(mu, AvtoGrafSensors.BackupBattery, point.BackupBattery.Value);
                            
                            //-----------Температура
                            if (point.DigitalSensor2Counter > 0)
                            {
                                int T = (int)((point.DigitalSensor2Counter / 2) - 60);
                                mu.VoltageAN1 = T;
                            }


                            //IMessage msg = MessageCreator.CreatePosition(mu, mu.ValidPosition);
                            //msg.Num = PacketID;
                            alRes.Add(mu);
                        }

                        //Добавляем подтверждение приема пакета
                        alRes.Add(new ConfirmPacket(Encoding.ASCII.GetBytes("DATA ACCEPT\r\n")));
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.TraceInformation("AvtoGraf OnData \r\n {0} \r\n Error: \r\n{1}", BitConverter.ToString(data), ex);
                throw;

            }

            return alRes;
        }

        private static void AddSensor(MobilUnit.Unit.MobilUnit mu, AvtoGrafSensors sensor, int value)
        {
            if (mu.SensorValues == null)
                mu.SensorValues = new Dictionary<int, long>();

            mu.SensorValues.Add((int) sensor, value);
        }

        public override bool SupportData(byte[] data)
        {
            string dataString = Encoding.ASCII.GetString(data);
            if (dataString.StartsWith("##") && dataString.IndexOf("@") == 7 && dataString.IndexOf("L") == 16 && dataString.Contains(":"))
            {
                //Между ## и @ серийный номер прибора - 5 цифр
                int n;
                if (int.TryParse(dataString.Substring(2, 5), out n))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
