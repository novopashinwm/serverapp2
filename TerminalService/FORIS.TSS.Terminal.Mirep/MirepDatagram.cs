﻿using System;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Mirep
{
	public class MirepDatagram
	{
		public string IMEI;
		public DateTime? Date;
		public TimeSpan? Time;
		public decimal? Latitude;
		public decimal? Longitude;
		public int? Satellites;
		public int? Speed;
		public int? Direction;
		public decimal? Height;
		public decimal? RunFromLastPoint;
		public decimal? CommonRun;
		public int? DigitalSensors;
		public decimal? ADC1;
		public decimal? ADC2;
		public decimal? ADC3;
		public long? ImpulseCounter;
		public decimal? Omnicom;
		public bool? SatellitesLessThen4;
		public bool? Moving;
		public bool? ExternalPower;
		public bool? SimCardPresent;

		public IMobilUnit ToMobilUnit()
		{
			DateTime dateTime = Date == null
				? (Time == null
					? DateTime.UtcNow
					: DateTime.UtcNow.Date.Add(Time.Value))
				: Date.Value.Add(Time ?? TimeSpan.Zero);

			var mu = new MobilUnit.Unit.MobilUnit
			{
				DeviceID   = IMEI,
				Time       = TimeHelper.GetSecondsFromBase(dateTime),
				Latitude   = (double)(Latitude ?? (decimal)MobilUnit.Unit.MobilUnit.InvalidLatitude),
				Longitude  = (double)(Longitude ?? (decimal)MobilUnit.Unit.MobilUnit.InvalidLongitude),
				Satellites = Satellites ?? ((SatellitesLessThen4 ?? false) ? 4 : 0),
				Speed      = Speed ?? 0,
				Course     = Direction ?? 0,
				Height     = (int)(Height ?? 0),
				Run        = (int)(CommonRun ?? 0),
				VoltageAN1 = (int)((ADC1 ?? 0) * 1000),
				//Известно, что к 2 аналоговому входу подключен датчик топлива,
				//поэтому ADC2->AN4, ImpulseCounter -> AN2
				//TODO: исправить, когда будет реализован функционал маппинга датчиков
				VoltageAN4 = (int)((ADC2 ?? 0) * 1000),
				VoltageAN3 = (int)((ADC3 ?? 0) * 1000),
				VoltageAN2 = (int)(ImpulseCounter ?? 0),
			};
			mu.Properties.Add(DeviceProperty.Protocol, Mirep.ProtocolName);
			return mu;
		}
	}
}