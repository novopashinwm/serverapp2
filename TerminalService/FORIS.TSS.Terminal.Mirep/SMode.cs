﻿using System;

namespace FORIS.TSS.Terminal.Mirep
{
	[Flags]
	enum SMode
	{
		/// <summary> 0-й бит (дата) </summary>
		Date                 = 0x00001,
		/// <summary> 1-й бит (время) </summary>
		Time                 = 0x00002,
		/// <summary> 2-й бит (координаты) </summary>
		Coordinates          = 0x00004,
		/// <summary> 3-й бит (координатные индикаторы) </summary>
		CoordinateIndicators = 0x00008,
		/// <summary> 4-й бит (количество видимых спутников) </summary>
		Satellites           = 0x00010,
		/// <summary> 5-й бит (мгновенная скорость) </summary>
		Speed                = 0x00020,
		/// <summary> 6-й бит (направление) </summary>
		Direction            = 0x00040,
		/// <summary> 7-й бит (высота) </summary>
		Height               = 0x00080,
		/// <summary> 8-й бит (пробег от последней точки) </summary>
		RunFromLastPoint     = 0x00100,
		/// <summary> 9-й бит (общий пробег) </summary>
		CommonRun            = 0x00200,
		/// <summary> 10-й бит (состояние цифровых датчиков) </summary>
		DigitalSensors       = 0x00400,
		/// <summary> 11-й бит (ADC1) </summary>
		ADC1                 = 0x00800,
		/// <summary> 12-й бит (состояние ТС состоит из 2 частей 1 часть определяет когда ТС: значение - 1 движется, значение - 2 неподвижно или значение - 3 количество видимых спутников ниже 4 вторая часть определяет дополнительное состояние устройства (битовая маска) бит 0 поднят - отключено внешнее питание, бит 4 поднят - отсутствует Сим-карта ) </summary>
		State                = 0x01000,
		/// <summary> 13-й бит (ADC2) </summary>
		ADC2                 = 0x02000,
		/// <summary> 14-й бит (ADC3) </summary>
		ADC3                 = 0x04000,
		/// <summary> 15-й бит (счетчик импульсов) </summary>
		ImpulseCounter       = 0x08000,
		/// <summary> 16-й бит (значение датчика Omnicom) </summary>
		Omnicom              = 0x10000,
	}
}