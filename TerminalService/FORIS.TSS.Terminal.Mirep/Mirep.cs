﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

/*
 в начале пакета идет smode и IMEI.
дальше пакет формируется по значению 
smode: битовая маска, которая позволяет изменять формат пакета
0-й бит (дата)
1-й бит (время)
2-й бит (координаты)
3-й бит (координатные индикаторы)
4-й бит (количество видимых спутников)
5-й бит (мгновенная скорость)
6-й бит (направление)
7-й бит (высота)
8-й бит (пробег от последней точки)
9-й бит (общий пробег)
10-й бит (состояние цифровых датчиков)
11-й бит (ADC1)
12-й бит (состояние ТС состоит из 2 частей 1 часть определяет когда ТС: значение - 1 движется, значение - 2 неподвижно или значение - 3 количество видимых спутников ниже 4 вторая часть определяет дополнительное состояние устройства (битовая маска) бит 0 поднят - отключено внешнее питание, бит 4 поднят - отсутствует Сим-карта ) 
13-й бит (ADC2)
14-й бит (ADC3)
15-й бит (счетчик импульсов)
16-й бит (значение датчика Omnicom)
*/

namespace FORIS.TSS.Terminal.Mirep
{
	public class Mirep : Device
	{
		private static readonly MirepReader Reader = new MirepReader();
		private static readonly string[] MessageSplitParameter = new[] { "\r\n" };
		internal const string ProtocolName = "MiREP";
		internal static class DeviceNames
		{
			internal const string Mirep = "MiREP";
		}
		public Mirep() : base(null, new[]
		{
			DeviceNames.Mirep,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			//Ожидаем строку в начале которой идет десятичное число, 
			//при этом количество битов в двоичном представлении равно количеству знаков ";"
			//Также проверяем первые две цифры IMEI - код производителя
			var text = Encoding.ASCII.GetString(data);

			var lines = text.Split(MessageSplitParameter, StringSplitOptions.RemoveEmptyEntries);

			foreach (var line in lines)
			{
				var firstSemicolonIndex = line.IndexOf(';');
				if (firstSemicolonIndex == -1)
					return false;

				int smode;
				if (!int.TryParse(line.Substring(0, firstSemicolonIndex), out smode))
					return false;

				//Ожидается количество ';' столько, сколько данных согласно smode плюс 1 (разделитель smode и IMEI)
				var expectingSemicolonCount = 1;
				for (var i = 0x01; i != 0x20000; i <<= 1)
				{
					if ((smode & i) != 0)
						++expectingSemicolonCount;
				}

				/*  А можно реализовать подсчет включенных битов вот так (google for explanation):
				 *  int i = 0;
				 *  for (; a > 0; a &= (a - 1)) i++;
				 *  return i;
				 *  Количество итераций равно количеству единиц.
				 */


				var receivedSemicolonCount = 0;
				foreach (var c in line)
				{
					if (c == ';')
						++receivedSemicolonCount;
				}

				if (expectingSemicolonCount != receivedSemicolonCount)
					return false;

				//Проверка на код производителя (первые две цифры IMEI), см. http://ru.wikipedia.org/wiki/IMEI
				if (!(line[firstSemicolonIndex + 1] == '3' &&
					  line[firstSemicolonIndex + 2] == '5'))
					return false;
			}
			return true;
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			//Считаем невозможной ситуацию, когда пакеты приходят по частям
			bufferRest = null;

			var text = Encoding.ASCII.GetString(data);

			var messages = text.Split(MessageSplitParameter, StringSplitOptions.RemoveEmptyEntries);
			var result = new List<object>(messages.Length);

			foreach (var message in messages)
			{
				var datagram = Reader.Read(message);
				if (datagram != null)
					result.Add(datagram.ToMobilUnit());
			}
			return result;
		}
	}
}