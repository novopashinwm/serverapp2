﻿using System;
using System.Diagnostics;
using System.Globalization;

namespace FORIS.TSS.Terminal.Mirep
{
	public class MirepReader
	{
		private static readonly Func<string, MirepDatagram, bool>[] FieldReaders =
			new Func<string, MirepDatagram, bool>[]
				{
					//0-й бит (дата)
					ReadDate,
					//1-й бит (время)
					ReadTime,
					//2-й бит (координаты)
					ReadCoordinates,
					//3-й бит (координатные индикаторы)
					ReadCoordinateIndicators,
					//4-й бит (количество видимых спутников)
					ReadSatellites,
					//5-й бит (мгновенная скорость)
					ReadSpeed,
					//6-й бит (направление)
					ReadDirection,
					//7-й бит (высота)
					ReadHeight,
					//8-й бит (пробег от последней точки)
					ReadRunFromLastPoint,
					//9-й бит (общий пробег)
					ReadCommonRun,
					//10-й бит (состояние цифровых датчиков)
					ReadDigitalSensors,
					//11-й бит (ADC1)
					ReadADC1,
					//12-й бит (состояние ТС состоит из 2 частей 1 часть определяет когда ТС: значение - 1 движется, значение - 2 неподвижно или значение - 3 количество видимых спутников ниже 4 вторая часть определяет дополнительное состояние устройства (битовая маска) бит 0 поднят - отключено внешнее питание, бит 4 поднят - отсутствует Сим-карта ) 
					ReadState,
					//13-й бит (ADC2)
					ReadADC2,
					//14-й бит (ADC3)
					ReadADC3,
					//15-й бит (счетчик импульсов)
					ReadImpulseCounter,
					//16-й бит (значение датчика Omnicom) 
					ReadOmnicom,
				};
		
		/// <remarks>
		/// В этом методе сосредоточена логика перехода от одного поля к другому, 
		/// а также обработка ошибок при парсинге значений
		/// </remarks>
		public MirepDatagram Read(string inputString)
		{
			var parts = inputString.Split(';');
			var smode = (SMode)int.Parse(parts[0]);
			var datagram = new MirepDatagram {IMEI = parts[1]};

			for (int mask = 0x1, readerIndex = 0, partIndex = 1;
				 1 <= mask && mask <= 0x1ffff;
				 mask <<= 1, ++readerIndex)
			{
				if (((SMode) mask & smode) == 0)
					continue; //Поле отсутствует в битовой маске, пропускаем его
				++partIndex;

				var reader = FieldReaders[readerIndex];
				if (reader == null)
					continue; //Читатель поля отсутствует в битовой маске, пропускаем его

				if (!reader(parts[partIndex], datagram))
				{
					Trace.WriteLine("Mirep: unable to read field " + (SMode) mask + ", data: " + inputString);
					return null;
				}
			}
			return datagram;
		}
		private static bool ReadDate(string s, MirepDatagram datagram)
		{
			//0-й бит (дата)
			int shortYear, month, day;
			if (s.Length != 6 ||
				!int.TryParse(s.Substring(0, 2), out day) ||
				!int.TryParse(s.Substring(2, 2), out month) ||
				!int.TryParse(s.Substring(4, 2), out shortYear))
			{
				return false;
			}
			datagram.Date = new DateTime(2000 + shortYear, month, day, 0, 0, 0, DateTimeKind.Utc);
			return true;
		}
		private static bool ReadTime(string s, MirepDatagram datagram)
		{
			//1-й бит (время)

			int hours, minutes, seconds;
			if (s.Length != 6 ||
				!int.TryParse(s.Substring(0, 2), out hours) ||
				!int.TryParse(s.Substring(2, 2), out minutes) ||
				!int.TryParse(s.Substring(4, 2), out seconds))
			{
				return false;
			}
			datagram.Time = new TimeSpan(hours, minutes, seconds);
			
			return true;
		}
		private static bool ReadCoordinates(string s, MirepDatagram datagram)
		{
			//2-й бит (координаты)

			const int minutesPartLength = 8;

			var coordinateParts = s.Split('-');
			if (coordinateParts.Length != 2)
			{
				return false;
			}

			var latitudeString = coordinateParts[0];
			if (latitudeString.Length < 1 + minutesPartLength || 2 + minutesPartLength < latitudeString.Length)
			{
				return false;
			}

			var longitudeString = coordinateParts[1];
			if (longitudeString.Length < 1 + minutesPartLength || 3 + minutesPartLength < longitudeString.Length)
			{
				return false;
			}

			int latitudeDegrees, longitudeDegrees;
			decimal latitudeMinutes, longitudeMinutes;
			if (!int.TryParse(latitudeString.Substring(0, latitudeString.Length - 8), out latitudeDegrees) ||
				!int.TryParse(longitudeString.Substring(0, longitudeString.Length - 8), out longitudeDegrees) ||
				!TryParseAsDecimal(latitudeString.Substring(2, 8), out latitudeMinutes) ||
				!TryParseAsDecimal(longitudeString.Substring(2, 8), out longitudeMinutes))
			{
				return false;
			}

			datagram.Latitude = (latitudeDegrees + latitudeMinutes/60);
			datagram.Longitude = (longitudeDegrees + longitudeMinutes/60);

			return true;
		}
		//3-й бит (координатные индикаторы)
		private static bool ReadCoordinateIndicators(string s, MirepDatagram datagram)
		{
			var coordinateIndicators = s.Split('-');
			if (coordinateIndicators.Length != 2)
				return false;

			if (coordinateIndicators[0] != "N" && datagram.Latitude.HasValue)
				datagram.Latitude = -datagram.Latitude;

			if (coordinateIndicators[1] != "E" && datagram.Longitude.HasValue)
				datagram.Longitude = -datagram.Longitude;
			
			return true;
		}
		//4-й бит (количество видимых спутников)
		private static bool ReadSatellites(string satellitesString, MirepDatagram datagram)
		{
			int satellites;
			if (!int.TryParse(satellitesString, out satellites))
				return false;
			datagram.Satellites = satellites;
			return true;
		}
		//5-й бит (мгновенная скорость)
		private static bool ReadSpeed(string speedString, MirepDatagram datagram)
		{
			int speed;
			if (!int.TryParse(speedString, out speed))
				return false;
			datagram.Speed = speed;
			return true;
		}
		//6-й бит (направление)
		private static bool ReadDirection(string directionString, MirepDatagram datagram)
		{
			int direction;
			if (!int.TryParse(directionString, out direction))
				return false;
			datagram.Direction = direction;
			return true;        
		}
		//7-й бит (высота)
		private static bool ReadHeight(string heightString, MirepDatagram datagram)
		{
			decimal height;
			if (!TryParseAsDecimal(heightString, out height))
				return false;
			datagram.Height = height;
			return true;
		}
		//8-й бит (пробег от последней точки)
		private static bool ReadRunFromLastPoint(string s, MirepDatagram datagram)
		{
			decimal runFromLastPoint;
			if (!TryParseAsDecimal(s, out runFromLastPoint))
				return false;
			datagram.RunFromLastPoint = runFromLastPoint;
			return true;
		}
		//9-й бит (общий пробег)
		private static bool ReadCommonRun(string s, MirepDatagram datagram)
		{
			decimal commonRun;
			if (!TryParseAsDecimal(s, out commonRun))
				return false;
			datagram.CommonRun = commonRun;
			return true;
		}
		//10-й бит (состояние цифровых датчиков)
		private static bool ReadDigitalSensors(string s, MirepDatagram datagram)
		{
			int digitalSensors;
			if (!int.TryParse(s, out digitalSensors))
				return false;
			datagram.DigitalSensors = digitalSensors;
			return true;
		}
		//11-й бит (ADC1)
		private static bool ReadADC1(string s, MirepDatagram datagram)
		{
			decimal adc1;
			if (!TryParseAsDecimal(s, out adc1))
				return false;
			datagram.ADC1 = adc1;
			return true;
		}
		//12-й бит (состояние ТС состоит из 2 частей
		//1 часть определяет когда ТС: 
		//значение - 1 движется, 
		//значение - 2 неподвижно или 
		//значение - 3 количество видимых спутников ниже 4 
		//вторая часть определяет дополнительное состояние устройства (битовая маска) 
		//бит 0 поднят - отключено внешнее питание, 
		//бит 4 поднят - отсутствует Сим-карта ) 
		private static bool ReadState(string s, MirepDatagram datagram)
		{
			int value;
			if (!int.TryParse(s, out value))
				return false;
			datagram.SatellitesLessThen4 = (value & 0x3) == 0x3;
			datagram.Moving = (value & 0x3) == 0x1;
			datagram.ExternalPower = (value & 0x10) == 0;
			datagram.SimCardPresent = (value & 0x80) == 0;
			
			return true;
		}
		//13-й бит (ADC2)
		private static bool ReadADC2(string s, MirepDatagram datagram)
		{
			decimal adc2;
			if (!TryParseAsDecimal(s, out adc2))
				return false;
			datagram.ADC2 = adc2;
			return true;
		}
		//14-й бит (ADC3)
		private static bool ReadADC3(string s, MirepDatagram datagram)
		{
			decimal adc3;
			if (!TryParseAsDecimal(s, out adc3))
				return false;
			datagram.ADC3 = adc3;
			return true;
		}
		//15-й бит (счетчик импульсов)
		private static bool ReadImpulseCounter(string s, MirepDatagram datagram)
		{
			long value;
			if (!long.TryParse(s, out value))
				return false;
			datagram.ImpulseCounter = value;
			return true;
		}
		//16-й бит (значение датчика Omnicom)
		private static bool ReadOmnicom(string s, MirepDatagram datagram)
		{
			decimal value;
			if (!TryParseAsDecimal(s, out value))
				return false;
			datagram.Omnicom = value;
			return true;
		}
		private static readonly IFormatProvider DotFormatProvider = new NumberFormatInfo {NumberDecimalSeparator = "."};
		private static bool TryParseAsDecimal(string s, out decimal result)
		{
			return decimal.TryParse(s, NumberStyles.Number, DotFormatProvider, out result);
		}
	}
}