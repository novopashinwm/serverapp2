﻿using System;
using System.Collections;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.Terminal.WatchWT8
{
	public class WatchWT8 : Device
	{
		internal const string ProtocolName = "WT8 Watch";
		internal static class DeviceNames
		{
			internal const string WT8Watch = "WT8 Watch";
		}
		public WatchWT8() : base(null, new[]
		{
			DeviceNames.WT8Watch,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			if (data == null) return false;

			string strVal = Encoding.ASCII.GetString(data);
			string[] parts = strVal.Split(',');
			if (parts.Length >= 9 && parts[0] == "IMEI") return true;

			return false;
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;
			if (!Initialized)
				throw new ApplicationException("Device not initialized");

			// list of wrapped responses
			ArrayList alRes = new ArrayList(1);
			WatchWT8Datagram datagram = new WatchWT8Datagram(data);

			if (datagram.IsValid)
			{
				MobilUnit.Unit.MobilUnit mu = UnitReceived(ProtocolName);
				mu.cmdType    = CmdType.Trace;
				mu.DeviceID   = datagram.IMEI;
				//mu.ID         = int.Parse(datagram.ID);
				mu.Latitude   = datagram.Lat;
				mu.Longitude  = datagram.Lng;
				mu.Speed      = datagram.Speed;
				mu.Time       = datagram.Time;
				mu.Satellites = 10;
				mu.CorrectGPS = true;
				mu.Height     = 0;

				alRes.Add(mu);
			}

			if (alRes.Count > 0)
				return alRes;

			return alRes;
		}
	}
}