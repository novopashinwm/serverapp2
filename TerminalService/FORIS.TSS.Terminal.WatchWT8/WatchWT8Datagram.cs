﻿using System;
using System.Globalization;
using System.Text;
using FORIS.TSS.BusinessLogic;

namespace FORIS.TSS.Terminal.WatchWT8
{
	public class WatchWT8Datagram
	{
		public string IMEI;
		public int    Time;
		public float  Lat;
		public float  Lng;
		public int    Speed;
		public bool   IsValid = false;

		public WatchWT8Datagram(byte[] data)
		{
			/* IMEI,
			 * 353358013628933,
			 * 2009/11/02, 
			 * 16:35:07, 
			 * N, 
			 * Lat:5546.8904, 
			 * E, 
			 * Lon:03735.8201, 
			 * Spd:0.00
			*/
			if (data == null) return;
			string strVal = Encoding.ASCII.GetString(data);
			string[] parts = strVal.Split(',');
			if (parts.Length < 9) return;

			IMEI = parts[1];
			DateTime time = DateTime.ParseExact(parts[2] + parts[3], "yyyy/MM/dd HH:mm:ss", CultureInfo.InvariantCulture);

			Time  = TimeHelper.GetSecondsFromBase(DateTime.UtcNow); //TimeHelper.GetSecondsFromBase(time);
			Lat   = float.Parse(parts[5].Split(':')[1], CultureInfo.InvariantCulture) / 100;
			Lng   = float.Parse(parts[7].Split(':')[1], CultureInfo.InvariantCulture) / 100;
			Lat   = (int)Math.Floor(Lat) + ((Lat - (int)Math.Floor(Lat)) / 60 * 100);
			Lng   = (int)Math.Floor(Lng) + ((Lng - (int)Math.Floor(Lng)) / 60 * 100);
			Speed = (int)float.Parse(parts[8].Split(':')[1], CultureInfo.InvariantCulture);

			IsValid = true;
		}
	}
}