﻿using System;
using System.Globalization;
using System.Text;
using FORIS.TSS.Communication;
using FORIS.TSS.Common;

namespace FORIS.TSS.Terminal.AVL
{
    public class AvlDatagram : Datagram
    {
        public static AvlDatagram Create(byte[] data)
        {
            string msg = Encoding.ASCII.GetString(data);
            string[] message = msg.Split('$');

            foreach (string str in message)
            {
                if (str.StartsWith("$GPRMC"))
                {
                    return new AvlDatagram(message);
                }
            }

            return null;
        }

        protected string[] m_message;

        readonly GPRMC rmc;

        public AvlDatagram(string[] message)
        {
            m_message = message;

            foreach (string str in message)
            {
                if (str.StartsWith("$GPRMC"))
                {
                    rmc = new GPRMC(str);
                }
            }
        }

        protected AvlDatagram()
        {

        }

        // <summary>
        /// response time
        /// </summary>
        public virtual int Time
        {
            get
            {
                return rmc.Time;
            }
        }

        /// <summary>
        /// unit latitude. negative - south latitude
        /// </summary>
        public virtual float Latitude
        {
            get
            {
                return rmc.North ? rmc.Latitude : -rmc.Latitude;
            }
        }

        /// <summary>
        /// unit longitude. negative - south longitude
        /// </summary>
        public virtual float Longitude
        {
            get
            {
                return rmc.East ? rmc.Longitude : -rmc.Longitude;
            }
        }

        /// <summary>
        /// unit speed
        /// </summary>
        public virtual int Speed
        {
            get
            {
                return rmc.Speed;
            }
        }

        /// <summary>
        /// pos is valid
        /// </summary>
        public virtual bool CorrectPos
        {
            get
            {
                return rmc.CorrectPos && rmc.Speed < MobilUnit.Unit.MobilUnit.GPSLimitMaxSpeed;
            }
        }
    }

    /// <summary>
    /// $GPRMC wrapper
    /// </summary>
    /// <example>
    /// $GPRMC,095838.000,V,2458.9733,N,12125.6583,E,0.41,79.21,220905,,*30
    /// </example>
    class GPRMC
    {
        readonly string[] arsFields;

        public int Size
        {
            get
            {
                return 66;
            }
        }

        public GPRMC(string data)
        {
            arsFields = data.Split(',');
            if (!(arsFields.Length == 12 || arsFields.Length == 13)) throw new
                 DatagramStructureException("Provided data not $GPRMC or unknown version", GetType(), null, -1);
        }

        public time_t Time
        {
            get
            {
                string time = arsFields[1], date = arsFields[9];
                if (!(time.Length == 10 || time.Length == 11 || time.Length == 9) || date.Length != 6) throw new ApplicationException(
                                                               "Time must be 10 symbols, date - 6");
                return new DateTime(int.Parse("20" + date.Substring(4, 2)), byte.Parse(date.Substring(2, 2)),
                    byte.Parse(date.Substring(0, 2)), byte.Parse(time.Substring(0, 2)),
                    byte.Parse(time.Substring(2, 2)), byte.Parse(time.Substring(4, 2)),
                    int.Parse(time.Substring(7)));
            }
        }

        public bool North
        {
            get
            {
                if (!this.CorrectPos)
                    return true;

                string N = arsFields[4];
                if (N != "N" && N != "S")
                    throw new DatagramStructureException("Latitude must be North or South", GetType(), null, -1);
                return N == "N";
            }
        }

        public float Latitude
        {
            get
            {
                if (!this.CorrectPos)
                    return 0;

                string lat = arsFields[3];
                if (lat.Length != 9)
                    throw new ApplicationException("Latitude must be 9 symbols");
                int g = byte.Parse(lat.Substring(0, 2));
                string point = CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
                float m = float.Parse(lat.Substring(2, 6).Replace(".", point));
                return g + m / 60;
            }
        }

        public bool East
        {
            get
            {
                if (!this.CorrectPos)
                    return true;

                string E = arsFields[6];
                if (E != "E" && E != "W")
                    throw new DatagramStructureException("Longitude must be East or West", GetType(), null, -1);
                return E == "E";
            }
        }

        public float Longitude
        {
            get
            {
                if (!this.CorrectPos)
                    return 0;

                string lon = arsFields[5];
                if (lon.Length != 10)
                    throw new ApplicationException("Longitude must be 10 symbols");
                int g = byte.Parse(lon.Substring(0, 3));
                string point = CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
                float m = float.Parse(lon.Substring(3, 6).Replace(".", point));
                return g + m / 60;
            }
        }

        /// <summary>
        /// 1 marine knot in km/h
        /// </summary>
        const double MarineKnot = 1.852;	// 50. / 27 = 1.(851)

        /// <summary>
        /// unit speed
        /// </summary>
        public int Speed
        {
            get
            {
                if (!this.CorrectPos)
                    return 0;

                string point = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.PercentDecimalSeparator;
                return (int)(double.Parse(arsFields[7].Replace(".", point)) * MarineKnot);
            }
        }

        /// <summary>
        /// unit movement direction
        /// </summary>
        public int Course
        {
            get
            {
                return !this.CorrectPos ? 0 : int.Parse(arsFields[8]);
            }
        }

        /// <summary>
        /// A - data valid, V - navigation receiver warning
        /// </summary>
        public bool CorrectPos
        {
            get
            {
                return arsFields[2] == "A";
            }
        }
    }
}
