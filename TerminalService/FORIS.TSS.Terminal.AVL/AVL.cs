﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using System.Text;
using FORIS.TSS.BusinessLogic;

namespace FORIS.TSS.Terminal.AVL
{
    /// <summary>
    /// Реализация протокола для трекеров TZ-AVL03, GT01, GT04, AVL01, AVL04
    /// <a href="https://tfsdoc.sts.sitronics.com/sites/TSS/DocLib/AVL-03/TZ-AVL03%20GPRS%20Data%20Format3.4.pdf">Протокол TZ-AVL03 версии 3.4</a>
    /// <a href="https://tfsdoc.sts.sitronics.com/sites/TSS/DocLib/AVL-04/TZ-AVL04%20GPRS%20TCP%20Communication%20Protocol%20V5.0.pdf">Протокол версии 5.0</a> 
    /// </summary>
    /// <remarks>Этот же парсер реализован в FORIS.TSS.Terminal.Meiligao.VT300</remarks>
    /// TODO: проверить, что функциональность протокола полностью поддерживается парсером Meiligao.VT300 и удалить дублирующий парсер
    public class AVL : Device
    {
        public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
        {
            bufferRest = null;
            if(data == null || 
                !SupportData(data))
                return null;
            List<byte> imeiBytes = new List<byte>();
            int l = 0;
            for (int i = 4; i < 12; i++ )
            {
                byte left = (byte) ((data[i] >> 4) + 0x30);
                byte right = (byte) ((data[i] & 0x0F) + 0x030);
                if(left == 0x3f)
                    break;
                imeiBytes.Add(left);
                l++;
                if (right == 0x3f)
                    break;
                imeiBytes.Add(right);
                l++;
            }
            //string str = ASCIIEncoding.ASCII.GetString(imeiBytes.GetRange(0, 11).ToArray());
            int commandType = BitConverter.ToUInt16(data, 11);
            switch(commandType)
            {
                case 0x5599:
                    {
                        ArrayList aRes = new ArrayList(1);
                        string strData = Encoding.ASCII.GetString(data, 13, data.Length - 13 - 2);
                        strData = "$GPRMC," + strData;
                        bool correct = strData.Contains("$GPRMC");
                        if (correct)
                        {
                            AvlDatagram dg = new AvlDatagram(new[] {strData});
                            var mu = UnitReceived();
                            mu.DeviceID = Encoding.ASCII.GetString(imeiBytes.GetRange(0, l).ToArray());
                            mu.cmdType = CmdType.Trace;
                            mu.Longitude = dg.Longitude;
                            mu.Latitude = dg.Latitude;
                            mu.Speed = dg.Speed;
                            mu.Time = dg.Time;
                            mu.Satellites = 10;
                            mu.VoltageAN1 = 0;
                            mu.VoltageAN2 = 0;
                            mu.VoltageAN3 = 0;
                            mu.VoltageAN4 = 0;
                            mu.PowerVoltage = 0;
                            mu.CorrectGPS = dg.CorrectPos;
                            aRes.Add(mu);
                         }
                        else
                        {
                            var mu = UnitReceived();
                            mu.DeviceID = Encoding.ASCII.GetString(imeiBytes.GetRange(0, l).ToArray());
                            mu.cmdType = CmdType.Trace;
                            mu.Longitude = 0;
                            mu.Latitude = 0;
                            mu.Speed = 0;
                            mu.Time = (int)
                                (DateTime.Now.ToUniversalTime() -
                                 TimeHelper.year1970).TotalSeconds;
                            mu.Satellites = 0;
                            mu.VoltageAN1 = 0;
                            mu.VoltageAN2 = 0;
                            mu.VoltageAN3 = 0;
                            mu.VoltageAN4 = 0;
                            mu.PowerVoltage = 0;
                            mu.CorrectGPS = false;
                            aRes.Add(mu);
                        }
                        return aRes;
                    }
                case 0x9999:
                    {
                        ArrayList aRes = new ArrayList(1);
                        string strData = Encoding.ASCII.GetString(data, 14, data.Length - 14 - 2);
                        strData = "$GPRMC," + strData;
                        bool correct = strData.Contains("$GPRMC");
                        if (correct)
                        {
                            AvlDatagram dg = new AvlDatagram(new[] {strData});
                            var mu = UnitReceived();
                            mu.DeviceID = Encoding.ASCII.GetString(imeiBytes.GetRange(0, l).ToArray());
                            mu.cmdType = CmdType.Trace;
                            mu.Longitude = dg.Longitude;
                            mu.Latitude = dg.Latitude;
                            mu.Speed = dg.Speed;
                            mu.Time = dg.Time;
                            mu.Satellites = 10;
                            mu.VoltageAN1 = 0;
                            mu.VoltageAN2 = 0;
                            mu.VoltageAN3 = 0;
                            mu.VoltageAN4 = 0;
                            mu.PowerVoltage = 0;
                            mu.CorrectGPS = dg.CorrectPos;
                            aRes.Add(mu);
                            if (data[13] != 0)
                            {
                                IUnitInfo ui = UnitInfo.Factory.Create(UnitInfoPurpose.TM140);
                                ui.DeviceID = mu.DeviceID;
                                object[] par = new object[4];
                                par[0] = ui;
                                par[1] = "";
                                par[2] = 1;
                                par[3] = 1;
                                aRes.Add(new NotifyEventArgs((time_t) mu.Time, TerminalEventMessage.NE_ALARM, null,
                                                             Severity.Lvl1, NotifyCategory.Controller, "Тревога", null,
                                                             par));
                            }
                        }
                        else
                        {
                            var mu = UnitReceived();
                            mu.DeviceID = Encoding.ASCII.GetString(imeiBytes.GetRange(0, l).ToArray());
                            mu.cmdType = CmdType.Trace;
                            mu.Longitude = 0;
                            mu.Latitude = 0;
                            mu.Speed = 0;
                            mu.Time = (int)
                                (DateTime.Now.ToUniversalTime() -
                                 TimeHelper.year1970).TotalSeconds;
                            mu.Satellites = 0;
                            mu.VoltageAN1 = 0;
                            mu.VoltageAN2 = 0;
                            mu.VoltageAN3 = 0;
                            mu.VoltageAN4 = 0;
                            mu.PowerVoltage = 0;
                            mu.CorrectGPS = false;
                            aRes.Add(mu);
                        }
                        return aRes;
                    }
                default:
                    Trace.WriteLine(GetType()+".OnData: Unknown command type: " + commandType);
                    return null;
            }
        }

        public override bool SupportData(byte[] data)
        {
            //Этот же парсер реализован в FORIS.TSS.Terminal.Meiligao.VT300 
            return false; // return data != null && data.Length > 3 && data[0] == '$' && data[1] == '$' && Datagram.IntFromBytesBE(data[2], data[3]) == data.Length - 2;
        }
    }
}
