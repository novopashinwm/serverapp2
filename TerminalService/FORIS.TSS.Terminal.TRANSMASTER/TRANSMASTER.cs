﻿using System;
using System.Collections;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Communication;

namespace FORIS.TSS.Terminal.TRANSMASTER
{
    public class TRANSMASTER : Device
    {
        public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
        {
            bufferRest = null;
            if(data == null || data.Length < 16 || data[0] != 0x5A)
                return null;

            int id = Datagram.IntFromBytesBE(data[6], data[5], data[4]);
            int numOfFrame = Datagram.IntFromBytesBE(data[10], data[9]);
            int numOfSubFrame = Datagram.IntFromBytesBE(data[12], data[11]);
            int len = Datagram.IntFromBytesBE(data[14], data[13]);
            int crc = data[15];
            if(data.Length < 16+len)
                return null;
            if(numOfSubFrame != 0) //Не целое сообщение, пока, в игнор его!!
                return null;
            if(data[16] != 0) //Не первичное сообщение
                return null;

            if(data[17] != 0x01 && data[17] != 0x02) //Позиция по времени или по событию
                return null;

            Int16 signature = (Int16)Datagram.IntFromBytesBE(data[19], data[20]);
            if( (signature & 0x2000) == 0)
                return null; //Нету позиции
            int pos = 21;
            if ( (signature & 0x8000) != 0)
            {
                pos += 4;
            }
            if ((signature & 0x4000) != 0)
            {
                pos += 10;
            }

            int status = data[pos];
            if(status != 1 && status != 2)
                return null;

            int latitude = Datagram.IntFromBytesBE(data[pos + 4], data[pos + 3], data[pos + 2], data[pos + 1]);
            int longitude = Datagram.IntFromBytesBE(data[pos + 8], data[pos + 7], data[pos + 6], data[pos + 5]);
            int speed = data[pos + 9];
            int time = Datagram.IntFromBytesBE(data[pos + 14], data[pos + 13], data[pos + 12], data[pos + 11]);

            double fLat = latitude / 100000.0 / 60.0;
            double fLong = longitude / 100000.0 / 60.0;
            pos += 15;
            int altitude = 0;
            if( (signature & 0x1000) != 0)
            {
                altitude = Datagram.IntFromBytesBE(data[pos + 1], data[pos + 0]);
                pos += 2;
            }
            if((signature & 0x0800) != 0) //– Параметры навигационного определения
            {
                pos += 4;
            }
            int numOfSat = 10; // Кол-во спутников
            if((signature & 0x0400) != 0)
            {
                numOfSat = data[pos];
                pos += (numOfSat + 1);
            }
            int rdt = 0; //Время отправки сообщения
            if((signature & 0x0200) != 0)
            {
                rdt = Datagram.IntFromBytesBE(data[pos + 3], data[pos + 2], data[pos+1],data[pos]);
                pos += 4;
            }
            if ((signature & 0x0100) != 0)
            {
                pos += 2;
            }
            int run = 0;
            if( (signature & 0x0080) != 0)
            {
                run = Datagram.IntFromBytesBE(data[pos + 3], data[pos + 2], data[pos + 1], data[pos]);
            }

            var mu = UnitReceived();
            mu.ID = id;
            mu.Latitude = fLat;
            mu.Longitude = fLong;
            mu.Time = (int)((DateTime.Parse("2000.01.01 00:00:00") - TimeHelper.year1970).TotalSeconds + time);
            mu.Speed = speed;
            mu.Satellites = numOfSat;
            mu.cmdType = CmdType.Trace;
            mu.Run = run;
            mu.CorrectGPS = status == 1 || status == 2;

            var aRes = new ArrayList(1) { mu };
            return aRes;
        }

        public override bool SupportData(byte[] data)
        {
            return data != null && data.Length > 17 && data[0] == 0x5A && data[16] == 0 && (data[17] == 0x01 || data[17] == 0x02);
        }
    }
}