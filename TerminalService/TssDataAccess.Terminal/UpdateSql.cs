using FORIS.DataAccess;

namespace FORIS.TSS.BusinessLogic.Terminal
{
    using System.Data.SqlClient;
    using System.Data.SqlTypes;
    
    
    /// <summary>
    /// <P>Allows to build SQL commands</P>
    /// </summary>
    public class UpdateSql
    {
        
        /// <summary>
        /// Backer for InsertCommand property
        /// </summary>
        private System.Data.SqlClient.SqlCommand theInsertCommand;
        
        /// <summary>
        /// Backer for UpdateCommand property
        /// </summary>
        private System.Data.SqlClient.SqlCommand theUpdateCommand;
        
        /// <summary>
        /// Backer for DeleteCommand property
        /// </summary>
        private System.Data.SqlClient.SqlCommand theDeleteCommand;
        
        private UpdateSql()
        {
        }
        
        /// <summary>
        /// Accessor for InsertCommand command
        /// </summary>
        public virtual System.Data.SqlClient.SqlCommand InsertCommand
        {
            get
            {
return theInsertCommand;
            }
        }
        
        /// <summary>
        /// Accessor for UpdateCommand command
        /// </summary>
        public virtual System.Data.SqlClient.SqlCommand UpdateCommand
        {
            get
            {
return theUpdateCommand;
            }
        }
        
        /// <summary>
        /// Accessor for DeleteCommand command
        /// </summary>
        public virtual System.Data.SqlClient.SqlCommand DeleteCommand
        {
            get
            {
return theDeleteCommand;
            }
        }
        
        public static UpdateSql Controller()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertController");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateController");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteController");
return u;
        }
        
        public static UpdateSql ControllerInfo()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertControllerInfo");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateControllerInfo");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteControllerInfo");
return u;
        }
        
        public static UpdateSql ControllerType()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertControllerType");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateControllerType");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteControllerType");
return u;
        }
        
        public static UpdateSql Media()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertMedia");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateMedia");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteMedia");
return u;
        }
        
        public static UpdateSql MediaAcceptors()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertMediaAcceptors");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateMediaAcceptors");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteMediaAcceptors");
return u;
        }
        
        public static UpdateSql MediaType()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertMediaType");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateMediaType");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteMediaType");
return u;
        }
        
        public static UpdateSql MonitoreeLog()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertMonitoreeLog");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateMonitoreeLog");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteMonitoreeLog");
return u;
        }
        
        public static UpdateSql Operator()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertOperator");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateOperator");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteOperator");
return u;
        }
        
        public static UpdateSql Session()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertSession");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateSession");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteSession");
return u;
        }
        
        public static UpdateSql Sysdiagrams()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertSysdiagrams");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateSysdiagrams");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteSysdiagrams");
return u;
        }
        
        public static UpdateSql Trail()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertTrail");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateTrail");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteTrail");
return u;
        }
        
        public static UpdateSql Vehicle()
        {
UpdateSql u = new UpdateSql();

            u.theInsertCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_InsertVehicle");

            u.theUpdateCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_UpdateVehicle");

            u.theDeleteCommand = (System.Data.SqlClient.SqlCommand)Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("dbo.H_DeleteVehicle");
return u;
        }
    }
}
