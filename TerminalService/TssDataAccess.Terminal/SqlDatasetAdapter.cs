﻿using System;
using System.Data;
using FORIS.DataAccess;

namespace FORIS.TSS.BusinessLogic.Terminal
{
	///<summary>
	/// Class for saving dataset into database
	///</summary>
	public class SqlDatasetAdapter : IDatasetAdapter
	{
		#region DataAdapters
		public TssDataAdapter adapterController;
		public TssDataAdapter adapterControllerInfo;
		public TssDataAdapter adapterControllerType;
		public TssDataAdapter adapterMedia;
		public TssDataAdapter adapterMediaAcceptors;
		public TssDataAdapter adapterMediaType;
		public TssDataAdapter adapterMonitoreeLog;
		public TssDataAdapter adapterOperator;
		public TssDataAdapter adapterSession;
		public TssDataAdapter adapterSysdiagrams;
		public TssDataAdapter adapterTrail;
		public TssDataAdapter adapterVehicle;
		#endregion DataAdapters
		protected int trailID = 0;
		protected IDbDataAdapterCollection adapters = new IDbDataAdapterCollection();
		public event BeforeDeleteHandler BeforeDelete = null;
		protected void FireBeforeDelete()
		{
			BeforeDelete?.Invoke();
		}
		protected int sessionID = 0;
		public int SessionID
		{
			get
			{
				return sessionID;
			}
			set
			{
				sessionID = value;
			}
		}
		public int TrailID
		{
			get
			{
				return trailID;
			}
			set
			{
				trailID = value;
			}
		}
		public IDbDataAdapterCollection Adapters
		{
			get
			{
				return adapters;
			}
		}
		public int CreateTransactionInDatabase(IDbConnection connection, IDbTransaction transaction)
		{
			using (IDbCommand command = Globals.TssDatabaseManager.DefaultDataBase.ProcedureAsCommand("CreateTransaction"))
			{
				System.Data.SqlClient.SqlParameter session = ((System.Data.SqlClient.SqlParameter)command.Parameters["@SESSION_ID"]);
				if (SessionID != 0)
				{
					session.Value = SessionID;
				}
				else
				{
					session.Value = DBNull.Value;
				}
				System.Data.SqlClient.SqlParameter time = ((System.Data.SqlClient.SqlParameter)command.Parameters["@TRAIL_TIME"]);
				time.Value = DateTime.Now.ToUniversalTime();
				command.Connection = connection;
				command.Transaction = transaction;
				object res = command.ExecuteScalar();
				TrailID = (int)((System.Decimal)res);
			}
			SetupParameters();
			return TrailID;
		}
		protected void SetupParameters()
		{
			foreach (IDbDataAdapter adapter in Adapters)
			{
				if (adapter.UpdateCommand != null)
				{
					if (adapter.UpdateCommand.Parameters.Contains("@TRAIL_ID"))
					{
						System.Data.SqlClient.SqlParameter par = (System.Data.SqlClient.SqlParameter)adapter.UpdateCommand.Parameters["@TRAIL_ID"];
						par.Value = TrailID;
					}
				}
				if (adapter.InsertCommand != null)
				{
					if (adapter.InsertCommand.Parameters.Contains("@TRAIL_ID"))
					{
						System.Data.SqlClient.SqlParameter par = (System.Data.SqlClient.SqlParameter)adapter.InsertCommand.Parameters["@TRAIL_ID"];
						par.Value = TrailID;
					}
				}
				if (adapter.DeleteCommand != null)
				{
					if (adapter.DeleteCommand.Parameters.Contains("@TRAIL_ID"))
					{
						System.Data.SqlClient.SqlParameter par = (System.Data.SqlClient.SqlParameter)adapter.DeleteCommand.Parameters["@TRAIL_ID"];
						par.Value = TrailID;
					}
				}
			}
		}
		public SqlDatasetAdapter()
		{
			#region Create dataadapters
			adapterControllerType = new TssDataAdapter();
			Adapters.Add(adapterControllerType);
			UpdateSql uControllerType = UpdateSql.ControllerType();
			adapterControllerType.InsertCommand = uControllerType.InsertCommand;
			adapterControllerType.UpdateCommand = uControllerType.UpdateCommand;
			adapterControllerType.DeleteCommand = uControllerType.DeleteCommand;
			adapterMediaType = new TssDataAdapter();
			Adapters.Add(adapterMediaType);
			UpdateSql uMediaType = UpdateSql.MediaType();
			adapterMediaType.InsertCommand = uMediaType.InsertCommand;
			adapterMediaType.UpdateCommand = uMediaType.UpdateCommand;
			adapterMediaType.DeleteCommand = uMediaType.DeleteCommand;
			adapterMedia = new TssDataAdapter();
			Adapters.Add(adapterMedia);
			UpdateSql uMedia = UpdateSql.Media();
			adapterMedia.InsertCommand = uMedia.InsertCommand;
			adapterMedia.UpdateCommand = uMedia.UpdateCommand;
			adapterMedia.DeleteCommand = uMedia.DeleteCommand;
			adapterMediaAcceptors = new TssDataAdapter();
			Adapters.Add(adapterMediaAcceptors);
			UpdateSql uMediaAcceptors = UpdateSql.MediaAcceptors();
			adapterMediaAcceptors.InsertCommand = uMediaAcceptors.InsertCommand;
			adapterMediaAcceptors.UpdateCommand = uMediaAcceptors.UpdateCommand;
			adapterMediaAcceptors.DeleteCommand = uMediaAcceptors.DeleteCommand;
			adapterMonitoreeLog = new TssDataAdapter();
			Adapters.Add(adapterMonitoreeLog);
			UpdateSql uMonitoreeLog = UpdateSql.MonitoreeLog();
			adapterMonitoreeLog.InsertCommand = uMonitoreeLog.InsertCommand;
			adapterMonitoreeLog.UpdateCommand = uMonitoreeLog.UpdateCommand;
			adapterMonitoreeLog.DeleteCommand = uMonitoreeLog.DeleteCommand;
			adapterOperator = new TssDataAdapter();
			Adapters.Add(adapterOperator);
			UpdateSql uOperator = UpdateSql.Operator();
			adapterOperator.InsertCommand = uOperator.InsertCommand;
			adapterOperator.UpdateCommand = uOperator.UpdateCommand;
			adapterOperator.DeleteCommand = uOperator.DeleteCommand;
			adapterSession = new TssDataAdapter();
			Adapters.Add(adapterSession);
			UpdateSql uSession = UpdateSql.Session();
			adapterSession.InsertCommand = uSession.InsertCommand;
			adapterSession.UpdateCommand = uSession.UpdateCommand;
			adapterSession.DeleteCommand = uSession.DeleteCommand;
			adapterSysdiagrams = new TssDataAdapter();
			Adapters.Add(adapterSysdiagrams);
			UpdateSql uSysdiagrams = UpdateSql.Sysdiagrams();
			adapterSysdiagrams.InsertCommand = uSysdiagrams.InsertCommand;
			adapterSysdiagrams.UpdateCommand = uSysdiagrams.UpdateCommand;
			adapterSysdiagrams.DeleteCommand = uSysdiagrams.DeleteCommand;
			adapterTrail = new TssDataAdapter();
			Adapters.Add(adapterTrail);
			UpdateSql uTrail = UpdateSql.Trail();
			adapterTrail.InsertCommand = uTrail.InsertCommand;
			adapterTrail.UpdateCommand = uTrail.UpdateCommand;
			adapterTrail.DeleteCommand = uTrail.DeleteCommand;
			adapterVehicle = new TssDataAdapter();
			Adapters.Add(adapterVehicle);
			UpdateSql uVehicle = UpdateSql.Vehicle();
			adapterVehicle.InsertCommand = uVehicle.InsertCommand;
			adapterVehicle.UpdateCommand = uVehicle.UpdateCommand;
			adapterVehicle.DeleteCommand = uVehicle.DeleteCommand;
			adapterController = new TssDataAdapter();
			Adapters.Add(adapterController);
			UpdateSql uController = UpdateSql.Controller();
			adapterController.InsertCommand = uController.InsertCommand;
			adapterController.UpdateCommand = uController.UpdateCommand;
			adapterController.DeleteCommand = uController.DeleteCommand;
			adapterControllerInfo = new TssDataAdapter();
			Adapters.Add(adapterControllerInfo);
			UpdateSql uControllerInfo = UpdateSql.ControllerInfo();
			adapterControllerInfo.InsertCommand = uControllerInfo.InsertCommand;
			adapterControllerInfo.UpdateCommand = uControllerInfo.UpdateCommand;
			adapterControllerInfo.DeleteCommand = uControllerInfo.DeleteCommand;
			#endregion
		}
		public void Update(DataSet dataset, IDbConnection connection, IDbTransaction transaction)
		{
			int trail_id = CreateTransactionInDatabase(connection, transaction);
			Cascades cascades = new Cascades(connection, transaction);
			cascades.TrailID = trail_id;
			cascades.CheckDatasetProperties(dataset);

			#region Setup dataadapters
			GenericHelpers.SetConnectionTransaction(adapterController, connection, transaction);
			GenericHelpers.SetConnectionTransaction(adapterControllerInfo, connection, transaction);
			GenericHelpers.SetConnectionTransaction(adapterControllerType, connection, transaction);
			GenericHelpers.SetConnectionTransaction(adapterMedia, connection, transaction);
			GenericHelpers.SetConnectionTransaction(adapterMediaAcceptors, connection, transaction);
			GenericHelpers.SetConnectionTransaction(adapterMediaType, connection, transaction);
			GenericHelpers.SetConnectionTransaction(adapterMonitoreeLog, connection, transaction);
			GenericHelpers.SetConnectionTransaction(adapterOperator, connection, transaction);
			GenericHelpers.SetConnectionTransaction(adapterSession, connection, transaction);
			GenericHelpers.SetConnectionTransaction(adapterSysdiagrams, connection, transaction);
			GenericHelpers.SetConnectionTransaction(adapterTrail, connection, transaction);
			GenericHelpers.SetConnectionTransaction(adapterVehicle, connection, transaction);
			#endregion
			// perform update
			SetupParameters();

			bool oldValue = dataset.EnforceConstraints;
			dataset.EnforceConstraints = false;
			try
			{
				ExecuteInsertCommands(dataset);
				ExecuteUpdateCommands(dataset);
				cascades.RunCascades();
			}
			finally
			{
				//dataset.EnforceConstraints = oldValue;
			}
		}
		public void ExecuteInsertCommands(DataSet dataset)
		{
			DataTable table = null;
			#region Insert records to tables in insert order
			table = dataset.Tables["CONTROLLER_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					adapterControllerType.Update(rows);
				}
			}
			table = dataset.Tables["MEDIA_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					adapterMediaType.Update(rows);
				}
			}
			table = dataset.Tables["MEDIA"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					adapterMedia.Update(rows);
				}
			}
			table = dataset.Tables["MEDIA_ACCEPTORS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					adapterMediaAcceptors.Update(rows);
				}
			}
			table = dataset.Tables["MONITOREE_LOG"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					adapterMonitoreeLog.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					adapterOperator.Update(rows);
				}
			}
			table = dataset.Tables["SESSION"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					adapterSession.Update(rows);
				}
			}
			table = dataset.Tables["sysdiagrams"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					adapterSysdiagrams.Update(rows);
				}
			}
			table = dataset.Tables["TRAIL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					adapterTrail.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					adapterVehicle.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					adapterController.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_INFO"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Added);
				if (rows != null && rows.Length > 0)
				{
					adapterControllerInfo.Update(rows);
				}
			}
			#endregion
		}
		public void ExecuteUpdateCommands(DataSet dataset)
		{
			DataTable table = null;
			#region Update records in tables in insert order
			table = dataset.Tables["CONTROLLER_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					adapterControllerType.Update(rows);
				}
			}
			table = dataset.Tables["MEDIA_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					adapterMediaType.Update(rows);
				}
			}
			table = dataset.Tables["MEDIA"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					adapterMedia.Update(rows);
				}
			}
			table = dataset.Tables["MEDIA_ACCEPTORS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					adapterMediaAcceptors.Update(rows);
				}
			}
			table = dataset.Tables["MONITOREE_LOG"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					adapterMonitoreeLog.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					adapterOperator.Update(rows);
				}
			}
			table = dataset.Tables["SESSION"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					adapterSession.Update(rows);
				}
			}
			table = dataset.Tables["sysdiagrams"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					adapterSysdiagrams.Update(rows);
				}
			}
			table = dataset.Tables["TRAIL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					adapterTrail.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					adapterVehicle.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					adapterController.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_INFO"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.ModifiedCurrent);
				if (rows != null && rows.Length > 0)
				{
					adapterControllerInfo.Update(rows);
				}
			}
			#endregion
		}
		public void ExecuteDeleteCommands(DataSet dataset)
		{
			DataTable table = null;
			#region Update records in tables in delete order
			table = dataset.Tables["CONTROLLER_INFO"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					adapterControllerInfo.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					adapterController.Update(rows);
				}
			}
			table = dataset.Tables["VEHICLE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					adapterVehicle.Update(rows);
				}
			}
			table = dataset.Tables["TRAIL"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					adapterTrail.Update(rows);
				}
			}
			table = dataset.Tables["sysdiagrams"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					adapterSysdiagrams.Update(rows);
				}
			}
			table = dataset.Tables["SESSION"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					adapterSession.Update(rows);
				}
			}
			table = dataset.Tables["OPERATOR"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					adapterOperator.Update(rows);
				}
			}
			table = dataset.Tables["MONITOREE_LOG"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					adapterMonitoreeLog.Update(rows);
				}
			}
			table = dataset.Tables["MEDIA_ACCEPTORS"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					adapterMediaAcceptors.Update(rows);
				}
			}
			table = dataset.Tables["MEDIA"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					adapterMedia.Update(rows);
				}
			}
			table = dataset.Tables["MEDIA_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					adapterMediaType.Update(rows);
				}
			}
			table = dataset.Tables["CONTROLLER_TYPE"];
			if (table != null)
			{
				DataRow[] rows = table.Select(null, null, DataViewRowState.Deleted);
				if (rows != null && rows.Length > 0)
				{
					adapterControllerType.Update(rows);
				}
			}
			#endregion
		}
	}
}