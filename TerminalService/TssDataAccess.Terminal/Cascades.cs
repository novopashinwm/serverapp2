﻿using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Text;
using FORIS.DataAccess;
using FORIS.TSS.BusinessLogic.Terminal.Schema;

namespace FORIS.TSS.BusinessLogic.Terminal
{
	/// <summary> Implements tables for the cascade deletion </summary>
	public class Cascades : CascadesBase
	{
		private IDbConnection  conn;
		private IDbTransaction tran;
		public Hashtable hashKey_Controller     = new Hashtable();
		public Hashtable hashKey_ControllerInfo = new Hashtable();
		public Hashtable hashKey_ControllerType = new Hashtable();
		public Hashtable hashKey_Media          = new Hashtable();
		public Hashtable hashKey_MediaAcceptors = new Hashtable();
		public Hashtable hashKey_MediaType      = new Hashtable();
		public Hashtable hashKey_MonitoreeLog   = new Hashtable();
		public Hashtable hashKey_Operator       = new Hashtable();
		public Hashtable hashKey_Session        = new Hashtable();
		public Hashtable hashKey_Sysdiagrams    = new Hashtable();
		public Hashtable hashKey_Trail          = new Hashtable();
		public Hashtable hashKey_Vehicle        = new Hashtable();

		public Cascades(IDbConnection connection, IDbTransaction transaction)
		{
			this.conn = connection;
			this.tran = transaction;

		}
		public virtual void RunCascades()
		{
			RunCascades(conn, tran);

		}
		public virtual void CheckDatasetProperties(DataSet dataset)
		{

			if (dataset.Tables.Contains("CONTROLLER"))
			{
				CheckControllerProperties(dataset.Tables["CONTROLLER"]);
			}
			if (dataset.Tables.Contains("CONTROLLER_INFO"))
			{
				CheckControllerInfoProperties(dataset.Tables["CONTROLLER_INFO"]);
			}
			if (dataset.Tables.Contains("CONTROLLER_TYPE"))
			{
				CheckControllerTypeProperties(dataset.Tables["CONTROLLER_TYPE"]);
			}
			if (dataset.Tables.Contains("MEDIA"))
			{
				CheckMediaProperties(dataset.Tables["MEDIA"]);
			}
			if (dataset.Tables.Contains("MEDIA_ACCEPTORS"))
			{
				CheckMediaAcceptorsProperties(dataset.Tables["MEDIA_ACCEPTORS"]);
			}
			if (dataset.Tables.Contains("MEDIA_TYPE"))
			{
				CheckMediaTypeProperties(dataset.Tables["MEDIA_TYPE"]);
			}
			if (dataset.Tables.Contains("OPERATOR"))
			{
				CheckOperatorProperties(dataset.Tables["OPERATOR"]);
			}
			if (dataset.Tables.Contains("SESSION"))
			{
				CheckSessionProperties(dataset.Tables["SESSION"]);
			}
			if (dataset.Tables.Contains("sysdiagrams"))
			{
				CheckSysdiagramsProperties(dataset.Tables["sysdiagrams"]);
			}
			if (dataset.Tables.Contains("TRAIL"))
			{
				CheckTrailProperties(dataset.Tables["TRAIL"]);
			}
			if (dataset.Tables.Contains("VEHICLE"))
			{
				CheckVehicleProperties(dataset.Tables["VEHICLE"]);
			}

		}
		public virtual void RunCascades(IDbConnection connection, IDbTransaction transaction)
		{

			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_ControllerType.Keys.Count > 0)
			{
				LoadRecords_FK_CONTROLLER_CONTROLLER_TYPE(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_MediaType.Keys.Count > 0)
			{
				LoadRecords_FK_MEDIA_MEDIA_TYPE(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Media.Keys.Count > 0)
			{
				LoadRecords_FK_MEDIA_ACCEPTORS_MEDIA(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Operator.Keys.Count > 0)
			{
				LoadRecords_FK_SESSION_OPERATOR(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Session.Keys.Count > 0)
			{
				LoadRecords_FK_TRAIL_SESSION(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Vehicle.Keys.Count > 0)
			{
				LoadRecords_FK_CONTROLLER_VEHICLE(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_Controller.Keys.Count > 0)
			{
				LoadRecords_FK_CONTROLLER_INFO_CONTROLLER(connection, transaction);
			}
			Debug.Assert(connection.State == ConnectionState.Open);
			if (this.hashKey_ControllerInfo.Keys.Count > 0) RemoveRecords_ControllerInfo(connection, transaction);
			if (this.hashKey_Controller.Keys.Count > 0) RemoveRecords_Controller(connection, transaction);
			if (this.hashKey_Vehicle.Keys.Count > 0) RemoveRecords_Vehicle(connection, transaction);
			if (this.hashKey_Trail.Keys.Count > 0) RemoveRecords_Trail(connection, transaction);
			if (this.hashKey_Sysdiagrams.Keys.Count > 0) RemoveRecords_Sysdiagrams(connection, transaction);
			if (this.hashKey_Session.Keys.Count > 0) RemoveRecords_Session(connection, transaction);
			if (this.hashKey_Operator.Keys.Count > 0) RemoveRecords_Operator(connection, transaction);
			if (this.hashKey_MediaAcceptors.Keys.Count > 0) RemoveRecords_MediaAcceptors(connection, transaction);
			if (this.hashKey_Media.Keys.Count > 0) RemoveRecords_Media(connection, transaction);
			if (this.hashKey_MediaType.Keys.Count > 0) RemoveRecords_MediaType(connection, transaction);
			if (this.hashKey_ControllerType.Keys.Count > 0) RemoveRecords_ControllerType(connection, transaction);

		}
		public virtual void CheckControllerProperties(DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ControllerPrimaryKey key = new ControllerPrimaryKey();
							key.ControllerId = (System.Int32)row["CONTROLLER_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Controller.Add(key, key.ControllerId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}
		public virtual void RemoveRecords_Controller(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteController", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ControllerPrimaryKey pk in hashKey_Controller.Keys)
			{
				sp["@CONTROLLER_ID"].Value = pk.ControllerId;
				sp.ExecuteNonQuery();
			}

		}
		public virtual void CheckControllerInfoProperties(DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ControllerInfoPrimaryKey key = new ControllerInfoPrimaryKey();
							key.ControllerId = (System.Int32)row["CONTROLLER_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ControllerInfo.Add(key, key.ControllerId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}
		public virtual void RemoveRecords_ControllerInfo(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteControllerInfo", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ControllerInfoPrimaryKey pk in hashKey_ControllerInfo.Keys)
			{
				sp["@CONTROLLER_ID"].Value = pk.ControllerId;
				sp.ExecuteNonQuery();
			}

		}
		public virtual void CheckControllerTypeProperties(DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							ControllerTypePrimaryKey key = new ControllerTypePrimaryKey();
							key.ControllerTypeId = (System.Int32)row["CONTROLLER_TYPE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_ControllerType.Add(key, key.ControllerTypeId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}
		public virtual void RemoveRecords_ControllerType(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteControllerType", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (ControllerTypePrimaryKey pk in hashKey_ControllerType.Keys)
			{
				sp["@CONTROLLER_TYPE_ID"].Value = pk.ControllerTypeId;
				sp.ExecuteNonQuery();
			}

		}
		public virtual void CheckMediaProperties(DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							MediaPrimaryKey key = new MediaPrimaryKey();
							key.MediaId = (System.Int32)row["MEDIA_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Media.Add(key, key.MediaId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}
		public virtual void RemoveRecords_Media(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteMedia", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (MediaPrimaryKey pk in hashKey_Media.Keys)
			{
				sp["@MEDIA_ID"].Value = pk.MediaId;
				sp.ExecuteNonQuery();
			}

		}
		public virtual void CheckMediaAcceptorsProperties(DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							MediaAcceptorsPrimaryKey key = new MediaAcceptorsPrimaryKey();
							key.MaId = (System.Int32)row["MA_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_MediaAcceptors.Add(key, key.MaId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}
		public virtual void RemoveRecords_MediaAcceptors(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteMediaAcceptors", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (MediaAcceptorsPrimaryKey pk in hashKey_MediaAcceptors.Keys)
			{
				sp["@MA_ID"].Value = pk.MaId;
				sp.ExecuteNonQuery();
			}

		}
		public virtual void CheckMediaTypeProperties(DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							MediaTypePrimaryKey key = new MediaTypePrimaryKey();
							key.Id = (System.Byte)row["ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_MediaType.Add(key, key.Id);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}
		public virtual void RemoveRecords_MediaType(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteMediaType", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (MediaTypePrimaryKey pk in hashKey_MediaType.Keys)
			{
				sp["@ID"].Value = pk.Id;
				sp.ExecuteNonQuery();
			}

		}
		public virtual void CheckOperatorProperties(DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							OperatorPrimaryKey key = new OperatorPrimaryKey();
							key.OperatorId = (System.Int32)row["OPERATOR_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Operator.Add(key, key.OperatorId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}
		public virtual void RemoveRecords_Operator(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteOperator", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (OperatorPrimaryKey pk in hashKey_Operator.Keys)
			{
				sp["@OPERATOR_ID"].Value = pk.OperatorId;
				sp.ExecuteNonQuery();
			}

		}
		public virtual void CheckSessionProperties(DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							SessionPrimaryKey key = new SessionPrimaryKey();
							key.SessionId = (System.Int32)row["SESSION_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Session.Add(key, key.SessionId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}
		public virtual void RemoveRecords_Session(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteSession", connection, transaction);
			foreach (SessionPrimaryKey pk in hashKey_Session.Keys)
			{
				sp["@SESSION_ID"].Value = pk.SessionId;
				sp.ExecuteNonQuery();
			}

		}
		public virtual void CheckSysdiagramsProperties(DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							SysdiagramsPrimaryKey key = new SysdiagramsPrimaryKey();
							key.DiagramId = (System.Int32)row["diagram_id", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Sysdiagrams.Add(key, key.DiagramId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}
		public virtual void RemoveRecords_Sysdiagrams(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteSysdiagrams", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (SysdiagramsPrimaryKey pk in hashKey_Sysdiagrams.Keys)
			{
				sp["@diagram_id"].Value = pk.DiagramId;
				sp.ExecuteNonQuery();
			}

		}
		public virtual void CheckTrailProperties(DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							TrailPrimaryKey key = new TrailPrimaryKey();
							key.TrailId = (System.Int32)row["TRAIL_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Trail.Add(key, key.TrailId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}
		public virtual void RemoveRecords_Trail(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteTrail", connection, transaction);
			foreach (TrailPrimaryKey pk in hashKey_Trail.Keys)
			{
				sp["@TRAIL_ID"].Value = pk.TrailId;
				sp.ExecuteNonQuery();
			}

		}
		public virtual void CheckVehicleProperties(DataTable table)
		{

			foreach (DataRow row in table.Rows)
			{
				switch (row.RowState)
				{
					case DataRowState.Deleted:
						{
							VehiclePrimaryKey key = new VehiclePrimaryKey();
							key.VehicleId = (System.Int32)row["VEHICLE_ID", DataRowVersion.Original];
							// This table doesn't contain DELETED_EVENT_ID field
							hashKey_Vehicle.Add(key, key.VehicleId);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
			}

		}
		public virtual void RemoveRecords_Vehicle(IDbConnection connection, IDbTransaction transaction)
		{

			StoredProcedure sp = Globals.TssDatabaseManager.DefaultDataBase.Procedure("H_DeleteVehicle", connection, transaction);
			sp["@TRAIL_ID"].Value = this.TrailID;
			foreach (VehiclePrimaryKey pk in hashKey_Vehicle.Keys)
			{
				sp["@VEHICLE_ID"].Value = pk.VehicleId;
				sp.ExecuteNonQuery();
			}

		}
		public virtual void LoadRecords_FK_CONTROLLER_CONTROLLER_TYPE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[CONTROLLER_ID] FROM [dbo].[CONTROLLER] t1 inner join [dbo].[CONTROLLER_TYPE] t2 on t1.[CONTROLLER_TYPE_ID]=t2.[CONTROLLER_TYPE_ID] WHERE t2.[CONTROLLER_TYPE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_ControllerType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ControllerPrimaryKey key = new ControllerPrimaryKey((System.Int32)reader["CONTROLLER_ID"]);
						if (hashKey_Controller.ContainsKey(key) == false)
						{
							hashKey_Controller.Add(key, key.ControllerId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}
		public virtual void LoadRecords_FK_CONTROLLER_INFO_CONTROLLER(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[CONTROLLER_ID] FROM [dbo].[CONTROLLER_INFO] t1 inner join [dbo].[CONTROLLER] t2 on t1.[CONTROLLER_ID]=t2.[CONTROLLER_ID] WHERE t2.[CONTROLLER_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Controller.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ControllerInfoPrimaryKey key = new ControllerInfoPrimaryKey((System.Int32)reader["CONTROLLER_ID"]);
						if (hashKey_ControllerInfo.ContainsKey(key) == false)
						{
							hashKey_ControllerInfo.Add(key, key.ControllerId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}
		public virtual void LoadRecords_FK_CONTROLLER_VEHICLE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[CONTROLLER_ID] FROM [dbo].[CONTROLLER] t1 inner join [dbo].[VEHICLE] t2 on t1.[VEHICLE_ID]=t2.[VEHICLE_ID] WHERE t2.[VEHICLE_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Vehicle.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ControllerPrimaryKey key = new ControllerPrimaryKey((System.Int32)reader["CONTROLLER_ID"]);
						if (hashKey_Controller.ContainsKey(key) == false)
						{
							hashKey_Controller.Add(key, key.ControllerId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}
		public virtual void LoadRecords_FK_MEDIA_ACCEPTORS_MEDIA(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[MA_ID] FROM [dbo].[MEDIA_ACCEPTORS] t1 inner join [dbo].[MEDIA] t2 on t1.[MEDIA_ID]=t2.[MEDIA_ID] WHERE t2.[MEDIA_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Media.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						MediaAcceptorsPrimaryKey key = new MediaAcceptorsPrimaryKey((System.Int32)reader["MA_ID"]);
						if (hashKey_MediaAcceptors.ContainsKey(key) == false)
						{
							hashKey_MediaAcceptors.Add(key, key.MaId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}
		public virtual void LoadRecords_FK_MEDIA_MEDIA_TYPE(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[MEDIA_ID] FROM [dbo].[MEDIA] t1 inner join [dbo].[MEDIA_TYPE] t2 on t1.[TYPE]=t2.[ID] WHERE t2.[ID] in (");
			bool first = true;
			foreach (int pk in hashKey_MediaType.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						MediaPrimaryKey key = new MediaPrimaryKey((System.Int32)reader["MEDIA_ID"]);
						if (hashKey_Media.ContainsKey(key) == false)
						{
							hashKey_Media.Add(key, key.MediaId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}
		public virtual void LoadRecords_FK_SESSION_OPERATOR(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[SESSION_ID] FROM [dbo].[SESSION] t1 inner join [dbo].[OPERATOR] t2 on t1.[OPERATOR_ID]=t2.[OPERATOR_ID] WHERE t2.[OPERATOR_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Operator.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						SessionPrimaryKey key = new SessionPrimaryKey((System.Int32)reader["SESSION_ID"]);
						if (hashKey_Session.ContainsKey(key) == false)
						{
							hashKey_Session.Add(key, key.SessionId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}
		public virtual void LoadRecords_FK_TRAIL_SESSION(IDbConnection connection, IDbTransaction transaction)
		{

			// select with narrow primary key
			StringBuilder sqlText = new StringBuilder(1024);
			sqlText.Append("SELECT t1.[TRAIL_ID] FROM [dbo].[TRAIL] t1 inner join [dbo].[SESSION] t2 on t1.[SESSION_ID]=t2.[SESSION_ID] WHERE t2.[SESSION_ID] in (");
			bool first = true;
			foreach (int pk in hashKey_Session.Values)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					sqlText.Append(", ");
				}
				sqlText.Append(pk);
			}
			sqlText.Append(")");
			Debug.Assert(connection.State == ConnectionState.Open);
			using (IDbCommand cmd = Globals.TssDatabaseManager.DefaultDataBase.Command(sqlText.ToString(), connection, transaction))
			{
				cmd.CommandType = CommandType.Text;
				Debug.Assert(connection.State == ConnectionState.Open);
				using (IDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						TrailPrimaryKey key = new TrailPrimaryKey((System.Int32)reader["TRAIL_ID"]);
						if (hashKey_Trail.ContainsKey(key) == false)
						{
							hashKey_Trail.Add(key, key.TrailId);
						}
					}
					Debug.Assert(connection.State == ConnectionState.Open);
				}
			}
			Debug.Assert(connection.State == ConnectionState.Open);

		}
	}
}