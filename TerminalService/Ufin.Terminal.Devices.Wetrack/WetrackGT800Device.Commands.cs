﻿using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.Common;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace Compass.Ufin.Terminal.Devices.Wetrack
{
	public partial class WetrackGT800Device
	{
		public override List<CommandStep> GetCommandSteps(IStdCommand command)
		{
			var apnCfg = GetInternetApnConfig(command);
			var srvAdd = Manager.GetConstant(Constant.ServerIP);
			var srvPrt = Manager.GetConstant(Constant.ServerPort);
			var rplPhn = ContactHelper.GetNormalizedPhone(command.GetParamValue(PARAMS.Keys.ReplyToPhone));
			switch (command.Target.DeviceType)
			{
				case DeviceNames.WetrackGT800:
					return new[]
					{
						new { Cmd = CmdType.Setup, Steps = new []
							{
								new { Wait = false, Text = $@"APN,{apnCfg.Name},{apnCfg.User},{apnCfg.Pass}#", },
								new { Wait = false, Text = $@"CENTER,A,+{rplPhn}#",                            },
								new { Wait = false, Text = $@"TIMER,60#",                                      },
								new { Wait = false, Text = $@"GMT,E,0#",                                       },
								new { Wait = false, Text = $@"SERVER,0,{srvAdd},{srvPrt}#",                    },
							}},
						new { Cmd = CmdType.SetInterval,    Steps = new [] { new { Wait = false, Text = $@"TIMER,60#",           }, }, },
						new { Cmd = CmdType.Status,         Steps = new [] { new { Wait = false, Text = $@"STATUS#",             }, }, },
						new { Cmd = CmdType.SetSos,         Steps = new [] { new { Wait = false, Text = $@"SOS,A,+79991112233#", }, }, },
						new { Cmd = CmdType.DeleteSOS,      Steps = new [] { new { Wait = false, Text = $@"SOS,D,+79991112233#", }, }, },
						new { Cmd = CmdType.AskPosition,    Steps = new [] { new { Wait = false, Text = $@"URL#",                }, }, },
						new { Cmd = CmdType.ReloadDevice,   Steps = new [] { new { Wait = false, Text = $@"RESET#",              }, }, },
						new { Cmd = CmdType.Immobilize,     Steps = new [] { new { Wait = false, Text = $@"RELAY,0#",            }, }, },
						new { Cmd = CmdType.Deimmobilize,   Steps = new [] { new { Wait = false, Text = $@"RELAY,1#",            }, }, },
					}
						?.FirstOrDefault(x => x.Cmd == command.Type)
						?.Steps
						?.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						?.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						?.ToList<CommandStep>();
				default:
					return base.GetCommandSteps(command);
			}
		}
	}
}