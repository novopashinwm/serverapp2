﻿namespace Compass.Ufin.Terminal.Devices.Wetrack
{
	public enum WetrackGT800Sensors
	{
		Sos            = 01,
		Ignition       = 02,
		GsmSignalLevel = 03,
		DigitalInput1  = 04,
		DigitalInput2  = 05,
		DigitalInput3  = 06,
		DigitalInput4  = 07,
		DigitalOutput1 = 08,
		DigitalOutput2 = 09,
		AnalogInput    = 10,
	}
}