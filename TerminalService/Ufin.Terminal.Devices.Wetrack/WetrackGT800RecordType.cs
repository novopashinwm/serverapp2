﻿namespace Compass.Ufin.Terminal.Devices.Wetrack
{
	public enum WetrackGT800RecordType
	{
		LGN, // Log in package
		NRM, // Locate/Alarm data packet
		HBT, // Device status information packet (heartbeat packets)
		EPB, // Emergency alarm package(Government Server)
	}
}