﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using FORIS.TSS.Common;

namespace Compass.Ufin.Terminal.Devices.Wetrack
{
	/// <summary> Запись в формате Jimi (HBT) </summary>
	public sealed class WetrackGT800RecordHBT
	{
		/// <summary> Идентификатор устройства </summary>
		public string DeviceId { get; private set; }
		/// <summary> Прошивка </summary>
		public string Firmware { get; private set; }
		private WetrackGT800RecordHBT() { }
		public static WetrackGT800RecordHBT Parse(WetrackGT800Record record)
		{
			var result = default(WetrackGT800RecordHBT);
			var length = record?.RecVals?.Length ?? 0;
			if (1 < length)
			{
				result = new WetrackGT800RecordHBT
				{
					Firmware = record.RecVals[0],
					DeviceId = record.RecVals[1],
				};
			}
			return result;
		}
	}
}