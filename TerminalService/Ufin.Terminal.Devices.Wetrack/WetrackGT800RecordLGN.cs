﻿using System.Globalization;
using FORIS.TSS.Common;

namespace Compass.Ufin.Terminal.Devices.Wetrack
{
	/// <summary> Запись в формате WetrackGT800 (LGN) </summary>
	public sealed class WetrackGT800RecordLGN
	{
		/// <summary> Номер машины прописанный в трекере </summary>
		public string   VehicleNo { get; private set; }
		/// <summary> Идентификатор устройства </summary>
		public string   DeviceId  { get; private set; }
		/// <summary> Прошивка </summary>
		public string   Firmware  { get; private set; }
		/// <summary> Протокол </summary>
		public string   Protocol  { get; private set; }
		/// <summary> Географическая широта мобильного объекта (градусы) </summary>
		public decimal? Lat      { get; private set; }
		/// <summary> Географическая долгота мобильного объекта (градусы) </summary>
		public decimal? Lng      { get; private set; }
		private WetrackGT800RecordLGN() { }
		public static WetrackGT800RecordLGN Parse(WetrackGT800Record record)
		{
			var result = default(WetrackGT800RecordLGN);
			var length = record?.RecVals?.Length ?? 0;
			if (1 < length)
			{
				result = new WetrackGT800RecordLGN
				{
					VehicleNo = record.RecVals[0],
					DeviceId  = record.RecVals[1],
				};
				if (2 < length)
					result.Firmware = record.RecVals[2];
				if (3 < length)
					result.Protocol = record.RecVals[3];
				if (8 == length)
				{
					var latD = decimal.Parse(record.RecVals[4], NumberStyles.Number, NumberHelper.FormatInfo);
					var latS = record.RecVals[5] == "N" ? +1 : -1;
					result.Lat = latS * latD;
					var lngD = decimal.Parse(record.RecVals[6], NumberStyles.Number, NumberHelper.FormatInfo);
					var lngS = record.RecVals[7] == "E" ? +1 : -1;
					result.Lng = lngS * lngD;
				}
			}
			return result;
		}
	}
}