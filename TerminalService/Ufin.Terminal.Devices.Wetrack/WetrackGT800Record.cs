﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Compass.Ufin.Terminal.Devices.Wetrack
{
	/// <summary> Запись в формате WetrackGT800 </summary>
	public sealed class WetrackGT800Record
	{
		private WetrackGT800Record() { }
		/// <summary> Тип записи </summary>
		public string   RecType     { get; private set; }
		/// <summary> Производитель </summary>
		public string   RecVendorId { get; private set; }
		/// <summary> Тело записи после типа и до завершающего символа </summary>
		public string   RecBody     { get; private set; }
		/// <summary> Массив значений параметров из RecBody разделитель запятая </summary>
		public string[] RecVals     { get { return null != RecBody ? RecBody.Split(',') : new string[0]; } }
		/// <summary> Returns a <see cref="string" /> that represents this instance. </summary>
		/// <returns> A <see cref="string" /> that represents this instance. </returns>
		public override string ToString()
		{
			return $"${RecType},{RecVendorId},{RecBody}*";
		}
		/// <summary> Регулярное выражение разбора строки(записи) </summary>
		private static readonly Regex JimiRecordAllRegex = new Regex(string.Empty
			+ $@"\$"
			+ $@"(?<{nameof(RecType)}>[^,]*?),"     // Тип записи (любые символы до запятой)
			+ $@"(?<{nameof(RecVendorId)}>[^,]*?)," // Производитель (любые символы до запятой)
			+ $@"(?<{nameof(RecBody)}>[^\*]*?)"     // Body (любые символы до звездочки)
			+ $@"\*",
			RegexOptions.Multiline | RegexOptions.Compiled);
		/// <summary> Проверить, содержит ли входной текст записи формата Jimi </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public static bool IsMatch(string data)
		{
			return JimiRecordAllRegex.IsMatch(data);
		}
		/// <summary> Разобрать входную строку на записи формата Jimi </summary>
		/// <remarks> В качестве набора парсеров используется набор по умолчанию, полученный из потомков в этой сборке </remarks>
		/// <param name="text"> Входная строка </param>
		/// <returns> Набор классов парсеров с результатами</returns>
		public static IEnumerable<WetrackGT800Record> GetRecords(byte[] data, out byte[] bufferRest)
		{
			var text = Encoding.ASCII.GetString(data);
			var matches = JimiRecordAllRegex
				.Matches(text)
				.OfType<Match>()
				.OrderBy(m => m.Index);
			var lastMatch = matches.LastOrDefault();
			if (null != lastMatch)
				bufferRest = Encoding.ASCII.GetBytes(
					text.Substring(lastMatch.Index + lastMatch.Length));
			else
				bufferRest = Encoding.ASCII.GetBytes(text);
			return matches
				.Select(m => new WetrackGT800Record
				{
					RecType     = m.Groups[nameof(RecType)].Value,
					RecVendorId = m.Groups[nameof(RecVendorId)].Value,
					RecBody     = m.Groups[nameof(RecBody)].Value,
				});
		}
	}
}