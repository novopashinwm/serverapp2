﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace Compass.Ufin.Terminal.Devices.Wetrack
{
	public partial class WetrackGT800Device : Device
	{
		internal static class DeviceNames
		{
			internal const string WetrackGT800 = "Wetrack GT800 (AIS-140)";
		}
		public WetrackGT800Device() : base(typeof(WetrackGT800Sensors), new[]
		{
			DeviceNames.WetrackGT800,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			if (data == null)
				return false;
			/*
			$[Baotou],[Character content],<Character content><,Character content><,check>*
			$LGN,MARK,000000000,358980100039578,V0.0.1,AIS140,19.307249,N,72.900520,E*
			*/
			var strVal = Encoding.ASCII.GetString(data);

			if (!strVal.StartsWith("$") || !strVal.Contains("*") || !strVal.Contains(","))
				return false;

			if (!Enum.GetNames(typeof(WetrackGT800RecordType)).Any(r => strVal.StartsWith("$" + r)))
				return false;

			return WetrackGT800Record.IsMatch(strVal);
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			var results = new List<object>();
			var records = WetrackGT800Record.GetRecords(data, out bufferRest);
			foreach (var record in records)
			{
				var enmRecType = default(WetrackGT800RecordType);
				if (!Enum.TryParse(record.RecType, true, out enmRecType))
					continue;
				switch (enmRecType)
				{
					case WetrackGT800RecordType.LGN:
						results.AddRange(ParseRecordLGN(record));
						break;
					case WetrackGT800RecordType.NRM:
						results.AddRange(ParseRecordNRM(record));
						break;
					case WetrackGT800RecordType.HBT:
						results.AddRange(ParseRecordHBT(record));
						break;
					case WetrackGT800RecordType.EPB:
						results.AddRange(ParseRecordEPB(record));
						break;
					default:
						break;
				}
			}
			return results;
		}
		public IEnumerable<object> ParseRecordLGN(WetrackGT800Record record)
		{
			var results = new List<object>();
			var pars = WetrackGT800RecordLGN.Parse(record);
			if (null != pars)
			{
				results.Add(new ConfirmPacket($@"${record.RecType},{DateTime.UtcNow:ddMMyyyyHHmmss}*"));
				results.Add(new ReceiverStoreToStateMessage
				{
					StateData = new ReceiverState { DeviceID = pars.DeviceId }
				});
			}
			return results;
		}
		public IEnumerable<object> ParseRecordNRM(WetrackGT800Record record)
		{
			var results = new List<object>();
			var pars = WetrackGT800RecordNRM.Parse(record);
			if (null != pars)
			{
				var mu = UnitReceived(nameof(WetrackGT800Device));
				mu.DeviceID     = pars.DeviceId;
				mu.cmdType      = CmdType.Trace;
				mu.Time         = TimeHelper.GetSecondsFromBase(pars.Timestamp);
				mu.Latitude     = MobilUnit.InvalidLatitude;
				mu.Longitude    = MobilUnit.InvalidLongitude;
				mu.CorrectGPS   = pars.Valid;
				if (pars.Valid)
				{
					mu.Latitude  = (double)pars.Lat;
					mu.Longitude = (double)pars.Lng;
					mu.Speed     = (int?)pars.Speed;
					mu.Course    = (int?)pars.Course;
					if (pars.Altitude.HasValue)
						mu.Height = (int)(pars.Altitude.Value * 100.0M);
					if (pars.Satellites.HasValue)
						mu.Satellites = pars.Satellites.Value;
				}
				mu.SetSensorValue((int)WetrackGT800Sensors.Sos,            pars.EmergencyStatus);
				mu.SetSensorValue((int)WetrackGT800Sensors.Ignition,       pars.Ignition);
				mu.SetSensorValue((int)WetrackGT800Sensors.GsmSignalLevel, pars.GsmSignalLevel);
				mu.SetSensorValue((int)WetrackGT800Sensors.DigitalInput1,  pars.DigitalInput1);
				mu.SetSensorValue((int)WetrackGT800Sensors.DigitalInput2,  pars.DigitalInput2);
				mu.SetSensorValue((int)WetrackGT800Sensors.DigitalInput3,  pars.DigitalInput3);
				mu.SetSensorValue((int)WetrackGT800Sensors.DigitalInput4,  pars.DigitalInput4);
				mu.SetSensorValue((int)WetrackGT800Sensors.DigitalOutput1, pars.DigitalOutput1);
				mu.SetSensorValue((int)WetrackGT800Sensors.DigitalOutput2, pars.DigitalOutput2);
				if (pars.AnalogInput.HasValue)
					mu.SetSensorValue((int)WetrackGT800Sensors.AnalogInput, (long)(pars.AnalogInput.Value * 1000M));
				results.Add(mu);
			}
			return results;
		}
		public IEnumerable<object> ParseRecordHBT(WetrackGT800Record record)
		{
			var results = new List<object>();
			var pars = WetrackGT800RecordHBT.Parse(record);
			if (null != pars)
				results.Add(new ConfirmPacket($@"${record.RecType}*"));
			return results;
		}
		public IEnumerable<object> ParseRecordEPB(WetrackGT800Record record)
		{
			var results = new List<object>();
			//var pars = JimiRecordEPB.Parse(record.RecBody);
			//if (null != pars)
			//{
			//	var mu = UnitReceived();
			//	mu.DeviceID     = record.RecCode;
			//	mu.cmdType      = CmdType.Trace;
			//	mu.Time         = TimeHelper.GetSecondsFromBase(pars.Timestamp);
			//	mu.Latitude     = MobilUnit.InvalidLatitude;
			//	mu.Longitude    = MobilUnit.InvalidLongitude;
			//	mu.CorrectGPS   = pars.Valid;
			//	if (pars.Valid)
			//	{
			//		mu.Latitude  = (double)pars.Lat;
			//		mu.Longitude = (double)pars.Lng;
			//		mu.Speed     = (int?)pars.Speed;
			//		mu.Course    = (int?)pars.Course;
			//	}
			//	results.Add(mu);
			//	results.Add(new ConfirmPacket("ok"));
			//}
			return results;
		}
	}
}