﻿using System;
using System.Text.RegularExpressions;

namespace Compass.Ufin.Terminal.Devices.Wetrack
{
	/// <summary> Запись в формате WetrackGT800 (EPB) </summary>
	public sealed class WetrackGT800RecordEPB
	{
		/// <summary> Временная метка </summary>
		public DateTime Timestamp       { get; private set; }
		/// <summary> Уровень заряда батареи </summary>
		public long?    BatteryLevel    { get; private set; }
		/// <summary> Напряжение батареи (в 100*В, сотых вольта) </summary>
		public long?    BatteryVoltage  { get; private set; }
		/// <summary> Внешнее напряжение (в 10*В, десятых вольта)</summary>
		public long?    ExternalVoltage { get; private set; }
		/// <summary> Что-то установлено </summary>
		public long?    Installed       { get; private set; }
		private WetrackGT800RecordEPB() { }
		/// <summary> Регулярное выражение разбора строки команды </summary>
		private static readonly Regex JimiRecordFormat05Regex = new Regex(string.Empty
			+ $@"^"
			+ $@"(\d{{6}})," // Дата  в формате: ddMMyy
			+ $@"(\d{{6}})," // Время в формате: HHmmss
			+ $@"(\d+),"     // "6" stands for battery level, range is 0 - 6，255 refers to invalid rate
			+ $@"(\d+),"     // "421" stands for battery voltage, eg 4.21V，65535 refers to invalid rate
			+ $@"(\d+),"     // "112" stands for battery input voltage, eg11.2V，65535 refers to invalid rate
			+ $@"(\d+)"      // "0" stands for not installed(0) or installed(1), 255 refers to invalid rate
			+ $@"$",
			RegexOptions.Multiline | RegexOptions.Compiled);
		public static WetrackGT800RecordEPB Parse(WetrackGT800Record record)
		{
			var result = default(WetrackGT800RecordEPB);
			//var match = JimiRecordFormat05Regex
			//	.Match(data);
			//if (!match.Success)
			//	return result;
			//var values = match
			//	.Groups
			//	.OfType<Group>()
			//	.Skip(1)
			//	.Select(g => g.Value)
			//	.ToArray();
			//if (null != values && 6 == values.Length)
			//{
			//	var date   = DateTime.ParseExact(values[0], "ddMMyy",
			//		CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal);
			//	var time   = DateTime.ParseExact(values[1], "HHmmss",
			//		CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal);
			//
			//	var blev = long.Parse(values[2]);
			//	var bvol = long.Parse(values[3]);
			//	var evol = long.Parse(values[4]);
			//	var inst = long.Parse(values[5]);
			//	result = new WetrackGT800RecordEPB
			//	{
			//		Timestamp       = date.Add(time - time.Date),
			//		BatteryLevel    = 00255 != blev ? blev         : (long?)null,
			//		BatteryVoltage  = 65535 != bvol ? bvol         : (long?)null,
			//		ExternalVoltage = 65535 != evol ? evol         : (long?)null,
			//		Installed       = 00255 != inst ? (inst & 0x1) : (long?)null,
			//	};
			//}
			return result;
		}
	}
}