﻿namespace FORIS.TSS.Terminal.GlobalSat.TR203
{
    public enum TR203Sensor
    {
        /// <summary>Емкость батареи в процентах</summary>
        BatteryCapacity = 0,
        /// <summary>Температура</summary>
        Temperature = 2,
        /// <summary>Статус: SOS</summary>
        SOSAlarm = 3,
        /// <summary>Статус: парковка</summary>
        ParkingAlarm = 4,
        /// <summary>Статус: спящий режим</summary>
        SleepingAlarm = 5,
        /// <summary>Статус: геозона</summary>
        GeoFenceAlarm = 6,
        /// <summary>Статус: превышение скорости</summary>
        SpeedAlarm = 7,
        /// <summary>Статус: низкий заряд батареи</summary>
        BatteryLowAlarm = 8,
        /// <summary>Статус: сигнализация входа/выхода из геозоны</summary>
        AutonomousGeoFenceAlarm = 9,
        /// <summary>Статус: Отключена главная батарея питания</summary>
        BatteryDisconnectionAlarm = 10,

        //Цифровые датчики
        DS1 = 21,
        DS2 = 22,
        DS3 = 23,
        /// <summary>Зажигание</summary>
        ACC = 24,

        //Цифровые выходы
        DOut1 = 31,
        DOut2 = 32,
        DOut3 = 33,

        /// <summary>Аналоговый вход 0</summary>
        AN0 = 41,
        /// <summary>Напряжение батареи, mV</summary>
        BatteryVoltage = 42,

        /// <summary>
        /// HDOP, see wikipedia for details
        /// </summary>
        Hdop = 43,

        /// <summary>
        /// 1=Sleeping
        /// 2=Periodic
        /// 3=On-line
        /// 4=Motion 
        /// </summary>
        OperationMode = 44,

        /// <summary>
        /// Hex value,
        /// 1=Ping report
        ///2=Periodic mode report
        ///3=On-line mode report
        ///4=Motion mode static report
        ///5=Motion mode moving report
        ///6=Motion mode static to moving report
        ///7=Motion mode moving to static report
        ///C=Parking mode report
        ///D=Parking mode alarm report
        ///E=Sleeping mode report
        /// </summary>
        ReportType = 45,
    }
}
