﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.GlobalSat.TR203
{
	class GlobalSatConfiguredParser : GlobalSatParser
	{
		private class ParseFieldAction
		{
			public readonly string Name;
			public readonly char CodingLetter;
			public readonly Action<IMobilUnit, TR203Reader> Do;

			public ParseFieldAction(char codingLetter, string name, Action<IMobilUnit, TR203Reader> @do)
			{
				Name = name;
				CodingLetter = codingLetter;
				Do = @do;
			}

			public override string ToString()
			{
				return Name;
			}
		}

		static GlobalSatConfiguredParser()
		{
			CodewordToParser = new[]
				{
					new ParseFieldAction('A', "GPSFixingMode", ParseGPSFixingMode),
					new ParseFieldAction('B', "UTCDateTime", ParseUTCDateTime),
					new ParseFieldAction('C', "LocalDateTime", ParseLocalDateTime),
					new ParseFieldAction('1', "LongitudeDegrees", ParseLongitudeDegrees),
					new ParseFieldAction('2', "LongitudeDegreesMinutes", ParseLongitudeDegreesMinutes),
					new ParseFieldAction('3', "LongitudeDegreesParts", ParseLongitudeDegreesParts),
					new ParseFieldAction('6', "LatitudeDegrees", ParseLatitudeDegrees),
					new ParseFieldAction('7', "LatitudeDegreesMinutes", ParseLatitudeDegreesMinutes),
					new ParseFieldAction('8', "LatitudeDegreesParts", ParseLatitudeDegreesParts),
					new ParseFieldAction('G', "Altitude", ParseAltitude),
					new ParseFieldAction('H', "SpeedKnots", ParseSpeedKnots),
					new ParseFieldAction('I', "SpeedKmPH", ParseSpeedKmPH),
					new ParseFieldAction('J', "SpeedMilePH", ParseSpeedMilePH),
					new ParseFieldAction('K', "Heading", ParseHeading),
					new ParseFieldAction('L', "SatellitesInUse", ParseSatellitesInUse),
					new ParseFieldAction('M', "HDOP", ParseHDOP),
					new ParseFieldAction('N', "BatteryCapacity", ParseBatteryCapacity),
					new ParseFieldAction('O', "Operation mode", GetSensorParser(TR203Sensor.OperationMode)),
					new ParseFieldAction('P', "AlarmStatus", ParseAlarmStatus),
					new ParseFieldAction('Z', "Geo-fence status", IgnoreParameter),
					new ParseFieldAction('Q', "Q", IgnoreParameter),
					new ParseFieldAction('R', "Report type", ParseReportType),
					new ParseFieldAction('S', "ParseIMEI", ParseIMEI),
					new ParseFieldAction('T', "T", ParseDeviceInfo),
					new ParseFieldAction('U', "U", IgnoreParameter),
					new ParseFieldAction('V', "DigitalInputStatus", ParseDigitalInputStatus),
					new ParseFieldAction('W', "DigitalOutputsStatus", ParseDigitalOutputsStatus),
					new ParseFieldAction('X', "IgnoreParameter", IgnoreParameter),
					new ParseFieldAction('Y', "JointIOStatis", ParseJointIOStatis),
					new ParseFieldAction('a', "AN0", ParseAN0),
					new ParseFieldAction('e', "e", IgnoreParameter),
					new ParseFieldAction('f', "f", IgnoreParameter),
					new ParseFieldAction('g', "g", IgnoreParameter),
					new ParseFieldAction('h', "h", IgnoreParameter),
					new ParseFieldAction('i', "i", IgnoreParameter),
					new ParseFieldAction('m', "MainBatteryVoltage", ParseMainBatteryVoltage),
					new ParseFieldAction('n', "n", ParseMainBatteryVoltage),
					new ParseFieldAction('o', "o", IgnoreParameter),
					new ParseFieldAction('p', "Temperature", ParseTemperature),
					new ParseFieldAction('s', "IMSI", ReadImsi),
					new ParseFieldAction('t', "MCC", ReadMCC),
					new ParseFieldAction('u', "MNC", ReadMNC),
					new ParseFieldAction('v', "LAC", ReadLAC),
					new ParseFieldAction('w', "CID", ReadCID),
					new ParseFieldAction('x', "BSIC", IgnoreParameter),
					new ParseFieldAction('y', "RSSI", ReadRSSI),
					new ParseFieldAction('z', "Cell ID (All)", ReadCellIDAll),
					new ParseFieldAction('?', "Unknown to be ignored", IgnoreParameter)

				}.ToDictionary(x => x.CodingLetter);

		}

		private static void ParseReportType(IMobilUnit mu, TR203Reader reader)
		{
			var reportType = ToReportType(reader.Read());

			if (reportType != null)
				mu.SensorValues[(int) TR203Sensor.ReportType] = (int) reportType;
		}

		private static GlobalSatReportType? ToReportType(string reportType)
		{
			switch (reportType)
			{
				case "1": return GlobalSatReportType.PingReport;
				case "4": return GlobalSatReportType.MotionModeStaticReport;
				case "5": return GlobalSatReportType.MotionModeMovingReport;
				case "6": return GlobalSatReportType.MotionModeStaticToMovingReport;
				case "7": return GlobalSatReportType.MotionModeMovingToStaticReport;
				case "8": return GlobalSatReportType.AngleChangeReport;
				case "E": return GlobalSatReportType.ErrorReport;
				case "G": return GlobalSatReportType.GeofenceAlarmReport;
				case "H": return GlobalSatReportType.AutonomousGeofenceAlarmReport;
				case "I": return GlobalSatReportType.SosAlarmReport;
				case "K": return GlobalSatReportType.SpeedAlarmReport;
				case "L": return GlobalSatReportType.Timer0Report;
				case "M": return GlobalSatReportType.Timer1Report;
				case "N": return GlobalSatReportType.Timer2Report;
				case "P": return GlobalSatReportType.L4Report;
				case "Q": return GlobalSatReportType.Stopwatch0Report;
				case "R": return GlobalSatReportType.Stopwatch1Report;
				case "V": return GlobalSatReportType.DigitalInputReport;
				case "a": return GlobalSatReportType.AnalogInputReport;
				case "e": return GlobalSatReportType.Counter0Report;
				case "f": return GlobalSatReportType.Counter1Report;
				case "i": return GlobalSatReportType.OdometerReport;
				case "j": return GlobalSatReportType.AccReport;
				case "l": return GlobalSatReportType.MainBatteryDisconnected;
				case "m": return GlobalSatReportType.MainBatteryLowAlarmReport;
				case "o": return GlobalSatReportType.OtaDownloadComplete;
				case "p": return GlobalSatReportType.OtaDownloadFail;
				case "q": return GlobalSatReportType.ParkingAlarmReport;
				case "t": return GlobalSatReportType.JammerReport;
				case "v": return GlobalSatReportType.PowerOnReport;
				case "w": return GlobalSatReportType.FtpDownloadSuccessReport;
				case "x": return GlobalSatReportType.FtpDownloadFailReport;
				case "y": return GlobalSatReportType.FtpUploadSuccessReport;
				case "z": return GlobalSatReportType.FtpUploadFailReport;
					default:
					return null;
			}
		}

		private static void ReadImsi(IMobilUnit mu, TR203Reader reader)
		{
			mu.IMSI = reader.Read();
		}

		private static Action<IMobilUnit, TR203Reader> GetSensorParser(TR203Sensor sensor)
		{
			return (unit, reader) =>
				{
					var s = reader.Read();
					int i;
					if (!int.TryParse(s, out i))
						return;
					
					if (unit.SensorValues == null)
						unit.SensorValues = new Dictionary<int, long>();

					unit.SensorValues[(int)sensor] = i;
				};
		}

		private static readonly Dictionary<char, ParseFieldAction> CodewordToParser;


		
		private readonly List<ParseFieldAction> _parsers;
		private readonly int _supportedPacketLength;
		private readonly string _configuration;

		public GlobalSatConfiguredParser(string configuration)
		{
			_configuration = configuration;
			_parsers = new List<ParseFieldAction>(configuration.Length);
			foreach (var codeword in configuration)
			{
				switch (codeword)
				{
					case '*':
					case 'U':
					case '!':
						continue;
					case 'B':
					case 'C':
						//Так как эти два параметра передаются в виде дата, время
						_supportedPacketLength += 2;
						break;
					case 'z':
						//Sample: "250,01,4fc5,2e9b,41,14"
						_supportedPacketLength += 6;
						break;
					default:
						_supportedPacketLength++;
						break;
				}


				ParseFieldAction parser;

				if (!CodewordToParser.TryGetValue(codeword, out parser))
				{
					parser = new ParseFieldAction(codeword, "Unknown", IgnoreParameter);
					Trace.TraceWarning("TR203: Unknown codeword: " + codeword);
				}
				_parsers.Add(parser);
			}

			//_supportedPacketLength--;
		}

		public int SupportedPacketLength
		{
			get {
				return _supportedPacketLength; 
			}
		}

		public override void Parse(IMobilUnit mobilUnit, TR203Reader reader)
		{
			foreach (var parser in _parsers)
				parser.Do(mobilUnit, reader);
		}

		public string Configuration { get { return _configuration; } }

		public string FormatDescription
		{
			get
			{
				return _parsers.Select(p => p.Name).Join(",");
			}
		}

		public override string ToString()
		{
			return string.Format("GlobalSatConfiguredParser: Length:{0}, config:{1}", SupportedPacketLength, _configuration);
		}
	}
}