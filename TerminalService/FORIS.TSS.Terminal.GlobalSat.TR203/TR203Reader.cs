﻿namespace FORIS.TSS.Terminal.GlobalSat.TR203
{
    class TR203Reader
    {
        private readonly string[] _fields;
        private int _position;

        public TR203Reader(string message)
        {
            _fields = message.Split(',');
            _position = -1;
        }

        public string Read()
        {
            if (_position == _fields.Length)
                return null;
            ++_position;
            return _position == _fields.Length ? null : _fields[_position];
        }
    }
}
