﻿using System;
using System.Globalization;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.Common;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.GlobalSat.TR203
{
    abstract class GlobalSatParser
    {
        public abstract void Parse(IMobilUnit mobilUnit, TR203Reader reader);

        protected static readonly NumberFormatInfo NumberFormatInfo = new NumberFormatInfo { NumberDecimalSeparator = "." };

        protected static void ParseHDOP(IMobilUnit mobilUnit, TR203Reader reader)
        {
            var valueString = reader.Read();
            var hdop = decimal.Parse(valueString, NumberFormatInfo); //xx.x
            mobilUnit.SensorValues.Add((int)TR203Sensor.Hdop, (long)(hdop * 10));
        }

        protected static void IgnoreParameter(IMobilUnit mobilUnit, TR203Reader reader)
        {
            reader.Read();
        }

        protected static void ParseGPSFixingMode(IMobilUnit mobilUnit, TR203Reader reader)
        {
            var fix = reader.Read();
            if (fix == "1" && mobilUnit is MobilUnit.Unit.MobilUnit)
            {
                (mobilUnit as MobilUnit.Unit.MobilUnit).CorrectGPS = false;
            }
        }

        protected static void ParseUTCDateTime(IMobilUnit mobilUnit, TR203Reader reader)
        {
            var ddmmyy = reader.Read();
            var hhmmss = reader.Read();

            DateTime dateTime;
            if (!DateTime.TryParseExact(ddmmyy + hhmmss, "ddMMyyHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime)) 
                throw new FormatException("Unable to parse date and time: " + ddmmyy + hhmmss);

            mobilUnit.Time = TimeHelper.GetSecondsFromBase(dateTime);
        }

        protected static void ParseLocalDateTime(IMobilUnit mobilUnit, TR203Reader reader)
        {
            reader.Read();
            reader.Read();
        }

        protected static void ParseLongitudeDegrees(IMobilUnit mobilUnit, TR203Reader reader)
        {
            //Format: (E or W)ddd.dddddd
            var s = reader.Read();
            var signChar = s[0];
            var valueString = s.Substring(1);
            var unsignedValue = double.Parse(valueString, NumberFormatInfo);
            mobilUnit.Longitude = signChar == 'E' ? unsignedValue : -unsignedValue;
        }

        protected static void ParseLongitudeDegreesMinutes(IMobilUnit mobilUnit, TR203Reader reader)
        {
            //Format: (E or W)dddmm.mmmm
            var s = reader.Read();
            ParseLongitudeDegreesMinutes(mobilUnit, s);
        }

        protected static void ParseLongitudeDegreesMinutes(IMobilUnit mobilUnit, string s)
        {
            var signChar = s[0];
            var valueString = s.Substring(1);
            var rawValue = decimal.Parse(valueString, NumberFormatInfo);
            var degrees = Math.Truncate(rawValue/100);
            var minutes = rawValue - degrees*100;
            var unsignedValue = (double) (degrees + minutes/60);
            mobilUnit.Longitude = signChar == 'E' ? unsignedValue : -unsignedValue;
        }

        protected static void ParseLongitudeDegreesParts(IMobilUnit mobilUnit, TR203Reader reader)
        {
            //(+ or -)ddddddddd
            //unit: 0.000001 degree
            var s = reader.Read();
            var i = int.Parse(s);
            mobilUnit.Longitude = 0.000001 * i;
        }

        protected static void ParseLatitudeDegrees(IMobilUnit mobilUnit, TR203Reader reader)
        {
            //Format: (N or S)dd.dddddd
            var s = reader.Read();
            ParseLatitudeDegrees(mobilUnit, s);
        }

        protected static void ParseLatitudeDegrees(IMobilUnit mobilUnit, string s)
        {
            var signChar = s[0];
            var valueString = s.Substring(1);
            var unsignedValue = double.Parse(valueString, NumberFormatInfo);
            mobilUnit.Latitude = signChar == 'N' ? unsignedValue : -unsignedValue;
        }

        protected static void ParseLatitudeDegreesMinutes(IMobilUnit mobilUnit, TR203Reader reader)
        {
            //Format: (N or S)ddmm.mmmm
            var s = reader.Read();
            ParseLatitudeDegreesMinutes(mobilUnit, s);
        }

        protected static void ParseLatitudeDegreesMinutes(IMobilUnit mobilUnit, string s)
        {
            var signChar = s[0];
            var valueString = s.Substring(1);
            var rawValue = decimal.Parse(valueString, NumberFormatInfo);
            var degrees = Math.Truncate(rawValue/100);
            var minutes = (rawValue - degrees*100);
            var unsignedValue = (double) (degrees + minutes/60);
            mobilUnit.Latitude = signChar == 'N' ? unsignedValue : -unsignedValue;
        }

        protected static void ParseLatitudeDegreesParts(IMobilUnit mobilUnit, TR203Reader reader)
        {
            //(+ or -)ddddddddd
            //unit: 0.000001 degree
            var s = reader.Read();
            var i = int.Parse(s);
            mobilUnit.Latitude = 0.000001 * i;
        }

        protected static void ParseAltitude(IMobilUnit mobilUnit, TR203Reader reader)
        {
            //xxxxx.x
            //Unit: meter
            var s = reader.Read();
            var altitudeInMeters = decimal.Parse(s, NumberFormatInfo);
            mobilUnit.Height = (int)(altitudeInMeters * 100);
        }

        protected static void ParseSpeedKnots(IMobilUnit mobilUnit, TR203Reader reader)
        {
            //xxx.xx
            //unit: knots (1.852km/hr)
            var s = reader.Read();
            var speedInKnots = decimal.Parse(s, NumberFormatInfo);
            mobilUnit.Speed = MeasureUnitHelper.KnotsToKMPH((double) speedInKnots);
        }

        protected static void ParseSpeedKmPH(IMobilUnit mobilUnit, TR203Reader reader)
        {
            //xxx
            //unit: km/hr        
            var s = reader.Read();
            mobilUnit.Speed = int.Parse(s, NumberFormatInfo);
        }

        protected static void ParseSpeedMilePH(IMobilUnit mobilUnit, TR203Reader reader)
        {
            //xxx
            //unit: mile/hr
            var s = reader.Read();
            var speedInMilePerHour = int.Parse(s);
            mobilUnit.Speed = (int)(speedInMilePerHour * 1.609344);
        }

        protected static void ParseHeading(IMobilUnit mobilUnit, TR203Reader reader)
        {
            //xxx
            //unit: degree
            var s = reader.Read();
            mobilUnit.Course = int.Parse(s);
        }

        protected static void ParseSatellitesInUse(IMobilUnit mobilUnit, TR203Reader reader)
        {
            var s = reader.Read();
            mobilUnit.Satellites = int.Parse(s);
        }

        protected static void ParseBatteryCapacity(IMobilUnit mobilUnit, TR203Reader reader)
        {
            //xx
            //unit: percent capacity
            var s = reader.Read();

            long value;
            if (long.TryParse(s, out value))
                mobilUnit.SensorValues[(int)TR203Sensor.BatteryCapacity] = value;
        }


        protected static void ParseAlarmStatus(IMobilUnit mobilUnit, TR203Reader reader)
        {
            //xx
            //unit: percent capacity
            var s = reader.Read();
            var value = byte.Parse(s, NumberStyles.HexNumber);
            mobilUnit.SensorValues[(int)TR203Sensor.SOSAlarm] = (value & 0x01) != 0 ? 1 : 0;
            mobilUnit.SensorValues[(int)TR203Sensor.ParkingAlarm] = (value & 0x02) != 0 ? 1 : 0;
            mobilUnit.SensorValues[(int)TR203Sensor.AutonomousGeoFenceAlarm] = (value & 0x04) != 0 ? 1 : 0;
            mobilUnit.SensorValues[(int)TR203Sensor.SleepingAlarm] = (value & 0x08) != 0 ? 1 : 0;
            mobilUnit.SensorValues[(int)TR203Sensor.GeoFenceAlarm] = (value & 0x10) != 0 ? 1 : 0;
            mobilUnit.SensorValues[(int)TR203Sensor.SpeedAlarm] = (value & 0x20) != 0 ? 1 : 0;
            mobilUnit.SensorValues[(int)TR203Sensor.BatteryDisconnectionAlarm] = (value & 0x40) != 0 ? 1 : 0;
            mobilUnit.SensorValues[(int)TR203Sensor.BatteryLowAlarm] = (value & 0x80) != 0 ? 1 : 0;
        }


        protected static void ParseIMEI(IMobilUnit mobilUnit, TR203Reader reader)
        {
            var s = reader.Read();
            mobilUnit.DeviceID = s;
        }

        protected static void ParseTemperature(IMobilUnit mobilUnit, TR203Reader reader)
        {
            //xx
            //unit: percent capacity
            var s = reader.Read();
            var value = int.Parse(s);
            mobilUnit.SensorValues[(int)TR203Sensor.Temperature] = value;
            //mobilUnit.VoltageAN1 = value;
        }

        protected static void ParseDigitalInputStatus(IMobilUnit mobilUnit, TR203Reader reader)
        {
            /*
             xx (hex digits)
                bit0=
                bit1=Input 1
                bit2=Input 2
                bit3=Input 3
                bit4=
                bit5=
                bit6=
                bit7= 
             */
            var s = reader.Read();
            var value = byte.Parse(s, NumberStyles.HexNumber);

            mobilUnit.SensorValues[(int)TR203Sensor.DS1] = (value & 0x02) != 0 ? 1 : 0;
            mobilUnit.SensorValues[(int)TR203Sensor.DS2] = (value & 0x04) != 0 ? 1 : 0;
            mobilUnit.SensorValues[(int)TR203Sensor.DS3] = (value & 0x08) != 0 ? 1 : 0;
        }

        protected static void ParseDigitalOutputsStatus(IMobilUnit mobilUnit, TR203Reader reader)
        {
            /*
             xx (hex digits)
                bit0=
                bit1=Output 1
                bit2=Output 2
                bit3=Output 3
                bit4=
                bit5=
                bit6=
                bit7=
             */
            var s = reader.Read();
            var value = byte.Parse(s, NumberStyles.HexNumber);

            mobilUnit.SensorValues[(int)TR203Sensor.DOut1] = (value & 0x02) != 0 ? 1 : 0;
            mobilUnit.SensorValues[(int)TR203Sensor.DOut2] = (value & 0x04) != 0 ? 1 : 0;
            mobilUnit.SensorValues[(int)TR203Sensor.DOut3] = (value & 0x08) != 0 ? 1 : 0;
        }

        protected static void ParseJointIOStatis(IMobilUnit mobilUnit, TR203Reader reader)
        {
            /*
             xxxx (hex digits)
            bit0=
            bit1=Input 1
            bit2=Input 2
            bit3= Input 3
            bit4=
            bit5=
            bit6=
            bit7=Motion status, 0=static, 1=moving
            bit8=
            bit9=Output 1
            bit10=Output 2
            bit11= Output 3
            bit12=
            bit13=ACC зажигание
            bit14=GPS antenna connected
            bit15=Main battery connected
             */
            var s = reader.Read();
            var value = ushort.Parse(s, NumberStyles.HexNumber);

            mobilUnit.SensorValues[(int)TR203Sensor.DS1] = (value & 0x02) != 0 ? 1 : 0;
            mobilUnit.SensorValues[(int)TR203Sensor.DS2] = (value & 0x04) != 0 ? 1 : 0;
            mobilUnit.SensorValues[(int)TR203Sensor.DS3] = (value & 0x08) != 0 ? 1 : 0;
            mobilUnit.SensorValues[(int)TR203Sensor.ACC] = (value & (1 << 13)) != 0 ? 1 : 0;
            
            //Выходы
            mobilUnit.SensorValues[(int)TR203Sensor.DOut1] = (value & (1 << 9)) != 0 ? 1 : 0;
            mobilUnit.SensorValues[(int)TR203Sensor.DOut2] = (value & (1 << 10)) != 0 ? 1 : 0;
            mobilUnit.SensorValues[(int)TR203Sensor.DOut3] = (value & (1 << 11)) != 0 ? 1 : 0;
        }

        protected static void ParseAN0(IMobilUnit mobilUnit, TR203Reader reader)
        {
            var s = reader.Read();
            var value = ParseVoltage(s);
            mobilUnit.SensorValues[(int)TR203Sensor.AN0] = (long)value;
            //mobilUnit.VoltageAN1 = (int)value;
        }

        private static decimal ParseVoltage(string s)
        {
            var indexOfmV = s.IndexOf("mV", StringComparison.Ordinal);
                //NOTE: не уверен, но ранее будто видел в протоколе пресловутое mV
            var valueString = indexOfmV == -1 ? s : s.Substring(0, indexOfmV);
            var value = decimal.Parse(valueString, NumberFormatInfo);
            return value;
        }

        protected static void ParseMainBatteryVoltage(IMobilUnit mobilUnit, TR203Reader reader)
        {
            var s = reader.Read();

            // Когда отключено внешнее питание трекер передает уровень заряда батареи (с "%" на конце) в процентах
            if (s.EndsWith("%"))
            {
                s = s.TrimEnd('%');
                mobilUnit.SensorValues[(int) TR203Sensor.BatteryCapacity] = long.Parse(s);
                return;
            }

            mobilUnit.SensorValues[(int) TR203Sensor.BatteryVoltage] = (long) (ParseVoltage(s));
        }

        /*ReadMCC),
         *ReadMNC), 
         *ReadLAC), 
         *ReadCID), 
         * ReadRSSI)          
         */


        private delegate void ParseNetworkParameter(string s, ref CellNetworkRecord record);

        private static void ReadNetworkParameter(IMobilUnit mu, TR203Reader reader, ParseNetworkParameter parse)
        {
            var s = reader.Read();
            if (mu.CellNetworks == null)
                mu.CellNetworks = new CellNetworkRecord[1];
            parse(s, ref mu.CellNetworks[0]);
        }

        protected static void ReadMCC(IMobilUnit mu, TR203Reader reader)
        {
            ReadNetworkParameter(mu, reader, (string mcc, ref CellNetworkRecord record) => record.CountryCode = mcc);
        }

        protected static void ReadMNC(IMobilUnit mu, TR203Reader reader)
        {
            ReadNetworkParameter(mu, reader, (string mnc, ref CellNetworkRecord record) => record.NetworkCode = mnc);
        }

        protected static void ReadLAC(IMobilUnit mu, TR203Reader reader)
        {
            ReadNetworkParameter(mu, reader, (string lac, ref CellNetworkRecord record) =>
                {
                    int i;
                    if (!int.TryParse(lac, NumberStyles.HexNumber, NumberFormatInfo.InvariantInfo, out i))
                        return;
                    record.LAC = i;
                });
        }

        protected static void ReadCID(IMobilUnit mu, TR203Reader reader)
        {
            ReadNetworkParameter(mu, reader, (string cid, ref CellNetworkRecord record) =>
                {
                    int cellId;
                    if (!int.TryParse(cid, NumberStyles.HexNumber, NumberFormatInfo.InvariantInfo, out cellId))
                        return;
                    record.CellID = cellId;
                });
        }

        protected static void ReadRSSI(IMobilUnit mu, TR203Reader reader)
        {
            ReadNetworkParameter(mu, reader, (string cid, ref CellNetworkRecord record) =>
                {
                    record.SignalStrength = int.Parse(cid);
                });
        }

        protected static void ReadCellIDAll(IMobilUnit mu, TR203Reader reader)
        {
            //Sample string "250,01,4fc5,2e9b,41,14"

            var mcc = reader.Read().TrimStart('"');
            var mnc = reader.Read();
            var lacString = reader.Read();
            var lac = int.Parse(lacString, NumberStyles.HexNumber);
            var cellIdString = reader.Read();
            var cellId = int.Parse(cellIdString, NumberStyles.HexNumber);
            reader.Read(); //BSIC, skip
            var signalStrengthString = reader.Read().TrimEnd('"');
            var signalStrength = int.Parse(signalStrengthString);

            mu.CellNetworks = new[]
                {
                    new CellNetworkRecord
                        {
                            CountryCode = mcc,
                            NetworkCode = mnc,
                            CellID = cellId,
                            LAC = lac,
                            Number = 0,
                            SignalStrength = signalStrength,
                        }
                };
        }

        protected static void ParseDeviceInfo(IMobilUnit mu, TR203Reader reader)
        {
            //Sample string: GTR-128
            var s = reader.Read();

            mu.Firmware = s;
        }
    }
}
