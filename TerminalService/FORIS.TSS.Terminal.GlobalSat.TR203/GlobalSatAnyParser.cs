﻿using System;
using System.Globalization;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.GlobalSat.TR203
{
    class GlobalSatAnyParser : GlobalSatParser
    {
        public override void Parse(IMobilUnit mobilUnit, TR203Reader reader)
        {
            while (true)
            {
                var s = reader.Read();
                if (s == null)
                    break;

                if (IsLatitudeDegreesMinutes(s))
                {
                    ParseLatitudeDegreesMinutes(mobilUnit, s);
                    continue;
                }

                if (IsLongitudeDegreesMinutes(s))
                {
                    ParseLongitudeDegreesMinutes(mobilUnit, s);
                    continue;
                }

                if (TryParseTime(s, mobilUnit))
                    continue;

                if (TryParseDate(s, mobilUnit))
                    continue;

                if (TryParseIMEI(s, mobilUnit))
                    continue;
            }

            if (mobilUnit.Latitude != 0 || mobilUnit.Longitude != 0)
            {
                mobilUnit.Satellites = 4;
            }
        }

        private bool TryParseIMEI(string s, IMobilUnit mobilUnit)
        {
            if (!ImeiHelper.IsImeiCorrect(s))
                return false;

            mobilUnit.DeviceID = s;

            return true;
        }

        private bool TryParseTime(string s, IMobilUnit mobilUnit)
        {
            if (s.Length != 6)
                return false;

            TimeSpan timespan;
            if (!TimeSpan.TryParseExact(s, "hhmmss", CultureInfo.InvariantCulture, out timespan))
                return false;

            var differenceFromCurrentTime = timespan - DateTime.UtcNow.TimeOfDay;

            if (TimeSpan.FromMinutes(5) < differenceFromCurrentTime)
                return false;

            if (differenceFromCurrentTime < TimeSpan.FromHours(-1))
                return false;

            mobilUnit.Time += (int)timespan.TotalSeconds;

            return true;
        }

        private bool TryParseDate(string s, IMobilUnit mobilUnit)
        {
            if (s.Length != 6)
                return false;

            DateTime datetime;
            if (!DateTime.TryParseExact(s, "ddMMyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out datetime))
                return false;

            var differenceFromCurrentTime = Math.Abs((datetime - DateTime.UtcNow).TotalDays);

            if (1 < differenceFromCurrentTime)
                return false;

            mobilUnit.Time += TimeHelper.GetSecondsFromBase(datetime);

            return true;
        }

        private bool IsLatitudeDegreesMinutes(string s)
        {
            return (s.StartsWith("N") || s.StartsWith("S")) && 4 <= s.IndexOf('.');
        }

        private bool IsLongitudeDegreesMinutes(string s)
        {
            return (s.StartsWith("E") || s.StartsWith("W")) && 4 <= s.IndexOf('.');
        }
    }
}
