﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using FORIS.TSS.TerminalService.Interfaces.Exceptions;

namespace FORIS.TSS.Terminal.GlobalSat.TR203
{
	public class TR203 : Device
	{
		private const string GlobalSatTr203 = "GlobalSat TR-203";
		private const string GlobalSatTr600 = "GlobalSat TR-600";
		private const string GlobalSatGtr128 = "GlobalSat GTR-128";
		private const string DefaultParserFormat = "TSPRYAB27GHKLMnaics*U!";


		public TR203() : base(typeof(TR203Sensor), new[] { GlobalSatTr203, GlobalSatTr600, GlobalSatGtr128 })
		{

		}

		private static readonly List<GlobalSatConfiguredParser> Format1Parsers;
		private static readonly List<GlobalSatConfiguredParser> Format0Parsers;
		private static readonly bool LogIncomingMessages;
		private static readonly GlobalSatParser AnyParser = new GlobalSatAnyParser();

		private readonly ConcurrentDictionary<string, bool> _timeoutConfigSent = new ConcurrentDictionary<string, bool>();

		static TR203()
		{
			//TODO: поскольку формат передачи данных от устройства можно конфигурировать на лету индивидуально для каждого трекера,
			//TODO: следует добавить функционал по определению формата сообщения из БД по IMEI
			Format0Parsers = CreateFormatsFromConfig(new[]
				{
					DefaultParserFormat,
					"SORPZAB27GHKLMN*U!",
					"SORPZAB27GHKLMNp*U!",
					"SORPB27GHKLMtuvw*U!",
					"SPRXAB27GHKLMnaicz*U!",
					"TSPRXYAB27GHKLMnaic*U!",
					"SPRYAB27GHKLMnaic???*U!",
					"SPRXYAB27GHKLMmnaefghio*U!",
					"SPRYAB27GHKLMmnaefiotuvwc*U!",
					"SROYXB27GHKLMnaefghiotuvwb*U!",
					"SPRXYAB27GHKLMmnaefghiotuvw*U!",
					"SPRXYAB27GHKLMmnaefghiotuvwb*U!"
				});
			Format1Parsers = CreateFormatsFromConfig(new[]
				{
					"SORPZAB27GHKLMN*U!",
					"SORPZAB27GHKLMNp*U!",
					"SPRXYAB27GHKLMmnaefghio*U!"
				});


			LogIncomingMessages =
				string.Equals(Config.GlobalsConfig.AppSettings["FORIS.TSS.Terminal.GlobalSat.TR203.LogIncomingMessages"],
							  "true",
							  StringComparison.OrdinalIgnoreCase);
		}

		private static string GetComplimentaryFormat(string format)
		{
			if (format.Contains("s") || !format.EndsWith("*U!") || format.Contains("?"))
				return null;

			return format.Substring(0, format.Length - "*U!".Length) + "s" + "*U!";

		}

		private static List<GlobalSatConfiguredParser> CreateFormatsFromConfig(string[] formats)
		{
			var result = new List<string>(formats.Length);

			foreach (var format in formats)
			{
				var additionalFormat = GetComplimentaryFormat(format);
				if (additionalFormat == null)
					continue;
				result.Add(additionalFormat);
			}

			result.AddRange(formats);

			return result.Select(CreateParser).ToList();
		}

		private static GlobalSatConfiguredParser CreateParser(string format)
		{
			var parser = new GlobalSatConfiguredParser(format);
			Trace.TraceInformation("Parser added: {0}\nSource string:{1}", parser.FormatDescription, format);
			return parser;
		}

		static IEnumerable<GlobalSatConfiguredParser> GetFormat0Parser(string packet)
		{
			var len = packet.Count(c => c == ',');
			return Format0Parsers.Where(parser => parser.SupportedPacketLength == len);
		}

		static IEnumerable<GlobalSatConfiguredParser> GetFormat1Parser(string packet)
		{
			var len = packet.Count(c => c == ',');
			return Format1Parsers.Where(parser => parser.SupportedPacketLength == len);
		}

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			if (LogIncomingMessages)
			{
				Trace.TraceInformation("{0}: forced log: {1}", GetType(), BitConverter.ToString(data, 0, count));
			}

			bufferRest = null;

			var startIndex = 0;
			var res = new ArrayList();

			string imei = null;
			if (stateData != null)
			{
				var receiverState = (ReceiverState)stateData;
				imei = receiverState.DeviceID;
			}

			if (imei != null)
			{
				var s = Encoding.ASCII.GetString(data, 0, count).Trim();
				if (s == imei + ",ACK")
					return new ArrayList { CreateCommandCompletedNotification(imei, CmdType.Unspecified) };
			}

			bool clearAlarmRequired = false;

			while (startIndex < count)
			{
				var delimeterIndex = Array.IndexOf(data, (byte)'!', startIndex, count - startIndex);
				if (delimeterIndex == -1)
				{
					if (data.Length < 3 || data[data.Length - 3] != (byte)'*')
					{
						bufferRest = new byte[count - startIndex];
						Array.Copy(data, startIndex, bufferRest, 0, bufferRest.Length);
						break;
					}
					delimeterIndex = count;
				}
				var packet = Encoding.ASCII.GetString(data, startIndex, delimeterIndex - startIndex + 1);
				var packetdata = packet.Trim();
				var validCrc = CheckCRC(packetdata);

				if (validCrc)
				{
					var parsers = GetApproriateParsers(packetdata);

					MobilUnit.Unit.MobilUnit mu = null;
					GlobalSatParser usedParser = null;
					foreach (var parser in parsers)
					{
						try
						{
							mu = ParseDataString(parser, packetdata);
							if (mu != null)
							{
								usedParser = parser;
								break;
							}
						}
						catch (FormatException e)
						{
							Trace.TraceError("{0}: error when parsing {1} using {2}: {3}", this, packetdata, parser, e.Message);
						}
						catch (Exception e)
						{
							Trace.TraceError("{0}: error when parsing {1} using {2}: {3}", this, packetdata, parser, e);
						}
					}

					bool isBufferedData = 2 < packetdata.Length && packetdata[2] == 'b';

					if (mu != null)
					{
						res.Add(mu);
						imei = mu.DeviceID;

						if (usedParser != null && !isBufferedData)
						{
							var correctedFormat = GetCorrectedFormat(usedParser);
							if (correctedFormat != null)
							{
								var command = "GSS," + imei + ",3,0,O3=" + correctedFormat + ",ON=" +
											  correctedFormat + "*QQ!";

								res.Add(new ConfirmPacket(Encoding.ASCII.GetBytes(command)));
							}
						}

						if (stateData == null)
							res.Add(new ReceiverStoreToStateMessage
							{
								StateData = new ReceiverState { DeviceID = mu.DeviceID }
							});

						//Снимаем режим тревоги на устройстве
						if (mu.SensorValues != null && mu.SensorValues.Count > 0 && !isBufferedData)
						{
							if (mu.SensorValues[(int)TR203Sensor.SOSAlarm] == 1)
							{
								var manager = Manager;
								var ui = UnitInfo.Factory.Create(UnitInfoPurpose.Undefined);
								ui.DeviceID = mu.DeviceID;
								if (manager != null && manager.FillUnitInfo(ui) && ui.Unique > 0)
								{
									var lastMobilUnit = manager.GetLastMobilUnit(ui.Unique);
									if (lastMobilUnit == null || lastMobilUnit.Time < mu.Time)
										clearAlarmRequired = true;
								}
							}
						}
					}
				}

				startIndex = delimeterIndex + 1;
			}

			if (!string.IsNullOrWhiteSpace(imei))
			{
				//TODO: пересмотреть логику диалога с устройством
				//Отправляем подтверждение о приёме данных, только если есть данные по устройству
				if (res.Cast<object>().Any(o => o is IMobilUnit))
					res.Add(new ConfirmPacket(Encoding.ASCII.GetBytes("ACK\r")));

				if (clearAlarmRequired)
					res.Add(
						new ConfirmPacket(
							Encoding.ASCII.GetBytes("GSC," + imei + ",Na*QQ!")));

				bool timeoutConfigSent;
				if (!_timeoutConfigSent.TryGetValue(imei, out timeoutConfigSent))
					timeoutConfigSent = false;

				if (!timeoutConfigSent)
				{
					_timeoutConfigSent[imei] = true;

					//Конфигурацию устройства можно запросить командой res.Add(new ConfirmPacket(Encoding.ASCII.GetBytes("GSC," + imei + ",L1(A0,A1,A2,A3,A4)*QQ!")));
					//Но она не работает.
					//Отправляем конфигурацию устройства, чтобы подтверждение получения определялось ответом ACK\r, без IMEI или Device_ID
					res.Add(
						new ConfirmPacket(Encoding.ASCII.GetBytes("GSS," + imei + ",1,0,A0=1,A1=1,A2=15,A3=3*QQ!")));
				}
			}

			return res;
		}

		private string GetCorrectedFormat(GlobalSatParser parser)
		{
			var configuredParser = parser as GlobalSatConfiguredParser;
			if (configuredParser == null)
				return DefaultParserFormat;

			var correctedFormat = GetComplimentaryFormat(configuredParser.Configuration);
			return correctedFormat;
		}

		public static bool CheckCRC(string s)
		{
			var starIndex = s.IndexOf('*');
			if (starIndex == -1)
				return false;

			var crcString = s.Substring(starIndex + 1, 2);

			if (crcString == "QQ")
				return true;

			int receivedCRC;
			if (!int.TryParse(crcString, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out receivedCRC))
				return false;

			var calculatedCRC = 0;
			foreach (var c in s)
			{
				if (c == '*')
					break;
				calculatedCRC ^= c;
			}

			if (2 < s.Length && s[2] == 'b' && calculatedCRC != receivedCRC)
			{
				calculatedCRC ^= 'b';
				calculatedCRC ^= 'r';
			}

			return calculatedCRC == receivedCRC;
		}

		private MobilUnit.Unit.MobilUnit ParseDataString(GlobalSatParser parser, string s)
		{
			var starIndex = s.IndexOf('*');
			var fieldsString = s.Substring(4, starIndex - 4);

			var mu = UnitReceived();
			var configuredParser = parser as GlobalSatConfiguredParser;
			if (configuredParser != null)
				mu.Properties[DeviceProperty.Protocol] += string.Format("|{0}", configuredParser.Configuration ?? string.Empty);

			mu.SensorValues = new Dictionary<int, long>();
			parser.Parse(mu, new TR203Reader(fieldsString));

			mu.CorrectGPS = mu.Position.ValidPosition &&
							(mu.Satellites >= 4) &&
							(Math.Abs(mu.Longitude) > 0.0000001 ||
							 Math.Abs(mu.Latitude) > 0.0000001);

			mu.RawBytes = Encoding.ASCII.GetBytes(s);

			if (mu.CellNetworks != null)
			{
				for (var i = 0; i != mu.CellNetworks.Length; ++i)
					mu.CellNetworks[i].LogTime = mu.Time;
			}

			return mu;
		}

		private static IEnumerable<GlobalSatParser> GetApproriateParsers(string s)
		{
			IEnumerable<GlobalSatParser> parsers;
			switch (s[2])
			{
				case 'r':
					parsers = GetFormat0Parser(s);
					break;
				case 'h':
					parsers = GetFormat1Parser(s);
					break;
				case 'b': //Пакеты из буфера
					parsers = GetFormat0Parser(s);
					break;
				default:
					parsers = null;
					break;
			}

			if (parsers != null)
			{
				foreach (var parser in parsers)
					yield return parser;
			}

			yield return AnyParser;
		}

		public override bool SupportData(byte[] data)
		{
			return data.Length > 3 &&
				   data[0] == 'G' &&
				   data[1] == 'S' &&
				   (data[2] == 'S' ||
					data[2] == 's' ||
					data[2] == 'G' ||
					data[2] == 'g' ||
					data[2] == 'r' ||
					data[2] == 'h' ||
					data[2] == 'b') &&
				   data[3] == ',';
		}

		public override List<CommandStep> GetCommandSteps(IStdCommand command)
		{
			var imei = command.Target.DeviceID;
			if (imei == null)
				throw new CommandSendingFailureException(CmdResult.DeviceIdIsNotSpecified);

			if (!ImeiHelper.IsImeiCorrect(imei))
				throw new CommandSendingFailureException(CmdResult.IncorrectDeviceId);

			var cmdType = command.Type;
			var deviceType = command.Target.DeviceType;

			var replyPhoneNumber = command.GetParamValue(PARAMS.Keys.ReplyToPhone);

			var commandSteps = GetCommandSteps(command, cmdType, imei, deviceType, replyPhoneNumber);

			return commandSteps;
		}

		private List<CommandStep> GetCommandSteps(IStdCommand command, CmdType cmdType, string imei, string deviceType, string replyPhoneNumber)
		{
			switch (cmdType)
			{
				case CmdType.Setup:
					var resultList = new List<CommandStep>(4);
					var registerSenderCommandText = string.Format("GSC,{0},N1(OD=01,OS=20,F0={1}{2})*QQ!",
						imei,
						"+",
						replyPhoneNumber);

					//На первую SMS не ответит или ответит через ACK
					resultList.Add(new SmsCommandStep(registerSenderCommandText) { WaitAnswer = null });

					//На эту SMS ответит координатами
					resultList.Add(new SmsCommandStep(registerSenderCommandText) { WaitAnswer = CmdType.AskPosition });


					//Перезагрузка трекера. Ответ не ожидается.
					resultList.Add(new SmsCommandStep("GSC," + imei + ",3,0,LH*QQ!") { WaitAnswer = null });

					var internetApnConfig = GetInternetApnConfig(command);

					resultList.Add(new SmsCommandStep(
						string.Format("GSS,{0},3,0,D1={1},D2={2},D3={3},E0={4},E1={5}*QQ!",
							imei,
							internetApnConfig.Name,
							internetApnConfig.User,
							internetApnConfig.Pass,
							Manager.GetConstant(Constant.ServerIP),
							Manager.GetConstant(Constant.ServerPort))));

					//Формат передачи данных
					var controllerTypeName = deviceType;
					if (controllerTypeName == GlobalSatGtr128)
						resultList.Add(
							new SmsCommandStep("GSS," + imei + ",3,0,O3=" + DefaultParserFormat + ",ON=" + DefaultParserFormat + "*QQ!"));

					//Команда для определения работы трекера, такая же как MODEWIDE=1 для Автографа.
					if (controllerTypeName == GlobalSatTr600)
						resultList.Add(
							new SmsCommandStep("GSS," + imei + ",3,0,Rd=1,Rl=1,Re=0,Rn=1,Rf=1,Ri=5,Ra=300,CC=00,Rz=1*QQ!"));

					//Перезагрузка трекера. Ответ не ожидается.
					resultList.Add(new SmsCommandStep("GSC," + imei + ",3,0,LH*QQ!") { WaitAnswer = null });

					return resultList;
				case CmdType.ReloadDevice:
					return new List<CommandStep> { new SmsCommandStep("GSC," + imei + ",3,0,LH*QQ!") };
				case CmdType.AskPosition:
					return null; //TODO: implement
				default:
					return base.GetCommandSteps(command);
			}
		}

		private const string ImeiGroup = "imei";
		private readonly Regex _ackRegex = new Regex(@"(?<" + ImeiGroup + @">\d{" + ImeiHelper.ImeiLength + @"})\,ACK");

		public override IList<object> ParseSms(IUnitInfo ui, string text)
		{
			var result = new List<object>();

			var ackMatch = _ackRegex.Match(text);
			if (ackMatch.Success)
			{
				var imei = ackMatch.Groups["imei"].Value;
				if (!ImeiHelper.IsImeiCorrect(imei))
					return null;
				result.Add(CommandCompleted(ui,
											CmdResult.Completed,
											CmdType.Unspecified));
				var mu = new MobilUnit.Unit.MobilUnit(ui) { CorrectGPS = false };
				mu.Properties.Add(DeviceProperty.IMEI, imei);
			}
			else
			{
				result.Add(ParsePositionSms(ui, text));
				result.Add(CommandCompleted(ui,
											CmdResult.Completed,
											CmdType.Setup));
			}

			return result;
		}

		private MobilUnit.Unit.MobilUnit ParsePositionSms(IUnitInfo ui, string text)
		{
			var reader = new SplittedStringReader(text, '\n', '\r', ':', ',');
			if (!reader.TrySkip("Ping report"))
				return null;
			var mu = new MobilUnit.Unit.MobilUnit(ui) { CorrectGPS = false };

			if (!reader.TrySkip("IMEI"))
			{
				mu.Time = TimeHelper.GetSecondsFromBase();
				return mu;
			}
			var imei = reader.ReadString();
			if (!ImeiHelper.IsImeiCorrect(imei))
				return null;
			mu.Properties.Add(DeviceProperty.IMEI, imei);
			if (!reader.TrySkip("Time"))
				return null;
			var date = reader.ReadDate("ddMMyy");
			var time = reader.ReadTime("hhmmss");
			if (time == null)
				return null;
			mu.Time = TimeHelper.GetSecondsFromBase(date + time.Value);
			if (!reader.TrySkip("GPS 3D fix"))
				return null;
			var latString = reader.ReadString();
			if (!latString.StartsWith("N") && !latString.StartsWith("S"))
				return null;
			double lat;
			if (!double.TryParse(latString.Substring(1), NumberStyles.Any, NumberHelper.FormatInfo, out lat))
				return null;
			if (latString.StartsWith("S"))
				lat = -lat;

			var lngString = reader.ReadString();
			if (!lngString.StartsWith("E") && !lngString.StartsWith("W"))
				return null;
			double lng;
			if (!double.TryParse(lngString.Substring(1), NumberStyles.Any, NumberHelper.FormatInfo, out lng))
				return null;
			if (lngString.StartsWith("W"))
				lng = -lng;

			mu.Latitude = lat;
			mu.Longitude = lng;
			mu.CorrectGPS = true;
			mu.Satellites = MobilUnit.Unit.MobilUnit.MinSatellites;

			if (!reader.TrySkip("Speed"))
				return null;
			var speedString = reader.ReadString();
			var speedEndIndex = speedString.IndexOf("K/H", StringComparison.Ordinal);
			if (speedEndIndex <= 0)
				return null;
			int speed;
			if (!int.TryParse(speedString.Substring(0, speedEndIndex), out speed))
				return null;
			//TODO: parse IO
			return mu;
		}

		public override byte[] GetCmd(IStdCommand cmd)
		{
			var s = GetCmdString(cmd);
			if (s == null)
				return base.GetCmd(cmd);
			return Encoding.ASCII.GetBytes(s);
		}

		private static string GetCmdString(IStdCommand cmd)
		{

			switch (cmd.Type)
			{
				case CmdType.Immobilize:
					return GetDigitalOutputControlString(cmd, 1, 1);
				case CmdType.Deimmobilize:
					return GetDigitalOutputControlString(cmd, 1, 0);
				case CmdType.CutOffElectricity:
					return GetDigitalOutputControlString(cmd, 2, 1);
				case CmdType.ReopenElectricity:
					return GetDigitalOutputControlString(cmd, 2, 0);
				case CmdType.CutOffFuel:
					return GetDigitalOutputControlString(cmd, 3, 1);
				case CmdType.ReopenFuel:
					return GetDigitalOutputControlString(cmd, 3, 0);
				default:
					return null;
			}
		}

		private static string GetDigitalOutputControlString(IStdCommand cmd, int number, int value)
		{
			if (number < 1 || 3 < number)
				throw new ArgumentOutOfRangeException("number", number, "Must be between 1 and 3");
			if (value < 0 || 1 < value)
				throw new ArgumentOutOfRangeException("value", value, "Must be either 0 or 1");

			return "GSC," + cmd.Target.DeviceID + ",Lo(" + number + "," + value + ")*QQ!";
		}
	}
}