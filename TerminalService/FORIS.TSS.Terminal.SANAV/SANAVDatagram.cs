using System;

using FORIS.TSS.Common;
using FORIS.TSS.Communication;

namespace FORIS.TSS.Terminal.SANAV
{
	/// <summary>
	/// base datagram for SANAV device
	/// </summary>
	class SANAVDatagram : Datagram
	{
	}
	
	struct SANAVDataType
	{
		// standard
		public const string IMEI		= "IMEI";
		public const string GPRMC		= "GPRMC";
		
		// SANAV
		public const string AutoReport	= "AUTO";
		public const string SOS			= "SOS";
		public const string LowPower	= "LP";
		public const string Poll		= "POLL";
	}
	
	/// <summary>
	/// base response datagram
	/// </summary>
	/// <example>
	/// http://www.sanav.com/eric-gga/gprs.aspx?imei=351277000000000&
	/// RMC= GPRMC,095838.000,A,2458.9733,N,12125.6583,E,0.41,79.21,220905,,*30,AUTO
	/// datagram starts with "imei". prefix omitted
	/// </example>
	class SANAVResponseDatagram : SANAVDatagram
	{
		/// <summary>
		/// link to datagram
		/// </summary>
		string sData;

		/// <summary>
		/// NMEA 0183 GPRMC
		/// </summary>
		GPRMC rmc;
		
		/// <summary>
		/// IMEI
		/// </summary>
		/// <example>
		/// imei=351277000000000&
		/// </example>
		public long IMEI
		{
			get
			{
				string imei = sData.Substring(0, 15);
				return long.Parse(imei);
			}
		}
		
		/// <summary>
		/// response time
		/// </summary>
		public int Time
		{
			get
			{
				return rmc.Time;
			}
		}
		
		/// <summary>
		/// unit latitude. negative - south latitude
		/// </summary>
		public float Latitude
		{
			get
			{
				return rmc.North? rmc.Latitude: -rmc.Latitude;
			}
		}
		
		/// <summary>
		/// unit longitude. negative - south longitude
		/// </summary>
		public float Longitude
		{
			get
			{
				return rmc.East? rmc.Longitude: -rmc.Longitude;
			}
		}
		
		/// <summary>
		/// unit speed
		/// </summary>
		public int Speed
		{
			get
			{
				return rmc.Speed;
			}
		}
		
		/// <summary>
		/// pos is valid
		/// </summary>
		public bool CorrectPos
		{
			get
			{
				return rmc.CorrectPos;
			}
		}
		
		private string m_Type;
		public string Type
		{
			get
			{
				return m_Type;
			}
		}
		/// <summary>
		/// ctor
		/// </summary>
		/// <param name="data">string with datagram</param>
		/// <param name="pos">pos of datagram</param>
		public SANAVResponseDatagram(string data, int pos)
		{
			sData = data.Substring(pos, data.IndexOf('*', pos) + 2 - pos);
			m_Type=data.Substring(data.LastIndexOf(',')+1, data.Length-data.LastIndexOf(',')-1);
			rmc = new GPRMC(data, pos + 16);

            if (data.Contains("SOS"))
                bPanic = true;
		}

        bool bPanic = false;
        public bool Panic
        {
            get { return bPanic; }
        }
	}
	
	/// <summary>
	/// GPRMC wrapper
	/// </summary>
	/// <example>
	/// GPRMC,095838.000,V,2458.9733,N,12125.6583,E,0.41,79.21,220905,,*30
	/// </example>
	class GPRMC
	{
		string[] arsFields;
		
		public int Size
		{
			get
			{
				return 66;
			}
		}
		
		public GPRMC(string data, int begin)
		{
			arsFields = data.Substring(begin, Size).Split(',');
			if(arsFields.Length != 12 || arsFields[0].IndexOf(SANAVDataType.GPRMC) < 0) throw new 
				DatagramStructureException("Provided data not GPRMC or unknown version", GetType(), null, -1);
		}
		
		public time_t Time
		{
			get
			{
				string time = arsFields[1], date = arsFields[9];
				if(time.Length != 10 || date.Length != 6) throw new ApplicationException(
															  "Time must be 10 symbols, date - 6");
				return new DateTime(int.Parse("20"+date.Substring(4, 2)), byte.Parse(date.Substring(2, 2)), 
					byte.Parse(date.Substring(0, 2)), byte.Parse(time.Substring(0, 2)), 
					byte.Parse(time.Substring(2, 2)), byte.Parse(time.Substring(4, 2)), 
					int.Parse(time.Substring(7, 3)));
			}
		}
		
		public bool North
		{
			get
			{
				if (!this.CorrectPos)
					return true;

				string N = arsFields[4];
				if(N != "N" && N != "S") 
					throw new DatagramStructureException("Latitude must be North or South", GetType(), null, -1);
				return N == "N";
			}
		}
		
		public float Latitude
		{
			get
			{
				if (!this.CorrectPos)
					return 0;

				string lat = arsFields[3];
				if(lat.Length != 9) 
					throw new ApplicationException("Latitude must be 9 symbols");
				int g=byte.Parse(lat.Substring(0, 2));
				string point=System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
				float m=float.Parse(lat.Substring(2, 6).Replace(".", point));
				return g + m/60; 
			}
		}
		
		public bool East
		{
			get
			{
				if (!this.CorrectPos)
					return true;

				string E = arsFields[6];
				if(E != "E" && E != "W") 
					throw new DatagramStructureException("Longitude must be East or West", GetType(), null, -1);
				return E == "E";
			}
		}
		
		public float Longitude
		{
			get
			{
				if (!this.CorrectPos)
					return 0;

				string lon = arsFields[5];
				if(lon.Length != 10) 
					throw new ApplicationException("Longitude must be 10 symbols");
				int g=byte.Parse(lon.Substring(0, 3));
				string point=System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
				float m=float.Parse(lon.Substring(3, 6).Replace(".", point));
				return g + m/60;
			}
		}

		/// <summary>
		/// 1 marine knot in km/h
		/// </summary>
		const double MarineKnot = 1.852;	// 50. / 27 = 1.(851)
		
		/// <summary>
		/// unit speed
		/// </summary>
		public int Speed
		{
			get
			{
				if (!this.CorrectPos)
					return 0;

				string point=System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.PercentDecimalSeparator;
				return (int)(double.Parse(arsFields[7].Replace(".", point)) * MarineKnot);
			}
		}
		
		/// <summary>
		/// unit movement direction
		/// </summary>
		public int Course
		{
			get
			{
				if (!this.CorrectPos)
					return 0;

				return int.Parse(arsFields[8]);
			}
		}
		
		/// <summary>
		/// A - data valid, V - navigation receiver warning
		/// </summary>
		public bool CorrectPos
		{
			get
			{
				return arsFields[2] == "A";
			}
		}
	}
	
	class SANAVAutoReportDatagram
	{
	}
}