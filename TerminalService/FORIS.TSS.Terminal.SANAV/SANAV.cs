using System.Collections;
using System.Globalization;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Communication;
using FORIS.TSS.Common;

namespace FORIS.TSS.Terminal.SANAV
{
	/// <summary> device class for SANAV controller </summary>
	public class SANAV : Device
	{
		readonly Encoding encASCII = Encoding.ASCII;

        public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
            bufferRest = null;
			string sData=encASCII.GetString(data, 0, count);

			sData = sData.TrimStart("SANAV".ToCharArray());

			int pos=sData.IndexOf("$GPRMC");
			if (pos == -1 )
				throw new DatagramStructureException("Not found $GPRMC in datagram", this.GetType(), data, 0);
			
			SANAVResponseDatagram dg = new SANAVResponseDatagram(sData, 0);

            ArrayList alRes = new ArrayList(1);

//#if !DEBUG
//            if(!dg.CorrectPos) return null;
//#endif

            var mu = UnitReceived();
            mu.DeviceID = dg.IMEI.ToString(CultureInfo.InvariantCulture);
            mu.cmdType = CmdType.Trace;
            mu.Time = dg.Time;
            mu.Latitude = dg.Latitude;
            mu.Longitude = dg.Longitude;
            mu.Speed = dg.Speed;
            mu.Satellites = 10;
            mu.CorrectGPS = dg.CorrectPos;


            alRes.Add(mu);

            if (dg.Panic)
            {
                IUnitInfo ui = UnitInfo.Factory.Create(UnitInfoPurpose.SANAV);
                ui.DeviceID = mu.DeviceID;
                object[] par = new object[4];
                par[0] = ui;
                par[1] = "";
                par[2] = 1;
                par[3] = 0;
                alRes.Add(new NotifyEventArgs((time_t)dg.Time, TerminalEventMessage.NE_ALARM, null,
                    Severity.Lvl1, NotifyCategory.Controller, "�������", null, par));
            }

            return alRes;
		}

		public void Init(CmdType link, IStdCommand cmd)
		{
			this.Mode=link;
		}


		public override bool SupportData (byte[] data)
		{
			string s = encASCII.GetString(data);
			return s.IndexOf("SANAV") != -1;
		}
	}
}