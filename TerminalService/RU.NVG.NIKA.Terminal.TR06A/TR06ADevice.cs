﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace RU.NVG.NIKA.Terminal.TR06A
{
	public class TR06ADevice : Device
	{
		internal static class DeviceNames
		{
			internal const string TR06A       = "TR06A";
			internal const string TKSTARLK106 = "TK STAR LK106";
			internal const string TKSTARTK109 = "TK STAR TK109";
			internal const string TKSTARTK909 = "TK STAR TK909";
			internal const string TKSTARTK911 = "TK STAR TK911";
			internal const string TKSTARXE208 = "TK STAR XE208";
		}
		public TR06ADevice()
			: base(typeof(TR06Sensor), new []
			{
				DeviceNames.TR06A,
				DeviceNames.TKSTARLK106,
				DeviceNames.TKSTARTK109,
				DeviceNames.TKSTARTK909,
				DeviceNames.TKSTARTK911,
				DeviceNames.TKSTARXE208,
			})
		{
			SupportsShortNumber = false;
		}
		public byte[][] AllowHeaders =
		{
			Encoding.ASCII.GetBytes("*HQ")
		};
		public override bool SupportData(byte[] data)
		{
			return data[data.Length - 1] == '#' && AllowHeaders.Any(header => data.StartsWith(header));
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			/*
				После того, как терминал присылает текстовый пакет с заголовком *HQ, он начинает слать бинарные пакеты
				в шестнадцатерично-десятичном формате, например
				Текстовый пакет
				*HQ,1102014588,V1,073627,A,4500.9510,N,03857.7802,E,004.4,183,310316,FFFFFBFF#
				Бинарный
				24 1102014588 073617 310316 4500 9969 00 03857 7946 f030191 fffffbff ff00e6
				Сходство текстового и бинарного протоколов очевидно.
				Сейчас сервер разрывает соединение после второго же пакета.
				Также приходят пакеты вида
			*/
			bufferRest = null;

			switch ((char)data[0])
			{
				case '*':
					var dataString = Encoding.ASCII.GetString(data);

					// TODO: разобраться
					if (dataString.Count(x => x == ',') == 2)
						return new ArrayList();
					// В Reader передаем строку без первого и последнего символа
					var reader = new TR06AReader(dataString.Substring(1, dataString.Length - 2), ',');
					// Пропускаем заголовок
					reader.Skip(1);
					// Идентификатор устройства
					var deviceId = reader.ReadString();
					// Команда
					var cmd      = reader.ReadString();
					switch (cmd)
					{
						case "V1":
							var time  = DateTime.UtcNow.ToString("yyyyMMddHHmmss");
							var reply = $"*HQ,{deviceId},V4,{cmd},{time}#";
							return (new object[]
							{
								reader.ReadUnit(deviceId, nameof(TR06ADevice)),
								new ConfirmPacket(reply),
							})
								.Where(o => null != o)
								.ToArray();
						default:
							$"{GetType()}.{nameof(OnData)} error: Command not supported yet\r\nData:\r\n{dataString}".TraceWarning();
							return new ArrayList();
					}
				case '$':
					return (new object[]
					{
						ReadBinaryUnit(new DataReader(data, 0, count)),
						// TODO: проверить, не нужно ли отправлять ответ на трекер
					})
						.Where(o => null != o)
						.ToArray();
				default:
					return new ArrayList();
			}
		}
		private IMobilUnit ReadBinaryUnit(DataReader r)
		{
			// Текстовый пакет
			// HQ,1102014588,V1,073627,A,     4500.9510,N, 03857.7802,E,004.4,183,310316,FFFFFBFF#
			// Бинарный
			// 24 1102014588    073617 310316 4500 9969 00 03857 7946 f030191 fffffbff ff00e6
			/////////////////////////////
			r.SkipChar('$');
			/////////////////////////////
			var deviceId = r.ReadBytes(5).ToHexString();
			/////////////////////////////
			var hours    = r.Bcd16(1);
			var minutes  = r.Bcd16(1);
			var seconds  = r.Bcd16(1);
			var day      = r.Bcd16(1);
			var month    = r.Bcd16(1);
			var year     = r.Bcd16(1);
			if (0 == day || 0 == month)
				return null;
			var dateTime = new DateTime(2000 + year, month, day, hours, minutes, seconds, DateTimeKind.Utc);
			/////////////////////////////
			var lat     = r.Bcd32(4);
			var latDeg  = lat / 1000000;
			var latMin  = lat % 1000000;
			/////////////////////////////
			var batteryLevel = r.Bcd16(1);
			/////////////////////////////
			var lngRaw   = r.ReadBigEndian64(5);
			var lngFlags = lngRaw % 16;
			lngRaw >>= 4;
			var lng      = NumberHelper.FromBcd(lngRaw, 5);
			var lngDeg   = lng / 1000000;
			var lngMin   = lng % 1000000;
			/////////////////////////////
			var speedDirections = r.Bcd32(3);
			/////////////////////////////
			//vehicle_status
			r.Skip(3);
			/////////////////////////////
			//User_alarm_flag
			r.Skip();
			/////////////////////////////
			var mu        = UnitReceived(nameof(TR06ADevice));
			mu.DeviceID   = deviceId;
			mu.Time       = TimeHelper.GetSecondsFromBase(dateTime);
			mu.Latitude   = MobilUnit.InvalidLatitude;
			mu.Longitude  = MobilUnit.InvalidLongitude;
			mu.CorrectGPS = (lngFlags & (1 << 1)) != 0;
			if (mu.CorrectGPS)
			{
				mu.Latitude  = latDeg + latMin / 10000.0 / 60.0;
				if ((lngFlags & (1 << 2)) == 0)
					mu.Latitude = -mu.Latitude;
				mu.Longitude = lngDeg + lngMin / 10000.0 / 60.0;
				if ((lngFlags & (1 << 3)) == 0)
					mu.Longitude = -mu.Longitude;
			}
			mu.Speed  = (int)(speedDirections / 1000);
			mu.Course = (int)(speedDirections % 1000);
			mu.SetSensorValue((int)TR06Sensor.BatteryLevel, batteryLevel);
			return mu;
		}
		public override List<CommandStep> GetCommandSteps(IStdCommand command)
		{
			switch (command.Type)
			{
				case CmdType.Setup:
					switch (command.Target.DeviceType)
					{
						case DeviceNames.TKSTARLK106:
						case DeviceNames.TKSTARTK109:
						case DeviceNames.TKSTARTK909:
						case DeviceNames.TKSTARXE208:
						case DeviceNames.TKSTARTK911:
							return GetTkStarSetupCommands(command);
						case DeviceNames.TR06A:
							return GetTr06aSetupCommands(command.Target, command);
					}
					break;
				case CmdType.SetInterval:
					switch (command.Target.DeviceType)
					{
						case DeviceNames.TKSTARLK106:
						case DeviceNames.TKSTARTK109:
						case DeviceNames.TKSTARTK909:
						case DeviceNames.TKSTARXE208:
						case DeviceNames.TKSTARTK911:
							return GetTkStarSetIntervalCommands(command);
					}
					break;
				case CmdType.Status:
					switch (command.Target.DeviceType)
					{
						case DeviceNames.TKSTARLK106:
						case DeviceNames.TKSTARTK109:
						case DeviceNames.TKSTARTK909:
						case DeviceNames.TKSTARXE208:
							return GetTkStarStatusCommands(command);
					}
					break;
				case CmdType.SetSos:
					switch (command.Target.DeviceType)
					{
						case DeviceNames.TKSTARLK106:
						case DeviceNames.TKSTARTK109:
						case DeviceNames.TKSTARTK909:
						case DeviceNames.TKSTARXE208:
							return GetTkStarSetSosCommands(command);
					}
					break;
				case CmdType.DeleteSOS:
					switch (command.Target.DeviceType)
					{
						case DeviceNames.TKSTARLK106:
						case DeviceNames.TKSTARTK109:
						case DeviceNames.TKSTARTK909:
						case DeviceNames.TKSTARXE208:
							return GetTkStarDeleteSOSCommands(command);
					}
					break;
				case CmdType.AskPosition:
					switch (command.Target.DeviceType)
					{
						case DeviceNames.TKSTARLK106:
						case DeviceNames.TKSTARTK109:
						case DeviceNames.TKSTARTK909:
						case DeviceNames.TKSTARXE208:
						case DeviceNames.TKSTARTK911:
							return GetTkStarAskPositionCommands(command);
					}
					break;
				case CmdType.ReloadDevice:
					switch (command.Target.DeviceType)
					{
						case DeviceNames.TKSTARLK106:
						case DeviceNames.TKSTARTK109:
						case DeviceNames.TKSTARTK909:
						case DeviceNames.TKSTARXE208:
						case DeviceNames.TKSTARTK911:
							return GetTkStarReloadDeviceCommands(command);
					}
					break;
				case CmdType.SetModeOnline:
					switch (command.Target.DeviceType)
					{
						case DeviceNames.TKSTARLK106:
						case DeviceNames.TKSTARTK109:
						case DeviceNames.TKSTARTK909:
						case DeviceNames.TKSTARXE208:
						case DeviceNames.TKSTARTK911:
							return GetTkStarSetModeOnlineCommands(command);
					}
					break;
				case CmdType.SetModeWaiting:
					switch (command.Target.DeviceType)
					{
						case DeviceNames.TKSTARLK106:
						case DeviceNames.TKSTARTK109:
						case DeviceNames.TKSTARTK909:
						case DeviceNames.TKSTARXE208:
						case DeviceNames.TKSTARTK911:
							return GetTkStarSetModeWaitingCommands(command);
					}
					break;
				case CmdType.MonitorModeOn:
					switch (command.Target.DeviceType)
					{
						case DeviceNames.TKSTARLK106:
						case DeviceNames.TKSTARTK109:
						case DeviceNames.TKSTARTK909:
						case DeviceNames.TKSTARXE208:
						case DeviceNames.TKSTARTK911:
							return GetTkStarMonitorModeOnCommands(command);
					}
					break;
				case CmdType.MonitorModeOff:
					switch (command.Target.DeviceType)
					{
						case DeviceNames.TKSTARLK106:
						case DeviceNames.TKSTARTK109:
						case DeviceNames.TKSTARTK909:
						case DeviceNames.TKSTARXE208:
						case DeviceNames.TKSTARTK911:
							return GetTkStarMonitorModeOffCommands(command);
					}
					break;
			}
			return base.GetCommandSteps(command);
		}
		private readonly Regex _addAdminAccountOk = new Regex(@"Add admin account \d OK!");
		private readonly Regex _imeiRegex = new Regex(@"IMEI:(?<imei>[\d]{15})");
		public override IList<object> ParseSms(IUnitInfo ui, string text)
		{
			var result = new List<object>();

			if (_addAdminAccountOk.IsMatch(text) ||
				text.StartsWith("The Admin already exists") ||
				text.EndsWith("OK!"))
			{
				result.Add(CommandCompleted(ui, CmdResult.Completed, CmdType.Setup));
				return result;
			}


			var imeiMatch = _imeiRegex.Match(text);
			var imeiGroup = imeiMatch.Groups["imei"];
			var imei = imeiGroup.Value;
			if (!string.IsNullOrWhiteSpace(imei))
			{
				result.Add(DeviceIdReceived(ui, imei.Substring(4, imei.Length - 5)));
				result.Add(CommandCompleted(ui, CmdResult.Completed, CmdType.Setup));
			}

			return result;
		}
		private static string GetPassword(IUnitInfo ui)
		{
			if (string.IsNullOrWhiteSpace(ui.Password))
			{
				switch (ui.DeviceType)
				{
					case DeviceNames.TR06A:
						return "6666";
					case DeviceNames.TKSTARLK106:
					case DeviceNames.TKSTARTK109:
					case DeviceNames.TKSTARTK909:
					case DeviceNames.TKSTARXE208:
					case DeviceNames.TKSTARTK911:
						return "123456";
				}
			}

			return ui.Password;
		}
		private string GetServiceCommand(IUnitInfo ui, string serviceNumber)
		{
			var password = GetPassword(ui);
			return string.Format(CultureInfo.InvariantCulture, "#{0}#AAD#2#{1}#", password, ContactHelper.GetNormalizedPhone(serviceNumber));
		}
		private string GetResetPasswordCommand()
		{
			return "#6666#RP#";
		}
		private string GetServerIpAndPortCommand(IUnitInfo ui)
		{
			var password = GetPassword(ui);
			return string.Format(CultureInfo.InvariantCulture, "#{0}#IP#{1}#{2}#",
				password,
				Manager.GetConstant(Constant.ServerIP),
				Manager.GetConstant(Constant.ServerPort));
		}
		private string GetServerDomainCommand(IUnitInfo ui)
		{
			if (string.IsNullOrWhiteSpace(Manager.GetConstant(Constant.AppDomain)))
				return null;

			var password = GetPassword(ui);
			return string.Format(CultureInfo.InvariantCulture, "#{0}#WZ#{1}#{2}#",
				password,
				Manager.GetConstant(Constant.AppDomain),
				Manager.GetConstant(Constant.ServerPort));
		}
		private List<CommandStep> GetTr06aSetupCommands(IUnitInfo ui, IStdCommand command)
		{
			var replyPhoneNumber = command.GetParamValue(PARAMS.Keys.ReplyToPhone);

			var result = new List<CommandStep>();
			if (GetPassword(ui) == "6666")
				result.Add(new SmsCommandStep(GetResetPasswordCommand()) { ReplyTo = replyPhoneNumber });

			result.Add(new SmsCommandStep(GetServiceCommand(ui, replyPhoneNumber)) { ReplyTo = replyPhoneNumber});
			result.Add(new SmsCommandStep(GetServerIpAndPortCommand(ui)) { ReplyTo = replyPhoneNumber });
			var serverDomainCommand = GetServerDomainCommand(ui);
			if (!string.IsNullOrWhiteSpace(serverDomainCommand))
				result.Add(new SmsCommandStep(serverDomainCommand) { ReplyTo = replyPhoneNumber });
			result.Add(new SmsCommandStep(string.Format(CultureInfo.InvariantCulture, "PARAM#")));

			foreach (var item in result.Cast<SmsCommandStep>())
			{
				item.WaitAnswer = CmdType.Setup;
			}

			return result;
		}
		private List<CommandStep> GetTkStarSetupCommands(IStdCommand command)
		{
			var devPwd = GetPassword(command.Target);
			var srvAdd = Manager.GetConstant(Constant.ServerIP);
			var srvPrt = Manager.GetConstant(Constant.ServerPort);
			var apnCfg = GetInternetApnConfig(command);
			var apnNam = apnCfg.Name;
			var apnUsr = apnCfg.User;
			var apnPwd = apnCfg.Pass;
			var rplPhn = ContactHelper.GetNormalizedPhone(command.GetParamValue(PARAMS.Keys.ReplyToPhone));
			return new List<CommandStep>
			{
				new SmsCommandStep($"begin{devPwd}"),
				new SmsCommandStep($"admin{devPwd} +{rplPhn}"),
				new SmsCommandStep($"apn{devPwd} {apnNam}"),
				new SmsCommandStep($"apnuser{devPwd} {apnUsr}"),
				new SmsCommandStep($"apnpasswd{devPwd} {apnPwd}"),
				new SmsCommandStep($"Addjz=0"),
				new SmsCommandStep($"nosleep{devPwd}"),
				new SmsCommandStep($"upload{devPwd} 60"),
				new SmsCommandStep($"timezone{devPwd} 0"),
				new SmsCommandStep($"gprs{devPwd}"),
				new SmsCommandStep($"adminip{devPwd} {srvAdd} {srvPrt}"),
			};
		}
		private List<CommandStep> GetTkStarSetIntervalCommands(IStdCommand command)
		{
			return new List<CommandStep>
			{
				new SmsCommandStep($@"upload{GetPassword(command.Target)} 60"),
			};
		}
		private List<CommandStep> GetTkStarStatusCommands(IStdCommand command)
		{
			return new List<CommandStep>
			{
				new SmsCommandStep($@"status{GetPassword(command.Target)}"),
			};
		}
		private List<CommandStep> GetTkStarSetSosCommands(IStdCommand command)
		{
			return new List<CommandStep>
			{
				new SmsCommandStep($@"sos{GetPassword(command.Target)} +79991112233"),
			};
		}
		private List<CommandStep> GetTkStarDeleteSOSCommands(IStdCommand command)
		{
			return new List<CommandStep>
			{
				new SmsCommandStep($@"nosos{GetPassword(command.Target)} +79991112233"),
			};
		}
		private List<CommandStep> GetTkStarAskPositionCommands(IStdCommand command)
		{
			return new List<CommandStep>
			{
				new SmsCommandStep($@"g{GetPassword(command.Target)}#"),
			};
		}
		private List<CommandStep> GetTkStarReloadDeviceCommands(IStdCommand command)
		{
			return new List<CommandStep>
			{
				new SmsCommandStep($@"Rst"),
			};
		}
		private List<CommandStep> GetTkStarSetModeOnlineCommands(IStdCommand command)
		{
			return new List<CommandStep>
			{
				new SmsCommandStep($@"nosleep{GetPassword(command.Target)}"),
			};
		}
		private List<CommandStep> GetTkStarSetModeWaitingCommands(IStdCommand command)
		{
			return new List<CommandStep>
			{
				new SmsCommandStep($@"sleep{GetPassword(command.Target)} shock"),
			};
		}
		private List<CommandStep> GetTkStarMonitorModeOnCommands(IStdCommand command)
		{
			return new List<CommandStep>
			{
				new SmsCommandStep($@"monitor{GetPassword(command.Target)}"),
			};
		}
		private List<CommandStep> GetTkStarMonitorModeOffCommands(IStdCommand command)
		{
			return new List<CommandStep>
			{
				new SmsCommandStep($@"tracker{GetPassword(command.Target)}"),
			};
		}
	}
}