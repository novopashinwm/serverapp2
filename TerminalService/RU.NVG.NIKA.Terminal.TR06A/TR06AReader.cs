﻿using System;
using System.Globalization;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace RU.NVG.NIKA.Terminal.TR06A
{
	public class TR06AReader : SplittedStringReader
	{
		public TR06AReader(string s, params char[] delimeter)
			: base(s, delimeter)
		{
		}
		public IMobilUnit ReadUnit(string deviceId, string protocol)
		{
			var time       = ReadString();
			var flag       = ReadString();
			var lat        = ReadGrad();
			var lahs       = ReadString();
			var lng        = ReadGrad();
			var lohs       = ReadString();
			var speed      = ReadDouble();
			var cource     = ReadInt();
			var date       = ReadString();
			var sensor     = ReadHexInt();
			var logTime    = GetTime(date, time);
			var correctGps = logTime > 0 && flag == "A" && lat.HasValue && lng.HasValue;

			var mu = new MobilUnit
			{
				DeviceID   = deviceId,
				Time       = logTime,
				CorrectGPS = correctGps,
				Latitude   = (lahs == "N" ? 1 : -1) * lat ?? MobilUnit.InvalidLatitude,
				Longitude  = (lohs == "E" ? 1 : -1) * lng ?? MobilUnit.InvalidLongitude,
				Speed      = speed.HasValue ? (int?)Math.Ceiling(speed.Value) : null,
				Course     = cource
			};
			mu.SetSensorValue((int)TR06Sensor.DigitalInputs, sensor);
			if (!string.IsNullOrWhiteSpace(protocol))
				mu.Properties.Add(DeviceProperty.Protocol, protocol);
			return mu;
		}
		private int GetTime(string date, string time)
		{
			if (time.Length != 6)
				return 0;
			if (date.Length != 6)
				return 0;

			var dateString = string.Format(CultureInfo.InvariantCulture, "{0}.{1}.{2}",
				date.Substring(0, 2),
				date.Substring(2, 2),
				date.Substring(4, 2));
			var timeString = string.Format(CultureInfo.InvariantCulture, "{0}:{1}:{2}",
				time.Substring(0, 2),
				time.Substring(2, 2),
				time.Substring(4));
			var dateTimeString = string.Format(CultureInfo.InvariantCulture, "{0} {1}", dateString, timeString);
			DateTime dateTime;
			if (!DateTime.TryParse(dateTimeString, out dateTime))
				return 0;

			return TimeHelper.GetSecondsFromBase(dateTime);
		}
		private double? ReadGrad()
		{
			var nmea = ReadDouble();
			if (!nmea.HasValue)
				return null;

			var degrees = Math.Floor(nmea.Value / 100);
			var minutes = (nmea - degrees * 100) / 60;
			return degrees + minutes;
		}
	}
}