using System;
using System.Collections.Generic;
using System.Text;
using FORIS.TSS.Communication;
using FORIS.TSS.Common;
using System.Diagnostics;
using FORIS.TSS.BusinessLogic;

namespace FORIS.TSS.Terminal.STEPPII
{
    public class STEPPIIDatagram : Datagram
    {
        public static STEPPIIDatagram Create(byte[] data)
        {
            string msg = ASCIIEncoding.ASCII.GetString(data);
            string[] message = msg.Split('$');

            foreach (string str in message)
            {
                if (str.StartsWith("GPRMC"))
                {
                    return new STEPPIIDatagram(message);
                }
                if(str.StartsWith("BIN"))
                {
                    return new STEPPIIBinDatagram(data);
                }
            }

            return null;
        }

        protected string[] m_message;
        readonly GPRMC rmc;
        protected GPIOP iop;
        public STEPPIIDatagram(string[] message)
        {
            m_message = message;

            foreach (string str in message)
            {
                if (str.StartsWith("GPRMC"))
                {
                 
                    rmc = new GPRMC(str);
                }
                if (str.StartsWith("GPIOP"))
                {
                    iop = new GPIOP(str);
                }
                
            }
        }

        protected STEPPIIDatagram()
        {
            
        }

        // <summary>
        /// response time
        /// </summary>
        public virtual int Time
        {
            get
            {
                return rmc.Time;
            }
        }

        /// <summary>
        /// unit latitude. negative - south latitude
        /// </summary>
        public virtual float Latitude
        {
            get
            {
                return rmc.North ? rmc.Latitude : -rmc.Latitude;
            }
        }

        /// <summary>
        /// unit longitude. negative - south longitude
        /// </summary>
        public virtual float Longitude
        {
            get
            {
                return rmc.East ? rmc.Longitude : -rmc.Longitude;
            }
        }

        /// <summary>
        /// unit speed
        /// </summary>
        public virtual int Speed
        {
            get
            {
                return rmc.Speed;
            }
        }

        /// <summary>
        /// pos is valid
        /// </summary>
        public virtual bool CorrectPos
        {
            get
            {
                return rmc.CorrectPos && rmc.Speed < MobilUnit.Unit.MobilUnit.GPSLimitMaxSpeed;
            }
        }

        public string IMEI
        {
            get
            {
                foreach (string str in m_message)
                {
                    if (str.StartsWith("IMEI"))
                    {
                        string s = str.Substring(str.IndexOf('=')+1);
                        if(s.IndexOf('*')>0)
                            s = s.Remove(s.IndexOf('*'));
                        s=s.Replace("IMEI:", "");
                        s=s.Replace("\r\n", "");
                        return s;
                    }
                }
                return string.Empty;
            }
        }

        public bool Ignition
        {
            get
            {
                if (iop != null)
                    return iop.Ignition;
                return false;
            }
        }
    }

    /// <summary>
    /// GPRMC wrapper
    /// </summary>
    /// <example>
    /// GPRMC,095838.000,V,2458.9733,N,12125.6583,E,0.41,79.21,220905,,*30
    /// </example>
    class GPRMC
    {
        readonly string[] arsFields;

        public int Size
        {
            get
            {
                return 66;
            }
        }

        public GPRMC(string data)
        {
            arsFields = data.Split(',');
            if (arsFields.Length != 12) throw new
                 DatagramStructureException("Provided data not GPRMC or unknown version", GetType(), null, -1);
        }

        public time_t Time
        {
            get
            {
                string time = arsFields[1], date = arsFields[9];
                if (time.Length != 10 || date.Length != 6) throw new ApplicationException(
                                                               "Time must be 10 symbols, date - 6");
                return new DateTime(int.Parse("20" + date.Substring(4, 2)), byte.Parse(date.Substring(2, 2)),
                    byte.Parse(date.Substring(0, 2)), byte.Parse(time.Substring(0, 2)),
                    byte.Parse(time.Substring(2, 2)), byte.Parse(time.Substring(4, 2)),
                    int.Parse(time.Substring(7, 3)));
            }
        }

        public bool North
        {
            get
            {
                if (!this.CorrectPos)
                    return true;

                string N = arsFields[4];
                if (N != "N" && N != "S")
                    throw new DatagramStructureException("Latitude must be North or South", GetType(), null, -1);
                return N == "N";
            }
        }

        public float Latitude
        {
            get
            {
                if (!this.CorrectPos)
                    return 0;

                string lat = arsFields[3];
                if (lat.Length != 9)
                    throw new ApplicationException("Latitude must be 9 symbols");
                int g = byte.Parse(lat.Substring(0, 2));
                string point = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
                float m = float.Parse(lat.Substring(2, 6).Replace(".", point));
                return g + m / 60;
            }
        }

        public bool East
        {
            get
            {
                if (!this.CorrectPos)
                    return true;

                string E = arsFields[6];
                if (E != "E" && E != "W")
                    throw new DatagramStructureException("Longitude must be East or West", GetType(), null, -1);
                return E == "E";
            }
        }

        public float Longitude
        {
            get
            {
                if (!this.CorrectPos)
                    return 0;

                string lon = arsFields[5];
                if (lon.Length != 10)
                    throw new ApplicationException("Longitude must be 10 symbols");
                int g = byte.Parse(lon.Substring(0, 3));
                string point = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
                float m = float.Parse(lon.Substring(3, 6).Replace(".", point));
                return g + m / 60;
            }
        }

        /// <summary>
        /// 1 marine knot in km/h
        /// </summary>
        const double MarineKnot = 1.852;	// 50. / 27 = 1.(851)

        /// <summary>
        /// unit speed
        /// </summary>
        public int Speed
        {
            get
            {
                if (!this.CorrectPos)
                    return 0;

                string point = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.PercentDecimalSeparator;
                return (int)(double.Parse(arsFields[7].Replace(".", point)) * MarineKnot);
            }
        }

        /// <summary>
        /// unit movement direction
        /// </summary>
        public int Course
        {
            get
            {
                if (!this.CorrectPos)
                    return 0;

                return int.Parse(arsFields[8]);
            }
        }

        /// <summary>
        /// A - data valid, V - navigation receiver warning
        /// </summary>
        public bool CorrectPos
        {
            get
            {
                return arsFields[2] == "A";
            }
        }
    }

    public class GPIOP
    {
        readonly string[] Fields;
        public GPIOP(string message)
        {
            Fields = message.Split(',');
            if (Fields.Length != 6) 
                throw new DatagramStructureException("Provided data not GPIOP or unknown version", GetType(), null, -1);
        }

        public bool Ignition
        {
            get
            {
                return Fields[1][0] == '1';
            }
        }
    }

    public class STEPPIIBinDatagram:STEPPIIDatagram
    {
        public STEPPIIBinDatagram(byte[] data)
        {
            m_message = ASCIIEncoding.ASCII.GetString(data).Split('$');

            foreach (string str in m_message)
            {
                if (str.StartsWith("GPIOP"))
                {
                    iop = new GPIOP(str);
                }
            }

            for(int i=0; i < data.Length;i++)
            {
                if(data[i]=='$'&&
                    data[i+1]=='B'&&
                    data[i+2]=='I'&&
                    data[i+3]=='N')
                {
                    byte[] binData = new byte[18];

                    for(int k=0; k < binData.Length; k++)
                    {
                        binData[k] = data[i + 4 + k];
                    }
                    Initialize(binData);
                    break;
                }

            }
        }

        private int time;
        private int latitude;
        private int longitude;
        private int speed;
        private int course;
        private bool valid;

        private void Initialize(byte[] data)
        {
            UInt16 i16 = 0;
            i16 = data[0];
            i16 <<= 8;
            i16 |= data[1];

            int day = (i16 & 0xF800) >> 11;
            int month = (i16 & 0x780) >> 7;
            int year = (i16 & 0x7F);

            UInt32 i32 = (((UInt32) data[2]) << 24) |
                         (((UInt32) data[3]) << 16) |
                         (((UInt32) data[4]) << 8) |
                         ((UInt32) data[5]);

            valid = (i32 & (1 << 31)) != 0;

            int hours = (int) ((i32 >> 22) & 0x1F);
            int minutes = (int) ((i32 >> 16) & 0x3F);
            int miliseconds = (int) (i32 & 0xFFFF);

            if (month != 0 && day != 0)
            {
                DateTime t = new DateTime(2000 + year, month, day);
                t = t.AddHours(hours);
                t = t.AddMinutes(minutes);
                t = t.AddMilliseconds(miliseconds);

                time = (int) (t - TimeHelper.year1970).TotalSeconds;
            }
            if(year == 80)
                time = (int) (DateTime.UtcNow - TimeHelper.year1970).TotalSeconds;

            latitude = BitConverter.ToInt32(new byte[] { data[9], data[8], data[7], data[6] }, 0);
            longitude = BitConverter.ToInt32(new byte[] { data[13], data[12], data[11], data[10] }, 00);
            speed = BitConverter.ToInt16(new byte[] { data[15], data[14] }, 0);
            speed = (speed*3600)/1000;
            course = BitConverter.ToInt16(new byte[] { data[17], data[16] }, 0);
        }

        // <summary>
        /// response time
        /// </summary>
        public override int Time
        {
            get
            {
                return time;
            }
        }

        /// <summary>
        /// unit latitude. negative - south latitude
        /// </summary>
        public override float Latitude
        {
            get
            {
                return ((float)latitude)/10000000;
            }
        }

        /// <summary>
        /// unit longitude. negative - south longitude
        /// </summary>
        public override float Longitude
        {
            get
            {
                return ((float)longitude) / 10000000;
            }
        }

        /// <summary>
        /// unit speed
        /// </summary>
        public override int Speed
        {
            get
            {
                return speed;
            }
        }

        /// <summary>
        /// pos is valid
        /// </summary>
        public override bool CorrectPos
        {
            get
            {
                return valid && Speed < MobilUnit.Unit.MobilUnit.GPSLimitMaxSpeed;
            }
        }
    }

    public class STEPPIIHistoryHelper
    {
        DateTime time;
        float latitude;
        float longitude;
        bool ignition;
        int speed;

        static readonly string point = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;

        public STEPPIIHistoryHelper(string[] fields)
        {
            try
            {
                time = DateTime.Parse(fields[0]) + TimeSpan.Parse(fields[1]);
                latitude = float.Parse(fields[4].Replace(".", point));
                longitude = float.Parse(fields[5].Replace(".", point));
                speed = (int.Parse(fields[7])*3600)/1000;
            }
            catch(Exception ex)
            {
                Trace.WriteLine(this.GetType() + " Can't recognize this: " +
                    fields + "\n" + ex.ToString());
            }
        }

        public time_t Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
            }
        }

        public float Latitude
        {
            get
            {
                return latitude;
            }
            set
            {
                latitude = value;
            }
        }

        public float Longitude
        {
            get
            {
                return longitude;
            }
            set
            {
                longitude = value;
            }
        }

        public int Speed
        {
            get
            {
                return speed;
            }
            set
            {
                speed = value;
            }
        }

        public bool Ignition
        {
            get
            {
                return ignition;
            }
            set
            {
                ignition = value;
            }
        }
    }
    class STEPPIIHistoryDatagram : STEPPIIDatagram
    {
        public List<STEPPIIHistoryHelper> History = new List<STEPPIIHistoryHelper>();
        public STEPPIIHistoryDatagram(string[] message)
            : base(message)
        {
            foreach (string str in message)
            {
                if (str == String.Empty ||
                    str.ToUpper().Contains("IMEI") ||
                    str.ToUpper().Contains("ERROR") || 
                    str.ToUpper().Contains("HISTORY"))
                    continue;

                string[] fields = str.Split(new char[] { ',' });

                if(fields.Length >= 9)
                    History.Add(new STEPPIIHistoryHelper(fields));
            }
        }

        new public static STEPPIIHistoryDatagram Create(byte[] data)
        {
            string msg = ASCIIEncoding.ASCII.GetString(data);
            string[] message = msg.Split('$');

            foreach (string str in message)
            {
                if (str.Contains("GPS.History.Read"))
                {
                    return new STEPPIIHistoryDatagram(message);
                }
            }

            return null;
        }
    }
}
