using System.Collections.Generic;
using System.Text;

namespace FORIS.TSS.Terminal.STEPPII
{
    public class BytesStringStack
    {
        readonly List<byte> bytes = new List<byte>();

        public void Push(byte data)
        {
            bytes.Add(data);
        }

        public void Push(byte[] data)
        {
            bytes.AddRange(data);
        }

        public string Pop()
        {
            int i = -1;

            for (int k = 0; k < bytes.Count; k++)
            {
                if (bytes[k] == 0x0A)
                {
                    i = k;
                    break;
                }
            }
            
            if (i >= 0)
            {
                string str = Encoding.ASCII.GetString(bytes.GetRange(0, i + 1).ToArray(), 0, i + 1);
                bytes.RemoveRange(0, i + 1);

                return str;
            }

            return string.Empty;
        }

        public string Pop(out byte[] data)
        {
            int i = -1;

            for (int k = 0; k < bytes.Count; k++)
            {
                if (bytes[k] == 0x0A)
                {
                    i = k;
                    break;
                }
            }

            if (i >= 0)
            {
                string str = Encoding.ASCII.GetString(bytes.GetRange(0, i + 1).ToArray(), 0, i + 1);
                data = bytes.GetRange(0, i + 1).ToArray();

                bytes.RemoveRange(0, i + 1);

                return str;
            }

            data = null;
            return string.Empty;
        }
    }
}