﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.STEPPII
{

	public class STEPPII : Device
	{
		#region IDevice Members

		public override byte[] AdjustSettings()
		{
			string text = "$PFAL,Sys.Timer0.Configure=cyclic,10000\r\n" +
						 "$PFAL,sys.timer0.start\r\n";

			Trace.WriteLine("STEPP II:  AdjustSettings() Sending text: "+text);

			return Encoding.ASCII.GetBytes(text);
		}

		const string sFromTimePar = "FromTime";
		const string sToTimePar = "ToTime";
		public override byte[] GetCmd(IStdCommand cmd)
		{
			switch (cmd.Type)
			{
				case CmdType.GetLog:
					DateTime From = (DateTime)cmd.Params[sFromTimePar];
					DateTime To = (DateTime)cmd.Params[sToTimePar];
					string strFrom = From.ToString("d.M.yyyy,H:m:s");
					string strTo = To.ToString("d.M.yyyy,H:m:s");
					string strSetRead = "$PFAL,GPS.History.SetRead," + strFrom + "-" + strTo + "\r\n\r\n";
					//string strRead = "$PFAL,GPS.History.Read,fmt=txt\r\n";

					Trace.WriteLine("Step II GetLog: " + strSetRead );

					return Encoding.ASCII.GetBytes(strSetRead);
				default:
					break;
			}
			return null;
		}

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;
			string sData =Encoding.ASCII.GetString(data, 0, count);
			int pos = sData.IndexOf("$GPRMC");
			int pos1 = sData.IndexOf("$BIN");
			if (pos != -1 || pos1 != -1)
			{

				STEPPIIDatagram dg = STEPPIIDatagram.Create(data);

				MobilUnit.Unit.MobilUnit mu = UnitReceived();

				mu.DeviceID = dg.IMEI;
				mu.cmdType = CmdType.Trace;
				mu.Time = dg.Time;
				mu.Latitude = dg.Latitude;
				mu.Longitude = dg.Longitude;
				mu.Speed = dg.Speed;
				mu.Satellites = 10;
				mu.CorrectGPS = dg.CorrectPos;

				return new IMobilUnit[] { mu };
			}
		   pos = sData.IndexOf("GPS.History.Read");
			if (pos != -1) //История
			{
				STEPPIIHistoryDatagram dg = STEPPIIHistoryDatagram.Create(data);

				MobilUnit.Unit.MobilUnit[] mu_ar = new MobilUnit.Unit.MobilUnit[dg.History.Count];
				for (int i = 0; i < dg.History.Count; i++)
				{
					mu_ar[i] = UnitReceived();
					mu_ar[i].Latitude = dg.History[i].Latitude;
					mu_ar[i].Longitude = dg.History[i].Longitude;
					mu_ar[i].Time= dg.History[i].Time;
					mu_ar[i].DeviceID = dg.IMEI;
					mu_ar[i].cmdType = CmdType.GetLog;
					mu_ar[i].Satellites = 10;
					mu_ar[i].Speed = dg.History[i].Speed;
					mu_ar[i].CorrectGPS = dg.CorrectPos;

			   }

				
				return mu_ar;
			}
			pos = sData.IndexOf("GPS.History.SetRead");
			if (pos != -1) //История
			{
				return null;
			}

			//throw new DatagramStructureException("Not found known symbols for STEPP II in datagram", this.GetType(), data, 0);
			Trace.WriteLine(this.GetType() + " " + sData);
			return null;
		}

		public override bool SupportData(byte[] data)
		{
			string msg = Encoding.ASCII.GetString(data);
			string[] message = msg.Split('$');

			if (!msg.StartsWith("$"))
				return false;

			foreach (string str in message)
			{
				if (str.StartsWith("PGID"))
					return false;
				if (str.StartsWith("GPRMC"))
					return true;
				if(str.Contains(",GPS.History.Read"))
					return true;
			}

			return false;
		}

		#endregion
	}
}