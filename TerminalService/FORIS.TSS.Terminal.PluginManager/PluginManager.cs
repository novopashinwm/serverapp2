﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace FORIS.Extension
{
	/// <summary>
	/// Summary description for PluginManager.
	/// </summary>
	public class PluginManager : System.ComponentModel.Component
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public PluginManager(System.ComponentModel.IContainer container) : this()
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			container.Add(this);
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		public PluginManager()
		{
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion

		Hashtable htWorkDir = new Hashtable();
		public DirectoryInfo[] WorkingDirectory
		{
			get
			{

				DirectoryInfo[] ardi = new DirectoryInfo[htWorkDir.Count];
				htWorkDir.Values.CopyTo(ardi, 0);
				return ardi;
			}
		}

		public bool AddDirectory(string name)
		{
			if(!Directory.Exists(name) || htWorkDir.ContainsKey(name)) return false;
			htWorkDir[name] = new DirectoryInfo(name);
			return true;
		}

		bool FullPath(string path)
		{
			return path.IndexOf(":") > -1;
		}

		public void Refresh()
		{
			if (null == PluginType)
				throw new NotSupportedException("Plug-in type not specified");

			if (0 != alPlugin.Count)
				alPlugin.Clear();
			if (0 != htWorkDir.Count)
			{
				foreach (DirectoryInfo di in htWorkDir.Values)
					LoadPlugins(di);
			}
			else
				LoadPlugins(new DirectoryInfo(Environment.CurrentDirectory));
		}

		ArrayList alPlugin = new ArrayList();
		void LoadPlugins(DirectoryInfo di)
		{
			var files = di.GetFiles("*.dll");
			foreach(FileInfo fi in files)
			{
				try
				{
					// Загружаем сборку
					var assem = Assembly.LoadFile(fi.FullName);
					// Получение всех типов сборки
					var types = assem
						.GetTypes()
						.Where(t => 1 == 1
							// Тип не должен быть абстрактным
							&& !t.IsAbstract
							// Должен иметь конструктор по умолчанию (такой конструктор может быть у абстрактного класса)
							&& null != t.GetConstructor(new Type[0])
							// Определяет, можно ли присвоить экземпляр указанного типа экземпляру типа плагина
							// (прямо или косвенно прямо или косвенно унаследован от типа плагина включая интерфейсы)
							&& tPluginType.IsAssignableFrom(t))
						.ToList();
					foreach (Type type in types)
					{
						// Поиск в списке плагинов, если не найдено, то i отрицательное число,
						// которое является двоичным дополнением индекса следующего элемента,
						// большего, чем value, или, если большего элемента не существует,
						// двоичным дополнением значения
						var idx = alPlugin.BinarySearch(type);
						if (0 > idx)
							alPlugin.Insert(~idx, new PluginInfo(type, assem, fi.FullName));
					}
				}
				catch (ReflectionTypeLoadException ex)
				{
					Trace.TraceError("{0}.LoadPlugins(); Plugin: {1}; Exception: {2};",
						GetType().Name, fi.FullName, ex);
					if (null != ex.LoaderExceptions)
					{
						foreach (var loaderException in ex.LoaderExceptions)
							Trace.WriteLine(loaderException.ToString());
					}
				}
				catch (Exception ex)
				{
					Trace.TraceError("{0}.LoadPlugins(); Plugin: {1}; Exception: {2};",
						GetType().Name, fi.FullName, ex);
				}
			}
		}

		Type tPluginType;
		string sPluginType;
		public Type PluginType
		{
			get
			{
				return tPluginType;
			}
			set
			{
				if (value == tPluginType)
					return;
				if (value != null && !(value.IsInterface || value.IsClass))
					throw new ArgumentOutOfRangeException("value", value, "value must be interface or class");

				sPluginType = (tPluginType = value).ToString();
				Refresh();
			}
		}

		public PluginInfo[] Plugins
		{
			get
			{
				return (PluginInfo[])alPlugin.ToArray(typeof(PluginInfo));
			}
		}

		public PluginInfo this[int i]
		{
			get
			{
				return null;
			}
		}
	}

	public class PluginInfo : IComparable
	{
		public PluginInfo(Type typ, Assembly asm, string filename)
		{
			PluginType     = typ;
			PluginAssembly = asm;
			PluginFile     = filename;
		}
		public Type     PluginType     { get; }
		public Assembly PluginAssembly { get; }
		public string   PluginFile     { get; }

		public object GetPlugin()
		{
			return Activator.CreateInstance(PluginType);
		}

		public int CompareTo(object o)
		{
			Type t = o as Type;
			if (null != t)
				return PluginType.FullName.CompareTo(t.FullName);
			return PluginType.FullName.CompareTo(((PluginInfo)o).PluginType.FullName);
		}

		public static implicit operator Type(PluginInfo pi)
		{
			return pi.PluginType;
		}
	}
}