﻿namespace RU.NVG.Terminal.Mercury
{
	enum MercuryMessageType
	{
		Position               = 0x01,
		AnalogDigitalConverter = 0x02,
		FuelFromCAN            = 0x03
	}
}