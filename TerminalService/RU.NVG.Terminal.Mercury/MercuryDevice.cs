﻿using System;
using System.Collections;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;

namespace RU.NVG.Terminal.Mercury
{
	public class MercuryDevice : Device
	{
		private const int HeaderLength = 2;
		private const int MessageNumberLength = 2;
		private const int MessageTypeLength = 1;
		private const int TailBytesCount = 2;
		private const int DataBlockLengthLength = 2;

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			var reader = new MercuryReader(data);
			var result = new ArrayList();
			while (true)
			{
				reader.Skip(HeaderLength); //header
				reader.Skip(MessageNumberLength); //message number
				var messageType = (MercuryMessageType) reader.ReadBigEndian32(MessageTypeLength); //message type
				var dataBlockLength = (int) reader.ReadBigEndian32(DataBlockLengthLength);
				if (reader.RemainingBytesCount < dataBlockLength + TailBytesCount)
				{
					reader.Rewind(HeaderLength + MessageNumberLength + MessageTypeLength + DataBlockLengthLength);
					bufferRest = reader.GetRemainingBytes();
					break;
				}

				var mu = UnitReceived(ControllerType.Names.Mercury);
				mu.SensorValues = new Dictionary<int, long>();
				mu.CorrectGPS = false;
				result.Add(mu);
				mu.DeviceID = reader.ReadDeviceId();

				switch (messageType)
				{
					case MercuryMessageType.Position:
						ReadPosition(reader, mu);
						break;
					case MercuryMessageType.AnalogDigitalConverter:
						ReadAnalogDigitalConverter(reader, mu);
						break;
					case MercuryMessageType.FuelFromCAN:
						ReadFuelFromCAN(reader, mu);
						break;
					default:
						reader.Skip(dataBlockLength);
						break;
				}

				reader.Skip(TailBytesCount);
			 
				if (!reader.Any())
				{
					bufferRest = null;
					break;
				}
			}
			
			return result;
		}

		private void ReadFuelFromCAN(MercuryReader reader, MobilUnit mu)
		{
			mu.Time = reader.ReadTime();
			mu.SensorValues.Add((int)MercurySensor.FuelFromCAN, (long)reader.ReadBigEndian64());
		}

		private void ReadAnalogDigitalConverter(MercuryReader reader, MobilUnit mu)
		{
			mu.Time = reader.ReadTime();
			mu.SensorValues.Add((int)MercurySensor.ADC1, reader.ReadBigEndian32(2));
			mu.SensorValues.Add((int)MercurySensor.ADC2, reader.ReadBigEndian32(2));
			mu.SensorValues.Add((int)MercurySensor.ADC3, reader.ReadBigEndian32(2));
			mu.SensorValues.Add((int)MercurySensor.ADC4, reader.ReadBigEndian32(2));
			mu.SensorValues.Add((int)MercurySensor.ADC5, reader.ReadBigEndian32(2));
			mu.SensorValues.Add((int)MercurySensor.ADC6, reader.ReadBigEndian32(2));
		}

		private static void ReadPosition(MercuryReader reader, MobilUnit mu)
		{
			var flags = reader.ReadByte();

			mu.Time = reader.ReadTime();

			mu.Latitude = reader.ReadBigEndian32(4) / 100000.0;
			mu.Longitude = reader.ReadBigEndian32(4)/100000.0;
			var altitude = reader.ReadBigEndian32(2);
			mu.Height = (flags & 0x40) != 0 ? (int)altitude : (int?)null;

			var speedGps = reader.ReadBigEndian32(2) / 10.0;
			mu.Speed = (int)Math.Round(speedGps);

			var speedTaho = reader.ReadBigEndian32(2)/10.0;
			mu.SensorValues.Add((int)MercurySensor.SpeedTaho, (long)Math.Round(speedTaho));

			var odometer = reader.ReadBigEndian32(4);
			mu.Run = (int)odometer;

			reader.Skip(1); //без значении - 1 байт /0x00/
			
			var signalStatus = reader.ReadByte();
			var azimuth = reader.ReadBigEndian32(2);
			mu.Course = (int)azimuth;

			var correctGPS = (flags & 0x80) != 0;
			mu.CorrectGPS = correctGPS;
			if (correctGPS)
				mu.Satellites = 4;
			
			mu.SensorValues.Add((int)MercurySensor.SpeedPulse, signalStatus & 0x01);
			mu.SensorValues.Add((int)MercurySensor.Ignition, (signalStatus >> 1) & 0x01);
			mu.SensorValues.Add((int)MercurySensor.Panic, (signalStatus >> 2) & 0x01);
			mu.SensorValues.Add((int)MercurySensor.Tamper, (signalStatus >> 3) & 0x01);
		}

		public override bool SupportData(byte[] data)
		{
			const int bytesBeforeBlockLength = HeaderLength + MessageNumberLength + MessageTypeLength;
			const int fixedPacketPartSize = bytesBeforeBlockLength + DataBlockLengthLength + TailBytesCount;
			if (data.Length < fixedPacketPartSize)
				return false;

			var dataBlockLength = (data[bytesBeforeBlockLength] << 8) + data[bytesBeforeBlockLength + 1];
			if (data.Length < fixedPacketPartSize + dataBlockLength)
				return false;

			var tailIndex = bytesBeforeBlockLength + 2 + dataBlockLength;
			if (data[tailIndex] != 0x20 || data[tailIndex + 1] != 0x01)
				return false;

			return true;
		}
	}
}