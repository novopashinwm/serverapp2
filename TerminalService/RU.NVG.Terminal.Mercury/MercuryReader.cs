﻿using System;
using System.Globalization;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common;

namespace RU.NVG.Terminal.Mercury
{
	class MercuryReader : DataReader
	{
		public MercuryReader(byte[] data) : base(data)
		{
		}

		public MercuryReader(byte[] data, int startIndex) : base(data, startIndex)
		{
		}

		public MercuryReader(byte[] data, int startIndex, int endIndex) : base(data, startIndex, endIndex)
		{
		}

		public string ReadDeviceId()
		{
			var deviceIdNumber = ReadBigEndian64();
			var deviceIdString = deviceIdNumber.ToString(CultureInfo.InvariantCulture);
			deviceIdString = deviceIdString.PadLeft(15, '0');
			return deviceIdString;
		}

		private static readonly int SecondsSince1970On2000 =
			TimeHelper.GetSecondsFromBase(new DateTime(2000, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc));

		public int ReadTime()
		{
			var secondsSince2000 = ReadBigEndian32(4);
			return SecondsSince1970On2000 + (int)secondsSince2000;
		}
	}
}