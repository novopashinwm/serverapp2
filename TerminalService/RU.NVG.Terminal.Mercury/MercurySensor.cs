﻿namespace RU.NVG.Terminal.Mercury
{
	public enum MercurySensor
	{
		/// <summary>Скорость по тахографу</summary>
		SpeedTaho = 1,
		/// <summary> Датчик движения </summary>
		SpeedPulse = 2,
		/// <summary>Зажигание</summary>
		Ignition = 3,
		/// <summary>Тревога</summary>
		Panic = 4,
		/// <summary>Вскрытие корпуса</summary>
		Tamper = 5,
		/// <summary>Топливо по данным CAN</summary>
		FuelFromCAN = 6,
		/// <summary>АЦП1</summary>
		ADC1 = 11,
		/// <summary>АЦП2</summary>
		ADC2 = 12,
		/// <summary>АЦП3</summary>
		ADC3 = 13,
		/// <summary>АЦП4</summary>
		ADC4 = 14,
		/// <summary>АЦП5</summary>
		ADC5 = 15,
		/// <summary>АЦП6</summary>
		ADC6 = 16
	}
}