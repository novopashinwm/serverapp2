﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using RU.NVG.Protocol.Egts;
using RU.NVG.Protocol.Egts.Enums;
using RU.NVG.Protocol.Egts.Helpers;
using RU.NVG.Protocol.Egts.Packet;
using RU.NVG.Protocol.Egts.Record;
using RU.NVG.Protocol.Egts.Record.Auth;

namespace RU.NVG.NIKA.Terminal.Egts
{
	public class EgtsDevice : Device
	{
		internal const string ProtocolName = EgtsHelper.ProtocolName;
		internal static class DeviceNames
		{
			internal const string Egts           = ProtocolName;
			internal const string GranitAuto     = "GRANIT (Vehicle)";
			internal const string GranitPersonal = "GRANIT (Personal)";
		}
		public EgtsDevice()
			: base(null, new[]
			{
				DeviceNames.Egts,
				DeviceNames.GranitAuto,
				DeviceNames.GranitPersonal
			})
		{
			RequiresAuthorization = false;
		}
		public bool RequiresAuthorization { get; set; }
		public override bool SupportData(byte[] data)
		{
			return data[0] == TransportPacket.ProtocolVersion
				&& data[1] == 0x00  // SKID
				&& data[3] == 0x0B  // HL
				&& data[4] == 0x00  // HE
				&& data[9] == 0x01; // EGTS_PT_APPDATA
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;

			var state = stateData as EgtsState;

			var result = new ArrayList();
			using (var stream = new MemoryStream(data, 0, count))
			using (var egtsReader = new EgtsReader(stream))
			{
				var startedPosition = egtsReader.BaseStream.Position;
				TransportPacket packet;
				while (egtsReader.TryReadPacket(out packet))
				{
					if (packet.ServiceFrame != null && packet.ServiceFrame.Type == PacketType.EgtsPtAppdata)
					{
						var serviceFrame = packet.ServiceFrame;
						if (null == state)
						{
							state = GetAuthentification(packet);
							if (state?.IsAuthentificated ?? false)
							{
								result.AddRange(ProcessAuthConfirmation(packet, state, ResponseResult.EGTS_PC_OK));
								result.Add(new ReceiverStoreToStateMessage {StateData = state});
								startedPosition = egtsReader.BaseStream.Position;
								continue;
							}
						}

						result.AddRange(ProcessConfirmation(packet, state, ResponseResult.EGTS_PC_OK));

						foreach (var serviceRecord in serviceFrame.Records)
						{
							var processResult = ProcessServiceRecord(serviceRecord, state);
							if (processResult != null)
								result.AddRange(processResult);
						}
					}

					startedPosition = egtsReader.BaseStream.Position;
				}

				if (startedPosition < egtsReader.BaseStream.Length)
					bufferRest = egtsReader.GetRest(startedPosition);

				return result;
			}
		}
		private List<object> ProcessServiceRecord(ServiceLayerRecord serviceRecord, EgtsState state)
		{
			switch (serviceRecord.ServiceCode)
			{
				case ServiceCode.EGTS_TELEDATA_SERVICE:
					return ProcessTeledataServiceRecord(serviceRecord, state);
			}

			return null;
		}
		private List<object> ProcessTeledataServiceRecord(ServiceLayerRecord serviceRecord, EgtsState state)
		{
			// Проверяем правильность параметров, либо есть идентификатор в самой записи, или это конечное устройство с Tid и/или Imei.
			var validParameters = serviceRecord.ObjectId.HasValue || (state?.IsAuthentificated ?? false);
			if (!validParameters)
				return null;

			// Определяем deviceId для объекта
			var deviceId = default(string);
			if (null != state && state.IsAuthentificated)
			{
				// Если ObjectId не совпадает с Tid, то это ретрансляция с какого-то сервера, поэтому deviceId=ObjectId
				if (serviceRecord.ObjectId.HasValue && !serviceRecord.ObjectId.Value.Equals(state.Tid))
					deviceId = serviceRecord.ObjectId.Value.ToString();
				// Если ObjectId отсутствует или совпадает с Tid, то это конечное устройство
				else
				{
					if (state.Device) // Если Imei есть, то используем его
						deviceId = state.Imei;
					else
					if (state.Telematica) // Если Imei нет, то используем Tid
						deviceId = state.Tid.ToString();
				}
			}
			else
				// Если есть только ObjectId, deviceId=ObjectId
				deviceId = serviceRecord.ObjectId.Value.ToString();

			var result = serviceRecord.ToMobilUnits();
			foreach (var mobilUnit in result)
				mobilUnit.DeviceID = deviceId;

			return result
				.Cast<object>()
				.ToList();
		}
		private List<object> ProcessConfirmation(TransportPacket packet, EgtsState state, ResponseResult responseResult)
		{
			var bufferSize = TransportPacket.MaxFrameSize;
			if (state != null)
			{
				bufferSize = state.BufferSize ?? bufferSize;
			}

			var serviceFrame = packet.ServiceFrame;
			var response = EgtsPtResponse.Create(packet.Pid, bufferSize);
			var result = new List<object>(serviceFrame.Records.Count());
			foreach (var serviceRecord in serviceFrame.Records)
			{
				if (!response.AddResponse(serviceRecord, responseResult))
				{
					var confirmPacket = GetConfirmPacket(response);
					result.Add(confirmPacket);
					response = EgtsPtResponse.Create(packet.Pid, bufferSize);
					response.AddResponse(serviceRecord, responseResult);
				}
			}

			if (response.Size > 3)
			{
				result.Add(GetConfirmPacket(response));
			}

			return result;
		}
		private List<object> ProcessAuthConfirmation(TransportPacket packet, EgtsState state, ResponseResult responseResult)
		{
			var bufferSize = TransportPacket.MaxFrameSize;
			if (state != null)
			{
				bufferSize = state.BufferSize ?? bufferSize;
			}

			var response = EgtsPtResponse.Create(packet.Pid, bufferSize);

			response.AddAuthResponse(responseResult);
			var result = new List<object>
			{
				GetConfirmPacket(response)
			};
			return result;
		}
		private ConfirmPacket GetConfirmPacket(EgtsPtResponse response)
		{
			var transportPacket = new TransportPacket(response)
			{
				Priority = Priority.Mid
			};
			var confirmationPacket = new ConfirmPacket(transportPacket.GetBytes());
			return confirmationPacket;
		}
		private EgtsState GetAuthentification(TransportPacket packet)
		{
			var authServiceRecord =
				packet.ServiceFrame.Records.FirstOrDefault(r => r.ServiceCode == ServiceCode.EGTS_AUTH_SERVICE);
			if (authServiceRecord == null)
				return null;

			var termIdentity =
				authServiceRecord.GetRecords()
					.FirstOrDefault(r => r.ServiceRecordType == (int) EgtsAuthServiceSubRecords.EGTS_SR_TERM_IDENTITY);
			if (termIdentity == null)
				return null;

			var state = new EgtsState();
			var identity = (EgtsSrTermIdentity)termIdentity;
			state.Imei       = identity.Imei;
			state.BufferSize = identity.BufferSize;
			state.Tid        = identity.Tid;

			return state;
		}
		public override List<CommandStep> GetCommandSteps(IStdCommand command)
		{
			if (command.Target.DeviceType != DeviceNames.GranitAuto &&
				command.Target.DeviceType != DeviceNames.GranitPersonal)
				return base.GetCommandSteps(command);

			switch (command.Type)
			{
				case CmdType.Setup:
					return GetGranitSetupCommandSteps(command);
				case CmdType.Restart:
					return new List<CommandStep>(1) {new SmsCommandStep("BB+RESET")};
				default:
					return base.GetCommandSteps(command);
			}
		}
		private List<CommandStep> GetGranitSetupCommandSteps(IStdCommand command)
		{
			var internetApnConfig = GetInternetApnConfig(command);

			var steps = new List<CommandStep>(4)
			{
				new SmsCommandStep("BB+SRV1PROT=EGTS"),
				new SmsCommandStep(string.Format("BB+SAPN={0},{1},{2}",
					internetApnConfig.Name,
					internetApnConfig.User,
					internetApnConfig.Pass)),
				new SmsCommandStep(string.Format("BB+SRV1={0},{1},{2}", Manager.GetConstant(Constant.ServerIP),
					Manager.GetConstant(Constant.AppDomain), Manager.GetConstant(Constant.ServerPort))),
				new SmsCommandStep("BB+RESET")
			};

			foreach (var step in steps)
				step.WaitAnswer = CmdType.Setup;

			return steps;
		}
	}
}