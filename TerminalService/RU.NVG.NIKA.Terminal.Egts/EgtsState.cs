﻿using System;

namespace RU.NVG.NIKA.Terminal.Egts
{
	[Serializable]
	public class EgtsState
	{
		/// <summary> IMEI терминала </summary>
		public string Imei;
		/// <summary> Размер буфера терминала для чтения </summary>
		public int?   BufferSize;
		/// <summary> Идентификатор терминала </summary>
		public uint   Tid;
		public bool   Device            => !string.IsNullOrEmpty(Imei);
		public bool   Telematica        => Tid > 0;
		public bool   IsAuthentificated => Telematica || Device;
	}
}