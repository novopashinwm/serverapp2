﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;

namespace FORIS.TSS.Terminal.UltraCom
{
	public class DeerCollar : Device
	{
		internal const string ProtocolName = ControllerType.Names.UltracomCollar;
		internal static class DeviceNames
		{
			internal const string UltracomCollar = ControllerType.Names.UltracomCollar;
		}

		private static readonly byte[] MessagePrefix =
			new byte[] { 0x06, 0x0A, 0x10, 0x38, 0x00, 0x7A, 0x01, 0x68 };

		public DeerCollar()
			: base(typeof(DeerCollarSensor), new[] 
			{
				DeviceNames.UltracomCollar
			})
		{
		}

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			var messageStartIndex = Array.IndexOf<byte>(data, 0x4C);
			if (messageStartIndex == -1)
				throw new ArgumentException(@"Cannot find message start", "data");
			bufferRest = null;

			var s = Encoding.ASCII.GetString(data, messageStartIndex, count - messageStartIndex);

			return new ArrayList { ReadMobilUnit(null, s) };
		}

		public override bool SupportData(byte[] data)
		{
			if (data.Length < MessagePrefix.Length)
				return false;

			for (var i = 0; i != MessagePrefix.Length; ++i)
			{
				if (data[i] != MessagePrefix[i])
					return false;
			}

			return true;
		}

		public override IList<object> ParseSms(IUnitInfo ui, string s)
		{
			var mu = ReadMobilUnit(ui, s);
			return new List<object> { mu };
		}

		private MobilUnit.Unit.MobilUnit ReadMobilUnit(IUnitInfo ui, string s)
		{
			Trace.TraceInformation("{0}.ReadMobilUnit: {1}: {2} ({3})",
								   this, ui != null ? ui.Telephone : null, s, Encoding.ASCII.GetBytes(s));

			var reader = new StringReader(s);
			reader.ReadBefore('L');
			reader.Skip('L');
			var hours = reader.ReadDecimalInt(2);
			var minutes = reader.ReadDecimalInt(2);
			var seconds = reader.ReadDecimalInt(2);
			var fixType = reader.ReadOneOf('A', 'V', 'v');
			var latitudeDegrees = reader.ReadDecimalInt(2);
			var latitudeMinutes = reader.ReadDecimal(6);
			var latitudeHemisphere = reader.ReadOneOf('N', 'S');
			var longitudeDegrees = reader.ReadDecimalInt(3);
			var longitudeMinutes = reader.ReadDecimal(6);
			var longitudeHemisphere = reader.ReadOneOf('E', 'W');
			var speedKmph = reader.ReadDecimalInt(3);
			var azimuth = reader.ReadDecimalInt(3);
			reader.Skip('l'); //small l means "Location message"
			var gsmSignalLevel = reader.ReadHexInt(2);
			var batteryLevel = reader.ReadDecimalInt(3);
			reader.Skip(4); //geofencing
			var satellites = reader.ReadHexInt(1);
			var height = reader.ReadDecimalInt(4);
			reader.Skip(2); //bark indication, irrelevant
			var powermode = reader.ReadOneOf('0', '1') - '0';
			reader.Skip(3); //auto send interval
			reader.Skip(60); //irrelevant
			var day = reader.ReadDecimalInt(2);
			var month = reader.ReadDecimalInt(2);
			var year = reader.ReadDecimalInt(2);

			//After date (e.g. 111213) there are six zerosand then the temperature with three digits.
			reader.Skip(6);
			var temperature = reader.ReadDecimalInt(3);
			reader.Skip(24 - 6 - 3);

			var version = reader.ReadString(2);

			var mu = ui != null ? new MobilUnit.Unit.MobilUnit(ui) : UnitReceived();
			mu.Time = TimeHelper.GetSecondsFromBase(
				new DateTime(year + 2000, month, day, hours, minutes, seconds, 0, DateTimeKind.Utc));
			mu.Latitude = (double)(latitudeDegrees + latitudeMinutes / 60);
			if (latitudeHemisphere == 'S')
				mu.Latitude = -mu.Latitude;
			mu.Longitude = (double)(longitudeDegrees + longitudeMinutes / 60);
			if (longitudeHemisphere == 'W')
				mu.Longitude = -mu.Longitude;
			mu.Speed = speedKmph;
			mu.Course = azimuth;
			mu.Satellites = satellites;
			mu.Height = height;
			mu.Firmware = version;
			mu.SensorValues = new Dictionary<int, long>
			{
				{ (int)DeerCollarSensor.BatteryLevel,   batteryLevel   },
				{ (int)DeerCollarSensor.PowerMode,      powermode      },
				{ (int)DeerCollarSensor.GsmSignalLevel, gsmSignalLevel },
				{ (int)DeerCollarSensor.Temperature,    temperature    }
			};
			mu.CorrectGPS = fixType == 'A';
			return mu;
		}
	}
}