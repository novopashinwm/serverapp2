﻿namespace FORIS.TSS.Terminal.UltraCom
{
	public enum DeerCollarSensor
	{
		/// <summary>Уровень батареи, в процентах</summary>
		BatteryLevel   = 1,
		/// <summary> powermode 0=full 1=powersaving</summary>
		PowerMode      = 2,
		/// <summary> GSM signal level 0-31 </summary>
		GsmSignalLevel = 3,
		/// <summary> Температура, градусы Цельсия </summary>
		Temperature    = 4
	}
}