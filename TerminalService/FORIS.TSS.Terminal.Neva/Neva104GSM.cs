﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Neva
{
	public class Neva104GSM : Device
	{
		private static readonly byte[] LastBytesBeforeCRC = new byte[] {0x0D, 0x0A, 0x03};

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;
			
			if (IsRegistrationPacket(data))
			{
				return AnswerToHeartbeat(data, count);
			}

			/*Heart-beat agreement：Meter will send 1 package data 7F 7F 7F as per the heart-beat time interval setting
			 * the server will return 06 after it receives this package data. 
			 * If the meter hasn’t received the return data from server within 10S, 
			 * the meter will send again after 10S. 
			 * It will send 2 times, if the meter still has not receive the return data, 
			 * meter will think the meter is disconnecting to the server, 
			 * then the meter will reconnect to the server automatically*/

			if (data.Length == 3 && data[0] == 0x7f && data[1] == 0x7f && data[2] == 0x7f)
			{
				var state = (Neva104GSMState)stateData;
				
				return new ArrayList 
				{ 
					new ConfirmPacket(new byte[] {0x06}),
					CreateConfirmPacket(state.DeviceAddress)
				};
			}

			//The meter will return the data after sending request information, format of data return:
			var reader = new DataReader(data, 0);
			//02 
			var firstByte=  reader.ReadByte();
			if (firstByte != 0x02)
				return null;
		
			//8-bit address 
			var deviceId = reader.ReadAsciiString(8);
			//8-bit kWh 
			var kWh = reader.ReadAsciiNumber10(8);
			//5-bit power
			var power = reader.ReadAsciiNumber10(5);
			// 5-bit voltage
			var voltage = reader.ReadAsciiNumber10(5);
			//4-bit current 
			var current = reader.ReadAsciiNumber10(4);
			//1-bit relay status  
			var relayStatus = reader.ReadAsciiNumber10(1);
			//3-bit times of cover opening 
			var coverOpeningTimes = reader.ReadAsciiNumber10(3);
			//4-bit heart-beat time interval 
			var heartBeatTimeInterval = reader.ReadAsciiNumber10(4);
			//5-bit port NO  
			reader.Skip(5);
			//1-bit DNS enable bit  
			reader.Skip(1);
			//IP or APN of main station 
			var correctIP = false;
			for (var i = 0; i != 4; ++i)
			{
				var b = reader.ReadByte();

				var correctIpPart = false;
				if (b < '0' || '9' < b)
					return null;

				for (var j = 0; j != 3; ++j)
				{
					b = reader.ReadByte();
					if (b == '.' && i != 3)
					{
						correctIpPart = true;
						break;
					}
					
					if (b == '!' && i == 3)
					{
						correctIP = true;
						correctIpPart = true;
						break;
					}
						
					if (b < '0' || '9' < b)
						return null;
				}

				if (!correctIpPart)
					return null;
			}
			if (!correctIP)
				return null;

			//0D 0A 03 
			foreach (var expectedByte in LastBytesBeforeCRC)
			{
				var readByte = reader.ReadByte();
				if (readByte != expectedByte)
					return null;
			}
			
			//Checksum
			reader.Skip();

			var mu = UnitReceived();
			mu.DeviceID = deviceId;
			mu.Time = TimeHelper.GetSecondsFromBase(DateTime.UtcNow);
			mu.SensorValues = new Dictionary<int, long>
			{
				{(int)Neva104GSMInput.KWh, kWh},
				{(int)Neva104GSMInput.Power, power},
				{(int)Neva104GSMInput.Voltage, voltage},
				{(int)Neva104GSMInput.Current, current},
				{(int)Neva104GSMInput.RelayStatus, relayStatus},
				{(int)Neva104GSMInput.CoverOpeningTimes, coverOpeningTimes},
				{(int)Neva104GSMInput.HeartBeatInterval, heartBeatTimeInterval},
			};
			mu.CorrectGPS = false;
			return new ArrayList 
			{ 
				mu,
				new ConfirmPacket(new byte[] {0x06})
			};
		}

		private static IList AnswerToHeartbeat(byte[] data, int count)
		{
			var s = Encoding.ASCII.GetString(data, 0, count);
			var deviceAddress = s.Substring(2, 8);
			var state = new Neva104GSMState { DeviceAddress = deviceAddress };

			return new ArrayList 
					   {
						   new ReceiverStoreToStateMessage { StateData = state },
						   CreateConfirmPacket(deviceAddress),
					   };
		}

		private static ConfirmPacket CreateConfirmPacket(string deviceAddress)
		{
			var request = new StringBuilder(13);
			request.Append("/?"); //2F 3F 
			request.Append(deviceAddress);
			request.Append("!\r\n");

			return new ConfirmPacket {Data = Encoding.ASCII.GetBytes(request.ToString())};
		}

		public override bool SupportData(byte[] data)
		{
			//2f3f30303030303030303030333021
			return IsRegistrationPacket(data);
		}

		public static bool IsRegistrationPacket(byte[] data)
		{
			//Как только счетчик успешно соединился, он инициирует загрузку 15 байт, 2F  3F (8 байт адреса счетчика) (4 байт пакетного интервала) 21
			//Этот пакет фактически является hearbeat-пакетом
			return data.Length == 15 && 
				   data[0] == 0x2f &&
				   data[1] == 0x3f &&
				   data[data.Length - 1] == 0x21;
		}
	}
}
