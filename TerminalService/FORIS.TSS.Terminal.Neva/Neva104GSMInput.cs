﻿namespace FORIS.TSS.Terminal.Neva
{
    enum Neva104GSMInput
    {
        KWh = 1,
        Power = 2,
        Voltage = 3,
        Current = 4,
        RelayStatus = 5,
        CoverOpeningTimes = 6,
        HeartBeatInterval = 7
    }
}
