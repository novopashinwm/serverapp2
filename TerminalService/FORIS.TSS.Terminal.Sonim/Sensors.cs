﻿namespace FORIS.TSS.Terminal.Sonim
{
	public enum Sensors
	{
		Gsm     = 30,
		Gprs    = 32,
		Battery = 33,
		Mode    = 34
	}
}