﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Sonim.Datagram
{
	public class PositionDatagram : BaseDatagram
	{
		const string ResponseTemplate = @"<?xml version=""1.0\"" encoding=""UTF-8""?><ack><ctag>{0}</ctag><uid>{1}</uid><status>200</status><minutes>{2}</minutes></ack>";
		private const NumberStyles BaseNumberStyle = NumberStyles.AllowCurrencySymbol | NumberStyles.Number;
		private readonly CultureInfo _decimalFormatProvider;

		private string Action
		{
			get
			{
				var result = this["action"];
				if (result == null)
					return null;
				return result.Trim();
			}
		}

		public decimal? Longitude
		{
			get
			{
				var ln = this["lon"];
				if (string.IsNullOrEmpty(ln)) return null;
				decimal result;
				if (decimal.TryParse(ln, BaseNumberStyle, _decimalFormatProvider, out result))
				{
					return result;
				}
				return null;
			}
		}
		public decimal? Latitude
		{
			get
			{
				var lt = this["lat"];
				if (string.IsNullOrEmpty(lt)) return null;
				decimal result;
				if (decimal.TryParse(lt, BaseNumberStyle, _decimalFormatProvider, out result))
				{
					return result;
				}
				return null;
			}
		}

		readonly int _time;
		public int Time
		{
			get
			{
				if (Action != "location")
					return _time;

				var timeString = this["time"];
				if (string.IsNullOrEmpty(timeString))
					return _time;
				DateTime date;
				if (DateTime.TryParseExact(timeString, "yyyyMMddHHmmss", null, DateTimeStyles.None, out date))
					return TimeHelper.GetSecondsFromBase(date);
				return _time;
			}
		}
		public int Speed
		{
			get
			{
				var speedString = this["spd"];
				if (string.IsNullOrEmpty(speedString)) return 0;
				double result;
				if (!double.TryParse(speedString, BaseNumberStyle, _decimalFormatProvider, out result))
					throw new ArgumentException("Can't convert Speed to double type");
				return (int)result;
			}
		}
		public int Course
		{
			get
			{
				var cString = this["dir"];
				if (string.IsNullOrEmpty(cString)) return 0;
				double result;
				if (!double.TryParse(cString, BaseNumberStyle, _decimalFormatProvider, out result))
					throw new ArgumentException("Can't convert Dir to double type");
				return (int)result;
			}
		}
		public bool CorrectGPS
		{
			get
			{
				return
					Longitude.HasValue && Latitude.HasValue
					&&
					(Longitude.Value != 0 || Latitude.Value != 0)
					&& !string.IsNullOrEmpty(this["gps"]) && string.Compare(this["gps"], "y", StringComparison.CurrentCultureIgnoreCase) == 0;
			}
		}
		public int Satellites
		{
			get
			{
				return MobilUnit.Unit.MobilUnit.MinSatellites;
			}
		}
		public Dictionary<int, long> SensorValues
		{
			get
			{
				var result = new Dictionary<int, long>();

				if (!string.IsNullOrEmpty(this["gsm"]))
					result.Add((int)Sensors.Gsm, string.Compare(this["gsm"], "y", StringComparison.CurrentCultureIgnoreCase) != 0 ? 0 : 1);

				if (!string.IsNullOrEmpty(this["gprs"]))
					result.Add((int)Sensors.Gprs, string.Compare(this["gprs"], "y", StringComparison.CurrentCultureIgnoreCase) != 0 ? 0 : 1);

				if (!string.IsNullOrEmpty(this["bat"]))
					result.Add((int)Sensors.Battery, Convert.ToInt32(this["bat"]));

				var mode = this["mode"];
				if (!string.IsNullOrEmpty(mode) && mode != "4")
					result.Add((int)Sensors.Mode, Convert.ToInt32(mode));

				var emergency = this["emg"];
				if (!string.IsNullOrEmpty(emergency))
				{
					switch (emergency)
					{
						case "0":
							result.Add((int)AlarmMessageType.AllOK, 1);
							result.Add((int)AlarmMessageType.AttentionNeededButNotUrgent, 0);
							result.Add((int)AlarmMessageType.AttentionNeededImmediately, 0);
							result.Add((int)AlarmMessageType.FullAlertSituation, 0);
							break;
						case "1":
							result.Add((int)AlarmMessageType.AttentionNeededButNotUrgent, 1);
							result.Add((int)AlarmMessageType.AttentionNeededImmediately, 0);
							result.Add((int)AlarmMessageType.FullAlertSituation, 0);
							break;
						case "2":
							result.Add((int)AlarmMessageType.AttentionNeededButNotUrgent, 0);
							result.Add((int)AlarmMessageType.AttentionNeededImmediately, 1);
							break;
						case "3":
							result.Add((int)AlarmMessageType.AttentionNeededButNotUrgent, 0);
							result.Add((int)AlarmMessageType.FullAlertSituation, 1);
							break;
					}
				}

				return result;
			}
		}

		public CellNetworkRecord[] CellNetworks
		{
			get
			{
				var mcc = this["mcc"];
				if (string.IsNullOrWhiteSpace(mcc))
					return null;

				var mnc = this["mnc"];
				if (string.IsNullOrWhiteSpace(mnc))
					return null;

				int lac;
				if (!int.TryParse(this["lac"], out lac))
					return null;

				int cellid;
				if (!int.TryParse(this["cellid"], out cellid))
					return null;

				return new[]
				{
					new CellNetworkRecord
					{
						LogTime     = _time,
						Number      = 0,
						CountryCode = mcc,
						NetworkCode = mnc,
						LAC         = lac,
						CellID      = cellid
					}
				};
			}
		}

		public PositionDatagram(string paket)
			: base(paket)
		{
			_time = TimeHelper.GetSecondsFromBase(DateTime.UtcNow);
			_decimalFormatProvider = CultureInfo.InvariantCulture;
		}

		public override IList<object> GetResponse()
		{
			var alRes = new List<object>();
			var mu = new MobilUnit.Unit.MobilUnit
			{
				DeviceID     = DeviceId,
				cmdType      = CmdType.Trace,
				Latitude     = Latitude.HasValue  ? (double)Latitude.Value  : MobilUnit.Unit.MobilUnit.InvalidLatitude,
				Longitude    = Longitude.HasValue ? (double)Longitude.Value : MobilUnit.Unit.MobilUnit.InvalidLongitude,
				Time         = Time,
				Speed        = Speed,
				Course       = Course,
				CorrectGPS   = CorrectGPS,
				Satellites   = Satellites,
				SensorValues = SensorValues,
				CellNetworks = CellNetworks
			};
			mu.Properties.Add(DeviceProperty.Protocol, Sonim.ProtocolName);
			alRes.Add(mu);
			return alRes;
		}
		public override string GetResponseString()
		{
			return string.Format(ResponseTemplate,
				this["ctag"],
				this["uid"],
				this["minutes"]);
		}
	}
}