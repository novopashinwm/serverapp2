﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace FORIS.TSS.Terminal.Sonim.Datagram
{
	public abstract class BaseDatagram
	{
		protected XDocument Packet { get; set; }
		public string DeviceId
		{
			get
			{
				var imei = this["imei"];
				if (string.IsNullOrEmpty(imei)) return null;
				return imei;
			}
		}

		protected BaseDatagram(string packet)
		{
			if (!CheckPacket(packet))
				throw new FormatException("Unrecognized format Sonim");

			Packet = XDocument.Parse(packet);
		}

		public static BaseDatagram Init(string paket)
		{
			if (!CheckPacket(paket)) return null;

			return new PositionDatagram(paket);
		}

		public static bool CheckPacket(string packet)
		{
			if (packet == null)
				return false;

			XDocument doc;
			try
			{
				doc = XDocument.Parse(packet);
			}
			catch (XmlException)
			{
				return false; //Unable to parse
			}

			if (!doc.DescendantNodes().Any()) return false;
			if (doc.Root == null) return false;

			return (doc.Root.Name == "rep" || doc.Root.Name == "res")
				&& doc.Root.Elements("action").Any()
				&& doc.Root.Elements("imei").Any();
		}

		protected string this[string key]
		{
			get
			{
				if (Packet.Root == null)
					return null;

				if (!Packet.Root.Elements(key).Any())
					return null;

				var elementByKey = Packet.Root.Element(key);

				if (elementByKey == null)
					return null;

				return elementByKey.Value;
			}
		}

		public abstract string GetResponseString();

		public abstract IList<object> GetResponse();
	}
}