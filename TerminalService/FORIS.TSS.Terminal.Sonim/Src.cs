﻿namespace FORIS.TSS.Terminal.Sonim
{
	public enum Src
	{
		RedKeyPressed      =  0,
		PhoneProblem       =  1,
		NetworkProblem     =  2,
		GPSProblem         =  3,
		PhoneIdle          =  4,
		HighImpact         =  5,
		FreeFall           =  6,
		PhoneTilt          =  7,
		EndToEndDisconnect =  8,
		NotAwake           =  9,
		ServerRequest      = 10,
		AmberKey           = 13,
		Greenkey           = 14,
		NumberKey4Pressed  = 17,
		NumberKey5Pressed  = 18,
		NumberKey6Pressed  = 19,
		NumberKey7Pressed  = 20,
		NumberKey8Pressed  = 21,
		NumberKey9Pressed  = 22,
		GeofenceEntry      = 23,
		GeofenceExit       = 24,
		ChargerConnected   = 25
	}
}