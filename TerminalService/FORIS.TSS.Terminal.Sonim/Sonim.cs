﻿using System.Collections;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Http;
using FORIS.TSS.Terminal.Sonim.Datagram;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Sonim
{
	public class Sonim : Device, IHttpDevice
	{
		internal const string ProtocolName = ControllerType.Names.Sonim;
		public bool CanProcess(IHttpRequest request)
		{
			return BaseDatagram.CheckPacket(request.Text);
		}

		public IList<object> OnData(IHttpRequest request)
		{
			if (!CanProcess(request)) return null;
			var datagram = BaseDatagram.Init(request.Text);
			var result = datagram.GetResponse();
			result.Add(new HttpResponse
			{
				Text = datagram.GetResponseString()
			});
			return result;
		}

		#region Device members
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;
			return null;
		}

		public override bool SupportData(byte[] data)
		{
			return false;
		}
		#endregion

		private static readonly string SonimTemplateParameterPhone = "{" + PARAMS.Keys.Phone + "}";

		private static readonly Dictionary<CmdType, string> SonimCommandTemplates =
			new Dictionary<CmdType, string>
			{
				{ CmdType.CancelAlarm, @"SONIMLW1#REQ#cancelalarm#{ctag}#{uid}#{minutes}#" },
				{ CmdType.ChangeMode, @"SONIMLW1#REQ#changeconfig#{ctag}#{uid}#LoneWorkerMode#{newmode}#{minutes}#" },
				{ CmdType.ReloadDevice, @"SONIMLW1#REQ#restart#{ctag}#{uid}#{minutes}#" },
				{ CmdType.VibrateRequest, @"SONIMLW1#REQ#vibrate#{ctag}#{uid}#{minutes}#" },
				{ CmdType.Callback, @"SONIMLW1#REQ#callphone#{ctag}#{uid}#" + SonimTemplateParameterPhone + "#" },
				{ CmdType.ShutdownDevice, @"SONIMLW1#REQ#shutdown#{ctag}#{uid}#{minutes}#" }
			};

		public override List<CommandStep> GetCommandSteps(IStdCommand command)
		{
			string template;
			if (!SonimCommandTemplates.TryGetValue(command.Type, out template))
				return base.GetCommandSteps(command);

			var commandText = template
				.Replace("{uid}", "1")
				.Replace("{ctag}", "1")
				.Replace("{minutes}", "1")
				.Replace(SonimTemplateParameterPhone,
						 command.Params.ContainsKey(PARAMS.Keys.Phone)
							 ? (string) command.Params[PARAMS.Keys.Phone]
							 : "")
				.Replace("{newmode}",
						 command.Params.ContainsKey("newmode") ? command.Params["newmode"].ToString() : "1");

			return new List<CommandStep> {new SmsCommandStep(commandText)};
		}
	}
}