﻿namespace FORIS.TSS.Terminal.Sonim
{
	public enum AlarmMessageType
	{
		AllOK                       = 26,
		AttentionNeededButNotUrgent = 27,
		AttentionNeededImmediately  = 28,
		FullAlertSituation          = 29,
	}
}