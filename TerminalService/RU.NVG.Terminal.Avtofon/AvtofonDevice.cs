﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using FORIS.TSS.TerminalService.Interfaces.Exceptions;

namespace RU.NVG.Terminal.Avtofon
{
	public class AvtofonDevice : Device
	{
		internal const string ProtocolName = "AvtoFon SE";
		internal static class DeviceNames
		{
			internal const string AvtofonSE = "AvtoFon SE";
		}
		public AvtofonDevice() : base(typeof(AvtofonSensor), new[]
		{
			DeviceNames.AvtofonSE,
		})
		{
			SupportsShortNumber = false;
		}
		public override bool SupportData(byte[] data)
		{
			return IsAuthorizationPacket(data);
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			if (IsAuthorizationPacket(data))
			{
				bufferRest = null;
				return ProcessAuthorizationPacket(data);
			}

			var dataReader = new DataReader(data, 0, count - 1);

			var packetType = dataReader.ReadByte();
			var result = new ArrayList();
			switch (packetType)
			{
				case WorkPacketType:
					if (count < WorkPacketLength)
					{
						bufferRest = data;
						return null;
					}
					if (!CheckCrc(data, WorkPacketLength))
					{
						bufferRest = null;
						return null;
					}
					ReadWorkPacket(dataReader, result);
					break;
				case BufferPacketType:
					if (count < BufferPacketLength)
					{
						bufferRest = data;
						return null;
					}
					if (!CheckCrc(data, BufferPacketLength))
					{
						bufferRest = null;
						return null;
					}

					ReadBufferPacket(dataReader, result);
					break;
			}

			var avtofonState = stateData as AvtofonReceiverState;
			if (avtofonState != null)
			{
				foreach (var item in result)
				{
					var mu = item as IMobilUnit;
					if (mu == null)
						continue;
					mu.DeviceID = avtofonState.DeviceID;
					mu.Firmware = avtofonState.Firmware.ToString(CultureInfo.InvariantCulture);
				}
			}

			bufferRest = dataReader.GetRemainingBytes();
			return result;
		}
		public override List<CommandStep> GetCommandSteps(IStdCommand command)
		{
			switch (command.Type)
			{
				case CmdType.Setup:
					return Setup(command);
				case CmdType.Callback:
					return PhoneCallback(command);
				case CmdType.SetModeOnline:
					return SetModeOnline(command);
				case CmdType.SetModeWaiting:
					return SetModeWaiting(command);
				case CmdType.SetModeBeacon:
					return SetModeBeacon(command);
				case CmdType.EnableMotionDetector:
					return EnableMotionDetector(command);
				case CmdType.DisableMotionDetector:
					return DisableMotionDetector(command);
				default: return base.GetCommandSteps(command);
			}
		}
		private const byte WorkPacketType = 0x11;
		private const int WorkPacketLength = 78;

		private const int BufferPacketType = 0x12;
		private const int BufferPacketLength = 257;
		private void ReadBufferPacket(DataReader dataReader, ArrayList result)
		{
			//тип памяти, из которой осуществляется отправка (SRAM или FLASH), количество валидных блоков
			var packetCount = dataReader.ReadByte() & 0x7; //3 бита
			dataReader.Skip(2);
			for (var i = 0; i != packetCount; ++i)
			{
				result.Add(ReadMobilUnitFromBuffer(dataReader));
			}

			for (var i = packetCount; i < 6; ++i)
			{
				dataReader.Skip(42);
			}
		}
		private MobilUnit ReadMobilUnitFromBuffer(DataReader dataReader)
		{
			var mu = UnitReceived(ProtocolName);
			mu.SensorValues = new Dictionary<int, long>();

			ReadState(dataReader, mu);
			ReadPowerVoltage(dataReader, mu);
			ReadDateTime(dataReader, mu);
			ReadTemperature(dataReader, mu);
			ReadLbsCellNetwork(dataReader, mu);
			ReadGps(dataReader, mu);
			//резерв
			dataReader.Skip(2);
			//CRC
			dataReader.Skip();
			return mu;
		}
		private void ReadWorkPacket(DataReader dataReader, ArrayList result)
		{
			var mu = UnitReceived(ProtocolName);
			mu.SensorValues = new Dictionary<int, long>();

			//интервал отправки данных в секундах
			dataReader.Skip();

			//различные установки маяка
			dataReader.Skip(8);

			//состояние маяка (вход, выход, акселерометр и т .п)
			ReadState(dataReader, mu);

			//оставшееся время работы канала в секундах (если канал выключен, то 0x00 0x00)
			dataReader.Skip(2);

			//напряжение питания батареи в вольтах (*0.05) --> mV
			ReadPowerVoltage(dataReader, mu);

			ReadDateTime(dataReader, mu);

			//Будильники
			dataReader.Skip(24);

			ReadTemperature(dataReader, mu);

			ReadLbsCellNetwork(dataReader, mu);

			ReadGps(dataReader, mu);
			//резерв
			dataReader.Skip(2);

			result.Add(mu);
		}
		private static void ReadGps(DataReader dataReader, MobilUnit mu)
		{
			var gpsStatus = dataReader.ReadByte();
			mu.Satellites = gpsStatus & 0x3f;
			var correctGPS = (gpsStatus >> 6) != 0;
			mu.CorrectGPS = correctGPS;

			
			if (correctGPS)
			{
				var gpsDateTime = dataReader.ReadDateTime();
				var latValue = dataReader.ReadBigEndian32(4);
				var lngValue = dataReader.ReadBigEndian32(4);
				var height = dataReader.ReadBigEndian32(2);
				var speedKnots = dataReader.ReadByte();
				var course = dataReader.ReadByte()*2;
				mu.Time = TimeHelper.GetSecondsFromBase(gpsDateTime);
				mu.Latitude = CoordinateToDegrees(latValue);
				mu.Longitude = CoordinateToDegrees(lngValue);
				mu.Height = (int) height;
				mu.Speed = (int) (speedKnots*1.852m);
				mu.Course = course;
			}
			else
			{
				dataReader.Skip(18);
			}

			//HDOP
			dataReader.Skip(2);
		}
		private static double CoordinateToDegrees(uint rawValue)
		{
			var value = (int) rawValue;
			var degrees = value/1000000;
			var minutes = (value%1000000)/10000d;
			return degrees + minutes/60;
		}
		private static void ReadDateTime(DataReader dataReader, MobilUnit mu)
		{
			var dateTime = dataReader.ReadDateTime();
			mu.Time = TimeHelper.GetSecondsFromBase(dateTime);
		}
		private static void ReadLbsCellNetwork(DataReader dataReader, MobilUnit mu)
		{
			var signalStrength = dataReader.ReadByte();
			var mcc = dataReader.ReadBigEndian32(2);
			var mnc = dataReader.ReadBigEndian32(2);
			var lac = dataReader.ReadBigEndian32(2);
			var cid = dataReader.ReadBigEndian32(2);

			if (signalStrength != 0xff)
			{
				mu.CellNetworks = new[]
					{
						new CellNetworkRecord
							{
								LogTime = mu.Time,
								Number = 0,
								CountryCode = mcc.ToString(CultureInfo.InvariantCulture),
								NetworkCode = mnc.ToString(CultureInfo.InvariantCulture),
								LAC = (int) lac,
								CellID = (int) cid,
								SignalStrength = signalStrength
							}
					};
			}
		}
		private static void ReadTemperature(DataReader dataReader, MobilUnit mu)
		{
			var temperature = (sbyte) dataReader.ReadByte();
			if (temperature != -100)
				mu.AddSensor(AvtofonSensor.Temperature, temperature);
		}
		private static void ReadPowerVoltage(DataReader dataReader, MobilUnit mu)
		{
			var powerVoltage = dataReader.ReadByte()*0.05*1000; //mV
			mu.AddSensor(AvtofonSensor.PowerVoltage, (long) powerVoltage);
		}
		private static void ReadState(DataReader dataReader, MobilUnit mu)
		{
			var state = dataReader.ReadByte();
			mu.AddSensorBit(AvtofonSensor.ExternalPower, state, 0);
			mu.AddSensorBit(AvtofonSensor.AlarmInput, state, 1);
			mu.AddSensorBit(AvtofonSensor.AlarmButton, state, 2);
			mu.AddSensorBit(AvtofonSensor.Motion, state, 3);
		}
		private static IList ProcessAuthorizationPacket(byte[] data)
		{
			var reader = new DataReader(data, 2);
			var firmware = (char) reader.ReadByte();
			var imei = new StringBuilder(15);
			imei.Append(reader.ReadByte() & 0x0F);
			for (var i = 0; i != 7; ++i)
			{
				var b = reader.ReadByte();
				imei.Append((b & 0xF0) >> 4);
				imei.Append(b & 0x0F);
			}
			var receiverState = new AvtofonReceiverState
				{
					DeviceID = imei.ToString(),
					Firmware = firmware
				};

			var answerBytes = new List<byte>(Encoding.ASCII.GetBytes("resp_crc="));
			answerBytes.Add(CalcCrc(data, 11));

			return new ArrayList
				{
					new ConfirmPacket {Data = answerBytes.ToArray()},
					new ReceiverStoreToStateMessage {StateData = receiverState}
				};
		}
		private static bool IsAuthorizationPacket(byte[] data)
		{
			if (data.Length != 12 || data[0] != 0x10)
				return false;

			return CheckCrc(data, data.Length);
		}
		private static byte CalcCrc(byte[] data, int count)
		{
			byte result = 0x3b;
			for (var i = 0; i != count; ++i)
			{
				var b = data[i];
				result += (byte)(0x56 ^ b);
				result++;
				result ^= (byte)(0xC5 + b);
				result--;
			}
			return result;
		}
		private static bool CheckCrc(byte[] data, int count)
		{
			return CalcCrc(data, count - 1) == data[data.Length - 1];
		}
		private List<CommandStep> EnableMotionDetector(IStdCommand command)
		{
			return Commands(
				command.Target,
				new SmsCommandStep("MEMS=1"));

		}
		private List<CommandStep> DisableMotionDetector(IStdCommand command)
		{
			return Commands(
				command.Target,
				new SmsCommandStep("MEMS=0"));

		}
		private List<CommandStep> SetModeBeacon(IStdCommand command)
		{
			return Commands(
				command.Target,
				SetupTime(),
				Sleep());
		}
		private static SmsCommandStep Sleep()
		{
			return new SmsCommandStep("sleep");
		}
		private List<CommandStep> SetModeWaiting(IStdCommand command)
		{
			return Commands(
				command.Target,
				SetupTime(),
				Sleep()
			);
		}
		private List<CommandStep> SetModeOnline(IStdCommand command)
		{
			return Commands(
				command.Target,
				SetupTime(),
				new SmsCommandStep("online"));
		}
		private List<CommandStep> PhoneCallback(IStdCommand command)
		{
			var callingPhoneNumber = command.Params[PARAMS.Keys.Phone] as string ?? command.Params[PARAMS.Keys.ReplyToPhone] as string;
			if (string.IsNullOrEmpty(callingPhoneNumber))
				throw new CommandSendingFailureException(CmdResult.PhoneNumberIsUnknown);

			return Commands(command.Target, new SmsCommandStep("+" + callingPhoneNumber), new SmsCommandStep("A") { WaitAnswer = null});
		}
		private readonly string _setupCommandParameter = new SetupCommandParameterBuilder().ToString();

		private class SetupCommandParameterBuilder
		{
			private const char WorkMode = '1';
			private const char Language  = '1';
			private const char SmsWait  = '*';
			private const char DelayBeforeSleep  = '*';
			private const char AudioTime  = '*';
			private const char LocationTime  = '*';
			private const char AGPS  = '*';
			private const char SmsView  = '7';
			private const char PasswordBruteForce  = '*';
			private const char BatteryLow  = '*';
			private const char OuterPower  = '*';
			private const char OuterInputMode  = '*';
			private const char SosMode = '*';
			private const char AccelerometerMode  = '*';
			private const char AlarmMode  = '*';
			private const char AccelerometerSensitivity  = '*';
			private const char StartMovingReactivationTime  = '*';
			private const char AlarmCoodrinates  = '*';
			private const char BufferUnsentGprs  = '1';
			private const char Reserve20  = '*';
			private const char Reserve21  = '*';
			private const char Reserve22  = '*';
			private const char Reserve23  = '*';
			private const char Reserve24  = '*';
			private const char Reserve25  = '*';

			public override string ToString()
			{
				return
					new string(new[]
						{
							WorkMode, Language, SmsWait, DelayBeforeSleep, AudioTime, LocationTime, AGPS, SmsView,
							PasswordBruteForce, BatteryLow, OuterPower, OuterInputMode, SosMode, AccelerometerMode,
							AlarmMode, AccelerometerSensitivity, StartMovingReactivationTime, AlarmCoodrinates,
							BufferUnsentGprs, Reserve20, Reserve21, Reserve22, Reserve23, Reserve24, Reserve25
						});
			}
		}
		private List<CommandStep> Setup(IStdCommand command)
		{
			var setupTime = SetupTime();

			var internetApnConfig = GetInternetApnConfig(command);

			var tomorrow = DateTime.UtcNow.Date.AddDays(1);
			
			return Commands(
					command.Target,
					//TODO: вытащить номер из параметров команды
					//Устанавливает номер телефона владельца, номер телефона совпадает с логином
					new SmsCommandStep("+" + command.Sender.Login),
					new SmsCommandStep("GMT=00"),
					setupTime,
					new SmsCommandStep("T1=" + tomorrow.ToString("ddMMyyyy") + ",1200,12H"),
					new SmsCommandStep("T2=1234,T2=01012030,1201,12H,G "),
					//Тонкая настройка режима online
					new SmsCommandStep("setup=0125560731411315601000000"),
					//Настройка режима отслеживания движения
					new SmsCommandStep("MEMS=1"),

					new SmsCommandStep("i1=" + internetApnConfig.Name),
					new SmsCommandStep("i2=" + Manager.GetConstant(Constant.ServerIP) + "." + Manager.GetConstant(Constant.ServerPort)),
					new SmsCommandStep("i3=060")
				);
		}
		private static SmsCommandStep SetupTime()
		{
			const string dtf = "ddMMyyyy,HHmm";
			var timeString = DateTime.UtcNow.ToString(dtf);
			var setupTime = new SmsCommandStep("TIME=" + timeString);
			return setupTime;
		}
		private List<CommandStep> Commands(IUnitInfo unitInfo, params SmsCommandStep[] smsCommands)
		{
			var password = unitInfo.Password;
			if (string.IsNullOrWhiteSpace(password))
				password = "1234";

			foreach (var smsCommand in smsCommands)
			{
				smsCommand.Text = password + "," + smsCommand.Text;
			}

			return smsCommands.Cast<CommandStep>().ToList();
		}
		public override IList<object> ParseSms(IUnitInfo ui, string text)
		{
			var result = new List<object>();
			string imei = TryGetImei(text);
			if (imei != null)
				result.Add(ImeiReceived(ui, imei));

			//TODO: разбирать координаты из SMS

			result.Add(CommandCompleted(ui, CmdResult.Completed, CmdType.Unspecified));

			return result;
		}
		private static string TryGetImei(string text)
		{
			const string imeiPrefix = "IMEI: ";
			if (imeiPrefix.Length + ImeiHelper.ImeiLength > text.Length) return null;
			var imeiPrefixStart = text.IndexOf(imeiPrefix, StringComparison.Ordinal);
			if (imeiPrefixStart == -1)
				return null;
			if (text.Length < imeiPrefixStart + imeiPrefix.Length + ImeiHelper.ImeiLength)
				return null;

			var imei = text.Substring(imeiPrefixStart + imeiPrefix.Length, ImeiHelper.ImeiLength);
			if (!ImeiHelper.IsImeiCorrect(imei))
				return null;

			return imei;
		}
	}
}