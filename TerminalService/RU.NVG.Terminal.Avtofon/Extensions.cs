﻿using System;
using FORIS.TSS.Common;
using FORIS.TSS.MobilUnit.Unit;

namespace RU.NVG.Terminal.Avtofon
{
	public static class Extensions
	{
		public static void AddSensor(this MobilUnit mu, AvtofonSensor sensor, long value)
		{
			mu.SensorValues.Add((int)sensor, value);
		}
		public static void AddSensorBit(this MobilUnit mu, AvtofonSensor sensor, byte b, int bit)
		{
			mu.SensorValues.Add((int)sensor, (b >> bit) & 0x01);
		}
		public static DateTime ReadDateTime(this DataReader reader)
		{
			var day      = reader.ReadByte();
			var month    = reader.ReadByte();
			var year     = reader.ReadByte();
			var hour     = reader.ReadByte();
			var minute   = reader.ReadByte();
			var second   = reader.ReadByte();
			var dateTime = new DateTime(year + 2000, month, day, hour, minute, second, DateTimeKind.Utc);
			return dateTime;
		}
		public static void SkipDateTime(this DataReader reader)
		{
			reader.Skip(6);
		}
	}
}