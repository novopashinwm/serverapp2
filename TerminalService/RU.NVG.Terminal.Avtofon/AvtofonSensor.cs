﻿namespace RU.NVG.Terminal.Avtofon
{
	public enum AvtofonSensor
	{
		ExternalPower = 1,
		AlarmInput    = 2,
		AlarmButton   = 3,
		Motion        = 4,
		PowerVoltage  = 5,
		Temperature   = 6
	}
}