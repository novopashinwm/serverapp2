using System;

namespace FORIS.TSS.Terminal.Messaging
{
    /// <summary>
    /// type of object used in message
    /// </summary>
    [Flags]
    public enum ObjectType
    {
        /// <summary>
        /// ��������� ���������� <see cref="IUnitInfo"/>
        /// </summary>
        Controller	    = 0x0100,
        /// <summary>
        /// �������� - ��������� <see cref="ITerminal"/>
        /// </summary>
        Terminal	    = 0x0200,
        Server		    = 0x0400,
        Client		    = 0x0800,
        Operator	    = 0x1000,
        Asid            = 0x2000,
        Phone           = 0x4000,
        Email           = 0x8000,

        AllSources      = 0x0600,
        AllDestinations = 0xf900,
        All             = 0xff00
    }
}