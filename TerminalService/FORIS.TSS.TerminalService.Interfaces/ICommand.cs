﻿using System;
using System.Runtime.InteropServices;

namespace FORIS.TSS.Terminal
{
	/// <summary> Базовый интерфейс для всех типов команд </summary>
	[Guid("5ECBEE5A-F3B9-4a37-B8FA-EFB19228B3DE")]
	public interface ICommand
	{
		/// <summary> Дополнительная информация о команде, зависящая от контекста </summary>
		object Tag  { get; set; }
		/// <summary> Текст команды </summary>
		string Text { get; set; }
	}
}