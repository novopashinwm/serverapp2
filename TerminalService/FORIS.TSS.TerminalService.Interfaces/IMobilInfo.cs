﻿namespace FORIS.TSS.TerminalService.Interfaces
{
	/// <summary> Информация о мобильном объекте </summary>
	public interface IMobilInfo
	{
		/// <summary> Идентификатор маршрута </summary>
		int    RouteID        { get; set; }
		/// <summary> Номер расписания (надо заменить на идентификатор расписания) </summary>
		string Schedule       { get; set; }
		/// <summary> Отклонение от расписания по времени </summary>
		int    TimeDeviation  { get; set; }
		/// <summary> Отклонение от траектории движения </summary>
		int    RouteDeviation { get; set; }
		/// <summary> Идентификатор зоны </summary>
		int    ZoneID         { get; set; }
	}
}