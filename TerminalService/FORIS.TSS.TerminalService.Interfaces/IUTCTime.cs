﻿namespace FORIS.TSS.Terminal
{
	/// <summary> Информация о времени </summary>
	public interface IUTCTime
	{
		/// <summary> Время в секундах от 01.01.1970 </summary>
		/// <remarks> Данный формат времени используется в данных, приходящих от мобильных объектов </remarks>
		int Time        { get; set; }
		/// <summary> Время поступления последней корректной позиции </summary>
		int CorrectTime { get; set; }
	}
}