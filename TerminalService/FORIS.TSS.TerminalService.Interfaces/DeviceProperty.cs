﻿using System;

namespace FORIS.TSS.TerminalService.Interfaces
{
	[Serializable]
	public enum DeviceProperty
	{
		/// <summary> IMEI трекера </summary>
		IMEI     = 0,
		/// <summary> Пароль для управления настройками </summary>
		Password = 1,
		/// <summary> IMSI установленной SIM-карты </summary>
		IMSI     = 2,
		/// <summary> Версия прошивки </summary>
		Firmware = 3,
		/// <summary> Протокол, по которому передаются данные с трекера </summary>
		Protocol = 4
		//TODO: добавить атрибуты типа "Часовой пояс", номер телефона и т.п.
	}
}