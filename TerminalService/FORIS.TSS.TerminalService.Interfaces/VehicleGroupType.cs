﻿namespace FORIS.TSS.TerminalService.Interfaces
{
    /// <summary>Тип группы устройств.</summary>
    /// <remarks>Строковое значение <see cref="VehicleGroupType.ToString()"/> хранится в таблице VehicleGroup, поле Type.</remarks>
    public enum VehicleGroupType
    {
        /// <summary>Тип группы, соответствующий роли "Дети"</summary>
        Child,
        /// <summary>Тип группы, соответствующий роли "Родители"</summary>
        Parent,
        /// <summary>Тип системной группы "Семья". Объединяет контакты в одну семью.</summary>
        /// <remarks>Такие группы нигде не отображаются и участвуют только в системной бизнес-логике.</remarks>
        Family
    }
}
