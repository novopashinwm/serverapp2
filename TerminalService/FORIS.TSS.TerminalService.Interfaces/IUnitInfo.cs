﻿using System;
using System.Collections.Generic;
using System.Net;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.Terminal
{
	/// <summary> Информация о контроллере мобильного объекта </summary>
	public interface IUnitInfo : ICloneable, IComparable
	{
		/// <summary> Vehicle ID (DB identifier) </summary>
		int Unique { get; set; }
		/// <summary> Идентификатор хранилища </summary>
		short StorageId { get; set; }
		/// <summary> Unit no. (controller number, not DB identifier) </summary>
		int ID { get; set; }
		/// <summary> Тип оборудования </summary>
		string DeviceType { get; set; }
		/// <summary> Идентификатор устройства, как правило IMEI или серийный номер </summary>
		string DeviceID { get; set; }
		/// <summary> Идентификатор мобильного приложения </summary>
		string AppId { get; set; }
		/// <summary> Phone no </summary>
		string Telephone { get; set; }
		/// <summary> IMSI, переданный устройством </summary>
		string IMSI { get; set; }
		/// <summary> IP адрес мобильного объекта </summary>
		IPEndPoint IP { get; set; }
		/// <summary> Порт, через который были получены данные </summary>
		int Port { get; set; }
		/// <summary> пароль устройства </summary>
		string Password { get; set; }
		/// <summary> Идентификатор приложения обслуживания устройства - сущности к которой привязываются услуги </summary>
		int? AsidId { get; set; }
		/// <summary> Маскированный MSISDN устройства - F(MSISDN) </summary>
		string Asid { get; set; }
		/// <summary> Уникальный идентификатор SIM-карты абонента </summary>
		string SimId { get; set; }
		/// <summary> Дата блокировки услуг на данном MSISDN устройства </summary>
		DateTime? BlockingDate { get; set; }
		/// <summary> Следует ли сохранять данные объекта наблюдения в БД </summary>
		bool StoreLog { get; set; }
		/// <summary> Разрешено ли удаленному терминалу удалять на сервере лог по обслуживаемому объекту наблюдению </summary>
		bool AllowDeleteLog { get; set; }
		/// <summary> Информация о маппинге датчиков </summary>
		Dictionary<SensorLegend, SensorMap[]> SensorMap { get; set; }
		/// <summary> Версия прошивки устройства по данным настроек устройства </summary>
		string FirmwareVersion { get; set; }
	}
}