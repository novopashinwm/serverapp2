﻿using System.Text;
using FORIS.TSS.Terminal;

namespace FORIS.TSS.TerminalService.Interfaces
{
	/// <summary> Пакет для ответа мобильному устройству на его запрос </summary>
	public class ConfirmPacket
	{
		public ConfirmPacket()
		{
		}
		public ConfirmPacket(byte[] data)
		{
			Data = data;
		}
		public byte[] Data;
		public IUnitInfo UnitInfo;
		public ConfirmPacket(string @string)
		{
			Data = Encoding.ASCII.GetBytes(@string);
		}
	}
}