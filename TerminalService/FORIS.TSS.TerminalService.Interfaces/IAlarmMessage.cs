namespace FORIS.TSS.Terminal.Messaging
{
    /// <summary>
    /// alarm message
    /// </summary>
    public interface IAlarmMessage : ITextMessage
    {
    }
}