﻿using System.Collections.Generic;

namespace FORIS.TSS.TerminalService.Interfaces
{
	/// <summary> Устройство, взаимодействие с которым осуществляется по протоколу HTTP </summary>
	public interface IHttpDevice
	{
		/// <summary> Возвращает true, если ли устройство может обработать такой запрос </summary>
		bool CanProcess(IHttpRequest request);
		/// <summary> Возвращает данные, полученные из http-запроса </summary>
		IList<object> OnData(IHttpRequest request);
	}
}