using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.Terminal.Messaging
{
    /// <summary>
    /// text message
    /// </summary>
    public interface ITextMessage : IMessage
    {
        string Code{get; set;}
        string Text{get; set;}
        MessageState State{get; set;}
    }
}