﻿using System;
using FORIS.TSS.Common;

namespace FORIS.TSS.Terminal.Messaging
{
	class CommandMessage : ICommandMessage
	{
		public CommandMessage(ICommand cmd)
		{
			Command = cmd;
		}
		
		protected IStdCommand cmd;

		#region ICommandMessage Members

		public ICommand Command
		{
			get
			{
				return cmd;
			}
			set
			{
				if(value is IStdCommand) cmd = value as IStdCommand;
				else throw new NotImplementedException("IStdCommand only supported");
			}
		}

		#endregion

		int iNum = -1;
		Direction enDir = Direction.Outgoing;
		DateTime dtTime = DateTime.UtcNow;
		Severity enSeverity = Severity.Lvl3;
		bool bConfirm;
		bool bLog;

		#region IMessage Members

		public int Num
		{
			get
			{
				return iNum;
			}
			set
			{
				iNum = value;
			}
		}

		public Direction Direction
		{
			get
			{
				return enDir;
			}
			set
			{
				enDir = value;
			}
		}

		public virtual MessageCategory Category
		{
			get
			{
				return MessageCategory.Command;
			}
			set
			{
			}
		}

		public MessageObject Sender
		{
			get
			{
				return new MessageObject(ObjectType.Operator, cmd.Sender, Direction.Outgoing);
			}
			set
			{
			}
		}

		public MessageObject Target
		{
			get
			{
				return new MessageObject(
					ObjectType.Controller, cmd.Target, Direction.Incoming);
			}
			set
			{
			}
		}

		public DateTime Time
		{
			get
			{
				return dtTime;
			}
			set
			{
				dtTime = value;
			}
		}

		public int Status
		{
			get
			{

				return 0;
			}
			set
			{

			}
		}

		public Severity Severity
		{
			get
			{
				return enSeverity;
			}
			set
			{
				enSeverity = value;
			}
		}

		public bool Confirm
		{
			get
			{
				return bConfirm;
			}
			set
			{
				bConfirm = value;
			}
		}

		public bool Log
		{
			get
			{
				return bLog;
			}
			set
			{
				bLog = value;
			}
		}

		public PARAMS Params
		{
			get
			{
				return cmd.Params;
			}
			set
			{
			}
		}

		public int? TermID { get; set; }

		#endregion
	}
}