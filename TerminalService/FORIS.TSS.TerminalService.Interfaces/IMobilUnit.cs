﻿using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.MobilUnit;
using FORIS.TSS.Terminal;

namespace FORIS.TSS.TerminalService.Interfaces
{
	/// <summary> Базовый интерфейс для всех типов мобильных объектов </summary>
	public interface IMobilUnit : IMobilInfo, IGPSData, IMobilStatus
	{
		/// <summary> Command type to identify response added </summary>
		CmdType cmdType { get; set; }
		/// <summary> ID терминала, с помощью которого принята информация </summary>
		int     TermID  { get; set; }
		/// <summary>Тип канала, через который был получены данные</summary>
		Media   Media   { get; set; }
		/// <summary> Вычисление курса мобильного объекта по 2м последним географическим точкам </summary>
		/// <param name="latitude"> Географическая широта предпоследней позиция </param>
		/// <param name="longitude"> Географическая долгота предпоследней позиция </param>
		/// <returns> Курс мобильного объекта </returns>
		int CalcCourse(double latitude, double longitude);
		int CalcCourse(IUnitPosition last);
	}
}