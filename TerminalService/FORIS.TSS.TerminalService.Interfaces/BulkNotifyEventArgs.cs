﻿using System;
using FORIS.TSS.Terminal;

namespace FORIS.TSS.TerminalService.Interfaces
{
	/// <summary> Аргумент события наличия сообщений для передачи большого количества сообщений одновременно </summary>
	/// <remarks> Передача события через remoting по одному <see cref="NotifyEventArgs"/> может приводить к падению производительности,
	/// т.к. ресурсы системы будут поглощены накладными расходами на организацию межпроцессного взаимодействия. </remarks>
	[Serializable]
	public class BulkNotifyEventArgs : EventArgs
	{
		public NotifyEventArgs[] SingleEventArgs;
	}
}