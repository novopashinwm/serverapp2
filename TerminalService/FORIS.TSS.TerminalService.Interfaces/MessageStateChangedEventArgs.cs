using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.TerminalService.Interfaces
{
    public class MessageStateChangedEventArgs : EventArgs
    {
        public int MessageId;
        public MessageState State;
        public MessageProcessingResult ProcessingResult;
    }
}