﻿using System.Data;

namespace FORIS.TSS.Common
{
	/// <summary> context. applied to context supporting object </summary>
	public interface IContext
	{
		/// <summary> init with filter ds </summary>
		/// <param name="ds"> filter ds </param>
		void Init(DataSet ds);
		/// <summary> apply filter to IGuarded </summary>
		/// <param name="obj"> source obj </param>
		/// <returns> object after filter </returns>
		IGuarded Check(IGuarded obj);
	}
	/// <summary> context supporting. must be inherited by any checked object </summary>
	public interface IGuarded
	{
		/// <summary> object after checking </summary>
		IGuarded Checked { get; }
		/// <summary> object without checking </summary>
		object Unchecked { get; }
		/// <summary> context object applied to this object </summary>
		IContext Context { get; }
		/// <summary> type of rechecking. true - every important change, false - manual by IGuarded.Requery() </summary>
		bool Auto        { get; set; }
		/// <summary> refresh context </summary>
		/// <returns> ISecurityContext.Checked after refresh </returns>
		IGuarded Requery();
	}
}