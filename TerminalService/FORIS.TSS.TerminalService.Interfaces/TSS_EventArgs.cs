﻿using System;
using System.Collections;
using FORIS.TSS.CommonTypes;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal
{
	/// <summary> Аргумент событий охранных функций </summary>
	[Serializable]
	public class GuardEventArgs : EventArgs
	{
		/// <summary> номер ТС </summary>
		private readonly int unigue;
		/// <summary> События охранных функций </summary>
		private readonly GuardEvents events;
		/// <summary> Показания цифровых датчиков </summary>
		/// <remarks> Показания зависят от конкретной реализации контроллера. Пока для контроллера SGGB снятие сигнала означает срабатывание: 1-сигнал есть (всё в порядке); 0 - сигнала нет (датчик сработал). </remarks>
		private readonly int sensors;
		/// <summary> События охранных функций </summary>
		public GuardEvents Events
		{
			get { return events; }
		}
		/// <summary> Номер ТС </summary>
		public int Unique
		{
			get { return unigue; }
		}
		public int Sensors
		{
			get { return sensors; }
		}
		/// <summary> События охранных функций </summary>
		/// <param name="events"> Флаги событий охранных функций </param>
		/// <param name="sensors"></param>
		/// <param name="unique"></param>
		public GuardEventArgs(GuardEvents events, int sensors, int unique)
		{
			this.events = events;
			this.sensors = sensors;
			this.unigue = unique;
		}
	}
	/// <summary> Аргумент события наличия принятых терминалом данных </summary>
	[Serializable]
	public class ReceiveEventArgs : EventArgs
	{
		/// <summary> Ссылка на информацию о мобильном объекте </summary>
		readonly IMobilUnit[] mobilUnits;
		/// <summary> Информация о мобильном объекте </summary>
		public IMobilUnit[] MobilUnits
		{
			get { return mobilUnits; }
		}
		/// <summary> Аргумент события наличия принятых терминалом данных </summary>
		/// <param name="mobilUnits"> Ссылка на информацию о мобильном объекте </param>
		public ReceiveEventArgs(params IMobilUnit[] mobilUnits)
		{
			this.mobilUnits = mobilUnits;
		}
	}
	/// <summary> Аргумент события необходимости отправить сигнал тревоги пользователю </summary>
	public class ReceiveAlarmArgs : EventArgs
	{
		private readonly IMobilUnit _mobilUnit;
		private readonly int[] _operatorIds;
		public IMobilUnit MobilUnit
		{
			get { return _mobilUnit; }
		}
		public int[] OperatorIds
		{
			get { return _operatorIds; }
		}
		public ReceiveAlarmArgs(IMobilUnit mobilUnit, int[] operatorIds)
		{
			_mobilUnit = mobilUnit;
			_operatorIds = operatorIds;
		}
	}
	/// <summary> Аргумент события наличия принятых терминалом данных </summary>
	[Serializable]
	public class PositionsEventArgs : EventArgs
	{
		/// <summary> Ссылка на информацию о мобильных объектах </summary>
		private readonly ArrayList lastPositions;
		/// <summary> Информация о мобильном объекте </summary>
		public ArrayList LastPositions
		{
			get { return lastPositions; }
		}
		public int Count
		{
			get { return lastPositions.Count; }
		}
		public IMobilUnit this[int unique]
		{
			get
			{
				IMobilUnit mu = null;
				foreach (IMobilUnit lp in lastPositions)
					if (lp.Unique == unique)
					{
						mu = lp;
						break;
					}
				return mu;
			}
		}
		/// <summary> Аргумент события наличия принятых клиентом данных о последних позициях ТС </summary>
		public PositionsEventArgs(ArrayList lastPositions)
		{
			this.lastPositions = lastPositions;
		}
		public PositionsEventArgs(IMobilUnit[] lastPositions)
		{
			this.lastPositions = new ArrayList(lastPositions.Length);
			this.lastPositions.AddRange(lastPositions);
		}
	}
	/// <summary> Аргумент события наличия принятых терминалом данных </summary>
	[Serializable]
	public class ReceiveLogEventArgs : EventArgs
	{
		/// <summary> Индекс машины, историю которой хотим получить </summary>
		readonly int unique;
		/// <summary> Гаражный номер </summary>
		readonly string garageNumber = String.Empty;
		/// <summary> Ссылка на информацию о мобильных объектах </summary>
		private readonly SortedList mobilUnits; // IMobilUnit
		private readonly int iLogLength;
		private readonly int iLogLengthNeeded;
		/// <summary> Информация о мобильных объектах </summary>
		public SortedList MobilUnits
		{
			get { return mobilUnits; }
		}
		public int Unique
		{
			get { return unique; }
		}
		/// <summary> Общее число имеющихся в базе значений истории за заданный период </summary>
		public int LogLength
		{
			get
			{
				return iLogLength;
			}
		}
		/// <summary> Число значений истории, запрашиваемое пользователем </summary>
		public int LogLengthNeeded
		{
			get
			{
				return iLogLengthNeeded;
			}
		}
		/// <summary> Гаражный номер  </summary>
		public string GarageNumber
		{
			get { return garageNumber; }
		}
		/// <summary> Аргумент события наличия принятых терминалом данных о истории движения </summary>
		/// <param name="mobilUnits"> Ссылка на информацию о мобильных объектах </param>
		/// <param name="unique"></param>
		/// <param name="iLogLength"></param>
		/// <param name="iLogLengthNeeded"></param>
		public ReceiveLogEventArgs(SortedList mobilUnits, int unique, int iLogLength, int iLogLengthNeeded)
		{
			this.unique           = unique;
			this.mobilUnits       = mobilUnits;
			this.iLogLength       = iLogLength;
			this.iLogLengthNeeded = iLogLengthNeeded;
		}
		public ReceiveLogEventArgs(SortedList mobilUnits, int unique, int iLogLength, int iLogLengthNeeded, string garageNumber) :
			this(mobilUnits, unique, iLogLength, iLogLengthNeeded)
		{
			this.garageNumber = garageNumber;
		}
	}
	/// <summary> object id enum, -1, 0 - special cases, > 0 - real object id </summary>
	public enum ObjectID : int
	{
		Unknown = -1,
		Server = 0,
	}

	/// <summary>
	/// notification importance, Lvl0 - highest
	/// </summary>
	public enum Severity : byte
	{
		/// <summary> only errs </summary>
		Lvl0,
		/// <summary> alarm </summary>
		Lvl1,
		Lvl2,
		Lvl3,
		/// <summary> normal </summary>
		Lvl4,
		Lvl5,
		Lvl6,
		Lvl7
	}
	/// <summary> category of notification </summary>
	public enum NotifyCategory : byte
	{
		General,
		Terminal,
		Server,
		Controller,
	}
	/// <summary> type of message to count </summary>
	public enum CountingMessageType
	{
		/// <summary> GPRS udp packet </summary>
		GPRS,
		/// <summary> GSM data link session </summary>
		GSM,
		/// <summary> sms message </summary>
		SMS
	}
	/// <summary> event argument of receiving a message needed to count </summary>
	[Serializable]
	public class ReceiveCountingEventArgs : EventArgs
	{
		private readonly CountingMessageType cmt;
		private readonly int iOperatorID;
		private readonly int iSenderID;
		private readonly string sSenderPhone;
		private readonly DateTime dtTimeBegin;
		private readonly int iDuration;
		private long lCount;
		/// <summary> type of a message </summary>
		public CountingMessageType MessageType
		{
			get
			{
				return cmt;
			}
		}
		/// <summary> operator id </summary>
		public int OperatorID
		{
			get
			{
				return iOperatorID;
			}
		}
		/// <summary> mobile unit id </summary>
		public int SenderID
		{
			get
			{
				return iSenderID;
			}
		}
		/// <summary> mobile unit phone number </summary>
		public string SenderPhone
		{
			get
			{
				return sSenderPhone;
			}
		}
		/// <summary> begin time of a message receiving </summary>
		public DateTime TimeBegin
		{
			get
			{
				return dtTimeBegin;
			}
		}
		/// <summary> duration of a message </summary>
		public int Duration
		{
			get
			{
				return iDuration;
			}
		}
		/// <summary> quantity of something that stored in object of this class </summary>
		public long Count
		{
			get
			{
				return lCount;
			}
			set
			{
				if (value > 0)
					lCount = value;
			}
		}
		public ReceiveCountingEventArgs(CountingMessageType cmt, int iOperatorID, int iSenderID, DateTime dtTimeBegin, int iDuration, long lCount)
		{
			this.cmt = cmt;
			this.iOperatorID = iOperatorID;
			this.iSenderID = iSenderID;
			this.sSenderPhone = "";
			this.dtTimeBegin = dtTimeBegin;
			this.iDuration = iDuration;
			this.lCount = (lCount > 0) ? lCount : 1;
		}
		public ReceiveCountingEventArgs(CountingMessageType cmt, int iOperatorID, string sSenderPhone, DateTime dtTimeBegin, int iDuration, long lCount)
		{
			this.cmt = cmt;
			this.iOperatorID = iOperatorID;
			this.iSenderID = 0;
			this.sSenderPhone = sSenderPhone;
			this.dtTimeBegin = dtTimeBegin;
			this.iDuration = iDuration;
			this.lCount = (lCount > 0) ? lCount : 1;
		}
	}
	/// <summary> event argument of sending messages, needed to count, to the server for saving their into db </summary>
	[Serializable]
	public class SaveCountingListEventArgs : EventArgs
	{
		private readonly Hashtable hList;
		/// <summary> list of messages </summary>
		public Hashtable List
		{
			get
			{
				return hList;
			}
		}
		public SaveCountingListEventArgs(Hashtable h)
		{
			this.hList = h;
		}
	}
}