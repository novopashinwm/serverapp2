﻿using System;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Common;

namespace FORIS.TSS.Terminal
{
	public interface IStdCommand : ICommand, IGuarded, ICloneable
	{
		/// <summary> info about sender </summary>
		IOperatorInfo Sender    { get; set; }
		/// <summary> Info about target obj </summary>
		IUnitInfo     Target    { get; set; }
		/// <summary> command type </summary>
		CmdType       Type      { get; set; }
		/// <summary> Тип терминала для отправки команды </summary>
		Media         MediaType { get; set; }
		/// <summary> named param list </summary>
		PARAMS Params           { get; set; }
	}
}