﻿namespace FORIS.TSS.Terminal
{
	public enum UnitInfoPurpose
	{
		Undefined,
		UnitPosition,
		MobilUnit,
		GSMTerminal,
		SMSTerminal,
		StdCommand,
		Firmware,
		Autorequest,
		SGGB,
		SANAV,
		STEPPII,
		TM140
	}
}