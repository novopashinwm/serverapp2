﻿using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.Terminal
{
	/// <summary> Информация о позиции мобильного объекта </summary>
	public interface IUnitPosition : IUTCTime
	{
		/// <summary> Географическая широта мобильного объекта (градусы) </summary>
		double       Latitude      { get; set; }
		/// <summary> Географическая долгота мобильного объекта (градусы) </summary>
		double       Longitude     { get; set; }
		/// <summary> Скорость мобильного объекта (км/ч) </summary>
		int?         Speed         { get; set; }
		/// <summary> Курс мобильного объекта (градусы) </summary>
		int?         Course        { get; set; }
		/// <summary> Высота (в сантиметрах) </summary>
		int?         Height        { get; set; }
		/// <summary> Радиус круга с центром <see cref="IUnitPosition"/>,
		/// внутри которого находится данное MLP-устройство</summary>
		int?         Radius        { get; set; }
		/// <summary>Тип позиции: GPS (по умолчанию), LBS, Yandex.Locator etc</summary>
		PositionType Type          { get; set; }
		/// <summary> unit that pos bound to </summary>
		IUnitInfo    UnitInfo      { get; }
		/// <summary> Признак валидной (правильной) позиции </summary>
		/// <remarks>"Правильная" позиция должна соответствовать возможным диапазонам долготы и широты.</remarks>
		bool         ValidPosition { get; }
	}
}