﻿using System;
using FORIS.TSS.Common;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Messaging
{
	class PositionMessage : IPositionMessage
	{
		IMobilUnit imuPos;
		bool bValid;
		
		#region IPositionMessage Members

		public IMobilUnit MobilUnit
		{
			get
			{
				return imuPos;
			}
			set
			{
				imuPos = value;
			}
		}

		public bool Valid
		{
			get
			{
				return bValid;
			}
			set
			{
				bValid = value;
			}
		}

		#endregion

		int iNum = -1;
		
		#region IMessage Members

		public int Num
		{
			get
			{
				return iNum;
			}
			set
			{
				iNum = value;
			}
		}

		public Direction Direction
		{
			get
			{
				return Direction.Incoming;
			}
			set
			{
			}
		}

		public MessageCategory Category
		{
			get
			{
				return MessageCategory.Position;
			}
			set
			{
			}
		}

		public MessageObject Sender
		{
			get
			{
				return new MessageObject(
					ObjectType.Controller, imuPos.UnitInfo, Direction.Outgoing);
			}
			set
			{
			}
		}

		public MessageObject Target
		{
			get
			{
				return new MessageObject(
					ObjectType.Server, ObjectID.Server, Direction.Incoming);
			}
			set
			{
			}
		}

		public DateTime Time
		{
			get
			{
				return (time_t)imuPos.Time;
			}
			set
			{
			}
		}

		public int Status
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}

		public Severity Severity
		{
			get
			{
				return Severity.Lvl2;
			}
			set
			{
			}
		}

		public bool Confirm
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		public bool Log
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		public PARAMS Params
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		public int? TermID { get; set; }

		#endregion
	}
}