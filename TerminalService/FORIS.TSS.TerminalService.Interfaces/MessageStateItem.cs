﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.TerminalService.Interfaces
{
	[Serializable]
	public class MessageStateItem
	{
		/// <summary> Идентификатор сообщения </summary>
		public int                     MessageId;
		/// <summary> Статус сообщения </summary>
		public MessageState            State;
		/// <summary> Результата обработки сообщения </summary>
		public MessageProcessingResult ProcessingResult;
	}
}