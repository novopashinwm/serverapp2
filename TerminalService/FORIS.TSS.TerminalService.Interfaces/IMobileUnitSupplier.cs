﻿using System;
using System.ComponentModel;
using FORIS.TSS.Terminal;

namespace FORIS.TSS.TerminalService.Interfaces
{
	public class MobileUnitCoupler :
		Component,
		IMobileUnitSupplier
	{
		#region Constructor & Dispose

		public MobileUnitCoupler(IContainer container)
			: this()
		{
			container.Add(this);
		}
		public MobileUnitCoupler()
		{
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				receive = null;
			}
			base.Dispose(disposing);
		}

		#endregion Constructor & Dispose

		#region Properties

		private IMobileUnitSupplier source;
		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public IMobileUnitSupplier Source
		{
			get { return source; }
			set
			{
				if (source != null)
				{
					source.Receive += source_Receive;
				}

				source = value;

				if (source != null)
				{
					source.Receive += source_Receive;
				}
			}
		}

		#endregion Properties

		#region Handle source events

		private void source_Receive(ReceiveEventArgs e)
		{
			OnReceive(e);
		}

		#endregion Handle source events

		#region IMobilUnitSupplier Members

		private ReceiveEventHandler receive;

		public event ReceiveEventHandler Receive
		{
			add    { receive -= value; receive += value; }
			remove { receive -= value; }
		}

		protected virtual void OnReceive(ReceiveEventArgs e)
		{
			if (receive == null)
				return;
			var handlers = receive.GetInvocationList();

			foreach (ReceiveEventHandler handler in handlers)
				handler.BeginInvoke(e, Receive_Callback, handler);
		}

		private void Receive_Callback(IAsyncResult asyncResult)
		{
			ReceiveEventHandler handler = (ReceiveEventHandler)asyncResult.AsyncState;

			try
			{
				handler.EndInvoke(asyncResult);
			}
			catch (Exception)
			{
				receive -= handler;
			}
		}

		private BulkNotifyEventHandler notify;

		public event BulkNotifyEventHandler Notify
		{
			add    { notify -= value; notify += value; }
			remove { notify -= value; }
		}

		protected virtual void OnNotify(NotifyEventArgs e)
		{
			if (notify == null)
				return;
			var handlers = notify.GetInvocationList();

			foreach (NotifyEventHandler handler in handlers)
				handler.BeginInvoke(e, notify_Callback, handler);
		}

		private void notify_Callback(IAsyncResult asyncResult)
		{
			var handler = (BulkNotifyEventHandler)asyncResult.AsyncState;

			try
			{
				handler.EndInvoke(asyncResult);
			}
			catch (Exception)
			{
				Notify -= handler;
			}
		}

		public void SendCommand(IStdCommand cmd)
		{
			source.SendCommand(cmd);
		}

		#endregion IMobilUnitSupplier Members
	}

	public interface IMobileUnitSupplier
	{
		event ReceiveEventHandler    Receive;
		event BulkNotifyEventHandler Notify;
		void SendCommand(IStdCommand cmd);
	}
}