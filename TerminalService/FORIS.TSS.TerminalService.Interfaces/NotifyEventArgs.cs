﻿using System;
using FORIS.TSS.BusinessLogic.Interfaces;

namespace FORIS.TSS.Terminal
{
	/// <summary> Аргумент события наличия сообщений </summary>
	[Serializable]
	public class NotifyEventArgs : EventArgs
	{
		public int? TermID;

		public readonly TerminalEventMessage Event;
		/// <summary> recipient </summary>
		IOperatorInfo op;
		public IOperatorInfo Operator
		{
			get
			{
				return op;
			}
			set
			{
				if (op == null || op.OperatorId == (int)ObjectID.Unknown) op = value;
			}
		}

		public readonly Severity       Lvl = Severity.Lvl4;
		public readonly NotifyCategory Cat = NotifyCategory.General;
		public readonly string         Message;
		public readonly Exception      Err;
		public readonly object[]       Tag;
		public readonly DateTime       Time = DateTime.UtcNow;

		#region .ctor
		/// <summary> Аргументы события наличия сообщений </summary>
		public NotifyEventArgs(IOperatorInfo op, NotifyCategory cat, Exception err)
			: this(TerminalEventMessage.NE_EXCEPTION, op, Severity.Lvl0, cat, err.Message, err)
		{
		}
		public NotifyEventArgs(TerminalEventMessage evt, IOperatorInfo op, Severity lvl, NotifyCategory cat, string message, Exception err, params object[] tag)
		{
			Event    = evt;
			Operator = op;
			Lvl      = lvl;
			Cat      = cat;
			Message  = message;
			Err      = err;
			Tag      = tag;
		}
		public NotifyEventArgs(DateTime dt, TerminalEventMessage evt, IOperatorInfo op, Severity lvl, NotifyCategory cat, string message, Exception err, params object[] tag)
			: this(evt, op, lvl, cat, message, err, tag)
		{
			Time = dt;
		}
		public NotifyEventArgs(TerminalEventMessage @event, params object[] tag)
		{
			Time  = DateTime.UtcNow;
			Event = @event;
			Tag   = tag;
		}

		#endregion .ctor
	}
}