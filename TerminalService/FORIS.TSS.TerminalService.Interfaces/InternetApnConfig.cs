﻿using System;

namespace FORIS.TSS.TerminalService.Interfaces
{
	/// <summary> Параметры для подключения к интернету </summary>
	[Serializable]
	public class InternetApnConfig
	{
		public string Name;
		public string User;
		public string Pass;
	}
}