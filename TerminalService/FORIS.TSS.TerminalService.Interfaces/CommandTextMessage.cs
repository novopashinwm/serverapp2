using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.Terminal.Messaging
{
    class CommandTextMessage : CommandMessage, ITextMessage
    {
        public CommandTextMessage(ICommand cmd) : base(cmd)
        {
        }
		
        public override MessageCategory Category
        {
            get
            {
                return MessageCategory.Command | MessageCategory.Text;
            }
            set
            {

            }
        }

        #region ITextMessage Members

        public string Code
        {
            get
            {
                return null;
            }
            set
            {
            }
        }

        public virtual string Text
        {
            get
            {
                return cmd.Params["Text"] as string;
            }
            set
            {
                cmd.Params["Text"] = value;
            }
        }

        public MessageState State
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }
		
        #endregion
    }
}