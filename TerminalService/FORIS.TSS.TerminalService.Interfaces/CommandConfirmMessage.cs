﻿namespace FORIS.TSS.Terminal.Messaging
{
	class CommandConfirmMessage : CommandTextMessage, IConfirmMessage
	{
		int state;

		public CommandConfirmMessage(ICommand cmd) : base(cmd)
		{
		}

		public override MessageCategory Category
		{
			get
			{
				return MessageCategory.Command | MessageCategory.Confirm;
			}
			set
			{
			}
		}

		#region IConfirmMessage Members

		public int ConfirmNo
		{
			get
			{
				return cmd.Params["ConfirmNo"] is int ? (int)cmd.Params["ConfirmNo"] : 0;
			}
			set
			{
				cmd.Params["ConfirmNo"] = value;
			}
		}

		#endregion IConfirmMessage Members

		int IConfirmMessage.State
		{
			get { return state; }
			set { state = value; }
		}
	}
}