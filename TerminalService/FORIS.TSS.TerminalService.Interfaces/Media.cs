﻿using System;

namespace FORIS.TSS.Terminal
{
	[Serializable]
	public enum Media : int
	{
		Unspecified          = 0,
		GPRS                 = 1,
		GSM                  = 2,
		SMS                  = 3,
		LBS                  = 4,
		RemoteTerminalServer = 5,
		RemoteVideoServer    = 6,
		Web                  = 7,
		TCP                  = 8,
		Emulator             = 9,
		Movireg              = 10,
	}
}