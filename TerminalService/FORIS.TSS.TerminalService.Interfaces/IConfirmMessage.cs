namespace FORIS.TSS.Terminal.Messaging
{
    public interface IConfirmMessage : IMessage
    {
        int ConfirmNo{get; set;}
        int State{get; set;}
    }
}