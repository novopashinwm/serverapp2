namespace FORIS.TSS.Terminal.Messaging
{
    class ConfirmMessage : TextMessage, IConfirmMessage
    {
        public override MessageCategory Category
        {
            get
            {
                return MessageCategory.Confirm;
            }
            set
            {
            }
        }
		
        int iConfirmNo;
        int state;

        public int ConfirmNo
        {
            get
            {
                return iConfirmNo;
            }
            set
            {
                iConfirmNo = value;
            }
        }

        int IConfirmMessage.State
        {
            get {return state;}
            set {state = value;}
        }
    }
}