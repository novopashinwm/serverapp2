using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal
{
	/// <summary> notification event messages </summary>
	/// <todo>object[] in Tag -> PARAMS</todo>
	public enum TerminalEventMessage
	{
		/// <summary> general exception </summary>
		NE_EXCEPTION = 0,
		/// <summary>
		/// terminal exception
		/// Tag[0] - LinkStatus value, Tag[1] - unit
		/// </summary>
		TM_EXCEPTION = 1,
		/// <summary>
		/// LinkStatus change, unitInfo is mandatory. 
		/// Tag[0] - LinkStatus value, Tag[1] - unit, Tag[2] - current operator, 
		/// Tag[3] - CmdType
		/// </summary>
		TM_LINKSTATUS = 2,
		/// <summary>
		/// alarm. Tag[0] - unit; Tag[1] - original message; Tag[2] - 1 - on, 0 - off; 
		/// [Tag[3] - message no. (-1 not specified)]
		/// </summary>
		NE_ALARM = 3,
		/// <summary>
		/// message. Tag[0] - unit; Tag[1] - code (-1 not specified); 
		/// Tag[2] - original message; [Tag[3] - message no. (-1 not specified)]
		/// </summary>
		NE_MESSAGE = 4,
		/// <summary>
		/// confirm message. Tag[0] - unit; Tag[1] - original message; 
		/// Tag[2] - number of message to confirm; 
		/// Tag[3] - confirm state, source message dependent;
		/// Tag[4] - message no. (-1 not specified); Tag[5] - CmdType to confirm
		/// </summary>
		NE_CONFIRM = 5,
		TM_MESSAGE = 6,
		/// <summary> ������������� ��������� ������ � ��������� </summary>
		NE_CONFIRM_FIRMWARE = 7,
		/// <summary> ��������� �������� ���������� </summary>
		NE_SETTINGS,
		/// <summary> ������ ������ ������ - "��������" </summary>
		NE_FREE,
		/// <summary> ������ ������ ������ - "�� �����" </summary>
		NE_TO_ORDER,
		/// <summary> ������ ������ ������ - "�� ������" </summary>
		NE_ORDER,
		/// <summary>���������� ������� ���������</summary>
		/// <remarks>Tag �������� <see cref="CommandResult"/></remarks>
		CommandCompleted,
		/// <summary>����������� � ������</summary>
		/// <remarks>
		/// �������������� ���������: 
		/// 1. ������������� ���������� (byte[])
		/// 2. ����� ������ (�����������)
		/// 3. ����� �����������  (�����������)
		/// 4. ������ �����������  (�����������)
		/// 5. ��������� ���� (�����������)
		/// </remarks>
		Picture,
		/// <summary>��� ����������</summary>
		AppLog,
		/// <summary>������� ������ ��� ��������� ����������� </summary>
		VideoRecordReplayUrlCreated,
		/// <summary> SMS </summary>
		SMS,
		/// <summary> ��������� ������ �� ������� ����������, � �������� ��� ������ </summary>
		VehicleIsUnpaid
	}
}