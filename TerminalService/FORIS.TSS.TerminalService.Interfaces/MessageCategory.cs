﻿using System;

namespace FORIS.TSS.Terminal.Messaging
{
	/// <summary> message category list </summary>
	[Flags]
	public enum MessageCategory
	{
		Command       =   1,
		Information   =   2,
		Position      =   4,
		Service       =   8,
		Guard         =  16,
		Communication =  32,
		Text          =  64,
		Alarm         = 128,
		Confirm       = 256
	}
}