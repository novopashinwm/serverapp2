﻿namespace FORIS.TSS.Terminal.Messaging
{
	/// <summary> direction used in messaging regarding to application server </summary>
	public enum Direction
	{
		Unknown  = 0,
		Internal = 1,
		Incoming = 2,
		Outgoing = 4
	}
}