﻿using System;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;

namespace FORIS.TSS.Terminal.Messaging
{
	[Serializable]
	class TextMessage : ITextMessage
	{
		string iCode;
		string sText;
		MessageState enState;

		#region ITextMessage Members

		public string Code
		{
			get
			{
				return iCode;
			}
			set
			{
				iCode = value;
			}
		}

		public string Text
		{
			get
			{
				return sText;
			}
			set
			{
				sText = value != null? value.Trim(): "";
			}
		}

		public MessageState State
		{
			get
			{
				return enState;
			}
			set
			{
				enState = value;
			}
		}
		
		#endregion

		int iNum = -1;
		Direction enDir = Direction.Unknown;
		MessageObject moSender, moTarget;
		DateTime dtTime = DateTime.UtcNow;
		Severity enSeverity = Severity.Lvl4;
		bool bLog, bConfirm;
		PARAMS par;

		#region IMessage Members

		public int Num
		{
			get
			{
				return iNum;
			}
			set
			{
				iNum = value;
			}
		}

		public Direction Direction
		{
			get
			{
				return enDir;
			}
			set
			{
				enDir = value;
			}
		}

		public virtual MessageCategory Category
		{
			get
			{
				return MessageCategory.Text;
			}
			set
			{
			}
		}

		public MessageObject Sender
		{
			get
			{
				return moSender;
			}
			set
			{
				moSender = value;
			}
		}

		public MessageObject Target
		{
			get
			{
				return moTarget;
			}
			set
			{
				moTarget = value;
			}
		}

		public DateTime Time
		{
			get
			{
				return dtTime;
			}
			set
			{
				dtTime = value;
			}
		}

		public int Status
		{
			get
			{

				return 0;
			}
			set
			{

			}
		}

		public Severity Severity
		{
			get
			{
				return enSeverity;
			}
			set
			{
				enSeverity = value;
			}
		}

		public bool Confirm
		{
			get
			{
				return bConfirm;
			}
			set
			{
				bConfirm = value;
			}
		}

		public bool Log
		{
			get
			{
				return bLog;
			}
			set
			{
				bLog = value;
			}
		}

		public PARAMS Params
		{
			get
			{
				return par;
			}
			set
			{
				par = value;
			}
		}

		public int? TermID { get; set; }

		#endregion
	}
}