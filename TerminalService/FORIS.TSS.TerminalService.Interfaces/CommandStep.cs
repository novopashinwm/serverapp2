﻿using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.TerminalService.Interfaces
{
	/// <summary> Шаг команды </summary>
	public abstract class CommandStep
	{
		/// <summary> Ожидаемый тип результата выполнения шага команды </summary>
		public CmdType? WaitAnswer = CmdType.Unspecified;
	}

	/// <summary> Шаг команды - отправка SMS</summary>
	public class SmsCommandStep : CommandStep
	{
		public string Text;
		public string ReplyTo;
		public SmsCommandStep(string text)
		{
			Text = text;
		}
		public static implicit operator SmsCommandStep(string text)
		{
			return new SmsCommandStep(text);
		}
	}
	/// <summary> Шаг команды - отправка данных по TCP/IP </summary>
	public class TcpCommandStep : CommandStep
	{
	}
}