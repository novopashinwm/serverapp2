﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.Interfaces;

namespace FORIS.TSS.Terminal.Messaging
{
	/// <summary>
	/// object used in message
	/// </summary>
	[Serializable]
	public class MessageObject
	{
		public ObjectType Type;
		public object Object;
		public Direction Direction;
		
		public MessageObject(ObjectType type, object obj, Direction dir)
		{
			Type = type;
			Object = obj;
			Direction = dir;
		}

		public static ObjectType GetSenderType(int type)
		{
			if ((type & 0x200) != 0)
				return ObjectType.Controller;
			if ((type & 0x400) != 0)
				return ObjectType.Operator;
			throw new ArgumentOutOfRangeException("type", type, @"Unable to determine sender");
		}

		public static ObjectType GetReceiverType(int type)
		{
			if ((type & 0x0100) != 0)
				return ObjectType.Controller;
			if ((type & 0x0800) != 0)
				return ObjectType.Operator;
			if ((type & 0x2000) != 0)
				return ObjectType.Asid;
			throw new ArgumentOutOfRangeException("type", type, @"Unable to determine receiver");
		}

		public static IEnumerable<MessageObject> GetMessageObjects(int? type, int? sourceID, int? destinationID)
		{
			if (type == null)
				yield break;
			if (sourceID != null)
				yield return new MessageObject(GetSenderType(type.Value), sourceID, Direction.Outgoing);
			if (destinationID != null)
				yield return new MessageObject(GetReceiverType(type.Value), destinationID, Direction.Incoming);
		}

		public int ID
		{
			get
			{
				var operatorInfo = Object as IOperatorInfo;
				if (operatorInfo != null)
					return operatorInfo.OperatorId;
				var @int = Object as int?;
				if (@int != null)
					return @int.Value;

				throw new InvalidOperationException("Not supported type of Object: " + (Object != null ? Object.GetType().ToString() : "NULL"));
			}
		}
		
		public int? Mask
		{
			get
			{
				switch (Direction)
				{
					case Direction.Incoming:
						switch (Type)
						{
							case ObjectType.Controller:
								return 0x100;
							case ObjectType.Operator:
								return 0x800;
							case ObjectType.Asid:
								return 0x2000;
							default:
								return null;
						}
					case Direction.Outgoing:
						switch (Type)
						{
							case ObjectType.Controller:
								return 0x200;
							case ObjectType.Operator:
								return 0x400;
							default:
								return null;
						}
					default:
						return null;
				}
			}
		}
	}
}