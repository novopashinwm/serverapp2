﻿using System;

namespace FORIS.TSS.CommonTypes
{
	/// <summary> События охранных функций </summary>
	[Flags]
	public enum GuardEvents
	{
		/// <summary> событие отсутствует </summary>
		None       = 0,
		/// <summary> кнопка SOS </summary>
		SOS        = 0x1,
		/// <summary> срабатывание датчика капота </summary>
		Cowling    = 0x2,
		/// <summary> срабатывание датчика дверей </summary>
		Doors      = 0x4,
		/// <summary> постановка на охрану </summary>
		Guard      = 0x8,
		/// <summary> сработала сигнализация </summary>
		Signalling = 0x10
	}
	#region DPoint
	[Serializable]
	public struct DPoint
	{
		public double x;
		public double y;

		public DPoint(double x, double y)
		{
			this.x = x;
			this.y = y;
		}
	}
	#endregion DPoint
	#region FPoint
	[Serializable]
	public struct FPoint
	{
		public float x;
		public float y;

		public FPoint(float x, float y)
		{
			this.x = x;
			this.y = y;
		}
	}
	#endregion FPoint
	#region IPoint
	[Serializable]
	public struct IPoint
	{
		public int x;
		public int y;

		public IPoint(int x, int y)
		{
			this.x = x;
			this.y = y;
		}
	}
	#endregion IPoint
	#region DRect
	[Serializable]
	public struct DRect
	{
		public double x1;
		public double x2;
		public double y1;
		public double y2;

		public DRect(double x1, double x2, double y1, double y2)
		{
			this.x1 = x1;
			this.x2 = x2;
			this.y1 = y1;
			this.y2 = y2;
		}

		public DPoint Center()
		{
			DPoint local0;

			local0 = new DPoint(x1 + x2 / 2, y1 + y2 / 2);
			return local0;
		}

		public double Height()
		{
			double local0;

			local0 = y2 - y1;
			return local0;
		}

		public double Width()
		{
			double local0;

			local0 = x2 - x1;
			return local0;
		}
	}
	#endregion DRect
	#region IRect
	[Serializable]
	public struct IRect
	{
		public int x1;
		public int x2;
		public int y1;
		public int y2;

		public IRect(int x1, int x2, int y1, int y2)
		{
			this.x1 = x1;
			this.x2 = x2;
			this.y1 = y1;
			this.y2 = y2;
		}

		public IPoint Center()
		{
			IPoint local0;

			local0 = new IPoint(x1 + x2 / 2, y1 + y2 / 2);
			return local0;
		}

		public int Height()
		{
			int local0;

			local0 = y2 - y1;
			return local0;
		}

		public int Width()
		{
			int local0;

			local0 = x2 - x1;
			return local0;
		}
	}
	#endregion IRect
	#region FRect
	[Serializable]
	public struct FRect
	{
		public float x1;
		public float x2;
		public float y1;
		public float y2;

		public FRect(float x1, float x2, float y1, float y2)
		{
			this.x1 = x1;
			this.x2 = x2;
			this.y1 = y1;
			this.y2 = y2;
		}

		public FPoint Center()
		{
			FPoint local0;

			local0 = new FPoint(x1 + x2 / 2, y1 + y2 / 2);
			return local0;
		}

		public float Height()
		{
			float local0;

			local0 = y2 - y1;
			return local0;
		}

		public float Width()
		{
			float local0;

			local0 = x2 - x1;
			return local0;
		}
	}
	#endregion IRect
}