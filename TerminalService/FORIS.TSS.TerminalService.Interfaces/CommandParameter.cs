﻿namespace FORIS.TSS.TerminalService.Interfaces
{
    public static class CommandParameter
    {
        public static class SendResultOver
        {
            public const string Key = "Send-Result-Over";
   
            public static class Values
            {
                public const string Sms = "SMS";
            }
        }

        public const string CommandID = "CommandID";

        public const string MaximumLocationAge = "MaximumLocationAge";

        /// <summary> Запрос по расписанию </summary>
        public const string ScheduledRequest = "ScheduledRequest";

        /// <summary> Был ли отправлен видимый запрос с просьбой активировать приложения для завершения поиска пользователя </summary>
        public static string ActivateAppRequest = "ActivateAppRequest";

        /// <summary> Маскированный номер отправителя запроса в LBS-Interworking </summary>
        public const string SenderAsid = "SenderAsid";

        /// <summary>
        /// Идентификатор запроса интерворкинга.
        /// </summary>
        public const string InterworkingUid = "Uid";
    }
}