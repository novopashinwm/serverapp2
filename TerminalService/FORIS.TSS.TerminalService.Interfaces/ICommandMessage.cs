namespace FORIS.TSS.Terminal.Messaging
{
    public interface ICommandMessage : IMessage
    {
        ICommand Command{get; set;}
    }
}