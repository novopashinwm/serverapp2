namespace FORIS.TSS.Terminal
{
    public struct UnitID
    {
        private int value;

        public UnitID( int value )
        {
            this.value = value;
        }

        public override bool Equals( object obj )
        {
            return this.value.Equals( ((UnitID)obj).value );
        }
        public override int GetHashCode()
        {
            return this.value.GetHashCode();
        }

        public static explicit operator UnitID( int value )
        {
            return new UnitID( value );
        }
    }
}