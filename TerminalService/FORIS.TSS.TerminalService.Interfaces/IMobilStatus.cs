﻿using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.MobilUnit
{
	public interface IMobilStatus
	{
		/// <summary> Статус мобильного объекта </summary>
		Status Status { get; set; }
	}
}