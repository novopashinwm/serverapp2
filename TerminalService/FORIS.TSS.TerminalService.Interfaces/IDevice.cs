﻿using System;
using System.Collections;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Communication;
using FORIS.TSS.TerminalService.Interfaces;

// ReSharper disable once CheckNamespace
namespace FORIS.TSS.Terminal
{
	/// <summary> controller type dependent information </summary>
	/// <remarks>
	/// encapsulates most if not all device specific info.
	/// work only main forms: CmdType (IStdCommand), IMobilUnit, Datagram, byte (bytes[])
	/// </remarks>
	public interface IDevice
	{
		/// <summary> init device with working mode and link type </summary>
		/// <param name="link"> link type </param>
		/// <param name="mode"> work mode </param>
		/// <remarks>almost all functionality depend on types
		/// </remarks>
		void Init(CmdType link, CmdType mode);
		/// <summary> init device with command type. set device working mode </summary>
		/// <param name="link"> link type </param>
		/// <param name="cmd"> command </param>
		/// <remarks>
		/// Initialized, InputDatagram, OutputDatagram, InitSeq, UninitSeq depend on mode
		/// </remarks>
		void Init(CmdType link, IStdCommand cmd);
		/// <summary> successfully initialized </summary>
		bool Initialized { get; }
		/// <summary> input datagram </summary>
		Datagram InputDatagram { get; }
		/// <summary> output datagram filled with data </summary>
		/// <param name="data"> sequence of bytes that contains byte form of datagram </param>
		/// <param name="begin"> start pos of datagram in byte sequence </param>
		/// <returns> out datagram </returns>
		Datagram OutputDatagram(byte[] data, out int begin);
		/// <summary> current mode </summary>
		/// <remarks>can be common (eg Trace) or communication protocol specific (eg GSM) </remarks>
		CmdType Mode { get; set; }
		/// <summary> byte sequence used to init device in current DataType </summary>
		byte[] InitSeq { get; }
		/// <summary> byte sequence used to uninit device in current DataType </summary>
		byte[] UninitSeq { get; }
		/// <summary>/// action after opening link
		/// </summary>
		/// <param name="lnk">link to read/write</param>
		void OnLinkOpen(ILink lnk);
		/// <summary> data received from controller </summary>
		/// <param name="data"> data block </param>
		/// <param name="count"> length </param>
		/// <param name="stateData"> receiver state object for Device usage </param>
		/// <param name="bufferRest"> data that should stay in buffer for usage with next received data </param>
		/// <returns> list of IMobilUnits </returns>
		IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest);
		void OnNoData();
		void OnError();
		/// <summary> link is closed </summary>
		void OnLinkClose();
		/// <summary> check if device support bytes as datagram </summary>
		/// <param name="data"> byte sequence </param>
		/// <returns> success </returns>
		bool SupportData(byte[] data);
		/// <summary> check if known datagram </summary>
		/// <param name="dg"> datagram </param>
		/// <returns> success </returns>
		bool SupportData(Datagram dg);
		/// <summary> terminal and device manager </summary>
		ITerminalManager Manager { get; set; }
		/// <summary> Создание подходящего терминала </summary>
		/// <param name="type"> Тип команды </param>
		/// <returns></returns>
		IEnumerable<ITerminal> CreateTerminal(CmdType type);
		/// <summary> Дополнительная настройка устройства при первом подключении </summary>
		byte[] AdjustSettings();
		ITerminal CreateTerminal(Media type);
		Type SensorEnumType { get; }
		IList<string> DeviceTypeNames { get; }
		/// <summary> Разобрать СМС полученное с номера устройства (ui заполняется по номеру телефона устройства) </summary>
		IList<object> ParseSms(IUnitInfo ui, string text);
		/// <summary> Получить набор шагов для выполнения команды </summary>
		List<CommandStep> GetCommandSteps(IStdCommand command);
		byte[] GetCmd(IStdCommand command);
		/// <summary> Поддерживает ли устройство отправку и приём SMS через короткий номер </summary>
		bool SupportsShortNumber { get; }
		/// <summary> Время отсутствия активности на сетевом интерфейса, после превышения которого связь разрывается сервером </summary>
		TimeSpan? MaxIdleTime { get; }
	}
}