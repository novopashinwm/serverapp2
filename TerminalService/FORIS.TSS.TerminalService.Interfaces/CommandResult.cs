﻿using System;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Terminal;

namespace FORIS.TSS.TerminalService.Interfaces
{
	/// <summary> Интерфейс кортежа данных результата обработки команды <see cref="IStdCommand"/> </summary>
	[Serializable]
	public class CommandResult
	{
		/// <summary>Тип обработанной команды</summary>
		public CmdType CmdType;

		/// <summary> Значение результата выполнения команды </summary>
		public object Value;

		/// <summary>Идентификатор команды в таблице dbo.Command</summary>
		public int? CommandID;

		/// <summary>Результат выполнения команды</summary>
		public CmdResult CmdResult;

		/// <summary> Дополнительная информация о результате команды, зависящая от контекста </summary>
		public IUnitInfo Tag;

		public override string ToString()
		{
			return string.Format("{0}: {1} - {2}", Tag, CmdType, CmdResult);
		}
	}
}