﻿namespace FORIS.TSS.TerminalService.Interfaces
{
	/// <summary> Устройство для захвата изображения </summary>
	public interface IPictureDevice
	{
		/// <summary> Номер камеры </summary>
		int    CameraNumber { get; }
		/// <summary> Номер фотографии </summary>
		int    PhotoNumber  { get; }
		/// <summary> Байты фотографии </summary>
		byte[] Bytes        { get; }
		/// <summary> URL фотографии на внешнем ресурсе </summary>
		string Url          { get; }
	}
}