﻿using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal
{
	/// <summary> Информация о контролере мобильного объекта </summary>
	public interface IUnitData : IUTCTime, IUnitInfo
	{
		/// <summary> input power voltage </summary>
		int PowerVoltage { get; set; }
		/// <summary> voltage on analog input AN1 </summary>
		int VoltageAN1 { get; set; }
		/// <summary> voltage on analog input AN2 </summary>
		int VoltageAN2 { get; set; }
		/// <summary> voltage on analog input AN3 </summary>
		int VoltageAN3 { get; set; }
		/// <summary> voltage on analog input AN4  </summary>
		int VoltageAN4 { get; set; }
		/// <summary> firmware version </summary>
		string Firmware { get; set; }
		/// <summary> Пробег </summary>
		int Run { get; set; }
		/// <summary> Данные со входов терминала </summary>
		Dictionary<int, long> SensorValues { get; set; }
		decimal? GetSensorValue(SensorLegend legend);
		/// <summary> Информация о сотовых сетях, которые видит устройство </summary>
		CellNetworkRecord[] CellNetworks { get; set; }
		/// <summary> Информация о сетях WLAN, которые видит устройство </summary>
		WlanRecord[] Wlans { get; set; }
		/// <summary> Сырые данные, на основании которых получен данный пакет; может быть не задано </summary>
		byte[] RawBytes { get; set; }
		/// <summary> Свойства трекера, полученные от него по любому каналу связи </summary>
		Dictionary<DeviceProperty, string> Properties { get; }
	}
}