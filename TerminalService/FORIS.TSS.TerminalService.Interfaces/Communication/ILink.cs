﻿namespace FORIS.TSS.Communication
{
	/// <summary> interface to represent read/write object </summary>
	public interface ILink
	{
		/// <summary> read sequence of bytes from link </summary>
		/// <param name="data"> buf for data </param>
		/// <returns> number of bytes read </returns>
		int Read(byte[] data);
		/// <summary> read sequence of symbols from link </summary>
		/// <param name="data"> buf for data </param>
		/// <returns> number of bytes read </returns>
		int Read(char[] data);
		/// <summary> write sequence of bytes to link </summary>
		/// <param name="data"> data to be send </param>
		void Write(byte[] data);
		/// <summary> write string to link </summary>
		/// <param name="data"> data to be send </param>
		void Write(string data);
		/// <summary> state of link </summary>
		bool Valid { get; }
		/// <summary> data from link </summary>
		event DataEventHandler Data;
	}
}