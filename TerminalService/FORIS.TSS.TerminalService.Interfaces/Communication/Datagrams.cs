﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace FORIS.TSS.Communication
{
	/// <summary> Базовый класс для всех датаграм </summary>
	public abstract class Datagram
	{
		/// <summary> максимальный размер датаграммы </summary>
		protected const uint g_MaxDatagramLength = UInt16.MaxValue;
		/// <summary> максимальный размер датаграммы </summary>
		public static uint MaxDatagramLength
		{
			get { return g_MaxDatagramLength; }
		}
		/// <summary> массив данных датаграммы </summary>
		protected byte[] bytes;

		#region **************************** statics *******************************
		/// <summary> целое число в массива байт </summary>
		/// <param name="n"> целое число </param>
		/// <param name="bytes"> массив байт </param>
		/// <param name="begin"> индекс начала </param>
		/// <param name="count"> количество байт </param>
		public static void IntToBytes(int n, byte[] bytes, int begin, int count)
		{
			int end = begin + count;
			bytes[begin] = (byte)(n & 0x00FF);
			for (int i = begin + 1; i < end; i++)
				bytes[i] = (byte)(n >>= 8);
		}

		public static void IntToBytesBE(int n, byte[] bytes, int begin, int count)
		{
			Debug.Assert(4 >= count);
			for (int i = begin + count - 1, end = begin; i >= end; --i, n >>= 8)
				bytes[i] = (byte)n;
		}

		/// <summary> слово в массива байт </summary>
		/// <param name="n"> слово </param>
		/// <param name="bytes"> массив байт </param>
		/// <param name="begin"> индекс начала </param>
		/// <param name="count"> количество байт </param>
		public static void ShortToBytes(short n, byte[] bytes, int begin, int count)
		{
			int end = begin + count;
			bytes[begin] = (byte)(n & 0x00FF);
			for (int i = begin + 1; i < end; i++)
				bytes[i] = (byte)(n >>= 8);
		}

		/// <summary> целое число из массива байт. little endian </summary>
		/// <param name="bytes"> массива байт </param>
		/// <returns> целое число </returns>
		public static int IntFromBytesLE(params byte[] bytes)
		{
			if (bytes.Length == 4 && BitConverter.IsLittleEndian)
				return BitConverter.ToInt32(bytes, 0);
			int n = 0;
			for (var i = 0; i != bytes.Length; ++i)
			{
				n <<= 8;
				n += bytes[bytes.Length - i - 1];
			}
			return n;
		}

		/// <summary> целое число из массива байт. big endian </summary>
		/// <param name="bytes"> массив байт </param>
		/// <returns> целое число </returns>
		public static int IntFromBytesBE(params byte[] bytes)
		{
			int L = 0;
			int n = bytes[0];
			while (++L < bytes.Length)
			{
				n <<= 8;
				n += bytes[L];
			}
			return n;
		}

		protected static long LongFromBytesLE(params byte[] bytes)
		{
			if (bytes.Length == 8 && BitConverter.IsLittleEndian)
				return BitConverter.ToInt64(bytes, 0);

			long n = 0;
			for (var i = 0; i != bytes.Length; ++i)
			{
				n <<= 8;
				n += bytes[bytes.Length - i - 1];
			}
			return n;
		}

		protected static long LongFromBytesBE(params byte[] bytes)
		{
			int i = 0;
			long n = bytes[0];
			while (++i < bytes.Length)
			{
				n <<= 8;
				n += bytes[i];
			}
			return n;
		}

		/// <summary> int from byte array </summary>
		/// <param name="bytes"> bytes </param>
		/// <param name="pos"> pos of begin </param>
		/// <param name="len"> number of bytes </param>
		/// <returns>int val</returns>
		public static int IntFromBytesBE(byte[] bytes, int pos, int len)
		{
			Debug.Assert(len < 5, "Are You Fucking Nuts?!!! It Does Not Fit System.Int32!!!");
			int res = 0;
			for (int i = pos, end = pos + len; i < end; ++i) res = (res << 8) + bytes[i];
			return res;
		}

		/// <summary> вещественное число из массива байт </summary>
		/// <param name="bytes"> массива байт </param>
		/// <returns> вещественное число - прямой порядок байт </returns>
		public static float FloatFromBytesBE(params byte[] bytes)
		{
			int i = IntFromBytesBE(bytes);
			return BitConverter.ToSingle(BitConverter.GetBytes(i), 0);
		}

		/// <summary> float from byte array </summary>
		/// <param name="bytes"> bytes </param>
		/// <param name="pos"> pos of begin </param>
		/// <param name="len"> number of bytes </param>
		/// <returns> float val </returns>
		public static float FloatFromBytesBE(byte[] bytes, int pos, int len)
		{
			int i = IntFromBytesBE(bytes, pos, len);
			var bytes4 = BitConverter.GetBytes(i);
			return BitConverter.ToSingle(bytes4, 0);
		}

		/// <summary> Делегат получение размера пакета </summary>
		/// <param name="data"></param>
		/// <param name="startIndex"></param>
		/// <returns></returns>
		public delegate int PacketLengthGetter(byte[] data, int startIndex);

		public static List<byte[]> SplitDataPackets(byte[] data, PacketLengthGetter packetLengthGetter, out byte[] bufferRest)
		{
			bufferRest = null;

			//Разбиваем данные на пакеты
			//получаем длину первого пакета
			var packetLength = packetLengthGetter(data, 0);

			if (packetLength < 0 || data.Length < packetLength)
			{
				bufferRest = data;
				return null;
			}

			var packets = new List<byte[]>();

			if (data.Length == packetLength)
			{
				packets.Add(data);
			}
			else
			{
				//все таки разбиваем
				var startIndex = 0;

				while (startIndex < data.Length - 1)
				{
					var currPacketLength = packetLengthGetter(data, startIndex);

					if (currPacketLength <= 0 || data.Length < startIndex + currPacketLength)
					{
						currPacketLength = data.Length - startIndex;
						var packetB = new byte[currPacketLength];
						Array.Copy(data, startIndex, packetB, 0, currPacketLength);
						bufferRest = packetB;
						break;
					}

					var packet = new byte[currPacketLength];
					Array.Copy(data, startIndex, packet, 0, currPacketLength);

					packets.Add(packet);

					startIndex += currPacketLength;
				}
			}

			return packets;
		}

		#endregion **************************** statics *******************************

		#region **************************** Атрибуты *******************************
		/// <summary> Размер датаграммы </summary>
		public virtual int Size
		{
			get { return bytes != null ? bytes.Length : 0; }
		}

		public virtual ushort CRC16
		{
			get { return 0; }
			set { }
		}

		#endregion **************************** Атрибуты *******************************

		#region .ctor

		/// <summary> Датаграмма </summary>
		public Datagram()
		{
		}
		/// <summary> Датаграмма </summary>
		/// <param name="data"> массив байт </param>
		public Datagram(byte[] data, int pos, int count) : this((uint)count)
		{
			Array.Copy(data, pos, bytes, 0, count);
		}
		/// <summary> Датаграмма </summary>
		/// <param name="capacity"> размер датаграммы </param>
		public Datagram(uint capacity)
		{
			if(capacity > g_MaxDatagramLength)
				bytes = new byte[g_MaxDatagramLength];
			else
				bytes = new byte[capacity];
		}

		#endregion .ctor

		public virtual byte[] GetBytes()
		{
			return bytes ?? (new byte[0]);
		}

		#region CRC routines

		/// <summary> Проверка контрольной суммы </summary>
		/// <returns>ну а как ты думаешь</returns>
		public virtual bool CheckCRC()
		{
			return CRC == CalcCRC();
		}

		/// <summary> CRC specified in datagram </summary>
		protected virtual int CRC
		{
			get
			{
				return -1;
			}
			set
			{
			}
		}
		/// <summary> recalculated CRC </summary>
		/// <returns></returns>
		protected virtual int CalcCRC()
		{
			return -1;
		}
		/// <summary> calculate and set CRC </summary>
		protected virtual int BuildCRC()
		{
			return CRC = CalcCRC();
		}
		/// <summary> CRC calculation by ZMODEM standard </summary>
		/// <param name="begin"> начало массива </param>
		/// <param name="end"> конец массива </param>
		protected ushort CRC_16_12_5_0(int begin, int end)
		{
			ushort crc = 0;
			for (int i = begin; i <= end; i++)
			{
				byte src = bytes[i];
				ushort a = (ushort)(src ^ (crc >> 8));
				crc <<= 8;
				crc ^= a;
				crc ^= (ushort)(a << 12);
				crc ^= (ushort)(a << 5);
				crc ^= a >>= 4;
				crc ^= a <<= 5;
				crc ^= (ushort)(a << 7);
			}
			return crc;
		}

		#endregion CRC routines

		/// <summary> check overall validity </summary>
		/// <returns></returns>
		public virtual bool Valid
		{
			get
			{
				return Size < g_MaxDatagramLength && CheckCRC();
			}
		}

		public override string ToString()
		{
			return bytes == null ? base.ToString() + " empty" : BitConverter.ToString(bytes);
		}
	}
}