using System;

namespace FORIS.TSS.Communication
{
	public class DataEventArgs : EventArgs
	{
		public LinkData Data { get; set; }
	}
}