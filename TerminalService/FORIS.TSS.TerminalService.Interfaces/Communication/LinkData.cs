namespace FORIS.TSS.Communication
{
    public class LinkData
    {
        byte[] arbData;
        int iBegin;
        int iLen;
		
        public byte[] Data
        {
            get
            {
                return arbData;
            }
        }

        public int Begin
        {
            get
            {
                return iBegin;
            }
        }
		
        public int Len
        {
            get
            {
                return iLen;
            }
        }
		
        public LinkData(byte[] data, int begin, int len)
        {
            arbData = data;
            iBegin = begin;
            iLen = len;
        }
		
        public LinkData(byte[] data, int begin) : this(data, begin, data.Length)
        {
        }
		
        public LinkData(byte[] data) : this(data, 0)
        {
        }
    }
}