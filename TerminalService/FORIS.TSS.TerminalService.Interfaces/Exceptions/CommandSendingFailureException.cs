﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.TerminalService.Interfaces.Exceptions
{
	/// <summary> Исключения, возникающие при отправке команд устройству и получении ошибки на сокете </summary>
	[Serializable]
	public class CommandSendingFailureException : Exception
	{
		public CmdResult? CmdResult
		{
			get { return base.Data["CmdResult"] as CmdResult?; }
			set { base.Data["CmdResult"] = value; }
		}

		public CommandSendingFailureException(CmdResult exceptionResult, string deviceId)
			: base("Terminal server throw SendCommand exception: " + exceptionResult.ToString() + " DEVICE_ID: " + deviceId)
		{
			CmdResult = exceptionResult;
		}

		public CommandSendingFailureException(CmdResult exceptionResult)
			: base("Command cannot be executed: " + exceptionResult.ToString())
		{
			CmdResult = exceptionResult;
		}

		public CommandSendingFailureException(string message) : base(message) { }

		public CommandSendingFailureException(string message, Exception inner) : base(message, inner) { }

		public CommandSendingFailureException()
		{ }

		protected CommandSendingFailureException(System.Runtime.Serialization.SerializationInfo info,
		System.Runtime.Serialization.StreamingContext context)
		{ }
	}
}