using System;

namespace FORIS.TSS.TerminalService.Interfaces.Exceptions
{
    /// <summary>
    /// ����������, �����������, ��� �������� ������� ����������, ������� �� ���������� � �������
    /// </summary>
    [Serializable]
    public class DeviceDisconnectedException: Exception
    {

        public DeviceDisconnectedException(string message) : base(message) { }

        public DeviceDisconnectedException(string message, Exception inner) : base(message, inner) { }
        
        public DeviceDisconnectedException()
        { }

        protected DeviceDisconnectedException(System.Runtime.Serialization.SerializationInfo info,
                                              System.Runtime.Serialization.StreamingContext context) { }
    }
}