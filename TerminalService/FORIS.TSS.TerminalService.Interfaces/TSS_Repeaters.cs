﻿/*
 * Для реализации вызова callback функций на стороне клиента
 * вводится понятие "Повторителя" (Repeater). Данный объект
 * позволяет устранить зависимость сервера от метаданных клиента,
 * в частности, от реализации callback вызовов на стороне клиента.
 * 
 * Объект сервера генерирует событие ServerEvent, на которое может
 * подписаться клиент. Клиент реализует свой делегат OnEvent, сервер
 * вызывает этот метод через объект повторителя:
 * 
 * EventHandler repeater = EventsRepeater.Create(new EventHandler(OnEvent));
 * serverObject.ServerEvent += repeater;
 * 
 */

using System;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal
{
	/// <summary> Делегат событий охранных функций </summary>
	public delegate void GuardEventHandler(GuardEventArgs args);
	/// <summary> Делегат события наличия принятых терминалом данных </summary>
	public delegate void ReceiveEventHandler(ReceiveEventArgs args);
	/// <summary> Делегат события наличия принятых клиентом данных о последних позициях ТС </summary>
	public delegate void LastPositionsEventHandler(PositionsEventArgs args);
	/// <summary> Делегат события наличия принятых клиентом данных о последних позициях ТС </summary>
	public delegate void NewPositionsEventHandler(PositionsEventArgs args);
	/// <summary> Делегат события наличия сообщений у терминала </summary>
	public delegate void NotifyEventHandler(NotifyEventArgs args);
	/// <summary> Делегат события наличия сообщений у терминала </summary>
	public delegate void BulkNotifyEventHandler(BulkNotifyEventArgs args);
	/// <summary> Делегат события наличия принятых терминалом данных о истории движения </summary>
	public delegate void ReceiveLogEventHandler(ReceiveLogEventArgs args);
	/// <summary> event handler delegate of receiving message needed to count </summary>
	public delegate void ReceiveCountingEventHandler(ReceiveCountingEventArgs rcea);
	/// <summary> event handler delegate of sending messages, needed to count, to the server for saving their into db </summary>
	public delegate void SaveCountingListEventHandler(SaveCountingListEventArgs sclea);
	/// <summary> Класс повторителя события наличия принятых терминалом данных </summary>
	public class ReceiveEventRepeater : MarshalByRefObject
	{
		private ReceiveEventHandler target;
		private ReceiveEventRepeater(ReceiveEventHandler target)
		{
			this.target += target;
		}
		public void Callback(ReceiveEventArgs args)
		{
			target(args);
		}
		public static ReceiveEventHandler Create(ReceiveEventHandler target)
		{
			ReceiveEventRepeater repeater = new ReceiveEventRepeater(target);
			return new ReceiveEventHandler(repeater.Callback);
		}
		public override object InitializeLifetimeService()
		{
			return null;
		}
	}
	/// <summary> Класс повторителя события наличия принятых терминалом данных о истории движения </summary>
	public class ReceiveLogEventRepeater : MarshalByRefObject
	{
		private ReceiveLogEventHandler target;
		private ReceiveLogEventRepeater(ReceiveLogEventHandler target)
		{
			this.target += target;
		}
		public void Callback(ReceiveLogEventArgs args)
		{
			target(args);
		}
		public static ReceiveLogEventHandler Create(ReceiveLogEventHandler target)
		{
			ReceiveLogEventRepeater repeater = new ReceiveLogEventRepeater(target);
			return new ReceiveLogEventHandler(repeater.Callback);
		}
		public override object InitializeLifetimeService()
		{
			return null;
		}
	}
	/// <summary> Класс повторителя события наличия принятых терминалом данных </summary>
	public class NotifyEventRepeater : MarshalByRefObject
	{
		private NotifyEventHandler target;
		private NotifyEventRepeater(NotifyEventHandler target)
		{
			this.target += target;
		}
		public void Callback(NotifyEventArgs args)
		{
			target(args);
		}
		public static NotifyEventHandler Create(NotifyEventHandler target)
		{
			NotifyEventRepeater repeater = new NotifyEventRepeater(target);
			return new NotifyEventHandler(repeater.Callback);
		}
		public override object InitializeLifetimeService()
		{
			return null;
		}
	}
	/// <summary> Класс повторителя событий охранных функций </summary>
	public class GuardEventRepeater : MarshalByRefObject
	{
		private GuardEventHandler target;
		private GuardEventRepeater(GuardEventHandler target)
		{
			this.target += target;
		}
		public void Callback(GuardEventArgs args)
		{
			target(args);
		}
		public static GuardEventHandler Create(GuardEventHandler target)
		{
			GuardEventRepeater repeater = new GuardEventRepeater(target);
			return new GuardEventHandler(repeater.Callback);
		}
		public override object InitializeLifetimeService()
		{
			return null;
		}
	}
}