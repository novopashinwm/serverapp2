﻿using System;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Messaging
{
	public static class MessageCreator
	{
		public static IMessage Create(ICommand cmd)
		{
			return CreateCommand(cmd);
		}

		public static IMessage CreateAlarm()
		{
			IMessage msg = new AlarmMessage();
			msg.Confirm = msg.Log = true;
			msg.Severity = Severity.Lvl1;
			return msg;
		}

		public static IMessage CreateText()
		{
			IMessage msg = new TextMessage();
			msg.Confirm = true;
			msg.Log = true;
			msg.Time = DateTime.UtcNow;
			return msg;
		}

		public static IMessage Create(NotifyEventArgs e)
		{
			switch( e.Event )
			{
				case TerminalEventMessage.NE_MESSAGE:
					{
						ITextMessage msg = new TextMessage();
						
						msg.Confirm = msg.Log = true;
						msg.Direction = Direction.Incoming;
						object[] par = (object[])e.Tag;
						IUnitInfo ui = par[0] as IUnitInfo;
						if( ui != null ) 
						{
							msg.Sender = new MessageObject(
								ObjectType.Controller, ui, Direction.Outgoing );
						}
						else
						{
							msg.Sender = new MessageObject(
								ObjectType.Terminal, null, Direction.Outgoing );
						}
						msg.Target =
							new MessageObject( ObjectType.Operator, e.Operator, Direction.Incoming );
						msg.Text = e.Message;
						msg.Severity = e.Lvl;
						if( par.Length > 3 ) msg.Num = (int)par[3];
						
						return msg;
					}

				case TerminalEventMessage.NE_ALARM:
					{
						IAlarmMessage msg = new AlarmMessage();
						msg.Confirm = msg.Log = true;
						msg.Direction = Direction.Incoming;
						object[] par = (object[])e.Tag;
						IUnitInfo ui = par[0] as IUnitInfo;
						if( ui != null ) msg.Sender = new MessageObject(
							ObjectType.Controller, ui, Direction.Outgoing );
						else msg.Sender = new MessageObject(
							ObjectType.Terminal, null, Direction.Outgoing );
						msg.Target =
							new MessageObject( ObjectType.Operator, e.Operator, Direction.Incoming );
						msg.Text = e.Message;
						msg.Severity = Severity.Lvl1;
						if( par.Length > 3 ) msg.Num = (int)par[3];
						
						return msg;
					}

				case TerminalEventMessage.NE_CONFIRM:
					{
						ConfirmMessage msg = new ConfirmMessage();
						msg.Confirm = false;
						msg.Log = true;
						msg.Direction = Direction.Incoming;
						object[] par = (object[])e.Tag;
						IUnitInfo ui = par[0] as IUnitInfo;
						if( ui != null ) msg.Sender = new MessageObject(
							ObjectType.Controller, ui, Direction.Outgoing );
						else msg.Sender = new MessageObject(
							ObjectType.Terminal, null, Direction.Outgoing );
						msg.Target =
							new MessageObject( ObjectType.Operator, e.Operator, Direction.Incoming );
						msg.Text = e.Message;
						msg.Severity = e.Lvl;
						msg.Num = (int)par[4];
						msg.ConfirmNo = (int)par[2];
						
						return msg;
					}
				case TerminalEventMessage.AppLog:
					{
						ITextMessage msg = new TextMessage();
						msg.Code = e.Event.ToString();
						msg.Time = e.Time;
						msg.Confirm = false;
						msg.Direction = Direction.Incoming;
						object[] par = (object[])e.Tag;
						IUnitInfo ui = par[0] as IUnitInfo;
						if (ui != null)
							msg.Sender = new MessageObject(
								ObjectType.Controller, ui, Direction.Outgoing);
						else msg.Sender = new MessageObject(
							ObjectType.Terminal, null, Direction.Outgoing);
						msg.Text = e.Message;
						msg.Severity = e.Lvl;
						return msg;
					}
				case TerminalEventMessage.SMS:
					{
						ITextMessage msg = new TextMessage();
						msg.Time = e.Time;
						msg.Confirm = false;
						msg.Direction = Direction.Incoming;
						var par = (object[])e.Tag;
						msg.Sender = par[0] as MessageObject;
						msg.Target = new MessageObject(ObjectType.Server, null, Direction.Incoming);
						msg.Text = e.Message;
						msg.Severity = e.Lvl;
						return msg;
					}


				default:
					return null;
			}
		}

		public static IMessage CreatePosition(IMobilUnit mu, bool correctGPS)
		{
			if (!correctGPS)
			{
				//mu.Latitude = Single.MaxValue;
				//mu.Longitude = Single.MaxValue;
			}


			IPositionMessage msg = new PositionMessage {MobilUnit = mu, Valid = true};
			if (correctGPS)
			{
				msg.MobilUnit.CorrectTime = msg.MobilUnit.Time;
			}
			else
			{
				msg.MobilUnit.CorrectTime = 0;
			}

			return msg;
		}

		public static IMessage CreateCommand()
		{
			return new CommandMessage(null);
		}

		public static IMessage CreateCommand(ICommand command)
		{
			IStdCommand cmd = command as IStdCommand;
			if(cmd == null) throw new ArgumentException("Command not supported");

			CommandMessage msg;
			switch(cmd.Type)
			{
				case CmdType.SendText:
					msg = new CommandTextMessage(cmd);
					msg.Log = msg.Confirm = true;
					break;
			
				case CmdType.Confirm:
					msg = new CommandConfirmMessage(cmd);
					((ITextMessage)msg).Text = "Сообщение доставлено";
					msg.Log = true;
					break;
			
				default:
					msg = new CommandMessage(cmd);
					break;
			}
			msg.Command = cmd;
			return msg;
		}
	}
}