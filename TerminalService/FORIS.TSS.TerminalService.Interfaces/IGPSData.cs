﻿namespace FORIS.TSS.Terminal
{
	/// <summary> Информация о данных, получаемых от мобильных объектов </summary>
	public interface IGPSData : IUnitData, IUnitPosition
	{
		/// <summary> Satellites qty </summary>
		int Satellites { get; set; }
	}
}