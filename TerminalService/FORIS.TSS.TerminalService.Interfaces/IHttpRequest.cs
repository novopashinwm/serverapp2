﻿using System;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.TerminalService.Interfaces
{
    /// <summary>
    /// Информация о входящем http-запросе
    /// </summary>
    public interface IHttpRequest
    {
        Uri RequestUrl { get; }

        /// <summary>Метод http-запроса (get, post, delete etc)</summary>
        HttpMethod Method { get; }

        /// <summary>
        /// Время получения запроса, UTC
        /// </summary>
        DateTime TimeReceived { get; }
        /// <summary>Значение http-заголовка X-Requestor</summary>
        /// <remarks>Платформа A-MLC передает в заголовке X-Requestor значение Asid для идентификации запрашивающего абонента</remarks>
        string Requestor { get; }
        
        /// <summary> Текст запроса </summary>
        string Text { get; }

        /// <summary> Идентификатор протокола устройства Ufin </summary>
        string XNikaDevice { get; }

        /// <summary> Идентификатор устройства Ufin </summary>
        string XNikaDeviceID { get; }
    }
}
