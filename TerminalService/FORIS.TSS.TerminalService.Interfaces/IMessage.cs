using System;

using FORIS.TSS.Common;

namespace FORIS.TSS.Terminal.Messaging
{
	/// <summary>
	/// server side communication message interface
	/// </summary>
	public interface IMessage
	{
		/// <summary>
		/// no.
		/// </summary>
		int Num{get; set;}
		/// <summary>
		/// message direction
		/// </summary>
		Direction Direction{get; set;}
		/// <summary>
		/// message category
		/// </summary>
		MessageCategory Category{get; set;}
		/// <summary>
		/// sender object info
		/// </summary>
		MessageObject Sender{get; set;}
		/// <summary>
		/// target object info
		/// </summary>
		MessageObject Target{get; set;}
		/// <summary>
		/// message time stamp
		/// </summary>
		DateTime Time{get; set;}
		/// <summary>
		/// status
		/// </summary>
		int Status{get; set;}
		/// <summary>
		/// message severity
		/// </summary>
		Severity Severity{get; set;}
		/// <summary>
		/// need confirm
		/// </summary>
		bool Confirm{get; set;}
		/// <summary>
		/// need log
		/// </summary>
		bool Log{get; set;}
		/// <summary>
		/// extra params. category specific
		/// </summary>
		PARAMS Params{get; set;}

        int? TermID { get; set; }
	}
}