﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Terminal;

namespace FORIS.TSS.TerminalService.Interfaces
{
	/// <summary> Расширения для удобной работы с классом <see cref="IStdCommand"/> </summary>
	public static class StdCommandExtensions
	{
		public static string GetParamValue(this IStdCommand command, string key)
		{
			if (command == null)
				throw new ArgumentNullException("command");

			if (command.Params == null || !command.Params.ContainsKey(key))
				return null;

			return command.Params[key] as string;
		}

		public static void SetParamValue(this IStdCommand command, string key, string value)
		{
			if (command == null)
				throw new ArgumentNullException("command");
			if (command.Params == null)
				command.Params = new PARAMS();
			command.Params[key] = value;
		}
		public static void SetCommandParametersFromVehicleAttributes(this IStdCommand command, List<VehicleAttribute> attributes)
		{
			SetCommandParameterFromVehicleAttributes(command, attributes, VehicleAttributeName.ApnName,     PARAMS.Keys.InternetApn.Name);
			SetCommandParameterFromVehicleAttributes(command, attributes, VehicleAttributeName.ApnUser,     PARAMS.Keys.InternetApn.User);
			SetCommandParameterFromVehicleAttributes(command, attributes, VehicleAttributeName.ApnPassword, PARAMS.Keys.InternetApn.Password);
		}
		private static void SetCommandParameterFromVehicleAttributes(IStdCommand command, List<VehicleAttribute> attributes, VehicleAttributeName attributeName, string commandParameterName)
		{
			var attribute = attributes.FirstOrDefault(x => x.Name == attributeName);
			if (attribute == null)
				return;
			SetParamValue(command, commandParameterName, attribute.Value);
		}
	}
}