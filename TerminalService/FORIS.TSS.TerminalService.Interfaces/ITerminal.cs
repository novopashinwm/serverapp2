﻿using System;
using System.Messaging;
using System.Runtime.InteropServices;

namespace FORIS.TSS.Terminal
{
	/// <summary> Тип очереди для передачи сообщений </summary>
	public enum QueueType
	{
		None    = 0,
		MSMQ    = 1,
		Program = 2
	}
	/// <summary> Информация о терминале </summary>
	[Guid("55ED3931-7501-4c5f-9620-67C3A6814FC7")]
	public interface ITerminalInfo
	{
		/// <summary> Id терминала </summary>
		int    ID   { get; }
		/// <summary> Название терминала </summary>
		string Name { get; }
	}
	/// <summary> Интерфейс терминала </summary>
	[Guid("3FE9186A-A3AB-4afe-9201-6E1AA5754E08")]
	public interface ITerminal : ITerminalInfo, IDisposable
	{
		/// <summary> Событие наличия данных у терминала </summary>
		event ReceiveEventHandler         ReceiveEvent;
		/// <summary> Событие наличия сообщений у терминала (ошибки, уведомления) </summary>
		event NotifyEventHandler          NotifyEvent;
		/// <summary> Событие получения сообщения, для которое необходимо считать count </summary>
		event ReceiveCountingEventHandler ReceiveCountingEvent;
		/// <summary> Максимальный размер очереди сообщений </summary>
		/// <remarks>По достижении этого размера очередь принудительно очищается.</remarks>
		int          QueueSize     { get; }
		/// <summary> Интервал времени, по истечении которого происходит принудительная очистка очереди сообщений </summary>
		int          TimerInterval { get; }
		/// <summary> Тип очереди для передачи сообщений </summary>
		QueueType    QueueType     { get; }
		/// <summary> Очередь для отсылки сообщений </summary>
		MessageQueue SendingQueue  { get; }
		/// <summary> Очередь принятых сообщений </summary>
		MessageQueue ReceivedQueue { get; }
		/// <summary> Передача команды от терминала мобильному объекту </summary>
		/// <param name="command">команда для мобильного объекта</param>
		/// <returns>Возвращает true, если удалось отправить команду и false в противном случае</returns>
		bool SendCommand(ICommand command);
		/// <summary> Инициализация терминала через список строк </summary>
		void Initialization(params string [] initStrings);
	}
}