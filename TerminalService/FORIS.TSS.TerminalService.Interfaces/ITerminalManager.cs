﻿using System;
using System.Runtime.InteropServices;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.Messaging;
using FORIS.TSS.Common;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO;

namespace FORIS.TSS.TerminalService.Interfaces
{
	//TODO: Разделить интерфейс на две или более  частей
	/* Одна часть должна предназначаться для пользования услугами терминалов:
	 * получения позиций, отправки команд/SMS, но не больше.
	 * Методы для внутреннего использования в менеджере терминалов отделить.
	 */

	/// <summary> Базовый интерфейс для всех типов терминальных менеджеров </summary>
	/// <remarks> Этот интерфейс такой большой, потому что устройства и слушатели используют его </remarks>
	[Guid(@"A0521193-9D82-42f3-AA4D-B3AB65BD7DA1")]
	public interface ITerminalManager
	{
		/// <summary> Создание объекта конкретного терминала </summary>
		/// <param name="terminalName"> Идентификатор </param>
		/// <returns> Ссылка на объект конкретного терминала </returns>
		ITerminal CreateTerminal(string terminalName);
		/// <summary> Создание главного терминала сервера </summary>
		/// <param name="terminalName"> Идентификатор </param>
		void CreateMainTerminal(string terminalName);
		/// <summary> Создание объекта конкретного терминала </summary>
		/// <param name="type"> Type of command to handle </param>
		/// <returns> Ссылка на объект конкретного терминала</returns>
		ITerminal CreateTerminal(CmdType type);
		/// <summary> Возвращает работающие терминалы, работающие по данному каналу </summary>
		/// <param name="media"> Тип канала связи </param>
		IEnumerable<ITerminal> GetLiveTerminals(Media media);
		/// <summary> Создание объекта конкретного терминала </summary>
		/// <param name="command"> Command to handle </param>
		/// <returns> Ссылка на объект конкретного терминала</returns>
		ITerminal CreateTerminal(ICommand command);
		/// <summary> Перенаправление команды конкретному терминалу </summary>
		/// <param name="command"> Команда для мобильного объекта </param>
		void SendCommand(IStdCommand command);
		/// <summary> Get device for specified unit </summary>
		/// <param name="ui"> Info about unit </param>
		/// <returns> Device representation </returns>
		IDevice GetDevice(IUnitInfo ui);
		/// <summary> Get devices supporting specified byte datagram </summary>
		/// <param name="packet">packet with datagram</param>
		/// <returns>device representations</returns>
		List<IDevice> GetDevices(byte[] packet);
		/// <summary> Get device for specified byte datagram </summary>
		/// <param name="packet">packet with datagram</param>
		/// <returns>device representation</returns>
		IDevice GetDevice(byte[] packet);
		/// <summary> Logging and confirming support of messages </summary>
		/// <param name="msg">message object</param>
		int? RegisterMessage(IMessage msg);
		/// <summary> Assign unique number per manager life </summary>
		/// <param name="msg">message object</param>
		/// <returns>message number</returns>
		int AssignMessageNumber(IMessage msg);
		/// <summary> Get previously registered message </summary>
		/// <param name="num"> Message no. </param>
		/// <returns> Message object or null if not found </returns>
		IMessage GetRegisteredMessage(int num);
		/// <summary> Отсылка СМС </summary>
		/// <param name="target"></param>
		/// <param name="text"></param>
		void SendSMS(string target, string text);
		/// <summary> Отсылка СМС </summary>
		/// <param name="target"></param>
		/// <param name="data"></param>
		void SendSMS(string target, byte[] data);
		/// <summary> Событие наличия данных у терминала </summary>
		event AnonymousEventHandler<ReceiveEventArgs>          ReceiveEvent;
		/// <summary> Событие наличия сообщений у терминала </summary>
		event AnonymousEventHandler<BulkNotifyEventArgs>       NotifyEvent;
		/// <summary> Возвращает отсортированный по времени список последних позиций из БД за указанный период времени </summary>
		/// <param name="from"> Начало периода. UTC </param>
		/// <param name="to"> Конец периода. UTC </param>
		/// <param name="id"> Идентификатор контроллера </param>
		/// <param name="interval"> Интервал между позициями. Секунды </param>
		/// <param name="count"> Максимальное количество возвращаемых позиций </param>
		/// <returns> Отсортированный по времени список последних позиций </returns>
		SortedList<int, IMobilUnit> GetLogFromDB(int from, int to, int id, int interval, int count);
		/// <summary> Получение типа терминала </summary>
		/// <param name="ui"></param>
		/// <returns></returns>
		string GetDeviceType(IUnitInfo ui);
		/// <summary> Публикация новых данных об объектах, полученных не через стандартные терминалы </summary>
		/// <param name="mobileUnits"> Массив мобильных объектов </param>
		void PublishMobileUnits(params IMobilUnit[] mobileUnits);
		/// <summary> Возвращает устройство, которое может обработать входящий HTTP-запрос.
		/// Если запрос не может быть обработан ни одним устройством, возвращает null. </summary>
		/// <param name="httpRequest"> Входящий http-запрос от устройства </param>
		/// <returns></returns>
		IHttpDevice GetHttpDevice(IHttpRequest httpRequest);
		/// <summary> Возвращает последние известные данные об объекте наблюдения с координатой или null, если не найден </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения, <see cref="IUnitInfo.Unique"/> </param>
		IUnitPosition GetLastPosition(int vehicleId);
		/// <summary> Возвращает последние известные данные об объекте наблюдения или null, если не найден </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения, <see cref="IUnitInfo.Unique"/> </param>
		IMobilUnit GetLastMobilUnit(int vehicleId);
		/// <summary> Заполняет информацию об объекте наблюдения </summary>
		/// <param name="ui"> Объект, в котором нужно заполнить эту информацию </param>
		/// <returns> Возвращает true, если удалось успешно заполнить информацию и false в противном случае (не было изменений) </returns>
		bool FillUnitInfo(IUnitInfo ui);
		/// <summary> Возвращает SimID, привязанный к asid'у. Если SimID не существует, создаёт его и возвращает </summary>
		/// <param name="asid"> Asid для которого следует вернуть SimID </param>
		string GetOrCreateSimIdByAsid(string asid);
		/// <summary> Возвращает SimID, привязанный к идентификатору устройства. Если SimID не существует, создаёт его и возвращает </summary>
		/// <param name="asid"> Идентификатор устройства для которого следует вернуть SimID.
		/// Это может быть IMEI, IMSI или что угодно на усмотрение мобильного приложения, что сохраняется между запусками приложения и желательно между установками.</param>
		string GetOrCreateSimIdByDeviceId(string asid);
		/// <summary> Возвращает статус блокировки по объекту наблюдения/приложению обслуживания </summary>
		/// <param name="simId"> Идентификатор приложения обслуживания </param>
		/// <returns> true, если заблокировано </returns>
		DateTime? GetBlockingDate(string simId);
		/// <summary> Возвращает список услуг по данному объекту наблюдения </summary>
		/// <param name="asidId"> Идентификатор приложения обслуживания </param>
		List<BillingService> GetServices(int asidId);
		/// <summary> Удаляет лог объекта наблюдения </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		void DeleteLog(int vehicleId);
		/// <summary> Уведомляет службу терминалов об изменении статуса сообщения </summary>
		/// <param name="items"> Информация об изменении статуса сообщений </param>
		void NotifyMessageState(IEnumerable<MessageStateItem> items);
		/// <summary> Возвращает значение системной константы </summary>
		/// <param name="constant"> Код константы </param>
		string GetConstant(Constant constant);
		IStdCommand GetWaitingCommand(IUnitInfo ui, CmdType cmdType);
		/// <summary> Получение сценария для выполнения команды </summary>
		CommandScenario GetScenario(IStdCommand command);
		/// <summary> Получение интервалов слежения для объекта наблюдения </summary>
		List<TrackingSchedule> GetTrackingSchedule(int vehicleId);
	}
}