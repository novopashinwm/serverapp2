using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Messaging
{
    public interface IPositionMessage : IMessage
    {
        IMobilUnit MobilUnit{get; set;}
        bool Valid{get; set;}
    }
}