﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.Helpers;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace RU.NVG.NIKA.Terminal.MayakSled
{
    public class MayakSledDevice : Device
    {
        private const string PrefixString = "%NVS";
        private readonly byte[] _prefix = Encoding.ASCII.GetBytes(PrefixString);

        public MayakSledDevice()
            : base(typeof(MayakSledSensors), new[] { "SOBR Chip" })
        {
        }

        public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
        {
            if (count < PrefixString.Length + 2)
            {
                bufferRest = data;
                return new ArrayList();
            }

            var packetString = Encoding.ASCII.GetString(data, 0, count);
            var reader = new StringReader(packetString);

            reader.Skip(PrefixString);
            var packetLength = reader.ReadHexInt(2);
            if (count < packetLength)
            {
                bufferRest = data;
                return new ArrayList();
            }
            bufferRest = null;

            if (64*1024 < packetLength)
                return null;

            var receivedCrc = reader.ReadHexInt(2);
            var contentStartIndex = PrefixString.Length + 2 + 2;
            var calculatedCrc = CalculateCrc(data, contentStartIndex, packetLength);

            if (calculatedCrc != receivedCrc)
                return null;

            var deviceID = reader.ReadString(8);
            
            var packetCode = reader.ReadString(1);

            var result = new ArrayList(2);

            switch (packetCode)
            {
                case "4":
                    //Настроечный пакет, пока игнорируется
                    Trace.TraceInformation("{0}: {1} - {2}", this, packetCode, packetString);
                    break;
                case "5":
                    //Пакет с навигационными данными
                    var mu = new MobilUnit {CorrectGPS = false, SensorValues = new Dictionary<int, long>()};
                    mu.DeviceID = deviceID;
                    mu.SensorValues[(int) MayakSledSensors.EventCode] = reader.ReadHexInt(2);
                    var positionDate = reader.ReadDateTime("ddMMyyHHmmss");
                    mu.Time = TimeHelper.GetSecondsFromBase(positionDate);
                    mu.CorrectGPS = reader.ReadOneOf('0', '1') == '1';

                    mu.Latitude = ReadPositionDegrees(reader, 2, 'S', 'N');
                    mu.Longitude = ReadPositionDegrees(reader, 3, 'W', 'E');
                    mu.Speed = (int) Math.Round(reader.ReadDecimal(4)/10m);
                    mu.Course = reader.ReadDecimalInt(3);
                    var temperatureSign = reader.ReadOneOf('-', '+');
                    var temperature = reader.ReadDecimalInt(2);
                    mu.SensorValues[(int) MayakSledSensors.ChipTemperature] = temperatureSign == '-'
                        ? -temperature
                        : temperature;
                    var cellNetworkRecord = new CellNetworkRecord {LogTime = mu.Time};
                    cellNetworkRecord.CountryCode = reader.ReadString(3);
                    cellNetworkRecord.NetworkCode = reader.ReadString(2);
                    cellNetworkRecord.LAC = reader.ReadHexInt(4);
                    cellNetworkRecord.CellID = reader.ReadHexInt(4);

                    mu.SensorValues[(int) MayakSledSensors.SentSmsCount] = reader.ReadHexInt(4);
                    mu.SensorValues[(int) MayakSledSensors.SentGprsCount] = reader.ReadHexInt(4);
                    mu.Satellites = reader.ReadDecimalInt(2);
                    cellNetworkRecord.SignalStrength = reader.ReadDecimalInt(2);
                    mu.CellNetworks = new[] {cellNetworkRecord};
                    mu.SensorValues[(int) MayakSledSensors.VoltageIsOk] = (byte) reader.ReadOneOf('0', '1');
                    mu.SensorValues[(int) MayakSledSensors.VoltageMV] = reader.ReadDecimalInt(3)*10;
                    var workMode = reader.ReadHexInt(1);
                    mu.SensorValues[(int) MayakSledSensors.Search] = workMode & 0x01;
                    mu.SensorValues[(int) MayakSledSensors.SimBalance] = reader.ReadDecimalInt(6);
                    result.Add(mu);
                    break;
            }

            result.Add(GetConfirmPacket(deviceID, packetCode));

            return result;
        }

        private ConfirmPacket GetConfirmPacket(string deviceID, string packetCode)
        {
            var prefix = "%SBR1200";
            var sb = new StringBuilder(prefix, 17);
            sb.Append(deviceID);
            sb.Append(packetCode);
            var bytes = Encoding.ASCII.GetBytes(sb.ToString());

            var crc = CalculateCrc(bytes, prefix.Length, bytes.Length);
            var crcString = crc.ToString("X2");
            bytes[prefix.Length-2] = (byte) crcString[0];
            bytes[prefix.Length-1] = (byte) crcString[1];

            return new ConfirmPacket(bytes);
        }

        private byte CalculateCrc(byte[] data, int startIndex, int endIndex)
        {
            byte result = 0;
            for (var i = startIndex; i != endIndex; ++i)
                result += data[i];
            return result;
        }

        private static double ReadPositionDegrees(StringReader reader, int degreesCount, params char[] hemisphereSigns)
        {
            var degrees = reader.ReadDecimalInt(degreesCount);
            var minutes = reader.ReadDecimalInt(2 + 4);
            var hemisphere = reader.ReadOneOf(hemisphereSigns);

            var result = degrees + minutes/60.0/10000.0;
            return hemisphere == hemisphereSigns[0] ? -result : result;
        }

        public override bool SupportData(byte[] data)
        {
            if (!data.StartsWith(_prefix))
                return false;
            return true;
        }
    }
}
