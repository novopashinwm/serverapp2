namespace RU.NVG.NIKA.Terminal.MayakSled
{
    public enum MayakSledSensors
    {
        EventCode = 1,
        ChipTemperature = 2,
        SentSmsCount = 3,
        SentGprsCount = 4,
        VoltageIsOk = 5,
        VoltageMV = 6,
        Search = 7,
        SimBalance = 8
    }
}