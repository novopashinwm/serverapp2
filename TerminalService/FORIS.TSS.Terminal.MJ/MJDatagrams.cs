﻿using System;
using System.Diagnostics;
using FORIS.TSS.Communication;

namespace FORIS.TSS.Terminal.MJ
{
	#region enum MJDatagramType
	/// <summary>
	/// Тип датаграмы UDP для мобильных объектов
	/// </summary>
	public enum MJDatagramType
	{
		OutputDatagram   = 0x0001,
		TemporaryCommand = 0x0080,
		PermanentCommand = 0x0081
	}
	#endregion // enum MJDatagramType
	public enum MJSignalQuality
	{
		NotDetermined = 0x0,
		Determined    = 0x40,
		Differential  = 0x80,
		Mix           = 0xc0
	}
	#region class MJDatagram : Datagram
	/// <summary>
	/// Датаграмма для обмена данными с мобильным устройством
	/// </summary>
	public abstract class MJDatagram : Datagram
	{
		/// <summary>
		/// базовое время UTC
		/// </summary>
		public static DateTime baseUTC = new DateTime(1970, 1, 1, 0, 0, 0);

		/// <summary>
		/// локальная временнАя зона
		/// </summary>
		public static TimeZone localZone = TimeZone.CurrentTimeZone;

		/// <summary>
		/// размер заголовка датаграммы
		/// </summary>
		public const int g_HeaderSize = 8;

		// данные для определения позиции MJ
		protected const float g_latitudeFactor = 1.0728837e-05f;
		protected const float g_longitudeFactor = 2.1457672e-05f;
		protected const int g_invalidPosition = 0x800000;

		#region **************************** Атрибуты *******************************
		/// <summary>
		/// version of protocol
		/// </summary>
		public byte Version
		{
			get { return bytes[0]; }
			set { bytes[0] = value; }
		}

		/// <summary>
		/// communication flags
		/// </summary>
		public byte Flags
		{
			get { return bytes[1]; }
			set { bytes[1] = value; }
		}

		/// <summary>
		/// datagram type
		/// </summary>
		public MJDatagramType Type
		{
			get { return (MJDatagramType)IntFromBytesLE(bytes[4], bytes[5]); }
			set { IntToBytes((int)value, bytes, 4, 2); }
		}

		/// <summary>
		/// size without this header
		/// </summary>
		public int DataSize
		{
			get 
			{ 
				return IntFromBytesLE(bytes[6], bytes[7]); 
			}
			set 
			{
				IntToBytes(value, bytes, 6, 2);
			}
		}

		#endregion //**************************** Атрибуты *******************************

		#region .ctor
		/// <summary>
		/// Датаграмма UDP
		/// </summary>
		public MJDatagram() : this(g_HeaderSize)
		{
		}

		/// <summary>
		/// Датаграмма UDP
		/// </summary>
		/// <param name="bytes">массив байт</param>
		public MJDatagram(byte[] bytes) : this((uint)bytes.Length)
		{
			bytes.CopyTo(this.bytes, 0);

			// данные по умолчанию
			Version = 1;
			Flags = 0;
			DataSize = bytes.Length - g_HeaderSize;
		}
				
		/// <summary>
		/// Датаграмма UDP
		/// </summary>
		/// <param name="capacity">размер датаграммы</param>
		public MJDatagram(uint capacity) : base(capacity)
		{
			Version = 1;
			Flags = 0;
			DataSize = bytes.Length - g_HeaderSize;
		}
		#endregion // .ctor

		#region Emulate()
		/// <summary>
		/// Эмулятор датаграмы
		/// </summary>
		/// <returns>новая датаграма с изменённым временем, скорость мобильного объекта и другими изменёнными параметрами</returns>
		public abstract MJDatagram Emulate();
		public abstract MJDatagram Emulate(int id, int seconds, float x, float y, int speed);
		#endregion // Emulate()
	}
	#endregion // class MJDatagram : Datagram
	#region class MJOutputDatagram : MJDatagram
	/// <summary>
	/// Structure of outgoing datagram (transmitted by mobile unit)
	/// </summary>
	/// <remarks>UDP datagram, size of user data = 44 bytes.Multibyte items are in LITTLE_ENDIAN format.
	/// byte[0] = version of protocol = 0x01
	/// byte[1] = communication flags = 0x00
	/// bytes[2..3] = identifier of mobile unit (ID16) = 16-bit unsigned, (0..65535)
	/// bytes[4..5] = datagram type = 0x0001
	/// bytes[6..7] = size without this header = 0x0024
	/// bytes[8..11] = time UTC from 1.1.1970, seconds (time_t)
	/// bytes[12..14] = latitude, signed, 24-bit, multiply coef for degrees = 1.0728837e-05, the value 0x800000 means invalid position information.
	/// byte [15] = velocity, km/h, 0..254, value 255 means invalid velocity information
	/// bytes[16..18] = longitude, signed, 24-bit, multiply coef for degrees = 2.1457672e-05
	/// byte [19] = spare (for future - heading), value 0xff means invalid information
	/// byte [20] = GPS receiver status, number of used GPS satellites (+ 0x40, if position is computed)
	/// byte [21] = oldness of position information (on develop)
	/// byte [22] = spare (for future - status of alarm module)
	/// byte [23] = spare (for future - status of alarm module)
	/// byte [24] = run level and GSM status @@@
	/// bytes[25..26] = spare
	/// byte [27] = input power voltage
	/// byte [28] = voltage on analog input AN1
	/// byte [29] = voltage on analog input AN2
	/// byte [30] = voltage on analog input AN3
	/// byte [31] = voltage on analog input AN4
	/// bytes[32..37] = spare
	/// bytes[38..39] = firmware version identifier
	/// bytes[40..41] = number of datagram to end of transmission
	/// bytes[42..43] = CRC of bytes[8..41], (algorithm from zmodem protocol - CRC16)
	/// </remarks>
	public class MJOutputDatagram : MJDatagram
	{
		#region **************************** Атрибуты *******************************
		/// <summary>
		/// identifier of mobile unit (ID16) = 16-bit unsigned, (0..65535)
		/// </summary>
		public int MUID
		{
			get { return IntFromBytesLE(bytes[2], bytes[3]); }
			set { IntToBytes(value, bytes, 2, 2); }
		}

		/// <summary>
		/// time UTC from 1.1.1970, seconds (typeof time_t in c++)
		/// </summary>
		public int Seconds
		{
			get { return IntFromBytesLE(bytes[8], bytes[9], bytes[10], bytes[11]); }
			set { IntToBytes(value, bytes, 8, 4); }
		}

		/// <summary>
		/// latitude, signed, 24-bit, multiply coef for degrees = 1.0728837e-05, the value 0x800000 means invalid position information.
		/// </summary>
		public float Latitude
		{
			get 
			{ 
				return g_latitudeFactor * IntFromBytesLE(bytes[12], bytes[13], bytes[14]); 
			}
			set
			{
				int latitude = (int)(value / g_latitudeFactor);
				IntToBytes(latitude, bytes, 12, 3);
			}
		}

		/// <summary>
		/// longitude, signed, 24-bit, multiply coef for degrees = 2.1457672e-05
		/// </summary>
		public float Longitude
		{
			get 
			{
				return g_longitudeFactor * IntFromBytesLE(bytes[16], bytes[17], bytes[18]); 
			}
			set
			{
				int longitude = (int)(value / g_longitudeFactor);
				IntToBytes(longitude, bytes, 16, 3);
			}
		}

		/// <summary>
		/// velocity, km/h, 0..254, value 255 means invalid velocity information
		/// </summary>
		public int Velocity
		{
			get { return bytes[15]; }
			set { bytes[15] = (byte)value; }
		}

		/// <summary>
		/// input power voltage
		/// </summary>
		public int PowerVoltage
		{
			get { return bytes[27];}
			set { bytes[27] = (byte)value; }
		}

		/// <summary>
		/// voltage on analog input AN1
		/// </summary>
		public int VoltageAN1
		{
			get { return bytes[28];}
			set { bytes[28] = (byte)value; }
		}

		/// <summary>
		/// voltage on analog input AN2
		/// </summary>
		public int VoltageAN2
		{
			get { return bytes[29];}
			set { bytes[29] = (byte)value; }
		}

		/// <summary>
		/// voltage on analog input AN3
		/// </summary>
		public int VoltageAN3
		{
			get { return bytes[30];}
			set { bytes[30] = (byte)value; }
		}

		/// <summary>
		/// voltage on analog input AN4
		/// </summary>
		public int VoltageAN4
		{
			get { return bytes[31];}
			set { bytes[31] = (byte)value; }
		}

		public int Spare1
		{
			get { return bytes[22]; }
			set { bytes[22] = (byte) value;}
		}

		public int Spare2
		{
			get { return bytes[23]; }
			set { bytes[23] = (byte)value; }
		}
		/// <summary>
		/// firmware version identifier
		/// </summary>
		public int FirmWare
		{
			get { return IntFromBytesLE(bytes[38], bytes[39]); }
			set { IntToBytes(value, bytes, 38, 2); }
		}

		/// <summary>
		/// Count of satellites used to determine position
		/// </summary>
		public int Satellite
		{
			get { return bytes[20] & 0x3f; }
			set { bytes[20] = (byte)(bytes[20] & 0xc0 | value & 0x3f); }
		}

		public MJSignalQuality SignalQuality
		{
			get { return (MJSignalQuality)(bytes[20] & 0xc0); }	
			set { bytes[20] = (byte)(bytes[20] & 0x3f | (int)value); }
		}

		public override ushort CRC16
		{
			get { return (ushort)IntFromBytesLE(bytes[42], bytes[43]); }
			set { IntToBytes(value, bytes, 42, 2); }
		}

		#endregion //**************************** Атрибуты *******************************

		#region .ctor
		public MJOutputDatagram(byte[] bytes) : base(bytes)
		{
		}

		public MJOutputDatagram(int id, int seconds, float x, float y, int velocity, int power, int an1, 
			int an2, int an3, int an4, ushort firmware, int sattelites) : base(44)
		{
			MUID     = id;
			Seconds  = seconds;
			Velocity = velocity;
			PowerVoltage = power;
			VoltageAN1 = an1;
			VoltageAN2 = an2;
			VoltageAN3 = an3;
			VoltageAN4 = an4;
			FirmWare   = firmware;
			Longitude  = x;
			Latitude   = y;
			Satellite  = sattelites;

			//
			CRC16 = CRC_16_12_5_0(8, 41);
		}
		#endregion // .ctor

		#region Emulate()
		/// <summary>
		/// Эмулятор датаграмы
		/// </summary>
		/// <returns>новая датаграма с изменённым временем, скорость мобильного объекта и другими изменёнными параметрами</returns>
		public override MJDatagram Emulate()
		{
			//DateTime newTime = (baseUTC + 
			//	new TimeSpan(0, 0, 0, DateTime.UtcNow.Second - baseUTC.Second)).ToLocalTime();

			Random random = new Random();
			IntToBytes(DateTime.UtcNow.Second - baseUTC.Second, bytes, 8, 4);
			IntToBytes(random.Next(0, 101), bytes, 15, 1);
				
			return this;
		}
		public override MJDatagram Emulate(int id, int seconds, float x, float y, int speed)
		{
			IntToBytes(id, bytes, 2, 2);
			IntToBytes(seconds, bytes, 8, 4);
			IntToBytes((int)(y/g_latitudeFactor), bytes, 12, 3);
			IntToBytes(speed, bytes, 15, 1);
			IntToBytes((int)(x/g_longitudeFactor), bytes, 16, 3);

			return this;
		}
		#endregion // Emulate()

		#region CheckCRC()
		public override bool CheckCRC()
		{
			try
			{
				// проверка валидности данных
				// закоментированно, т.к. не ясно, что данный флаг означает
				//			if(IntFromBytes(bytes[19]) == 0xff)
				//			{
				//				Trace.WriteLine(String.Format("MJOutputDatagram error: invalid information id={0}", this.MUID));
				//				//return false;
				//			}

				ushort crc = (ushort) IntFromBytesLE(bytes[42], bytes[43]);
				ushort crc2 = CRC_16_12_5_0(8, 41);
				if (crc != crc2)
				{
					Trace.WriteLine(String.Format(
										"MJOutputDatagram error: crc={0} crc2={1} id={2}", crc, crc2, MUID));
					return false;
				}
				return true;
			}
			catch(Exception)
			{
				return false;
			}
		}
		#endregion // CheckCRCCRC()

		public override bool Valid
		{
			get
			{
				return SignalQuality != MJSignalQuality.NotDetermined && CheckCRC() && 
					IntFromBytesLE(bytes[12], bytes[13], bytes[14]) != 0x800000;
			}
		}
	}
	#endregion // class MJOutputDatagram : MJDatagram
	#region class MJInputDatagram : MJDatagram
	/// <summary>
	/// Structure of incoming datagram (received by mobile unit)
	/// </summary>
	/// <remarks>
	/// UDP datagram, size of user data = 22 bytes.
	/// Multibyte items are in LITTLE_ENDIAN format.
	/// byte [0] = version of protocol = 0x01
	/// byte [1] = communication flags = 0x00
	/// bytes[2..3] = spare
	/// bytes[4..5] = datagram type: 0x0080=temporary_command, 0x0081=permanent_command
	/// bytes[6..7] = size without this header = 0x000e
	/// bytes[8..9] = period of datagram transmission (by mobile unit) in seconds
	/// bytes[10..11] = total number of requested datagrams
	/// bytes[12..15] = spare
	/// bytes[16..17] = flags: lowest bit = use minimum distance limit (see next)
	/// bytes[18..19] = minimum distance limit for datagram transmission (for active state of UIN2 (UZAKL))
	/// bytes[20..21] = minimum distance limit for datagram transmission (for passive state of UIN2 (UZAKL))
	/// </remarks>
	public class MJInputDatagram : MJDatagram
	{
		#region **************************** Атрибуты *******************************
		public int Period
		{
			set { IntToBytes(value, bytes, 8, 2); }
		}
		#endregion //**************************** Атрибуты *******************************

		#region .ctor
		public MJInputDatagram(byte[] bytes) : base(bytes)
		{
		}
		#endregion // .ctor

		#region Emulate()
		/// <summary>
		/// Эмулятор датаграмы
		/// </summary>
		/// <returns>новая датаграма с изменённым временем, скорость мобильного объекта и другими изменёнными параметрами</returns>
		public override MJDatagram Emulate()
		{
			MJInputDatagram datagram = new MJInputDatagram(bytes);

			return datagram;
		}
		public override MJDatagram Emulate(int id, int seconds, float x, float y, int speed)
		{
			MJInputDatagram datagram = new MJInputDatagram(bytes);

			return datagram;
		}
		#endregion // Emulate()

		public override bool Valid
		{
			get
			{
				return true;
			}
		}
	}
	#endregion // class MJInputDatagram : MUDatagram
}