﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using FORIS.TSS.Communication;
using FORIS.TSS.Terminal.UDP;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.MJ
{
	public class MJ2016ListenerEventArgs : UDPTransmitterEventArgs
	{
		public MJ2016ListenerEventArgs(Datagram datagram, IPEndPoint ip, bool bIsCorrect)
			: base(datagram, ip, bIsCorrect)
		{
		}
		public override IMobilUnit[] GetMobilUnits()
		{
			var dg = datagram as MJOutputDatagram;

			if (dg == null)
				throw new Exception("Wrong type of a datagram");

			IMobilUnit mu = new MobilUnit.Unit.MobilUnit
			{
				ID = dg.MUID,
				IP = ip,
				Time = dg.Seconds,
				Latitude = dg.Latitude,
				Longitude = dg.Longitude,
				Speed = dg.Velocity != 0xff ? dg.Velocity : 0,
				Course = null,
				PowerVoltage = dg.PowerVoltage,
				VoltageAN1 = dg.VoltageAN1,
				VoltageAN2 = dg.VoltageAN2,
				VoltageAN3 = dg.VoltageAN3,
				VoltageAN4 = dg.VoltageAN4,
				Firmware = dg.FirmWare.ToString(CultureInfo.InvariantCulture),
				Satellites = dg.Satellite,
				CorrectGPS = MobilUnit.Unit.MobilUnit.MinSatellites <= dg.Satellite
			};

			mu.Properties.Add(DeviceProperty.Protocol, MJ.ProtocolName);

			if (mu.SensorValues == null)
				mu.SensorValues = new Dictionary<int, long>();

			mu.SensorValues[(int)MJSensors.Voltage1] = dg.VoltageAN1;
			mu.SensorValues[(int)MJSensors.Voltage2] = dg.VoltageAN2;
			mu.SensorValues[(int)MJSensors.Voltage3] = dg.VoltageAN3;
			mu.SensorValues[(int)MJSensors.Voltage4] = dg.VoltageAN4;
			mu.SensorValues[(int)MJSensors.Spare1] = dg.Spare1;
			mu.SensorValues[(int)MJSensors.Spare2] = dg.Spare2;


			return new[] { mu };
		}
	}
}