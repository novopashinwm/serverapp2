﻿namespace FORIS.TSS.Terminal.MJ
{
	enum MJSensors
	{
		Voltage1 = 0,
		Voltage2 = 1,
		Voltage3 = 2,
		Voltage4 = 3,
		Spare1   = 4,
		Spare2   = 5,
	}
}