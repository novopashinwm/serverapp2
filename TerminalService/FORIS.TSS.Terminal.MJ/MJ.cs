﻿using System.Collections;
using System.Net;
using FORIS.TSS.Terminal.Messaging;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.MJ
{
	public class MJ : Device
	{
		internal const string ProtocolName = "MegaJet";
		internal static class DeviceNames
		{
			internal const string MegaJet = "MegaJet";
		}
		public MJ() : base(typeof(MJSensors), new[]
		{
			DeviceNames.MegaJet,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			if (data.Length != 44)
				return false;
			var bytesLength = data.Length - MJDatagram.g_HeaderSize;
			var length = MJDatagram.IntFromBytesLE(data[6], data[7]);
			return data[0] == 1 && data[1] == 0; /* && new MJOutputDatagram(data).CheckCRC()*/;
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;
			MJOutputDatagram datagramMJ2016 = new MJOutputDatagram(data);
			MJ2016ListenerEventArgs mjArgs = new MJ2016ListenerEventArgs(datagramMJ2016,
				(new IPEndPoint(IPAddress.Any, IPEndPoint.MinPort)), datagramMJ2016.Valid);

			ArrayList aRes = new ArrayList(1);

			foreach (IMobilUnit unit in mjArgs.GetMobilUnits())
			{
				IMessage msg = MessageCreator.CreatePosition(unit, unit.ValidPosition);
				aRes.Add(msg);
			}

			return aRes;
		}
	}
}