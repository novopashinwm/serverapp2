﻿using System.Collections;
using System.Text;

namespace FORIS.TSS.Terminal.Meiligao.MVT380
{
	public class MVT380 : Device
	{
		internal const string ProtocolName = "Meiligao MVT380";
		internal static class DeviceNames
		{
			internal const string MeiligaoMVT380 = "Meiligao MVT380";
		}
		public MVT380() : base(null, new[]
		{
			DeviceNames.MeiligaoMVT380,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			if (data == null) return false;

			/*
			$$<package flag 1byte from 0x41 to 0x7A><L>,<IMEI>,<command>,<data><*checksum>\r\n
			*/
			string strVal = Encoding.ASCII.GetString(data);
			if (strVal.Length < 17 || !strVal.StartsWith("$$") || strVal.IndexOf("\r\n") < 0 || data[2] < 0x41 || data[2] > 0x7A)
				return false;

			//длина
			string lenStr = strVal.Substring(3, strVal.IndexOf(',', 3) - 3);
			int lenVal;
			if (int.TryParse(lenStr, out lenVal))
			{
				if (lenVal == strVal.IndexOf("\r\n") - 4)
					return true;
			}

			/*
			if (DataProcessor.SupportCommand(data))
			{
				return true;
			}
			*/
			return false;
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			return Parse(data, out bufferRest);
		}
		public IList Parse(byte[] data, out byte[] bufferRest)
		{
			return DataProcessor.Process(data, out bufferRest);
		}
	}
}