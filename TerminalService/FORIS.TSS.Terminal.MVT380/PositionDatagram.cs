﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Meiligao.MVT380
{
	public class PositionDatagram: BaseDatagram
	{
		public DateTime? Time;
		public bool GPSCorrect;
		public double? Lat;
		public double? Lng;
		public int? Speed;
		public int? Course;
		public byte? HDOP;
		public int? Satellites;
		/// <summary>
		/// Высота в сантиметрах
		/// </summary>
		public int? Altitude;
		public int? AN1;
		public int? AN2;
		public int? DS;

		public PositionDatagram (byte[] data):base(data)
		{
			if (IsCorrectCRC)
			{
				//Дата Время
				DateTime time;
				if (DateTime.TryParseExact(PacketPart(Packet_Parts.DateTime),
										   "yyMMddHHmmss", CultureInfo.InvariantCulture,
										   DateTimeStyles.AdjustToUniversal, out time))
				{
					Time = time;
				}

				if (Time != null && Time.HasValue)
				{
					GPSCorrect = PacketPart(Packet_Parts.GPSStatus) == "A";
				}

				if (GPSCorrect)
				{
					Satellites = int.Parse(PacketPart(Packet_Parts.Satellites), CultureInfo.InvariantCulture);
					Lat = double.Parse(PacketPart(Packet_Parts.Latitude), CultureInfo.InvariantCulture);
					Lng = double.Parse(PacketPart(Packet_Parts.Longitude), CultureInfo.InvariantCulture);
					Speed = int.Parse(PacketPart(Packet_Parts.Speed), CultureInfo.InvariantCulture);
					Course = int.Parse(PacketPart(Packet_Parts.Course), CultureInfo.InvariantCulture);
					HDOP = (byte)double.Parse(PacketPart(Packet_Parts.HDOP), CultureInfo.InvariantCulture);
					Altitude = int.Parse(PacketPart(Packet_Parts.Altitude), CultureInfo.InvariantCulture);
				}

				int ds;
				if (int.TryParse(PacketPart(Packet_Parts.State), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out ds))
				{
					DS = ds;
				}

				//Аналоговый датчик
				var adStr = PacketPart(Packet_Parts.AD);
				if (!string.IsNullOrEmpty(adStr))
				{
					int ad;
					var adArr = adStr.Split('|');
					if (adArr.Length > 0)
					{
						if (int.TryParse(adArr[0], NumberStyles.HexNumber, CultureInfo.InvariantCulture, out ad))
						{
							AN1 = ad;
						}
					}

					if (adArr.Length > 1)
					{
						if (int.TryParse(adArr[1], NumberStyles.HexNumber, CultureInfo.InvariantCulture, out ad))
						{
							AN2 = ad;
						}
					}
				}

			}

		}

		public override IList GetResponses()
		{
			if (Time == null || !Time.HasValue)
				return null;
			
			var res = new ArrayList();

			var mu = new MobilUnit.Unit.MobilUnit
			{
				cmdType = CmdType.Trace,
				DeviceID = ID,
				Time = BusinessLogic.TimeHelper.GetSecondsFromBase(Time.Value),
				CorrectGPS = GPSCorrect
			};
			mu.Properties.Add(DeviceProperty.Protocol, MVT380.ProtocolName);

			if (GPSCorrect)
			{
				mu.Latitude = Lat.Value;
				mu.Longitude = Lng.Value;
				mu.Satellites = HDOP.HasValue?HDOP.Value:(byte)10;
				if (Speed.HasValue) mu.Speed = Speed.Value;
				if (Course.HasValue) mu.Course = Course.Value;
				if (Altitude.HasValue) mu.Height = Altitude.Value;
			}

			//if (AN1.HasValue) mu.VoltageAN1 = AN1.Value;
			//if (AN2.HasValue) mu.VoltageAN2 = AN2.Value;

			if (AN1.HasValue || AN2.HasValue || DS.HasValue)
			{
				//Заполняем массив показаний датчиков
				mu.SensorValues = new Dictionary<int, long>();

				if (AN1.HasValue) mu.SensorValues.Add((int) Sensors.AN1, AN1.Value);
				if (AN2.HasValue) mu.SensorValues.Add((int)Sensors.AN2, AN2.Value);

				if (DS.HasValue)
				{
					mu.SensorValues.Add((int)Sensors.D1, (DS.Value & 0x01) == 0x01? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D2, (DS.Value & 0x02) == 0x02 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D3, (DS.Value & 0x04) == 0x04 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D4, (DS.Value & 0x08) == 0x08 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D5, (DS.Value & 0x10) == 0x10 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D6, (DS.Value & 0x20) == 0x20 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D7, (DS.Value & 0x40) == 0x40 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D8, (DS.Value & 0x80) == 0x80 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D9, (DS.Value & 0x100) == 0x100 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D10, (DS.Value & 0x200) == 0x200 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D11, (DS.Value & 0x400) == 0x400 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D12, (DS.Value & 0x800) == 0x800 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D13, (DS.Value & 0x1000) == 0x1000 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D14, (DS.Value & 0x1000) == 0x2000 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D15, (DS.Value & 0x1000) == 0x4000 ? 1 : 0);
					mu.SensorValues.Add((int)Sensors.D16, (DS.Value & 0x1000) == 0x8000 ? 1 : 0);
				}
			}

			res.Add(mu);
			return res;
		}

		public enum Sensors
		{
			AN1 = 1,
			AN2 = 2,
			D1 = 11,
			D2 = 12,
			D3 = 13,
			D4 = 14,
			D5 = 15,
			D6 = 16,
			D7 = 17,
			D8 = 18,
			D9 = 19,
			D10 = 20,
			D11 = 21,
			D12 = 22,
			D13 = 23,
			D14 = 24,
			D15 = 25,
			D16 = 26,
		}
	}
}