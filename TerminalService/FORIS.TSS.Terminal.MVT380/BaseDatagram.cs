﻿using System;
using System.Collections;
using System.Text;

namespace FORIS.TSS.Terminal.Meiligao.MVT380
{
    public enum Packet_Parts
    {
        Header = 0,
        IMEI = 1,
        Command = 2,
        EventCode = 3,
        Latitude = 4,
        Longitude = 5,
        DateTime = 6,
        GPSStatus = 7,
        Satellites = 8,
        GSMSignal = 9,
        Speed = 10,
        Course = 11,
        HDOP = 12,
        Altitude = 13,
        Journey = 14,
        Runtime = 15,
        BaseID = 16,
        State = 17,
        AD = 18,
        RFID = 19
    }

    public abstract class BaseDatagram
    {
        protected byte[] Data;
        protected string Command;
        protected bool IsCorrectCRC;
        protected string ID;
        protected string[] PacketParts;

        protected BaseDatagram(byte[] data)
        {
            Data = data;
            PacketParts = Encoding.ASCII.GetString(data).Split(',');
            Command = PacketPart(Packet_Parts.Command);
            ID = PacketPart(Packet_Parts.IMEI);
            CheckCRC();
        }

        protected string PacketPart(Packet_Parts part)
        {
            if (PacketParts.Length <= (int)part)
                return string.Empty;

            return PacketParts[(int) part];
        }

        protected void CheckCRC()
        {
            IsCorrectCRC = true;
            return; 
            //Нет алгоритма подсчета CRC
            /*
            var crc = BitConverter.ToUInt16(new byte[] { Data[Data.Length - 3], Data[Data.Length - 4] }, 0);
            var calculatedCRC = CalculateCRC();
            IsCorrectCRC = calculatedCRC == crc; 
            if (!IsCorrectCRC)
            {
                Trace.WriteLine(string.Format(this.GetType() + ".CheckCRC - wrong CRC in packet: \r\n {0}", BitConverter.ToString(Data)));
            }
            */ 
        }
        

        protected ushort CalculateCRC()
        {
            var crc = new Crc16Ccitt(InitialCrcValue.NonZero1);
            var toCalc = new byte[Data.Length - 4];
            Array.Copy(Data, toCalc, Data.Length - 4);
            var crcVal = crc.ComputeChecksum(toCalc);
            return crcVal;
        }

        public abstract IList GetResponses();
    }
}
