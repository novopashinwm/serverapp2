﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace FORIS.TSS.Terminal.Meiligao.MVT380
{
    public class DataProcessor
    {
        public static bool SupportCommand(byte[] data)
        {
            string strVal = Encoding.ASCII.GetString(data);
            var parts = strVal.Split(',');
            /*
             * Поддерживаемые команды: 
             * AAA - приход координат
            */
            string command = parts[(int)Packet_Parts.Command];
            switch (command)
            {
                case "AAA":
                    return true;
            }

            return false;
        }

        private static List<byte[]> SplitDataToPackets(byte[] data, out byte[] bufferRest)
        {
            bufferRest = null;
            var originalData = data;
            var res = new List<byte[]>();
            if (data == null || data.Length == 0)
                return null;

            while (data.Length > 0)
            {

                if (data.Length < 4)
                {
                    bufferRest = data;
                    break;
                }

                //Длина пакета
                string strVal = Encoding.ASCII.GetString(data);
                
                if (strVal.IndexOf(',', 3) < 0)
                {
                    bufferRest = data;
                    break;
                }
                
                string lenStr = strVal.Substring(3, strVal.IndexOf(',', 3)-3);
                int packetLength;
                if (!int.TryParse(lenStr, out packetLength))
                {
                    Trace.WriteLine(string.Format("MVT380 Wrong packet format: \r\n {0}", BitConverter.ToString(originalData)));
                    break;
                }

                packetLength = packetLength + 6;

                if (data.Length < packetLength)
                {
                    bufferRest = data;
                    break;
                }

                var recArr = new byte[packetLength];
                Array.Copy(data, 0, recArr, 0, packetLength);
                res.Add(recArr);

                var newData = new byte[data.Length - packetLength];
                Array.Copy(data, packetLength, newData, 0, data.Length - packetLength);
                data = newData;
            }


            return res;
        }

        public static IList Process(byte[] data, out byte[] bufferRest)
        {
            var packets = SplitDataToPackets(data, out bufferRest);

            if (packets == null || packets.Count == 0)
                return null;

            var alRes = new ArrayList();

            foreach (var packet in packets)
            {
                var responses = ProcessPacket(packet);
                if (responses != null && responses.Count > 0)
                {
                    alRes.AddRange(responses);
                }
            }

            return alRes;
        }

        private static IList ProcessPacket(byte[] packet)
        {
            string strVal = Encoding.ASCII.GetString(packet);


            //Если поддерживаемая команда, обрабатываем ее
            if (SupportCommand(packet))
            {
                var parts = strVal.Split(',');
                BaseDatagram datagram = null;
                var command = parts[(int)Packet_Parts.Command];

                switch(command)
                {
                    case "AAA": //Координаты
                        datagram = new PositionDatagram(packet);
                        break;
                }
                if (datagram != null)
                    return datagram.GetResponses();

            }

            return null;
        }
    }
}
