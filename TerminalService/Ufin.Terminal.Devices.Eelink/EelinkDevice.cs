﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.Messaging;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace Compass.Ufin.Terminal.Devices.Eelink
{
	public class EelinkDevice : Device
	{
		internal static class DeviceNames
		{
			internal const string KeelinK20   = "Keelin K20";
			internal const string KeelinGPT18 = "Keelin GPT18";
		}
		private readonly byte[] _header = { 0x67, 0x67 };

		public EelinkDevice() : base(typeof(EelinkSensor), new[] 
		{
			DeviceNames.KeelinK20,
			DeviceNames.KeelinGPT18,
		})
		{
		}

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			var result = new ArrayList();
			bufferRest = null;
			var reader = new DataReader(data, 0, count);

			var staticPartLen = _header.Length + 1 + 2;
			// Если размер меньше статической части пакета, возвращаем байты обратно
			if (reader.RemainingBytesCount < staticPartLen)
				bufferRest = reader.GetRemainingBytes();
			// Цикл чтения пакета
			while (reader.Any())
			{
				// Заголовок
				reader.Skip(_header);
				var packetType    = (Protocol)reader.ReadByte();
				var packetContLen = (ushort)reader.ReadBigEndian32(2);
				var packetReadLen = reader.BytesRead;
				// Если оставшейся длины не хватит для разбора пакета то возвращаем обратно, может это часть следующего пакета
				if (reader.RemainingBytesCount < packetContLen)
				{
					reader.Rewind(staticPartLen);
					bufferRest = reader.GetRemainingBytes();
					break;
				}
				// Идентификатор пакета
				var serialNumber = (ushort)reader.ReadBigEndian32(2);
				// Разбираем контент пакета
				switch (packetType)
				{
					case Protocol.LoginPacket:
						// Получить IMEI устройства
						var imei = reader.ReadBytes(8).ToHexString().TrimStart('0');
						if (!ImeiHelper.IsImeiCorrect(imei))
							throw new ArgumentException($"Invalid IMEI: {imei}");
						stateData = new ReceiverState { DeviceID = imei };
						result.Add(new ReceiverStoreToStateMessage
						{
							StateData = stateData
						});
						// Version 1.X, далее идут 2 байта, которые пока не нужны
						// Version 2.X, далее идут байты, которые пока не нужны
						// Подтверждение
						result.Add(MakeConfirmPacket(packetType, serialNumber));
						break;
					case Protocol.GpsPacket:
						result.Add(ReadGps(reader));
						// Подтверждение не требуется
						break;
					case Protocol.HeardbeatPacket:
						// TODO добавить разбор данных поля состояния
						result.Add(ReadHeartbeat(reader));
						// Подтверждение
						result.Add(MakeConfirmPacket(packetType, serialNumber));
						break;
					case Protocol.AlarmPacket:
						result.Add(ReadAlarm(reader));
						// Подтверждение
						result.Add(MakeConfirmPacket(packetType, serialNumber));
						break;
					case Protocol.TerminalStatePacket:
						result.Add(ReadTerminalState(reader));
						// Подтверждение
						result.Add(MakeConfirmPacket(packetType, serialNumber));
						break;
					case Protocol.SmsCommandUploadPacket:
						foreach (var x in ReadSmsCommandUpload(reader))
							result.Add(x);
						// Подтверждение
						result.Add(MakeConfirmPacket(packetType, serialNumber));
						break;
					case Protocol.LocationPackage:
						result.Add(ReadLocationPackage(reader));
						// Подтверждение не требуется для TCP
						break;
					default:
						// Подтверждение получения по умолчанию
						result.Add(MakeConfirmPacket(packetType, serialNumber));
						break;
				}
				// Читаем оставшиеся байты распознанной команды
				reader.ReadBytes(packetContLen + packetReadLen - reader.BytesRead);
			}
			// Если есть разобранные объекты наблюдения, указываем DeviceId
			foreach (var mu in result.OfType<IMobilUnit>())
				mu.DeviceID = (stateData as ReceiverState)?.DeviceID;
			// Результат
			return result;
		}

		private MobilUnit ReadHeartbeat(DataReader reader)
		{
			var mu = UnitReceived();
			ReadDeviceStatus(reader, mu);
			return mu;
		}

		private IEnumerable ReadSmsCommandUpload(DataReader reader)
		{
			var mu = ReadGpsBase(reader);
			yield return mu;

			var msisdn = reader.ReadAsciiString(21);
			var bytes = reader.ReadBytes(reader.RemainingBytesCount);
			var text = Encoding.UTF8.GetString(bytes);

			var notifyEventArgs =
				new NotifyEventArgs(
					TimeHelper.GetDateTimeUTC(mu.Time),
					TerminalEventMessage.SMS,
					null,
					Severity.Lvl7,
					NotifyCategory.Controller,
					text,
					null,
					new MessageObject(ObjectType.Phone, msisdn, Direction.Incoming));
			yield return notifyEventArgs;
		}
		private MobilUnit ReadTerminalState(DataReader reader)
		{
			var mu = ReadGpsBase(reader);
			reader.Skip(1); // похоже, что это автомобильная тематика
			ReadDeviceStatus(reader, mu);
			return mu;
		}
		private void ReadDeviceStatus(DataReader reader, MobilUnit mu)
		{
			var deviceStatus = reader.ReadBigEndian32(2);
			if ((deviceStatus & (1 << 8)) != 0)
			{
				var chargerPlugged = (deviceStatus & (1 << 7)) != 0;
				mu.SetSensorValue((int)EelinkSensor.ChargerPlugged, chargerPlugged);
			}
		}
		private MobilUnit ReadAlarm(DataReader reader)
		{
			var mu = ReadGpsBase(reader);
			var alarmType = reader.ReadByte();
			mu.SetSensorValue((int)EelinkSensor.AlarmType, alarmType);
			return mu;
		}
		private MobilUnit ReadGps(DataReader r)
		{
			var mu = ReadGpsBase(r);

			ReadDeviceStatus(r, mu);
			var batteryVoltage = r.ReadBigEndian32(2);
			var signalStrength = r.ReadBigEndian32(2);
			var an1 = r.ReadBigEndian32(2);
			var an2 = r.ReadBigEndian32(2);

			mu.SetSensorValue((int)EelinkSensor.BatteryVoltage, batteryVoltage);

			var cellNetworkRecord = mu.CellNetworks[0];
			cellNetworkRecord.SignalStrength = (int)signalStrength - 110;
			mu.CellNetworks[0] = cellNetworkRecord;

			mu.SetSensorValue((int)EelinkSensor.AnalogueInput1, an1);
			mu.SetSensorValue((int)EelinkSensor.AnalogueInput2, an2);

			return mu;
		}
		private MobilUnit ReadGpsBase(DataReader r)
		{
			var mu = UnitReceived();
			
			mu.Time          = (int) r.ReadBigEndian32(4);
			var latSeconds   = ((int)r.ReadBigEndian32(4)) / 500M;
			mu.Latitude      = (double)(latSeconds / 3600);
			var lngSeconds   = ((int)r.ReadBigEndian32(4)) / 500M;
			mu.Longitude     = (double)(lngSeconds / 3600);
			mu.Speed         = r.ReadByte();
			mu.Course        = (int)r.ReadBigEndian32(2);
			var cell         = new CellNetworkRecord();
			cell.CountryCode = r.ReadBigEndian32(2).ToString();
			cell.NetworkCode = r.ReadBigEndian32(2).ToString();
			cell.LAC         = (int)r.ReadBigEndian32(2);
			cell.CellID      = (int)r.ReadBigEndian32(3);
			mu.CellNetworks  = new[] { cell };

			var positionStatus = r.ReadByte();
			mu.CorrectGPS = (positionStatus & 0x01) == 1;

			mu.SetSensorValue((int)EelinkSensor.AlarmType, 0);
			return mu;
		}
		private MobilUnit ReadLocationPackage(DataReader dr)
		{
			var mu = UnitReceived();
			ReadDevicePositionV2(dr, mu);
			ReadDeviceStatusV2(dr, mu);
			mu.SetSensorValue((int)EelinkSensor.BatteryVoltage, dr.ReadBigEndian32(2));
			mu.SetSensorValue((int)EelinkSensor.AnalogueInput1, dr.ReadBigEndian32(2));
			mu.SetSensorValue((int)EelinkSensor.AnalogueInput2, dr.ReadBigEndian32(2));
			// Mileage is accumulated only when GPS is fixed.
			if (mu.CorrectGPS)
				mu.Run = (int)dr.ReadBigEndian32(4);
			mu.SetSensorValue((int)EelinkSensor.AlarmType, 0);
			return mu;
		}
		private void ReadDevicePositionV2(DataReader dr, MobilUnit mu)
		{
			// Время позиции
			mu.Time = (int)dr.ReadBigEndian32(4);
			// Маска наличия данных
			var mask = new BitArray(dr.ReadBytes(1));
			// GPS Data. The following data are related to GPS and valid only if BIT0 of mask is 1
			if (mask[0])
			{
				mu.Latitude   = (double)(((int)dr.ReadBigEndian32(4)) / 500M / 3600);
				mu.Longitude  = (double)(((int)dr.ReadBigEndian32(4)) / 500M / 3600);
				mu.Height     = (int)dr.ReadBigEndian32(2) * 100;
				mu.Speed      = (int)dr.ReadBigEndian32(2);
				mu.Course     = (int)dr.ReadBigEndian32(2);
				mu.Satellites = (int)dr.ReadByte();
			}
			// BSID0. The following data are related to home base station and valid only if BIT1 of mask is 1
			var BSID0 = default(CellNetworkRecord);
			if (mask[1])
			{
				BSID0 = new CellNetworkRecord
				{
					LogTime        = mu.Time,
					//MCC,   2, Mobile Country Code — Unsigned 16 bits integer
					CountryCode    = dr.ReadBigEndian32(2).ToString(),
					//MNC,   2, Mobile Network Code — Unsigned 16 bits integer
					NetworkCode    = dr.ReadBigEndian32(2).ToString(),
					//LAC,   2, Location Area Code — Unsigned 16 bits integer
					LAC            = (int)dr.ReadBigEndian32(2),
					//CID,   4, Cell ID with RNC — Unsigned 32 bits integer
					CellID         = (int)dr.ReadBigEndian32(4),
					//RxLev, 1, Cell signal level — Unsigned 8 bits integer(0: -110dB 1:-109dB 2:-108dB … 110: 0dB)
					SignalStrength = (int)dr.ReadByte() - 110,
				};
			}
			// BSID1. The following data are related to the 1st neighbor base station and valid only if BIT2 of mask is 1
			var BSID1 = default(CellNetworkRecord);
			if (mask[2])
			{
				BSID1 = new CellNetworkRecord
				{
					LogTime        = BSID0.LogTime,
					//MCC,   2, Mobile Country Code — Unsigned 16 bits integer
					CountryCode    = BSID0.CountryCode,
					//MNC,   2, Mobile Network Code — Unsigned 16 bits integer
					NetworkCode    = BSID0.NetworkCode,
					//LAC,   2, Location Area Code — Unsigned 16 bits integer
					LAC            = (int)dr.ReadBigEndian32(2),
					//CID,   4, Cell ID with RNC — Unsigned 32 bits integer
					CellID         = (int)dr.ReadBigEndian32(4),
					//RxLev, 1, Cell signal level — Unsigned 8 bits integer(0: -110dB 1:-109dB 2:-108dB … 110: 0dB)
					SignalStrength = (int)dr.ReadByte() - 110,
				};
			}
			// BSID2. The following data are related to the 2nd neighbor base station and valid only if BIT3 of mask is 1
			var BSID2 = default(CellNetworkRecord);
			if (mask[3])
			{
				BSID2 = new CellNetworkRecord
				{
					LogTime        = BSID0.LogTime,
					//MCC,   2, Mobile Country Code — Unsigned 16 bits integer
					CountryCode    = BSID0.CountryCode,
					//MNC,   2, Mobile Network Code — Unsigned 16 bits integer
					NetworkCode    = BSID0.NetworkCode,
					//LAC,   2, Location Area Code — Unsigned 16 bits integer
					LAC            = (int)dr.ReadBigEndian32(2),
					//CID,   4, Cell ID with RNC — Unsigned 32 bits integer
					CellID         = (int)dr.ReadBigEndian32(4),
					//RxLev, 1, Cell signal level — Unsigned 8 bits integer(0: -110dB 1:-109dB 2:-108dB … 110: 0dB)
					SignalStrength = (int)dr.ReadByte() - 110,
				};
			}
			mu.CellNetworks = (new[] { BSID0, BSID1, BSID2 }).Where(i => !i.Equals(default(CellNetworkRecord))).ToArray();
			// BSS0. The following data are related to the 1st WiFi hot-spot and valid only if BIT4 of mask is 1
			var BSS0 = default(WlanRecord);
			if (mask[4])
			{
				BSS0 = new WlanRecord
				{
					LogTime        = mu.Time,
					WlanMacAddress = dr.ReadBytes(6).ToHexString(),
					SignalStrength = (int)dr.ReadBigEndian32(1),
					WlanSSID       = "",
				};
			}
			// BSS1. The following data are related to the 2nd WiFi hot-spot and valid only if BIT5 of mask is 1
			var BSS1 = default(WlanRecord);
			if (mask[5])
			{
				BSS1 = new WlanRecord
				{
					LogTime        = mu.Time,
					WlanMacAddress = dr.ReadBytes(6).ToHexString(),
					SignalStrength = (int)dr.ReadBigEndian32(1),
					WlanSSID       = "",
				};
			}
			// BSS2. The following data are related to the 3rd WiFi hot-spot and valid only if BIT6 of mask is 1
			var BSS2 = default(WlanRecord);
			if (mask[6])
			{
				BSS2 = new WlanRecord
				{
					LogTime        = mu.Time,
					WlanMacAddress = dr.ReadBytes(6).ToHexString(),
					SignalStrength = (int)dr.ReadBigEndian32(1),
					WlanSSID       = "",
				};
			}
			mu.Wlans = (new[] { BSS0, BSS1, BSS2 }).Where(i => !i.Equals(default(WlanRecord))).ToArray();
			mu.SetSensorValue((int)EelinkSensor.AlarmType, 0);
		}
		private void ReadDeviceStatusV2(DataReader dr, MobilUnit mu)
		{
			var statusBits = new BitArray(
				BitConverter.GetBytes(
					(ushort)dr.ReadBigEndian32(2)));
			// Bit:00, 1: GPS is fixed, 0: GPS is not fixed
			mu.CorrectGPS = statusBits.Get(0);
			// Bit:07, 1: External charging is supported 0: No external charging
			if (statusBits.Get(7))
				// Bit:08, 1: Device is charging (only valid when bit 7 is 1) 0: Device is not charging
				mu.SetSensorValue((int)EelinkSensor.ChargerPlugged, statusBits.Get(8));
		}
		private ConfirmPacket MakeConfirmPacket(Protocol protocol, ushort serialNumber)
		{
			var bytes = new List<byte>();

			bytes.Add(0x67);
			bytes.Add(0x67);
			bytes.Add((byte)protocol);
			bytes.Add(0);
			bytes.Add(2);
			bytes.AddRange(NumberHelper.ToBE(serialNumber));

			return new ConfirmPacket(bytes.ToArray());
		}
		public override bool SupportData(byte[] data)
		{
			if (data.Length < 2 + 1 + 2 + 2 || 1024 < data.Length)
				return false;
			// header
			if (data[0] != 0x67 || data[1] != 0x67)
				return false;

			// protocol number
			if (data[2] != (byte)Protocol.LoginPacket)
				return false;

			// packet size cannot be greater than 1024
			var packetSize = data.FromBE2UInt32(3, 2);
			if (1024 - (2 + 1 + 2) < packetSize)
				return false;

			return true;
		}
		public override List<CommandStep> GetCommandSteps(IStdCommand command)
		{
			switch (command.Type)
			{
				case CmdType.Setup:
					return GetSetupSteps(command);
				case CmdType.SetInterval:
					return GetSetIntervalCommandSteps(command);
				case CmdType.Status:
					return GetStatusCommandSteps(command);
				case CmdType.SetSos:
					return GetSetSosSteps(command);
				case CmdType.DeleteSOS:
					return GetDeleteSOSSteps(command);
				case CmdType.AddContact:
					return GetAddContactSteps(command);
				case CmdType.DeleteContact:
					return GetDeleteContactSteps(command);
				case CmdType.AskPosition:
					return GetAskPositionSteps(command);
				case CmdType.ReloadDevice:
					return GetReloadDeviceSteps(command);
				case CmdType.SetModeOnline:
					return GetSetModeOnlineSteps(command);
				case CmdType.SetModeWaiting:
					return GetSetModeWaitingSteps(command);
				case CmdType.Callback:
					return GetCallbackSteps(command);
				default:
					return base.GetCommandSteps(command);
			}
		}
		private static string GetPassword(IUnitInfo ui)
		{
			if (string.IsNullOrWhiteSpace(ui.Password))
			{
				switch (ui.DeviceType)
				{
					case DeviceNames.KeelinGPT18:
						return "0000";
				}
			}
			return ui.Password;
		}
		private List<CommandStep> GetSetupSteps(IStdCommand command)
		{
			var devPwd = GetPassword(command.Target);
			var srvAdd = Manager.GetConstant(Constant.ServerIP);
			var srvPrt = Manager.GetConstant(Constant.ServerPort);
			var apnCfg = GetInternetApnConfig(command);
			var rplPhn = ContactHelper.GetNormalizedPhone(command.GetParamValue(PARAMS.Keys.ReplyToPhone));
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.KeelinK20:
					commands = new List<string>
					{
						$@"apn,{apnCfg.Name},{apnCfg.User},{apnCfg.Pass}#",
						$@"saving,0#",
						$@"timer,60#",
						$@"gmt,e,0#",
						$@"server,0,{srvAdd},{srvPrt}#",
					};
					break;
				case DeviceNames.KeelinGPT18:
					commands = new List<string>
					{
						$@"login,{devPwd}#",
						$@"manager,1,+{rplPhn},admin#",
						$@"apn,{apnCfg.Name},{apnCfg.User},{apnCfg.Pass}#",
						$@"gmt,e,0#",
						$@"lang,0#",
						$@"gsm,0#",
						$@"gps,0#",
						$@"collect,30,200,30,10,1#",
						$@"server,""tcp://{srvAdd}:{srvPrt}""#",
					};
					break;
			}
			var result = new List<CommandStep>(
				commands.Select(x => new SmsCommandStep(x))
			);

			return result;
		}
		private List<CommandStep> GetSetIntervalCommandSteps(IStdCommand command)
		{
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.KeelinK20:
					commands = new List<string>
					{
						$@"timer,60#"
					};
					break;
				case DeviceNames.KeelinGPT18:
					commands = new List<string>
					{
						$@"collect,30,200,30,10,1#"
					};
					break;
			}
			return new List<CommandStep>(commands.Select(x => new SmsCommandStep(x)));
		}
		private List<CommandStep> GetStatusCommandSteps(IStdCommand command)
		{
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.KeelinK20:
					commands = new List<string>
					{
						$@"status#"
					};
					break;
				case DeviceNames.KeelinGPT18:
					commands = new List<string>
					{
						$@"status#"
					};
					break;
			}
			return new List<CommandStep>(commands.Select(x => new SmsCommandStep(x)));
		}
		private List<CommandStep> GetSetSosSteps(IStdCommand command)
		{
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.KeelinK20:
					commands = new List<string>
					{
						$@"sos,a,+79991112233#"
					};
					break;
			}
			return new List<CommandStep>(commands.Select(x => new SmsCommandStep(x)));
		}
		private List<CommandStep> GetDeleteSOSSteps(IStdCommand command)
		{
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.KeelinK20:
					commands = new List<string>
					{
						$@"sos,d,+79991112233#"
					};
					break;
			}
			return new List<CommandStep>(commands.Select(x => new SmsCommandStep(x)));
		}
		private List<CommandStep> GetAddContactSteps(IStdCommand command)
		{
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.KeelinGPT18:
					commands = new List<string>
					{
						$@"manager,2,+79991112233,contactName#"
					};
					break;
			}
			return new List<CommandStep>(commands.Select(x => new SmsCommandStep(x)));
		}
		private List<CommandStep> GetDeleteContactSteps(IStdCommand command)
		{
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.KeelinGPT18:
					commands = new List<string>
					{
						$@"manager,2#"
					};
					break;
			}
			return new List<CommandStep>(commands.Select(x => new SmsCommandStep(x)));
		}
		private List<CommandStep> GetAskPositionSteps(IStdCommand command)
		{
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.KeelinK20:
					commands = new List<string>
					{
						$@"url#"
					};
					break;
				case DeviceNames.KeelinGPT18:
					commands = new List<string>
					{
						$@"url#"
					};
					break;
			}
			return new List<CommandStep>(commands.Select(x => new SmsCommandStep(x)));
		}
		private List<CommandStep> GetReloadDeviceSteps(IStdCommand command)
		{
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.KeelinK20:
					commands = new List<string>
					{
						$@"reset#"
					};
					break;
				case DeviceNames.KeelinGPT18:
					commands = new List<string>
					{
						$@"reset#"
					};
					break;
			}
			return new List<CommandStep>(commands.Select(x => new SmsCommandStep(x)));
		}
		private List<CommandStep> GetSetModeOnlineSteps(IStdCommand command)
		{
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.KeelinK20:
					commands = new List<string>
					{
						$@"saving,0#"
					};
					break;
				case DeviceNames.KeelinGPT18:
					commands = new List<string>
					{
						$@"gps,0#"
					};
					break;
			}
			return new List<CommandStep>(commands.Select(x => new SmsCommandStep(x)));
		}
		private List<CommandStep> GetSetModeWaitingSteps(IStdCommand command)
		{
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.KeelinK20:
					commands = new List<string>
					{
						$@"saving,1#"
					};
					break;
				case DeviceNames.KeelinGPT18:
					commands = new List<string>
					{
						$@"gps,1#"
					};
					break;
			}
			return new List<CommandStep>(commands.Select(x => new SmsCommandStep(x)));
		}
		private List<CommandStep> GetCallbackSteps(IStdCommand command)
		{
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.KeelinGPT18:
					commands = new List<string>
					{
						$@"listen,+79991112233#"
					};
					break;
			}
			return new List<CommandStep>(commands.Select(x => new SmsCommandStep(x)));
		}
	}
}