﻿namespace Compass.Ufin.Terminal.Devices.Eelink
{
	public enum Protocol : byte
	{
		/// <summary> Version 1.X и 2.X (разное наполнение) </summary>
		/// <remarks> Tracker to Server, Ответ необходим? Да</remarks>
		LoginPacket            = 0x01,
		/// <summary> Version 1.X </summary>
		/// <remarks> Tracker to Server, Ответ необходим? Нет </remarks>
		GpsPacket              = 0x02,
		/// <summary> Version 1.X и 2.X </summary>
		/// <remarks> Tracker to Server, Ответ необходим? Да </remarks>
		HeardbeatPacket        = 0x03,
		/// <summary> Version 1.X </summary>
		/// <remarks> Tracker to Server, Ответ необходим? Да </remarks>
		AlarmPacket            = 0x04,
		/// <summary> Version 1.X </summary>
		/// <remarks> Tracker to Server, Ответ необходим? Да </remarks>
		TerminalStatePacket    = 0x05,
		/// <summary> Version 1.X </summary>
		/// <remarks> Tracker to Server, Ответ необходим? Да </remarks>
		SmsCommandUploadPacket = 0x06,
		/// <summary> Version 1.X </summary>
		/// <remarks> Tracker to Server, Ответ необходим? Да </remarks>
		ObdPacket              = 0x07,
		/// <summary> Version 1.X </summary>
		/// <remarks> Tracker to Server, Ответ необходим? Да </remarks>
		ObdFaultCodesPacket    = 0x09,
		/// <summary> Version 1.X </summary>
		/// <remarks> Tracker to Server, Ответ необходим? Да </remarks>
		PhotoInformationPacket = 0x0E,
		/// <summary> Version 1.X </summary>
		/// <remarks> Tracker to Server, Ответ необходим? Да </remarks>
		PhotoContentPacket     = 0x0F,
		/// <summary> Version 2.X </summary>
		/// <remarks> Tracker to Server, Ответ необходим? Да для UDP и нет для TCP </remarks>
		LocationPackage        = 0x12,
		/// <summary> Version 2.X </summary>
		/// <remarks> Tracker to Server, Ответ необходим? Да </remarks>
		WarningPackage         = 0x14,
		/// <summary> Version 2.X </summary>
		/// <remarks> Tracker to Server, Ответ необходим? Да </remarks>
		ReportPackage          = 0x15,
		/// <summary> Version 2.X </summary>
		/// <remarks> Tracker to Server, Ответ необходим? Да </remarks>
		MessagePackage         = 0x16,
		/// <summary> Version 2.X </summary>
		/// <remarks> Tracker to Server, Ответ необходим? Да </remarks>
		ObdDataPackage         = 0x17,
		/// <summary> Version 2.X </summary>
		/// <remarks> Tracker to Server, Ответ необходим? Да </remarks>
		ObdBodyPackage         = 0x18,
		/// <summary> Version 2.X </summary>
		/// <remarks> Tracker to Server, Ответ необходим? Да </remarks>
		ObdFaultPackage        = 0x19,
		/// <summary> Version 2.X </summary>
		/// <remarks> Tracker to Server, Ответ необходим? Да </remarks>
		PedometerPackage       = 0x1A,
		/// <summary> Version 2.X </summary>
		/// <remarks> Tracker to Server, Ответ необходим? Да </remarks>
		ParamSetPackage        = 0x1B,
		/// <summary> Version 1.X и 2.X </summary>
		/// <remarks> Server to Tracker, Ответ необходим? Да </remarks>
		InstructionPackage     = 0x80,
		/// <summary> Version 1.X и 2.X </summary>
		/// <remarks> Server to Tracker, Ответ необходим? Да </remarks>
		BroadcastPackage       = 0x81
	}
}