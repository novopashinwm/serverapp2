﻿namespace Compass.Ufin.Terminal.Devices.Eelink
{
	public enum EelinkSensor
	{
		AlarmType      = 1,
		BatteryVoltage = 2,
		AnalogueInput1 = 3,
		AnalogueInput2 = 4,
		ChargerPlugged = 5
	}
}