﻿using System.ComponentModel;
using FORIS.TSS.ServerApplication.Database;

namespace FORIS.TSS.ServerApplication.Terminal
{
	public class TerminalServerDatabaseManager : DatabaseManager
	{
		public TerminalServerDatabaseManager()
			: base()
		{
		}
		public TerminalServerDatabaseManager(IContainer container)
			: base(container)
		{
		}
	}
}