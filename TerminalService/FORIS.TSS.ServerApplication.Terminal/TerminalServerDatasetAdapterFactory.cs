﻿using System.ComponentModel;
using FORIS.DataAccess;
using FORIS.TSS.BusinessLogic.Terminal;
using FORIS.TSS.ServerApplication.Database;

namespace FORIS.TSS.ServerApplication.Terminal
{
	/// <summary>
	/// 
	/// </summary>
	/// <remarks>
	/// Этот компонент может организовать весьма хитрое 
	/// управление адаптерами (кэширование, предварительное 
	/// создание и настройка и т.д. и т.п.)
	/// </remarks>
	public class TerminalServerDatasetAdapterFactory : Component, IDatasetAdapterFactory
	{
		#region Constructor &

		public TerminalServerDatasetAdapterFactory()
		{
		}
		public TerminalServerDatasetAdapterFactory(IContainer container)
		{
			container.Add(this);
		}

		#endregion Constructor &

		public IDatasetAdapter GetAdapter()
		{
			return new SqlDatasetAdapter();
		}
	}
}