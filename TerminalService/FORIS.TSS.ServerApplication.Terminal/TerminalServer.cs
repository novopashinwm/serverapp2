﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Net.Sockets;
using System.Runtime.Remoting;
using System.Threading;
using FORIS.DataAccess;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.Common;
using FORIS.TSS.Config;
using FORIS.TSS.Helpers;
using FORIS.TSS.ServerApplication.Diagnostic;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.ServerApplication.Terminal
{
	/// <summary>
	///
	/// </summary>
	/// <remarks>
	/// Компонент не может разрабатываться в дизайнере,
	/// так как унаследован от обобщенного типа.
	/// Будем, однако, надеяться, что в будущем это станет возможным
	/// </remarks>
	public class TerminalServer : ServerBase<TerminalServer>
	{
		#region Controls & Components

		private IContainer                    _components;
		private TerminalServerDispatcher      _serverDispatcher;
		private TerminalServerDatabaseManager _databaseManager;

		#endregion Controls & Components

		#region Constructor & Dispose

		public TerminalServer(SessionManager<TerminalServer> sessionManager, NameValueCollection properties)
			: base(sessionManager, properties)
		{
			InitializeComponent();
			Init();
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
				_components?.Dispose();
			base.Dispose(disposing);
		}

		#endregion Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			_components       = new Container();
			_serverDispatcher = new TerminalServerDispatcher(_components);
			_databaseManager  = new TerminalServerDatabaseManager(_components) { Database = Globals.TssDatabaseManager.DefaultDataBase };
		}

		#endregion Component Designer generated code

		protected override ServerDispatcher<TerminalServer> GetServerDispatcher()
		{
			return _serverDispatcher;
		}
		protected override IDatabaseDataSupplier GetDatabaseDataSupplier()
		{
			return _databaseManager;
		}
		/// <summary> Менеджер терминалов </summary>
		protected TerminalManager terminalManager;
		/// <summary> Интерфейс менеджера терминалов </summary>
		/// <remarks> Через него персональные серверы сессий могут пользоваться услугами менеджера терминалов </remarks>
		public ITerminalManager TerminalManager
		{
			get
			{
				return terminalManager;
			}
		}

		#region Start & Stop

		protected override void Start()
		{
			#region PerformanceCounters

			Counters.Instance.ResetCounter(CounterGroup.Count,          Counters.Instances.Tcp);
			Counters.Instance.ResetCounter(CounterGroup.Count,          Counters.Instances.Http);
			Counters.Instance.ResetCounter(CounterGroup.Count,          Counters.Instances.SaveQueue);
			Counters.Instance.ResetCounter(CounterGroup.Count,          Counters.Instances.ReceiveQueue);
			Counters.Instance.ResetCounter(CounterGroup.Count,          Counters.Instances.TcpConnections);

			Counters.Instance.ResetCounter(CounterGroup.CountPerSecond, Counters.Instances.Tcp);
			Counters.Instance.ResetCounter(CounterGroup.CountPerSecond, Counters.Instances.Http);
			Counters.Instance.ResetCounter(CounterGroup.CountPerSecond, Counters.Instances.SaveQueue);
			Counters.Instance.ResetCounter(CounterGroup.CountPerSecond, Counters.Instances.ReceiveQueue);
			Counters.Instance.ResetCounter(CounterGroup.CountPerSecond, Counters.Instances.TcpConnections);

			#endregion

			terminalManager = new TerminalManager();
			foreach (var storage in Globals.TssDatabaseManager.GetStorages())
				terminalManager.AddSavingMobileQueue((int)storage);

			CreateMainTerminal("TerminalMovireg");
			CreateMainTerminal("Terminal");
			CreateMainTerminal("TerminalTCP");
			CreateMainTerminal("TerminalMLP");
			CreateMainTerminal("TerminalMSMS");
			CreateMainTerminal("TerminalEmulator");
			CreateMainTerminal("GeneratingEmulator");
			CreateMainTerminal("TerminalHttp");
			CreateMainTerminal("TerminalFtp");
			CreateMainTerminal("TerminalLocator");
			CreateMainTerminal("TerminalOrder285");

			terminalManager.ReceiveEvent += tmManager_ReceiveEvent;
			terminalManager.NotifyEvent  += tmManager_NotifyEvent;
		}
		protected override void Stop()
		{
			terminalManager?.Dispose();
			terminalManager = null;
		}

		#endregion Start & Stop

		private void CreateMainTerminal(string configKey)
		{
			var configValue = GlobalsConfig.AppSettings[configKey];
			if (configValue == null)
				return;
			var terminalNames = configValue.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

			foreach (var terminalName in terminalNames)
				terminalManager.CreateMainTerminal(terminalName);
		}

		/// <summary> Событие наличия данных у терминала </summary>
		public event AnonymousEventHandler<ReceiveEventArgs>    ReceiveEvent;
		/// <summary> Событие наличия сообщений у терминала </summary>
		public event AnonymousEventHandler<BulkNotifyEventArgs> NotifyEvent;

		#region Handle tmManager events

		void tmManager_ReceiveEvent(ReceiveEventArgs args)
		{
			if (ReceiveEvent == null)
				return;

			var ie = ReceiveEvent.GetInvocationList().GetEnumerator();

			while (ie.MoveNext())
			{
				var handler = (AnonymousEventHandler<ReceiveEventArgs>)ie.Current;
				try
				{
					handler.Invoke(args);
				}
				catch (RemotingException)
				{
					Trace.TraceWarning("Unsubscribing {0}.ReceiveEvent because of remoting error", GetType().Name);
					try
					{
						ReceiveEvent -= handler;
					}
					catch (Exception ex)
					{
						Trace.TraceError("{0}.ReceiveEvent - unable to unsubscribe: {1}", GetType().Name, ex);
					}
				}
				catch (ThreadAbortException)
				{
					Trace.TraceInformation("Unsubscribing {0}.ReceiveEvent because of thread abort", GetType().Name);
					try
					{
						ReceiveEvent -= handler;
					}
					catch (Exception ex)
					{
						Trace.TraceError("{0}.ReceiveEvent - unable to unsubscribe: {1}", GetType().Name, ex);
					}
				}
				catch (SocketException)
				{
					Trace.TraceWarning("Unsubscribing {0}.ReceiveEvent because of socket error", GetType().Name);
					try
					{
						ReceiveEvent -= handler;
					}
					catch (Exception ex)
					{
						Trace.TraceError("{0}.ReceiveEvent - unable to unsubscribe: {1}", GetType().Name, ex);
					}
				}
			}
		}

		void tmManager_NotifyEvent(BulkNotifyEventArgs args)
		{
			if (NotifyEvent == null)
				return;

			var ie = NotifyEvent.GetInvocationList().GetEnumerator();

			while (ie.MoveNext())
			{
				var handler = (AnonymousEventHandler<BulkNotifyEventArgs>)ie.Current;
				try
				{
					handler.Invoke(args);
				}
				catch (RemotingException)
				{
					Trace.TraceWarning("Unsubscribing {0}.NotifyEvent because of remoting error", GetType().Name);
					try
					{
						NotifyEvent -= handler;
					}
					catch (Exception ex)
					{
						Trace.TraceError(GetType().Name + ".NotifyEvent - unable to unsubscribe: " + ex);
					}
				}
				catch (ThreadAbortException)
				{
					Trace.TraceInformation("Unsubscribing {0}.NotifyEvent because of thread abort", GetType().Name);
					try
					{
						NotifyEvent -= handler;
					}
					catch (Exception ex)
					{
						Trace.TraceError(GetType().Name + ".NotifyEvent - unable to unsubscribe: " + ex);
					}
				}
				catch (SocketException)
				{
					Trace.TraceWarning("Unsubscribing {0}.NotifyEvent because of socket error", GetType().Name);
					try
					{
						NotifyEvent -= handler;
					}
					catch (Exception ex)
					{
						Trace.TraceError(GetType().Name + ".NotifyEvent - unable to unsubscribe: " + ex);
					}
				}
			}
		}

		#endregion Handle tmManager events

		#region Commands & SMS

		public void SendCommand(IStdCommand command)
		{
			terminalManager?.SendCommand(command);
		}

		public void SendSMS(string target, string text)
		{
			terminalManager?.SendSMS(target, text);
		}

		public void SendSMS(string target, byte[] data)
		{
			terminalManager?.SendSMS(target, data);
		}

		#endregion Commands & SMS
	}
}