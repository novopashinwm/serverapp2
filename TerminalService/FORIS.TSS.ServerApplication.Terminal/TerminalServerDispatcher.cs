﻿using System.ComponentModel;

using FORIS.TSS.Common;

namespace FORIS.TSS.ServerApplication.Terminal
{
	public class TerminalServerDispatcher : ServerDispatcher<TerminalServer>, ITerminalServerItem
	{
		#region Constructor &

		public TerminalServerDispatcher(IContainer container)
		{
			container.Add(this);
		}
		public TerminalServerDispatcher()
		{
		}

		#endregion Constructor &

		#region Ambassadors

		private readonly TerminalServerAmbassadorCollection ambassadors = new TerminalServerAmbassadorCollection();
		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public new TerminalServerAmbassadorCollection Ambassadors
		{
			get { return this.ambassadors; }
		}
		protected override AmbassadorCollection<IServerItem<TerminalServer>> GetAmbassadorCollection()
		{
			return this.ambassadors;
		}

		#endregion Ambassadors
	}
	public class TerminalServerAmbassadorCollection : ServerAmbassadorCollection<TerminalServer>
	{
		public new TerminalServerAmbassador this[int index]
		{
			get { return (TerminalServerAmbassador)base[index]; }
		}
	}
	public class TerminalServerAmbassador : ServerAmbassador<TerminalServer>
	{
	}
	public interface ITerminalServerItem : IServerItem<TerminalServer>
	{
	}
}