using System.ComponentModel;
using System.Data;

using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Terminal.Schema;

namespace FORIS.TSS.ServerApplication.Terminal
{
	public class TerminalServerDatabaseSchema : Component, IDatabaseSchema
	{

		#region IDatabaseSchema Members

		public void MakeSchema(DataSet dataSet)
		{
			DatabaseSchema.MakeSchema(dataSet);
		}

		#endregion IDatabaseSchema Members
	}
}