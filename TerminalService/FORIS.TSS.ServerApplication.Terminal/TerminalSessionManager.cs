﻿namespace FORIS.TSS.ServerApplication.Terminal
{
	public class TerminalSessionManager : SessionManager<TerminalServer>
	{
		private readonly TerminalServerDispatcher _serverDispatcher = new TerminalServerDispatcher();
		protected override ServerDispatcher<TerminalServer> GetServerDispatcher()
		{
			return _serverDispatcher;
		}
		/// <summary> Метод создает объект сессии на сервере </summary>
		/// <returns> объект сессии - персональный сервер </returns>
		/// <remarks>
		/// Возможно следует передавать какие-нибудь параметры
		/// в этот метод, чтоб можно было создавать объекты
		/// сессий различных типов в зависимости от запроса клиента
		/// </remarks>
		protected override PersonalServerBase<TerminalServer> OnCreatePersonalServer()
		{
			return new TerminalPersonalServer();
		}
	}
}