﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Net.Sockets;
using System.Runtime.Remoting;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Data;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Security;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.BusinessLogic.Terminal;
using FORIS.TSS.Common;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.ServerApplication.Terminal
{
	public class TerminalPersonalServer : PersonalServerBase<TerminalServer>, ITerminalPersonalServer

	{
		#region Controls & Components

		private System.ComponentModel.IContainer components;

		#endregion Controls & Components

		#region Constructor & Dispose

		public TerminalPersonalServer()
		{
			InitializeComponent();
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		#endregion Constructor & Dispose

		#region Component Designer generated code

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}

		#endregion Component Designer generated code

		public override string ApplicationName
		{
			get { return "Терминал"; }
		}

		#region Security

		protected override bool CheckPassword(string password)
		{
			throw new NotImplementedException();
		}
		protected override SetNewPasswordResult SetPassword(string password, int idOperator)
		{
			throw new NotImplementedException();
		}
		protected override SetNewLoginResult SetNewLogin(string login)
		{
			throw new NotImplementedException();
		}

		#endregion Security

		public override void Init(TerminalServer server, SessionManager<TerminalServer> sessionManager, BusinessLogic.Interfaces.ISessionInfo sessionInfo)
		{
			base.Init(server, sessionManager, sessionInfo);

			server.ReceiveEvent += server_ReceiveEvent;
			server.NotifyEvent  += server_NotifyEvent;
		}
		/// <summary> Флаг разрешающий известить подписчиков о готовности данных </summary>
		private bool bCanRiseReadyEvent = true;
		/// <summary> Список для накопления поступающих позиций </summary>
		private readonly List<IMobilUnit> mobilUnits = new List<IMobilUnit>(100);
		/// <summary> Обработчик события получения данных от терминалов </summary>
		/// <param name="args"> Данные терминалов </param>
		void server_ReceiveEvent(ReceiveEventArgs args)
		{
			if (args.MobilUnits != null)
			{
				lock (mobilUnits)
				{
					mobilUnits.AddRange(args.MobilUnits);
				}
				OnDataReady();
			}
		}
		/// <summary> Обработчик события получения сообщений от терминалов </summary>
		/// <param name="args"></param>
		void server_NotifyEvent(BulkNotifyEventArgs args)
		{
			if (NotifyEvent == null) return;

			Delegate[] handlers = NotifyEvent.GetInvocationList();

			foreach (AnonymousEventHandler<BulkNotifyEventArgs> handler in handlers)
			{
				handler.BeginInvoke(args, NotifyEvent_Callback, handler);
			}
		}
		private void NotifyEvent_Callback(IAsyncResult asyncResult)
		{
			var handler =
				(AnonymousEventHandler<BulkNotifyEventArgs>)asyncResult.AsyncState;

			try
			{
				handler.EndInvoke(asyncResult);
			}
			catch (RemotingException)
			{
				Trace.TraceWarning("{0}.NotifyEvent_Callback failed, handler will be unsubscribed", GetType().Name);
				NotifyEvent -= handler;
			}
			catch (SocketException)
			{
				Trace.TraceWarning("{0}.NotifyEvent_Callback failed, handler will be unsubscribed", GetType().Name);
				NotifyEvent -= handler;
			}
			catch (Exception e)
			{
				Trace.TraceError("{0}", e);
				NotifyEvent -= handler;
			}
		}

		#region ITerminalPersonalServer Members

		#region Receive event

		private readonly object                       dataReadyLockObject = new object();
		private      AnonymousEventHandler<EventArgs> dataReady;
		public event AnonymousEventHandler<EventArgs> DataReadyEvent
		{
			add    { dataReady -= value; dataReady += value; }
			remove { dataReady -= value; }
		}

		protected virtual void OnDataReady()
		{
			lock (dataReadyLockObject)
			{
				if (!bCanRiseReadyEvent) return;

				bCanRiseReadyEvent = false;
			}

			if (dataReady != null)
			{
				Delegate[] Handlers = dataReady.GetInvocationList();

				foreach (AnonymousEventHandler<EventArgs> handler in Handlers)
				{
					handler.BeginInvoke(null, DataReadyEvent_Callback, handler);
				}
			}
		}

		private void DataReadyEvent_Callback(IAsyncResult asyncResult)
		{
			AnonymousEventHandler<EventArgs> Handler =
				(AnonymousEventHandler<EventArgs>)asyncResult.AsyncState;

			try
			{
				Handler.EndInvoke(asyncResult);
			}
			catch (Exception ex)
			{
				Trace.TraceError(GetType().Name + ".DataReadyEvent_Callback error: {0}\n{1}", ex.Message, ex.StackTrace);
				dataReady -= Handler;
			}
		}

		#endregion // DataReady event

		public event AnonymousEventHandler<BulkNotifyEventArgs> NotifyEvent;

		public void SendSMS(string target, string text)
		{
			Debug.Assert(serverInstance != null, "SendSMS. Server is null");
			serverInstance.SendSMS(target, text);
		}
		public void SendSMS(string target, byte[] data)
		{
			Debug.Assert(serverInstance != null, "SendSMS. Server is null");
			serverInstance.SendSMS(target, data);
		}
		public void SendCommand(IStdCommand command)
		{
			Debug.Assert(serverInstance != null, "SendCommand. Server is null");
			serverInstance.SendCommand(command);
		}
		public IMobilUnit[] GetPositions()
		{
			IMobilUnit[] result;

			lock (mobilUnits)
			{
				result = new IMobilUnit[mobilUnits.Count];

				mobilUnits.CopyTo(result, 0);
				mobilUnits.Clear();

				lock (dataReadyLockObject)
				{
					bCanRiseReadyEvent = true;
				}
			}

			return result;
		}
		public ITerminalManager GetTerminalManager()
		{
			return serverInstance.TerminalManager;
		}

		/// <summary> Возвращает отсортированный по времени список последних позиций ТС из БД за указанный период времени </summary>
		/// <param name="begTime"> Начало периода. UTC </param>
		/// <param name="endTime"> Конец периода. UTC </param>
		/// <param name="controllerId"> Идентификатор контроллера </param>
		/// <param name="interval"> Интервал между позициями. Секунды </param>
		/// <param name="maxCount"> Максимальное количество возвращаемых позиций </param>
		/// <returns> Отсортированный по времени список последних позиций </returns>
		public SortedList<int, IMobilUnit> GetLogFromDB(DateTime begTime, DateTime endTime, int controllerId, int interval, int maxCount)
		{
			#region Preconditions

			if (begTime.Kind != DateTimeKind.Utc)
				throw new ArgumentException("Time kind must be UTC", nameof(begTime));

			if (endTime.Kind != DateTimeKind.Utc)
				throw new ArgumentException("Time kind must be UTC", nameof(endTime));

			#endregion // Preconditions

			return serverInstance.TerminalManager.GetLogFromDB(
				TimeHelper.GetSecondsFromBase(begTime),
				TimeHelper.GetSecondsFromBase(endTime),
				controllerId,
				interval,
				maxCount);
		}

		protected override void OnClosing()
		{
			base.OnClosing();

			serverInstance.ReceiveEvent -= server_ReceiveEvent;
			serverInstance.NotifyEvent  -= server_NotifyEvent;
		}

		#endregion

		public override TssPrincipalInfo GetPrincipal()
		{
			return new TssPrincipalInfo(
				SessionInfo.OperatorInfo.Login,
				"OPERATOR",
				SessionInfo.OperatorInfo.OperatorId,
				TssPrincipalType.Operator);
		}
	}
}