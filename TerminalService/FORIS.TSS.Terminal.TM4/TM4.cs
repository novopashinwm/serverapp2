﻿using System.Net;
using FORIS.TSS.TerminalService.Interfaces;
using System.Collections;
using FORIS.TSS.Terminal.Messaging;

namespace FORIS.TSS.Terminal.TM4
{
    public class TM4 : Device
    {
        public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
        {
            bufferRest = null;
            TM4Datagram dg = new TM4Datagram(data);
            TM4ListenerEventArgs tm4Args = new TM4ListenerEventArgs(dg,
                (new IPEndPoint(IPAddress.Any, IPEndPoint.MinPort)), dg.CorrectGPS);

            ArrayList aRes = new ArrayList(1);

            IMobilUnit unit = tm4Args.GetMobilUnits()[0];

            IMessage msg = MessageCreator.CreatePosition(unit, unit.ValidPosition);
            aRes.Add(msg);

            return aRes;
        }

        public override bool SupportData(byte[] data)
        {
            return data.Length == 43 && data[0] == 7;
        }
    }
}
