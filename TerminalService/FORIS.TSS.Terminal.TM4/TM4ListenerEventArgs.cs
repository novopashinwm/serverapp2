﻿using System;
using System.Net;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.Communication;
using FORIS.TSS.Terminal.UDP;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.TM4
{
    public class TM4ListenerEventArgs : UDPTransmitterEventArgs
    {
        public TM4ListenerEventArgs(Datagram datagram, IPEndPoint ip, bool bIsCorrect)
            : base(datagram, ip, bIsCorrect)
        {
        }

        public override IMobilUnit[] GetMobilUnits()
        {
            TM4Datagram dg = datagram as TM4Datagram;

            if (dg == null)
                throw new Exception("Wrong type of a datagram");

            var mu = new MobilUnit.Unit.MobilUnit
            {
                ID = dg.ID,
                IP = ip,
                Time = dg.Time,
                Latitude = dg.Latitude,
                Longitude = dg.Longitude,
                Speed = dg.Speed,
                Course = (int) dg.Course
            };
            mu.Properties.Add(DeviceProperty.Protocol, ControllerType.Names.TM4);
            /*
            mu.PowerVoltage = dg.PowerVoltage;
            mu.VoltageAN1 = dg.VoltageAN1;
            mu.VoltageAN2 = dg.VoltageAN2;
            mu.VoltageAN3 = dg.VoltageAN3;
            mu.VoltageAN4 = dg.VoltageAN4;
            mu.Firmware = dg.FirmWare;
            */
            mu.Satellites = 10; // dg.Satellite;
            /*
            if ((dg.Spare2 & 0x2) != 0)
                mu.DigitalSensors &= ~0x20;
            else
                mu.DigitalSensors |= 0x20;
            */
            return new[] { mu };
        }
    }
}