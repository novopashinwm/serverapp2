﻿using System;
using System.Collections;
using System.Diagnostics;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Communication;

namespace FORIS.TSS.Terminal.TM4
{
	public class TM4Datagram: Datagram
	{
		//TODO: Аналоговые датчики
		//TODO: Тревога

		#region переменные класса
		//byte[] bytes;
		#endregion переменные класса

		#region Свойства
		protected int _id;
		public int ID
		{
			get
			{
				return _id;
			}
			protected set
			{
				_id = value;
			}
		}
		
		protected int _time;
		public int Time
		{
			get
			{
				return _time;
			}
			protected set
			{
				_time = value;
			}
		}

		protected double _latitude;
		public double Latitude
		{
			get
			{
				return _latitude;
			}
			protected set
			{
				_latitude = value;
			}
		}

		protected double _longitude;
		public double Longitude
		{
			get
			{
				return _longitude;
			}
			protected set
			{
				_longitude = value;
			}
		}

		protected double _course;
		public double Course
		{
			get
			{
				return _course;
			}
			protected set
			{
				_course = value;
			}
		}

		protected int _speed;
		public int Speed
		{
			get
			{
				return _speed;
			}
			protected set
			{
				_speed = value;
			}
		}

		protected bool _correctGPS;
		public bool CorrectGPS
		{
			get
			{
				return _correctGPS;
			}
			protected set
			{
				_correctGPS = value;
			}
		}

		protected bool _alarm;
		public bool Alarm
		{
			get
			{
				return _alarm;
			}
			protected set
			{
				_alarm = value;
			}
		}

		protected bool _isDigitalSensors;
		public bool IsDigitalSensors
		{
			get
			{
				return _isDigitalSensors;
			}
			protected set
			{
				_isDigitalSensors = value;
			}
		}

		#endregion Свойства


		#region .ctor
		/// <summary>
		/// Датаграмма UDP
		/// </summary>
		public TM4Datagram() : this(1)
		{
		}

		/// <summary>
		/// Датаграмма UDP
		/// </summary>
		/// <param name="bytes">массив байт</param>
		public TM4Datagram(byte[] bytes)
			: this((uint)bytes.Length)
		{
			bytes.CopyTo(this.bytes, 0);
			ID = (int)GetUInt32(1);
			CorrectGPS = GetBitValue(20, 0);
			Alarm = GetBitValue(20, 1);
			//IsDigitalSensors = (GetBitValue(20, 2)==false) && (GetBitValue(20, 3)==false);
			//секунд от 01.01.2000 (из расчета, что в каждом месяце 31 день), а нам надо от 1970 года
			double sec = (double)GetUInt32(5);
			DateTime tempTime = new DateTime(2000 + (int)Math.Floor(sec / 60 / 60 / 24 / 31 / 12), 1 + (int)Math.Floor((sec / 60 / 60 / 24 / 31) % 12), 1 + (int)Math.Floor((sec / 60 / 60 / 24) % 31), (int)Math.Floor((sec / 60 / 60) % 24), (int)Math.Floor((sec / 60) % 60), (int)Math.Floor(sec % 60));
			Time = (int)(tempTime - TimeHelper.year1970).TotalSeconds;
			Latitude = GetUInt32(9) * Math.Pow(10, -8) * TSS.MobilUnit.Unit.MobilUnit.RadiansToDegrees;
			Longitude = GetUInt32(13) * Math.Pow(10, -8) * TSS.MobilUnit.Unit.MobilUnit.RadiansToDegrees;
			Speed = (int)(GetUInt16(21) / 100);
			Course = GetUInt16(23) * Math.Pow(10, -3) * TSS.MobilUnit.Unit.MobilUnit.RadiansToDegrees;
		}
		
		/// <summary>
		/// Датаграмма UDP
		/// </summary>
		/// <param name="capacity">размер датаграммы</param>
		public TM4Datagram(uint capacity)
			: base(capacity)
		{
		}
		#endregion // .ctor

		#region overrides of Datagram
		public override bool CheckCRC()
		{
			//TODO: Implement
			//return true;
			
			try
			{
				ushort crc = (ushort)IntFromBytesLE(bytes[41], bytes[42]);
				ushort crc2 = CRC_16_12_5_0(1, 40);
				if (crc != crc2)
				{
					Trace.WriteLine(String.Format(
										"TM4OutputDatagram error: crc={0} crc2={1} id={2}", crc, crc2, ID));
					return false;
				}
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public override bool Valid
		{
			get
			{
				return CorrectGPS;
				/*
				return SignalQuality != MJSignalQuality.NotDetermined && CheckCRC() &&
					IntFromBytesLE(bytes[12], bytes[13], bytes[14]) != 0x800000;
				 */ 
			}
		}

		public override ushort CRC16
		{
			get { 
				return (ushort)IntFromBytesLE(bytes[41], bytes[42]); 
			}
			set { 
				// IntToBytes(value, bytes, 41, 2); 
			}
		}
		#endregion overrides of Datagram


		protected UInt32 GetUInt32(int startIndex)
		{
			return BitConverter.ToUInt32(bytes, startIndex);
		}

		protected UInt16 GetUInt16(int startIndex)
		{
			return BitConverter.ToUInt16(bytes, startIndex);
		}

		protected bool GetBitValue(int byteIndex, int bitIndex)
		{
			bool res = new BitArray(new byte[1] { bytes[byteIndex] }).Get(bitIndex);
			return res;
		}
	}
}