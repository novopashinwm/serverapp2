﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace RU.NVG.Terminal.WatchDA690
{
	public class WatchDA690Device : Device
	{
		private static readonly byte[] LogonPrefix     = Encoding.ASCII.GetBytes("$$," + ImeiPrefix);
		private const string           ImeiPrefix      = "simei:";
		private static readonly byte[] ImeiPrefixBytes = Encoding.ASCII.GetBytes(ImeiPrefix);
		internal const string ProtocolName = "GlobalSat DA-690";
		internal static class DeviceNames
		{
			internal const string WatchDA690 = "GlobalSat DA-690";
		}
		public WatchDA690Device() : base(typeof(WatchDA690Sensors), new[]
		{
			DeviceNames.WatchDA690,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			if (data.Length < LogonPrefix.Length)
				return false;

			if (data.Take(LogonPrefix.Length).SequenceEqual(LogonPrefix))
				return true;

			if (!data.Take(ImeiPrefixBytes.Length).SequenceEqual(ImeiPrefixBytes))
				return false;

			if (data.Length - ImeiPrefixBytes.Length < 15)
				return false;

			return data.TakeAfter(ImeiPrefixBytes.Length - 1).Take(15).All(c => '0' <= c && c <= '9');
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			var semicolonLastIndex = Array.LastIndexOf(data, (byte)';', count - 1, count);
			if (semicolonLastIndex != count - 1)
			{
				bufferRest = new byte[count - semicolonLastIndex - 1];
				Array.Copy(data, semicolonLastIndex + 1, bufferRest, 0, bufferRest.Length);
			}
			else
			{
				bufferRest = null;
			}

			var inputString = Encoding.ASCII.GetString(data, 0, count);
			var parts = inputString.Split(';');
			var result = new ArrayList(parts.Length);

			foreach (var part in parts)
			{
				var commaCount = part.Count(c => c == ',');
				if (commaCount == 20)
				{
					MobilUnit mu;
					string answer;
					if (TryParseData(part, out mu, out answer))
					{
						if (!string.IsNullOrWhiteSpace(answer))
							result.Add(new ConfirmPacket(Encoding.ASCII.GetBytes(answer)));
						result.Add(mu);
					}
				}
				else if (commaCount == 2)
				{
					ConfirmPacket confirmPacket;
					if (TryParsePingPacket(part, out confirmPacket))
						result.Add(confirmPacket);
				}
			}

			if (stateData == null)
			{
				var mu = (IMobilUnit)result.Cast<object>().FirstOrDefault(o => o is IMobilUnit);

				if (mu != null && mu.DeviceID != null)
				{
					var imei = mu.DeviceID;
					//TODO: брать периодичность мониторинга из настроек терминала
					result.Add(new ConfirmPacket(Encoding.ASCII.GetBytes("@@,simei:" + imei + ",D,30s;")));
				}
				result.Add(new ReceiverStoreToStateMessage { StateData = new object() });
			}

			return result;
		}
		public override IEnumerable<ITerminal> CreateTerminal(CmdType type)
		{
			switch (type)
			{
				case CmdType.SendText:
					return Manager.GetLiveTerminals(Media.TCP);
				default:
					return base.CreateTerminal(type);

			}
		}
		public override byte[] GetCmd(IStdCommand cmd)
		{
			switch (cmd.Type)
			{
				case CmdType.SendText:
					return Encoding.ASCII.GetBytes(cmd.Text);
				default:
					return base.GetCmd(cmd);
			}
		}
		private bool TryParsePingPacket(string part, out ConfirmPacket confirmPacket)
		{
			var reader = new SplittedStringReader(part, ',');
			confirmPacket = null;
			if (reader.ReadString() != "$$")
				return false;
			if (!reader.ReadString().StartsWith(ImeiPrefix))
				return false;
			if (reader.ReadString() != "A")
				return false;
			confirmPacket = new ConfirmPacket(Encoding.ASCII.GetBytes("LOAD"));
			return true;
		}
		private bool TryParseData(string part, out MobilUnit mu, out string answer)
		{
			answer = null;
			var reader = new SplittedStringReader(part, ',');
			mu = UnitReceived(ProtocolName);
			mu.CorrectGPS = false;
			mu.SensorValues = new Dictionary<int, long>();
			var imeiField = reader.ReadString();
			if (imeiField.Length < ImeiPrefix.Length)
				return false;
			var imei = imeiField.Substring(ImeiPrefix.Length);
			mu.DeviceID = imei;
			reader.Skip(2);
			var command = reader.ReadString();
			bool sosAlarm = false;
			switch (command)
			{
				case "SMS":
					//TODO: сделать запрос к серверу геокодирования и вернуть информацию о местоположении, например, адрес + геозоны + ближайшую точку + ближайших друзей
					answer = Encoding.BigEndianUnicode.GetBytes("Вы в Москве!").ToHexString();
						//"0051000D91" + "##" + "35" //"0008008C05000304030177ED4FE167E58BE2003B0047005000536B635E38003B65F695F4003A00335206949F524D003B897F535700330032002E00317C73002C4E2D56FD5E7F4E1C77016DF157335E02798F7530533A534E5F3A53178DEF003300300033003453F7002090AE653F7F167801003A0020003500310038003000320038003B901F5EA6003A97596B62003B"
						//+ "$$" + 
						//"0051000D91" + "##" + "45" //"0008008C050003040302002800320032002E003500340037003900300030002C003100310034002E003000380037003300340035002900470053004D8F8552A95B9A4F4D003A4E1C531700320035002E00317C73002C4E2D56FD5E7F4E1C77016DF157335E02798F7530533A534E53D153178DEF0039003753F7002090AE653F7F167801003A00200035003100380030"
						//+ "$$" + 
						//"0051000D91" + "##" + "65"; //"0008003C05000304030300320038003B65F695F4003A5F53524D003B0028003400360030002C00300030002C0032003700420033002C00300045003900440029";
					break;
				case "weather":
					answer =

						Encoding.BigEndianUnicode.GetBytes(
							//NOTE: обязательный заголовок, без него погода не отображается
							@"当前天气："
							//NOTE: данные в двух следующих строках отображаются на одном экране; первая строка также отображается на главном экране
							+ @"Малооблачно, T：28℃,влажн.：66%,ветер：СВ：3 м/с" 
							+ @"|ПН,11-11℃,cn_heavyrain.gif,дождь1" 
							//NOTE: эти данные отображаются на следующих экранах
							+ @"|ВТ,12-12℃,chance_of_rain.gif,возможен дождь2" 
							+ @"|СР,13-13℃,chance_of_rain.gif,возможен дождь3" 
							//NOTE: следующая строка не отображается на экране, но без неё ошибка на часах
							+ @"|ЧТ,14-14℃,chance_of_rain.gif,возможен дождь4"
							).ToHexString();
					break;
				case "SOS":
					sosAlarm = true;
					answer = Encoding.BigEndianUnicode.GetBytes("Тревога в Москве по IMEI " + imei + "!").ToHexString();
					break;
			}
			mu.SensorValues.Add((int) WatchDA690Sensors.SOS, sosAlarm ? 1 : 0);

			var batteryCapacity = reader.ReadInt();
			mu.SensorValues.Add((int) WatchDA690Sensors.BatteryCapacity, batteryCapacity);
			var gsmSignalStrength = reader.ReadInt();

			var course = reader.ReadDouble();
			mu.Course = course != null ? (int)course.Value : (int?)null;
			var date = reader.ReadDate("yyMMddHHmmss");
			var validGps = reader.ReadString() == "A";
			//NOTE: при отсутствии GPS устройство присылает последнюю дату-время, поэтому нужно использовать время сервера
			mu.Time = TimeHelper.GetSecondsFromBase(validGps ? date : DateTime.UtcNow);
			mu.Latitude = ReadCoordinate(reader, "S") ?? 0;
			mu.Longitude = ReadCoordinate(reader, "W") ?? 0;
			var speedKnots = reader.ReadDouble();
			mu.Speed = speedKnots != null ? MeasureUnitHelper.KnotsToKMPH(speedKnots.Value) : (int?) null;
			mu.CorrectGPS = validGps;

			var cellNetwork = new CellNetworkRecord();
			cellNetwork.LogTime = mu.Time;
			cellNetwork.LAC = reader.ReadHexInt();
			cellNetwork.CellID = reader.ReadHexInt();
			cellNetwork.CountryCode = reader.ReadString();
			cellNetwork.NetworkCode = reader.ReadString().TrimStart('0');
			cellNetwork.SignalStrength = gsmSignalStrength;
			mu.CellNetworks = new[] {cellNetwork};

			var options = reader.ReadString();
			var optionsReader = new SplittedStringReader(options, 'x');
			optionsReader.Skip(4);
			var timeZoneOffSetHours = optionsReader.ReadInt();
			if (validGps)
				mu.Time -= timeZoneOffSetHours*3600;
			return true;
		}
		private static double? ReadCoordinate(SplittedStringReader reader, string negativeHemisphere)
		{
			var latString = reader.ReadString();
			var latHemisphere = reader.ReadString();
			decimal lat;
			if (!decimal.TryParse(latString, NumberStyles.Any, NumberHelper.FormatInfo, out lat)) 
				return null;

			var degrees = (int) (lat/100);
			var minutes = lat - degrees*100;
			var result = (double) (degrees + minutes/60m);
			if (latHemisphere == negativeHemisphere)
				return -result;
			return result;
		}

	}
}