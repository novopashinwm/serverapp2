﻿using System;

namespace RU.NVG.Terminal.WatchDA690
{
	[Serializable]
	public enum WatchDA690Sensors
	{
		BatteryCapacity = 1,
		SOS             = 2
	}
}