﻿using System.Collections;
using System.Text;
using FORIS.TSS.Terminal;

namespace RU.NVG.NIKA.Terminal.WP20DIMTS
{
    public class WP20DIMTSDevice : Device
    {
        public const string Signature = "ATL";

        public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
        {
            bufferRest = null;
            var dataString = Encoding.ASCII.GetString(data, 0, count);
            var reader = new WP20DIMTSReader(dataString);
            var unit = reader.ReadUnit();
            return new ArrayList {unit};
        }

        public override bool SupportData(byte[] data)
        {
            var parts = Encoding.ASCII.GetString(data).Split(',');
            return parts[0].Trim() == Signature &&
                (parts[1].Trim() == "L" || parts[1].Trim() == "H") &&
                (parts[3].Trim() == "T" || parts[3].Trim() == "A") &&
                (parts[5].Trim() == "A" || parts[5].Trim() == "V");
        }
    }
}
