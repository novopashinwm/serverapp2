﻿namespace RU.NVG.NIKA.Terminal.WP20DIMTS
{
    public enum WP20DIMTSSensor
    {
        Connected = 1,
        DigitalInput1 = 2,
        DigitalInput2 = 3,
        OpenStatus = 4,
        SimStatus = 5,
        BatteryStatus = 6,
        DigitalOutput1 = 7,
        DigitalOutput2 = 8,
        Fuel = 9,
        Tank = 10,
        InternalBattery = 11,
        Alert = 12,
        FuelVoltage = 13
    }
}
