﻿using System;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace RU.NVG.NIKA.Terminal.WP20DIMTS
{
    public class WP20DIMTSReader
    {
        private readonly SplittedStringReader _reader;

        public WP20DIMTSReader(string packetString)
        {
            _reader = new SplittedStringReader(packetString, ',');
        }

        public IMobilUnit ReadUnit()
        {
            _reader.Skip(1);
            var liveDataStatus = _reader.ReadString().Trim();
            var imei = _reader.ReadString().Trim();
            var alertDataStatus = _reader.ReadString().Trim();
            var firmware = _reader.ReadString().Trim();
            var gpsValidity = _reader.ReadString().Trim();
            var time = _reader.ReadString().Trim().Substring(0, 6);
            var date = _reader.ReadString().Trim().Substring(0, 6);
            var lat = ReadDegrees();
            var lahs = _reader.ReadString().Trim();
            var lon = ReadDegrees();
            var lohs = _reader.ReadString().Trim();
            var speed = _reader.ReadDouble();
            var cource = _reader.ReadDouble();
            var satellites = _reader.ReadNullableInt();
            var altitude = _reader.ReadDouble();
            var digitalInputs = _reader.ReadString().Trim();
            var fuelVoltage = _reader.ReadDouble();
            var fuel = _reader.ReadDouble();
            var tank = _reader.ReadDouble();
            var odometer = _reader.ReadDouble();
            var internalBattery = _reader.ReadDouble();
            var signalStrength = _reader.ReadNullableInt();
            var mcc = _reader.ReadString().Trim();
            var mnc = _reader.ReadString().Trim();
            var lac = _reader.ReadNullableInt();
            var cellId = _reader.ReadNullableInt();

            var logTime = ReadDateTime(date, time);

            var mobilUnit = new MobilUnit
            {
                DeviceID = imei,
                CorrectGPS = gpsValidity == "A",
                Time = logTime.HasValue ? TimeHelper.GetSecondsFromBase(logTime.Value) : 0,
                Latitude = (lahs == "N" ? 1 : -1)*lat ?? MobilUnit.InvalidLatitude,
                Longitude = (lohs == "E" ? 1 : -1)*lon ?? MobilUnit.InvalidLongitude,
                Speed = speed.HasValue ? (int?) Math.Ceiling(speed.Value) : null,
                Course = cource.HasValue ? (int?) Math.Ceiling(cource.Value) : null,
                Satellites = satellites ?? 0,
                Height = (int?) altitude ?? 0,
                Firmware = firmware,
                Run = (int) (odometer ?? 0),
                CellNetworks = new[]
                {
                    new CellNetworkRecord
                    {
                        CountryCode = mcc,
                        NetworkCode = mnc,
                        CellID = cellId ?? 0,
                        LAC = lac ?? 0,
                        Number = 0,
                        SignalStrength = signalStrength ?? 0,
                    }
                },
            };

            mobilUnit.Properties.Add(DeviceProperty.Protocol, ControllerType.Names.WP20DIMTS);

            if (fuel.HasValue)
                mobilUnit.SetSensorValue((int)WP20DIMTSSensor.Fuel, (int)Math.Ceiling(fuel.Value));
            if (tank.HasValue)
                mobilUnit.SetSensorValue((int)WP20DIMTSSensor.Tank, (int)Math.Ceiling(tank.Value));
            if (internalBattery.HasValue)
                mobilUnit.SetSensorValue((int)WP20DIMTSSensor.InternalBattery, (int)Math.Ceiling(internalBattery.Value));
            if(fuelVoltage.HasValue) 
                mobilUnit.SetSensorValue((int)WP20DIMTSSensor.FuelVoltage, (int) Math.Ceiling(fuelVoltage.Value));

            mobilUnit.SetSensorValue((int)WP20DIMTSSensor.Alert, alertDataStatus == "A" ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP20DIMTSSensor.Connected, digitalInputs[0] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP20DIMTSSensor.DigitalInput1, digitalInputs[1] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP20DIMTSSensor.DigitalInput2, digitalInputs[2] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP20DIMTSSensor.OpenStatus, digitalInputs[3] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP20DIMTSSensor.SimStatus, digitalInputs[4] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP20DIMTSSensor.BatteryStatus, digitalInputs[5] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP20DIMTSSensor.DigitalOutput1, digitalInputs[6] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP20DIMTSSensor.DigitalOutput2, digitalInputs[7] == '1' ? 1 : 0);

            return mobilUnit;
        }

        private double? ReadDegrees()
        {
            var nmea = _reader.ReadDouble();
            if (!nmea.HasValue)
                return null;

            var degrees = Math.Floor(nmea.Value / 100);
            var minutes = (nmea - degrees * 100) / 60;
            return degrees + minutes;
        }

        public DateTime? ReadDateTime(string dateString, string timeString)
        {
            var result = (DateTime?)null;
            int temp;

            if (!string.IsNullOrEmpty(dateString) && dateString.Length == 6)
            {
                var year = int.TryParse(dateString.Substring(4, 2), out temp) ? temp + 2000 : 0;
                var month = int.TryParse(dateString.Substring(2, 2), out temp) ? temp : 0;
                var day = int.TryParse(dateString.Substring(0, 2), out temp) ? temp : 0;
                if (year > 0 && month > 0)
                {
                    result = new DateTime(year, month, day);
                }
            }

            if (result.HasValue && !string.IsNullOrEmpty(timeString) && timeString.Length == 6)
            {
                var hours = int.TryParse(timeString.Substring(0, 2), out temp) ? temp : 0;
                var minutes = int.TryParse(timeString.Substring(2, 2), out temp) ? temp : 0;
                var seconds = int.TryParse(timeString.Substring(4, 2), out temp) ? temp : 0;
                if (hours > 0 && minutes > 0 && seconds > 0)
                {
                    return result.Value.Add(new TimeSpan(hours, minutes, seconds));
                }
            }

            return null;
        }
    }
}
