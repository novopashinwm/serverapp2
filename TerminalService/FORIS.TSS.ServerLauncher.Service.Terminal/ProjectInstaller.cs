﻿using System.ComponentModel;
using System.Configuration.Install;

namespace TerminalService
{
	[RunInstaller(true)]
	public partial class ProjectInstaller : Installer
	{
		public ProjectInstaller()
		{
			InitializeComponent();
		}
	}
}