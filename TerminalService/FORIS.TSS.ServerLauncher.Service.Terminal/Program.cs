﻿using System;
using System.ServiceProcess;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.ServerLauncher.Application;

namespace TerminalService
{
	static class Program
	{
		static void Main()
		{
			try
			{
				var servicesToRun = new ServiceBase[]
				{
					new TerminalService(),
				};
				if (Environment.UserInteractive)
					TrayIconApplicationContext.RunInteractive("Terminal Server", servicesToRun);
				else
					ServiceBase.Run(servicesToRun);
			}
			catch (Exception ex)
			{
				$"Application start"
					.WithException(ex, true)
					.TraceError();
			}
		}
	}
}