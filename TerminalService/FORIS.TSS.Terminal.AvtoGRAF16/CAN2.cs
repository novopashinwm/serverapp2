﻿namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public class CAN2 : Rec
	{
		public ushort FuelLevel1;
		public ushort FuelLevel2;
		public ushort FuelLevel3;
		public ushort FuelLevel4;
		public ushort FuelLevel5;
		public ushort FuelLevel6;

		public override string ToString()
		{
			return base.ToString() +
				   string.Format(", FuelLevel1={0},fl2={1}, fl3={2}, fl4={3}, fl5={4}, fl6={5}", FuelLevel1, FuelLevel2, FuelLevel3, FuelLevel4, FuelLevel5, FuelLevel6);
		}

		public CAN2(RecParts recParts, int recType)
			: base(recParts, recType)
		{
			Parse();
		}

		protected void Parse()
		{
			FuelLevel1 = ParseFuelLevel(_recParts.d2, 8);
			FuelLevel2 = ParseFuelLevel(_recParts.d2, 16);
			FuelLevel3 = ParseFuelLevel(_recParts.d2, 24);

			FuelLevel4 = ParseFuelLevel(_recParts.d3, 8);
			FuelLevel5 = ParseFuelLevel(_recParts.d3, 16);
			FuelLevel6 = ParseFuelLevel(_recParts.d3, 24);
		}

		protected ushort ParseFuelLevel(uint part, int startBit)
		{
			//все показания уровня топлива приходят как 0.4% на бит, а храним десятые доли процента
			ushort res = (ushort)(((part >> startBit) & 0xFF) * 4);
			if (res == 1020) { res = 0; }
			return res;
		}
	}
}