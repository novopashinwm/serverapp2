﻿namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public class CAN1 : Rec
	{
		public byte Speed;
		public bool CruiseControl;
		public bool Brake;
		public bool ParkingBrake;
		public bool Clutch;
		public byte Accelerator;
		public int? FuelRate;

		public override string ToString()
		{
			return base.ToString() +
				   string.Format(", Speed={0},CruiseControl={1}, Brake={2}, ParkingBrake={3}, Clutch={4}, Accelerator={5}, FuelRate={6} ", Speed, CruiseControl, Brake, ParkingBrake, Clutch, Accelerator, FuelRate);
		}

		public CAN1(RecParts recParts, int recType)
			: base(recParts, recType)
		{
			ParseSpeed();
			ParseBoolValues();
			ParseАccelerator();
			ParseFuelRate();
		}

		protected void ParseSpeed()
		{
			Speed = (byte)((_recParts.d2 >> 8) & 0xFF);
			if (Speed == 255)
			{
				Speed = 0;
			}
		}

		protected void ParseBoolValues()
		{
			CruiseControl = (_recParts.d2 & (1 << 17)) > 0;
			Brake = (_recParts.d2 & (1 << 19)) > 0;
			Clutch = (_recParts.d2 & (1 << 21)) > 0;
			ParkingBrake = (_recParts.d2 & (1 << 23)) > 0;
		}

		protected void ParseАccelerator()
		{
			//(0.4% хода педали /1)
			Accelerator = (byte)((_recParts.d2 >> 24) * 0.4);
			if (Accelerator > 100)
			{
				Accelerator = 0;
			}
		}

		protected void ParseFuelRate()
		{
			//показания приходят в (0.5 литров /1), а храним в десятых долях литра = X * 0.5 * 10 = X * 5
			var value = _recParts.d3 >> 8;
				
			FuelRate = (value == 0xffffff) ? (int?) null : (int)(value * 5);
		}
	}
}