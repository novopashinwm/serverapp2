﻿using System;

namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public class RecDigSensors1_2 : Rec
	{
		public RecDigSensors1_2(RecParts recParts, int recType)
			: base(recParts, recType)
		{
			ParseSensor1();
			ParseSensor2();
		}

		public override string ToString()
		{
			return base.ToString() + ", DSum1=" + Sensor1.ToString() + ", DSum2=" + Sensor2.ToString();
		}

		public Int32 Sensor1;
		public Int32 Sensor2;

		protected void ParseSensor1()
		{
			Sensor1 = (Int32)(_recParts.d2 >> 8);
		}

		protected void ParseSensor2()
		{
			Sensor2 = (Int32)(_recParts.d3 >> 8);
		}
	}
}