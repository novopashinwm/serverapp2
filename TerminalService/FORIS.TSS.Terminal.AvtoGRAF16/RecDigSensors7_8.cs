﻿namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public class RecDigSensors7_8 : RecDigSensors1_2
	{
		public RecDigSensors7_8(RecParts recParts, int recType)
			: base(recParts, recType)
		{
		}

		public override string ToString()
		{
			return base.ToString() + ", DSum7=" + Sensor1.ToString() + ", DSum8=" + Sensor2.ToString();
		}

	}
}