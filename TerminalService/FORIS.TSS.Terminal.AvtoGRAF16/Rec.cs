﻿using System;
using System.Text;

namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public class Rec
	{
		protected RecParts _recParts;

		public DateTime Time;
		public int RecType;
		public bool DigitalSensor1;
		public bool DigitalSensor2;
		public bool DigitalSensor3;
		public bool DigitalSensor4;
		public bool DigitalSensor5;
		public bool DigitalSensor6;
		public bool DigitalSensor7;
		public bool DigitalSensor8;



		protected void ParseTime()
		{
			int totalSec = (int)(_recParts.d1 & 0x3FFFFFFF);
			int res = totalSec;
			int year = (int)Math.Floor((double)(totalSec / 32140800)) + 2009;
			res = totalSec - ((year - 2009) * 32140800);
			int month = (int)Math.Floor((double)(res / 2678400)) + 1;
			res = res - ((month - 1) * 2678400);
			int day = (int)Math.Floor((double)(res / 86400)) + 1;
			res = res - ((day - 1) * 86400);
			Time = new DateTime(year, month, day).AddSeconds(res);
		}


		protected void ParseInputs()
		{
			byte inputs = _recParts.Bytes[12];
			DigitalSensor1 = (inputs & 1) > 0;
			DigitalSensor2 = (inputs & 2) > 0;
			DigitalSensor3 = (inputs & 4) > 0;
			DigitalSensor4 = (inputs & 8) > 0;
			DigitalSensor5 = (inputs & 16) > 0;
			DigitalSensor6 = (inputs & 32) > 0;
			DigitalSensor7 = (inputs & 64) > 0;
			DigitalSensor8 = (inputs & 128) > 0;
		}

		public override string ToString()
		{
			var sb = new StringBuilder(20);
			sb.Append("Дата:");
			sb.Append(Time);
			sb.Append(", Тип:");
			sb.Append(RecType);

			if (typeof(Rec) == GetType())
			{
				sb.Append(", RecParts.Bytes: ");
				sb.Append(BitConverter.ToString(_recParts.Bytes));
				sb.Append(", d1: ");
				sb.Append(_recParts.d1.ToString("x"));
				sb.Append(", d2: ");
				sb.Append(_recParts.d2.ToString("x"));
				sb.Append(", d3: ");
				sb.Append(_recParts.d3.ToString("x"));
			}

			return sb.ToString();
		}

		public Rec(RecParts recParts, int recType)
		{
			_recParts = recParts;
			RecType = recType;
			ParseTime();
			ParseInputs();
		}
	}
}