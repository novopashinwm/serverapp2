﻿using System;
using FORIS.TSS.MobilUnit.Unit;

namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public class TracePoint
	{
		public void FillDigitalSensors(Rec rec)
		{
			DigitalSensor1 = rec.DigitalSensor1;
			DigitalSensor2 = rec.DigitalSensor2;
			DigitalSensor3 = rec.DigitalSensor3;
			DigitalSensor4 = rec.DigitalSensor4;
			DigitalSensor5 = rec.DigitalSensor5;
			DigitalSensor6 = rec.DigitalSensor6;
			DigitalSensor7 = rec.DigitalSensor7;
			DigitalSensor8 = rec.DigitalSensor8;
		}

		public TracePoint(Rec rec)
		{
			Time = rec.Time;
			FillDigitalSensors(rec);
		}

		public DateTime Time;
		public double Lat;
		public double Lng;
		public SourceOfCoordinates? SourceOfCoordinates;
		public Int16 Speed;
		public Int16 Course;
		public byte Satellites;
		public double? HDOP;
		/// <summary>
		/// Высота в сантиметрах
		/// </summary>
		public int Height;
		public bool GPSCorrect;
		public bool DigitalSensor1;
		public bool DigitalSensor2;
		public bool DigitalSensor3;
		public bool DigitalSensor4;
		public bool DigitalSensor5;
		public bool DigitalSensor6;
		public bool DigitalSensor7;
		public bool DigitalSensor8;
		public int? DigitalSensor1Counter;
		public int? DigitalSensor2Counter;
		public int? DigitalSensor3Counter;
		public int? DigitalSensor4Counter;
		public int? DigitalSensor5Counter;
		public int? DigitalSensor6Counter;
		public int? DigitalSensor7Counter;
		public int? DigitalSensor8Counter;
		public int? AnalogSensor1 = int.MaxValue;
		public int? AnalogSensor2 = int.MaxValue;
		/// <summary>
		/// Напряжение питания, мВ
		/// </summary>
		public int? PowerVoltage;
		public CANInfo CAN;
		public int? LLS1;
		public int? LLS2;
		public int? LLS3;
		public int? LLS4;
		public int? LLS5;
		public int? LLS6;
		public int? LLS7;
		public int? LLS8;

		/// <summary>Данные с шины 1Wire</summary>
		public long? Wire1;
		/// <summary>Температура 1, 1/16 градуса на бит, сдвиг -55 градусов</summary>
		public int? Temperature1;
		/// <summary>Температура 2, 1/16 градуса на бит, сдвиг -55 градусов</summary>
		public int? Temperature2;
		/// <summary>Температура 3, 1/16 градуса на бит, сдвиг -55 градусов</summary>
		public int? Temperature3;
		/// <summary>Температура 4, 1/16 градуса на бит, сдвиг -55 градусов</summary>
		public int? Temperature4;
		/// <summary>Температура 5, 1/16 градуса на бит, сдвиг -55 градусов</summary>
		public int? Temperature5;
		/// <summary>Температура 6, 1/16 градуса на бит, сдвиг -55 градусов</summary>
		public int? Temperature6;
		/// <summary>Температура 7, 1/16 градуса на бит, сдвиг -55 градусов</summary>
		public int? Temperature7;
		/// <summary>Температура 8, 1/16 градуса на бит, сдвиг -55 градусов</summary>
		public int? Temperature8;
		/// <summary>Температура прибора, градусы</summary>
		public int? OwnTemperature;
	}
}