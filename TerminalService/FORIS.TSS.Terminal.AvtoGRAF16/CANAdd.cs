﻿namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	/// <summary> Вспомогательная запись CAN (47) </summary>
	public class CANAdd : Rec
	{
		/// <summary> Температура воздуха, °C </summary>
		public decimal? AirT { get; set; }

		public override string ToString()
		{
			return base.ToString() +
				   string.Format(", AirT={0}", AirT);
		}


		public CANAdd(RecParts recParts, int recType)
			: base(recParts, recType)
		{
			//0,03125 °C/бит, сдвиг -273°C
			var value = ((_recParts.d2 >> 8) & 0xFFFF);

			if (value != 0xFFFF)
				AirT = (value*0.03125m - 273m);
		}
	}
}