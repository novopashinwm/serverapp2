﻿using System;

namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public class RecMotion : Rec
	{
		public Int16 Speed;
		public Int16 Course;
		public byte Satellites;
		public byte HDOP;
		/// <summary>
		/// Высота в сантиметрах
		/// </summary>
		public int Height;

		/// <summary>
		/// 1 marine knot in km/h
		/// </summary>
		const double MarineKnot = 1.852;	// 50. / 27 = 1.(851)

		public RecMotion(RecParts recParts, int recType)
			: base(recParts, recType)
		{
			ParseSpeed();
			ParseCourse();
			ParseHeight();
			ParseSatellites();
			ParseHDOP();
		}

		public override string ToString()
		{
			return base.ToString() + string.Format(", Speed={0}, Course={1}, Height={2}, Satellites={3}, HDOP={4}", Speed, Course, Height, Satellites, HDOP);
		}

		protected void ParseSpeed()
		{
			Speed = (Int16)(((_recParts.d2 >> 8) & 0x3FFF) / 10 * MarineKnot);
			if (Speed < 3)
			{
				Speed = 0;
			}
		}

		protected void ParseCourse()
		{
			Course = (Int16)(_recParts.d2 >> 23);
		}

		protected void ParseHeight()
		{
			Height = (int)((_recParts.d3 >> 8) & 0x7FFF) * 100;
			Height += (int)((_recParts.d3 & 0xFF) * 0.4);
		}

		protected void ParseSatellites()
		{
			Satellites = (byte)((_recParts.d3 >> 23) & 0xF);
		}

		protected void ParseHDOP()
		{
			HDOP = (byte)(_recParts.d3 >> 27);
		}

	}
}