﻿namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	class Rec1Wire : Rec
	{
		public readonly long Value;

		public Rec1Wire(RecParts recParts, int recType) : base(recParts, recType)
		{
			long lower3Bytes = recParts.d2 << 8;
			long higher3Bytes = recParts.d3 << 8;
			Value = lower3Bytes | (higher3Bytes >> 24);
		}
	}
}