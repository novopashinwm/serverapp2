﻿using System.Text;

namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	/// <summary> Пакеты 36 и 37 - датчики температуры (подключаются через 1wire) </summary>
	public class Rec1WireTemperature : Rec
	{
		public Rec1WireTemperature(RecParts recParts, int recType)
			: base(recParts, recType)
		{
			T1 = (int)((_recParts.d2 & 0x000fff00) >> 8);
			T2 = (int)((_recParts.d2 & 0xfff00000) >> 20);
			T3 = (int)((_recParts.d3 & 0x000fff00) >> 8);
			T4 = (int)((_recParts.d3 & 0xfff00000) >> 20);
			OwnTemperature = ((int)(_recParts.d3 & 0x000000ff)) - 100;
		}

		/// <summary>Температура, 1/16 градуса на бит, сдвиг -55 градусов</summary>
		public readonly int T1;
		/// <summary>Температура, 1/16 градуса на бит, сдвиг -55 градусов</summary>
		public readonly int T2;
		/// <summary>Температура, 1/16 градуса на бит, сдвиг -55 градусов</summary>
		public readonly int T3;
		/// <summary>Температура, 1/16 градуса на бит, сдвиг -55 градусов</summary>
		public readonly int T4;

		/// <summary>Температура прибора, градусы</summary>
		public readonly int OwnTemperature;

		public override string ToString()
		{
			var sb = new StringBuilder(base.ToString());

			var @base = (RecTypes) RecType == RecTypes.Wire1T1_4 ? 0 : 4;

			const string prefix = ", 1Wire Temperature ";
			sb.Append(prefix);
			sb.Append(1 + @base);
			sb.Append(": ");
			sb.Append(T1);
			sb.Append(prefix);
			sb.Append(2 + @base);
			sb.Append(": ");
			sb.Append(T2);
			sb.Append(prefix);
			sb.Append(3 + @base);
			sb.Append(": ");
			sb.Append(T3);
			sb.Append(prefix);
			sb.Append(4 + @base);
			sb.Append(": ");
			sb.Append(T4);

			sb.Append(", OwnTemperature:");
			sb.Append(OwnTemperature);

			return sb.ToString();
		}
	}
}