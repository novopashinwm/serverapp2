﻿namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public enum RecTypes : int
	{
		RecPosition = 0,
		RecAnSensors1_2 = 1,
		RecDigSensors1_2 = 2,
		RecDigSensors3_4 = 3,
		RecMotion = 4,
		RecDigSensors5_6 = 5,
		Wire1 = 6,
		RecDigSensors7_8 = 7,
		LLS1_4 = 8,
		LLS5_8 = 9,
		CAN1 = 10,
		CAN2 = 11,
		CAN3 = 12,
		CAN4 = 13,
		CAN5 = 14,
		CAN6_start = 16,
		CAN6_end = 31,
		Wire1T1_4 = 36,
		Wire1T5_8 = 37,
		CANAdd = 47
	}
}