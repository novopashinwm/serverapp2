﻿namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public class RecDigSensors5_6 : RecDigSensors1_2
	{
		public RecDigSensors5_6(RecParts recParts, int recType)
			: base(recParts, recType)
		{
		}

		public override string ToString()
		{
			return base.ToString() + ", DSum5=" + Sensor1.ToString() + ", DSum6=" + Sensor2.ToString();
		}

	}
}