﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using FORIS.TSS.MobilUnit.Unit;

namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public class AvtoGRAF16Datagram
	{
		protected List<Rec> _records = new List<Rec>();
		protected List<TracePoint> _tracePoints = new List<TracePoint>();
		protected string _id;
		private string _password;
		protected byte[] _bytes;
		protected enum RecOrderType
		{
			undefined = 0,
			before = 1,
			after = 2
		}
		public List<Rec> Records
		{ get { return _records; } }
		public List<TracePoint> TracePoints
		{ get { return _tracePoints; } }
		public string ID
		{ get { return _id; } }
		public string Password { get { return _password; }}
		public byte PacketID = 0;
		public bool isValid
		{
			get
			{
				if (!(_bytes != null && _bytes.Length >= 19 && _bytes[0] == 0x24)) // && _bytes[9] == 0))
					return false;

				if (!CheckCRC())
					return false;

				return true;
			}
		}

		#region CRC

		private bool CheckCRC()
		{
			if (!CheckCRC(_bytes))
			{
				Trace.WriteLine(string.Format(this.GetType() + ".CheckCRC - wrong CRC in packet: \r\n {0}",
											  BitConverter.ToString(_bytes)
									));

				return false;
			}
			else
			{
				return true;
			}


		}
		public static bool CheckCRC(byte[] data)
		{
			if (data == null || data.Length < 9)
				return false;

			byte calcCRC = CRC_1wire(data, 0, data.Length - 1);
			byte packetCRC = data[data.Length - 1];
			bool res = calcCRC == packetCRC;
			if (!res)
			{
				string hexStr = BitConverter.ToString(data);
				if (hexStr.IndexOf("FF-FF-FF-FF-FF-FF-FF-FF-FF-FF-FF-FF-FF-FE-FF") >= 0)
				{
					//Для плохих пакетов в процессе скачки черного ящика пропускаем ошибки CRC
					return true;
				}
			}

			return res;
		}
		private static readonly byte[] Tabl_CRC = new byte[]
		{0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126, 32, 163, 253, 31, 65,
		157, 195, 33, 127, 252, 162, 64, 30, 95, 1, 227, 189, 62, 96, 130, 220,
		35, 125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3, 128, 222, 60, 98,
		190, 224, 2, 92, 223, 129, 99, 61, 124, 34, 192, 158, 29, 67, 161, 255,
		70, 24, 250, 164, 39, 121, 155, 197, 132, 218, 56, 102, 229, 187, 89, 7,
		219, 133, 103, 57, 186, 228, 6, 88, 25, 71, 165, 251, 120, 38, 196, 154,
		101, 59, 217, 135, 4, 90, 184, 230, 167, 249, 27, 69, 198, 152, 122, 36,
		248, 166, 68, 26, 153, 199, 37, 123, 58, 100, 134, 216, 91, 5, 231, 185,
		140, 210, 48, 110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113, 147, 205,
		17, 79, 173, 243, 112, 46, 204, 146, 211, 141, 111, 49, 178, 236, 14, 80,
		175, 241, 19, 77, 206, 144, 114, 44, 109, 51, 209, 143, 12, 82, 176, 238,
		50, 108, 142, 208, 83, 13, 239, 177, 240, 174, 76, 18, 145, 207, 45, 115,
		202, 148, 118, 40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 139,
		87, 9, 235, 181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170, 72, 22,
		233, 183, 85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246, 168,
		116, 42, 200, 150, 21, 75, 169, 247, 182, 232, 10, 84, 215, 137, 107, 53};

		//Контрольная сумма рассчитывается по алгоритму 1wire:
		private static byte CRC_1wire(byte[] bytes, int offset, int crcLen)
		{
			byte crc = 0;
			for (int i = offset; i < offset + crcLen; i++)
				crc = Tabl_CRC[crc ^ bytes[i]];
			return crc;
		}
		///Контрольная сумма каждой записи:
		/*				
		record[15] = CRC_1wire(record, 15);
		Следует учесть, что при расчете контрольной суммы каждой записи
		надо считать данные не переданными:
				record.Flags |= 0x00000100; // not sent to 1st server
				record.Flags |= 0x00000200; // not sent to 2nd server
		Это связано с организацией флеш-памяти прибора – при отмечании
		записей как отправленных нет возможности перезаписывать контрольную сумму

		Контрольная сумма передачи данных:
		send_data[N-1] = CRC_1wire(send_data, N-1);
		*/
		#endregion CRC

		public static Rec GetFirstRecord(byte[] bytes)
		{
			try
			{
				if (bytes == null || bytes.Length < 19 + 16)
					return null;

				byte[] recArr = new byte[16];
				Array.Copy(bytes, 19, recArr, 0, 16);
				if (!CheckCRC(recArr))
					return null;
				return new Rec(new RecParts(recArr), 0);
			}
			catch (Exception ex)
			{
				Trace.WriteLine("Error getting first record: " + BitConverter.ToString(bytes) + " " + ex);
				return null;
			}
		}
		public static int GetPacketLenght(byte[] bytes, int startIndex)
		{
			if (bytes == null || bytes.Length < startIndex + 5)
				return -1;


			return (int)BitConverter.ToUInt32(bytes, startIndex + 1);
		}
		public AvtoGRAF16Datagram(byte[] bytes, string firmwareVersion)
		{
			_bytes = bytes;

			if (!isValid)
			{
				return;
			}

			_id = BitConverter.ToInt32(bytes, 5).ToString();  //dataString.Split(new char[] { '@' })[0].Substring(2);
			_password = Encoding.ASCII.GetString(bytes, 5 + 4 + 2, 8);

			PacketID = bytes[10];

			_records = new List<Rec>();
			for (int i = 19; i <= bytes.Length - 17; i += 16)
			{
				byte[] recArr = new byte[16];
				Array.Copy(bytes, i, recArr, 0, 16);

				Rec rec = RecFactory.GetRec(recArr, firmwareVersion);
				if (rec != null)
				{
					_records.Add(rec);
				}
			}

			FillTracePoints(firmwareVersion);
		}
		protected bool isCANRecord(Rec rec)
		{
			return (rec.RecType >= (int)RecTypes.CAN1 && rec.RecType <= (int)RecTypes.CAN6_end && rec.RecType != 15 || rec.RecType == (int)RecTypes.CANAdd);
		}
		protected void FillTracePoints(string firmwareVersion)
		{
			_tracePoints = new List<TracePoint>();
			TracePoint point = null;
			foreach (Rec rec in _records)
			{
				//Если разница между записями больше 9 секунд - новая запись
				//Или если пришли новые координаты
				if (point == null
					|| Math.Abs((point.Time - rec.Time).TotalSeconds) > 9
					|| (point.Lat != 0 && point.Lng != 0 && rec.RecType == (int)RecTypes.RecPosition))
				{
					point = new TracePoint(rec);
					_tracePoints.Add(point);
				}

				point.FillDigitalSensors(rec);

				switch (rec.RecType)
				{
					case (int)RecTypes.RecPosition:
						var posRec = (RecPosition)rec;
						point.Lat = posRec.Lat;
						point.Lng = posRec.Lng;
						point.Time = posRec.Time;
						point.GPSCorrect = posRec.GPSCorrect;
						point.SourceOfCoordinates = posRec.SourceOfCoordinates;
						point.HDOP = posRec.HDOP;
						break;

					case (int)RecTypes.RecMotion:
						var motionRec = (RecMotion)rec;
						point.Speed = motionRec.Speed;
						point.Course = motionRec.Course;
						point.Satellites = motionRec.Satellites;
						point.Height = motionRec.Height;
						break;

					case (int)RecTypes.RecDigSensors1_2:
						var digSensors1_2 = (RecDigSensors1_2)rec;
						point.DigitalSensor1Counter = digSensors1_2.Sensor1;
						point.DigitalSensor2Counter = digSensors1_2.Sensor2;
						break;

					case (int)RecTypes.RecAnSensors1_2:
						var anSensors1_2 = (RecAnSensors1_2)rec;
						point.AnalogSensor1 = anSensors1_2.Sensor1;
						point.AnalogSensor2 = anSensors1_2.Sensor2;
						point.PowerVoltage = anSensors1_2.PowerVoltage * 17; //0.017В/бит, т.е. 17мВ/бит
						break;

					case (int)RecTypes.LLS1_4:
						var LLS1_4 = (LLS1_4)rec;
						point.LLS1 = LLS1_4.LLS1;
						point.LLS2 = LLS1_4.LLS2;
						point.LLS3 = LLS1_4.LLS3;
						point.LLS4 = LLS1_4.LLS4;
						break;

					case (int)RecTypes.LLS5_8:
						var LLS5_8 = (LLS5_8)rec;
						point.LLS5 = LLS5_8.LLS1;
						point.LLS6 = LLS5_8.LLS2;
						point.LLS7 = LLS5_8.LLS3;
						point.LLS8 = LLS5_8.LLS4;
						break;
					case (int)RecTypes.Wire1:
						point.Wire1 = ((Rec1Wire) rec).Value;
						break;
					case (int)RecTypes.Wire1T1_4:
						var rec1WireTemperature14 = (Rec1WireTemperature) rec;
						point.Temperature1 = rec1WireTemperature14.T1;
						point.Temperature2 = rec1WireTemperature14.T2;
						point.Temperature3 = rec1WireTemperature14.T3;
						point.Temperature4 = rec1WireTemperature14.T4;
						point.OwnTemperature = rec1WireTemperature14.OwnTemperature;
						break;
					case (int)RecTypes.Wire1T5_8:
						var rec1WireTemperature58 = (Rec1WireTemperature)rec;
						point.Temperature5 = rec1WireTemperature58.T1;
						point.Temperature6 = rec1WireTemperature58.T2;
						point.Temperature7 = rec1WireTemperature58.T3;
						point.Temperature8 = rec1WireTemperature58.T4;
						point.OwnTemperature = rec1WireTemperature58.OwnTemperature;
						break;
					default:
						if (isCANRecord(rec))
						{
							FillCANInfo(point, rec, firmwareVersion);
						}
						break;
				}
			}
		}
		protected void FillCANInfo(TracePoint point, Rec rec, string firmwareVersion)
		{
			if (point.CAN == null)
				point.CAN = new CANInfo();

			var res = point.CAN;

			if (isCANRecord(rec))
			{
				res.Time = TSS.BusinessLogic.TimeHelper.GetSecondsFromBase(rec.Time);

				switch (rec.RecType)
				{
					case (int)RecTypes.CAN1:
						var c = (CAN1)rec;
						res.Brake = c.Brake;
						res.Clutch = c.Clutch;
						res.CruiseControl = c.CruiseControl;
						res.FuelRate = c.FuelRate;
						res.ParkingBrake = c.ParkingBrake;
						res.Speed = c.Speed;
						res.Accelerator = c.Accelerator;
						break;
					case (int)RecTypes.CAN2:
						var c2 = (CAN2)rec;
						res.FuelLevel1 = c2.FuelLevel1;
						res.FuelLevel2 = c2.FuelLevel2;
						res.FuelLevel3 = c2.FuelLevel3;
						res.FuelLevel4 = c2.FuelLevel4;
						res.FuelLevel5 = c2.FuelLevel5;
						res.FuelLevel6 = c2.FuelLevel6;
						break;
					case (int)RecTypes.CAN3:
						var c3 = (CAN3)rec;
						res.EngHours = c3.EngHours;
						res.Revs = c3.Revs;
						res.RunToCarMaintenance = c3.RunToCarMaintenance;
						break;
					case (int)RecTypes.CAN4:
						var c4 = (CAN4)rec;
						res.CoolantT = c4.CoolantT;
						res.EngOilT = c4.EngOilT;
						res.FuelT = c4.FuelT;
						res.PressurizationAirT = c4.PressurizationAirT;
						break;
					case (int)RecTypes.CAN5:
						var c5 = (CAN5)rec;
						res.DayRun = c5.DayRun;
						res.TotalRun = c5.TotalRun;
						break;
					case (int)RecTypes.CANAdd:
						var canAdd = (CANAdd) rec;
						res.AirT = canAdd.AirT;
						break;
					default:
						if (rec.RecType >= (int)RecTypes.CAN6_start && rec.RecType <= (int)RecTypes.CAN6_end)
						{
							var c6 = (CAN6)rec;
							switch (c6.AxleNumber)
							{
								case 1:
									res.AxleLoad1 = c6.AxleLoad;
									break;
								case 2:
									res.AxleLoad2 = c6.AxleLoad;
									break;
								case 3:
									res.AxleLoad3 = c6.AxleLoad;
									break;
								case 4:
									res.AxleLoad4 = c6.AxleLoad;
									break;
								case 5:
									res.AxleLoad5 = c6.AxleLoad;
									break;
								case 6:
									res.AxleLoad6 = c6.AxleLoad;
									break;
							}
						}
						break;
				}
			}
		}
	}
}