﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Communication;
using FORIS.TSS.Helpers;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public class AvtoGRAF16 : Device
	{
		public AvtoGRAF16()
			: base(typeof(AvtoGrafSensors), new[] { ControllerType.Names.AvtoGraf, ControllerType.Names.AvtoGrafCan })
		{
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;
			// list of wrapped responses
			var alRes = new ArrayList();

			string firmwareVersion;
			var state = stateData as ParserState;
			if (state == null || data[0] == '@' && data[1] == '@')
			{
				var parserState = new ParserState();
				//Вероятно, первый пакет
				string deviceId;
				if (data[0] == '@' && data[1] == '@')
				{
					if (data[count - 1] != ';')
					{
						bufferRest = data;
						return alRes;
					}

					var numberEnd = GetConfDataNumberEndIndex(data);
					if (numberEnd == null)
					{
						Trace.TraceWarning("{0}: Invalid Avtograf configuration data: {1}", this, data);
						return null;
					}

					deviceId = Encoding.ASCII.GetString(data, 2, numberEnd.Value - 2);
					firmwareVersion = GetFirmwareVersion(data);
					if (firmwareVersion != null)
					{
						Trace.TraceInformation("{0}: firmware received from {1}: {2}", this, deviceId, firmwareVersion);

						var mu = UnitReceived();
						mu.DeviceID   = deviceId;
						mu.CorrectGPS = false;
						mu.Time       = TimeHelper.GetSecondsFromBase();
						mu.Properties.Add(DeviceProperty.Firmware, firmwareVersion);
						alRes.Add(mu);
						parserState.DeviceID        = mu.DeviceID;
						parserState.FirmwareVersion = firmwareVersion;
						state                       = parserState;
						alRes.Add(new ReceiverStoreToStateMessage { StateData = parserState });
					}
					alRes.Add(new ConfirmPacket(Encoding.ASCII.GetBytes("CONF ACCEPT\r\n")));
				}
				else
				{
					//С 5го по 8й байты - идентификатор устройства, little endian
					UInt32 serialNumber = 0;
					for (var i = 0; i != 4; ++i)
						serialNumber |= ((UInt32) data[i + 5] << (i*8));
					deviceId = serialNumber.ToString(CultureInfo.InvariantCulture);
				}

				if (state == null)
				{
					var unitInfo = UnitInfo.Factory.Create(UnitInfoPurpose.Firmware);
					unitInfo.DeviceID = deviceId;
					if (Manager != null && Manager.FillUnitInfo(unitInfo))
					{
						firmwareVersion             = unitInfo.FirmwareVersion;
						parserState.DeviceID        = unitInfo.DeviceID;
						parserState.FirmwareVersion = firmwareVersion;
					}
					else
					{
						firmwareVersion = string.Empty;
					}
					alRes.Add(new ReceiverStoreToStateMessage { StateData = parserState });
				}
				else
				{
					firmwareVersion = state.FirmwareVersion;
				}
			}
			else
			{
				firmwareVersion = state.FirmwareVersion;
			}

			if (!(data[0] == '@' && data[1] == '@'))
			{
				//Разбиваем данные на пакеты
				var packets = Datagram.SplitDataPackets(data, AvtoGRAF16Datagram.GetPacketLenght, out bufferRest);

				if (packets != null)
				{
					foreach (var packet in packets)
					{
						ProcessPacket(packet, alRes, firmwareVersion);
					}
				}
			}

			return alRes;
		}
		private readonly byte[] _versionPrefix = Encoding.ASCII.GetBytes(";VERSION=");
		private string GetFirmwareVersion(byte[] data)
		{
			var index = data.IndexOf(_versionPrefix);
			if (index == null)
				return null;
			var versionStartIndex = index.Value + _versionPrefix.Length;
			var versionEndIndex = Array.IndexOf(data, (byte) ';', versionStartIndex);
			if (versionEndIndex == -1)
				return null;
			return Encoding.ASCII.GetString(data, versionStartIndex, versionEndIndex - versionStartIndex);
		}
		public void ProcessPacket(byte[] packet, ArrayList alRes, string firmwareVersion)
		{
			var datagram = new AvtoGRAF16Datagram(packet, firmwareVersion);

			if (datagram.isValid)
			{
				if (datagram.TracePoints != null)
				{
					foreach (TracePoint point in datagram.TracePoints)
					{
						var mu = UnitReceived(ControllerType.Names.AvtoGraf);
						mu.cmdType    = CmdType.Trace;
						mu.DeviceID   = datagram.ID;
						mu.Latitude   = point.Lat;
						mu.Longitude  = point.Lng;
						mu.Speed      = point.Speed;
						mu.Course     = point.Course;
						mu.Time       = TimeHelper.GetSecondsFromBase(point.Time);
						mu.Satellites = point.Satellites;
						mu.CorrectGPS = point.GPSCorrect && point.Satellites >= MobilUnit.Unit.MobilUnit.MinSatellites;
						mu.Height     = point.GPSCorrect ? point.Height : (int?) null;
						mu.Properties.Add(DeviceProperty.Password, datagram.Password);

						//-----------Температура
						if (point.DigitalSensor2Counter > 10)
						{
							var T = (int) ((point.DigitalSensor2Counter/2) - 60);
							mu.VoltageAN1 = T;
						}

						//Аналоговый датчик топлива - подключаем к аналоговому входу 1
						//Поэтому пишем в VoltageAN4 - где для всех хранится топливо
						if (point.AnalogSensor1.HasValue && point.AnalogSensor1 < int.MaxValue)
							mu.VoltageAN4 = point.AnalogSensor1.Value;

						if (point.AnalogSensor2.HasValue && point.AnalogSensor2 < int.MaxValue)
							mu.VoltageAN2 = point.AnalogSensor2.Value;

						if (point.PowerVoltage.HasValue)
							mu.PowerVoltage = point.PowerVoltage.Value;

						//Заполняем массив показаний датчиков


						mu.SensorValues = new Dictionary<int, long>();
						
						if (point.HDOP != null)
							mu.SensorValues.Add((int) AvtoGrafSensors.HDOP, (long) Math.Round(point.HDOP.Value*100));

						if (point.SourceOfCoordinates != null)
							mu.SensorValues.Add((int) AvtoGrafSensors.SourceOfCoordinates,
								(long) point.SourceOfCoordinates.Value);

						if (point.AnalogSensor1.HasValue && point.AnalogSensor1 < int.MaxValue)
							mu.SensorValues.Add((int) AvtoGrafSensors.AN1, point.AnalogSensor1.Value);

						if (point.AnalogSensor2.HasValue && point.AnalogSensor2 < int.MaxValue)
							mu.SensorValues.Add((int) AvtoGrafSensors.AN2, point.AnalogSensor2.Value);

						if (point.PowerVoltage.HasValue)
						{
							mu.SensorValues.Add((int) AvtoGrafSensors.PowerVoltage, point.PowerVoltage.Value);
						}

						mu.SensorValues.Add((int) AvtoGrafSensors.D1, point.DigitalSensor1 ? 1 : 0);
						mu.SensorValues.Add((int) AvtoGrafSensors.D2, point.DigitalSensor2 ? 1 : 0);
						mu.SensorValues.Add((int) AvtoGrafSensors.D3, point.DigitalSensor3 ? 1 : 0);
						mu.SensorValues.Add((int) AvtoGrafSensors.D4, point.DigitalSensor4 ? 1 : 0);
						mu.SensorValues.Add((int) AvtoGrafSensors.D5, point.DigitalSensor5 ? 1 : 0);
						mu.SensorValues.Add((int) AvtoGrafSensors.D6, point.DigitalSensor6 ? 1 : 0);
						mu.SensorValues.Add((int) AvtoGrafSensors.D7, point.DigitalSensor7 ? 1 : 0);
						mu.SensorValues.Add((int) AvtoGrafSensors.D8, point.DigitalSensor8 ? 1 : 0);

						if (point.DigitalSensor1Counter.HasValue)
							mu.SensorValues.Add((int) AvtoGrafSensors.D_SUM1, point.DigitalSensor1Counter.Value);
						if (point.DigitalSensor2Counter.HasValue)
							mu.SensorValues.Add((int) AvtoGrafSensors.D_SUM2, point.DigitalSensor2Counter.Value);
						if (point.DigitalSensor3Counter.HasValue)
							mu.SensorValues.Add((int) AvtoGrafSensors.D_SUM3, point.DigitalSensor3Counter.Value);
						if (point.DigitalSensor4Counter.HasValue)
							mu.SensorValues.Add((int) AvtoGrafSensors.D_SUM4, point.DigitalSensor4Counter.Value);

						if (point.LLS1.HasValue)
							mu.SensorValues.Add((int) AvtoGrafSensors.LLS1, point.LLS1.Value);
						if (point.LLS2.HasValue)
							mu.SensorValues.Add((int) AvtoGrafSensors.LLS2, point.LLS2.Value);
						if (point.LLS3.HasValue)
							mu.SensorValues.Add((int) AvtoGrafSensors.LLS3, point.LLS3.Value);
						if (point.LLS4.HasValue)
							mu.SensorValues.Add((int) AvtoGrafSensors.LLS4, point.LLS4.Value);
						if (point.LLS5.HasValue)
							mu.SensorValues.Add((int) AvtoGrafSensors.LLS5, point.LLS5.Value);
						if (point.LLS6.HasValue)
							mu.SensorValues.Add((int) AvtoGrafSensors.LLS6, point.LLS6.Value);
						if (point.LLS7.HasValue)
							mu.SensorValues.Add((int) AvtoGrafSensors.LLS7, point.LLS7.Value);
						if (point.LLS8.HasValue)
							mu.SensorValues.Add((int) AvtoGrafSensors.LLS8, point.LLS8.Value);

						AddSensorValue(mu, AvtoGrafSensors.OwnTemperature, point.OwnTemperature);

						AddTemperatureValue(mu, AvtoGrafSensors.Temperature1, point.Temperature1);
						AddTemperatureValue(mu, AvtoGrafSensors.Temperature2, point.Temperature2);
						AddTemperatureValue(mu, AvtoGrafSensors.Temperature3, point.Temperature3);
						AddTemperatureValue(mu, AvtoGrafSensors.Temperature4, point.Temperature4);
						AddTemperatureValue(mu, AvtoGrafSensors.Temperature5, point.Temperature5);
						AddTemperatureValue(mu, AvtoGrafSensors.Temperature6, point.Temperature6);
						AddTemperatureValue(mu, AvtoGrafSensors.Temperature7, point.Temperature7);
						AddTemperatureValue(mu, AvtoGrafSensors.Temperature8, point.Temperature8);

						if (point.CAN != null)
						{
							mu.Properties[DeviceProperty.Protocol] = ControllerType.Names.AvtoGrafCan;

							var can = point.CAN;
							AddSensorValue(mu, AvtoGrafSensors.CAN_Speed, can.Speed);
							AddSensorValue(mu, AvtoGrafSensors.CAN_CruiseControl, can.CruiseControl);
							AddSensorValue(mu, AvtoGrafSensors.CAN_Brake, can.Brake);
							AddSensorValue(mu, AvtoGrafSensors.CAN_ParkingBrake, can.ParkingBrake);
							AddSensorValue(mu, AvtoGrafSensors.CAN_Clutch, can.Clutch);
							AddSensorValue(mu, AvtoGrafSensors.CAN_Accelerator, can.Accelerator);
							AddSensorValue(mu, AvtoGrafSensors.CAN_FuelRate, can.FuelRate);
							AddSensorValue(mu, AvtoGrafSensors.CAN_FuelLevel1, can.FuelLevel1);
							AddSensorValue(mu, AvtoGrafSensors.CAN_FuelLevel2, can.FuelLevel2);
							AddSensorValue(mu, AvtoGrafSensors.CAN_FuelLevel3, can.FuelLevel3);
							AddSensorValue(mu, AvtoGrafSensors.CAN_FuelLevel4, can.FuelLevel4);
							AddSensorValue(mu, AvtoGrafSensors.CAN_FuelLevel5, can.FuelLevel5);
							AddSensorValue(mu, AvtoGrafSensors.CAN_FuelLevel6, can.FuelLevel6);
							AddSensorValue(mu, AvtoGrafSensors.CAN_Revs, can.Revs);
							AddSensorValue(mu, AvtoGrafSensors.CAN_RunToCarMaintenance, can.RunToCarMaintenance);
							AddSensorValue(mu, AvtoGrafSensors.CAN_EngHours, can.EngHours);
							AddSensorValue(mu, AvtoGrafSensors.CAN_CoolantT, can.CoolantT);
							AddSensorValue(mu, AvtoGrafSensors.CAN_EngOilT, can.EngOilT);
							AddSensorValue(mu, AvtoGrafSensors.CAN_FuelT, can.FuelT);
							AddSensorValue(mu, AvtoGrafSensors.CAN_PressurizationAirT, can.PressurizationAirT);
							AddSensorValue(mu, AvtoGrafSensors.CAN_AirT, can.AirT, 5);
							AddSensorValue(mu, AvtoGrafSensors.CAN_TotalRun, can.TotalRun);
							AddSensorValue(mu, AvtoGrafSensors.CAN_DayRun, can.DayRun);
							AddSensorValue(mu, AvtoGrafSensors.CAN_AxleLoad1, can.AxleLoad1);
							AddSensorValue(mu, AvtoGrafSensors.CAN_AxleLoad2, can.AxleLoad2);
							AddSensorValue(mu, AvtoGrafSensors.CAN_AxleLoad3, can.AxleLoad3);
							AddSensorValue(mu, AvtoGrafSensors.CAN_AxleLoad4, can.AxleLoad4);
							AddSensorValue(mu, AvtoGrafSensors.CAN_AxleLoad5, can.AxleLoad5);
							AddSensorValue(mu, AvtoGrafSensors.CAN_AxleLoad6, can.AxleLoad6);
						}

						if (point.Wire1.HasValue)
							AddSensorValue(mu, AvtoGrafSensors.Wire1, point.Wire1);

						alRes.Add(mu);
					}
				}

				//Добавляем подтверждение приема пакета
				alRes.Add(new ConfirmPacket(Encoding.ASCII.GetBytes("DATA ACCEPT=" + datagram.PacketID + "\r\n")));
			}
		}
		private void AddSensorValue(MobilUnit.Unit.MobilUnit mu, AvtoGrafSensors sensor, decimal? value, int precision)
		{
			if (value == null)
				return;

			var toStore = value.Value;
			while (precision != 0)
			{
				toStore *= 10m;
				--precision;
			}
			mu.SensorValues[(int) sensor] = (long)toStore;
		}
		private void AddTemperatureValue(MobilUnit.Unit.MobilUnit mu, AvtoGrafSensors temperatureSensor, int? value)
		{
			if (value == null || value.Value == 0xfff)
				return;
			
			//Температура в градусах
			var sensorValue = value.Value*10000L/16 - 55*10000;
			mu.SensorValues[(int)temperatureSensor] = sensorValue;
		}
		private static void AddSensorValue(MobilUnit.Unit.MobilUnit mu, AvtoGrafSensors sensor, bool? value)
		{
			if (value.HasValue)
				mu.SensorValues.Add((int)sensor, value.Value ? 1 : 0);
		}
		private static void AddSensorValue(MobilUnit.Unit.MobilUnit mu, AvtoGrafSensors sensor, int? value)
		{
			if (value.HasValue)
				mu.SensorValues.Add((int)sensor, value.Value);
		}
		private static void AddSensorValue(MobilUnit.Unit.MobilUnit mu, AvtoGrafSensors sensor, long? value)
		{
			if (value.HasValue)
				mu.SensorValues.Add((int)sensor, value.Value);
		}
		private static void AddSensorValue(MobilUnit.Unit.MobilUnit mu, AvtoGrafSensors sensor, byte? value)
		{
			if (value.HasValue)
				mu.SensorValues.Add((int)sensor, value.Value);
		}
		private static int? GetConfDataNumberEndIndex(byte[] data)
		{
			for (var i = 2 /*@@*/+ 5 /*Минимальная длина номера*/; i != 2 + 5 + 3; ++i)
			{
				if (data[i] == '@')
					return i;
			}
			return null;
		}
		public override bool SupportData(byte[] data)
		{
			if (data == null)
				return false;

			if (data.Length < 19)
				return false;

			//Пакет конфигурации, передается 1 раз в сутки в том же tcp/ip соединении, что и основные данные
			if (data.Length >= 26 && data[0] == '@' && data[1] == '@')
			{
				var numberEndIndex = GetConfDataNumberEndIndex(data);
				if (numberEndIndex == null)
					return false;

				for (var i = 2; i != numberEndIndex; ++i)
					if (data[i] < '0' || '9' < data[i])
						return false;

				return data[numberEndIndex.Value + 8 + 1] == 'L' && data[numberEndIndex.Value + 8 + 1 + 8 + 1] == ':';
			}

			//Обычные данные
			var dataLen = new byte[4];
			Array.Copy(data, 1, dataLen, 0, 4);
			var len = ((dataLen[3] << 24) | (dataLen[2] << 16) | (dataLen[1] << 8) | (dataLen[0]));

			return data.Length >= 19 && data.Length == len && data[0] == 0x24 && AvtoGRAF16Datagram.CheckCRC(data);
		}

		private static readonly List<string> SmsDeviceParameters =
			new List<string>
				{
					"APNFULL",
					"IP",
					"PORT",
					"MODEWIDE",
					"MODE1",
					"PERIODCOUNT34",
					"INPFLAGS3",
					"INPFLAGS4"
				};
		private string GetRecommendedParameterValue(IStdCommand command, string parameterName)
		{
			switch (parameterName)
			{
				case "APNFULL":
					var internetApnConfig = GetInternetApnConfig(command);
					return string.Format("\"{0}\",\"{1}\",\"{2}\"",
										 internetApnConfig.Name,
										 internetApnConfig.User,
										 internetApnConfig.Pass);
				case "IP":
					return Manager.GetConstant(Constant.ServerIP);
				case "PORT":
					return Manager.GetConstant(Constant.ServerPort);
				case "MODEWIDE":
					return "1";
				case "MODE1":
					return "0";
				case "PERIODCOUNT34":
					return "60";
				case "INPFLAGS3":
					return "0,1,0,0,F,0,0,0,0";
				case "INPFLAGS4":
					return "0,1,0,0,F,0,0,0,0";
				default:
					return null;
			}
		}
		public override List<CommandStep> GetCommandSteps(IStdCommand command)
		{
			var commandType = command.Type;
			var password = command.Target.Password;

			if (string.IsNullOrWhiteSpace(password))
				password = "testtest";

			// Убрано ограничение, т.к. при не правильном пароле не выдается сценарий
			//if (password.Length != 8)
			//	throw new CommandSendingFailureException(CmdResult.IncorrectPassword);

			List<CommandStep> result;
			switch (commandType)
			{
				case CmdType.Setup:
					result = SmsDeviceParameters.ConvertAll<CommandStep>(
						parameterName =>
						new SmsCommandStep(password + " " + parameterName + "=" + GetRecommendedParameterValue(command, parameterName) + ";")
						{
							WaitAnswer = CmdType.Setup
						});
					break;
				case CmdType.AskPosition:
					result = new List<CommandStep>
					{
						new SmsCommandStep(string.Format("GET {0};", password)) { WaitAnswer = CmdType.AskPosition },
					};
					break;
				default:
					return base.GetCommandSteps(command);
			}

			return result;
		}
		public override IList<object> ParseSms(IUnitInfo ui, string text)
		{
			var openingBracketIndex = text.IndexOf('(');
			if (openingBracketIndex < 1)
				return null;
			var numberString = text.Substring(0, openingBracketIndex);
			int number;
			if (!int.TryParse(numberString, out number))
				return null;
			
			var result = new List<object>(1);
			var mobilUnit = new MobilUnit.Unit.MobilUnit(ui) {CorrectGPS = false, Time = TimeHelper.GetSecondsFromBase()};
			mobilUnit.Properties.Add(DeviceProperty.IMEI, number.ToString(CultureInfo.InvariantCulture));
			result.Add(mobilUnit);

			var closingBracketIndex = text.IndexOf(')', openingBracketIndex);
			if (closingBracketIndex == -1)
				return null;
			if (closingBracketIndex == text.Length - 1)
				return null;
			if (text[closingBracketIndex + 1] != '#')
				return null;
			int contentIndex = closingBracketIndex + 2;
			if (text.IndexOf("GPRS:", contentIndex, "GPRS:".Length, StringComparison.Ordinal) != -1)
				contentIndex += "GPRS:".Length;

			var content = text.Substring(contentIndex);

			var parameterName =
				SmsDeviceParameters.FirstOrDefault(
					p => content.Length >= p.Length + 2 &&
						 content.StartsWith(p) &&
						 content[p.Length] == '=' &&
						 content[content.Length-1] == ';');

			if (parameterName != null)
			{
				//TODO: проверять полученное значение - content.Substring(parameterName.Length + 1, content.Length - parameterName.Length - 1 - 1);
				result.Add(CommandCompleted(ui, CmdResult.Completed, CmdType.Setup));
			}
			else
			{
				if (!TryParseNmeaString(content, mobilUnit))
					return null;
			}

			return result;
		}
		private bool TryParseNmeaString(string content, MobilUnit.Unit.MobilUnit mu)
		{
			var reader = new SplittedStringReader(content, ',');
			var timeOfDay = reader.ReadDecimal();
			if (timeOfDay == null)
				return false;
			var gpsValidity = reader.ReadString();
			mu.CorrectGPS = gpsValidity == "A";
			var lat = ReadCoordinate(reader, "S");
			if (lat == null)
				return false;
			var lng = ReadCoordinate(reader, "N");
			if (lng == null)
				return false;
			var speedKnots = reader.ReadDouble();
			mu.Speed = speedKnots != null ? MeasureUnitHelper.KnotsToKMPH(speedKnots.Value) : (int?) null;
			var course = reader.ReadDecimal();
			mu.Course = course != null ? (int) Math.Round(course.Value) : (int?) null;
			var date = reader.ReadInt();

			var year = date%100;
			var month = (date/100)%100;
			var day = (date/10000);
			var hour = (int) (timeOfDay/10000);
			var minute = (int) ((timeOfDay/100)%100);
			var second = (int) (timeOfDay%100);

			var dateTime = new DateTime(
				2000 + year, month, day,
				hour, minute, second,
				0,
				DateTimeKind.Utc);

			mu.Time = TimeHelper.GetSecondsFromBase(dateTime);
			mu.Latitude = (double) lat.Value;
			mu.Longitude = (double) lng.Value;
			return true;
		}
		private decimal? ReadCoordinate(SplittedStringReader reader, string negativeHemisphere)
		{
			var number = reader.ReadDecimal();
			if (number == null)
				return null;
			var hemisphere = reader.ReadString();

			var degrees = (int) (number/100);
			var minutes = number - degrees*100;

			var result = degrees + minutes/60;

			return hemisphere != negativeHemisphere ? result : -result;
		}
	}
}