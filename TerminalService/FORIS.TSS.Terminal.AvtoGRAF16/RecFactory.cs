﻿using System;
using System.Diagnostics;

namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	internal class RecFactory
	{
		protected static int GetRecType(RecParts recParts)
		{
			int recType = 0;
			if ((recParts.d1 & (1 << 31)) != 0)
			{
				recType = (int)(recParts.d2 & 0xFF);
			}

			return recType;
		}

		/// <summary>
		/// Проверка на плохие записи отправляемые при пересылке черного ящика. Ошбики в них не логируем
		/// </summary>
		/// <param name="hexStr"></param>
		/// <returns></returns>
		private static bool isFakeRecord(string hexStr)
		{
			if (hexStr.StartsWith("FF-FF-FF-FF-FF-FF-FF-FF-FF-FF-FF-FF-FF-FE-FF"))
			{
				return true;
			}

			return false;
		}

		public static Rec GetRec(byte[] recArr, string firmwareVersion)
		{
			Rec rec = null;
			try
			{
				RecParts recParts = new RecParts(recArr);
				if (recParts.d1 != 0xFFFFFFFF)
				{
					int recType = GetRecType(recParts);

					switch (recType)
					{
						case (int)RecTypes.RecPosition:
							rec = new RecPosition(recParts, recType);
							break;
						case (int)RecTypes.RecMotion:
							rec = new RecMotion(recParts, recType);
							break;
						case (int)RecTypes.RecAnSensors1_2:
							rec = new RecAnSensors1_2(recParts, recType);
							break;
						case (int)RecTypes.RecDigSensors1_2:
							rec = new RecDigSensors1_2(recParts, recType);
							break;
						case (int)RecTypes.RecDigSensors3_4:
							rec = new RecDigSensors3_4(recParts, recType);
							break;
						case (int)RecTypes.RecDigSensors5_6:
							rec = new RecDigSensors5_6(recParts, recType);
							break;
						case (int)RecTypes.RecDigSensors7_8:
							rec = new RecDigSensors7_8(recParts, recType);
							break;
						case (int)RecTypes.CAN1:
							rec = new CAN1(recParts, recType);
							break;
						case (int)RecTypes.CAN2:
							rec = new CAN2(recParts, recType);
							break;
						case (int)RecTypes.CAN3:
							rec = new CAN3(recParts, recType);
							break;
						case (int)RecTypes.CAN4:
							rec = new CAN4(recParts, recType);
							break;
						case (int)RecTypes.CAN5:
							rec = new CAN5(recParts, recType, firmwareVersion);
							break;
						case (int)RecTypes.CANAdd:
							rec = new CANAdd(recParts, recType);
							break;
						case (int)RecTypes.LLS1_4:
							rec = new LLS1_4(recParts, recType);
							break;
						case (int)RecTypes.LLS5_8:
							rec = new LLS5_8(recParts, recType);
							break;
						case (int)RecTypes.Wire1:
							rec = new Rec1Wire(recParts, recType);
							break;
						case (int)RecTypes.Wire1T1_4:
						case (int)RecTypes.Wire1T5_8:
							rec = new Rec1WireTemperature(recParts, recType);
							break;
						default:
							if (recType >= (int)RecTypes.CAN6_start && recType <= (int)RecTypes.CAN6_end)
							{
								rec = new CAN6(recParts, recType);
							}
							else
							{
								rec = null; // new Rec(recParts, recType);
							}
							break;
					}
				}
			}
			catch (Exception ex)
			{
				string hexStr = BitConverter.ToString(recArr);
				if (!isFakeRecord(hexStr))
				{
					Trace.WriteLine(
						string.Format(
							"{0}: AvtoGraf16Datagram GetRec Error parsing record: \r\n {1} \r\n Error: \r\n{2}",
							DateTime.Now,
							hexStr,
							ex
							));
				}
				rec = null;
				//throw;
			}

			return rec;
		}
	}
}