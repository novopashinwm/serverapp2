﻿using System;

namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public class RecAnSensors1_2 : Rec
	{
		public RecAnSensors1_2(RecParts recParts, int recType)
			: base(recParts, recType)
		{
			ParseSensor1();
			ParseSensor2();
			ParsePowerVoltage();
		}

		public override string ToString()
		{
			return base.ToString() + ", AnSensor1=" + Sensor1.ToString() + ", AnSensor2=" + Sensor2.ToString();
		}

		public Int32 Sensor1;
		public Int32 Sensor2;
		public Int32 PowerVoltage;

		protected void ParseSensor1()
		{
			Sensor1 = (Int32)(_recParts.d2 >> 8 & 0x3FF);
		}

		protected void ParseSensor2()
		{
			Sensor2 = (Int32)(_recParts.d3 >> 8 & 0x3FF);
		}

		protected void ParsePowerVoltage()
		{
			PowerVoltage = (Int32) ((_recParts.d2 >> 18) & 0x3FF);
		}
	}
}