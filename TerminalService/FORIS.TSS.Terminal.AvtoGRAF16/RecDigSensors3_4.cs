﻿namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public class RecDigSensors3_4 : RecDigSensors1_2
	{
		public RecDigSensors3_4(RecParts recParts, int recType)
			: base(recParts, recType)
		{
		}

		public override string ToString()
		{
			return base.ToString() + ", DSum3=" + Sensor1.ToString() + ", DSum4=" + Sensor2.ToString();
		}

	}
}