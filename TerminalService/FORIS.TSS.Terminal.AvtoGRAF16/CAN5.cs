﻿using System;
using System.Diagnostics;

namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public class CAN5 : Rec
	{
		public long TotalRun;
		public uint? DayRun;

		public override string ToString()
		{
			return base.ToString() +
				   string.Format(", TotalRun={0}, DayRun={1}", TotalRun, DayRun);
		}


		public CAN5(RecParts recParts, int recType, string firmwareVersion)
			: base(recParts, recType)
		{
			try
			{
				if (firmwareVersion != null &&
					firmwareVersion.StartsWith("AGTK-"))
				{

					/*В некоторых нелинейных (префикс AGTK-) прошивках (9.50 и выше) были ошибки в записи пробега с CAN. 
					 *Ошибка заключалась в сдвиге данных второго слова записи CAN 5 (тип 14) на 1 байт вправо, 
					 *то есть старшие 8 бит общего пробега >>автомобиля записывались в нулевой байт второго слова, 
					 *а не первый, а суточный пробег автомобиля записывался в первый и второй байт, вместо второго и третьего. 
					 *Данная ошибка отсутствует в версии прибора 9.73, а >>также в версии 9.92 и выше. Запросить версию прошивки можно с помощью команды GVERSION.
					*/

					var v = firmwareVersion.Substring("AGTK-".Length);

					if (v != "9.73" &&
						String.CompareOrdinal(v, "9.5") >= 0 &&
						String.CompareOrdinal(v, "9.92") < 0)
					{
						checked
						{
							//Общий пробег (5м /1), храним в м
							TotalRun = ((long) ((_recParts.d2 >> 8) + (_recParts.d3 << 24))*5);
							// Суточный пробег (40м /1), храним в м
							var dayRunRawValue = _recParts.d3 >> 8;
							if (dayRunRawValue != 0xffff)
								DayRun = ((dayRunRawValue & 0xffff)*40);
						}
						return;
					}
				}
				
				//Согласно спецификации
				//Общий пробег (5м /1), храним в м
				checked
				{
					TotalRun = ((long) ((_recParts.d2 >> 8) + (((_recParts.d3 >> 8) & 0xFF) << 24))*5);
					// Суточный пробег (40м /1), храним в м
					var dayRunRawValue = _recParts.d3 >> 16;
					if (dayRunRawValue != 0xffff)
						DayRun = (dayRunRawValue*40);
				}
			}
			catch (Exception ex)
			{
				Trace.TraceError("{0}", ex);
			}
		}
	}
}