﻿using System.Collections.Generic;
using System.Linq;

namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public class CAN6 : Rec
	{
		public int AxleLoad;
		public byte AxleNumber;

		public override string ToString()
		{
			return base.ToString() +
				   string.Format(", AxleNumber={0}, AxleLoad={1}", AxleNumber, AxleLoad);
		}

		public CAN6(RecParts recParts, int recType)
			: base(recParts, recType)
		{
			//Суммируем нагрузку на каждое колесо и умножаем на (128кг на бит).
			var wheelLoads = GetWheelLoads();
			AxleLoad = wheelLoads.Where(w => w != 0xFF).Sum(w => (int)w) * 128;

			AxleNumber = (byte)((recType & 0xF) + 1);
		}

		protected ushort ParseOneWheelLoad(uint part, int startBit)
		{
			//все показания уровня топлива приходят как (128кг на бит), а храним кг
			var wheelLoadBits = ((part >> startBit) & 0xFF);

			return (ushort)wheelLoadBits;
		}

		private IEnumerable<ushort> GetWheelLoads()
		{
			yield return ParseOneWheelLoad(_recParts.d2, 8);
			yield return ParseOneWheelLoad(_recParts.d2, 16);
			yield return ParseOneWheelLoad(_recParts.d2, 24);
			yield return ParseOneWheelLoad(_recParts.d3, 8);
			yield return ParseOneWheelLoad(_recParts.d3, 16);
			yield return ParseOneWheelLoad(_recParts.d3, 24);
		}
	}
}