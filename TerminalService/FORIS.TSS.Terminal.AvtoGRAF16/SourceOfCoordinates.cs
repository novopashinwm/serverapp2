﻿namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	/// <summary> Источник координат </summary>
	public enum SourceOfCoordinates
	{
		Default  = 0,
		Gps      = 1,
		Glonass  = 2,
		Combined = 3,
	}
}