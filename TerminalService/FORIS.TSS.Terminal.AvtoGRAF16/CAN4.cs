﻿namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public class CAN4 : Rec
	{
		public short CoolantT { get; set; }
		public short EngOilT { get; set; }
		public short FuelT { get; set; }

		/// <summary>
		/// Температура наддувочного воздуха
		/// </summary>
		public short PressurizationAirT { get; set; }

		public override string ToString()
		{
			return base.ToString() +
				   string.Format(", CoolantT={0}, EngOilT={1}, FuelT={2}, PressAirT={3}", CoolantT, EngOilT, FuelT, PressurizationAirT);
		}


		public CAN4(RecParts recParts, int recType)
			: base(recParts, recType)
		{
			// Температура охлаждающей жидкости 1 градус /1 (сдвиг -40 градусов)
			CoolantT = (short)(((_recParts.d2 >> 8) & 0xFF) - 40);
			if (CoolantT == 215)
			{
				CoolantT = 0;
			}
			// Температура масла в двигателе 0.03125 градус /1 (сдвиг -272 градусов)
			EngOilT = (short)(((_recParts.d2 >> 16) * 0.03125) - 272);
			// Температура топлива 1 градус /1 (сдвиг -40 градусов)
			FuelT = (short)(((_recParts.d3 >> 8) & 0xFF) - 40);
			if (FuelT == 215)
			{
				FuelT = 0;
			}

			PressurizationAirT = (short) (((_recParts.d3 << 16) & 0xFF) - 40);
		}
	}
}