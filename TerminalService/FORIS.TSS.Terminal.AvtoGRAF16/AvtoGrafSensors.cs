﻿namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public enum AvtoGrafSensors
	{
		AN1 = 1,
		AN2 = 2,
		/// <summary>Напряжение питания терминала, мВ</summary>
		PowerVoltage = 3,
		/// <summary> Качество координат (1 - наилучшее, 7 - наихудшее)
		/// =ln(HDOP * 10) - 1; если 0 - нет приема, 7 - нет данных о приеме
		/// </summary>
		HDOP = 4,
		/// <summary> Источник координат, возможные значения <see cref="FORIS.TSS.Terminal.AvtoGRAF16.SourceOfCoordinates"/> </summary>
		SourceOfCoordinates = 5,
		D1 = 11,
		D2 = 12,
		D3 = 13,
		D4 = 14,
		D5 = 15,
		D6 = 16,
		D7 = 17,
		D8 = 18,
		// ReSharper disable InconsistentNaming
		D_SUM1 = 21,
		D_SUM2 = 22,
		D_SUM3 = 23,
		D_SUM4 = 24,
		D_SUM5 = 25,
		D_SUM6 = 26,
		D_SUM7 = 27,
		D_SUM8 = 28,
		// ReSharper restore InconsistentNaming
		LLS1 = 31,
		LLS2 = 32,
		LLS3 = 33,
		LLS4 = 34,
		LLS5 = 35,
		LLS6 = 36,
		LLS7 = 37,
		LLS8 = 38,

		Wire1 = 39,

		/// <summary>Температура прибора, градусы</summary>
		OwnTemperature = 40,

		/// <summary>Температура 1, 1 бит - 0.0001 градуса</summary>
		Temperature1 = 41,
		/// <summary>Температура 2, 1 бит - 0.0001 градуса</summary>
		Temperature2 = 42,
		/// <summary>Температура 3, 1 бит - 0.0001 градуса</summary>
		Temperature3 = 43,
		/// <summary>Температура 4, 1 бит - 0.0001 градуса</summary>
		Temperature4 = 44,
		/// <summary>Температура 5, 1 бит - 0.0001 градуса</summary>
		Temperature5 = 45,
		/// <summary>Температура 6, 1 бит - 0.0001 градуса</summary>
		Temperature6 = 46,
		/// <summary>Температура 7, 1 бит - 0.0001 градуса</summary>
		Temperature7 = 47,
		/// <summary>Температура 8, 1 бит - 0.0001 градуса</summary>
		Temperature8 = 48,

		//CAN
		// ReSharper disable InconsistentNaming
		CAN_Speed = 100,
		CAN_CruiseControl = 101,
		CAN_Brake = 102,
		CAN_ParkingBrake = 103,
		CAN_Clutch = 104,
		CAN_Accelerator = 105,
		CAN_FuelRate = 106,
		CAN_FuelLevel1 = 107,
		CAN_FuelLevel2 = 108,
		CAN_FuelLevel3 = 109,
		CAN_FuelLevel4 = 110,
		CAN_FuelLevel5 = 111,
		CAN_FuelLevel6 = 112,
		CAN_Revs = 113,
		CAN_RunToCarMaintenance = 114,
		CAN_EngHours = 115,
		CAN_CoolantT = 116,
		CAN_EngOilT = 117,
		CAN_FuelT = 118,
		CAN_TotalRun = 119,
		CAN_DayRun = 120,
		CAN_AxleLoad1 = 121,
		CAN_AxleLoad2 = 122,
		CAN_AxleLoad3 = 123,
		CAN_AxleLoad4 = 124,
		CAN_AxleLoad5 = 125,
		CAN_AxleLoad6 = 126,
		CAN_PressurizationAirT = 127,
		CAN_AirT = 128
		// ReSharper restore InconsistentNaming
	}
}