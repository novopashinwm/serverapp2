﻿using System;
using System.Globalization;

namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public struct RecParts
	{
		public RecParts(byte[] recArr)
		{
			Bytes = recArr;
			d1 = BitConverter.ToUInt32(recArr, 0);
			d2 = BitConverter.ToUInt32(recArr, 4);
			d3 = BitConverter.ToUInt32(recArr, 8);
		}

		internal static UInt32 ConvertHexStringToUInt32(string hexString)
		{
			UInt32 res = 0;
			res = BitConverter.ToUInt32(new byte[] { byte.Parse(hexString.Substring(0, 2), NumberStyles.HexNumber), byte.Parse(hexString.Substring(2, 2), NumberStyles.HexNumber), byte.Parse(hexString.Substring(4, 2), NumberStyles.HexNumber), byte.Parse(hexString.Substring(6, 2), NumberStyles.HexNumber) }, 0);
			return res;
		}

		public UInt32 d1;
		public UInt32 d2;
		public UInt32 d3;
		public byte[] Bytes;
	}
}