﻿using System;

namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public class RecPosition : Rec
	{
		public double Lat;
		public double Lng;
		public bool GPSCorrect;
		public double? HDOP;
		public SourceOfCoordinates SourceOfCoordinates;

		[Obsolete("Метод не учитывает знак числа")]
		protected double ParseCoord(Int32 value)
		{
			return (value * 0.00005) / 60;

			/*
			double res = 0.0;
			//Широта или Долгота со знаком (отрицательная - южная / западная) 0.00005 минут на бит
			//Math.Floor((double)(_recParts.d3 / 10000000)) + (double)((_recParts.d3 % 10000000) * 100) / 60 / 10000000;
			//градусы
			res = Math.Floor((double)(value / 10000000));

			//минуты
			res += (double)((value % 10000000) * 100) / 60 / 10000000; //Math.Floor((double)((value % 10000000) / 100000)) / 60;
			//доли минут
			//res += Math.Floor((double)((value % 100000))) / 60 / 1000000;
			return res;
			*/
		}

		protected void ParseLat()
		{
			//TODO: учитывать знак числа 
			int val = Convert.ToInt32((_recParts.d2 & 0xFFFFFFF));
			Lat = ParseCoord(val);
		}
		protected void ParseLng()
		{
			//TODO: учитывать знак числа 
			int val = Convert.ToInt32((_recParts.d3 & 0x1FFFFFFF));
			Lng = ParseCoord(val);
		}

		private void ParseHDOP()
		{
			var hdopValue = _recParts.d3 >> 29;
			if (0 < hdopValue && hdopValue < 7)
				HDOP = Math.Exp(hdopValue + 1)/10;
			else
				HDOP = null;
		}

		protected void ParseGPSCorrect()
		{
			var hdopValue = _recParts.d3 >> 29;
			GPSCorrect = (0 < hdopValue && hdopValue < 7);
		}

		private void ParseSourceOfCoordinates()
		{
			SourceOfCoordinates = (FORIS.TSS.Terminal.AvtoGRAF16.SourceOfCoordinates) ((_recParts.d2 >> 29) & 0x3);
		}


		public RecPosition(RecParts recParts, int recType)
			: base(recParts, recType)
		{
			ParseLat();
			ParseLng();
			ParseHDOP();
			ParseGPSCorrect();
			ParseSourceOfCoordinates();
		}

		public override string ToString()
		{
			return base.ToString() + ", lat=" + Lat + ", lng=" + Lng + ", GPSCorrect=" + GPSCorrect + ", HDOP=" + HDOP +
				   string.Format(", d1={0},d2={1},d3={2},d4={3},d5={4},d6={5},d7={6},d8={7}",
					   DigitalSensor1, DigitalSensor2, DigitalSensor3, DigitalSensor4,
					   DigitalSensor5, DigitalSensor6, DigitalSensor7, DigitalSensor8);
		}
	}
}