﻿namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	public class CAN3 : Rec
	{
		public byte Revs;
		public ushort RunToCarMaintenance;
		public int EngHours;

		public override string ToString()
		{
			return base.ToString() +
				   string.Format(", Revs={0},RunToCarMaintenance={1}, EngHours={2}", Revs, RunToCarMaintenance, EngHours);
		}

		public CAN3(RecParts recParts, int recType)
			: base(recParts, recType)
		{
			/// Обороты двигателя (32 об/мин /1), а храним в 100об/мин
			Revs = (byte)((_recParts.d2 >> 8) & 0xFF);
			Revs = (byte)(Revs == 255 ? 0 : (Revs * 32 / 100));
			/// Пробег до ТО (5км /1, сдвиг 160635км), а храним в 10км/ 1
			RunToCarMaintenance = (ushort)(((_recParts.d2 >> 16) * 5 - 160635) / 10);
			/// Моточасы (0.05 ч. /1 ), а храним в часах
			EngHours = (int)((_recParts.d3 >> 8) * 0.05);
		}


	}
}