﻿using System.Text;

namespace FORIS.TSS.Terminal.AvtoGRAF16
{
	/// <summary> Подходит для парсинга LLS как 1-4, так и 5-8 </summary>
	public class LLS : Rec
	{
		public LLS(RecParts recParts, int recType)
			: base(recParts, recType)
		{
			if ((_recParts.d3 & 0x01) != 0)
				LLS1 = (int)((_recParts.d2 & 0x000fff00) >> 8);
			if ((_recParts.d3 & 0x02) != 0)
				LLS2 = (int)((_recParts.d2 & 0xfff00000) >> 20);
			if ((_recParts.d3 & 0x04) != 0)
				LLS3 = (int)((_recParts.d3 & 0x000fff00) >> 8);
			if ((_recParts.d3 & 0x08) != 0)
				LLS4 = (int)((_recParts.d3 & 0xfff00000) >> 20);
		}

		public int? LLS1;
		public int? LLS2;
		public int? LLS3;
		public int? LLS4;

		public override string ToString()
		{
			var sb = new StringBuilder(base.ToString());

			var @base = (RecTypes)RecType == RecTypes.LLS1_4 ? 0 : 4;

			if (LLS1 != null)
			{
				sb.Append(", LLS");
				sb.Append(1 + @base);
				sb.Append(": ");
				sb.Append(LLS1);
			}

			if (LLS2 != null)
			{
				sb.Append(", LLS");
				sb.Append(2 + @base);
				sb.Append(": ");
				sb.Append(LLS2);
			}

			if (LLS3 != null)
			{
				sb.Append(", LLS");
				sb.Append(3 + @base);
				sb.Append(": ");
				sb.Append(LLS3);
			}

			if (LLS4 != null)
			{
				sb.Append(", LLS");
				sb.Append(4 + @base);
				sb.Append(": ");
				sb.Append(LLS4);
			}

			return sb.ToString();
		}
	}
}