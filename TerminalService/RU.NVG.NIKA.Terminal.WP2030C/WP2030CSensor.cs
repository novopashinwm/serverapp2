﻿namespace RU.NVG.NIKA.Terminal.WP2030C
{
    public enum WP2030CSensor
    {
        Ignition = 1,
        Door = 2,
        Sos = 3,
        DigitalInput4 = 4,
        DigitalInput5 = 5,
        DigitalInput6 = 6,
        DigitalInput7 = 7,
        DigitalInput8 = 8,
        DigitalInput9 = 9,
        DigitalInput10 = 10,
        DigitalInput11 = 11,
        DigitalInput12 = 12,
        DigitalInput13 = 13,
        DigitalInput14 = 14,

        Voltage = 20,
        Fuel = 21,
        Tank = 22,
        Temperature = 23,
        InternalBattery = 24
    }
}
