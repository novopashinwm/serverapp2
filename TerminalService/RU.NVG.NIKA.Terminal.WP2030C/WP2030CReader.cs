﻿using System;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace RU.NVG.NIKA.Terminal.WP2030C
{
    public class WP2030CReader
    {
        private readonly SplittedStringReader _reader;

        public WP2030CReader(string packetString)
        {
            _reader = new SplittedStringReader(packetString, ',', '$', '#', '*');
        }

        public IMobilUnit ReadUnit()
        {
            var mobilUnit = new MobilUnit();
            var imei = _reader.ReadString();
            var imeiPosition = imei.IndexOf(WP2030CDevice.Signature, StringComparison.InvariantCultureIgnoreCase) + WP2030CDevice.Signature.Length;
            imei = imei.Substring(imeiPosition);
            mobilUnit.DeviceID = imei;
            mobilUnit.Properties.Add(DeviceProperty.Protocol, ControllerType.Names.WP2030C);
            while (_reader.HasMoreData())
            {
                var packetDataType = _reader.ReadString();
                switch (packetDataType)
                {
                    case "GPRMC":
                        ReadGps(mobilUnit);
                        break;
                    case "LOC":
                        ReadLocation(mobilUnit);
                        break;
                    default:
                        if (packetDataType.Length == 14)
                            ReadAlarm(packetDataType, mobilUnit);
                        break;
                }
            }

            return mobilUnit;
        }

        private void ReadAlarm(string digitalInputs, MobilUnit mobilUnit)
        {
            var voltage = _reader.ReadDouble();
            var fuel = _reader.ReadDouble();
            var tank = _reader.ReadDouble();
            var odometer = _reader.ReadDouble();
            var temperature = _reader.ReadDouble();
            var internalBattery = _reader.ReadDouble();
            var signalStrength = _reader.ReadNullableInt();
            var mcc = _reader.ReadString();
            var mnc = _reader.ReadString();
            var lac = _reader.ReadNullableInt();
            var cellId = _reader.ReadNullableInt();

            if (voltage.HasValue)
                mobilUnit.SetSensorValue((int)WP2030CSensor.Voltage, (int)Math.Ceiling(voltage.Value));
            if(fuel.HasValue)
                mobilUnit.SetSensorValue((int)WP2030CSensor.Fuel, (int)Math.Ceiling(fuel.Value));
            if (tank.HasValue)
                mobilUnit.SetSensorValue((int)WP2030CSensor.Tank, (int)Math.Ceiling(tank.Value));
            if (odometer.HasValue)
                mobilUnit.Run = (int) Math.Ceiling(odometer.Value);
            if (temperature.HasValue)
                mobilUnit.SetSensorValue((int)WP2030CSensor.Temperature, (int)Math.Ceiling(temperature.Value));
            if(internalBattery.HasValue)
                mobilUnit.SetSensorValue((int)WP2030CSensor.InternalBattery, (int)Math.Ceiling(internalBattery.Value));
            mobilUnit.CellNetworks = new[]
                {
                    new CellNetworkRecord
                        {
                            CountryCode = mcc,
                            NetworkCode = mnc,
                            CellID = cellId ?? 0,
                            LAC = lac ?? 0,
                            Number = 0,
                            SignalStrength = signalStrength ?? 0,
                        }
                };
            mobilUnit.SetSensorValue((int)WP2030CSensor.Ignition, digitalInputs[0] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP2030CSensor.Door, digitalInputs[1] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP2030CSensor.Sos, digitalInputs[2] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP2030CSensor.DigitalInput4, digitalInputs[3] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP2030CSensor.DigitalInput5, digitalInputs[4] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP2030CSensor.DigitalInput6, digitalInputs[5] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP2030CSensor.DigitalInput7, digitalInputs[6] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP2030CSensor.DigitalInput8, digitalInputs[7] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP2030CSensor.DigitalInput9, digitalInputs[8] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP2030CSensor.DigitalInput10, digitalInputs[9] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP2030CSensor.DigitalInput11, digitalInputs[10] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP2030CSensor.DigitalInput12, digitalInputs[11] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP2030CSensor.DigitalInput13, digitalInputs[12] == '1' ? 1 : 0);
            mobilUnit.SetSensorValue((int)WP2030CSensor.DigitalInput14, digitalInputs[13] == '1' ? 1 : 0);
        }

        private void ReadLocation(MobilUnit mobilUnit)
        {
            var location = _reader.ReadString();
        }

        private void ReadGps(MobilUnit mobilUnit)
        {
            var time = _reader.ReadString().Trim().Substring(0, 6);
            var validity = _reader.ReadString();
            var lat = ReadDegrees();
            var lahs = _reader.ReadString();
            var lon = ReadDegrees();
            var lohs = _reader.ReadString();
            var speed = _reader.ReadDouble();
            var cource = _reader.ReadDouble();
            var date = _reader.ReadString().Trim().Substring(0, 6);
            var magnetic = _reader.ReadDouble();
            var magneticVariation = _reader.ReadString();
            var mode = _reader.ReadString();
            var checksum = _reader.ReadString();

            var dateTime = ReadDateTime(date, time);
            mobilUnit.Time = dateTime.HasValue ? TimeHelper.GetSecondsFromBase(dateTime.Value) : 0;
            mobilUnit.CorrectGPS = validity == "A";
            mobilUnit.Latitude = (lahs == "N" ? 1 : -1)*lat ?? MobilUnit.InvalidLatitude;
            mobilUnit.Longitude = (lohs == "E" ? 1 : -1)*lon ?? MobilUnit.InvalidLongitude;
            mobilUnit.Speed = speed.HasValue ? (int?)Math.Ceiling(speed.Value * 1.85200) : null;
            mobilUnit.Course = cource.HasValue ? (int?)Math.Ceiling(cource.Value) : null;
        }

        private double? ReadDegrees()
        {
            var nmea = _reader.ReadDouble();
            if (!nmea.HasValue)
                return null;

            var degrees = Math.Floor(nmea.Value / 100);
            var minutes = (nmea - degrees * 100) / 60;
            return degrees + minutes;
        }

        public DateTime? ReadDateTime(string dateString, string timeString)
        {
            var result = (DateTime?)null;
            int temp;

            if (!string.IsNullOrEmpty(dateString) && dateString.Length == 6)
            {
                var year = int.TryParse(dateString.Substring(4, 2), out temp) ? temp + 2000 : 0;
                var month = int.TryParse(dateString.Substring(2, 2), out temp) ? temp : 0;
                var day = int.TryParse(dateString.Substring(0, 2), out temp) ? temp : 0;
                if (year > 0 && month > 0)
                {
                    result = new DateTime(year, month, day);
                }
            }

            if (result.HasValue && !string.IsNullOrEmpty(timeString) && timeString.Length == 6)
            {
                var hours = int.TryParse(timeString.Substring(0, 2), out temp) ? temp : 0;
                var minutes = int.TryParse(timeString.Substring(2, 2), out temp) ? temp : 0;
                var seconds = int.TryParse(timeString.Substring(4, 2), out temp) ? temp : 0;
                if (hours > 0 && minutes > 0 && seconds > 0)
                {
                    return result.Value.Add(new TimeSpan(hours, minutes, seconds));
                }
            }

            return null;
        }
    }
}
