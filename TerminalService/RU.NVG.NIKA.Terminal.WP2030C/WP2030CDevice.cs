﻿using System.Collections;
using System.Text;
using FORIS.TSS.Terminal;

namespace RU.NVG.NIKA.Terminal.WP2030C
{
    public class WP2030CDevice : Device
    {
        public const string Signature = "ATL";

        public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
        {
            bufferRest = null;
            var dataString = Encoding.ASCII.GetString(data, 0, count);
            var reader = new WP2030CReader(dataString);
            var unit = reader.ReadUnit();
            return new ArrayList {unit};
        }

        public override bool SupportData(byte[] data)
        {
            var offset = data[0] != ' ' ? 0 : 1;

            if (!(data[offset] == 0x01 && data[data.Length - 2] == 0x02
                ||
                data[offset] == 0x03 && data[data.Length - 2] == 0x04))
                return false;

            var parts = Encoding.ASCII.GetString(data).Split(',');
            if (!parts[0].Contains(Signature) && !parts[parts.Length - 1].Contains(Signature))
                return false;

            return true;
        }
    }
}
