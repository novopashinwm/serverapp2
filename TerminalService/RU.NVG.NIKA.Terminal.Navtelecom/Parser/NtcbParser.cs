﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers;
using RU.NVG.NIKA.Terminal.Navtelecom.Enums;
using RU.NVG.NIKA.Terminal.Navtelecom.Model;

namespace RU.NVG.NIKA.Terminal.Navtelecom.Parser
{
	public class NtcbParser
	{
		public NtcbRecordType GetRecordType(byte[] data)
		{
			if (data.StartsWith(NtcbPacket.HandshakeInpSignature))
				return NtcbRecordType.Handshake;
			if (data.StartsWith(NtcbPacket.FlexConfigurationInpSignature))
				return NtcbRecordType.FlexConfiguration;
			if (data.StartsWith(NtcbPacket.TouchMemoryInpSignature))
				return NtcbRecordType.TouchMemory;
			if (data.StartsWith(NtcbPacket.AskCurrentStateInpSignature))
				return NtcbRecordType.AskCurrentState;
			throw new NotImplementedException("not");
		}

		public string ParseImei(byte[] data)
		{
			if (data.Length != 19)
				return null;

			var reader = new DataReader(data);
			var signature = reader.ReadBytes(3);
			if (!NtcbPacket.HandshakeInpSignature.SequenceEqual(signature)) 
				return null;

			var punct = reader.ReadByte();
			var deviceId = reader.ReadBytes(15);
			return Encoding.ASCII.GetString(deviceId);
		}

		public FlexConfiguration ReadFlexConfiguration(byte[] data, out List<FlexField> fields)
		{
			fields = new List<FlexField>();
			if (data.Length < 4)
				return null;

			var reader = new DataReader(data);
			var signature = reader.ReadBytes(6);
			if (!NtcbPacket.FlexConfigurationInpSignature.SequenceEqual(signature)) 
				return null;

			var protocol        = reader.ReadByte();
			var protocolVersion = reader.ReadByte();
			var structVersion   = reader.ReadByte();
			var bitCount        = reader.ReadByte();
			var byteCount       = (int)Math.Ceiling(bitCount / 8d);
			var fieldBytes      = reader.ReadBytes(byteCount);

			var bitFields = new BitArray(fieldBytes);
			for (var i = 0; i < fieldBytes.Length; i++)
			{
				for (var bit = 0; bit < 8; bit++)
				{
					var index = i * 8 + bit;
					if (bitFields[index])
					{
						var enumIndex = (i + 1) * 8 - bit;
						var field = (FlexField)enumIndex;
						fields.Add(field);
					}
				}
			}

			return new FlexConfiguration
			{
				ProtocolVersion = protocolVersion,
				StructVersion   = structVersion,
				Protocol        = protocol,
			};
		}
	}
}