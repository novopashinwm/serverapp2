﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using RU.NVG.NIKA.Terminal.Navtelecom.Attributes;
using RU.NVG.NIKA.Terminal.Navtelecom.Enums;

namespace RU.NVG.NIKA.Terminal.Navtelecom.Extensions
{
	public static class FlexFieldExtensions
	{
		private readonly static ConcurrentDictionary<FlexField, FlexFieldAttribute> FlexFieldMeta =
			new ConcurrentDictionary<FlexField, FlexFieldAttribute>();
		static FlexFieldExtensions()
		{
			LoadFlexFieldMeta();
		}
		/// <summary> Загрузка при первом обращении описания полей формата FLEX, помеченных атрибутом <see cref="FlexFieldAttribute"/></summary>
		private static void LoadFlexFieldMeta()
		{
			var enumType = typeof(FlexField);
			foreach (FlexField flexField in Enum.GetValues(enumType))
			{
				var member = enumType.GetMember(flexField.ToString()).FirstOrDefault();
				if (member == null)
					continue;
				var metaAttribute = member
					.GetCustomAttributes(typeof(FlexFieldAttribute), false)
					.OfType<FlexFieldAttribute>()
					.FirstOrDefault();
				if (metaAttribute == null)
					continue;
				FlexFieldMeta.GetOrAdd(flexField, metaAttribute);
			}
		}
		/// <summary> Получить длину поля FLEX по его идентификатору </summary>
		/// <param name="field"></param>
		/// <returns></returns>
		public static uint GetFieldLength(this FlexField field)
		{
			return field.GetFieldMeta().Length;
		}
		/// <summary> Получить тип поля FLEX по его идентификатору </summary>
		/// <param name="field"></param>
		/// <returns></returns>
		public static TypeCode GetFieldType(this FlexField field)
		{
			return field.GetFieldMeta().TypeCode;
		}
		/// <summary> Получить информацию поля FLEX по его идентификатору </summary>
		/// <param name="field"></param>
		/// <returns></returns>
		public static FlexFieldAttribute GetFieldMeta(this FlexField field)
		{
			var result = default(FlexFieldAttribute);
			if (!FlexFieldMeta.TryGetValue(field, out result))
				throw new KeyNotFoundException(field.ToString());
			return result;
		}
	}
}