﻿using System.IO;
using RU.NVG.NIKA.Terminal.Navtelecom.Helpers;
using RU.NVG.NIKA.Terminal.Navtelecom.Model;

namespace RU.NVG.NIKA.Terminal.Navtelecom.Writer
{
	public class NtcbWriter : BinaryWriter
	{
		public NtcbWriter(Stream stream)
			: base(stream)
		{
		}

		public void WritePacket(NtcbPacket packet)
		{
			using (var stream = new MemoryStream())
			{
				using (var writer = new BinaryWriter(stream))
				{
					writer.Write(NtcbPacket.Preamble);
					writer.Write(packet.ConsumerId);
					writer.Write(packet.SenderId);
					writer.Write((short)packet.Data.Length);
					writer.Write(packet.Data.Checksum());

					var headerBytes    = stream.ToArray();
					var headerChecksum = headerBytes.Checksum();
					Write(headerBytes);
					Write(headerChecksum);
					Write(packet.Data);
				}
			}
		}

		public void HandshakeConfirm()
		{
			Write(NtcbPacket.HandshakeOutSignature);
		}

		public void AcceptFlexConfiguration(FlexConfiguration configuration)
		{
			Write(NtcbPacket.FlexConfigurationOutSignature);
			Write(configuration.Protocol);
			Write(configuration.ProtocolVersion);
			Write(configuration.StructVersion);
		}
		public void TouchMemoryConfirm()
		{
			Write(NtcbPacket.TouchMemoryOutSignature);
		}
		public void WriteAskPosition()
		{
			Write(NtcbPacket.AskCurrentStateOutSignature);
		}
	}
}