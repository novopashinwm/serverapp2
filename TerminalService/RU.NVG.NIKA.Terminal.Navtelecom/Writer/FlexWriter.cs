﻿using System.IO;
using FORIS.TSS.Helpers;
using RU.NVG.NIKA.Terminal.Navtelecom.Enums;
using RU.NVG.NIKA.Terminal.Navtelecom.Model;

namespace RU.NVG.NIKA.Terminal.Navtelecom.Writer
{
	public class FlexWriter : BinaryWriter
	{
		public FlexWriter(Stream stream) : base(stream)
		{
		}

		public void AcceptAlarmRecord(FlexAlarmRecord record)
		{
			var stream = new MemoryStream(6);
			var writer = new BinaryWriter(stream);
			writer.Write(FlexPacket.StartSignature);
			writer.Write((byte)FlexRecordType.AlarmRecord);
			writer.Write(record.EventIndex);

			var data = stream.ToArray();
			var crc = data.Crc8();
			Write(data);
			Write(crc);
		}

		public void AcceptBlackBoxRecord(FlexBlackBoxRecord record)
		{
			var stream = new MemoryStream();
			var writer = new BinaryWriter(stream);
			writer.Write(FlexPacket.StartSignature);
			writer.Write((byte)FlexRecordType.BlackBoxRecord);
			writer.Write((byte)record.Records.Count);
			var data = stream.ToArray();
			var crc = data.Crc8();
			Write(data);
			Write(crc);
		}

		public void AcceptCurrentStateRecord()
		{
			var stream = new MemoryStream();
			var writer = new BinaryWriter(stream);
			writer.Write(FlexPacket.StartSignature);
			writer.Write((byte)FlexRecordType.CurrentStateRecord);

			var data = stream.ToArray();
			var crc = data.Crc8();
			Write(data);
			Write(crc);
		}
		public void AcceptExtAlarmRecord(FlexAlarmRecordExtra record)
		{
			var stream = new MemoryStream(6);
			var writer = new BinaryWriter(stream);
			writer.Write(FlexPacket.StartSignature);
			writer.Write((byte)FlexRecordType.ExtAlarmRecord);
			writer.Write(record.EventIndex);

			var data = stream.ToArray();
			var crc = data.Crc8();
			Write(data);
			Write(crc);
		}

		public void AcceptExtBlackBoxRecord(FlexBlackBoxRecordExtra record)
		{
			var stream = new MemoryStream();
			var writer = new BinaryWriter(stream);
			writer.Write(FlexPacket.StartSignature);
			writer.Write((byte)FlexRecordType.ExtBlackBoxRecord);
			writer.Write((byte)record.Records.Count);

			var data = stream.ToArray();
			var crc = data.Crc8();
			Write(data);
			Write(crc);
		}
	}
}