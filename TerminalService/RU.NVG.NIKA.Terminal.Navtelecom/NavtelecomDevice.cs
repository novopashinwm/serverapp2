﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using RU.NVG.NIKA.Terminal.Navtelecom.Enums;
using RU.NVG.NIKA.Terminal.Navtelecom.Model;
using RU.NVG.NIKA.Terminal.Navtelecom.Parser;
using RU.NVG.NIKA.Terminal.Navtelecom.Reader;
using RU.NVG.NIKA.Terminal.Navtelecom.Writer;

namespace RU.NVG.NIKA.Terminal.Navtelecom
{
	public class NavtelecomDevice : Device
	{
		internal const string ProtocolName = "Navtelecom";
		internal static class DeviceNames
		{
			internal const string NavtelecomSignal_S2551 = "Navtelecom SIGNAL S-2551";
			internal const string NavtelecomSmart_S2333A = "Navtelecom SMART S-2333A";
			internal const string NavtelecomSmart_S2430  = "Navtelecom SMART S-2430";
			internal const string NavtelecomSmart_S2435  = "Navtelecom SMART S-2435";
		}

		public NavtelecomDevice()
			: base(typeof(NavtelecomSensor), new[]
			{
				DeviceNames.NavtelecomSignal_S2551,
				DeviceNames.NavtelecomSmart_S2333A,
				DeviceNames.NavtelecomSmart_S2430,
				DeviceNames.NavtelecomSmart_S2435
			})
		{
		}

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			var state = stateData as NavtelecomState;
			var protocol = DetectProtocol(data);
			switch (protocol)
			{
				case Protocol.Ntcb:
					return ParseNtcbPacket(data, state, out bufferRest);
				case Protocol.Flex:
					return ParseFlexPacket(data, state, out bufferRest);
				default:
					throw new NotImplementedException();
			}
		}

		public override bool SupportData(byte[] data)
		{
			if (data.Length < 4)
				return false;

			return data[0] == NtcbPacket.Preamble[0]
				&& data[1] == NtcbPacket.Preamble[1]
				&& data[2] == NtcbPacket.Preamble[2]
				&& data[3] == NtcbPacket.Preamble[3];
		}

		private Protocol DetectProtocol(byte[] data)
		{
			if (FlexReader.CanProcess(data))
				return Protocol.Flex;
			if (NtcbReader.CanProcess(data))
				return Protocol.Ntcb;

			throw new NotImplementedException("format not supported");
		}

		private IList ParseNtcbPacket(byte[] data, NavtelecomState state, out byte[] bufferRest)
		{
			bufferRest = null;
			var result = new ArrayList();

			var processed = true;
			var reader = new NtcbReader(data);
			NtcbPacket requestPacket;
			while ((requestPacket = reader.ReadPacket()) != null)
			{
				var parser     = new NtcbParser();
				var packetType = parser.GetRecordType(requestPacket.Data);
				using (var dataStream = new MemoryStream())
				{
					using (var writer = new NtcbWriter(dataStream))
					{
						if (state == null)
						{
							var imei = parser.ParseImei(requestPacket.Data);
							if (string.IsNullOrEmpty(imei))
								return null;
							state = new NavtelecomState { Imei = imei };
							var stateMessage = new ReceiverStoreToStateMessage { StateData = state };
							result.Add(stateMessage);
							writer.HandshakeConfirm();
						}
						else
						{
							switch (packetType)
							{
								case NtcbRecordType.Handshake:
									writer.HandshakeConfirm();
									break;
								case NtcbRecordType.FlexConfiguration:
									List<FlexField> fields;
									var configuration = parser.ReadFlexConfiguration(requestPacket.Data, out fields);
									if (FlexPacket.IsAcceptible(configuration))
									{
										state.FlexConfiguration = configuration;
										state.FlexFields        = fields;
										state.Protocol          = Protocol.Flex;
										writer.AcceptFlexConfiguration(configuration);
									}
									else
										writer.AcceptFlexConfiguration(FlexPacket.Version1);
									break;
								case NtcbRecordType.TouchMemory:
									// Пока не понятно, что с этим делать, но ответить нужно корректно
									writer.TouchMemoryConfirm();
									break;
								case NtcbRecordType.AskCurrentState:
									// В ответ ничего не пишем и не обрабатываем
									break;
								default:
									processed = false;
									break;
							}
						}

						if (processed)
						{
							var confirmBytes = dataStream.ToArray();
							if (confirmBytes.Any())
							{
								var responsePacket = new NtcbPacket
								{
									ConsumerId = requestPacket.SenderId,
									SenderId   = requestPacket.ConsumerId,
									Data       = confirmBytes
								};
								result.Add(new ConfirmPacket(responsePacket.GetBytes()));
							}
						}
					}
				}
			}

			if (processed)
				bufferRest = reader.GetRemainingBytes();

			return result;
		}

		private IList ParseFlexPacket(byte[] data, NavtelecomState state, out byte[] bufferRest)
		{
			var result = new ArrayList();
			var recordType = FlexPacket.GetRecordType(data);
			var reader = new FlexReader(data, state);
			var stream = new MemoryStream();
			var writer = new FlexWriter(stream);
			var processed = true;
			switch (recordType)
			{
				case FlexRecordType.AlarmRecord:
					var alarmRecord = reader.ReadAlarmRecord();
					var alarmUnit = alarmRecord.Record.ToMobilUnit(state.Imei);

					result.Add(alarmUnit);

					var disablingUnit = DisableQuickEvents(alarmUnit);
					if (disablingUnit != null)
						result.Add(disablingUnit);

					writer.AcceptAlarmRecord(alarmRecord);
					break;
				case FlexRecordType.BlackBoxRecord:
					var blackBoxRecord = reader.ReadBlackBoxRecord();
					var units = blackBoxRecord.Records.Select(r => r.ToMobilUnit(state.Imei)).ToArray();
					result.AddRange(units);
					writer.AcceptBlackBoxRecord(blackBoxRecord);
					break;
				case FlexRecordType.CurrentStateRecord:
					var currentStateRecord = reader.ReadCurrentStateRecord();
					var unit = currentStateRecord.ToMobilUnit(state.Imei);
					result.Add(unit);
					writer.AcceptCurrentStateRecord();
					break;
				case FlexRecordType.ExtAlarmRecord:
					var extAlarmRecord = reader.ReadExtAlarmRecord();
					if (null != extAlarmRecord?.Record)
						result.Add(extAlarmRecord.Record.ToMobilUnit(state.Imei));
					writer.AcceptExtAlarmRecord(extAlarmRecord);
					break;
				case FlexRecordType.ExtBlackBoxRecord:
					var extBlackBoxRecord = reader.ReadExtBlackBoxRecord();
					if (null != extBlackBoxRecord?.Records)
						result.AddRange(extBlackBoxRecord.Records.Select(r => r.ToMobilUnit(state.Imei)).ToArray());
					writer.AcceptExtBlackBoxRecord(extBlackBoxRecord);
					break;
				case FlexRecordType.PingRecord:
					// Просто читаем байт этой команды, ответа не требуется
					reader.ReadByte();
					break;
				default:
					processed = false;
					break;
			}

			foreach (var unit in result.OfType<IMobilUnit>())
				unit.Properties.Add(DeviceProperty.Protocol, ProtocolName);

			var confirmBytes = stream.ToArray();
			if (confirmBytes.Any())
				result.Add(new ConfirmPacket(confirmBytes));

			bufferRest = processed ? reader.GetRemainingBytes() : null;
			return result;
		}

		private static readonly NavtelecomSensor[] EventSensors =
{
			NavtelecomSensor.SH1_WeakBlow,
			NavtelecomSensor.SH2_StrongBlow,
			NavtelecomSensor.SH4_Inclination
		};

		private IMobilUnit DisableQuickEvents(MobilUnit alarmUnit)
		{
			if (alarmUnit.SensorValues == null ||
				alarmUnit.SensorValues.Count == 0)
				return null;

			var eventSensorsToDisable = new List<NavtelecomSensor>(EventSensors.Length);

			foreach (var eventSensor in EventSensors)
			{
				long value;
				if (!alarmUnit.SensorValues.TryGetValue((int)eventSensor, out value))
					continue;

				if (value != 1)
					continue;

				eventSensorsToDisable.Add(eventSensor);
			}

			return new MobilUnit
			{
				DeviceID     = alarmUnit.DeviceID,
				Time         = alarmUnit.Time + 1,
				CorrectGPS   = false,
				SensorValues = eventSensorsToDisable.ToDictionary(x => (int)x, x => 0L)
			};
		}
		protected override IEnumerable<string> GetSetupCommands(string ip, string port, string apnName, string apnUser, string apnPassword,
			string devicePassword)
		{
			var set1 = $"SET1 {ip} {port}";
			var set2 = $"SET2 {apnName} {apnUser} {apnPassword}";
			yield return set1;
			yield return set2;
		}
	}
}