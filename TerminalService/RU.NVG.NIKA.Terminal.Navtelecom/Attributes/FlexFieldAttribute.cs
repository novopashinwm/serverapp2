﻿using System;

namespace RU.NVG.NIKA.Terminal.Navtelecom.Attributes
{
	/// <summary> Flex field length attribute used for concrete <see cref="FlexField"/> item. </summary>
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
	public sealed class FlexFieldAttribute : Attribute
	{
		public FlexFieldAttribute(uint length, TypeCode typeCode)
		{
			Length   = length;
			TypeCode = typeCode;

		}
		public uint     Length   { get; private set; }
		public TypeCode TypeCode { get; private set; }
	}
}