﻿using System.Collections.Generic;
using FORIS.TSS.MobilUnit.Unit;
using RU.NVG.NIKA.Terminal.Navtelecom.Enums;

namespace RU.NVG.NIKA.Terminal.Navtelecom.Model
{
	public class FlexTeledataRecordExtra : FlexTeledataRecord
	{
		public FlexTeledataRecordExtra(Dictionary<FlexField, byte[]> fieldValues)
			: base(fieldValues)
		{
		}
		/// <summary> 1. Длина данных, U16, суммарный размер полей пакета(под номером 2-15) </summary>
		public ushort      FullSize      { get; set; }
		/// <summary> 2. Версия структуры данных, U8, Версия структуры данных статической части пакета </summary>
		public FlexVersion StructVersion { get; set; }
		/// <summary> 3. Длина данных, U8, длина статической части пакета (поля 4-14) </summary>
		public byte        StatSize      { get; set; }
		/// <summary> 15. Динамическая часть пакета - U8 Набор данных, зависит от произошедшего события </summary>
		public List<FlexRecordExtra> ExtraData;
		public class FlexRecordExtra
		{
			/// <summary> Тип поля, U8, Переменная, определяющая формат следующего поля </summary>
			public FlexRecordExtraType Type { get; set; }
			/// <summary> Длина поля, U8 </summary>
			public byte                Size { get; set; }
			/// <summary> Данные зависящие от типа поля и размером, заданным "Длинной поля" </summary>
			public object              Data { get; set; }
		}
		public override MobilUnit ToMobilUnit(string imei)
		{
			var mobilUnit = base.ToMobilUnit(imei);
			if (0 < (ExtraData?.Count ?? 0))
			{
			}
			return mobilUnit;
		}
	}
}