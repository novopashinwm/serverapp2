﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common;
using FORIS.TSS.MobilUnit.Unit;
using RU.NVG.NIKA.Terminal.Navtelecom.Enums;
using RU.NVG.NIKA.Terminal.Navtelecom.Extensions;

namespace RU.NVG.NIKA.Terminal.Navtelecom.Model
{
	public class FlexTeledataRecord
	{
		private readonly Dictionary<FlexField, byte[]> _values;
		public FlexTeledataRecord(Dictionary<FlexField, byte[]> values)
		{
			_values = values;
		}
		public int? Time
		{
			get { return (int?)GetUintValue(FlexField.Time); }
		}
		public int? LastValidTime
		{
			get { return (int?)GetUintValue(FlexField.LastValidTime); }
		}
		public double? Latitude
		{
			get
			{
				var value = GetUintValue(FlexField.LastValidLatitude);
				if (null == value)
					return null;
				return value / (60d * 10000d);
			}
		}
		public double? Longitude
		{
			get
			{
				var value = GetUintValue(FlexField.LastValidLongitude);
				if (null == value)
					return null;
				return value / (60d * 10000d);
			}
		}
		public int? Altitude
		{
			get
			{
				var value = (int?)GetUintValue(FlexField.LastValidAltitude);
				if (null == value)
					return null;
				return (int?)(value/10f);
			}
		}
		public bool? CorrectGps
		{
			get
			{
				var value = GetUintValue(FlexField.NavigationState);
				if (null == value)
					return null;
				return (value & 0x2) > 0;
			}
		}
		public bool? NavigationOn
		{
			get
			{
				var value = GetUintValue(FlexField.NavigationState);
				if (null == value)
					return null;
				return (value & 0x1) > 0;
			}
		}
		public short? Sattelites
		{
			get
			{
				var value = GetUintValue(FlexField.NavigationState);
				if (null == value)
					return null;
				var sattelites = (value & 0xfc) >> 2;
				return (short?) (sattelites > 0 && sattelites <= 32 ? sattelites : null);
			}
		}
		public int? Speed
		{
			get
			{
				var value = GetFloatValue(FlexField.Speed);
				if (null == value)
					return null;
				return value != null ? (int)Math.Round(value.Value) : (int?) null;
			}
		}
		public int? Cource
		{
			get
			{
				var value = GetUintValue(FlexField.Cource);
				if (null == value)
					return null;
				return (int?) (value >= 0 && value <= 360 ? value : null);
			}
		}
		public bool? Alarm
		{
			get
			{
				var value = GetUintValue(FlexField.DeviceState);
				if (null == value)
					return null;
				return (value & 0x2) > 0;
			}
		}
		public bool? TestMode
		{
			get
			{
				var value = GetUintValue(FlexField.DeviceState);
				if (null == value)
					return null;
				return (value & 0x1) > 0;
			}
		}
		public bool? GsmOn
		{
			get
			{
				var value = GetUintValue(FlexField.FunctionalState);
				if (null == value)
					return null;
				return (value & 0x1) > 0;
			}
		}
		public bool? UsbOn
		{
			get
			{
				var value = GetUintValue(FlexField.FunctionalState);
				if (null == value)
					return null;
				return (value & 0x2) > 0;
			}
		}
		public bool? ClockSyncByGps
		{
			get
			{
				var value = GetUintValue(FlexField.FunctionalState);
				if (null == value)
					return null;
				return (value & 0x8) == 0;
			}
		}
		public byte? ActiveSim
		{
			get
			{
				var value = GetUintValue(FlexField.FunctionalState);
				if (null == value)
					return null;
				return (byte?) ((value & 0x10) > 0 ? 2 : 1);
			}
		}
		public bool? GsmRegistered
		{
			get
			{
				var value = GetUintValue(FlexField.FunctionalState);
				if (null == value)
					return null;
				return (value & 0x20) > 0;
			}
		}
		public bool? Roaming
		{
			get
			{
				var value = GetUintValue(FlexField.FunctionalState);
				if (null == value)
					return null;
				return (value & 0x40) > 0;
			}
		}
		public bool? EngineOn
		{
			get
			{
				var value = GetUintValue(FlexField.FunctionalState);
				if (null == value)
					return null;
				return (value & 0x80) > 0;
			}
		}
		public int? MainAccVoltage
		{
			get
			{
				var value = (int?) GetUintValue(FlexField.MainAccVoltage);
				if (null == value)
					return null;
				return value/1000;
			}
		}
		public int? SecondAccVoltage
		{
			get
			{
				var value = (int?) GetUintValue(FlexField.SecondAccVoltage);
				if (null == value)
					return null;
				return value / 1000;
			}
		}
		public int? MotoHours
		{
			get
			{
				return (int?)GetUintValue(FlexField.MotoHours);
			}
		}
		public bool? DigitalInput1
		{
			get
			{
				var value = GetUintValue(FlexField.DigitalInputs1);
				if (null == value)
					return null;
				return (value & 0x1) > 0;
			}
		}
		public bool? DigitalInput2
		{
			get
			{
				var value = GetUintValue(FlexField.DigitalInputs1);
				if (null == value)
					return null;
				return (value & 0x2) > 0;
			}
		}
		public bool? DigitalInput3
		{
			get
			{
				var value = GetUintValue(FlexField.DigitalInputs1);
				if (null == value)
					return null;
				return (value & 0x4) > 0;
			}
		}
		public bool? DigitalInput4
		{
			get
			{
				var value = GetUintValue(FlexField.DigitalInputs1);
				if (null == value)
					return null;
				return (value & 0x8) > 0;
			}
		}
		public bool? DigitalInput5
		{
			get
			{
				var value = GetUintValue(FlexField.DigitalInputs1);
				if (null == value)
					return null;
				return (value & 0x10) > 0;
			}
		}
		public bool? DigitalInput6
		{
			get
			{
				var value = GetUintValue(FlexField.DigitalInputs1);
				if (null == value)
					return null;
				return (value & 0x20) > 0;
			}
		}
		public bool? DigitalInput7
		{
			get
			{
				var value = GetUintValue(FlexField.DigitalInputs1);
				if (null == value)
					return null;
				return (value & 0x40) > 0;
			}
		}
		public bool? DigitalInput8
		{
			get
			{
				var value = GetUintValue(FlexField.DigitalInputs1);
				if (null == value)
					return null;
				return (value & 0x80) > 0;
			}
		}
		/// <summary> Напряжение на аналоговом входе 1 (Ain1) в милливольтах 0-65535 мВ </summary>
		public ushort? Ain1Voltage                          =>                           (ushort?)GetUintValue(FlexField.Ain1Voltage);
		/// <summary> Напряжение на аналоговом входе 2 (Ain2) в милливольтах 0-65535 мВ </summary>
		public ushort? Ain2Voltage                          =>                           (ushort?)GetUintValue(FlexField.Ain2Voltage);
		/// <summary> Напряжение на аналоговом входе 3 (Ain3) в милливольтах 0-65535 мВ </summary>
		public ushort? Ain3Voltage                          =>                           (ushort?)GetUintValue(FlexField.Ain3Voltage);
		/// <summary> Напряжение на аналоговом входе 4 (Ain4) в милливольтах 0-65535 мВ </summary>
		public ushort? Ain4Voltage                          =>                           (ushort?)GetUintValue(FlexField.Ain4Voltage);
		/// <summary> Напряжение на аналоговом входе 5 (Ain5) в милливольтах 0-65535 мВ </summary>
		public ushort? Ain5Voltage                          =>                           (ushort?)GetUintValue(FlexField.Ain5Voltage);
		/// <summary> Напряжение на аналоговом входе 6 (Ain6) в милливольтах 0-65535 мВ </summary>
		public ushort? Ain6Voltage                          =>                           (ushort?)GetUintValue(FlexField.Ain6Voltage);
		/// <summary> Напряжение на аналоговом входе 7 (Ain7) в милливольтах 0-65535 мВ </summary>
		public ushort? Ain7Voltage                          =>                           (ushort?)GetUintValue(FlexField.Ain7Voltage);
		/// <summary> Напряжение на аналоговом входе 8 (Ain8) в милливольтах 0-65535 мВ </summary>
		public ushort? Ain8Voltage                          =>                           (ushort?)GetUintValue(FlexField.Ain8Voltage);
		public ushort? FuelSensorFreq1                      => GetValueWithRange<ushort>((ushort?)GetUintValue(FlexField.FuelSensorFreq1),                       0, 20000);
		public ushort? FuelSensorFreq2                      => GetValueWithRange<ushort>((ushort?)GetUintValue(FlexField.FuelSensorFreq2),                       0, 20000);
		/// <summary> Уровень топлива, измеренный датчиком уровня топлива 1 RS-485 </summary>
		public ushort? FuelSensor1Level_RS485               => GetValueWithRange<ushort>((ushort?)GetUintValue(FlexField.FuelSensor1Level_RS485),                0, 65499);
		/// <summary> Уровень топлива, измеренный датчиком уровня топлива 2 RS-485 </summary>
		public ushort? FuelSensor2Level_RS485               => GetValueWithRange<ushort>((ushort?)GetUintValue(FlexField.FuelSensor2Level_RS485),                0, 65499);
		/// <summary> Уровень топлива, измеренный датчиком уровня топлива 3 RS-485 </summary>
		public ushort? FuelSensor3Level_RS485               => GetValueWithRange<ushort>((ushort?)GetUintValue(FlexField.FuelSensor3Level_RS485),                0, 65499);
		/// <summary> Уровень топлива, измеренный датчиком уровня топлива 4 RS-485 </summary>
		public ushort? FuelSensor4Level_RS485               => GetValueWithRange<ushort>((ushort?)GetUintValue(FlexField.FuelSensor4Level_RS485),                0, 65499);
		/// <summary> Уровень топлива, измеренный датчиком уровня топлива 5 RS-485 </summary>
		public ushort? FuelSensor5Level_RS485               => GetValueWithRange<ushort>((ushort?)GetUintValue(FlexField.FuelSensor5Level_RS485),                0, 65499);
		/// <summary> Уровень топлива, измеренный датчиком уровня топлива 6 RS-485 </summary>
		public ushort? FuelSensor6Level_RS485               => GetValueWithRange<ushort>((ushort?)GetUintValue(FlexField.FuelSensor6Level_RS485),                0, 65499);
		public ushort? FuelSensorLevel_RS232                => GetValueWithRange<ushort>((ushort?)GetUintValue(FlexField.FuelSensorLevel_RS232),                 0, 65499);
		public uint?   EventId                              =>          GetUintValue(FlexField.EventId);
		public sbyte?  TemperatureSensor1                   => GetValueWithRange<sbyte> ( (sbyte?)GetUintValue(FlexField.TemperatureSensor1),                 -55, +125);
		public sbyte?  TemperatureSensor2                   => GetValueWithRange<sbyte> ( (sbyte?)GetUintValue(FlexField.TemperatureSensor2),                 -55, +125);
		public sbyte?  TemperatureSensor3                   => GetValueWithRange<sbyte> ( (sbyte?)GetUintValue(FlexField.TemperatureSensor3),                 -55, +125);
		public sbyte?  TemperatureSensor4                   => GetValueWithRange<sbyte> ( (sbyte?)GetUintValue(FlexField.TemperatureSensor4),                 -55, +125);
		public sbyte?  TemperatureSensor5                   => GetValueWithRange<sbyte> ( (sbyte?)GetUintValue(FlexField.TemperatureSensor5),                 -55, +125);
		public sbyte?  TemperatureSensor6                   => GetValueWithRange<sbyte> ( (sbyte?)GetUintValue(FlexField.TemperatureSensor6),                 -55, +125);
		public sbyte?  TemperatureSensor7                   => GetValueWithRange<sbyte> ( (sbyte?)GetUintValue(FlexField.TemperatureSensor7),                 -55, +125);
		public sbyte?  TemperatureSensor8                   => GetValueWithRange<sbyte> ( (sbyte?)GetUintValue(FlexField.TemperatureSensor8),                 -55, +125);
		public ushort? CanFuelLevelLitres                   => GetValueWithRange<ushort>((ushort?)GetUintValue(FlexField.CanFuelLevel),                         0,  32766); // 0.1Litre
		public ushort? CanFuelLevelPercents                 =>
			(ushort?)(GetValueWithRange<ushort>((ushort?)GetUintValue(FlexField.CanFuelLevel), 0x8000, 0x8064) & 0x7FFF); // Percents
		public long?   CanFuelConsumptionTotal
		{
			get
			{
				var value = GetFloatValue(FlexField.CanFuelConsumptionTotal);
				return value.HasValue && 0 < value.Value ? (long?)Math.Round(value.Value) : null;
			}
		}
		public ushort? CanEngineRpm                         => GetValueWithRange<ushort>((ushort?)GetUintValue(FlexField.CanEngineRpm),                         0, 65534);
		public sbyte?  CanEngineCoolantTemperature          => GetValueWithRange<sbyte> ( (sbyte?)GetUintValue(FlexField.CanEngineCoolantTemperature),       -127, +127);
		public long?   CanMileageTotal
		{
			get
			{
				var value = GetFloatValue(FlexField.CanMileageTotal);
				return value.HasValue && 0 < value.Value ? (long?)Math.Round(value.Value) : null;
			}
		}
		public ushort? CanAxleLoad1                         => GetValueWithRange<ushort>((ushort?)GetUintValue(FlexField.CanAxleLoad1),                         0, 65534);
		public ushort? CanAxleLoad2                         => GetValueWithRange<ushort>((ushort?)GetUintValue(FlexField.CanAxleLoad2),                         0, 65534);
		public ushort? CanAxleLoad3                         => GetValueWithRange<ushort>((ushort?)GetUintValue(FlexField.CanAxleLoad3),                         0, 65534);
		public ushort? CanAxleLoad4                         => GetValueWithRange<ushort>((ushort?)GetUintValue(FlexField.CanAxleLoad4),                         0, 65534);
		public ushort? CanAxleLoad5                         => GetValueWithRange<ushort>((ushort?)GetUintValue(FlexField.CanAxleLoad5),                         0, 65534);
		public byte?   CanGasPedalPosition                  => GetValueWithRange<byte>  (  (byte?)GetUintValue(FlexField.CanGasPedalPosition),                  0, 100);
		public byte?   CanBrakePedalPosition                => GetValueWithRange<byte>  (  (byte?)GetUintValue(FlexField.CanBrakePedalPosition),                0, 100);
		public byte?   CanEngineLoad                        => GetValueWithRange<byte>  (  (byte?)GetUintValue(FlexField.CanEngineLoad),                        0, 100);
		public ushort? CanLiquidLevelDieselExhaustFilter    => GetValueWithRange<ushort>((ushort?)GetUintValue(FlexField.CanLiquidLevelDieselExhaustFilter),    0, 32766);
		public uint?   CanEngineRuntimeTotal                =>                                    GetUintValue(FlexField.CanEngineRuntimeTotal);
		public short?  CanDistanceBeforeTechnicalInspection => GetValueWithRange<short> ( (short?)GetUintValue(FlexField.CanDistanceBeforeTechnicalInspection), 0, 32767);
		public byte?   CanSpeed                             => GetValueWithRange<byte>  (  (byte?)GetUintValue(FlexField.CanSpeed),                             0,   254);
		public T? GetValueWithRange<T>(T? rawValue, T minValue, T maxValue) where T : struct, IComparable<T>
		{
			if (!rawValue.HasValue)
				return rawValue;
			if (minValue.CompareTo(rawValue.Value) <= 0 &&
				maxValue.CompareTo(rawValue.Value) >= 0)
				return rawValue;
			return default(T?);
		}
		public uint? GetUintValue(FlexField field)
		{
			var value = default(byte[]);
			if (_values.TryGetValue(field, out value))
				return value?.FromLE2UInt32(0, (int)field.GetFieldLength());
			return default(uint?);
		}
		public float? GetFloatValue(FlexField field)
		{
			var value = default(byte[]);
			if (_values.TryGetValue(field, out value))
				return null != value ? BitConverter.ToSingle(value, 0) : default(float?);
			return default(float?);
		}
		public virtual MobilUnit ToMobilUnit(string imei)
		{
			var mobilUnit = new MobilUnit
			{
				Time       = Time      ?? TimeHelper.GetSecondsFromBase(DateTime.UtcNow),
				Latitude   = Latitude  ?? MobilUnit.InvalidLatitude,
				Longitude  = Longitude ?? MobilUnit.InvalidLatitude,
				Height     = Altitude,
				Speed      = Speed,
				Course     = Cource,
				CorrectGPS = (CorrectGps ?? false),
				DeviceID   = imei
			};

			if (Sattelites.HasValue)
				mobilUnit.Satellites = Sattelites.Value;

			mobilUnit.SetSensorValue((int)NavtelecomSensor.Alarm,                                Alarm);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.Engine,                               EngineOn);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.Navigation,                           NavigationOn);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.Gsm,                                  GsmOn);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.MainAccVoltage,                       MainAccVoltage);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.SecondAccVoltage,                     SecondAccVoltage);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.MotoHours,                            MotoHours);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.DigitalInput1,                        DigitalInput1);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.DigitalInput2,                        DigitalInput2);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.DigitalInput3,                        DigitalInput3);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.DigitalInput4,                        DigitalInput4);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.DigitalInput5,                        DigitalInput5);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.DigitalInput6,                        DigitalInput6);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.DigitalInput7,                        DigitalInput7);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.DigitalInput8,                        DigitalInput8);

			mobilUnit.SetSensorValue((int)NavtelecomSensor.Ain1Voltage,                          Ain1Voltage);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.Ain2Voltage,                          Ain2Voltage);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.Ain3Voltage,                          Ain3Voltage);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.Ain4Voltage,                          Ain4Voltage);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.Ain5Voltage,                          Ain5Voltage);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.Ain6Voltage,                          Ain6Voltage);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.Ain7Voltage,                          Ain7Voltage);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.Ain8Voltage,                          Ain8Voltage);

			mobilUnit.SetSensorValue((int)NavtelecomSensor.FuelSensorFreq1,                      FuelSensorFreq1);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.FuelSensorFreq2,                      FuelSensorFreq2);

			mobilUnit.SetSensorValue((int)NavtelecomSensor.FuelSensor1Level_RS485,               FuelSensor1Level_RS485);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.FuelSensor2Level_RS485,               FuelSensor2Level_RS485);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.FuelSensor3Level_RS485,               FuelSensor3Level_RS485);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.FuelSensor4Level_RS485,               FuelSensor4Level_RS485);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.FuelSensor5Level_RS485,               FuelSensor5Level_RS485);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.FuelSensor6Level_RS485,               FuelSensor6Level_RS485);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.FuelSensorLevel_RS232,                FuelSensorLevel_RS232);

			mobilUnit.SetSensorValue((int)NavtelecomSensor.TemperatureSensor1,                   TemperatureSensor1);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.TemperatureSensor2,                   TemperatureSensor2);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.TemperatureSensor3,                   TemperatureSensor3);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.TemperatureSensor4,                   TemperatureSensor4);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.TemperatureSensor5,                   TemperatureSensor5);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.TemperatureSensor6,                   TemperatureSensor6);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.TemperatureSensor7,                   TemperatureSensor7);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.TemperatureSensor8,                   TemperatureSensor8);

			mobilUnit.SetSensorValue((int)NavtelecomSensor.CanFuelLevelLitres,                   CanFuelLevelLitres);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.CanFuelLevelPercents,                 CanFuelLevelPercents);

			mobilUnit.SetSensorValue((int)NavtelecomSensor.CanFuelConsumptionTotal,              CanFuelConsumptionTotal);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.CanEngineRpm,                         CanEngineRpm);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.CanEngineCoolantTemperature,          CanEngineCoolantTemperature);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.CanMileageTotal,                      CanMileageTotal);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.CanAxleLoad1,                         CanAxleLoad1);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.CanAxleLoad2,                         CanAxleLoad2);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.CanAxleLoad3,                         CanAxleLoad3);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.CanAxleLoad4,                         CanAxleLoad4);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.CanAxleLoad5,                         CanAxleLoad5);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.CanGasPedalPosition,                  CanGasPedalPosition);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.CanBrakePedalPosition,                CanBrakePedalPosition);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.CanEngineLoad,                        CanEngineLoad);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.CanLiquidLevelDieselExhaustFilter,    CanLiquidLevelDieselExhaustFilter);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.CanEngineRuntimeTotal,                CanEngineRuntimeTotal);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.CanDistanceBeforeTechnicalInspection, CanDistanceBeforeTechnicalInspection);
			mobilUnit.SetSensorValue((int)NavtelecomSensor.CanSpeed,                             CanSpeed);

			// Состояния счетчиков пассажиропотока
			var passengersCountersStates = GetUintValue(FlexField.FIELD117);
			if (passengersCountersStates.HasValue && 0xFFFF != (passengersCountersStates.Value & 0xFFFF))
			{
				// Счетчик пассажиропотока 1
				mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers1Error,    ((passengersCountersStates.Value & 0x00FF) >> 0) >> 0 & 1);
				mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers1Sabotage, ((passengersCountersStates.Value & 0xFF00) >> 8) >> 0 & 1);
				// Счетчик пассажиропотока 2
				mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers2Error,    ((passengersCountersStates.Value & 0x00FF) >> 0) >> 1 & 1);
				mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers2Sabotage, ((passengersCountersStates.Value & 0xFF00) >> 8) >> 1 & 1);
				// Счетчик пассажиропотока 3
				mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers3Error,    ((passengersCountersStates.Value & 0x00FF) >> 0) >> 2 & 1);
				mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers3Sabotage, ((passengersCountersStates.Value & 0xFF00) >> 8) >> 2 & 1);
				// Счетчик пассажиропотока 4
				mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers4Error,    ((passengersCountersStates.Value & 0x00FF) >> 0) >> 3 & 1);
				mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers4Sabotage, ((passengersCountersStates.Value & 0xFF00) >> 8) >> 3 & 1);
				// Счетчик пассажиропотока 5
				mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers5Error,    ((passengersCountersStates.Value & 0x00FF) >> 0) >> 4 & 1);
				mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers5Sabotage, ((passengersCountersStates.Value & 0xFF00) >> 8) >> 4 & 1);
				// Счетчик пассажиропотока 6
				mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers6Error,    ((passengersCountersStates.Value & 0x00FF) >> 0) >> 5 & 1);
				mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers6Sabotage, ((passengersCountersStates.Value & 0xFF00) >> 8) >> 5 & 1);
				// Счетчик пассажиропотока 7
				mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers7Error,    ((passengersCountersStates.Value & 0x00FF) >> 0) >> 6 & 1);
				mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers7Sabotage, ((passengersCountersStates.Value & 0xFF00) >> 8) >> 6 & 1);
			}
			// Счетчик пассажиропотока 1
			var passengersCounter1Values = GetUintValue(FlexField.FIELD110);
			if (passengersCounter1Values.HasValue && 0xFFFF != (passengersCounter1Values.Value & 0xFFFF))
			{
				var inp1Val = ((passengersCounter1Values.Value & 0x00FF) >> 0);
				var out1Val = ((passengersCounter1Values.Value & 0xFF00) >> 8);
				if (inp1Val <= 150 && out1Val <= 150)
				{
					mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers1Input,  inp1Val);
					mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers1Output, out1Val);
				}
			}
			// Счетчик пассажиропотока 2
			var passengersCounter2Values = GetUintValue(FlexField.FIELD111);
			if (passengersCounter2Values.HasValue && 0xFFFF != (passengersCounter2Values.Value & 0xFFFF))
			{
				var inp2Val = ((passengersCounter2Values.Value & 0x00FF) >> 0);
				var out2Val = ((passengersCounter2Values.Value & 0xFF00) >> 8);
				if (inp2Val <= 150 && out2Val <= 150)
				{
					mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers2Input,  inp2Val);
					mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers2Output, out2Val);
				}
			}
			// Счетчик пассажиропотока 3
			var passengersCounter3Values = GetUintValue(FlexField.FIELD112);
			if (passengersCounter3Values.HasValue && 0xFFFF != (passengersCounter3Values.Value & 0xFFFF))
			{
				var inp3Val = ((passengersCounter3Values.Value & 0x00FF) >> 0);
				var out3Val = ((passengersCounter3Values.Value & 0xFF00) >> 8);
				if (inp3Val <= 150 && out3Val <= 150)
				{
					mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers3Input,  inp3Val);
					mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers3Output, out3Val);
				}
			}
			// Счетчик пассажиропотока 4
			var passengersCounter4Values = GetUintValue(FlexField.FIELD113);
			if (passengersCounter4Values.HasValue && 0xFFFF != (passengersCounter4Values.Value & 0xFFFF))
			{
				var inp4Val = ((passengersCounter4Values.Value & 0x00FF) >> 0);
				var out4Val = ((passengersCounter4Values.Value & 0xFF00) >> 8);
				if (inp4Val <= 150 && out4Val <= 150)
				{
					mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers4Input,  inp4Val);
					mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers4Output, out4Val);
				}
			}
			// Счетчик пассажиропотока 5
			var passengersCounter5Values = GetUintValue(FlexField.FIELD114);
			if (passengersCounter5Values.HasValue && 0xFFFF != (passengersCounter5Values.Value & 0xFFFF))
			{
				var inp5Val = ((passengersCounter5Values.Value & 0x00FF) >> 0);
				var out5Val = ((passengersCounter5Values.Value & 0xFF00) >> 8);
				if (inp5Val <= 150 && out5Val <= 150)
				{
					mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers5Input,  inp5Val);
					mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers5Output, out5Val);
				}
			}
			// Счетчик пассажиропотока 6
			var passengersCounter6Values = GetUintValue(FlexField.FIELD115);
			if (passengersCounter6Values.HasValue && 0xFFFF != (passengersCounter6Values.Value & 0xFFFF))
			{
				var inp6Val = ((passengersCounter6Values.Value & 0x00FF) >> 0);
				var out6Val = ((passengersCounter6Values.Value & 0xFF00) >> 8);
				if (inp6Val <= 150 && out6Val <= 150)
				{
					mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers6Input,  inp6Val);
					mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers6Output, out6Val);
				}
			}
			// Счетчик пассажиропотока 7
			var passengersCounter7Values = GetUintValue(FlexField.FIELD116);
			if (passengersCounter7Values.HasValue && 0xFFFF != (passengersCounter7Values.Value & 0xFFFF))
			{
				var inp7Val = ((passengersCounter7Values.Value & 0x00FF) >> 0);
				var out7Val = ((passengersCounter7Values.Value & 0xFF00) >> 8);
				if (inp7Val <= 150 && out7Val <= 150)
				{
					mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers7Input,  inp7Val);
					mobilUnit.SetSensorValue((int)NavtelecomSensor.Passengers7Output, out7Val);
				}
			}
			if (EventId != null)
			{
				var eventIdValue = EventId.Value;
				switch (eventIdValue)
				{
					case 0x11A1:
						mobilUnit.SetSensorValue((int)NavtelecomSensor.SH1_WeakBlow, 1);
						break;
					case 0x11A4:
						mobilUnit.SetSensorValue((int)NavtelecomSensor.SH1_WeakBlow, 0);
						break;

					case 0x11B1:
						mobilUnit.SetSensorValue((int)NavtelecomSensor.SH2_StrongBlow, 1);
						break;
					case 0x11B4:
						mobilUnit.SetSensorValue((int)NavtelecomSensor.SH2_StrongBlow, 0);
						break;

					case 0x11C1:
						mobilUnit.SetSensorValue((int)NavtelecomSensor.SH3_Move, 1);
						break;
					case 0x11C4:
						mobilUnit.SetSensorValue((int)NavtelecomSensor.SH3_Move, 0);
						break;

					case 0x11D1:
						mobilUnit.SetSensorValue((int)NavtelecomSensor.SH4_Inclination, 1);
						break;
					case 0x11D4:
						mobilUnit.SetSensorValue((int)NavtelecomSensor.SH4_Inclination, 0);
						break;
					default:
						mobilUnit.SetSensorValue((int)NavtelecomSensor.AlarmIndex, eventIdValue);
						break;
				}
			}

			return mobilUnit;
		}
	}
}