﻿namespace RU.NVG.NIKA.Terminal.Navtelecom.Model
{
	public class FlexAlarmRecordExtra
	{
		public uint                    EventIndex;
		public FlexTeledataRecordExtra Record;
	}
}