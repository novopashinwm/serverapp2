﻿using System.IO;
using System.Text;
using RU.NVG.NIKA.Terminal.Navtelecom.Writer;

namespace RU.NVG.NIKA.Terminal.Navtelecom.Model
{
	public class NtcbPacket
	{
		// Префикс команды в любом направлении
		public static readonly byte[] Preamble                      = Encoding.ASCII.GetBytes("@NTC");

		// Сообщение со строкой индивидуального идентификатора устройства.
		public static readonly byte[] HandshakeInpSignature         = Encoding.ASCII.GetBytes("*>S");     // From Dev To Srv
		public static readonly byte[] HandshakeOutSignature         = Encoding.ASCII.GetBytes("*<S");     // From Srv To Dev

		// Сообщение конфигурации полей FLEX
		public static readonly byte[] FlexConfigurationInpSignature = Encoding.ASCII.GetBytes("*>FLEX");  // From Dev To Srv
		public static readonly byte[] FlexConfigurationOutSignature = Encoding.ASCII.GetBytes("*<FLEX");  // From Srv To Dev

		// Сообщение незарегистрированного ключа Touch Memory
		public static readonly byte[] TouchMemoryInpSignature       = Encoding.ASCII.GetBytes("*>TMKEY"); // From Dev To Srv
		public static readonly byte[] TouchMemoryOutSignature       = Encoding.ASCII.GetBytes("*<TMKEY"); // From Srv To Dev

		// Запрос текущего состояния устройства
		public static readonly byte[] AskCurrentStateInpSignature   = Encoding.ASCII.GetBytes("*#A");     // From Dev To Srv
		public static readonly byte[] AskCurrentStateOutSignature   = Encoding.ASCII.GetBytes("*?A");     // From Srv To Dev

		public uint ConsumerId;

		public uint SenderId;

		public byte[] Data;

		public byte[] GetBytes()
		{
			using (var stream = new MemoryStream())
			{
				using (var writer = new NtcbWriter(stream))
				{
					writer.WritePacket(this);
					return stream.ToArray();
				}
			}
		}
	}
}