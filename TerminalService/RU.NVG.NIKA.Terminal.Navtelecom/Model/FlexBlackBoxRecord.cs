﻿using System.Collections.Generic;

namespace RU.NVG.NIKA.Terminal.Navtelecom.Model
{
	public class FlexBlackBoxRecord
	{
		public List<FlexTeledataRecord> Records;
	}
}