﻿using System;
using System.Collections.Generic;
using System.Linq;
using RU.NVG.NIKA.Terminal.Navtelecom.Enums;
using RU.NVG.NIKA.Terminal.Navtelecom.Extensions;

namespace RU.NVG.NIKA.Terminal.Navtelecom.Model
{
	public class FlexPacket
	{
		public static readonly FlexConfiguration Version1 = new FlexConfiguration
		{
			Protocol        = 0xB0, //FLEX
			ProtocolVersion = (byte)FlexVersion.FLEX1,
			StructVersion   = (byte)FlexVersion.FLEX1,
		};
		public static readonly FlexConfiguration Version2 = new FlexConfiguration
		{
			Protocol        = 0xB0, //FLEX
			ProtocolVersion = (byte)FlexVersion.FLEX2,
			StructVersion   = (byte)FlexVersion.FLEX2,
		};
		public static readonly FlexConfiguration Version3 = new FlexConfiguration
		{
			Protocol        = 0xB0, //FLEX
			ProtocolVersion = (byte)FlexVersion.FLEX3,
			StructVersion   = (byte)FlexVersion.FLEX3,
		};

		public const byte StartSignature           = (byte)'~';
		public const byte CurrentDataSignature     = (byte)FlexRecordType.CurrentStateRecord;
		public const byte PingSignature            = (byte)FlexRecordType.PingRecord;
		public const byte ExtBlackBoxDataSignature = (byte)FlexRecordType.ExtBlackBoxRecord;
		public const byte ExtAlarmDataSignature    = (byte)FlexRecordType.ExtAlarmRecord;

		public static bool IsAcceptible(FlexConfiguration configuration)
		{
			return configuration != null
				&&
				(
					(configuration.Protocol == Version1.Protocol && configuration.ProtocolVersion == Version1.ProtocolVersion && configuration.StructVersion == Version1.StructVersion)
					||
					(configuration.Protocol == Version2.Protocol && configuration.ProtocolVersion == Version2.ProtocolVersion && configuration.StructVersion == Version2.StructVersion)
					||
					(configuration.Protocol == Version3.Protocol && configuration.ProtocolVersion == Version3.ProtocolVersion && configuration.StructVersion == Version3.StructVersion)
				);
		}
		public static int GetRecordLength(IEnumerable<FlexField> fields)
		{
			return (int)fields.Sum(f => f.GetFieldLength());
		}
		public static FlexRecordType GetRecordType(byte[] data)
		{
			if(data[0] == PingSignature)
				return FlexRecordType.PingRecord;

			if(data[0] != StartSignature)
				throw new ArgumentOutOfRangeException("data", data, "signature is not valid");

			var recordType = data[1];
			switch (recordType)
			{
				case (byte)FlexRecordType.BlackBoxRecord:                return FlexRecordType.BlackBoxRecord;
				case (byte)FlexRecordType.AlarmRecord:                   return FlexRecordType.AlarmRecord;
				case (byte)FlexRecordType.CurrentStateRecord:            return FlexRecordType.CurrentStateRecord;
				case (byte)FlexRecordType.PingRecord:                    return FlexRecordType.PingRecord;
				case (byte)FlexRecordType.ExtBlackBoxRecord:             return FlexRecordType.ExtBlackBoxRecord;
				case (byte)FlexRecordType.ExtAlarmRecord:                return FlexRecordType.ExtAlarmRecord;
				case (byte)FlexRecordType.ServiceMessage_Query:          return FlexRecordType.ServiceMessage_Query;
				case (byte)FlexRecordType.ServiceMessage_Information:    return FlexRecordType.ServiceMessage_Information;
				case (byte)FlexRecordType.ServiceMessage_Unavailability: return FlexRecordType.ServiceMessage_Unavailability;
				case (byte)FlexRecordType.ServiceMessage_Order:          return FlexRecordType.ServiceMessage_Order;
				case (byte)FlexRecordType.ServiceMessage_Response:       return FlexRecordType.ServiceMessage_Response;
				case (byte)FlexRecordType.ServiceMessage_Failure:        return FlexRecordType.ServiceMessage_Failure;
				case (byte)FlexRecordType.ServiceMessage_Notification:   return FlexRecordType.ServiceMessage_Notification;
				case (byte)FlexRecordType.ServiceMessage_Get:            return FlexRecordType.ServiceMessage_Get;
				case (byte)FlexRecordType.ServiceMessage_Lack:           return FlexRecordType.ServiceMessage_Lack;
				case (byte)FlexRecordType.ServiceMessage_Data:           return FlexRecordType.ServiceMessage_Data;
				case (byte)FlexRecordType.ServiceMessage_Put:            return FlexRecordType.ServiceMessage_Put;
				case (byte)FlexRecordType.ServiceMessage_Saturation:     return FlexRecordType.ServiceMessage_Saturation;
				case (byte)FlexRecordType.ServiceMessage_More:           return FlexRecordType.ServiceMessage_More;
			}
			throw new ArgumentOutOfRangeException("data", data, "signature is not valid");
		}
	}
}