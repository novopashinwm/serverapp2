﻿namespace RU.NVG.NIKA.Terminal.Navtelecom.Model
{
	public class FlexConfiguration
	{
		public byte Protocol;
		public byte ProtocolVersion;
		public byte StructVersion;
	}
}