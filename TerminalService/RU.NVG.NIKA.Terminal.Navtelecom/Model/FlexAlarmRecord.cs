﻿namespace RU.NVG.NIKA.Terminal.Navtelecom.Model
{
	public class FlexAlarmRecord
	{
		public uint               EventIndex;
		public FlexTeledataRecord Record;
	}
}