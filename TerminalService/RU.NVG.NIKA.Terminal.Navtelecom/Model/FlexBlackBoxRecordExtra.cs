﻿using System.Collections.Generic;

namespace RU.NVG.NIKA.Terminal.Navtelecom.Model
{
	public class FlexBlackBoxRecordExtra
	{
		public List<FlexTeledataRecordExtra> Records;
	}
}