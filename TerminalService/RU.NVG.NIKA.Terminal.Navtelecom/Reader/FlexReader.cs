﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers;
using RU.NVG.NIKA.Terminal.Navtelecom.Enums;
using RU.NVG.NIKA.Terminal.Navtelecom.Extensions;
using RU.NVG.NIKA.Terminal.Navtelecom.Model;

namespace RU.NVG.NIKA.Terminal.Navtelecom.Reader
{
	public class FlexReader : DataReader
	{
		private readonly List<int> _fields;

		private readonly int _recordLength;

		public FlexReader(byte[] data, NavtelecomState state)
			: base(data)
		{
			var fields = state?.FlexFields ?? new List<FlexField>();
			_fields = fields
				.Cast<int>()
				.OrderBy(s => s)
				.ToList();
			_recordLength = FlexPacket.GetRecordLength(fields);
		}
		public FlexAlarmRecord ReadAlarmRecord()
		{
			var dataLength = 2 + _recordLength + 4;
			if (RemainingBytesCount < dataLength + 1) 
				return null;

			var prefix = ReadBytes(2);
			if ((byte)FlexRecordType.AlarmRecord != prefix[1])
				return null;

			var eventIndex = ReadLittleEndian32(4);
			var record     = ReadTeledataRecord();
			var crc        = ReadByte();
			var dataBytes = _data.Take(dataLength).ToArray();
			var computedCrc = dataBytes.Crc8();
			if (crc != computedCrc)
				throw new ArgumentException("checksum is not valid");

			return new FlexAlarmRecord
			{
				EventIndex = eventIndex,
				Record     = record
			};
		}
		public FlexBlackBoxRecord ReadBlackBoxRecord()
		{
			if (RemainingBytesCount < 3)
				return null;

			var prefix = ReadBytes(3);
			if ((byte)FlexRecordType.BlackBoxRecord != prefix[1])
				return null;

			var count      = prefix[2];
			var dataLength = 3 + _recordLength * count;
			if (RemainingBytesCount < dataLength - 3)
				return null;

			var records = new List<FlexTeledataRecord>(count);
			for (var i = 0; i < count; i ++)
				records.Add(ReadTeledataRecord());

			var dataBytes = _data.Take(dataLength).ToArray();

			var crc = ReadByte();
			var computedCrc = dataBytes.Crc8();
			if (crc != computedCrc)
				throw new ArgumentException("checksum is not valid");

			return new FlexBlackBoxRecord
			{
				Records = records
			};
		}
		public FlexTeledataRecord ReadCurrentStateRecord()
		{
			var dataLength = 2 + _recordLength;
			if (RemainingBytesCount < dataLength + 1)
				return null;

			var prefix = ReadBytes(2);
			if (FlexPacket.CurrentDataSignature != prefix[1])
				return null;

			var record      = ReadTeledataRecord();
			var crc         = ReadByte();
			var dataBytes   = _data.Take(dataLength).ToArray();
			var computedCrc = dataBytes.Crc8();
			if (crc != computedCrc)
				throw new ArgumentException("checksum is not valid");

			return record;
		}
		public FlexTeledataRecord ReadTeledataRecord()
		{
			var values = new Dictionary<FlexField, byte[]>();
			foreach (var field in _fields)
			{
				var flexField = (FlexField)field;
				var fieldSize = (int)flexField.GetFieldLength();
				var bytes     = ReadBytes(fieldSize);
				values.Add(flexField, bytes);
			}
			return new FlexTeledataRecord(values);
		}
		public FlexAlarmRecordExtra ReadExtAlarmRecord()
		{
			if (RemainingBytesCount < 8)
				return null;

			var prefix = ReadBytes(2);
			if ((byte)FlexRecordType.ExtAlarmRecord != prefix[1])
				return null;

			var eventIndex = ReadLittleEndian32(4);
			var dataLength = 6;
			var itemLenBytes = _data.Skip(dataLength).Take(2).ToArray();
			if (2 != itemLenBytes.Length)
				return null;
			dataLength += 2 + BitConverter.ToUInt16(itemLenBytes, 0);
			var record     = ReadExtTeledataRecord();
			var dataBytes  = _data.Take(dataLength).ToArray();
			var realCrc    = ReadByte();
			var calcCrc    = dataBytes.Crc8();
			if (realCrc != calcCrc)
				throw new ArgumentException("checksum is not valid");

			return new FlexAlarmRecordExtra
			{
				EventIndex = eventIndex,
				Record     = record
			};
		}
		public FlexBlackBoxRecordExtra ReadExtBlackBoxRecord()
		{
			if (RemainingBytesCount < 3)
				return null;

			var prefix = ReadBytes(3);
			if ((byte)FlexRecordType.ExtBlackBoxRecord != prefix[1])
				return null;

			var count      = prefix[2];
			var dataLength = 3;
			for (var i = 0; i < count; i++)
			{
				var itemLenBytes = _data.Skip(dataLength).Take(2).ToArray();
				if (2 != itemLenBytes.Length)
					return null;
				dataLength += 2 + BitConverter.ToUInt16(itemLenBytes, 0);
			}
			if (RemainingBytesCount < dataLength - 3)
				return null;

			var records = new List<FlexTeledataRecordExtra>(count);
			for (var i = 0; i < count; i ++)
				records.Add(ReadExtTeledataRecord());

			var dataBytes = _data.Take(dataLength).ToArray();

			var realCrc = ReadByte();
			var calcCrc = dataBytes.Crc8();
			if (realCrc != calcCrc)
				throw new ArgumentException("checksum is not valid");

			return new FlexBlackBoxRecordExtra
			{
				Records = records
			};
		}
		public FlexTeledataRecordExtra ReadExtTeledataRecord()
		{
			if (RemainingBytesCount < 2)
				return null;
			// Читаем размер пакета
			var recordFullSize = ReadLittleEndian32(2);
			if (RemainingBytesCount < recordFullSize)
				return null;
			// Читаем версию статической части пакета
			var recordStatVers = (FlexVersion)ReadByte();
			// Читаем размер статической части пакета
			var recordStatSize =              ReadByte();
			// Читаем поля статической части пакета
			var fieldValues = new Dictionary<FlexField, byte[]>()
			{
				{ FlexField.RecordId,           default(byte[]) },
				{ FlexField.EventId,            default(byte[]) },
				{ FlexField.Time,               default(byte[]) },
				{ FlexField.NavigationState,    default(byte[]) },
				{ FlexField.LastValidTime,      default(byte[]) },
				{ FlexField.LastValidLatitude,  default(byte[]) },
				{ FlexField.LastValidLongitude, default(byte[]) },
				{ FlexField.LastValidAltitude,  default(byte[]) },
				{ FlexField.Speed,              default(byte[]) },
				{ FlexField.Cource,             default(byte[]) },
				{ FlexField.Odometer,           default(byte[]) },
			};
			foreach (var fieldValue in fieldValues.ToArray())
				fieldValues[fieldValue.Key] = ReadBytes((int)fieldValue.Key.GetFieldLength());
			// Читаем поля динамической части пакета
			var recordDynaData = new List<FlexTeledataRecordExtra.FlexRecordExtra>();
			var recordDynaSize = recordFullSize - recordStatSize - 2;
			if (0 < recordDynaSize)
			{
				var positionBeg = Position;
				while ((Position - positionBeg) < recordDynaSize)
				{
					var flexRecordExtra = new FlexTeledataRecordExtra.FlexRecordExtra
					{
						Type = (FlexRecordExtraType)ReadByte(),
						Size = ReadByte(),
					};
					var data = ReadBytes(flexRecordExtra.Size);
					switch (flexRecordExtra.Type)
					{
						case FlexRecordExtraType.DriverCard:
							flexRecordExtra.Data = Encoding.ASCII
								//.GetString(ReadBytes(flexRecordExtra.Size)
								.GetString(data
								.Where(b => 0 != b)
								.ToArray());
							break;
						case FlexRecordExtraType.TouchMemory:
						case FlexRecordExtraType.RfidTag:
							flexRecordExtra.Data = BitConverter
								//.ToUInt64(ReadBytes(flexRecordExtra.Size), 0)
								.ToUInt64(data, 0)
								.ToString();
							break;
						default:
							flexRecordExtra.Data = data;
							break;
					}
					recordDynaData.Add(flexRecordExtra);
				}
			}
			// Возвращаем результат
			return new FlexTeledataRecordExtra(fieldValues)
			{
				FullSize      = (ushort)recordFullSize,
				StructVersion = recordStatVers,
				StatSize      = recordStatSize,
				ExtraData     = recordDynaData,
			};
		}
		public static bool CanProcess(byte[] data)
		{
			return data[0] == '~' || data[0] == 0x7F;
		}
	}
}