﻿using System;
using System.Linq;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers;
using RU.NVG.NIKA.Terminal.Navtelecom.Helpers;
using RU.NVG.NIKA.Terminal.Navtelecom.Model;

namespace RU.NVG.NIKA.Terminal.Navtelecom
{
	public class NtcbReader : DataReader
	{
		private const int HeaderLength = 16;

		public NtcbPacket ReadPacket()
		{
			if (RemainingBytesCount < HeaderLength)
				return null;

			var headBytes = ReadBytes(HeaderLength - 1);

			var headerReader = new DataReader(headBytes);
			var preamble     = headerReader.ReadBytes(4);
			var consumerId   = headerReader.ReadLittleEndian32(4);
			var senderId     = headerReader.ReadLittleEndian32(4);
			var dataLength   = (int)headerReader.ReadLittleEndian32(2);
			// контрольная сумма данных в пакете
			var csd = headerReader.ReadByte();
			// контрольная сумма заголовка в пакете
			var csp = ReadByte();

			if (RemainingBytesCount < dataLength)
				return null;

			var dataBytes = ReadBytes(dataLength);

			if(!NtcbPacket.Preamble.SequenceEqual(preamble))
				throw new ArgumentException("preamble don't equal");

			// контрольная сумма данных расчетная
			var dataChecksum = dataBytes.Checksum();
			if (dataChecksum != csd)
				throw new ArgumentException("data checksum is not valid");

			// контрольная сумма заголовка расчетная
			var headChecksum = headBytes.Checksum();
			if(headChecksum != csp)
				throw new ArgumentException("header checksum is not valid");

			return new NtcbPacket
			{
				ConsumerId = consumerId,
				SenderId   = senderId,
				Data       = dataBytes,
			};
		}

		public static bool CanProcess(byte[] data)
		{
			return data.StartsWith(NtcbPacket.Preamble);
		}

		public NtcbReader(byte[] data) : base(data)
		{
		}
	}
}