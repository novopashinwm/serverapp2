﻿namespace RU.NVG.NIKA.Terminal.Navtelecom.Helpers
{
	public static class PacketHelpers
	{
		public static byte Checksum(this byte[] bytes)
		{
			int result = 0;
			foreach (var b in bytes)
				result ^= b;
			return (byte)result;
		}
	}
}