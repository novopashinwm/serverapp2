﻿using System.Collections.Generic;
using RU.NVG.NIKA.Terminal.Navtelecom.Enums;
using RU.NVG.NIKA.Terminal.Navtelecom.Model;

namespace RU.NVG.NIKA.Terminal.Navtelecom
{
	public class NavtelecomState
	{
		public string            Imei;
		public Protocol          Protocol;
		public FlexConfiguration FlexConfiguration;
		public List<FlexField>   FlexFields;
	}
}