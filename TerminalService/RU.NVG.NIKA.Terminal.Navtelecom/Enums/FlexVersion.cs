﻿namespace RU.NVG.NIKA.Terminal.Navtelecom.Enums
{
	public enum FlexVersion : byte
	{
		/// <summary> FLEX1 (DEC: 10, HEX: 0x0A) </summary>
		FLEX1 = 0x0A,
		/// <summary> FLEX2 (DEC: 20, HEX: 0x14) </summary>
		FLEX2 = 0x14,
		/// <summary> FLEX3 (DEC: 30, HEX: 0x1E) </summary>
		FLEX3 = 0x1E,
	}
}