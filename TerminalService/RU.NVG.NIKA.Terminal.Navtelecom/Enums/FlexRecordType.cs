﻿namespace RU.NVG.NIKA.Terminal.Navtelecom.Enums
{
	public enum FlexRecordType : byte
	{
		#region Телеметрические сообщения FLEX 1.0
		/// <summary>
		/// Сообщение поддержки соединения протокола FLEX.
		/// </summary>
		PingRecord                    = (byte)0x7f,
		/// <summary>
		/// ~A<size><x[0]-x[count-1]><crc8>
		/// Передача накопленных телеметрических сообщений из черного ящика со структурой вида FLEX. Флаги полей
		/// либо берутся из настроек устройства, либо от сервера и указываются в отдельной команде в начале соединения.
		/// </summary>
		BlackBoxRecord                = (byte)'A',
		/// <summary>
		/// ~T<eventindex><x><crc8>
		/// Передача внеочередных сообщений со структурой вида FLEX. Флаги полей либо берутся из настроек устройства,
		/// либо от сервера и указываются в отдельной команде в начале соединения.
		/// </summary>
		AlarmRecord                   = (byte)'T',
		/// <summary>
		/// ~С<x><crc8>
		/// Передача сообщений текущей телеметрии со структурой вида FLEX. Флаги полей либо берутся из настроек
		/// устройства, либо от сервера и указываются в отдельной команде в начале соединения.
		/// </summary>
		CurrentStateRecord            = (byte)'C',
		#endregion Телеметрические сообщения FLEX 1.0
		#region Телеметрические сообщения FLEX 2.0
		/// <summary>
		/// ~E<count><x[0]-x[count - 1]><crc8>
		/// Передача накопленных дополнительных телеметрических
		/// сообщений из черного ящика со структурой вида FLEX 2.0.
		/// </summary>
		ExtBlackBoxRecord             = (byte)'E',
		/// <summary>
		/// ~X<eventindex><x><crc8>
		/// Передача дополнительных внеочередных сообщений со
		/// структурой вида FLEX 2.0.
		/// </summary>
		ExtAlarmRecord                = (byte)'X',
		#endregion Телеметрические сообщения FLEX 2.0
		#region Служебные сообщения FLEX 2.0
		/// <summary>
		/// (query) Запрос данных (версии, состояния устройства и др.).
		/// </summary>
		ServiceMessage_Query          = (byte)'Q',
		/// <summary> (information) Ответ на запрос, в случае если запрашиваемая информация доступна. </summary>
		ServiceMessage_Information    = (byte)'I',
		/// <summary> (unavailability) Ответ на запрос, в случае если запрашиваемая информация не доступна. </summary>
		ServiceMessage_Unavailability = (byte)'U',
		/// <summary> (order) Команда (включение выходных линий, постановка на охрану и др.). </summary>
		ServiceMessage_Order          = (byte)'O',
		/// <summary> (response) Ответ на команду в случае её выполнения. </summary>
		ServiceMessage_Response       = (byte)'R',
		/// <summary> (failure) Ответ на команду, в случае если она не выполнена. </summary>
		ServiceMessage_Failure        = (byte)'F',
		/// <summary> (notification) Оповещение. </summary>
		ServiceMessage_Notification   = (byte)'N',
		/// <summary> (get) Запрос блока данных. </summary>
		ServiceMessage_Get            = (byte)'G',
		/// <summary> (lack) Отрицательный ответ на запрос блока. </summary>
		ServiceMessage_Lack           = (byte)'L',
		/// <summary> (data) Блок данных, передаваемый по запросу. </summary>
		ServiceMessage_Data           = (byte)'D',
		/// <summary> (put) Загрузка блока данных. </summary>
		ServiceMessage_Put            = (byte)'P',
		/// <summary> (saturation) Отрицательный ответ на загрузку блока данных. </summary>
		ServiceMessage_Saturation     = (byte)'S',
		/// <summary> (more) Подтверждение загрузки блока данных. </summary>
		ServiceMessage_More           = (byte)'M',
		#endregion Служебные сообщения FLEX 2.0
	}
}