﻿namespace RU.NVG.NIKA.Terminal.Navtelecom.Enums
{
	public enum FlexEvent : ushort
	{
		/// <summary> Снятие с охраны по линии статуса STS </summary>
		LSTS_G_N          = 04096,
		/// <summary> Постановка на охрану по линии статуса STS по профилю №1 </summary>
		LSTS_G_Y          = 04097,
		/// <summary> Постановка на охрану по линии статуса STS по профилю №2 </summary>
		LSTS_G_2          = 04098,
		/// <summary> Постановка на охрану по линии статуса STS по профилю №3 </summary>
		LSTS_G_3          = 04099,
		/// <summary> Переход в тестовый режим по линии статуса STS </summary>
		LSTS_TM           = 04106,
		/// <summary> Вход I1 активирован </summary>
		I1_Y              = 04353,
		/// <summary> Вход I1 в норме </summary>
		I1_NORM           = 04356,
		/// <summary> Вход I2 активирован </summary>
		I2_Y              = 04369,
		/// <summary> Вход I2 в норме </summary>
		I2_NORM           = 04372,
		/// <summary> Вход I3 активирован </summary>
		I3_Y              = 04385,
		/// <summary> Вход I3 в норме </summary>
		I3_NORM           = 04388,
		/// <summary> Вход I4 активирован </summary>
		I4_Y              = 04401,
		/// <summary> Вход I4 в норме </summary>
		I4_NORM           = 04404,
		/// <summary> Вход I5 активирован </summary>
		I5_Y              = 04417,
		/// <summary> Вход I5 короткозамкнут </summary>
		I5_SH             = 04418,
		/// <summary> Вход I5 оборван </summary>
		I5_BREAK          = 04419,
		/// <summary> Вход I5 в норме </summary>
		I5_NORM           = 04420,
		/// <summary> Вход I5 предварительно активирован </summary>
		I5_WRN            = 04421,
		/// <summary> Вход I6 активирован </summary>
		I6_Y              = 04433,
		/// <summary> Вход I6 короткозамкнут </summary>
		I6_SH             = 04434,
		/// <summary> Вход I6 оборван </summary>
		I6_BREAK          = 04435,
		/// <summary> Вход I6 в норме </summary>
		I6_NORM           = 04436,
		/// <summary> Вход I6 предварительно активирован </summary>
		I6_WRN            = 04437,
		/// <summary> Сработал датчик слабого удара </summary>
		SH1_Y             = 04513,
		/// <summary> Датчик слабого удара в норме </summary>
		SH1_N             = 04516,
		/// <summary> Сработал датчик сильного удара </summary>
		SH2_Y             = 04529,
		/// <summary> Датчик сильного удара в норме </summary>
		SH2_N             = 04532,
		/// <summary> Сработал датчик перемещения </summary>
		SH3_Y             = 04545,
		/// <summary> Датчик перемещения в норме </summary>
		SH3_N             = 04548,
		/// <summary> Сработал датчик наклона </summary>
		SH4_Y             = 04561,
		/// <summary> Датчик наклона в норме </summary>
		SH4_N             = 04564,
		/// <summary> Вход I7 активирован </summary>
		I7_Y              = 04628,
		/// <summary> Вход I7 короткозамкнут </summary>
		I7_SH             = 04629,
		/// <summary> Вход I7 оборван </summary>
		I7_BREAK          = 04630,
		/// <summary> Вход I7 в норме </summary>
		I7_NORM           = 04631,
		/// <summary> Вход I8 активирован </summary>
		I8_Y              = 04644,
		/// <summary> Вход I8 короткозамкнут </summary>
		I8_SH             = 04645,
		/// <summary> Вход I8 оборван </summary>
		I8_BREAK          = 04646,
		/// <summary> Вход I8 в норме </summary>
		I8_NORM           = 04647,
		/// <summary> Напряжение на основном аккумуляторе понизилось ниже нижнего порога </summary>
		AG_DOWN           = 04656,
		/// <summary> Напряжение на основном аккумуляторе повысилось выше нижнего порога (восстановилось) </summary>
		AG_NORM           = 04657,
		/// <summary> Напряжение на резервном аккумуляторе понизилось ниже нижнего порога </summary>
		AR_DOWN           = 04672,
		/// <summary> Напряжение на резервном аккумуляторе повысилось выше нижнего порога (восстановилось) </summary>
		AR_NORM           = 04673,
		/// <summary> Включено зажигание (по напряжению питания) </summary>
		IGN_ON            = 04688,
		/// <summary> Выключено зажигание (по напряжению питания) </summary>
		IGN_OFF           = 04689,
		/// <summary> Температура на первом внешнем датчике понизилась ниже нижнего порога </summary>
		T1_DOWN           = 04880,
		/// <summary> Температура на первом внешнем датчике повысилась выше верхнего порога </summary>
		T1_UP             = 04882,
		/// <summary> Температура на первом внешнем датчике вернулась в норму </summary>
		T1_NORM           = 04883,
		/// <summary> Первый внешний температурный датчик оборван </summary>
		T1_BREAK          = 04884,
		/// <summary> Температура на втором внешнем датчике понизилась ниже нижнего порога </summary>
		T2_DOWN           = 04896,
		/// <summary> Температура на втором внешнем датчике повысилась выше верхнего порога </summary>
		T2_UP             = 04898,
		/// <summary> Температура на втором внешнем датчике вернулась в норму </summary>
		T2_NORM           = 04899,
		/// <summary> Второй внешний температурный датчик оборван </summary>
		T2_BREAK          = 04900,
		/// <summary> Температура на третьем внешнем датчике понизилась ниже нижнего порога </summary>
		T3_DOWN           = 04912,
		/// <summary> Температура на третьем внешнем датчике повысилась выше верхнего порога </summary>
		T3_UP             = 04914,
		/// <summary> Температура на третьем внешнем датчике вернулась в норму </summary>
		T3_NORM           = 04915,
		/// <summary> Третий внешний температурный датчик оборван </summary>
		T3_BREAK          = 04916,
		/// <summary> Температура на четвертом внешнем датчике понизилась ниже нижнего порога </summary>
		T4_DOWN           = 04928,
		/// <summary> Температура на четвертом внешнем датчике повысилась выше верхнего порога </summary>
		T4_UP             = 04930,
		/// <summary> Температура на четвертом внешнем датчике вернулась в норму </summary>
		T4_NORM           = 04931,
		/// <summary> Четвертый внешний температурный датчик оборван </summary>
		T4_BREAK          = 04932,
		/// <summary> Автоматическое включение выходной линии О1 </summary>
		O1_AUTO_Y         = 05173,
		/// <summary> Автоматическое выключение выходной линии О1 </summary>
		O1_AUTO_N         = 05172,
		/// <summary> Автоматическое включение выходной линии О2 </summary>
		O2_AUTO_Y         = 05153,
		/// <summary> Автоматическое выключение выходной линии О2 </summary>
		O2_AUTO_N         = 05152,
		/// <summary> Автоматическое включение выходной линии О3 </summary>
		O3_AUTO_Y         = 05169,
		/// <summary> Автоматическое выключение выходной линии О3 </summary>
		O3_AUTO_N         = 05168,
		/// <summary> Автоматическое включение выходной линии О4 </summary>
		O4_AUTO_Y         = 05185,
		/// <summary> Автоматическое выключение выходной линии О4 </summary>
		O4_AUTO_N         = 05184,
		/// <summary> Включение устройства </summary>
		START             = 05376,
		/// <summary> Отправка SMS на заданный номер для синхронизации часов </summary>
		SYNC_SMS          = 05377,
		/// <summary> Вход в рабочий режим </summary>
		WORK_MODE         = 05378,
		/// <summary> Изменилось состояние датчика глушения </summary>
		JD_CHNG           = 05379,
		/// <summary> Тревога </summary>
		EVNT_ALARM_GLOBAL = 05380,
		/// <summary> Команда синхронизация данных с сервером 1 </summary>
		SYNC_SRV1         = 05393,
		/// <summary> Команда синхронизация данных с сервером 2 </summary>
		SYNC_SRV2         = 05394,
		/// <summary> Команда синхронизация данных с сервером 3 </summary>
		SYNC_SRV3         = 05395,
		/// <summary> Таймер сигнала жизни по SMS </summary>
		TMR_SMS           = 05633,
		/// <summary> Таймер записи телеметрии в память в обычном режиме </summary>
		TMR_B_G_0         = 05634,
		/// <summary> Таймер записи телеметрии в память в режиме охраны по профилю №1 </summary>
		TMR_B_G_1         = 05635,
		/// <summary> Таймер отправки накопленных в черном ящике теле- мерических отсчётов на GPRS сервер </summary>
		TMR_GPRS          = 05636,
		/// <summary> Включение GPS </summary>
		GPS_ON            = 05889,
		/// <summary> Выключение GPS </summary>
		GPS_OFF           = 05890,
		/// <summary> Первая валидная навигация после включения </summary>
		GPS_NAV           = 05891,
		/// <summary> Пройден заданный путь </summary>
		GPS_LEN           = 05892,
		/// <summary> Курс изменён на угол, больше заданного </summary>
		GPS_CHCURS        = 05893,
		/// <summary> Скорость меньше минимально заданной </summary>
		GPS_VMIN          = 05894,
		/// <summary> Скорость больше максимально заданной </summary>
		GPS_VMAX          = 05895,
		/// <summary> Курс стабилизирован </summary>
		GPS_STCURS        = 05896,
		/// <summary> Объект тронулся с места </summary>
		GPS_GO            = 05897,
		/// <summary> Объект остановился </summary>
		GPS_STOP          = 05898,
		/// <summary> Таймер записи телеметрии от последнего текущего события </summary>
		GPS_TIMER         = 05899,
		/// <summary> Происходит эвакуация </summary>
		EVAC              = 05900,
		/// <summary> Окончание эвакуации </summary>
		EVAC_END          = 05901,
		/// <summary> Автоинформатор: объект вошёл в геозону </summary>
		AINF_GZIN         = 05904,
		/// <summary> Автоинформатор: объект вышел из геозоны </summary>
		AINF_GZOUT        = 05905,
		/// <summary> Режимы движения: превышение скорости </summary>
		SM_MAX            = 05906,
		/// <summary> Режимы движения: скорость в норме </summary>
		SM_NORM           = 05907,
		/// <summary> EcoDriving: Превышена скорость. Порог 1 </summary>
		ECD_SPD_M1        = 05936,
		/// <summary> EcoDriving: Скорость в норме. Порог 1 </summary>
		ECD_SPD_N1        = 05937,
		/// <summary> EcoDriving: Превышена скорость. Порог 2 </summary>
		ECD_SPD_M2        = 05938,
		/// <summary> EcoDriving: Скорость в норме. Порог 2 </summary>
		ECD_SPD_N2        = 05939,
		/// <summary> EcoDriving: Превышена скорость. Порог 3 </summary>
		ECD_SPD_M3        = 05940,
		/// <summary> EcoDriving: Скорость в норме. Порог 3 </summary>
		ECD_SPD_N3        = 05941,
		/// <summary> EcoDriving: Превышена скорость. Порог 4 </summary>
		ECD_SPD_M4        = 05942,
		/// <summary> EcoDriving: Скорость в норме. Порог 4 </summary>
		ECD_SPD_N4        = 05943,
		/// <summary> EcoDriving: Превышена скорость. Порог 5 </summary>
		ECD_SPD_M5        = 05944,
		/// <summary> EcoDriving: Скорость в норме. Порог 5 </summary>
		ECD_SPD_N5        = 05945,
		/// <summary> EcoDriving: Превышена скорость. Порог 6 </summary>
		ECD_SPD_M6        = 05946,
		/// <summary> EcoDriving: Скорость в норме. Порог 6 </summary>
		ECD_SPD_N6        = 05947,
		/// <summary> EcoDriving: Превышено ускорение. Порог 1 </summary>
		ECD_BST_M1        = 05952,
		/// <summary> EcoDriving: Ускорение в норме. Порог 1 </summary>
		ECD_BST_N1        = 05953,
		/// <summary> EcoDriving: Превышено ускорение. Порог 2 </summary>
		ECD_BST_M2        = 05954,
		/// <summary> EcoDriving: Ускорение в норме. Порог 2 </summary>
		ECD_BST_N2        = 05955,
		/// <summary> EcoDriving: Превышено ускорение. Порог 3 </summary>
		ECD_BST_M3        = 05956,
		/// <summary> EcoDriving: Ускорение в норме. Порог 3 </summary>
		ECD_BST_N3        = 05957,
		/// <summary> EcoDriving: Превышено ускорение. Порог 4 </summary>
		ECD_BST_M4        = 05958,
		/// <summary> EcoDriving: Ускорение в норме. Порог 4 </summary>
		ECD_BST_N4        = 05959,
		/// <summary> EcoDriving: Превышено замедление. Порог 1 </summary>
		ECD_RTD_M1        = 05968,
		/// <summary> EcoDriving: Замедление в норме. Порог 1 </summary>
		ECD_RTD_N1        = 05969,
		/// <summary> EcoDriving: Превышено замедление. Порог 2 </summary>
		ECD_RTD_M2        = 05970,
		/// <summary> EcoDriving: Замедление в норме. Порог 2 </summary>
		ECD_RTD_N2        = 05971,
		/// <summary> EcoDriving: Превышено замедление. Порог 3 </summary>
		ECD_RTD_M3        = 05972,
		/// <summary> EcoDriving: Замедление в норме. Порог 3 </summary>
		ECD_RTD_N3        = 05973,
		/// <summary> EcoDriving: Превышено замедление. Порог 4 </summary>
		ECD_RTD_M4        = 05974,
		/// <summary> EcoDriving: Замедление в норме. Порог 4 </summary>
		ECD_RTD_N4        = 05975,
		/// <summary> EcoDriving: Превышено ускорение влево. Порог 1 </summary>
		ECD_LFT_M1        = 05984,
		/// <summary> EcoDriving: Ускорение влево в норме. Порог 1 </summary>
		ECD_LFT_N1        = 05985,
		/// <summary> EcoDriving: Превышено ускорение влево. Порог 2 </summary>
		ECD_LFT_M2        = 05986,
		/// <summary> EcoDriving: Ускорение влево в норме. Порог 2 </summary>
		ECD_LFT_N2        = 05987,
		/// <summary> EcoDriving: Превышено ускорение влево. Порог 3 </summary>
		ECD_LFT_M3        = 05988,
		/// <summary> EcoDriving: Ускорение влево в норме. Порог 3 </summary>
		ECD_LFT_N3        = 05989,
		/// <summary> EcoDriving: Превышено ускорение влево. Порог 4 </summary>
		ECD_LFT_M4        = 05990,
		/// <summary> EcoDriving: Ускорение влево в норме. Порог 4 </summary>
		ECD_LFT_N4        = 05991,
		/// <summary> EcoDriving: Превышено ускорение вправо. Порог 1 </summary>
		ECD_RGT_M1        = 06000,
		/// <summary> EcoDriving: Ускорение вправо в норме. Порог 1 </summary>
		ECD_RGT_N1        = 06001,
		/// <summary> EcoDriving: Превышено ускорение вправо. Порог 2 </summary>
		ECD_RGT_M2        = 06002,
		/// <summary> EcoDriving: Ускорение вправо в норме. Порог 2 </summary>
		ECD_RGT_N2        = 06003,
		/// <summary> EcoDriving: Превышено ускорение вправо. Порог 3 </summary>
		ECD_RGT_M3        = 06004,
		/// <summary> EcoDriving: Ускорение вправо в норме. Порог 3 </summary>
		ECD_RGT_N3        = 06005,
		/// <summary> EcoDriving: Превышено ускорение вправо. Порог 4 </summary>
		ECD_RGT_M4        = 06006,
		/// <summary> EcoDriving: Ускорение вправо в норме. Порог 4 </summary>
		ECD_RGT_N4        = 06007,
		/// <summary> EcoDriving: Превышено вертикальное ускорение. Порог 1 </summary>
		ECD_JMP_M1        = 06016,
		/// <summary> EcoDriving: Вертикальное ускорение в норме. Порог 1 </summary>
		ECD_JMP_N1        = 06017,
		/// <summary> EcoDriving: Превышено вертикальное ускорение. Порог 2 </summary>
		ECD_JMP_M2        = 06018,
		/// <summary> EcoDriving: Вертикальное ускорение в норме. Порог 2 </summary>
		ECD_JMP_N2        = 06019,
		/// <summary> EcoDriving: Превышено вертикальное ускорение. Порог 3 </summary>
		ECD_JMP_M3        = 06020,
		/// <summary> EcoDriving: Вертикальное ускорение в норме. Порог 3 </summary>
		ECD_JMP_N3        = 06021,
		/// <summary> EcoDriving: Превышено вертикальное ускорение. Порог 4 </summary>
		ECD_JMP_M4        = 06022,
		/// <summary> EcoDriving: Вертикальное ускорение в норме. Порог 4 </summary>
		ECD_JMP_N4        = 06023,
		/// <summary> Напряжение на входе A1 или A2 повысилось на заданный порог </summary>
		VOLT_UP           = 06048,
		/// <summary> Напряжение на входе A1 или A2 понизилось на заданный порог </summary>
		VOLT_DOWN         = 06064,
		/// <summary> Уровень топлива повысился на заданный порог </summary>
		FUEL_UP           = 06080,
		/// <summary> Уровень топлива понизился на заданный порог </summary>
		FUEL_DOWN         = 06096,
		/// <summary> Уровень топлива изменился (CAN) </summary>
		CAN_FLVL_CHG      = 06112,
		/// <summary> Вес изменился (CAN) </summary>
		CAN_VW_CHG        = 06113,
		/// <summary> Обороты двигателя изменились (CAN) </summary>
		CAN_RPM_CHG       = 06114,
		/// <summary> Нагрузка на двигатель изменилась (CAN) </summary>
		CAN_ELD_CHG       = 06115,
		/// <summary> Положение педали газа изменилось (CAN) </summary>
		CAN_ACCL_CHG      = 06116,
		/// <summary> Положение педали тормоза изменилось (CAN) </summary>
		CAN_BRK_CHG       = 06117,
		/// <summary> Скорость изменилась (CAN) </summary>
		CAN_VEL_CHG       = 06118,
		/// <summary> Обороты больше максимально заданных (CAN) </summary>
		CAN_RPM_MAX       = 06120,
		/// <summary> Обороты меньше максимально заданных (CAN) </summary>
		CAN_RPM_NORM      = 06121,
		/// <summary> Включение GSM </summary>
		GSM_ON            = 06145,
		/// <summary> Выключение GSM </summary>
		GSM_OFF           = 06146,
		/// <summary> Регистрация GSM в сети </summary>
		REG_Y             = 06147,
		/// <summary> Потеря регистрации GSM </summary>
		REG_N             = 06148,
		/// <summary> SIM-карта извлечена </summary>
		SIM_OUT           = 06149,
		/// <summary> SIM-карта установлена </summary>
		SIM_IN            = 06150,
		/// <summary> Постановка на охрану по брелоку </summary>
		TM_X_G_Y          = 06398,
		/// <summary> Снятие с охраны по брелоку </summary>
		TM_X_G_N          = 06399,
		/// <summary> Постановка на охрану по брелоку №1 </summary>
		TM_1_G_Y          = 06400,
		/// <summary> Снятие с охраны по брелоку №1 </summary>
		TM_1_G_N          = 06401,
		/// <summary> Постановка на охрану по брелоку №2 </summary>
		TM_2_G_Y          = 06402,
		/// <summary> Снятие с охраны по брелоку №2 </summary>
		TM_2_G_N          = 06403,
		/// <summary> Постановка на охрану по брелоку №3 </summary>
		TM_3_G_Y          = 06404,
		/// <summary> Снятие с охраны по брелоку №3 </summary>
		TM_3_G_N          = 06405,
		/// <summary> Постановка на охрану по брелоку №4 </summary>
		TM_4_G_Y          = 06406,
		/// <summary> Снятие с охраны по брелоку №4 </summary>
		TM_4_G_N          = 06407,
		/// <summary> Постановка на охрану по брелоку №5 </summary>
		TM_5_G_Y          = 06408,
		/// <summary> Снятие с охраны по брелоку №5 </summary>
		TM_5_G_N          = 06409,
		/// <summary> Постановка на охрану по брелоку №6 </summary>
		TM_6_G_Y          = 06410,
		/// <summary> Снятие с охраны по брелоку №6 </summary>
		TM_6_G_N          = 06411,
		/// <summary> Постановка на охрану по брелоку №7 </summary>
		TM_7_G_Y          = 06412,
		/// <summary> Снятие с охраны по брелоку №7 </summary>
		TM_7_G_N          = 06413,
		/// <summary> Постановка на охрану по брелоку №8 </summary>
		TM_8_G_Y          = 06414,
		/// <summary> Снятие с охраны по брелоку №8 </summary>
		TM_8_G_N          = 06415,
		/// <summary> Постановка на охрану по брелоку №9 </summary>
		TM_9_G_Y          = 06416,
		/// <summary> Снятие с охраны по брелоку №9 </summary>
		TM_9_G_N          = 06417,
		/// <summary> Постановка на охрану по брелоку №10 </summary>
		TM_10_G_Y         = 06418,
		/// <summary> Снятие с охраны по брелоку №10 </summary>
		TM_10_G_N         = 06419,
		/// <summary> Постановка на охрану по брелоку №11 </summary>
		TM_11_G_Y         = 06420,
		/// <summary> Снятие с охраны по брелоку №11 </summary>
		TM_11_G_N         = 06421,
		/// <summary> Постановка на охрану по брелоку №12 </summary>
		TM_12_G_Y         = 06422,
		/// <summary> Снятие с охраны по брелоку №12 </summary>
		TM_12_G_N         = 06423,
		/// <summary> Постановка на охрану по брелоку №13 </summary>
		TM_13_G_Y         = 06424,
		/// <summary> Снятие с охраны по брелоку №13 </summary>
		TM_13_G_N         = 06425,
		/// <summary> Постановка на охрану по брелоку №14 </summary>
		TM_14_G_Y         = 06426,
		/// <summary> Снятие с охраны по брелоку №14 </summary>
		TM_14_G_N         = 06427,
		/// <summary> Постановка на охрану по брелоку №15 </summary>
		TM_15_G_Y         = 06428,
		/// <summary> Снятие с охраны по брелоку №15 </summary>
		TM_15_G_N         = 06429,
		/// <summary> Постановка на охрану по брелоку №16 </summary>
		TM_16_G_Y         = 06430,
		/// <summary> Снятие с охраны по брелоку №16 </summary>
		TM_16_G_N         = 06431,
		/// <summary> Постановка на охрану по брелоку №17 </summary>
		TM_17_G_Y         = 06432,
		/// <summary> Снятие с охраны по брелоку №17 </summary>
		TM_17_G_N         = 06433,
		/// <summary> Постановка на охрану по брелоку №18 </summary>
		TM_18_G_Y         = 06434,
		/// <summary> Снятие с охраны по брелоку №18 </summary>
		TM_18_G_N         = 06435,
		/// <summary> Постановка на охрану по брелоку №19 </summary>
		TM_19_G_Y         = 06436,
		/// <summary> Снятие с охраны по брелоку №19 </summary>
		TM_19_G_N         = 06437,
		/// <summary> Постановка на охрану по брелоку №20 </summary>
		TM_20_G_Y         = 06438,
		/// <summary> Снятие с охраны по брелоку №20 </summary>
		TM_20_G_N         = 06439,
		/// <summary> Постановка на охрану по брелоку №21 </summary>
		TM_21_G_Y         = 06440,
		/// <summary> Снятие с охраны по брелоку №21 </summary>
		TM_21_G_N         = 06441,
		/// <summary> Постановка на охрану по брелоку №22 </summary>
		TM_22_G_Y         = 06442,
		/// <summary> Снятие с охраны по брелоку №22 </summary>
		TM_22_G_N         = 06443,
		/// <summary> Постановка на охрану по брелоку №23 </summary>
		TM_23_G_Y         = 06444,
		/// <summary> Снятие с охраны по брелоку №23 </summary>
		TM_23_G_N         = 06445,
		/// <summary> Постановка на охрану по брелоку №24 </summary>
		TM_24_G_Y         = 06446,
		/// <summary> Снятие с охраны по брелоку №24 </summary>
		TM_24_G_N         = 06447,
		/// <summary> Постановка на охрану по брелоку №25 </summary>
		TM_25_G_Y         = 06448,
		/// <summary> Снятие с охраны по брелоку №25 </summary>
		TM_25_G_N         = 06449,
		/// <summary> Постановка на охрану по брелоку №26 </summary>
		TM_26_G_Y         = 06450,
		/// <summary> Снятие с охраны по брелоку №26 </summary>
		TM_26_G_N         = 06451,
		/// <summary> Постановка на охрану по брелоку №27 </summary>
		TM_27_G_Y         = 06452,
		/// <summary> Снятие с охраны по брелоку №27 </summary>
		TM_27_G_N         = 06453,
		/// <summary> Постановка на охрану по брелоку №28 </summary>
		TM_28_G_Y         = 06454,
		/// <summary> Снятие с охраны по брелоку №28 </summary>
		TM_28_G_N         = 06455,
		/// <summary> Постановка на охрану по брелоку №29 </summary>
		TM_29_G_Y         = 06456,
		/// <summary> Снятие с охраны по брелоку №29 </summary>
		TM_29_G_N         = 06457,
		/// <summary> Постановка на охрану по брелоку №30 </summary>
		TM_30_G_Y         = 06458,
		/// <summary> Снятие с охраны по брелоку №30 </summary>
		TM_30_G_N         = 06459,
		/// <summary> Постановка на охрану по брелоку №31 </summary>
		TM_31_G_Y         = 06460,
		/// <summary> Снятие с охраны по брелоку №31 </summary>
		TM_31_G_N         = 06461,
		/// <summary> Постановка на охрану по брелоку №32 </summary>
		TM_32_G_Y         = 06462,
		/// <summary> Снятие с охраны по брелоку №32 </summary>
		TM_32_G_N         = 06463,
		/// <summary> Постановка на охрану по брелоку №33 </summary>
		TM_33_G_Y         = 06464,
		/// <summary> Снятие с охраны по брелоку №33 </summary>
		TM_33_G_N         = 06465,
		/// <summary> Постановка на охрану по брелоку №34 </summary>
		TM_34_G_Y         = 06466,
		/// <summary> Снятие с охраны по брелоку №34 </summary>
		TM_34_G_N         = 06467,
		/// <summary> Постановка на охрану по брелоку №35 </summary>
		TM_35_G_Y         = 06468,
		/// <summary> Снятие с охраны по брелоку №35 </summary>
		TM_35_G_N         = 06469,
		/// <summary> Постановка на охрану по брелоку №36 </summary>
		TM_36_G_Y         = 06470,
		/// <summary> Снятие с охраны по брелоку №36 </summary>
		TM_36_G_N         = 06471,
		/// <summary> Постановка на охрану по брелоку №37 </summary>
		TM_37_G_Y         = 06472,
		/// <summary> Снятие с охраны по брелоку №37 </summary>
		TM_37_G_N         = 06473,
		/// <summary> Постановка на охрану по брелоку №38 </summary>
		TM_38_G_Y         = 06474,
		/// <summary> Снятие с охраны по брелоку №38 </summary>
		TM_38_G_N         = 06475,
		/// <summary> Постановка на охрану по брелоку №39 </summary>
		TM_39_G_Y         = 06476,
		/// <summary> Снятие с охраны по брелоку №39 </summary>
		TM_39_G_N         = 06477,
		/// <summary> Постановка на охрану по брелоку №40 </summary>
		TM_40_G_Y         = 06478,
		/// <summary> Снятие с охраны по брелоку №40 </summary>
		TM_40_G_N         = 06479,
		/// <summary> Постановка на охрану по брелоку №41 </summary>
		TM_41_G_Y         = 06480,
		/// <summary> Снятие с охраны по брелоку №41 </summary>
		TM_41_G_N         = 06481,
		/// <summary> Постановка на охрану по брелоку №42 </summary>
		TM_42_G_Y         = 06482,
		/// <summary> Снятие с охраны по брелоку №42 </summary>
		TM_42_G_N         = 06483,
		/// <summary> Постановка на охрану по брелоку №43 </summary>
		TM_43_G_Y         = 06484,
		/// <summary> Снятие с охраны по брелоку №43 </summary>
		TM_43_G_N         = 06485,
		/// <summary> Постановка на охрану по брелоку №44 </summary>
		TM_44_G_Y         = 06486,
		/// <summary> Снятие с охраны по брелоку №44 </summary>
		TM_44_G_N         = 06487,
		/// <summary> Постановка на охрану по брелоку №45 </summary>
		TM_45_G_Y         = 06488,
		/// <summary> Снятие с охраны по брелоку №45 </summary>
		TM_45_G_N         = 06489,
		/// <summary> Постановка на охрану по брелоку №46 </summary>
		TM_46_G_Y         = 06490,
		/// <summary> Снятие с охраны по брелоку №46 </summary>
		TM_46_G_N         = 06491,
		/// <summary> Постановка на охрану по брелоку №47 </summary>
		TM_47_G_Y         = 06492,
		/// <summary> Снятие с охраны по брелоку №47 </summary>
		TM_47_G_N         = 06493,
		/// <summary> Постановка на охрану по брелоку №48 </summary>
		TM_48_G_Y         = 06494,
		/// <summary> Снятие с охраны по брелоку №48 </summary>
		TM_48_G_N         = 06495,
		/// <summary> Постановка на охрану по брелоку №49 </summary>
		TM_49_G_Y         = 06496,
		/// <summary> Снятие с охраны по брелоку №49 </summary>
		TM_49_G_N         = 06497,
		/// <summary> Постановка на охрану по брелоку №50 </summary>
		TM_50_G_Y         = 06498,
		/// <summary> Снятие с охраны по брелоку №50 </summary>
		TM_50_G_N         = 06499,
		/// <summary> Постановка на охрану по брелоку №51 </summary>
		TM_51_G_Y         = 06500,
		/// <summary> Снятие с охраны по брелоку №51 </summary>
		TM_51_G_N         = 06501,
		/// <summary> Постановка на охрану по брелоку №52 </summary>
		TM_52_G_Y         = 06502,
		/// <summary> Снятие с охраны по брелоку №52 </summary>
		TM_52_G_N         = 06503,
		/// <summary> Постановка на охрану по брелоку №53 </summary>
		TM_53_G_Y         = 06504,
		/// <summary> Снятие с охраны по брелоку №53 </summary>
		TM_53_G_N         = 06505,
		/// <summary> Постановка на охрану по брелоку №54 </summary>
		TM_54_G_Y         = 06506,
		/// <summary> Снятие с охраны по брелоку №54 </summary>
		TM_54_G_N         = 06507,
		/// <summary> Постановка на охрану по брелоку №55 </summary>
		TM_55_G_Y         = 06508,
		/// <summary> Снятие с охраны по брелоку №55 </summary>
		TM_55_G_N         = 06509,
		/// <summary> Постановка на охрану по брелоку №56 </summary>
		TM_56_G_Y         = 06510,
		/// <summary> Снятие с охраны по брелоку №56 </summary>
		TM_56_G_N         = 06511,
		/// <summary> Постановка на охрану по брелоку №57 </summary>
		TM_57_G_Y         = 06512,
		/// <summary> Снятие с охраны по брелоку №57 </summary>
		TM_57_G_N         = 06513,
		/// <summary> Постановка на охрану по брелоку №58 </summary>
		TM_58_G_Y         = 06514,
		/// <summary> Снятие с охраны по брелоку №58 </summary>
		TM_58_G_N         = 06515,
		/// <summary> Постановка на охрану по брелоку №59 </summary>
		TM_59_G_Y         = 06516,
		/// <summary> Снятие с охраны по брелоку №59 </summary>
		TM_59_G_N         = 06517,
		/// <summary> Постановка на охрану по брелоку №60 </summary>
		TM_60_G_Y         = 06518,
		/// <summary> Снятие с охраны по брелоку №60 </summary>
		TM_60_G_N         = 06519,
		/// <summary> Постановка на охрану по брелоку №61 </summary>
		TM_61_G_Y         = 06520,
		/// <summary> Снятие с охраны по брелоку №61 </summary>
		TM_61_G_N         = 06521,
		/// <summary> Постановка на охрану по брелоку №62 </summary>
		TM_62_G_Y         = 06522,
		/// <summary> Снятие с охраны по брелоку №62 </summary>
		TM_62_G_N         = 06523,
		/// <summary> Постановка на охрану по брелоку №63 </summary>
		TM_63_G_Y         = 06524,
		/// <summary> Снятие с охраны по брелоку №63 </summary>
		TM_63_G_N         = 06525,
		/// <summary> Постановка на охрану по брелоку №64 </summary>
		TM_64_G_Y         = 06526,
		/// <summary> Снятие с охраны по брелоку №64 </summary>
		TM_64_G_N         = 06527,
		/// <summary> Событие прикладывания ключа </summary>
		TM_PUT            = 06655,
		/// <summary> Событие отсутствия ключей на шине </summary>
		TM_CLR            = 06656,
		/// <summary> Прикладывание зарегистрированного ключа </summary>
		TM_KEY_X          = 08192,
		/// <summary> Прикладывание ключа 1 </summary>
		TM_KEY_1          = 08193,
		/// <summary> Прикладывание ключа 2 </summary>
		TM_KEY_2          = 08194,
		/// <summary> Прикладывание ключа 3 </summary>
		TM_KEY_3          = 08195,
		/// <summary> Прикладывание ключа 4 </summary>
		TM_KEY_4          = 08196,
		/// <summary> Прикладывание ключа 5 </summary>
		TM_KEY_5          = 08197,
		/// <summary> Прикладывание ключа 6 </summary>
		TM_KEY_6          = 08198,
		/// <summary> Прикладывание ключа 7 </summary>
		TM_KEY_7          = 08199,
		/// <summary> Прикладывание ключа 8 </summary>
		TM_KEY_8          = 08200,
		/// <summary> Прикладывание ключа 9 </summary>
		TM_KEY_9          = 08201,
		/// <summary> Прикладывание ключа 10 </summary>
		TM_KEY_10         = 08202,
		/// <summary> Прикладывание ключа 11 </summary>
		TM_KEY_11         = 08203,
		/// <summary> Прикладывание ключа 12 </summary>
		TM_KEY_12         = 08204,
		/// <summary> Прикладывание ключа 13 </summary>
		TM_KEY_13         = 08205,
		/// <summary> Прикладывание ключа 14 </summary>
		TM_KEY_14         = 08206,
		/// <summary> Прикладывание ключа 15 </summary>
		TM_KEY_15         = 08207,
		/// <summary> Прикладывание ключа 16 </summary>
		TM_KEY_16         = 08208,
		/// <summary> Прикладывание ключа 17 </summary>
		TM_KEY_17         = 08209,
		/// <summary> Прикладывание ключа 18 </summary>
		TM_KEY_18         = 08210,
		/// <summary> Прикладывание ключа 19 </summary>
		TM_KEY_19         = 08211,
		/// <summary> Прикладывание ключа 20 </summary>
		TM_KEY_20         = 08212,
		/// <summary> Прикладывание ключа 21 </summary>
		TM_KEY_21         = 08213,
		/// <summary> Прикладывание ключа 22 </summary>
		TM_KEY_22         = 08214,
		/// <summary> Прикладывание ключа 23 </summary>
		TM_KEY_23         = 08215,
		/// <summary> Прикладывание ключа 24 </summary>
		TM_KEY_24         = 08216,
		/// <summary> Прикладывание ключа 25 </summary>
		TM_KEY_25         = 08217,
		/// <summary> Прикладывание ключа 26 </summary>
		TM_KEY_26         = 08218,
		/// <summary> Прикладывание ключа 27 </summary>
		TM_KEY_27         = 08219,
		/// <summary> Прикладывание ключа 28 </summary>
		TM_KEY_28         = 08220,
		/// <summary> Прикладывание ключа 29 </summary>
		TM_KEY_29         = 08221,
		/// <summary> Прикладывание ключа 30 </summary>
		TM_KEY_30         = 08222,
		/// <summary> Прикладывание ключа 31 </summary>
		TM_KEY_31         = 08223,
		/// <summary> Прикладывание ключа 32 </summary>
		TM_KEY_32         = 08224,
		/// <summary> Прикладывание ключа 33 </summary>
		TM_KEY_33         = 08225,
		/// <summary> Прикладывание ключа 34 </summary>
		TM_KEY_34         = 08226,
		/// <summary> Прикладывание ключа 35 </summary>
		TM_KEY_35         = 08227,
		/// <summary> Прикладывание ключа 36 </summary>
		TM_KEY_36         = 08228,
		/// <summary> Прикладывание ключа 37 </summary>
		TM_KEY_37         = 08229,
		/// <summary> Прикладывание ключа 38 </summary>
		TM_KEY_38         = 08230,
		/// <summary> Прикладывание ключа 39 </summary>
		TM_KEY_39         = 08231,
		/// <summary> Прикладывание ключа 40 </summary>
		TM_KEY_40         = 08232,
		/// <summary> Прикладывание ключа 41 </summary>
		TM_KEY_41         = 08233,
		/// <summary> Прикладывание ключа 42 </summary>
		TM_KEY_42         = 08234,
		/// <summary> Прикладывание ключа 43 </summary>
		TM_KEY_43         = 08235,
		/// <summary> Прикладывание ключа 44 </summary>
		TM_KEY_44         = 08236,
		/// <summary> Прикладывание ключа 45 </summary>
		TM_KEY_45         = 08237,
		/// <summary> Прикладывание ключа 46 </summary>
		TM_KEY_46         = 08238,
		/// <summary> Прикладывание ключа 47 </summary>
		TM_KEY_47         = 08239,
		/// <summary> Прикладывание ключа 48 </summary>
		TM_KEY_48         = 08240,
		/// <summary> Прикладывание ключа 49 </summary>
		TM_KEY_49         = 08241,
		/// <summary> Прикладывание ключа 50 </summary>
		TM_KEY_50         = 08242,
		/// <summary> Прикладывание ключа 51 </summary>
		TM_KEY_51         = 08243,
		/// <summary> Прикладывание ключа 52 </summary>
		TM_KEY_52         = 08244,
		/// <summary> Прикладывание ключа 53 </summary>
		TM_KEY_53         = 08245,
		/// <summary> Прикладывание ключа 54 </summary>
		TM_KEY_54         = 08246,
		/// <summary> Прикладывание ключа 55 </summary>
		TM_KEY_55         = 08247,
		/// <summary> Прикладывание ключа 56 </summary>
		TM_KEY_56         = 08248,
		/// <summary> Прикладывание ключа 57 </summary>
		TM_KEY_57         = 08249,
		/// <summary> Прикладывание ключа 58 </summary>
		TM_KEY_58         = 08250,
		/// <summary> Прикладывание ключа 59 </summary>
		TM_KEY_59         = 08251,
		/// <summary> Прикладывание ключа 60 </summary>
		TM_KEY_60         = 08252,
		/// <summary> Прикладывание ключа 61 </summary>
		TM_KEY_61         = 08253,
		/// <summary> Прикладывание ключа 62 </summary>
		TM_KEY_62         = 08254,
		/// <summary> Прикладывание ключа 63 </summary>
		TM_KEY_63         = 08255,
		/// <summary> Прикладывание ключа 64 </summary>
		TM_KEY_64         = 08256,
		/// <summary> Уведомление о смене параметров </summary>
		UPSET_NOT         = 08449,
		/// <summary> Уведомление о смене прошивки </summary>
		UPFRM_NOT         = 08450,
		/// <summary> Уведомление о недостаточном балансе лицевого счета </summary>
		BLNS_NOT          = 08451,
		/// <summary> Уведомление о не отвечающем сервере </summary>
		CNCT_ERR          = 08452,
		/// <summary> Уведомление об ошибке при скачивании прошивки </summary>
		FIRM_ERR          = 08453,
		/// <summary> Уведомление о том, что версия прошивки последняя </summary>
		FIRM_LAST         = 08454,
		/// <summary> Автопостановка на охрану </summary>
		AUTO_GUARD        = 08705,
		/// <summary> Автоматическая перепостановка на охрану </summary>
		AUTO_REGRD        = 08706,
		/// <summary> Смена настроек по SMS формата №1 </summary>
		SET               = 08960,
		/// <summary> Смена настроек по SMS формата №2 </summary>
		SET_1             = 08961,
		/// <summary> Смена настроек по SMS формата №3 </summary>
		SET_2             = 08962,
		/// <summary> Смена настроек по SMS формата №4 </summary>
		SET_3             = 08963,
		/// <summary> Смена настроек ключей Touch Memory по SMS </summary>
		SET_TM            = 08964,
		/// <summary> Отправка уведомления о смене настроек по SMS формата №1 </summary>
		GET               = 09216,
		/// <summary> Отправка уведомления о смене настроек по SMS формата №2 </summary>
		GET_1             = 09217,
		/// <summary> Отправка уведомления о смене настроек по SMS формата №3 </summary>
		GET_2             = 09218,
		/// <summary> Отправка уведомления о смене настроек ключей Touch Memory по SMS </summary>
		GET_TM            = 09219,
		/// <summary> Выход из спящего режима </summary>
		PW_WAKEUP         = 09472,
		/// <summary> Переход в спящий режим </summary>
		PW_SLEEP          = 09473,
		/// <summary> Отключение устройства </summary>
		PW_TURNOFF        = 09474,
		/// <summary> Изменение состояния водителя </summary>
		DRV_ST_CHG        = 09485,
		/// <summary> Получено сообщение для водителя </summary>
		DD_M_REC          = 09486,
		/// <summary> Сообщение прочитано водителем </summary>
		DD_M_READ         = 09487,
		/// <summary> Состояние водителя сменилось на «На вызове» </summary>
		DRV_S0            = 09488,
		/// <summary> Состояние водителя сменилось на «В рейсе» </summary>
		DRV_S1            = 09489,
		/// <summary> Состояние водителя сменилось на «Свободен» </summary>
		DRV_S2            = 09490,
		/// <summary> Состояние водителя сменилось на «Ожидание» </summary>
		DRV_S3            = 09491,
		/// <summary> Состояние водителя сменилось на «Возвращение» </summary>
		DRV_S4            = 09492,
		/// <summary> Состояние водителя сменилось на «Резерв» </summary>
		DRV_S5            = 09493,
		/// <summary> Состояние водителя сменилось на «В работе» </summary>
		DRV_S6            = 09494,
		/// <summary> Состояние водителя сменилось на «Перерыв» </summary>
		DRV_S7            = 09495,
		/// <summary> Состояние водителя сменилось на «Готовность» </summary>
		DRV_S8            = 09496,
		/// <summary> Состояние водителя сменилось на «Обед» </summary>
		DRV_S9            = 09497,
		/// <summary> Состояние водителя сменилось на «Отдых» </summary>
		DRV_S10           = 09498,
		/// <summary> Состояние водителя сменилось на «Ремонт» </summary>
		DRV_S11           = 09499,
		/// <summary> Состояние водителя сменилось на «Загрузка» </summary>
		DRV_S12           = 09500,
		/// <summary> Состояние водителя сменилось на «Разгрузка» </summary>
		DRV_S13           = 09501,
		/// <summary> Состояние водителя сменилось на «Поломка» </summary>
		DRV_S14           = 09502,
		/// <summary> Состояние водителя сменилось на «ДТП» </summary>
		DRV_S15           = 09503,
		/// <summary> Монитор давления в шинах: системная тревога. </summary>
		P_SYS             = 09504,
		/// <summary> Датчик давления в шинах: критическая тревога (например, разряд батареи) </summary>
		P_SENS            = 09505,
		/// <summary> Подъём температуры шины выше первого установленного порога </summary>
		P_HIGH1           = 09506,
		/// <summary> Подъём температуры шины выше второго установленного порога </summary>
		P_HIGH2           = 09507,
		/// <summary> Падение давления в шине ниже первого установленного порога </summary>
		P_LOW1            = 09508,
		/// <summary> Падение давления в шине ниже второго установленного порога </summary>
		P_LOW2            = 09509,
		/// <summary> Подъём давления в шине выше установленного порога </summary>
		P_HIGH            = 09510,
		/// <summary> Тахограф: Установка карты в слот №1 </summary>
		TA_C1_IN          = 09520,
		/// <summary> Тахограф: Установка карты в слот №2 </summary>
		TA_C2_IN          = 09521,
		/// <summary> Тахограф: Изменение состояния карты №1 </summary>
		TA_C1_S           = 09522,
		/// <summary> Тахограф: Изменение состояния карты №2 </summary>
		TA_C2_S           = 09523,
		/// <summary> Тахограф: Изменение режима работы водителя №1 </summary>
		TA_DRV1_M         = 09524,
		/// <summary> Тахограф: Изменение режима работы водителя №2 </summary>
		TA_DRV2_M         = 09525,
		/// <summary> Тахограф: Нарушение скоростного режима движения (превышение скорости) </summary>
		TA_O_SPEED        = 09526,
		/// <summary> Тахограф: Нарушение режима работы водителя (превышение времени непрерывного вождения) </summary>
		TA_O_TIME         = 09527,
		/// <summary> Тахограф: Включение криптованного датчика скорости </summary>
		TA_CIPF_ON        = 09528,
		/// <summary> Тахограф: Отключение криптованного датчика скорости </summary>
		TA_CIPF_OFF       = 09529,
		/// <summary> Тахограф: Распечатка с принтера </summary>
		TA_PRINT          = 09530,
		/// <summary> Тахограф: Отключение тахографа (переход тахографа в энергосберегающий режим / прерывание обмена данными по инициативе тахографа) </summary>
		TA_OFF            = 09531,
		/// <summary> Тахограф: Ошибка при работе с тахографом (ошибка тахографа) </summary>
		TA_ERROR          = 09532,
		/// <summary> Событие от алкозамка </summary>
		ALCO              = 09552,
		/// <summary> Тахометр: Обороты двигателя превысили заданную границу </summary>
		TAH_UP            = 09728,
		/// <summary> Тахометр: Обороты двигателя понизились ниже заданной границы </summary>
		TAH_DAWN          = 09729,
		/// <summary> Тахометр: Оброты двигателя повысились на заданное значение </summary>
		TAH_T_UP          = 09730,
		/// <summary> Тахометр: Оброты двигателя понизились на заданное значение </summary>
		TAH_T_DAWN        = 09731,
		/// <summary> Старт отсчёта для счётчика импульсов 1 </summary>
		PULSE_START1      = 09732,
		/// <summary> Окончание отсчёта для счётчика импульсов 1 </summary>
		PULSE_STOP1       = 09733,
		/// <summary> Промежуточное значение счётчика импульсов 1 </summary>
		PULSE_WORK1       = 09734,
		/// <summary> Старт отсчёта для счётчика импульсов 2 </summary>
		PULSE_START2      = 09735,
		/// <summary> Окончание отсчёта для счётчика импульсов 2 </summary>
		PULSE_STOP2       = 09736,
		/// <summary> Промежуточное значение счётчика импульсов 2 </summary>
		PULSE_WORK2       = 09737,
		/// <summary> Радиометка активна </summary>
		RFID_PUT          = 09984,
		/// <summary> Радиометка неактивна </summary>
		RFID_CLR          = 09985,
		/// <summary> Датчик пассажиропотока: начало подсчета </summary>
		PCNT_BGN          = 10496,
		/// <summary> Датчик пассажиропотока: окончание подсчета </summary>
		PCNT_END          = 10497,
		/// <summary> Датчик пассажиропотока: переполнение счетчика </summary>
		PCNT_OVRFLW       = 10498,
		/// <summary> Датчик пассажиропотока: ошибка </summary>
		PCNT_ERROR        = 10499,
		/// <summary> Неподдерживаемая команда </summary>
		C_NOT             = 13107,
		/// <summary> Запросы EGTS </summary>
		EGTS_REQ          = 13568,
		/// <summary> Снятие с охраны по первому таймеру </summary>
		TMR_G_N1          = 16384,
		/// <summary> Постановка на охрану по первому таймеру по профилю №1 </summary>
		TMR_G_Y1          = 16385,
		/// <summary> Снятие с охраны по второму таймеру </summary>
		TMR_G_N2          = 16400,
		/// <summary> Постановка на охрану по второму таймеру по профилю №1 </summary>
		TMR_G_Y2          = 16401,
		/// <summary> Запрос модели и версии </summary>
		R_V               = 40960,
		/// <summary> Запрос баланса л/с SIM-карты </summary>
		R_B               = 40961,
		/// <summary> Запрос текущего состояния объекта </summary>
		R_A               = 40962,
		/// <summary> Запрос информации из DF на ближайший момент времени перед интересующим </summary>
		R_L               = 40963,
		/// <summary> Запрос информации из DF на ближайший момент времени после интересующего </summary>
		R_R               = 40964,
		/// <summary> Запрос информации из DF по индексу </summary>
		R_I               = 40965,
		/// <summary> Запрос кода последнего введенного ключа Touch Memory </summary>
		R_TM              = 40966,
		/// <summary> Запрос текущего состояния акселерометра (с получением состояния внутреннего прерывания) </summary>
		R_ACL2            = 40967,
		/// <summary> Запрос режима (по voice dtmf) </summary>
		R_MD              = 40968,
		/// <summary> Запрос индивидуального идентификатора устройства </summary>
		R_S               = 40969,
		/// <summary> Запрос телефонов абонентов голосового оповещения </summary>
		R_PVD             = 40970,
		/// <summary> Запрос телефонов абонентов стандартных SMS </summary>
		R_PSMS_ST         = 40971,
		/// <summary> Запрос телефонов абонентов пользовательских SMS </summary>
		R_PSMS_U          = 40972,
		/// <summary> Запрос имен абонентов голосового дозвона </summary>
		R_NVD             = 40973,
		/// <summary> Запрос имен абонентов стандартных SMS </summary>
		R_NSMS_ST         = 40974,
		/// <summary> Запрос имен абонентов пользовательских SMS </summary>
		R_NSMS_U          = 40975,
		/// <summary> Запрос на уровни напряжения на входах и данных акселерометра </summary>
		R_MEASU           = 40976,
		/// <summary> Запрос названия звуковой темы в устройстве </summary>
		R_SNDTH           = 40977,
		/// <summary> Запрос значений топливных датчиков </summary>
		R_FUEL            = 40978,
		/// <summary> Запрос количества спутников </summary>
		R_SAT             = 40979,
		/// <summary> Запрос текущего значения дисперсии напряжения в бортовой сети </summary>
		R_DISP            = 40980,
		/// <summary> Запрос значений топливных датчиков </summary>
		R_FUEL_UN         = 40981,
		/// <summary> Запрос позиции устройства (LBS) </summary>
		E_LBS             = 40982,
		/// <summary> Запрос версии модуля ключей TM </summary>
		R_TM_V            = 40983,
		/// <summary> Запрос прошивки с сервера GPRS </summary>
		R_FIRM            = 41008,
		/// <summary> Запрос очередной страницы с сервера </summary>
		R_WPAGE           = 41009,
		/// <summary> Запрос на соединение с конфигуратором у службы RCS </summary>
		R_IDCHNL          = 41010,
		/// <summary> Команда/запрос калибровки акселерометра </summary>
		ACL_C             = 41011,
		/// <summary> Запрос ревизии устройства </summary>
		R_REV             = 41012,
		/// <summary> Запрос информации о внешнем датчике наклона </summary>
		R_EDAS            = 41013,
		/// <summary> Запрос радиометки </summary>
		R_RFID            = 41014,
		/// <summary> Асинхронный ответ на команду/запрос калибровки акселерометра </summary>
		ACL_R             = 41015,
		/// <summary> Команда/запрос модулю фиксации ДТП (снятие защиты с файла) </summary>
		C_RAI             = 41016,
		/// <summary> Событие фиксации ДТП: лёгкое ДТП </summary>
		RAI_L             = 41017,
		/// <summary> Событие фиксации ДТП: тяжёлое ДТП </summary>
		RAI_H             = 41018,
		/// <summary> Событие завершения формирования файла с профилем ДТП </summary>
		RAI_FILE          = 41019,
		/// <summary> Запрос данных холодильной установки </summary>
		C_REFR            = 41023,
		/// <summary> Запрос данных цифровой камеры </summary>
		DCAM_RQS          = 41024,
		/// <summary> Запрос данных тахографа </summary>
		TACH_RQS          = 41025,
		/// <summary> Команда файловой системе SD-карты </summary>
		SDFS_CMD          = 41026,
		/// <summary> Команда хранилищу flash памяти </summary>
		C_DEP             = 41027,
		/// <summary> Команда зуммеру </summary>
		C_BEEP            = 41028,
		/// <summary> Запрос версии прошивки модема </summary>
		GPS_FIRM          = 41029,
		/// <summary> Весовой индикатор: вес стабилен </summary>
		WIND_STB          = 41030,
		/// <summary> MODBUS: запрос ОК </summary>
		MDBS_OK           = 41031,
		/// <summary> Запрос статуса устройства </summary>
		R_STAT            = 41033,
		/// <summary> Запрос ICCID </summary>
		R_ICCID           = 41034,
		/// <summary> Запрос текущего режима охраны </summary>
		R_GUARD           = 41041,
		/// <summary> Включение О1 </summary>
		C_O1_Y            = 41232,
		/// <summary> Выключение О1 </summary>
		C_O1_N            = 41234,
		/// <summary> Включение О2 </summary>
		C_O2_Y            = 41248,
		/// <summary> Выключение О2 </summary>
		C_O2_N            = 41250,
		/// <summary> Включение О3 </summary>
		C_O3_Y            = 41264,
		/// <summary> Выключение О3 </summary>
		C_O3_N            = 41266,
		/// <summary> Включение О4 </summary>
		C_O4_Y            = 41280,
		/// <summary> Выключение О4 </summary>
		C_O4_N            = 41282,
		/// <summary> Команда на включение микрофона </summary>
		C_MIC             = 41472,
		/// <summary> Команда на подключение к GPRS-Интернет </summary>
		C_GPRS            = 41473,
		/// <summary> Команда на переход в тестовый режим </summary>
		C_TM              = 41474,
		/// <summary> Команда на запись страницы DF </summary>
		C_W_PAGE          = 41475,
		/// <summary> Команда на чтение страницы DF </summary>
		C_R_PAGE          = 41476,
		/// <summary> Команда записи программы с последующей немедленной перезагрузкой </summary>
		C_U               = 41477,
		/// <summary> Команда записи настроек с последующей перезагрузкой </summary>
		C_P               = 41478,
		/// <summary> Команда на выход из режима прослушивания </summary>
		C_NOTMIC          = 41479,
		/// <summary> Команда на разрешение входящего CSD </summary>
		C_ACSD            = 41480,
		/// <summary> Команда на разрешение входящего VD при подключенном GPRS </summary>
		C_AVD             = 41481,
		/// <summary> Команда сброса устройства и последующей перезагрузки </summary>
		C_RST             = 41482,
		/// <summary> Команда коннекта к серверу для смены прошивки </summary>
		C_GPUPFRM         = 41483,
		/// <summary> Команда коннекта к серверу для общения с конфигуратором </summary>
		C_GPNTCNT         = 41484,
		/// <summary> Команда сброса максимальных значений акселерометра </summary>
		C_ACCRES          = 41485,
		/// <summary> Команда сброса настроек </summary>
		C_SETTRES         = 41486,
		/// <summary> Команда очистки черного ящика </summary>
		C_BBOXRES         = 41487,
		/// <summary> Передача данных в прозрачном режиме </summary>
		C_RETRANS         = 41488,
		/// <summary> Подтверждение передачи данных в прозрачном режиме </summary>
		A_RETRANS         = 41489,
		/// <summary> Команда установки пробега </summary>
		С_MLG             = 41490,
		/// <summary> Команда установки программы CAN-LOG </summary>
		C_CNPRG           = 41491,
		/// <summary> Команда настройки внутреннего прерывания акселерометра (для вывода устройства из режима энергосбережения) </summary>
		C_ACLI            = 41492,
		/// <summary> Команда настройки отладочного режима </summary>
		C_LOG             = 41495,
		/// <summary> Команда автоинформатору </summary>
		AINF_CMD          = 41496,
		/// <summary> Команда цифровой камере </summary>
		DCAM_CMD          = 41497,
		/// <summary> Команда тахографу </summary>
		TACH_CMD          = 41498,
		/// <summary> Отправка тестового email </summary>
		EMAIL_SEND        = 41499,
		/// <summary> Запрос кода разблокировки (для ЕНДС) </summary>
		C_UNLOCK          = 41500,
		/// <summary> Команда повторной отправки телеметрии (включая ч/я на SD) </summary>
		C_REPET           = 41501,
		/// <summary> Запись страницы бинарного файла ключей TM </summary>
		C_TM_W            = 41504,
		/// <summary> Чтение страницы бинарного файла ключей TM </summary>
		C_TM_R            = 41505,
		/// <summary> Обновление бинарного файла ключей TM </summary>
		C_TM_U            = 41506,
		/// <summary> Команда/запрос работы с приоритетом SIM-карты </summary>
		C_CH_SIM          = 41512,
		/// <summary> Произвольные команды RS </summary>
		C_USER_RS         = 41513,
		/// <summary> Подтверждение приема телеметрии </summary>
		A_T               = 41729,
		/// <summary> Подтверждение хендшейка </summary>
		A_S               = 41730,
		/// <summary> Подтверждение приема массива телеметрических сообщений </summary>
		A_TA              = 41731,
		/// <summary> Ответ на сообщение FLEX </summary>
		A_FLEX            = 41732,
		/// <summary> Ответ на команду файловой системе SD-карты </summary>
		SDFS_ANS          = 41733,
		/// <summary> Ответ на команду автоинформатору </summary>
		AINF_ANS          = 41734,
		/// <summary> Оповещение автоинформатора </summary>
		AINF_NTF          = 41735,
		/// <summary> Ответ на команду цифровой камере </summary>
		DCAM_ANS          = 41736,
		/// <summary> Оповещение цифровой камеры </summary>
		DCAM_NTF          = 41737,
		/// <summary> Ответ на команду тахографу </summary>
		TACH_ANS          = 41738,
		/// <summary> Передача данных в прозрачном режиме </summary>
		S_RETRANS         = 41744,
		/// <summary> Отправка ответа отправителю данных в прозрачном режиме </summary>
		R_RETRANS         = 41745,
		/// <summary> Снятие с охраны по команде </summary>
		C_G_N             = 41984,
		/// <summary> Постановка на охрану по команде по профилю №1 </summary>
		C_G_Y             = 41985,
		/// <summary> Постановка на охрану по команде по профилю №2 </summary>
		C_G_2             = 41986,
		/// <summary> Постановка на охрану по команде по профилю №3 </summary>
		C_G_3             = 41987,
		/// <summary> Команда на задание уставки температуры для первого датчика </summary>
		C_T1              = 42241,
		/// <summary> Команда на задание уставки температуры для второго датчика </summary>
		C_T2              = 42242,
		/// <summary> Команда на задание уставки температуры для третьего датчика </summary>
		C_T3              = 42243,
		/// <summary> Команда на задание уставки температуры для четвертого датчика </summary>
		C_T4              = 42244,
		/// <summary> Команда на сброс двухпроводных датчиков на шлейфе I5 </summary>
		C_RESTW1          = 42501,
		/// <summary> Команда на сброс двухпроводных датчиков на шлейфе I6 </summary>
		C_RESTW2          = 42502,
		/// <summary> Команда на блокирование контура управления T1 </summary>
		CL_T1             = 42753,
		/// <summary> Команда на блокирование контура управления T2 </summary>
		CL_T2             = 42754,
		/// <summary> Команда на блокирование контура управления T3 </summary>
		CL_T3             = 42755,
		/// <summary> Команда на блокирование контура управления T4 </summary>
		CL_T4             = 42756,
		/// <summary> Команда на разблокирование контура управления T1 </summary>
		CU_T1             = 43009,
		/// <summary> Команда на разблокирование контура управления T2 </summary>
		CU_T2             = 43010,
		/// <summary> Команда на разблокирование контура управления T3 </summary>
		CU_T3             = 43011,
		/// <summary> Команда на разблокирование контура управления T4 </summary>
		CU_T4             = 43012,
		/// <summary> Блокирование датчика №1 по команде </summary>
		IN1_LOCK          = 45057,
		/// <summary> Блокирование датчика №2 по команде </summary>
		IN2_LOCK          = 45058,
		/// <summary> Блокирование датчика №3 по команде </summary>
		IN3_LOCK          = 45059,
		/// <summary> Блокирование датчика №4 по команде </summary>
		IN4_LOCK          = 45060,
		/// <summary> Блокирование датчика №5 по команде </summary>
		IN5_LOCK          = 45061,
		/// <summary> Блокирование датчика №6 по команде </summary>
		IN6_LOCK          = 45062,
		/// <summary> Блокирование датчика №7 по команде </summary>
		IN7_LOCK          = 45063,
		/// <summary> Блокирование датчика №8 по команде </summary>
		IN8_LOCK          = 45064,
		/// <summary> Блокирование датчика №9 по команде </summary>
		IN9_LOCK          = 45065,
		/// <summary> Разблокирование датчика №1 по команде </summary>
		IN1_UNLOCK        = 45073,
		/// <summary> Разблокирование датчика №2 по команде </summary>
		IN2_UNLOCK        = 45074,
		/// <summary> Разблокирование датчика №3 по команде </summary>
		IN3_UNLOCK        = 45075,
		/// <summary> Разблокирование датчика №4 по команде </summary>
		IN4_UNLOCK        = 45076,
		/// <summary> Разблокирование датчика №5 по команде </summary>
		IN5_UNLOCK        = 45077,
		/// <summary> Разблокирование датчика №6 по команде </summary>
		IN6_UNLOCK        = 45078,
		/// <summary> Разблокирование датчика №7 по команде </summary>
		IN7_UNLOCK        = 45079,
		/// <summary> Разблокирование датчика №8 по команде </summary>
		IN8_UNLOCK        = 45080,
		/// <summary> Разблокирование датчика №9 по команде </summary>
		IN9_UNLOCK        = 45081,
		/// <summary> Блокирование всех абонентов стандартных SMS по команде </summary>
		PH_STLCK          = 45088,
		/// <summary> Блокирование абонента №2 стандартных SMS по команде </summary>
		PH2_STLCK         = 45090,
		/// <summary> Блокирование абонента №3 стандартных SMS по команде </summary>
		PH3_STLCK         = 45091,
		/// <summary> Блокирование абонента №4 стандартных SMS по команде </summary>
		PH4_STLCK         = 45092,
		/// <summary> Блокирование абонента №5 стандартных SMS по команде </summary>
		PH5_STLCK         = 45093,
		/// <summary> Разблокирование всех абонентов стандартных SMS по команде </summary>
		PH_STULCK         = 45104,
		/// <summary> Разблокирование абонента №2 стандартных SMS по команде </summary>
		PH2_STULCK        = 45106,
		/// <summary> Разблокирование абонента №3 стандартных SMS по команде </summary>
		PH3_STULCK        = 45107,
		/// <summary> Разблокирование абонента №4 стандартных SMS по команде </summary>
		PH4_STULCK        = 45108,
		/// <summary> Разблокирование абонента №5 стандартных SMS по команде </summary>
		PH5_STULCK        = 45109,
		/// <summary> Блокирование всех абонентов пользовательских SMS по команде </summary>
		PH_ULCK           = 45120,
		/// <summary> Блокирование абонента №2 пользовательских SMS по команде </summary>
		PH2_ULCK          = 45122,
		/// <summary> Блокирование абонента №3 пользовательских SMS по команде </summary>
		PH3_ULCK          = 45123,
		/// <summary> Блокирование абонента №4 пользовательских SMS по команде </summary>
		PH4_ULCK          = 45124,
		/// <summary> Блокирование абонента №5 пользовательских SMS по команде </summary>
		PH5_ULCK          = 45125,
		/// <summary> Разблокирование всех абонентов пользовательских SMS по команде </summary>
		PH_UULCK          = 45136,
		/// <summary> Разблокирование абонента №2 пользовательских SMS по команде </summary>
		PH2_UULCK         = 45138,
		/// <summary> Разблокирование абонента №3 пользовательских SMS по команде </summary>
		PH3_UULCK         = 45139,
		/// <summary> Разблокирование абонента №4 пользовательских SMS по команде </summary>
		PH4_UULCK         = 45140,
		/// <summary> Разблокирование абонента №5 пользовательских SMS по команде </summary>
		PH5_UULCK         = 45141,
		/// <summary> Блокирование всех абонентов голосового оповещения по команде </summary>
		PH_VDLCK          = 45152,
		/// <summary> Блокирование абонента №1 голосового оповещения по команде </summary>
		PH1_VDLCK         = 45153,
		/// <summary> Блокирование абонента №2 голосового оповещения по команде </summary>
		PH2_VDLCK         = 45154,
		/// <summary> Блокирование абонента №3 голосового оповещения по команде </summary>
		PH3_VDLCK         = 45155,
		/// <summary> Блокирование абонента №4 голосового оповещения по команде </summary>
		PH4_VDLCK         = 45156,
		/// <summary> Блокирование абонента №5 голосового оповещения по команде </summary>
		PH5_VDLCK         = 45157,
		/// <summary> Разблокирование всех абонентов голосового оповещения по команде </summary>
		PH_VDULCK         = 45168,
		/// <summary> Разблокирование абонента №1 голосового оповещения по команде </summary>
		PH1_VDULCK        = 45169,
		/// <summary> Разблокирование абонента №2 голосового оповещения по команде </summary>
		PH2_VDULCK        = 45170,
		/// <summary> Разблокирование абонента №3 голосового оповещения по команде </summary>
		PH3_VDULCK        = 45171,
		/// <summary> Разблокирование абонента №4 голосового оповещения по команде </summary>
		PH4_VDULCK        = 45172,
		/// <summary> Разблокирование абонента №5 голосового оповещения по команде </summary>
		PH5_VDULCK        = 45173,
		/// <summary> Набор последнего номера с гарнитуры Bluetooth </summary>
		PH_CALL           = 45184,
		/// <summary> Команда модулю Bluetooth </summary>
		BT_CMD            = 45185,
		/// <summary> Команда отмены дальнейшего голосового оповещения </summary>
		DIALCSL           = 49152,
		/// <summary> Ввод пароля от хоста </summary>
		PSS_APT           = 49408,
		/// <summary> Неправильный пароль </summary>
		PSS_NCK           = 49409,
		/// <summary> Передача текущего состояния системы (пинг) </summary>
		PING              = 65280,
	}
}