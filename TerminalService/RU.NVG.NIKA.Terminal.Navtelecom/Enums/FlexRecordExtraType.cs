﻿namespace RU.NVG.NIKA.Terminal.Navtelecom.Enums
{
	public enum FlexRecordExtraType : byte
	{
		/// <summary> 0x01 – ключ Touch Memory </summary>
		TouchMemory = 0x01,
		/// <summary> 0x02 – карта водителя 1,2 (тахограф), номер карты зависит от кода события </summary>
		DriverCard = 0x02,
		/// <summary> 0x03 – код радиометки RFID </summary>
		RfidTag    = 0x03,
	}
}