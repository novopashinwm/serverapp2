﻿namespace RU.NVG.NIKA.Terminal.Navtelecom.Enums
{
	public enum NtcbRecordType
	{
		Handshake,
		FlexConfiguration,
		AskCurrentState,
		TouchMemory,
	}
}