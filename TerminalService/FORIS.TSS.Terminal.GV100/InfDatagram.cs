﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.GV100
{
	public class InfDatagram : BaseDatagram
	{
		public DateTime? Time;
		//public bool? Ignition;
		public int? AN1;
		public byte? DS_in;
		public byte? DS_out;
		public bool? Tow;
		public bool? MoveSensor;
		public int? BackupBatteryVCC;
		public bool? MainSupply;


		public InfDatagram(string data)
			: base(data)
		{
			Parse();
		}

		private void Parse()
		{
			if (DataParts == null || DataParts.Length == 0)
				return;

			ID = GetDataPart(InfDataParts.ID);

			//Дата Время
			DateTime time;
			if (DateTime.TryParseExact(GetDataPart(InfDataParts.SendTime), "yyyyMMddHHmmss",
									   CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out time))
			{
				Time = time;

				var State = (States)byte.Parse(GetDataPart(InfDataParts.State), NumberStyles.HexNumber);
				switch (State)
				{
					case States.Tow:
						Tow = true;
						break;
					case States.EngineOffAndFakeTow:
						//Ignition = false;
						break;
					case States.EngineOffAndMove:
						//Ignition = false;
						MoveSensor = true;
						break;
					case States.EngineOffAndStillness:
						//Ignition = false;
						MoveSensor = false;
						break;
					case States.EngineOnAndMove:
						//Ignition = true;
						MoveSensor = true;
						break;
					case States.EngineOnAndStillness:
						//Ignition = true;
						MoveSensor = false;
						break;
					case States.SensorMove:
						MoveSensor = true;
						break;
					case States.SensorStillness:
						MoveSensor = false;
						break;
				}

				AN1 = (int)double.Parse(GetDataPart(InfDataParts.AN1), CultureInfo.InvariantCulture);
				DS_in = byte.Parse(GetDataPart(InfDataParts.DigitalInput), NumberStyles.HexNumber);
				DS_out = byte.Parse(GetDataPart(InfDataParts.DigitalOutput), NumberStyles.HexNumber);
				MainSupply = GetDataPart(InfDataParts.MainSupply) == "1";
				if (!MainSupply.Value)
					BackupBatteryVCC = (int)(double.Parse(GetDataPart(InfDataParts.BackupBatteryVCC), CultureInfo.InvariantCulture) * 10);
			}
		}

		private string GetDataPart(InfDataParts part)
		{
			return DataParts[(int)part];
		}

		//GPRMC формат:  hhmmss.dd,S,xxmm.dddd,<N|S>,yyymm.dddd,<E|W>,s.s,h.h,ddmmyy,d.d,D*HH
		protected enum InfDataParts
		{
			Command = 0,
			ProtocolVersion = 1,
			ID = 2,
			Empty = 3,
			State = 4,
			ICCID = 5,
			CSQ_RSSI = 6,
			CSQ_BER = 7,
			MainSupply = 8,
			Reserved = 9,
			BackupBatteryOn = 10,
			BackupBatteryVCC = 11,
			Charging = 12,
			LED_on = 13,
			GPS_OnNeed = 14,
			GPS_Antenna = 15,
			LastFixUTCTime = 16,
			AN1 = 17,
			DigitalInput = 18,
			DigitalOutput = 19,
			TimeZoneOffset = 20,
			DaylightSaving = 21,
			SendTime = 22,
			CountNumber = 23
		}

		protected enum States : byte
		{
			Tow = 0x16,
			EngineOffAndFakeTow = 0x1A,
			EngineOffAndStillness = 0x11,
			EngineOffAndMove = 0x12,
			EngineOnAndStillness = 0x21,
			EngineOnAndMove = 0x22,
			SensorStillness = 0x41,
			SensorMove = 0x42,
		}

		public override IList GetResponses()
		{
			if (Time == null || !Time.HasValue)
				return null;

			var res = new ArrayList();

			var mu = new MobilUnit.Unit.MobilUnit
			{
				cmdType = CmdType.Trace,
				DeviceID = ID,
				Time = BusinessLogic.TimeHelper.GetSecondsFromBase(Time.Value),
				Satellites = 0,
				CorrectGPS = false
			};
			mu.Properties.Add(DeviceProperty.Protocol, GV100.ProtocolName);

			if (AN1.HasValue) mu.VoltageAN1 = AN1.Value;

			//Заполняем массив показаний датчиков
			mu.SensorValues = new Dictionary<int, long>();

			if (AN1.HasValue) mu.SensorValues.Add((int)Sensors.AN1, AN1.Value);
			//if (Ignition.HasValue) mu.SensorValues.Add((int)Sensors.Ignition, Ignition.Value?1:0);
			if (Tow.HasValue) mu.SensorValues.Add((int)Sensors.Tow, Tow.Value ? 1 : 0);
			if (MoveSensor.HasValue) mu.SensorValues.Add((int)Sensors.MoveSensor, MoveSensor.Value ? 1 : 0);
			if (MainSupply.HasValue) mu.SensorValues.Add((int)Sensors.MainSupply, MainSupply.Value ? 1 : 0);
			if (BackupBatteryVCC.HasValue) mu.SensorValues.Add((int)Sensors.BackupBatteryVCC, BackupBatteryVCC.Value);

			if (DS_in.HasValue)
			{
				mu.SensorValues.Add((int)Sensors.D1, (DS_in.Value & 0x01) == 0x01 ? 1 : 0);
				mu.SensorValues.Add((int)Sensors.D2, (DS_in.Value & 0x02) == 0x02 ? 1 : 0);
				mu.SensorValues.Add((int)Sensors.D3, (DS_in.Value & 0x04) == 0x04 ? 1 : 0);
				mu.SensorValues.Add((int)Sensors.D4, (DS_in.Value & 0x08) == 0x08 ? 1 : 0);
				mu.SensorValues.Add((int)Sensors.D5, (DS_in.Value & 0x10) == 0x10 ? 1 : 0);
			}

			if (DS_out.HasValue)
			{
				mu.SensorValues.Add((int)Sensors.D_OUT1, (DS_out.Value & 0x01) == 0x01 ? 1 : 0);
				mu.SensorValues.Add((int)Sensors.D_OUT2, (DS_out.Value & 0x02) == 0x02 ? 1 : 0);
				mu.SensorValues.Add((int)Sensors.D_OUT3, (DS_out.Value & 0x04) == 0x04 ? 1 : 0);
				mu.SensorValues.Add((int)Sensors.D_OUT4, (DS_out.Value & 0x08) == 0x08 ? 1 : 0);
			}

			res.Add(mu);
			return res;
		}


		public enum Sensors
		{
			AN1 = 1,
			D1 = 11,
			D2 = 12,
			D3 = 13,
			D4 = 14,
			D5 = 15,
			D_OUT1 = 20,
			D_OUT2 = 21,
			D_OUT3 = 22,
			D_OUT4 = 23,
			//Ignition = 30,
			Tow = 40,
			MoveSensor = 50,
			MainSupply = 60,
			BackupBatteryVCC = 70,
		}
	}
}