﻿using System;

namespace FORIS.TSS.Terminal.GV100
{
	public class SttDatagram : PositionDatagram
	{
		public SttDatagram(string data) : base(data)
		{
		}

		protected override string GetDataPart(PositionDataParts part)
		{
			//конвертируем enum (они совпадают по названию значений)
			if (!Enum.IsDefined(typeof(SttPositionDataParts), part.ToString()))
			{
				return string.Empty;
			}

			if (part == PositionDataParts.HDOP)
			{
				return "10";
			}

			var partStt = (SttPositionDataParts)Enum.Parse(typeof(SttPositionDataParts), part.ToString());
			return DataParts[(int)partStt];
		}

		private enum SttPositionDataParts
		{
			Command = 0,
			ProtocolVersion = 1,
			ID = 2,
			State = 4,
			HDOP = 5,
			Speed = 6,
			Azimuth = 7,
			Altitude = 8,
			Longitude = 9,
			Latitude = 10,
			Time = 11,
			MCC = 12,
			MNC = 13,
			LAC = 14,
			CellID = 15,
			TimingAdvance = 16,
			SendTime = 17,
			CountNumber = 18
		}
	}
}