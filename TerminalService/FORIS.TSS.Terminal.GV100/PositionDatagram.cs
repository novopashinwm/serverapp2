﻿using System;
using System.Collections;
using System.Globalization;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.GV100
{
	public class PositionDatagram : BaseDatagram
	{
		public DateTime? Time;
		public bool GPSCorrect;
		public double? Lat;
		public double? Lng;
		public int? Speed;
		public byte? HDOP;
		/// <summary>
		/// Высота в сантиметрах
		/// </summary>
		public int? Altitude;

		public PositionDatagram(string data) : base(data)
		{
			if (IsCorrectCRC)
			{
				Parse();
			}
		}

		private void Parse()
		{
			if (DataParts == null || DataParts.Length == 0)
				return;

			ID = GetDataPart(PositionDataParts.ID);

			//Дата Время
			DateTime time;
			if (DateTime.TryParseExact(GetDataPart(PositionDataParts.Time), "yyyyMMddHHmmss",
									   CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out time))
			{
				Time = time;

				HDOP = byte.Parse(GetDataPart(PositionDataParts.HDOP));

				GPSCorrect = HDOP > 0;

				if (GPSCorrect)
				{
					Lat = double.Parse(GetDataPart(PositionDataParts.Latitude), CultureInfo.InvariantCulture);
					Lng = double.Parse(GetDataPart(PositionDataParts.Longitude), CultureInfo.InvariantCulture);
					Speed = (int)(double.Parse(GetDataPart(PositionDataParts.Speed), CultureInfo.InvariantCulture));
					Altitude = (int)(double.Parse(GetDataPart(PositionDataParts.Altitude), CultureInfo.InvariantCulture)) * 100;
				}
			}
		}

		protected virtual string GetDataPart(PositionDataParts part)
		{
			return DataParts[(int)part];
		}

		protected enum PositionDataParts
		{
			Command = 0,
			ProtocolVersion = 1,
			ID = 2,
			ReportID_AN1 = 4,
			ReportType = 5,
			Number = 6,
			HDOP = 7,
			Speed = 8,
			Azimuth = 9,
			Altitude = 10,
			Longitude = 11,
			Latitude = 12,
			Time = 13,
			MCC = 14,
			MNC = 15,
			LAC = 16,
			CellID = 17,
			TimingAdvance = 18,
			Mileage = 19,
			SendTime = 20,
			CountNumber = 21
		}

		public override IList GetResponses()
		{
			if (Time == null || !Time.HasValue)
				return null;

			var res = new ArrayList();

			var mu = new MobilUnit.Unit.MobilUnit
			{
				cmdType = CmdType.Trace,
				DeviceID = ID,
				Time = BusinessLogic.TimeHelper.GetSecondsFromBase(Time.Value),
				CorrectGPS = GPSCorrect,
				Satellites = 0
			};
			mu.Properties.Add(DeviceProperty.Protocol, GV100.ProtocolName);

			if (GPSCorrect)
			{
				mu.Latitude = Lat.Value;
				mu.Longitude = Lng.Value;
				mu.Satellites = HDOP.HasValue ? HDOP.Value : (byte)10;
				if (Speed.HasValue) mu.Speed = Speed.Value;
				if (Altitude.HasValue) mu.Height = Altitude.Value;
			}

			res.Add(mu);
			return res;
		}

	}
}