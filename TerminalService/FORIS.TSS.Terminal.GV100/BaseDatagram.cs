﻿using System.Collections;

namespace FORIS.TSS.Terminal.GV100
{
	public abstract class BaseDatagram
	{
		protected string Command;
		protected bool IsCorrectCRC = true;
		protected string ID;
		protected string[] DataParts;

		public BaseDatagram(string data)
		{
			Command = data.Substring(8, 3).ToUpper();
			DataParts = data.Split(',');
		}

		public abstract IList GetResponses();
	}
}