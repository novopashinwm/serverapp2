﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace FORIS.TSS.Terminal.GV100
{
	public static class DataProcessor
	{
		private static List<string> SplitDataToPackets(byte[] data, out byte[] bufferRest)
		{
			bufferRest = null;
			var res = new List<string>();
			if (data == null || data.Length == 0)
				return null;

			var strVal = Encoding.ASCII.GetString(data);
			var parts = strVal.Split(new string[] { "$" }, StringSplitOptions.RemoveEmptyEntries);

			if (!strVal.EndsWith("$"))
			{
				bufferRest = Encoding.ASCII.GetBytes(parts[parts.Length - 1]);
				res = parts.ToList().Where(str => str != parts[parts.Length - 1]).ToList();
			}
			else
			{
				res = parts.ToList();
			}

			return res;
		}

		public static IList Process(byte[] data, out byte[] bufferRest)
		{
			var packets = SplitDataToPackets(data, out bufferRest);

			if (packets == null || packets.Count == 0)
				return null;

			var alRes = new ArrayList();

			foreach (var packet in packets)
			{
				try
				{
					var responses = ProcessPacket(packet);
					if (responses != null && responses.Count > 0)
					{
						alRes.AddRange(responses);
					}
				}
				catch (Exception ex)
				{
					Trace.WriteLine(string.Format("Error in GV100 during process data: {0}", Encoding.ASCII.GetString(data)));
					Trace.WriteLine(ex.ToString());
					Trace.WriteLine(ex.StackTrace);
					if (ex.InnerException != null)
						Trace.WriteLine(ex.InnerException.ToString());

				}
			}

			return alRes;
		}

		private static IList ProcessPacket(string packet)
		{
			var command = packet.Substring(8, 3).ToUpper();

			BaseDatagram datagram = null;
			switch (command)
			{
				case "TOW":
				case "AIS":
				case "DIS":
				case "IOB":
				case "FRI":
				case "GEO":
				case "SPD":
				case "SOS":
				case "RTL":
					datagram = new PositionDatagram(packet);
					break;

				case "INF":
					datagram = new InfDatagram(packet);
					break;

				case "STT":
					datagram = new SttDatagram(packet);
					break;
			}
			if (datagram != null)
				return datagram.GetResponses();

			return null;
		}
	}
}