﻿using System.Collections;
using System.Text;

namespace FORIS.TSS.Terminal.GV100
{
	public class GV100 : Device
	{
		internal const string ProtocolName = "Queclink GV100";
		internal static class DeviceNames
		{
			internal const string QueclinkGV100 = "Queclink GV100";
		}
		public GV100() : base(null, new[]
		{
			DeviceNames.QueclinkGV100,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			if (data == null) return false;

			/*
			+RESP:GTXXX,<param1>,<param2>,...$
			*/
			string strVal = Encoding.ASCII.GetString(data);
			if (strVal.Length > 12 && (strVal.StartsWith("+RESP:GT") || strVal.StartsWith("+BUFF:GT")) && strVal.EndsWith("$"))
				return true;

			return false;
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			return Parse(data, out bufferRest);
		}
		public IList Parse(byte[] data, out byte[] bufferRest)
		{
			return DataProcessor.Process(data, out bufferRest);
		}
	}
}