using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Collections;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using System.Diagnostics;
using System.Data;
using System.Threading;
using System.IO;

namespace FORIS.TSS.Terminal.TM140
{
    public class TM140 : Device
    {
        private readonly TraceSwitch tm140TraceSwitch = new TraceSwitch("TM140", "Info from TM140 device");

        private readonly Timer VtoTtimer;

        public TM140()
        {
            LoadVtoT();

            VtoTtimer = new Timer(VtoTtimer_Callback, null, VtoTPeriod * 1000, VtoTPeriod*1000);
        }

        private void VtoTtimer_Callback(object state)
        {
            try
            {
                FileInfo fileInfo = new FileInfo(VtoTPath);
                if(DateTime.Now - fileInfo.LastWriteTime <= TimeSpan.FromSeconds(VtoTPeriod-1))
                {
                    LoadVtoT();
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLineIf(tm140TraceSwitch.TraceError, ex.ToString(), tm140TraceSwitch.DisplayName);
            }
        }

         ~TM140()
        {
            VtoTtimer.Dispose();
        }

        #region IDevice Members

        public override byte[] AdjustSettings()
        {
            const string text = "$PFAL,Sys.Timer0.Configure=cyclic,10000\r\n" +
                                "$PFAL,sys.timer0.start\r\n";

            Trace.WriteLine("TM140:  AdjustSettings() Sending text: " + text);

            return Encoding.ASCII.GetBytes(text);
        }


        public override byte[] GetCmd(IStdCommand cmd)
        {
            switch (cmd.Type)
            {
                case CmdType.SetSettings:
                    {
                        #region ���������

                        if (cmd.Params.ContainsKey("DEBUG") && (string) cmd.Params["DEBUG"] == "ON")
                        {
                            byte[] data = Encoding.ASCII.GetBytes("$PMTSK,GSM,S,DEBUG=ON");
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("DEBUG") && (string)cmd.Params["DEBUG"] == "OFF")
                        {
                            byte[] data = Encoding.ASCII.GetBytes("$PMTSK,GSM,S,DEBUG=OFF");
                            data[0] = 0x02; //$
                            return data;
                        }

                        if (cmd.Params.ContainsKey("Custom"))
                        {
                            string str = (string)cmd.Params["Custom"];
                            str = str.Replace('_', '\x11');
                            str = str.Replace('$', '\x02');
                            byte[] data = Encoding.ASCII.GetBytes(str);
                            //data[0] = 0x02; //$

                            return data;
                        }

                        if (cmd.Params.ContainsKey("Phone"))
                        {
                            byte[] data =
                                Encoding.ASCII.GetBytes("$PMTSK,GSM,S,ADMINTELNUM=" + (string) cmd.Params["Phone"]);
                            data[0] = 0x02; //$
                            return data;
                        }

                        if (cmd.Params.ContainsKey("EntryPoint"))
                        {
                            byte[] data =
                                Encoding.ASCII.GetBytes("$PMTSK,GSM,S,APN=" + (string) cmd.Params["EntryPoint"]);
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("Login"))
                        {
                            byte[] data = Encoding.ASCII.GetBytes("$PMTSK,GSM,S,NAME=" + (string) cmd.Params["Login"]);
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("Password"))
                        {
                            byte[] data =
                                Encoding.ASCII.GetBytes("$PMTSK,GSM,S,PASS= " + (string) cmd.Params["Password"]);
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("INTERVAL"))
                        {
                            byte[] data =
                                Encoding.ASCII.GetBytes("$PMTSK,TIMER,S,RMCGM,#" + (int) cmd.Params["INTERVAL"]);
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("IntervalGoodPositionMoving"))
                        {
                            byte[] data =
                                Encoding.ASCII.GetBytes("$PMTSK,TIMER,S,RMCGM,#" +
                                                        (int) cmd.Params["IntervalGoodPositionMoving"]);
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("IntervalBadPosition"))
                        {
                            byte[] data =
                                Encoding.ASCII.GetBytes("$PMTSK,TIMER,S,RMCGI,#" +
                                                        (int) cmd.Params["IntervalBadPosition"]);
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("IntervalGoodPositionStop"))
                        {
                            byte[] data =
                                Encoding.ASCII.GetBytes("$PMTSK,TIMER,S,RMCGS,#" +
                                                        (int) cmd.Params["IntervalGoodPositionStop"]);
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("DivisorBadPosition"))
                        {
                            byte[] data =
                                Encoding.ASCII.GetBytes("$PMTSK,TIMER,S,RMCFI,#" +
                                                        (int) cmd.Params["DivisorBadPosition"]);
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("DivisorGoodPositionMove"))
                        {
                            byte[] data =
                                Encoding.ASCII.GetBytes("$PMTSK,TIMER,S,RMCFM,#" +
                                                        (int) cmd.Params["DivisorGoodPositionMove"]);
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("DivisorGoodPositionStop"))
                        {
                            byte[] data =
                                Encoding.ASCII.GetBytes("$PMTSK,TIMER,S,RMCFS,#" +
                                                        (int) cmd.Params["DivisorGoodPositionStop"]);
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("AutomaticDetalization"))
                        {
                            string str = "$PMTSK,BB,RNS_TX_MODE " + ((bool) cmd.Params["AutomaticDetalization"] ? 1 : 0);
                            str = str.Replace('_', '\x11');
                            byte[] data = Encoding.ASCII.GetBytes(str);
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("BBDecimationMode"))
                        {
                            byte[] data =
                                Encoding.ASCII.GetBytes(
                                    ("$PMTSK,BB,SAMPLE_MODE " + (int) cmd.Params["BBDecimationMode"]).Replace('_',
                                                                                                              '\x11'));
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("BlackBoxAutomaticMode"))
                        {
                            byte[] data =
                                Encoding.ASCII.GetBytes(
                                    ("$PMTSK,BB,TX_MODE " + ((bool) cmd.Params["BlackBoxAutomaticMode"] ? 1 : 0)).
                                        Replace('_', '\x11'));
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("MissedPointsDecimationPoints"))
                        {
                            byte[] data =
                                Encoding.ASCII.GetBytes(
                                    ("$PMTSK,BB,SAMPLE_POINT " + (int) cmd.Params["MissedPointsDecimationPoints"]).
                                        Replace('_', '\x11'));
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("MissedSecondsDecimationTime"))
                        {
                            byte[] data =
                                Encoding.ASCII.GetBytes(
                                    ("$PMTSK,BB,SAMPLE_TIME " + (int) cmd.Params["MissedSecondsDecimationTime"]).Replace
                                        ('_', '\x11'));
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("OffVoltageThreshold"))
                        {
                            byte[] data =
                                Encoding.ASCII.GetBytes("$PMTSK,TIMER,S,UOFF,#" +
                                                        (int) cmd.Params["OffVoltageThreshold"]);
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("SleepModeVoltageThreshold"))
                        {
                            byte[] data =
                                Encoding.ASCII.GetBytes("$PMTSK,TIMER,S,UPIT,#" +
                                                        (int) cmd.Params["SleepModeVoltageThreshold"]);
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("TimerRecordSleepMode"))
                        {
                            byte[] data =
                                Encoding.ASCII.GetBytes("$PMTSK,TIMER,S,TECONOM,#" +
                                                        (int) cmd.Params["TimerRecordSleepMode"]);
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("TimerSleepMode"))
                        {
                            byte[] data =
                                Encoding.ASCII.GetBytes("$PMTSK,TIMER,S,TOFF,#" + (int) cmd.Params["TimerSleepMode"]);
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("SleepModeEnable"))
                        {
                            byte[] data =
                                Encoding.ASCII.GetBytes("$PMTSK,TIMER,S,ECONOM,#" + (int)cmd.Params["SleepModeEnable"]);
                            data[0] = 0x02; //$
                            return data;
                        }
                        if (cmd.Params.ContainsKey("SERVER_IP") && cmd.Params.ContainsKey("SERVER_PORT"))
                        {
                            string ip = (string) cmd.Params["SERVER_IP"];
                            int port = (int) cmd.Params["SERVER_PORT"];
                            string[] digits = ip.Split('.');
                            for (int i = 0; i < digits.Length; i++)
                            {
                                if (digits[i].Length == 2) digits[i] = "0" + digits[i];
                                if (digits[i].Length == 1) digits[i] = "00" + digits[i];
                            }
                            ip = string.Format("{0}.{1}.{2}.{3}:{4}", digits[0], digits[1], digits[2], digits[3], port);
                            byte[] data = Encoding.ASCII.GetBytes("$PMTSK,GSM,S,IPS=" + ip);
                            data[0] = 0x02; //$
                            return data;
                        }

                        #endregion //���������
                    }
                    break;
                case CmdType.GetLog:
                    {
                        /*
                         * ������ ������� �������:
                         *   $PMTSK,BB,DDS 11.04.08 10:00-11.04.08 11:30
                         */
                        DateTime dtFrom = TimeHelper.year1970.AddSeconds((int) cmd.Params["FromTime"]);
                        DateTime dtTo = TimeHelper.year1970.AddSeconds((int) cmd.Params["ToTime"]);
                        string strFrom = dtFrom.ToString("dd.MM.yy HH:mm");
                        string strTo = dtTo.ToString("dd.MM.yy HH:mm");
                        string strGetLog = string.Format("$PMTSK,BB,DDS {0}-{1}", strFrom, strTo);

                        byte[] data = Encoding.ASCII.GetBytes(strGetLog);
                        data[0] = 0x02;
                        return data;
                    }
                default:
                    break;
            }
            return null;
        }

        public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
        {
            bufferRest = null;
            string sData = Encoding.ASCII.GetString(data, 0, count);


            if (sData.Contains("WARNING!!!"))
            {
                ArrayList ares = new ArrayList(1)
                    {
                        new NotifyEventArgs((time_t) DateTime.Now.ToUniversalTime(),
                                            TerminalEventMessage.NE_ALARM, null,
                                            Severity.Lvl1, NotifyCategory.Controller, "�������",
                                            null, new object[4])
                    };
                return ares;
            }
            string[] strings = sData.Split(new[] {"<end>\r\n"}, StringSplitOptions.RemoveEmptyEntries);

            var aRes = new ArrayList(4);
            foreach (string s in strings)
            {
                byte[] d = Encoding.ASCII.GetBytes(s);
                int pos = s.IndexOf("$GPRMC", StringComparison.Ordinal);

                if (pos == -1) continue;

                TM140Datagram dg = TM140Datagram.Create(d);

                double t = VtoT(dg.Analog2);
                int T = ((int) (t*10 + 5))/10;

                var mu = UnitReceived();
                mu.DeviceID = dg.IMEI;
                mu.cmdType = dg.Log ? CmdType.GetLog : CmdType.Trace;
                mu.Time = dg.Time;
                mu.Latitude = dg.Latitude;
                mu.Longitude = dg.Longitude;
                mu.Speed = dg.Speed;
                mu.Satellites = 10;
                mu.CorrectGPS = dg.CorrectPos;
                mu.VoltageAN4 = Convert.ToInt32(dg.Fuel);
                mu.PowerVoltage = Convert.ToInt32(dg.Volatage);
                mu.VoltageAN1 = t > int.MinValue ? T : int.MinValue;
                mu.SensorValues = new Dictionary<int, long>
                {
                    {(int) TM140Sensor.Ignition, dg.Ignition ? 1 : 0},
                    {(int) TM140Sensor.Alarm, dg.SOS ? 1 : 0},
                    {(int) TM140Sensor.PowerVoltage, (long) dg.Volatage},
                    {(int) TM140Sensor.Fuel, (long) dg.Fuel},
                };

                aRes.Add(mu);

                if (!dg.SOS) continue;
                IUnitInfo ui = UnitInfo.Factory.Create(UnitInfoPurpose.TM140);
                ui.DeviceID = mu.DeviceID;
                object[] par = new object[4];
                par[0] = ui;
                par[1] = "";
                par[2] = 1;
                par[3] = 1;
                aRes.Add(new NotifyEventArgs((time_t) mu.Time, TerminalEventMessage.NE_ALARM, null,
                                             Severity.Lvl1, NotifyCategory.Controller, "�������", null, par));
            }

            return aRes;
        }

        public override bool SupportData(byte[] data)
        {
            string msg = Encoding.ASCII.GetString(data);
            string[] message = msg.Split('$');

            foreach (string str in message)
            {
                if (str.StartsWith("PGID"))
                    return true;
            }

            return false;
        }

        #endregion

        #region Temperature


        private readonly SortedList<double, double> VtoTlist = new SortedList<double, double>();
        private string VtoTPath
        {
            get { return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "VtoT.xml"); }
        }
         
        private const int VtoTPeriod = 60;

        void LoadVtoT()
        {
            try
            {
                Debug.WriteLine("Load VtoT...");
                DataSet ds = new DataSet();
                ds.ReadXml(VtoTPath);

                lock (VtoTlist)
                {
                    VtoTlist.Clear();

                    foreach (DataRow row in ds.Tables["VtoT"].Rows)
                    {
                        double V = double.Parse((string)row["V"], CultureInfo.InvariantCulture);
                        double T = double.Parse((string)row["T"], CultureInfo.InvariantCulture);
                        VtoTlist[V] = T;
#if DEBUG
                        Trace.WriteLine("Load VtoT. V = "+V+"\tT = "+T);
#endif
                    }
                }
#if DEBUG
                Trace.WriteLine("VtoT table length: " + VtoTlist.Count);
#endif
            }
            catch (Exception ex)
            {
                Trace.WriteLineIf(tm140TraceSwitch.TraceError, ex.ToString(), tm140TraceSwitch.DisplayName);
            }
        }

        /// <summary>
        /// ����������� ���������� ������� ����������� � �����������
        /// </summary>
        /// <param name="v">����������, � ������������</param>
        /// <returns></returns>
        double VtoT(double v)
        {
            if(v == 0.0)
                return int.MinValue;
            lock (VtoTlist)
            {
                if (VtoTlist.Count == 0)
                    return 0.0;

                double V = v/1000.0;
                if (V < VtoTlist.Keys[0])
                    return VtoTlist.Keys[0];
                if (V > VtoTlist.Keys[VtoTlist.Keys.Count - 1])
                    return VtoTlist.Keys[VtoTlist.Keys.Count - 1];
                
                for(int i=0; i < VtoTlist.Keys.Count-1; i++)
                {
                    if(V>=VtoTlist.Keys[i] && V<=VtoTlist.Keys[i+1])
                    {
                        return VtoTlist[VtoTlist.Keys[i]] +
                               ((VtoTlist[VtoTlist.Keys[i + 1]] - VtoTlist[VtoTlist.Keys[i]])/
                                (VtoTlist.Keys[i + 1] - VtoTlist.Keys[i]))*(V - VtoTlist.Keys[i]);
                    }
                }
            }

            return 0.0;
        }


        #endregion //Temperature
    }
}