using System;
using System.Globalization;
using System.Text;
using FORIS.TSS.Communication;
using FORIS.TSS.Common;

namespace FORIS.TSS.Terminal.TM140
{

//**************************************************
// �������� ��������� TM140
//**************************************************

//$PGID,M0AK0521*47
//$GPRMC,074946.891,A,5505.9808,N,06008.1432,E,0.16,207.39,210208,,,A*61
//$GPIOP,00000000,00000000,0.00,0.00,10.93*48
//$<end>

//������ ������, ������������� ������� � ����.  
//$PGID,M0AK0521*47
//M0AK0521 � ��� ������������� ������� � ���� GPRS (����������)
//*47 � ����������� �����

//������ ������, ��������� � ������� NMEA-0183
//$GPRMC,074946.891,A,5505.9808,N,06008.1432,E,0.16,207.39,210208,,,A*61

//������ ������, ��������� ������
//$GPIOP,00000000,00000000,0.00,0.00,10.93*48
//���.   76543210           �1   �2   �3

//*48 � ����������� �����
//������������� �� ��������
//7 -  ���� ING,      (������� 6 -�1)
//6 -  ������� �������� ���������� �� �������� �����
//       0 � �������� on-line
//       1 � ������ �� �������� �����
//5 -  ������� ������� �� ��������� ������
//       0 � ��� ������� 
//       1 � ��������� ������� �� ��������� ������
//4 -  ������
//3 � ���� IN4,      (������� 9 � �1)
//2 � ���� IN3,      (������� 8 ��1) 
//1 � ���� IN2,      (������� 7 -�1)
//0 � ���� IN1,      (������� 6 -�1)
//�1   - ���������� ���� 0 (������� 3 � �1)
//�2   - ���������� ���� 1 (������� 4 � �1)
//�3   - ���������� ������� �������� ����

//�������� ������, ���������� ���������.
//$<end>

//**************************************************
//**************************************************
//**************************************************

    public class TM140Datagram : Datagram
    {
        public static TM140Datagram Create(byte[] data)
        {
            string msg = Encoding.ASCII.GetString(data);
            string[] message = msg.Split('$');

            foreach (string str in message)
            {
                if (str.StartsWith("GPRMC"))
                {
                    return new TM140Datagram(message);
                }
            }

            return null;
        }

        protected string[] m_message;
        
		readonly GPRMC rmc;
        protected GPIOP iop;
		readonly PGID pgid;

        public TM140Datagram(string[] message)
        {
            m_message = message;

            foreach (string str in message)
            {
				if (str.StartsWith("PGID"))
				{
					pgid = new PGID(str);
				}
				if (str.StartsWith("GPRMC"))
                {
                    rmc = new GPRMC(str);
                }
                if (str.StartsWith("GPIOP"))
                {
                    iop = new GPIOP(str);
                }
                
            }
        }

        protected TM140Datagram()
        {
            
        }

        // <summary>
        /// response time
        /// </summary>
        public virtual int Time
        {
            get
            {
                return rmc.Time;
            }
        }

        /// <summary>
        /// unit latitude. negative - south latitude
        /// </summary>
        public virtual float Latitude
        {
            get
            {
                return rmc.North ? rmc.Latitude : -rmc.Latitude;
            }
        }

        /// <summary>
        /// unit longitude. negative - south longitude
        /// </summary>
        public virtual float Longitude
        {
            get
            {
                return rmc.East ? rmc.Longitude : -rmc.Longitude;
            }
        }

        /// <summary>
        /// unit speed
        /// </summary>
        public virtual int Speed
        {
            get
            {
                return rmc.Speed;
            }
        }

        /// <summary>
        /// pos is valid
        /// </summary>
        public virtual bool CorrectPos
        {
            get
            {
                return rmc.CorrectPos && rmc.Speed < MobilUnit.Unit.MobilUnit.GPSLimitMaxSpeed;
            }
        }

        public string IMEI
        {
            get
            {
                foreach (string str in m_message)
                {
                    if (str.StartsWith("IMEI"))
                    {
                        string s = str.Substring(str.IndexOf('=')+1);
                        if(s.IndexOf('*')>0)
                            s = s.Remove(s.IndexOf('*'));
                        return s;
                    }
                }

				// ���� IMEI ���, �� ���������� ID ����������
            	return ID;
            }
        }

		public string ID
		{
			get
			{
			    return pgid != null ? pgid.ID : String.Empty;
			}
		}

        public bool Log
        {
            get { return iop != null && iop.Log; }
        }

        public bool Ignition
        {
            get
            {
                return iop != null && iop.Ignition;
            }
        }

        public bool SOS
        {
            get
            {
                return iop != null && iop.SOS;
            }
        }

    	public double Fuel
    	{
    		get
    		{
    		    return iop != null ? iop.Fuel : 0f;
    		}
    	}

        public double Analog2
        {
            get
            {
                if (iop != null)
                    return iop.Analog2;
                return 0f;
            }
        }

        public double Volatage
        {
            get
            {
                if (iop != null)
                    return iop.Voltage;
                return 0f;
            }
        }
    }

    /// <summary>
    /// GPRMC wrapper
    /// </summary>
    /// <example>
    /// GPRMC,095838.000,V,2458.9733,N,12125.6583,E,0.41,79.21,220905,,*30
    /// </example>
    class GPRMC
    {
        readonly string[] arsFields;

        public int Size
        {
            get
            {
                return 66;
            }
        }

        public GPRMC(string data)
        {
            arsFields = data.Split(',');
            if (!(arsFields.Length == 13 || arsFields.Length == 12))
            {
                throw new DatagramStructureException("Provided data not GPRMC or unknown version", GetType(),
                                                     new byte[1], 0);
            }
        }


        public time_t Time
        {
            get
            {
                string time = arsFields[1], date = arsFields[9];
                if (!(time.Length == 10 || time.Length == 11 || time.Length == 9) || date.Length != 6) throw new ApplicationException(
                                                               "Time must be 10 symbols, date - 6");
                return new DateTime(int.Parse("20" + date.Substring(4, 2)), byte.Parse(date.Substring(2, 2)),
                    byte.Parse(date.Substring(0, 2)), byte.Parse(time.Substring(0, 2)),
                    byte.Parse(time.Substring(2, 2)), byte.Parse(time.Substring(4, 2)),
                    int.Parse(time.Substring(7)));
            }
        }

        public bool North
        {
            get
            {
                if (!this.CorrectPos)
                    return true;

                string N = arsFields[4];
                if (N != "N" && N != "S")
                    throw new DatagramStructureException("Latitude must be North or South", GetType(), new byte[1], 0);
                return N == "N";
            }
        }

        public float Latitude
        {
            get
            {
                if (!this.CorrectPos)
                    return 0;

                string lat = arsFields[3];
                if (lat.Length != 9)
                    throw new ApplicationException("Latitude must be 9 symbols");
                int g = byte.Parse(lat.Substring(0, 2));
                string point = CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
                float m = float.Parse(lat.Substring(2, 6).Replace(".", point));
                return g + m / 60;
            }
        }

        public bool East
        {
            get
            {
                if (!this.CorrectPos)
                    return true;

                string E = arsFields[6];
                if (E != "E" && E != "W")
                    throw new DatagramStructureException("Longitude must be East or West", GetType(), new byte[1], 0);
                return E == "E";
            }
        }

        public float Longitude
        {
            get
            {
                if (!this.CorrectPos)
                    return 0;

                string lon = arsFields[5];
                if (lon.Length != 10)
                    throw new ApplicationException("Longitude must be 10 symbols");
                int g = byte.Parse(lon.Substring(0, 3));
                string point = CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
                float m = float.Parse(lon.Substring(3, 6).Replace(".", point));
                return g + m / 60;
            }
        }

        /// <summary>
        /// 1 marine knot in km/h
        /// </summary>
        const double MarineKnot = 1.852;	// 50. / 27 = 1.(851)

        /// <summary>
        /// unit speed
        /// </summary>
        public int Speed
        {
            get
            {
                if (!this.CorrectPos)
                    return 0;

                string point = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.PercentDecimalSeparator;
                return (int)(double.Parse(arsFields[7].Replace(".", point)) * MarineKnot);
            }
        }

        /// <summary>
        /// unit movement direction
        /// </summary>
        public int Course
        {
            get
            {
                return !this.CorrectPos ? 0 : int.Parse(arsFields[8]);
            }
        }

        /// <summary>
        /// A - data valid, V - navigation receiver warning
        /// </summary>
        public bool CorrectPos
        {
            get
            {
                return arsFields[2] == "A";
            }
        }
    }

    public class GPIOP
    {
        readonly string[] Fields;
        public GPIOP(string message)
        {
            Fields = message.Split(',');
            if (Fields.Length != 6)
                throw new DatagramStructureException("Provided data not GPIOP or unknown version", GetType(), new byte[1], 0);
        }

        public bool Log
        {
            get
            {
                return Fields[1][1] == '1';
            }
        }

        public bool Ignition
        {
            get
            {
                return Fields[1][0] == '1';
            }
        }

        public bool SOS
        {
            get
            {
                return Fields[1][7] == '1' && Fields[1][1] == '0';
            }
        }

    	public double Fuel
    	{
    		get
    		{
				if (Fields[3] != null)
				{
					return Double.Parse(Fields[3], CultureInfo.InvariantCulture) * 1000;
				}
    			return 0f;
    		}
    	}

        public double Analog2
        {
            get
            {
                if(Fields[4]!=null)
                {
                    return Double.Parse(Fields[4], CultureInfo.InvariantCulture) * 1000;
                }
                return 0f;
            }
        }

        public double Voltage
        {
            get
            {
                if (Fields[5] != null)
                {
                    string[] tmp = Fields[5].Split('*');
                    return Double.Parse(tmp[0], CultureInfo.InvariantCulture) * 1000;
                }
                return 0f;
            }
        }
    }

	public class PGID
	{
		readonly string[] Fields;

		public PGID(string message)
        {
            Fields = message.Split(',');
            if (Fields.Length != 2)
                throw new DatagramStructureException("Provided data not PGID or unknown version", GetType(), new byte[1], 0);
        }

		public string ID
		{
			get
			{
				string s = Fields[1];
				if (s.IndexOf('*') > 0)
					s = s.Remove(s.IndexOf('*'));
				return s;
			}
		}
	}
}
