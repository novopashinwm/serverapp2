﻿using System;

namespace FORIS.TSS.Terminal.TM140
{
    [Serializable]
    public enum TM140Sensor
    {
        Ignition = 1,
        Alarm = 2,
        PowerVoltage = 3,
        Fuel = 4
    }
}
