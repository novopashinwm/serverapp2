﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.Common;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace RU.NVG.NIKA.Terminal.Smarts
{
	public partial class SmartsDevice
	{
		private string GetPassword(IStdCommand command)
		{
			var password = command.Target.Password;
			if (string.IsNullOrWhiteSpace(password))
			{
				switch (command.Target.DeviceType)
				{
					case DeviceNames.SmartGT02A:
						password = "666666";
						break;
					case DeviceNames.SmartTR06A:
						password = "6666";
						break;
					case DeviceNames.SmartAnimalMK2:
						password = "000000";
						break;
					default:
						password = string.Empty;
						break;
				}
			}
			return password;
		}
		public override byte[] GetCmd(IStdCommand command)
		{
			if (null == command?.Target?.DeviceID)
				return default(byte[]);
			var commands = new[]
			{
				new { DevType = DeviceNames.SmartGT06N, CmdType = CmdType.Immobilize,   CmdText = $@"RELAY,1#"     },
				new { DevType = DeviceNames.SmartGT06N, CmdType = CmdType.Deimmobilize, CmdText = $@"RELAY,0#"     },
				new { DevType = DeviceNames.SmartRP01,  CmdType = CmdType.Immobilize,   CmdText = $@"DYD,000000#"  },
				new { DevType = DeviceNames.SmartRP01,  CmdType = CmdType.Deimmobilize, CmdText = $@"HFYD,000000#" },
			};
			var comandContentTxt = commands
				?.FirstOrDefault(x => x.DevType == command.Target.DeviceType && x.CmdType == command.Type)
				?.CmdText;
			if (default(string) == comandContentTxt)
				return base.GetCmd(command);
			// Формируем массив байт команды, из 5 байт служебных полей и текста команды
			var comandContentBin = new byte[5]
				.Concat(Encoding.ASCII.GetBytes(comandContentTxt))
				.ToArray();
			// Заполняем длину контента команды
			comandContentBin[0] = (byte)(comandContentBin.Length - 1); // Длина массива минус длина поля длины
																	   // Заполняем тип команды
			BitConverter.GetBytes((int)command.Type).CopyTo(comandContentBin, 1);

			return Gt06GetAnswer(
				0x80,   // Пакет команды сервера
				0x0000, // Серийный номер команды, что предавать неизвестно, отправляем 0x0000
				comandContentBin)
				?.Data;
		}
		public override List<CommandStep> GetCommandSteps(IStdCommand command)
		{
			var devPsw = GetPassword(command);
			var apnCfg = GetInternetApnConfig(command);
			var srvAdd = Manager.GetConstant(Constant.ServerIP);
			var srvPrt = Manager.GetConstant(Constant.ServerPort);
			var mstPhn = ContactHelper.GetNormalizedPhone(command.Params[PARAMS.Keys.Phone] as string ?? command.Params[PARAMS.Keys.ReplyToPhone] as string);
			var result = default(List<CommandStep>);
			switch (command.Target.DeviceType)
			{
				case DeviceNames.SmartGT03:
				case DeviceNames.SmartGT03A:
				case DeviceNames.SmartGT06:
				case DeviceNames.SmartGT06N:
				case DeviceNames.SmartRP01:
				case DeviceNames.SmartGT07:
					result = new[]
					{
						new { Cmd = CmdType.Setup, Steps = new []
							{
								new { Wait = false, Text = $@"apn,{apnCfg.Name},{apnCfg.User},{apnCfg.Pass}#" },
								new { Wait = false, Text = $@"saving,0#"                                      },
								new { Wait = false, Text = $@"timer,60#"                                      },
								new { Wait = false, Text = $@"gmt,e,0#"                                       },
								new { Wait = true,  Text = $@"server,0,{srvAdd},{srvPrt}#"                    },
							}},
						new { Cmd = CmdType.SetInterval,    Steps = new [] { new { Wait = false, Text = $@"timer,60#"           }, }, },
						new { Cmd = CmdType.Status,         Steps = new [] { new { Wait = false, Text = $@"status#"             }, }, },
						new { Cmd = CmdType.SetSos,         Steps = new [] { new { Wait = false, Text = $@"sos,a,+79991112233#" }, }, },
						new { Cmd = CmdType.DeleteSOS,      Steps = new [] { new { Wait = false, Text = $@"sos,d,+79991112233#" }, }, },
						new { Cmd = CmdType.AskPosition,    Steps = new [] { new { Wait = false, Text = $@"url#"                }, }, },
						new { Cmd = CmdType.ReloadDevice,   Steps = new [] { new { Wait = false, Text = $@"reset#"              }, }, },
						new { Cmd = CmdType.SetModeOnline,  Steps = new [] { new { Wait = false, Text = $@"saving,0#"           }, }, },
						new { Cmd = CmdType.SetModeWaiting, Steps = new [] { new { Wait = false, Text = $@"saving,1#"           }, }, },
					}
						?.FirstOrDefault(x => x.Cmd == command.Type)
						?.Steps
						?.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						?.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						?.ToList<CommandStep>();
					break;
				case DeviceNames.SmartTR06A:
					result = new[]
					{
						new { Cmd = CmdType.Setup, Steps = new []
							{
								new { Wait = false, Text = $@"#{devPsw}#sapn#{apnCfg.Name}#{apnCfg.User}#{apnCfg.Pass}###" },
								new { Wait = false, Text = $@"#{devPsw}#aad#1#{mstPhn}#"                                   },
								new { Wait = false, Text = $@"#{devPsw}#SMT#60#"                                           },
								new { Wait = false, Text = $@"#{devPsw}#SST#30#"                                           },
								new { Wait = false, Text = $@"#{devPsw}#gprs#1#"                                           },
								new { Wait = false, Text = $@"#{devPsw}#stz#e0#"                                           },
								new { Wait = true,  Text = $@"#{devPsw}#IP#{srvAdd}#{srvPrt}#"                             },
							}},
						new { Cmd = CmdType.Immobilize,     Steps = new [] { new { Wait = false, Text = $@"#{devPsw}#CF#"        }, }, },
						new { Cmd = CmdType.Deimmobilize,   Steps = new [] { new { Wait = false, Text = $@"#{devPsw}#OF#"        }, }, },
						new { Cmd = CmdType.SetInterval,    Steps = new [] { new { Wait = false, Text = $@"#{devPsw}#SMT#60#"    }, }, },
						new { Cmd = CmdType.Status,         Steps = new [] { new { Wait = false, Text = $@"status#"              }, }, },
						new { Cmd = CmdType.AskPosition,    Steps = new [] { new { Wait = false, Text = $@"#{devPsw}#GL#"        }, }, },
						new { Cmd = CmdType.ReloadDevice,   Steps = new [] { new { Wait = false, Text = $@"#{devPsw}#Reset#"     }, }, },
						new { Cmd = CmdType.Callback,       Steps = new [] { new { Wait = false, Text = $@"#{devPsw}#CALLADMIN#" }, }, },
						new { Cmd = CmdType.MonitorModeOn,  Steps = new [] { new { Wait = false, Text = $@"#{devPsw}#OM#"        }, }, },
						new { Cmd = CmdType.MonitorModeOff, Steps = new [] { new { Wait = false, Text = $@"#{devPsw}#CM#"        }, }, },
					}
						?.FirstOrDefault(x => x.Cmd == command.Type)
						?.Steps
						?.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						?.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						?.ToList<CommandStep>();
					break;
				case DeviceNames.SmartAnimalMK2:
					result = new[]
					{
						new { Cmd = CmdType.Setup, Steps = new []
							{
								new { Wait = false, Text = $@"CENTER,000000,{mstPhn}#"                        },
								new { Wait = false, Text = $@"apn,{apnCfg.Name},{apnCfg.User},{apnCfg.Pass}#" },
								new { Wait = false, Text = $@"lang,0#"                                        },
								new { Wait = false, Text = $@"gmt,e,0#"                                       },
								new { Wait = false, Text = $@"gps,1#"                                         },
								new { Wait = false, Text = $@"hbt,60#"                                        },
								new { Wait = false, Text = $@"server,0,{srvAdd},{srvPrt}#"                    },
							}},
						new { Cmd = CmdType.SetInterval,     Steps = new [] { new { Wait = false, Text = $@"wkmd,0#60#" }, }, },
						new { Cmd = CmdType.Status,          Steps = new [] { new { Wait = false, Text = $@"st#"        }, }, },
						new { Cmd = CmdType.AskPosition,     Steps = new [] { new { Wait = false, Text = $@"Where#"     }, }, },
						new { Cmd = CmdType.ReloadDevice,    Steps = new [] { new { Wait = false, Text = $@"reset#"     }, }, },
						new { Cmd = CmdType.SetModeOnline,   Steps = new [] { new { Wait = false, Text = $@"wkmd,0#"    }, }, },
						new { Cmd = CmdType.SetModeWaiting,  Steps = new [] { new { Wait = false, Text = $@"wkmd,1#"    }, }, },
						new { Cmd = CmdType.TurnLightOn,     Steps = new [] { new { Wait = false, Text = $@"aa,1#"      }, }, },
						new { Cmd = CmdType.TurnLightOff,    Steps = new [] { new { Wait = false, Text = $@"aa,0#"      }, }, },
					}
						?.FirstOrDefault(x => x.Cmd == command.Type)
						?.Steps
						?.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						?.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						?.ToList<CommandStep>();
					break;
				default:
					if (!string.IsNullOrWhiteSpace(devPsw))
						devPsw = $@",{devPsw}";
					result = new[]
					{
						new { Cmd = CmdType.Setup, Steps = new []
							{
								new { Wait = true,  Text = $@"PARAM{devPsw}#"                                         },
								new { Wait = false, Text = $@"APN{devPsw},{apnCfg.Name},{apnCfg.User},{apnCfg.Pass}#" },
								new { Wait = false, Text = $@"SERVER{devPsw},0,{srvAdd},{srvPrt},0#"                  },
								new { Wait = false, Text = $@"GMT{devPsw},E,0,0#"                                     },
								new { Wait = false, Text = $@"GPRSON{devPsw},1#"                                      },
								new { Wait = false, Text = $@"TIMER{devPsw},10#"                                      },
							}},
					}
						?.FirstOrDefault(x => x.Cmd == command.Type)
						?.Steps
						?.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						?.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						?.ToList<CommandStep>();
					break;
			}
			return 0 != (result?.Count ?? 0)
				? result
				: base.GetCommandSteps(command);
		}
		public override IList<object> ParseSms(IUnitInfo ui, string text)
		{
			var result = new List<object>(1);
			if (text == "OK")
			{
				result.Add(CommandCompleted(ui, CmdResult.Completed, CmdType.Unspecified));
				return result;
			}
			if (text == "SET SERVER OK")
			{
				result.Add(CommandCompleted(ui, CmdResult.Completed, CmdType.Setup));
				return result;
			}
			if (text == "Attention!!!battery too low,please charge." ||
				text == "Low Battery Shutdown!")
			{
				//TODO: передавать в систему состояния тревог
				//Пока просто игнорируем
				return result;
			}
			if (text.StartsWith("Error:"))
			{
				result.Add(CommandCompleted(ui, CmdResult.Failed, CmdType.Unspecified));
				return result;
			}
			if (text.StartsWith("IMEI:"))
			{
				//TODO: передавать в систему IMEI и по возможности фиксировать его в качестве IMEI для передачи данных - реализовать то же самое для AvtoGraf'а
				//Пример строки: IMEI:353492049056460;TimeZone:E,0,0;SOS:40b1a6a12a965267ed860ec6f12dff96,6452,,;
				var imeiEndIndex = text.IndexOf(';');
				if (imeiEndIndex != ImeiHelper.ImeiLength + "IMEI:".Length)
					return null;
				var imei = text.Substring("IMEI:".Length, ImeiHelper.ImeiLength);
				if (!ImeiHelper.IsImeiCorrect(imei))
				{
					Trace.TraceWarning("{0}: Incorrect IMEI {1} got from message from SMS {2}", this, imei, text);
					return null;
				}

				//TODO: собрать остальные параметры и сохранить их в коллекцию настроек трекера, чтобы показывать для диагностики
				var mu = ImeiReceived(ui, imei);
				result.Add(mu);

				result.Add(CommandCompleted(ui, CmdResult.Completed, CmdType.Setup));

				return result;
			}
			if (text.StartsWith("Lat:"))
			{
				//TODO: разобрать координаты и вернуть IMobilUnit
				MobilUnit mu = ParsePositionSms(ui, text);
				if (mu == null)
					return null;

				result.Add(mu);
				result.Add(CommandCompleted(ui, CmdResult.Completed, CmdType.AskPosition));
				return result;
			}

			return null;
		}
		private MobilUnit ParsePositionSms(IUnitInfo ui, string text)
		{
			//Пример SMS: Lat:N55.721672,Lon:E37.639342,Course:115.60,Speed:3.6984,DateTime:14-08-18  15:18:24

			//TODO: хранить в настройках часовой пояс трекера
			var mu = new MobilUnit(ui);
			mu.Time = TimeHelper.GetSecondsFromBase();
			mu.CorrectGPS = true;

			var reader = new SplittedStringReader(text, ',', ':');
			if (!reader.TrySkip("Lat"))
				return null;

			double lat;
			if (!TryParseCoordinate(reader.ReadString(), out lat, 'S'))
				return null;
			mu.Latitude = lat;

			if (!reader.TrySkip("Lon"))
				return null;
			double lng;
			if (!TryParseCoordinate(reader.ReadString(), out lng, 'W'))
				return null;
			mu.Longitude = lng;

			if (!reader.TrySkip("Course"))
				return null;
			var course = reader.ReadDouble();
			if (course == null)
				return null;
			mu.Course = (int)Math.Round(course.Value);

			if (!reader.TrySkip("Speed"))
				return null;

			var speed = reader.ReadDouble();
			if (speed == null)
				return null;
			mu.Speed = (int)Math.Round(speed.Value);

			//Дату игнорируем, т.к. нет информации о часовом поясе
			return mu;
		}
		private static bool TryParseCoordinate(string s, out double result, char negativeHemisphere)
		{
			result = double.NaN;
			if (s.Length < 2)
				return false;
			if (!double.TryParse(s.Substring(1), NumberStyles.Any, NumberHelper.FormatInfo, out result))
				return false;
			if (s[0] == negativeHemisphere)
				result = -result;
			return true;
		}
	}
}