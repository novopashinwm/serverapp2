﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace RU.NVG.NIKA.Terminal.Smarts
{
	public partial class SmartsDevice : Device
	{
		internal static class DeviceNames
		{
			internal const string SmartBabyBear  = "BB-mobile Baby Bear";
			internal const string SmartGT02A     = "Smart GT02A";
			internal const string SmartGT02B     = "Smart GT02B";
			internal const string SmartGT03      = "Smart GT03";
			internal const string SmartGT03A     = "Smart GT03A";
			internal const string SmartGT05      = "Smart GT05";
			internal const string SmartGT06      = "Smart GT06";
			internal const string SmartGT06N     = "Smart GT06N";
			internal const string SmartRP01      = "Smart RP01";
			internal const string SmartGT07      = "Smart GT07";
			internal const string SmartGT09B     = "Smart GT09B";
			internal const string SmartTR02      = "Smart TR02";
			internal const string SmartTR06      = "Smart TR06";
			internal const string SmartTR06A     = "Smart TR06A";
			internal const string SmartAnimalMK2 = "Smart Animal MK2";
		}
		internal static readonly byte[] Prefix6868 = { 0x68, 0x68 };
		internal static readonly byte[] Prefix7878 = { 0x78, 0x78 };
		internal static readonly byte[] Prefix7979 = { 0x79, 0x79 };
		internal static readonly byte[] Suffix0D0A = { 0x0D, 0x0A };

		private const byte LoginPacket = 0x01;

		public SmartsDevice() : base(typeof(SmartsDeviceSensor), new[]
		{
			DeviceNames.SmartBabyBear,
			DeviceNames.SmartGT02A,
			DeviceNames.SmartGT02B,
			DeviceNames.SmartGT03,
			DeviceNames.SmartGT03A,
			DeviceNames.SmartGT05,
			DeviceNames.SmartGT06,
			DeviceNames.SmartGT06N,
			DeviceNames.SmartRP01,
			DeviceNames.SmartGT07,
			DeviceNames.SmartGT09B,
			DeviceNames.SmartTR02,
			DeviceNames.SmartTR06,
			DeviceNames.SmartTR06A,
			DeviceNames.SmartAnimalMK2,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			// Минимальная длина пакета логина
			if (data.Length < 18)
				return false;
			// Первые два байта
			if (!data.StartsWith(Prefix7878) && !data.StartsWith(Prefix6868))
				return false;
			// Последние два байта
			if (!data.EndsWith(Suffix0D0A))
				return false;
			// Проверяем IMEI (для пакета типа Login)
			if (data[3] == LoginPacket)
			{
				if (9 < data[4] % 16)
					return false;
				for (var i = 5; i != 5 + 14 / 2; ++i)
				{
					if (9 < data[i] / 16)
						return false;
					if (9 < data[i] % 16)
						return false;
				}
			}
			return true;
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;
			var result = new ArrayList(1);
			string deviceId = null;
			if (!data.StartsWith(Prefix6868))
			{
				if (stateData == null && data[3] == LoginPacket)
				{
					var imei = new StringBuilder(15);
					imei.Append(data[4] % 16);
					for (var i = 5; i != 5 + 14 / 2; ++i)
					{
						imei.Append(data[i] / 16);
						imei.Append(data[i] % 16);
					}
					deviceId = (imei.ToString());
					result.Add(
						new ReceiverStoreToStateMessage
						{
							StateData = new ReceiverState
							{
								DeviceID = deviceId
							}
						});
				}
				else
				{
					var state = (ReceiverState)stateData;
					if (state != null)
						deviceId = state.DeviceID;
				}
			}
			if (data.StartsWith(Prefix7878))
			{
				// Разбиваем на пакеты
				var recordDatas = ExtractPackets(data, Prefix7878, Suffix0D0A)
					.ToList();
				foreach (var recordData in recordDatas)
				{
					switch (recordData[3])
					{
						case LoginPacket:
							result.Add(Gt06LoginPacket(recordData));
							break;
						case 0x12:
							result.Add(Gt06LocationDataPacket(recordData));
							break;
						case 0x13:
							result.AddRange(Gt06HeartbeatPacket(recordData));
							break;
						case 0x15:
							result.AddRange(Gt06OnlineCommandAnswerPacket(recordData, deviceId));
							break;
						case 0x16:
							result.AddRange(Gt06AlarmDataPacket(recordData));
							break;
						case 0x18:
							result.Add(ParseLbs(recordData));
							break;
						case 0x1e:
						case 0x22:
							result.Add(ParseLbsGpsMergedPacket(recordData));
							break;
						case 0x8A:
							result.Add(GetTimePacketAnswer(recordData));
							break;
						default:
							Trace.TraceWarning("{0} unknown protocol type {1:X} for deviceId={2}: {3}",
								this,
								recordData[3],
								deviceId,
								BitConverter.ToString(data));
							break;
					}
				}
				foreach (var o in result)
				{
					var mu = o as IMobilUnit;
					if (mu == null)
						continue;
					mu.DeviceID = deviceId;
				}
				// Остаток возвращаем назад
				var bytesUsedCount = recordDatas.Sum(a => a.Length);
				if (0 < bytesUsedCount && bytesUsedCount < data.Length)
					bufferRest = data.Skip(bytesUsedCount).ToArray();
			}
			else if (data.StartsWith(Prefix7979))
			{
				result.Add(new ConfirmPacket { Data = Get7979Answer() });
			}
			else if (data.StartsWith(Prefix6868))
			{
				var mobilUnit = ParseGt02(data);
				if (mobilUnit != null)
					result.Add(mobilUnit);

				result.Add(new ConfirmPacket { Data = Get6868Answer() });
			}
			return result;
		}
		private IEnumerable<byte[]> ExtractPackets(byte[] data, byte[] prefix, byte[] suffix)
		{
			var reader = new DataReader(data, 0, data.Length);

			while (reader.Any())
			{
				if (reader.RemainingBytesCount < prefix.Length)
					yield break;

				if (!reader.ReadBytes(prefix.Length).SequenceEqual(prefix))
					continue;

				if (1 > reader.RemainingBytesCount)
					yield break;

				var packetLength = reader.ReadByte();
				if (packetLength > reader.RemainingBytesCount)
					yield break;
				reader.Skip(packetLength);

				if (reader.RemainingBytesCount < suffix.Length)
					yield break;
				reader.Skip(suffix);

				var count = prefix.Length + 1 + packetLength + suffix.Length;
				reader.Rewind(count);
				yield return reader.ReadBytes(count);
			}
		}
		private IMobilUnit ParseLbsGpsMergedPacket(byte[] data)
		{
			return Gt06LocationDataPacket(data);
		}
		private MobilUnit ParseGt02(byte[] data)
		{
			var reader = new DataReader(data);
			//Info header
			reader.Skip(2);
			//Content-Length - всегда должно быть 0x25
			reader.Skip(1);
			//Reserved
			reader.Skip(2);
			//DeviceID
			var imei = new StringBuilder(15);
			imei.Append(reader.ReadByte() & 0x0f);
			for (var i = 0; i != 7; ++i)
			{
				var b = reader.ReadByte();
				imei.Append(b/16);
				imei.Append(b%16);
			}
			var mu = UnitReceived(DeviceNames.SmartGT02A);
			mu.DeviceID = imei.ToString();
			//Information serial number
			reader.Skip(2);
			//Protocol number
			var protocolNumber = reader.ReadByte();
			switch (protocolNumber)
			{
				case 0x01:
					return null;
				case 0x10:
					mu.Time      = ReadDateTime(reader);
					mu.Latitude  = reader.ReadBigEndian32(4)/30000.0/60;
					mu.Longitude = reader.ReadBigEndian32(4)/30000.0/60;
					mu.Speed     = reader.ReadByte();
					var course   = reader.ReadBigEndian32(2);
					mu.Course    = (int)(course & 0x03ff);

					//Reserved bytes
					reader.Skip(3);

					var status = reader.ReadBigEndian32(4);
					var correctGPS = (status & 0x01) != 0;
					mu.Satellites = correctGPS ? MobilUnit.MinSatellites : 0;
					//Может, следует брать данные из пакета типа 0x01?
					mu.CorrectGPS = correctGPS;
					if ((status & 0x02) == 0)
						mu.Latitude = -mu.Latitude;
					if ((status & 0x04) == 0)
						mu.Longitude = -mu.Longitude;
					mu.SensorValues = new Dictionary<int, long>
					{
						{ (int)SmartsDeviceSensor.Charging, status & 0x8 }
					};
					break;
			}

			return mu;
		}
		private int ReadDateTime(DataReader reader)
		{
			var year   = reader.ReadByte();
			var month  = reader.ReadByte();
			var day    = reader.ReadByte();
			var hour   = reader.ReadByte();
			var minute = reader.ReadByte();
			var second = reader.ReadByte();

			var datetime = new DateTime(2000 + year, month, day, hour, minute, second, DateTimeKind.Utc);
			return TimeHelper.GetSecondsFromBase(datetime);
		}
		private byte[] Get6868Answer()
		{
			return new byte[]
				{
					//Info Header
					0x54, 0x68,
					//Protocol NO.
					0x01,
					//End mark
					0x0d, 0x0a
				};
		}
		private byte[] Get7979Answer()
		{
			return new byte[]
				{
					0x78, 0x78,
					0x05, 0x01, 0x05,
					0x01, 0x05, 0x01,
					0x0d, 0x0a
				};
		}
		private IMobilUnit ParseLbs(byte[] data)
		{
			var reader = new DataReader(data);
			reader.Skip(11);
			var mu = UnitReceived(DeviceNames.SmartGT06);
			mu.Time = TimeHelper.GetSecondsFromBase();
			mu.CorrectGPS = false;

			var countryCode = reader.ReadByte().ToString(CultureInfo.InvariantCulture);
			var networkCode = reader.ReadByte().ToString(CultureInfo.InvariantCulture);

			var cellNetworks = new List<CellNetworkRecord>(7);

			for (var i = 0; i != cellNetworks.Capacity; ++i)
			{
				var cellNetwork = new CellNetworkRecord();
				cellNetwork.LogTime        = mu.Time;
				cellNetwork.Number         = cellNetworks.Count;
				cellNetwork.CountryCode    = countryCode;
				cellNetwork.NetworkCode    = networkCode;
				cellNetwork.LAC            = (int)reader.ReadBigEndian32(2);
				cellNetwork.CellID         = (int)reader.ReadBigEndian32(3);
				cellNetwork.SignalStrength = reader.ReadByte();
				if (cellNetwork.SignalStrength == 0xff)
					continue;
				cellNetworks.Add(cellNetwork);
			}

			mu.CellNetworks = cellNetworks.ToArray();

			return mu;
		}
		/// <summary> Login Information packet </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		private ConfirmPacket Gt06LoginPacket(byte[] data)
		{
			var reader = new SmartsDeviceReader(data);
			reader.Skip(8);
			var serial = reader.ReadSerial();

			return Gt06GetAnswer(reader.ProtocolNumber, serial);
		}
		/// <summary> Positioning Data (UTC) </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		private IMobilUnit Gt06LocationDataPacket(byte[] data)
		{
			var reader = new SmartsDeviceReader(data);

			if (reader.RemainingBytesCount < 32)
			{
				Trace.TraceWarning(GetType().Name + ": The data packet has a length less than necessary, the packet length is {0}, and {1} is required.\nData:\n{2}",
					data.Length, 36, BitConverter.ToString(data));
				return null;
			}
			var dateTime        = reader.ReadDateTime();
			var infoSatelites   = reader.ReadByte();
			var satellites      = infoSatelites & 15;
			var latitude        = reader.ReadBigEndian32(4) / 30000.0 / 60;
			var longitude       = reader.ReadBigEndian32(4) / 30000.0 / 60;
			var speed           = reader.ReadByte();
			var statusAndCourse = reader.ReadBigEndian32(2);

			var mcc    = (short)reader.ReadBigEndian32(2);
			var mnc    = reader.ReadByte();
			var lac    = reader.ReadBigEndian32(2);
			var cellId = reader.ReadBigEndian32(3);
			reader.ReadSerial();

			var status = (statusAndCourse & 0xFC00) >> 10;
			//TODO: выяснить, что с этими полями
			//var gpsRealTime = (status & 8) == 8;
			//var gpsHasPosition = (status & 4) == 4;
			var correctGps = 4 <= satellites;
			if (!correctGps)
				dateTime = DateTime.UtcNow;

			var time = TimeHelper.GetSecondsFromBase(dateTime);
			var mu = UnitReceived(DeviceNames.SmartGT06);
			mu.Time         = time;
			mu.Latitude     = (status & 0x01) == 0 ? -latitude : latitude;
			mu.Longitude    = (status & 0x02) != 0 ? -longitude : longitude;
			mu.CorrectGPS   = correctGps;
			mu.Speed        = speed;
			mu.Course       = (int)(statusAndCourse & 0x03ff);
			mu.Satellites   = satellites;
			mu.CellNetworks = new[]
			{
				new CellNetworkRecord
				{
					LogTime     = time,
					CountryCode = mcc.ToString(CultureInfo.InvariantCulture),
					NetworkCode = mnc.ToString(CultureInfo.InvariantCulture),
					LAC         = (int)lac,
					CellID      = (int)cellId,
				}
			};

			return mu;
		}
		/// <summary> Alarm Data (UTC) </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		private object[] Gt06AlarmDataPacket(byte[] data)
		{
			var reader = new SmartsDeviceReader(data);
			var dateTime = reader.ReadDateTime();
			var infoSatelites = reader.ReadByte();
			var satellites = infoSatelites & 15;
			var latitude = reader.ReadBigEndian32(4) / 30000.0 / 60;
			var longitude = reader.ReadBigEndian32(4) / 30000.0 / 60;
			var speed = reader.ReadByte();
			var statusAndCourse = reader.ReadBigEndian32(2);

			//var lbsLength = reader.ReadByte();
			reader.Skip();
			var mcc = (short)reader.ReadBigEndian32(2);
			var mnc = reader.ReadByte();
			var lac = reader.ReadBigEndian32(2);
			var cellId = reader.ReadBigEndian32(3);

			var ti = reader.ReadTerminalInformation();
			var voltageLevel = reader.ReadByte();
			var signalStrength = reader.ReadByte();
			//var langBytes = reader.ReadBytes(2);
			reader.Skip(2);
			var serial = reader.ReadSerial();

			var alarm = (ti[3] ? 1 : 0) + (ti[4] ? 2 : 0) + (ti[5] ? 4 : 0);
			var status = (statusAndCourse & 0xFC00) >> 10;
			var course = (int) (statusAndCourse & 0x03ff);
			//TODO: выяснить, что с этими полями
			//var gpsRealTime = (status & 8) == 8;
			//var gpsHasPosition = (status & 4) == 4;
			var correctGps = 4 <= satellites;
			if (!correctGps)
				dateTime = DateTime.UtcNow;
			var time = TimeHelper.GetSecondsFromBase(dateTime);

			var mu = UnitReceived(DeviceNames.SmartGT06);
			mu.Time = time;
			mu.Latitude = (status & 0x01) == 0 ? -latitude : latitude;
			mu.Longitude = (status & 0x02) != 0 ? -longitude : longitude;
			mu.CorrectGPS = correctGps;
			mu.Speed = speed;
			mu.Course = course;
			mu.Satellites = satellites;
			mu.CellNetworks = new[]
			{
				new CellNetworkRecord
				{
					LogTime        = time,
					CountryCode    = mcc.ToString(CultureInfo.InvariantCulture),
					NetworkCode    = mnc.ToString(CultureInfo.InvariantCulture),
					LAC            = (int)lac,
					CellID         = (int)cellId,
					SignalStrength = signalStrength
				}
			};

			mu.SetSensorValue((int)SmartsDeviceSensor.Status, ti[0] ? 1 : 0);
			mu.SetSensorValue((int)SmartsDeviceSensor.Acc, ti[1] ? 1 : 0);
			mu.SetSensorValue((int)SmartsDeviceSensor.Charging, ti[2] ? 1 : 0);
			mu.SetSensorValue((int)SmartsDeviceSensor.Alarm, alarm);
			//mu.SetSensorValue((int)SmartsDeviceSensor.GpsTracking, ti[6] ? 1 : 0);
			mu.SetSensorValue((int)SmartsDeviceSensor.GasOilElectricityConnected, ti[7] ? 1 : 0);
			mu.SetSensorValue((int)SmartsDeviceSensor.VoltageLevel, voltageLevel);

			return new object[]
				{
					Gt06GetAnswer(reader.ProtocolNumber, serial),
					mu
				};
		}
		/// <summary> Status Information (Heartbeat Packet) </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		private object[] Gt06HeartbeatPacket(byte[] data)
		{
			var reader = new SmartsDeviceReader(data);
			var ti = reader.ReadTerminalInformation();
			var voltageLevel = reader.ReadByte();

			//var signalStrength = reader.ReadByte();
			reader.Skip();

			//var alarmByte = reader.ReadByte();
			reader.Skip();

			//var languageByte = reader.ReadByte();
			reader.Skip();

			var alarm = (ti[3] ? 1 : 0) + (ti[4] ? 2 : 0) + (ti[5] ? 4 : 0);
			var serial = reader.ReadSerial();

			var date = DateTime.UtcNow;
			var mu = UnitReceived(DeviceNames.SmartGT06);
			mu.Time = TimeHelper.GetSecondsFromBase(date);
			mu.CorrectGPS = false;
			mu.SetSensorValue((int)SmartsDeviceSensor.Status, ti[0] ? 1 : 0);
			mu.SetSensorValue((int)SmartsDeviceSensor.Acc, ti[1] ? 1 : 0);
			mu.SetSensorValue((int)SmartsDeviceSensor.Charging, ti[2] ? 1 : 0);
			mu.SetSensorValue((int)SmartsDeviceSensor.Alarm, alarm);
			//mu.SetSensorValue((int)SmartsDeviceSensor.GpsTracking, ti[6] ? 1 : 0);
			mu.SetSensorValue((int)SmartsDeviceSensor.GasOilElectricityConnected, ti[7] ? 1 : 0);
			mu.SetSensorValue((int)SmartsDeviceSensor.VoltageLevel, voltageLevel);

			return new object[]
			{
				Gt06GetAnswer(reader.ProtocolNumber, serial),
				mu
			};
		}
		/// <summary> Ответ на онлайн команду (команду посланную через соединение, по которому отправляются данные) /// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		private object[] Gt06OnlineCommandAnswerPacket(byte[] data, string deviceId)
		{
			var reader = new SmartsDeviceReader(data);
			// Читаем длину команды
			var comandContentLength = reader.ReadByte() - 4; // или 6
			// Читаем посланную сервером информацию для идентификации команды
			var comandType = BitConverter.ToInt32(reader.ReadBytes(4), 0);
			// Читаем ответ устройства
			var comandContentBin = reader.ReadBytes(comandContentLength);
			var comandContentTxt = Encoding.ASCII.GetString(comandContentBin);
			reader.Skip(2);
			// Читаем информацию о последовательности сообщений
			reader.ReadSerial();

			var cmdResult = comandContentTxt.ToLowerInvariant().Contains("success")
				? CmdResult.Completed
				: CmdResult.Failed;
			if (CmdResult.Failed == cmdResult)
				Trace.TraceWarning("Device '{0}' reported that it was command failed, reason: {1}",
					deviceId, comandContentTxt);
			else
				Trace.TraceInformation("Device '{0}' reported that it was command completed, reason: {1}",
					deviceId, comandContentTxt);

			var cmdType = CmdType.Unspecified;
			Enum.TryParse(comandType.ToString(), true, out cmdType);

			var ui = UnitInfo.Factory.Create(UnitInfoPurpose.Undefined);
			ui.DeviceID = deviceId;
			Manager.FillUnitInfo(ui);
			return new object[]
			{
				CommandCompleted(ui, cmdResult, cmdType)
			};
		}
		private object GetTimePacketAnswer(byte[] data)
		{
			var utcnow = DateTime.UtcNow;
			var reader = new SmartsDeviceReader(data);
			return Gt06GetAnswer(
				reader.ProtocolNumber,
				reader.ReadSerial(),
				new byte[]
				{
					(byte)(utcnow.Year - 2000),
					(byte)(utcnow.Month),
					(byte)(utcnow.Day),
					(byte)(utcnow.Hour),
					(byte)(utcnow.Minute),
					(byte)(utcnow.Second),
				});
		}
		private ConfirmPacket Gt06GetAnswer(byte protocol, short serial, byte[] content = null)
		{
			// Коррекция контента
			content = content ?? new byte[0];
			var answer = new byte[10 + content.Length];
			// Префикс
			answer[00] = Prefix7878[0];
			answer[01] = Prefix7878[1];
			// Длина пакета от следующего байта до суффикса
			answer[02] = (byte)(answer.Length - 5);
			// Протокол
			answer[03] = protocol;
			// Контент
			Buffer.BlockCopy(content, 0, answer, 04, content.Length);
			// Серийный номер пакета
			answer[answer.Length - 6] = (byte)((serial >> 0) & 0xff);
			answer[answer.Length - 5] = (byte)((serial >> 8) & 0xff);
			// Контрольная сумма (big endian, поэтому наоборот)
			var crc = SmartsDeviceCrc16.GetCrc16(answer, 2, answer.Length - 4);
			answer[answer.Length - 4] = (byte)((crc >> 8) & 0xff);
			answer[answer.Length - 3] = (byte)((crc >> 0) & 0xff);
			// Суффикс пакета
			answer[answer.Length - 2] = Suffix0D0A[0];
			answer[answer.Length - 1] = Suffix0D0A[1];
			return new ConfirmPacket(answer);
		}
	}
}