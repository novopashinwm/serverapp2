﻿using System;

namespace RU.NVG.NIKA.Terminal.Smarts
{
	[Serializable]
	public enum SmartsDeviceSensor
	{
		Charging                   = 1,
		VoltageLevel               = 2,
		GasOilElectricityConnected = 3,
		GpsTracking                = 4,
		Alarm                      = 5,
		Acc                        = 6,
		Status                     = 7
	}
}