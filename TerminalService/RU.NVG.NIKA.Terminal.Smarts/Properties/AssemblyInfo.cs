﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("RU.NVG.NIKA.Terminal.Smarts")]
[assembly: AssemblyDescription("Support for GPS trackers by Smarts: Baby Bear child tracker and car trackers GT06, G06N")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("F19A97EF-F267-4A72-B8E8-581B9153D8D7")]