﻿using System;
using System.Collections;
using System.Data;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers;

namespace RU.NVG.NIKA.Terminal.Smarts
{
	public class SmartsDeviceReader : DataReader
	{
		private byte _packetLength;
		public SmartsDeviceReader(byte[] data) : base(data)
		{
			ProtocolNumber = ReadProtocol();
		}
		public SmartsDeviceReader(byte[] data, int startIndex) : base(data, startIndex)
		{
		}
		public SmartsDeviceReader(byte[] data, int startIndex, int endIndex) : base(data, startIndex, endIndex)
		{
		}
		public byte ProtocolNumber { get; }
		public DateTime ReadDateTime()
		{
			var year   = ReadByte() + 2000;
			var month  = ReadByte();
			var day    = ReadByte();
			var hour   = ReadByte();
			var minute = ReadByte();
			var second = ReadByte();

			return new DateTime(year, month, day, hour, minute, second, DateTimeKind.Utc);
		}
		private byte ReadProtocol()
		{
			var header = ReadBytes(2);
			if (!header.StartsWith(SmartsDevice.Prefix7878))
				throw new FormatException("Packet header must start with Prefix7878");
			_packetLength = ReadByte();
			var protocolNumber = ReadByte();
			return protocolNumber;
		}
		public BitArray ReadTerminalInformation()
		{
			var value = ReadByte();
			var result = new BitArray(new[] { value });
			return result;
		}
		public short ReadSerial(bool checkCrc = true)
		{
			if (RemainingBytesCount > 6)
				Skip(RemainingBytesCount - 6);

			var serialNumber = (short)ReadLittleEndian32(2);
			var errorCheck   = (ushort)ReadBigEndian32(2);
			if (checkCrc)
			{
				var checksum = GetChecksum();
				if (errorCheck != checksum)
					throw new DataException("Checksum is not valid");
			}

			var stopBits = ReadBytes(2);
			if (!stopBits.StartsWith(SmartsDevice.Suffix0D0A))
				throw new FormatException("No terminal symbols");
			return serialNumber;
		}
		public ushort GetChecksum()
		{
			return SmartsDeviceCrc16.GetCrc16(_data, 2, _packetLength + 1);
		}
	}
}