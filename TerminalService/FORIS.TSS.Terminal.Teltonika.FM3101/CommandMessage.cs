﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using FORIS.TSS.Common;

namespace FORIS.TSS.Terminal.Teltonika.FM3101
{
	public class CommandMessage
	{
		public static readonly byte[] MessPreamble = new byte[] { 0x00, 0x00, 0x00, 0x00 };
		public static CommandMessage GetRequest(string requestContent)
		{
			return new CommandMessage
			{
				MessCodec     = 0x0C,
				MessQuantity  = 0x01,
				MessDirection = 0x05,
				MessContent   = requestContent,
			};
		}
		public static CommandMessage GetResponse(byte[] responseContent)
		{
			if (null == responseContent)
				return default(CommandMessage);
			var dataReader = new DataReader(responseContent);
			dataReader.Skip(MessPreamble);
			dataReader.Skip(4);
			return new CommandMessage
			{
				MessCodec       = dataReader.ReadByte(),
				MessQuantity    = dataReader.ReadByte(),
				MessDirection   = dataReader.ReadByte(),
				MessContentData = dataReader.ReadBytes((int)dataReader.ReadBigEndian32(4)),
			};
		}

		public byte   MessCodec       { get; protected set; }
		public byte   MessQuantity    { get; protected set; }
		public byte   MessDirection   { get; protected set; }
		public byte[] MessContentData { get; protected set; }
		public string MessContent
		{
			get { return Encoding.ASCII.GetString(MessContentData ?? new byte[0]);  }
			set { MessContentData = Encoding.ASCII.GetBytes(value ?? string.Empty); }
		}
		public uint   MessContentSize => (uint)MessContentData.Length;
		public byte[] MessData        => new[] { MessCodec, MessQuantity, MessDirection }
			.Concat(MessContentSize.ToBE())
			.Concat(MessContentData)
			.Concat(new[] { MessQuantity })
			.ToArray();
		public uint   MessSize        => (uint)MessData.Length;
		public byte[] GetBytes()
		{
			return MessPreamble
				?.Concat(MessSize.ToBE())
				?.Concat(MessData)
				?.Concat(((uint)TeltonikaDevice.GetCRC16(MessData, 0, MessData.Length, 0xA001, 0)).ToBE())
				?.ToArray();
		}
	}
}