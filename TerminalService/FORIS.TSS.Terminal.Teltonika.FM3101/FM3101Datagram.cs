﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;

namespace FORIS.TSS.Terminal.Teltonika.FM3101
{
	public class FM3101Datagram
	{
		readonly List<AvlData> _datagrams = new List<AvlData>();
		public List<AvlData> SubData
		{
			get { return _datagrams; }
		}
		public byte[] Data
		{
			set
			{
				try
				{
					byte[] data = value;

					//4-7 байты - Int32 с DataLength
					int codecId      = data[8];
					int numberOfData = data[9];
					var codec        = (Codecs)codecId;
					switch (codec)
					{
						case Codecs.AvlData:
						case Codecs.GH1201Data:
						case Codecs.Codec_8:
						case Codecs.Codec_8_Extended:
							int bytePos = 10;
							int bitPos  = 00;

							for (int i = 0; i < numberOfData; i++)
								_datagrams.Add(AvlData.Create(codec, data, ref bytePos, ref bitPos));

							if (bytePos != data.Length - 5)
							{
								$"{this.GetTypeName()}: Invalid bytePos after parsing records\r\nData:\r\n{BitConverter.ToString(data)}"
									.TraceWarning();
								_datagrams.Clear(); // Обнуляем количество принятых пакетов
								return;
							}
							break;
						default:
							throw new Exception("Codec type is not implemented. CodecId = " + codecId);
					}
				}
				catch (Exception ex)
				{
					(this.GetTypeName()
						.WithException(ex) + $"\r\nData:\r\n{BitConverter.ToString(value)}")
						.TraceWarning();
				}
			}
		}
	}
}