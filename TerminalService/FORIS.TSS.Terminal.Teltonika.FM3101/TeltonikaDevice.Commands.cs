﻿using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Teltonika.FM3101
{
	public partial class TeltonikaDevice
	{
		internal class SetupParameters
		{
			public string            SrvAdd;
			public string            SrvPrt;
			public InternetApnConfig ApnCfg;
		}
		public override byte[] GetCmd(IStdCommand command)
		{
			var cmdTyp = command?.Type;
			var devTyp = command?.Target?.DeviceType;
			if (null == command?.Target?.DeviceID)
				return default(byte[]);
			var commands = new[]
			{
				new { DevType = DeviceNames.TeltonikaFMB920, CmdType = CmdType.Immobilize,   CmdText = "setdigout 1" },
				new { DevType = DeviceNames.TeltonikaFMB920, CmdType = CmdType.Deimmobilize, CmdText = "setdigout 0" },
			};
			commands = commands
				.Union(
					from dev in new[]
					{
						DeviceNames.TeltonikaFMB001, DeviceNames.TeltonikaFMB010, DeviceNames.TeltonikaFMB110, DeviceNames.TeltonikaFMB120,
						DeviceNames.TeltonikaFMB125, DeviceNames.TeltonikaFMB140, DeviceNames.TeltonikaFMB202, DeviceNames.TeltonikaFMB630,
						DeviceNames.TeltonikaFMB640, DeviceNames.TeltonikaFMB900, DeviceNames.TeltonikaFMB920,
					}
					from cmd in new[]
					{
						new { Typ = CmdType.Teltonika_ConnectFOTA,            Cmd = "web_connect"        },
						new { Typ = CmdType.Teltonika_SetAnInOff,             Cmd = "setpsram 50180 0"   },
						new { Typ = CmdType.Teltonika_SetAnInpRec1To1Per5Min, Cmd = "setpsram 50080 0"   },
						new { Typ = CmdType.Teltonika_SetExtVltRecTo1Per5Min, Cmd = "setpsram 7018 300"  },
						new { Typ = CmdType.Teltonika_SetVoltageControlOff,   Cmd = "setpsram 50085 300" },
					}
					where dev == devTyp && cmd.Typ == cmdTyp.Value
					select new { DevType = dev, CmdType = cmd.Typ, CmdText = cmd.Cmd }
				)
				.ToArray();
			return commands
				?.Where(x => x.DevType == command?.Target?.DeviceType && x.CmdType == command?.Type)
				?.Select(x => CommandMessage.GetRequest(x.CmdText).GetBytes())
				?.FirstOrDefault() ?? base.GetCmd(command);
		}
		private object[] ParseCmdAnswerPacket(byte[] data, string deviceId)
		{
			if (null == deviceId)
				return default(object[]);
			var response = CommandMessage.GetResponse(data);
			if (null == response)
				return default(object[]);
			var ui = UnitInfo.Factory.Create(UnitInfoPurpose.Undefined);
			ui.DeviceID = deviceId;
			Manager.FillUnitInfo(ui);
			return new object[] { CommandCompleted(ui, CmdResult.Completed, CmdType.Unspecified) };
		}
		public override IList<object> ParseSms(IUnitInfo unitInfo, string text)
		{
			var devTyp = unitInfo?.DeviceType;
			switch (devTyp)
			{
				case DeviceNames.TeltonikaFMB001:
				case DeviceNames.TeltonikaFMB010:
				case DeviceNames.TeltonikaFMB110:
				case DeviceNames.TeltonikaFMB120:
				case DeviceNames.TeltonikaFMB125:
				case DeviceNames.TeltonikaFMB140:
				case DeviceNames.TeltonikaFMB202:
				case DeviceNames.TeltonikaFMB630:
				case DeviceNames.TeltonikaFMB640:
				case DeviceNames.TeltonikaFMB900:
				case DeviceNames.TeltonikaFMB920:
				case DeviceNames.TeltonikaTMT250:
				case DeviceNames.TeltonikaTST100:
					return base.ParseSms(unitInfo, text);
				default:
					if (text.StartsWith("Digital Outputs are set to: 10."))
						return new List<object> { CommandCompleted(unitInfo, CmdResult.Completed, CmdType.Immobilize)   };
					if (text.StartsWith("Digital Outputs are set to: 00."))
						return new List<object> { CommandCompleted(unitInfo, CmdResult.Completed, CmdType.Deimmobilize) };
					return base.ParseSms(unitInfo, text);
			}
		}
		public override List<CommandStep> GetCommandSteps(IStdCommand command)
		{
			switch (command.Type)
			{
				case CmdType.Setup:
					return GetSetupCommands(command);
				case CmdType.SetInterval:
					return GetSetIntervalCommandSteps(command);
				case CmdType.Status:
					return GetStatusCommandSteps(command);
				case CmdType.ReloadDevice:
					return GetReloadDeviceCommandSteps(command);
				case CmdType.Immobilize:
					return GetImmobilizeCommandSteps(command);
				case CmdType.Deimmobilize:
					return GetDeimmobilizeCommandSteps(command);
				case CmdType.AskGPSPosition:
				case CmdType.AskGoogleLink:
				case CmdType.AskBattery:
				case CmdType.BluetoothScan:
				case CmdType.BluetoothDiscoveredList:
				case CmdType.BluetoothConnectedList:
				case CmdType.SetOdometer:
				case CmdType.GetOdometer:
				case CmdType.GetOBDInfo:
				case CmdType.GetFaultCodes:
				case CmdType.GetVin:
					return GetSomeSimpleCommandSteps(command);
				default:
					return base.GetCommandSteps(command);
			}
		}
		private SetupParameters GetSetupParameters(IStdCommand command)
		{
			return new SetupParameters
			{
				SrvAdd = Manager.GetConstant(Constant.ServerIP),
				SrvPrt = Manager.GetConstant(Constant.ServerPort),
				ApnCfg = GetInternetApnConfig(command),
			};
		}
		private List<CommandStep> GetSetupCommands(IStdCommand command)
		{
			var devTyp = command?.Target?.DeviceType;
			if (null == devTyp)
				return default(List<CommandStep>);
			var stpPrs = GetSetupParameters(command);
			if (null == stpPrs)
				return default(List<CommandStep>);

			var devIdn = command?.Target?.DeviceID;
			var devMod = 0; // Пока не знаю, что это
			var srvAdd = stpPrs.SrvAdd;
			var srvPrt = stpPrs.SrvPrt;
			var apnNam = stpPrs.ApnCfg.Name;
			var apnUsr = stpPrs.ApnCfg.User;
			var apnPwd = stpPrs.ApnCfg.Pass;
			switch (devTyp)
			{
				case DeviceNames.TeltonikaFM1110:
					return new List<CommandStep>
					{
						GetSmsCommandStep(command, "setparam 1115 " + srvAdd),
						GetSmsCommandStep(command, "setparam 1116 " + srvPrt),
						GetSmsCommandStep(command, "setparam 1112 " + apnNam),
						GetSmsCommandStep(command, "setparam 1113 " + apnUsr),
						GetSmsCommandStep(command, "setparam 1114 " + apnPwd),
						//Протокол - TCP
						GetSmsCommandStep(command, "setparam 1117 0"),
						//Flush
						GetSmsCommandStep(command,
							"flush " + devIdn +
							"," + apnNam +
							"," + apnUsr +
							"," + apnPwd +
							"," + srvAdd +
							"," + srvPrt +
							"," + devMod
							)
					};
				case DeviceNames.TeltonikaFMB001:
				case DeviceNames.TeltonikaFMB010:
				case DeviceNames.TeltonikaFMB110:
				case DeviceNames.TeltonikaFMB120:
				case DeviceNames.TeltonikaFMB125:
				case DeviceNames.TeltonikaFMB140:
				case DeviceNames.TeltonikaFMB202:
				case DeviceNames.TeltonikaFMB630:
				case DeviceNames.TeltonikaFMB640:
				case DeviceNames.TeltonikaFMB900:
				case DeviceNames.TeltonikaFMB920:
				case DeviceNames.TeltonikaTMT250:
				case DeviceNames.TeltonikaTST100:
					return new List<CommandStep>
					{
						GetSmsCommandStep(command, $"setparam 2004:{srvAdd}"),
						GetSmsCommandStep(command, $"setparam 2005:{srvPrt}"),
						GetSmsCommandStep(command, $"setparam 2001:{apnNam}"),
						GetSmsCommandStep(command, $"setparam 2002:{apnUsr}"),
						GetSmsCommandStep(command, $"setparam 2003:{apnPwd}"),
						GetSmsCommandStep(command, $"setparam 10000:300;10004:1;10005:300"),
						GetSmsCommandStep(command, $"setparam 10200:300;10204:1;10205:300"),
						GetSmsCommandStep(command, $"setparam 10050:30;10054:1;10055:60"),
						GetSmsCommandStep(command, $"setparam 10250:30;10254:1;10255:60"),
						GetSmsCommandStep(command, $"flush {devIdn},{apnNam},{apnUsr},{apnPwd},{srvAdd},{srvPrt},{devMod}"),
					};
				default:
					return default(List<CommandStep>);
			}
		}
		private List<CommandStep> GetSetIntervalCommandSteps(IStdCommand command)
		{
			var devTyp = command?.Target?.DeviceType;
			if (null == devTyp)
				return default(List<CommandStep>);
			switch (devTyp)
			{
				case DeviceNames.TeltonikaFMB001:
				case DeviceNames.TeltonikaFMB010:
				case DeviceNames.TeltonikaFMB110:
				case DeviceNames.TeltonikaFMB120:
				case DeviceNames.TeltonikaFMB125:
				case DeviceNames.TeltonikaFMB140:
				case DeviceNames.TeltonikaFMB202:
				case DeviceNames.TeltonikaFMB630:
				case DeviceNames.TeltonikaFMB640:
				case DeviceNames.TeltonikaFMB900:
				case DeviceNames.TeltonikaFMB920:
				case DeviceNames.TeltonikaTMT250:
				case DeviceNames.TeltonikaTST100:
					return new List<CommandStep>
					{
						GetSmsCommandStep(command, "setparam 10000:300;10004:1;10005:300"),
						GetSmsCommandStep(command, "setparam 10200:300;10204:1;10205:300"),
						GetSmsCommandStep(command, "setparam 10050:30;10054:1;10055:30"),
						GetSmsCommandStep(command, "setparam 10250:30;10254:1;10255:30"),
					};
				default:
					return default(List<CommandStep>);
			}
		}
		private List<CommandStep> GetStatusCommandSteps(IStdCommand command)
		{
			var devTyp = command?.Target?.DeviceType;
			if (null == devTyp)
				return default(List<CommandStep>);
			switch (devTyp)
			{
				case DeviceNames.TeltonikaFMB001:
				case DeviceNames.TeltonikaFMB010:
				case DeviceNames.TeltonikaFMB110:
				case DeviceNames.TeltonikaFMB120:
				case DeviceNames.TeltonikaFMB125:
				case DeviceNames.TeltonikaFMB140:
				case DeviceNames.TeltonikaFMB202:
				case DeviceNames.TeltonikaFMB630:
				case DeviceNames.TeltonikaFMB640:
				case DeviceNames.TeltonikaFMB900:
				case DeviceNames.TeltonikaFMB920:
				case DeviceNames.TeltonikaTMT250:
				case DeviceNames.TeltonikaTST100:
					return new List<CommandStep>
					{
						GetSmsCommandStep(command, "getstatus"),
					};
				default:
					return default(List<CommandStep>);
			}
		}
		private List<CommandStep> GetReloadDeviceCommandSteps(IStdCommand command)
		{
			var devTyp = command?.Target?.DeviceType;
			if (null == devTyp)
				return default(List<CommandStep>);
			switch (devTyp)
			{
				case DeviceNames.TeltonikaFMB001:
				case DeviceNames.TeltonikaFMB010:
				case DeviceNames.TeltonikaFMB110:
				case DeviceNames.TeltonikaFMB120:
				case DeviceNames.TeltonikaFMB125:
				case DeviceNames.TeltonikaFMB140:
				case DeviceNames.TeltonikaFMB202:
				case DeviceNames.TeltonikaFMB630:
				case DeviceNames.TeltonikaFMB640:
				case DeviceNames.TeltonikaFMB900:
				case DeviceNames.TeltonikaFMB920:
				case DeviceNames.TeltonikaTMT250:
				case DeviceNames.TeltonikaTST100:
					return new List<CommandStep>
					{
						GetSmsCommandStep(command, "cpureset"),
					};
				default:
					return default(List<CommandStep>);
			}
		}
		private List<CommandStep> GetImmobilizeCommandSteps(IStdCommand command)
		{
			var devTyp = command?.Target?.DeviceType;
			if (null == devTyp)
				return default(List<CommandStep>);
			switch (devTyp)
			{
				case DeviceNames.TeltonikaFMB001:
				case DeviceNames.TeltonikaFMB010:
				case DeviceNames.TeltonikaFMB110:
				case DeviceNames.TeltonikaFMB120:
				case DeviceNames.TeltonikaFMB125:
				case DeviceNames.TeltonikaFMB140:
				case DeviceNames.TeltonikaFMB202:
				case DeviceNames.TeltonikaFMB630:
				case DeviceNames.TeltonikaFMB640:
				case DeviceNames.TeltonikaFMB900:
				case DeviceNames.TeltonikaTMT250:
				case DeviceNames.TeltonikaTST100:
					return new List<CommandStep>
					{
						GetSmsCommandStep(command, "setdigout 1")
					};
				case DeviceNames.TeltonikaFMB920:
					return new List<CommandStep>
					{
						GetTcpCommandStep(command, CmdType.Immobilize)
					};
				default:
					return new List<CommandStep>
					{
						GetSmsCommandStep(command, "setdigout 10", CmdType.Immobilize)
					};
			}
		}
		private List<CommandStep> GetDeimmobilizeCommandSteps(IStdCommand command)
		{
			var devTyp = command?.Target?.DeviceType;
			if (null == devTyp)
				return default(List<CommandStep>);
			switch (devTyp)
			{
				case DeviceNames.TeltonikaFMB001:
				case DeviceNames.TeltonikaFMB010:
				case DeviceNames.TeltonikaFMB110:
				case DeviceNames.TeltonikaFMB120:
				case DeviceNames.TeltonikaFMB125:
				case DeviceNames.TeltonikaFMB140:
				case DeviceNames.TeltonikaFMB202:
				case DeviceNames.TeltonikaFMB630:
				case DeviceNames.TeltonikaFMB640:
				case DeviceNames.TeltonikaFMB900:
				case DeviceNames.TeltonikaTMT250:
				case DeviceNames.TeltonikaTST100:
					return new List<CommandStep>
					{
						GetSmsCommandStep(command, "setdigout 0")
					};
				case DeviceNames.TeltonikaFMB920:
					return new List<CommandStep>
					{
						GetTcpCommandStep(command, CmdType.Deimmobilize)
					};
				default:
					return new List<CommandStep>
					{
						GetSmsCommandStep(command, "setdigout 00", CmdType.Deimmobilize)
					};
			}
		}
		private List<CommandStep> GetSomeSimpleCommandSteps(IStdCommand command)
		{
			var cmdTyp = command?.Type;
			var devTyp = command?.Target?.DeviceType;
			if (null == devTyp || null == cmdTyp)
				return default(List<CommandStep>);

			var deviceCommand =(
					from dev in new[]
					{
						DeviceNames.TeltonikaFMB001, DeviceNames.TeltonikaFMB010,
						DeviceNames.TeltonikaFMB110, DeviceNames.TeltonikaFMB120,
						DeviceNames.TeltonikaFMB125, DeviceNames.TeltonikaFMB140,
						DeviceNames.TeltonikaFMB202, DeviceNames.TeltonikaFMB630,
						DeviceNames.TeltonikaFMB640, DeviceNames.TeltonikaFMB900,
						DeviceNames.TeltonikaFMB920,
					}
					from cmd in new[]
					{
						new { Typ = CmdType.AskGPSPosition,           Cmd = "getgps"      },
						new { Typ = CmdType.AskGoogleLink,            Cmd = "ggps"        },
						new { Typ = CmdType.AskBattery,               Cmd = "battery"     },
						new { Typ = CmdType.BluetoothScan,            Cmd = "btscan"      },
						new { Typ = CmdType.BluetoothDiscoveredList,  Cmd = "btgetlist 0" },
						new { Typ = CmdType.BluetoothConnectedList,   Cmd = "btgetlist 2" },
						new { Typ = CmdType.SetOdometer,              Cmd = "odoset:0"    },
						new { Typ = CmdType.GetOdometer,              Cmd = "odoget"      },
						new { Typ = CmdType.GetOBDInfo,               Cmd = "obdinfo"     },
						new { Typ = CmdType.GetFaultCodes,            Cmd = "faultcodes"  },
						new { Typ = CmdType.GetVin,                   Cmd = "getvin"      },
					}
					where dev == devTyp && cmd.Typ == cmdTyp.Value
					select cmd
				)
				.Union(
					from dev in new[]
					{
						DeviceNames.TeltonikaTMT250, DeviceNames.TeltonikaGH5200, DeviceNames.TeltonikaTST100,
					}
					from cmd in new[]
					{
						new { Typ = CmdType.AskGPSPosition,           Cmd = "getgps"      },
						new { Typ = CmdType.AskGoogleLink,            Cmd = "ggps"        },
						new { Typ = CmdType.AskBattery,               Cmd = "battery"     },
					}
					where dev == devTyp && cmd.Typ == cmdTyp.Value
					select cmd
				)
				.FirstOrDefault();
			if (null == deviceCommand)
				return default(List<CommandStep>);

			return new List<CommandStep>
			{
				GetSmsCommandStep(command, deviceCommand.Cmd, deviceCommand.Typ),
			};
		}
		private static SmsCommandStep GetSmsCommandStep(IStdCommand command, string commandText, CmdType? waitAnswer = null)
		{
			var password = command?.Target?.Password;
			if (string.IsNullOrEmpty(password))
				password = " ";
			return new SmsCommandStep($"{password} {commandText}") { WaitAnswer = waitAnswer };
		}
		private static TcpCommandStep GetTcpCommandStep(IStdCommand command, CmdType? waitAnswer = null)
		{
			return new TcpCommandStep { WaitAnswer = waitAnswer };
		}
	}
}