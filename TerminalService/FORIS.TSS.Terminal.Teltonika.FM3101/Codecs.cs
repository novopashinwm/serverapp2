﻿namespace FORIS.TSS.Terminal.Teltonika.FM3101
{
	enum Codecs
	{
		AvlData           =    1,
		BarcodeData       =    2,
		RfidData          =    3,
		SmsAvlData        =    4,
		VlantanaData      =    5,
		IButtonData       =    6,
		/// <summary> Handheld GPS/GSM Tracker </summary>
		GH1201Data        =    7,
		/// <summary> https://wiki.teltonika-gps.com/view/Codec#Codec_8 </summary>
		Codec_8           = 0x08,
		/// <summary> https://wiki.teltonika-gps.com/view/Codec#Codec_8_Extended </summary>
		Codec_8_Extended  = 0x8E,
		/// <summary> https://wiki.teltonika-gps.com/view/Codec#Codec_12 </summary>
		GprsCommands12    =   12,
		ReservedForConfig =  255,
	}
}