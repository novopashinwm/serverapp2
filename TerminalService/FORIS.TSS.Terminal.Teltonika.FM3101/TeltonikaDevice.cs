﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.Teltonika.FM3101
{
	public partial class TeltonikaDevice : Device
	{
		internal const string ProtocolName = "Teltonika";
		internal static class DeviceNames
		{
			internal const string TeltonikaFM1100 = "Teltonika FM1100";
			internal const string TeltonikaFM1110 = "Teltonika FM1110";
			internal const string TeltonikaFM1125 = "Teltonika FM1125";
			internal const string TeltonikaFM1200 = "Teltonika FM1200";
			internal const string TeltonikaFM2200 = "Teltonika FM2200";
			internal const string TeltonikaFM3101 = "Teltonika FM3101";
			internal const string TeltonikaFM5300 = "Teltonika FM5300";
			internal const string TeltonikaFMA202 = "Teltonika FMA202";
			internal const string TeltonikaFMB001 = "Teltonika FMB001";
			internal const string TeltonikaFMB010 = "Teltonika FMB010";
			internal const string TeltonikaFMB110 = "Teltonika FMB110";
			internal const string TeltonikaFMB120 = "Teltonika FMB120";
			internal const string TeltonikaFMB125 = "Teltonika FMB125";
			internal const string TeltonikaFMB140 = "Teltonika FMB140";
			internal const string TeltonikaFMB202 = "Teltonika FMB202";
			internal const string TeltonikaFMB630 = "Teltonika FMB630";
			internal const string TeltonikaFMB640 = "Teltonika FMB640";
			internal const string TeltonikaFMB900 = "Teltonika FMB900";
			internal const string TeltonikaFMB920 = "Teltonika FMB920";
			internal const string TeltonikaGH3000 = "Teltonika GH3000";
			internal const string TeltonikaTMT250 = "Teltonika TMT250";
			internal const string TeltonikaGH5200 = "Teltonika GH5200";
			internal const string TeltonikaTST100 = "Teltonika TST100";
		}
		public TeltonikaDevice()
			: base(null, new[]
		{
			DeviceNames.TeltonikaFM1100,
			DeviceNames.TeltonikaFM1110,
			DeviceNames.TeltonikaFM1125,
			DeviceNames.TeltonikaFM1200,
			DeviceNames.TeltonikaFM2200,
			DeviceNames.TeltonikaFM3101,
			DeviceNames.TeltonikaFM5300,
			DeviceNames.TeltonikaFMA202,
			DeviceNames.TeltonikaFMB001,
			DeviceNames.TeltonikaFMB010,
			DeviceNames.TeltonikaFMB110,
			DeviceNames.TeltonikaFMB120,
			DeviceNames.TeltonikaFMB125,
			DeviceNames.TeltonikaFMB140,
			DeviceNames.TeltonikaFMB202,
			DeviceNames.TeltonikaFMB630,
			DeviceNames.TeltonikaFMB640,
			DeviceNames.TeltonikaFMB900,
			DeviceNames.TeltonikaFMB920,
			DeviceNames.TeltonikaGH3000,
			DeviceNames.TeltonikaTMT250,
			DeviceNames.TeltonikaGH5200,
			DeviceNames.TeltonikaTST100,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			// Первый пакет, содержащий строку с IMEI
			if (data.Length == 17 && data[0] == 0x00 && data[1] == 0x0F)
				return true;
			// Первые 4 байта - преамбула
			if (data.Length >= 04 && data[0] == 0x00 && data[1] == 0x00 && data[2] == 0x00 && data[3] == 0x00)
				return true;
			return false;
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			var device = (stateData as ReceiverState)?.DeviceID;
			var parsed = Parse(data, device, out bufferRest);
			if (device != null && parsed != null)
			{
				foreach (var item in parsed)
				{
					var mu = item as IMobilUnit;
					if (mu == null)
						continue;
					mu.DeviceID = device;
				}
			}
			return parsed;
		}
		public static int GetCRC16(byte[] buffer, int offset, int bufLen, int polynom, int preset)
		{
			preset  &= 0xFFFF;
			polynom &= 0xFFFF;
			int crc = preset;
			for (int i = 0; i < bufLen; i++)
			{
				int data = buffer[i + offset] & 0xFF;
				crc ^= data;
				for (int j = 0; j < 8; j++)
				{
					if ((crc & 0x0001) != 0)
					{
						crc = (crc >> 1) ^ polynom;
					}
					else
					{
						crc = crc >> 1;
					}
				}
			}
			return crc & 0xFFFF;
		}
		private static readonly byte[]        PreambleData = new byte[]        { 0x00, 0x00, 0x00, 0x00 };
		private static readonly ConfirmPacket AcceptPacket = new ConfirmPacket { Data = new byte[] { 0x01 } };
		private static readonly ConfirmPacket NoDataPacket = new ConfirmPacket { Data = PreambleData };
		private static readonly IList         NoData       = new[]             { NoDataPacket };
		private IList Parse(byte[] data, string deviceId, out byte[] bufferRest)
		{
			bufferRest = null;
			// Первый пакет, содержащий строку с IMEI. Нужно ответить посылкой 0x01, чтобы терминал начал присылать данные
			if (data.Length == 17 && data[0] == 0x00 && data[1] == 0x0F)
			{
				var deviceID = new byte[15];
				Array.Copy(data, 2, deviceID, 0, deviceID.Length);

				return new ArrayList
				{
					new ReceiverStoreToStateMessage { StateData = new ReceiverState { DeviceID = Encoding.ASCII.GetString(deviceID) } },
					AcceptPacket
				};
			}

			// Первые 4 байта - преамбула
			if (!(data.Length >= 04 && data[0] == 0x00 && data[1] == 0x00 && data[2] == 0x00 && data[3] == 0x00))
			{
				Trace.TraceWarning(GetType() + ": Wrong datagram received: {0}", BitConverter.ToString(data));
				return NoData;
			}

			// dataLength - количество байт в посылке, исключая преамбулу (4B), количество байт (4B) и CRC (4B)
			int dataLength = (data[4] << 24) | (data[5] << 16) | (data[6] << 8) | data[7];

			// В устройстве хранится не более 1Мб данных, так что странно принимать данные более 2Мб
			if (1024 * 1024 * 2 < dataLength)
			{
				Trace.TraceWarning(GetType() + ": dataLength too high, data: {0}", BitConverter.ToString(data));
				return NoData;
			}

			if (data.Length < dataLength + 12)
			{
				// накапливать пакеты с данными, пока не будет получен полный набор данных.
				bufferRest = (byte[])data.Clone();
				return null;
			}

			bufferRest = new byte[data.Length - dataLength - 12];
			Array.Copy(data, dataLength + 12, bufferRest, 0, data.Length - dataLength - 12);

			var codecID              = data[8];
			var recordsCountInTheBeg = data[9];
			var recordsCountInTheEnd = (int)data[8 + dataLength - 1];

			var codec = (Codecs)codecID;
			if (codec == Codecs.GH1201Data &&
				recordsCountInTheEnd != recordsCountInTheBeg)
			{
				Trace.TraceWarning(GetType() + ": Number of data in the start and end does not match, data: {0}", BitConverter.ToString(data));
				return NoData;
			}

			var calculatedCRC = GetCRC16(data, 8, dataLength, 0xA001, 0);
			// CRC в пакете хранится в 16 младших разрядах слова, идущего за массивом записей
			var receivedCRC = (data[8 + dataLength + 2] << 8) | data[8 + dataLength + 2 + 1];
			if (calculatedCRC != receivedCRC)
			{
				Trace.TraceWarning(GetType() + ": Invalid crc: {0}", BitConverter.ToString(data));
				return NoData;
			}
			if (codec == Codecs.GprsCommands12)
			{
				return ParseCmdAnswerPacket(data, deviceId);
			}
			var dt         = new FM3101Datagram { Data = data };

			var mobilUnits = new List<object>(dt.SubData.Count + 1);

			var protocol = $@"{ProtocolName}.{codec}";

			foreach (AvlData dg in dt.SubData)
			{
				var mobilUnit = UnitReceived(protocol);
				mobilUnits.Add(mobilUnit);

				mobilUnit.DeviceID   = deviceId;
				mobilUnit.cmdType    = CmdType.Trace;
				mobilUnit.Time       = dg.Time;
				mobilUnit.Latitude   = dg.Latitude;
				mobilUnit.Longitude  = dg.Longitude;
				mobilUnit.Speed      = dg.Speed;
				mobilUnit.Course     = dg.Course;
				mobilUnit.Height     = dg.Altitude;
				mobilUnit.Satellites = dg.Satellites;
				mobilUnit.CorrectGPS = dg.ValidGPS && dg.Satellites >= 3;

				if (dg.Properties != null && dg.Properties.Count != 0)
				{
					mobilUnit.SensorValues = new Dictionary<int, long>(dg.Properties.Count);
					foreach (var property in dg.Properties)
						AssignProperty(codec, mobilUnit, property);
					//тревога
				}
				if (mobilUnit.SensorValues != null)
				{
					// Обработка свойств, которые приходят из раздела событий, но преобразуются в цифровой датчик
					if ((Manager?.FillUnitInfo(mobilUnit) ?? false) && mobilUnit.Unique > 0)
					{
						var eventAvlId = 236;
						if ((mobilUnit.SensorMap?.SelectMany(m => m.Value)?.Any(m => m.InputNumber == eventAvlId) ?? false)
							&& !mobilUnit.SensorValues.ContainsKey(eventAvlId))
							mobilUnit.SensorValues[eventAvlId] = 0;
					}
				}
				else
					mobilUnit.SensorValues = new Dictionary<int, long>();
				mobilUnit.SensorValues[1000] = dg.Priority == 2 ? 1 : 0;
			}

			var confirmData = BitConverter.GetBytes(dt.SubData.Count);
			Array.Reverse(confirmData); //to Big-Endian
			mobilUnits.Add(new ConfirmPacket { Data = confirmData });

			return mobilUnits;
		}
		private static void AssignProperty(Codecs codec, MobilUnit.Unit.MobilUnit unit, FM3101Property property)
		{
			if (!(property.value is IConvertible))
				return;
			unit.SensorValues.Add(property.ID, Convert.ToInt64(property.value));
			switch (codec)
			{
				case Codecs.Codec_8:
				case Codecs.Codec_8_Extended:
					switch (property.ID)
					{
						case 9:
							unit.VoltageAN1   = (Int16)property.value;
							break;
						//case 10:
						//	unit.VoltageAN2   = (Int16)property.value;
						//	break;
						//case 11:
						//	unit.VoltageAN3   = (Int16)property.value;
						//	break;
						//case 19:
						//	unit.VoltageAN4   = (Int16)property.value;
						//	break;
						case 66:
							unit.PowerVoltage = (Int16)property.value;
							break;
					}
					break;
			}
		}
	}
}