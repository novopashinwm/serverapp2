﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Communication;
using FORIS.TSS.Common;

namespace FORIS.TSS.Terminal.Teltonika.FM3101
{
	public class AvlData : Datagram
	{
		private static readonly int SecondsFrom1970To2007 = TimeHelper.GetSecondsFromBase(
			new DateTime(2007, 01, 01, 0, 0, 0, 0, DateTimeKind.Utc));

		internal static AvlData Create(Codecs codec, byte[] data, ref int BytePos, ref int BitPos)
		{
			AvlData dt = new AvlData { data = data, ByteCounter = BytePos, BitCounter = BitPos };
			switch (codec)
			{
				case Codecs.AvlData:
					dt.ReadAvlData();
					break;
				case Codecs.GH1201Data:
					dt.ReadGH1201();
					break;
				case Codecs.Codec_8:
					dt.ReadCodec8XData(dt.ReadCodec8Properties);
					break;
				case Codecs.Codec_8_Extended:
					dt.ReadCodec8XData(dt.ReadCodec8EProperties);
					break;
			}

			BytePos = dt.ByteCounter;
			BitPos  = dt.BitCounter;

			return dt;
		}

		int   _priority   = 0;
		float _longitude  = 9999.0f;
		float _latitude   = 9999.0f;
		int   _altitude   = 0;
		int   _angle      = 0;
		int   _satellites = 0;
		int   _speed      = 0;
		int   numberOfProperties;
		List<FM3101Property> _properties = new List<FM3101Property>();

		/// <summary>Побитово извлекает из потока данных целочисленное слово длиной в size байт</summary>
		/// <remarks>Считается, что слово хранится в потоке в Big-Endian, а массив после извлечения - Little-Endian</remarks>
		/// <param name="size">Длина слова в байтах</param>
		private byte[] GetIntFromData(int size)
		{
			byte[] tmp = new byte[size];
			for (int i = size-1; i >= 0; i--)
			{
				for (int j = 0; j < 8; j++)
				{
					tmp[i] >>= 1;
					tmp[i] |= (byte)(PopOneBitFromProperties() << 7);
				}
			}
			return tmp;
		}
		private uint PopUIntFromBitsBigEndian(int bits)
		{
			if (bits < 0 || 32 < bits)
				throw new ArgumentOutOfRangeException("bits", bits, "Value must be between 0 and 31");

			uint result = 0;

			for (var i = 0; i != bits; ++i)
				result |= (((uint) PopOneBitFromProperties()) << i);

			return result;
		}
		private float PopFloatFromBytesBE()
		{
			if (BitCounter != 0)
				throw new InvalidOperationException("Invalid BitCounter value: " + BitCounter + ". Only zero value allowed");

			var result = FloatFromBytesBE(data, ByteCounter, 4);
			ByteCounter += 4;
			return result;
		}
		void ReadAvlData()
		{
			long timestamp = BitConverter.ToInt64(GetIntFromData(8), 0);
			_time              = (int) ((timestamp + 500)/1000);
			_priority          = (int)GetIntFromData(1)[0];
			_longitude         = ((float)BitConverter.ToInt32(GetIntFromData(4), 0)) / 10000000;
			_latitude          = ((float)BitConverter.ToInt32(GetIntFromData(4), 0)) / 10000000;
			_altitude          = BitConverter.ToInt16(GetIntFromData(2), 0);
			_angle             = BitConverter.ToInt16(GetIntFromData(2), 0);
			_satellites        = (int)GetIntFromData(1)[0];
			_speed             = BitConverter.ToInt16(GetIntFromData(2), 0);
			numberOfProperties = (int)GetIntFromData(1)[0];
			ValidGPS           = true;
			ReadAvlProperties();
		}
		void ReadGH1201()
		{
			try
			{
				_priority = data[ByteCounter] >> 6;
				var timeStampFrom2007 = ((data[ByteCounter] & 0x3f) << 24) |
										( data[ByteCounter+1] << 16) |
										( data[ByteCounter+2] << 8) |
										data[ByteCounter+3];

				ByteCounter += 4;

				_time = timeStampFrom2007 + SecondsFrom1970To2007;

				//Глобальная маска
				bool hasGPSElement = PopOneBitFromProperties() != 0;
				bool hasIOElement1B = PopOneBitFromProperties() != 0;
				bool hasIOElement2B = PopOneBitFromProperties() != 0;
				bool hasIOElement4B = PopOneBitFromProperties() != 0;

				//Оставшиеся 4 бита
				PopOneBitFromProperties();
				PopOneBitFromProperties();
				PopOneBitFromProperties();
				PopOneBitFromProperties();

				if (hasGPSElement)
				{
					var hasLatLng           = PopOneBitFromProperties() != 0;
					var hasAlt              = PopOneBitFromProperties() != 0;
					var hasAngle            = PopOneBitFromProperties() != 0;
					var hasSpeed            = PopOneBitFromProperties() != 0;
					var hasSatellites       = PopOneBitFromProperties() != 0;
					var hasCellID           = PopOneBitFromProperties() != 0;
					var hasGSMSignalQuality = PopOneBitFromProperties() != 0;
					var hasOperatorCode     = PopOneBitFromProperties() != 0;

					if (hasLatLng)
					{
						_latitude  = PopFloatFromBytesBE();
						_longitude = PopFloatFromBytesBE();
					}

					if (hasAlt)
						_altitude = IntFromBytesBE(GetIntFromData(2));

					if (hasAngle)
						_angle = IntFromBytesBE(GetIntFromData(1)) * 360 / 256;

					if (hasSpeed)
						_speed = IntFromBytesBE(GetIntFromData(1));

					if (hasSatellites)
						_satellites = IntFromBytesBE(GetIntFromData(1));

					ValidGPS = hasLatLng;

					if (hasCellID) //Local area code and cell id
						SkipBytes(4);

					if (hasGSMSignalQuality)
						SkipBytes(1);

					if (hasOperatorCode)
						SkipBytes(4);
				}

				if (hasIOElement1B)
				{
					var quantity = IntFromBytesBE(GetIntFromData(1));
					SkipBytes(2 * quantity);
				}
				if (hasIOElement2B)
				{
					var quantity = IntFromBytesBE(GetIntFromData(1));
					SkipBytes(3 * quantity);
				}
				if (hasIOElement4B)
				{
					var quantity = IntFromBytesBE(GetIntFromData(1));
					SkipBytes(5 * quantity);
				}
			}
			catch (Exception ex)
			{
				Trace.WriteLine(ex + Environment.NewLine +
					(bytes != null
						? ("Data: " + BitConverter.ToString(bytes) + Environment.NewLine)
						: string.Empty) +
					"ByteCounter: " + ByteCounter + Environment.NewLine +
					"BitCounter" + BitCounter);
				throw;
			}
		}
		private void ReadCodec8XData(Action readCodec8XProperties)
		{
			var timestamp = LongFromBytesLE(GetIntFromData(8));
			_time         = (int)((timestamp + 500) / 1000);
			_priority     = GetIntFromData(1)[0];

			_longitude    = (float)IntToGeo(IntFromBytesLE(GetIntFromData(4)));
			_latitude     = (float)IntToGeo(IntFromBytesLE(GetIntFromData(4)/*, 0*/));

			_altitude     = IntFromBytesLE(GetIntFromData(2));
			_angle        = IntFromBytesLE(GetIntFromData(2));
			_satellites   = GetIntFromData(1)[0];
			_speed        = IntFromBytesLE(GetIntFromData(2)/*, 0*/);

			if (_speed == 0xFF)
			{
				ValidGPS = false;
				_speed   = 0;
			}
			else
			{
				ValidGPS = true;
			}
			readCodec8XProperties();
		}
		private void ReadCodec8Properties()
		{
			//Skip Event IO ID
			SkipBytes(1);

			//Total IO Count
			SkipBytes(1);

			var count1B = GetIntFromData(1)[0];
			for (var i = 0; i != count1B; ++i)
			{
				var evt1BId  = GetIntFromData(1)[0];
				var evt1BVal = GetIntFromData(1)[0];
				_properties.Add(new FM3101Property
				{
					ID    = evt1BId,
					value = evt1BVal,
					type  = typeof(Byte),
				});
			}

			var count2B = GetIntFromData(1)[0];
			for (var i = 0; i != count2B; ++i)
			{
				var evt2BId  =        GetIntFromData(1)[0];
				var evt2BVal = (Int16)GetIntFromData(2).FromLE2UInt16(0, 2);
				_properties.Add(new FM3101Property
				{
					ID    = evt2BId,
					value = evt2BVal,
					type  = typeof(Int16),
				});
			}

			var count4B = GetIntFromData(1)[0];
			for (var i = 0; i != count4B; ++i)
			{
				var evt4BId  =        GetIntFromData(1)[0];
				var evt4BVal = (Int32)GetIntFromData(4).FromLE2UInt32(0, 4);
				_properties.Add(new FM3101Property
				{
					ID    = evt4BId,
					value = evt4BVal,
					type  = typeof(Int32),
				});
			}

			var count8B = GetIntFromData(1)[0];
			for (var i = 0; i != count8B; ++i)
			{
				var evt8BId  =        GetIntFromData(1)[0];
				var evt8BVal = (Int64)GetIntFromData(8).FromLE2UInt64(0, 8);
				_properties.Add(new FM3101Property
				{
					ID    = evt8BId,
					value = evt8BVal,
					type  = typeof(Int64),
				});
			}
		}
		private void ReadCodec8EProperties()
		{
			//Skip Event IO ID
			SkipBytes(2);

			//Total IO Count
			SkipBytes(2);

			var count1B = GetIntFromData(2).FromLE2UInt16(0, 2);
			for (var i = 0; i != count1B; ++i)
			{
				var evt1BId  = GetIntFromData(2).FromLE2UInt16(0, 2);
				var evt1BVal = GetIntFromData(1)[0];
				_properties.Add(new FM3101Property
				{
					ID    = evt1BId,
					value = evt1BVal,
					type  = typeof(Byte),
				});
			}

			var count2B = GetIntFromData(2).FromLE2UInt16(0, 2);
			for (var i = 0; i != count2B; ++i)
			{
				var evt2BId  =        GetIntFromData(2).FromLE2UInt16(0, 2);
				var evt2BVal = (Int16)GetIntFromData(2).FromLE2UInt16(0, 2);
				_properties.Add(new FM3101Property
				{
					ID    = evt2BId,
					value = evt2BVal,
					type  = typeof(Int16),
				});
			}

			var count4B = GetIntFromData(2).FromLE2UInt16(0, 2);
			for (var i = 0; i != count4B; ++i)
			{
				var evt4BId  =        GetIntFromData(2).FromLE2UInt16(0, 2);
				var evt4BVal = (Int32)GetIntFromData(4).FromLE2UInt32(0, 4);
				_properties.Add(new FM3101Property
				{
					ID    = evt4BId,
					value = evt4BVal,
					type  = typeof(Int32),
				});
			}

			var count8B = GetIntFromData(2).FromLE2UInt16(0, 2);
			for (var i = 0; i != count8B; ++i)
			{
				var evt8BId  =        GetIntFromData(2).FromLE2UInt16(0, 2);
				var evt8BVal = (Int64)GetIntFromData(8).FromLE2UInt64(0, 8);
				_properties.Add(new FM3101Property
				{
					ID    = evt8BId,
					value = evt8BVal,
					type  = typeof(Int64),
				});
			}

			var countXB = GetIntFromData(2).FromLE2UInt16(0, 2);
			for (var i = 0; i != countXB; ++i)
			{
				var evtXBId  = GetIntFromData(2).FromLE2UInt16(0, 2);
				var evtXBLen = GetIntFromData(2).FromLE2UInt16(0, 2);
				var evtXBVal = GetIntFromData(evtXBLen);
				_properties.Add(new FM3101Property
				{
					ID    = evtXBId,
					value = evtXBVal,
					type  = evtXBVal.GetType(),
				});
			}
		}
		private static double IntToGeo(int i)
		{
			const double precision = 10000000.0;
			return i / precision;
		}
		private void SkipBits(int count)
		{
			var newBits = BitCounter + count;
			BitCounter = newBits%8;
			ByteCounter += newBits/8;
		}
		private void SkipBytes(int count)
		{
			ByteCounter += count;
		}
		void ReadAvlProperties()
		{
			int PropNumber = 0; //Номер свойства
			while (true)
			{
				int bit = PopOneBitFromProperties();
				if (bit == 0)
				{
					PropNumber++;
					continue;
				}
				//Св-во c ID PropNumber существует, будем смотреть, что в нем.
				//Для этого прочитаем еще 3 бита
				int PropType = 0;
				for (int i = 0; i < 3; i++)
				{
					PropType <<= 1;
					PropType |= PopOneBitFromProperties();
				}

				switch ((PropertyType)PropType)
				{
					case PropertyType.boolean:
						bool value = PopOneBitFromProperties() == 1;
						FM3101Property p = new FM3101Property();
						p.ID = PropNumber;
						p.type = typeof(bool);
						p.value = value;
						_properties.Add(p);
						break;
					case PropertyType.integer:
						int _value = 0;
						for (int i = 0; i < 32; i++)
						{
							_value <<= 1;
							_value |= PopOneBitFromProperties();
						}
						FM3101Property _p = new FM3101Property();
						_p.ID = PropNumber;
						_p.type = typeof(int);
						_p.value = _value;
						_properties.Add(_p);
						break;
					default:
						throw new Exception("FM3101: Unknown property type!");
				}
				PropNumber++;
				if (_properties.Count >= this.NumberOfProperties)
					break;
			}

		}
		/// <summary>
		/// Данные
		/// </summary>
		byte[] data;
		public int Time
		{
			get
			{
				//long timestamp = BitConverter.ToInt64(new byte[] { data[pos + 7], data[pos + 6], data[pos + 5], data[pos + 4], data[pos + 3], data[pos + 2], data[pos + 1], data[pos] }, 0);
				return _time;
			}
		}
		public int Priority
		{
			get
			{
				//return (int)data[pos + 8];
				return _priority;
			}
		}
		public float Longitude
		{
			get
			{
				//return ((float)BitConverter.ToInt32(new byte[] { data[pos + 12], data[pos + 11], data[pos + 10], data[pos + 9] }, 0)) / 10000000;
				return _longitude;
			}
		}
		public float Latitude
		{
			get
			{
				//return ((float)BitConverter.ToInt32(new byte[] { data[pos + 16], data[pos + 15], data[pos + 14], data[pos + 13] }, 0)) / 10000000;
				return _latitude;
			}
		}
		public int Altitude
		{
			get
			{
				//return (int)BitConverter.ToInt16(new byte[] { data[pos + 18], data[pos + 17] }, 0);
				return _altitude;
			}
		}
		public bool ValidGPS
		{
			get; private set;
		}
		public int Course
		{
			get
			{
				//return (int)BitConverter.ToInt16(new byte[] { data[pos + 20], data[pos + 19] }, 0);
				return _angle;
			}
		}
		public int Satellites
		{
			get
			{
				//return (int)data[pos + 21];
				return _satellites;
			}
		}
		public int Speed
		{
			get
			{
				//return (int)BitConverter.ToInt16(new byte[] { data[pos + 23], data[pos + 22] }, 0); ;
				return _speed;
			}
		}
		public int NumberOfProperties
		{
			get
			{
				//return (int)data[pos + 24];
				return numberOfProperties;
			}
		}
		private int ByteCounter = 0;
		private int BitCounter = 0;
		private int _time;
		private int PopOneBitFromProperties()
		{
			byte current_byte = data[ByteCounter];

			int result = ((current_byte >> BitCounter) & (byte)0x01);

			BitCounter++;
			if (BitCounter >= 8)
			{
				BitCounter = 0;
				ByteCounter++;
			}

			return result;
		}
		public List<FM3101Property> Properties
		{
			get
			{
				return _properties;
			}
		}
	}
}