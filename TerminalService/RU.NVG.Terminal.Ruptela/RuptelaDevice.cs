﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace RU.NVG.Terminal.Ruptela
{
    public class RuptelaDevice : Device
    {
        private static class FieldLength
        {
            public const int PacketLength = 2;
            public const int IMEI = 8;
            public const int CommandId = 1;
            public const int PayloadDataMin = 1;
            public const int PayloadDataMax = 1019;
            public const int CRC = 2;
        }

        public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
        {
            var result = new ArrayList();
            var reader = new DataReader(data, 0, count);
            if (reader.RemainingBytesCount < 2)
            {
                bufferRest = data.TakeToArray(count); 
                return result;
            }

            var packetLength = (int)reader.ReadBigEndian32(2);
            if (count < FieldLength.PacketLength + packetLength + FieldLength.CRC)
            {
                bufferRest = data.TakeToArray(count);
                return result;
            }

            reader.Skip(packetLength);
            var receivedCrc = reader.ReadBigEndian32(FieldLength.CRC);
            var calculatedCrc = CalculateCrc(data, FieldLength.PacketLength, packetLength);
            if (receivedCrc != calculatedCrc)
            {
                bufferRest = null;
                return null;
            }

            reader.Rewind(packetLength + FieldLength.CRC);
            var imei = reader.ReadBigEndian64()
                .ToString(CultureInfo.InvariantCulture)
                .PadLeft(ImeiHelper.ImeiLength, '0');
            var deviceId = imei;
            var commandId = reader.ReadByte();
            if (commandId == 1)
            {
                reader.Skip(); //Records left
                var numberOfRecords = reader.ReadByte();
                for (var i = 0; i != numberOfRecords; ++i)
                {
                    var mu = UnitReceived(ControllerType.Names.Ruptela);
                    mu.DeviceID = deviceId;
                    result.Add(mu);
                    mu.Time = (int)reader.ReadBigEndian32(4);
                    reader.Skip(1); //Time stamp extension
                    reader.Skip(1); //Priority
                    mu.Longitude = (double) (((int)reader.ReadBigEndian32(4))/10000000M);
                    mu.Latitude = (double)(((int)reader.ReadBigEndian32(4)) / 10000000M);
                    mu.Height = (int)Math.Round(reader.ReadBigEndian32(2)/10M);
                    mu.Course = (int) Math.Round(reader.ReadBigEndian32(2)/10M);
                    mu.Satellites = reader.ReadByte();
                    mu.CorrectGPS = mu.Satellites != 0;
                    mu.Speed = (int)Math.Round(reader.ReadBigEndian32(2) / 10M);
                    reader.Skip(1); //HDOP
                    reader.Skip(1); //IO Data caused record
                    mu.SensorValues = new Dictionary<int, long>();

                    var ioCount1Byte = reader.ReadByte();
                    for (var j = 0; j != ioCount1Byte; ++j)
                    {
                        var id = reader.ReadByte();
                        var value = reader.ReadByte();
                        mu.SensorValues[id] = value;
                    }

                    var ioCount2Byte = reader.ReadByte();
                    for (var j = 0; j != ioCount2Byte; ++j)
                    {
                        var id = reader.ReadByte();
                        var value = (short)reader.ReadBigEndian32(2);
                        mu.SensorValues[id] = value;
                    }

                    var ioCount4Byte = reader.ReadByte();
                    for (var j = 0; j != ioCount4Byte; ++j)
                    {
                        var id = reader.ReadByte();
                        var value = (int)reader.ReadBigEndian32(4);
                        mu.SensorValues[id] = value;
                    }

                    var ioCount8Byte = reader.ReadByte();
                    for (var j = 0; j != ioCount8Byte; ++j)
                    {
                        var id = reader.ReadByte();
                        var value = (long)reader.ReadBigEndian64();
                        mu.SensorValues[id] = value;
                    }
                }
            }
            else
            {
                reader.Skip(reader.RemainingBytesCount - FieldLength.CRC);
            }

            bufferRest = null;
            result.Add(new ConfirmPacket(new byte[]
                {
                    0x00, 0x02, 0x64, 0x01, 0x13, 0xbc
                }));

            return result;
        }

        public override bool SupportData(byte[] data)
        {
            if (data.Length <
                FieldLength.PacketLength + FieldLength.IMEI + FieldLength.CommandId + FieldLength.PayloadDataMin +
                FieldLength.CRC)
                return false;

            var reader = new DataReader(data);
            var packetLength = (int)reader.ReadBigEndian32(FieldLength.PacketLength);
            if (FieldLength.IMEI + FieldLength.CommandId + FieldLength.PayloadDataMax < packetLength)
                return false;
            if (packetLength < FieldLength.IMEI + FieldLength.CommandId + FieldLength.PayloadDataMin)
                return false;

            var imei = reader.ReadBigEndian64(FieldLength.IMEI);

            //Уточнить данный признак у компании Ruptela
            if (imei < 860000000000000UL || 869999999999999UL < imei)
                return false;

            if (data.Length == FieldLength.PacketLength + packetLength + FieldLength.CRC)
            {
                reader.Skip(reader.RemainingBytesCount - FieldLength.CRC);
                var receivedCrc = reader.ReadBigEndian32(FieldLength.CRC);
                var calculatedCrc = CalculateCrc(data, FieldLength.PacketLength,  packetLength);
                if (receivedCrc != calculatedCrc)
                    return false;
            }

            return true;
        }

        private static ushort CalculateCrc(byte[] pucData, int index, int count)
        {
            const ushort usPoly = 0x8408; //reversed 0x1021
            ushort usCRC = 0;
            //--------------------------------------------------------------------
            for (int i = index, stopIndex = index + count; i < stopIndex; i++)
            {
                usCRC ^= pucData[i];
                byte ucBit;
                for (ucBit = 0; ucBit < 8; ucBit++)
                {
                    byte ucCarry = (byte) (usCRC & 1);
                    usCRC >>= 1;
                    if (ucCarry != 0)
                    {
                        usCRC ^= usPoly;
                    }
                }
            }
            return usCRC;
        }
    }
}
