﻿using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Linq;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers;
using RU.NVG.Protocol.Egts.Enums;
using RU.NVG.Protocol.Egts.Helpers;
using RU.NVG.Protocol.Egts.Interface;
using RU.NVG.Protocol.Egts.Packet;

namespace RU.NVG.Protocol.Egts
{
	/// <summary> Пакет транспортного уровня </summary>
	public class TransportPacket : IPacket
	{
		public const int ProtocolVersion = 0x01;

		/// <summary>
		/// Максимальный размер фрейма данных
		/// </summary>
		public const int MaxFrameSize = 65517;

		/// <summary>
		///
		/// </summary>
		protected class Header
		{
			private BitArray _arr = new BitArray(8);

			/// <summary>
			/// (Route) определяет необходимость дальнейшей маршрутизации данного пакета на удаленный аппаратно-программный комплекс,
			/// а также наличие опциональных параметров PRA, RCA, TTL, необходимых для маршрутизации данного пакета.
			/// Если поле имеет значение 1, то необходима маршрутизация и поля PRA, RCA, TTL присутствуют в пакете.
			/// Данное поле устанавливает Диспетчер того аппаратно-программного комплекса, на котором сгенерирован пакет,
			/// или абонентский терминал, сгенерировавший пакет для отправки на аппаратно-программный комплекс,
			/// в случае установки в нем параметра "HOME_DISPATCHER_ID", определяющего адрес аппаратно-программного комплекса,
			/// на котором данный абонентский терминал зареги-стрирован.
			/// </summary>
			internal bool Rte
			{
				get { return _arr[5]; }
				set { _arr[5] = value; }
			}

			/// <summary>
			/// (Encryption Algorithm) определяет код алгоритма, используемый для шифрования данных из поля SFRD.
			/// Если поле имеет значение 00, то данные в поле SFRD не шифруются
			/// </summary>
			internal byte Ena
			{
				get { return (byte)((_arr[3] ? 1 : 0) + (_arr[4] ? 2 : 0)); }
				set
				{
					_arr[3] = (value & 1) == 1;
					_arr[4] = (value & 2) == 2;
				}
			}

			/// <summary>
			/// (Compressed) определяет, используется ли сжатие дан-ных из поля SFRD.
			/// Если поле имеет значение 1, то данные в поле SFRD считаются сжатыми.
			/// </summary>
			internal bool Cmp
			{
				get { return _arr[2]; }
				set { _arr[2] = value; }
			}

			/// <summary>
			/// определяет приоритет маршрутизации данного пакета и может принимать следующие значения:
			///     00 - наивысший
			///     01 - высокий
			///     10 - средний
			///     11 - низкий
			/// При получении пакета Диспетчер производит маршрутизацию пакета с более высоким приоритетом быстрее, чем пакетов с низким приоритетом
			/// </summary>
			internal byte Pr
			{
				get { return (byte)((_arr[0] ? 1 : 0) + (_arr[1] ? 2 : 0)); }
				set
				{
					_arr[0] = (value & 1) == 1;
					_arr[1] = (value & 2) == 2;
				}
			}

			/// <summary>
			/// Значение заголовка.
			/// </summary>
			internal byte Value
			{
				get
				{
					var arr = new byte[1];
					_arr.CopyTo(arr, 0);
					return arr[0];
				}
				set
				{
					_arr = new BitArray(new[] { value });
				}
			}
		}

		/// <summary>
		/// содержит номер пакета Транспортного уровня, увеличивающийся на 1 при отправке каждого нового пакета на стороне отправителя.
		/// Значения в данном поле изменяются по правилам циклического счетчика в диапазоне от 0 до 65535,
		/// т.е. при достижении значения 65535 следующее значение 0
		/// </summary>
		public ushort Pid
		{
			get { return _pid; }
			set { _pid = value; }
		}

		/// <summary>
		/// Приоритет обработки пакета.
		/// </summary>
		public Priority Priority
		{
			get { return (Priority)Hr.Pr; }
			set { Hr.Pr = (byte)value; }
		}

		/// <summary>
		/// определяет идентификатор ключа, используемого при шифровании
		/// </summary>
		protected byte Skid;

		/// <summary>
		/// доп. информация и флаги
		/// </summary>
		protected Header Hr = new Header
		{
			Rte = false,
			Ena = 0x00,
			Cmp = false,
		};

		/// <summary>
		/// длина заголовка Транспортного уровня в байтах с учетом байта контрольной суммы (поля HCS)
		/// </summary>
		protected byte Hl
		{
			get
			{
				return (byte)(Hr.Rte ? 16 : 11);
			}
		}

		/// <summary>
		/// определяет применяемый метод кодирования следующей за данным параметром части заголовка Транспортного уровня
		/// </summary>
		protected byte He;

		/// <summary>
		/// определяет размер в байтах поля данных SFRD, содержащего информацию протокола Уровня поддержки услуг
		/// </summary>
		protected ushort Fdl
		{
			get
			{
				return (ushort)Sfrd.Length;
			}
		}

		/// <summary>
		/// тип пакета Транспортного уровня
		/// </summary>
		protected PacketType Pt
		{
			get { return ServiceFrame.Type; }
		}

		/// <summary>
		/// адрес аппаратно-программного комплекса, на котором данный пакет сгенерирован.
		/// Данный адрес является уникальным в рамках сети и используется для создания пакета-подтверждения на принимающей стороне
		/// </summary>
		protected ushort Pra;

		/// <summary>
		/// адрес аппаратно-программного комплекса, для которого данный пакет предназначен.
		/// По данному адресу производится идентификация принадлежности пакета определенного аппаратно-программного комплекса
		/// и его маршрутизация при использовании промежуточных аппаратно-программных комплексов
		/// </summary>
		protected ushort Rca;

		/// <summary>
		/// Поле TTL - время жизни пакета при его маршрутизации между аппаратно-программными комплексами.
		/// Использование данного параметра предотвращает зацикливание пакета при ретрансляции в системах со сложной топологией адресных пунктов.
		/// </summary>
		protected byte Ttl;

		/// <summary>
		/// структура данных, зависящая от типа пакета и содержащая информацию Протокола уровня поддержки услуг
		/// </summary>
		protected byte[] Sfrd
		{
			get { return ServiceFrame.GetBytes(); }
		}

		/// <summary>
		/// структура данных, зависящая от типа пакета и содержащая информацию Протокола уровня поддержки услуг
		/// </summary>
		public IServiceFrame ServiceFrame { get; private set; }

		/// <summary>
		///
		/// </summary>
		/// <param name="serviceFrame"></param>
		public TransportPacket(IServiceFrame serviceFrame)
		{
			ServiceFrame = serviceFrame;
			_pid = (ushort)PidGenerator.NextPid();
		}

		internal TransportPacket()
		{
		}

		/// <summary>
		/// Записывает объект в поток с помощью BinaryWriter.
		/// </summary>
		/// <param name="writer"></param>
		public void WriteTo(BinaryWriter writer)
		{
			var bytes = GetHeaderBytes();
			using (var stream = new MemoryStream())
			using (var streamWriter = new BinaryWriter(stream))
			{
				streamWriter.Write(bytes);
				streamWriter.Write(bytes.Crc8());
				if (Sfrd != null)
				{
					streamWriter.Write(Sfrd);
					streamWriter.Write(Sfrd.Crc16Ccitt());
				}

				streamWriter.Flush();
				writer.Write(stream.ToArray());
			}
		}

		/// <summary>
		/// Читает значения полей из потока с помощью BinaryReader.
		/// </summary>
		/// <param name="reader"></param>
		/// <returns></returns>
		public bool ReadFrom(BinaryReader reader)
		{
			byte[] buf;
			int readed;
			DataReader dataReader;

			buf = new byte[10];
			readed = reader.BaseStream.Read(buf, 0, buf.Length);
			if (readed < buf.Length)
				return false;
			dataReader = new DataReader(buf);

			//Чтение данных из потока.
			var prv = dataReader.ReadByte();
			if (prv != ProtocolVersion)
				throw new ArgumentException("Version must be 0x01");

			var skid = dataReader.ReadByte();
			var hrByte = dataReader.ReadByte();
			var hr = new Header { Value = hrByte };
			var hl = dataReader.ReadByte();
			var he = dataReader.ReadByte();
			var fdl = (ushort)dataReader.ReadLittleEndian32(2);
			var pid = (ushort)dataReader.ReadLittleEndian32(2);
			var pt = (PacketType)dataReader.ReadByte();

			ushort pra = 0, rca = 0;
			byte ttl = 0;
			if (hr.Rte)
			{
				buf = new byte[5];
				readed = reader.BaseStream.Read(buf, 0, buf.Length);
				if (readed < buf.Length)
					return false;

				dataReader = new DataReader(buf);

				pra = (ushort)dataReader.ReadLittleEndian32(2);
				rca = (ushort)dataReader.ReadLittleEndian32(2);
				ttl = dataReader.ReadByte();
			}

			buf = new byte[1 + (fdl > 0 ? fdl + 2 : 0)];
			readed = reader.BaseStream.Read(buf, 0, buf.Length);
			if (readed < buf.Length)
				return false;

			dataReader = new DataReader(buf);

			var headerChecksum = dataReader.ReadByte();
			var sfrd = fdl > 0 ? dataReader.ReadBytes(fdl) : null;
			var sfrdChecksum = sfrd != null ? (ushort)dataReader.ReadLittleEndian32(2) : 0;
			if (fdl == 0)
			{
				if (reader.BaseStream.Position + 2 <= reader.BaseStream.Length)
				{
					var emptyChecksum = reader.ReadBytes(2);
					if (!emptyChecksum.SequenceEqual(new byte[] { 0xff, 0xff }))
						reader.BaseStream.Seek(-2, SeekOrigin.Current);
				}
			}

			using (var stream = new MemoryStream())
			using (var writer = new BinaryWriter(stream))
			{
				writer.Write(prv);
				writer.Write(skid);
				writer.Write(hrByte);
				writer.Write(hl);
				writer.Write(he);
				writer.Write(fdl);
				writer.Write(pid);
				writer.Write((byte)pt);
				if (hr.Rte)
				{
					writer.Write(pra);
					writer.Write(rca);
					writer.Write(ttl);
				}

				var headerBytes = stream.ToArray();
				var computedHeaderChecksum = headerBytes.Crc8();
				if (headerChecksum != computedHeaderChecksum)
				{
					throw new ArgumentException("header checksum");
				}
			}

			//Чтение структуры поддержки сервисов.
			IServiceFrame frame = null;
			if (sfrd != null)
			{
				//Проверяем контрольную сумму данных.
				var computedSfrdChecksum = sfrd.Crc16Ccitt();
				if (computedSfrdChecksum != sfrdChecksum)
				{
					throw new ArgumentException("sfrd checksum");
				}

				switch (pt)
				{
					case PacketType.EgtsPtAppdata:
						frame = EgtsPtAppdata.FromBytes(sfrd);
						break;
					case PacketType.EgtsPtResponse:
						frame = EgtsPtResponse.FromBytes(sfrd);
						break;
					default:
						throw new NotImplementedException(pt.ToString());
				}
			}

			// Присваивание значений полей.
			ServiceFrame = frame;
			Skid = skid;
			Hr.Value = hr.Value;
			He = he;
			Pid = pid;
			if (!Hr.Rte) return true;

			Pra = pra;
			Rca = rca;
			Ttl = ttl;

			return true;
		}

		public override string ToString()
		{
			return string.Format(CultureInfo.CurrentCulture,
				"Pid={0}\r\nPriority={1}\r\nSkid={2}\r\nHr={3}\r\nHl={4}\r\nHe={5}\r\nFdl={6}\r\nPt={7}\r\nPra={8}\r\nRca={9}\r\nTtl={10}\r\nSfrd={11}\r\n",
				Pid,
				Priority,
				Skid,
				Hr.Value,
				Hl,
				He,
				Fdl,
				Pt,
				Pra,
				Rca,
				Ttl,
				ServiceFrame);
		}

		/// <summary>
		///
		/// </summary>
		/// <returns></returns>
		private byte[] GetHeaderBytes()
		{
			using (var stream = new MemoryStream())
			using (var writer = new BinaryWriter(stream))
			{
				WriteHeader(writer);
				return stream.ToArray();
			}
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="writer"></param>
		private void WriteHeader(BinaryWriter writer)
		{
			writer.Write(Prv);
			writer.Write(Skid);
			writer.Write(Hr.Value);
			writer.Write(Hl);
			writer.Write(He);
			writer.Write((short)Fdl);
			writer.Write(Pid);
			writer.Write((byte)Pt);

			//PRV, PRF, PR, CMP, ENA, RTE, HL, HE, FDL, PID, PT, PRA, RCA, TTL, HCS
			if (Hr.Rte)
			{
				writer.Write(Pra);
				writer.Write(Rca);
				writer.Write(Ttl);
			}
		}

		/// <summary> Значение данного параметра инкрементируется каждый раз при внесении изменений в структуру заголовка </summary>
		private const byte Prv = 0x01;

		private ushort _pid;
	}
}