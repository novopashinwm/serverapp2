﻿using RU.NVG.Protocol.Egts.Record.Auth;

namespace RU.NVG.Protocol.Egts.Services
{
	/// <summary> Класс для работы с сервисом EGTS_AUTH_SERVICE </summary>
	public static class AuthServiceFabric
	{
		public static EgtsSrTermIdentity TermIdentity(string imei)
		{
			var termIdentity = new EgtsSrTermIdentity
			{
				Imei = imei,
			};
			return termIdentity;
		}
		public static EgtsSrTermIdentity TermIdentity(int tid)
		{
			var termIdentity = new EgtsSrTermIdentity
			{
				Tid = (uint)tid,
			};
			return termIdentity;
		}
	}
}