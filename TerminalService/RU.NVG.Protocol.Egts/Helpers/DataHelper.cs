﻿using System;
using System.IO;
using FORIS.TSS.BusinessLogic;
using RU.NVG.Protocol.Egts.Interface;

namespace RU.NVG.Protocol.Egts.Helpers
{
    public static class DataHelper
    {
        public static readonly DateTime Year2010 = new DateTime(2010, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static readonly int Diff2010FromBase = (int)(Year2010 - TimeHelper.year1970).TotalSeconds;

        public static byte[] GetBytes(this IWritable record)
        {
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                record.WriteTo(writer);
                return stream.ToArray();
            }
        }

    }
}
