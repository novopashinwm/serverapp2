﻿using System;
using System.IO;
using RU.NVG.Protocol.Egts.Enums;
using RU.NVG.Protocol.Egts.Interface;
using RU.NVG.Protocol.Egts.Packet;
using RU.NVG.Protocol.Egts.Record;
using RU.NVG.Protocol.Egts.Record.Auth;
using RU.NVG.Protocol.Egts.Record.Teledata;

namespace RU.NVG.Protocol.Egts.Helpers
{
	public class EgtsReader : BinaryReader
	{
		public EgtsReader(Stream input) : base(input)
		{
		}
		/// <summary> Не блокирующий метод для чтения из сетевого потока </summary>
		/// <param name="packet"></param>
		/// <returns></returns>
		public bool TryReadPacket(out TransportPacket packet)
		{
			packet = null;
			var startPosition = BaseStream.Position;
			if (startPosition == BaseStream.Length)
				return false;

			packet = ReadPacket();
			return packet != null;
		}
		/// <summary> Блокирующий метод для чтения из сетевого потока </summary>
		/// <returns></returns>
		public TransportPacket ReadPacket()
		{
			var packet = new TransportPacket();
			return packet.ReadFrom(this)
				? packet
				: null;
		}
		public EgtsPtAppdata ReadAppdataPacket()
		{
			if (BaseStream.Position == BaseStream.Length)
				return null;

			var result = new EgtsPtAppdata();
			result.ReadFrom(this);
			return result;
		}
		public EgtsPtResponse ReadResponsePacket()
		{
			if (BaseStream.Position == BaseStream.Length)
				return null;

			var result = new EgtsPtResponse();
			result.ReadFrom(this);
			return result;
		}
		public ServiceLayerRecord ReadServiceLayerRecord()
		{
			if (BaseStream.Position == BaseStream.Length)
				return null;

			var result = new ServiceLayerRecord();
			result.ReadFrom(this);
			return result;
		}
		/// <summary> Читает подзапись уровня поддержки услуг </summary>
		/// <returns></returns>
		internal ServiceLayerSubRecord ReadServiceLayerSubRecord(ServiceLayerRecord serviceLayerRecord)
		{
			if (BaseStream.Position == BaseStream.Length)
				return null;

			var serviceLayerSubRecord = new ServiceLayerSubRecord();
			serviceLayerSubRecord.ReadFrom(this, serviceLayerRecord);
			return serviceLayerSubRecord;
		}
		/// <summary> Читает подзапись </summary>
		/// <param name="serviceCode"></param>
		/// <param name="srt"></param>
		/// <returns></returns>
		public ISubRecord ReadSubRecord(ServiceCode serviceCode, byte srt)
		{
			if (srt == 0)
			{
				return ReadResponseSubRecord(serviceCode);
			}

			switch (serviceCode)
			{
				case ServiceCode.EGTS_AUTH_SERVICE:
					var authSubRecordType = (EgtsAuthServiceSubRecords)srt;
					switch (authSubRecordType)
					{
						case EgtsAuthServiceSubRecords.EGTS_SR_RESULT_CODE:
							return ReadResultCodeSubRecord();
						case EgtsAuthServiceSubRecords.EGTS_SR_TERM_IDENTITY:
							return ReadTermIdentity();
						case EgtsAuthServiceSubRecords.EGTS_SR_MODULE_DATA:
							return null;
						default:
							throw new NotImplementedException($"not implemented EgtsAuthServiceSubRecords: {authSubRecordType}");
					}
				case ServiceCode.EGTS_TELEDATA_SERVICE:
					var teledataSubRecordType = (EgtsTeledataServiceSubRecords)srt;
					switch (teledataSubRecordType)
					{
						case EgtsTeledataServiceSubRecords.EGTS_SR_POS_DATA:
							return ReadPosData();
						case EgtsTeledataServiceSubRecords.EGTS_SR_EXT_POS_DATA:
							return ReadExtPosData();
						case EgtsTeledataServiceSubRecords.EGTS_SR_STATE_DATA:
							return ReadStateData();
						case EgtsTeledataServiceSubRecords.EGTS_SR_AD_SENSORS_DATA:
							return ReadAdSensorsData();
						case EgtsTeledataServiceSubRecords.EGTS_SR_LIQUID_LEVEL_SENSOR:
							return ReadLiquidLevel();
						case EgtsTeledataServiceSubRecords.EGTS_SR_COUNTERS_DATA:
							return ReadCountersData();
						default:
							throw new NotImplementedException($"not implemented EgtsTeledataServiceSubRecords: {teledataSubRecordType}");
					}
				default:
					throw new NotImplementedException($"not implemented serviceCode: {serviceCode}");
			}
		}
		public byte[] GetRest(long position)
		{
			if (position > BaseStream.Length)
				return null;

			var restLength = BaseStream.Length - position;
			var result = new byte[restLength];
			BaseStream.Seek(position, SeekOrigin.Begin);
			BaseStream.Read(result, 0, result.Length);

			return result;
		}
		private ISubRecord ReadExtPosData()
		{
			var result = new EgtsSrExtPosData();
			result.ReadFrom(this);
			return result;
		}
		private ISubRecord ReadTermIdentity()
		{
			var result = new EgtsSrTermIdentity();
			result.ReadFrom(this);
			return result;
		}
		private ISubRecord ReadPosData()
		{
			var result = new EgtsSrPosData();
			result.ReadFrom(this);
			return result;
		}
		private ISubRecord ReadLiquidLevel()
		{
			var result = new EgtsSrLiquidLevelSensor();
			result.ReadFrom(this);
			return result;
		}
		private ISubRecord ReadStateData()
		{
			var result = new EgtsSrStateData();
			result.ReadFrom(this);
			return result;
		}
		private ISubRecord ReadAdSensorsData()
		{
			var result = new EgtsSrAdSensorsData();
			result.ReadFrom(this);
			return result;
		}
		private ISubRecord ReadCountersData()
		{
			var result = new EgtsSrCountersData();
			result.ReadFrom(this);
			return result;
		}
		private ISubRecord ReadResponseSubRecord(ServiceCode serviceCode)
		{
			var result = new EgtsSrRecordResponse(serviceCode);
			result.ReadFrom(this);
			return result;
		}
		private ISubRecord ReadResultCodeSubRecord()
		{
			var result = new EgtsSrResultCode();
			result.ReadFrom(this);
			return result;
		}
	}
}