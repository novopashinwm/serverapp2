﻿using System.IO;
using RU.NVG.Protocol.Egts.Interface;

namespace RU.NVG.Protocol.Egts.Helpers
{
	public class EgtsWriter : BinaryWriter
	{
		public EgtsWriter(Stream stream) : base(stream)
		{
		}
		public void Write(IPacket packet)
		{
			packet.WriteTo(this);
		}
	}
}