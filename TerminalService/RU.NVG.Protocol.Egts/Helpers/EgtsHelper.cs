﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.TerminalService.Interfaces;
using RU.NVG.Protocol.Egts.Enums;
using RU.NVG.Protocol.Egts.Record;
using RU.NVG.Protocol.Egts.Record.Teledata;

namespace RU.NVG.Protocol.Egts.Helpers
{
	public static class EgtsHelper
	{
		public const string ProtocolName = "EGTS";
		/// <summary> Преобразование сервисной записи EGTS в набор мобильных объектов </summary>
		/// <param name="rec"> Сервисная запись EGTS </param>
		public static List<IMobilUnit> ToMobilUnits(this ServiceLayerRecord rec)
		{
			var records   = rec.GetRecords();
			var result    = new List<IMobilUnit>(records.Count);
			var mobilUnit = default(MobilUnit);
			foreach (var subRecord in records)
			{
				switch (subRecord.Service)
				{
					case ServiceCode.EGTS_TELEDATA_SERVICE:
						var teledataServiceRecordType = (EgtsTeledataServiceSubRecords)subRecord.ServiceRecordType;
						switch (teledataServiceRecordType)
						{
							case EgtsTeledataServiceSubRecords.EGTS_SR_POS_DATA:
								var posData = (EgtsSrPosData)subRecord;
								mobilUnit = new MobilUnit
								{
									Latitude   = posData.Latitude,
									Longitude  = posData.Longitude,
									Time       = TimeHelper.GetSecondsFromBase(posData.NavigationTime),
									CorrectGPS = posData.Valid,
									Run        = (int)posData.Odometer,
									Height     = posData.Altitude * 100,
									Speed      = (int?)Math.Round(posData.Speed),
									Course     = posData.Move ? posData.Course : (int?)null
								};

								mobilUnit.SetSensorValue((int)EgtsSensor.DigitalInput1, posData.DigitalInput1 ? 1 : 0);
								mobilUnit.SetSensorValue((int)EgtsSensor.DigitalInput2, posData.DigitalInput2 ? 1 : 0);
								mobilUnit.SetSensorValue((int)EgtsSensor.DigitalInput3, posData.DigitalInput3 ? 1 : 0);
								mobilUnit.SetSensorValue((int)EgtsSensor.DigitalInput4, posData.DigitalInput4 ? 1 : 0);
								mobilUnit.SetSensorValue((int)EgtsSensor.DigitalInput5, posData.DigitalInput5 ? 1 : 0);
								mobilUnit.SetSensorValue((int)EgtsSensor.DigitalInput6, posData.DigitalInput6 ? 1 : 0);
								mobilUnit.SetSensorValue((int)EgtsSensor.DigitalInput7, posData.DigitalInput7 ? 1 : 0);
								mobilUnit.SetSensorValue((int)EgtsSensor.DigitalInput8, posData.DigitalInput8 ? 1 : 0);
								if (posData.Ignition.HasValue)
									mobilUnit.SetSensorValue((int)EgtsSensor.Ignition, posData.Ignition.Value ? 1 : 0);

								mobilUnit.Properties.Add(DeviceProperty.Protocol, ProtocolName);
								result.Add(mobilUnit);
								break;
							case EgtsTeledataServiceSubRecords.EGTS_SR_EXT_POS_DATA:
								var exPosData = (EgtsSrExtPosData)subRecord;
								if (mobilUnit == null)
									break;
								if (exPosData.Hdop.HasValue)
									mobilUnit.SetSensorValue((int)EgtsSensor.Hdop, exPosData.Hdop.Value);
								if (exPosData.Vdop.HasValue)
									mobilUnit.SetSensorValue((int)EgtsSensor.Vdop, exPosData.Vdop.Value);
								if (exPosData.Pdop.HasValue)
									mobilUnit.SetSensorValue((int)EgtsSensor.Pdop, exPosData.Pdop.Value);
								if (exPosData.Satellites.HasValue)
									mobilUnit.Satellites = exPosData.Satellites.Value;

								break;
							case EgtsTeledataServiceSubRecords.EGTS_SR_AD_SENSORS_DATA:
								var sensorsData = (EgtsSrAdSensorsData)subRecord;
								if (mobilUnit == null)
									break;

								if (sensorsData.AnalogueInput1 != null)
									mobilUnit.SetSensorValue((int)EgtsSensor.AnalogueInput1, sensorsData.AnalogueInput1.Value);
								if (sensorsData.AnalogueInput2 != null)
									mobilUnit.SetSensorValue((int)EgtsSensor.AnalogueInput2, sensorsData.AnalogueInput2.Value);
								if (sensorsData.AnalogueInput3 != null)
									mobilUnit.SetSensorValue((int)EgtsSensor.AnalogueInput3, sensorsData.AnalogueInput3.Value);
								if (sensorsData.AnalogueInput4 != null)
									mobilUnit.SetSensorValue((int)EgtsSensor.AnalogueInput4, sensorsData.AnalogueInput4.Value);
								if (sensorsData.AnalogueInput5 != null)
									mobilUnit.SetSensorValue((int)EgtsSensor.AnalogueInput5, sensorsData.AnalogueInput5.Value);
								if (sensorsData.AnalogueInput6 != null)
									mobilUnit.SetSensorValue((int)EgtsSensor.AnalogueInput6, sensorsData.AnalogueInput6.Value);
								if (sensorsData.AnalogueInput7 != null)
									mobilUnit.SetSensorValue((int)EgtsSensor.AnalogueInput7, sensorsData.AnalogueInput7.Value);
								if (sensorsData.AnalogueInput8 != null)
									mobilUnit.SetSensorValue((int)EgtsSensor.AnalogueInput8, sensorsData.AnalogueInput8.Value);
								break;
							case EgtsTeledataServiceSubRecords.EGTS_SR_STATE_DATA:
								var stateData = (EgtsSrStateData)subRecord;
								if (mobilUnit == null)
									break;
								break;
							case EgtsTeledataServiceSubRecords.EGTS_SR_LIQUID_LEVEL_SENSOR:
								var liquidLevelSensor = (EgtsSrLiquidLevelSensor)subRecord;
								if (mobilUnit == null)
									break;

								if (liquidLevelSensor.Valid && liquidLevelSensor.Value.HasValue)
								{
									if (liquidLevelSensor.Unit == EgtsSrLiquidLevelSensor.MeasureUnit.Liters)
										mobilUnit.SetSensorValue((int)EgtsSensor.LiquidLevelLiters, liquidLevelSensor.Value.Value);
									if (liquidLevelSensor.Unit == EgtsSrLiquidLevelSensor.MeasureUnit.NoUnit)
										mobilUnit.SetSensorValue((int)EgtsSensor.LiquidLevelNoUnit, liquidLevelSensor.Value.Value);
									if (liquidLevelSensor.Unit == EgtsSrLiquidLevelSensor.MeasureUnit.Percentage)
										mobilUnit.SetSensorValue((int)EgtsSensor.LiquidLevelPercentage, liquidLevelSensor.Value.Value);
								}
								break;
							default:
								break;
						}
						break;
				}
			}

			return result;
		}
		/// <summary> Формат подзаписи EGTS_SR_POS_DATA </summary>
		/// <param name="mu"> Мобильный объект </param>
		/// <param name="withoutSensors"> Преобразовать без датчиков? </param>
		/// <returns> Всегда возвращает не null </returns>
		public static EgtsSrPosData ToSrPosData(this IMobilUnit mu, bool withoutSensors = false)
		{
			// Преобразуем стандартные поля подзаписи без датчиков
			var subRecord = new EgtsSrPosData
			{
				NavigationTime = TimeHelper.GetDateTimeUTC(mu.Time),
				Latitude       = mu.Latitude,
				Longitude      = mu.Longitude,
				Valid          = mu.ValidPosition,
				Odometer       = mu.Run,
				Altitude       = mu.Height / 100,
			};
			if (mu.Speed.HasValue)
				subRecord.Speed = mu.Speed.Value;
			if (mu.Course.HasValue)
				subRecord.Course = mu.Course.Value;

			// Датчики не нужны или в пакете нет датчиков, то дальше нечего преобразовывать
			if (withoutSensors || mu.SensorValues == null)
				return subRecord;

			// Проверяем исходный протокол, и действуем по разному в зависимости EGTS это или нет
			if(ProtocolName.Equals(mu.GetProtocolName(), StringComparison.OrdinalIgnoreCase))
			{// EGTS, работаем напрямую по датчикам EGTS, без зависимости от производных датчиков
				var mappings = new Dictionary<EgtsSensor, Action<long>>
				{
					{ EgtsSensor.DigitalInput1, (v) => subRecord.DigitalInput1 = v == 1 },
					{ EgtsSensor.DigitalInput2, (v) => subRecord.DigitalInput2 = v == 1 },
					{ EgtsSensor.DigitalInput3, (v) => subRecord.DigitalInput3 = v == 1 },
					{ EgtsSensor.DigitalInput4, (v) => subRecord.DigitalInput4 = v == 1 },
					{ EgtsSensor.DigitalInput5, (v) => subRecord.DigitalInput5 = v == 1 },
					{ EgtsSensor.DigitalInput6, (v) => subRecord.DigitalInput6 = v == 1 },
					{ EgtsSensor.DigitalInput7, (v) => subRecord.DigitalInput7 = v == 1 },
					{ EgtsSensor.DigitalInput8, (v) => subRecord.DigitalInput8 = v == 1 },
					{ EgtsSensor.Ignition,      (v) => subRecord.Ignition      = v == 1 },
				};
				foreach (var mapping in mappings)
				{
					if (mu.SensorValues.TryGetValue((int)mapping.Key, out var sensorValue))
						mapping.Value(sensorValue);
				}
			}
			else
			{// Любой протокол, кроме EGTS, работаем через производные датчики
				if (0 < (mu.SensorMap?.Count ?? 0))
				{
					var mappings = new Dictionary<SensorLegend, Action<long>>
					{
						{ SensorLegend.ToEgtsDigitalSensor1, (v) => subRecord.DigitalInput1 = v == 1 },
						{ SensorLegend.ToEgtsDigitalSensor2, (v) => subRecord.DigitalInput2 = v == 1 },
						{ SensorLegend.ToEgtsDigitalSensor3, (v) => subRecord.DigitalInput3 = v == 1 },
						{ SensorLegend.ToEgtsDigitalSensor4, (v) => subRecord.DigitalInput4 = v == 1 },
						{ SensorLegend.ToEgtsDigitalSensor5, (v) => subRecord.DigitalInput5 = v == 1 },
						{ SensorLegend.ToEgtsDigitalSensor6, (v) => subRecord.DigitalInput6 = v == 1 },
						{ SensorLegend.ToEgtsDigitalSensor7, (v) => subRecord.DigitalInput7 = v == 1 },
						{ SensorLegend.ToEgtsDigitalSensor8, (v) => subRecord.DigitalInput8 = v == 1 },
						{ SensorLegend.ToEgtsIgnition,       (v) => subRecord.Ignition      = v == 1 },
					};
					foreach (var mapping in mappings)
					{
						var sensorValue = mu.GetRawValuesByLegend(mapping.Key)?.FirstOrDefault();
						if (sensorValue.HasValue)
							mapping.Value(sensorValue.Value);
					}
				}
			}

			return subRecord;
		}
		/// <summary> Формат подзаписи EGTS_SR_EXT_POS_DATA </summary>
		/// <param name="mu"> Мобильный объект </param>
		/// <param name="withoutSensors"> Преобразовать без датчиков? </param>
		/// <returns> Всегда возвращает не null </returns>
		public static EgtsSrExtPosData ToSrExPosData(this IMobilUnit mu, bool withoutSensors = false)
		{
			// Преобразуем стандартные поля подзаписи без датчиков
			var subRecord = new EgtsSrExtPosData
			{
				Satellites = mu.Satellites > 0 ? (byte?)mu.Satellites : null
			};

			// Датчики не нужны или в пакете нет датчиков, то дальше нечего преобразовывать
			if (withoutSensors || mu.SensorValues == null)
				return subRecord;

			// Проверяем исходный протокол, и действуем по разному в зависимости EGTS это или нет
			if (ProtocolName.Equals(mu.GetProtocolName(), StringComparison.OrdinalIgnoreCase))
			{// EGTS, работаем напрямую по датчикам EGTS, без зависимости от производных датчиков
				var mappings = new Dictionary<EgtsSensor, Action<long>>
				{
					{ EgtsSensor.Hdop, (v) => subRecord.Hdop = (ushort?)v },
					{ EgtsSensor.Vdop, (v) => subRecord.Vdop = (ushort?)v },
					{ EgtsSensor.Pdop, (v) => subRecord.Pdop = (ushort?)v },
				};
				foreach (var mapping in mappings)
				{
					if (mu.SensorValues.TryGetValue((int)mapping.Key, out var sensorValue))
						mapping.Value(sensorValue);
				}
			}
			else
			{// Любой протокол, кроме EGTS, работаем через производные датчики
				if (0 < (mu.SensorMap?.Count ?? 0))
				{
					var mappings = new Dictionary<SensorLegend, Action<long>>
					{
						{ SensorLegend.ToEgtsHDOP, (v) => subRecord.Hdop = (ushort?)v },
						{ SensorLegend.ToEgtsVDOP, (v) => subRecord.Vdop = (ushort?)v },
						{ SensorLegend.ToEgtsPDOP, (v) => subRecord.Pdop = (ushort?)v },
					};
					foreach (var mapping in mappings)
					{
						var sensorValue = mu.GetRawValuesByLegend(mapping.Key)?.FirstOrDefault();
						if (sensorValue.HasValue)
							mapping.Value(sensorValue.Value);
					}
				}
			}

			return subRecord;
		}
		/// <summary> Формат подзаписи EGTS_SR_AD_SENSORS_DATA </summary>
		/// <param name="mu"> Мобильный объект </param>
		/// <returns> Возвращает null, если нет значения ни одного аналогового датчика </returns>
		public static EgtsSrAdSensorsData ToSrAdSensorsData(this IMobilUnit mu)
		{
			// В пакете нет датчиков нечего преобразовывать
			if (mu.SensorValues == null)
				return null;
			var subRecord = new EgtsSrAdSensorsData();
			// Проверяем исходный протокол, и действуем по разному в зависимости EGTS это или нет
			if (ProtocolName.Equals(mu.GetProtocolName(), StringComparison.OrdinalIgnoreCase))
			{// EGTS, работаем напрямую по датчикам EGTS, без зависимости от производных датчиков
				var mappings = new Dictionary<EgtsSensor, Action<long>>
				{
					{ EgtsSensor.AnalogueInput1, (v) => subRecord.AnalogueInput1 = (int?)v },
					{ EgtsSensor.AnalogueInput2, (v) => subRecord.AnalogueInput2 = (int?)v },
					{ EgtsSensor.AnalogueInput3, (v) => subRecord.AnalogueInput3 = (int?)v },
					{ EgtsSensor.AnalogueInput4, (v) => subRecord.AnalogueInput4 = (int?)v },
					{ EgtsSensor.AnalogueInput5, (v) => subRecord.AnalogueInput5 = (int?)v },
					{ EgtsSensor.AnalogueInput6, (v) => subRecord.AnalogueInput6 = (int?)v },
					{ EgtsSensor.AnalogueInput7, (v) => subRecord.AnalogueInput7 = (int?)v },
					{ EgtsSensor.AnalogueInput8, (v) => subRecord.AnalogueInput8 = (int?)v },
				};
				foreach (var mapping in mappings)
				{
					if (mu.SensorValues.TryGetValue((int)mapping.Key, out var sensorValue))
						mapping.Value(sensorValue);
				}
			}
			else
			{// Любой протокол, кроме EGTS, работаем через производные датчики
				if (0 < (mu.SensorMap?.Count ?? 0))
				{
					var mappings = new Dictionary<SensorLegend, Action<long>>
					{
						{ SensorLegend.ToEgtsAnalogSensor1, (v) => subRecord.AnalogueInput1 = (int?)v },
						{ SensorLegend.ToEgtsAnalogSensor2, (v) => subRecord.AnalogueInput2 = (int?)v },
						{ SensorLegend.ToEgtsAnalogSensor3, (v) => subRecord.AnalogueInput3 = (int?)v },
						{ SensorLegend.ToEgtsAnalogSensor4, (v) => subRecord.AnalogueInput4 = (int?)v },
						{ SensorLegend.ToEgtsAnalogSensor5, (v) => subRecord.AnalogueInput5 = (int?)v },
						{ SensorLegend.ToEgtsAnalogSensor6, (v) => subRecord.AnalogueInput6 = (int?)v },
						{ SensorLegend.ToEgtsAnalogSensor7, (v) => subRecord.AnalogueInput7 = (int?)v },
						{ SensorLegend.ToEgtsAnalogSensor8, (v) => subRecord.AnalogueInput8 = (int?)v },
					};
					foreach (var mapping in mappings)
					{
						var sensorValue = mu.GetRawValuesByLegend(mapping.Key)?.FirstOrDefault();
						if (sensorValue.HasValue)
							mapping.Value(sensorValue.Value);
					}
				}
			}
			var hasData =
				subRecord.AnalogueInput1.HasValue ||
				subRecord.AnalogueInput2.HasValue ||
				subRecord.AnalogueInput3.HasValue ||
				subRecord.AnalogueInput4.HasValue ||
				subRecord.AnalogueInput5.HasValue ||
				subRecord.AnalogueInput6.HasValue ||
				subRecord.AnalogueInput7.HasValue ||
				subRecord.AnalogueInput8.HasValue;
			return hasData ? subRecord : null;
		}
		/// <summary> Формат подзаписи EGTS_SR_LIQUID_LEVEL_SENSOR </summary>
		/// <param name="mu"> Мобильный объект </param>
		/// <returns></returns>
		public static IEnumerable<EgtsSrLiquidLevelSensor> ToSrLiquidLevelSensors(this IMobilUnit mu)
		{
			if (null != mu.SensorValues)
			{
				if (mu.SensorValues.ContainsKey((int)EgtsSensor.LiquidLevelLiters))
				{
					var liquidLevelSensor = new EgtsSrLiquidLevelSensor();
					liquidLevelSensor.SetValueWithUnit(
						(int?)mu.SensorValues[(int)EgtsSensor.LiquidLevelLiters],
						EgtsSrLiquidLevelSensor.MeasureUnit.Liters);
					yield return liquidLevelSensor;
				}
				if (mu.SensorValues.ContainsKey((int)EgtsSensor.LiquidLevelNoUnit))
				{
					var liquidLevelSensor = new EgtsSrLiquidLevelSensor();
					liquidLevelSensor.SetValueWithUnit(
						(int?)mu.SensorValues[(int)EgtsSensor.LiquidLevelNoUnit],
						EgtsSrLiquidLevelSensor.MeasureUnit.NoUnit);
					yield return liquidLevelSensor;
				}
				if (mu.SensorValues.ContainsKey((int)EgtsSensor.LiquidLevelPercentage))
				{
					var liquidLevelSensor = new EgtsSrLiquidLevelSensor();
					liquidLevelSensor.SetValueWithUnit(
						(int?)mu.SensorValues[(int)EgtsSensor.LiquidLevelPercentage],
						EgtsSrLiquidLevelSensor.MeasureUnit.Percentage);
					yield return liquidLevelSensor;
				}
			}
		}
	}
}