﻿using System.Threading;

namespace RU.NVG.Protocol.Egts
{
	public static class PidGenerator
	{
		private static long _lastPid;

		public static long LastPid
		{
			get { return _lastPid; }
		}

		public static long NextPid()
		{
			return Interlocked.Increment(ref _lastPid) % 65536;
		}
	}
}