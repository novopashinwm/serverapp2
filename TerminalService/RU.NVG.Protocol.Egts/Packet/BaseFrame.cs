﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using RU.NVG.Protocol.Egts.Enums;
using RU.NVG.Protocol.Egts.Helpers;
using RU.NVG.Protocol.Egts.Interface;
using RU.NVG.Protocol.Egts.Record;

namespace RU.NVG.Protocol.Egts.Packet
{
	public abstract class BaseFrame : IServiceFrame
	{
		private readonly int _maxSize;
		/// <summary> Структуры SDR 1, SDR 2, SDR n содержат информацию Уровня поддержки услуг </summary>
		private readonly List<ServiceLayerRecord> _sdrs = new List<ServiceLayerRecord>();
		public abstract PacketType Type { get; }
		/// <summary> Размер записи в байтах </summary>
		public virtual int Size
		{
			get { return _sdrs.Sum(sdr => sdr.Size); }
		}
		/// <summary> Записи данных, содержащиеся в пакете </summary>
		public IEnumerable<ServiceLayerRecord> Records
		{
			get { return _sdrs; }
		}
		/// <summary> Получить массив байт </summary>
		/// <returns></returns>
		public byte[] GetBytes()
		{
			using (var stream = new MemoryStream())
			using (var writer = new BinaryWriter(stream))
			{
				WriteTo(writer);
				return stream.ToArray();
			}
		}
		public virtual void ReadFrom(EgtsReader reader)
		{
			var record = reader.ReadServiceLayerRecord();
			while (record != null)
			{
				_sdrs.Add(record);
				record = reader.ReadServiceLayerRecord();
			}
		}
		/// <summary> Записать данные в писатель потока </summary>
		/// <param name="writer"></param>
		public virtual void WriteTo(BinaryWriter writer)
		{
			foreach (var record in Records)
				record.WriteTo(writer);
		}
		protected BaseFrame(int maxSize)
		{
			_maxSize = maxSize;
		}
		/// <summary> Добавляет подзапись в фрейм данных </summary>
		/// <param name="oid"> Идентификатор объекта </param>
		/// <param name="subRecord"> Подзапись данных для сервиса </param>
		/// <returns> true если запись добавлена </returns>
		protected virtual bool AddSubRecord(ISubRecord subRecord, uint? oid = null)
		{
			var serviceCode = subRecord.Service;
			var serviceLayerRecord = _sdrs.FirstOrDefault(sdr => sdr.ServiceCode == serviceCode && (!oid.HasValue || sdr.Oid == oid.Value));
			var serviceLayerSubRecord = new ServiceLayerSubRecord(subRecord);
			if (serviceLayerRecord == null)
			{
				serviceLayerRecord = ServiceLayerRecord.Create(oid, serviceCode);
				serviceLayerRecord.Add(serviceLayerSubRecord);
				if (serviceLayerRecord.Size + Size > _maxSize) 
					return false;
				_sdrs.Add(serviceLayerRecord);
			}
			else
			{
				if (serviceLayerSubRecord.Size + Size > _maxSize) 
					return false;
				serviceLayerRecord.Add(serviceLayerSubRecord);
			}

			return true;
		}
	}
}