﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using RU.NVG.Protocol.Egts.Enums;
using RU.NVG.Protocol.Egts.Helpers;
using RU.NVG.Protocol.Egts.Interface;
using RU.NVG.Protocol.Egts.Record;
using RU.NVG.Protocol.Egts.Record.Auth;

namespace RU.NVG.Protocol.Egts.Packet
{
	/// <summary> Подтверждение на пакет Транспортного уровня </summary>
	public class EgtsPtResponse : BaseFrame
	{
		/// <summary> Идентификатор пакета Транспортного уровня, подтверждение на который сформировано </summary>
		public ushort Rpid;
		/// <summary> Код результата обработки части пакета, относящейся к Транспортному уровню </summary>
		public ResponseResult Pr;
		public override PacketType Type
		{
			get
			{
				return PacketType.EgtsPtResponse;
			}
		}
		/// <summary> Размер записи в байтах </summary>
		public override int Size
		{
			get
			{
				return base.Size + 2 + 1;
			}
		}
		internal EgtsPtResponse(int maxSize = TransportPacket.MaxFrameSize) : base(maxSize)
		{
		}
		public override void WriteTo(BinaryWriter writer)
		{
			writer.Write(Rpid);
			writer.Write((byte)Pr);
			base.WriteTo(writer);
		}
		public override void ReadFrom(EgtsReader reader)
		{
			var rpid = reader.ReadInt16();
			var pr   = reader.ReadByte();

			Rpid = (ushort)rpid;
			Pr   = (ResponseResult)pr;
			base.ReadFrom(reader);
		}
		public IEnumerable<EgtsSrRecordResponse> GetErrors(IServiceFrame requestFrame)
		{
			var responses = Records.SelectMany(sdr => sdr.SubRecords.Select(sr => sr.DataRecord)).OfType<EgtsSrRecordResponse>().ToList();
			return requestFrame.Records
				.Select(serviceLayerRecord => serviceLayerRecord.RecordId)
				.Select(rid => responses.FirstOrDefault(r => r.Crn == rid))
				.Where(response => response != null && response.Rst != ResponseResult.EGTS_PC_OK);
		}
		public static IServiceFrame FromBytes(byte[] sfrd)
		{
			using (var stream = new MemoryStream(sfrd))
			using (var reader = new EgtsReader(stream))
			{
				return reader.ReadResponsePacket();
			}
		}
		public bool AddResponse(ServiceLayerRecord serviceRecord, ResponseResult result)
		{
			var response = new EgtsSrRecordResponse(serviceRecord.ServiceCode)
			{
				Crn = serviceRecord.RecordId,
				Rst = result
			};
			return AddSubRecord(response);
		}
		public bool AddAuthResponse(ResponseResult resultCode)
		{
			var response = new EgtsSrResultCode
			{
				Code = resultCode
			};
			return AddSubRecord(response);
		}
		public static EgtsPtResponse Create(ushort pid, int maxSize = TransportPacket.MaxFrameSize)
		{
			return new EgtsPtResponse(maxSize)
			{
				Rpid = pid
			};
		}
	}
}