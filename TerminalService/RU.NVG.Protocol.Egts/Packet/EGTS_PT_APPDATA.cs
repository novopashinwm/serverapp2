﻿using System.Globalization;
using System.IO;
using System.Linq;
using RU.NVG.Protocol.Egts.Enums;
using RU.NVG.Protocol.Egts.Helpers;
using RU.NVG.Protocol.Egts.Interface;
using RU.NVG.Protocol.Egts.Record.Auth;
using RU.NVG.Protocol.Egts.Record.Teledata;

namespace RU.NVG.Protocol.Egts.Packet
{
	/// <summary> Пакет, содержащий данные протокола Уровня поддержки услуг </summary>
	public class EgtsPtAppdata : BaseFrame
	{
		public override PacketType Type
		{
			get { return PacketType.EgtsPtAppdata; }
		}
		internal EgtsPtAppdata(int maxSize = TransportPacket.MaxFrameSize) : base(maxSize)
		{
		}
		/// <summary> Добавляет в пакет запись местоположения </summary>
		/// <param name="id"> Идентификатор объекта </param>
		/// <param name="posData"> Данные местоположения </param>
		/// <returns></returns>
		public bool Add(int id, EgtsSrPosData posData)
		{
			return AddSubRecord(posData, (uint) id);
		}
		/// <summary> Добавляет в пакет запись местоположения </summary>
		/// <param name="id"> Идентификатор объекта </param>
		/// <param name="exPosData"> Данные местоположения </param>
		/// <returns></returns>
		public bool Add(int id, EgtsSrExtPosData exPosData)
		{
			return AddSubRecord(exPosData, (uint)id);
		}
		/// <summary> Добавляет в пакет запись EGTS_SR_AD_SENSORS_DATA об аналоговых датчиках </summary>
		/// <param name="id"> Идентификатор объекта </param>
		/// <param name="adSensorsData"> Данные об аналоговых датчиках </param>
		/// <returns></returns>
		public bool Add(int id, EgtsSrAdSensorsData adSensorsData)
		{
			return AddSubRecord(adSensorsData, (uint)id);
		}
		/// <summary> Добавляет в пакет запись EGTS_SR_LIQUID_LEVEL_SENSOR о данных уровня жидкости </summary>
		/// <param name="id"> Идентификатор объекта </param>
		/// <param name="liquidLevelSensor"> Данные уровня жидкости </param>
		/// <returns></returns>
		public bool Add(int id, EgtsSrLiquidLevelSensor liquidLevelSensor)
		{
			return AddSubRecord(liquidLevelSensor, (uint)id);
		}
		/// <summary> Добавляет данные авторизации </summary>
		/// <param name="termIdentity"></param>
		/// <returns></returns>
		public bool Add(EgtsSrTermIdentity termIdentity)
		{
			return AddSubRecord(termIdentity);
		}
		public override string ToString()
		{
			return string.Join("\r\n", Records.Select(sdr => string.Format(CultureInfo.CurrentCulture, "{0}", sdr)));
		}
		public static IServiceFrame FromBytes(byte[] sfrd)
		{
			using (var stream = new MemoryStream(sfrd))
			using (var reader = new EgtsReader(stream))
			{
				return reader.ReadAppdataPacket();
			}
		}
		/// <summary> Создание экземпляра </summary>
		/// <returns></returns>
		public static EgtsPtAppdata Create(int maxSize = TransportPacket.MaxFrameSize)
		{
			return new EgtsPtAppdata(maxSize);
		}
	}
}