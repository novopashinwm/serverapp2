﻿using System.IO;

namespace RU.NVG.Protocol.Egts.Interface
{
	public interface IReadable
	{
		void ReadFrom(BinaryReader reader);
	}
}