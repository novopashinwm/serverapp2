﻿using RU.NVG.Protocol.Egts.Enums;
using RU.NVG.Protocol.Egts.Record;

namespace RU.NVG.Protocol.Egts.Interface
{
	public interface IService
	{
		ServiceLayerRecord GetRecord();
		ServiceCode Code { get; }
	}
}