﻿namespace RU.NVG.Protocol.Egts.Interface
{
	public interface IPacket : IWritable
	{
		ushort Pid { get; }
	}
}