﻿using System.IO;

namespace RU.NVG.Protocol.Egts.Interface
{
	public interface IWritable
	{
		void WriteTo(BinaryWriter writer);
	}
}