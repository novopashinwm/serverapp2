﻿using System.Collections.Generic;
using RU.NVG.Protocol.Egts.Enums;
using RU.NVG.Protocol.Egts.Record;

namespace RU.NVG.Protocol.Egts.Interface
{
	/// <summary> Пакет уровня поддержки услуг </summary>
	public interface IServiceFrame : IWritable
	{
		/// <summary> Тип пакета </summary>
		PacketType Type { get; }
		/// <summary> Размер пакета </summary>
		int Size { get; }
		/// <summary> Записи фрейма данных </summary>
		IEnumerable<ServiceLayerRecord> Records { get; }
	}
}