﻿using RU.NVG.Protocol.Egts.Enums;

namespace RU.NVG.Protocol.Egts.Interface
{
	public interface ISubRecord : IWritable
	{
		byte ServiceRecordType { get; }
		ServiceCode Service { get; }
	}
}