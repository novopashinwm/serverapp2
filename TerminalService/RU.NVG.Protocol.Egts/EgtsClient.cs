﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Threading.Tasks;
using FORIS.TSS.Helpers;
using RU.NVG.Protocol.Egts.Enums;
using RU.NVG.Protocol.Egts.Helpers;
using RU.NVG.Protocol.Egts.Packet;
using RU.NVG.Protocol.Egts.Record;
using RU.NVG.Protocol.Egts.Record.Auth;

namespace RU.NVG.Protocol.Egts
{
	public class EgtsClient : IDisposable
	{
		#region IDisposable
		private volatile bool _disposed = false;
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		protected virtual void Dispose(bool disposing)
		{
			if (_disposed)
				return;
			if (disposing)
				DropConnection();
			_disposed = true;
		}
		#endregion IDisposable
		#region Блок параметров read-only
		private readonly TimeSpan _timeOut = TimeSpan.FromSeconds(30);
		private readonly string   _host;
		private readonly int      _port;
		private readonly int?     _tid;
		private readonly string   _imei;
		#endregion Блок параметров read-only

		#region Блок параметров volatile сетевых компонентов
		private volatile TcpClient     _client;
		private volatile NetworkStream _stream;
		#endregion Блок параметров volatile сетевых компонентов

		public bool Connected
		{
			get
			{
				if (_disposed)
					throw new ObjectDisposedException(GetType().FullName);

				return
					(_client?.Connected ?? false) &&
					(_stream?.CanRead   ?? false) &&
					(_stream?.CanWrite  ?? false);
			}
		}
		public EgtsClient(string host, int port, int? tid = null, string imei = null)
		{
			_host = host;
			_port = port;
			_tid  = tid;
			_imei = imei;
		}
		public void Send(EgtsPtAppdata reqFrame)
		{
			var reqPacket = new TransportPacket(reqFrame) { Priority = Priority.Mid };
			ConnectIfNeeded();
			try
			{
				var resPacket = SendAndReadAnswer(reqPacket);
				if (null != resPacket)
				{
					var resRecord = resPacket.ServiceFrame
						.Records
						// У записей пакета выбираем все подзаписи
						.SelectMany(r => r.SubRecords)
						// Выбираем первую подзапись типа EgtsSrRecordResponse
						.FirstOrDefault(r => r.DataRecord is EgtsSrRecordResponse);
					if (null != resRecord)
					{
						var datRecord = (EgtsSrRecordResponse)resRecord.DataRecord;
						if (datRecord.Rst != ResponseResult.EGTS_PC_OK)
							Trace.TraceWarning(GetType().Name + ": Invalid response from '{0}:{1}':{2}", _host, _port, datRecord.Rst);
					}
					else
						Trace.TraceWarning(GetType().Name + ": Invalid response from '{0}:{1}': no EgtsSrRecordResponse", _host, _port);
				}
				else
					Trace.TraceWarning(GetType().Name + ": Data exchange with '{0}:{1}' failed", _host, _port);
			}
			catch (Exception e)
			{
				Trace.TraceWarning(GetType().Name + ": Send to '{0}:{1}' failed: {2}\n{3}", _host, _port, e, e.StackTrace);
			}
		}
		private TransportPacket SendAndReadAnswer(TransportPacket reqPacket)
		{
			var resPacket = default(TransportPacket);
			if (Connected)
			{
				var sendTask = SendTransportPacket(reqPacket);
				var recvTask = sendTask
					.ContinueWith((prevTask) =>
					{
						return RecvTransportPacket().Result;
					}, TaskContinuationOptions.NotOnFaulted);
				try
				{
					sendTask.Wait();
					if (recvTask.Wait(_timeOut))
						resPacket = recvTask.Result;
				}
				catch (AggregateException ae)
				{
					foreach (var ex in ae.InnerExceptions)
						Trace.TraceError(string.Format(GetType().Name + ": {0} error: {1}\nStackTrace:\n{2}",
							MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace));
				}
			}
			return resPacket;
		}
		private async Task SendTransportPacket(TransportPacket transportPacket)
		{
			using (var memoryStream = new MemoryStream())
			{
				using (var egtsWriter = new EgtsWriter(memoryStream))
				{
					egtsWriter.Write(transportPacket);
					memoryStream.Position = 0;
					if (Connected)
						memoryStream.CopyTo(_stream);
					else
						throw new ApplicationException(string.Format("Egts client lost connection to '{0}:{1}'", _host, _port));
					await Task.FromResult(0);
				}
			}
		}
		private async Task<TransportPacket> RecvTransportPacket()
		{
			var transportPacket = default(TransportPacket);

			if (!Connected)
				throw new ApplicationException(string.Format("Egts client lost connection to '{0}:{1}'", _host, _port));

			using (var memoryStream = new MemoryStream())
			{
				var tcpsBuffer = new byte[_client.ReceiveBufferSize];
				var readLength = 0;
				do
				{
					readLength = await _stream.ReadAsync(tcpsBuffer, 0, tcpsBuffer.Length);
					memoryStream.Write(tcpsBuffer, 0, readLength);
				}
				while (readLength == tcpsBuffer.Length);
				memoryStream.Position = 0;
				using (var egtsReader = new EgtsReader(memoryStream))
					transportPacket = egtsReader.ReadPacket();
			}
			return transportPacket;
		}
		private void InitConnection()
		{
			_client = new TcpClient();
			_client.Connect(_host, _port, _timeOut);
			_stream = _client.GetStream();
			_stream.ReadTimeout = (int)_timeOut.TotalMilliseconds;
		}
		private void DropConnection()
		{
			using (var stream = _stream) { _stream = null; }
			using (var client = _client) { _client = null; }
		}
		protected void ConnectIfNeeded()
		{
			// Если соединение утеряно
			if (!Connected)
			{
				// Очищаем внутренние ресурсы
				DropConnection();
				// Создаем внутренние ресурсы
				InitConnection();
				// Проверка состояния подключения
				if (!Connected)
					throw new ArgumentException("Egts client couldn't connect to " + _host + ":" + _port);
				// Отправка аутентификации и анализ ответа
				if (_tid.HasValue)
				{
					var requestFrame = EgtsPtAppdata.Create();
					requestFrame.Add(new EgtsSrTermIdentity
					{
						Tid  = (uint)_tid.Value,
						Imei = _imei,
					});

					var resPacket = SendAndReadAnswer(new TransportPacket(requestFrame));
					var errors = (resPacket?.ServiceFrame as EgtsPtResponse)
						?.GetErrors(requestFrame)
						?.ToArray();
					if (errors?.Any() ?? false)
					{
						var errorMessage = string.Join(";", errors.Select(e => string.Format("{0}-{1}", e.Crn, e.Rst)));
						throw new ArgumentException("Egts client couldn't connect to " + _host + ":" + _port + " - " + errorMessage);
					}
				}
			}
		}
	}
}