﻿namespace RU.NVG.Protocol.Egts.Enums
{
	public enum EgtsDeviceWorkMode
	{
		Passive           = 0,
		Era               = 1,
		Active            = 2,
		EmergencyCall     = 3,
		EmergencyTracking = 4,
		Testing           = 5,
		Autoservice       = 6,
		SoftwareLoading   = 7
	}
}