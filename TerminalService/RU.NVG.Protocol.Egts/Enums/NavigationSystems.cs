﻿using System;

namespace RU.NVG.Protocol.Egts.Enums
{
	[Flags]
	public enum NavigationSystems
	{
		/// 0   = система не определена;
		Unknown = 0,
		/// 1   = ГЛОНАСС;
		Glonass = 1,
		/// 2   = GPS;
		Gps = 2,
		/// 4   = Galileo;
		Galileo = 4,
		/// 8   = Compass;
		Compass = 8,
		/// 16 = Beidou;
		Beidou = 16,
		/// 32 = DORIS;
		DORIS = 32,
		/// 64 = IRNSS;
		IRNSS = 64,
		/// 128 = QZSS.
		QZSS = 128
	}
}