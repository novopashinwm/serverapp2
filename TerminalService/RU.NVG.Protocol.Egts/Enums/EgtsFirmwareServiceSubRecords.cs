﻿namespace RU.NVG.Protocol.Egts.Enums
{
	public enum EgtsFirmwareServiceSubRecords
	{
		/// <summary> Подзапись применяется для осуществления подтверждения записи ППУ из пакета типа EGTS_PT_APPDATA </summary>
		EGTS_SR_RECORD_RESPONSE = 0,
		/// <summary>
		/// Подзапись предназначена для передачи на АС данных, которые разбиваются на части и передаются последовательно. 
		/// Данная подзапись применяется для передачи больших объектов, длина которых не позволяет передать их на АС одним пакетом
		/// </summary>
		EGTS_SR_SERVICE_PART_DATA = 33,
		/// <summary> Подзапись предназначена для передачи на АС данных, которые не разбиваются на части, а передаются одним пакетом </summary>
		EGTS_SR_SERVICE_FULL_DATA = 34
	}
}