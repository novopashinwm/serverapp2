﻿namespace RU.NVG.Protocol.Egts.Enums
{
	/// <summary> Приоритет </summary>
	public enum Priority
	{
		/// <summary> Низкий </summary>
		Low      = 3,
		/// <summary> Средний </summary>
		Mid      = 2,
		/// <summary> Высокий </summary>
		High     = 1,
		/// <summary> Очень высокий </summary>
		VeryHigh = 0
	}
}