﻿namespace RU.NVG.Protocol.Egts.Enums
{
	public enum ResponseResult
	{
		/// <summary> успешно обработано </summary>
		EGTS_PC_OK              = 000,
		/// <summary> в процессе обработки </summary>
		EGTS_PC_IN_PROGRESS     = 001,
		/// <summary> не поддерживаемый протокол </summary>
		EGTS_PC_UNS_PROTOCOL    = 128,
		/// <summary> ошибка декодирования </summary>
		EGTS_PC_DECRYPT_ERROR   = 129,
		/// <summary> обработка запрещена </summary>
		EGTS_PC_PROC_DENIED     = 130,
		/// <summary> неверный формат заголовка </summary>
		EGTS_PC_INC_HEADERFORM  = 131,
		/// <summary> неверный формат данных </summary>
		EGTS_PC_INC_DATAFORM    = 132,
		/// <summary> не поддерживаемый тип </summary>
		EGTS_PC_UNS_TYPE        = 133,
		/// <summary> неверное количество параметров </summary>
		EGTS_PC_NOTEN_PARAMS    = 134,
		/// <summary> попытка повторной обработки </summary>
		EGTS_PC_DBL_PROC        = 135,
		/// <summary> обработка данных от источника запрещена </summary>
		EGTS_PC_PROC_SRC_DENIED = 136,
		/// <summary> ошибка контрольной суммы заголовка </summary>
		EGTS_PC_HEADERCRC_ERROR = 137,
		/// <summary> ошибка контрольной суммы данных </summary>
		EGTS_PC_DATACRC_ERROR   = 138,
		/// <summary> некорректная длина данных </summary>
		EGTS_PC_INVDATALEN      = 139,
		/// <summary> маршрут не найден </summary>
		EGTS_PC_ROUTE_NFOUND    = 140,
		/// <summary> маршрут закрыт </summary>
		EGTS_PC_ROUTE_CLOSED    = 141,
		/// <summary> маршрутизация запрещена </summary>
		EGTS_PC_ROUTE_DENIED    = 142,
		/// <summary> неверный адрес </summary>
		EGTS_PC_INVADDR         = 143,
		/// <summary> превышено количество ретрансляции данных </summary>
		EGTS_PC_TTLEXPIRED      = 144,
		/// <summary> нет подтверждения </summary>
		EGTS_PC_NO_ACK          = 145,
		/// <summary> объект не найден </summary>
		EGTS_PC_OBJ_NFOUND      = 146,
		/// <summary> событие не найдено </summary>
		EGTS_PC_EVNT_NFOUND     = 147,
		/// <summary> сервис не найден </summary>
		EGTS_PC_SRVC_NFOUND     = 148,
		/// <summary> сервис запрещен </summary>
		EGTS_PC_SRVC_DENIED     = 149,
		/// <summary> неизвестный тип сервиса </summary>
		EGTS_PC_SRVC_UNKN       = 150,
		/// <summary> авторизация запрещена </summary>
		EGTS_PC_AUTH_DENIED     = 151,
		/// <summary> объект уже существует </summary>
		EGTS_PC_ALREADY_EXISTS  = 152,
		/// <summary> идентификатор не найден </summary>
		EGTS_PC_ID_NFOUND       = 153,
		/// <summary> неправильная дата и время </summary>
		EGTS_PC_INC_DATETIME    = 154,
		/// <summary> ошибка ввода/вывода </summary>
		EGTS_PC_IO_ERROR        = 155,
		/// <summary> недостаточно ресурсов </summary>
		EGTS_PC_NO_RES_AVAIL    = 156,
		/// <summary> внутренний сбой модуля </summary>
		EGTS_PC_MODULE_FAULT    = 157,
		/// <summary> сбой в работе цепи питания модуля </summary>
		EGTS_PC_MODULE_PWR_FLT  = 158,
		/// <summary> сбой в работе микроконтроллера модуля </summary>
		EGTS_PC_MODULE_PROC_FLT = 159,
		/// <summary> сбой в работе программы модуля </summary>
		EGTS_PC_MODULE_SW_FLT   = 160,
		/// <summary> сбой в работе внутреннего ПО модуля </summary>
		EGTS_PC_MODULE_FW_FLT   = 161,
		/// <summary> сбой в работе блока ввода/вывода модуля </summary>
		EGTS_PC_MODULE_IO_FLT   = 162,
		/// <summary> сбой в работе внутренней памяти модуля </summary>
		EGTS_PC_MODULE_MEM_FLT  = 163,
		/// <summary> тест не пройден </summary>
		EGTS_PC_TEST_FAILED     = 164
	}
}