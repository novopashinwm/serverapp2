﻿namespace RU.NVG.Protocol.Egts.Enums
{
	public enum PacketType
	{
		/// <summary> подтверждение на пакет Транспортного уровня </summary>
		EgtsPtResponse      = 0,
		/// <summary> пакет, содержащий данные протокола Уровня поддержки услуг </summary>
		EgtsPtAppdata       = 1,
		/// <summary> пакет, содержащий данные протокола Уровня поддержки услуг с цифровой подписью </summary>
		EgtsPtSignedAppdata = 2
	}
}