﻿namespace RU.NVG.Protocol.Egts.Enums
{
	public enum EgtsSensor
	{
		/// <summary> Цифровой вход 1 </summary>
		DigitalInput1 = 1,
		/// <summary> Цифровой вход 2 </summary>
		DigitalInput2 = 2,
		/// <summary> Цифровой вход 3 </summary>
		DigitalInput3 = 3,
		/// <summary> Цифровой вход 4 </summary>
		DigitalInput4 = 4,
		/// <summary> Цифровой вход 5 </summary>
		DigitalInput5 = 5,
		/// <summary> Цифровой вход 6 </summary>
		DigitalInput6 = 6,
		/// <summary> Цифровой вход 7 </summary>
		DigitalInput7 = 7,
		/// <summary> Цифровой вход 8 </summary>
		DigitalInput8 = 8,
		/// <summary> Зажигание </summary>
		Ignition = 10,
		/// <summary> Снижение точности в горизонтальной плоскости </summary>
		Hdop = 11,
		/// <summary> Снижение точности в вертикальной плоскости </summary>
		Vdop = 12,
		/// <summary> Снижение точности по местоположению </summary>
		Pdop = 13,
		/// <summary> Аналоговый вход 1 </summary>
		AnalogueInput1 = 14,
		/// <summary> Аналоговый вход 2 </summary>
		AnalogueInput2 = 15,
		/// <summary> Аналоговый вход 3 </summary>
		AnalogueInput3 = 16,
		/// <summary> Аналоговый вход 4 </summary>
		AnalogueInput4 = 17,
		/// <summary> Аналоговый вход 5 </summary>
		AnalogueInput5 = 18,
		/// <summary> Аналоговый вход 6 </summary>
		AnalogueInput6 = 19,
		/// <summary> Аналоговый вход 7 </summary>
		AnalogueInput7 = 20,
		/// <summary> Аналоговый вход 8 </summary>
		AnalogueInput8 = 21,
		/// <summary> Уровень жидкости в литрах </summary>
		LiquidLevelLiters = 22,
		/// <summary> Уровень жидкости в процентах </summary>
		LiquidLevelPercentage = 23,
		/// <summary> Уровень жидкости не тарированный </summary>
		LiquidLevelNoUnit = 24
	}
}