﻿using System.Collections;
using System.IO;
using RU.NVG.Protocol.Egts.Enums;

namespace RU.NVG.Protocol.Egts.Record.Auth
{
	public class EgtsSrServiceInfo : ServiceSubRecordBase
	{
		public class SrvParameters
		{
			private readonly BitArray _arr = new BitArray(8);
			/// <summary>
			/// (Service Attribute) битовый флаг, атрибут сервиса:
			/// 0 = поддерживаемый сервис
			/// 1 = запрашиваемый сервис
			/// </summary>
			public bool Srva
			{
				get { return _arr[7]; }
				set { _arr[7] = value; }
			}
			/// <summary>
			/// (Service Routing Priority) битовое поле, приоритет с точки зрения трансляции на него данных
			/// (в случае масштабирования системы и применения нескольких экземпляров приложений одного типа сервиса) определяется битами 0 и 1
			/// 00 = наивысший
			/// 01 = высокий
			/// 10 = средний
			/// 11 = низкий
			/// </summary>
			public byte Srvrp
			{
				get { return (byte) ((_arr[0] ? 1 : 0) + (_arr[1] ? 1 : 0)*2); }
				set
				{
					_arr[0] = (value & 1) == 1;
					_arr[1] = (value & 2) == 2;
				}
			}
			public byte Value
			{
				get
				{
					var bytes = new byte[1];
					_arr.CopyTo(bytes, 0);
					return bytes[0];
				}
			}
		}
		/// <summary>
		/// тип сервиса, определяет функциональную принадлежность
		/// </summary>
		public ServiceCode St;
		/// <summary>
		/// определяет текущее состояние сервиса
		/// </summary>
		public ServiceState Sst;
		/// <summary>
		/// определяет параметры сервиса
		/// </summary>
		public SrvParameters Srvp;
		public byte[] GetBytes()
		{
			throw new System.NotImplementedException();
		}
		public override byte ServiceRecordType
		{
			get { return (byte)EgtsAuthServiceSubRecords.EGTS_SR_SERVICE_INFO; }
		}
		public override ServiceCode Service
		{
			get
			{
				return ServiceCode.EGTS_AUTH_SERVICE;
			}
		}

		public override void WriteTo(BinaryWriter writer)
		{
			throw new System.NotImplementedException();
		}
	}
}