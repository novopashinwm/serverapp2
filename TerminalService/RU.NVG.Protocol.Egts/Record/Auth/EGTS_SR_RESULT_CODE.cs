﻿using System.IO;
using RU.NVG.Protocol.Egts.Enums;

namespace RU.NVG.Protocol.Egts.Record.Auth
{
	public class EgtsSrResultCode : ServiceSubRecordBase
	{
		internal byte Rcd;
		public ResponseResult Code
		{
			get { return (ResponseResult) Rcd; }
			set { Rcd = (byte) value; }
		}
		public override void WriteTo(BinaryWriter writer)
		{
			writer.Write(Rcd);
		}
		public void ReadFrom(BinaryReader reader)
		{
			Rcd = reader.ReadByte();
		}
		public override byte ServiceRecordType
		{
			get { return (byte) EgtsAuthServiceSubRecords.EGTS_SR_RESULT_CODE; }
		}
		public override ServiceCode Service
		{
			get { return ServiceCode.EGTS_AUTH_SERVICE; }
		}
	}
}