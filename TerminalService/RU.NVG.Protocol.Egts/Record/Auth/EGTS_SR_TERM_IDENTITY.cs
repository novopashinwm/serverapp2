﻿using System.Collections;
using System.IO;
using System.Text;
using RU.NVG.Protocol.Egts.Enums;

namespace RU.NVG.Protocol.Egts.Record.Auth
{
	public class EgtsSrTermIdentity : ServiceSubRecordBase
	{
		public class IdentityFlags
		{
			private BitArray _arr = new BitArray(8);
			/// <summary>
			/// битовый флаг, определяющий наличие поля MSISDN в подзаписи
			/// (если бит равен 1, то поле передаётся, если 0, то не передаётся)
			/// </summary>
			public bool Mne
			{
				get { return _arr[7]; }
				set { _arr[7] = value; }
			}
			/// <summary>
			/// битовый флаг, определяющий наличие поля BS в подзаписи
			/// (если бит равен 1, то поле передаётся, если 0, то не передаётся)
			/// </summary>
			public bool Bse
			{
				get { return _arr[6]; }
				set { _arr[6] = value; }
			}
			/// <summary>
			/// битовый флаг определяет наличие поля NID в подзаписи
			/// (если бит равен 1, то поле передаётся, если 0, то не передаётся)
			/// </summary>
			public bool Nide
			{
				get { return _arr[5]; }
				set { _arr[5] = value; }
			}
			/// <summary>
			/// битовый флаг предназначен для определения алгоритма использования Сервисов
			/// (если бит равен 1, то используется «простой» алгоритм, если 0, то алгоритм «запросов» на использование сервисов)
			/// </summary>
			public bool Ssra
			{
				get { return _arr[4]; }
				set { _arr[4] = value; }
			}
			/// <summary>
			/// битовый флаг, который определяет наличие поля LNGC в подзаписи
			/// (если бит равен 1, то поле передаётся, если 0, то не передаётся)
			/// </summary>
			public bool Lngce
			{
				get { return _arr[3]; }
				set { _arr[3] = value; }
			}
			/// <summary>
			/// битовый флаг, который определяет наличие поля IMSI в подзаписи
			/// (если бит равен 1, то поле передаётся, если 0, то не передаётся)
			/// </summary>
			public bool Imsie
			{
				get { return _arr[2]; }
				set { _arr[2] = value; }
			}
			/// <summary>
			/// битовый флаг,  который определяет наличие поля IMEI в подзаписи
			/// (если бит равен 1, то поле передаётся, если 0, то не передаётся)
			/// </summary>
			public bool Imeie
			{
				get { return _arr[1]; }
				set { _arr[1] = value; }
			}
			/// <summary>
			/// битовый флаг, который определяет наличие поля HDID в подзаписи
			/// (если бит равен 1, то поле передаётся, если 0, то не передаётся)
			/// </summary>
			public bool Hdide
			{
				get { return _arr[0]; }
				set { _arr[0] = value; }
			}
			/// <summary>
			/// Значение флагов.
			/// </summary>
			public byte Value
			{
				get
				{
					var bytes = new byte[1];
					_arr.CopyTo(bytes, 0);
					return bytes[0];
				}
				set { _arr = new BitArray(new[] {value}); }
			}
		}
		public class NidFlags
		{
			private BitArray _arr = new BitArray(24);
			/// <summary>
			/// код страны
			/// </summary>
			public ushort Mnc
			{
				set
				{
					_arr[0] = (value & 0x1)     == 0x1;
					_arr[1] = (value & 0x2)     == 0x2;
					_arr[2] = (value & 0x4)     == 0x4;
					_arr[3] = (value & 0x8)     == 0x8;
					_arr[4] = (value & 0x10)    == 0x10;
					_arr[5] = (value & 0x20)    == 0x20;
					_arr[6] = (value & 0x40)    == 0x40;
					_arr[7] = (value & 0x80)    == 0x80;
					_arr[8] = (value & 0x100)   == 0x100;
					_arr[9] = (value & 0x200)   == 0x200;
				}
			}
			/// <summary>
			/// код мобильной сети в пределах страны
			/// </summary>
			public ushort Mcc
			{
				set
				{
					_arr[10] = (value & 0x1)    == 0x1;
					_arr[11] = (value & 0x2)    == 0x2;
					_arr[12] = (value & 0x4)    == 0x4;
					_arr[13] = (value & 0x8)    == 0x8;
					_arr[14] = (value & 0x10)   == 0x10;
					_arr[15] = (value & 0x20)   == 0x20;
					_arr[16] = (value & 0x40)   == 0x40;
					_arr[17] = (value & 0x80)   == 0x80;
					_arr[18] = (value & 0x100)  == 0x100;
					_arr[19] = (value & 0x200)  == 0x200;
				}
			}
			public byte[] Value
			{
				get
				{
					var bytes = new byte[3];
					_arr.CopyTo(bytes, 0);
					return bytes;
				}
				set
				{
					_arr = new BitArray(value);
				}
			}
		}
		public uint Tid
		{
			get { return _tid; }
			set { _tid = value; }
		}
		public string Imei
		{
			get { return _flags.Imeie ? _imei : null; }
			set
			{
				_flags.Imeie = !string.IsNullOrEmpty(value);
				_imei = _flags.Imeie ? value : string.Empty;
			}
		}
		public short? BufferSize
		{
			get
			{
				return _flags.Bse ? (short?) _bs : null;
			}
			set
			{
				_flags.Bse = value != null;
				_bs = (ushort)(value ?? 0);
			}
		}
		/// <summary>
		/// уникальный идентификатор, назначаемый при программировании АС.
		/// Наличие значения 0 в данном поле означает, что АС не прошла процедуру конфигурирования или прошла её не полностью.
		/// Данный идентификатор назначается оператором системы «ЭРА-ГЛОНАСС» и однозначно определяет набор учетных данных АС.
		/// TID назначается при инсталляции АС как дополнительного оборудования и передаче оператору учетных данных АТ (IMSI, IMEI, serial_id).
		/// В случае использования АС в качестве штатного устройства, TID сообщается оператору автопроизводителем вместе с учетными данными (VIN, IMSI, IMEI)
		/// </summary>
		uint _tid;
		/// <summary>
		/// идентификатор сети оператора, в которой зарегистрирована АС.
		/// Используются 20 младших бит. Представляет пару кодов MCC-MNC
		/// </summary>
		readonly IdentityFlags _flags = new IdentityFlags
		{
			Ssra = true
		};
		/// <summary>
		/// идентификатор «домашней» ТП
		/// (подробная учётная информация о терминале хранится на данной ТП)
		/// </summary>
		ushort _hdid;
		/// <summary>
		/// идентификатор мобильного устройства (модема).
		/// При невозможности определения данного параметра, АС должна заполнять данное поле значением 0 во всех 15-ти символах
		/// </summary>
		string _imei;
		/// <summary>
		/// идентификатор мобильного абонента.
		/// При невозможности определения данного параметра, устройство должно заполнять данное поле значением 0 во всех 16-ти символах
		/// </summary>
		string _imsi;
		/// <summary>
		/// код языка, предпочтительного к использованию на стороне АСв соответствии с iso639-2,
		/// например, “rus” – русский
		/// </summary>
		string _lngc;
		/// <summary>
		/// идентификатор сети оператора, в которой зарегистрирована АС.
		/// Используются 20 младших бит.
		/// Представляет пару кодов MCC-MNC .Структура поля NID представлена в Таблице 19
		/// </summary>
		readonly NidFlags _nid = new NidFlags();
		/// <summary>
		/// максимальный размер буфера приёма АС в байтах.
		/// Размер каждого пакета информации, передаваемого на АС, не должен превышать данного значения.
		/// Значение поля BS может принимать различные значения (1024, 2048, 4096),
		/// и зависит от реализации аппаратной и программной частей конкретной АС
		/// </summary>
		ushort _bs;
		/// <summary>
		/// телефонный номер мобильного абонента.
		/// При невозможности определения данного параметра, устройство должно заполнять данное поле значением 0 во всех 15-ти символах
		/// (формат описан в ITU-T E.164 The international public telecommunication numbering plan (Международный план нумерации общедоступных телекоммуникаций)).
		/// </summary>
		string _msisdn;
		public override void WriteTo(BinaryWriter writer)
		{
			writer.Write(_tid);
			writer.Write(_flags.Value);
			if (_flags.Hdide)
				writer.Write(_hdid);
			if (_flags.Imeie)
			{
				var imeiBytes = new byte[15];
				var imei = _imei.Substring(0, 15);
				Encoding.ASCII.GetBytes(imei, 0, imei.Length, imeiBytes, 0);
				writer.Write(imeiBytes);
			}
			if (_flags.Imsie)
			{
				var imsieBytes = new byte[16];
				var imsie = _imsi.Substring(0, 16);
				Encoding.ASCII.GetBytes(imsie, 0, imsie.Length, imsieBytes, 0);
				writer.Write(imsieBytes);
			}
			if (_flags.Lngce)
			{
				var lngcBytes = new byte[3];
				var lngc = _lngc.Substring(0, 3);
				Encoding.ASCII.GetBytes(lngc, 0, lngc.Length, lngcBytes, 0);
				writer.Write(lngcBytes);
			}
			if (_flags.Nide)
				writer.Write(_nid.Value);
			if (_flags.Bse)
				writer.Write(_bs);
			if (_flags.Mne)
			{
				var msisdnBytes = new byte[15];
				var msisdn = _msisdn.Substring(0, 15);
				Encoding.ASCII.GetBytes(msisdn, 0, msisdn.Length, msisdnBytes, 0);
				writer.Write(msisdnBytes);
			}
		}
		public void ReadFrom(BinaryReader reader)
		{
			var tid = (uint)reader.ReadInt32();
			var flagsByte = reader.ReadByte();
			_flags.Value = flagsByte;
			ushort hdid = 0;
			if (_flags.Hdide)
			{
				hdid = (ushort) reader.ReadInt16();
			}

			var imei = string.Empty;
			if (_flags.Imeie)
			{
				var imeiBytes = reader.ReadBytes(15);
				imei = Encoding.ASCII.GetString(imeiBytes).TrimEnd('\0');
			}

			var imsi = string.Empty;
			if (_flags.Imsie)
			{
				var imsieBytes = reader.ReadBytes(16);
				imsi = Encoding.ASCII.GetString(imsieBytes);
			}

			var lngc = string.Empty;
			if (_flags.Lngce)
			{
				var lngcBytes = reader.ReadBytes(3);
				lngc = Encoding.ASCII.GetString(lngcBytes);
			}

			byte[] nid = null;
			if (_flags.Nide)
				nid = reader.ReadBytes(3);

			var bs = (ushort) 0;
			if (_flags.Bse)
				bs = (ushort) reader.ReadInt16();

			var msisdn = string.Empty;
			if (_flags.Mne)
			{
				var msisdnBytes = reader.ReadBytes(15);
				msisdn = Encoding.ASCII.GetString(msisdnBytes);
			}

			_tid = tid;
			if(_flags.Hdide)
				_hdid = hdid;
			if(_flags.Imeie)
				_imei = imei;
			if(_flags.Imsie)
				_imsi = imsi;
			if(_flags.Lngce)
				_lngc = lngc;
			if(_flags.Nide)
				_nid.Value = nid;
			if(_flags.Bse)
				_bs = bs;
			if(_flags.Mne)
				_msisdn = msisdn;
		}
		public byte[] GetBytes()
		{
			using (var stream = new MemoryStream())
			using (var writer = new BinaryWriter(stream))
			{
				WriteTo(writer);
				return stream.ToArray();
			}
		}
		public override byte ServiceRecordType
		{
			get { return (byte)EgtsAuthServiceSubRecords.EGTS_SR_TERM_IDENTITY; }
		}
		public override ServiceCode Service
		{
			get
			{
				return ServiceCode.EGTS_AUTH_SERVICE;
			}
		}
	}
}