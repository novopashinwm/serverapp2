﻿using System.Collections;
using System.IO;
using System.Text;
using RU.NVG.Protocol.Egts.Enums;

namespace RU.NVG.Protocol.Egts.Record.Auth
{
	public class EgtsSrAuthParams : ServiceSubRecordBase
	{
		public const byte Delimeter = 0;

		public class AuthParamsFlags
		{
			private readonly BitArray _arr = new BitArray(8);

			/// <summary>
			/// битовый флаг, определяет наличие поля EXP и следующего за ним разделителя D
			/// (если 1, то поля присутствуют)
			/// </summary>
			public bool Exe
			{
				get { return _arr[6]; }
				set { _arr[6] = value; }
			}
			/// <summary>
			/// битовый флаг, определяет наличие поля SS и следующего за ним разделителя D
			/// (если 1, то поля присутствуют)
			/// </summary>
			public bool Sse
			{
				get { return _arr[5]; }
				set { _arr[5] = value; }
			}
			/// <summary>
			/// битовый флаг, определяет наличие поля MSZ
			/// (если 1, то поле присутствует)
			/// </summary>
			public bool Mse
			{
				get { return _arr[4]; }
				set { _arr[4] = value; }
			}
			/// <summary>
			/// битовый флаг, определяет наличие поля ISL
			/// (если 1, то поле присутствует)
			/// </summary>
			public bool Isle
			{
				get { return _arr[3]; }
				set { _arr[3] = value; }
			}
			/// <summary>
			/// битовый флаг, определяет наличие полей PKL и PBK
			/// (если 1, то поля присутствуют)
			/// </summary>
			public bool Pke
			{
				get { return _arr[2]; }
				set { _arr[2] = value; }
			}
			/// <summary>
			/// битовое поле, определяющее требуемый алгоритм шифрования пакетов.
			/// Если данное поле содержит значение 0 0, то шифрование не применяется,
			/// и подзапись EGTS_SR_AUTH_PARAMS содержит только один байт, иначе,
			/// в зависимости от типа алгоритма, наличие дополнительных параметров определяется остальными битами поля FLG
			/// </summary>
			public byte Ena
			{
				get { return (byte) ((_arr[1] ? 1 : 0)*2 + (_arr[0] ? 1 : 0)); }
				set
				{
					_arr[0] = (value & 1) == 1;
					_arr[1] = (value & 2) == 2;
				}
			}
			public byte Value
			{
				get
				{
					var bytes = new byte[1];
					_arr.CopyTo(bytes, 0);
					return bytes[0];
				}
			}
		}

		public readonly AuthParamsFlags Flags = new AuthParamsFlags();

		/// <summary> Длина публичного ключа в байтах </summary>
		public ushort Pkl;
		/// <summary> Данные публичного ключа </summary>
		public byte[] Pbk;
		/// <summary> Результирующая длина идентификационных данных </summary>
		public ushort Isl;
		/// <summary> Параметр, применяемый в процессе шифрования </summary>
		public ushort Msz;
		/// <summary> Специальная серверная последовательность байт, применяемая в процессе шифрования </summary>
		public string Ss;
		/// <summary> Специальная последовательность, используемая в процессе шифрования </summary>
		public string Exp;
		public override void WriteTo(BinaryWriter writer)
		{
			writer.Write(Flags.Value);
			if (Flags.Pke)
			{
				writer.Write(Pkl);
				writer.Write(Pbk);
			}
			if (Flags.Isle)
				writer.Write(Isl);
			if (Flags.Mse)
				writer.Write(Msz);
			if (Flags.Sse)
			{
				var ssBytes = Encoding.ASCII.GetBytes(Ss);
				writer.Write(ssBytes);
				writer.Write(Delimeter);
			}
			if (Flags.Exe)
			{
				var expBytes = Encoding.ASCII.GetBytes(Exp);
				writer.Write(expBytes);
				writer.Write(Delimeter);
			}
		}
		public override byte ServiceRecordType
		{
			get { return (byte) EgtsAuthServiceSubRecords.EGTS_SR_AUTH_PARAMS; }
		}
		public override ServiceCode Service
		{
			get
			{
				return ServiceCode.EGTS_AUTH_SERVICE;
			}
		}
	}
}