﻿using System;
using System.IO;
using RU.NVG.Protocol.Egts.Enums;

namespace RU.NVG.Protocol.Egts.Record.Auth
{
	public class EgtsSrVehicleData : ServiceSubRecordBase
	{
		/// <summary>
		/// идентификационный номер транспортного средства (структура описана в ISO 3779);
		/// </summary>
		public string Vin;
		public byte[] GetBytes()
		{
			throw new NotImplementedException();
		}
		public override byte ServiceRecordType
		{
			get { return (byte)EgtsAuthServiceSubRecords.EGTS_SR_VEHICLE_DATA; }
		}
		public override ServiceCode Service
		{
			get
			{
				return ServiceCode.EGTS_AUTH_SERVICE;
			}
		}
		public override void WriteTo(BinaryWriter writer)
		{
			throw new NotImplementedException();
		}
	}
}