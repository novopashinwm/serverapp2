﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common.Helpers;
using RU.NVG.Protocol.Egts.Enums;
using RU.NVG.Protocol.Egts.Helpers;
using RU.NVG.Protocol.Egts.Interface;

namespace RU.NVG.Protocol.Egts.Record
{
	public class ServiceLayerRecord : IWritable
	{
		internal class Flags
		{
			private BitArray _arr = new BitArray(8);

			/// <summary>
			/// (Object ID  Field Exists),  битовое поле,  определяющее наличие в данном пакете поля OID:
			/// 1 = поле OID присутствует
			/// 0 = поле OID отсутствует
			/// </summary>
			public bool Obfe
			{
				get { return _arr[0]; }
				set { _arr[0] = value; }
			}

			/// <summary>
			/// EVFE  –  (Event ID Field  Exists),  битовое поле,  определяющее наличие в данном пакете поля EVID:
			/// 1 = поле EVID присутствует
			/// 0 = поле EVID отсутствует
			/// </summary>
			public bool Evfe
			{
				get { return _arr[1]; }
				set { _arr[1] = value; }
			}

			/// <summary>
			/// TMFE  –  (Time Field Exists),  битовое поле,  определяющее наличие в данном пакете поля TM:
			/// 1 = поле TM присутствует
			/// 0 = поле TM отсутствует
			/// </summary>
			public bool Tmfe
			{
				get { return _arr[2]; }
				set { _arr[2] = value; }
			}

			/// <summary>
			/// (Record Processing Priority),  битовое поле, определяющее приоритет обработки данной записи Сервисом:
			/// 00 – наивысший
			/// 01 – высокий
			/// 10 – средний
			/// 11 – низкий
			/// </summary>
			public Priority Rpp
			{
				get { return (Priority)((_arr[3] ? 1 : 0) + (_arr[4] ? 2 : 0)); }
				set
				{
					var val = (byte) value;
					_arr[3] = (val & 1) == 1;
					_arr[4] = (val & 2) == 2;
				}
			}

			/// <summary>
			/// (Group),  битовый флаг, определяющий принадлежность передаваемых данных определённой группе, идентификатор которой указан в поле OID:
			/// 1 = данные предназначены для группы;
			/// 0 = принадлежность группе отсутствует
			/// </summary>
			public bool Grp
			{
				get { return _arr[5]; }
				set { _arr[5] = value; }
			}

			/// <summary>
			/// (Recipient Service On Device),  битовый флаг, определяющий расположение Сервиса-получателя:
			/// 1 = Сервис-получатель расположен на стороне АС;
			/// 0 = Сервис-получатель расположен на ТП.
			/// </summary>
			public bool Rsod
			{
				get { return _arr[6]; }
				set { _arr[6] = value; }
			}

			/// <summary>
			/// (Source Service On Device),  битовый флаг, определяющий расположение Сервиса-отправителя:
			/// 1 = Сервис-отправитель расположен на стороне АС;
			/// 0 = Сервис- отправитель расположен на ТП.
			/// </summary>
			public bool Ssod
			{
				get { return _arr[7]; }
				set { _arr[7] = value; }
			}

			public byte Value
			{
				get
				{
					var bytes = new byte[1];
					_arr.CopyTo(bytes, 0);
					return bytes[0];
				}
				set { _arr = new BitArray(new[] {value}); }
			}
		}

		/// <summary>
		/// Идентификатор объекта.
		/// </summary>
		public uint? ObjectId
		{
			get { return Rfl.Obfe ? (uint?)Oid : null; }
		}

		/// <summary>
		/// номер записи. Значения в данном поле изменяются по правилам циклического счётчика в диапазоне от 0 до 65535,
		/// т.е. при достижении значения 65535, следующее значение должно быть 0.
		/// Значение из данного поля используется для подтверждения записи
		/// </summary>
		public ushort RecordId
		{
			get { return _rid; }
			set { _rid = value; }
		}

		/// <summary>
		/// Приоритет обработки.
		/// </summary>
		public Priority Priority
		{
			get { return Rfl.Rpp; }
			set { Rfl.Rpp = value; }
		}

		/// <summary>
		/// Rl + Rn + Rfl + Oid? + Evid? + Tm? + Sst + Rst + Srt + Srl + Srd
		/// </summary>
		public int Size
		{
			get { return 2 + 2 + 1 + (Rfl.Obfe ? 4 : 0) + (Rfl.Evfe ? 4 : 0) + (Rfl.Tmfe ? 4 : 0) + 1 + 1 + Rl; }
		}

		/// <summary>
		/// Код сервиса.
		/// </summary>
		public ServiceCode ServiceCode
		{
			get { return Sst; }
			set
			{
				Sst = value;
				Rst = value;
			}
		}

		/// <summary>
		/// Подзаписи, которые содержит запись сервисного уровня.
		/// </summary>
		public List<ISubRecord> GetRecords()
		{
			return SubRecords.Select(r => r.DataRecord).ToList();
		}

		/// <summary>
		/// параметр определяет размер данных из поля RD
		/// </summary>
		internal ushort Rl
		{
			get { return (ushort) SubRecords.Sum(r => r.Size); }
		}

		/// <summary>
		/// содержит битовые флаги, определяющие наличие в данном пакете полей OID, EVID и TM, характеризующих содержащиеся в записи данные
		/// </summary>
		internal Flags Rfl = new Flags();

		/// <summary>
		/// идентификатор объекта, создавшего данную запись, или для которого данная запись предназначена (уникальный идентификатор АС),
		/// либо идентификатор группы (при GRP=1).
		/// При передаче от АС в одном пакете Транспортного Уровня  нескольких записей подряд для разных сервисов,
		/// но от одного и того же объекта, поле OID может присутствовать только в первой записи, а в последующих записях может быть опущено
		/// </summary>
		internal uint Oid;

		/// <summary>
		/// уникальный идентификатор события.
		/// Поле EVID задаёт некий глобальный идентификатор события и применяется,
		/// когда необходимо логически связать с одним единственным событием набор нескольких информационных сущностей,
		/// причём сами сущности могут быть разнесены как по разным информационным пакетам, так и по времени.
		/// При этом прикладное ПО имеет возможность объединить все эти сущности воедино в момент представления пользователю информации о событии.
		/// Например, если с нажатием тревожной кнопки связывается серия фотоснимков, поле EVID должно указываться в каждой сервисной записи,
		/// связанной с этим событием на протяжении передачи всех сущностей, связанных с данным событием,
		/// как бы долго не длилась передача всего пула информации
		/// </summary>
		internal uint Evid;

		/// <summary>
		/// время формирования записи на стороне Отправителя (секунды с 00:00:00 01.01.2010 UTC).
		/// Если в  одном Пакете Транспортного Уровня передаются несколько записей, относящихся к одному объекту и моменту времени,
		/// то поле метки времени TM может передаваться только в составе первой записи
		/// </summary>
		internal uint Tm;

		/// <summary>
		/// идентификатор тип Сервиса-отправителя, создавшего данную запись.
		/// Например, Сервис, обрабатывающий навигационные данные на стороне АС, Сервис  команд на стороне ТП  и т.д
		/// </summary>
		internal ServiceCode Sst;

		/// <summary>
		/// идентификатор тип Сервиса-получателя данной записи.
		/// Например, Сервис, обрабатывающий навигационные данные на стороне ТП, Сервис обработки команд на стороне АС и т.д
		/// </summary>
		internal ServiceCode Rst;

		/// <summary>
		/// Список подзаписей сервиса.
		/// </summary>
		internal List<ServiceLayerSubRecord> SubRecords = new List<ServiceLayerSubRecord>();

		public void WriteTo(BinaryWriter writer)
		{
			writer.Write(Rl);
			writer.Write(RecordId);
			writer.Write(Rfl.Value);
			if(Rfl.Obfe)
				writer.Write(Oid);
			if(Rfl.Evfe)
				writer.Write(Evid);
			if(Rfl.Tmfe)
				writer.Write(Tm);
			writer.Write((byte) Sst);
			writer.Write((byte) Rst);
			foreach (var subRecord in SubRecords)
			{
				subRecord.WriteTo(writer);
			}
		}

		public void ReadFrom(BinaryReader reader)
		{
			var rl = (ushort)reader.ReadInt16();
			var rn = (ushort)reader.ReadInt16();
			var rflByte = reader.ReadByte();
			var rfl = new Flags { Value = rflByte };
			uint oid = 0;
			if(rfl.Obfe)
				oid = (uint) reader.ReadInt32();
			uint evid = 0;
			if(rfl.Evfe)
				evid = (uint) reader.ReadInt32();
			uint tm = 0;
			if(rfl.Tmfe)
				tm = (uint) reader.ReadInt32();
			var sst = reader.ReadByte();
			var rst = reader.ReadByte();
			var rd = reader.ReadBytes(rl);

			RecordId = rn;
			Rfl.Value = rfl.Value;
			if (Rfl.Obfe)
				Oid = oid;
			if (Rfl.Evfe)
				Evid = evid;
			if (Rfl.Tmfe)
				Tm = tm;
			Sst = (ServiceCode)sst;
			Rst = (ServiceCode)rst;

			using (var stream = new MemoryStream(rd))
			using (var egtsReader = new EgtsReader(stream))
			{
				var endOfRecords = false;
				do
				{
					try
					{
						var serviceLayerSubRecord = egtsReader.ReadServiceLayerSubRecord(this);
						endOfRecords = serviceLayerSubRecord == null;
						if (serviceLayerSubRecord != null)
							SubRecords.Add(serviceLayerSubRecord);
					}
					catch (NotImplementedException ex)
					{
						var data = reader.BaseStream as MemoryStream;
						Trace.TraceWarning("{0} {1}: {2}", this, ex, StringHelper.SmartFormatBytes(data != null ? data.ToArray() : rd));
					}
				}
				while (!endOfRecords);
			}
		}

		internal void Add(ServiceLayerSubRecord record)
		{
			SubRecords.Add(record);
		}

		public void RemoveAt(int index)
		{
			SubRecords.RemoveAt(index);
		}

		private ushort _rid;

		private static int _lastRid;

		private static int NextRid()
		{
			return ++_lastRid == 65535 ? 0 : _lastRid;
		}

		public static ServiceLayerRecord Create(uint? oid, ServiceCode sc)
		{
			var record = new ServiceLayerRecord
				{
					RecordId = (ushort)NextRid(),
					Sst = sc,
					Rst = sc,
					Tm = (uint)(TimeHelper.GetSecondsFromBase(DateTime.UtcNow) - DataHelper.Diff2010FromBase),
					Oid = oid.HasValue ? oid.Value : default(uint),
					Rfl = new Flags
					{
						Tmfe = true,
						Obfe = oid.HasValue,
					},
					Priority = Priority.Mid
				};
			return record;
		}
	}
}