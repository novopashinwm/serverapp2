﻿using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using RU.NVG.Protocol.Egts.Enums;
using RU.NVG.Protocol.Egts.Helpers;

namespace RU.NVG.Protocol.Egts.Record.Teledata
{
	/// <summary>
	/// Формат подзаписи EGTS_SR_POS_DATA
	/// </summary>
	public class EgtsSrPosData : ServiceSubRecordBase
	{
		private BitArray _dinArray = new BitArray(8);

		/// <summary>
		/// определяет дополнительные параметры навигационной посылки
		/// </summary>
		internal class Flags
		{
			private BitArray _arr = new BitArray(8);

			/// <summary>
			/// битовый флаг определяет наличие поля ALT в подзаписи
			/// </summary>
			public bool Alte
			{
				get { return _arr[7]; }
				set { _arr[7] = value; }
			}

			/// <summary>
			/// битовый флаг определяет полушарие долготы
			/// 0 - восточная долгота: 1 - западная долгота;
			/// </summary>
			public bool Lohs
			{
				get { return _arr[6]; }
				set { _arr[6] = value; }
			}

			/// <summary>
			/// битовый флаг определяет полушарие широты
			/// 0 - северная широта; 1 - южная широта;
			/// </summary>
			public bool Lahs
			{
				get { return _arr[5]; }
				set { _arr[5] = value; }
			}

			/// <summary>
			/// битовый флаг, признак движения
			/// </summary>
			public bool Mv
			{
				get { return _arr[4]; }
				set { _arr[4] = value; }
			}

			/// <summary>
			/// битовый флаг, признак отправки данных из памяти ("черный ящик")
			/// </summary>
			public bool Bb
			{
				get { return _arr[3]; }
				set { _arr[3] = value; }
			}

			/// <summary>
			/// битовое поле, тип используемой системы
			/// 0 - система координат WGS-84; 1 - государственная геоцентрическая система координат (ПЗ-90.02);
			/// </summary>
			public bool Cs
			{
				get { return _arr[2]; }
				set { _arr[2] = value; }
			}

			/// <summary>
			/// битовое поле, тип определения координат
			/// 0 - 2D fix; 1 - 3D fix;
			/// </summary>
			public bool Fix
			{
				get { return _arr[1]; }
				set { _arr[1] = value; }
			}

			/// <summary>
			/// битовый флаг, признак "валидности" координатных данных
			/// 1 - данные "валидны"; 0 - "невалидные" данные;
			/// </summary>
			public bool Vld
			{
				get { return _arr[0]; }
				set { _arr[0] = value; }
			}

			/// <summary>
			/// Преобразованное значение.
			/// </summary>
			public byte Value
			{
				get
				{
					var bytes = new byte[1];
					_arr.CopyTo(bytes, 0);
					return bytes[0];
				}
				set
				{
					var bytes = new byte[1];
					bytes[0] = value;
					_arr = new BitArray(bytes);
				}
			}
		}

		/// <summary>
		/// контейнер содержит скорость
		/// </summary>
		internal class SpdContainer
		{
			private BitArray _arr = new BitArray(8);

			/// <summary>
			/// (Direction the Highest bit) старший бит (8) параметра DIR
			/// </summary>
			public bool Dirh
			{
				get { return _arr[7]; }
				set { _arr[7] = value; }
			}

			/// <summary>
			/// (Altitude Sign) битовый флаг, определяет высоту относительно уровня моря и имеет смысл только при установленном флаге ALTE:
			/// 0 - точка выше уровня моря; 1 - ниже уровня моря;
			/// </summary>
			public bool Alts
			{
				get { return _arr[6]; }
				set { _arr[6] = value; }
			}

			/// <summary>
			/// старшие биты
			/// скорость в км/ч с дискретностью 0,1 км/ч (используется 14 младших бит);
			/// </summary>
			public byte Spd
			{
				get
				{
					var res = new byte[1];
					_arr.CopyTo(res, 0);
					return (byte) (res[0] & 0x3F);
				}
				set
				{
					if(value > 0x3F)
						throw new InvalidCastException("value > 63: " + value);

					_arr[0] = (value & 0x1) > 0;
					_arr[1] = (value & 0x2) > 0;
					_arr[2] = (value & 0x4) > 0;
					_arr[3] = (value & 0x8) > 0;
					_arr[4] = (value & 0x10) > 0;
					_arr[5] = (value & 0x20) > 0;
				}
			}

			/// <summary>
			/// Преобразованное значение.
			/// </summary>
			public byte Value
			{
				get
				{
					var bytes = new byte[1];
					_arr.CopyTo(bytes, 0);
					return bytes[0];
				}
				set
				{
					_arr = new BitArray(new [] {value});
				}
			}
		}

		/// <summary>
		/// время навигации (количество секунд с 00:00:00 01.01.2010 UTC);
		/// </summary>
		public DateTime NavigationTime
		{
			get { return TimeHelper.GetDateTimeUTCFromBase(DataHelper.Year2010, (int) Ntm); }
			set { Ntm = (uint)TimeHelper.GetSecondsFromBase(value, DataHelper.Year2010); }
		}

		/// <summary>
		/// широта
		/// </summary>
		public double Latitude
		{
			get
			{
				return ((double)Lat / 0xFFFFFFFF) * 90 * (Flg.Lahs ? -1 : 1);
			}
			set
			{
				Flg.Lahs = value < 0;
				Lat = (uint) (Math.Abs(value)*0xFFFFFFFF/90);
			}
		}

		/// <summary>
		/// долгота
		/// </summary>
		public double Longitude
		{
			get
			{
				return ((double)Long / 0xFFFFFFFF) * 180 * (Flg.Lohs ? -1 : 1);
			}
			set
			{
				Flg.Lohs = value < 0;
				Long = (uint)(Math.Abs(value) * 0xFFFFFFFF / 180);
			}
		}

		/// <summary>
		/// Скорость. (км/ч с дискретностью 0.1)
		/// </summary>
		public double Speed
		{
			get
			{
				return ((Spdc.Spd << 8) + Spd) / 10.0;
			}
			set
			{
				if(value > 0x3FFF)
					throw new InvalidCastException("value > 16383: " + value);

				var speed = (int)Math.Round(value * 10.0);
				Spd = (byte)(speed & 0x00FF);
				Spdc.Spd = (byte)(speed >> 8);
			}
		}

		/// <summary>
		/// Одометр.
		/// </summary>
		public double Odometer
		{
			get
			{
				return ((Odm[2] << 16) + (Odm[1] << 8) + Odm[0]) / 10.0;
			}
			set
			{
				var odometer = (int)Math.Round(value * 10.0);
				Odm[2] = (byte)((odometer & 0xFF0000) >> 16);
				Odm[1] = (byte)((odometer & 0x00FF00) >> 8);
				Odm[0] = (byte)((odometer & 0x0000FF));
			}
		}

		/// <summary>
		/// Высота
		/// </summary>
		public int? Altitude
		{
			get { return Flg.Alte ? (Spdc.Alts ? -1 : 1) * ((Alt[2] << 16) + (Alt[1] << 8) + Alt[0]) : (int?) null; }
			set
			{
				Flg.Alte = value.HasValue;
				if (!value.HasValue) return;

				Spdc.Alts = value < 0;

				Alt[2] = (byte)((value & 0xFF0000) >> 16);
				Alt[1] = (byte)((value & 0x00FF00) >> 8);
				Alt[0] = (byte)((value & 0x0000FF));
			}
		}

		public bool Move
		{
			get { return Flg.Mv; }
		}

		/// <summary>
		/// Курс.
		/// </summary>
		public int Course
		{
			get { return Dir + (((Spdc.Dirh ? 1 : 0) << 8)); }
			set
			{
				Dir = (byte)(value & 0xFF);
				Spdc.Dirh = (value & 0x100) == 0x100;
			}
		}

		/// <summary>
		/// Признак валидности позиции.
		/// </summary>
		public bool Valid
		{
			get { return Flg.Vld; }
			set { Flg.Vld = value; }
		}

		/// <summary>
		/// Показания цифрового входа 1
		/// </summary>
		public bool DigitalInput1
		{
			get { return _dinArray[0]; }
			set { _dinArray[0] = value; }
		}

		/// <summary>
		/// Показания цифрового входа 2
		/// </summary>
		public bool DigitalInput2
		{
			get { return _dinArray[1]; }
			set { _dinArray[1] = value; }
		}

		/// <summary>
		/// Показания цифрового входа 3
		/// </summary>
		public bool DigitalInput3
		{
			get { return _dinArray[2]; }
			set { _dinArray[2] = value; }
		}

		/// <summary>
		/// Показания цифрового входа 4
		/// </summary>
		public bool DigitalInput4
		{
			get { return _dinArray[3]; }
			set { _dinArray[3] = value; }
		}

		/// <summary>
		/// Показания цифрового входа 5
		/// </summary>
		public bool DigitalInput5
		{
			get { return _dinArray[4]; }
			set { _dinArray[4] = value; }
		}

		/// <summary>
		/// Показания цифрового входа 6
		/// </summary>
		public bool DigitalInput6
		{
			get { return _dinArray[5]; }
			set { _dinArray[5] = value; }
		}

		/// <summary>
		/// Показания цифрового входа 7
		/// </summary>
		public bool DigitalInput7
		{
			get { return _dinArray[6]; }
			set { _dinArray[6] = value; }
		}

		/// <summary>
		/// Показания цифрового входа 8
		/// </summary>
		public bool DigitalInput8
		{
			get { return _dinArray[7]; }
			set { _dinArray[7] = value; }
		}

		/// <summary>
		/// Включено ли зажигание
		/// </summary>
		public bool? Ignition
		{
			get
			{
				if (Src == 0 || Src == 5)
					return Src == 0;

				return null;
			}
			set { Src = (byte) (value.HasValue && value.Value ? 0 : 5); }
		}

		/// <summary>
		/// время навигации (количество секунд с 00:00:00 01.01.2010 UTC);
		/// </summary>
		internal uint Ntm;

		/// <summary>
		/// широта по модулю, градусы/90 · 0xFFFFFFFF и взята целая часть
		/// </summary>
		internal uint Lat;

		/// <summary>
		/// долгота по модулю, градусы/180 · 0xFFFFFFFF и взята целая часть;
		/// </summary>
		internal uint Long;

		/// <summary>
		/// определяет дополнительные параметры навигационной посылки
		/// </summary>
		internal Flags Flg = new Flags
			{
				Mv = true
			};

		/// <summary>
		/// младшие биты;
		/// скорость в км/ч с дискретностью 0,1 км/ч (используется 14 младших бит);
		/// </summary>
		internal byte Spd;

		/// <summary>
		/// контейнер содержит старшие биты скорости и флаги
		/// </summary>
		internal SpdContainer Spdc = new SpdContainer();

		/// <summary>
		/// направление движения. Определяется как угол в градусах,
		/// который отсчитывается по часовой стрелке между северным направлением географического меридиана и направлением движения в точке измерения
		/// (дополнительно старший бит находится в поле DIRH)
		/// </summary>
		internal byte Dir;

		/// <summary>
		/// пройденное расстояние (пробег) в км, с дискретностью 0,1 км.
		/// </summary>
		internal byte[] Odm = new byte[3];

		/// <summary>
		/// битовые флаги, определяют состояние основных дискретных входов 1 ... 8
		/// (если бит равен 1, то соответствующий вход активен, если 0, то не-активен).
		/// Данное поле включено для удобства использования и экономии трафика при работе в системах мониторинга транспорта базового уровня
		/// </summary>
		internal byte Din
		{
			get
			{
				var bytes = new byte[1];
				_dinArray.CopyTo(bytes, 0);
				return bytes[0];
			}
			set
			{
				var bytes = new [] {value};
				_dinArray = new BitArray(bytes);
			}
		}

		/// <summary>
		/// определяет источник (событие), инициировавший посылку данной навигационной информации (информация представлена в Таблице N 3);
		///0 таймер при включенном зажигании
		///1 пробег заданной дистанции
		///2 превышение установленного значения угла поворота
		///3 ответ на запрос
		///4 изменение состояния входа X
		///5 таймер при выключенном зажигании
		///6 отключение периферийного оборудования
		///7 превышение одного из заданных порогов скорости
		///8 перезагрузка центрального процессора (рестарт)
		///9 перегрузка по выходу Y
		///10 сработал датчик вскрытия корпуса прибора
		///11 переход на резервное питание/отключение внешнего питания
		///12 снижение напряжения источника резервного питания ниже порогового значения
		///13 нажата "тревожная кнопка"
		///14 запрос на установление голосовой связи с оператором
		///15 экстренный вызов
		///16 появление данных от внешнего сервиса
		///17 зарезервировано
		///18 зарезервировано
		///19 неисправность резервного аккумулятора
		///20 резкий разгон
		///21 резкое торможение
		///22 отключение или неисправность навигационного модуля
		///23 отключение или неисправность датчика автоматической идентификации события ДТП
		///24 отключение или неисправность антенны GSM/UMTS
		///25 отключение или неисправность антенны навигационной системы
		///26 зарезервировано
		///27 снижение скорости ниже одного из заданных порогов
		///28 перемещение при выключенном зажигании
		///29 таймер в режиме "экстренное слежение"
		///30 начало/окончание навигации
		///31 "нестабильная навигация" (превышение порога частоты прерывания режима навигации при включенном зажигании или режиме экстренного слежения)
		///32 установка IP соединения
		///33 нестабильная регистрация в сети подвижной радиотелефонной связи
		///34 "нестабильная связь" (превышение порога частоты прерывания/восстановления IP соединения при включенном зажигании или режиме экстренного слежения)
		///35 изменение режима работы
		/// </summary>
		internal byte Src;

		/// <summary>
		/// высота над уровнем моря, м (опциональный параметр, наличие которого определяется битовым флагом ALTE)
		/// </summary>
		internal byte[] Alt = new byte[3];

		/// <summary>
		/// данные, характеризующие источник (событие) из поля SRC.
		/// Наличие и интерпретация значения данного поля определяется полем SRC.
		/// </summary>
		internal short Srcd;

		public override byte ServiceRecordType
		{
			get { return (byte) EgtsTeledataServiceSubRecords.EGTS_SR_POS_DATA; }
		}

		public override ServiceCode Service
		{
			get
			{
				return ServiceCode.EGTS_TELEDATA_SERVICE;
			}
		}

		/// <summary>
		/// Записать результат в поток.
		/// </summary>
		/// <param name="writer"></param>
		public override void WriteTo(BinaryWriter writer)
		{
			writer.Write(Ntm);
			writer.Write(Lat);
			writer.Write(Long);
			writer.Write(Flg.Value);
			writer.Write(Spd);
			writer.Write(Spdc.Value);
			writer.Write(Dir);
			writer.Write(Odm);
			writer.Write(Din);
			writer.Write(Src);
			if(Flg.Alte)
				writer.Write(Alt);
			writer.Write(Srcd);
		}

		public void ReadFrom(BinaryReader reader)
		{
			Ntm = (uint) reader.ReadInt32();
			Lat = (uint) reader.ReadInt32();
			Long = (uint) reader.ReadInt32();
			Flg = new Flags()
			{
				Value = reader.ReadByte()
			};
			Spd = reader.ReadByte();
			Spdc.Value = reader.ReadByte();
			Dir = reader.ReadByte();
			Odm = reader.ReadBytes(3);
			Din = reader.ReadByte();
			Src = reader.ReadByte();

			if (Flg.Alte)
				Alt = reader.ReadBytes(3);

			if(reader.BaseStream.Position < reader.BaseStream.Length)
				Srcd = reader.ReadInt16();
		}

		public byte[] GetBytes()
		{
			using (var stream = new MemoryStream())
			using (var writer = new BinaryWriter(stream))
			{
				WriteTo(writer);
				return stream.ToArray();
			}
		}

		public override string ToString()
		{
			return string.Format(CultureInfo.CurrentCulture,
				"{0}\r\n{1}\r\n{2}\r\n{3}\r\n{4}\r\n{5}\r\n{6}\r\n{7}\r\n{8}\r\n{9}\r\n{10}\r\n{11}\r\n",
				Ntm,
				Lat,
				Long,
				Flg.Value,
				Spd,
				Spdc.Value,
				Dir,
				string.Join(string.Empty, Odm.Select(o => o.ToString(CultureInfo.InvariantCulture))),
				Din,
				Src,
				string.Join(string.Empty, Alt.Select(o => o.ToString(CultureInfo.InvariantCulture))),
				Srcd);
		}
	}
}