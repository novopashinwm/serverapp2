﻿using System.Collections;
using System.IO;
using FORIS.TSS.Common;
using RU.NVG.Protocol.Egts.Enums;
using RU.NVG.Protocol.Egts.Helpers;

namespace RU.NVG.Protocol.Egts.Record.Teledata
{
	public class EgtsSrCountersData : ServiceSubRecordBase
	{
		private class CounterFlags
		{
			private BitArray _array = new BitArray(8);
			public byte Value
			{
				get
				{
					var result = new byte[1];
					_array.CopyTo(result, 0);
					return result[0];
				}
				set { _array = new BitArray(new[] { value }); }
			}
			public bool Cfe1
			{
				get { return _array[0]; }
				set { _array[0] = value; }
			}
			public bool Cfe2
			{
				get { return _array[1]; }
				set { _array[1] = value; }
			}
			public bool Cfe3
			{
				get { return _array[2]; }
				set { _array[2] = value; }
			}
			public bool Cfe4
			{
				get { return _array[3]; }
				set { _array[3] = value; }
			}
			public bool Cfe5
			{
				get { return _array[4]; }
				set { _array[4] = value; }
			}
			public bool Cfe6
			{
				get { return _array[5]; }
				set { _array[5] = value; }
			}
			public bool Cfe7
			{
				get { return _array[6]; }
				set { _array[6] = value; }
			}
			public bool Cfe8
			{
				get { return _array[7]; }
				set { _array[7] = value; }
			}
		}
		private CounterFlags Flags = new CounterFlags();
		private byte[] _cn1;
		private byte[] _cn2;
		private byte[] _cn3;
		private byte[] _cn4;
		private byte[] _cn5;
		private byte[] _cn6;
		private byte[] _cn7;
		private byte[] _cn8;
		public int? Counter1 => (int?)_cn1?.FromLE2UInt32(0, 3);
		public int? Counter2 => (int?)_cn2?.FromLE2UInt32(0, 3);
		public int? Counter3 => (int?)_cn3?.FromLE2UInt32(0, 3);
		public int? Counter4 => (int?)_cn4?.FromLE2UInt32(0, 3);
		public int? Counter5 => (int?)_cn5?.FromLE2UInt32(0, 3);
		public int? Counter6 => (int?)_cn6?.FromLE2UInt32(0, 3);
		public int? Counter7 => (int?)_cn7?.FromLE2UInt32(0, 3);
		public int? Counter8 => (int?)_cn8?.FromLE2UInt32(0, 3);
		public override void WriteTo(BinaryWriter writer)
		{
			throw new System.NotImplementedException();
		}
		public void ReadFrom(EgtsReader reader)
		{
			Flags.Value = reader.ReadByte();
			if (Flags.Cfe1)
				_cn1 = reader.ReadBytes(3);
			if (Flags.Cfe2)
				_cn2 = reader.ReadBytes(3);
			if (Flags.Cfe3)
				_cn3 = reader.ReadBytes(3);
			if (Flags.Cfe4)
				_cn4 = reader.ReadBytes(3);
			if (Flags.Cfe5)
				_cn5 = reader.ReadBytes(3);
			if (Flags.Cfe6)
				_cn6 = reader.ReadBytes(3);
			if (Flags.Cfe7)
				_cn7 = reader.ReadBytes(3);
			if (Flags.Cfe8)
				_cn8 = reader.ReadBytes(3);
		}
		public override byte ServiceRecordType
		{
			get { return (int)EgtsTeledataServiceSubRecords.EGTS_SR_COUNTERS_DATA; }
		}
		public override ServiceCode Service
		{
			get { return ServiceCode.EGTS_TELEDATA_SERVICE; }
		}
	}
}