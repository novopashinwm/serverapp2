﻿using System.Collections;
using System.IO;
using FORIS.TSS.Common;
using RU.NVG.Protocol.Egts.Enums;
using RU.NVG.Protocol.Egts.Helpers;
using RU.NVG.Protocol.Egts.Interface;

namespace RU.NVG.Protocol.Egts.Record.Teledata
{
	public class EgtsSrLiquidLevelSensor : ISubRecord
	{
		public enum MeasureUnit
		{
			NoUnit     = 0,
			Percentage = 1,
			Liters     = 2
		}

		private class Flags
		{
			private BitArray _arr = new BitArray(8);

			/// <summary>
			/// порядковый номер датчика
			/// </summary>
			public byte Llsn
			{
				get
				{
					return (byte)
					(
						(_arr[0] ? 1 : 0) +
						2 * (_arr[1] ? 1 : 0) +
						4 * (_arr[2] ? 1 : 0)
					);
				}
				set
				{
					_arr[0] = (value & 0x1) > 0;
					_arr[1] = (value & 0x2) > 0;
					_arr[2] = (value & 0x4) > 0;
				}
			}

			/// <summary>
			/// (Raw Data Flag) флаг определяющий формат поля LLSD данной подзаписи.
			/// 0 = поле LLSD имеет размер 4 байта (тип данных UINT) и содержит показания ДУЖ в единицах измерения, определяемых полем LLSVU;
			/// 1 = поле LLSD содержит данные ДУЖ в неизменном виде, как они поступили из внешнего порта АТ
			///     (размер поля LLSD при этом определяется исходя из общей длины данной подзаписи и размеров расположенных перед LLSD полей).
			/// </summary>
			public bool Rdf
			{
				get { return _arr[3]; }
				set { _arr[3] = value; }
			}

			/// <summary>
			/// битовый флаг, определяющий единицы измерения показаний ДУЖ.
			///00 =  неторированное показание датчика;
			///01 = показания датчика в % от общего объема емкости;
			///10 = показания с дискретностью в 0.1 литра.
			/// </summary>
			public byte Llsvu
			{
				get
				{
					return (byte)
					(
						(_arr[4] ? 1 : 0) +
						2*(_arr[5] ? 1 : 0)
					);
				}
				set
				{
					_arr[4] = (value & 0x1) > 0;
					_arr[5] = (value & 0x2) > 0;
				}
			}

			/// <summary>
			/// (Liquid Level Sensor Error Flag) битовый флаг, определяющий наличие ошибок при считывании значения датчика уровня жидкости:
			/// 0 =  ошибок не обнаружено;
			/// 1 = ошибка при считывании показаний датчика.
			/// </summary>
			public bool Llsef
			{
				get { return _arr[6]; }
				set { _arr[6] = value; }
			}

			public byte Value
			{
				get
				{
					var buf = new byte[1];
					_arr.CopyTo(buf, 0);
					return buf[0];
				}
				set { _arr = new BitArray(new[] {value}); }
			}
		}

		private readonly Flags _info = new Flags();

		private short _mAddr;

		private byte[] _llsd;

		public bool Valid
		{
			get { return !_info.Llsef; }
			set { _info.Llsef = !value; }
		}

		public MeasureUnit Unit
		{
			get { return (MeasureUnit) _info.Llsvu; }
		}
		/// <summary>
		/// Метод установки значения, вместе с единицей измерения.
		/// Т.к. хранимое значение зависит от единиц измерения, то выполняем это одним методом.
		/// </summary>
		/// <param name="val"></param>
		/// <param name="unit"></param>
		public void SetValueWithUnit(int? val, MeasureUnit unit)
		{
			_info.Llsvu = (byte)unit;
			Value       = val;
		}

		/// <summary>
		/// Показания в 4 байтовом формате.
		/// </summary>
		public int? Value
		{
			get
			{
				if (!Valid)
					return null;

				if (_info.Rdf)
					return null;

				var dataReader = new DataReader(_llsd);
				var value = (int)dataReader.ReadLittleEndian32(4);
				if (Unit == MeasureUnit.Liters)
					value = (int)(value / 10f);

				return value;
			}
			set
			{
				_llsd = null;
				Valid = value != null;
				if (value == null)
					return;

				_info.Rdf = false;
				var val = value.Value;
				if (Unit == MeasureUnit.Liters)
					val = val * 10;

				_llsd = new byte[4];
				_llsd[0] = (byte)((val >> 00) & 0xFF);
				_llsd[1] = (byte)((val >> 08) & 0xFF);
				_llsd[2] = (byte)((val >> 16) & 0xFF);
				_llsd[3] = (byte)((val >> 24) & 0xFF);
			}
		}

		/// <summary>
		/// Бинарные данные с внешнего порта.
		/// </summary>
		public byte[] RawValue
		{
			get
			{
				if (!Valid)
					return null;

				if (!_info.Rdf)
					return null;

				return _llsd;
			}
			set
			{
				_llsd = null;
				Valid = value != null;
				if (value == null)
					return;

				_info.Rdf = true;
				_llsd = value;
			}
		}

		public void ReadFrom(EgtsReader reader)
		{
			_info.Value = reader.ReadByte();
			_mAddr = reader.ReadInt16();
			var length = _info.Rdf ? (int) (reader.BaseStream.Length - reader.BaseStream.Position) : 4;
			_llsd = reader.ReadBytes(length);
		}

		public void WriteTo(BinaryWriter writer)
		{
			writer.Write(_info.Value);
			writer.Write(_mAddr);
			writer.Write(_llsd);
		}

		public byte ServiceRecordType
		{
			get { return (byte) EgtsTeledataServiceSubRecords.EGTS_SR_LIQUID_LEVEL_SENSOR; }
		}

		public ServiceCode Service
		{
			get { return ServiceCode.EGTS_TELEDATA_SERVICE; }
		}
	}
}