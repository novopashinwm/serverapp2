﻿using System.Collections;
using System.IO;
using RU.NVG.Protocol.Egts.Enums;

namespace RU.NVG.Protocol.Egts.Record.Teledata
{
	public class EgtsSrStateData : ServiceSubRecordBase
	{
		internal class Flags
		{
			private BitArray _arr = new BitArray(8);
			/// <summary> Преобразованное значение </summary>
			public byte Value
			{
				get
				{
					var bytes = new byte[1];
					_arr.CopyTo(bytes, 0);
					return bytes[0];
				}
				set
				{
					var bytes = new byte[1];
					bytes[0] = value;
					_arr = new BitArray(bytes);
				}
			}
			/// <summary> Навигационный модуль включен </summary>
			public bool Nms
			{
				get { return _arr[2]; }
				set { _arr[2] = value; }
			}
			/// <summary> Внешний резервный источник </summary>
			public bool Ibu
			{
				get { return _arr[1]; }
				set { _arr[1] = value; }
			}
			/// <summary> Внутренняя батарея </summary>
			public bool Bbu
			{
				get { return _arr[0]; }
				set { _arr[0] = value; }
			}
		}
		/// <summary> Текущий режим работы </summary>
		internal byte St;
		/// <summary> Напряжение основного источника питания </summary>
		internal byte Mpsv;
		/// <summary> Напряжение резервной батареи </summary>
		internal byte Bbv;
		/// <summary> Напряжение внутренней батареи </summary>
		internal byte Ibv;
		internal Flags Flag = new Flags();
		public EgtsDeviceWorkMode Mode
		{
			get { return (EgtsDeviceWorkMode) St; }
		}
		public bool IsNavigate
		{
			get { return Flag.Nms; }
		}
		public override byte ServiceRecordType
		{
			get { return (int) EgtsTeledataServiceSubRecords.EGTS_SR_STATE_DATA; }
		}
		public override ServiceCode Service
		{
			get { return ServiceCode.EGTS_TELEDATA_SERVICE; }
		}
		public override void WriteTo(BinaryWriter writer)
		{
			writer.Write(St);
			writer.Write(Mpsv);
			writer.Write(Bbv);
			writer.Write(Ibv);
			writer.Write(Flag.Value);
		}
		public void ReadFrom(BinaryReader reader)
		{
			var st = reader.ReadByte();
			var mpsv = reader.ReadByte();
			var bbv = reader.ReadByte();
			var ibv = reader.ReadByte();
			var flags = reader.ReadByte();

			St = st;
			Mpsv = mpsv;
			Bbv = bbv;
			Ibv = ibv;
			Flag.Value = flags;
		}
	}
}