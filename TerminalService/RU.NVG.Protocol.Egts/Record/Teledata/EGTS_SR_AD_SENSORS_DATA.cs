﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using RU.NVG.Protocol.Egts.Enums;
using RU.NVG.Protocol.Egts.Helpers;

namespace RU.NVG.Protocol.Egts.Record.Teledata
{
	public class EgtsSrAdSensorsData : ServiceSubRecordBase
	{
		private class DIOE
		{
			private BitArray _array = new BitArray(8);
			public byte Value
			{
				get
				{
					var result = new byte[1];
					_array.CopyTo(result, 0);
					return result[0];
				}
				set { _array = new BitArray(new[] { value }); }
			}
			public bool Dioe1
			{
				get { return _array[0]; }
				set { _array[0] = value; }
			}
			public bool Dioe2
			{
				get { return _array[1]; }
				set { _array[1] = value; }
			}
			public bool Dioe3
			{
				get { return _array[2]; }
				set { _array[2] = value; }
			}
			public bool Dioe4
			{
				get { return _array[3]; }
				set { _array[3] = value; }
			}
			public bool Dioe5
			{
				get { return _array[4]; }
				set { _array[4] = value; }
			}
			public bool Dioe6
			{
				get { return _array[5]; }
				set { _array[5] = value; }
			}
			public bool Dioe7
			{
				get { return _array[6]; }
				set { _array[6] = value; }
			}
			public bool Dioe8
			{
				get { return _array[7]; }
				set { _array[7] = value; }
			}
		}
		private class ASFE
		{
			private BitArray _array = new BitArray(8);
			public byte Value
			{
				get
				{
					var result = new byte[1];
					_array.CopyTo(result, 0);
					return result[0];
				}
				set { _array = new BitArray(new[] { value }); }
			}
			public bool Asfe1
			{
				get { return _array[0]; }
				set { _array[0] = value; }
			}
			public bool Asfe2
			{
				get { return _array[1]; }
				set { _array[1] = value; }
			}
			public bool Asfe3
			{
				get { return _array[2]; }
				set { _array[2] = value; }
			}
			public bool Asfe4
			{
				get { return _array[3]; }
				set { _array[3] = value; }
			}
			public bool Asfe5
			{
				get { return _array[4]; }
				set { _array[4] = value; }
			}
			public bool Asfe6
			{
				get { return _array[5]; }
				set { _array[5] = value; }
			}
			public bool Asfe7
			{
				get { return _array[6]; }
				set { _array[6] = value; }
			}
			public bool Asfe8
			{
				get { return _array[7]; }
				set { _array[7] = value; }
			}
		}
		private DIOE DIO = new DIOE();
		private byte Dout;
		private ASFE ASF = new ASFE();
		/// <summary>
		/// показания дополнительных дискретных входов
		/// </summary>
		private byte Adio1;
		private byte Adio2;
		private byte Adio3;
		private byte Adio4;
		private byte Adio5;
		private byte Adio6;
		private byte Adio7;
		private byte Adio8;
		/// <summary>
		/// значение аналоговых датчиков с 1 по 8 соответственно
		/// </summary>
		private byte[] Ans1;
		private byte[] Ans2;
		private byte[] Ans3;
		private byte[] Ans4;
		private byte[] Ans5;
		private byte[] Ans6;
		private byte[] Ans7;
		private byte[] Ans8;
		private const int EGTS_ANALOGUEINPUT_BYTES_LENGTH = 3;
		public int? AnalogueInput1
		{
			get
			{
				if (!ASF.Asfe1)
					return null;
				return GetAnalogueInput(Ans1, 0);
			}
			set
			{
				ASF.Asfe1 = SetAnalogueInput(value, out Ans1);
			}
		}
		public int? AnalogueInput2
		{
			get
			{
				if (!ASF.Asfe2)
					return null;
				return GetAnalogueInput(Ans2, 0);
			}
			set
			{
				ASF.Asfe2 = SetAnalogueInput(value, out Ans2);
			}
		}
		public int? AnalogueInput3
		{
			get
			{
				if (!ASF.Asfe3)
					return null;
				return GetAnalogueInput(Ans3, 0);
			}
			set
			{
				ASF.Asfe3 = SetAnalogueInput(value, out Ans3);
			}
		}
		public int? AnalogueInput4
		{
			get
			{
				if (!ASF.Asfe4)
					return null;
				return GetAnalogueInput(Ans4, 0);
			}
			set
			{
				ASF.Asfe4 = SetAnalogueInput(value, out Ans4);
			}
		}
		public int? AnalogueInput5
		{
			get
			{
				if (!ASF.Asfe5)
					return null;
				return GetAnalogueInput(Ans5, 0);
			}
			set
			{
				ASF.Asfe5 = SetAnalogueInput(value, out Ans5);
			}
		}
		public int? AnalogueInput6
		{
			get
			{
				if (!ASF.Asfe6)
					return null;
				return GetAnalogueInput(Ans6, 0);
			}
			set
			{
				ASF.Asfe6 = SetAnalogueInput(value, out Ans6);
			}
		}
		public int? AnalogueInput7
		{
			get
			{
				if (!ASF.Asfe7)
					return null;
				return GetAnalogueInput(Ans7,0);
			}
			set
			{
				ASF.Asfe7 = SetAnalogueInput(value, out Ans7);
			}
		}
		public int? AnalogueInput8
		{
			get
			{
				if (!ASF.Asfe8)
					return null;
				return GetAnalogueInput(Ans8, 0);
			}
			set
			{
				ASF.Asfe8 = SetAnalogueInput(value, out Ans8);
			}
		}
		/// <summary>
		/// Заполнение значения Аналогового датчика.
		/// </summary>
		/// <param name="value">Nullable значение.</param>
		/// <param name="ans">Ссылка на поле содержащее значение Аналогового датчика</param>
		/// <returns>Содержит ли значение параметр?"<paramref name="ans"/>".</returns>
		private bool SetAnalogueInput(int? value, out byte[] ans)
		{
			if (value.HasValue)
			{
				ValidateNumericRange(value.Value);
				var src = BitConverter.GetBytes(value.Value);
				if (!BitConverter.IsLittleEndian)
					Array.Reverse(src);
				ans = src.Take(EGTS_ANALOGUEINPUT_BYTES_LENGTH).ToArray();
			}
			else
				ans = null;
			return value.HasValue;
		}
		private static void ValidateNumericRange(int value)
		{
			const int MaxValue32 = +8388607; // Максимально возможное значение для Int24
			const int MinValue32 = -8388608; // Минимально  возможное значение для Int24
			if (value > (MaxValue32 + 1) || value < MinValue32)
				throw new OverflowException($"Value of {value} will not fit in a 24-bit signed integer");
		}
		/// <summary> Получение значения аналогового датчика </summary>
		/// <param name="ans"></param>
		/// <param name="offset"></param>
		/// <returns></returns>
		private int? GetAnalogueInput(byte[] ans, uint offset)
		{
			var length = (uint)EGTS_ANALOGUEINPUT_BYTES_LENGTH;
			if (ans == null || offset + length > ans.Length)
			{
				if (ans == null)
					throw new ArgumentNullException(nameof(ans));
				if (offset + length > ans.Length)
					throw new ArgumentOutOfRangeException(nameof(length), $"{nameof(offset)} of {offset} and {nameof(length)} of {length} will exceed array size of {ans.Length}");
			}
			// Байты выбираем в соответствии с протоколом "ANS1 … ANS8 – значение аналоговых датчиков с 1 по 8 соответственно [младший байт вперёд/LittleEndian]."
			return ApplyBitMask(ans[0] | ans[1] << 8 | ans[2] << 16);
		}
		private static int ApplyBitMask(int value)
		{
			const int bitMask = -16777216;
			// Проверяем установлен ли 23 бит(нумерация с 0), для определения знака
			if ((value & 0x00800000) > 0)
				value |=  bitMask; // Отрицательное значение
			else
				value &= ~bitMask; // Положительное значение
			return value;
		}
		public override void WriteTo(BinaryWriter writer)
		{
			writer.Write(DIO.Value);
			writer.Write(Dout);
			writer.Write(ASF.Value);

			if (DIO.Dioe1)
				writer.Write(Adio1);
			if (DIO.Dioe2)
				writer.Write(Adio2);
			if (DIO.Dioe3)
				writer.Write(Adio3);
			if (DIO.Dioe4)
				writer.Write(Adio4);
			if (DIO.Dioe5)
				writer.Write(Adio5);
			if (DIO.Dioe6)
				writer.Write(Adio6);
			if (DIO.Dioe7)
				writer.Write(Adio7);
			if (DIO.Dioe8)
				writer.Write(Adio8);

			if (ASF.Asfe1)
				writer.Write(Ans1);
			if (ASF.Asfe2)
				writer.Write(Ans2);
			if (ASF.Asfe3)
				writer.Write(Ans3);
			if (ASF.Asfe4)
				writer.Write(Ans4);
			if (ASF.Asfe5)
				writer.Write(Ans5);
			if (ASF.Asfe6)
				writer.Write(Ans6);
			if (ASF.Asfe7)
				writer.Write(Ans7);
			if (ASF.Asfe8)
				writer.Write(Ans8);
		}
		public void ReadFrom(EgtsReader reader)
		{
			var dioe = reader.ReadByte();
			var dout = reader.ReadByte();
			var asfe = reader.ReadByte();

			DIO.Value = dioe;
			Dout = dout;
			ASF.Value = asfe;

			if (DIO.Dioe1)
				Adio1 = reader.ReadByte();
			if (DIO.Dioe2)
				Adio2 = reader.ReadByte();
			if (DIO.Dioe3)
				Adio3 = reader.ReadByte();
			if (DIO.Dioe4)
				Adio4 = reader.ReadByte();
			if (DIO.Dioe5)
				Adio5 = reader.ReadByte();
			if (DIO.Dioe6)
				Adio6 = reader.ReadByte();
			if (DIO.Dioe7)
				Adio7 = reader.ReadByte();
			if (DIO.Dioe8)
				Adio8 = reader.ReadByte();

			if (ASF.Asfe1)
				Ans1 = reader.ReadBytes(EGTS_ANALOGUEINPUT_BYTES_LENGTH);
			if (ASF.Asfe2)
				Ans2 = reader.ReadBytes(EGTS_ANALOGUEINPUT_BYTES_LENGTH);
			if (ASF.Asfe3)
				Ans3 = reader.ReadBytes(EGTS_ANALOGUEINPUT_BYTES_LENGTH);
			if (ASF.Asfe4)
				Ans4 = reader.ReadBytes(EGTS_ANALOGUEINPUT_BYTES_LENGTH);
			if (ASF.Asfe5)
				Ans5 = reader.ReadBytes(EGTS_ANALOGUEINPUT_BYTES_LENGTH);
			if (ASF.Asfe6)
				Ans6 = reader.ReadBytes(EGTS_ANALOGUEINPUT_BYTES_LENGTH);
			if (ASF.Asfe7)
				Ans7 = reader.ReadBytes(EGTS_ANALOGUEINPUT_BYTES_LENGTH);
			if (ASF.Asfe8)
				Ans8 = reader.ReadBytes(EGTS_ANALOGUEINPUT_BYTES_LENGTH);
		}
		public override byte ServiceRecordType
		{
			get { return (int)EgtsTeledataServiceSubRecords.EGTS_SR_AD_SENSORS_DATA; }
		}
		public override ServiceCode Service
		{
			get { return ServiceCode.EGTS_TELEDATA_SERVICE; }
		}
	}
}