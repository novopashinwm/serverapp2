﻿using System;
using System.Collections;
using System.IO;
using RU.NVG.Protocol.Egts.Enums;
using RU.NVG.Protocol.Egts.Helpers;
using RU.NVG.Protocol.Egts.Interface;

namespace RU.NVG.Protocol.Egts.Record.Teledata
{
	public class EgtsSrExtPosData : ISubRecord
	{
		internal class Flags
		{
			private BitArray _array = new BitArray(8);
			public byte Value
			{
				get
				{
					var result = new byte[1];
					_array.CopyTo(result, 0);
					return result[0];
				}
				set { _array = new BitArray(new[] {value}); }
			}
			/// <summary>
			/// (VDOP Field Exists) определяет наличие поля VDOP
			/// </summary>
			public bool Vfe
			{
				get { return _array[0]; }
				set { _array[0] = value; }
			}
			/// <summary>
			/// (HDOP Field Exists) определяет наличие поля HDOP
			/// </summary>
			public bool Hfe
			{
				get { return _array[1]; }
				set { _array[1] = value; }
			}
			/// <summary>
			/// (PDOP Field Exists) определяет наличие поля PDOP
			/// </summary>
			public bool Pfe
			{
				get { return _array[2]; }
				set { _array[2] = value; }
			}
			/// <summary>
			/// (Satellites Field Exists) определяет наличие данных о текущем количестве видимых спутников SAT, и типе используемой навигационной спутниковой системы NS
			/// </summary>
			public bool Sfe
			{
				get { return _array[3]; }
				set { _array[3] = value; }
			}
			/// <summary>
			/// (Navigation System Field Exists) определяет наличие данных о типах используемых навигационных спутниковых систем
			/// </summary>
			public bool Nsfe
			{
				get { return _array[4]; }
				set { _array[4] = value; }
			}
		}
		/// <summary>
		/// Флаги.
		/// </summary>
		private readonly Flags _flags = new Flags();
		/// <summary>
		/// снижение точности в вертикальной плоскости (значение, умноженное на 100);
		/// </summary>
		private ushort _vdop;
		/// <summary>
		/// снижение точности в горизонтальной плоскости (значение, умноженное на 100)
		/// </summary>
		private ushort _hdop;
		/// <summary>
		/// снижение точности по местоположению (значение, умноженное на 100)
		/// </summary>
		private ushort _pdop;
		/// <summary>
		/// количество видимых спутников
		/// </summary>
		private byte _satellites;
		/// <summary>
		/// битовые флаги, характеризующие используемые навигационные спутниковые системы
		/// 0   = система не определена;
		/// 1   = ГЛОНАСС;
		/// 2   = GPS;
		/// 4   = Galileo;
		/// 8   = Compass;
		/// 16 = Beidou;
		/// 32 = DORIS;
		/// 64 = IRNSS;
		/// 128 = QZSS.
		/// </summary>
		internal ushort _ns;
		public ushort? Vdop
		{
			get { return _flags.Vfe ? _vdop : (ushort?)null; }
			set
			{
				_flags.Vfe = value.HasValue;
				_vdop = value ?? 0;
			}
		}
		public ushort? Hdop
		{
			get { return _flags.Hfe ? _hdop : (ushort?)null; }
			set
			{
				_flags.Hfe = value.HasValue;
				_hdop = value ?? 0;
			}
		}
		public ushort? Pdop
		{
			get { return _flags.Pfe ? _pdop : (ushort?)null; }
			set
			{
				_flags.Pfe = value.HasValue;
				_pdop = value ?? 0;
			}
		}
		public byte? Satellites
		{
			get { return _flags.Sfe ? _satellites : (byte?)null; }
			set
			{
				_flags.Sfe = value.HasValue;
				_satellites = value ?? 0;
			}
		}
		public NavigationSystems NavigationSystems
		{
			get { return _flags.Nsfe ? (NavigationSystems)_ns : NavigationSystems.Unknown; }
			set
			{
				_flags.Nsfe = value != NavigationSystems.Unknown;
				_ns = (ushort) value;
			}
		}
		public byte ServiceRecordType
		{
			get { return (byte)EgtsTeledataServiceSubRecords.EGTS_SR_EXT_POS_DATA; }
		}
		public ServiceCode Service
		{
			get { return ServiceCode.EGTS_TELEDATA_SERVICE; }
		}
		public void ReadFrom(EgtsReader egtsReader)
		{
			_flags.Value = egtsReader.ReadByte();
			if (_flags.Vfe)
				_vdop = egtsReader.ReadUInt16();
			if (_flags.Hfe)
				_hdop = egtsReader.ReadUInt16();
			if (_flags.Pfe)
				_pdop = egtsReader.ReadUInt16();
			if (_flags.Sfe)
				_satellites = egtsReader.ReadByte();
			if (_flags.Nsfe || _flags.Sfe)
				_ns = egtsReader.ReadUInt16();
		}
		public void WriteTo(BinaryWriter writer)
		{
			writer.Write(_flags.Value);
			if(_flags.Vfe)
				writer.Write(_vdop);
			if(_flags.Hfe)
				writer.Write(_hdop);
			if(_flags.Pfe)
				writer.Write(_pdop);
			if(_flags.Sfe)
				writer.Write(_satellites);
			if (_flags.Nsfe || _flags.Sfe)
				writer.Write(_ns);
		}
	}
}