﻿using System.Globalization;
using System.IO;
using RU.NVG.Protocol.Egts.Enums;

namespace RU.NVG.Protocol.Egts.Record
{
	public class EgtsSrRecordResponse : ServiceSubRecordBase
	{
		public override byte ServiceRecordType
		{
			get { return 0; }
		}

		/// <summary>
		/// номер подтверждаемой записи (значение поля RN из обрабатываемой записи)
		/// </summary>
		public ushort Crn;

		/// <summary>
		/// статус обработки записи
		/// </summary>
		public ResponseResult Rst;

		public override ServiceCode Service
		{
			get { return _serviceCode; }
		}

		public EgtsSrRecordResponse(ServiceCode code)
		{
			_serviceCode = code;
		}

		public override void WriteTo(BinaryWriter writer)
		{
			writer.Write(Crn);
			writer.Write((byte) Rst);
		}

		public void ReadFrom(BinaryReader reader)
		{
			var crn = reader.ReadInt16();
			var rst = reader.ReadByte();

			Crn = (ushort) crn;
			Rst = (ResponseResult) rst;
		}

		public override string ToString()
		{
			return string.Format(CultureInfo.InvariantCulture, "{0} - {1}", Crn, Rst);
		}

		private readonly ServiceCode _serviceCode;
	}
}