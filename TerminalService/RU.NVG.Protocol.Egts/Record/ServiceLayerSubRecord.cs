﻿using System.IO;
using RU.NVG.Protocol.Egts.Helpers;
using RU.NVG.Protocol.Egts.Interface;

namespace RU.NVG.Protocol.Egts.Record
{
	internal class ServiceLayerSubRecord : IWritable
	{
		/// <summary>
		/// тип подзаписи (подтип передаваемых данных в рамках общего набора типов одного Сервиса).
		/// Тип 0 – специальный, зарезервирован за подзаписью подтверждения данных для каждого сервиса.
		/// Конкретные значения номеров типов подзаписей определяются логикой самого Сервиса.
		/// Протокол оговаривает лишь то, что этот номер должен присутствовать, а нулевой идентификатор зарезервирован
		/// </summary>
		internal byte Srt
		{
			get { return DataRecord.ServiceRecordType; }
		}
		/// <summary> длина данных в байтах подзаписи в поле SRD </summary>
		internal ushort Srl;
		/// <summary> данные подзаписи. Наполнение данного поля специфично для каждого сочетания идентификатора Сервиса и типа подзаписи </summary>
		internal byte[] Srd;
		/// <summary> Размер записи </summary>
		public int Size
		{
			get { return 1 + 2 + Srd.Length; }
		}
		/// <summary> Запись данных </summary>
		public ISubRecord DataRecord { get; private set; }
		public ServiceLayerSubRecord(ISubRecord serviceDataRecord)
		{
			DataRecord = serviceDataRecord;
			Srd = serviceDataRecord.GetBytes();
			Srl = (ushort)Srd.Length;
		}
		internal ServiceLayerSubRecord()
		{
		}
		public void WriteTo(BinaryWriter writer)
		{
			writer.Write(Srt);
			writer.Write(Srl);
			writer.Write(Srd);
		}
		public void ReadFrom(EgtsReader reader, ServiceLayerRecord serviceLayerRecord)
		{
			var srt = reader.ReadByte();
			var srl = (ushort) reader.ReadInt16();
			var srd = reader.ReadBytes(srl);

			ISubRecord serviceDataRecord;
			using (var stream = new MemoryStream(srd))
			using (var egtsReader = new EgtsReader(stream))
			{
				serviceDataRecord = egtsReader.ReadSubRecord(serviceLayerRecord.ServiceCode, srt);
			}

			Srl = srl;
			Srd = srd;
			DataRecord = serviceDataRecord;
		}
	}
}