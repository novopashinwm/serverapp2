﻿using System.Collections;
using System.IO;
using RU.NVG.Protocol.Egts.Enums;

namespace RU.NVG.Protocol.Egts.Record.Firmware
{
	public class EgtsSrServiceFullData : ServiceSubRecordBase
	{
		public const byte Delimeter = 0;

		public class ObjectAttribute
		{
			private BitArray _arr = new BitArray(8);

			/// <summary>
			/// – тип сущности по содержанию. Определены следующие значения данного поля:
			/// 00 = данные внутреннего ПО («прошивка»);
			/// 01 = блок конфигурационных параметров
			/// </summary>
			public byte Ot
			{
				get { return (byte) ((_arr[2] ? 1 : 0) + (_arr[3] ? 1 : 0)*2); }
				set
				{
					_arr[2] = (value & 0x01) == 0x01;
					_arr[3] = (value & 0x02) == 0x02;
				}
			}

			/// <summary>
			/// тип модуля, для которого предназначена передаваемая сущность. Определены следующие значения данного поля:
			/// 00 = периферийное оборудование;
			/// 01 = АC
			/// </summary>
			public byte Mt
			{
				get { return (byte) ((_arr[0] ? 1 : 0) + (_arr[1] ? 1 : 0)*2); }
				set
				{
					_arr[0] = (value & 0x01) == 0x01;
					_arr[1] = (value & 0x02) == 0x02;
				}
			}

			public byte Value
			{
				get
				{
					var bytes = new byte[1];
					_arr.CopyTo(bytes, 0);
					return bytes[0];
				}
				set { _arr = new BitArray(new[] {value}); }
			}
		}

		/// <summary>
		/// характеристика принадлежности передаваемой сущности
		/// </summary>
		public readonly ObjectAttribute Oa  = new ObjectAttribute();

		/// <summary>
		/// номер компонента в случае принадлежности сущности непосредственно АС или идентификатор периферийного модуля/порта,
		/// подключенного к АС, в зависимости от значения параметра MT
		/// </summary>
		public byte Cmi;

		/// <summary>
		/// версия передаваемой сущности
		/// (старший байт – число до точки – major  version, младший,  после точки – minor version,
		/// например версия 2.34 будет представлена числом 0x0222)).
		/// </summary>
		public ushort Ver;

		/// <summary>
		/// сигнатура (контрольная сумма), всей передаваемой сущности. Используется алгоритм СRC16-CCITT
		/// </summary>
		public ushort Wos;

		/// <summary>
		/// имя файла передаваемой сущности (данное поле опционально и может иметь нулевую длину
		/// +
		/// разделитель строковых параметров (всегда имеет значение 0)
		/// </summary>
		public string Fn;

		public override byte ServiceRecordType
		{
			get { return (byte) EgtsFirmwareServiceSubRecords.EGTS_SR_SERVICE_FULL_DATA; }
		}

		public override ServiceCode Service
		{
			get
			{
				return ServiceCode.EGTS_FIRMWARE_SERVICE;
			}
		}

		public override void WriteTo(BinaryWriter writer)
		{
			writer.Write(Cmi);
			writer.Write(Ver);
			writer.Write(Wos);
			writer.Write(Fn);
			writer.Write(Delimeter);
		}
	}
}