﻿using System.IO;
using RU.NVG.Protocol.Egts.Enums;
using RU.NVG.Protocol.Egts.Interface;

namespace RU.NVG.Protocol.Egts.Record
{
	public abstract class ServiceSubRecordBase : ISubRecord
	{
		public abstract void WriteTo(BinaryWriter writer);
		public abstract byte ServiceRecordType { get; }
		public abstract ServiceCode Service { get; }
	}
}