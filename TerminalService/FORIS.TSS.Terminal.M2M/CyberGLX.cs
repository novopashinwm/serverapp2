﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.M2M
{
	public class CyberGLX : Device
	{
		internal const string ProtocolName = "M2M-Cyber GLX";
		internal static class DeviceNames
		{
			internal const string CyberGLX = ProtocolName;
		}
		public CyberGLX() : base(typeof (CyberGLXSensor), new []
		{
			DeviceNames.CyberGLX
		})
		{
		}

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;

			var packetLength = data[2];
			var packetType = data[3];
			var packetNumber = data[4] | (data[5] << 8);

			try
			{
				if (data.Length < packetLength)
				{
					bufferRest = (byte[]) data.Clone();
					return null; //Ждем продолжения пакета
				}

				if (data.Length > packetLength)
				{
					bufferRest = new byte[data.Length - packetLength];
					Array.Copy(data, packetLength, bufferRest, 0, bufferRest.Length);
				}

				if (data[packetLength - 1] != 0xFA)
					return new List<object> {AnswerDatagram(packetType, packetNumber, AnswerType.UnknownCommand)};

				var receivedCRC = data[packetLength - 2];
				var calculatedCRC = CalculateCRC(data, 2, packetLength - 4);

				if (receivedCRC != calculatedCRC)
				{
					Trace.WriteLine("CyberGLX: Invalid CRC, data: " + BitConverter.ToString(data));
					return new List<object> {AnswerDatagram(packetType, packetNumber, AnswerType.InvalidCRC)};
				}

				switch ((MessageType)(packetType & 0x3f))
				{
					case MessageType.Registration:
						{
							var registrationDatagram = new RegistrationDatagram(data);

							var state = new ReceiverStoreToStateMessage
											{
												StateData = new CyberGLXReceiverState
																{
																	DeviceID = registrationDatagram.DeviceID,
																	Firmware = registrationDatagram.Firmware
																}
											};

							return new List<object> {(object)state, AnswerDatagram(packetType, packetNumber, AnswerType.Success)};
						}
					case MessageType.Coordinates:
					case MessageType.Coordinates01:
						{
							var datagram = AvlData.Create(data);
							
							var mobileUnit = GetMobileUnit(datagram);

							var state = stateData as CyberGLXReceiverState;
							if (state != null)
							{
								mobileUnit.DeviceID = state.DeviceID;
								mobileUnit.Firmware = state.Firmware.ToString(CultureInfo.InvariantCulture);
							}

							return new List<object>
									   {
										   AnswerDatagram(packetType, packetNumber, AnswerType.Success),
										   mobileUnit
									   };
						}
					case MessageType.IsAlive:
						return new List<object> {AnswerDatagram(packetType, 0, AnswerType.Success)};
					default:
						Trace.WriteLine("CyberGLX: Unknown command, data: " + BitConverter.ToString(data));
						return new List<object> {AnswerDatagram(packetType, packetNumber, AnswerType.UnknownCommand)};
				}
			}
			catch (Exception exception)
			{
				Trace.WriteLine("CyberGLX: " + exception);
				return new List<object> {AnswerDatagram(packetType, packetNumber, AnswerType.ServerError)};
			}
		}

		private IMobilUnit GetMobileUnit(AvlData datagram)
		{
			var mu = UnitReceived();
			if (mu.SensorValues == null)
				mu.SensorValues = new Dictionary<int, long>();

			mu.cmdType = CmdType.Trace;
			mu.Time = TimeHelper.GetSecondsFromBase(datagram.DateTime);
			mu.Latitude = (double) datagram.Latitute;
			mu.Longitude = (double) datagram.Longitude;
			mu.Speed = (int) datagram.Speed;
			mu.Satellites = datagram.Satellites;
			if (datagram.DigitalSensors != null)
			{
				for (var i = 0; i != 8; ++i)
				{
					mu.SensorValues.Add(
						(int) CyberGLXSensor.DI1 + i,
						(datagram.DigitalSensors.Value >> i) & 0x01);
				}
			}

			mu.SensorValues[(int)CyberGLXSensor.B1] = (datagram.PacketSource & 0x0400) != 0 ? 1 : 0;
			mu.SensorValues[(int)CyberGLXSensor.B2] = (datagram.PacketSource & 0x0100) != 0 ? 1 : 0;
			mu.SensorValues[(int)CyberGLXSensor.B3] = (datagram.PacketSource & 0x0080) != 0 ? 1 : 0;
			mu.SensorValues[(int)CyberGLXSensor.B4] = (datagram.PacketSource & 0x0040) != 0 ? 1 : 0;
			mu.SensorValues[(int)CyberGLXSensor.B5] = (datagram.PacketSource & 0x0020) != 0 ? 1 : 0;

			if (datagram.V1 != null)
				mu.SensorValues.Add((int)CyberGLXSensor.AI1, datagram.V1.Value);
			if (datagram.V2 != null)
				mu.SensorValues.Add((int)CyberGLXSensor.AI2, datagram.V2.Value);
			if (datagram.V3 != null)
				mu.SensorValues.Add((int)CyberGLXSensor.AI3, datagram.V3.Value);
			if (datagram.V4 != null)
				mu.SensorValues.Add((int)CyberGLXSensor.AI4, datagram.V4.Value);

			mu.Course = (int) Math.Round(datagram.Course);
			mu.CorrectGPS = datagram.CorrectGPS && datagram.Satellites >= MobilUnit.Unit.MobilUnit.MinSatellites;

			return mu;
		}

		private static ConfirmPacket AnswerDatagram(byte messageType, int packetNumber, AnswerType answerType)
		{
			var data = new byte[]
							   {
								   0xAF,
								   0xFF,
								   0x09,
								   messageType,
								   (byte) (packetNumber & 0xFF),
								   (byte) ((packetNumber >> 8) & 0xFF),
								   (byte) answerType,
								   0, //CRC
								   0xFA
							   };

			data[7] = CalculateCRC(data, 2, 5);

			return new ConfirmPacket { Data = data };
		}

		private static byte CalculateCRC(byte[] data, int startIndex, int length)
		{
			byte crc = 0;
			for (int i = 0; i != length; i++)
			{
				int tmp = crc << 1;
				tmp += data[startIndex + i];
				crc = (byte) ((tmp & 0xFF) + (tmp >> 8));
			}
			return crc;
		}

		public override bool SupportData(byte[] data)
		{
			return data.Length > 2 &&
				   data[0] == 0xAF &&   //Начало пакета
				   data[1] == 0xFF &&   //Начало пакета
				   data[3] == 0x05 &&   //Тип пакета - 5 - пакет регистрации
				   data[4] == 0x00 &&   //Сквозной номер младший байт для первого пакета в TCP-потоке
				   data[5] == 0x00;     //Сквозной номер старший байт для первого пакета в TCP-потоке

		}
	}
}
