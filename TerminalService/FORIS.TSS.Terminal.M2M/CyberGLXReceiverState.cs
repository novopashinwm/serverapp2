﻿using System;

namespace FORIS.TSS.Terminal.M2M
{
    [Serializable]
    class CyberGLXReceiverState : ReceiverState
    {
        public int Firmware;
    }
}
