﻿namespace FORIS.TSS.Terminal.M2M
{
    public enum CyberGLXSensor
    {
        DI1 = 1,
        DI2 = 2,
        DI3 = 3,
        DI4 = 4,
        DI5 = 5,
        DI6 = 6,
        DI7 = 7,
        DI8 = 8,

        AI1 = 11,
        AI2 = 12,
        AI3 = 13,
        AI4 = 14,

        B1 = 21,
        B2 = 22,
        B3 = 23,
        B4 = 24,
        B5 = 25,
    }
}
