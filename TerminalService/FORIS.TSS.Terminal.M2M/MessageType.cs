﻿namespace FORIS.TSS.Terminal.M2M
{
    enum MessageType
    {
        /// <summary>
        /// Координатное сообщение, входящее
        /// </summary>
        Coordinates = 0,
        /// <summary> Еще одно координатное сообщение, отсутствует в документе протокола, но работает аналогично пакету 0 </summary>
        Coordinates01 = 1,
        /// <summary>
        /// Ответ о приеме пакета, исходящее
        /// </summary>
        Registration = 5,
        /// <summary>
        /// Проверка TCP-соединения, входящее
        /// </summary>
        IsAlive = 6,
        /// <summary>
        /// Синхронизация времени, исходящее
        /// </summary>
        Sync = 21
    }
}
