﻿using System.Text;
using FORIS.TSS.Communication;

namespace FORIS.TSS.Terminal.M2M
{
    class RegistrationDatagram : Datagram
    {
        public RegistrationDatagram(byte[] data)
        {
            bytes = data;
        }

        public string DeviceID
        {
            get
            {
                var result = new byte[16];

                for (var i = 0; i != 8; ++i)
                {
                    var b = bytes[i + 12];
                    result[i*2] = (byte) ((b >> 4) + '0');
                    result[i*2 + 1] = (byte) ((b & 0x0F) + '0');
                }

                return Encoding.ASCII.GetString(result);
            }
        }

        public int Firmware
        {
            get
            {
                return bytes[30] | (bytes[31] << 8);
            }
        }
    }
}
