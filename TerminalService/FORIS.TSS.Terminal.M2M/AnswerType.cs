﻿namespace FORIS.TSS.Terminal.M2M
{
    enum AnswerType
    {
        /// <summary>
        /// 0 - успешное выполнение												
        /// </summary>
        Success = 0,
        /// <summary>
        /// 1 - ошибка CRC												
        /// </summary>
        InvalidCRC = 1,
        /// <summary>
        /// 2 - неизвестная (ошибочная) команда, или неформатный пакет от прибора												
        /// </summary>
        UnknownCommand = 2,
        /// <summary>
        /// 3 - требование авторизации												
        /// </summary>
        AuthorizationRequired = 3,
        /// <summary>
        /// 4 - ошибка сервера		
        /// </summary>
        ServerError = 4,
        /// <summary>
        /// 5 - неверный параметр												
        /// </summary>
        InvalidParameter = 5,
        /// <summary>
        /// 6 - большое значение параметра												
        /// </summary>
        ParameterTooHigh = 6,
        /// <summary>
        /// 7 - текстовый ответ на команду												
        /// </summary>
        TextAnswer = 7,
        /// <summary>
        /// 8 - Такой блок прошивки уже имеется												
        /// </summary>
        FirmwareAlreadyExists = 8
    }
}
