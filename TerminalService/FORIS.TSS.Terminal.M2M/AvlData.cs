﻿using System.Collections.Generic;

namespace FORIS.TSS.Terminal.M2M
{
    class AvlData : CyberGLXDatagram
    {
        internal static AvlData Create(byte[] data)
        {
            return new AvlData
                             {
                                 bytes = data
                             };
        }

        public int Satellites
        {
            get
            {
                return bytes[12];
            }
        }

        public decimal Latitute
        {
            get
            {
                //градусы + минуты
                var result = BCDFromIndex(13) + 
                             (
                                 BCDFromIndex(14) +
                                 BCDFromIndex(15)*0.01m +
                                 BCDFromIndex(16)*0.0001m)/60m;
                return (bytes[22] & 0x01) != 0 ? result : -result;
            }
        }

        public decimal Longitude
        {
            get
            {
                var result = BCDFromByte((byte) (bytes[17] & 0x0F))*100m +
                             BCDFromIndex(18) +
                             (BCDFromIndex(19) +
                              BCDFromIndex(20)*0.01m +
                              BCDFromIndex(21)*0.0001m)/60m;
                return (bytes[22] & 0x02) == 0 ? result : -result;
            }
        }

        public bool CorrectGPS
        {
            get
            {
                return (bytes[22] & 0x04) != 0;
            }
        }

        public decimal Speed
        {
            get
            {
                var speedKmhX10 = BCDFromIndex(23)*100 + BCDFromIndex(24);
                return speedKmhX10/10m;
            }
        }

        public decimal Course
        {
            get
            {
                return BCDFromIndex(25) * 10m +
                       BCDFromIndex(26) * 0.1m;
            }
        }

        /// <summary> Пробег в км </summary>
        /// <remarks>Виртуальный одометер на основании данных GPS</remarks>
        public decimal Odometer
        {
            get
            {
                return BCDFromIndex(27)*1000m +
                       BCDFromIndex(28)*10m +
                       BCDFromIndex(29)*0.1m;
            }
        }

        public int? V1
        {
            get
            {
                //if ((bytes[22] & 0x10) == 0)
                //    return null;

                if ((bytes[17] & 0x10) == 0)
                    return null;

                var bin = (bytes[35] << 8) | bytes[34];
                return ConvertBinToMillivolt(bin);
            }
        }

        private static int ConvertBinToMillivolt(int analogValue)
        {
            return analogValue; // *12000 / 0xffff;
        }

        public int? V2
        {
            get
            {
                //if ((bytes[22] & 0x10) == 0)
                //    return null;

                if ((bytes[17] & 0x20) == 0)
                    return null;

                var bin = (bytes[37] << 8) | bytes[36];
                return ConvertBinToMillivolt(bin);
            }
        }

        public int? V3
        {
            get
            {
                //if ((bytes[22] & 0x10) == 0)
                //    return null;

                if ((bytes[17] & 0x40) == 0)
                    return null;

                var bin = (bytes[39] << 8) | bytes[38];
                return ConvertBinToMillivolt(bin);
            }
        }

        public int? V4
        {
            get
            {
                //Согласно протоколу (bytes[22] & 0x10) == 0) означает, что данные не передаются
                //Однако 
                //if ((bytes[22] & 0x10) == 0)
                //    return null;

                if ((bytes[17] & 0x80) == 0)
                    return null;

                var bin = (bytes[41] << 8) | bytes[40];
                return ConvertBinToMillivolt(bin);
            }
        }

        public byte? DigitalSensors
        {
            get
            {
                if (((bytes[22] >> 3) & 0x01) != 0)
                    return null;

                return bytes[30];
            }
        }

        /// <summary>
        /// Счетные входы
        /// </summary>
        /// <remarks>
        /// «Счетные входы»  -  переменный массив 3-х байтовых значений, bin-формат. 
        /// Счетным входам предшествует байт маски. 
        /// Например, если в байте маски наличия значений счётных входов установлены нулевой, пятый и шестой биты, 
        /// это означает наличие в пакете данных от третьего, шестого и седьмого счётных входов 
        /// (входы нумеруются от одного до восьми, а биты от нуля до семи). 
        /// При этом далее в пакете только 9 байт. 
        /// Первые три из них должны быть интерпретированы, как показания первого счётного входа, 
        /// следующие, как показания шестого счётного входа, следующие, как седьмого.
        /// </remarks>
        public int?[] Counters
        {
            get
            {
                if ((bytes[22] & 0x20) == 0)
                    return null;

                var result = new int?[8];
                var maskByte = bytes[42];
                for (int i = 0,
                         mask = 0x1,
                         bytesIndex = 0;
                     i != 8;
                     ++i,
                     mask <<= 1)
                {
                    if ((maskByte & mask) == 0)
                        continue;
                    result[i] =
                        (bytes[43 + bytesIndex + 2] << 16) |
                        (bytes[43 + bytesIndex + 1] << 8) |
                        (bytes[43 + bytesIndex]);

                    bytesIndex += 3;
                }
                return result;
            }
        }

        public ushort PacketSource
        {
            get { return (ushort) ((bytes[32] << 8) + bytes[33]); }
        }
    }
}
