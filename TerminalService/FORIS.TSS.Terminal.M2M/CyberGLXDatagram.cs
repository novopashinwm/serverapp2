﻿using System;
using FORIS.TSS.Communication;

namespace FORIS.TSS.Terminal.M2M
{
    public abstract class CyberGLXDatagram : Datagram
    {
        public DateTime DateTime
        {
            get
            {
                var year = BCDFromIndex(8) + 2000;
                var month = BCDFromIndex(7);
                var day = BCDFromIndex(6);
                var hour = BCDFromIndex(9);
                var minute = BCDFromIndex(10);
                var second = BCDFromIndex(11);

                return new DateTime(year, month, day, hour, minute, second, DateTimeKind.Utc);
            }
        }

        protected int BCDFromIndex(int index)
        {
            var b = bytes[index];
            return BCDFromByte(b);
        }

        protected static int BCDFromByte(byte b)
        {
            return (b >> 4) * 10 + (b & 0x0F);
        }
    }
}
