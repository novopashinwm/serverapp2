﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using FORIS.TSS.TerminalService.Interfaces.Exceptions;

namespace RU.NVG.NIKA.Terminal.Galileo
{
	public class GalileoDevice : Device
	{
		internal const string ProtocolName = "Galileo";
		internal static class DeviceNames
		{
			internal const string Galileosky50              = "Galileosky v 5.0";
			internal const string Galileosky70              = "Galileosky 7.0";
			internal const string Galileosky70Lite          = "Galileosky 7.0 Lite";
			internal const string Galileosky70BaseBlockLite = "Galileosky Base Block Lite";
			internal const string GalileoskyObdII           = "Galileosky OBD-II";
		}

		public GalileoDevice()
			: base(typeof(GalileoSensor), new[]
			{
				DeviceNames.Galileosky50,
				DeviceNames.Galileosky70,
				DeviceNames.Galileosky70Lite,
				DeviceNames.Galileosky70BaseBlockLite,
				DeviceNames.GalileoskyObdII,
			})
		{
		}

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			var result = new ArrayList();
			if (count < 5)
			{
				bufferRest = data.Take(count).ToArray();
				return result;
			}

			var reader = new DataReader(data, 0, count);
			var receiverState = stateData as GalileoReceiverState;
			if (receiverState == null)
			{
				receiverState = new GalileoReceiverState();
				result.Add(new ReceiverStoreToStateMessage { StateData = receiverState });
			}

			var header = reader.ReadByte();
			if (header != 0x01)
			{
				bufferRest = null;
				return null;
			}

			var length = (int)(reader.ReadLittleEndian32(2) & 0x7fff);
			if (reader.RemainingBytesCount < length + 2)
			{
				reader.Rewind(1 + 2);
				bufferRest = reader.GetRemainingBytes();
				return result;
			}

			var calculatedCrc = CalculateCrc(data, length + 2 + 1);
			result.Add(
				new ConfirmPacket(
					new byte[]
						{
							0x02,
							(byte)  (calculatedCrc       & 0xff),
							(byte) ((calculatedCrc >> 8) & 0xff)
						}));

			reader.Skip(length);
			var receivedCrc = reader.ReadLittleEndian32(2);
			if (receivedCrc != calculatedCrc)
			{
				bufferRest = null;
				return result;
			}

			reader.Rewind(length + 2);

			MobilUnit mu = UnitReceived(ProtocolName);
			mu.SensorValues = new Dictionary<int, long>();
			mu.CorrectGPS = false;
			var single = true;
			while (2 < reader.RemainingBytesCount)
			{
				var tag = (GalileoTag)reader.ReadByte();
				switch (tag)
				{
					case GalileoTag.RecordNumber:
						mu = UnitReceived(ProtocolName);
						mu.SensorValues = new Dictionary<int, long>();
						mu.CorrectGPS = false;
						result.Add(mu);
						reader.Skip(2);
						single = false;
						break;
					case GalileoTag.Imei:
						receiverState.DeviceID = reader.ReadAsciiString(15);
						break;
					case GalileoTag.HardwareVersion:
						var hardwareVersion = reader.ReadByte().ToString(CultureInfo.InvariantCulture);
						if (receiverState.Firmware == null)
							receiverState.Firmware = hardwareVersion;
						else
							receiverState.Firmware = hardwareVersion + "." + receiverState.Firmware;
						break;
					case GalileoTag.SoftwareVersion:
						var softwareVersion = reader.ReadByte().ToString(CultureInfo.InvariantCulture);
						if (receiverState.Firmware == null)
							receiverState.Firmware = softwareVersion;
						else
							receiverState.Firmware = receiverState.Firmware + "." + softwareVersion;
						break;
					default:
						ReadTag(reader, tag, mu);
						break;
				}
			}

			if (single)
				result.Add(mu);

			foreach (var o in result)
			{
				var mobilUnit = o as MobilUnit;
				if (mobilUnit == null)
					continue;
				mobilUnit.DeviceID = receiverState.DeviceID;
				mobilUnit.FirmwareVersion = receiverState.Firmware;
			}

			reader.Skip(2);

			bufferRest = null;
			return result;
		}
		#region Команды трекера
		public override IList<object> ParseSms(IUnitInfo ui, string text)
		{
			if (text.ToLowerInvariant().StartsWith("Reset".ToLowerInvariant()))
				return new List<object> { CommandCompleted(ui, CmdResult.Completed, CmdType.ReloadDevice) };
			if (text.ToLowerInvariant().StartsWith("WRPERIOD".ToLowerInvariant()))
				return new List<object> { CommandCompleted(ui, CmdResult.Completed, CmdType.SetInterval)  };

			if (text.ToLowerInvariant().StartsWith("FLASHARCHIVE".ToLowerInvariant()))
				return new List<object> { CommandCompleted(ui, CmdResult.Completed, CmdType.Setup)        };

			if (text.ToLowerInvariant().StartsWith("SERVERIP".ToLowerInvariant()))
				return new List<object> { CommandCompleted(ui, CmdResult.Completed, CmdType.Unspecified)  };

			if (text.ToLowerInvariant().StartsWith("GPRS".ToLowerInvariant()))
				return new List<object> { CommandCompleted(ui, CmdResult.Completed, CmdType.Unspecified)  };

			if (text.ToLowerInvariant().StartsWith("Phones".ToLowerInvariant()))
				return new List<object> { CommandCompleted(ui, CmdResult.Completed, CmdType.Unspecified)  };

			return base.ParseSms(ui, text);
		}
		private string GetPassword(IStdCommand command)
		{
			if (string.IsNullOrWhiteSpace(command?.Target?.Password))
			{
				switch (command?.Target?.DeviceType)
				{
					case DeviceNames.Galileosky50:
					case DeviceNames.Galileosky70:
					case DeviceNames.Galileosky70Lite:
					case DeviceNames.Galileosky70BaseBlockLite:
					case DeviceNames.GalileoskyObdII:
					default:
						return "1234";
				}
			}
			return command?.Target?.Password;
		}
		public override List<CommandStep> GetCommandSteps(IStdCommand command)
		{
			switch (command.Type)
			{
				case CmdType.Setup:
					return GetSetupCommandSteps(command);
				case CmdType.SetInterval:
					return GetSetIntervalCommandSteps(command);
				case CmdType.ReloadDevice:
					return GetReloadDeviceCommandSteps(command);
				default:
					return base.GetCommandSteps(command);
			}
		}
		private List<CommandStep> GetSetupCommandSteps(IStdCommand command)
		{
			if (CmdType.Setup != command.Type)
				throw new CommandSendingFailureException(CmdResult.Failed);

			var devPsw = GetPassword(command);
			var apnCfg = GetInternetApnConfig(command);
			var srvAdd = Manager.GetConstant(Constant.ServerIP);
			var srvPrt = Manager.GetConstant(Constant.ServerPort);

			switch (command.Target.DeviceType)
			{
				case DeviceNames.Galileosky50:
				case DeviceNames.Galileosky70:
				case DeviceNames.Galileosky70Lite:
				case DeviceNames.Galileosky70BaseBlockLite:
				case DeviceNames.GalileoskyObdII:
				default:
					return new[]
					{
						new { Wait = CmdType.Unspecified, Text = $@"AddPhone {devPsw}"                             },
						new { Wait = CmdType.Unspecified, Text = $@"APN {apnCfg.Name},{apnCfg.User},{apnCfg.Pass}" },
						new { Wait = CmdType.Unspecified, Text = $@"Serverip {srvAdd},{srvPrt}"                    },
						new { Wait = CmdType.SetInterval, Text = $@"WrPeriod 30,1800"                              },
						new { Wait = CmdType.Setup,       Text = $@"FLASHARCHIVE 1"                                },
					}
						.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait })
						.ToList<CommandStep>();
			}
		}
		private List<CommandStep> GetSetIntervalCommandSteps(IStdCommand command)
		{
			if (CmdType.SetInterval != command.Type)
				throw new CommandSendingFailureException(CmdResult.Failed);
			switch (command.Target.DeviceType)
			{
				case DeviceNames.Galileosky50:
				case DeviceNames.Galileosky70:
				case DeviceNames.Galileosky70Lite:
				case DeviceNames.Galileosky70BaseBlockLite:
				case DeviceNames.GalileoskyObdII:
				default:
					return new List<CommandStep>
					{
						new SmsCommandStep($@"WrPeriod 30,1800") { WaitAnswer = command.Type }
					};
			}
			
		}
		private List<CommandStep> GetReloadDeviceCommandSteps(IStdCommand command)
		{
			if (CmdType.ReloadDevice != command.Type)
				throw new CommandSendingFailureException(CmdResult.Failed);
			switch (command.Target.DeviceType)
			{
				case DeviceNames.Galileosky50:
				case DeviceNames.Galileosky70:
				case DeviceNames.Galileosky70Lite:
				case DeviceNames.Galileosky70BaseBlockLite:
				case DeviceNames.GalileoskyObdII:
				default:
					return new List<CommandStep>
					{
						new SmsCommandStep($@"Reset") { WaitAnswer = command.Type }
					};
			}
		}
		#endregion Команды трекера

		private void ReadTag(DataReader reader, GalileoTag tag, MobilUnit mu)
		{
			switch (tag)
			{
				case GalileoTag.HardwareVersion:
					var hardwareVersion = reader.ReadByte().ToString(CultureInfo.InvariantCulture);
					if (mu.FirmwareVersion == null)
						mu.FirmwareVersion = hardwareVersion;
					else
						mu.FirmwareVersion = hardwareVersion + "." + mu.FirmwareVersion;
					break;
				case GalileoTag.SoftwareVersion:
					var softwareVersion = reader.ReadByte().ToString(CultureInfo.InvariantCulture);
					if (mu.FirmwareVersion == null)
						mu.FirmwareVersion = softwareVersion;
					else
						mu.FirmwareVersion = mu.FirmwareVersion + "." + softwareVersion;
					break;
				case GalileoTag.Imei:
					mu.DeviceID = reader.ReadAsciiString(15);
					break;
				case GalileoTag.DeviceId:
					reader.Skip(2);
					break;
				case GalileoTag.RecordNumber:
					reader.Skip(2);
					break;
				case GalileoTag.DateTime:
					mu.Time = (int) reader.ReadLittleEndian32(4);
					break;
				case GalileoTag.Coordinates:
					var satellitesAndValidity = reader.ReadByte();
					mu.Satellites = satellitesAndValidity & 0x0f;
					var latitude  = (int)reader.ReadLittleEndian32(4);
					var longitude = (int) reader.ReadLittleEndian32(4);
					mu.Latitude   = (double) (latitude/1000000m);
					mu.Longitude  = (double) (longitude/1000000m);
					mu.CorrectGPS = (satellitesAndValidity & 0xf0) == 0 && (latitude != 0 || longitude != 0);
					break;
				case GalileoTag.SpeedAndCourse:
					mu.Speed  = (int)Math.Round(reader.ReadLittleEndian32(2)/10m);
					mu.Course = (int)Math.Round(reader.ReadLittleEndian32(2)/10m);
					break;
				case GalileoTag.Altitude:
					mu.Height = (int) reader.ReadLittleEndian32(2);
					break;
				case GalileoTag.HDOP:
					mu.SetSensorValue((int) GalileoSensor.HDOP, reader.ReadByte());
					break;
				case GalileoTag.DeviceStatus:
					ParseDeviceStatus(mu, reader.ReadLittleEndian32(2));
					break;
				case GalileoTag.PowerVoltage:
					ParseAnalogSensor(mu, GalileoSensor.PowerVoltage, reader.ReadLittleEndian32(2));
					break;
				case GalileoTag.BatteryVoltage:
					ParseAnalogSensor(mu, GalileoSensor.BatteryVoltage, reader.ReadLittleEndian32(2));
					break;
				case GalileoTag.DeviceTemperature:
					ParseAnalogSensor(mu, GalileoSensor.DeviceTemperature, reader.ReadByte());
					break;
				case GalileoTag.Acceleration:
					ParseAcceleration(mu, reader.ReadLittleEndian32(4));
					break;
				case GalileoTag.OutputStatus:
					ParseDigitalSensors(mu, GalileoSensor.OutputStatus0, reader.ReadLittleEndian32(2));
					break;
				case GalileoTag.InputStatus:
					ParseDigitalSensors(mu, GalileoSensor.InputStatus0, reader.ReadLittleEndian32(2));
					break;
				case GalileoTag.Input0:
				case GalileoTag.Input1:
				case GalileoTag.Input2:
				case GalileoTag.Input3:
					ParseAnalogSensor(mu,
						GalileoSensor.AnalogInput0 + (tag - GalileoTag.Input0),
						reader.ReadLittleEndian32(2));
					break;
				case GalileoTag.RS232_0:
				case GalileoTag.RS232_1:
					ParseAnalogSensor(mu, GalileoSensor.RS232_0 + (tag - GalileoTag.RS232_0), reader.ReadLittleEndian32(2));
					break;
				case GalileoTag.Temperature0:
				case GalileoTag.Temperature1:
				case GalileoTag.Temperature2:
				case GalileoTag.Temperature3:
				case GalileoTag.Temperature4:
				case GalileoTag.Temperature5:
				case GalileoTag.Temperature6:
				case GalileoTag.Temperature7:
					ParseTemperature(mu, reader.ReadLittleEndian32(2), GalileoSensor.Temperature0 + (tag - GalileoTag.Temperature0));
					break;
				case GalileoTag.iButton1ID:
					ParseAnalogSensor(mu, GalileoSensor.IButton1ID, reader.ReadLittleEndian32(4));
					break;
				case GalileoTag.CAN_A0:
					ParseCanA0(mu, reader.ReadLittleEndian32(4));
					break;
				case GalileoTag.CAN_A1:
					ParseCanA1(mu, reader.ReadLittleEndian32(4));
					break;
				case GalileoTag.CAN_B0:
					ParseCanB0(mu, reader.ReadLittleEndian32(4));
					break;
				case GalileoTag.CAN_B1:
					reader.Skip(4);
					break;
				case GalileoTag.CAN8BITR0 :
				case GalileoTag.CAN8BITR1 :
				case GalileoTag.CAN8BITR2 :
				case GalileoTag.CAN8BITR3 :
				case GalileoTag.CAN8BITR4 :
				case GalileoTag.CAN8BITR5 :
				case GalileoTag.CAN8BITR6 :
				case GalileoTag.CAN8BITR7 :
				case GalileoTag.CAN8BITR8 :
				case GalileoTag.CAN8BITR9 :
				case GalileoTag.CAN8BITR10:
				case GalileoTag.CAN8BITR11:
				case GalileoTag.CAN8BITR12:
				case GalileoTag.CAN8BITR13:
				case GalileoTag.CAN8BITR14:
					reader.Skip(1);
					break;
				case GalileoTag.iButton2ID:
					ParseAnalogSensor(mu, GalileoSensor.IButton2ID, reader.ReadLittleEndian32(4));
					break;
				case GalileoTag.GPSRunMeters:
					ParseAnalogSensor(mu, GalileoSensor.GpsRunMeters, reader.ReadLittleEndian32(4));
					break;
				case GalileoTag.iButtonStates:
					reader.Skip(4);
					break;
				case GalileoTag.CAN16BITR0:
				case GalileoTag.CAN16BITR1:
				case GalileoTag.CAN16BITR2:
				case GalileoTag.CAN16BITR3:
				case GalileoTag.CAN16BITR4:
					ParseAnalogSensor(mu,
						GalileoSensor.Can16Bitr0 + (tag - GalileoTag.CAN16BITR0),
						reader.ReadLittleEndian32(2));

					break;
				case GalileoTag.CAN32BITR0:
				case GalileoTag.CAN32BITR1:
				case GalileoTag.CAN32BITR2:
				case GalileoTag.CAN32BITR3:
					ParseAnalogSensor(mu,
						GalileoSensor.Can32Bitr0 + (tag - GalileoTag.CAN32BITR0),
						reader.ReadLittleEndian32(4));
					break;
				case GalileoTag.Input4:
				case GalileoTag.Input5:
				case GalileoTag.Input6:
				case GalileoTag.Input7:
					ParseAnalogSensor(mu,
						GalileoSensor.AnalogInput4 + (tag - GalileoTag.Input4),
						reader.ReadLittleEndian32(2));
					break;
				case GalileoTag.SensorDS1923_0:
				case GalileoTag.SensorDS1923_1:
				case GalileoTag.SensorDS1923_2:
				case GalileoTag.SensorDS1923_3:
				case GalileoTag.SensorDS1923_4:
				case GalileoTag.SensorDS1923_5:
				case GalileoTag.SensorDS1923_6:
				case GalileoTag.SensorDS1923_7:
					//TODO: реализовать по запросу от клиента
					reader.Skip(3);
					break;
				case GalileoTag.RS485_0:
				case GalileoTag.RS485_1:
				case GalileoTag.RS485_2:
					ParseAnalogSensor(mu,
						GalileoSensor.RS485LLS0 + (tag - GalileoTag.RS485_0),
						reader.ReadLittleEndian32(2));
					break;
				case GalileoTag.RS485_3 :
				case GalileoTag.RS485_4 :
				case GalileoTag.RS485_5 :
				case GalileoTag.RS485_6 :
				case GalileoTag.RS485_7 :
				case GalileoTag.RS485_8 :
				case GalileoTag.RS485_9 :
				case GalileoTag.RS485_10:
				case GalileoTag.RS485_11:
				case GalileoTag.RS485_12:
				case GalileoTag.RS485_13:
				case GalileoTag.RS485_14:
				case GalileoTag.RS485_15:
					//Младшие 2 байта: без знаковое целое, относительный уровень топлива.
					ParseAnalogSensor(mu,
						GalileoSensor.RS485LLS3 + (tag - GalileoTag.RS485_3),
						reader.ReadLittleEndian32(2));
					//Старший байт: целое со знаком, температура, С
					ParseAnalogSensor(mu,
						GalileoSensor.RS485LTS3 + (tag - GalileoTag.RS485_3),
						(sbyte)reader.ReadByte());
					reader.Skip(3);
					break;
				case GalileoTag.LLS_T_R232_0:
					reader.Skip();
					break;
				case GalileoTag.LLS_T_485_0:
				case GalileoTag.LLS_T_485_1:
				case GalileoTag.LLS_T_485_2:
					ParseAnalogSensor(mu,
						GalileoSensor.RS485LTS3 + (tag - GalileoTag.LLS_T_485_0),
						(sbyte)reader.ReadByte());
					break;
				case GalileoTag.CAN8BITR15:
				case GalileoTag.CAN8BITR16:
				case GalileoTag.CAN8BITR17:
				case GalileoTag.CAN8BITR18:
				case GalileoTag.CAN8BITR19:
				case GalileoTag.CAN8BITR20:
				case GalileoTag.CAN8BITR21:
				case GalileoTag.CAN8BITR22:
				case GalileoTag.CAN8BITR23:
				case GalileoTag.CAN8BITR24:
				case GalileoTag.CAN8BITR25:
				case GalileoTag.CAN8BITR26:
				case GalileoTag.CAN8BITR27:
				case GalileoTag.CAN8BITR28:
				case GalileoTag.CAN8BITR29:
				case GalileoTag.CAN8BITR30:
					reader.Skip(1);
					break;
				case GalileoTag.CAN16BITR5 :
				case GalileoTag.CAN16BITR6 :
				case GalileoTag.CAN16BITR7 :
				case GalileoTag.CAN16BITR8 :
				case GalileoTag.CAN16BITR9 :
				case GalileoTag.CAN16BITR10:
				case GalileoTag.CAN16BITR11:
				case GalileoTag.CAN16BITR12:
				case GalileoTag.CAN16BITR13:
				case GalileoTag.CAN16BITR14:
					reader.Skip(2);
					break;
				case GalileoTag.CAN32BITR5 :
				case GalileoTag.CAN32BITR6 :
				case GalileoTag.CAN32BITR7 :
				case GalileoTag.CAN32BITR8 :
				case GalileoTag.CAN32BITR9 :
				case GalileoTag.CAN32BITR10:
				case GalileoTag.CAN32BITR11:
				case GalileoTag.CAN32BITR12:
				case GalileoTag.CAN32BITR13:
				case GalileoTag.CAN32BITR14:
					reader.Skip(4);
					break;
				case GalileoTag.ElectricitySupplyMeter:
					reader.Skip(4);
					break;
				case GalileoTag.Refrigerator:
					reader.Skip(4);
					break;
				case GalileoTag.EcoDrive:
					ParseAnalogSensor(mu, GalileoSensor.EcoDriveAcceleration, reader.ReadByte());
					ParseAnalogSensor(mu, GalileoSensor.EcoDriveBreaking, reader.ReadByte());
					ParseAnalogSensor(mu, GalileoSensor.EcoDriveTurningAcceleration, reader.ReadByte());
					ParseAnalogSensor(mu, GalileoSensor.EcoDriveBumpImpact, reader.ReadByte());
					break;
				case GalileoTag.PressurePro:
					reader.Skip(68);
					break;
				case GalileoTag.Dosimeter:
					reader.Skip(3);
					break;
				default:
					throw new ArgumentOutOfRangeException("tag", tag, "Unknown tag value");
			}
		}
		private void ParseTemperature(MobilUnit mu, uint data, GalileoSensor sensor)
		{
			//2 байта
			//Младший байт: беззнаковое целое, идентификатор.
			//Старший байт: целое со знаком, температура.
			//Например, получено: 01 10.
			//Идентификатор: 01.
			//Температура: 16.

			//var id = (byte)(data & 0xFF); //Этот параметр игнорируется, т.к. тэг не нужен

			sbyte value = (sbyte) ((data >> 8) & 0xff);

			mu.SensorValues[(int)sensor] = value;
		}
		private void ParseCanB0(MobilUnit mu, uint value)
		{
			mu.SensorValues[(int) GalileoSensor.CanRunMeters] = ((long) value)*5;
		}
		private void ParseCanA0(MobilUnit mu, uint value)
		{
			mu.SensorValues[(int) GalileoSensor.TotalFuelConsumption] = value/2;
		}
		private void ParseCanA1(MobilUnit mu, uint value)
		{
			mu.SensorValues[(int) GalileoSensor.FuelLevel] = (long) Math.Round((value & 0xff)*0.4);
			mu.SensorValues[(int) GalileoSensor.CoolantT] = ((int) ((value >> 8) & 0xff)) - 40;
			mu.SensorValues[(int) GalileoSensor.Revs] = (long) Math.Round(((value >> 16) & 0xffff)*0.125);
		}
		private void ParseAcceleration(MobilUnit mu, uint value)
		{
			for (var i = 0; i != 3; ++i)
			{
				mu.SensorValues[(int) (GalileoSensor.AccelerationX + i)] = (value >> (10*i)) & 0x3ff;
			}
		}
		private void ParseDigitalSensors(MobilUnit mu, GalileoSensor sensor, uint value)
		{
			for (var i = 0; i != 16; ++i)
			{
				mu.SensorValues[(int) (sensor + i)] = (value >> i) & 0x01;
			}
		}
		private void ParseAnalogSensor(MobilUnit mu, GalileoSensor sensor, long value)
		{
			mu.SensorValues[(int) sensor] = value;
		}
		private void ParseDeviceStatus(MobilUnit mu, uint value)
		{
			for (var sensor = GalileoSensor.VibrationMotion; sensor != GalileoSensor.SignalLevelQuality; ++sensor)
			{
				mu.SensorValues[(int)(sensor)] = (value >> (int)sensor) & 0x01;
			}

			mu.SensorValues[(int)(GalileoSensor.SignalLevelQuality)] = (value >> (int)GalileoSensor.SignalLevelQuality) & 0x03;

			for (var sensor = GalileoSensor.SignallingMode; sensor <= GalileoSensor.Alarm; ++sensor)
			{
				mu.SensorValues[(int)(sensor)] = (value >> (int)sensor) & 0x01;
			}
		}
		public override bool SupportData(byte[] data)
		{
			if (data.Length < 5)
				return false;

			if (data[0] != 0x01)
				return false;

			var reader = new DataReader(data);
			reader.Skip(1);
			var length = (int) (reader.ReadLittleEndian32(2) & 0x7f);

			if (reader.RemainingBytesCount != length + 2)
				return false;

			reader.Skip(length);

			//TODO: проверить контрольную сумму пакета
			var receivedCrc = reader.ReadLittleEndian32(2);
			var calculatedCrc = CalculateCrc(data, data.Length-2);

			return receivedCrc == calculatedCrc;
		}
		private uint CalculateCrc(byte[] data, int count)
		{
			byte uchCRCHi = 0xFF; /* high byte of CRC initialized */
			byte uchCRCLo = 0xFF; /* low byte of CRC initialized */
			uint uIndex;          /* will index into CRC lookup table */

			for (var i = 0; i != count; ++i)
			{
				uIndex = (uint) (uchCRCLo ^ data[i]); /* calculate the CRC */
				uchCRCLo = (byte) (uchCRCHi ^ AuchCRCHi[uIndex]);
				uchCRCHi = AuchCRCLo[uIndex];
			}
			return (uint) (uchCRCHi << 8 | uchCRCLo);
		}
		/* Table of CRC values for high–order byte */
		private static readonly byte[] AuchCRCHi =
		{
			0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
			0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
			0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01,
			0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
			0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81,
			0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
			0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01,
			0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
			0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
			0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
			0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01,
			0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
			0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
			0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
			0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01,
			0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
			0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
			0x40
		};
		/* Table of CRC values for low–order byte */
		private static readonly byte[] AuchCRCLo =
		{
			0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7, 0x05, 0xC5, 0xC4,
			0x04, 0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09,
			0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD,
			0x1D, 0x1C, 0xDC, 0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
			0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32, 0x36, 0xF6, 0xF7,
			0x37, 0xF5, 0x35, 0x34, 0xF4, 0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A,
			0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE,
			0x2E, 0x2F, 0xEF, 0x2D, 0xED, 0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
			0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 0x61, 0xA1, 0x63, 0xA3, 0xA2,
			0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F,
			0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68, 0x78, 0xB8, 0xB9, 0x79, 0xBB,
			0x7B, 0x7A, 0xBA, 0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
			0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0, 0x50, 0x90, 0x91,
			0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C,
			0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98, 0x88,
			0x48, 0x49, 0x89, 0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
			0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83, 0x41, 0x81, 0x80,
			0x40
		};
	}
}