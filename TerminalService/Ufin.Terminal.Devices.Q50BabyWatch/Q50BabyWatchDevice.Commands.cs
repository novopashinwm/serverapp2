﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.Common;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace Compass.Ufin.Terminal.Devices.Q50BabyWatch
{
	public partial class Q50BabyWatchDevice
	{
		private string GetPassword(IStdCommand command)
		{
			var password = command.Target.Password;
			if (string.IsNullOrEmpty(password))
				password = "123456";
			return password;
		}
		public override List<CommandStep> GetCommandSteps(IStdCommand command)
		{
			var devPsw = GetPassword(command);
			var apnCfg = GetInternetApnConfig(command);
			var srvAdd = Manager.GetConstant(Constant.ServerIP);
			var srvPrt = Manager.GetConstant(Constant.ServerPort);
			var mstPhn = ContactHelper.GetNormalizedPhone(command.Params[PARAMS.Keys.Phone] as string ?? command.Params[PARAMS.Keys.ReplyToPhone] as string);
			switch (command.Target.DeviceType)
			{
				case DeviceNames.FuturewayPetTrackerFP03:
				case DeviceNames.SentarT58:
				case DeviceNames.SentarQ60X:
				case DeviceNames.I365A12:
				case DeviceNames.I365A16:
				case DeviceNames.I365A18:
				case DeviceNames.I365A21:
				case DeviceNames.I365FA23:
				case DeviceNames.I365PetTracker:
					return new[]
					{
						new { Cmd = CmdType.Setup, Steps = new []
							{
								new { Wait = false, Text = $@"pw,{devPsw},apn,{apnCfg.Name},{apnCfg.User},{apnCfg.Pass}#", },
								new { Wait = false, Text = $@"pw,{devPsw},upload,60#",                                     },
								new { Wait = false, Text = $@"pw,{devPsw},lz,0,0#",                                        },
								new { Wait = false, Text = $@"pw,{devPsw},cr#",                                            },
								new { Wait = false, Text = $@"pw,{devPsw},ip,{srvAdd},{srvPrt}#",                          },
							}},
						new { Cmd = CmdType.SetInterval,  Steps = new [] { new { Wait = false, Text = $@"pw,{devPsw},upload,60#",            }, }, },
						new { Cmd = CmdType.Status,       Steps = new [] { new { Wait = false, Text = $@"pw,{devPsw},ts#",                   }, }, },
						new { Cmd = CmdType.SetSos,       Steps = new [] { new { Wait = false, Text = $@"pw,{devPsw},sos1,+79991112233#",    }, }, },
						new { Cmd = CmdType.DeleteSOS,    Steps = new [] { new { Wait = false, Text = $@"pw,{devPsw},sos1#",                 }, }, },
						new { Cmd = CmdType.AskPosition,  Steps = new [] { new { Wait = true,  Text = $@"pw,{devPsw},url#",                  }, }, },
						new { Cmd = CmdType.ReloadDevice, Steps = new [] { new { Wait = false, Text = $@"pw,{devPsw},reset#",                }, }, },
						new { Cmd = CmdType.Callback,     Steps = new [] { new { Wait = false, Text = $@"pw,{devPsw},monitor,+79991112233#", }, }, },
						new { Cmd = CmdType.FindDevice,   Steps = new [] { new { Wait = false, Text = $@"pw,{devPsw},find#",                 }, }, },
					}
						?.FirstOrDefault(x => x.Cmd == command.Type)
						?.Steps
						?.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						?.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						?.ToList<CommandStep>();
				case DeviceNames.SinoTrackST901:
				case DeviceNames.SinoTrackST901A:
					return new[]
					{
						new { Cmd = CmdType.Setup, Steps = new []
							{
								new { Wait = false, Text = $@"apn{devPsw} {apnCfg.Name} {apnCfg.User} {apnCfg.Pass}", },
								new { Wait = false, Text = $@"cqgps",                                                 },
								new { Wait = false, Text = $@"nslp{devPsw}",                                          },
								new { Wait = false, Text = $@"sossms{devPsw}",                                        },
								new { Wait = false, Text = $@"ip {srvAdd} {srvPrt}",                                  },
							}},
						new { Cmd = CmdType.Immobilize,    Steps = new [] { new { Wait = false, Text = $@"dy"               }, }, },
						new { Cmd = CmdType.Deimmobilize,  Steps = new [] { new { Wait = false, Text = $@"ky"               }, }, },
						new { Cmd = CmdType.Status,        Steps = new [] { new { Wait = false, Text = $@"cxzt"             }, }, },
						new { Cmd = CmdType.SetSos,        Steps = new [] { new { Wait = false, Text = $@"101#79991112233#" }, }, },
						new { Cmd = CmdType.DeleteSOS,     Steps = new [] { new { Wait = false, Text = $@"d101#"            }, }, },
						new { Cmd = CmdType.PowerAlarmOn,  Steps = new [] { new { Wait = false, Text = $@"pwrsms{devPsw},1" }, }, },
						new { Cmd = CmdType.PowerAlarmOff, Steps = new [] { new { Wait = false, Text = $@"pwrsms{devPsw},0" }, }, },
						new { Cmd = CmdType.ShakeAlarmOn,  Steps = new [] { new { Wait = false, Text = $@"125#"             }, }, },
						new { Cmd = CmdType.ShakeAlarmOff, Steps = new [] { new { Wait = false, Text = $@"126#"             }, }, },
						new { Cmd = CmdType.AskPosition,   Steps = new [] { new { Wait = true,  Text = $@"g1234"            }, }, },
						new { Cmd = CmdType.ReloadDevice,  Steps = new [] { new { Wait = false, Text = $@"cq"               }, }, },
					}
						?.FirstOrDefault(x => x.Cmd == command.Type)
						?.Steps
						?.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						?.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						?.ToList<CommandStep>();
				case DeviceNames.ReachfarRFV28:
				case DeviceNames.ReachfarRFV30:
				case DeviceNames.ReachfarRFV32:
				case DeviceNames.ReachfarRFV36:
				case DeviceNames.ReachfarRFV40:
				case DeviceNames.ReachfarRFV42:
				case DeviceNames.ReachfarRFV44:
				case DeviceNames.ReachfarRFV46:
					return new[]
					{
						new { Cmd = CmdType.Setup, Steps = new []
							{
								new { Wait = false, Text = $@"{devPsw},sos1,{mstPhn}#",                                },
								new { Wait = false, Text = $@"apn,{apnCfg.Name},user,{apnCfg.User},pd,{apnCfg.Pass}#", },
								new { Wait = false, Text = $@"ip,{srvAdd},{srvPrt}#",                                  },
								new { Wait = false, Text = $@"tim,60#",                                                },
								new { Wait = false, Text = $@"lag,2#",                                                 },
							}},
						new { Cmd = CmdType.SetInterval,   Steps = new [] { new { Wait = false, Text = $@"tim,60#" }, }, },
						new { Cmd = CmdType.Status,        Steps = new [] { new { Wait = false, Text = $@"dsp#"    }, }, },
						new { Cmd = CmdType.AskPosition,   Steps = new [] { new { Wait = true,  Text = $@"dw#"     }, }, },
						new { Cmd = CmdType.SetModeOnline, Steps = new [] { new { Wait = false, Text = $@"gon#"    }, }, },
						new { Cmd = CmdType.ReloadDevice,  Steps = new [] { new { Wait = false, Text = $@"reset#"  }, }, },
					}
						?.FirstOrDefault(x => x.Cmd == command.Type)
						?.Steps
						?.Where(i => !string.IsNullOrWhiteSpace(i.Text))
						?.Select(i => new SmsCommandStep(i.Text) { WaitAnswer = i.Wait ? command.Type : (CmdType?)null })
						?.ToList<CommandStep>();
				default:
					return base.GetCommandSteps(command);
			}
		}
		public override IList<object> ParseSms(IUnitInfo ui, string text)
		{
			switch (ui.DeviceType)
			{
				case DeviceNames.SinoTrackST901:
				case DeviceNames.SinoTrackST901A:
				case DeviceNames.FuturewayPetTrackerFP03:
				case DeviceNames.SentarT58:
				case DeviceNames.SentarQ60X:
				case DeviceNames.I365A12:
				case DeviceNames.I365A16:
				case DeviceNames.I365A18:
				case DeviceNames.I365A21:
				case DeviceNames.I365FA23:
				case DeviceNames.I365PetTracker:
					return base.ParseSms(ui, text);
				case DeviceNames.ReachfarRFV28:
				case DeviceNames.ReachfarRFV30:
				case DeviceNames.ReachfarRFV32:
				case DeviceNames.ReachfarRFV36:
				case DeviceNames.ReachfarRFV40:
				case DeviceNames.ReachfarRFV42:
				case DeviceNames.ReachfarRFV44:
				case DeviceNames.ReachfarRFV46:
					return base.ParseSms(ui, text);
				default:
					return base.ParseSms(ui, text);
			}
		}
		#region Методы разбора СМС
		private readonly Func<IUnitInfo, string, IList<object>> SinoTrackST901XParseAnswerSms4AskPosition = (ui, text) =>
		{
			var result = default(List<object>);
			var match = Regex.Match(text, string.Empty
				+ $@"^"
				+ $@"url:\s*?"
				+ $@"http://maps\.google\.com/maps\?q="
				+ $@"(?<LatS>[NS])(?<LatV>[^,]*?),"                          // Lat: [NS]dd.dddddd
				+ $@"(?<LngS>[EW])(?<LngV>[^\s]*?)\s*?"                      // Lng: [EW][d]dd.dddddd
				+ $@"Locate date:(?<Date>\d{{2,4}}-\d{{1,2}}-\d{{1,2}})\s*?" // Дата: 2019-3-7
				+ $@"Locate time:(?<Time>\d{{1,2}}:\d{{1,2}}:\d{{1,2}})"     // Время: 16:7:14
				,//+ $@"$",
				RegexOptions.Multiline | RegexOptions.Compiled);
			if (!(match?.Success ?? false))
				return result;
			result = new List<object>();
			var values = match
				?.Groups
				?.OfType<Group>()
				?.Skip(1)
				?.Select(g => g.Value)
				?.ToArray();
			if (null != values && 6 == values.Length)
			{
				var latS = values[0] == "N" ? +1 : -1;
				var latV = decimal.Parse(values[1], NumberStyles.Number, NumberHelper.FormatInfo);
				var lngS = values[2] == "E" ? +1 : -1;
				var lngV = decimal.Parse(values[3], NumberStyles.Number, NumberHelper.FormatInfo);

				var date = DateTime.ParseExact(values[4], "yyyy-M-d",
					CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal);
				var time = DateTime.ParseExact(values[5], "H:m:s",
					CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal);
				var mobilUnit = new MobilUnit(ui)
				{
					Time       = date.Add(time - time.Date).ToLogTime(),
					Latitude   = (double)(latS * latV),
					Longitude  = (double)(lngS * lngV),
					CorrectGPS = true,
				};
				result.Add(mobilUnit);
			}
			return result;
		};
		/// <summary> Регулярное выражение разбора строки ответа на команду AskPosition, для SinoTrackST901(A)  </summary>
		private static readonly Regex SinoTrackST901XFormatRegex = new Regex(string.Empty
			+ $@"^"
			+ $@"url:\s*?"
			+ $@"http://maps\.google\.com/maps\?q="
			+ $@"(?<LatS>[NS])(?<LatV>[^,]*?),"                          // Lat: [NS]dd.dddddd
			+ $@"(?<LngS>[EW])(?<LngV>[^\s]*?)\s*?"                      // Lng: [EW][d]dd.dddddd
			+ $@"Locate date:(?<Date>\d{{2,4}}-\d{{1,2}}-\d{{1,2}})\s*?" // Дата: 2019-3-7
			+ $@"Locate time:(?<Time>\d{{1,2}}:\d{{1,2}}:\d{{1,2}})"     // Время: 16:7:14
			,//+ $@"$",
			RegexOptions.Multiline | RegexOptions.Compiled);
		#endregion Методы разбора СМС
	}
}