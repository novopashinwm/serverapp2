﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace Compass.Ufin.Terminal.Devices.Q50BabyWatch
{
	public partial class Q50BabyWatchDevice : Device
	{
		internal static class DeviceNames
		{
			internal const string KidTrackerQ50X          = "Sentar Q50/Q50S";
			internal const string KidTrackerQ40X          = "Sentar Q40/Q40S";
			internal const string KidTrackerV80           = "Sentar V80";
			internal const string KidTrackerV81           = "Sentar V81";
			internal const string SentarT58               = "Sentar T58 (V82)";
			internal const string SentarQ60X              = "Sentar Q60/Q60S";
			internal const string SentarQ80               = "Sentar Q80";
			internal const string EFortuneDS99            = "MonkeyG DS99";
			internal const string EFortuneDeest69         = "MonkeyG Deest 69";
			internal const string FuturewayPetTrackerFP03 = "Futureway PetTracker FP03";
			internal const string SinoTrackST901          = "SinoTrack ST-901";
			internal const string SinoTrackST901A         = "SinoTrack ST-901A";
			// i365 Tech
			internal const string I365A12                 = "i365 A12";
			internal const string I365A16                 = "i365 A16";
			internal const string I365A18                 = "i365 A18";
			internal const string I365A21                 = "i365 A21";
			internal const string I365FA23                = "i365 FA23";
			internal const string I365PetTracker          = "i365 PetTracker";
			// Reachfar
			internal const string ReachfarRFV28          = "Reachfar RF-V28";
			internal const string ReachfarRFV30          = "Reachfar RF-V30";
			internal const string ReachfarRFV32          = "Reachfar RF-V32";
			internal const string ReachfarRFV36          = "Reachfar RF-V36";
			internal const string ReachfarRFV40          = "Reachfar RF-V40";
			internal const string ReachfarRFV42          = "Reachfar RF-V42";
			internal const string ReachfarRFV44          = "Reachfar RF-V44";
			internal const string ReachfarRFV46          = "Reachfar RF-V46";
		}
		public Q50BabyWatchDevice() : base(deviceTypeNames: new[]
		{
			DeviceNames.KidTrackerQ50X,
			DeviceNames.KidTrackerQ40X,
			DeviceNames.KidTrackerV80,
			DeviceNames.KidTrackerV81,
			DeviceNames.SentarT58,
			DeviceNames.SentarQ60X,
			DeviceNames.SentarQ80,
			DeviceNames.EFortuneDS99,
			DeviceNames.EFortuneDeest69,
			DeviceNames.FuturewayPetTrackerFP03,
			DeviceNames.SinoTrackST901,
			DeviceNames.SinoTrackST901A,
			DeviceNames.I365A12,
			DeviceNames.I365A16,
			DeviceNames.I365A18,
			DeviceNames.I365A21,
			DeviceNames.I365FA23,
			DeviceNames.I365PetTracker,
			DeviceNames.ReachfarRFV28,
			DeviceNames.ReachfarRFV30,
			DeviceNames.ReachfarRFV32,
			DeviceNames.ReachfarRFV36,
			DeviceNames.ReachfarRFV40,
			DeviceNames.ReachfarRFV42,
			DeviceNames.ReachfarRFV44,
			DeviceNames.ReachfarRFV46,
		})
		{
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			// Из реализации SupportData, любой входящий массив должен начинаться '[' и заканчиваться ']', но пакетов может быть несколько
			bufferRest = null;
			var result = new List<object>();
			var packets  = Encoding.ASCII.GetString(data)
				.Split(new[] { ']', '[' }, StringSplitOptions.RemoveEmptyEntries);
			foreach (var packet in packets)
			{
				var packetString = $@"[{packet}]";
				var packetReader = new StringReader(packetString);
				packetReader.Skip('[');
				var header = packetReader.ReadString(2);
				packetReader.Skip('*');
				var deviceId = packetReader.ReadString(10);
				packetReader.Skip('*');
				var len = packetReader.ReadHexInt(4);
				packetReader.Skip('*');
				var packetContent = packetReader.ReadBefore(']');
				var packetContentReader = new SplittedStringReader(packetContent, ',');
				if (0 == packetContentReader.Length)
					continue;
				var command = packetContentReader.ReadString();
				switch (command)
				{
					// Команды на которые нужно отвечать
					case "LK":
					case "AL":
					case "AL_WCDMA":
					case "AL_LTE":
						result.Add(new ConfirmPacket(Encoding.ASCII.GetBytes($"[{header}*{deviceId}*{command.Length::X4}*{command}]")));
						break;
					// Команды для которых не нужно отвечать
					case "UD":
					case "UD2":
					case "UD3":
					case "UD_WCDMA":
					case "UD_LTE":
						IMobilUnit mu = ReadGpsData(packetContentReader);
						mu.DeviceID = deviceId;
						result.Add(mu);
						break;
					// Неизвестные команды трассируем и на всякий случай отвечаем
					default:
						$"{GetType()}.{nameof(OnData)} error: Command not supported yet\r\nData:\r\n{packetString}".TraceWarning();
						result.Add(new ConfirmPacket(Encoding.ASCII.GetBytes($"[{header}*{deviceId}*{command.Length::X4}*{command}]")));
						break;
				}
			}
			return result;
		}
		private IMobilUnit ReadGpsData(SplittedStringReader reader)
		{
			var mu = UnitReceived();

			var date = reader.ReadDate("ddMMyy");
			var time = reader.ReadTime("hhmmss");
			if (time == null)
				return null;
			mu.Time = TimeHelper.GetSecondsFromBase(date + time.Value);

			var validPosition = reader.ReadString();
			mu.CorrectGPS = validPosition == "A";
			mu.Latitude = (double)(reader.ReadDecimal() ?? 0M);
			if (reader.ReadString() == "S")
				mu.Latitude = -mu.Latitude;
			mu.Longitude = (double)(reader.ReadDecimal() ?? 0M);
			if (reader.ReadString() == "W")
				mu.Longitude = -mu.Longitude;
			mu.Speed      = reader.ReadDecimalAsNullableInt();
			mu.Course     = reader.ReadDecimalAsNullableInt();
			mu.Height     = reader.ReadDecimalAsNullableInt();
			mu.Satellites = reader.ReadInt();

			var gsmSignalLevel = reader.ReadInt();
			var batteryLevel   = reader.ReadInt();
			var stepsCount     = reader.ReadInt();
			var rollNumber     = reader.ReadInt();
			var terminalState  = reader.ReadHexInt();
			mu.SensorValues = new Dictionary<int, long>
			{
				{ (int)Q50BabyWatchSensor.BatteryLevel,   batteryLevel   },
				{ (int)Q50BabyWatchSensor.GsmSignalLevel, gsmSignalLevel },
			};
			//TODO: статусы, сеть и т.п.

			return mu;
		}
		public override bool SupportData(byte[] data)
		{
			if (data.Length < 30)
				return false;

			if (data[0] != '[')
				return false;

			if (data[3] != '*')
				return false;

			if (data[14] != '*')
				return false;

			//if (data[20] != 'L')
			//	return false;

			//if (data[21] != 'K')
			//	return false;

			if (data[data.Length - 1] != ']')
				return false;

			return true;
		}
	}
}