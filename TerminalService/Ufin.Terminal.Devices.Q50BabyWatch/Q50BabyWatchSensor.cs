﻿namespace Compass.Ufin.Terminal.Devices.Q50BabyWatch
{
	public enum Q50BabyWatchSensor
	{
		/// <summary>Уровень батареи, в процентах</summary>
		BatteryLevel   = 1,
		/// <summary> GSM signal level 0-100 </summary>
		GsmSignalLevel = 2,
	}
}