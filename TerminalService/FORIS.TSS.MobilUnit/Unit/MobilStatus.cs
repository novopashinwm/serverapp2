using System;
using System.Collections.Generic;
using System.Text;
using FORIS.TSS.MobilUnit;
using FORIS.TSS.TerminalService.Interfaces;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.Terminal
{
    /// <summary>
    /// ����� �������� ��� ��������� ��������
    /// </summary>
    [Serializable]
    public class MobilStatus : IMobilStatus
    {
        //
        private Status status; 


        public MobilStatus()
        { 
            this.status = Status.None;
        }

        public Status Status
        {
            get { return this.status; }
            set { this.status = value; }
        }

        public static void SetStatus(IMobilUnit mu, Status status)
        {
            if (status != Status.None)
                mu.Status |= status;
            else
                mu.Status = status;   
        }

        public static void UnSetStatus(IMobilUnit mu, Status status)
        {
            mu.Status &= ~status; 
        }

        public static void UnSetStatus(Status currentStatus, Status status)
        {
            currentStatus &= ~status;
        }  

        public static void CheckStatusParcingMoving(IMobilUnit mu)
        {
            // ���� ��� ���������
            if ((mu.Status & Status.Parcing) > 0 && mu.Speed > 0)
            {
                mu.Status |= Status.ParcingMoving;
            }
        }


    }

}
