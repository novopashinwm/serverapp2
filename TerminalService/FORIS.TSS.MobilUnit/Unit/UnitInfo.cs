﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.Terminal
{
	/// <summary> IUnitInfo std implementation </summary>
	[Serializable]
	public class UnitInfo : IUnitInfo
	{
		int iVID, iUnitNo;
		short storageId;
		string arbDeviceID;
		string sPhone = String.Empty;
		IPEndPoint ipAddr = new IPEndPoint(IPAddress.Any, 0);
		string appId = string.Empty;

		#region IUnitInfo Members

		public int Port { get; set; }
		public string Password { get; set; }

		public int Unique
		{
			get
			{
				return iVID;
			}
			set
			{
				iVID = value;
			}
		}

		public short StorageId
		{
			get { return storageId; }
			set { storageId = value; }
		}

		public int ID
		{
			get { return iUnitNo; }
			set { iUnitNo = value; }
		}

		public string DeviceType { get; set; }

		public string DeviceID
		{
			get
			{
				return arbDeviceID;
			}
			set
			{
				arbDeviceID = value;
			}
		}

		public string AppId
		{
			get
			{
				return appId;
			}
			set
			{
				appId = value;
			}
		}

		public string Telephone
		{
			get
			{
				return sPhone;
			}
			set
			{
				sPhone = value != null ? String.Concat(value.Split(' ')) : "";
			}
		}

		public string IMSI { get; set; }

		public IPEndPoint IP
		{
			get
			{
				return ipAddr;
			}
			set
			{
				ipAddr = value;
			}
		}

		private int? _asidId;
		public int? AsidId
		{
			get { return _asidId; }
			set { _asidId = value; }
		}

		private string _asid;

		public string Asid
		{
			get { return _asid; }
			set { _asid = value; }
		}

		private string _simId;

		public string SimId
		{
			get { return _simId; }
			set { _simId = value; }
		}

		public DateTime? BlockingDate { get; set; }

		public bool StoreLog { get; set; }
		public bool AllowDeleteLog { get; set; }
		public Dictionary<SensorLegend, SensorMap[]> SensorMap { get; set; }
		public string FirmwareVersion { get; set; }

		#endregion

		static UnitInfo()
		{
			s_factory = UnitInfoFactory.Instance;
		}

		private UnitInfo()
		{
		}

		/// <summary> copy unit info </summary>
		/// <returns>copied object</returns>
		public object Clone()
		{
			UnitInfo ui = (UnitInfo)MemberwiseClone();
			if (arbDeviceID != null)
			{
				ui.DeviceID = arbDeviceID;
			}
			ui.IP = new IPEndPoint(ipAddr.Address, ipAddr.Port);
			return ui;
		}

		public override string ToString()
		{
			var sb = new StringBuilder(32);
			if (iVID != 0)
				sb.Append("VehicleID = " + iVID);
			if (iUnitNo != 0)
			{
				if (sb.Length != 0)
					sb.Append(", ");
				sb.Append("UnitNo = " + iUnitNo);
			}
			if (sPhone != "")
			{
				if (sb.Length != 0)
					sb.Append(", ");
				sb.Append("Phone = " + sPhone);
			}
			if (!string.IsNullOrWhiteSpace(DeviceType))
			{
				if (sb.Length != 0)
					sb.Append(", ");
				sb.Append("DeviceType = " + DeviceType);
			}

			return sb.ToString();
		}
		#region IComparable Members

		public int CompareTo(object obj)
		{
			IUnitInfo a = obj as IUnitInfo;
			if (a == null) throw new ArgumentException("Object not IUnitInfo or null");

			if (a.Unique != 0 && Unique != 0) return a.Unique.CompareTo(Unique);
			if (a.ID != 0 && ID != 0) return a.ID.CompareTo(ID);
			if (a.Telephone != "" && Telephone != "") return a.Telephone.CompareTo(Telephone);
			return -1;
		}

		#endregion

		public bool Empty
		{
			get
			{
				return Unique == 0 && ID == 0 && Telephone == "";
			}
		}

		#region Factory

		private static UnitInfoFactory s_factory;

		public static UnitInfoFactory Factory
		{
			get { return s_factory; }
		}

		public class UnitInfoFactory
		{
			private static UnitInfoFactory s_instance;

			public static UnitInfoFactory Instance
			{
				get { return s_instance; }
			}

			static UnitInfoFactory()
			{
				s_instance = new UnitInfoFactory();
			}

#if DEBUG
			private Dictionary<UnitInfoPurpose, List<Common.WeakReference<UnitInfo>>> storage;
			private Dictionary<UnitInfoPurpose, int> totalCreations;
			private System.Threading.Timer sweepTimer;
#endif
			private UnitInfoFactory()
			{
#if DEBUG
				this.storage = new Dictionary<UnitInfoPurpose, List<Common.WeakReference<UnitInfo>>>();
				this.totalCreations = new Dictionary<UnitInfoPurpose, int>();

				foreach (UnitInfoPurpose purpose in Enum.GetValues(typeof(UnitInfoPurpose)))
				{
					this.storage.Add(purpose, new List<Common.WeakReference<UnitInfo>>());
					this.totalCreations.Add(purpose, 0);
				}

				this.sweepTimer =
					new System.Threading.Timer(
						new System.Threading.TimerCallback(sweepTimer_Callback),
						null,
						TimeSpan.FromSeconds(60),
						TimeSpan.FromSeconds(60)
						);
#endif
			}
#if DEBUG
			~UnitInfoFactory()
			{
				this.sweepTimer.Dispose();
			}
#endif

#if DEBUG
			private void sweepTimer_Callback(object target)
			{
				lock (this.storage)
				{
					foreach (KeyValuePair<UnitInfoPurpose, List<Common.WeakReference<UnitInfo>>> entry in this.storage)
					{
						UnitInfoPurpose Purpose = entry.Key;
						List<Common.WeakReference<UnitInfo>> List = entry.Value;

						List<Common.WeakReference<UnitInfo>> Dead = new List<Common.WeakReference<UnitInfo>>(List.Count);

						foreach (Common.WeakReference<UnitInfo> reference in List)
						{
							UnitInfo ValueHolder = reference.Target;

							if (ValueHolder == null)
							{
								Dead.Add(reference);
							}
						}

						foreach (Common.WeakReference<UnitInfo> deadReference in Dead)
						{
							List.Remove(deadReference);
						}
					}
				}
			}
#endif

			public UnitInfo Create(UnitInfoPurpose purpose)
			{
				UnitInfo Value = new UnitInfo();
#if DEBUG
				lock (this.storage)
				{
					this.totalCreations[purpose]++;
					this.storage[purpose].Add(new Common.WeakReference<UnitInfo>(Value));
				}
#endif
				return Value;
			}

			[Obsolete()]
			public void Register(UnitInfo value, UnitInfoPurpose purpose)
			{
#if DEBUG
				/* Сначала поищем этот объект среди имеющихся,
				 * и удалим его регистрацию,
				 * а потом уже будем регистрировать согласно 
				 * указанной цели
				 */

				lock (this.storage)
				{
					foreach (List<Common.WeakReference<UnitInfo>> list in this.storage.Values)
					{
						Common.WeakReference<UnitInfo> ValueReference = null;

						foreach (Common.WeakReference<UnitInfo> reference in list)
						{
							UnitInfo ValueHolder = reference.Target;

							if (ValueHolder != null && ValueHolder == value)
							{
								ValueReference = reference;
							}
						}

						if (ValueReference != null)
						{
							list.Remove(ValueReference);
						}
					}

					this.storage[purpose].Add(new Common.WeakReference<UnitInfo>(value));
				}
#endif
			}
		}

		#endregion // Factory

	}
}