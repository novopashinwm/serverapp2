﻿using System;
using System.Collections.Generic;
using System.Net;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Config;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.MobilUnit.Unit
{
	/// <summary> Базовый класс для всех типов мобильных объектов </summary>
	[Serializable]
	public class MobilUnit : IMobilUnit
	{
		public static readonly double InvalidLongitude = Common.GeoHelper.InvalidLongitude;
		public static readonly double InvalidLatitude  = Common.GeoHelper.InvalidLatitude;

		/// <summary> Создать неполную копию текущего объекта  </summary>
		/// <returns>copied object</returns>
		/// <remarks>all ref members shared between copies</remarks>
		public object Clone()
		{
			return MemberwiseClone();
		}

		/// <summary> Радиус планеты Земля </summary>
		const double EarthRadius = 6367444.0;

		public static double DegreesToRadians = Math.PI/180f;
		public static double RadiansToDegrees = 180f/Math.PI;

		private readonly Dictionary<DeviceProperty, string> _properties = new Dictionary<DeviceProperty, string>();

		/// <summary> данные о позиции мобильного объекта </summary>
		protected UnitPosition position = new UnitPosition();

		/// <summary> данные о статусе мобильного объекта </summary>
		protected MobilStatus mobilStatus = new MobilStatus();
		/// <summary> данные о статусе мобильного объекта </summary>
		public Status Status
		{
			get { return mobilStatus.Status; }
			set { mobilStatus.Status = value; }
		}
		/// <summary> unit info </summary>
		protected IUnitInfo unitInfo = Terminal.UnitInfo.Factory.Create(UnitInfoPurpose.MobilUnit);
		/// <summary> подаваемое напряжение </summary>
		protected int powerVoltage = int.MaxValue;
		/// <summary> напряжение на аналоговом входе №1 </summary>
		protected int voltageAN1 = int.MaxValue;
		/// <summary> напряжение на аналоговом входе №2 </summary>
		protected int voltageAN2 = int.MaxValue;
		/// <summary> напряжение на аналоговом входе №3 </summary>
		protected int voltageAN3 = int.MaxValue;
		/// <summary> напряжение на аналоговом входе №4 </summary>
		protected int voltageAN4 = int.MaxValue;
		/// <summary> ID терминала, с помощью которого получена информация о мобильном устройстве </summary>
		protected int termID;
		/// <summary> satellites qty </summary>
		int satellites = -1;
		/// <summary> firmware </summary>
		string firmware;
		/// <summary> command type </summary>
		protected CmdType cmdtype = CmdType.Unspecified;
		/// <summary> Максимальная скорость, при которой GPS данные считаются некорректными. </summary>
		/// <remarks>Смотри сюда <see cref="ValidPosition"/></remarks>
		public static int GPSLimitMaxSpeed = 2000;
		/// <summary> Минимально допустимое кол-во спутников, при котором GPS-позиция считается корректной </summary>
		public static int MinSatellites = 3;
		/// <summary> Данные о позиции мобильного объекта </summary>
		public UnitPosition Position
		{
			get { return position; }
		}
		public PositionType Type
		{
			get { return position.Type; }
			set { position.Type = value; }
		}
		public IUnitInfo UnitInfo
		{
			get
			{
				return unitInfo;
			}
		}
		/// <summary> Unique ID (VEHICLE_ID) </summary>
		public int Unique
		{
			get { return unitInfo.Unique; }
			set { unitInfo.Unique = value; }
		}
		/// <summary> Unit No </summary>
		public int ID
		{
			get { return unitInfo.ID; }
			set { unitInfo.ID = value; }
		}
		public string DeviceType
		{
			get { return unitInfo.DeviceType; }
			set { unitInfo.DeviceType = value; }
		}
		/// <summary> Идентификатор устройства </summary>
		public string DeviceID
		{
			get { return unitInfo.DeviceID; }
			set { unitInfo.DeviceID=value; }
		}
		public string AppId
		{
			get { return unitInfo.AppId; }
			set { unitInfo.AppId = value; }
		}
		/// <summary> Номер мобильного устройства </summary>
		public string Telephone
		{
			get { return unitInfo.Telephone; }
			set
			{
				unitInfo.Telephone = string.IsNullOrWhiteSpace(value) ? null : string.Concat(value.Split(' ', '-'));
			}
		}
		public string IMSI { get; set; }
		/// <summary> IP адрес мобильного объекта </summary>
		public IPEndPoint IP
		{
			get { return unitInfo.IP;  }
			set { unitInfo.IP = value; }
		}
		public int Port
		{
			get { return unitInfo.Port;  }
			set { unitInfo.Port = value; }
		}
		public string Asid
		{
			get { return unitInfo.Asid;  }
			set { unitInfo.Asid = value; }
		}
		public int? AsidId
		{
			get { return unitInfo.AsidId;  }
			set { unitInfo.AsidId = value; }
		}
		public string SimId
		{
			get { return unitInfo.SimId;  }
			set { unitInfo.SimId = value; }
		}
		public DateTime? BlockingDate { get; set; }
		public bool StoreLog
		{
			get { return unitInfo.StoreLog;  }
			set { unitInfo.StoreLog = value; }
		}
		public bool AllowDeleteLog
		{
			get { return unitInfo.AllowDeleteLog;  }
			set { unitInfo.AllowDeleteLog = value; }
		}
		public Dictionary<SensorLegend, SensorMap[]> SensorMap
		{
			get { return unitInfo.SensorMap;  }
			set { unitInfo.SensorMap = value; }
		}
		public string FirmwareVersion
		{
			get { return unitInfo.FirmwareVersion;  }
			set { unitInfo.FirmwareVersion = value; }
		}
		/// <summary> Идентификатор хранилища </summary>
		public short StorageId
		{
			get { return unitInfo.StorageId;  }
			set { unitInfo.StorageId = value; }
		}
		/// <summary> Время регистрации параметров мобильного объекта </summary>
		/// <remarks> Имеется в виду локальное время трекера, синхронизированное с мировым по GPS.</remarks>
		public int Time { get; set; }
		private int? _correctTime;
		/// <summary> Время поступления последней корректной позиции </summary>
		public int CorrectTime
		{
			get
			{
				if (_correctTime.HasValue)
					return _correctTime.Value;
				return ValidPosition ? Time : 0;
			}
			set { _correctTime = value; }
		}
		/// <summary> Географическая широта мобильного объекта (градусы) </summary>
		public double Latitude
		{
			get { return position.Latitude;  }
			set { position.Latitude = value; }
		}
		/// <summary> Географическая долгота мобильного объекта (градусы) </summary>
		public double Longitude
		{
			get { return position.Longitude;  }
			set { position.Longitude = value; }
		}
		/// <summary> Скорость мобильного объекта (км/ч) </summary>
		public int? Speed
		{
			get { return position.Speed;  }
			set { position.Speed = value; }
		}
		/// <summary> Курс мобильного объекта (градусы) </summary>
		public int? Course
		{
			get { return position.Course;  }
			set { position.Course = value; }
		}
		/// <summary> Высота (в сантиметрах) </summary>
		public int? Height
		{
			get { return position.Height;  }
			set { position.Height = value; }
		}
		/// <summary> Значения всех датчиков. ключ-номер датчика, значение - значение показания датчика </summary>
		public Dictionary<int, long> SensorValues { get; set; }
		public void SetSensorValue(int key, long value)
		{
			if (SensorValues == null)
				SensorValues = new Dictionary<int, long>();
			SensorValues[key] = value;
		}
		public void SetSensorValue(int key, long? value)
		{
			if (value.HasValue)
				SetSensorValue(key, value.Value);
			else
				SensorValues?.Remove(key);
		}
		public void SetSensorValue(int key, bool value)
		{
			SetSensorValue(key, value ? 1 : 0);
		}
		public void SetSensorValue(int key, bool? value)
		{
			if (value.HasValue)
				SetSensorValue(key, value.Value);
			else
				SensorValues?.Remove(key);
		}
		/// <summary> Подаваемое напряжение </summary>
		public int PowerVoltage
		{
			get { return powerVoltage;  }
			set { powerVoltage = value; }
		}
		/// <summary> Напряжение на аналоговом входе №1 </summary>
		public int VoltageAN1
		{
			get { return voltageAN1;  }
			set { voltageAN1 = value; }
		}
		/// <summary> Напряжение на аналоговом входе №2 </summary>
		public int VoltageAN2
		{
			get { return voltageAN2;  }
			set { voltageAN2 = value; }
		}
		/// <summary> Напряжение на аналоговом входе №3 </summary>
		public int VoltageAN3
		{
			get { return voltageAN3;  }
			set { voltageAN3 = value; }
		}
		/// <summary> Напряжение на аналоговом входе №4 </summary>
		public int VoltageAN4
		{
			get { return voltageAN4;  }
			set { voltageAN4 = value; }
		}
		/// <summary> ID терминала, с помощью которого получена информация о мобильном устройстве </summary>
		public int TermID
		{
			get { return termID;  }
			set { termID = value; }
		}
		public Media Media { get; set; }
		protected int routeID = -1;
		public int RouteID
		{
			get { return routeID;  }
			set { routeID = value; }
		}
		protected string schedule;
		public string Schedule
		{
			get { return schedule;  }
			set { schedule = value; }
		}
		/// <summary> Количество спутников </summary>
		public int Satellites
		{
			get { return satellites;  }
			set { satellites = value; }
		}
		/// <summary> firmware </summary>
		public string Firmware
		{
			get { return firmware;  }
			set { firmware = value; }
		}
		protected int run;
		/// <summary> Пробег </summary>
		public int Run
		{
			get { return run;}
			set { run = value;}
		}
		/// <summary> Тип команды для определения ответа </summary>
		public CmdType cmdType
		{
			get { return cmdtype; }
			set { cmdtype = value; }
		}
		/// <summary> Отклонение от расписания по времени </summary>
		protected int timeDeviation;
		public int TimeDeviation
		{
			get { return timeDeviation; }
			set { timeDeviation = value; }
		}
		/// <summary> Отклонение от траектории движения </summary>
		protected int routeDeviation;
		public int RouteDeviation
		{
			get { return routeDeviation; }
			set { routeDeviation = value; }
		}
		protected int zoneID = -1;
		public int ZoneID
		{
			get { return zoneID; }
			set { zoneID = value; }
		}

		#region .ctor
		public MobilUnit()
		{
		}
		public MobilUnit(IUnitInfo unitInfo)
		{
			this.unitInfo = unitInfo;
		}
		/// <summary> kind of copy ctor </summary>
		/// <param name="mu">initializer</param>
		public MobilUnit(IMobilUnit mu)
		{
			cmdtype      = mu.cmdType;
			ID           = mu.ID;
			IP           = new IPEndPoint(mu.IP.Address, mu.IP.Port);
			powerVoltage = mu.PowerVoltage;
			Telephone    = mu.Telephone;
			Time         = mu.Time;
			Unique       = mu.Unique;
			voltageAN1   = mu.VoltageAN1;
			voltageAN2   = mu.VoltageAN2;
			voltageAN3   = mu.VoltageAN3;
			voltageAN4   = mu.VoltageAN4;
			position.SetPosition(mu);
			// TODO: временный код, необходимо доделать архитектуру мобильного объекта
			routeID      = mu.RouteID;
			zoneID       = mu.ZoneID;
			schedule     = mu.Schedule;
			firmware     = mu.Firmware;
			satellites   = mu.Satellites;
			termID       = mu.TermID;
			run          = mu.Run;
		}
		#endregion .ctor
		#region CalcCourse
		/// <summary> Вычисление курса мобильного объекта по 2м последним географическим точкам </summary>
		/// <param name="last"> Предпоследняя позиция </param>
		/// <returns> Курс мобильного объекта </returns>
		public int CalcCourse(IUnitPosition last)
		{
			return (position.Course = CalcCourse(last.Latitude, last.Longitude)).Value;
		}
		/// <summary> Вычисление курса мобильного объекта по 2м последним географическим точкам </summary>
		/// <param name="latitude"> Географическая широта предпоследней позиция </param>
		/// <param name="longitude"> Географическая долгота предпоследней позиция </param>
		/// <returns> Курса мобильного объекта </returns>
		public int CalcCourse(double latitude, double longitude)
		{
			if (!ValidPosition || !(latitude < 90f && longitude < 180f))
				return 0;

			double norm = SLClose(longitude, latitude, Longitude, Latitude);
			if (norm == 0.0f)
				return 0;
			double arg = EarthRadius * Math.Abs(DegreesToRadians * (Latitude - latitude)) / norm;
			if (arg < -1)
				arg = -1;
			else if (arg > 1)
				arg = +1;
			double arc = Math.Asin(arg);
			arc *= RadiansToDegrees; // (180.0 / Math.PI); // degree in first quadrant
			int signx = 1;
			if (Longitude - longitude < 0) signx = -1;
			int signy = 1;
			if (Latitude - latitude < 0) signy = -1;
			if (signx > 0 && signy > 0) arc = 90.0 - arc;
			if (signx > 0 && signy < 0) arc = 90.0 + arc;
			if (signx < 0 && signy < 0) arc = 270.0 - arc;
			if (signx < 0 && signy > 0) arc = 270.0 + arc;
			return Convert.ToInt32(arc); // ????
		}
		public double SLClose(double x1, double y1, double x2, double y2)
		{
			double dx = DegreesToRadians*(x2 - x1);
			double dy = DegreesToRadians*(y2 - y1);
			double xm = EarthRadius * Math.Cos(DegreesToRadians*(y1)) * dx;
			double ym = EarthRadius * dy;
			return Math.Sqrt(xm*xm + ym*ym);
		}
		#endregion CalcCourse
		public int? Radius
		{
			get { return position.Radius; }
			set { position.Radius = value; }
		}
		public decimal? GetSensorValue(SensorLegend legend)
		{
			if (SensorMap == null || SensorValues == null)
				return null;

			SensorMap[] maps;

			if (!SensorMap.TryGetValue(legend, out maps) || maps == null)
				return null;

			decimal? result = null;
			foreach (var map in maps)
			{
				long rawSensorValue;
				if (!SensorValues.TryGetValue(map.InputNumber, out rawSensorValue))
					continue;
				if (rawSensorValue < map.MinValue)
					continue;
				if (map.MaxValue < rawSensorValue)
					continue;
				if (result == null)
					result = 0m;
				result += map.Multiplier * rawSensorValue + map.Constant;
			}

			return result;
		}
		public CellNetworkRecord[] CellNetworks { get; set; }
		public WlanRecord[]        Wlans        { get; set; }
		public byte[]              RawBytes     { get; set; }
		public Dictionary<DeviceProperty, string> Properties { get { return _properties; } }

		/// <summary cref="ValidPosition"> Признак валидной (правильной) позиции </summary>
		/// <remarks>"Правильная" позиция должна соответствовать возможным диапазонам долготы и широты.</remarks>
		public bool ValidPosition
		{
			get
			{
				var hdopCheck = true;
				if (Type == PositionType.GPS && SensorMap != null && SensorMap.ContainsKey(SensorLegend.HDOP))
					hdopCheck = GetSensorValue(SensorLegend.HDOP) < 1.1m;

				// "правильная" широта и долгота
				//return position.ValidPosition && position.Speed < GPSLimitMaxSpeed;
				return
					hdopCheck              &&
					position.ValidPosition &&
					-090 <= Latitude  && Latitude  <= 090 &&
					-180 <= Longitude && Longitude <= 180 &&
					(position.Speed ?? 0) < GPSLimitMaxSpeed;
			}
		}

		/// <summary> Корректность позиции, при получении позиции это свойство лучше присваивать последним </summary>
		public bool CorrectGPS
		{
			get { return position.CorrectGPS; }
			set { position.CorrectGPS = value; }
		}

		/// <summary> Настройка ограничений для определения корректности позиций GPS </summary>
		public static void SetLimits()
		{
			// 1. кол-во спутников, которое считается корректным
			string s = GlobalsConfig.AppSettings["Satellite"];
			int minSatellites;
			MinSatellites = (int.TryParse(s, out minSatellites)) ? minSatellites : 4;

			// 2. максимально допустимая скорость, которая считается корректной
			s = GlobalsConfig.AppSettings["GPSLimitMaxSpeed"];
			if (s != null)
				GPSLimitMaxSpeed = int.Parse(GlobalsConfig.AppSettings["GPSLimitMaxSpeed"]);
		}

		#region IComparable Members

		public int CompareTo(object obj)
		{
			var a = obj as IMobilUnit;
			if(null == a)
				throw new ArgumentException("Object not IMobilUnit or null");

			var res = UnitInfo.CompareTo(a.UnitInfo);
			return 0 != res ? res : Time.CompareTo(a.Time);
		}

		#endregion

		public string Password { get; set; }

		public override string ToString()
		{
			return "Unique=" + Unique;
		}
	}
}