﻿using System;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.MobilUnit.Unit
{
	[Serializable]
	public class PictureUnit : MobilUnit, IPictureDevice
	{
		private byte[] _bytes;
		private string _url;
		private int    _cameraNumber;
		private int    _photoNumber;
		public int CameraNumber
		{
			get { return _cameraNumber; }
			set { _cameraNumber = value; }
		}
		public int PhotoNumber
		{
			get { return _photoNumber; }
			set { _photoNumber = value; }
		}
		public byte[] Bytes
		{
			get { return _bytes; }
			set { _bytes = value; }
		}
		public string Url
		{
			get { return _url; }
			set { _url = value; }
		}
	}
}