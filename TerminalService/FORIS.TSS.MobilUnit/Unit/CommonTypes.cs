﻿using System;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.CommonTypes;

namespace FORIS.TSS.Terminal
{
	/// <summary> Данные о позиции, скорости, курсе мобильного объекта </summary>
	[Serializable]
	public class UnitPosition : IUnitPosition
	{
		/// <summary> Географическое положение мобильного объекта </summary>
		/// <remarks> x-долгота, y-широта </remarks>
		protected DPoint point = new DPoint();
		/// <summary> Скорость (км/ч) </summary>
		/// <remarks> Speed in km/h. If tolerance is negative, it is unknown. </remarks>
		protected int? speed;
		/// <summary> Курс (градусы) </summary>
		/// <remarks> Course in degrees, 0=north. If tolerance is negative, it is unknown. </remarks>
		protected int? course;
		/// <summary> Высота над уровнем моря в см </summary>
		protected int? height;
		/// <summary> Время регистрации параметров мобильного объекта (в секундах) </summary>
		protected int time;

		protected IUnitInfo unitInfo =
			Terminal.UnitInfo.Factory.Create(UnitInfoPurpose.UnitPosition);

		/// <summary> Время регистрации параметров мобильного объекта </summary>
		public int Time
		{
			get { return time; }
			set { time = value; }
		}

		public int CorrectTime { get; set; }
		/// <summary> Географическая широта мобильного объекта (градусы) </summary>
		public double Latitude
		{
			get { return point.y; }
			set { point.y = value; }
		}
		/// <summary> Географическая долгота мобильного объекта (градусы) </summary>
		public double Longitude
		{
			get { return point.x; }
			set { point.x = value; }
		}
		/// <summary> Скорость мобильного объекта (км/ч) </summary>
		public int? Speed
		{
			get { return speed; }
			set { speed = value; }
		}
		/// <summary> Курс мобильного объекта (градусы) </summary>
		public int? Course
		{
			get { return course; }
			set { course = value; }
		}
		/// <summary> Высота (в сантиметрах) </summary>
		public int? Height
		{
			get { return height; }
			set { height = value; }
		}
		public int? Radius { get; set; }
		public PositionType Type { get; set; }
		public IUnitInfo UnitInfo
		{
			get
			{
				return unitInfo;
			}
		}
		public bool CorrectGPS { get; set; } = true;
		/// <summary> Признак валидной (правильной) позиции </summary>
		/// <remarks>"Правильная" позиция должна соответствовать возможным диапазонам долготы и широты.</remarks>
		public bool ValidPosition
		{
			get
			{
				// "правильная" широта и долгота
				return CorrectGPS;// point.y < 90f && point.x < 180f;
			}
		}
		public UnitPosition()
		{
		}
		public UnitPosition(IUnitPosition up)
		{
			SetPosition(up);
		}
		public void SetPosition(IUnitPosition up)
		{
			course = up.Course;
			point  = new DPoint(up.Longitude, up.Latitude);
			speed  = up.Speed;
			time   = up.Time;
		}
	}
}