﻿using FORIS.TSS.BusinessLogic.Enums;

namespace FORIS.TSS.Terminal
{
	/// <summary> Фабрика для создания команд </summary>
	public static class CommandCreator
	{
		public static IStdCommand CreateCommand(CmdType type)
		{
			switch (type)
			{
				default: return new StdCommand(type);
			}
		}
	}
}