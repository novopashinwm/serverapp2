﻿using System;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Interfaces;
using FORIS.TSS.Common;

namespace FORIS.TSS.Terminal
{
	/// <summary> command class </summary>
	[Serializable]
	class StdCommand : IStdCommand
	{
		/// <summary> command sender info </summary>
		IOperatorInfo sender = new OperatorInfo((int)ObjectID.Unknown, null, null);
		/// <summary> command target info </summary>
		private IUnitInfo target = UnitInfo.Factory.Create(UnitInfoPurpose.StdCommand);
		/// <summary> command type </summary>
		CmdType cmdtype = CmdType.Unspecified;
		/// <summary> parameter list to use with command </summary>
		PARAMS param = new PARAMS();
		public StdCommand()
		{
		}
		public StdCommand(CmdType type)
		{
			cmdtype = type;
		}
		#region IStdCommand Members

		/// <summary> info about sender </summary>
		public IOperatorInfo Sender
		{
			get
			{
				return sender;
			}
			set
			{
				if (sender.OperatorId != (int)ObjectID.Unknown) return;

				if (sender != value) valid = false;
				sender = value;
			}
		}
		public IUnitInfo Target
		{
			get
			{
				return target;
			}
			set
			{
				if (target != value) valid = false;
				target = value;
			}
		}
		public CmdType Type
		{
			get
			{
				return cmdtype;
			}
			set
			{
				if (cmdtype != value) valid = false;
				cmdtype = value;
			}
		}
		public Media MediaType
		{
			get { return mediaType; }
			set { mediaType = value; }
		}
		public PARAMS Params
		{
			get
			{
				return param;
			}
			set
			{
				if (param != value) valid = false;
				param = value;
			}
		}

		#endregion

		#region ICommand Members

		/// <summary> дополнительная информация о команде, зависящая от контекста </summary>
		protected object tag;
		/// <summary> текст команды </summary>
		protected string text;
		/// <summary> Дополнительная информация о команде, зависящая от контекста </summary>
		public virtual object Tag
		{
			get { return tag; }
			set { tag = value; }
		}
		/// <summary> Текст команды </summary>
		public virtual string Text
		{
			get { return text; }
			set { text = value; }
		}

		#endregion

		#region IGuarded Members

		IGuarded guarded;
		public IGuarded Checked
		{
			get
			{
				if (auto && !valid) Requery();
				return guarded;
			}
		}
		public object Unchecked
		{
			get
			{
				return this;
			}
		}
		public IContext Context
		{
			get
			{
				return ctx;
			}
		}

		bool auto;
		bool valid;
		public bool Auto
		{
			get
			{
				return auto;
			}
			set
			{
				auto = value;
			}
		}
		readonly IContext ctx = new CommandContext();
		private Media mediaType = Media.Unspecified;
		public IGuarded Requery()
		{
			valid = true;
			return guarded = ctx.Check(this);
		}

		#endregion

		/// <summary> make copy </summary>
		/// <returns> copy with shallow copy of Params and Sender </returns>
		public object Clone()
		{
			StdCommand cmd = (StdCommand)MemberwiseClone();
			cmd.target = (IUnitInfo)target.Clone();
			return cmd;
		}
		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendFormat("Sender = {4}, Type = {0}\nTarget: {1}\nTag = {2}, Text = {3}\n",
				cmdtype, target, tag, text, sender);
			if (param != null && param.Count != 0) sb.AppendFormat("with params:\n{0}", param);
			return sb.ToString();
		}
	}
}