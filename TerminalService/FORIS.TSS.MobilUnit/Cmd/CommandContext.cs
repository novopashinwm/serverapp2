﻿using System;
using System.Data;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;

namespace FORIS.TSS.Terminal
{
	[Serializable]
	class CommandContext : IContext
	{
		DataSet ds;

		public void Init(DataSet _ds)
		{
			ds = _ds;
		}

		public IGuarded Check(IGuarded obj)
		{
			if (ds == null)
			{
				$"Context dataset is empty"
					.CallTraceWarning();
				return obj;
			}

			IStdCommand cmd = obj as IStdCommand;
			if (cmd.Type != CmdType.Trace && cmd.Type != CmdType.GetLog) return cmd;

			// "switch" parameters for CmdType.Trace and CmdType.GetLog modified according to 
			// user per unit rights. only superusers (OPERATORGROUP_ID = 1) can use "force" 
			// values (-1 - force off, 2 - force on). for other users values changed to 
			// "request" (0 - off, 1 - on)
			DataRow[] rows = ds.Tables["OPERATORGROUP_OPERATOR"].Select(
				"OPERATORGROUP_ID < 3 and OPERATOR_ID = " + cmd.Sender.OperatorId.ToString());
			if (rows.Length > 0) return obj;

			int sw = (int)cmd.Params["switch"];
			cmd.Params["switch"] = sw == -1 ? 0 : sw == 2 ? 1 : sw;

			$"CommandContext.Check. \"switch\" changed: {sw} -> {cmd.Params["switch"]}"
				.CallTraceWarning();

			return cmd;
		}
	}
}