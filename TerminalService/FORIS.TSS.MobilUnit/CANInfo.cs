﻿using System;

namespace FORIS.TSS.MobilUnit.Unit
{
	/// <summary> Класс c информацией с CAN шины </summary>
	[Serializable]
	public class CANInfo
	{

		#region ICANInfo Members

		protected int _time;
		/// <summary>
		/// Время, когда данные были сняты с шины CAN (секунд от 01.01.1970)
		/// </summary>
		public int Time
		{
			get { return _time; }
			set { _time = value; }
		}

		protected byte? _speed;
		public byte? Speed
		{
			get { return _speed; }
			set { _speed = value; }
		}

		protected bool? _cruiseControl;
		public bool? CruiseControl
		{
			get
			{
				return _cruiseControl;
			}
			set
			{
				_cruiseControl = value;
			}
		}

		protected bool? _brake;
		public bool? Brake
		{
			get
			{
				return _brake;
			}
			set
			{
				_brake = value;
			}
		}

		protected bool? _parkingBrake;
		public bool? ParkingBrake
		{
			get
			{
				return _parkingBrake;
			}
			set
			{
				_parkingBrake = value;
			}
		}

		protected bool? _clutch;
		public bool? Clutch
		{
			get
			{
				return _clutch;
			}
			set
			{
				_clutch = value;
			}
		}

		protected byte? _accelerator;
		public byte? Accelerator
		{
			get
			{
				return _accelerator;
			}
			set
			{
				_accelerator = value;
			}
		}

		protected int? _fuelRate;
		public int? FuelRate
		{
			get
			{
				return _fuelRate;
			}
			set
			{
				_fuelRate = value;
			}
		}

		protected ushort? _fuelLevel1;
		public ushort? FuelLevel1
		{
			get
			{
				return _fuelLevel1;
			}
			set
			{
				_fuelLevel1 = value;
			}
		}

		protected ushort? _fuelLevel2;
		public ushort? FuelLevel2
		{
			get
			{
				return _fuelLevel2;
			}
			set
			{
				_fuelLevel2 = value;
			}
		}

		protected ushort? _fuelLevel3;
		public ushort? FuelLevel3
		{
			get
			{
				return _fuelLevel3;
			}
			set
			{
				_fuelLevel3 = value;
			}
		}

		protected ushort? _fuelLevel4;
		public ushort? FuelLevel4
		{
			get
			{
				return _fuelLevel4;
			}
			set
			{
				_fuelLevel4 = value;
			}
		}

		protected ushort? _fuelLevel5;
		public ushort? FuelLevel5
		{
			get
			{
				return _fuelLevel5;
			}
			set
			{
				_fuelLevel5 = value;
			}
		}

		protected ushort? _fuelLevel6;
		public ushort? FuelLevel6
		{
			get
			{
				return _fuelLevel6;
			}
			set
			{
				_fuelLevel6 = value;
			}
		}

		protected byte? _revs;
		public byte? Revs
		{
			get
			{
				return _revs;
			}
			set
			{
				_revs = value;
			}
		}

		protected ushort? _runToCarMaintenance;
		public ushort? RunToCarMaintenance
		{
			get
			{
				return _runToCarMaintenance;
			}
			set
			{
				_runToCarMaintenance = value;
			}
		}

		protected int? _engHours;
		public int? EngHours
		{
			get
			{
				return _engHours;
			}
			set
			{
				_engHours = value;
			}
		}

		protected short? _coolantT;
		public short? CoolantT
		{
			get
			{
				return _coolantT;
			}
			set
			{
				_coolantT = value;
			}
		}

		protected short? _engOilT;
		public short? EngOilT
		{
			get
			{
				return _engOilT;
			}
			set
			{
				_engOilT = value;
			}
		}

		protected short? _fuelT;
		public short? FuelT
		{
			get
			{
				return _fuelT;
			}
			set
			{
				_fuelT = value;
			}
		}

		private short? _pressurizationAirT;
		public short? PressurizationAirT
		{
			get { return _pressurizationAirT; }
			set { _pressurizationAirT = value; }
		}

		protected long? _totalRun;
		public long? TotalRun
		{
			get
			{
				return _totalRun;
			}
			set
			{
				_totalRun = value;
			}
		}


		protected uint? _dayRun;
		public uint? DayRun
		{
			get
			{
				return _dayRun;
			}
			set
			{
				_dayRun = value;
			}
		}

		protected int? _axleLoad1;
		public int? AxleLoad1
		{
			get
			{
				return _axleLoad1;
			}
			set
			{
				_axleLoad1 = value;
			}
		}

		protected int? _axleLoad2;
		public int? AxleLoad2
		{
			get
			{
				return _axleLoad2;
			}
			set
			{
				_axleLoad2 = value;
			}
		}

		protected int? _axleLoad3;
		public int? AxleLoad3
		{
			get
			{
				return _axleLoad3;
			}
			set
			{
				_axleLoad3 = value;
			}
		}

		protected int? _axleLoad4;
		public int? AxleLoad4
		{
			get
			{
				return _axleLoad4;
			}
			set
			{
				_axleLoad4 = value;
			}
		}


		protected int? _axleLoad5;
		public int? AxleLoad5
		{
			get
			{
				return _axleLoad5;
			}
			set
			{
				_axleLoad5 = value;
			}
		}

		protected int? _axleLoad6;
		public int? AxleLoad6
		{
			get
			{
				return _axleLoad6;
			}
			set
			{
				_axleLoad6 = value;
			}
		}

		private decimal? _airT;
		public decimal? AirT
		{
			get { return _airT; }
			set { _airT = value; }
		}

		#endregion
	}
}