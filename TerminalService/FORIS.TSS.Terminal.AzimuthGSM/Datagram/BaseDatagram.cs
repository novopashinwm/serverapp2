﻿using System;
using System.Collections;
using System.Linq;
using System.Text.RegularExpressions;

namespace FORIS.TSS.Terminal.AzimuthGSM.Datagram
{
	public abstract class BaseDatagram
	{
		public string DeviceId { get; protected set; }
		public string Command { get; protected set; }
		public string Packet { get; protected set; }
		/// <summary> Сообщение от устройства </summary>
		protected static readonly Regex MessageDeviceRegex          = new Regex(@"(?<=!).{1,}(?=\*)\*{1,}[A-Fa-f0-9]{2}", RegexOptions.Compiled);
		/// <summary> Данные заключенные между ! и * </summary>
		protected static readonly Regex DataDeviceRegex             = new Regex(@"(?<=!).{1,}(?=\*)",                     RegexOptions.Compiled);
		/// <summary> Получение контрольной суммы </summary>
		protected static readonly Regex ControlSummRegex            = new Regex(@"(?<=\*)[A-Fa-f0-9]{2}",                 RegexOptions.Compiled);
		/// <summary> Получение ID устройства и типа сообщения </summary>
		protected static readonly Regex DeviceIdAndCommandTypeRegex = new Regex(@"(?<=!)[A-Za-z0-9]{4},[0-9]{2}",         RegexOptions.Compiled);

		protected static readonly char[] CommadSplitter = new char[] { ',' };

		/// <summary> сообщение о подтверждении </summary>
		public string ConfirmMessage
		{
			get
			{
				var strDeviceId = Convert.ToInt32(DeviceId).ToString("X");
				while (strDeviceId.Length < 4)
					strDeviceId = "0" + strDeviceId;
				string message = strDeviceId + ",FF," + Command + "," + GetControlSumm(Packet);
				var ctSumm = CalculationControllSumm(message).ToString("X");
				if (ctSumm.Length == 1) ctSumm = "0" + ctSumm;
				message = "!" + message + "*" + ctSumm;
				return message;
			}
		}

		public static bool ChecklSum(string packet)
		{
			var isMatch = MessageDeviceRegex.IsMatch(packet);
			bool controllSummIsCorrect = false;
			if (isMatch)
			{
				var dataSumm = DataDeviceRegex.Match(packet).Value;
				var controllSumm = GetControlSumm(packet);
				controllSummIsCorrect = CalculationControllSumm(dataSumm) == Convert.ToInt32(controllSumm, 16);
			}
			return isMatch && controllSummIsCorrect;
		}

		public static BaseDatagram Init(string packet)
		{
			if (!ChecklSum(packet)) return null;
			var arDeviceANDCommand = DeviceIdAndCommandTypeRegex.Match(packet).Value.Split(CommadSplitter);
			BaseDatagram result = null;
			switch (arDeviceANDCommand[1])
			{
				case "03":
					result = new PositionDatagram(packet);
					break;
			}
			return result;
		}

		/// <summary> расчет контрольной суммы </summary>
		/// <param name="packet"></param>
		/// <returns></returns>
		public static int CalculationControllSumm(string packet)
		{
			byte result = packet.Select(chr => (byte)chr).Aggregate<byte, byte>(0, (current, b) => (byte)(current + b));
			return Convert.ToInt32(result.ToString("X"), 16);
		}

		/// <summary> получить контрольную сумму из пакета </summary>
		/// <param name="paket"></param>
		/// <returns></returns>
		static string GetControlSumm(string paket)
		{
			return ControlSummRegex.Match(paket).Value;
		}

		public abstract IList GetResponses();
	}
}