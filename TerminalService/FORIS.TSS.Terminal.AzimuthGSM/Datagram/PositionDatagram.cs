﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.AzimuthGSM.Datagram
{
	class PositionDatagram : BaseDatagram
	{

		/// <summary>
		/// получение отчетов
		/// </summary>
		private static readonly Regex ArrayReportsRegex = new Regex(@"(?<=![A-Fa-f0-9]{4},[0-9]{2},[A-Za-z0-9]{8},)[A-Za-z0-9,;]{1,}");
		private static readonly char[] ReportsSpliter = new[] { ';' };
		readonly string[] _arreports;

		/// <summary>
		/// Бит 10 – статус зажигания (Ignition) - 0 –подключено на “0”, 1 –подключено на ”1” = 32-10
		/// </summary>
		private const int IndIgnitionConnect = 22;
		/// <summary>
		/// Бит 11 – зажигание не подключено (IgnitionError) = 32-11
		/// </summary>
		private const int IndIgnitionOn = 21;

		private const int
			IndSensorIN1 = 16, IndSensorIN2 = 15, IndSensorIN3 = 14,
			IndSensorOUT1 = 19, IndSensorOUT2 = 18, IndSensorOUT3 = 17;

		public PositionDatagram(string packet)
		{
			var arDeviceANDCommand = DeviceIdAndCommandTypeRegex.Match(packet).Value.Split(CommadSplitter);
			Command = arDeviceANDCommand[1];
			Packet = packet;
			DeviceId = Convert.ToInt32(arDeviceANDCommand[0], 16).ToString();
			_arreports = ArrayReportsRegex.Match(packet).Value.Split(ReportsSpliter);
		}

		/// <summary>
		/// сформировать объект для отчета
		/// </summary>
		/// <param name="report"></param>
		/// <returns></returns>
		MobilUnit.Unit.MobilUnit CreateMobileUnit(string report)
		{
			var result = new MobilUnit.Unit.MobilUnit();
			var arReportParams = report.Split(CommadSplitter);
			//arReportParams[0] - биты флагов состояния
			result.DeviceID = DeviceId;
			result.cmdType = CmdType.Trace;
			result.Longitude = GetLatLng(arReportParams[2]);
			result.Latitude = GetLatLng(arReportParams[1]);
			var dateData = GetDateFromHEX(arReportParams[3]);
			result.Time = TimeHelper.GetSecondsFromBase(dateData);
			result.Properties.Add(DeviceProperty.Protocol, AzimuthGsmDevice.ProtocolName);

			//скорость
			result.Speed = (int)(Convert.ToInt32(arReportParams[4].Substring(0, 3), 16) * 1.853 * 0.1);
			//курс
			if (arReportParams[4].Contains("b"))
				result.Course = Convert.ToInt32(arReportParams[4].Substring(arReportParams[4].LastIndexOf("b", StringComparison.Ordinal), 2), 16) * 2;
			//количество спутников
			if (arReportParams[4].Contains("z"))
			{
				int startWith = arReportParams[4].LastIndexOf("z", StringComparison.Ordinal);
				result.Satellites =
					//количество GPS
					Convert.ToInt32(arReportParams[4].Substring(startWith + 1, 1), 16) +
					//глонасс
					Convert.ToInt32(arReportParams[4].Substring(startWith + 2, 1), 16);
			}
			//биты флагов состояний
			string bitsFlagStates = Convert.ToString(Convert.ToInt32(arReportParams[0], 16), 2);
			var sb = new StringBuilder();
			for (int i = bitsFlagStates.Length; i <= 31; i++) sb.Append("0");
			sb.Append(bitsFlagStates);
			bitsFlagStates = sb.ToString();

			//зажигание
			var ignitionConnect = bitsFlagStates[IndIgnitionConnect] == '1';
			var ignitionOn = bitsFlagStates[IndIgnitionOn] == '1';

			result.SensorValues = new Dictionary<int, long>
			{
				{(int) Sensors.IN1,  bitsFlagStates[IndSensorIN1] == '1' ? 1 : 0},
				{(int) Sensors.IN2,  bitsFlagStates[IndSensorIN2] == '1' ? 1 : 0},
				{(int) Sensors.IN3,  bitsFlagStates[IndSensorIN3] == '1' ? 1 : 0},
				{(int) Sensors.OUT1, bitsFlagStates[IndSensorOUT1] == '1' ? 1 : 0},
				{(int) Sensors.OUT2, bitsFlagStates[IndSensorOUT2] == '1' ? 1 : 0},
				{(int) Sensors.OUT3, bitsFlagStates[IndSensorOUT3] == '1' ? 1 : 0}
			};
			if (ignitionConnect)
				result.SensorValues.Add((int)Sensors.Ignition, ignitionOn ? 1 : 0);

			result.CorrectGPS = result.Satellites >= 3;
			return result;
		}

		/// <summary>
		/// получить долготу
		/// </summary>
		/// <param name="hexValue"></param>
		/// <returns></returns>
		static double GetLatLng(string hexValue)
		{
			var decValue = GetIntFromHEX(hexValue); //dddmmmmmmm
			int d = decValue / (int)Math.Pow(10, 7);
			var min = (decValue - d * (int)Math.Pow(10, 7)) / (decimal)(60 * Math.Pow(10, 5));
			return (double)(d + min);
		}



		/// <summary>
		/// преобразовать полученное выражение в 
		/// 16-ой системе в десятичную (старший бит числа определяет знак)
		/// </summary>
		/// <param name="hexValue"></param>
		/// <returns></returns>
		static int GetIntFromHEX(string hexValue)
		{
			var maxBit = false;
			string bitValue = Convert.ToString(Convert.ToInt32(hexValue, 16), 2);
			maxBit = bitValue.Length == 32 && bitValue[0] == '1';
			if (maxBit)
				bitValue = bitValue.Substring(1, bitValue.Length - 1);
			int result = Convert.ToInt32(bitValue, 2);
			if (maxBit) result = result * -1;
			return result;
		}

		/// <summary>
		/// получить время отчета
		/// взято из документации по устройству стр. 6
		/// </summary>
		/// <param name="hexValue"></param>
		/// <returns></returns>
		static DateTime GetDateFromHEX(string hexValue)
		{
			var j = Convert.ToInt32(hexValue, 16);
			var date = j / 86400;
			var t = j - (date * 86400);
			date += 731000;
			var y = (4 * date - 1) / 146097;
			var d = (4 * date - 1 - 146097 * y) / 4;
			date = (4 * d + 3) / 1461;
			d = (4 * d + 7 - 1461 * date) / 4;
			var m = (5 * d - 3) / 153;
			d = (5 * d + 2 - 153 * m) / 5;
			y = 100 * y + date;
			if (m < 10)
			{
				m += 3;
			}
			else
			{
				m -= 9;
				y = y + 1;
			}
			var sec = t % 60;
			t /= 60;
			var hour = t / 60;
			var minute = t % 60;
			return new DateTime(y, m, d, hour, minute, sec);
		}

		public override IList GetResponses()
		{
			var result = new ArrayList();
			result.AddRange(_arreports.Select(CreateMobileUnit).ToArray());
			result.Add(new ConfirmPacket(Encoding.ASCII.GetBytes(ConfirmMessage)));
			return result;
		}


	}
}