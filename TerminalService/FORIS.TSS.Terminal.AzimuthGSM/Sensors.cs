﻿namespace FORIS.TSS.Terminal.AzimuthGSM
{
	/// <summary> Сенсоры на уст-ве </summary>
	public enum Sensors
	{
		IN1      = 1,
		IN2      = 2,
		IN3      = 3,
		OUT1     = 4,
		OUT2     = 5,
		OUT3     = 6,
		Ignition = 7
	}
}