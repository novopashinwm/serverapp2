﻿namespace FORIS.TSS.Terminal.AzimuthGSM
{
	/// <summary> статус навигационного решения </summary>
	public enum NavagationStatus
	{
		/// <summary> координаты не определены </summary>
		CoordinatesNotDefined = 0,
		/// <summary> координаты определены, 2D решение </summary>
		CoordinatesDefined2D  = 1,
		/// <summary> координаты определены, 3D решение </summary>
		CoordinatesDefined3D  = 2
	}
}