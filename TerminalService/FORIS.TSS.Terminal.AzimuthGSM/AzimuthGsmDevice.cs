﻿using System.Collections;
using System.Text;

namespace FORIS.TSS.Terminal.AzimuthGSM
{
	/// <summary> Азимут </summary>
	public class AzimuthGsmDevice : Device
	{
		internal const string ProtocolName = "Azimuth GSM";
		internal static class DeviceNames
		{
			internal const string AzimuthGSM = "Azimuth GSM";
		}
		public AzimuthGsmDevice() : base(typeof(Sensors), new[]
		{
			DeviceNames.AzimuthGSM,
		})
		{
		}
		/// <summary> Проверка поддержки формата sample !1234,02,7B2D01DC,2715,00*E2 </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public override bool SupportData(byte[] data)
		{
			if (data == null)
				return false;
			string dataString = Encoding.ASCII.GetString(data);
			return Datagram.BaseDatagram.ChecklSum(dataString);
		}
		/// <summary> Обработка пришедших данных </summary>
		/// <param name="data"></param>
		/// <param name="count"></param>
		/// <param name="stateData"></param>
		/// <param name="bufferRest"></param>
		/// <returns></returns>
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			return DataProcessor.Process(data, out bufferRest);
		}
	}
}