﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Common
{
	public abstract class BaseDeviceTests<TDevice> where TDevice : class, IDevice, new()
	{
		private TDevice _device;
		protected TDevice Device
		{
			get
			{
				if (_device != null)
					return _device;
				_device = new TDevice
				{
					Manager = CreateTerminalManager()
				};
				return _device;
			}
		}
		protected virtual ITerminalManager CreateTerminalManager()
		{
			return new TerminalManager();
		}
		protected IDevice GetDevice(byte[] data)
		{
			var devices = Device.Manager.GetDevices(data);
			Assert.IsNotNull(devices);
			Assert.AreEqual(1, devices.Count, string.Empty
				+ $"Several devices supports the same data: "
				+ $"{StringHelper.SmartFormatBytes(data)}\n"
				+ $"{string.Join(", ", devices.Select(d => d.GetType().FullName))}");
			var device = devices.FirstOrDefault();
			Assert.IsNotNull(device);
			Assert.IsInstanceOfType(device, typeof(TDevice));
			return device;
		}
		protected void CheckSupportDataByASCII(string data)
		{
			CheckSupportDataByBytes(Encoding.ASCII.GetBytes(data));
		}
		protected void CheckSupportDataByHex(string data)
		{
			CheckSupportDataByBytes(data.HexStringToByteArray());
		}
		protected void CheckSupportDataByBytes(byte[] bytes)
		{
			Assert.AreEqual(typeof(TDevice), GetDevice(bytes).GetType());
			Assert.IsTrue(Device.SupportData(bytes));
		}
		protected IEnumerable<object> GetResults(params byte[][] packets)
		{
			if (packets == null || packets.Length == 0)
				yield break;

			var bufferRest = default(byte[]);
			var stateData  = default(object);
			foreach (var packet in packets)
			{
				var packetData = packet;
				if (bufferRest != null && bufferRest.Length != 0)
					packetData = bufferRest.Concat(packetData).ToArray();

				var results = Device.OnData(packetData, packetData.Length, stateData, out bufferRest);
				if (results == null)
					continue;

				var stateMess = results
					?.OfType<ReceiverStoreToStateMessage>()
					?.FirstOrDefault();
				if (stateMess != null)
					stateData = stateMess.StateData;

				foreach (var result in results.OfType<object>())
					yield return result;
			}
		}
		protected IEnumerable<IMobilUnit> GetMobilUnits(params byte[][] packets)
		{
			if (packets == null || packets.Length == 0)
				yield break;

			byte[] bufferRest = null;
			object stateData  = null;
			foreach (var packet in packets)
			{
				var data = packet;
				if (bufferRest != null && bufferRest.Length != 0)
					data = bufferRest.Concat(data).ToArray();

				var result = Device.OnData(data, data.Length, stateData, out bufferRest);

				if (result == null)
					throw new ArgumentOutOfRangeException("packets", packets, @"Value cannot be parsed");

				var storeToStateMessage = result.OfType<ReceiverStoreToStateMessage>().FirstOrDefault();
				if (storeToStateMessage != null)
					stateData = storeToStateMessage.StateData;

				foreach (var mobilUnit in result.OfType<IMobilUnit>())
					yield return mobilUnit;
			}
		}
		protected IEnumerable<IMobilUnit> GetMobilUnits(params string[] packets)
		{
			if (packets == null)
				yield break;

			var bytePackets = Array.ConvertAll(packets, p => Encoding.ASCII.GetBytes(p));

			foreach (var mu in GetMobilUnits(bytePackets))
				yield return mu;
		}
		protected IEnumerable<IMobilUnit> GetMobilUnitsByHex(params string[] packets)
		{
			if (packets == null)
				yield break;

			var bytePackets = Array.ConvertAll(packets, StringHelper.HexStringToByteArray);

			foreach (var mu in GetMobilUnits(bytePackets))
				yield return mu;
		}
		protected IList OnDataByBytes(byte[] data, object state = null)
		{
			byte[] bufferRest;
			return Device.OnData(data, data.Length, state, out bufferRest);
		}
		protected IList OnDataByHex(string data, object state = null)
		{
			return OnDataByBytes(data.HexStringToByteArray(), state);
		}
		protected IList OnDataByASCII(string data, object state = null)
		{
			return OnDataByBytes(Encoding.ASCII.GetBytes(data), state);
		}
	}
}