﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.TerminalService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RU.NVG.Protocol.Egts;
using RU.NVG.Protocol.Egts.Enums;
using RU.NVG.Protocol.Egts.Helpers;
using RU.NVG.Protocol.Egts.Packet;

namespace Compass.Ufin.Terminal.Tests.Protocols
{
	[TestClass]
	public class EgtsProtocolTests
	{
		private static readonly int[] Tids = new[]
		{
			8093,
			8094,
			8095,
			7946,
			8513,
			8588
		};
		[TestMethod]
		public void SerializeDeserialize()
		{
			//EGTS_SR_RECORD_RESPONSE
			CheckString("0100030B0010001786008D010000060000001801010003000100001411");
			//EGTS_SR_TERM_IDENTITY
			CheckString("0100010b00220098d301c9170099d3019911000001010114000100000002303030303435303500000000000000aee5");
		}
		[TestMethod]
		public void Bug1006281()
		{
			CheckString("01-00-00-0B-00-2C-00-FC-E2-01-D2-21-00-DE-F8-81-DC-72-02-00-02-02-10-15-00-A9-6B-EE-04-47-2C-E4-9D-12-06-19-35-00-00-00-00-00-00-00-00-00-11-06-00-1C-63-00-00-03-00-50-B0-01-00-00-0B-00-2C-00-FD-E2-01-94-21-00-DF-F8-81-DC-72-02-00-02-02-10-15-00-C8-6B-EE-04-47-2C-E4-9D-12-06-19-35-00-00-00-00-00-00-00-00-00-11-06-00-1C-63-00-00-03-00-DF-2B-01-00-00-0B-00-3D-00-FE-E2-01-F1-32-00-E0-F8-81-DC-72-02-00-02-02-10-15-00-C9-49-20-0A-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-11-06-00-1C-63-00-00-03-00-12-06-00-00-00-01-DC-0F-00-14-05-00-02-00-00-28-04");
		}
		[TestMethod]
		public void TestClient()
		{
			var uri = new Uri("egts://213.221.0.198:7023");
			using (var client = new EgtsClient(uri.Host, uri.Port))
			{
				var mu = new MobilUnit
				{
					DeviceID = "123456789012346",
					Time = TimeHelper.GetSecondsFromBase(DateTime.UtcNow),
					Latitude = 55.722674999999995,
					Longitude = 37.639274999999998,
					CorrectGPS = true,
					Speed = 260,
					Course = 320,
					Satellites = 4,
					Run = 11111,
					Height = 200
				};
				mu.SetSensorValue((int)EgtsSensor.DigitalInput1, 1);
				mu.SetSensorValue((int)EgtsSensor.DigitalInput2, 1);
				mu.SetSensorValue((int)EgtsSensor.DigitalInput3, 1);
				mu.SetSensorValue((int)EgtsSensor.DigitalInput4, 1);
				mu.SetSensorValue((int)EgtsSensor.DigitalInput5, 1);
				mu.SetSensorValue((int)EgtsSensor.DigitalInput6, 1);
				mu.SetSensorValue((int)EgtsSensor.DigitalInput7, 1);
				mu.SetSensorValue((int)EgtsSensor.DigitalInput8, 1);
				mu.SetSensorValue((int)EgtsSensor.Hdop, 10);
				mu.SetSensorValue((int)EgtsSensor.Pdop, 20);
				mu.SetSensorValue((int)EgtsSensor.Vdop, 30);
				mu.SetSensorValue((int)EgtsSensor.Ignition, 1);

				var posData = mu.ToSrPosData();
				var exPosData = mu.ToSrExPosData();

				var requestFrame = EgtsPtAppdata.Create();
				var i = 0;
				while (requestFrame.Add(Tids[i], posData) && requestFrame.Add(Tids[i], exPosData))
					i = ++i == 5 ? 0 : i;

				i = 0;
				while (i < 1)
				{
					client.Send(requestFrame);
					i ++;
				}

				Thread.Sleep(10000);
			}

			Thread.Sleep(10000);
		}
		private MobilUnit GetMobilUnit()
		{
			var mu = new MobilUnit()
			{
				Unique     = 123456,
				DeviceID   = "352093082309062",
				Time       = TimeHelper.GetSecondsFromBase(DateTime.UtcNow),
				Latitude   = 55.722674999999995,
				Longitude  = 37.639274999999998,
				CorrectGPS = true,
				Speed      = 260,
				Course     = 320,
				Satellites = 4,
				Run        = 11111,
				Height     = 200
			};
			// Заполняем протокол как собственный
			mu.Properties.Add(DeviceProperty.Protocol, EgtsHelper.ProtocolName);
			// Заполняем маппинг датчиков
			mu.SensorMap = new SensorMap[]
			{
				new SensorMap { SensorLegend = SensorLegend.ToEgtsDigitalSensor1, InputNumber = (int)EgtsSensor.DigitalInput1,  Multiplier = 1.0M, Constant = 0.0M, MinValue = 0,               MaxValue = 1 },
				new SensorMap { SensorLegend = SensorLegend.ToEgtsDigitalSensor2, InputNumber = (int)EgtsSensor.DigitalInput2,  Multiplier = 1.0M, Constant = 0.0M, MinValue = 0,               MaxValue = 1 },
				new SensorMap { SensorLegend = SensorLegend.ToEgtsDigitalSensor3, InputNumber = (int)EgtsSensor.DigitalInput3,  Multiplier = 1.0M, Constant = 0.0M, MinValue = 0,               MaxValue = 1 },
				new SensorMap { SensorLegend = SensorLegend.ToEgtsDigitalSensor4, InputNumber = (int)EgtsSensor.DigitalInput4,  Multiplier = 1.0M, Constant = 0.0M, MinValue = 0,               MaxValue = 1 },
				new SensorMap { SensorLegend = SensorLegend.ToEgtsDigitalSensor5, InputNumber = (int)EgtsSensor.DigitalInput5,  Multiplier = 1.0M, Constant = 0.0M, MinValue = 0,               MaxValue = 1 },
				new SensorMap { SensorLegend = SensorLegend.ToEgtsDigitalSensor6, InputNumber = (int)EgtsSensor.DigitalInput6,  Multiplier = 1.0M, Constant = 0.0M, MinValue = 0,               MaxValue = 1 },
				new SensorMap { SensorLegend = SensorLegend.ToEgtsDigitalSensor7, InputNumber = (int)EgtsSensor.DigitalInput7,  Multiplier = 1.0M, Constant = 0.0M, MinValue = 0,               MaxValue = 1 },
				new SensorMap { SensorLegend = SensorLegend.ToEgtsDigitalSensor8, InputNumber = (int)EgtsSensor.DigitalInput8,  Multiplier = 1.0M, Constant = 0.0M, MinValue = 0,               MaxValue = 1 },
				new SensorMap { SensorLegend = SensorLegend.ToEgtsHDOP,           InputNumber = (int)EgtsSensor.Hdop,           Multiplier = 1.0M, Constant = 0.0M, MinValue = ushort.MinValue, MaxValue = ushort.MaxValue },
				new SensorMap { SensorLegend = SensorLegend.ToEgtsPDOP,           InputNumber = (int)EgtsSensor.Pdop,           Multiplier = 1.0M, Constant = 0.0M, MinValue = ushort.MinValue, MaxValue = ushort.MaxValue },
				new SensorMap { SensorLegend = SensorLegend.ToEgtsVDOP,           InputNumber = (int)EgtsSensor.Vdop,           Multiplier = 1.0M, Constant = 0.0M, MinValue = ushort.MinValue, MaxValue = ushort.MaxValue },
				new SensorMap { SensorLegend = SensorLegend.ToEgtsIgnition,       InputNumber = (int)EgtsSensor.Ignition,       Multiplier = 1.0M, Constant = 0.0M, MinValue = 0,               MaxValue = 1 },
				new SensorMap { SensorLegend = SensorLegend.ToEgtsAnalogSensor1,  InputNumber = (int)EgtsSensor.AnalogueInput1, Multiplier = 1.0M, Constant = 0.0M, MinValue = uint.MinValue,   MaxValue = uint.MaxValue },
				new SensorMap { SensorLegend = SensorLegend.ToEgtsAnalogSensor2,  InputNumber = (int)EgtsSensor.AnalogueInput2, Multiplier = 1.0M, Constant = 0.0M, MinValue = uint.MinValue,   MaxValue = uint.MaxValue },
				new SensorMap { SensorLegend = SensorLegend.ToEgtsAnalogSensor3,  InputNumber = (int)EgtsSensor.AnalogueInput3, Multiplier = 1.0M, Constant = 0.0M, MinValue = uint.MinValue,   MaxValue = uint.MaxValue },
				new SensorMap { SensorLegend = SensorLegend.ToEgtsAnalogSensor4,  InputNumber = (int)EgtsSensor.AnalogueInput4, Multiplier = 1.0M, Constant = 0.0M, MinValue = uint.MinValue,   MaxValue = uint.MaxValue },
				new SensorMap { SensorLegend = SensorLegend.ToEgtsAnalogSensor5,  InputNumber = (int)EgtsSensor.AnalogueInput5, Multiplier = 1.0M, Constant = 0.0M, MinValue = uint.MinValue,   MaxValue = uint.MaxValue },
				new SensorMap { SensorLegend = SensorLegend.ToEgtsAnalogSensor6,  InputNumber = (int)EgtsSensor.AnalogueInput6, Multiplier = 1.0M, Constant = 0.0M, MinValue = uint.MinValue,   MaxValue = uint.MaxValue },
				new SensorMap { SensorLegend = SensorLegend.ToEgtsAnalogSensor7,  InputNumber = (int)EgtsSensor.AnalogueInput7, Multiplier = 1.0M, Constant = 0.0M, MinValue = uint.MinValue,   MaxValue = uint.MaxValue },
				new SensorMap { SensorLegend = SensorLegend.ToEgtsAnalogSensor8,  InputNumber = (int)EgtsSensor.AnalogueInput8, Multiplier = 1.0M, Constant = 0.0M, MinValue = uint.MinValue,   MaxValue = uint.MaxValue },
			}
				.ToDictionary(k => k.SensorLegend, v => new SensorMap[] { v });
			// Заполняем значения датчиков
			mu.SetSensorValue((int)EgtsSensor.DigitalInput1,  01);
			mu.SetSensorValue((int)EgtsSensor.DigitalInput2,  01);
			mu.SetSensorValue((int)EgtsSensor.DigitalInput3,  01);
			mu.SetSensorValue((int)EgtsSensor.DigitalInput4,  01);
			mu.SetSensorValue((int)EgtsSensor.DigitalInput5,  01);
			mu.SetSensorValue((int)EgtsSensor.DigitalInput6,  01);
			mu.SetSensorValue((int)EgtsSensor.DigitalInput7,  01);
			mu.SetSensorValue((int)EgtsSensor.DigitalInput8,  01);
			mu.SetSensorValue((int)EgtsSensor.Hdop,           10);
			mu.SetSensorValue((int)EgtsSensor.Pdop,           20);
			mu.SetSensorValue((int)EgtsSensor.Vdop,           30);
			mu.SetSensorValue((int)EgtsSensor.Ignition,       01);
			mu.SetSensorValue((int)EgtsSensor.AnalogueInput1, -4062500);
			mu.SetSensorValue((int)EgtsSensor.AnalogueInput2, 42);
			mu.SetSensorValue((int)EgtsSensor.AnalogueInput3, 43);
			mu.SetSensorValue((int)EgtsSensor.AnalogueInput4, 44);
			mu.SetSensorValue((int)EgtsSensor.AnalogueInput5, 45);
			mu.SetSensorValue((int)EgtsSensor.AnalogueInput6, 46);
			mu.SetSensorValue((int)EgtsSensor.AnalogueInput7, 47);
			mu.SetSensorValue((int)EgtsSensor.AnalogueInput8, 48);

			return mu;
		}
		[TestMethod]
		public void TestEgtsClientYandex()
		{
			// Получить подготовленный мобильный объект
			var mu = GetMobilUnit();
			// Создать клиента и отправить данные
			using (var egtsClient = new EgtsClient("egts-test.yandex.net", 4000, mu.Unique, mu.DeviceID))
			{
				///////////////////////////////////////////////
				var requestFrame = EgtsPtAppdata.Create();
				requestFrame.Add(mu.Unique, mu.ToSrPosData(true));
				///////////////////////////////////////////////
				/// Перед отправкой трассируем отправку
				$"Sending packet for {mu.Unique} to EGTS server:\n{requestFrame.ToJson()}"
					.CallTraceInformation();
				///////////////////////////////////////////////
				egtsClient.Send(requestFrame);
			}
		}
		[TestMethod]
		public void MobliUnitToEgtsTest()
		{
			// Получить подготовленный мобильный объект
			var mu = GetMobilUnit();
			///////////////////////////////////////////////
			/// Перед отправкой трассируем отправку
			var requestFrame = EgtsPtAppdata.Create();
			if (requestFrame.Add(mu.Unique, mu.ToSrPosData()))
			{
				if (requestFrame.Add(mu.Unique, mu.ToSrExPosData()))
				{
					var adSensorsData = mu.ToSrAdSensorsData();
					if (adSensorsData != null)
						requestFrame.Add(mu.Unique, adSensorsData);
				}
			}
			$"Sending packet for {mu.Unique} to EGTS server:{requestFrame.ToJson()}"
				.CallTraceInformation();
			///////////////////////////////////////////////
			// Преобразуем MobileUnit в подзаписи Egts из протокола Egts
			var p1SubRecord1 = mu.ToSrPosData();
			var p1SubRecord2 = mu.ToSrExPosData();
			var p1SubRecord3 = mu.ToSrAdSensorsData();

			// Меняем протокол на любой
			mu.Properties[DeviceProperty.Protocol] = "Любой протокол";

			// Преобразуем MobileUnit в подзаписи Egts из любого протокола не Egts
			var p2SubRecord1 = mu.ToSrPosData();
			var p2SubRecord2 = mu.ToSrExPosData();
			var p2SubRecord3 = mu.ToSrAdSensorsData();

			requestFrame.Records
				.ToList()
				.ForEach(r =>
				{
					r.ToMobilUnits().ForEach(m => m.SensorValues.ToJson().CallTraceInformation());
					r.GetRecords().ForEach(s => s.ToJson().CallTraceInformation());
				});

			// После всех преобразований подзаписи попарно, должны совпадать, подзапись 1
			Assert.AreEqual(p1SubRecord1.DigitalInput1,  p2SubRecord1.DigitalInput1);
			Assert.AreEqual(p1SubRecord1.DigitalInput2,  p2SubRecord1.DigitalInput2);
			Assert.AreEqual(p1SubRecord1.DigitalInput3,  p2SubRecord1.DigitalInput3);
			Assert.AreEqual(p1SubRecord1.DigitalInput4,  p2SubRecord1.DigitalInput4);
			Assert.AreEqual(p1SubRecord1.DigitalInput5,  p2SubRecord1.DigitalInput5);
			Assert.AreEqual(p1SubRecord1.DigitalInput6,  p2SubRecord1.DigitalInput6);
			Assert.AreEqual(p1SubRecord1.DigitalInput7,  p2SubRecord1.DigitalInput7);
			Assert.AreEqual(p1SubRecord1.DigitalInput8,  p2SubRecord1.DigitalInput8);
			Assert.AreEqual(p1SubRecord1.Ignition,       p2SubRecord1.Ignition);
			// После всех преобразований подзаписи попарно, должны совпадать, подзапись 2
			Assert.AreEqual(p1SubRecord2.Satellites,     p2SubRecord2.Satellites);
			Assert.AreEqual(p1SubRecord2.Hdop,           p2SubRecord2.Hdop);
			Assert.AreEqual(p1SubRecord2.Vdop,           p2SubRecord2.Vdop);
			Assert.AreEqual(p1SubRecord2.Pdop,           p2SubRecord2.Pdop);
			// После всех преобразований подзаписи попарно, должны совпадать, подзапись 3
			Assert.AreEqual(p1SubRecord3.AnalogueInput1, p2SubRecord3.AnalogueInput1);
			Assert.AreEqual(p1SubRecord3.AnalogueInput2, p2SubRecord3.AnalogueInput2);
			Assert.AreEqual(p1SubRecord3.AnalogueInput3, p2SubRecord3.AnalogueInput3);
			Assert.AreEqual(p1SubRecord3.AnalogueInput4, p2SubRecord3.AnalogueInput4);
			Assert.AreEqual(p1SubRecord3.AnalogueInput5, p2SubRecord3.AnalogueInput5);
			Assert.AreEqual(p1SubRecord3.AnalogueInput6, p2SubRecord3.AnalogueInput6);
			Assert.AreEqual(p1SubRecord3.AnalogueInput7, p2SubRecord3.AnalogueInput7);
			Assert.AreEqual(p1SubRecord3.AnalogueInput8, p2SubRecord3.AnalogueInput8);
		}
		private void CheckString(string data)
		{
			var bytes = StringHelper.HexStringToByteArray(data);

			var requestStream = new MemoryStream(bytes);
			var responseStream = new MemoryStream(bytes);
			using (var egtsReader = new EgtsReader(requestStream))
			using (var egtsWriter = new EgtsWriter(responseStream))
			{
				TransportPacket packet;
				while(egtsReader.TryReadPacket(out packet))
				{
					packet.WriteTo(egtsWriter);
				}
			}

			var packetBytesString = BitConverter.ToString(responseStream.ToArray()).Replace("-", string.Empty);
			var bytesString = BitConverter.ToString(bytes).Replace("-", string.Empty);
			Assert.IsTrue(string.Compare(packetBytesString, bytesString, StringComparison.OrdinalIgnoreCase) == 0);
		}
	}
}