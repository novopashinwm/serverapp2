﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Compass.Ufin.Terminal.Devices.Eelink;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class EelinkDeviceTests : BaseDeviceTests<EelinkDevice>
	{
		private static readonly Dictionary<Protocol, Tuple<string, string>> PacketsDictionary =
			new []
			{
				new
				{
					Key = Protocol.LoginPacket,
					Upl = "676701000C000103525440716774710120",
					Dnl = "67670100020001",
				},
				new
				{
					Key = Protocol.HeardbeatPacket,
					Upl = "676703000400070188",
					Dnl = "67670300020007"
				},
				new
				{
					Key = Protocol.LocationPackage,
					Upl = "67671200410022590BD94203026B940D0C3952AD0021000000000001CC0001A53F0170F0AB1301890F08000000000000C2D0001C001600000000000000000000000000000000",
					Dnl = default(string)
				},
/*
				new
				{
					Key = Protocol.WarningPackage,
					Upl = "6767140024000A590BD54903026B940D0C3952AD0021000400000501CC0001A53F0170F0AB19020789",
					Dnl = "676714004A000A534F53E68AA5E8ADA621E5B9BFE4B89CE79C81E6B7B1E59CB3E5B882E58D97E5B1B1E58CBAE9BD90E6B091E9819333EFBC88E8B79DE5AE87E998B3E5A4A7E58EA630E7B1B3EFBC89"
				},
				new
				{
					Key = Protocol.ReportPackage,
					Upl = "6767150024000B590BD57103026B940D0C3952AD0021000000000501CC0001A53F0170F0AB18020789",
					Dnl = "6767150002000B"
				},
				new
				{
					Key = Protocol.MessagePackage,
					Upl = "6767160039000D590BD5AF03026B940D0C3952AD0021000000000501CC0001A53F0170F0AB17323031383536363232313235300000000000000000313233",
					Dnl = "6767160056000D323031383536363232313235300000000000000000E5B9BFE4B89CE79C81E6B7B1E59CB3E5B882E58D97E5B1B1E58CBAE9BD90E6B091E9819333EFBC88E8B79DE5AE87E998B3E5A4A7E58EA63139E7B1B3EFBC89"
				},
				// Не все типы пакетов
				new
				{
					Key = Protocol.ParamSetPackage,
					Upl = "67671B009E000500010432009266DF000008053FC0A20341303EFE8110D414404C0680185610CEF3A23C8C18154005AB64300BD0AAA845755C0CE331CF0C1B036478B843D0EA288988320B42D068956405053C11A4588FA38803FD599EC6EF4B7383D0FC3FB7333919EA637F3D8EFB1D79F9D27B8D7782191146AE344DC0766F01599EE898BBE5ED3217444DBECA0AB4BADA4B08224A48F235D59759EDEB2A24EE9C20",
					Dnl = "67671B0003000500"
				},
				new
				{
					Key = Protocol.InstructionPackage,
					Upl = "67678000905788014C754C75494D45493A3335323534343037313637373437310A494D53493A393436303031373331393831373638340A49434349443A38393836303131343834313230323133303338320A53595354454D3A4D373630315F56322E302E350A56455253494F4E3A4D584150505F56322E302E350A4255494C443A4D617920203520323031372030393A32313A3038",
					Dnl = "676780000F5788014C754C7576657273696F6E23"
				},
*/
			}
			.ToDictionary(k => k.Key, v => new Tuple<string, string>(v.Upl, v.Dnl));

		[TestMethod]
		public void SupportData()
		{
			var hexData = "67:67:01:00:0c:81:b4:03:54:18:80:47:80:40:54:01:0c";
			var bytes = hexData.HexStringToByteArray();
			CheckSupportDataByBytes(bytes);
		}

		[TestMethod]
		public void CheckPackets()
		{
			var stateData = default(object);
			foreach (var item in PacketsDictionary)
			{
				var result = OnDataByBytes(item.Value.Item1.HexStringToByteArray(), stateData);
				var stateMess = result
					?.OfType<ReceiverStoreToStateMessage>()
					?.FirstOrDefault();
				if (null != stateMess)
					stateData = stateMess.StateData;
				var confrm = result.OfType<ConfirmPacket>().FirstOrDefault();
				Assert.AreEqual(item.Value.Item2, confrm?.Data?.ToHexString());
			}
		}


		[TestMethod]
		public void LoginPacket()
		{
			var loginHexData = "67:67:01:00:0c:81:b4:03:54:18:80:47:80:40:54:01:0c";

			var result = OnDataByHex(loginHexData);

			var imei =
				result.OfType<ReceiverStoreToStateMessage>()
					.Select(x => (x.StateData as ReceiverState)?.DeviceID)
					.Single();

			Assert.AreEqual("354188047804054", imei);

			var response = result.OfType<ConfirmPacket>()
				.Select(x => x.Data)
				.Single();

			Assert.AreEqual((byte)0x67, response[0]);
			Assert.AreEqual((byte)0x67, response[1]);
			Assert.AreEqual((byte)0x01, response[2]);
			Assert.AreEqual((byte)0x81, response[response.Length-2]);
			Assert.AreEqual((byte)0xb4, response[response.Length-1]);
		}
		
		[TestMethod]
		public void SingleData()
		{
			var hexData =
				"67:67:02:00:25:86:12:58:44:52:e7:05:fc:d8:63:04:0a:34:44:00:00:00:00:fa:00:02:1e:39:00:34:f1:01:00:89:0f:07:00:2f:00:00:00:00";

			var loginHexData = "67:67:01:00:0c:81:b4:03:54:18:80:47:80:40:54:01:0c";

			var loginResult = OnDataByHex(loginHexData);
			var state =
				loginResult.OfType<ReceiverStoreToStateMessage>()
					.Select(x => x.StateData)
					.Single();

			var result = OnDataByHex(hexData, state);
			var mu = result.OfType<IMobilUnit>().Single();

			var sb = new StringBuilder();
			Helper.Append(sb, mu);
			Console.WriteLine(sb.ToString());

			Assert.IsTrue(mu.ValidPosition);
			Assert.AreEqual("354188047804054", mu.DeviceID);
		}

		[TestMethod]
		public void MultipleData()
		{
			var hexData = "67:67:02:00:25:84:19:58:41:9e:7b:05:fd:57:09:04:0a:3b:6e:00:00:00:00:fa:00:02:1e:39:00:52:d7:01:00:89:0f:86:00:27:00:00:00:00:67:67:02:00:25:84:1a:58:41:9f:c7:05:fd:57:75:04:0a:3a:6e:00:00:00:00:fa:00:02:1e:39:00:52:d7:01:00:89:0f:83:00:27:00:00:00:00:67:67:02:00:25:84:1b:58:41:a6:43:05:fd:56:64:04:0a:39:73:00:00:dc:00:fa:00:02:1e:39:00:52:d7:01:00:89:0f:78:00:17:00:00:00:00:67:67:02:00:25:84:1c:58:41:a6:9e:05:fd:53:ac:04:0a:38:98:00:00:c3:00:fa:00:02:1e:39:00:64:8a:01:00:89:0f:78:00:21:00:00:00:00:67:67:02:00:25:84:1d:58:41:a6:bc:05:fd:55:85:04:0a:39:19:02:01:4e:00:fa:00:02:1e:39:00:64:8a:01:00:89:0f:6e:00:11:00:00:00:00:67:67:02:00:25:84:1e:58:41:a6:f8:05:fd:57:11:04:0a:37:30:04:01:10:00:fa:00:02:1e:39:00:64:8a:01:00:89:0f:6a:00:22:00:00:00:00:67:67:02:00:25:84:1f:58:41:a7:17:05:fd:57:57:04:0a:34:1e:01:01:21:00:fa:00:02:1e:39:00:52:d7:01:00:89:0f:75:00:2a:00:00:00:00:67:67:02:00:25:84:20:58:41:a7:35:05:fd:57:72:04:0a:33:8b:00:01:18:00:fa:00:02:1e:39:00:52:d7:01:00:89:0f:77:00:36:00:00:00:00:67:67:02:00:25:84:21:58:41:a7:53:05:fd:56:22:04:0a:2f:f8:03:00:c2:00:fa:00:02:1e:39:00:52:d7:01:00:89:0f:78:00:2d:00:00:00:00:67:67:02:00:25:84:22:58:41:a7:71:05:fd:53:31:04:0a:2d:6a:05:00:c4:00:fa:00:02:1e:39:00:52:d7:01:00:89:0f:77:00:2f:00:00:00:00:67:67:02:00:25:84:23:58:41:a7:8f:05:fd:51:4b:04:0a:2d:78:03:00:79:00:fa:00:02:1e:39:00:52:d7:01:00:89:0f:78:00:28:00:00:00:00:67:67:02:00:25:84:24:58:41:a7:ae:05:fd:4e:33:04:0a:2f:fa:04:00:82:00:fa:00:02:1e:39:00:52:d7:01:00:89:0f:72:00:1b:00:00:00:00:67:67:02:00:25:84:25:58:41:a7:cc:05:fd:4d:d3:04:0a:35:3d:03:00:75:00:fa:00:02:1e:39:00:64:8a:01:00:89:0f:73:00:23:00:00:00:00:67:67:02:00:25:84:26:58:41:a7:ea:05:fd:4d:2b:04:0a:36:79:03:00:90:00:fa:00:02:1e:39:00:64:8a:01:00:89:0f:73:00:29:00:00:00:00:67:67:02:00:25:84:27:58:41:a8:08:05:fd:4b:84:04:0a:38:f1:03:00:90:00:fa:00:02:1e:39:00:64:8a:01:00:89:0f:71:00:22:00:00:00:00:67:67:02:00:25:84:28:58:41:a8:26:05:fd:49:a4:04:0a:3c:e2:02:00:85:00:fa:00:02:1e:39:00:64:8a:01:00:89:0f:70:00:20:00:00:00:00:67:67:02:00:25:84:29:58:41:a8:45:05:fd:48:60:04:0a:40:a1:03:00:96:00:fa:00:02:1e:39:00:64:8a:01:00:89:0f:6f:00:21:00:00:00:00:67:67:02:00:25:84:2a:58:41:a8:63:05:fd:46:f5:04:0a:42:d3:02:00:b6:00:fa:00:02:1e:39:00:64:8a:01:00:89:0f:73:00:20:00:00:00:00:67:67:02:00:25:84:2b:58:41:a8:81:05:fd:45:08:04:0a:40:db:00:00:a4:00:fa:00:02:1e:39:00:64:8a:01:00:89:0f:72:00:20:00:00:00:00:67:67:02:00:25:84:2c:58:41:a8:9f:05:fd:44:55:04:0a:40:cf:00:00:fa:00:fa:00:02:1e:39:00:64:8a:01:00:89:0f:59:00:08:00:00:00:00";
			var result = OnDataByHex(hexData);
			var mobilUnitCount = result.OfType<IMobilUnit>().Count();
			Assert.AreEqual(20, mobilUnitCount);
		}
		private static byte[][] packets = new[]
		{
			"67:67:01:00:0c:0c:f6:03:52:54:40:74:10:98:11:01:0c",
			"67:67:12:00:2d:0c:f7:5c:18:78:7a:03:05:fe:c1:9d:04:0c:2f:a5:00:9d:00:00:00:00:08:00:fa:00:02:25:f7:00:00:2c:95:1d:01:89:10:1c:00:00:00:00:00:00:47:89",
			"67:67:0a:00:26:0c:f8:5c:10:4f:80:00:00:02:34:00:00:01:a6:00:00:01:91:00:00:75:9f:00:00:02:34:00:00:01:a6:00:00:01:91:00:00:75:9f",
			"67:67:12:00:2d:0c:f9:5c:18:78:fc:03:05:fe:c1:9d:04:0c:2f:a5:00:9d:00:00:00:00:08:00:fa:00:02:25:f7:00:00:2c:95:1d:01:89:10:21:00:00:00:00:00:00:47:89",
			"67:67:03:00:04:0c:fa:01:89",
			"67:67:12:00:2d:0c:fb:5c:18:79:7d:03:05:fe:c1:9d:04:0c:2f:a5:00:9d:00:00:00:00:09:00:fa:00:02:25:f7:00:00:2c:95:1d:01:89:10:1f:00:00:00:00:00:00:47:89",
			"67:67:03:00:04:0c:fc:01:89",
			"67:67:12:00:2d:0c:fd:5c:18:79:ff:03:05:fe:c1:9d:04:0c:2f:a5:00:9d:00:00:00:00:09:00:fa:00:02:25:f7:00:00:2c:95:1d:01:89:10:1d:00:00:00:00:00:00:47:89",
			"67:67:12:00:2d:0c:fe:5c:18:7a:81:03:05:fe:c1:9d:04:0c:2f:a5:00:9d:00:00:00:00:08:00:fa:00:02:25:f7:00:00:2c:95:1d:01:89:10:1c:00:00:00:00:00:00:47:89",
			"67:67:03:00:04:0c:ff:01:89",
		}.Select(StringHelper.HexStringToByteArray).ToArray();
		[TestMethod]
		public void MultiplePackets()
		{
			var stateData = default(object);
			for (var i = 0; i < packets.Length; i++)
			{
				var packet = packets[i];
				var result = OnDataByBytes(packet, stateData);
				var stateMess = result
					?.OfType<ReceiverStoreToStateMessage>()
					?.FirstOrDefault();
				if (null != stateMess)
					stateData = stateMess.StateData;
			}
		}

	}
}