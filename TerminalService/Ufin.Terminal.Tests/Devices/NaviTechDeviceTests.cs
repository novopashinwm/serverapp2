﻿using System.Linq;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.NaviTech.VersionUtp4;
using FORIS.TSS.TerminalService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class NaviTechDeviceTests
	{
		private NaviTechDevice _device;

		private NaviTechDevice Device
		{
			get { return _device ?? (_device = new NaviTechDevice()); }
		}

		[TestMethod]
		public void SupportData()
		{
			var bytes = StringHelper.HexStringToByteArray(
				"000042033836313331313030313632393534310000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000071");

			var tm = new TerminalManager();
			var devices = tm.GetDevices(bytes);
			Assert.AreEqual(1, devices.Count);
			Assert.AreEqual(Device.GetType(), devices.Single().GetType());

			byte[] bufferRest;
			devices.Single().OnData(bytes, bytes.Length, null, out bufferRest);
		}

		[TestMethod]
		public void Parse()
		{
			byte[] data = StringHelper.HexStringToByteArray("00003618e000005002be0b684243c36042a40d0000007c00242351743e53cb0f2e07000000000000000000000000000000000000000000003618e000005000be0b684243c36042a40d0000007c0024234d743e53f60eff0c000000000000000000000000000000000000000000003618e000005002be0b684243c36042a40d0000007c0024234b743e53c40fa709000000000000000000000000000000000000000000003618e000005002be0b684243c36042a40d0000007c00242349743e53c80f4107000000000000000000000000000000000000000000003618e000005002be0b684243c36042a40d0000007c00242348743e53c10f6e05000000000000000000000000000000000000000000003618e000005002be0b684243c36042a40d0000007c00242344743e53fc0f3509000000000000000000000000000000000000000000003618e000005002be0b684243c36042a40d0000007c00242342743e53e90f6e0a000000000000000000000000000000000000000000003618e000001002ce0b68423bc36042a40d0000007c00472341743e53c00fe4040000000000000000000000000000000000000000");
			byte[] bufferRest;
			var items = Device.OnData(data, data.Length, null, out bufferRest);
			var mobilUnits = items.OfType<IMobilUnit>().ToList();
			Assert.IsNull(bufferRest);
			Assert.AreNotEqual(0, mobilUnits.Count);
		}
	}
}