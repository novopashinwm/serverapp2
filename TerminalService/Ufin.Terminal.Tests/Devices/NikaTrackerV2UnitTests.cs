﻿using System;
using FORIS.TSS.Helpers;
using FORIS.TSS.Terminal.Symbian;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class NikaTrackerV2UnitTests
	{
		[TestMethod]
		public void DeserializeData()
		{
			var originalValue = GetData();
			TestSerialization(originalValue);
		}

		private static void TestSerialization(NikaTrackerV2Data originalValue)
		{
			var serializedValue   = JsonHelper.SerializeObjectToJson(originalValue);
			var deserializedValue = JsonHelper.DeserializeObjectFromJson<NikaTrackerV2Data>(serializedValue);
			var serializedAgain   = JsonHelper.SerializeObjectToJson(deserializedValue);

			Assert.AreEqual(serializedValue, serializedAgain);
		}

		[TestMethod]
		public void EmptyData()
		{
			var emptyData = new NikaTrackerV2Data();
			TestSerialization(emptyData);
		}

		[TestMethod]
		public void DataWithExcessElements()
		{
			const string jsonString = @"{""all_items"":[{""lat"":32.343,""lng"":54.343,""date"":""2013-08-23T08:25:18.363Z""},{""key"":1,""value"":1,""date"":""2013-08-23T08:25:18.363Z""},{""mac"":""test_mac"",""ssid"":""test_ssid"",""date"":""2013-08-23T08:25:18.363Z""},{""cell_id"":100,""lac"":101,""mcc"":""test_mcc"",""mnc"":""test_mnc"",""signal_strength"":0,""date"":""2013-08-23T08:25:18.363Z""},{""text"":""test log message"",""date"":""2013-08-23T08:25:18.365Z""},{""code"":12,""date"":""2013-08-23T08:25:18.365Z""}],""app_logs"":[{""text"":""test log message"",""date"":""2013-08-23T08:25:18.365Z""}],""cells"":[{""cell_id"":100,""lac"":101,""mcc"":""test_mcc"",""mnc"":""test_mnc"",""signal_strength"":0,""date"":""2013-08-23T08:25:18.363Z""}],""locations"":[{""lat"":32.343,""lng"":54.343,""date"":""2013-08-23T08:25:18.363Z""}],""sensors"":[{""key"":1,""value"":1,""date"":""2013-08-23T08:25:18.363Z""}],""versions"":[{""code"":12,""date"":""2013-08-23T08:25:18.365Z""}],""wlans"":[{""mac"":""test_mac"",""ssid"":""test_ssid"",""date"":""2013-08-23T08:25:18.363Z""}]}";
			var deserializedValue = JsonHelper.DeserializeObjectFromJson<NikaTrackerV2Data>(jsonString);

			Assert.AreEqual(deserializedValue.Versions[0].Code, "12");
		}

		[TestMethod]
		public void DataFrom358150046757754()
		{
			const string jsonString =
				@"{""locations"":[{""accuracy"":20,""altitude"":0,""direction"":0,""lat"":55.7218475,""lng"":37.6392069,""satellites"":0,""speed"":0,""date"":""2013-08-26T07:10:08.708Z""}],""sensors"":[{""key"":22,""value"":380,""date"":""2013-08-26T07:10:48.535Z""},{""key"":12,""value"":1,""date"":""2013-08-26T07:10:48.526Z""},{""key"":5,""value"":30,""date"":""2013-08-26T07:10:48.472Z""},{""key"":30,""value"":0,""date"":""2013-08-26T07:10:11.124Z""},{""key"":29,""value"":0,""date"":""2013-08-26T07:10:11.114Z""},{""key"":28,""value"":100,""date"":""2013-08-26T07:10:11.104Z""},{""key"":27,""value"":0,""date"":""2013-08-26T07:10:11.092Z""},{""key"":26,""value"":0,""date"":""2013-08-26T07:10:11.070Z""},{""key"":25,""value"":0,""date"":""2013-08-26T07:10:11.046Z""}]}";

			var value = JsonHelper.DeserializeObjectFromJson<NikaTrackerV2Data>(jsonString);

			Assert.IsTrue(value.Locations[0].Lat > 0 && value.Locations[0].Lng > 0);
		}

		private static NikaTrackerV2Data GetData()
		{
			return new NikaTrackerV2Data
			{
				Locations = new[]
						{
							new NikaTrackerV2Data.Location
								{
									Date = new DateTime(2013, 08, 01, 12, 34, 56),
									Lat = 57.123456,
									Lng = 37.123456,
									Accuracy = 1234,
									Altitude = 1234,
									Direction = 123,
									Satellites = 6,
									Speed = 123
								},
							new NikaTrackerV2Data.Location
								{
									Date = new DateTime(2013, 08, 01, 13, 43, 34),
									Lat = 57.123456,
									Lng = 37.123456,
									Accuracy = 1234,
									Altitude = 1234,
									Direction = 123,
									Satellites = 6,
									Speed = 123
								}
						},
				Sensors = new[]
						{
							new NikaTrackerV2Data.Sensor
								{
									Date = new DateTime(2013, 08, 01, 12, 34, 56),
									Key = "BatteryLevel",
									Value = 95
								},
							new NikaTrackerV2Data.Sensor
								{
									Date = new DateTime(2013, 08, 01, 12, 34, 56),
									Key = "IsPlugged",
									Value = 0
								}
						},
				Cells = new[]
						{
							new NikaTrackerV2Data.Cell
								{
									Date = new DateTime(2013, 08, 01, 12, 34, 56),
									mcc = "250",
									mnc = "01",
									cell_id = 12345,
									lac = 2345,
									signal_strength = 56
								}
						},
				Wlans = new[]
						{
							new NikaTrackerV2Data.Wlan
								{
									Date = new DateTime(2013, 08, 01, 12, 34, 56),
									mac = "1234567890ABCDEF10",
									ssid = "SuperWifi",
									channel = 10,
									signal_strength = 11
								}
						},
				App_logs = new[]
						{
							new NikaTrackerV2Data.AppLog
								{
									Date = new DateTime(2013, 08, 01, 12, 34, 56),
									Text = "Hello, world!"
								}
						},
				Versions = new[]
						{
							new NikaTrackerV2Data.Version
								{
									Date = new DateTime(2013, 08, 01, 12, 34, 56),
									Code = "1.2.3.4"
								}
						}
			};
		}
	}
}