﻿using System.Linq;
using System.Text;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.Common.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RU.NVG.NIKA.Terminal.TR06A;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class TR06ADeviceTests : BaseDeviceTests<TR06ADevice>
	{
		[TestMethod]
		public void TestInput()
		{
			var bytes = StringHelper.HexStringToByteArray("2a48512c313130323030373631332c56312c3135323532332c412c343530322e323734332c4e2c30333835382e313933362c452c3031332e332c3130332c3036303331352c464646464642464623");
			var device = GetDevice(bytes);
			byte[] bufferRest;
			var result = device.OnData(bytes, bytes.Length, null, out bufferRest);
			Assert.IsNotNull(result);
		}
		[TestMethod]
		public void Binary()
		{
			var bytes = StringHelper.HexStringToByteArray("24 1102014588    073617 310316 4500 9969 00 03857 7946 f030191 fffffbff ff00e6");
			var result = OnDataByBytes(bytes);
			Assert.IsNotNull(result);
		}
		[TestMethod]
		public void TR06A_OnDataSupportedPacket_Txt()
		{
			var packetText = default(string);
			packetText = @"*HQ,4106051685,V1,111559,A,5037.4830,N,04239.5664,E,000.00,000,220120,FFFDF9FE#";
			var mobilUnits1 = GetMobilUnits(Encoding.ASCII.GetBytes(packetText))
				.ToList();
		}
		public void TR06A_OnDataUnSupportedPacket_Txt()
		{
			var packetText = default(string);
			packetText = @"*HQ,4106051685,V19,111559,A,5037.4830,N,04239.5664,E,000.00,000,220120,,+79377306628,897010273190084517FF,FFFDF9FE#";
			var mobilUnits1 = GetMobilUnits(Encoding.ASCII.GetBytes(packetText))
				.ToList();
		}
		[TestMethod]
		public void TR06A_OnDataBug_EmptyDateTime_Hex()
		{
			var packetData = default(string);
			packetData = @"24-11-02-00-70-17-00-00-00-00-00-00-53-14-67-31-00-05-01-46-60-0D-00-00-00-FF-FF-FB-FF-FF-00-0B";
			var mobilUnits1 = GetMobilUnitsByHex(packetData)
				.ToList();
			packetData = @"24-11-02-00-70-17-00-00-00-00-00-00-53-14-67-31-00-05-01-46-60-0D-00-00-00-FF-FF-FB-FF-FF-00-0D";
			var mobilUnits2 = GetMobilUnitsByHex(packetData)
				.ToList();
		}
	}
}