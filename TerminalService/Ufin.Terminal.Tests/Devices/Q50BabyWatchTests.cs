﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Compass.Ufin.Terminal.Devices.Q50BabyWatch;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.TerminalService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class Q50BabyWatchTests : BaseDeviceTests<Q50BabyWatchDevice>
	{
		[TestMethod]
		public void SupportData()
		{
			var stringData = "[3G*2104023933*0009*LK,0,0,62]";

			CheckSupportDataByASCII(stringData);
		}

		[TestMethod]
		public void LoginPacket()
		{
			var stringData = "[3G*2104023933*0009*LK,0,0,62]";

			var result = OnDataByASCII(stringData);

			var response = 
				Encoding.ASCII.GetString(
				result.OfType<ConfirmPacket>()
				.Select(x => x.Data)
				.Single());

			Assert.AreEqual("[3G*2104023933*0002*LK]", response);
		}
		
		[TestMethod]
		public void SingleData3G()
		{
			var stringData =
				"[3G*6105424614*00CB*UD2,101216,123949,A,55.826950,N,37.6545600,E,5.57,247.8,0.0,7,93,100,0,0,00000000,7,255,250,2,7737,21207,141,7737,21202,148,7737,25738,141,7737,11181,135,7737,6740,130,7737,6744,127,7737,25736,125,0,37.7]";

			var result = OnDataByASCII(stringData);

			var mu = result.OfType<IMobilUnit>().Single();

			var sb = new StringBuilder();
			Helper.Append(sb, mu);
			Trace.TraceInformation(sb.ToString());

			Assert.IsTrue(mu.ValidPosition);
			Assert.AreEqual("6105424614", mu.DeviceID);
			Assert.IsTrue(Math.Abs(mu.Latitude-55.826950) < 0.0000001);
			Assert.IsTrue(Math.Abs(mu.Longitude-37.6545600) < 0.0000001);
			Assert.AreEqual(new DateTime(2016, 12, 10, 12, 39, 49, DateTimeKind.Utc), TimeHelper.GetDateTimeUTC(mu.Time));
		}
		[TestMethod]
		public void SingleDataSG()
		{
			var stringData =
				"[SG*9090000197*0094*UD,020919,151046,A,54.723621,N,37.122785,E,0.0000,000,1,09,100,90,0,50,00000000,4,1,250,2,4003,3584,137,4003,3585,130,4003,12002,125,4003,11893,115,,00]";

			var result = OnDataByASCII(stringData);

			var mu = result.OfType<IMobilUnit>().Single();

			var sb = new StringBuilder();
			Helper.Append(sb, mu);
			Trace.TraceInformation(sb.ToString());

			Assert.IsTrue(mu.ValidPosition);
			Assert.AreEqual("9090000197", mu.DeviceID);
			Assert.IsTrue(Math.Abs(mu.Latitude  - 54.723621) < 0.0000001);
			Assert.IsTrue(Math.Abs(mu.Longitude - 37.122785) < 0.0000001);
			Assert.AreEqual(new DateTime(2019, 09, 02, 15, 10, 46, DateTimeKind.Utc), TimeHelper.GetDateTimeUTC(mu.Time));
		}
		[TestMethod]
		public void MultipleData3G()
		{
			var stringData = string.Empty
				+ "[3G*2104023933*0009*LK,0,0,67]"
				+ "[3G*2104023933*017B*CONFIG,TY:g36a,UL:400,SY:0,CM:0,WT:0,HR:0,TB:0,CS:0,PP:0,AB:0,HH:1,TR:1,MO:1,FL:0,VD:0,DD:0,SD:1,XY:0,WF:0,WX:0,PH:0,RW:0,MT:1,XD:0,XL:0,YF:1,SM:0,HF:0,JX:0,WL:2,BQ:0,QQ:0,DL:0,HT:0,PB:0,RS:000000,DW:0,SS:0,OF:0,IN:0,JT:0,LG:0+1,GH:0,BT:0,CL:0,FA:0,FD:1,CT:1,SO:1,ME:1,LR:0,TO:1,RR:1,AC:0,DC:0,RD:0,RY:0,XM:0,YJ:0,EM:0,VL:0+0,PV:0,FY:0,VR:G36S_RFHZ_V28_V1.1.0_2019.07.06_10.01.32]"
				+ "[3G*2104023933*003C*ICCID,897010269314093962FF,865121040239335,9250026931409396,]"
				+ "[3G*2104023933*0183*UD2,301219,133714,V,55.827140,N,37.6550817,E,0.00,0.0,0.0,0,100,67,0,0,00000000,7,255,250,1,1604,16616,153,1604,16612,157,1604,16613,156,1604,42292,143,1604,16611,141,1604,16614,134,1604,16615,132,5,fast_inet,74:4d:28:06:d7:54,-58,digipulse2,1c:b7:2c:d6:ea:04,-61,SILVEROFF_GUEST,1c:b7:2c:d6:ea:05,-61,strato,90:8d:78:c8:22:c2,-70,HP-Print-7C-Officejet Pro 861,dc:4a:3e:26:ea:7c,-78,19.4]"
				+ "[3G*2104023933*0173*UD,301219,135718,A,55.827117,N,37.6551450,E,0.00,259.7,0.0,9,100,67,0,0,00000000,6,0,250,2,9736,11181,157,9736,21202,138,9736,21214,135,9736,21207,134,9734,50727,132,9734,22322,130,5,fast_inet,74:4d:28:06:d7:54,-62,digipulse2,1c:b7:2c:d6:ea:04,-75,SILVEROFF_GUEST,1c:b7:2c:d6:ea:05,-75,HP-Print-7C-Officejet Pro 861,dc:4a:3e:26:ea:7c,-87,strato,90:8d:78:c8:22:c2,-88,24.8]"
				+ "[3G*1703010342*0003*TKQ]";

			var result = OnDataByASCII(stringData);

			var mus = result.OfType<IMobilUnit>().ToList();

			var sb = new StringBuilder();
			Helper.Append(sb, mus, typeof(Q50BabyWatchSensor));
			Trace.TraceInformation(sb.ToString());
		}
	}
}