﻿using System.Linq;
using Compass.Ufin.Terminal.Devices.ApmKingsTrack;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.Common.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class ApmktAIS140DeviceTests : BaseDeviceTests<ApmktAIS140Device>
	{
		[TestMethod]
		public void ApmktAIS140Device_Support()
		{
			Assert.IsTrue(realPackets.All(p => Device.SupportData(p)));
		}
		[TestMethod]
		public void ApmktAIS140Device_OnData()
		{
			Assert.AreNotEqual(0, GetResults(realPackets)?.Count() ?? 0);
		}
		private static byte[][] realPackets = new[]
		{
			"24:2c:31:30:2c:41:50:4d:2c:31:2e:31:2e:32:2c:4e:52:2c:30:31:2c:4c:2c:38:36:39:32:34:37:30:34:35:32:31:34:32:39:30:2c:55:4e:4b:4e:4f:57:4e:2c:31:2c:30:39:31:32:32:30:32:30:2c:31:33:30:30:31:33:2c:31:32:2e:39:38:31:37:36:32:2c:4e:2c:38:30:2e:31:36:34:36:36:38:2c:45:2c:30:2e:30:2c:32:36:33:2e:36:2c:30:39:2c:32:34:2e:30:2c:32:2e:34:2c:31:2e:31:2c:43:45:4c:4c:4f:4e:45:2c:30:2c:31:2c:31:34:2e:33:2c:34:2e:31:2c:30:2c:31:37:2c:34:30:34:2c:36:34:2c:30:39:32:31:2c:39:33:38:33:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:30:30:30:2c:30:30:2c:30:31:33:33:35:30:2c:30:30:31:33:2c:2a:0d:0a",
			"24:2c:31:30:31:2c:41:50:4d:2c:31:2e:31:2e:32:2c:38:36:39:32:34:37:30:34:35:32:31:34:32:39:30:2c:30:39:31:32:32:30:32:30:2c:31:33:30:36:31:32:2c:31:30:30:2c:33:30:2c:30:2c:31:30:2c:31:32:30:2c:30:30:30:30:2c:30:2e:30:2c:30:2e:30:2c:2a:0d:0a",
			"24:2c:35:30:30:2c:38:36:39:32:34:37:30:34:35:32:31:34:32:39:30:2c:38:39:39:31:30:34:37:33:31:32:31:38:30:33:38:35:32:30:39:30:2c:30:39:31:32:32:30:32:30:2c:31:33:30:36:31:34:2c:30:2c:30:2c:30:31:31:38:2c:2c:2a:0d:0a",
			"24:2c:31:30:2c:41:50:4d:2c:31:2e:31:2e:32:2c:4e:52:2c:30:31:2c:4c:2c:38:36:39:32:34:37:30:34:35:32:31:34:32:39:30:2c:55:4e:4b:4e:4f:57:4e:2c:31:2c:30:39:31:32:32:30:32:30:2c:31:33:30:36:31:34:2c:31:32:2e:39:38:31:37:37:35:2c:4e:2c:38:30:2e:31:36:34:36:37:31:2c:45:2c:30:2e:30:2c:30:2e:30:2c:31:31:2c:32:32:2e:35:2c:32:2e:31:2c:30:2e:39:2c:43:45:4c:4c:4f:4e:45:2c:30:2c:31:2c:31:34:2e:33:2c:34:2e:31:2c:30:2c:31:38:2c:34:30:34:2c:36:34:2c:30:39:32:31:2c:39:33:38:33:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:30:30:30:2c:30:30:2c:30:31:33:33:35:33:2c:30:30:31:37:2c:2a:0d:0a",
			"24:2c:32:30:30:2c:41:50:4d:2c:31:2e:31:2e:32:2c:4e:52:2c:30:31:2c:48:2c:38:36:39:32:34:37:30:34:35:32:31:34:32:39:30:2c:55:4e:4b:4e:4f:57:4e:2c:31:2c:30:39:31:32:32:30:32:30:2c:31:33:35:37:35:38:2c:31:32:2e:39:38:31:38:37:33:2c:4e:2c:38:30:2e:31:36:34:37:33:32:2c:45:2c:30:2e:30:2c:30:2e:30:2c:30:38:2c:32:39:2e:33:2c:31:2e:35:2c:30:2e:38:2c:43:45:4c:4c:4f:4e:45:2c:30:2c:31:2c:31:34:2e:33:2c:34:2e:31:2c:30:2c:31:39:2c:34:30:34:2c:36:34:2c:30:39:32:31:2c:39:33:38:31:2c:39:33:38:33:2c:30:39:32:31:2c:31:39:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:2c:30:30:30:30:2c:30:30:2c:30:31:33:33:37:39:2c:30:30:31:38:2c:2a:0d:0a",
		}.Select(StringHelper.HexStringToByteArray).ToArray();
	}
}