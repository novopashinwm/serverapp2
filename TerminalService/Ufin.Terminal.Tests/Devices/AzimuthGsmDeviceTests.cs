﻿using System.Linq;
using System.Text;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.Terminal.AzimuthGSM;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class AzimuthGsmDeviceTests : BaseDeviceTests<AzimuthGsmDevice>
	{
		[TestMethod]
		public void AzimuthGsm_Support()
		{
			// Valid (Assert.IsFalse иногда выдает ошибки)
			Assert.AreEqual(true,  Device.SupportData(Encoding.ASCII.GetBytes("!1234,02,7B2D01DC,2715,00*E2")));
			// Invalid (Assert.IsTrue иногда выдает ошибки)
			Assert.AreEqual(false, Device.SupportData(Encoding.ASCII.GetBytes("!1,860719020212514;")));
			Assert.AreEqual(false, Device.SupportData(Encoding.ASCII.GetBytes("!D,16/2/15,10:4:47,55.739330,37.769604,0,0,1b0002,0.0,79,0,13,0;")));
		}

		[TestMethod]
		public void AzimuthGsm_OnData()
		{
			Assert.AreEqual(0, (GetMobilUnits("!1234,02,7B2D01DC,2715,00*E2")?.Count() ?? 0));
		}
	}
}