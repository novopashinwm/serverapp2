﻿using System.Linq;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;
using FORIS.TSS.TerminalService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RU.NVG.NIKA.Terminal.Zenith;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class ZenithRS101Tests
	{
		private ZenithRS1102Device _device;

		private ZenithRS1102Device Device
		{
			get { return _device ?? (_device = new ZenithRS1102Device()); }
		}

		[TestMethod]
		public void TwoFrames56()
		{
			CheckPacketCount(
				"af84262128283f25292d255a2e4d4f20482a7b4a2436216c48205d46742020202020202021202020202020202220252d5320262125462547380d0aaf84262128284a25292d255a2e416420482a7b7f2372217648205d46742020202020222020202020272020202020252d532026212546235e7b0d0a",
				2);
		}

		private void CheckPacketCount(string hexString, int expectedPacketCount)
		{
			var packet = StringHelper.HexStringToByteArray(hexString);
			Assert.IsTrue(Device.SupportData(packet));

			byte[] bufferRest;
			var result = Device.OnData(packet, packet.Length, null, out bufferRest);

			Assert.IsNull(bufferRest);

			Assert.AreEqual(result.OfType<IMobilUnit>().Count(), expectedPacketCount);

			Assert.IsTrue(
				result.OfType<IMobilUnit>().First().DeviceID.SequenceEqual(
					result.OfType<IMobilUnit>().Last().DeviceID
					));
		}

		[TestMethod]
		public void TwoFrames78()
		{
			CheckPacketCount(
				"af8427832d3e4d2c292d255a41536020515b54442020202f5246332b662d20333f838383832020427a838383833c2341380d0aaf8427832d40532c292d255a41536020515b54442020202f5246332b662d20333f838383832020427a838383833c2420320d0a",
				2);
		}
	}
}