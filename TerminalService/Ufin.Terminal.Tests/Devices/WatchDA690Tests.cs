﻿using System.Linq;
using System.Text;
using Compass.Ufin.Terminal.Tests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RU.NVG.Terminal.WatchDA690;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class WatchDA690Tests : BaseDeviceTests<WatchDA690Device>
	{
		private static readonly byte[][] ValidPackets = new[]
			{
				"$$,simei:352581250260312,A;",
				"simei:352581250260312,,,weather,87,26,0.00,140429173340,V,,N,,E,0.00,01FF,4636,250,01,1x0x0*0x1*3600x+4,ru-ru,;",
				"simei:352581250260312,,,weather,84,25,0.00,140503155338,V,,N,,E,0.00,76F2,29C4,250,01,1x0x0*0x1*3600x+4,ru-ru,;"
			}.Select(s => Encoding.ASCII.GetBytes(s)).ToArray();

		[TestMethod]
		public void SupportData()
		{
			foreach (var validPacket in ValidPackets)
			{
				Assert.IsTrue(Device.SupportData(validPacket));
			}
		}

		[TestMethod]
		public void OnData()
		{
			foreach (var validPacket in ValidPackets)
			{
				byte[] bufferRest;
				Assert.IsNotNull(Device.OnData(validPacket, validPacket.Length, null, out bufferRest));
			}
		}
	}
}