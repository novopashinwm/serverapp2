﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.Common;
using FORIS.TSS.Terminal.Devices;
using FORIS.TSS.TerminalService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class DevEchoDeviceTests : BaseDeviceTests<DevEchoDevice>
	{
		private readonly Random _devRand = new Random();
		private readonly byte[] _devEcho = Encoding.ASCII.GetBytes("/dev/echo");
		/// <summary> Тестирование метода: <see cref="TestDevEchoDevice.SupportData(byte[])"/> </summary>
		[TestMethod]
		public void DevEcho_SupportData()
		{
			var data = _devEcho.Concat(
				Enumerable.Repeat((byte)_devRand.Next(0, 255), _devRand.Next(0, 100))).ToArray();
			using (new Stopwatcher(MethodBase.GetCurrentMethod().Name, null, TimeSpan.Zero))
			{
				CheckSupportDataByBytes(data);
			}
		}
		/// <summary> Тестирование метода: <see cref="TestDevEchoDevice.OnData(byte[], int, object, out byte[])"/> </summary>
		[TestMethod]
		public void DevEcho_OnData()
		{
			var data = _devEcho.Concat(
				Enumerable.Repeat((byte)_devRand.Next(0, 255), _devRand.Next(0, 100))).ToArray();
			using (new Stopwatcher(MethodBase.GetCurrentMethod().Name, null, TimeSpan.Zero))
			{
				var responses = OnDataByBytes(data);
				Assert.IsNotNull(responses);
				Assert.IsTrue(responses.OfType<ConfirmPacket>().Count() > 0);
			}
		}
	}
}