﻿using System.Linq;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.GlobalOrient;
using FORIS.TSS.Terminal.Terminal;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class GlobalOrientTests : BaseDeviceTests<GlobalOrient>
	{
		private byte[] _sample1 = StringHelper.HexStringToByteArray("7e7e1c0002006d440200000000000000006400010000000000060002000200062302000004000000000000");

		private byte[] _sample2 = StringHelper.HexStringToByteArray("7e7e52000200cc7f020000000000000100640001002c07000000008fe15a55ab087216f4bb4f21e063000000005f000000b90007020200040004000000000000000000000000000000000000000000000008000200000000000801020000000000");

		[TestMethod]
		public void Test()
		{
			byte[] bufferRest;
			var data = Device.OnData(_sample1, _sample1.Length, null, out bufferRest);
			Assert.IsNotNull(data);
			var stateMessage = data.OfType<ReceiverStoreToStateMessage>().FirstOrDefault();
			Assert.IsNotNull(stateMessage);
			data = Device.OnData(_sample2, _sample2.Length, stateMessage.StateData, out bufferRest);
			Assert.IsNotNull(data);
		}

		[TestMethod]
		public void Immobilize()
		{
			var bytes = Device.GetCmd(CommandCreator.CreateCommand(CmdType.Immobilize));
			Assert.AreEqual(27, bytes.Length);
		}

		[TestMethod]
		public void ImmobilizeAnswer()
		{
			var list = OnDataByHex("7E-7E-0E-00-02-00-D8-2A-02-02-00-00-00-00-00-00-00-00-00-00-00-02-00-00-00-00-00-00-00");
			Assert.AreEqual(0, list.Count);
		}
	}
}