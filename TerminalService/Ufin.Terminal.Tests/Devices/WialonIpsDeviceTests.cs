﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using Compass.Ufin.Terminal.Devices.Wialon;
using Compass.Ufin.Terminal.Devices.Wialon.Ips;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class WialonIpsDeviceTests : BaseDeviceTests<WialonIpsDevice>
	{

		private static WialonReceiverState CreateReceiverState()
		{
			return new WialonReceiverState
			{
				DeviceID = "6080"
			};
		}
		[TestMethod]
		public void SupportData()
		{
			Assert.IsTrue(Device.SupportData(GetBytes("#L#6080;AA55AA55\r\n")));
			Assert.IsTrue(Device.SupportData(GetBytes("#L#2.0;6080;AA55AA55\r\n")));
		}
		private static byte[] GetBytes(string s)
		{
			return Encoding.ASCII.GetBytes(s);
		}
		[TestMethod]
		public void Logins()
		{
			var results = default(IList);
			var confirm = default(ConfirmPacket);
			var devsmes = default(WialonReceiverState);
			var deviden = "6080";

			#region Правилный, версия 1.x

			results = Parse($"#L#{deviden};AA55AA55\r\n", null);
			confirm = results.OfType<ConfirmPacket>().FirstOrDefault();
			Assert.IsNotNull(confirm);
			Assert.AreEqual("#AL#1\r\n", Encoding.ASCII.GetString(confirm.Data));
			devsmes = results.OfType<ReceiverStoreToStateMessage>().FirstOrDefault()?.StateData as WialonReceiverState;
			Assert.IsNotNull(devsmes);
			Assert.AreEqual(WialonReceiverState.DefaultVersion, devsmes.Version);
			Assert.AreEqual(deviden,                               devsmes.DeviceID);

			#endregion Правилный, версия 1.x

			#region Неправилный, версия 1.x

			results = Parse($"#L#{deviden}\r\n", null);
			confirm = results.OfType<ConfirmPacket>().FirstOrDefault();
			Assert.IsNotNull(confirm);
			Assert.AreEqual("#AL#0\r\n", Encoding.ASCII.GetString(confirm.Data));

			#endregion Неправилный, версия 1.x

			#region Правилный, версия 2.x
			var message = string.Empty;
			message += $"2.2;{deviden};AA55AA55;";
			message += Encoding.ASCII.GetBytes(WialonIpsRecord.GetCrc(message).Value.ToBE().ToHexString()).ToHexString();
			results = Parse($"#L#{message}\r\n", null);
			confirm = results.OfType<ConfirmPacket>().FirstOrDefault();
			Assert.IsNotNull(confirm);
			Assert.AreEqual("#AL#1\r\n", Encoding.ASCII.GetString(confirm.Data));
			devsmes = results.OfType<ReceiverStoreToStateMessage>().FirstOrDefault()?.StateData as WialonReceiverState;
			Assert.IsNotNull(devsmes);
			Assert.AreEqual("2.2", devsmes.Version);
			Assert.AreEqual(deviden,       devsmes.DeviceID);

			#endregion Правилный, версия 2.x

			#region Неправилный, версия 2.x

			results = Parse($"#L#2.2;{deviden};AA55AA55;CRC\r\n", null);
			confirm = results.OfType<ConfirmPacket>().FirstOrDefault();
			Assert.IsNotNull(confirm);
			// Нарушена структура сообщения.
			Assert.AreEqual("#AL#0\r\n", Encoding.ASCII.GetString(confirm.Data));

			#endregion Неправилный, версия 2.x

			#region Неправилный, версия 2.x

			results = Parse($"#L#2.2;{deviden};AA55AA55;30303030\r\n", null);
			confirm = results.OfType<ConfirmPacket>().FirstOrDefault();
			Assert.IsNotNull(confirm);
			// Ошибка проверки контрольной суммы.
			Assert.AreEqual("#AL#10\r\n", Encoding.ASCII.GetString(confirm.Data));

			#endregion Неправилный, версия 2.x

		}
		[TestMethod]
		public void TestSd()
		{
			var state = ParseLoginMessageWithConfirm("#L#6080;AA55AA55\r\n", "6080");
			var result = Parse("#SD#190711;143105;5311.2023;N;04458.2690;E;0;0;279;15\r\n", state);
			var confirmPacketIsOk = CheckConfirm(result, "#ASD#1\r\n");
			Assert.IsTrue(confirmPacketIsOk);
		}
		[TestMethod]
		public void ParseLoginMessage()
		{
			ParseLoginMessageWithConfirm("#L#6080;AA55AA55\r\n", "6080");
			ParseLoginMessageWithConfirm("#L#351513054766701;NA\r\n", "351513054766701");
		}
		private WialonReceiverState ParseLoginMessageWithConfirm(string message, string deviceId)
		{
			var result = Parse(message, null);

			WialonReceiverState state = null;
			var deviceIdIsStored = false;
			var confirmPacketIsOk = CheckConfirm(result, "#AL#1\r\n");

			foreach (var x in result)
			{
				var stateMessage = x as ReceiverStoreToStateMessage;
				if (stateMessage == null)
					continue;
				state = (WialonReceiverState)stateMessage.StateData;
				deviceIdIsStored = state.DeviceID == deviceId;
			}

			Assert.IsTrue(confirmPacketIsOk);
			Assert.IsTrue(deviceIdIsStored);
			Assert.IsNotNull(state);
			return state;
		}
		private static bool CheckConfirm(IList result, string expectedMessage)
		{
			var confirms = result.OfType<ConfirmPacket>().Select(confirmPacket => Encoding.ASCII.GetString(confirmPacket.Data));
			return confirms.Contains(expectedMessage);
		}
		private IList Parse(string input, WialonReceiverState state)
		{
			var bytes = GetBytes(input);
			var result = Device.OnData(bytes, bytes.Length, state, out bytes);
			return result;
		}
		[TestMethod]
		public void ParseBufferMessage()
		{
			const string input =
				"#B#020913;171431;0000.0000;S;00000.0000;E;0;0;NA;14;0.0;0;1;2.839,0.0;NA;SIM:1:89701010085287399261,SQ:1:4864,FT0:1:0,FL0:1:0,FR0:1:0,FT1:1:0,FL1:1:0,FR1:1:0,T:1:31065,JM:1:0|020913;171531;0000.0000;S;00000.0000;E;0;0;NA;14;0.0;0;1;2.839,0.0;NA;SIM:1:89701010085287399261,SQ:1:4864,FT0:1:0,FL0:1:0,FR0:1:0,FT1:1:0,FL1:1:0,FR1:1:0,T:1:31065,JM:1:0|020913;171631;0000.0000;S;00000.0000;E;0;0;NA;14;0.0;0;1;2.842,0.0;NA;SIM:1:89701010085287399261,SQ:1:4864,FT0:1:0,FL0:1:0,FR0:1:0,FT1:1:0,FL1:1:0,FR1:1:0,T:1:31065,JM:1:0\r\n";

			var result = Parse(input, CreateReceiverState());

			CheckConfirm(result, "AB#3\r\n");
		}
		[TestMethod]
		public void CheckParseIsCorrect()
		{
			const string input =
				"#B#090913;112547;5555.3832;N;03737.2114;E;0;7;NA;14;0.7;0;1;3.92,0.0;NA;SIM:1:89701010085287400499,SQ:1:5120,FT0:1:0,FL0:1:0,FR0:1:0,FT1:1:0,FL1:1:0,FR1:1:0,PPIN0:1:140,PPOUT0:1:123,PPIN1:1:321,PPOUT1:1:421,T:1:9473,JM:1:0\r\n";

			var result = Parse(input, CreateReceiverState());

			CheckConfirm(result, "AB#1\r\n");

			var mu = result.OfType<IMobilUnit>().First();

			Assert.IsTrue(Math.Abs(mu.Latitude - 55.92305) < 0.00001);
			Assert.IsTrue(Math.Abs(mu.Longitude - 37.62019) < 0.00001);

			Assert.AreEqual(140, mu.SensorValues[(int)WialonIpsSensor.PPIN]);
			Assert.AreEqual(123, mu.SensorValues[(int)WialonIpsSensor.PPOUT]);
			Assert.AreEqual(321, mu.SensorValues[(int)WialonIpsSensor.PPIN + 1]);
			Assert.AreEqual(421, mu.SensorValues[(int)WialonIpsSensor.PPOUT + 1]);
		}
		[TestMethod]
		public void Ping()
		{
			CheckConfirm(Parse("#P#\r\n", CreateReceiverState()), "#AP#\r\n");
		}
		[TestMethod]
		public void PhotoSingle()
		{
			var pictureBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 11, 12 };
			const string fileName = "sample.jpg";
			var input = "#I#" + pictureBytes.Length + ";0;0;070512;124010;" + fileName + "\r\n";
			var bytes = Encoding.ASCII.GetBytes(input).Concat(pictureBytes).ToArray();

			byte[] bufferRest;
			var result = Device.OnData(bytes, bytes.Length, CreateReceiverState(), out bufferRest).Cast<object>();

			var mu = (IPictureDevice)result.First(o => o is IPictureDevice);
			Assert.IsTrue(pictureBytes.SequenceEqual(mu.Bytes));
			Assert.AreEqual(fileName, mu.Url);
		}
		[TestMethod]
		public void Photo2Blocks()
		{
			var firstBlock = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 11, 12 };
			var secondBlock = new byte[] { 12, 11, 10, 9, 8, 7, 6 };

			const string fileName = "sample.jpg";
			var firstBlockInput =
				Encoding.ASCII.GetBytes("#I#" + firstBlock.Length + ";0;1;070512;124010;" + fileName + "\r\n")
						.Concat(firstBlock)
						.ToArray();
			var secondBlockInput =
				Encoding.ASCII.GetBytes("#I#" + secondBlock.Length + ";1;1;070512;124010;" + fileName + "\r\n")
						.Concat(secondBlock)
						.ToArray();

			byte[] bufferRest;
			var receiverState = CreateReceiverState();
			Device.OnData(firstBlockInput, firstBlockInput.Length, receiverState, out bufferRest);
			var result = Device.OnData(secondBlockInput, secondBlockInput.Length, receiverState, out bufferRest).Cast<object>();

			var mu = (IPictureDevice)result.First(o => o is IPictureDevice);
			Assert.IsTrue(firstBlock.Concat(secondBlock).SequenceEqual(mu.Bytes));
			Assert.AreEqual(fileName, mu.Url);
		}
		[TestMethod]
		public void ParseBufferOfShortDataPackets()
		{
			const string input =
				"#B#260814;084444;6139.7932;N;05050.3372;E;0;28647;111;10|260814;084449;6139.7932;N;05050.3373;E;0;28647;111;10|260814;084454;6139.7931;N;05050.3374;E;0;28647;111;10|260814;084459;6139.7932;N;05050.3373;E;0;28647;111;10|260814;084504;6139.7932;N;05050.3374;E;0;28647;111;10|260814;084509;6139.7931;N;05050.3373;E;0;28647;111;10|260814;084514;6139.7931;N;05050.3373;E;0;28647;111;10|260814;084519;6139.7931;N;05050.3373;E;0;28647;112;10|260814;084524;6139.7930;N;05050.3373;E;0;28647;112;10|260814;084529;6139.7929;N;05050.3373;E;0;28647;112;10|260814;084534;6139.7928;N;05050.3373;E;0;28647;112;10|260814;084539;6139.7927;N;05050.3373;E;0;28647;112;10|260814;084544;6139.7927;N;05050.3373;E;0;28647;112;10|260814;084549;6139.7926;N;05050.3372;E;0;28647;112;10|260814;084554;6139.7926;N;05050.3372;E;0;28647;112;10|260814;084559;6139.7925;N;05050.3372;E;0;28647;112;10|260814;084604;6139.7925;N;05050.3372;E;0;28647;113;10|260814;084609;6139.7924;N;05050.3371;E;0;28647;113;10|260814;084615;6139.7923;N;05050.3371;E;0;28647;113;10|260814;084620;6139.7922;N;05050.3371;E;0;28647;113;10|\r\n+++";

			var result = Parse(input, CreateReceiverState());

			CheckConfirm(result, "AB#20\r\n");

			Assert.AreEqual(20, result.OfType<IMobilUnit>().Count());
		}
		[TestMethod]
		public void WialonIpsDevice_SupportData()
		{
			Assert.IsTrue(Device.SupportData(Encoding.ASCII.GetBytes(Packet01)));
			Assert.IsTrue(Device.SupportData(Encoding.ASCII.GetBytes(Packet02)));
			Assert.IsTrue(Device.SupportData(Encoding.ASCII.GetBytes(Packet03)));
			Assert.IsFalse(Device.SupportData(Encoding.ASCII.GetBytes(Packet04)));
			Assert.IsTrue(Device.SupportData(Encoding.ASCII.GetBytes(Packet05)));
			Assert.IsFalse(Device.SupportData(Encoding.ASCII.GetBytes(Packet06)));
			Assert.IsTrue(Device.SupportData(Encoding.ASCII.GetBytes(Packet07)));
			Assert.IsTrue(Device.SupportData(Encoding.ASCII.GetBytes(Packet08)));
			Assert.IsTrue(Device.SupportData(Encoding.ASCII.GetBytes(Packet09)));
			Assert.IsFalse(Device.SupportData(Encoding.ASCII.GetBytes(Packet10)));
			Assert.IsTrue(Device.SupportData(Encoding.ASCII.GetBytes(Packet11)));
			Assert.IsTrue(Device.SupportData(Encoding.ASCII.GetBytes(Packet12)));
			Assert.IsTrue(Device.SupportData(Encoding.ASCII.GetBytes(Packet13)));
			Assert.IsTrue(Device.SupportData(Encoding.ASCII.GetBytes(Packet14)));
			Assert.IsTrue(Device.SupportData(Encoding.ASCII.GetBytes(Packet15)));
			Assert.IsTrue(Device.SupportData(Encoding.ASCII.GetBytes(Packet16)));
			Assert.IsTrue(Device.SupportData(Encoding.ASCII.GetBytes(Packet17)));
			Assert.IsTrue(Device.SupportData(Encoding.ASCII.GetBytes(Packet18)));
			Assert.IsTrue(Device.SupportData(Encoding.ASCII.GetBytes(Packet19)));
		}
		[TestMethod]
		public void WialonCombineDevice_OnData()
		{
			var xxx = GetResults(PacketsAsArrays);
		}
		#region Данные
		public readonly byte[][] PacketsAsArrays;
		public readonly string Packet01 = "#L#2.0;42001300083;;CE45\r\n";
		public readonly string Packet02 = "#L#123456789012345;test\r\n";
		public readonly string Packet03 = "#L#2002;NA\r\n";
		public readonly string Packet04 = "#P#\r\n";
		public readonly string Packet05 = "#D#101118;061143;0756.0930;N;12338.6403;E;18.223;99.766;-4.000;10;0.800;NA;NA;NA;NA;101_521347:1:521249,101_521126:1:6593598,101_521127:1:774780,101_521072_21.1:1:0,101_521072_21.2:1:71353;F24A\r\n";
		public readonly string Packet06 = "99999999#D#101118;061143;0756.0930;N;12338.6403;E;18.223;99.766;-4.000;10;0.800;NA;NA;NA;NA;101_521347:1:521249,101_521126:1:6593598,101_521127:1:774780,101_521072_21.1:1:0,101_521072_21.2:1:71353;F24A\r\n";
		public readonly string Packet07 = "#D#151216;135910;5321.1466;N;04441.7929;E;87;156;265.000000;12;1.000000;241;NA;NA;NA;odo:2:0.000000,total_fuel:1:430087,can_fls:1:201,can_taho:1:11623,can_mileage:1:140367515\r\n";
		public readonly string Packet08 = "#D#151216;140203;5312.59514;N;04830.37834;E;53;273;NA;10;NA;NA;NA;NA;NA;EvId:1:1,Board:2:12.81,Accum:2:4.28\r\n";
		public readonly string Packet09 = "#SD#270413;205601;5544.6025;N;03739.6834;E;1;2;3;4\r\n";
		public readonly string Packet10 = "99999999#SD#270413;205601;5544.6025;N;03739.6834;E;1;2;3;4\r\n";
		public readonly string Packet11 = "#SD#021214;065947;2237.7552;N;11404.8851;E;0.000;;170.9;5\r\n";
		public readonly string Packet12 = "#D#270413;205601;5544.6025;N;03739.6834;E;1;2;3;4;0.0;0;0;14.77,0.02,3.6;NA;count1:1:564,fuel:2:45.8,hw:3:V4.5\r\n";
		public readonly string Packet13 = "#D#190114;051312;4459.6956;N;04105.9930;E;35;306;204.000000;12;NA;452986639;NA;106.000000;NA;sats_gps:1:9,sats_glonass:1:3,balance:2:12123.000000,stay_balance:1:0\r\n";
		public readonly string Packet14 = "#D#021214;065947;2237.7552;N;11404.8851;E;0.000;;170.9;5;1.74;NA;NA;NA;NA;NA\r\n";
		public readonly string Packet15 = "#D#021214;065947;2237.7552;N;11404.8851;E;0.000;;170.9;5;1.74;NA;NA;;NA;NA\r\n";
		public readonly string Packet16 = "#B#080914;073235;5027.50625;N;03026.19321;E;0.700;0.000;NA;4;NA;NA;NA;;NA;Батарея:3:100 %|080914;073420;5027.50845;N;03026.18854;E;1.996;292.540;NA;4;NA;NA;NA;;NA;Батарея:3:100 %\r\n";
		public readonly string Packet17 = "#B#110914;102132;5027.50728;N;03026.20369;E;1.979;288.170;NA;NA;NA;NA;NA;;NA;Батарея:3:100 %\r\n";
		public readonly string Packet18 = "#B#110315;045857;5364.0167;N;06127.8262;E;0;155;965;7;2.40;4;0;;NA;Uacc:2:3.4,Iacc:2:0.000,Uext:2:13.2,Tcpu:2:14.4,Balance:2:167.65,GPS:3:Off\r\n";
		public readonly string Packet19 = "#B#110315;045857;5364.0167;N;06127.8262;E;0;155;965;7;2.40;4;0;14.77,0.02,3.6;AB45DF01145;\r\n";

		public WialonIpsDeviceTests()
		{
			PacketsAsArrays = new[]
			{
				Encoding.ASCII.GetBytes(Packet01),
				Encoding.ASCII.GetBytes(Packet02),
				Encoding.ASCII.GetBytes(Packet03),
				Encoding.ASCII.GetBytes(Packet04),
				Encoding.ASCII.GetBytes(Packet05),
				Encoding.ASCII.GetBytes(Packet06),
				Encoding.ASCII.GetBytes(Packet07),
				Encoding.ASCII.GetBytes(Packet08),
				Encoding.ASCII.GetBytes(Packet09),
				Encoding.ASCII.GetBytes(Packet10),
				Encoding.ASCII.GetBytes(Packet11),
				Encoding.ASCII.GetBytes(Packet12),
				Encoding.ASCII.GetBytes(Packet13),
				Encoding.ASCII.GetBytes(Packet14),
				Encoding.ASCII.GetBytes(Packet15),
				Encoding.ASCII.GetBytes(Packet16),
				Encoding.ASCII.GetBytes(Packet17),
				Encoding.ASCII.GetBytes(Packet18),
				Encoding.ASCII.GetBytes(Packet19),
			};
		}

		#endregion Данные

	}
}