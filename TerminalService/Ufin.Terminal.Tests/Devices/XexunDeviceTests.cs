﻿using System.Linq;
using System.Text;
using Compass.Ufin.Terminal.Tests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RU.NVG.NIKA.Terminal.Xexun;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class XexunDeviceTests : BaseDeviceTests<XexunDevice>
	{
		private static readonly byte[][] ValidPackets = new[]
			{
				"1404222312,+79083534670,GPRMC,231219.856,A,5543.4298,N,03738.3874,E,1.36,159.77,220414,,,A*60,L,imei:013227001857709,106tM",
				"1405221335,+79083534670,GPRMC,133535.381,A,5543.3103,N,03738.3308,E,0.00,284.37,220514,,,A*6D,F,imei:013227001857709,106.t"
			}.Select(s => Encoding.ASCII.GetBytes(s)).ToArray();

		[TestMethod]
		public void SupportData()
		{
			foreach (var validPacket in ValidPackets)
			{
				Assert.IsTrue(Device.SupportData(validPacket));
			}
		}

		[TestMethod]
		public void OnData()
		{
			foreach (var validPacket in ValidPackets)
			{
				byte[] bufferRest;
				Assert.AreEqual(1, Device.OnData(validPacket, validPacket.Length, null, out bufferRest).Count);
				Assert.IsNull(bufferRest);
			}
		}
	}
}