﻿using System.Linq;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Terminal.Terminal;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RU.NVG.Terminal.Avtofon;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class AvtofonDeviceTests
	{
		[TestMethod]
		public void AuthorizationPacket()
		{
			var bytes = "1056700869158009336690ba".HexStringToByteArray();

			var device = new AvtofonDevice();

			Assert.IsTrue(device.SupportData(bytes));

			byte[] bufferRest;
			var result = device.OnData(bytes, bytes.Length, null, out bufferRest);

			var message = (ReceiverStoreToStateMessage)result.Cast<object>().Single(x => x is ReceiverStoreToStateMessage);

			var imei = ((AvtofonReceiverState)message.StateData).DeviceID;

			Assert.AreEqual("869158009336690", imei);
		}

		[TestMethod]
		public void BufferPacket()
		{
			var bytes = @"1216018B007219090D0D330F214700FA000101FE9F664519090D092F12034DD776023A6E85007C00000019FFFF0C007219090D0D3319214400FA000101FE2FCC4519090D092F1C034DD776023A6E8D007E00000019FFFFEC007019090D0D3323214400FA000101FE2FCC4519090D092F26034DD77B023A6EB2008000000019FFFF0C007219090D0D332D214400FA000101FE2FCC4519090D092F30034DD772023A6ECF007F00000019FFFF5A007219090D0D3337214300FA000101FE2FCC4519090D092F3A034DD767023A6EEE007E00000019FFFFC0007219090D0D3405214300FA000101FE2FCC4519090D093008034DD75C023A6F24007F00000019FFFFB609"
				.HexStringToByteArray();

			var device = new AvtofonDevice();

			byte[] bufferRest;
			var result = device.OnData(bytes, bytes.Length, null, out bufferRest);

			Assert.AreEqual(6, result.Count);
		}

		[TestMethod]
		public void WorkPacket()
		{
			var bytes = @"113c9225603445045000010000001b0b0d1029151c0b0d0c0005a046ffffffff030c0d0c00276047ffffffff213c00fa000101fe2fcc000000000000000000000000000000000000000063ffffce"
				.HexStringToByteArray();

			var device = new AvtofonDevice();

			byte[] bufferRest;
			var result = device.OnData(bytes, bytes.Length, null, out bufferRest);

			Assert.AreEqual(1, result.Count);
		}
	}
}