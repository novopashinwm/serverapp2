﻿using System.Linq;
using System.Text;
using Compass.Ufin.Terminal.Tests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RU.NVG.NIKA.Terminal.MayakSled;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class MayakSledTests : BaseDeviceTests<MayakSledDevice>
	{
		private static readonly string[] StringPackets =
		{
			"%NVS61AD05000433502011115040146055043021N082561755E0000223+05250019805161F0000000B091412880  3161",
			"%NVS4FDC050004334*T2#5*T3#5*T4#20*T5#5*N#1*M#1*F#0*CL#06*12#24*24#1000*01#0008*"
		};

		private static readonly byte[][] ValidPackets =
			StringPackets
				.Select(Encoding.ASCII.GetBytes)
				.ToArray();

		[TestMethod]
		public void SupportData()
		{
			foreach (var validPacket in ValidPackets)
			{
				Assert.IsTrue(Device.SupportData(validPacket));
			}
		}

		[TestMethod]
		public void GetDevice()
		{
			foreach (var validPacket in ValidPackets)
			{
				var device = GetDevice(validPacket);
				Assert.AreEqual(Device.GetType(), device.GetType());
			}
		}

		[TestMethod]
		public void OnData()
		{
			foreach (var validPacket in StringPackets)
			{
				var mu = GetMobilUnits(validPacket).FirstOrDefault();
				if (mu != null)
					Assert.AreEqual("250", mu.CellNetworks[0].CountryCode);
			}
		}
	}
}