﻿using System.Linq;
using System.Text;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.GPS608;
using FORIS.TSS.TerminalService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class JB101DeviceTests : BaseDeviceTests<JB101>
	{
		[TestMethod]
		public void JB101_Support()
		{
			CheckSupportDataByASCII("(040100000141BP05000040100000141200307A2838.3211N07702.4776E000.01057020.000001000007L0888C192)");
		}
		[TestMethod]
		public void JB101_OnData()
		{
			var list = GetMobilUnits("(040100000141BP05000040100000141200307A2838.3211N07702.4776E000.01057020.000001000007L0888C192)")
				.Union(GetMobilUnits("(040100001834BR00200307A2838.3216N07702.4782E000.11057290.000001000007L0888C192)"))
				.Union(GetMobilUnits("(040100000141BV000)"));
			Assert.AreEqual(2, list.Count());
		}
		[TestMethod]
		public void JB101_CutOffElectricityCommand()
		{
			//////////////////////////////////////////////////////////////////////
			var commandObj = CommandCreator.CreateCommand(CmdType.CutOffElectricity);
			commandObj.Target.DeviceID = "40100000141";
			//////////////////////////////////////////////////////////////////////
			var bytes = Device.GetCmd(commandObj);
			//////////////////////////////////////////////////////////////////////
			var commandTxt = Encoding.ASCII.GetString(bytes);
			//////////////////////////////////////////////////////////////////////
			Assert.AreEqual("(040100000141AV000)", commandTxt);
		}
		[TestMethod]
		public void JB101_CutOffElectricityAnswer()
		{
			//////////////////////////////////////////////////////////////////////
			var commandAns = OnDataByASCII("(040100000141BV000)")
				?.OfType<NotifyEventArgs>()
				?.FirstOrDefault()
				?.Tag
				?.OfType<CommandResult>()
				?.FirstOrDefault();
			//////////////////////////////////////////////////////////////////////
			Assert.AreEqual(CmdType.CutOffElectricity, commandAns.CmdType);
		}
	}
}