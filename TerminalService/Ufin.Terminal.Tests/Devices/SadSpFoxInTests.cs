﻿using System;
using System.Linq;
using System.Text;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.TerminalService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RU.NVG.Terminal.SadSpFoxIn;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class SadSpFoxInTests : BaseDeviceTests<SadSpFoxInDevice>
	{
		private static readonly byte[][] ValidPackets = new[]
		{
			"SA200STT;356976008525470;111;20081017;07:41:56;00100;+37.478519;+126.886819;000.012;000.00;9;1;0;11.30;698.66;000000;2;1;0;0072",
			"SA200STTH;356976008525470;111;20081017;07:41:56;00100;+37.478519;+126.886819;000.012;000.00;9;1;0;11.30;698.66;000000;2;1;0;0072",
			"SA200EMG;356976008525470;111;20121027;08:31:03;0d643;+28.527383;+077.280768;000.485;000.00;0;0;0;11.73;698.66;000000;1;0;0",
			"SA200EMGH;356976008525470;111;20121027;08:31:03;0d643;+28.527383;+077.280768;000.485;000.00;0;0;0;11.73;698.66;000000;1;0;0",
			"SA200EVT;356976008525470;111;20081017;07:41:56;00100;+37.478519;+126.886819;000.012;000.00;9;1;0;12.00;698.66;110000;1;0;0",
			"SA200EVTH;356976008525470;111;20081017;07:41:56;00100;+37.478519;+126.886819;000.012;000.00;9;1;0;12.00;698.66;110000;1;0;0",
			"SA200ALT;356976008525470;111;20121027;08:31:03;0d643;+28.527383;+077.280768;000.485;000.00;9;1;0; 0.00;695.21;000000;3;0;0",
			"SA200ALTH;356976008525470;111;20121027;08:31:03;0d643;+28.527383;+077.280768;000.485;000.00;9;1;0; 0.00;695.21;000000;3;0;0"
		}.Select(s => Encoding.ASCII.GetBytes(s)).ToArray();


		[TestMethod]
		public void SupportData()
		{
			foreach (var validPacket in ValidPackets)
			{
				Assert.IsTrue(Device.SupportData(validPacket));
			}
		}

		[TestMethod]
		public void OnData()
		{
			foreach (var validPacket in ValidPackets)
			{
				byte[] bufferRest;
				Assert.AreEqual(1, Device.OnData(validPacket, validPacket.Length, null, out bufferRest).Count);
			}
		}

		[TestMethod]
		public void Parse()
		{
			const string packetString =
				"SA200STT;356976008525470;111;20081017;07:41:56;00100;+37.478519;+126.886819;000.012;000.00;9;1;0;11.30;698.66;000000;2;1;0;0072";
			var data = Encoding.ASCII.GetBytes(packetString);

			byte[] bufferRest;
			var mu = (IMobilUnit)Device.OnData(data, data.Length, null, out bufferRest)[0];

			Assert.AreEqual("356976008525470", mu.DeviceID);
			Assert.AreEqual("111", mu.Firmware);
			Assert.AreEqual(new DateTime(2008, 10, 17, 07, 41, 56, DateTimeKind.Utc), TimeHelper.GetDateTimeUTC(mu.Time));
			Assert.IsTrue(Math.Abs(mu.Latitude - 37.478519) < 0.000001);
			Assert.IsTrue(Math.Abs(mu.Longitude - 126.886819) < 0.000001);
			Assert.AreEqual(mu.Speed, 0);
			Assert.AreEqual(mu.Satellites, 9);
			Assert.AreEqual(11300, mu.SensorValues[(int)SadSpFoxInDeviceSensor.MainPowerVoltage]);
			Assert.AreEqual(698660, mu.SensorValues[(int)SadSpFoxInDeviceSensor.WaterLevelSensorVoltage]);
			Assert.AreEqual(0, mu.SensorValues[(int)SadSpFoxInDeviceSensor.Ignition]);
			Assert.AreEqual(0, mu.SensorValues[(int)SadSpFoxInDeviceSensor.Input1TripStartKey]);
			Assert.AreEqual(0, mu.SensorValues[(int)SadSpFoxInDeviceSensor.Input2WaterDispenseKey]);
			Assert.AreEqual(0, mu.SensorValues[(int)SadSpFoxInDeviceSensor.Input3SOSKey]);
			Assert.AreEqual(0, mu.SensorValues[(int)SadSpFoxInDeviceSensor.Output1]);
			Assert.AreEqual(0, mu.SensorValues[(int)SadSpFoxInDeviceSensor.Output2]);
			Assert.AreEqual(1, mu.SensorValues[(int)SadSpFoxInDeviceSensor.Trip]);
			Assert.AreEqual(0, mu.SensorValues[(int)SadSpFoxInDeviceSensor.Panic]);
		}
	}
}