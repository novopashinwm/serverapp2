﻿using System;
using System.Linq;
using Compass.Ufin.Terminal.Devices.MiniFinder;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class MiniFinderDeviceTests : BaseDeviceTests<MiniFinderDevice>
	{
		[TestMethod]
		public void SupportData()
		{
			CheckSupportDataByASCII(@"!1,860719020212514;");
		}

		[TestMethod]
		public void OnData()
		{
			var loginPacket = @"!1,860719020212514;";
			var dataPacket = @"!D,14/10/15,12:3:7,55.739738,37.768299,1,0,1e0001,174.9,51,5,16,0;";

			var mu = GetMobilUnits(loginPacket, dataPacket).Single();
			Assert.AreEqual("860719020212514", mu.DeviceID);
			Assert.IsTrue(mu.ValidPosition);
			Assert.IsTrue(NumberHelper.AreEqual(55.739738, mu.Latitude, 0.000001));
			Assert.IsTrue(NumberHelper.AreEqual(37.768299, mu.Longitude, 0.000001));
			Assert.AreEqual(new DateTime(2015, 10, 14, 12, 3, 7), TimeHelper.GetDateTimeUTC(mu.Time));

			//TODO: проверить состояние тревоги
		}

		[TestMethod]
		public void BadDate()
		{
			var loginPacket = @"!1,860719020212514;";
			var dataPacket = @"!D,16/2/15,10:4:47,55.739330,37.769604,0,0,1b0002,0.0,79,0,13,0;";

			var mu = GetMobilUnits(loginPacket, dataPacket).Single();
			Assert.IsTrue(mu.ValidPosition);
		}
	}
}