﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.AvtoGRAF16;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class AvtoGraf16DeviceTests : BaseDeviceTests<AvtoGRAF16>
	{
		[TestMethod]
		public void Version()
		{
			var bytes = Encoding.ASCII.GetBytes(
				string.Join(Environment.NewLine, new[]
				{
					"@@175000@77420006LCC010000:",
					"----------",
					"HARDWARE=000000BF;HVERSION=13;VERSION=AGTK-10.11;;IMEI=353469040411506;ICCID=8970139905013104344;SUPERPASSWORD=;IP=89.188.108.102;PERIODWR=10;PERIODCOUNT12=0;PERIODCOUNT34=0;PERIODANALOG=200;PERIODSEND=200;PORT=2225;PASSWORD=77420006;ALIAS=;PIN=0000;APNFULL=\"internet.mts.ru\",\"mts\",\"mts\";TELSMS1=;TELSMS2=;TELSMS3=;TELSMS4=;TELSMS5=;TELSMS6=;TELSMS7=;TELSMS8=;INALIAS1=Input 1;INALIAS2=Input 2;INALIAS3=Input 3;INALIAS4=Input 4;INALIAS5=Input 5;"
				}));

			Assert.IsTrue(Device.SupportData(bytes));

			byte[] bufferRest;
			var result = Device.OnData(bytes, bytes.Length, null, out bufferRest);

			var mu = (IMobilUnit)result.Cast<object>().First(x => x is IMobilUnit);
			Assert.AreEqual("175000", mu.DeviceID);
			Assert.AreEqual("AGTK-10.11", mu.Properties[DeviceProperty.Firmware]);
		}

		[TestMethod]
		public void Simple()
		{
			var bytes = @"2444000000745b120000fc6d74733333333333897e198b08f00400010000000ef7e62b987e190b48e4fcf3bcefbf220ef7e69b987e198b04010000009a800d0ef7e63445"
				.HexStringToByteArray();
			Assert.IsTrue(Device.SupportData(bytes));
			byte[] bufferRest;
			Assert.AreEqual(3, Device.OnData(bytes, bytes.Length, new ParserState
			{
				DeviceID = "175000",
				FirmwareVersion = "AGTK-10.11"
			}, out bufferRest).Count);
		}

		[TestMethod]
		public void Bug960023()
		{
			var bytes = "000"
				.HexStringToByteArray();
			Assert.IsFalse(Device.SupportData(bytes));
		}

		private static byte[][] packets = new[]
		{
			"40403238383331324074657374746573744c43423031303030303a0d0a2d2d2d2d0d0a54454c4348414e47453d2b37393136313630343337363b444154454348414e47453d30332e30372e323031333b54494d454348414e47453d30363a31383a34303b0d0a2d2d2d2d2d2d2d2d2d2d0d0a48415244574152453d38303030303246423b4c4f434b3d30303030303030303b4856455253494f4e3d32363b56455253494f4e3d4147544b2d31302e35323b3b4c4f414445523d5553422d322e3141473b3b494d45493d3335353039343034383939323934393b49434349443d383937303133393930353031333130343139353b494d53493d3235303031363137303636373333363b535550455250415353574f52443dff3b49503d3137322e32302e342e3232343b504f52543d31323335303b504552494f4457523d333b504552494f44434f554e5431323d333630303b504552494f44434f554e5433343d333630303b504552494f44414e414c4f473d333630303b504552494f4453454e",
			"443d33303b50415353574f52443d74657374746573743b414c4941533dffffffffffffffffff3b50494e3d373435393b41504e46554c4c3d226e696b612e6d736b222c226d7473222c226d7473223b54454c534d53313dffffffffffffffffffffffffffffff3b",
			"40403238383331324074657374746573744c43393031303030303a54454c534d53323dffffffffffffffffffffffffffffff3b54454c534d53333dffffffffffffffffffffffffffffff3b54454c534d53343dffffffffffffffffffffffffffffff3b54454c534d53353dffffffffffffffffffffffffffffff3b54454c534d53363dffffffffffffffffffffffffffffff3b54454c534d53373dffffffffffffffffffffffffffffff3b54454c534d53383dffffffffffffffffffffffffffffff3b494e414c494153313dffffffffffffffffff3b494e414c494153323dffffffffffffffffff3b494e414c494153333dffffffffffffffffff3b494e414c494153343dffffffffffffffffff3b494e414c494153353dffffffffffffffffff3b494e414c494153363dffffffffffffffffff3b494e414c494153373dffffffffffffffffff3b494e414c494153383dffffffffffffffffff3b4d4f444557523d413b4d4f4445313d313b4d4f4445574944453d313b46554c4c4f4e4c494e453d303b4d4f4445544e3d303b44495354414e43453d303b544f54414c44495354414e43453d31383734303b54454c5550313dffffffffffffffffffffffffffffff3b54454c5550323dffffffffffffffffffffffffffffff3b52494e474f55543d4e3b",
			"24d401000038660400000174657374746573740bdaa1081a40fff3b2cfbf220ef7e60c0bdaa18804000000008f000f0ef7e61d19daa18802030000010000000ef3e61919daa1081a40fff3b2cfbf220ef3e6e619daa18804000000008f000f0ef3e6f732dca108000000a0000000000ef7ee6537dca1089a40fff320d0bf020ef7ee4f37dca1880402005a0000c0f70ef7ee9556dca1085040fff3cecfbf220ef7e6d556dca1880400005a008d000f0ef7e63f75dca1085040fff3cecfbf220ef7e6c575dca1880400005a008d000f0ef7e62f93dca1085040fff3cecfbf220ef7e67a93dca1",
			"880400005a008d000f0ef7e690b2dca1085040fff3cecfbf220ef7e673b2dca1880400005a008d000f0ef7e699d0dca1085040fff3cecfbf220ef7e6e8d0dca1880400005a008d000f0ef7e602efdca1084e40fff3cecfbf220ef7e6b1efdca1880400005a008d000f0ef7e68c0edda1084e40fff3cccfbf220ef7e6f30edda1880400005a008d000f0ef7e6b42ddda1084e40fff3cccfbf220ef7e6e32ddda1880400005a008d000f0ef7e6a44bdda1084e40fff3cacfbf220ef7e6c44bdda1880400005a008d000f0ef7e60d6adda1084c40fff3cacfbf220ef7e67e6adda1880400005a008c000f0ef7e63353",
			 "24d401000038660400000274657374746573740bdaa1081a40fff3b2cfbf220ef7e60c0bdaa18804000000008f000f0ef7e61d19daa18802030000010000000ef3e61919daa1081a40fff3b2cfbf220ef3e6e619daa18804000000008f000f0ef3e6f732dca108000000a0000000000ef7ee6537dca1089a40fff320d0bf020ef7ee4f37dca1880402005a0000c0f70ef7ee9556dca1085040fff3cecfbf220ef7e6d556dca1880400005a008d000f0ef7e63f75dca1085040fff3cecfbf220ef7e6c575dca1880400005a008d000f0ef7e62f93dca1085040fff3cecfbf220ef7e67a93dca1880400005a008d000f0ef7e690b2dca1085040fff3cecfbf220ef7e673b2dca1880400005a008d000f0ef7e699d0dca1085040fff3cecfbf220ef7e6e8d0dca1880400005a008d000f0ef7e602efdca1084e40fff3cecfbf220ef7e6b1efdca1880400005a008d000f0ef7e68c0edda1084e40fff3cccfbf220ef7e6f30edda1880400005a008d000f0ef7e6b42ddda1084e40fff3cccfbf220ef7e6e32ddda1880400005a008d000f0ef7e6a44bdda1084e40fff3cacfbf220ef7e6c44bdda1880400005a008d000f0ef7e60d6adda1084c40fff3cacfbf220ef7e67e6adda1880400005a008c000f0ef7e633d4",
			"24d401000038660400000374657374746573740bdaa1081a40fff3b2cfbf220ef7e60c0bdaa18804000000008f000f0ef7e61d19daa18802030000010000000ef3e61919daa1081a40fff3b2cfbf220ef3e6e619daa18804000000008f000f0ef3e6f732dca108000000a0000000000ef7ee6537dca1089a40fff320d0bf020ef7ee4f37dca1880402005a0000c0f70ef7ee9556dca1085040fff3cecfbf220ef7e6d556dca1880400005a008d000f0ef7e63f75dca1085040fff3cecfbf220ef7e6c575dca1880400005a008d000f0ef7e62f93dca1085040fff3cecfbf220ef7e67a93dca1880400005a008d000f0ef7e690b2dca1085040fff3cecfbf220ef7e673b2dca1880400005a008d000f0ef7e699d0dca1085040fff3cecfbf220ef7e6e8d0dca1880400005a008d000f0ef7e602efdca1084e40fff3cecfbf220ef7e6b1efdca1880400005a008d000f0ef7e68c0edda1084e40fff3cccfbf220ef7e6f30edda1880400005a008d000f0ef7e6b42ddda1084e40fff3cccfbf220ef7e6e32ddda1880400005a008d000f0ef7e6a44bdda1084e40fff3cacfbf220ef7e6c44bdda1880400005a008d000f0ef7e60d6adda1084c40fff3cacfbf220ef7e67e6adda1880400005a008c000f0ef7e6335e",
			"24d401000038660400000474657374746573740bdaa1081a40fff3b2cfbf220ef7e60c0bdaa18804000000008f000f0ef7e61d19daa18802030000010000000ef3e61919daa1081a40fff3b2cfbf220ef3e6e619daa18804000000008f000f0ef3e6f732dca108000000a0000000000ef7ee6537dca1089a40fff320d0bf020ef7ee4f37dca1880402005a0000c0f70ef7ee9556dca1085040fff3cecfbf220ef7e6d556dca1880400005a008d000f0ef7e63f75dca1085040fff3cecfbf220ef7e6c575dca1880400005a008d000f0ef7e62f93dca1085040fff3cecfbf220ef7e67a93dca1880400005a008d000f0ef7e690b2dca1085040ff",
			"f3cecfbf220ef7e673b2dca1880400005a008d000f0ef7e699d0dca1085040fff3cecfbf220ef7e6e8d0dca1880400005a008d000f0ef7e602efdca1084e40fff3cecfbf220ef7e6b1efdca1880400005a008d000f0ef7e68c0edda1084e40fff3cccfbf220ef7e6f30edda1880400005a008d000f0ef7e6b42ddda1084e40fff3cccfbf220ef7e6e32ddda1880400005a008d000f0ef7e6a44bdda1084e40fff3cacfbf220ef7e6c44bdda1880400005a008d000f0ef7e60d6adda1084c40fff3cacfbf220ef7e67e6adda1880400005a008c000f0ef7e633c3"
		}.Select(StringHelper.HexStringToByteArray).ToArray();

		[TestMethod]
		public void TestAccept()
		{
			ParserState state = null;
			byte[] bufferRest = null;
			foreach (var packet in packets)
			{
				var length = (bufferRest != null ? bufferRest.Length : 0) + packet.Length;
				var data = new List<byte>(length);
				if (bufferRest != null)
					data.AddRange(bufferRest);
				data.AddRange(packet);
				var dataArray = data.ToArray();
				var result = Device.OnData(dataArray, dataArray.Length, state, out bufferRest);
				Assert.IsNotNull(result);
				if (bufferRest == null)
				{
					var confirmPacket = result.OfType<ConfirmPacket>().FirstOrDefault();
					Assert.IsNotNull(confirmPacket);
				}

				var receiveState = result.OfType<ReceiverStoreToStateMessage>().FirstOrDefault();
				if (receiveState != null)
					state = (ParserState)receiveState.StateData;
			}
		}

		[TestMethod]
		public void SetupDeviceWithCustomAPN()
		{
			var command = CommandCreator.CreateCommand(CmdType.Setup);
			command.Target = new MobilUnit { Password = "password" };
			command.Params[PARAMS.Keys.InternetApn.Name] = "customAPN";
			command.Params[PARAMS.Keys.InternetApn.User] = "customUser";
			command.Params[PARAMS.Keys.InternetApn.Password] = "customPassword";
			var commandSteps = Device.GetCommandSteps(command);

			Assert.IsTrue(commandSteps.Any(s => (s is SmsCommandStep) && ((SmsCommandStep)s).Text.Contains("customAPN")));
			Assert.IsTrue(commandSteps.Any(s => (s is SmsCommandStep) && ((SmsCommandStep)s).Text.Contains("customUser")));
			Assert.IsTrue(commandSteps.Any(s => (s is SmsCommandStep) && ((SmsCommandStep)s).Text.Contains("customPassword")));
		}

		[TestMethod]
		public void SetupDeviceWithDefaultAPN()
		{
			var command = CommandCreator.CreateCommand(CmdType.Setup);
			command.Target = new MobilUnit { Password = "password" };

			var commandSteps = Device.GetCommandSteps(command);

			Assert.IsTrue(commandSteps.Any(s => (s is SmsCommandStep) && ((SmsCommandStep)s).Text.Contains("internet.mts.ru")));
			Assert.IsTrue(commandSteps.Any(s => (s is SmsCommandStep) && ((SmsCommandStep)s).Text.Contains("mts")));
			Assert.IsTrue(commandSteps.Any(s => (s is SmsCommandStep) && ((SmsCommandStep)s).Text.Contains("mts")));
		}
	}
}