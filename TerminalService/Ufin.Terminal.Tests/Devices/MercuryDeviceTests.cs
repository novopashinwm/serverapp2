﻿using FORIS.TSS.Common.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RU.NVG.Terminal.Mercury;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class MercuryDeviceTests
	{
		private MercuryDevice _device;

		private MercuryDevice Device
		{
			get { return _device ?? (_device = new MercuryDevice()); }
		}

		[TestMethod]
		public void SupportData()
		{
			Assert.IsTrue(Device.SupportData(StringHelper.HexStringToByteArray(
				"10020003010023000146a1df9eef40c01acec5390055065500396ecc00940000000000000004000800ff2001")));
			Assert.IsTrue(Device.SupportData(StringHelper.HexStringToByteArray(
				"10020003020018000146a1df9eef401acec539a9a09050002053b0788088e02001")));
			Assert.IsTrue(Device.SupportData(StringHelper.HexStringToByteArray(
				"10020003030014000146a1df9eef401acec539ffffffffffffffff2001")));
			Assert.IsTrue(Device.SupportData(StringHelper.HexStringToByteArray(
				"10020003010023000146a1df9eef40c01acec5390055065500396ecc00940000000000000004000800ff200110020003020018000146a1df9eef401acec539a9a09050002053b0788088e0200110020003030014000146a1df9eef401acec539ffffffffffffffff2001")));
		}

		[TestMethod]
		public void Parse()
		{
			byte[] data = StringHelper.HexStringToByteArray("10020003010023000146a1df9eef40c01acec5390055065500396ecc00940000000000000004000800ff200110020003020018000146a1df9eef401acec539a9a09050002053b0788088e0200110020003030014000146a1df9eef401acec539ffffffffffffffff2001");
			byte[] bufferRest;
			Assert.AreEqual(
				3,
				Device.OnData(data, data.Length, null, out bufferRest).Count);
		}
	}
}