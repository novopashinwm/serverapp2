﻿using System.Linq;
using System.Text;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.Common.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RU.NVG.NIKA.Terminal.WP2030C;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class WP2030CTests : BaseDeviceTests<WP2030CDevice>
	{
		private static readonly byte Punc = Encoding.ASCII.GetBytes(",").First();

		private static byte[] GetPacket(string sample, byte start, byte end, byte checksum)
		{
			return new[] {(byte)' ', start}.Concat(Encoding.ASCII.GetBytes(sample)).Concat(new[] {end, checksum}).ToArray();
		}

		private readonly byte[][] Samples = {
			GetPacket("ATL862951020494309,$GPRMC,155021.000,A,2829.5967,N,07713.7792,E,0000,93.75,170315,,,D*4B,#01101011000100,0,0,0,0.01041088,0,4,28,404,10,10,43abATL", 0x01, 0x02, 0x3f),
			GetPacket("ATL356895037533745,$GPRMC,111719.000,A,2838.0045,N,07713.3707,E,0.00,,120810,,,A*75$LOC,Connaught Circus  Connaught Place  New Delhi  Delhi  India,#01100111001010,N.C,N.C,N.C,12345.67,31.4,4.2,21,MCC,MNC,LAC,CellID,ATL", 0x01, 0x02, 0x7A),
			GetPacket("ATL356895037533745,$GPRMC,111719.000,A,2838.0045,N,07713.3707,E,0.00,,120810,,,A*75$LOC,Connaught Circus  Connaught Place  New Delhi  Delhi  India,#01100111001010,2.3,35.67,80,12345.67,31.4,4.2,21,MCC,MNC,LAC,CellID,ATL", 0x01, 0x02, 0x7A),
			GetPacket("ATL356895037533745,$GPRMC,111719.000,A,2838.0045,N,07713.3707,E,0.00,, 120810,,,A*75$LOC, NAME NOT FOUND ,#01111001001010,2.3,35.67,80,12345.67,31.4,4.2,21,MCC,MNC,LAC,CellID,ATL", 0x01, 0x02, 0x7A),
			GetPacket("ATL356895037533745,$GPRMC,111719.000,A,2838.0045,N,07713.3707,E,0.00,,120810,,,A*75#01100111001010,2.3,35.67,80,12345.67,31.4,4.2,21,MCC,MNC,LAC,CellID,ATL", 0x01, 0x02, 0x7A),
			GetPacket("ATL356895037533745,$GPRMC,111719.000,A,2838.0045,N,07713.3707,E,0.00,,120810,,,A*75$LOC,Connaught Circus  Connaught Place  New Delhi  Delhi  India,#01100101101010,2.3,35.67,80,12345.67,31.4,4.2,21,MCC,MNC,LAC,CellID,ATL", 0x03, 0x04, 0x7A),
		};

		private readonly byte[] BugBytes =
			StringHelper.HexStringToByteArray(
				"20-01-41-54-4C-38-36-32-39-35-31-30-32-30-34-39-34-33-30-39-2C-24-47-50-52-4D-43-2C-31-35-35-30-32-31-2E-30-30-30-2C-41-2C-32-38-32-39-2E-35-39-36-37-2C-4E-2C-30-37-37-31-33-2E-37-37-39-32-2C-45-2C-30-30-30-30-2C-39-33-2E-37-35-2C-31-37-30-33-31-35-2C-2C-2C-44-2A-34-42-2C-23-30-31-31-30-31-30-31-31-30-30-30-31-30-30-2C-30-2C-30-2C-30-2C-30-2E-30-31-30-34-31-30-38-38-2C-30-2C-34-2C-32-38-2C-34-30-34-2C-31-30-2C-31-30-2C-34-33-61-62-41-54-4C-02-3F");

		private readonly byte[] BuggyBytes = StringHelper.HexStringToByteArray("01-41-54-4C-38-36-35-39-30-34-30-32-31-39-33-33-34-31-38-2C-24-47-50-52-4D-43-2C-31-33-30-32-33-33-2E-30-30-30-2C-41-2C-32-38-33-32-2E-30-36-35-33-2C-4E-2C-30-37-37-32-35-2E-34-36-32-35-2C-45-2C-30-30-30-30-2C-31-35-2E-34-38-2C-33-30-30-33-31-35-2C-2C-2C-44-2A-34-30-2C-23-30-31-31-30-31-30-31-31-30-30-30-31-30-30-2C-30-2C-30-2C-30-2C-31-37-39-33-33-2E-39-35-2C-34-31-2C-34-2C-32-35-2C-34-30-34-2C-31-30-2C-37-62-2C-64-65-61-30-41-54-4C-02-58");

		[TestMethod]
		public void TestSample()
		{
			foreach (var sample in Samples)
			{
				Assert.IsTrue(Device.SupportData(sample));
			}

			byte[] bufferRest;
			foreach (var sample in Samples)
			{
				Assert.AreEqual(Device.OnData(sample, sample.Length, null, out bufferRest).Count, 1);
			}
		}

		[TestMethod]
		public void TestBug()
		{
			var device = GetDevice(BugBytes);

			byte[] bufferRest;
			Assert.AreEqual(device.OnData(BugBytes, BugBytes.Length, null, out bufferRest).Count, 1);
		}

		[TestMethod]
		public void TestBuggyBytes()
		{
			var device = GetDevice(BuggyBytes);
			byte[] bufferRest;
			Assert.IsNotNull(device.OnData(BuggyBytes, BuggyBytes.Length, null, out bufferRest));
		}
	}
}