﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.Common;
using FORIS.TSS.Terminal.Devices;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class DevNullDeviceTests : BaseDeviceTests<DevNullDevice>
	{
		private readonly Random _devRand = new Random();
		private readonly byte[] _devNull = Encoding.ASCII.GetBytes("/dev/null");
		/// <summary> Тестирование метода: <see cref="TestDevNullDevice.SupportData(byte[])"/> </summary>
		[TestMethod]
		public void DevNull_SupportData()
		{
			var data = _devNull.Concat(
				Enumerable.Repeat((byte)_devRand.Next(0, 255), _devRand.Next(0, 100))).ToArray();
			using (new Stopwatcher(MethodBase.GetCurrentMethod().Name, null, TimeSpan.Zero))
			{
				CheckSupportDataByBytes(data);
			}
		}
		/// <summary> Тестирование метода: <see cref="TestDevNullDevice.OnData(byte[], int, object, out byte[])"/> </summary>
		[TestMethod]
		public void DevNull_OnData()
		{
			var data = _devNull.Concat(
				Enumerable.Repeat((byte)_devRand.Next(0, 255), _devRand.Next(0, 100))).ToArray();
			using (new Stopwatcher(MethodBase.GetCurrentMethod().Name, null, TimeSpan.Zero))
			{
				var responses = OnDataByBytes(data);
				Assert.IsNotNull(responses);
				Assert.IsFalse(responses.Count > 0);
			}
		}
	}
}