﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Compass.Ufin.Terminal.Tests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RU.NVG.Terminal.SadSpFoxIn;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class SadSpFoxInDeviceTests : BaseDeviceTests<SadSpFoxInDevice>
	{
		private static readonly List<byte[]> Packets = new[]
			{
				@"SA200EMG;356976008525470;111;20121027;08:31:03;0d643;+28.527383;+077.280768;000.485;000.00;0;0;0;11.73;698.66;000000;1;0;0",
				@"SA200EMGH;356976008525470;111;20121027;08:31:03;0d643;+28.527383;+077.280768;000.485;000.00;0;0;0;11.73;698.66;000000;1;0;0"
			}.Select(Encoding.ASCII.GetBytes).ToList();

		private const string HistoryData = @"$<GPS.History.Read>
SA200STTH;356976008525470;111;20081017;07:41:56;00100;+37.478519;+126.886819;000.012;000.00;9;1;0;11.30; 698.66;000000;2;1;0;0072
SA200STTH;356976008525470;111;20081017;07:41:56;00100;+37.478519;+126.886819;000.012;000.00;9;1;0;11.30; 698.66;000000;2;1;0;0072
SA200STTH;356976008525470;111;20081017;07:41:56;00100;+37.478519;+126.886819;000.012;000.00;9;1;0;11.30; 698.66;000000;2;1;0;0072
SA200STTH;356976008525470;111;20081017;07:41:56;00100;+37.478519;+126.886819;000.012;000.00;9;1;0;11.30; 698.66;000000;2;1;0;0072
$SUCCESS
$<end>";

		private const string Bug969036Sample = @"
25.700;0.000;000000;2;0;0;0
SA200STT;358696049347175;111;20150318;09:30:23;9E96;+28.4336459;+077.136350;0;63.81;8;1;3656460;25.700;0.000;000000;2;0;0;0
SA200STT;358696049347175;111;20150318;09:30:33;9E96;+28.4336593;+077.136355;0;63.81;9;1;3656460;25.700;0.000;000000;2;0;0;0
$<GPS.History.Read>
SA200STT;358696049347175;111;20150318;09:30:47;9E96;+28.4336362;+077.136342;0;63.81;9;1;3656460;25.659;0.000;000000;2;0;0;0
SA200STT;358696049347175;111;20150318;09:30:57;9E96;+28.4336356;+077.136341;0;63.81;8;1;3656460;25.659;0.000;000000;2;0;0;0
SA200STT;358696049347175;111;20150318;09:31:07;9E96;+28.4336198;+077.136324;0;63.81;8;1;3656460;25.659;0.000;000000;2;0;0;0
$readout completed
$SUCCESS
$<end>
SA200STT;358696049347175;111;20150318;09:31:17;9E96;+28.4336109;+077.136310;0;63.81;8;1;3656460;";

		[TestMethod]
		public void SupportData()
		{
			foreach (var packet in Packets)
			{
				Assert.IsTrue(Device.SupportData(packet));
			}
		}

		[TestMethod]
		public void OnData()
		{
			foreach (var packet in Packets)
			{
				byte[] bufferRest;
				Assert.AreNotEqual(0, Device.OnData(packet, packet.Length, null, out bufferRest).Count);
			}
		}

		[TestMethod]
		public void OnHistoryData()
		{
			var packet = Encoding.ASCII.GetBytes(HistoryData);
			Assert.IsTrue(Device.SupportData(packet));
			byte[] bufferRest;
			Assert.AreEqual(4, Device.OnData(packet, packet.Length, null, out bufferRest).Count);
		}

		[TestMethod]
		public void Bug969036()
		{
			var packet = Encoding.ASCII.GetBytes(Bug969036Sample);
			byte[] bufferRest;
			Assert.AreEqual(Device.OnData(packet, packet.Length, null, out bufferRest).Count, 5);
		}
	}
}