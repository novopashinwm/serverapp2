﻿using System;
using System.Linq;
using System.Text;
using Compass.Ufin.Terminal.Devices.MegaStek;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class MegaStekDeviceTests : BaseDeviceTests<MegaStekDevice>
	{
		[TestMethod]
		public void SupportData()
		{
			var stringData = "$MGV002,860719021420652,,S,191216,204803,V,5549.62765,N,03739.30239,E,00,00,00,99.9,,,153.5,,250,02,1E39,52D7,19,,,,,,,,,10,100,Timer;!";

			CheckSupportDataByASCII(stringData);
		}

		[TestMethod]
		public void SingleData()
		{
			var stringData =
				"$MGV002,860719021420652,,S,191216,204803,V,5549.62765,N,03739.30239,E,00,00,00,99.9,,,153.5,,250,02,1E39,52D7,19,,,,,,,,,10,100,Timer;!";

			var result = OnDataByASCII(stringData);

			var mu = result.OfType<IMobilUnit>().Single();

			var sb = new StringBuilder();
			Helper.Append(sb, mu);
			Console.WriteLine(sb.ToString());

			Assert.IsFalse(mu.ValidPosition);
			Assert.AreEqual("860719021420652", mu.DeviceID);
			Assert.AreEqual(new DateTime(2016, 12, 19, 20, 48, 03, DateTimeKind.Utc), TimeHelper.GetDateTimeUTC(mu.Time));
		}

		[TestMethod]
		public void SingleData2()
		{
			var stringData =
				"$MGV002,860719020193193,DeviceName,R,240214,104742,A,2238.20471,N,11401.97967,E,00,03,00,1.20,0.462,356.23,137.9,1.5,460,07,262C,0F54,25, 0000,0000,0,0,0,28.5,28.3,,10,100,Timer;!";

			var result = OnDataByASCII(stringData);

			var mu = result.OfType<IMobilUnit>().Single();

			var sb = new StringBuilder();
			Helper.Append(sb, mu, ((IDevice)Device).SensorEnumType);
			Console.WriteLine(sb.ToString());

			Assert.IsTrue(mu.ValidPosition);
			Assert.AreEqual("860719020193193", mu.DeviceID);
			Assert.AreEqual(new DateTime(2014, 02, 24, 10, 47, 42, DateTimeKind.Utc), TimeHelper.GetDateTimeUTC(mu.Time));
		}

	}
}