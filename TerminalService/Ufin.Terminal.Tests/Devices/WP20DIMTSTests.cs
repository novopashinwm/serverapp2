﻿using System.Linq;
using System.Text;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.Common.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RU.NVG.NIKA.Terminal.WP20DIMTS;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class WP20DIMTSTests : BaseDeviceTests<WP20DIMTSDevice>
	{
		private static byte[] GetPacket(string sample, byte checksum)
		{
			return Encoding.ASCII.GetBytes(sample).Concat(new[] { checksum }).ToArray();
		}

		private readonly byte[][] Samples = {
			GetPacket("ATL,L,356895037533745,T, A5_900_GM_DIMTS_FH,A,111719,120810, 2838.0045,N,07713.3707,E,73.8, 231.8, 8, 233.5,01100101,N.C,N.C,N.C,12345.67,4.2,21,MCC,MNC,LAC,CellID", 0x7A),
			GetPacket("ATL,L,356895037533745,A, A5_900_GM_DIMTS_FH, A,111719,120810, 2838.0045,N,07713.3707,E,73.8, 231.8,8, 233.5,01100101,N.C,N.C,N.C,12345.67,4.2,21,MCC,MNC,LAC,CellID", 0x7A),
			GetPacket("ATL,H,356895037533745,T, A5_900_GM_DIMTS_FH, A,111719,120810, 2838.0045,N,07713.3707,E,73.8, 231.8,8, 233.5,01100101,N.C,N.C,N.C,12345.67,4.2,21,MCC,MNC,LAC,CellID", 0x7A),
		};

		private readonly byte[] PacketBytes =
			StringHelper.HexStringToByteArray(
				"41544c2c4c2c3836323935313032303530323430382c542c41355f3930305f474d5f44494d54535f46482c412c3131303533302c3234303331352c323832392e333236342c4e2c30373730352e383135352c452c302e302c33302e33362c302c302c31303130303030302c302c302c302c302e303030312c342c32372c3430342c31302c3066662c353037362c35");

		[TestMethod]
		public void TestSample()
		{
			foreach (var sample in Samples)
			{
				Assert.IsTrue(Device.SupportData(sample));
			}

			byte[] bufferRest;
			foreach (var sample in Samples)
			{
				Assert.AreEqual(Device.OnData(sample, sample.Length, null, out bufferRest).Count, 1);
			}
		}

		[TestMethod]
		public void TestBytes()
		{
			var device = GetDevice(PacketBytes);

			byte[] bufferRest;
			Assert.AreEqual(device.OnData(PacketBytes, PacketBytes.Length, null, out bufferRest).Count, 1);
		}
	}
}