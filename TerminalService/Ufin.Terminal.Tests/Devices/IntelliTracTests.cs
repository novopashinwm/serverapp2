﻿using System.Collections;
using System.Linq;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.IntelliTrac;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class IntelliTracTests : BaseDeviceTests<IntelliTrac>
	{
		[TestMethod]
		public void ParseSynPacket()
		{
			var result = OnDataByHex("fa:f8:b6:dd:ff:ca:9a:3b");
			Assert.IsTrue(result.OfType<ConfirmPacket>().Any());
			Assert.IsTrue(HasMobileUnitWithDeviceID(result));
		}

		[TestMethod]
		public void ParseNavigation()
		{
			var result = OnDataByHex("31:30:30:30:30:30:30:32:35:35:2c:32:30:31:36:30:32:31:30:31:31:34:39:31:36:2c:38:32:2e:38:38:30:33:34:35:2c:35:34:2e:39:39:32:32:35:35:2c:33:37:2c:32:36:35:2c:39:39:2c:38:2c:32:2c:31:2c:30:2c:30:2e:30:36:34:2c:30:2e:30:36:32:0d:0a");

			Assert.IsTrue(HasMobileUnitWithDeviceID(result));
			Assert.IsTrue(result.OfType<IMobilUnit>().Any());
		}

		private static bool HasMobileUnitWithDeviceID(IList result)
		{
			return result.OfType<ReceiverStoreToStateMessage>().All(x => ((ReceiverState)x.StateData).DeviceID == "1000000255");
		}

		[TestMethod]
		public void ParseCommandAnswer()
		{
			const string answerHex = "24:4f:4b:3a:54:52:41:43:4b:49:4e:47:0d:0a";
			var result = OnDataByHex(answerHex);
			var commandResult = (CommandResult)((object[])result.Cast<NotifyEventArgs>().Single().Tag)[0];
			Assert.AreEqual(CmdResult.Completed, commandResult.CmdResult);
			Assert.AreEqual(CmdType.SetSettings, commandResult.CmdType);
		}
	}
}