﻿using System;
using System.Linq;
using System.Text;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.Terminal.GPS608.V2;
using FORIS.TSS.TerminalService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class GPS608DeviceTests : BaseDeviceTests<GPS608>
	{
		[TestMethod]
		public void EverythingIsOff()
		{
			CheckSensors("(008130691969BP05000008130691969120207V0000.0000N00000.0000E000.01143220000000000000000L0000009A)",
				mu => mu.SensorValues.Values.All(v => v == 0));
		}

		[TestMethod]
		public void HeadLightsOn()
		{
			CheckDigitalSensor(
				"(008130691969BP05000008130691969120201A2830.0098N07704.7788E000.008402812.0601001000000L0000000B)",
				GPS608Sensors.Pin03LightsOrange,
				true);
		}

		[TestMethod]
		public void HeadLightsOff()
		{
			CheckDigitalSensor(
				"(008130691969BP05000008130691969120201A2830.0098N07704.7788E000.008483812.0600001000000L0000000B)",
				GPS608Sensors.Pin03LightsOrange,
				false);
		}

		[TestMethod]
		public void IgnitionIsOn()
		{
			CheckDigitalSensor("(008130691969BP05000008130691969120207A2837.7703N07713.4524E000.012171249.3760003000000L00000056)",
				GPS608Sensors.Pin14GreenIgnition, true);
		}

		[TestMethod]
		public void DoorIsOn()
		{
			CheckDigitalSensor("(008130691969BP05000008130691969120207A2837.7703N07713.4524E000.012220249.3760007000000L00000056)",
				GPS608Sensors.Pin13BrownDoorControl, true);
		}

		private void CheckSensors(string packet, Func<IMobilUnit, bool> predicate)
		{
			var bytes = Encoding.ASCII.GetBytes(packet);
			byte[] bufferRest;
			var packets = Device.OnData(bytes, bytes.Length, null, out bufferRest);

			Assert.IsTrue(packets.OfType<IMobilUnit>().All(predicate));
		}

		private void CheckDigitalSensor(string packet, GPS608Sensors sensor, bool value)
		{
			long valueToCompare = value ? 1 : 0;

			CheckSensors(packet,
				mu =>
				{
					long headlight;
					return
						mu.SensorValues.TryGetValue((int)sensor, out headlight) &&
						headlight == valueToCompare;
				});
		}
	}
}