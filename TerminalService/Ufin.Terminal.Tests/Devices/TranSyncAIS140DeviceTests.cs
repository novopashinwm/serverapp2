﻿using System;
using System.Linq;
using System.Text;
using Compass.Ufin.Terminal.Devices.TranSync;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class TranSyncAIS140DeviceTests : BaseDeviceTests<TranSyncAIS140Device>
	{
		private static readonly Random randomObj = new Random();
		[TestMethod]
		public void TranSyncAIS140_Support()
		{
			foreach (var packet in packets)
				CheckSupportDataByBytes(packet);
		}
		[TestMethod]
		public void TranSyncAIS140_OnData()
		{
			Assert.AreNotEqual(0, GetResults(packets).Count());
		}
		/// <summary> Набор пакетов </summary>
		private readonly byte[][] packets = new[]
		{
			"3a:3a:2b:00:e3:08:68:99:70:37:72:41:17:09:49:10:14:01:11:10:3b:35:03:11:94:f0:08:44:9b:70:00:00:00:00:61:34:07:0a:00:06:0e:2a:03:63:00:00:01:05:00:89:61:59:04:02:05:00:00:00:00:00:05:02:30:a4:23:23",
			"3a:3a:2b:00:e3:08:68:99:70:37:72:41:17:02:21:10:14:01:11:0e:34:31:03:11:92:28:08:44:9b:60:00:00:00:00:61:34:87:0a:00:06:0d:2a:03:63:00:00:01:05:00:89:61:59:04:02:05:00:00:00:00:00:05:02:30:fc:23:23",
			"3a:3a:2b:00:e3:08:68:99:70:37:72:41:17:09:68:10:14:01:11:11:01:15:03:11:94:c8:08:44:9b:a0:00:00:00:00:61:34:07:00:00:06:00:2a:03:3f:00:05:01:05:00:89:61:59:04:02:05:00:00:00:00:00:05:02:30:d0:23:23",
		}.Select(StringHelper.HexStringToByteArray).ToArray();
	}
}