﻿using System;
using System.Collections.Generic;
using System.Linq;
using Compass.Ufin.Terminal.Tests.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RU.NVG.NIKA.Terminal.Smarts;

namespace Compass.Ufin.Terminal.Tests.Devices
{
	[TestClass]
	public class SmartsDeviceTests : BaseDeviceTests<SmartsDevice>
	{
		private static readonly List<byte[]> InitPackets =
			new[]
				{
					"78781101035349204905646010123202000bdfa00d0a",
					"78780d010358899050697211000c3f410d0a"
				}
				.Select(StringHelper.HexStringToByteArray).ToList();

		private static readonly List<byte[]> DataPackets = new[]
			{
				@"78 78 1f 12 0e 05 1e 11  26 38 c7 05 fa 75 15 04 09 c3 7d 01 14 01 00 fa  01 01 ff 00 8a a7 00 3f 49 d3 0d 0a",
				"787825160e0517123733c005fa72960409ca880115320900fa0101ff002fcc1006040202001382b20d0a",
				"78781f120e0517123733c005fa72960409ca8801153200fa0101ff002fcc000a8ef00d0a",
				"78781f120e0517123733c005fa72960409ca8801153200fa0101ff002fcc000b9f790d0a",
				"78781f120e051e0a290fc505fa71980409c8e800140000fa0101ff002fcc0027a50b0d0a",
				"78783b1800000000000000fa0101ff002fcc3e01ff009f664101ff0046334301ff009f654401ff0046374801ff002fc949020300a9834aff000200053a1d0d0a",
				"7878471e0e051d16392cc505fa72570409ca7c00153f00fa0101ff002fcc3a01ff009f663d0000000000ff0000000000ff0000000000ff0000000000ff0000000000ffff00020046c9bd0d0a"
			}
			.Select(StringHelper.HexStringToByteArray).ToList();

		[TestMethod]
		public void SupportData()
		{
			foreach (var bytes in InitPackets)
			{
				Assert.IsTrue(Device.SupportData(bytes));
			}
		}
		[TestMethod]
		public void OnData()
		{
			foreach (var bytes in DataPackets)
			{
				byte[] bufferRest;
				var result = Device.OnData(bytes, bytes.Length, new ReceiverState(), out bufferRest);
				Assert.IsNull(bufferRest);
				Assert.IsNotNull(result);
			}
		}
		[TestMethod]
		public void Test932606()
		{
			var loginPacket = StringHelper.HexStringToByteArray("78780d0103534920490085370001d1d10d0a");
			var locationPackets = @"78781f120e0602170125c00600a10a04059bd301149c00fa0101ff008aa6000252c70d0a
78781f120e0602170125c00600a10a04059bd301149c00fa0101ff008aa6000437f10d0a
78781f120e0602170125c00600a10a04059bd301149c00fa0101ff008aa6000526780d0a
78781f120e0602170125c00600a10a04059bd301149c00fa0101ff008aa6000614e30d0a
78781f120e0602170125c00600a10a04059bd301149c00fa0101ff008aa60007056a0d0a
78781f120e0602170125c00600a10a04059bd301149c00fa0101ff008aa60008fd9d0d0a
78781f120e0602170125c00600a10a04059bd301149c00fa0101ff008aa60009ec140d0a
78781f120e0602170125c00600a10a04059bd301149c00fa0101ff008aa60009ec140d0a78780a130406040002000ac4010d0a
78781f120e0602170125c00600a10a04059bd301149c00fa0101ff008aa6000bcf060d0a
78781f120e0602170125c00600a10a04059bd301149c00fa0101ff008aa6000cbbb90d0a
78781f120e0602170125c00600a10a04059bd301149c00fa0101ff008aa6000daa300d0a"
				.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
				.Select(StringHelper.HexStringToByteArray)
				.ToList();

			var heartbitPacket = StringHelper.HexStringToByteArray("78780a130406040002000359c00d0a");
			var packets = new List<byte[]> {loginPacket, heartbitPacket};
			packets.AddRange(locationPackets);
			foreach (var packet in packets)
			{
				byte[] bufferRest;
				var result = Device.OnData(packet, packet.Length, null, out bufferRest);
				Assert.IsNotNull(result);
			}
		}
		[TestMethod]
		public void Test()
		{
			var state = new ReceiverState
				{
					DeviceID = "123456789012346"
				};

			var locationPacket = StringHelper.HexStringToByteArray("787825160f010e0c262fc005fa780f0409cb2701151b0900fa0101ff008aa61006040202015199010d0a");
			byte[] bufferRest;
			var result = Device.OnData(locationPacket, locationPacket.Length, state, out bufferRest);
			Assert.IsNull(bufferRest);
			Assert.IsNotNull(result);

			var heartbeatPacket = StringHelper.HexStringToByteArray("78780a13440604000204f659840d0a");
			result = Device.OnData(heartbeatPacket, heartbeatPacket.Length, state, out bufferRest);
			Assert.IsNull(bufferRest);
			Assert.IsNotNull(result);
		}
		[TestMethod]
		public void Bug964084()
		{
			byte[] bufferRest;
			var bytes1 = StringHelper.HexStringToByteArray("78-78-11-01-03-58-74-00-50-05-24-88-10-12-00-02-00-01-7E-15-0D-0A".Replace("-", string.Empty));
			var result1 = Device.OnData(bytes1, bytes1.Length, null, out bufferRest);
			Assert.IsNotNull(result1);

			// Контрольная сумма рассчитана от первого пакета
			var bytes2 = StringHelper.HexStringToByteArray("78-78-1F-12-0F-03-0B-09-02-38-C8-03-1F-38-F0-08-24-C8-20-35-D5-14-01-94-60-00-AA-00-47-C5-00-D0-0B-01-0D-0A-78-78-1F-12-0F-03-0B-09-03-1A-C8-03-1F-3D-30-08-24-A5-30-36-D5-15-01-94-60-02-44-00-98-D2-00-D1-CF-01-0D-0A".Replace("-", string.Empty));
			var result2 = Device.OnData(bytes2, bytes2.Length, null, out bufferRest);
			Assert.IsNotNull(result2);
		}
		[TestMethod]
		public void SmartsDevice_BadPacket()
		{
			byte[] bufferRest;
			var bytes  = StringHelper.HexStringToByteArray("78-78-0A-13-04-06-04-00-02-0D-0A-74-79-0D-0A");
			var result = Device.OnData(bytes, bytes.Length, null, out bufferRest);
			Assert.IsNotNull(result);
		}
		[TestMethod]
		public void ChecksumTest()
		{
			var data = StringHelper.HexStringToByteArray(
				"78781f120f0a070d3a1eca0311f148084546101f54c601940a008b00cb30053dbf4c0d0a");
			var reader = new SmartsDeviceReader(data);
			var calculatedChecksum = reader.GetChecksum();
			var receivedChecksum = (data[data.Length - 4] << 8) + data[data.Length - 3];
			Assert.AreEqual(receivedChecksum, calculatedChecksum);
		}
		[TestMethod]
		public void GT06N()
		{
			foreach (var s in new []
			{
				"78780d01035841102014903200f1dbbc0d0a",
				"78781F120F070A042A18CA03102230084A140015D42F01940A00920086F3057878190D0A"
			})
			{
				var bytes = StringHelper.HexStringToByteArray(s);
				if (bytes.Length == 18)
					Assert.IsTrue(Device.SupportData(bytes));
				byte[] bufferRest;
				var result = Device.OnData(bytes, bytes.Length, null, out bufferRest);
				Assert.IsNull(bufferRest);
				Assert.IsNotNull(result);
			}
		}
		[TestMethod]
		public void GpsLbsMergedData()
		{
			const string hex = @"787822220f0a090b0028c8030fb60808489f9000140801940a016e006fcd00000000354a870d0a";
			var bytes = StringHelper.HexStringToByteArray(hex);
			byte[] bufferRest;
			var result = Device.OnData(bytes, bytes.Length, null, out bufferRest);
			Assert.IsNull(bufferRest);
			Assert.IsNotNull(result);
			
			Assert.IsTrue(result.OfType<IMobilUnit>().Single().ValidPosition);
		}
		[TestMethod]
		public void BugNothingToReadData()
		{
			const string hex = @"78-78-1F-12-13-0A-16-01-0D-0A-CA-01-6A-35-74-08-54-8B-90-2B-D4-22-01-94-56-81-4D-00-78-29-32-33-50-24-0D-0A-78-78-1F-12-13-0A-16-01-0D-12-CA-01-6A-39-C8-08-54-8E-90-17-D4-25-01-94-56-81-4D-00-78-29-32-36-DD-21-0D-0A-78-78-1F-12-13-0A-16-01-0D-1A-CA-01-6A-3C-F6-08-54-91-00-20-D4-20-01-94-56-81-4D-00-78-29-32-3B-E7-97-0D-0A-78-78-1F-12-13-0A-16-01-0D-23-CA-01-6A-42-1C-08-54-94-70-2D-D4-20-01-94-56-81-4D-00-78-29-32-3E-4D-1B-0D-0A-78-78-1F-12-13-0A-16-01-0D-2B-CA-01-6A-45-FE-08-54-97-20-22-D4-23-01-94-56-81-4D-00-78-29-32-43-BF-37-0D-0A-78-78-1F-12-13-0A-16-01-0D-32-CA-01-6A-48-68-08-54-98-E0-0E-D4-2B-01-94-56-81-4D-00-78-29-32-46-89-26-0D-0A-78-78-1F-12-13-0A-16-01-0D-3A-CA-01-6A-4A-24-08-54-9A-10-14-D4-24-01-94-56-81-4D-00-78-29-32-4A-83-09-0D-0A-78-78-1F-12-13-0A-16-01-0E-06-CA-01-6A-4C-9E-08-54-9C-40-1A-D4-2C-01-94-56-81-4D-00-78-29-32-4E-56-31-0D-0A-78-78-1F-12-13-0A-16-01-0E-0D-CA-01-6A-4F-DC-08-54-9F-90-30-D4-2C-01-94-56-81-4D-00-78-29-32-52-AE-0A-0D-0A-78-78-1F-12-13-0A-16-01-0E-15-CA-01-6A-55-2C-08-54-A6-90-44-D4-38-01-94-56-81-4D-00-78-29-32-56-AC-DC-0D-0A";
			var bytes = StringHelper.HexStringToByteArray(hex);
			byte[] bufferRest;
			var result = Device.OnData(bytes, bytes.Length, null, out bufferRest);
			Assert.IsNull(bufferRest);
			Assert.IsNotNull(result);
		}
		[TestMethod]
		public void BugValueMustBeBetween1and34()
		{
			const string hex = @"78-78-1F-12-13-0A-15-15-07-01-CF-02-78-64-F0-07-C6-A3-30-00-44-F4-01-94-62-15-FF-00-75-4B-05-0D-0A-FA-0D-0A-78-78-1F-12-13-0A-15-15-0F-15-CF-02-78-64-F0-07-C6-A3-30-00-44-F4-01-94-62-15-FF-00-75-4B-05-19-7D-43-0D-0A-78-78-1F-12-13-0A-15-15-10-01-CF-02-78-64-F0-07-C6-A3-30-00-44-F4-01-94-62-15-FF-00-75-4B-05-1D-0E-A3-0D-0A-78-78-1F-12-13-0A-15-15-13-02-CF-02-78-64-F0-07-C6-A3-30-00-44-F4-01-94-62-15-FF-00-75-4B-05-23-A9-D8-0D-0A-78-78-1F-12-13-0A-15-15-1C-00-C4-02-78-6B-7C-07-C6-92-58-00-55-3E-01-94-62-15-FF-00-75-4B-00-09-35-AF-0D-0A-78-78-1F-12-13-0A-15-15-1D-15-C5-02-78-6B-DC-07-C6-91-F8-00-45-1A-01-94-62-15-FF-00-75-4B-00-0F-FB-4F-0D-0A-78-78-1F-12-13-0A-15-15-1E-0B-C5-02-78-6B-DC-07-C6-91-F8-00-45-1A-01-94-62-15-FF-00-75-4B-00-15-37-C9-0D-0A-78-78-1F-12-13-0A-15-15-25-1F-C5-02-78-6B-DC-07-C6-91-F8-00-45-1A-01-94-62-15-FF-00-75-4B-00-21-20-90-0D-0A-78-78-1F-12-13-0A-15-15-2C-01-C5-02-78-6B-DC-07-C6-91-F8-00-45-1A-01-94-62-15-FF-00-75-4B-00-34-68-6B-0D-0A-78-78-1F-12-13-0A-15-15-2C-1F-C5-02-78-6B-DC-07-C6-91-F8-00-45-1A-01-94-62-15-FF-00-75-4B-00-38-3C-96-0D-0A";
			var bytes = StringHelper.HexStringToByteArray(hex);
			byte[] bufferRest;
			var result = Device.OnData(bytes, bytes.Length, null, out bufferRest);
			Assert.IsNull(bufferRest);
			Assert.IsNotNull(result);
		}
		[TestMethod]
		public void Gt06OnlineCommandAnswerPacket()
		{
			const string hex = @"78:78:0d:01:03:53:70:10:93:46:38:63:00:4c:43:17:0d:0a"
				+ @"78:78:0a:13:46:06:04:00:02:00:4d:02:07:0d:0a"
				+ @"78:78:1f:12:14:03:17:05:22:06:c6:03:12:96:60:08:43:fd:e0:00:d4:22:01:94:0b:04:34:00:6a:e6:00:4e:fa:a1:0d:0a"
				+ @"78:78:3b:15:33:17:00:00:00:43:75:74:20:6f:66:66:20:74:68:65:20:66:75:65:6c:20:73:75:70:70:6c:79:3a:20:53:75:63:63:65:73:73:21:20:53:70:65:65:64:3a:20:30:6b:6d:2f:68:2e:00:02:00:51:fd:b4:0d:0a";
			var bytes = StringHelper.HexStringToByteArray(hex);
			byte[] bufferRest;
			var result = Device.OnData(bytes, bytes.Length, null, out bufferRest);
			Assert.IsNull(bufferRest);
			Assert.IsNotNull(result);
		}
	}
}