﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Net;
using System.Text;

using FORIS.TSS.Common;
using FORIS.TSS.Communication;

namespace FORIS.TSS.Terminal.SGGB
{
	/// <summary>
	/// Датаграмма для обмена данными с мобильным устройством SGGB
	/// </summary>
	public abstract class SGGBDatagram : Datagram
	{
		protected static TraceSwitch tsSGGB = 
			new TraceSwitch("TraceSGGB", "SGGB datagram related info");

		#region .ctor

		public SGGBDatagram()
		{
		}

		public SGGBDatagram(uint capacity) : base(capacity)
		{
		}

		#endregion // .ctor

		#region CRC

		/// <summary>
		/// CRC_ID – контрольная сумма полей ID отправителя, ID пакета и ДП
		/// </summary>
		public int CRC_ID
		{
			get
			{
				return IntFromBytesBE(bytes[14], bytes[15]);
			}
			set
			{
				IntToBytesBE(value, bytes, 14, 2);
			}
		}

		/// <summary>
		/// CRC_ID – контрольная сумма полей ID отправителя, ID пакета и ДП.
		/// </summary>
		/// <returns>контрольная сумма 1</returns>
		protected int CalcCRC_ID()
		{
			int crc = 0;
			for (int i = 1; i < 14; i++) crc += bytes[i];

			return crc;
		}

		/// <summary>
		/// build and set CRC_ID
		/// </summary>
		/// <returns>crc</returns>
		protected int BuildCRC_ID()
		{
			return CRC_ID = CalcCRC_ID();
		}

		/// <summary>
		/// CRC in datagram
		/// </summary>
		protected override int CRC
		{
			get
			{
				return IntFromBytesBE(bytes[Size - 2], bytes[Size - 1]);
			}
			set
			{
				IntToBytesBE(value, bytes, Size - 2, 2);
			}
		}

		/// <summary>
		/// CRC – контрольная сумма вычисляется сложением всех полей за исключением: ИНП и CRC
		/// </summary>
		/// <returns>контрольная сумма 2</returns>
		protected override int CalcCRC()
		{
			int crc = 0;
			for(int i = 1, cnt = Size - 2; i < cnt; ++i) crc += bytes[i];
			//Нам нужны только 2 младших байта...
			crc &= 0xffff;
			return crc;
		}

		public override bool CheckCRC()
		{
			return CRC_ID == CalcCRC_ID() && base.CheckCRC();
		}

		#endregion CRC

		/// <summary>
		/// unit no. applied by user. 2 bytes
		/// </summary>
		public int UnitNo
		{
			get
			{
				return IntFromBytesBE(bytes[7], bytes[8]);
			}
		}

		/// <summary>
		/// id of packet. 2 bytes
		/// </summary>
		public int PacketID
		{
			get
			{
				return IntFromBytesBE(bytes[9], bytes[10]);
			}
		}
		
		protected void SetPacketID(int id)
		{
			//if(id < 0 || id > 0xffff) throw new ArgumentOutOfRangeException(
			//    "id", id, "PacketID must be [0..0xffff]");
			IntToBytesBE(id, bytes, 9, 2);
		}

		/// <summary>
		/// start position of data block in datagram
		/// </summary>
		protected int DataPos
		{
			get
			{
				return 16;
			}
		}

		/// <summary>
		/// data block size in datagram
		/// </summary>
		public int DataSize
		{
			get
			{
				return IntFromBytesBE(bytes[11], bytes[12], bytes[13]);
			}
		}

		/// <summary>
		/// Информационное поле - список объектов SGGBSubData
		/// </summary>
		protected ArrayList subs = null;

		/// <summary>
		/// Информационное поле данных, список объектов SGGBSubData
		/// </summary>
		public ArrayList DataField
		{
			get { return subs; }
		}
	}

	/// <summary>
	/// Блок данных датаграммы для обмена данными с мобильным устройством SGGB
	/// </summary>
	/// <remarks>Структура блока данных
	/// <p>
	/// bytes[0]			- identifier of datagram (SGGBDataType)
	/// bytes[1..2]			- len - 3
	/// bytes[3..(len - 1)] - type specific
	/// </p>
	/// </remarks>
	public abstract class SGGBSubData : IComparable
	{
		protected static TraceSwitch tsSGGB = 
			new TraceSwitch("TraceSGGB", "SGGB sub data related info");

		/// <summary>
		/// массив данных
		/// </summary>
		protected byte[] bytes;
		public byte[] Bytes
		{
			get
			{
				return bytes;
			}
		}

		/// <summary>
		/// начало блока данных
		/// </summary>
		protected int begin;

		/// <summary>
		/// sizeof(ID) + sizeof(Size)
		/// </summary>
		protected const int hdrsize = 3;

		/// <summary>
		/// pos in byte sequence after the header
		/// </summary>
		protected int DataPos
		{
			get
			{
				return begin + hdrsize;
			}
		}

		/// <summary>
		/// ID of sub data block. use abstract prop for not forget to change val
		/// </summary>
		/// <remarks>each sub data type has unique ID.
		/// commonly it's theoretical id and may differ from real byte values.
		/// for real val use GetID</remarks>
		public abstract int ID{get;}

		/// <summary>
		/// sub data size
		/// </summary>
		/// <remarks>full sub data size.
		/// commonly it's theoretical size and may differ from real byte values.
		/// for real size use GetSize</remarks>
		public int Size
		{
			get
			{
				return hdrsize + DataSize;
			}
		}
		
		/// <summary>
		/// data size of block. size without header. 
		/// use abstract prop for not forget to change val
		/// </summary>
		protected abstract int DataSize{get;}

		/// <summary>
		/// very useful
		/// </summary>
		protected Encoding encoding = Encoding.GetEncoding(1251);

		/// <summary>
		/// get ID of sub data in byte sequence
		/// </summary>
		/// <param name="data">bytes where to seek sub data ID</param>
		/// <param name="begin">begin pos of sub data in byte sequence</param>
		/// <returns>ID, 0 - bad</returns>
		public static int GetID(byte[] data, int begin)
		{
			return data[begin];
		}
		
		public static void SetID(byte[] data, int begin, SGGBDataType id)
		{
			data[begin] = (byte)id;
		}

		/// <summary>
		/// get size of sub data in byte sequence
		/// </summary>
		/// <param name="data">bytes where to seek sub data size</param>
		/// <param name="begin">begin pos of sub data in byte sequence</param>
		/// <returns>size</returns>
		public static int GetSize(byte[] data, int begin)
		{
			return Datagram.IntFromBytesBE(data[begin + 1], data[begin + 2]) + hdrsize;
		}

		public static void SetSize(byte[] data, int begin, int size)
		{
			if(size < 0 || size > 0xffff) throw new ArgumentOutOfRangeException(
				"SGGB sub data size must be [0..0xffff]");
			
			Datagram.IntToBytesBE(size - hdrsize, data, begin + 1, 2);
		}
		
		/// <summary>
		/// check packet header (ID + Size)
		/// </summary>
		/// <returns></returns>
		protected virtual bool CheckHdr()
		{
			return ID == GetID(bytes, begin) && Size == GetSize(bytes, begin);
		}

		#region .ctor
		/// <summary>
		/// for data from controller
		/// </summary>
		public SGGBSubData(byte[] bytes, int begin)
		{
			this.bytes = bytes;
			this.begin = begin;
		}

		public SGGBSubData(bool init)
		{
			if(init) Init();
		}
		
		#endregion // .ctor

		protected void Init()
		{
			bytes = new byte[Size];
			SetID(bytes, begin, (SGGBDataType)ID);
			SetSize(bytes, begin, Size);
		}
		
		#region IComparable Members

		public virtual int CompareTo(object obj)
		{
			if(obj is int) return ID.CompareTo(obj);

			SGGBSubData sub = obj as SGGBSubData;
			if(sub == null) throw new ArgumentException(
				"Incorrect type of object - " + obj.GetType(), "obj");

			return ID.CompareTo(sub.ID);
		}

		#endregion

		public virtual bool Valid
		{
			get
			{
				return CheckHdr();
			}
		}

		public static SGGBSubData Create(byte[] data, int posBegin)
		{
			switch((SGGBDataType)GetID(data, posBegin))
			{
			case SGGBDataType.GPS:
					return null;//new SGGBSubDataGPS(data, posBegin);

			case SGGBDataType.Settings:
				return new SGGBSubDataSettings(data, posBegin);

			case SGGBDataType.Log:
					return null;//new SGGBSubDataLog(data, posBegin);

			case SGGBDataType.Text:
				return new SGGBSubDataText(data, posBegin);

			case SGGBDataType.Alarm:
				return new SGGBSubDataAlarm(data, posBegin);

			case SGGBDataType.Confirm :
				return new SGGBSubDataConfirm(data, posBegin);
			case SGGBDataType.ConfirmFirmware:
				return new SGGBSubDataConfirmFrirmware(data, posBegin);
   
			default:
				if(!tsSGGB.TraceWarning) 
					return null;

				Trace.WriteLineIf(tsSGGB.TraceWarning, "UNEXPECTED SUB DATA TYPE - " + (SGGBDataType)GetID(data, posBegin), "SGGB");
				int subN = GetSize(data, posBegin);
				byte[] usd = new byte[subN];
				Array.Copy(data, posBegin, usd, 0, subN);
				Trace.WriteLineIf(tsSGGB.TraceWarning, "\t" + BitConverter.ToString(usd));
				return null;
			}
		}
		
		public override string ToString()
		{
			return bytes == null? base.ToString() + " empty": 
				BitConverter.ToString(bytes, begin, GetSize(bytes, begin)); 
		}
	}

	//struct FIELD_INFO
	//{
	//    /// <summary>
	//    /// pos from the start of fields section
	//    /// </summary>
	//    public int pos;
	//    /// <summary>
	//    /// size of field
	//    /// </summary>
	//    public int size;
	//    /// <summary>
	//    /// field type
	//    /// </summary>
	//    public Type type;
		
	//    public FIELD_INFO(int _pos, int _size) : this(_pos, _size, typeof(int))
	//    {
	//    }
		
	//    public FIELD_INFO(int _pos, int _size, Type _type)
	//    {
	//        pos = _pos;
	//        size = _size;
	//        type = _type;
	//    }
		
	//    /// <summary>
	//    /// get field value from bytes
	//    /// </summary>
	//    /// <param name="bytes">bytes with datagrams containing fields</param>
	//    /// <param name="datapos">start of fields section</param>
	//    /// <param name="datalen">len of fields section</param>
	//    /// <returns></returns>
	//    public object GetValue(byte[] bytes, int datapos, int datalen)
	//    {
	//        if(pos + size > datalen) return null;
			
	//        if(type == typeof(int)) return Datagram.IntFromBytesBE(bytes, datapos + pos, size);
	//        if(type == typeof(float)) return Datagram.FloatFromBytesBE(bytes, datapos + pos, size);
	//        return null;
	//    }
	//}
	
	public enum SGGBDataType : byte
	{
		/// <summary>
		/// each datagram starts from
		/// </summary>
		SGGB	= 0xAA,
		/// <summary>
		/// Инф. блок содежит  состояния модуля на момент времени стоящего в поле «время в сек от начала суток»
		/// </summary>
		GPS		= 0x01,
		/// <summary>
		/// Задать управляющее воздействие
		/// </summary>
		Control	= 0x02,
		/// <summary>
		/// Вывести текстовое сообщение
		/// </summary>
		SendText	= 0x04,
		/// <summary>
		/// пришло текстовое сообщение
		/// </summary>
		Text		= 0x05,
		/// <summary>
		/// Запрос на выдачу архива
		/// </summary>
		GetLog		= 0x06,
		/// <summary>
		/// Ответ на запрос GetLog.
		/// Инф. блок содежит  состояния модуля на момент времени стоящего в поле «время в сек от начала суток» 
		/// </summary>
		Log			= 0x07,
		/// <summary>
		/// get lower and upper time bounds of controller log
		/// </summary>
		GetLogBound	= 0x08,
		/// <summary>
		/// sos from controller
		/// </summary>
		Alarm			= 0x09,
		/// <summary>
		/// Изменить настройки модуля
		/// </summary>
		SetSettings		= 0x0A,
		/// <summary>
		/// Запрос на чтение текущих настроек/параметров модуля
		/// </summary>
		GetSettings		= 0x0E,
		/// <summary>
		/// Текущие настройки/параметры модуля (ответ на запрос GetSettings)
		/// </summary>
		Settings		= 0x0B,
		/// <summary>
		/// send next firmware part
		/// </summary>
		SetFirmware		= 0xC,
		/// <summary>
		/// confirm receive next part of firmware
		/// </summary>
		ConfirmFirmware	= 0xD,
		/// <summary>
		/// confirm from controller
		/// </summary>
		Confirm			= 0x11,
		/// <summary>
		/// send firmware part to unit
		/// </summary>
		SendFirmware	= 0x12,
		/// <summary>
		/// clear unit log memory
		/// </summary>
		FormatLog		= 0x13,
		/// <summary>
		/// controller log bounds
		/// </summary>
		LogBound		= 0x14,
		/// <summary>
		/// send confirm to controller
		/// </summary>
		SendConfirm		= 0x22,
		/// <summary>
		/// unit restart
		/// </summary>
		Restart			= 0xee,
	}
	/*
		Пример пакета
		AA-09-02-14-59-03-00-00-00-F6-10-00-00-25-01-A6-01-00-22-00-F6-10-37-28-3F-7A-86-D8-4E-00-25-25-3E-F6-2A-1C-45-01-FF-F7-00-01-00-03-2F-88-22-AB-53-3B-E5-60-42-0D-D7
		Расшифровка пакета
		"Шапка пакета" - 16 байт
		AA - признак макета
		( 0)
		09-02-14-59-03-00-00 - ID устройства
		( 1  2  3  4  5  6  7)
		00-F6-10  - ID пакета
		( 8  9 10)
		00-00-25  - длина пакета
		(11 12 13)
		01-A6     - CRC1
		(14 15)
		Далее идут информационные блоки
		Например:
		01-00-22-00-F6-10-37-28-3F-7A-86-D8-4E-00-25-25-3E-F6-2A-1C-45-01-FF-F7-00-01-00-03-2F-88-22-AB-53-3B-E5-60-42
		01        - идентификатор блока
		( 0)
		00-22     - размер пакета
		( 1  2)
		00-F6-10  - время в секундах от начала дня
		( 3  4  5)
		37-28-3F-7A-86-D8-4E  - широта (см. документацию)
		( 6  7  8  9 10 11 12) 
		00-25-25-3E-F6-2A-1C-45 - долгота (см. документацию)
		(13 14 15 16 17 18 19 20) 
		01   - признак корекктной информации GPS
			(21) 
		FF-F7  - цифровые датчики (старшие 12 бит)
		(22 23)
		00-01  - аналоговые датчики вход 1
		(24 25)
		00-03  - аналоговые датчики вход 2
		(26 27) 
		2F     - цифровая линия управления
		(28) 
		88-22  - аналоговая линия управления 1
		(29 30)
		AB-53  - аналоговая линия управления 2
		(31 32) 
		3B-E5-60-42  - скорость (см. документацию)
		(33 34 35 36)
		В конце пакета присутствует ещё одна контрольная сумма
		0D-D7
		(37 38)
	*/

	/// <summary>
	/// 0x01. GPS данные (контролер -> диспетчер)
	/// </summary>
	/// <remarks>
	/// <p>Номера байтов даны от начала блока</p>
	/// byte[3..5] = время в секундах от начала месяца
	/// byte[6] = latitude, градусы
	/// byte[7] = latitude, минуты
	/// byte[8..11] = latitude, дробная часть минут (float)
	/// byte[12] = N-север, S-юг
	/// byte[13..14] = longitude, градусы
	/// byte[15] = longitude, минуты
	/// byte[16..19] = longitude, дробная часть минут (float)
	/// byte[20] = E-восток, W-запад
	/// byte[21] = корректность данных 1-корректное, 0-нет
	/// byte[22..23] = цифровые датчики
	/// byte[24..25] = аналоговые датчики - вход1
	/// byte[26..27] = аналоговые датчики - вход2
	/// byte[28] = цифровые линии управления (7..0 биты)
	/// byte[29..30] = аналоговые линии управления - линия1
	/// byte[31..32] = аналоговые линии управления - линия2
	/// byte[33..36] = speed in knots
	/// </remarks>
	public class SGGBSubDataGPS : SGGBSubDataControl
	{
		public override int ID
		{
			get
			{
				return (int)SGGBDataType.GPS;
			}
		}

		protected override int DataSize
		{
			get
			{
				return GetSize(bytes, begin) - hdrsize;
			}
		}

		public override bool Valid
		{
			get
			{
				// correct header, correct time
				return base.Valid && Seconds > 0 && (time_t)Seconds - DateTime.UtcNow < TimeSpan.FromHours(1);
			}
		}

		/// <summary>
		/// time_t
		/// </summary>
		/// <remarks>but seconds from beginning of month received</remarks>
		public int Seconds
		{
			get
			{
				int pos = DataPos + FieldInfo.PosSeconds; //GetFieldPos("Seconds"),
				int size = FieldInfo.SizeSeconds; // GetFieldSize("Seconds");

					if(size == 4) return Datagram.IntFromBytesBE(
						bytes[pos], bytes[pos + 1], bytes[pos + 2], bytes[pos + 3]);
				
				Debug.Fail("No more short time support");
				Debug.Assert(size == 3, "Unsupported time");
				
				// secs from beginning of month
				int secs = Datagram.IntFromBytesBE(bytes[pos], bytes[pos + 1], bytes[pos + 2]);
				// time bug spec
				if(secs > 32 * 24 * 60 * 60) throw new DatagramFieldException(
					"Incorrect datagram field received. Seconds - " + secs, GetType(), 
					bytes, begin, "Seconds");

				DateTime tm = DateTime.Today;
				// add secs to begin of current month
				tm = new DateTime(tm.Year, tm.Month, 1);
				// check if prev month
				if(tm.AddSeconds(secs) - DateTime.UtcNow > TimeSpan.FromDays(2)) 
					tm = tm.AddMonths(-1);
				// check our time bug
				else if(tm.AddSeconds(secs) - DateTime.UtcNow > TimeSpan.FromHours(2)) 
					tm = tm.AddDays(-1);

				tm = tm.AddSeconds(secs);
#if DEBUG
				DateTime now = DateTime.UtcNow;
				if(tm - now > TimeSpan.FromMinutes(1))
					Trace.WriteLineIf(tsSGGB.TraceError, String.Format(
						"Incorrect time in pos: {0}, now: {1}, diff: {2}", tm, now, tm - now), 
						"SGGB");
#endif
				// convert tm to time_t
				return (time_t)tm;
			}
		}

		/// <summary>
		/// latitude
		/// </summary>
		public float Latitude
		{
			get
			{
				int pos = DataPos + FieldInfo.PosLatitude; // GetFieldPos("Latitude");
				// градусы
				float latitude = Datagram.IntFromBytesBE(bytes[pos]);
				// минуты 
				latitude += Datagram.IntFromBytesBE(bytes[pos + 1]) / 60.0f;
				// доли минут 
				latitude += Datagram.FloatFromBytesBE(bytes[pos + 2], bytes[pos + 3], 
					bytes[pos + 4], bytes[pos + 5]) / 60;
				
				Debug.Assert(North, "north lat expected");
				return North? latitude: -1 * latitude;
			}
		}

		/// <summary>
		/// north or south latitude
		/// </summary>
		public bool North
		{
			get
			{
				return ((char)bytes[DataPos + FieldInfo.PosNorth /*GetFieldPos("North")*/]).ToString().ToUpper() != "S";
			}
		}

		/// <summary>
		/// longitude
		/// </summary>
		public float Longitude
		{
			get
			{
				int pos = DataPos + FieldInfo.PosLongitude; // GetFieldPos("Longitude");
				// градусы
				float longitude = Datagram.IntFromBytesBE(bytes[pos], bytes[pos + 1]);
				// минуты 
				longitude += Datagram.IntFromBytesBE(bytes[pos + 2]) / 60.0f;
				// доли минут 
				longitude += Datagram.FloatFromBytesBE(bytes[pos + 3], bytes[pos + 4], 
					bytes[pos + 5], bytes[pos + 6]) / 60;
				
				Debug.Assert(East, "east long expected");
				return East? longitude: -1 * longitude;
			}
		}

		/// <summary>
		/// east or west longitude
		/// </summary>
		public bool East
		{
			get
			{
				return ((char)bytes[DataPos + FieldInfo.PosEast /*GetFieldPos("East")*/]).ToString().ToUpper() != "W";  
			}
		}

		/// <summary>
		/// Флаг корректности
		/// </summary>
		public bool CorrectGPS
		{
			get
			{
				return Satellites >= MobilUnit.Unit.MobilUnit.MinSatellites && Longitude > 0 && Longitude < 180 && Latitude > 0 && Latitude < 90 && Speed <= MobilUnit.Unit.MobilUnit.GPSLimitMaxSpeed;
			}
		}
		
		/// <summary>
		/// satellites count
		/// </summary>
		public int Satellites
		{
			get
			{
				int sat = bytes[DataPos + FieldInfo.PosCorrectGPS /*GetFieldPos("CorrectGPS")*/];
				return sat > 0? sat: -1;
			}
		}

		/// <summary>
		/// 1 marine knot in km/h
		/// </summary>
		const double MarineKnot = 1.852;	// 50. / 27 = 1.(851)

		/// <summary>
		/// Скорость км/ч
		/// </summary>
		public int Speed
		{
			get
			{
				int pos = DataPos + FieldInfo.PosSpeed; // GetFieldPos("Speed");
				float speed = 
					Datagram.FloatFromBytesBE(bytes[pos], bytes[pos + 1], bytes[pos + 2], bytes[pos + 3]);
				return (byte)(speed * MarineKnot);
			}
		}

		#region Датчики digital

		public class DigitSensors
		{
			BitMask bmDigits;
			public DigitSensors(int mask, int len)
			{
				bmDigits = new BitMask(mask, len);
			}
			
			public int Count
			{
				get
				{
					return bmDigits.Count;
				}
			}
			
			public bool this[int i]
			{
				get
				{
					return bmDigits[i];
				}
			}
			
			public static implicit operator int(DigitSensors ds)
			{
				return ds.bmDigits;
			}
		}
		
		public DigitSensors DigitSensor
		{
			get
			{
				return new DigitSensors(Digits, 12);
			}
		}
		
//		/* датчики
//		black bug  - бит 5
//		от сигнализации сигнал о аварийной ситуации - бит 4
//		от сигнализации о постановке на охрану - бит 3 
//		от схемы слежения за дверьми - бит 2 - 
//		от схемы слежения за капотом - бит 1 - нет
//		кнопка SOS сейчас  - бит 0           - да если нажата-0
//		*/
//		/// <summary>
//		/// Цифровой датчик №1
//		/// </summary>
//		/// <remarks>Кнопка SOS: true-тревога; false-нормальный режим</remarks>
//		public bool DigitSensor1
//		{
//			get { return (Digits & 0x1) == 0; }
//		}
//
//		/// <summary>
//		/// Цифровой датчик №2
//		/// </summary>
//		/// <remarks>Слежение за капотом: true-открыт; false-закрыт</remarks>
//		public bool DigitSensor2
//		{
//			get { return (Digits & 0x2) == 0; }
//		}
//
//		/// <summary>
//		/// Цифровой датчик №3
//		/// </summary>
//		/// <remarks>Слежение за дверьми: true-открыт; false-закрыт</remarks>
//		public bool DigitSensor3
//		{
//			get { return (Digits & 0x4) == 0; }
//		}
//
//		/// <summary>
//		/// Цифровой датчик №4
//		/// </summary>
//		/// <remarks>Постановке на охрану: true-на охране; false-нормальный режим</remarks>
//		public bool DigitSensor4
//		{
//			get { return (Digits & 0x8) != 0; }
//		}
//
//		/// <summary>
//		/// Цифровой датчик №5
//		/// </summary>
//		/// <remarks>Cигнал о аварийной ситуации: true-сработала сигнализация; false-нормальный режим</remarks>
//		public bool DigitSensor5
//		{
//			get { return (Digits & 0x10) == 0; }
//		}
//
//		/// <summary>
//		/// Цифровой датчик №6
//		/// </summary>
//		/// <remarks>BlackBug: true-?; false-?</remarks>
//		public bool DigitSensor6
//		{
//			get { return (Digits & 0x20) == 0; }
//		}
//
//		/// <summary>
//		/// Цифровой датчик №7
//		/// </summary>
//		/// <remarks>?</remarks>
//		public bool DigitSensor7
//		{
//			get { return (Digits & 0x40) == 0; }
//		}
//
//		/// <summary>
//		/// Цифровой датчик №8
//		/// </summary>
//		/// <remarks>?</remarks>
//		public bool DigitSensor8
//		{
//			get { return (Digits & 0x80) == 0; }
//		}
//
//		/// <summary>
//		/// Цифровой датчик №9
//		/// </summary>
//		/// <remarks>?</remarks>
//		public bool DigitSensor9
//		{
//			get { return (Digits & 0x100) == 0; }
//		}
//
//		/// <summary>
//		/// Цифровой датчик №10
//		/// </summary>
//		/// <remarks>?</remarks>
//		public bool DigitSensor10
//		{
//			get { return (Digits & 0x200) == 0; }
//		}
//
//		/// <summary>
//		/// Цифровой датчик №11
//		/// </summary>
//		/// <remarks>?</remarks>
//		public bool DigitSensor11
//		{
//			get { return (Digits & 0x400) == 0; }
//		}
//
//		/// <summary>
//		/// Цифровой датчик №12
//		/// </summary>
//		/// <remarks>?</remarks>
//		public bool DigitSensor12
//		{
//			get { return (Digits & 0x800) == 0; }
//		}

		/// <summary>
		/// Цифровой датчик 
		/// </summary>
		public int Digits
		{
			get
			{
				int pos = DataPos + FieldInfo.PosDigits; // GetFieldPos("Digits");
				return Datagram.IntFromBytesBE(bytes[pos], bytes[pos + 1]);
			}
		}

		#endregion Датчики digital

		public int AnalogSensor1
		{
			get
			{
				int pos = DataPos + FieldInfo.PosAnalogSensor1; // GetFieldPos("AnalogSensor1");
				return Datagram.IntFromBytesBE(bytes[pos], bytes[pos + 1]);
			}
		}

		public int AnalogSensor2
		{
			get
			{
				int pos = DataPos + FieldInfo.PosAnalogSensor2; // GetFieldPos("AnalogSensor2");
				return Datagram.IntFromBytesBE(bytes[pos], bytes[pos + 1]);
			}
		}
		
		public const int AbsoluteZero = - 273;
		/// <summary>
		/// 
		/// </summary>
		public int Temperature
		{
			get
			{
				object val = DataPos + FieldInfo.PosTemperature; // GetFieldInfo("Temperature").GetValue(bytes, DataPos, DataSize);
				if((val is int)) return (int)val;
				else return AbsoluteZero;
			}
		}

		/* линии управления, состояние
		бит 0 - светодиод
		бит 1 - управление бензонасосом 1
		бит 2 - управление бензонасосом 2
		бит 3 - управление поворотниками
		бит 4 - управление клаксоном
		*/

		/// <summary>
		/// pos in byte sequence at the beginning of control lines
		/// </summary>
		public override int ControlPos
		{
			get
			{
				return DataPos + FieldInfo.PosControlPos; // GetFieldPos("ControlPos");
			}
		}

		#region Field info
		
		//FIELD_INFO GetFieldInfo(string name)
		//{
		//    return (FIELD_INFO)parFieldInfo[name];
		//}

		//int GetFieldPos(string name)
		//{
		//    return DataPos + GetFieldInfo(name).pos;
		//}
		
		//int GetFieldSize(string name)
		//{
		//    return GetFieldInfo(name).size;
		//}
		
		//static PARAMS parOldFieldInfo = new PARAMS(), parNewFieldInfo = new PARAMS();
		//PARAMS parFieldInfo;
		
		#endregion Field info

		#region .ctor


		//static SGGBSubDataGPS()
		//{
		//    //parOldFieldInfo.Add("Seconds", new FIELD_INFO(0, 3));
		//    //parOldFieldInfo.Add("Latitude", new FIELD_INFO(3, 6));
		//    //parOldFieldInfo.Add("North", new FIELD_INFO(9, 1));
		//    //parOldFieldInfo.Add("Longitude", new FIELD_INFO(10, 7));
		//    //parOldFieldInfo.Add("East", new FIELD_INFO(17, 1));
		//    //parOldFieldInfo.Add("CorrectGPS", new FIELD_INFO(18, 1));
		//    //parOldFieldInfo.Add("Digits", new FIELD_INFO(19, 2));
		//    //parOldFieldInfo.Add("AnalogSensor1", new FIELD_INFO(21, 2));
		//    //parOldFieldInfo.Add("AnalogSensor2", new FIELD_INFO(23, 2));
		//    //parOldFieldInfo.Add("ControlPos", new FIELD_INFO(25, 5));
		//    //parOldFieldInfo.Add("Speed", new FIELD_INFO(30, 4));
			
		//    //parNewFieldInfo.Add("Seconds", new FIELD_INFO(0, 4));
		//    //parNewFieldInfo.Add("Latitude", new FIELD_INFO(4, 6));
		//    //parNewFieldInfo.Add("North", new FIELD_INFO(10, 1));
		//    //parNewFieldInfo.Add("Longitude", new FIELD_INFO(11, 7));
		//    //parNewFieldInfo.Add("East", new FIELD_INFO(18, 1));
		//    //parNewFieldInfo.Add("CorrectGPS", new FIELD_INFO(19, 1));
		//    //parNewFieldInfo.Add("Digits", new FIELD_INFO(20, 2));
		//    //parNewFieldInfo.Add("AnalogSensor1", new FIELD_INFO(22, 2));
		//    //parNewFieldInfo.Add("AnalogSensor2", new FIELD_INFO(24, 2));
		//    //parNewFieldInfo.Add("ControlPos", new FIELD_INFO(26, 5));
		//    //parNewFieldInfo.Add("Speed", new FIELD_INFO(31, 4));
		//    //parNewFieldInfo.Add("Temperature", new FIELD_INFO(35, 1));
		//}
		
		/// <summary>
		/// GPS данные (контроллер -> диспетчер)
		/// </summary>
		public SGGBSubDataGPS(byte[] data, int begin) : base(data, begin)
		{
			switch(GetSize(data, begin) - hdrsize)	// same as DataSize but more obvious
			{
			case 34:	// 3 byte time. secs from month beginning
				//parFieldInfo = parOldFieldInfo;
				break;
			
			case 35:	// 4 byte time. time_t
			case 36:	// 4 byte time. time_t with temperature
				//parFieldInfo = parNewFieldInfo;
				break;
			
			default: throw new NotSupportedException(
				"Sub data not supported. Type - " + GetType() + "\n\t" + BitConverter.ToString(data, begin));
			}
		}

		#endregion // .ctor
		
		public override string ToString()
		{
			return string.Format("Time - {0}, Lat - {1}, Long - {2}, Sat - {3}\n\t{4}", 
				(time_t)Seconds, Latitude, Longitude, Satellites, base.ToString());
		}
	}

	/// <summary>
	/// 0x02. Данные управления (диспетчер -> контроллер)
	/// </summary>
	public class SGGBSubDataControl : SGGBSubData
	{
		#region .ctor

		public SGGBSubDataControl(byte[] data, int begin) : base(data, begin)
		{
		}

		public SGGBSubDataControl() : base(true)
		{
		}

		#endregion // .ctor

		public override int ID
		{
			get
			{
				return (int)SGGBDataType.Control;
			}
		}

		protected override int DataSize
		{
			get
			{
				return 5;
			}
		}

		#region Control lines

		// TODO: generalize control lines

		/// <summary>
		/// pos in byte sequence at the beginning of control lines
		/// </summary>
		public virtual int ControlPos
		{
			get
			{
				return DataPos;
			}
		}

		public bool Diode
		{
			get
			{
				return BitMask.GetBit(bytes[ControlPos], 0);
			}
			set
			{
				BitMask.SetBit(ref bytes[ControlPos], 0, value);
			}
		}

		public bool Pump
		{
			get
			{
				return !BitMask.GetBit(bytes[ControlPos], 1);
			}
			set
			{
				BitMask.SetBit(ref bytes[ControlPos], 1, !value);
			}
		}

		public bool Pump1
		{
			get
			{
				return !BitMask.GetBit(bytes[ControlPos], 2);
			}
			set
			{
				BitMask.SetBit(ref bytes[ControlPos], 2, !value);
			}
		}

		public bool Lights
		{
			get
			{
				return BitMask.GetBit(bytes[ControlPos], 3);
			}
			set
			{
				BitMask.SetBit(ref bytes[ControlPos], 3, value);
			}
		}

		public bool Horn
		{
			get
			{
				return BitMask.GetBit(bytes[ControlPos], 4);
			}
			set
			{
				BitMask.SetBit(ref bytes[ControlPos], 4, value);
			}
		}

		public int AnalogLine1
		{
			get
			{
				return Datagram.IntFromBytesBE(bytes[ControlPos + 1], bytes[ControlPos + 2]);
			}
			set
			{
				if(value < 0 || value > 0xffff) throw new ArgumentOutOfRangeException(
					"Analog line 1 must be [0..0xffff]");
				Datagram.IntToBytes(value, bytes, ControlPos + 1, 2);
			}
		}

		public int AnalogLine2
		{
			get
			{
				return Datagram.IntFromBytesBE(bytes[ControlPos + 3], bytes[ControlPos + 4]);
			}
			set
			{
				if(value < 0 || value > 0xffff) throw new ArgumentOutOfRangeException(
					"Analog line 2 must be [0..0xffff]");
				Datagram.IntToBytes(value, bytes, ControlPos + 3, 2);
			}
		}

		#endregion Control lines
	}

	/// <summary>
	/// Посылка/получение текстового сообщения.
	/// (диспетчер -> контроллер) 0x4, (диспетчер &lt;- контроллер) 0x5
	/// </summary>
	public class SGGBSubDataText : SGGBSubData
	{
		#region .ctor

		/// <summary>
		/// посылка текстового сообщения (диспетчер -> контроллер). max length - 240. 
		/// screen size
		/// </summary>
		public SGGBSubDataText(string text) : base(false)
		{
			text = text.Trim();
			if(text.Length > 240) Trace.WriteLineIf(tsSGGB.TraceWarning, 
				"Text too long. Max length - 240", "SGGB");
			bytes = new byte[hdrsize + text.Length];
			SetID(bytes, begin, SGGBDataType.SendText);
			SetSize(bytes, begin, bytes.Length);
			encoding.GetBytes(text, 0, text.Length, bytes, DataPos);
		}
		
		/// <summary>
		/// получение текстового сообщения
		/// </summary>
		public SGGBSubDataText(byte[] bytes, int begin) : base(bytes, begin)
		{
		}
		
		#endregion // .ctor

		public override int ID
		{
			get
			{
				return GetID(bytes, begin);
			}
		}

		protected override int DataSize
		{
			get
			{
				return GetSize(bytes, begin) - hdrsize;
			}
		}

		protected override bool CheckHdr()
		{
			return ID == (int)SGGBDataType.SendText || ID == (int)SGGBDataType.Text;
		}

		public string Text
		{
			get
			{
				return encoding.GetString(bytes, DataPos, DataSize);
			}
		}
	}

	/// <summary>
	/// Запрос на выдачу архива (диспетчер -> контролер). ID = 0x06
	/// </summary>
	public class SGGBSubDataGetLog : SGGBSubData
	{
		#region .ctor

		/// <summary>
		/// Запрос на выдачу архива (диспетчер -> контролер)
		/// </summary>
		public SGGBSubDataGetLog() : base(true)
		{
		}

		#endregion // .ctor

		public override int ID
		{
			get
			{
				return (int)SGGBDataType.GetLog;
			}
		}

		protected override int DataSize
		{
			get
			{
				return 8;
			}
		}

		/// <summary>
		/// controller memory from time
		/// </summary>
		public DateTime From
		{
			get
			{
				return (time_t)Datagram.IntFromBytesBE(bytes[DataPos], 
					bytes[DataPos + 1], bytes[DataPos + 2], bytes[DataPos + 3]);
			}
			set
			{
				int tm = (time_t)value;
				if(tm < 0) throw new ArgumentOutOfRangeException("value out of range");
				
				Datagram.IntToBytesBE(tm, bytes, DataPos, 4);
			}
		}

		/// <summary>
		/// controller memory to time
		/// </summary>
		public DateTime To
		{
			get
			{
				return (time_t)Datagram.IntFromBytesBE(bytes[DataPos + 4], 
					bytes[DataPos + 5], bytes[DataPos + 6], bytes[DataPos + 7]);
			}
			set
			{
				int tm = (time_t)value;
				if(tm < 0) throw new ArgumentOutOfRangeException("value out of range");
				
				Datagram.IntToBytesBE(tm, bytes, DataPos + 4, 4);
			}
		}
	}

	/// <summary>
	/// SGGBSubConfirmSetFrirmware - поддтверждение приема пакета с частью прошивки
	/// </summary>
	public class SGGBSubDataConfirmFrirmware : SGGBSubData
	{
		public SGGBSubDataConfirmFrirmware(byte[] data, int begin)
			: base(data, begin)
		{
		}

		

		public override int ID
		{
			get
			{
				return GetID(bytes, begin);
			}
		}

		protected override int DataSize
		{
			get
			{
				return 1;
			}
		}

		public int Index
		{
			get
			{
				return Datagram.IntFromBytesBE(bytes[DataPos]);
			}
		}
	}

	/// <summary>
	/// SGGBSubDataSetFirmware - отсылка перепрошивки модулю
	/// </summary>
	/// <remarks>
	/// bytes[0]		- ID = SGGBDataType.SetFirmware = 0x0C
	/// bytes[1..2]		- Размер пакета
	/// bytes[3]		- Порядковый номер пакета
	/// bytes[4..n]		- Байты прошивки
	/// </remarks>
	public class SGGBSubDataSetFirmware : SGGBSubData
	{
		#region .ctor
		public SGGBSubDataSetFirmware(int SizeOfData)
			: base(true)
		{
			datasize = SizeOfData;
			Init();
		}
		#endregion
		public override int ID
		{
			get
			{
				return (int)SGGBDataType.SetFirmware;
			}
		}

		int datasize;
		protected override int DataSize
		{
			get
			{
				return datasize;
			}
		}

		/// <summary>
		/// Данные прошивки
		/// </summary>
		public byte[] FirmwareData
		{
			set
			{
				value.CopyTo(bytes, DataPos + 1);
			}
		}

		/// <summary>
		/// Номер пакета
		/// </summary>
		public int Index
		{
			set
			{
				bytes.SetValue((byte)value, DataPos);
			}
			get
			{
				return (int)bytes[DataPos];
			}
		}
	}

	/// <summary>
	/// Данные архива (контролер -> диспетчер). 0x07
	/// </summary>
	/// <remarks>same as GPS but id</remarks>
	public class SGGBSubDataLog : SGGBSubDataGPS
	{
		#region .ctor

		/// <summary>
		/// Данные архива (контролер -> диспетчер)
		/// </summary>
		public SGGBSubDataLog(byte[] bytes, int begin) : base(bytes, begin)
		{
		}

		#endregion // .ctor

		public override int ID
		{
			get
			{
				return (int)SGGBDataType.Log;
			}
		}
	}
	/// <summary>
	/// request for getting settings
	/// </summary>
	/// <remarks>
	/// bytes[0]	- ID = SGGBDataType.GetSettings = 0x0E
	/// bytes[1..2]	- Size = 0
	/// </remarks>
	public class SGGBSubDataGetSettings : SGGBSubData
	{
		public SGGBSubDataGetSettings() : base(true)
		{
		}

		public override int ID
		{
			get
			{
				return (int)SGGBDataType.GetSettings;
			}
		}

		protected override int DataSize
		{
			get
			{
				return 0;
			}
		}
	}

	public class SGGBSubDataRestart : SGGBSubData
	{
		public SGGBSubDataRestart()
			: base(true)
		{
		}

		public override int ID
		{
			get
			{
				return (int)SGGBDataType.Restart;
			}
		}

		protected override int DataSize
		{
			get
			{
				return 0;
			}
		}
	}

	/// <summary>
	/// sub data to set controller settings
	/// </summary>
	/// <remarks>
	/// bytes[0]		- ID = SGGBDataType.SetSettings = 0x0A
	/// bytes[1..2]		- Size = 40 (???). actually 55
	/// bytes[3..6]		- dispatcher IP
	/// bytes[7..8]		- dispatcher port
	/// bytes[9..21]	- reserved
	///	bytes[22..37]	- dispatcher phone for voice, with city code
	///	bytes[38..53]	- dispatcher phone for data
	///	bytes[54]		- send interval. 10-60
	///	bytes[55]		- '0' - udp, '1' - tcp. char
	///	bytes[56..57]	- unit no.
	/// </remarks>
	public class SGGBSubDataSetSettings : SGGBSubData
	{
		public override int ID
		{
			get
			{
				return (int)SGGBDataType.SetSettings;
			}
		}

		protected override int DataSize
		{
			get
			{
				return 151;
			}
		}

		/// <summary>
		/// ctor for empty object. initialized
		/// </summary>
		public SGGBSubDataSetSettings() : base(true)
		{
			//Debug.Fail("not properly implemented");
		}

		/// <summary>
		/// dispatcher IP
		/// </summary>
		public IPAddress IP
		{
			get
			{
				return new IPAddress(
					new byte[4]{bytes[DataPos], bytes[DataPos + 1], bytes[DataPos + 2], bytes[DataPos + 3]});
			}
			set
			{
				Array.Copy(value.GetAddressBytes(), 0, bytes, DataPos, 4);
			}
		}

		/// <summary>
		/// dispatcher port
		/// </summary>
		public int Port
		{
			get
			{
				return Datagram.IntFromBytesBE(bytes[DataPos + 4], bytes[DataPos + 5]);
			}
			set
			{
				Datagram.IntToBytesBE(value, bytes, DataPos + 4, 2);
			}
		}

		public int TimeZone
		{
			get { return (int)bytes[DataPos + 18]; }
			set { bytes[DataPos + 18] = (byte)value; }
		}

		/// <summary>
		/// Voice phone
		/// </summary>
		public string VoicePhone
		{
			get
			{
				return encoding.GetString(bytes, DataPos + 19, 16).TrimEnd();
			}
			set
			{
				if(value == null) value = "";
				if(value.Length > 16) 
					throw new ArgumentException("Voice phone number too long");

				encoding.GetBytes(value, 0, value.Length, bytes, DataPos + 19);
			}
		}

		/// <summary>
		/// Data phone
		/// </summary>
		public string DataPhone
		{
			get
			{
				return encoding.GetString(bytes, DataPos + 35, 16).TrimEnd();
			}
			set
			{
				if(value == null) value = "";
				if(value.Length > 16) 
					throw new ArgumentException("Data phone number too long");

				encoding.GetBytes(value, 0, value.Length, bytes, DataPos + 35);
			}
		}

		/// <summary>
		/// send interval
		/// </summary>
		public int Interval
		{
			get
			{
				return bytes[DataPos + 51];
			}
			set
			{
				bytes[DataPos + 51] = (byte)value;
			}
		}

		/// <summary>
		/// true - TCP, false - UDP
		/// </summary>
		public bool TCP
		{
			get
			{
				return bytes[DataPos + 52] != '0';
			}
			set
			{
				bytes[DataPos + 52] = (byte)(value? '1': '0');
			}
		}

		/// <summary>
		/// assignable unit no
		/// </summary>
		public int UnitNo
		{
			get
			{
				return Datagram.IntFromBytesBE(bytes[DataPos + 53], bytes[DataPos + 54]);
			}
			set
			{
				Datagram.IntToBytesBE(value, bytes, DataPos + 53, 2);
			}
		}

		/// <summary>
		/// Internet Entry Point
		/// </summary>
		public string EntryPoint
		{
			get
			{
				return encoding.GetString(bytes, DataPos + 55, 32).TrimEnd();
			}
			set
			{
				if (value == null) value = "";
				if (value.Length > 32)
					throw new ArgumentException("Entry point is too long");

				encoding.GetBytes(value, 0, value.Length, bytes, DataPos + 55);
			}
		}

		/// <summary>
		/// Internet Login
		/// </summary>
		public string Login
		{
			get
			{
				return encoding.GetString(bytes, DataPos + 87, 32).TrimEnd();
			}
			set
			{
				if (value == null) value = "";
				if (value.Length > 32)
					throw new ArgumentException("Login is too long");

				encoding.GetBytes(value, 0, value.Length, bytes, DataPos + 87);
			}
		}

		// <summary>
		/// Internet Password
		/// </summary>
		public string Password
		{
			get
			{
				return encoding.GetString(bytes, DataPos + 119, 32).TrimEnd();
			}
			set
			{
				if (value == null) value = "";
				if (value.Length > 32)
					throw new ArgumentException("Password is too long");

				encoding.GetBytes(value, 0, value.Length, bytes, DataPos + 119);
			}
		}
	}

	/// <summary>
	/// controller settings
	/// </summary>
	/// <remarks>
	/// bytes[0]		- ID = SGGBDataType.Settings = 0x0B
	/// bytes[1..2]		- Size = 57 (???). actually 85
	/// bytes[3..6]		- dispatcher IP
	/// bytes[7..8]		- dispatcher port
	///	bytes[9..21]	- reserved
	///	bytes[22..37]	- dispatcher voice phone, with city code
	///	bytes[38..53]	- dispatcher data phone
	///	bytes[54]		- send interval. 10-60
	///	bytes[55]		- 0 - udp, 1 - tcp. char
	///	bytes[56..70]	- module firmware version string
	///	bytes[71..72]	- reserved
	///	bytes[73..87]	- terminal firmware version string
	/// </remarks>
	public class SGGBSubDataSettings : SGGBSubData
	{
		public SGGBSubDataSettings(byte[] data, int begin) : base(data, begin)
		{
			//Debug.Fail("not properly implemented");
		}

		public override int ID
		{
			get
			{
				return (byte)SGGBDataType.Settings;
			}
		}

		protected override int DataSize
		{
			get
			{
				return 179;
			}
		}

		/// <summary>
		/// dispatcher IP
		/// </summary>
		public IPAddress IP
		{
			get
			{
				return new IPAddress(new byte[4]{bytes[DataPos], bytes[DataPos + 1], 
					bytes[DataPos + 2], bytes[DataPos + 3]});
			}
			set
			{
				Array.Copy(value.GetAddressBytes(), 0, bytes, DataPos, 4);
			}
		}

		/// <summary>
		/// dispatcher port
		/// </summary>
		public int Port
		{
			get
			{
				return Datagram.IntFromBytesBE(bytes[DataPos + 4], bytes[DataPos + 5]);
			}
			set
			{
				Datagram.IntToBytesBE(value, bytes, DataPos + 4, 2);
			}
		}

		/// <summary>
		/// Часовой пояс
		/// </summary>
		public int TimeZone
		{
			get
			{
				return (int)bytes[DataPos + 18];
			}
		}

		/// <summary>
		/// Voice phone
		/// </summary>
		public string VoicePhone
		{
			get
			{
				return encoding.GetString(bytes, DataPos + 19, 16).TrimEnd();
			}
			set
			{
				if(value == null) value = "";
				if(value.Length > 16) 
					throw new ArgumentException("Voice phone number too long");

				encoding.GetBytes(value, 0, value.Length, bytes, DataPos + 19);
			}
		}

		/// <summary>
		/// Data phone
		/// </summary>
		public string DataPhone
		{
			get
			{
				return encoding.GetString(bytes, DataPos + 35, 16).TrimEnd();
			}
			set
			{
				if(value == null) value = "";
				if(value.Length > 16) 
					throw new ArgumentException("Data phone number too long");

				encoding.GetBytes(value, 0, value.Length, bytes, DataPos + 35);
			}
		}

		/// <summary>
		/// data send interval
		/// </summary>
		public int Interval
		{
			get
			{
				return bytes[DataPos + 51];
			}
			set
			{
				bytes[DataPos + 51] = (byte)value;
			}
		}

		/// <summary>
		/// true - TCP, false - UDP
		/// </summary>
		public bool TCP
		{
			get
			{
				return bytes[DataPos + 52] != '0';
			}
			set
			{
				bytes[DataPos + 52] = (byte)(value? '1': '0');
			}
		}

		/// <summary>
		/// Точка входа в интернет
		/// </summary>
		public string InetEntryPoint
		{
			get
			{
				string result = encoding.GetString(bytes, DataPos + 53, 32);

				int n = result.IndexOf("\0".ToCharArray()[0]);

				if (n >= 0)
					result = result.Remove(n);

				return result;
			}
		}

		/// <summary>
		/// Логин, для доступа в интернет
		/// </summary>
		public string Login
		{
			get
			{
				string result = encoding.GetString(bytes, DataPos + 85, 32);

				int n = result.IndexOf("\0".ToCharArray()[0]);

				if (n >= 0)
					result = result.Remove(n);

				return result;
			}
		}

		/// <summary>
		/// Пароль, для доступа в интернет
		/// </summary>
		public string Password
		{
			get
			{
				string result = encoding.GetString(bytes, DataPos + 117, 32);

				int n = result.IndexOf("\0".ToCharArray()[0]);

				if (n >= 0)
					result = result.Remove(n);

				return result;
			}
		}

		/// <summary>
		/// module (unit) firmware
		/// </summary>
		public string ModuleFW
		{
			get
			{
				return encoding.GetString(bytes, DataPos + 149, 15);
			}
		}

		/// <summary>
		/// terminal (display) firmware
		/// </summary>
		public string TerminalFW
		{
			get
			{
				return encoding.GetString(bytes, DataPos + 164, 15);
			}
		}
	}

	public class SGGBSubDataAlarm : SGGBSubDataGPS
	{
		public SGGBSubDataAlarm(byte[] data, int begin) : base(data, begin)
		{
		}

		public override int ID
		{
			get
			{
				return (int)SGGBDataType.Alarm;
			}
		}
	}

	/// <summary>
	/// confirm message
	/// </summary>
	public class SGGBSubDataConfirm : SGGBSubData
	{
		/// <summary>
		/// incoming confirm
		/// </summary>
		/// <param name="data">byte sequence</param>
		/// <param name="begin">begin pos</param>
		public SGGBSubDataConfirm(byte[] data, int begin) : base(data, begin)
		{
		}

		public SGGBSubDataConfirm(int packetID) : base(false)
		{
			bytes = new byte[Size];
			SetID(bytes, begin, SGGBDataType.SendConfirm);
			SetSize(bytes, begin, Size);
			PacketID = packetID;
		}

		public override int ID
		{
			get
			{
				return GetID(bytes, begin);
			}
		}

		protected override int DataSize
		{
			get
			{
				return 3;
			}
		}

		public int PacketID
		{
			get
			{
				return Datagram.IntFromBytesBE(bytes[DataPos], bytes[DataPos + 1]);
			}
			set
			{
				//if(value < 0 || value > 0xffff) throw new ArgumentOutOfRangeException(
				//    "PacketID", value, "PacketID must be in [0..0xffff]");

				Datagram.IntToBytesBE(value, bytes, DataPos, 2);
			}
		}

		public int State
		{
			get
			{
				return bytes[DataPos + 2];
			}
			set
			{
				bytes[DataPos + 2] = (byte)value;
			}
		}
	}

	/// <summary>
	/// Датаграмма для обмена данными с мобильным устройством SGGB. Данные поступают от устройства.
	/// </summary>
	/// <remarks>
	/// <p>
	/// byte[0] = version of protocol = 0xAA
	/// byte[1..7] = identifier of mobile unit (7 bytes)
	/// bytes[8..10] = identifier of datagram
	/// bytes[11..13] = N length of data N
	/// bytes[14..15] = CRC_1
	/// bytes[16..16+N] = data
	/// bytes[17+N..18+N] = CRC_2
	/// </p>
	/// </remarks>
	public class SGGBOutputDatagram : SGGBDatagram
	{
		#region **************************** Атрибуты *******************************

		/// <summary>
		/// identifier of datagram
		/// </summary>
		public int DataID
		{
			get
			{
				return IntFromBytesBE(bytes[8], bytes[9], bytes[10]);
			}
			set
			{
				IntToBytesBE(value, bytes, 8, 3);
			}
		}

		#endregion // **************************** Атрибуты *******************************

		#region .ctor
		/// <summary>
		/// Датаграмма для обмена данными с мобильным устройством SGGB. 
		/// Данные поступают от устройства.
		/// </summary>
		SGGBOutputDatagram(byte[] data, int pos)
		{
			// find start point 0xAA
			int i = Array.IndexOf(data, (byte)SGGBDataType.SGGB, pos);
			// not found or too short
			if(i == -1 || i + 18 > data.Length) throw new DatagramNotFoundException(
				"Datagram not found or too short", GetType(), data, 0);

			// full size
			int size = 18 + IntFromBytesBE(data[i + 11], data[i + 12], data[i + 13]);
			if(i + size > data.Length) throw new DatagramNotFoundException(
				"Datagram not found or too short", GetType(), data, 0);

			// copy full datagram
			bytes = new byte[size];
			Array.Copy(data, i, bytes, 0, size);

			Debug.WriteLineIf(tsSGGB.TraceVerbose, BitConverter.ToString(bytes), "SGGB");
		}

		/// <summary>
		/// Датаграмма для обмена данными с мобильным устройством SGGB. Данные поступают от устройства.
		/// </summary>
		public SGGBOutputDatagram(uint capacity) : base(capacity)
		{
		}

		#endregion // .ctor

		/// <summary>
		/// create output datagram from sub data
		/// </summary>
		/// <param name="subs">list of sub data</param>
		/// <returns>output datagram</returns>
		public static SGGBOutputDatagram Create(params SGGBSubData[] subs)
		{
			// add all subs' bytes to al
			ArrayList al = new ArrayList(256);
			foreach(SGGBSubData sub in subs) al.AddRange(sub.Bytes);

			// create dg with empty bytes
			SGGBOutputDatagram dg = new SGGBOutputDatagram(18 + (uint)al.Count);
			dg.subs = new ArrayList(subs);
			// ID
			dg.bytes[0] = 0xAA;
			// data length
			IntToBytesBE(al.Count, dg.bytes, 11, 3);
			// create CRC_ID
			dg.BuildCRC_ID();
			// copy subs' bytes to dg
			Debug.Assert(dg.GetBytes().Length == 18 + al.Count);
			al.CopyTo(dg.GetBytes(), 16);
			// create CRC
			dg.BuildCRC();

			return dg;
		}

		public static SGGBOutputDatagram Create(byte[] data)
		{
			int begin = 0;
			SGGBOutputDatagram dg = Create(data, ref begin);
			if(dg != null && dg.Valid && begin != dg.Size) 
				throw new DatagramException("Pos must be after datagram");
			return dg;
		}
		
		/// <summary>
		/// create datagram from byte sequence
		/// </summary>
		/// <param name="data">bytes</param>
		/// <param name="pos">in - pos to search datagram from, out - pos after dg, -1 - not found</param>
		/// <returns>datagram if found whole or null if not</returns>
		public static SGGBOutputDatagram Create(byte[] data, ref int pos)
		{
			try
			{
				Debug.Assert(pos > -1 && pos < g_MaxDatagramLength, "pos out of range");

				// find start point 0xAA
				pos = Array.IndexOf(data, (byte)SGGBDataType.SGGB, pos);
				// not found or too short
				if (pos == -1 || pos + 18 > data.Length || data[11] != 0) return null;

				// full size
				int size = 18 + IntFromBytesBE(data[pos + 11], data[pos + 12], data[pos + 13]);
				if (size >= g_MaxDatagramLength)	// data too big so move pos to next 0xAA
				{	// invalid datagram or wrong 0xAA
					Trace.WriteLineIf(tsSGGB.TraceVerbose, "Size in datagram too big", "SGGB");
					pos = Array.IndexOf(data, (byte)SGGBDataType.SGGB, pos + 1);
					return null;
				}
				// too short or accidental 0xAA not the beginning 
				if (pos + size > data.Length) return null;

				SGGBOutputDatagram dg = new SGGBOutputDatagram(data, pos);
				if (dg.Valid) pos += dg.Size;
				else
				{
					pos = Array.IndexOf(data, (byte)SGGBDataType.SGGB, pos + 1);
					return dg;
				}

				// заполняем блоки данных
				int n = dg.DataSize;
				int posBegin = dg.DataPos; // начало блока данных
				int posEnd = posBegin + n;

				dg.subs = new ArrayList(1);
				while (posBegin < posEnd)
				{
					// sub data real size
					int subN = SGGBSubData.GetSize(dg.bytes, posBegin);
					if (subN < 1)
					{
						pos = Array.IndexOf(data, (byte)SGGBDataType.SGGB, pos + 1);
						if (pos == -1) pos = data.Length;
						return dg;
					}

					try
					{
						SGGBSubData sub = SGGBSubData.Create(dg.bytes, posBegin);
						if (sub != null) dg.subs.Add(sub);
					}
					catch (DatagramFieldException ex)
					{
						Trace.WriteLineIf(tsSGGB.TraceError, ex.Message + "\n\t" + string.Format(
							"Field = {0}, Type = {1}, Data =\n\t{2}", ex.Field, ex.DatagramType,
							BitConverter.ToString(ex.Data)), "SGGB");
					}
					catch (Exception ex)
					{
						Trace.WriteLineIf(tsSGGB.TraceError, ex.Message, "SGGB");
					}
					finally
					{
						posBegin += subN;
					}
				}
				return dg;
			}
			catch (Exception ex)
			{
				Trace.WriteLine("SGGBOutputDatagram Create(...) " + ex.Message +
					"\r\nStack:\r\n" + ex.StackTrace);
				return null;
			}
		}
	}

	/// <summary>
	/// Датаграмма для обмена данными с мобильным устройством SGGB. 
	/// Данные передаются на устройство.
	/// </summary>
	public class SGGBInputDatagram : SGGBDatagram
	{
		#region .ctor

		/// <summary>
		/// Датаграмма для обмена данными с мобильным устройством SGGB. Данные передаются на устройство.
		/// </summary>
		public SGGBInputDatagram(uint capacity) : base(capacity)
		{
			// ID
			bytes[0] = (byte)SGGBDataType.SGGB;
		}

		#endregion // .ctor

		/// <summary>
		/// create input datagram from sub data list
		/// </summary>
		/// <param name="packetID">list of sub data</param>
		/// <param name="subs">list of sub data</param>
		/// <returns>datagram</returns>
		public static SGGBInputDatagram Create(int packetID, params SGGBSubData[] subs)
		{
			// add all subs' bytes to al
			ArrayList al = new ArrayList(256);
			foreach(SGGBSubData sub in subs)
			{
				if(!sub.Valid) 
					throw new DatagramException("Sub data not properly initialized");
				al.AddRange(sub.Bytes);
			}

			// create dg with empty bytes
			SGGBInputDatagram dg = new SGGBInputDatagram(18 + (uint)al.Count);
			dg.SetPacketID(packetID);
			// data length
			IntToBytesBE(al.Count, dg.bytes, 11, 3);
			// create CRC_ID
			dg.BuildCRC_ID();
			// copy subs' bytes to dg
			Debug.Assert(dg.GetBytes().Length == 18 + al.Count);
			al.CopyTo(dg.GetBytes(), 16);
			// create CRC
			dg.BuildCRC();

			return dg;
		}
		
		public static SGGBInputDatagram Create(params SGGBSubData[] subs)
		{
			return Create(0, subs);
		}
	}
}