﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Communication;
using FORIS.TSS.Config;
using FORIS.TSS.Terminal.Messaging;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.SGGB
{
	/// <summary>
	/// SGGB device
	/// </summary>
	/// <remarks>contains all SGGB controller specific</remarks>
	public class SGGB :  Device
	{
		//
		/// <summary>
		/// link type. UDP, GSM, SMS only
		/// </summary>
		private readonly CmdType ctLink = CmdType.Unspecified;

		// TODO params wrapper
		// named param list
		const string SFromTimePar = "FromTime";
		const string SToTimePar = "ToTime";
		const string SMsgNoPar = "MessageNo";
		
		// log request response
		const string SBeforeBegin = "Начало запрашиваемого интервала выходит " + 
			"за пределы архива, запрос проигнорирован";
		const string SAfterEnd = "Конец запрашиваемого интервала выходит " + 
			"за пределы архива, запрос проигнорирован";
		const string SFinishBeforeStart = "Начало запрашиваемого интервала " + 
			"больше конца запрашиваемого интервала, запрос проигнорирован";
		const string SInitInProcess = "Инициализация архива не закончена, ожидайте";
		static readonly string[] ArLogRes = new[]
			{
				null, SBeforeBegin, SAfterEnd, SFinishBeforeStart, SInitInProcess
			};
		
		// send text response
		const string SDelivered = "Сообщение доставлено";
		const string SAccepted = "Сообщение прочитано";
		static readonly string[] ArTextRes = new[]
			{
				null, SDelivered, SAccepted
			};

		readonly static TraceSwitch TsSGGB = new TraceSwitch("TraceSGGB", "Info from SGGB device");

		readonly TraceSwitch _tsDg = new TraceSwitch("Datagram", "Info about datagram");

		/// <summary>
		/// logger
		/// </summary>
		static readonly StreamWriter SwSGGBData;

		/// <summary>
		/// Сохранять "плохие" позиции
		/// </summary>
		static readonly bool BStoreBadPositions;

		static SGGB()
		{
			var sLogLoc = GlobalsConfig.AppSettings["LogLocation"];
			if (!string.IsNullOrWhiteSpace(sLogLoc))
			{
				if (!sLogLoc.EndsWith(@"\"))
					sLogLoc += @"\";

				try
				{
					SwSGGBData = new StreamWriter(sLogLoc + "sggbdata.log", true) {AutoFlush = true};
				}
				catch (Exception e)
				{
					Trace.TraceError("{0}: Unable to init logger: {1}", typeof (SGGB), e);
				}
			}
			if(GlobalsConfig.AppSettings["StoreBadPositions"] != null)
				BStoreBadPositions = GlobalsConfig.AppSettings["StoreBadPositions"].ToLower() == "true";
		}

		#region IDevice Members
		internal static class DeviceNames
		{
			internal const string SureGuardSGGB = "SureGuard SGGB";
		}
		public SGGB() : base(null, new[]
		{
			DeviceNames.SureGuardSGGB,
		})
		{
		}

		public override Datagram InputDatagram
		{
			get
			{
				return new SGGBInputDatagram(0);
			}
		}

		public override Datagram OutputDatagram(byte[] data, out int begin)
		{
			begin = 0;
			try
			{
				return SGGBOutputDatagram.Create(data, ref begin);
			}
			catch(DatagramNotFoundException)	// full dg can be partial
			{
				return null;
			}
		}
		ArrayList OutputDatagram(byte[] data)
		{
			// list of output dg
			var alOut = new ArrayList(1);
			if (data.Length == 0) return alOut;

			byte[] buf = data;

			int prev, pos = 0, len = buf.Length;
			do
			{
				prev = pos;
				// make attempt create output dg
				SGGBOutputDatagram dg = SGGBOutputDatagram.Create(buf, ref pos);
				// succeeded
				if (dg != null)
				{
					Debug.Assert(!dg.Valid || pos > 0 && pos <= len, "Incorrect pos of next dg");
					alOut.Add(dg);
				}
			}
			while (pos > -1 && pos < len && prev < pos && alOut.Count < 256);	// pause parsing at 256 dg
			// another try. look if it's SMS and just sub data received
			if (ctLink == CmdType.SMS)
			{
				SGGBSubData sub = SGGBSubData.Create(buf, 0);
				if (sub == null)	// no more data expected
				{
					return null;
				}
				Debug.Assert(sub.Size <= data.Length,
					"byte count in sub data > input bytes");
				alOut.Add(SGGBOutputDatagram.Create(sub));
			}
			
			return alOut;
		}

		/// <summary>
		/// convert command to byte sequence
		/// </summary>
		/// <param name="cmd">standard command</param>
		/// <returns>byte sequence</returns>
		public override byte[] GetCmd(IStdCommand cmd)
		{
			SGGBSubData data;
			switch(cmd.Type)
			{
			case CmdType.Control:
				{
				var sub = new SGGBSubDataControl();
				sub.Pump = sub.Pump1 = (int)cmd.Params["Pump"] != 0;
				sub.Lights = (int)cmd.Params["Lights"] != 0;
				sub.Horn = (int)cmd.Params["Horn"] != 0;
				data = sub;
				}
				break;

			case CmdType.SendText:
				data = new SGGBSubDataText(cmd.Params["Text"].ToString());
				break;
				
			case CmdType.Confirm:
				data = new SGGBSubDataConfirm((int)cmd.Params["ConfirmNo"]);
				break;

				case CmdType.GetLog:
					{
						var sub = new SGGBSubDataGetLog
						{
							From = ((DateTime)cmd.Params[SFromTimePar]),
							To = ((DateTime)cmd.Params[SToTimePar])
						};
						data = sub;
					}
					break;
				case CmdType.SetFirmware:
					{
						var sub = new SGGBSubDataSetFirmware(((byte[])cmd.Params["Data"]).Length + 1)
						{
							Index = ((int)cmd.Params["Index"]),
							FirmwareData = ((byte[])cmd.Params["Data"])
						};

						data = sub;
					}
					break;
				case CmdType.GetSettings:
				{
					var sub = new SGGBSubDataGetSettings();
	 
					data = sub;
				}
				break;
			case CmdType.Restart:
				{
					var sub = new SGGBSubDataRestart();

					data = sub;
				}
				break;
			case CmdType.SetSettings:
				{
					Debug.WriteLine("SGGB: SetSettings...");
					var sub = new SGGBSubDataSetSettings
													 {
														 IP = ((IPAddress) cmd.Params["IP"]),
														 Port = ((int) cmd.Params["Port"]),
														 TCP = ((bool) cmd.Params["Tcp"]),
														 Interval = ((int) cmd.Params["Interval"]),
														 VoicePhone = ((string) cmd.Params["VoicePhone"]),
														 DataPhone = ((string) cmd.Params["DataPhone"]),
														 TimeZone = ((int) cmd.Params["TimeZone"]),
														 EntryPoint = ((string) cmd.Params["EntryPoint"]),
														 Login = ((string) cmd.Params["Login"]),
														 Password = ((string) cmd.Params["Password"]),
														 UnitNo = ((int) cmd.Params["UnitNo"])
													 };

					data = sub;
				}
				break;
				
			default: return null;
			}

			SGGBDatagram dg;
			if(cmd.Params[SMsgNoPar] is int) 
				dg = SGGBInputDatagram.Create((int)cmd.Params[SMsgNoPar], data);
			else dg = SGGBInputDatagram.Create(data);
			
			Debug.Assert(dg.Valid, "Datagram invalid");

			if (ctLink == CmdType.SMS)
				return Postprocess(dg.GetBytes());

			return dg.GetBytes();
		}

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;
			if(!Initialized) 
				throw new ApplicationException("Device not initialized");

			// list of wrapped responses
			var alRes = new ArrayList(1);

			if (ctLink == CmdType.SMS)
				data = Preprocess(data, count);
			try
			{
				List<int> datagrams;
				DatagramasShiftsInPacket(data, out datagrams);
				if (datagrams.Count == 0 && ctLink != CmdType.SMS)
					return null;
				foreach (int offset in datagrams)
				{
					//Для истории и позиций делаем отельный парсинг
					if (DataIsGpsOrLog(data, offset))
					{
						int packetID;
						MobilUnit.Unit.MobilUnit mu = ParseGPSData(data, offset, out packetID);
						if (mu != null)
						{
							IMessage msg = MessageCreator.CreatePosition(mu, mu.ValidPosition);
							msg.Num = packetID;
							alRes.Add(msg);
						}
					}
					else if(IsDataConfirm(data, offset))
					{
						
					}
				}
				//if (alRes.Count > 0)
				//    return alRes;
				// parse cycle
				foreach(SGGBOutputDatagram dg in OutputDatagram(data))
				{
					if(!dg.Valid) 
					{
						// !! WARN !!. if dg is bad we must reject all buf
						// but we can suggest it's possible to find next dg in buf
						//Debug.Fail("Invalid datagram");
						Debug.WriteLineIf(TsSGGB.TraceWarning, "Invalid datagram", "SGGB");
						Debug.Indent();
						Debug.WriteLineIf(TsSGGB.TraceWarning, dg, "SGGB");
						Debug.Unindent();
						
						if(!_tsDg.TraceWarning) 
							continue;
						
						if (SwSGGBData != null)
						{
							SwSGGBData.WriteLine("{0}: Invalid datagram", DateTime.Now);
							SwSGGBData.WriteLine("\t" + dg);
						}
						continue;
					}

					IUnitInfo ui = UnitInfo.Factory.Create(UnitInfoPurpose.SGGB);
					ui.ID = dg.UnitNo;
					
					// loop for sub data in dg
					foreach(SGGBSubData sub in dg.DataField)
					{
						if(!sub.Valid)
						{
							if(TsSGGB.TraceVerbose)
							{
								Debug.WriteLine(DateTime.Now);
								Debug.WriteLine(string.Format("Invalid sub data {0:X2}", sub.ID), "SGGB");
								Debug.Indent();
								Debug.WriteLine(sub);
								Debug.WriteLine("in datagram");
								Debug.WriteLine(dg);
								Debug.Unindent();
							}
							if(_tsDg.TraceWarning && SwSGGBData != null)
							{
								SwSGGBData.WriteLine(
									"{0}: Invalid sub data {1:X2}", DateTime.Now, sub.ID);
								SwSGGBData.WriteLine("\t" + sub);
								SwSGGBData.WriteLine("\t in datagram");
								SwSGGBData.WriteLine("\t" + dg);
							}
#if !DEBUG
							if(sub.ID != (int)SGGBDataType.Alarm) continue;
#endif
						}
						// confirm received
						if(sub.ID == (int)SGGBDataType.Confirm)
						{
							var conf = (SGGBSubDataConfirm)sub;
							string txt = "Подтверждение";
							if(Manager != null)
							{
								var msg = 
									Manager.GetRegisteredMessage(conf.PacketID) as ICommandMessage;
								if(msg != null) 
								{
									var cmd = msg.Command as IStdCommand;
									if(cmd != null)
									{
										switch(cmd.Type)
										{	// log request refused
										case CmdType.GetLog:
											txt = ArLogRes[conf.State];
											break;
											// message delivered
										case CmdType.SendText:
											txt = ArTextRes[conf.State];
											break;
										}
									}
								}
							}
							// add confirm to out list
							alRes.Add(new NotifyEventArgs(TerminalEventMessage.NE_CONFIRM, null, Severity.Lvl3, 
								NotifyCategory.Controller, txt, null, ui, "", conf.PacketID, conf.State, 
								dg.PacketID, Mode));
							continue;
						}

						if(ctLink != CmdType.SMS && ctLink != CmdType.UDP && ctLink != CmdType.GSM) 
							continue;

						var gps = sub as SGGBSubDataGPS;
						if (gps != null)
						{
							if (!sub.Valid) continue;

							var pos = gps;
							IMobilUnit mu = GetMobilUnit(dg.UnitNo, pos);

							bool correctGPS = pos.CorrectGPS;

							IMessage msg = MessageCreator.CreatePosition(mu, correctGPS);
							msg.Num = dg.PacketID;

							if (!correctGPS)
							{
								if (TsSGGB.TraceVerbose)
								{
									Debug.WriteLine(DateTime.Now);
									Debug.WriteLine(string.Format("Invalid pos data {0:X2}", sub.ID), "SGGB");
									Debug.Indent();
									Debug.WriteLine(gps);
									Debug.WriteLine("in datagram");
									Debug.WriteLine(dg);
									Debug.Unindent();
								}
								if (_tsDg.TraceWarning && SwSGGBData != null)
								{
									SwSGGBData.WriteLine(
										"{0}: Invalid pos data {1:X2}", DateTime.Now, sub.ID);
									SwSGGBData.WriteLine("\t" + sub);
									SwSGGBData.WriteLine("\t in datagram");
									SwSGGBData.WriteLine("\t" + dg);
								}

								if (!BStoreBadPositions)
									goto label1;
							}
							alRes.Add(msg);
						}
					label1:
						if (sub.ID == (int)SGGBDataType.Text)
						{
							string txt = ((SGGBSubDataText)sub).Text;
							var par = new object[4];
							par[0] = ui;
							par[1] = -1;
							par[2] = txt;
							par[3] = dg.PacketID;
							alRes.Add( new NotifyEventArgs( TerminalEventMessage.NE_MESSAGE, null, Severity.Lvl3, 
								NotifyCategory.Controller, txt, null, par));
						}
						else if(sub.ID == (int)SGGBDataType.Alarm)
						{
							var alr = (SGGBSubDataAlarm)sub;
							var par = new object[4];
							par[0] = ui;
							par[1] = "";
							par[2] = 1;
							par[3] = dg.PacketID;
							alRes.Add( new NotifyEventArgs( (time_t)alr.Seconds, TerminalEventMessage.NE_ALARM, null, 
								Severity.Lvl1, NotifyCategory.Controller, "Тревога", null, par));
						}
						else if (sub.ID == (int)SGGBDataType.ConfirmFirmware)
						{
							var conf = (SGGBSubDataConfirmFrirmware)sub;
							object[] par = new object[4];
							par[0] = ui;
							par[1] = conf.Index;
							par[2] = par[3] = "";
							alRes.Add(new NotifyEventArgs(TerminalEventMessage.NE_CONFIRM_FIRMWARE, null,
								Severity.Lvl2, NotifyCategory.Controller, "Прошивка", null, par));
						}
						else if (sub.ID == (int)SGGBDataType.Settings)
						{
							Debug.WriteLine(DateTime.Now + " SGGB Settings Received");
							SGGBSubDataSettings settings = (SGGBSubDataSettings)sub;
							var par = new object[13];
							par[0] = ui;
							par[1] = settings.VoicePhone;
							par[2] = settings.ModuleFW;
							par[3] = settings.TerminalFW;
							par[4] = settings.TCP;
							par[5] = settings.Port;
							par[6] = settings.IP;
							par[7] = settings.Interval;
							par[8] = settings.DataPhone;
							par[9] = settings.InetEntryPoint;
							par[10] = settings.Login;
							par[11] = settings.Password;
							par[12] = settings.TimeZone;
							alRes.Add(new NotifyEventArgs(TerminalEventMessage.NE_SETTINGS, null,
								Severity.Lvl2, NotifyCategory.Controller, "Настройки контроллера", null, par));
						}
					}
				}
			}
			catch(Exception ex)
			{
				Trace.WriteLine(ex.ToString(), "SGGB");
			}
			return alRes;
		}

		private static bool IsDataConfirm(byte[] data, int offset)
		{
			if (data[offset + 0] != (byte)SGGBDataType.SGGB)
				return false;


			var id = (SGGBDataType)SGGBSubData.GetID(data, offset + 16);
			if(id != SGGBDataType.Confirm)
				return false;

			return true;
		}

		private MobilUnit.Unit.MobilUnit ParseGPSData(byte[] bytes, int offset, out int packetID)
		{
			const int headerSize = 19;

			const float marineKnot = (float)1.852;

			int pos = 9;
			packetID = Datagram.IntFromBytesBE(bytes[offset + pos], bytes[offset + pos + 1]);

			#region longitude
			
			pos = headerSize + FieldInfo.PosLongitude;
			float longitude = Datagram.IntFromBytesBE(bytes[offset + pos], bytes[offset + pos + 1]);
			// минуты 
			longitude += Datagram.IntFromBytesBE(bytes[offset + pos + 2]) / 60.0f;
			// доли минут 
			longitude += Datagram.FloatFromBytesBE(bytes[offset + pos + 3], bytes[offset + pos + 4],
				bytes[offset + pos + 5], bytes[offset + pos + 6]) / 60;
			
			#endregion //longitude

			#region latitude
			pos = headerSize + FieldInfo.PosLatitude;
			// градусы
			float latitude = Datagram.IntFromBytesBE(bytes[offset + pos]);
			// минуты 
			latitude += Datagram.IntFromBytesBE(bytes[offset + pos + 1]) / 60.0f;
			// доли минут 
			latitude += Datagram.FloatFromBytesBE(bytes[offset + pos + 2], bytes[offset + pos + 3],
				bytes[offset + pos + 4], bytes[offset + pos + 5]) / 60;
			#endregion //latitude

			#region north

			bool north = (char) bytes[offset + headerSize + FieldInfo.PosNorth] == 'N';

			#endregion //north

			#region east

			bool east = (char) bytes[offset + headerSize + FieldInfo.PosEast] == 'E';

			#endregion //east

			#region seconds

			int seconds;

			pos = headerSize + FieldInfo.PosSeconds;
			int size = FieldInfo.SizeSeconds;

			if (size == 4) 
				seconds = Datagram.IntFromBytesBE(
					 bytes[offset + pos], bytes[offset + pos + 1], bytes[offset + pos + 2], bytes[offset + pos + 3]);
			else
			{
				#region Старая прошивка

				Debug.Fail("No more short time support");
				Debug.Assert(size == 3, "Unsupported time");

				// secs from beginning of month
				int secs = Datagram.IntFromBytesBE(bytes[offset + pos], bytes[offset + pos + 1], bytes[offset + pos + 2]);
				// time bug spec
				if (secs > 32 * 24 * 60 * 60) throw new DatagramFieldException(
					 "Incorrect datagram field received. Seconds - " + secs, typeof(SGGB),
					 bytes, 0, "Seconds");

				DateTime tm = DateTime.Today;
				// add secs to begin of current month
				tm = new DateTime(tm.Year, tm.Month, 1);
				// check if prev month
				if (tm.AddSeconds(secs) - DateTime.UtcNow > TimeSpan.FromDays(2))
					tm = tm.AddMonths(-1);
				// check our time bug
				else if (tm.AddSeconds(secs) - DateTime.UtcNow > TimeSpan.FromHours(2))
					tm = tm.AddDays(-1);

				tm = tm.AddSeconds(secs);
#if DEBUG
				DateTime now = DateTime.UtcNow;
				if (tm - now > TimeSpan.FromMinutes(1))
					Trace.WriteLineIf(TsSGGB.TraceError, String.Format(
						"Incorrect time in pos: {0}, now: {1}, diff: {2}", tm, now, tm - now),
						"SGGB");
#endif
				// convert tm to time_t
				seconds = (time_t)tm;

				#endregion Старая прошивка
			}

			#endregion //seconds

			#region satellites

			int satellites = bytes[offset + headerSize + FieldInfo.PosCorrectGPS];

			satellites = satellites > 0 ? satellites : -1;

			#endregion //satellites

			#region speed

			pos = headerSize + FieldInfo.PosSpeed;
			float speed =
				Datagram.FloatFromBytesBE(bytes[offset + pos], bytes[offset + pos + 1], bytes[offset + pos + 2], bytes[offset + pos + 3]);
			speed *= marineKnot;

			#endregion //speeed

			#region correctGPS

			bool correctGPS = 
				satellites > 0 && 
				longitude > 0 && longitude < 180 &&
				latitude > 0 && latitude < 90 &&
				(byte)speed <= MobilUnit.Unit.MobilUnit.GPSLimitMaxSpeed;

			#endregion //correctGPS

			#region analogSensor1
			
			pos = headerSize + FieldInfo.PosAnalogSensor1;
			int analogSensor1 = Datagram.IntFromBytesBE(bytes[offset + pos], bytes[offset + pos + 1]);
			
			#endregion analogSensor1

			#region analogSensor2

			pos = headerSize + FieldInfo.PosAnalogSensor2;
			int analogSensor2 = Datagram.IntFromBytesBE(bytes[offset + pos], bytes[offset + pos + 1]);

			#endregion analogSensor2

			#region temperature

			int temperature = Datagram.IntFromBytesBE(bytes, headerSize + FieldInfo.PosTemperature, 1);
			
			#endregion temperature

			#region digits
			
			pos = headerSize + FieldInfo.PosDigits;
			int digits = Datagram.IntFromBytesBE(bytes[offset + pos], bytes[offset + pos + 1]);

			#endregion

			var mu = UnitReceived();
			mu.ID = Datagram.IntFromBytesBE(bytes[offset + 7], bytes[offset + 8]);

			var id = (SGGBDataType)SGGBSubData.GetID(bytes, 16+offset);
			if (id == SGGBDataType.GPS)
			{
				mu.cmdType = CmdType.Trace;
			}
			else if(id == SGGBDataType.Log)
			{
				mu.cmdType = CmdType.GetLog;
			}
			else
			{
				throw new InvalidOperationException("Invalid mu.cmdType " + mu.cmdType);
			}

			mu.Latitude = north ? latitude : -1 * latitude;
			mu.Longitude = east ? longitude : -1 * longitude;
			mu.Speed = (int)speed;
			mu.Time = seconds;
			mu.VoltageAN1 = temperature != SGGBSubDataGPS.AbsoluteZero ? temperature : int.MaxValue;

			mu.VoltageAN2 = int.MaxValue;
			mu.VoltageAN3 = analogSensor1;
			mu.VoltageAN4 = analogSensor2;

			mu.CorrectGPS = correctGPS;

			return mu;
		}

		private static bool DataIsGpsOrLog(byte[] data, int offset)
		{
			if(data[offset + 0] != (byte)SGGBDataType.SGGB)
				return false;

			if(offset+57 > data.Length)
				return false;

			int size = 18 + Datagram.IntFromBytesBE(data[offset + 11], data[offset + 12], data[offset + 13]);
			if(size != 57)
				return false;
			SGGBDataType id = (SGGBDataType)SGGBSubData.GetID(data, offset + 16);
			if (id == SGGBDataType.GPS || id == SGGBDataType.Log)
				return true;

			return false;
		}

		/// <summary>
		/// Возвращает начала всех датаграмм в пакете
		/// </summary>
		/// <param name="data">пакет с данными</param>
		/// <param name="list">список смещений датаграмм в пакете</param>
		private static void DatagramasShiftsInPacket(byte[] data, out List<int> list)
		{
			list = new List<int>();

			int begin = 0;

			for (; ;)
			{
				if(begin >= data.Length)
					break;
				int pos = Array.IndexOf(data, (byte) SGGBDataType.SGGB, begin);
				if (pos >= 0 && pos+11 < data.Length && data[11]==0)
				{
					list.Add(pos);

					int size = 18 + Datagram.IntFromBytesBE(data[pos + 11], data[pos + 12], data[pos + 13]);
					begin = pos+size;
				}
				else
					break;
			}
		}

		/// <summary>
		/// check if device support bytes as datagram
		/// </summary>
		/// <param name="data">byte sequence</param>
		/// <returns>success</returns>
		public override bool SupportData(byte[] data)
		{
			if(!(data[0]==0xAA && data.Length > 11 && data[11] ==0) )
				return false;

			List<int> list;
			DatagramasShiftsInPacket(data, out list);
			if(list != null && list.Count != 0)
			{
				foreach(int i in list)
				{
					if (IsDataConfirm(data, i))
						return true;
					if(DataIsGpsOrLog(data, i))
						return true;
				}
			}

			// make attempt create output dg
			int prev, pos = 0, len = data.Length;
			do
			{
				prev = pos;
				// make attempt create output dg
				SGGBOutputDatagram dg = SGGBOutputDatagram.Create(data, ref pos);
				// succeeded
				if(dg != null && dg.Valid) return true;
			}
			while(pos > -1 && pos < len && prev < pos);

			return false;
		}

		/// <summary>
		/// check if known datagram
		/// </summary>
		/// <param name="dg">datagram</param>
		/// <returns>success</returns>
		public override bool SupportData(Datagram dg)
		{
			return dg is SGGBDatagram;
		}

		#endregion

		/// <summary>
		/// create IMobilUnit from suitable sub data
		/// </summary>
		/// <param name="unitNo">controller no.</param>
		/// <param name="sub">sub data. GPS based block</param>
		/// <returns>mobil unit</returns>
		IMobilUnit GetMobilUnit(int unitNo, SGGBSubDataGPS sub)
		{
			MobilUnit.Unit.MobilUnit mu = UnitReceived();
			mu.ID = unitNo;
			mu.cmdType = (sub.ID == (int) SGGBDataType.Log ? CmdType.GetLog : CmdType.Trace);
			mu.Latitude = sub.Latitude;
			mu.Longitude = sub.Longitude;
			mu.Speed = sub.Speed;
			mu.Time = sub.Seconds;
			mu.Satellites = sub.Satellites;
			// Log -> GetLog. GPS, Alarm -> Trace

			mu.PowerVoltage = int.MaxValue;
			
			int temper = sub.Temperature;
			mu.VoltageAN1 = temper != SGGBSubDataGPS.AbsoluteZero ? temper : int.MaxValue;
			
			mu.VoltageAN2 = int.MaxValue;
			mu.VoltageAN3 = sub.AnalogSensor1;
			mu.VoltageAN4 = sub.AnalogSensor2;

			mu.CorrectGPS = sub.CorrectGPS;

			return mu;
		}

		/// <summary>
		/// preprocess data from controller. ONLY COUNT BYTES
		/// </summary>
		/// <param name="data">bytes from controller</param>
		/// <param name="count">bytes length</param>
		/// <returns>res bytes</returns>
		byte[] Preprocess(byte[] data, int count)
		{
			byte[] res;

			// SMS data in special format. preprocessing.
			// every byte divided into 2.
			// ex. res[i] = LOBYTE(data[2 * i]) << 4 | LOBYTE(data[2 * i + 1])
			if(ctLink == CmdType.SMS)
			{
				res = new byte[count / 2];
				for(int i = 0, len = res.Length; i < len; ++i) 
					res[i] = (byte)((data[2 * i] & 0xf) << 4 | data[2 * i + 1] & 0xf);
				return res;
			}
			res = new byte[count];
			Array.Copy(data, res, count);
			return res;
		}

		/// <summary>
		/// post process data to controller
		/// </summary>
		/// <param name="data">bytes to controller</param>
		/// <returns>res bytes</returns>
		byte[] Postprocess(byte[] data)
		{
			// sms data in special format. post processing.
			// every byte divided into 2.
			// ex. res[2 * i] = 0x60 | HIBYTE(data[i]); res[2 * i + 1] = 0x60 | LOBYTE(data[i])
			if(ctLink == CmdType.SMS)
			{
				byte[] res = new byte[data.Length * 2];
				for(int i = 0, len = data.Length; i < len; ++i) 
				{
					res[2 * i] = (byte)(0x40 | data[i] >> 4);
					res[2 * i + 1] = (byte)(0x40 | data[i] & 0xf);
				}
				return res;
			}
			return data;
		}
	}
}
