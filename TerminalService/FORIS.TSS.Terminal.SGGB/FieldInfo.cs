﻿namespace FORIS.TSS.Terminal.SGGB
{
	public class FieldInfo
	{
		public static readonly int PosSeconds        = 0;  // 4
		public static readonly int PosLatitude       = 4; // 6
		public static readonly int PosNorth          = 10; // 1
		public static readonly int PosLongitude      = 11; // 7
		public static readonly int PosEast           = 18; // 1
		public static readonly int PosCorrectGPS     = 19; // 1
		public static readonly int PosDigits         = 20; // 2
		public static readonly int PosAnalogSensor1  = 22; // 2
		public static readonly int PosAnalogSensor2  = 24; // 2
		public static readonly int PosControlPos     = 26; // 5
		public static readonly int PosSpeed          = 31; // 4
		public static readonly int PosTemperature    = 35; // 1

		public static readonly int SizeSeconds       = 4;
		public static readonly int SizeLatitude      = 6;
		public static readonly int SizeNorth         = 1;
		public static readonly int SizeLongitude     = 7;
		public static readonly int SizeEast          = 1;
		public static readonly int SizeCorrectGPS    = 1;
		public static readonly int SizeDigits        = 2;
		public static readonly int SizeAnalogSensor1 = 2;
		public static readonly int SizeAnalogSensor2 = 2;
		public static readonly int SizeControlPos    = 5;
		public static readonly int SizeSpeed         = 4;
		public static readonly int SizeTemperature   = 1;
	}
}