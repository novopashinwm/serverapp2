﻿namespace Compass.Ufin.Terminal.Devices.MiniFinder
{
	public enum MiniFinderSensors
	{
		Battery  = 1,
		SOS      = 2,
		FallDown = 3,
		Motion   = 4,
		Movement = 5
	}
}