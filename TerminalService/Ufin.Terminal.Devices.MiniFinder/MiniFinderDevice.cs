﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Terminal;
using FORIS.TSS.Helpers;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace Compass.Ufin.Terminal.Devices.MiniFinder
{
	public class MiniFinderDevice : Device
	{
		internal static class DeviceNames
		{
			internal const string MiniFinderAttoVG30 = "MiniFinder Atto (VG30)";
			internal const string MiniFinderPicoVG10 = "MiniFinder Pico (VG10)";
			internal const string MiniFinderPicoVG20 = "MiniFinder Pico (VG20)";
		}
		public MiniFinderDevice() : base(typeof (MiniFinderSensors), new[]
		{
			DeviceNames.MiniFinderAttoVG30,
			DeviceNames.MiniFinderPicoVG10,
			DeviceNames.MiniFinderPicoVG20,
		})
		{
		}

		private const string LoginHeader = "!1";
		private const string RealtimeDataHeader = "!D";
		private readonly byte[] _loginPrefix = Encoding.ASCII.GetBytes(LoginHeader + ",");

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;

			var s = Encoding.ASCII.GetString(data, 0, count-1);
			var reader = new SplittedStringReader(s, ',');

			var result = new ArrayList(1);

			var receiverState = stateData as ReceiverState;

			var header = reader.ReadString();
			switch (header)
			{
				case LoginHeader:
					var imei = reader.ReadString();
					result.Add(new ReceiverStoreToStateMessage {StateData = new ReceiverState {DeviceID = imei}});
					break;
				case RealtimeDataHeader:
					var mobilUnit = ReadMobilUnit(reader);
					if (mobilUnit == null)
						return null;
					if (receiverState != null)
						mobilUnit.DeviceID = receiverState.DeviceID;
					result.Add(mobilUnit);
					break;
			}
			
			return result;
		}

		private MobilUnit ReadMobilUnit(SplittedStringReader reader)
		{
			var date = reader.TryReadDate("dd/MM/yy");
			var time = reader.ReadTime(@"h\:m\:s");
			int logTime;
			if (date != null && time != null)
				logTime = TimeHelper.GetSecondsFromBase(date.Value.Add(time.Value));
			else
				logTime = TimeHelper.GetSecondsFromBase();

			var latitude = reader.ReadDouble();
			var longitude = reader.ReadDouble();
			var speed = reader.ReadDouble();
			var course = reader.ReadDouble();
			var flags = reader.ReadHexInt();
			var altitude = reader.ReadDouble();
			var battery = reader.ReadInt();
			var satellitesInUse = reader.ReadInt();
			reader.Skip(2); //satellite in view + reserved
			var mu = new MobilUnit() { CorrectGPS = false };

			mu.Time = logTime;

			if (latitude != null && longitude != null && speed != null && course != null && altitude != null)
			{
				mu.Latitude   = latitude.Value;
				mu.Longitude  = longitude.Value;
				mu.Speed      = (int)Math.Round(speed.Value);
				mu.Course     = (int)Math.Round(course.Value);
				mu.Height     = (int)Math.Round(altitude.Value);
				mu.Satellites = satellitesInUse;
				mu.CorrectGPS = true;
			}

			mu.SensorValues = new Dictionary<int, long>();
			mu.SensorValues[(int)MiniFinderSensors.Battery]  = battery;
			mu.SensorValues[(int)MiniFinderSensors.SOS]      = (flags >>  6) & 0x01;
			mu.SensorValues[(int)MiniFinderSensors.FallDown] = (flags >>  8) & 0x01;
			mu.SensorValues[(int)MiniFinderSensors.Motion]   = (flags >> 14) & 0x01;
			mu.SensorValues[(int)MiniFinderSensors.Movement] = (flags >> 15) & 0x01;

			return mu;
		}

		public override bool SupportData(byte[] data)
		{
			//!1,860719020212514;

			if (data.Length != _loginPrefix.Length /*!1,*/+ ImeiHelper.ImeiLength + 1 /*;*/)
				return false;

			if (!data.StartsWith(_loginPrefix) || data[data.Length - 1] != ';')
				return false;

			var imei = Encoding.ASCII.GetString(data, _loginPrefix.Length, ImeiHelper.ImeiLength);

			if (!ImeiHelper.IsImeiCorrect(imei))
				return false;

			return true;
		}

		public override List<CommandStep> GetCommandSteps(IStdCommand command)
		{
			switch (command.Type)
			{
				case CmdType.Setup:
					return GetSetupSteps(command);
				case CmdType.SetInterval:
					return GetSetIntervalSteps(command);
				case CmdType.Status:
					return GetStatusSteps(command);
				case CmdType.SetSos:
					return GetSetSosSteps(command);
				case CmdType.DeleteSOS:
					return GetDeleteSOSSteps(command);
				case CmdType.AskPosition:
					return GetAskPositionSteps(command);
				case CmdType.SetModeOnline:
					return GetSetModeOnlineSteps(command);
				case CmdType.SetModeWaiting:
					return GetSetModeWaitingSteps(command);
				default:
					return base.GetCommandSteps(command);
			}
		}
		private List<CommandStep> GetSetupSteps(IStdCommand command)
		{
			var apnCfg = GetInternetApnConfig(command);
			var srvAdd = Manager.GetConstant(Constant.ServerIP);
			var srvPrt = Manager.GetConstant(Constant.ServerPort);
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.MiniFinderAttoVG30:
				case DeviceNames.MiniFinderPicoVG10:
				case DeviceNames.MiniFinderPicoVG20:
					commands = new List<string>
					{
						$@"s1,{apnCfg.Name},{apnCfg.User},{apnCfg.Pass}",
						$@"ti60s",
						$@"tz+00",
						$@"ps0",
						$@"ds0",
						$@"cl0",
						$@"s2",
						$@"ip1,{srvAdd},{srvPrt}",
					};
					break;
			}
			return commands.ConvertAll(x => (CommandStep)new SmsCommandStep(x));
		}

		private List<CommandStep> GetSetIntervalSteps(IStdCommand command)
		{
			var apnCfg = GetInternetApnConfig(command);
			var srvAdd = Manager.GetConstant(Constant.ServerIP);
			var srvPrt = Manager.GetConstant(Constant.ServerPort);
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.MiniFinderAttoVG30:
				case DeviceNames.MiniFinderPicoVG10:
				case DeviceNames.MiniFinderPicoVG20:
					commands = new List<string>
					{
						$@"ti60s",
					};
					break;
			}
			return commands.ConvertAll(x => (CommandStep)new SmsCommandStep(x));
		}
		private List<CommandStep> GetStatusSteps(IStdCommand command)
		{
			var apnCfg = GetInternetApnConfig(command);
			var srvAdd = Manager.GetConstant(Constant.ServerIP);
			var srvPrt = Manager.GetConstant(Constant.ServerPort);
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.MiniFinderAttoVG30:
				case DeviceNames.MiniFinderPicoVG10:
				case DeviceNames.MiniFinderPicoVG20:
					commands = new List<string>
					{
						$@"status",
					};
					break;
			}
			return commands.ConvertAll(x => (CommandStep)new SmsCommandStep(x));
		}
		private List<CommandStep> GetSetSosSteps(IStdCommand command)
		{
			var apnCfg = GetInternetApnConfig(command);
			var srvAdd = Manager.GetConstant(Constant.ServerIP);
			var srvPrt = Manager.GetConstant(Constant.ServerPort);
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.MiniFinderAttoVG30:
					break;
				case DeviceNames.MiniFinderPicoVG10:
				case DeviceNames.MiniFinderPicoVG20:
					commands = new List<string>
					{
						$@"a1,+79991112233",
					};
					break;
			}
			return commands.ConvertAll(x => (CommandStep)new SmsCommandStep(x));
		}
		private List<CommandStep> GetDeleteSOSSteps(IStdCommand command)
		{
			var apnCfg = GetInternetApnConfig(command);
			var srvAdd = Manager.GetConstant(Constant.ServerIP);
			var srvPrt = Manager.GetConstant(Constant.ServerPort);
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.MiniFinderAttoVG30:
					break;
				case DeviceNames.MiniFinderPicoVG10:
				case DeviceNames.MiniFinderPicoVG20:
					commands = new List<string>
					{
						$@"a0",
					};
					break;
			}
			return commands.ConvertAll(x => (CommandStep)new SmsCommandStep(x));
		}
		private List<CommandStep> GetAskPositionSteps(IStdCommand command)
		{
			var apnCfg = GetInternetApnConfig(command);
			var srvAdd = Manager.GetConstant(Constant.ServerIP);
			var srvPrt = Manager.GetConstant(Constant.ServerPort);
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.MiniFinderAttoVG30:
				case DeviceNames.MiniFinderPicoVG10:
				case DeviceNames.MiniFinderPicoVG20:
					commands = new List<string>
					{
						$@"loc",
					};
					break;
			}
			return commands.ConvertAll(x => (CommandStep)new SmsCommandStep(x));
		}
		private List<CommandStep> GetSetModeOnlineSteps(IStdCommand command)
		{
			var apnCfg = GetInternetApnConfig(command);
			var srvAdd = Manager.GetConstant(Constant.ServerIP);
			var srvPrt = Manager.GetConstant(Constant.ServerPort);
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.MiniFinderAttoVG30:
				case DeviceNames.MiniFinderPicoVG10:
				case DeviceNames.MiniFinderPicoVG20:
					commands = new List<string>
					{
						$@"ps0",
					};
					break;
			}
			return commands.ConvertAll(x => (CommandStep)new SmsCommandStep(x));
		}
		private List<CommandStep> GetSetModeWaitingSteps(IStdCommand command)
		{
			var apnCfg = GetInternetApnConfig(command);
			var srvAdd = Manager.GetConstant(Constant.ServerIP);
			var srvPrt = Manager.GetConstant(Constant.ServerPort);
			var commands = new List<string>();
			switch (command.Target.DeviceType)
			{
				case DeviceNames.MiniFinderAttoVG30:
				case DeviceNames.MiniFinderPicoVG10:
				case DeviceNames.MiniFinderPicoVG20:
					commands = new List<string>
					{
						$@"ps1",
					};
					break;
			}
			return commands.ConvertAll(x => (CommandStep)new SmsCommandStep(x));
		}
	}
}