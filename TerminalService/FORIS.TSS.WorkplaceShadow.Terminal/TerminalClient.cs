﻿using System;
using System.Collections.Specialized;
using FORIS.TSS.BusinessLogic.Terminal;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using FORIS.TSS.WorkplaceSharnier;

namespace FORIS.TSS.WorkplaceShadow.Terminal
{
	public class TerminalClient :
		ClientBase<ITerminalPersonalServer>,
		IMobileUnitSupplier
	{
		#region Constructor & Dispose

		public TerminalClient(NameValueCollection nameValueCollection)
			: base(nameValueCollection)
		{

		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				receive = null;
			}

			base.Dispose(disposing);
		}

		#endregion Constructor & Dispose

		#region Start & Stop

		protected override void Start()
		{
			Session.DataReadyEvent += Session_DataReadyEvent;
			Session.NotifyEvent    += Session_NotifyEvent;

			Session_DataReadyEvent(null);
		}
		protected override void Stop()
		{
			if (Session == null)
				return;
			Session.DataReadyEvent -= Session_DataReadyEvent;
			Session.NotifyEvent    -= Session_NotifyEvent;
		}

		#endregion Start & Stop

		#region Handle Session events

		private void Session_DataReadyEvent(EventArgs e)
		{
			lock (syncRoot)
			{
				if (State == ClientState.Connected)
				{
					IMobilUnit[] data = Session.GetPositions();

					OnReceive(new ReceiveEventArgs(data));
				}
			}
		}
		private void Session_NotifyEvent(BulkNotifyEventArgs args)
		{
			OnNotify(args);
		}

		#endregion Handle Session events

		#region IMobileUnitSupplier Members

		private      ReceiveEventHandler receive;
		public event ReceiveEventHandler Receive
		{
			add    { receive -= value; receive += value; }
			remove { receive -= value; }
		}
		protected virtual void OnReceive(ReceiveEventArgs e)
		{
			if (receive == null)
				return;
			Delegate[] handlers = receive.GetInvocationList();

			foreach (ReceiveEventHandler handler in handlers)
			{
				handler.Invoke(e);
			}
		}
		private      BulkNotifyEventHandler notify;
		public event BulkNotifyEventHandler Notify
		{
			add    { notify -= value; notify += value; }
			remove { notify -= value; }
		}
		protected virtual void OnNotify(BulkNotifyEventArgs e)
		{
			if (notify == null)
				return;
			var handlers = notify.GetInvocationList();
			foreach (BulkNotifyEventHandler handler in handlers)
				handler.BeginInvoke(e, notify_Callback, handler);
		}
		private void notify_Callback(IAsyncResult asyncResult)
		{
			var handler = (BulkNotifyEventHandler)asyncResult.AsyncState;
			try
			{
				handler.EndInvoke(asyncResult);
			}
			catch (Exception)
			{
				Notify -= handler;
			}
		}
		public void SendCommand(IStdCommand cmd)
		{
			Session.SendCommand(cmd);
		}
		#endregion IMobileUnitSupplier Members

		protected override void ReConnect()
		{
			throw new NotImplementedException();
		}
	}
}