﻿using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FORIS.TSS.Helpers;

namespace RU.NVG.NIKA.Terminal.DIMTS
{
    public class DIMTSDevice : Device
    {
        private const byte Punct = 44; // ASCII

        public DIMTSDevice() : base(typeof(DIMTSSensor), new [] { "DIMTS" })
        {
        }

        public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
        {
            bufferRest = null;
            byte[] messageData = data;
            if (count != data.Length)
            {
                messageData = new byte[count];
                Array.Copy(data, messageData, count);
            }
            var messageReader = new DIMTSDataReader(messageData);

            var units = new ArrayList(5);
            while (messageReader.Next())
            {
                var unit = messageReader.Read();
                units.Add(unit);
            }

            return units.Count > 0 ? units : null;
        }

        public override bool SupportData(byte[] data)
        {
            var result = false;
            if (data[0] == '$')
            {
                result = true;
                var packetCount = data.Count(item => item == 10) + 1;
                var startIndex = 0;
                for (var i = 0; i != packetCount && startIndex != data.Length; i++)
                {
                    var endIndex = Array.IndexOf(data, (byte)10, startIndex);
                    var fieldsLength = data.Count(Punct, startIndex, endIndex) + 1;
                    if (fieldsLength >= DIMTSDataReader.FieldCountToPrimaryServer)
                    {
                        if (!DIMTSDataReader.ByteHeaders.Any(header => data.StartsWith(header, startIndex)))
                        {
                            result = false;
                            break;
                        }
                    }
                    else
                    {
                        result = false;
                        break;                        
                    }


                    startIndex = endIndex + 1;
                }

            }

            return result;
        }
    }
}
