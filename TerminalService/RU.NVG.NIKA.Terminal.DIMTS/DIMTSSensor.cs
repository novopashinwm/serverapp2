﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RU.NVG.NIKA.Terminal.DIMTS
{
    public enum DIMTSSensor
    {
        DigitalInput = 1,

        Ignition = 100,

        SimTamper = 201,
        BoxTamper = 202,
        BatteryTamper = 203,

        BatteryLevel = 300
    }
}
