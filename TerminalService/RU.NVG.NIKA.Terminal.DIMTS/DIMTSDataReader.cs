﻿using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FORIS.TSS.BusinessLogic.DTO;

namespace RU.NVG.NIKA.Terminal.DIMTS
{
    internal class DIMTSDataReader
    {
        internal const int FieldCountToPrimaryServer = 18;

        internal const int FieldCountToSecondaryServer = 21;

        internal const int FieldCountAdded = 2;

        internal static readonly IEnumerable<string> Headers = new List<string> {
            "$loc","$bak","$rmv","$btl","$tmp","$smt","$btt"
        };

        internal static readonly IEnumerable<byte[]> ByteHeaders = Headers.Select(item => Encoding.ASCII.GetBytes(item));

        private SplittedStringReader _reader;

        private readonly List<byte>[] _messages;

        private readonly int _messagesCount;

        private int _index;

        private int _readedIndex;

        public DIMTSDataReader(IEnumerable<byte> message)
        {
            _messages = message.GetSequences(item => item != (byte)10).ToArray();
            _messagesCount = _messages.Count();
            _index = 0;
            _readedIndex = -1;
        }

        public bool Next()
        {
            var hasMoreMessages = _messagesCount > _index;
            if (hasMoreMessages)
            {
                var message = Encoding.ASCII.GetString(_messages[_index].ToArray());
                _reader = new SplittedStringReader(message, ',');
                _index++;
            }

            return hasMoreMessages;
        }

        public IMobilUnit Read()
        {
            if (_readedIndex == _index)
            {
                return null;
            }

            _readedIndex = _index;
            _reader.Skip(1);
            var imei = _reader.ReadString();
            var dateString = _reader.ReadString();
            var timeString = _reader.ReadString();
            var correctGps = _reader.ReadBoolean();
            var latitude = ReadLatitude();
            var longitude = ReadLongitude();
            var speed = _reader.ReadDouble();
            var heading = _reader.ReadDouble();
            var cellId = _reader.ReadNullableInt();
            var signalStrength = _reader.ReadNullableInt();
            var gsmNumber = _reader.ReadInt();
            var batteryLevel = _reader.ReadInt();
            var ignition = _reader.ReadBoolean();
            var digitalInput = _reader.ReadString();
            var tamperStatus = _reader.ReadString();
            string softwareVersion = null, geoAddress = null;
            var odometer = (int?)null;
            if (_reader.Length == FieldCountToSecondaryServer || _reader.Length == FieldCountToSecondaryServer + FieldCountAdded)
            {
                if (_reader.Length == FieldCountToSecondaryServer + FieldCountAdded)
                {
                    _reader.Skip(2);
                }

                softwareVersion = _reader.ReadString();
                odometer = _reader.ReadInt();
                geoAddress = _reader.ReadString();//TODO: Not used;
            }

            int logTime;
            if (!TryParseDateTime(dateString, timeString, out logTime))
            {
                return null;
            }

            var mobilUnit = new MobilUnit
            {
                DeviceID = imei
            };
            mobilUnit.Properties.Add(DeviceProperty.IMEI, imei);
            mobilUnit.Time = logTime;
            mobilUnit.CorrectGPS = correctGps;
            mobilUnit.Latitude = latitude.HasValue ? latitude.Value : MobilUnit.InvalidLatitude;;
            mobilUnit.Longitude = longitude.HasValue ? longitude.Value : MobilUnit.InvalidLongitude;
            mobilUnit.Speed = speed.HasValue ? (int)Math.Round(speed.Value) : default(int);
            mobilUnit.Course = heading.HasValue ? (int?)Math.Round(heading.Value) : null;

            mobilUnit.Properties.Add(DeviceProperty.Protocol, ControllerType.Names.DIMTS);
            if (cellId.HasValue || signalStrength.HasValue)
            {
                mobilUnit.CellNetworks = new[] {
                    new CellNetworkRecord 
                    {
                        CountryCode = string.Empty,
                        NetworkCode = string.Empty,
                        CellID = cellId.HasValue ? cellId.Value : default(int),
                        SignalStrength = signalStrength.HasValue ? signalStrength.Value : default(int),
                        Number = gsmNumber
                    }
                };
            }

            if (softwareVersion != null && odometer.HasValue && geoAddress != null)
            {
                mobilUnit.FirmwareVersion = softwareVersion;
                mobilUnit.Run = odometer.HasValue ? odometer.Value : default(int);
            }

            if (!string.IsNullOrEmpty(digitalInput))
            {
                for (var i = 0; i < digitalInput.Length; i++)
                {
                    var ch = digitalInput[i];
                    mobilUnit.SetSensorValue(((int)DIMTSSensor.DigitalInput) + i, ch == 'H' ? 1 : 0);
                }
            }

            if (!string.IsNullOrEmpty(tamperStatus))
            {
                mobilUnit.SetSensorValue((int)DIMTSSensor.SimTamper, tamperStatus[0] == 'N' ? 0 : 1);
                mobilUnit.SetSensorValue((int)DIMTSSensor.BoxTamper, tamperStatus[1] == 'N' ? 0 : 1);
                mobilUnit.SetSensorValue((int)DIMTSSensor.BatteryTamper, tamperStatus[2] == 'N' ? 0 : 1);
            }

            mobilUnit.SetSensorValue((int)DIMTSSensor.Ignition, ignition ? 1 : 0);
            mobilUnit.SetSensorValue((int)DIMTSSensor.BatteryLevel, batteryLevel);

            return mobilUnit;
        }

        public double? ReadLatitude()
        {
            return ReadCoordinate("S");
        }

        public double? ReadLongitude()
        {
            return ReadCoordinate("W");
        }

        private double? ReadCoordinate(string negativeHemisphereSign)
        {
            var coordinateString = _reader.ReadString();
            var hemisphere = _reader.ReadString();

            decimal lat;
            bool coordinateIsValid = decimal.TryParse(coordinateString, NumberStyles.AllowDecimalPoint, NumberHelper.FormatInfo, out lat);
            if (!coordinateIsValid)
                return null;

            lat = NmeaToDegrees(lat);

            if (hemisphere == negativeHemisphereSign)
                lat = -lat;

            return (double?)lat;
        }

        private static decimal NmeaToDegrees(decimal nmea)
        {
            var degrees = Math.Floor(nmea / 100m);
            var minutes = (nmea - degrees * 100m) / 60m;
            return degrees + minutes;
        }

        public static bool TryParseDateTime(string date, string time, out int logTime)
        {
            DateTime dateTime;
            if (!DateTime.TryParseExact(date + time, "ddMMyyyyHHmmss", CultureInfo.InvariantCulture,
                                        DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal, out dateTime))
            {
                logTime = 0;
                return false;
            }

            logTime = TimeHelper.GetSecondsFromBase(dateTime);
            return true;
        }
    }
}
