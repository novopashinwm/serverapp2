﻿using System;
using System.Collections.Generic;

namespace Compass.Ufin.Terminal.Devices.Wialon
{
	[Serializable]
	public class WialonPhoto
	{
		public string       Name;
		public int          LogTime;
		public int          ExpectedPartCount { get { return Parts.Capacity; } }
		public List<byte[]> Parts;
	}
}