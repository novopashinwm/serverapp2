﻿using System.Collections.Generic;
using FORIS.TSS.Terminal;

namespace Compass.Ufin.Terminal.Devices.Wialon
{
	public class WialonReceiverState : ReceiverState
	{
		/// <summary> Версия протокола по умолчанию, все версии до 2.0, не передают версию </summary>
		public const string DefaultVersion = "1.x";
		/// <summary> Версия протокола, по умолчанию присваивается константная, т.к. в протоколах до 2.0 версия не передается </summary>
		public string             Version  = DefaultVersion;
		public string             Password;
		/// <summary> Набор фотографий в памяти для конкретного подключения, можно использовать для разряжения отправки запросов на устройство </summary>
		public List<WialonPhoto>  Photos;
	}
}