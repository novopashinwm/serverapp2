﻿namespace Compass.Ufin.Terminal.Devices.Wialon.Ips
{
	public enum WialonIpsSensor
	{
		/// <summary> Цифровой вход </summary>
		DI    = 0,
		/// <summary> Аналоговый вход </summary>
		AI    = 100,
		/// <summary> Входящий пассажиропоток </summary>
		PPIN  = 200,
		/// <summary> Исходящий пассажиропоток </summary>
		PPOUT = 300,
		/// <summary>Уровень топлива</summary>
		FL    = 400,
		/// <summary>Температура топлива</summary>
		FT    = 410,
		/// <summary> Датчик температуры </summary>
		T     = 421,
		/// <summary> HDOP </summary>
		HDOP  = 430,
		/// <summary> Данные с шины CAN, 500..599 </summary>
		CAN   = 500,
		CAN10 = 511,
		CAN11 = 512,
		CAN12 = 513,
		CAN13 = 514,

		CAN20 = 521,
		CAN21 = 522,
		CAN22 = 523,
		CAN23 = 524,

		CAN30 = 531,
		CAN31 = 532,
		CAN32 = 533,
		CAN33 = 534,

		CAN40 = 541,
		CAN41 = 542,
		CAN42 = 543,
		CAN43 = 544,

		CAN50 = 551,
		CAN51 = 552,
		CAN52 = 553,
		CAN53 = 554,

		CAN60 = 561,
		CAN61 = 562,
		CAN62 = 563,
		CAN63 = 564,

		CAN70 = 571,
		CAN71 = 572,
		CAN72 = 573,
		CAN73 = 574,
	}
}