﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.Helpers;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace Compass.Ufin.Terminal.Devices.Wialon.Ips
{
	public class WialonIpsDevice : Device
	{
		internal const string ProtocolName = "Wialon IPS";
		internal static class DeviceNames
		{
			internal const string Wialon = "Wialon";
		}
		public WialonIpsDevice()
			: base(typeof(WialonIpsSensor))
		{
			SupportsShortNumber          = false;
		}
		public override bool SupportData(byte[] data)
		{
			// Длина пакета больше или равна 5
			if (data.Length < 5)
				return false;
			// Пакет начинается с символа '#'
			if (data[0] != '#')
				return false;
			// Пакет заканчивается последовательностью "\r\n" (0x0D, 0x0A)
			if (data[data.Length - 2] != '\r' ||
				data[data.Length - 1] != '\n')
				return false;
			// В наличии имеется второй символ '#'
			var secondSharp = Array.IndexOf(data, (byte) '#', 1);
			if (secondSharp == -1)
				return false;
			// Принимаем все пакеты указанного формата
			return WialonIpsRecord.IsMatch(data);
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			var results = new List<object>();
			var records = WialonIpsRecord.GetRecords(data, out bufferRest);
			var rvstate = stateData as WialonReceiverState;
			//////////////////////////////////////////////////////////
			foreach (var record in records)
			{
				switch (record.RecType)
				{
					case "L":
						results.AddRange(ParseLoginMessage(record));
						break;
					case "B":
						results.AddRange(ParseBufferMessage(rvstate, record.RecBody));
						break;
					case "D":
						results.AddRange(ParseFullDataPacketMessage(rvstate, record.RecBody));
						break;
					case "SD":
						results.AddRange(ParseShortDataPacketMessage(rvstate, record.RecBody));
						break;
					case "P":
						results.AddRange(ParsePingPacketMessage(record.RecBody));
						break;
					case "I":
						results.AddRange(ParsePhotoMessage(record.RecBody, rvstate, data, count, ref bufferRest));
						break;
					default:
						Trace.TraceWarning("{0}: message type {1} is not supported: {2}, buffer rest: {3}",
							this, record.RecType, record.RecBody, bufferRest != null ? BitConverter.ToString(bufferRest) : null);
						break;
				}
			}
			//////////////////////////////////////////////////////////
			if (results == null)
				return null;
			//////////////////////////////////////////////////////////
			if (rvstate?.DeviceID != null)
			{
				foreach (var result in results)
				{
					var mu = result as IMobilUnit;
					if (mu == null)
						continue;
					mu.DeviceID = rvstate.DeviceID;
				}
			}
			//////////////////////////////////////////////////////////
			return results;
			//////////////////////////////////////////////////////////
		}

		private readonly Regex _integerAdditionalParamRegex = new Regex(@"(?<name>[a-zA-Z]+)(?<number>[0-9]+)\:1:(?<value>[0-9]+)");
		private readonly Regex _integerParamRegex           = new Regex(@"(?<name>[a-zA-Z]+)\:1:(?<value>[0-9]+)");
		private readonly Regex _canBusRegex                 = new Regex(@"CAN(?<number>[0-9]+)\:3\:(?<value>.+)");

		/// <summary> Метод разбирает пакет логина </summary>
		/// <param name="record"> Класс пакета </param>
		/// <returns></returns>
		/// <remarks>
		/// 1.x - #L#IMEI;Password\r\n
		/// 2.x - #L#Protocol_version;IMEI;Password;CRC16\r\n
		/// IMEI     - уникальный ID контроллера, IMEI или серийный номер
		/// Password - пароль для доступа к устройству, если отсутствует, то передаётся "NA"
		/// </remarks>
		private IEnumerable<object> ParseLoginMessage(WialonIpsRecord record)
		{
			var results = new List<object>();
			if (record == null)
				return results;

			var receiverState = new WialonReceiverState();
			var serverConfirm = default(ConfirmPacket);
			var recVals = record.RecBody?.Split(';') ?? new string[0];
			if (2 == recVals.Length) // Версия протокола 1.x
			{
				receiverState.DeviceID = recVals[0];
				receiverState.Password = recVals[1];
			}
			else if (4 == recVals.Length) // Версия протокола >= 2.0
			{

				var dataCrc = recVals[3].All(c => c.IsHexDigit())
					? WialonIpsRecord.WialonIpsEncoding
						?.GetString(recVals[3].HexStringToByteArray())
						?.HexStringToByteArray()
						?.FromBE2UInt16(0, sizeof(ushort))
					: default;
				if (dataCrc.HasValue)
				{
					var calcCrc = WialonIpsRecord.GetCrc(
						record.RecBody.Substring(0, record.RecBody.LastIndexOf(recVals[3])));
					if (calcCrc.HasValue)
					{
						if (dataCrc.Value == calcCrc.Value)
						{
							receiverState.Version  = recVals[0];
							receiverState.DeviceID = recVals[1];
							receiverState.Password = recVals[2];
						}
						else
							serverConfirm = Answer("#AL#10"); // Ошибка проверки контрольной суммы.
					}
					else
						serverConfirm = Answer("#AL#0"); // Нарушена структура сообщения.
				}
				else
					serverConfirm = Answer("#AL#0"); // Нарушена структура сообщения.
			}
			else
				serverConfirm = Answer("#AL#0"); // Нарушена структура сообщения.
			// Если ответ еще не сформирован, то все хорошо с контрольной суммой и структурой, поэтому проверяем пароль
			if (serverConfirm == default(ConfirmPacket))
			{
				// Проверка пароля производится для любой версии протокола
				if (CheckDevicePassword(receiverState))
				{
					// Добавляем в ответ, только если пароль правильный
					results.Add(new ReceiverStoreToStateMessage { StateData = receiverState });
					serverConfirm = Answer("#AL#1"); // Авторизация объекта на сервере прошла успешно.
				}
				else
					serverConfirm = Answer("#AL#01"); // Ошибка проверки пароля.
			}
			////////////////////////////////////////////////////////////////////////////////////
			results.Add(serverConfirm);
			////////////////////////////////////////////////////////////////////////////////////
			return results;
		}
		private bool CheckDevicePassword(WialonReceiverState state)
		{
			// Пока проверки нет
			return true;
		}
		private IEnumerable<object> ParsePhotoMessage(string message, WialonReceiverState state, byte[] data, int count, ref byte[] bufferRest)
		{
			var reader = new SplittedStringReader(message, ';');

			var binaryBytesCount = reader.ReadInt();

			if (100*1024 < binaryBytesCount)
				return null; //Разорвать соединение

			var blockIndex = reader.ReadInt();
			if (blockIndex < 0)
				return null;

			//TODO: разобраться, в документации странность "count номер последнего блока при нумерации с нуля", 
			//TODO: т.е. по-английски написано одно, а по-русски - другое
			var blockCount = reader.ReadInt() + 1;

			//Serge Maslyakov: Нужно отвязать валидность фотки от ее имени.
			//Т.к. там может быть невалидная дата
			reader.Skip(2); //date and time
			
			var name = reader.ReadString();
			
			if (blockCount < 0)
				return null;
			if (1024 * 1024 < blockCount * binaryBytesCount)
				return null; //Разорвать соединение
			if (blockCount < blockIndex)
				return null;

			if (bufferRest.Length < binaryBytesCount)
			{
				bufferRest = new byte[count];
				Array.Copy(data, 0, bufferRest, 0, count);
				return new List<object>();
			}

			if (state.Photos == null)
				state.Photos = new List<WialonPhoto>(1);

			var photo = state.Photos.FirstOrDefault(p => p.Name == name);
			if (photo == null)
			{
				photo = new WialonPhoto
				{
					LogTime = TimeHelper.GetSecondsFromBase(),
					Name    = name,
					Parts   = new List<byte[]>(blockCount)
				};
				state.Photos.Add(photo);
			}
			if (photo.Parts.Count != blockIndex)
				return null;

			var partBytes = new byte[binaryBytesCount];
			Array.Copy(bufferRest, partBytes, binaryBytesCount);
			photo.Parts.Add(partBytes);

			if (binaryBytesCount == bufferRest.Length)
			{
				bufferRest = null;
			}
			else
			{
				var newBufferRest = new byte[bufferRest.Length - binaryBytesCount];
				Array.Copy(bufferRest, binaryBytesCount, newBufferRest, 0, newBufferRest.Length);
				bufferRest = newBufferRest;
			}

			var results = new List<object>(3) { Answer("#AI#" + blockIndex + ";1") };

			if (photo.Parts.Count != photo.ExpectedPartCount)
				return results;

			var pictureUnit = new PictureUnit();
			pictureUnit.Time       = photo.LogTime;
			pictureUnit.DeviceID   = state.DeviceID;
			pictureUnit.Bytes      = photo.Parts.SelectMany(p => p).ToArray();
			pictureUnit.Url        = photo.Name;
			pictureUnit.CorrectGPS = false;
			results.Add(pictureUnit);

			state.Photos.Remove(photo);

			results.Add(Answer("#AI#;1"));

			return results;
		}
		private IEnumerable<object> ParsePingPacketMessage(string message)
		{
			if (message != string.Empty)
				return null; //TODO: disconnect receiver

			return new List<object> { Answer("#AP#") };
		}
		private IEnumerable<object> ParseFullDataPacketMessage(WialonReceiverState state, string message)
		{
			var mu = ParseFullDataPacket(state, message);
			return mu != null
				? new List<object>
				{
					mu,
					Answer("#AD#1")
				}
				: new List<object>
				{
					//TODO: определять структуру пакета
					Answer("#AD#-1")
				};
		}
		private MobilUnit ParseFullDataPacket(WialonReceiverState state, string dataPacket)
		{
			var reader = new WialonDataPacketReader(dataPacket);
			var mu = ReadCoordinates(state, reader);
			if (mu == null)
				return null;
			if (mu.SensorValues == null)
				mu.SensorValues = new Dictionary<int, long>();

			var digitalInputString = reader.ReadString();
			byte digitalInput;
			if (byte.TryParse(digitalInputString, out digitalInput))
			{
				var bitArray = new BitArray(BitConverter.GetBytes(digitalInput));

				for (var i = 0; i != bitArray.Length; ++i)
				{
					mu.SensorValues.Add(((int)WialonIpsSensor.DI) + i, bitArray[i] ? 1 : 0);
				}
			}

			reader.Skip(1); //цифровые выходы

			var adcString = reader.ReadString(); //аналоговые входы
			var adcInputStrings = adcString.Split(',');
			for (var i = 0; i != adcInputStrings.Length; ++i)
			{
				var adcInputString = adcInputStrings[i];
				decimal value;
				if (!decimal.TryParse(adcInputString, NumberStyles.AllowDecimalPoint, NumberHelper.FormatInfo, out value))
					continue;

				mu.SensorValues.Add(((int)WialonIpsSensor.AI) + i + 1, (long)(value * 1000) /*mV*/);
			}

			reader.Skip(1); //ibutton

			var paramsString = reader.ReadString();
			var @params = paramsString.Split(',');
			foreach (var param in @params)
			{
				if (_integerAdditionalParamRegex.IsMatch(param))
				{
					var m = _integerAdditionalParamRegex.Match(param);
					var nameString   = m.Groups["name"].Value;
					var numberString = m.Groups["number"].Value;
					var valueString  = m.Groups["value"].Value;

					WialonIpsSensor sensor;
					if (!Enum.TryParse(nameString, out sensor))
						continue;
					int number;
					if (!int.TryParse(numberString, out number))
						continue;
					long value;
					if (!long.TryParse(valueString, out value))
						continue;

					if ((sensor == WialonIpsSensor.PPIN || sensor == WialonIpsSensor.PPOUT) && value == 0)
						continue;

					mu.SensorValues.Add(((int)sensor) + number, value);
					continue; //anything else is not supported yet
				}

				if (_canBusRegex.IsMatch(param))
				{
					var m = _canBusRegex.Match(param);
					var numberString = m.Groups["number"].Value;
					var valueString = m.Groups["value"].Value;
					int number;
					if (!int.TryParse(numberString, out number))
						continue;
					long value;
					if (!long.TryParse(valueString, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out value))
						continue;

					for (var i = 0; i != 4; ++i)
					{
						var partValue = (value >> (i * 8)) & 0xff;

						mu.SensorValues.Add((int)(WialonIpsSensor.CAN) + (number * 10 + i + 1), partValue);
					}

					continue;
				}

				if (_integerParamRegex.IsMatch(param))
				{
					var m = _integerParamRegex.Match(param);
					var nameString = m.Groups["name"].Value;
					var valueString = m.Groups["value"].Value;
					WialonIpsSensor sensor;
					if (!Enum.TryParse(nameString, out sensor))
						continue;
					long value;
					if (!long.TryParse(valueString, out value))
						continue;
					mu.SensorValues.Add(((int)sensor), value);
				}
			}

			return mu;
		}
		private IEnumerable<object> ParseShortDataPacketMessage(WialonReceiverState state, string message)
		{
			var mu = ParseShortDataPacket(state, message);
			if (mu == null)
				return new List<object> { Answer("#ASD#-1") };

			return new List<object>
			{
				mu,
				Answer("#ASD#1")
			};
		}
		private MobilUnit ParseShortDataPacket(WialonReceiverState state, string dataPacket)
		{
			var reader = new WialonDataPacketReader(dataPacket);
			return ReadCoordinates(state, reader);
		}
		private ConfirmPacket Answer(string message)
		{
			return new ConfirmPacket { Data = Encoding.ASCII.GetBytes(message + WialonIpsRecord.RecordEndSign) };
		}
		private IEnumerable<object> ParseBufferMessage(WialonReceiverState state, string message)
		{
			var parts = message.Split('|');

			var result = new List<object>();
			foreach (var part in parts)
			{
				var mu = ParseDataPacket(state, part);
				if (mu != null)
					result.Add(mu);
			}

			result.Add(Answer("#AB#" + result.Count));

			return result;
		}
		private MobilUnit ParseDataPacket(WialonReceiverState state, string s)
		{
			if (string.IsNullOrWhiteSpace(s))
				return null;

			var partsCount = s.Count(c => c == ';') + 1;

			if (partsCount == 10)
				return ParseShortDataPacket(state, s);

			if (partsCount == 16)
				return ParseFullDataPacket(state, s);

			Trace.TraceInformation("{0}: parts count is {1} for packet {2}", this, partsCount, s);
			return null;
		}
		private MobilUnit ReadCoordinates(WialonReceiverState state, WialonDataPacketReader reader)
		{
			var logTime = reader.ReadDateTime();
			if (logTime == null)
				return null;
			var lat        = reader.ReadLatitude();
			var lng        = reader.ReadLongitude();
			var speed      = reader.ReadSpeed();
			var course     = reader.ReadCourse();
			var height     = reader.HasMoreData() ? reader.ReadHeight()     : null;
			var satellites = reader.HasMoreData() ? reader.ReadSatellites() : null;
			var hdop       = reader.HasMoreData() ? reader.ReadHdop()       : null;

			var mu = UnitReceived($"{ProtocolName} {state?.Version}".Trim() );
			mu.Time      = logTime.Value;
			mu.Latitude  = lat != null ? (double)lat.Value : MobilUnit.InvalidLatitude;
			mu.Longitude = lng != null ? (double)lng.Value : MobilUnit.InvalidLongitude;

			mu.Speed      = speed;
			mu.Course     = course;
			mu.Height     = height;
			mu.Satellites = satellites ?? 0;
			mu.CorrectGPS = lat != null && lng != null && MobilUnit.MinSatellites <= mu.Satellites && (lat.Value != 0 || lng.Value != 0);

			if (hdop != null)
			{
				if (mu.SensorValues == null)
					mu.SensorValues = new Dictionary<int, long>();
				mu.SensorValues.Add((int)WialonIpsSensor.HDOP, (long)(hdop.Value * 100));
			}

			return mu;
		}
		public override List<CommandStep> GetCommandSteps(IStdCommand command)
		{
			switch (command.Target.DeviceType)
			{
				case DeviceNames.Wialon:
					var replyTo = command.GetParamValue(PARAMS.Keys.ReplyToPhone);
					switch (command.Type)
					{
						case CmdType.ReloadDevice:
							return new List<CommandStep>
							{
								new SmsCommandStep("&reset")
								{
									/*Рестартует "молча"*/ 
									WaitAnswer = null,
									ReplyTo = replyTo
								}
							};
						case CmdType.Setup:
							var internetApnConfig = GetInternetApnConfig(command);
							return new List<CommandStep>
							{
								new SmsCommandStep("&number?")
								{
									WaitAnswer = CmdType.Setup,
									ReplyTo = replyTo
								},
								new SmsCommandStep(string.Format(
									@"&ipapn={0},{1},{2}",
									internetApnConfig.Name,
									internetApnConfig.User,
									internetApnConfig.Pass))
								{
									ReplyTo = replyTo
								},
								new SmsCommandStep(string.Format(
									@"&ipaddr={0}",
									Manager.GetConstant(Constant.ServerIP)))
								{
									ReplyTo = replyTo
								},
								new SmsCommandStep(string.Format(
									@"&ipport={0}",
									Manager.GetConstant(Constant.ServerPort)))
								{
									ReplyTo = replyTo
								},
								new SmsCommandStep(@"&foto=00F0,09600")
								{
									ReplyTo = replyTo
								},
								new SmsCommandStep(@"&reset")
								{
									ReplyTo = replyTo,
									WaitAnswer = null
								}
							};
					}
					break;
			}
			return base.GetCommandSteps(command);
		}

		private readonly Regex _tsGlonassOkRegex   = new Regex(@"ok\s+\.\s+");
		private readonly Regex _tsGlonassImeiRegex = new Regex(@"(?<imei>\d+)\s+\.\s+");

		public override IList<object> ParseSms(IUnitInfo ui, string text)
		{
			var result = new List<object>();
			switch (ui.DeviceType)
			{
				case DeviceNames.Wialon:
					if (_tsGlonassOkRegex.IsMatch(text))
					{
						result.Add(CommandCompleted(ui, CmdResult.Completed, CmdType.Unspecified));
						return result;
					}

					if (_tsGlonassImeiRegex.IsMatch(text))
					{
						var match = _tsGlonassImeiRegex.Match(text);
						var deviceId = match.Groups["imei"].Value;
						result.Add(DeviceIdReceived(ui, deviceId));
						result.Add(CommandCompleted(ui, CmdResult.Completed, CmdType.Setup));
						return result;
					}

					break;
			}
			return null;
		}
	}
}