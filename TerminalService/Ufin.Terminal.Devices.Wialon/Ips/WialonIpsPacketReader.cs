﻿using System;
using System.Globalization;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common;
using FORIS.TSS.Terminal;

namespace Compass.Ufin.Terminal.Devices.Wialon.Ips
{
	class WialonDataPacketReader
	{
		private readonly SplittedStringReader _reader;
		public WialonDataPacketReader(string s)
		{
			_reader = new SplittedStringReader(s, ';');
		}
		public decimal? ReadLatitude()
		{
			return ReadCoordinate("S");
		}
		public decimal? ReadLongitude()
		{
			return ReadCoordinate("W");
		}
		private decimal? ReadCoordinate(string negativeHemisphereSign)
		{
			var coordinateString = _reader.ReadString();
			var hemisphere = _reader.ReadString();

			decimal lat;
			bool coordinateIsValid = decimal.TryParse(coordinateString, NumberStyles.AllowDecimalPoint, NumberHelper.FormatInfo, out lat);
			if (!coordinateIsValid)
				return null;

			lat = NmeaToDegrees(lat);

			if (hemisphere == negativeHemisphereSign)
				lat = -lat;

			return lat;
		}
		private static decimal NmeaToDegrees(decimal nmea)
		{
			var degrees = Math.Floor(nmea / 100m);
			var minutes = (nmea - degrees * 100m) / 60m;
			return degrees + minutes;
		}
		public int? ReadDateTime()
		{
			var dateString = _reader.ReadString();
			var timeString = _reader.ReadString();
			if (dateString == "NA" || timeString == "NA")
				return null;

			int logTime;
			if (!TryParseDateTime(dateString, timeString, out logTime))
				return null;

			return logTime;
		}
		public static bool TryParseDateTime(string date, string time, out int logTime)
		{
			DateTime dateTime;
			if (!DateTime.TryParseExact(date + time, "ddMMyyHHmmss", CultureInfo.InvariantCulture,
				DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal, out dateTime))
			{
				logTime = 0;
				return false;
			}

			logTime = TimeHelper.GetSecondsFromBase(dateTime);
			return true;
		}
		public int? ReadSpeed()
		{
			return _reader.ReadNullableInt();
		}
		public int? ReadCourse()
		{
			var result = _reader.ReadNullableInt();
			if (result == null)
				return null;
			if (result < 0 || 359 < result)
				return null;
			return result;
		}
		public int? ReadSatellites()
		{
			return _reader.ReadNullableInt();
		}
		public int? ReadHeight()
		{
			var result = _reader.ReadNullableInt();
			if (result == null)
				return null;
			return result.Value * 100; //м => см
		}
		public void Skip(int count)
		{
			_reader.Skip(count);
		}
		public string ReadString()
		{
			return _reader.ReadString();
		}
		public bool HasMoreData()
		{
			return _reader.HasMoreData();
		}
		public decimal? ReadHdop()
		{
			return _reader.ReadDecimal();
		}
	}
}