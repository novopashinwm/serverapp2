﻿using System;
using System.Collections;

namespace Compass.Ufin.Terminal.Devices.Wialon.Combine
{
	/// <summary> Подзапись состояний цифровых входов и выходов </summary>
	public sealed class WialonCombinePacketDataSubRecord02
	{
		/// <summary> Состояния цифровых входов </summary>
		public BitArray Inputs  { get; private set; }
		/// <summary> Состояния цифровых вЫходов </summary>
		public BitArray Outputs { get; private set; }

		private WialonCombinePacketDataSubRecord02() { }
		public static WialonCombinePacketDataSubRecord02 Parse(WialonCombineDataReader dataReader)
		{
			if ((dataReader?.RemainingBytesCount ?? 0) < 8)
				return default;
			return new WialonCombinePacketDataSubRecord02
			{
				Inputs  = new BitArray(BitConverter.GetBytes(dataReader.ReadBigEndian32())),
				Outputs = new BitArray(BitConverter.GetBytes(dataReader.ReadBigEndian32())),
			};
		}
	}
}