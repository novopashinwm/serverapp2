﻿namespace Compass.Ufin.Terminal.Devices.Wialon.Combine
{
	public enum WialonCombineSensor : ushort
	{
		Ignition     = 1241,
		Sos          = 1251,
		PowerVoltage = 1252,
		CanFuel      = 2803,
	}
}