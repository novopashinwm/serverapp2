﻿using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.Common.Helpers;

namespace Compass.Ufin.Terminal.Devices.Wialon.Combine
{
	/// <summary> Пакет логина в формате Wialon Combine </summary>
	public sealed class WialonCombinePacketData
	{
		/// <summary> Время UTC(такой же как logTime) </summary>
		public uint                                    Time  { get; private set; }
		/// <summary> Количество подзаписей </summary>
		public uint                                    Count { get; private set; }
		/// <summary> Набор данных произвольных полей «Custom Parameters» </summary>
		public WialonCombinePacketDataSubRecordParam[] Rec00 { get; private set; }
		/// <summary> Навигационные данные «Position Data» </summary>
		public WialonCombinePacketDataSubRecord01      Rec01 { get; private set; }
		/// <summary> Значения цифровых входов и выходов «I/O» </summary>
		public WialonCombinePacketDataSubRecord02      Rec02 { get; private set; }
		/// <summary> Тип записи «Picture» </summary>
		/// <remarks> Часть фотоизображения, снятого камерой прибора «Picture» </remarks>
		public WialonCombinePacketDataSubRecord03      Rec03 { get; private set; }
		/// <summary> Информация о параметрах сотовой сети «LBS Parameters» </summary>
		public WialonCombinePacketDataSubRecord04      Rec04 { get; private set; }
		/// <summary> Параметры для передачи значений топлива «Fuel Parameters»</summary>
		public WialonCombinePacketDataSubRecordParam[] Rec05 { get; private set; }
		/// <summary> Данные для передачи значений температуры «Temperature Parameters» </summary>
		public WialonCombinePacketDataSubRecordParam[] Rec06 { get; private set; }
		/// <summary> Данные с CAN-шины «CAN Parameters» </summary>
		public WialonCombinePacketDataSubRecordParam[] Rec07 { get; private set; }
		/// <summary> Данные счетчиков «Counter Parameters» </summary>
		public WialonCombinePacketDataSubRecordParam[] Rec08 { get; private set; }
		/// <summary> Данные аналоговых датчиков «Analog Parameters (ADC)» </summary>
		public WialonCombinePacketDataSubRecordParam[] Rec09 { get; private set; }
		/// <summary> Данные, предназначенные для идентификации водителя «Driver code Parameters» </summary>
		public WialonCombinePacketDataSubRecordParam[] Rec10 { get; private set; }
		/// <summary> Файл, записанный тахографом «Tacho File» </summary>
		public WialonCombinePacketDataSubRecord11      Rec11 { get; private set; }
		public override string ToString()
		{
			return this.ToJson();
		}
		private WialonCombinePacketData() { }
		public static WialonCombinePacketData Parse(WialonCombinePacket packet)
		{
			// Нет данных выходим или не тот пакет
			if (packet?.Type != (ushort?)WialonCombinePacketType.Data)
				return default;
			// Создаем читателя и разбираем
			return Parse(new WialonCombineDataReader(packet.Data));
		}
		private static WialonCombinePacketData Parse(WialonCombineDataReader dataReader)
		{
			if (dataReader == null)
				return default;
			// Проверяем наличие байт для обязательных полей
			if (dataReader.RemainingBytesCount < 5)
				return default;
			// Создаем объект возврата
			var result = new WialonCombinePacketData()
			{
				Time  = dataReader.ReadBigEndian32(),
				Count = dataReader.ReadByte(),
			};
			// Разбираем массив данных
			for(int i = 0; i < result.Count; i++)
			{
				// Читаем тип подзаписи, и если невозможно, то прекращаем цикл
				var type = dataReader.ReadExpandableBigEndian16();
				if (!type.HasValue)
					break;
				switch ((WialonCombinePacketDataSubRecordType)type.Value)
				{
					case WialonCombinePacketDataSubRecordType.CustomParameters:
						result.Rec00 = WialonCombinePacketDataSubRecordParam.ExtractParams(dataReader);
						break;
					case WialonCombinePacketDataSubRecordType.PositionData:
						result.Rec01 = WialonCombinePacketDataSubRecord01.Parse(dataReader);
						break;
					case WialonCombinePacketDataSubRecordType.IoData:
						result.Rec02 = WialonCombinePacketDataSubRecord02.Parse(dataReader);
						break;
					case WialonCombinePacketDataSubRecordType.Picture:
						result.Rec03 = WialonCombinePacketDataSubRecord03.Parse(dataReader);
						break;
					case WialonCombinePacketDataSubRecordType.LbsParameters:
						result.Rec04 = WialonCombinePacketDataSubRecord04.Parse(dataReader);
						break;
					case WialonCombinePacketDataSubRecordType.FuelParameters:
						result.Rec05 = WialonCombinePacketDataSubRecordParam.ExtractParams(dataReader);
						break;
					case WialonCombinePacketDataSubRecordType.TemperatureParameters:
						result.Rec06 = WialonCombinePacketDataSubRecordParam.ExtractParams(dataReader);
						break;
					case WialonCombinePacketDataSubRecordType.CanParameters:
						result.Rec07 = WialonCombinePacketDataSubRecordParam.ExtractParams(dataReader);
						break;
					case WialonCombinePacketDataSubRecordType.CounterParameters:
						result.Rec08 = WialonCombinePacketDataSubRecordParam.ExtractParams(dataReader);
						break;
					case WialonCombinePacketDataSubRecordType.AnalogParameters:
						result.Rec09 = WialonCombinePacketDataSubRecordParam.ExtractParams(dataReader);
						break;
					case WialonCombinePacketDataSubRecordType.DriverСodeParameters:
						result.Rec10 = WialonCombinePacketDataSubRecordParam.ExtractParams(dataReader);
						break;
					case WialonCombinePacketDataSubRecordType.TachoFile:
						result.Rec11 = WialonCombinePacketDataSubRecord11.Parse(dataReader);
						break;
					case WialonCombinePacketDataSubRecordType.DriverMssage:
						dataReader.ReadNullTerminaredString();
						break;
					default:
						break;
				}
			}
			return result;
		}
	}
}