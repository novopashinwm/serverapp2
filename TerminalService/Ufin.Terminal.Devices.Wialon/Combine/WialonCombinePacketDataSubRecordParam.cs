﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Compass.Ufin.Terminal.Devices.Wialon.Combine
{
	public sealed class WialonCombinePacketDataSubRecordParam
	{
		/// <summary> Номер поля </summary>
		public ushort   Number     { get; private set; }
		/// <summary> Тип поля </summary>
		public TypeCode Type       { get; private set; }
		/// <summary> Значение поля </summary>
		public object   Value      { get; private set; }
		/// <summary> Число, на которое нужно разделить «Value». чтобы получить значение датчика </summary>
		public long     Multiplier { get; private set; }
		/// <summary> Получить значение для помещения в SensorValues </summary>
		/// <returns></returns>
		public long? GetSensorValue()
		{
			switch (Type)
			{
				case TypeCode.Byte:
				case TypeCode.UInt16:
				case TypeCode.UInt32:
				case TypeCode.UInt64:
				case TypeCode.SByte:
				case TypeCode.Int16:
				case TypeCode.Int32:
				case TypeCode.Int64:
					return Convert.ToInt64(Value);
				case TypeCode.Single:
				case TypeCode.Double:
					return (long)(Convert.ToDouble(Value) * 1000.0);
			};
			return default;
		}
		private WialonCombinePacketDataSubRecordParam() { }
		public static WialonCombinePacketDataSubRecordParam[] ExtractParams(WialonCombineDataReader dataReader)
		{
			// Для прочтения всего необходимо вызвать ToArray, чтобы итератор (yield) прочитал все
			return ExtractParamsQuery(dataReader).ToArray();
		}
		private static IEnumerable<WialonCombinePacketDataSubRecordParam> ExtractParamsQuery(WialonCombineDataReader dataReader)
		{
			var index = 0;
			if ((dataReader?.RemainingBytesCount ?? 0) < 2)
				yield break;
			var count = dataReader.ReadExpandableBigEndian16();
			if (!count.HasValue)
				yield break;
			if (count.Value == 0)
				yield break;
			while(dataReader.Any() && count.Value > index++)
			{
				if ((dataReader?.RemainingBytesCount ?? 0) < 2)
					yield break;
				var sensNumb  = dataReader.ReadExpandableBigEndian16();
				if (!sensNumb.HasValue)
					yield break;
				if ((dataReader?.RemainingBytesCount ?? 0) < 2)
					yield break;
				var sensDefs  = dataReader.ReadByte();
				var sensType  = (byte)(sensDefs & 0x01F);                 // Тип в младших 5 байтах
				var sensMult  = (byte)(sensType < 8 ? sensDefs >> 5 : 0); // Степень 10 множителя хранится в 3 старших битах для типов ниже 8
				var sensParam = new WialonCombinePacketDataSubRecordParam { Number = sensNumb.Value, Multiplier = (long)Math.Pow(10, sensMult) };
				var sensBytes = default(byte[]);
				switch (sensType)
				{
					case 00: sensParam.Type = System.Type.GetTypeCode(typeof(byte));
						if (dataReader.RemainingBytesCount < 1)
							yield break;
						sensParam.Value = (byte)dataReader.ReadByte();
						break;
					case 01: sensParam.Type = System.Type.GetTypeCode(typeof(ushort));
						if (dataReader.RemainingBytesCount < 2)
							yield break;
						sensParam.Value = (ushort)dataReader.ReadBigEndian16();
						break;
					case 02: sensParam.Type = System.Type.GetTypeCode(typeof(uint));
						if (dataReader.RemainingBytesCount < 4)
							yield break;
						sensParam.Value = (uint)dataReader.ReadBigEndian32();
						break;
					case 03: sensParam.Type = System.Type.GetTypeCode(typeof(ulong));
						if (dataReader.RemainingBytesCount < 8)
							yield break;
						sensParam.Value = (ulong)dataReader.ReadBigEndian64();
						break;
					case 04: sensParam.Type = System.Type.GetTypeCode(typeof(sbyte));
						if (dataReader.RemainingBytesCount < 1)
							yield break;
						sensParam.Value = (sbyte)dataReader.ReadByte();
						break;
					case 05: sensParam.Type = System.Type.GetTypeCode(typeof(short));
						if (dataReader.RemainingBytesCount < 2)
							yield break;
						sensParam.Value = (short)dataReader.ReadBigEndian16();
						break;
					case 06: sensParam.Type = System.Type.GetTypeCode(typeof(int));
						if (dataReader.RemainingBytesCount < 4)
							yield break;
						sensParam.Value = (int)dataReader.ReadBigEndian32();
						break;
					case 07: sensParam.Type = System.Type.GetTypeCode(typeof(long));
						if (dataReader.RemainingBytesCount < 8)
							yield break;
						sensParam.Value = (long)dataReader.ReadBigEndian64();
						break;
					case 08: sensParam.Type = System.Type.GetTypeCode(typeof(float));
						if (dataReader.RemainingBytesCount < 4)
							yield break;
						sensBytes = dataReader.ReadBytes(4);
						if (BitConverter.IsLittleEndian)
							sensBytes = sensBytes.Reverse().ToArray();
						sensParam.Value = BitConverter.ToSingle(sensBytes, 0);
						break;
					case 09: sensParam.Type = System.Type.GetTypeCode(typeof(double));
						if (dataReader.RemainingBytesCount < 8)
							yield break;
						sensBytes = dataReader.ReadBytes(8);
						if (BitConverter.IsLittleEndian)
							sensBytes = sensBytes.Reverse().ToArray();
						sensParam.Value = BitConverter.ToDouble(sensBytes, 0);
						break;
					case 10: sensParam.Type = System.Type.GetTypeCode(typeof(string));
						var val = dataReader.ReadNullTerminaredString();
						if (string.IsNullOrEmpty(val))
							yield break;
						sensParam.Value = val;
						break;
					default:
						yield break;
				}
				yield return sensParam;
			}
		}
	}
}