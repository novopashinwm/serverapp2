﻿using System;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common;
using FORIS.TSS.TerminalService.Interfaces;

namespace Compass.Ufin.Terminal.Devices.Wialon.Combine
{
	/// <summary> Пакет в формате Wialon Combine </summary>
	public sealed class WialonCombinePacket
	{
		private static readonly byte[] PacketPrefixSign = { 0x24, 0x24 };
		private static readonly byte[] PacketAnswerSign = { 0x40, 0x40 };

		private WialonCombinePacket() { }

		/// <summary> Тип пакета</summary>
		public ushort             Type { get; private set; }
		/// <summary> Seq — порядковый номер (циклический 0 — 65535) </summary>
		public ushort             Seq  { get; private set; }
		/// <summary> CRC16 — контрольная сумма </summary>
		/// <remarks>  Рассчитывается от начала заголовка (Head) до последнего байта полезных данных(Data) </remarks>
		public ushort             Crc  { get; private set; }
		/// <summary> Data — полезные данные. Зависит от типа пакета </summary>
		public ArraySegment<byte> Data { get; private set; }
		/// <summary> Pack — полные данные пакета </summary>
		public ArraySegment<byte> Pack { get; private set; }
		/// <summary> Rest — остаток данных во входном массиве данные пакета </summary>
		public ArraySegment<byte> Rest { get; private set; }
		public bool               IsValid => Type != (ushort)WialonCombinePacketType.KeepAlive
			? Crc == Pack.Take(Pack.Count - 2).ToArray().GetCrc()
			: true;
		/// <summary> Проверить, содержит ли входной массив байт записи формата Wialon Combine </summary>
		/// <param name="data"> Пакет данных </param>
		/// <returns></returns>
		public static bool IsMatch(byte[] data)
		{
			// Для ускорения используется итератор (yield), который найдет первую запись и успокоится и не будет читать все.
			return ExtractPacketsQuery(data).Any();
		}
		public static WialonCombinePacket[] ExtractPackets(byte[] data)
		{
			// Для прочтения всего необходимо вызвать ToArray, чтобы итератор (yield) прочитал все
			return ExtractPacketsQuery(data).ToArray();
		}
		private static IEnumerable<WialonCombinePacket> ExtractPacketsQuery(byte[] data)
		{
			// Нет данных выходим
			if (data == null)
				yield break;
			// Создаем читателя
			var packetReader = new WialonCombineDataReader(data);
			// Пытаемся найти все пакеты во входном массиве байт
			while (packetReader.Any())
			{
				// Самый маленький пакет Keep-Alive (5 или 6 байт)
				if (packetReader.RemainingBytesCount < 5)
					yield break;
				// Хватит ли байт для чтения заголовка
				if (packetReader.RemainingBytesCount < PacketPrefixSign.Length)
					yield break;
				// Сохраняем позицию, для дальнейшего формирования индексов WialonCombinePacket
				var packetPackBeg = packetReader.Position;
				// Если это не заголовок, то берем следующие байты
				if (!packetReader.ReadBytes(PacketPrefixSign.Length).SequenceEqual(PacketPrefixSign))
				{
					// Чтобы искать начало сдвигаясь по одному байту
					packetReader.Rewind(PacketPrefixSign.Length - 1);
					continue;
				}
				// Больше нет данных
				if (1 > packetReader.RemainingBytesCount)
					yield break;
				// Определяем тип пакета (поле расширяемое)
				var packetType = packetReader.ReadExpandableBigEndian16();
				if (!packetType.HasValue)
					yield break;
				// Определяем порядковый номер сообщения
				if (packetReader.RemainingBytesCount < 2)
					yield break;
				var packetSeq = packetReader.ReadBigEndian16(2);
				// Создаем объект пакета
				var packet = new WialonCombinePacket()
				{
					Type = packetType.Value,
					Seq  = packetSeq,
					Crc  = default,
					Data = default,
					Pack = new ArraySegment<byte>(data, packetPackBeg, packetReader.BytesRead - packetPackBeg),
					Rest = new ArraySegment<byte>(data, packetReader.Position, packetReader.RemainingBytesCount),
				};
				// Если это поддержка соединения, то данных и контрольной суммы нет
				if (packetType == (ushort)WialonCombinePacketType.KeepAlive)
				{
					yield return packet;
					continue;
				}
				// Определяем длину данных до контрольной суммы (поле расширяемое)
				if (packetReader.RemainingBytesCount < 2)
					yield break;
				var packeDatatLen = packetReader.ReadExpandableBigEndian32();
				if (!packeDatatLen.HasValue)
					yield break;
				// Проверяем наличие байт для полученной длины пакета и контрольной суммы
				if (packetReader.RemainingBytesCount < packeDatatLen + 2)
					yield break;
				// Заполняем поле Data результата
				packet.Data = new ArraySegment<byte>(data, packetReader.Position, (int)packeDatatLen.Value);
				// Пропускаем (читаем) байты поля Data
				packetReader.Skip((int)packeDatatLen.Value);
				// Заполняем поле контрольной суммы
				packet.Crc  = packetReader.ReadBigEndian16(2);
				// Заполняем поле Pack
				packet.Pack = new ArraySegment<byte>(data, packetPackBeg, packetReader.Position - packetPackBeg);
				// Заполняем поле Rest остатков байт из входного массива
				packet.Rest = new ArraySegment<byte>(data, packetReader.Position, packetReader.RemainingBytesCount);
				// Возвращаем результат
				yield return packet;
			}
		}
		public static ConfirmPacket GetConfirmPacket(WialonCombinePacket packet, byte answerCode)
		{
			return new ConfirmPacket(PacketAnswerSign
				.Concat(new byte[] { answerCode })
				.Concat(packet.Seq.ToBE())
				.ToArray());
		}
		public static byte[] GetCommandPacket(byte[] data)
		{
			data = data ?? new byte[0];
			var len = (ushort)(data.Length + 5);
			var cmd = PacketAnswerSign
				.Concat(new byte[] { (byte)WialonCombineAnswerCode.Command })
				.Concat(len.ToBE())
				.Concat(((uint)TimeHelper.GetSecondsFromBase()).ToBE())
				.Concat(new byte[] { (byte)WialonCombineCommandType.CustomCommand })
				.Concat(data)
				.ToArray();
			var crc = cmd.GetCrc();
			return cmd
				.Concat(crc.HasValue ? crc.Value.ToBE() : ((ushort)0).ToBE())
				.ToArray();
		}
	}
}