﻿namespace Compass.Ufin.Terminal.Devices.Wialon.Combine
{
	/// <summary> Подзапись LBS параметров </summary>
	public sealed class WialonCombinePacketDataSubRecord04
	{
		/// <summary> MCC (Mobile Country Code) — код страны </summary>
		public ushort MCC     { get; private set; }
		/// <summary> MNC (Mobile Network Code) — код сотовой сети </summary>
		public ushort MNC     { get; private set; }
		/// <summary> LAC (Local Area Code) — код локальной зоны </summary>
		/// <remarks> Локальная зона — это совокупность базовых станций, которые обслуживаются одним контроллером базовых станций. </remarks>
		public ushort LAC     { get; private set; }
		/// <summary> Cell ID — идентификатор соты; присваивается оператором каждому сектору каждой базовой станции </summary>
		public ushort CellID  { get; private set; }
		/// <summary> Rx level — уровень принимаемого по данному каналу радиосигнала на входе в приёмник GSM-модема </summary>
		public ushort RxLevel { get; private set; }
		/// <summary> TA (Timing Advance) — параметр компенсации времени прохождения сигнала от GSM-модема до базовой станции </summary>
		/// <remarks> Фактически означает расстояние до базовой станции. </remarks>
		public ushort TA      { get; private set; }

		private WialonCombinePacketDataSubRecord04() { }
		public static WialonCombinePacketDataSubRecord04 Parse(WialonCombineDataReader dataReader)
		{
			if ((dataReader?.RemainingBytesCount ?? 0) < 12)
				return default;
			return new WialonCombinePacketDataSubRecord04
			{
				MCC     = dataReader.ReadBigEndian16(),
				MNC     = dataReader.ReadBigEndian16(),
				LAC     = dataReader.ReadBigEndian16(),
				CellID  = dataReader.ReadBigEndian16(),
				RxLevel = dataReader.ReadBigEndian16(),
				TA      = dataReader.ReadBigEndian16(),
			};
		}
	}
}