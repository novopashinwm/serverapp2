﻿using System.Linq;
using System.Text;

namespace Compass.Ufin.Terminal.Devices.Wialon.Combine
{
	/// <summary> Тип записи «Picture» </summary>
	public sealed class WialonCombinePacketDataSubRecord03
	{
		/// <summary> Ind* — порядковый номер передаваемого блока (нумерация с нуля) </summary>
		public ushort Ind   { get; private set; }
		/// <summary> Len** — размер блока фотоизображения </summary>
		public uint   Len   { get; private set; }
		/// <summary> Count* — номер последнего блока при нумерации с нуля </summary>
		public ushort Count { get; private set; }
		/// <summary> Name — имя передаваемого изображения. Текстовое поле, заканчивающееся 0x00 </summary>
		public string Name  { get; private set; }
		/// <summary> Bin — бинарный блок изображения </summary>
		public byte[] Bin   { get; private set; }

		private WialonCombinePacketDataSubRecord03() { }
		public static WialonCombinePacketDataSubRecord03 Parse(WialonCombineDataReader dataReader)
		{
			if ((dataReader?.RemainingBytesCount ?? 0) < 4)
				return default;
			var ind = dataReader.ReadExpandableBigEndian16();
			if (!ind.HasValue)
				return default;
			var len = dataReader.ReadExpandableBigEndian32();
			if (!len.HasValue)
				return default;
			var cnt = dataReader.ReadExpandableBigEndian16();
			if (!cnt.HasValue)
				return default;
			var nam = dataReader.ReadNullTerminaredString();
			if (string.IsNullOrEmpty(nam))
				return default;
			if ((dataReader?.RemainingBytesCount ?? 0) < len.Value)
				return default;
			var bin = dataReader.ReadBytes((int)len.Value);
			return new WialonCombinePacketDataSubRecord03
			{
				Ind   = ind.Value,
				Len   = len.Value,
				Count = cnt.Value,
				Name  = nam,
				Bin   = bin,
			};
		}
	}
}