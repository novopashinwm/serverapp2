﻿namespace Compass.Ufin.Terminal.Devices.Wialon.Combine
{
	public enum WialonCombinePacketDataSubRecordType : ushort
	{
		CustomParameters      = 00,
		PositionData          = 01,
		IoData                = 02,
		Picture               = 03,
		LbsParameters         = 04,
		FuelParameters        = 05,
		TemperatureParameters = 06,
		CanParameters         = 07,
		CounterParameters     = 08,
		AnalogParameters      = 09,
		DriverСodeParameters  = 10,
		TachoFile             = 11,
		DriverMssage          = 12,
	}
}