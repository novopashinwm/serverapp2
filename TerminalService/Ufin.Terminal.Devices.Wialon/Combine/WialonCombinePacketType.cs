﻿namespace Compass.Ufin.Terminal.Devices.Wialon.Combine
{
	public enum WialonCombinePacketType : ushort
	{
		/// <summary> Пакет логина </summary>
		Login     = 0,
		/// <summary> Пакет данных </summary>
		Data      = 1,
		/// <summary> Пакет поддержки соединения (не содержит полезной информации и контрольной суммы) </summary>
		KeepAlive = 2,
		/// <summary> Пакет подтверждения устройством приема блока данных </summary>
		ACK       = 3,
	}
}