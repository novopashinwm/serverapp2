﻿namespace Compass.Ufin.Terminal.Devices.Wialon.Combine
{
	/// <summary> Подзапись местоположения </summary>
	public sealed class WialonCombinePacketDataSubRecord01
	{
		/// <summary> Широта, градусы </summary>
		public double Lat        { get; private set; }
		/// <summary> Долгота, градусы </summary>
		public double Lng        { get; private set; }
		/// <summary> Скорость, км/ч </summary>
		public int    Speed      { get; private set; }
		/// <summary> Курс, градусы (0 - 359) </summary>
		public int    Cource     { get; private set; }
		/// <summary> Высота над уровнем моря, метры </summary>
		public int    Alt        { get; private set; }
		/// <summary> Количество видимых спутников </summary>
		public int    Satellites { get; private set; }
		/// <summary> Значение снижения точности в горизонтальной плоскости </summary>
		public double Hdop       { get; private set; }

		private WialonCombinePacketDataSubRecord01() { }
		public static WialonCombinePacketDataSubRecord01 Parse(WialonCombineDataReader dataReader)
		{
			if ((dataReader?.RemainingBytesCount ?? 0) < 17)
				return default;
			return new WialonCombinePacketDataSubRecord01
			{
				Lat        = dataReader.ReadBigEndian32() / 1000000.0,
				Lng        = dataReader.ReadBigEndian32() / 1000000.0,
				Speed      = dataReader.ReadBigEndian16(),
				Cource     = dataReader.ReadBigEndian16(),
				Alt        = dataReader.ReadBigEndian16(),
				Satellites = dataReader.ReadByte(),
				Hdop       = dataReader.ReadBigEndian16() / 100.0,
			};
		}
	}
}