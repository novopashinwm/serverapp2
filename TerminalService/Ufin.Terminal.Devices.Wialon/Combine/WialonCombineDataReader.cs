﻿using System;
using System.Linq;
using System.Text;
using FORIS.TSS.Common;

namespace Compass.Ufin.Terminal.Devices.Wialon.Combine
{
	/// <summary> Поточное чтение массива байт с добавлением специфики Wialon Combine </summary>
	public sealed class WialonCombineDataReader : DataReader
	{
		public WialonCombineDataReader(ArraySegment<byte> data)
			: base(data.Array, data.Offset, data.Offset + data.Count)
		{
		}
		public WialonCombineDataReader(byte[] data)
			: base(data)
		{
		}
		public WialonCombineDataReader(byte[] data, int startIndex)
			: base(data, startIndex)
		{
		}
		public WialonCombineDataReader(byte[] data, int startIndex, int endIndex)
			: base(data, startIndex, endIndex)
		{
		}
		public UInt16? ReadExpandableBigEndian16()
		{
			if (RemainingBytesCount <= 0)
				return default;
			var type = ReadBigEndian16(1);
			if (0x80 == (type & 0x80))
			{
				if (RemainingBytesCount <= 0)
					return default;
				type = Rewind(1).ReadBigEndian16(2);
			}
			return (UInt16?)(type & 0x7FFF);
		}
		public UInt32? ReadExpandableBigEndian32()
		{
			if (RemainingBytesCount <= 1)
				return default;
			var type = ReadBigEndian32(2);
			if (0x8000 == (type & 0x8000))
			{
				if (RemainingBytesCount <= 1)
					return default;
				type = Rewind(2).ReadBigEndian32(4);
			}
			return (UInt32?)(type & 0x7FFFFFFF);
		}
		public string ReadNullTerminaredString()
		{
			if (!Any())
				return default;
			var result = Encoding.ASCII.GetString(
				ReadBytesWhile(b => b != 0x00)
				.ToArray());
			if (!Any())
				return default;
			Skip(1);
			return result;
		}
	}
}