﻿namespace Compass.Ufin.Terminal.Devices.Wialon.Combine
{
	public enum WialonCombineAnswerCode : byte
	{
		/// <summary> 0 Пакет успешно зарегистрирован </summary>
		Success       = 0,
		/// <summary> 1 Ошибка авторизации </summary>
		AuthError     = 1,
		/// <summary> 3 Пакет не зарегистрирован </summary>
		NotRegistered = 2,
		/// <summary> 4 Ошибка CRC </summary>
		CrcError      = 3,
		/// <summary> 255 Команда на устройство </summary>
		Command       = 255,
	}
}