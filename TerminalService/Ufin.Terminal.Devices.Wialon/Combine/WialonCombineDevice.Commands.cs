﻿using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Terminal;

namespace Compass.Ufin.Terminal.Devices.Wialon.Combine
{
	public partial class WialonCombineDevice
	{
		public override byte[] GetCmd(IStdCommand command)
		{
			if (null == command?.Target?.DeviceID)
				return default;
			var vegaDevs = new[]
			{
				new { DevType = DeviceNames.VegaMt21,   },
				new { DevType = DeviceNames.VegaMt22,   },
				new { DevType = DeviceNames.VegaMt23,   },
				new { DevType = DeviceNames.VegaMt24,   },
				new { DevType = DeviceNames.VegaMt25,   },
				new { DevType = DeviceNames.VegaMtxExt, },
				new { DevType = DeviceNames.VegaMtxInt, },
				new { DevType = DeviceNames.VegaMtxLte, },
			};
			var vegaCmds = new[]
			{
				// Очистить черный ящик          |custom_msg|tcp|"#bboxclear#"    |1|0|
				new { CmdType = CmdType.BlackBoxClear,   CmdText = $"#bboxclear#\r\n"     },
				// Сделать фотографию            |custom_msg|tcp|"#makephoto#"    |1|0|
				new { CmdType = CmdType.CapturePicture,  CmdText = $"#makephoto#\r\n"     },
				// CAN: Активировать CAN-скрипт N|custom_msg|tcp|"#runcanscriptN#"|1|0|
				new { CmdType = CmdType.RunCanScript01,  CmdText = $"#runcanscript1#\r\n" },
				new { CmdType = CmdType.RunCanScript02,  CmdText = $"#runcanscript2#\r\n" },
				new { CmdType = CmdType.RunCanScript03,  CmdText = $"#runcanscript3#\r\n" },
				// Выход N: вкл                  |custom_msg|tcp|"#setoutN=1#" |1|0|
				// Выход N: выкл                 |custom_msg|tcp|"#setoutN=0#" |1|0|
				new { CmdType = CmdType.SetOut01On,      CmdText = $"#setout1=1#\r\n"     },
				new { CmdType = CmdType.SetOut01Off,     CmdText = $"#setout1=0#\r\n"     },
				new { CmdType = CmdType.SetOut02On,      CmdText = $"#setout2=1#\r\n"     },
				new { CmdType = CmdType.SetOut02Off,     CmdText = $"#setout2=0#\r\n"     },
				// Дополнительный выход N: вкл   |custom_msg|tcp|"#setextoutN=1#" |1|0|
				// Дополнительный выход N: выкл  |custom_msg|tcp|"#setextoutN=0#" |1|0|
				new { CmdType = CmdType.SetExtOut01On,   CmdText = $"#setextout1=1#\r\n"  },
				new { CmdType = CmdType.SetExtOut01Off,  CmdText = $"#setextout1=0#\r\n"  },
				new { CmdType = CmdType.SetExtOut02On,   CmdText = $"#setextout2=1#\r\n"  },
				new { CmdType = CmdType.SetExtOut02Off,  CmdText = $"#setextout2=0#\r\n"  },
				new { CmdType = CmdType.SetExtOut03On,   CmdText = $"#setextout3=1#\r\n"  },
				new { CmdType = CmdType.SetExtOut03Off,  CmdText = $"#setextout3=0#\r\n"  },
				new { CmdType = CmdType.SetExtOut04On,   CmdText = $"#setextout4=1#\r\n"  },
				new { CmdType = CmdType.SetExtOut04Off,  CmdText = $"#setextout4=0#\r\n"  },
				new { CmdType = CmdType.SetExtOut05On,   CmdText = $"#setextout5=1#\r\n"  },
				new { CmdType = CmdType.SetExtOut05Off,  CmdText = $"#setextout5=0#\r\n"  },
				new { CmdType = CmdType.SetExtOut06On,   CmdText = $"#setextout6=1#\r\n"  },
				new { CmdType = CmdType.SetExtOut06Off,  CmdText = $"#setextout6=0#\r\n"  },
				new { CmdType = CmdType.SetExtOut07On,   CmdText = $"#setextout7=1#\r\n"  },
				new { CmdType = CmdType.SetExtOut07Off,  CmdText = $"#setextout7=0#\r\n"  },
				new { CmdType = CmdType.SetExtOut08On,   CmdText = $"#setextout8=1#\r\n"  },
				new { CmdType = CmdType.SetExtOut08Off,  CmdText = $"#setextout8=0#\r\n"  },
				new { CmdType = CmdType.SetExtOut09On,   CmdText = $"#setextout9=1#\r\n"  },
				new { CmdType = CmdType.SetExtOut09Off,  CmdText = $"#setextout9=0#\r\n"  },
				new { CmdType = CmdType.SetExtOut10On,   CmdText = $"#setextout10=1#\r\n" },
				new { CmdType = CmdType.SetExtOut10Off,  CmdText = $"#setextout10=0#\r\n" },
				new { CmdType = CmdType.SetExtOut11On,   CmdText = $"#setextout11=1#\r\n" },
				new { CmdType = CmdType.SetExtOut11Off,  CmdText = $"#setextout11=0#\r\n" },
				new { CmdType = CmdType.SetExtOut12On,   CmdText = $"#setextout12=1#\r\n" },
				new { CmdType = CmdType.SetExtOut12Off,  CmdText = $"#setextout12=0#\r\n" },
				new { CmdType = CmdType.SetExtOut13On,   CmdText = $"#setextout13=1#\r\n" },
				new { CmdType = CmdType.SetExtOut13Off,  CmdText = $"#setextout13=0#\r\n" },
				new { CmdType = CmdType.SetExtOut14On,   CmdText = $"#setextout14=1#\r\n" },
				new { CmdType = CmdType.SetExtOut14Off,  CmdText = $"#setextout14=0#\r\n" },
				new { CmdType = CmdType.SetExtOut15On,   CmdText = $"#setextout15=1#\r\n" },
				new { CmdType = CmdType.SetExtOut15Off,  CmdText = $"#setextout15=0#\r\n" },
			};

			return vegaDevs
				?.SelectMany(d => vegaCmds.Select(c => new { d.DevType, c.CmdType, c.CmdText }))
				?.Where(x => x.DevType == command?.Target?.DeviceType && x.CmdType == command?.Type)
				?.Select(x => WialonCombinePacket.GetCommandPacket(Encoding.ASCII.GetBytes(x.CmdText)))
				?.FirstOrDefault() ?? base.GetCmd(command);
		}
	}
}