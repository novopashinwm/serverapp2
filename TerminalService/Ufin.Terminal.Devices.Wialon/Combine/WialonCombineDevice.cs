﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace Compass.Ufin.Terminal.Devices.Wialon.Combine
{
	public partial class WialonCombineDevice : Device
	{
		internal const string ProtocolName = "Wialon Combine";
		internal static class DeviceNames
		{
			internal const string Wialon     = "Wialon";
			internal const string VegaMt21   = "Вега МТ-21";
			internal const string VegaMt22   = "Вега МТ-22";
			internal const string VegaMt23   = "Вега МТ-23";
			internal const string VegaMt24   = "Вега МТ-24";
			internal const string VegaMt25   = "Вега МТ-25";
			internal const string VegaMtxInt = "Вега MT X Int";
			internal const string VegaMtxExt = "Вега MT X Ext";
			internal const string VegaMtxLte = "Вега MT X LTE";
		}
		public WialonCombineDevice() : base(typeof(WialonCombineSensor), new[]
		{
			DeviceNames.Wialon,
			DeviceNames.VegaMt21,
			DeviceNames.VegaMt22,
			DeviceNames.VegaMt23,
			DeviceNames.VegaMt24,
			DeviceNames.VegaMt25,
			DeviceNames.VegaMtxInt,
			DeviceNames.VegaMtxExt,
			DeviceNames.VegaMtxLte,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			return WialonCombinePacket.IsMatch(data);
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			var results = new List<object>();
			var packets = WialonCombinePacket.ExtractPackets(data);
			var lastPacket = packets.LastOrDefault();
			if (null != lastPacket)
				bufferRest = lastPacket.Rest.ToArray();
			else
				bufferRest = data;
			var rvstate = stateData as WialonReceiverState;
			//////////////////////////////////////////////////////////
			foreach (var packet in packets)
			{
				// Проверяем контрольную сумму
				if (!packet.IsValid)
				{
					results.Add(WialonCombinePacket.GetConfirmPacket(packet, (byte)WialonCombineAnswerCode.CrcError));
					continue;
				}
				switch (packet.Type)
				{
					case (ushort)WialonCombinePacketType.Login:
						results.AddRange(ParseLoginPacket(packet));
						// Получаем в локальную переменную результат логина, нужно если в одном пакете и логин и прочие пакеты
						rvstate = results
							?.OfType<ReceiverStoreToStateMessage>()
							?.FirstOrDefault()
							?.StateData as WialonReceiverState;
						break;
					case (ushort)WialonCombinePacketType.Data:
						results.AddRange(ParseDataPacket(packet, rvstate));
						break;
					case (ushort)WialonCombinePacketType.KeepAlive:
					case (ushort)WialonCombinePacketType.ACK:
						results.Add(WialonCombinePacket.GetConfirmPacket(packet, (byte)WialonCombineAnswerCode.Success));
						break;
					default:
						Trace.TraceWarning("{0}: message type {1} is not supported: {2}, buffer rest: {3}",
							this,
							packet.Type,
							BitConverter.ToString(packet.Pack.ToArray() ?? new byte[0]),
							BitConverter.ToString(bufferRest            ?? new byte[0]));
						results.Add(WialonCombinePacket.GetConfirmPacket(packet, (byte)WialonCombineAnswerCode.NotRegistered));
						break;
				}
			}
			//////////////////////////////////////////////////////////
			if (rvstate != null)
			{
				foreach (var result in results)
				{
					var mu = result as IMobilUnit;
					if (mu == null)
						continue;
					mu.DeviceID                            = rvstate?.DeviceID;
					mu.Properties[DeviceProperty.Protocol] = $"{ProtocolName} {rvstate.Version}".Trim();
				}
			}
			//////////////////////////////////////////////////////////
			return results;
			//////////////////////////////////////////////////////////
		}
		private IEnumerable<object> ParseLoginPacket(WialonCombinePacket packet)
		{
			var results = new List<object>();
			if (packet?.Type != (ushort?)WialonCombinePacketType.Login)
				return results;

			var packetLogin = WialonCombinePacketLogin.Parse(packet);
			if (packetLogin?.ReceiverState != null)
			{
				if (CheckDevicePassword(packetLogin?.ReceiverState))
				{
					results.Add(new ReceiverStoreToStateMessage { StateData = packetLogin.ReceiverState });
					results.Add(WialonCombinePacket.GetConfirmPacket(packet, (byte)WialonCombineAnswerCode.Success));
				}
				else
					results.Add(WialonCombinePacket.GetConfirmPacket(packet, (byte)WialonCombineAnswerCode.AuthError)); // Ошибка проверки пароля.
			}
			return results;
		}
		private bool CheckDevicePassword(WialonReceiverState state)
		{
			// Пока проверки нет
			return true;
		}
		private IEnumerable<object> ParseDataPacket(WialonCombinePacket packet, WialonReceiverState state = null)
		{
			var results = new List<object>();
			if (packet?.Type != (ushort?)WialonCombinePacketType.Data)
				return results;

			var packetData = WialonCombinePacketData.Parse(packet);
			if (packetData != null)
			{
				/////////////////////////////////////////////
				//$"{new { state?.DeviceID, state?.Password, state?.Version, Data = packetData }.ToJson()}".CallTraceInformation();
				/////////////////////////////////////////////
				if (packetData.Rec00 != null)
				{
					var mu        = UnitReceived();
					mu.Time       = (int)packetData.Time;
					mu.CorrectGPS = false;
					foreach(var sensor in packetData.Rec00)
						mu.SetSensorValue(sensor.Number, sensor.GetSensorValue());
					//var sensor = default(WialonCombinePacketDataSubRecordParam);
					//sensor = packetData.Rec00?.FirstOrDefault(p => p.Number == (ushort)WialonCombineSensor.Ignition);
					//if (sensor != null)
					//	mu.SetSensorValue(sensor.Number, sensor.GetSensorValue());
					//sensor = packetData.Rec00?.FirstOrDefault(p => p.Number == (ushort)WialonCombineSensor.Sos);
					//if (sensor != null)
					//	mu.SetSensorValue(sensor.Number, sensor.GetSensorValue());
					//sensor = packetData.Rec00?.FirstOrDefault(p => p.Number == (ushort)WialonCombineSensor.PowerVoltage);
					//if (sensor != null)
					//	mu.SetSensorValue(sensor.Number, sensor.GetSensorValue());
					if (0 != (mu.SensorValues?.Count ?? 0))
						results.Add(mu);
				}
				if (packetData.Rec01 != null)
				{
					var mu        = UnitReceived();
					mu.Time       = (int)packetData.Time;
					mu.Latitude   = packetData.Rec01?.Lat ?? GeoHelper.InvalidLatitude;
					mu.Longitude  = packetData.Rec01?.Lng ?? GeoHelper.InvalidLongitude;
					mu.Speed      = packetData.Rec01?.Speed;
					mu.Course     = packetData.Rec01?.Cource;
					mu.Satellites = packetData.Rec01?.Satellites ?? 0;
					if ((packetData.Rec01?.Alt).HasValue)
						mu.Height = packetData.Rec01.Alt * 100;
					results.Add(mu);
				}
				if (packetData.Rec02 != null)
				{
					// TODO: Заполняем цифровые датчики
				}
				if (packetData.Rec03 != null && state != null)
				{
					if (state.Photos == null)
						state.Photos = new List<WialonPhoto>(1);

					var photo = state.Photos.FirstOrDefault(p => p.Name == packetData.Rec03.Name);
					if (photo == null)
					{
						photo = new WialonPhoto
						{
							LogTime = (int)packetData.Time,
							Name    = packetData.Rec03.Name,
							Parts   = new List<byte[]>(packetData.Rec03.Count + 1)
						};
						state.Photos.Add(photo);
					}
					if (photo.Parts.Count < photo.ExpectedPartCount)
						photo.Parts.Add(packetData.Rec03.Bin);

					if (photo.Parts.Count == photo.ExpectedPartCount)
					{
						state.Photos.Remove(photo);
						results.Add(new PictureUnit
						{
							Time       = photo.LogTime,
							Bytes      = photo.Parts.SelectMany(p => p).ToArray(),
							Url        = photo.Name,
							CorrectGPS = false,
						});
					}
				}
				if (packetData.Rec04 != null)
				{
					// TODO: Заполняем информацию о сотовой сети
				}
				results.Add(WialonCombinePacket.GetConfirmPacket(packet, (byte)WialonCombineAnswerCode.Success));
			}
			else
				results.Add(WialonCombinePacket.GetConfirmPacket(packet, (byte)WialonCombineAnswerCode.NotRegistered));
			return results;
		}
	}
}