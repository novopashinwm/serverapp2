﻿using System.Linq;
using System.Text;

namespace Compass.Ufin.Terminal.Devices.Wialon.Combine
{
	/// <summary> Пакет логина в формате Wialon Combine </summary>
	public sealed class WialonCombinePacketLogin
	{
		public WialonReceiverState ReceiverState { get; private set; }
		private WialonCombinePacketLogin() { }
		public static WialonCombinePacketLogin Parse(WialonCombinePacket packet)
		{
			// Нет данных выходим или не тот пакет
			if (packet?.Type != (ushort?)WialonCombinePacketType.Login)
				return default;
			// Создаем читателя и разбираем
			return Parse(new WialonCombineDataReader(packet.Data));
		}
		private static WialonCombinePacketLogin Parse(WialonCombineDataReader dataReader)
		{
			if (dataReader == null)
				return default;
			// Проверяем наличие байт для обязательных полей
			if ((dataReader?.RemainingBytesCount ?? 0) < 2)
				return default;
			var version = dataReader.ReadExpandableBigEndian16();
			if (!version.HasValue)
				return default;
			if ((dataReader?.RemainingBytesCount ?? 0) < 1)
				return default;
			var flags    = dataReader.ReadByte();
			var deviceId = default(string);
			var password = default(string);
			var flagsDid = (flags >> 4) & 0x0F;
			var flagsPwd = (flags >> 0) & 0x0F;
			switch (flagsDid)
			{
				case 1:
					if ((dataReader?.RemainingBytesCount ?? 0) < 2)
						return default;
					deviceId = dataReader.ReadBigEndian16().ToString();
					break;
				case 2:
					if ((dataReader?.RemainingBytesCount ?? 0) < 4)
						return default;
					deviceId = dataReader.ReadBigEndian32().ToString();
					break;
				case 3:
					if ((dataReader?.RemainingBytesCount ?? 0) < 8)
						return default;
					deviceId = dataReader.ReadBigEndian64().ToString();
					break;
				case 4:
					deviceId = dataReader.ReadNullTerminaredString();
					break;
				default:
					break;
			}
			switch (flagsPwd)
			{
				case 1:
					if ((dataReader?.RemainingBytesCount ?? 0) < 2)
						return default;
					password = dataReader.ReadBigEndian16().ToString();
					break;
				case 2:
					if ((dataReader?.RemainingBytesCount ?? 0) < 4)
						return default;
					password = dataReader.ReadBigEndian32().ToString();
					break;
				case 3:
					if ((dataReader?.RemainingBytesCount ?? 0) < 8)
						return default;
					password = dataReader.ReadBigEndian64().ToString();
					break;
				case 4:
					password = dataReader.ReadNullTerminaredString();
					break;
				default:
					break;
			}

			return new WialonCombinePacketLogin()
			{
				ReceiverState = new WialonReceiverState()
				{
					Version  = $"ver. {version}",
					DeviceID = deviceId,
					Password = password,
				}
			};
		}
	}
}