﻿namespace Compass.Ufin.Terminal.Devices.Wialon.Combine
{
	public enum WialonCombineCommandType : byte
	{
		CustomCommand = 0,
		FirmwareBlock = 1,
		ConfigBlock   = 2,
	}
}