﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RU.NVG.NIKA.Terminal.TL2000
{
	public enum TL2000Sensor
	{
		ADC = 11,
		ExternalBatteryVoltage = 12,
		InternalBatteryVoltage = 13,
		PCBTemperature = 14,
		ExternalTemperature = 15,
		Moving = 16,
		Accel = 17,
		AccelCorneringValue = 18,
		Trip = 19,
		SignalStrength = 20,

		DigitalInput1 = 101,
		DigitalInput2 = 102,
		DigitalInput3 = 103,
		DigitalOutput1 = 104,
		DigitalOutput2 = 105,

//        Geofence1Status = 301,
//        Geofence2Status = 302,
//        Geofence3Status = 303,
//        Geofence4Status = 304,
//        Geofence5Status = 305,
//        Geofence6Status = 306,
//        Geofence7Status = 307,
//        Geofence8Status = 308,
//        Geofence9Status = 309,

		/// <summary>
		/// Trackpro will send 1 in case of over speeding and 0 if it is in a specified speed limit
		/// </summary>
//        OverSpeedStatus = 201,
		/// <summary>
		/// Trackpro can track AC is switched on when ignition is OFF
		/// </summary>
		ACStatus = 202,
		/// <summary>
		/// Immobiliser status can be 0 or 1
		/// </summary>
//        ImmobilizerStatus = 203,
		/// <summary>
		/// External temperature alert
		/// </summary>
		//ExternalTemperatureStatus = 204,
		/// <summary>
		/// 1 – harsh acceleration, 2 – harsh braking, 3- harsh cornering, 0 – NULL
		/// </summary>
		DrivingStatus = 206,
		/// <summary>
		/// Trackpro model reconnect alert
		/// </summary>
		ModelStatus = 207,
		/// <summary>
		/// Low internal battery voltage alert
		/// </summary>
//        LowInternalBattery = 208,
		/// <summary>
		/// External battery removal alert
		/// </summary>
		ExternalBatteryRemoved = 209,
		/// <summary>
		/// GPS error alert
		/// </summary>
		GpsError = 210,
		/// <summary>
		/// Towed away alert
		/// </summary>
		TowedAway = 211,
		/// <summary>
		/// GPRS available and not able to connect to server
		/// </summary>
		CouldntConnectToServer = 212,
		/// <summary>
		/// SLEEP – 2 / DEEP SLEEP -0 / ACTIVE -1
		/// </summary>
		Mode = 213,
		/// <summary>
		/// RF ID / iBUTTON
		/// </summary>
//        RFId = 214,
		/// <summary>
		/// SMS count
		/// </summary>
		SmsCount = 215,
	}
}