﻿using System;
using System.Globalization;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.Common;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace RU.NVG.NIKA.Terminal.TL2000
{
	class TL2000Reader : SplittedStringReader
	{
		public TL2000Reader(string s, params char[] delimeter) : base(s, delimeter)
		{
		}

		public Commands ReadCommand()
		{
			var s = ReadString();
			if(!s.StartsWith("("))
				throw new FormatException("No start element (");

			Commands command;
			if(!Enum.TryParse(s.Substring(1), out command))
			{
				throw new NotSupportedException("Command not supported by TL2000 protocol: " + s.Substring(1));
			}

			return command;
		}

		public double? ReadLatitude()
		{
			var s = ReadString();
			double result;
			if (double.TryParse(s, NumberStyles.Any, NumberHelper.FormatInfo, out result))
			{
				return result;
			}

			return null;
		}

		public double? ReadLongitude()
		{
			var s = ReadString();
			double result;
			if (double.TryParse(s, NumberStyles.Any, NumberHelper.FormatInfo, out result))
			{
				return result;
			}
			return null;
		}

		public int ReadCheckSum()
		{
			var s = ReadString();
			if(!s.EndsWith(")"))
				throw new FormatException("No end element )");

			return int.Parse(s.Substring(0, s.Length - 1));
		}
	
		public DateTime? ReadDateTime()
		{
			var result = (DateTime?) null;
			var dateString = ReadString().Trim();
			var timeString = ReadString().Trim();
			int temp;

			if (!string.IsNullOrEmpty(dateString) && dateString.Length == 6)
			{
				var year = int.TryParse(dateString.Substring(4, 2), out temp) ? temp + 2000 : 0;
				var month = int.TryParse(dateString.Substring(2, 2), out temp) ? temp : 0;
				var day = int.TryParse(dateString.Substring(0, 2), out temp) ? temp : 0;
				if (year > 0 && month > 0)
				{
					result = new DateTime(year, month, day);
				}
			}

			if (result.HasValue && !string.IsNullOrEmpty(timeString) && timeString.Length == 6)
			{
				var hours = int.TryParse(timeString.Substring(0, 2), out temp) ? temp : 0;
				var minutes = int.TryParse(timeString.Substring(2, 2), out temp) ? temp : 0;
				var seconds = int.TryParse(timeString.Substring(4, 2), out temp) ? temp : 0;
				if (hours > 0 && minutes > 0 && seconds > 0)
				{
					return result.Value.Add(new TimeSpan(hours, minutes, seconds));
				}
			}
				
			return null;
		}

		public MobilUnit ReadUnit()
		{
			MobilUnit result;
			var command = ReadCommand();
			switch (command)
			{
				case Commands.TLA:
					result = ProcessAlertDataProtocol();
					break;
				case Commands.TLB:
					result = ProcessNormalDataProtocol();
					break;
				case Commands.TLC:
					result = ProcessConfigurationProtocol();
					break;
				case Commands.TLL:
					result = ProcessDataProtocol();
					break;
				case Commands.TLN:
					result = ProcessNormalDataProtocol();
					break;
				default:
					throw new NotSupportedException("Commnand not supported by TL2000 protocol: " + command);
			}
			return result;
		}

		private MobilUnit ProcessDataProtocol()
		{
			var imei = ReadString();
			var date = ReadDateTime();
			var lat = ReadLatitude();
			var lng = ReadLongitude();
			var speed = ReadNullableInt();
			var cource = ReadNullableInt();
			var altitude = ReadNullableInt();
			var noSatelites = ReadNullableInt();
			var dataType = (DataType)ReadInt();
			var signalStrength = ReadNullableInt();
			var digitalInput = ReadString();
			var digitalOutput = ReadString();
			var adc = ReadDouble();
			var externalBatteryVoltage = ReadDouble();
			var internalBatteryVoltage = ReadDouble();
			var pcbTemperature = ReadDouble();
			var externalTemperatureSensorData = ReadDouble();
			var moving = ReadNullableBoolean();
			var accel = ReadDouble();
			var accelCorneringValue = ReadDouble();
			var trip = ReadNullableInt();
			var odometer = ReadNullableInt();

			var mobilUnit = new MobilUnit
			{
				DeviceID = imei,
				Time = TimeHelper.GetSecondsFromBase(date ?? DateTime.UtcNow),
				Latitude = lat.HasValue ? lat.Value : MobilUnit.InvalidLatitude,
				Longitude = lng.HasValue ? lng.Value : MobilUnit.InvalidLongitude,
				Speed = speed,
				Course = cource,
				Height = altitude,
				Satellites = noSatelites.HasValue ? noSatelites.Value : 0,
				Run = odometer.HasValue ? odometer.Value : 0,
				//[12:34:05] Shruti Kuber: To consider whether lat long is valid or not,
				//[12:34:13] Shruti Kuber: check parameter GPS status
				//[12:34:15] Shruti Kuber: if 1
				//[12:34:24] Shruti Kuber: only then consider GPS values as valid
				CorrectGPS = dataType == DataType.GpsTimeGpsLocation
			};

			mobilUnit.Properties.Add(DeviceProperty.IMEI, imei);
			mobilUnit.Properties.Add(DeviceProperty.Protocol, ControllerType.Names.TL2000);
			if (adc.HasValue)
				mobilUnit.SetSensorValue((int)TL2000Sensor.ADC, (long)adc.Value);
			if (accel.HasValue)
				mobilUnit.SetSensorValue((int)TL2000Sensor.Accel, (long)accel.Value);
			if (accelCorneringValue.HasValue)
				mobilUnit.SetSensorValue((int)TL2000Sensor.AccelCorneringValue, (long)accelCorneringValue.Value);
			if (externalBatteryVoltage.HasValue)
				mobilUnit.SetSensorValue((int)TL2000Sensor.ExternalBatteryVoltage, (long)externalBatteryVoltage.Value);
			if (internalBatteryVoltage.HasValue)
				mobilUnit.SetSensorValue((int)TL2000Sensor.InternalBatteryVoltage, (long)internalBatteryVoltage.Value);
			if (pcbTemperature.HasValue)
				mobilUnit.SetSensorValue((int)TL2000Sensor.PCBTemperature, (long)pcbTemperature.Value);
			if (externalTemperatureSensorData.HasValue)
				mobilUnit.SetSensorValue((int)TL2000Sensor.ExternalTemperature, (long)externalTemperatureSensorData.Value);
			if (moving.HasValue)
				mobilUnit.SetSensorValue((int)TL2000Sensor.Moving, moving.Value ? 1 : 0);
			if (signalStrength.HasValue)
				mobilUnit.SetSensorValue((int)TL2000Sensor.SignalStrength, signalStrength.Value);
			if (trip.HasValue)
				mobilUnit.SetSensorValue((int)TL2000Sensor.Trip, trip.Value);

			if (digitalInput.Length == 3)
			{
				var digitalInputs = digitalInput.Select(ch => ch == '1').ToArray();
				mobilUnit.SetSensorValue((int)TL2000Sensor.DigitalInput1, digitalInputs[0] ? 1 : 0);
				mobilUnit.SetSensorValue((int)TL2000Sensor.DigitalInput2, digitalInputs[1] ? 1 : 0);
				mobilUnit.SetSensorValue((int)TL2000Sensor.DigitalInput3, digitalInputs[2] ? 1 : 0);
			}

			if (digitalOutput.Length == 2)
			{
				var digitalOutputs = digitalOutput.Select(ch => ch == '1').ToArray();
				mobilUnit.SetSensorValue((int)TL2000Sensor.DigitalOutput1, digitalOutputs[0] ? 1 : 0);
				mobilUnit.SetSensorValue((int)TL2000Sensor.DigitalOutput2, digitalOutputs[1] ? 1 : 0);
			}

			return mobilUnit;
		}

		private MobilUnit ProcessAlertDataProtocol()
		{
			var mobilUnit = (MobilUnit)ProcessDataProtocol();
			if (mobilUnit == null) return null;

			var overSpeedStatus = ReadNullableBoolean();
			var acStatus = ReadNullableBoolean();
			var immobilizerStatus = ReadNullableBoolean();
			var externalTemperatureStatus = ReadNullableBoolean();
			var geofenceStatus = ReadString();
			var drivingStatus = ReadNullableInt();
			var modelStatus = ReadNullableInt();
			var lowInternalBattery = ReadNullableInt();
			var externalBatteryRemoved = ReadNullableInt();
			var gpsError = ReadNullableInt();
			var towedAway = ReadNullableInt();
			var notAbleConnectToServer = ReadNullableInt();
			var mode = ReadNullableInt();
			var rfId = ReadString();
			var smsCount = ReadNullableInt();

			var packetNo = ReadInt();
			var checksum = ReadCheckSum();

			//if(overSpeedStatus.HasValue) 
			//    mobilUnit.SetSensorValue((int)TL2000Sensor.OverSpeedStatus, overSpeedStatus.Value ? 1 : 0);
			if (acStatus.HasValue)
				mobilUnit.SetSensorValue((int)TL2000Sensor.ACStatus, acStatus.Value ? 1 : 0);
			//if (immobilizerStatus.HasValue)
			//    mobilUnit.SetSensorValue((int)TL2000Sensor.ImmobilizerStatus, immobilizerStatus.Value ? 1 : 0);
			//if (externalTemperatureStatus.HasValue)
			//    mobilUnit.SetSensorValue((int)TL2000Sensor.ExternalTemperatureStatus, externalTemperatureStatus.Value ? 1 : 0);
			if (drivingStatus.HasValue)
				mobilUnit.SetSensorValue((int)TL2000Sensor.DrivingStatus, drivingStatus.Value);
			if (modelStatus.HasValue)
				mobilUnit.SetSensorValue((int)TL2000Sensor.ModelStatus, modelStatus.Value);
			//if(lowInternalBattery.HasValue)
			//    mobilUnit.SetSensorValue((int)TL2000Sensor.LowInternalBattery, lowInternalBattery.Value);
			if (externalBatteryRemoved.HasValue)
				mobilUnit.SetSensorValue((int)TL2000Sensor.ExternalBatteryRemoved, externalBatteryRemoved.Value);
			if (gpsError.HasValue)
				mobilUnit.SetSensorValue((int)TL2000Sensor.GpsError, gpsError.Value);
			if (towedAway.HasValue)
				mobilUnit.SetSensorValue((int)TL2000Sensor.TowedAway, towedAway.Value);
			if (mode.HasValue)
				mobilUnit.SetSensorValue((int)TL2000Sensor.Mode, mode.Value);
			if (smsCount.HasValue)
				mobilUnit.SetSensorValue((int)TL2000Sensor.SmsCount, smsCount.Value);

			//if (!string.IsNullOrEmpty(geofenceStatus) && geofenceStatus.Length == 9)
			//{
			//    mobilUnit.SetSensorValue((int)TL2000Sensor.Geofence1Status, geofenceStatus[0] - '0');
			//    mobilUnit.SetSensorValue((int)TL2000Sensor.Geofence2Status, geofenceStatus[1] - '0');
			//    mobilUnit.SetSensorValue((int)TL2000Sensor.Geofence3Status, geofenceStatus[2] - '0');
			//    mobilUnit.SetSensorValue((int)TL2000Sensor.Geofence4Status, geofenceStatus[3] - '0');
			//    mobilUnit.SetSensorValue((int)TL2000Sensor.Geofence5Status, geofenceStatus[4] - '0');
			//    mobilUnit.SetSensorValue((int)TL2000Sensor.Geofence6Status, geofenceStatus[5] - '0');
			//    mobilUnit.SetSensorValue((int)TL2000Sensor.Geofence7Status, geofenceStatus[6] - '0');
			//    mobilUnit.SetSensorValue((int)TL2000Sensor.Geofence8Status, geofenceStatus[7] - '0');
			//    mobilUnit.SetSensorValue((int)TL2000Sensor.Geofence9Status, geofenceStatus[8] - '0');
			//}

			return mobilUnit;
		}

		private MobilUnit ProcessNormalDataProtocol()
		{
			var mobilUnit = ProcessDataProtocol();
			var packetNo = ReadInt();
			var checksum = ReadCheckSum();

			return mobilUnit;
		}

		private MobilUnit ProcessConfigurationProtocol()
		{
			return null;
		}
	}
}