﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace RU.NVG.NIKA.Terminal.TL2000
{
	[SuppressMessage("ReSharper", "InconsistentNaming")]
	enum Commands
	{
		/// <summary>
		/// Configuration protocol - 1468 bytes
		/// </summary>
		TLC,
		/// <summary>
		/// Blackout data protocol ( Max 10 [Configurable ] data packets ) TLB and TLL
		/// </summary>
		TLL,
		/// <summary>
		/// Blackout data protocol ( Max 10 [Configurable ] data packets ) TLB and TLL
		/// </summary>
		TLB,
		/// <summary>
		/// Normal data protocol - 126 bytes
		/// </summary>
		TLN,
		/// <summary>
		/// Alert protocol
		/// ( Normal data + alerts + events ) [165 Bytes – Single Data Packet, 1815 – Multiple Data packet (Max 11 data packets)]
		/// </summary>
		TLA
	}

	enum DataType
	{
		NoData = 0,
		GpsTimeGpsLocation = 1,
		GsmTimeAndPreviousGpsLocation = 2,
		PreviousGpsTimeAndOldGpsLocation = 3,
		GsmTimeAndGsmLocation = 4
	}

	/// <summary>
	/// TL2000 autocop
	/// </summary>
	public class TL2000Device : Device
	{
		private readonly string[] _allowedCommands = new[] { Commands.TLA, Commands.TLB, Commands.TLC, Commands.TLL, Commands.TLN,  }.Select(c => c.ToString()).ToArray();

		public TL2000Device()
			: base(null, new[] { ControllerType.Names.TL2000 })
		{
		}

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;
			var result = new List<object>();
			var packetString = Encoding.ASCII.GetString(data);
			var partialIndex = packetString.LastIndexOf(");", StringComparison.Ordinal) + 2;
			var isPartial = partialIndex != packetString.Length;
			if (isPartial)
			{
				var partialPacket = packetString.Substring(partialIndex, packetString.Length - partialIndex);
				bufferRest = Encoding.ASCII.GetBytes(partialPacket);
				packetString = packetString.Substring(0, partialIndex);
			}

			var parts = packetString.Substring(0, packetString.Length - 1).Split(';');
			foreach (var part in parts)
			{
				var reader = new TL2000Reader(part, ',');
				var mobilUnit = reader.ReadUnit();
				if (mobilUnit != null)
				{
					result.Add(mobilUnit);
				}
			}

			var confirmString = string.Format(CultureInfo.InvariantCulture, "@O#{0}$", parts.Length);
			var confirmBytes = Encoding.ASCII.GetBytes(confirmString);
			result.Add(new ConfirmPacket(confirmBytes));

			return result;
		}

		public override bool SupportData(byte[] data)
		{
			var packet = Encoding.ASCII.GetString(data);
			if (!packet.StartsWith("("))
				return false;
			var parts = packet.Substring(0, packet.Length - 1).Split(';');
			if (!parts.All(s => s.StartsWith("(")))
				return false;

			if (!parts.All(s => _allowedCommands.Any(c => c == s.Substring(1, 3))))
				return false;

			return true;
		}
	}
}