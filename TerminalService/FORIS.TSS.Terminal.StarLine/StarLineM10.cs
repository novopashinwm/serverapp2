﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.Common;
using FORIS.TSS.Terminal.Terminal;
using FORIS.TSS.TerminalService.Interfaces;

namespace FORIS.TSS.Terminal.StarLine
{
	public class StarLineM10 : Device
	{
		internal const string ProtocolName = "StarLine M10";
		internal static class DeviceNames
		{
			internal const string StarLineM10 = "StarLine M10";
		}
		public StarLineM10() : base(null, new[]
		{
			DeviceNames.StarLineM10,
		})
		{
		}
		public override bool SupportData(byte[] data)
		{
			if (data.Length != 19 || //Длина авторизационного пакета
				data[0] != 'A') //Тип пакета - "Авторизация"
				return false;

			var calculatedCrc = CalculateCrc(data, 0, data.Length - 1);
			var receivedCrc = data[data.Length - 1];

			return calculatedCrc == receivedCrc;
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;

			var packetTypeByte = data[0];

			//Одна посылка - один пакет (официальный ответ от strakhov@starline-alarm.ru)

			switch (packetTypeByte)
			{
				case (byte)'A':
					{
						const int registrationPacketSize = 19;
						if (stateData != null || count != registrationPacketSize)
						{
							Trace.WriteLine("Unexpected header packet: " + BitConverter.ToString(data));
							return null;
						}

						var header = ReadAutorizationPacket(new DataReader(data, 1, count - 1));
						var calculatedCrc = CalculateCrc(data, 0, registrationPacketSize-1);

						return new List<object>
								   {
									   new ConfirmPacket {Data = (new List<byte>(_responsePrefix) {calculatedCrc}).ToArray()},
									   new ReceiverStoreToStateMessage
										   {
											   StateData = header
										   }
								   };
					}
				case 2:
					{
						const int workPacketSize = 34;
						if (data.Length < workPacketSize || count < workPacketSize)
						{
							bufferRest = data;
							return null;
						}
						var calculatedCrc = CalculateCrc(data, 0, count-1);
						var receivedCrc = data[workPacketSize - 1];

						if (calculatedCrc != receivedCrc)
							return null;

						var mobilUnit = ReadWorkPacket(new DataReader(data, 1, workPacketSize - 1));
						var header = stateData as StarLineM10Header;
						if (header != null)
						{
							mobilUnit.DeviceID = header.DeviceID;
							mobilUnit.Firmware = header.Firmware.ToString(CultureInfo.InvariantCulture);
							mobilUnit.Telephone = header.Phone;
						}
						return new List<object> { mobilUnit };
					}
				default:
					Trace.WriteLine("Unknown packet subtype for StarLineM10: " + BitConverter.ToString(data));
					return null;
			}
		}
		private readonly byte[] _responsePrefix = Encoding.ASCII.GetBytes("resp_crc=");
		private const string WorkModes = "FSAGW";
		private IMobilUnit ReadWorkPacket(DataReader reader)
		{
			var alarmAndBatteryByte = reader.ReadByte();
			//1 бит (7-й) - состояние входа (0 – не активен, 1 – активен (тревога))
			var alarm = (alarmAndBatteryByte & 0x80) != 0;
			//1 бит (0-6-й) - состояние батареи в процентах (не больше 100, если = 100, подключено внешнее питание)
			var batteryStateValue = alarmAndBatteryByte & 0x7F;
			var batteryLevelPercentage = batteryStateValue < 100 ? batteryStateValue : (int?)null;
			var outerPower = batteryStateValue == 100;
			//8+8 бит - оставшееся время работы канала (если = 0 – канал выключен), если неограниченно, =0xFFFF
			var channelWorkTimeLeftValue = reader.ReadBigEndian32(2);
			var channelWorkTimeLeft = 0 < channelWorkTimeLeftValue && channelWorkTimeLeftValue < 0xffff ? channelWorkTimeLeftValue : (uint?)null;
			var channelEnabled = channelWorkTimeLeftValue != 0;
			var channelWorkTimeInfinite = channelWorkTimeLeftValue == 0xffff;
			//8 бит -  температура (число со знаком), если нет данных = -100
			var temperatureByte = reader.ReadByte();
			var temperatureSignedValue = (int)(((temperatureByte & 0x80) == 0) ? temperatureByte : (0xffff0000 | temperatureByte));
			var temperatureValue = temperatureSignedValue == -100 ? (int?)null : temperatureSignedValue;
			//8 бит -  интервал пробуждения (от 0 до 240)
			var wakeUpIntervalByte = reader.ReadByte();
			var wakeUpInterval = wakeUpIntervalByte <= 240 ? wakeUpIntervalByte : (int?)null;
			//8 бит -  минуты или часы для времени сна (M или H)
			var sleepingTimeType = (char)reader.ReadByte();
			//8 бит -  режим работы (F, S, A, G, W)
			var workMode = (char)reader.ReadByte();
			//8 бит -  интервал передачи GPRS-пакетов, в секундах (от 0 до 240)
			var gprsPacketTransmittingIntervalByte = reader.ReadByte();
			var gprsPacketTransmittingInterval = gprsPacketTransmittingIntervalByte <= 240 ? gprsPacketTransmittingIntervalByte : (int?)null;
			//8 бит - MCC (трехзначное десятичное чисдо, например = 250), если нет данных = 0xFF
			var mccByte = reader.ReadByte();
			var mcc = mccByte != 0xff ? mccByte : (int?)null;
			//8 бит - MNC (двухзначное десятичное чисдо, например = 01), если нет данных = 0xFF
			var mncByte = reader.ReadByte();
			var mnc = mncByte != 0xFF ? mncByte : (int?)null;
			//8+8 бит - LAC (четырехзначное шестнадцатиричное чисдо, например = 772F), если нет данных = 0xFFFF
			var lacValue = reader.ReadBigEndian32(2);
			var lac = lacValue != 0xffff ? lacValue : (uint?)null;
			var cidValue = reader.ReadBigEndian32(2);
			var cid = cidValue != 0xffff ? cidValue : (uint?)null;

			//Поле 4 (18 байт)
			//2 бита (6-7-й) - статус GPS-данных  (0 – нет данных от GPS-приемника, 1 - устаревшие координаты, 2 – действительные координаты)
			//6 бит (0-5-й) - количество видимых спутников (если нет данных, =0)
			var gpsStatusByte = reader.ReadByte();
			var gpsStatus = gpsStatusByte >> 6;
			var satelliteCount = (gpsStatusByte & 0x3f);
			//24 бита – время по Гринвичу (hhmmss) (если нет данных, =0)
			//время по Гринвичу, шестизначное число в формате чч:мм:сс, при необходимости «добивается» нулями 
			var hhmmss = reader.ReadBigEndian32(3);
			var hours = hhmmss / 100 / 100;
			var minutes = (hhmmss / 100) % 100;
			var seconds = hhmmss % 100;
			//24 бита – дата (ddmmyy) (если нет данных, =0)
			//дата, шестизначное число в формате дд:мм:гг, при необходимости «добивается» нулями
			var ddmmyy = reader.ReadBigEndian32(3);
			var dayOfMonth = ddmmyy / 100 / 100;
			var month = (ddmmyy / 100) % 100;
			var shortYear = ddmmyy % 100;
			//8 бит – широта (градусы) (если нет данных, =0)
			var latDegrees = reader.ReadByte();
			//20 бит (4-27-й) - широта (минуты, доли минут, если нет данных, =0)
			//1 бит (0-й) - часть света (0-юг, 1-север) (если нет данных, =0)
			var latMinutesValue = reader.ReadBigEndian32(3);
			var latMinutes = (latMinutesValue >> 4) / 10000m;
			var northSemisphere = (latMinutesValue & 0x01) != 0;
			//8 бит – долгота (градусы) (если нет данных, =0)
			var lngDegrees = reader.ReadByte();
			//20 бит (4-27-й) - долгота (минуты, доли минут, если нет данных, =0)
			//1 бит (0-й) - часть света (0-запад, 1-восток) (если нет данных, =0)
			var lngMinutesValue = reader.ReadBigEndian32(3);
			var lngMinutes = (lngMinutesValue >> 4) / 10000m;
			var eastSemisphere = (lngMinutesValue & 0x01) != 0;
			var speedKnots = reader.ReadByte();
			var courseDegrees = reader.ReadBigEndian32(2);

			var mobilUnit = UnitReceived();

			if (mobilUnit.SensorValues == null)
				mobilUnit.SensorValues = new Dictionary<int, long>();
			mobilUnit.SensorValues.Add((int) InputType.Alarm, alarm ? 1 : 0);
			if (batteryLevelPercentage != null)
				mobilUnit.SensorValues.Add((int) InputType.BatteryLevel, batteryLevelPercentage.Value);
			mobilUnit.SensorValues.Add((int)InputType.OuterPower, outerPower ? 1 : 0);
			mobilUnit.SensorValues.Add((int)InputType.ChannelEnabled, channelEnabled ? 1 : 0);
			if (channelWorkTimeLeft != null)
				mobilUnit.SensorValues.Add((int)InputType.ChannelWorkTime, channelWorkTimeLeft.Value);
			mobilUnit.SensorValues.Add((int)InputType.ChannelWorkTimeInfinite, channelWorkTimeInfinite ? 1 : 0);
			if (temperatureValue != null)
				mobilUnit.SensorValues.Add((int)InputType.Temperature, temperatureValue.Value);
			if (wakeUpInterval != null && wakeUpInterval.Value != 0)
			{
				switch (sleepingTimeType)
				{
					case 'M':
						mobilUnit.SensorValues.Add((int)InputType.WakeUpInterval, wakeUpInterval.Value * 60);
						break;
					case 'H':
						mobilUnit.SensorValues.Add((int)InputType.WakeUpInterval, wakeUpInterval.Value * 60 * 60);
						break;
				}
			}

			var workModeIndex = WorkModes.IndexOf(workMode);
			if (workModeIndex != -1)
				mobilUnit.SensorValues.Add((int)InputType.WorkMode, workMode);
			if (gprsPacketTransmittingInterval != null)
				mobilUnit.SensorValues.Add((int)InputType.GprsPacketTransmissionInterval, gprsPacketTransmittingInterval.Value);

			var logTime = TimeHelper.GetSecondsFromBase(
				new DateTime(2000 + (int)shortYear, (int)month, (int)dayOfMonth,
							 (int)hours, (int)minutes, (int)seconds, 
							 DateTimeKind.Utc));
			mobilUnit.Time = logTime;
			if (mcc != null && mnc != null && lac != null && cid != null)
			{
				mobilUnit.CellNetworks = new[] 
				{ 
					new CellNetworkRecord
					{
						LogTime = logTime,
						Number = 0,
						CellID = (int) cid.Value,
						CountryCode = mcc.Value.ToString(),
						LAC = (int) lac.Value,
						NetworkCode = mnc.Value.ToString(),
					}
				};
			}
			mobilUnit.Satellites = satelliteCount;
			mobilUnit.Latitude = (double) (latDegrees + latMinutes / 60.0m);
			if (!northSemisphere)
				mobilUnit.Latitude = -mobilUnit.Latitude;
			mobilUnit.Longitude = (double)(lngDegrees + lngMinutes / 60.0m);

			if (!eastSemisphere)
				mobilUnit.Longitude = -mobilUnit.Longitude;
			
			mobilUnit.CorrectGPS = gpsStatus != 0 &&
				mobilUnit.Latitude != 0 && mobilUnit.Longitude != 0; //При включении после полной разрядки батареи устройство передает координаты (0;0).

			mobilUnit.Speed = (int)(1.852 * speedKnots);
			mobilUnit.Course = (int?) courseDegrees;
			
			return mobilUnit;
		}
		private static byte CalculateCrc(byte[] data, int index, int count)
		{
			var reader = new DataReader(data, index, index + count);
			byte crc = 0x3B;
			while (reader.Any())
			{
				var @byte = reader.ReadByte();
				crc += (byte)(0x56 ^ @byte);
				crc++;
				crc ^= (byte)(0xC5 + @byte);
				crc--;
			}
			return crc;
		}
		private static StarLineM10Header ReadAutorizationPacket(DataReader reader)
		{
			//16x4 бит – IMEI (15 цифр, старший полубайт – незначащий, нулевой)
			var imeiStringBuilder = new StringBuilder(15);
			imeiStringBuilder.Append(reader.ReadByte());//Первая цифра IMEI
			for (var i = 0; i != (15-1)/2; ++i)
			{
				var @byte = reader.ReadByte();
				imeiStringBuilder.Append(@byte >> 4);
				imeiStringBuilder.Append(@byte & 0x0F);
			}
			var deviceID = imeiStringBuilder.ToString();

			//4 бита (4-7-й) - тип системы (0 – 15), для маяка = 4
			//4 бита (0-3-й) - версия «железа» системы (0 – 15), пока = 3
			//8 бит – версия ПО системы (0 – 255), латинская буква, пока = а 
			var firmware = (int)reader.ReadBigEndian32(2);

			//10x4 бит – Login (10 последних цифр номера телефона)
			var phoneStringBuilder = new StringBuilder(10);
			for (var i = 0; i != 10/2; ++i)
			{
				var @byte = reader.ReadByte();
				phoneStringBuilder.Append(@byte >> 4);
				phoneStringBuilder.Append(@byte & 0x0F);
			}
			var phone = phoneStringBuilder.ToString();
				
			//4x4 бит - Password (4 цифры)
			reader.Skip(4 / 2);

			return new StarLineM10Header
			{
				DeviceID = deviceID,
				Firmware = firmware,
				Phone = phone
			};
							   
		}

	}
}