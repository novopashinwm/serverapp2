﻿using System;

namespace FORIS.TSS.Terminal.StarLine
{
	[Serializable]
	class StarLineM10Header
	{
		public string DeviceID;
		public int    Firmware;
		public string Phone;
	}
}