﻿namespace FORIS.TSS.Terminal.StarLine
{
	enum InputType
	{
		Alarm                          = 0,
		BatteryLevel                   = 1,
		ChannelEnabled                 = 2,
		ChannelWorkTime                = 3,
		ChannelWorkTimeInfinite        = 4,
		GprsPacketTransmissionInterval = 5,
		OuterPower                     = 6,
		Temperature                    = 7,
		WakeUpInterval                 = 8,
		WorkMode                       = 9
	}
}