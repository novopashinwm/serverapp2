﻿namespace Compass.Ufin.Terminal.Devices.MegaStek
{
	public enum MegastekSensors
	{
		Sos = 6,
		BatteryLevel = 11,

		DigitalInput1 = 21,
		DigitalInput2 = 22,
		DigitalInput3 = 23,
		DigitalInput4 = 24,
		DigitalInput5 = 25,
		DigitalInput6 = 26,
		DigitalInput7 = 27,
		DigitalInput8 = 28,
		DigitalOutput1 = 31,
		DigitalOutput2 = 32,
		DigitalOutput3 = 33,
		DigitalOutput4 = 34,
		AnalogInput1 = 41,
		AnalogInput2 = 42,
		AnalogInput3 = 43,
		TemperatureSensor1 = 51,
		TemperatureSensor2 = 52,

		Hdop = 230,
		IsPlugged = 15,
		Belt = 62,
		Event = 71
	}
}