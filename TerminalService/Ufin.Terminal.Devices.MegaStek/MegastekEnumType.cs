﻿namespace Compass.Ufin.Terminal.Devices.MegaStek
{
	public enum MegastekEnumType
	{
		Restart  = 1,
		PowerOn  = 2,
		PowerOff = 3,
		Sos      = 4,
	}
}