﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;

namespace Compass.Ufin.Terminal.Devices.MegaStek
{
	public class MegaStekDevice : Device
	{
		public MegaStekDevice()
			: base(typeof(MegastekSensors), new[]
			{
				"Megastek MT60",
				"Megastek MT70",
				"Megastek MT90",
			})
		{
			
		}

		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			bufferRest = null;

			var reader = new SplittedStringReader(Encoding.ASCII.GetString(data, 0, count), ',');

			reader.Skip(1);

			var imei = reader.ReadString();
			var mu = new MobilUnit();
			mu.DeviceID = imei;

			//Device name
			reader.Skip(1);


			//<GPRS real-time/stored data flag>: ‘R’ means this GPRS data is real - time data, ‘S’ means this GPRS data is stored data.
			reader.Skip(1);

			var date = reader.ReadDate("ddMMyy");
			var time = reader.ReadTime("hhmmss");
			if (time == null)
				return null;

			mu.Time = TimeHelper.GetSecondsFromBase(date + time.Value);
			mu.CorrectGPS = reader.ReadString() == "A";
			var latitude = reader.ReadDecimal();
			var latitudePositive = reader.ReadString() == "N";
			var longitude = reader.ReadDecimal();
			var longitudePositive = reader.ReadString() == "E";
			if (mu.CorrectGPS && latitude != null && longitude != null)
			{
				var latitudeDegrees = (int) latitude.Value/100;
				var latitudeMinutes = (latitude - latitudeDegrees*100);
				mu.Latitude = (double) (latitudeDegrees + latitudeMinutes/60);
				if (!latitudePositive)
					mu.Latitude = -mu.Latitude;

				var longitudeDegrees = (int) longitude.Value/100;
				var longitudeMinutes = (longitude - longitudeDegrees*100);
				mu.Longitude = (double) (longitudeDegrees + longitudeMinutes/60);
				if (!longitudePositive)
					mu.Longitude = -mu.Longitude;
			}

			mu.Satellites = reader.ReadInt() + reader.ReadInt() + reader.ReadInt();
			var hdop = reader.ReadDecimal();
			if (hdop != null)
				mu.SetSensorValue((int) MegastekSensors.Hdop, (long) (hdop*100));

			var speedKnots = reader.ReadDecimal();
			if (speedKnots != null)
				mu.Speed = MeasureUnitHelper.KnotsToKMPH((double) speedKnots.Value);

			var course = reader.ReadDecimal();
			if (course != null)
				mu.Course = (int) Math.Round(course.Value);

			var height = reader.ReadDecimal();
			if (height != null)
				mu.Height = (int) Math.Round(height.Value);

			// mileage
			reader.Skip(1);

			var cnr = new CellNetworkRecord();
			cnr.LogTime = mu.Time;
			cnr.CountryCode = reader.ReadString();
			cnr.NetworkCode = reader.ReadString();
			cnr.LAC = reader.ReadHexInt();
			cnr.CellID = reader.ReadHexInt();
			cnr.SignalStrength = reader.ReadInt();
			mu.CellNetworks = new[]
			{
				cnr
			};

			var digitalInputs = reader.ReadString();
			for (var i = 0; i != digitalInputs.Length; ++i)
			{
				mu.SetSensorValue((int) MegastekSensors.DigitalInput1 + i, digitalInputs[i] == '1' ? 1 : 0);
			}
			var digitalOutputs = reader.ReadString();
			for (var i = 0; i != digitalOutputs.Length; ++i)
			{
				mu.SetSensorValue((int) MegastekSensors.DigitalOutput1 + i, digitalOutputs[i] == '1' ? 1 : 0);
			}

			for (var i = 0; i != 3; ++i)
				mu.SetSensorValue((int)MegastekSensors.AnalogInput1 + i, reader.ReadNullableInt());

			for (var i = 0; i != 2; ++i)
				mu.SetSensorValue((int)MegastekSensors.TemperatureSensor1 + i, reader.ReadNullableInt());

			//RFID information (reserved). (only used for vehicle tracker)
			reader.Skip(1);

			var accessories = reader.ReadString();
			mu.SetSensorValue((int)MegastekSensors.IsPlugged, accessories[1]-'0');
			mu.SetSensorValue((int)MegastekSensors.Belt, accessories[0] - '0');

			mu.SetSensorValue((int)MegastekSensors.BatteryLevel, reader.ReadInt());

			var @event = reader.ReadString();
			@event = @event.Substring(0, @event.IndexOf(";", StringComparison.OrdinalIgnoreCase));

			mu.SetSensorValue((int)MegastekSensors.Sos, 1);

			MegastekEnumType eventEnum;
			if (Enum.TryParse(@event, out eventEnum))
			{
				switch (eventEnum)
				{
					case MegastekEnumType.Sos:
						mu.SetSensorValue((int)MegastekSensors.Sos, 1);
						break;
				}
			}

			return new ArrayList {mu};
		}

		private static readonly byte[] Header = Encoding.ASCII.GetBytes("$MG");

		public override bool SupportData(byte[] data)
		{
			if (!data.StartsWith(Header))
				return false;

			if (data[data.Length - 1] != '!')
				return false;

			foreach (var b in data)
			{
				if (b < 32 || 127 < b)
					return false;
			}

			return true;
		}

		protected override IEnumerable<string> GetSetupCommands(
			string ip, string port, 
			string apnName, string apnUser, string apnPassword,
			string devicePassword)
		{
			yield return $"$SMS,000000;W020,180;!";
			yield return $"$SMS,000000;W002,{apnName},{apnUser},{apnPassword};!";
			yield return $"$SMS,000000;W003,{ip},{port};!";
			yield return $"$SMS,000000;W005,1;!";
			yield return $"$SMS,000000;W009,1;!";
			yield return $"$SMS,000000;W016,2;!";
		}
	}
}