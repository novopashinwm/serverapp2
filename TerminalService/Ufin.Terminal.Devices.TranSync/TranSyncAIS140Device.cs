﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.Helpers;
using FORIS.TSS.MobilUnit.Unit;
using FORIS.TSS.Terminal;

namespace Compass.Ufin.Terminal.Devices.TranSync
{
	public partial class TranSyncAIS140Device : Device
	{
		internal static class DeviceNames
		{
			internal const string TranSyncAIS140 = "TranSync AIS 140";
		}
		public TranSyncAIS140Device() : base(typeof(TranSyncAIS140Sensors), new[]
		{
			DeviceNames.TranSyncAIS140,
		})
		{
		}
		internal static readonly byte[] Prefix2A2A = { 0x2a, 0x2a };
		internal static readonly byte[] Prefix3A3A = { 0x3a, 0x3a };
		internal static readonly byte[] Suffix2323 = { 0x23, 0x23 };
		public override bool SupportData(byte[] data)
		{
			if (data.Length < 13)
				return false;

			// Первые два байта
			if (!data.StartsWith(Prefix2A2A) && !data.StartsWith(Prefix3A3A))
				return false;
			// Последние два байта
			if (!data.EndsWith(Suffix2323))
				return false;

			// Проверяем IMEI (начинается с 5 байта)
			if (9 < data[5] % 16)
				return false;
			for (var i = 6; i != 6 + 14 / 2; ++i)
			{
				if (9 < data[i] / 16)
					return false;
				if (9 < data[i] % 16)
					return false;
			}
			return true;
		}
		public override IList OnData(byte[] data, int count, object stateData, out byte[] bufferRest)
		{
			// Пакет всегда приходит один и целиком (т.к. последние байты должны)
			bufferRest = null;
			var dataReader = new DataReader(data);
			var results    = new List<object>();
			var prefixData = dataReader.ReadBytes(2);
			if (prefixData.SequenceEqual(Prefix3A3A))
				return ParseRecord3A3A(dataReader).ToList();
			if (prefixData.SequenceEqual(Prefix2A2A))
				return ParseRecord2A2A(dataReader).ToList();
			return results;
		}
		public IEnumerable<object> ParseRecord3A3A(DataReader dataReader)
		{
			var results = new List<object>();
			if (null == dataReader)
				return results;
			// (len=1) Packet length The total no of bytes from “Packet Length” to “Additional Info”
			var packetLen = dataReader.ReadByte();
			// (len=2) LAC Location Area Code(LAC) included in LAI consists of two bytes and is encoded
			dataReader.Skip(2);
			// (len=8) DeviceId is IMEI
			var deviceId = string
				// Переводим 8 байт в двоично-десятичную строку, из 2 символов
				.Concat(dataReader.ReadBytes(8).Select(x => $@"{x >> 4}{x & 0x0F}"))
				// Т.к. в таком преобразовании 16 символов, а IMEI это 15 последних
				.Substring(1);
			// (len=2) Information Serial Number
			dataReader.Skip(2);
			// (len=1) Protocol Number
			dataReader.Skip(1);
			// (len=6) Date&time UTC
			var timestamp = new DateTime(
				2000 + dataReader.ReadByte(),
				dataReader.ReadByte(),
				dataReader.ReadByte(),
				dataReader.ReadByte(),
				dataReader.ReadByte(),
				dataReader.ReadByte(),
				DateTimeKind.Utc);
			// (len=4) Latitude (without sign)
			var lat = dataReader.ReadBigEndian32(4) / 1800000M;
			// (len=4) Longitude (without sign)
			var lng = dataReader.ReadBigEndian32(4) * 180M / 324000000M;
			// (len=1) speed
			var speed = dataReader.ReadByte();
			// (len=2) Course
			var course = BitConverter.ToInt16(dataReader.ReadBytes(2), 0);
			// (len=1) MNC
			dataReader.Skip(1);
			// (len=2) Cell ID
			dataReader.Skip(2);
			// (len=4) Status
			var statusByte0 = new BitArray(dataReader.ReadBytes(1)); // Read BYTE_0 from Status
			var valid       = statusByte0[0];
			var latSign     = statusByte0[1] ? +1M : -1M;
			var lngSign     = statusByte0[2] ? +1M : -1M;
			var ignitionOn  = statusByte0[3];
			var powerOn     = statusByte0[4];
			var AircondOn   = statusByte0[5];
			var digitalOut1 = statusByte0[6];
			var digitalOut2 = statusByte0[7];
			dataReader.Skip();                                     // Read BYTE_1 from Status
			var statusByte2 = dataReader.ReadByte();               // Read BYTE_2 from Status
			var emergencyOn = default(int?);
			if (10 /*Emergency Alert ON*/ == statusByte2 || 11/*Emergency Alert ON*/ == statusByte2)
				emergencyOn = 10 == statusByte2 ? 1 : 0;
			dataReader.Skip();                                     // Read BYTE_3 from Status
			// GSM Signal Strength (GSM maximum signal strength is 31 then the value is 0x1F)
			var gsmSignalLevel = dataReader.ReadByte();
			// Voltage Level (Internal Battery Voltage level Max value is 42, then the value is 0x2A, i.e. 4.2v.)
			var voltageLevel   = dataReader.ReadByte();
			// Number of satellites used for fix
			var satellites = dataReader.ReadByte();
			// Формируем ответ
			var mu = UnitReceived(nameof(TranSyncAIS140Device));
			mu.DeviceID     = deviceId;
			mu.cmdType      = CmdType.Trace;
			mu.Time         = TimeHelper.GetSecondsFromBase(timestamp);
			mu.Latitude     = MobilUnit.InvalidLatitude;
			mu.Longitude    = MobilUnit.InvalidLongitude;
			mu.CorrectGPS   = valid;
			if (mu.CorrectGPS)
			{
				mu.Latitude   = (double)(lat * latSign);
				mu.Longitude  = (double)(lng * lngSign);
				mu.Speed      = speed;
				mu.Course     = course;
				mu.Satellites = satellites;
			}
			mu.SetSensorValue((int)TranSyncAIS140Sensors.Sos,            emergencyOn);
			mu.SetSensorValue((int)TranSyncAIS140Sensors.Ignition,       ignitionOn  ? 1 : 0);
			mu.SetSensorValue((int)TranSyncAIS140Sensors.GsmSignalLevel, gsmSignalLevel);
			mu.SetSensorValue((int)TranSyncAIS140Sensors.AirConditioner, AircondOn   ? 1 : 0);
			mu.SetSensorValue((int)TranSyncAIS140Sensors.DigitalOutput1, digitalOut1 ? 1 : 0);
			mu.SetSensorValue((int)TranSyncAIS140Sensors.DigitalOutput2, digitalOut1 ? 1 : 0);
			results.Add(mu);

			return results;
		}
		public IEnumerable<object> ParseRecord2A2A(DataReader dataReader)
		{
			var results = new List<object>();
			return results;
		}
	}
}