﻿namespace Compass.Ufin.Terminal.Devices.TranSync
{
	public enum TranSyncAIS140Sensors
	{
		Sos            = 01,
		Ignition       = 02,
		GsmSignalLevel = 03,
		AirConditioner = 04,
		DigitalOutput1 = 05,
		DigitalOutput2 = 06,
	}
}