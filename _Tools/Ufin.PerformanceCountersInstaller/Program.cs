﻿using System;
using FORIS.TSS.ServerApplication.Diagnostic;
using FORIS.TSS.ServerApplication.Geo.Address;

namespace Compass.Ufin.PerformanceCountersInstaller
{
	internal static class Program
	{
		private static void Main(string[] args)
		{
			try
			{
				Counters.CreateCounters();
				GeoAddressCachePerfCounters.CreateCategory();
				GeocodingResolverPerfCounters.CreateCategory();
				DistanceResolverPerfCounters.CreateCategory();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}

			Console.WriteLine("Done...");
			Console.ReadKey(true);
		}
	}
}