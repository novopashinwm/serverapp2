﻿using System;

namespace FORIS.TSS.Communication
{
	/// <summary>
	/// base datagram exception with byte sequence
	/// </summary>
	public class DatagramByteException : DatagramTypedException
	{
		public new byte[] Data;

		public DatagramByteException(string message, Type type, byte[] data, int begin)
			: base(message, type)
		{
			int len = data.Length - begin;
			Array.Copy(data, begin, Data = new byte[len], 0, len);
		}
	}
}