﻿using System;

namespace FORIS.TSS.Communication
{
	/// <summary>
	/// no valid datagram in byte sequence
	/// </summary>
	public class DatagramNotFoundException : DatagramByteException
	{
		public DatagramNotFoundException(string message, Type type, byte[] data, int begin)
			: base(message, type, data, begin)
		{
		}
	}
}