﻿using System;

namespace FORIS.TSS.Communication
{
	/// <summary>
	/// incorrect field value exception
	/// </summary>
	public class DatagramFieldException : DatagramByteException
	{
		public string Field;

		public DatagramFieldException(string message, Type type, byte[] data, int begin,
									  string field) : base(message, type, data, begin)
		{
			Field = field;
		}
	}
}