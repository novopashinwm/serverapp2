﻿using System;

namespace FORIS.TSS.Communication
{
	/// <summary>
	/// base datagram exception
	/// </summary>
	public class DatagramException : ApplicationException
	{
		public DatagramException(string message) : base(message)
		{
		}
	}
}