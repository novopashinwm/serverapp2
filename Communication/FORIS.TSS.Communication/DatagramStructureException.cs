﻿using System;

namespace FORIS.TSS.Communication
{
	/// <summary>
	/// structural error exception
	/// </summary>
	public class DatagramStructureException : DatagramByteException
	{
		public DatagramStructureException(string message, Type type, byte[] data, int begin)
			: base(message, type, data, begin)
		{
		}
	}
}