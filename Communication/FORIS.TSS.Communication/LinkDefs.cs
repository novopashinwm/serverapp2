﻿using System;

namespace FORIS.TSS.Communication
{
	/// <summary>
	/// Possible Link Error codes
	/// </summary>
	[Serializable]
	public enum ErrorCode : sbyte
	{
		OK,
		Unknown,
		SysError,
		NotFound,
		NoResponse,
		Busy,
		Disconnected,
		Timeout,
		EOF,
		Unavailable,
		WrongParam,
		BadData,
	};

	
	/// <summary>
	/// Link Status values
	/// </summary>
	[Serializable]
	public enum LinkStatus : sbyte
	{
		OK,
		Closed,
		Error,
		Open,
		Busy,
	};

	/// <summary>
	/// working mode
	/// </summary>
	public enum Mode
	{
		/// <summary>
		/// normal work cycle
		/// </summary>
		Normal			= 0,
		/// <summary>
		/// outgoing call request
		/// </summary>
		Calling			= 1,
		/// <summary>
		/// incoming call request
		/// </summary>
		Offering		= 2,
		/// <summary>
		/// incoming call accepted
		/// </summary>
		Answer			= 3,
		/// <summary>
		/// call held
		/// </summary>
		AnswerHold		= 4,
		/// <summary>
		/// outgoing call, active call on hold
		/// </summary>
		AnswerCalling 	= 5
	}
}