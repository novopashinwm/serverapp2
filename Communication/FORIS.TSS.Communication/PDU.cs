﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace FORIS.TSS.Communication
{
	/// <summary>
	/// sms pdu manipulation
	/// </summary>
	public abstract class PDU
	{
		const string PDU_TOA_INTERNATIONAL_TELEPHONE = "91";
		const string PDU_TOA_UNKNOWN_TELEPHONE = "81";

		static char[] emitoa_tab = new char[128]
		{
			'\x40',       /* 0X00 <@> COMMERCIAL AT */
			'\xa3',       /* 0X01 <> POUND SIGN */
			'\x24',       /* 0X02 <$> DOLLAR SIGN */
			'\xa5',       /* 0x03 <> YEN SIGN */
			'\xe8',       /* 0X04 <> LATIN SMALL LETTER E WITH GRAVE */
			'\xe9',       /* 0X05 <> LATIN SMALL LETTER E WITH ACUTE */
			'\xf9',       /* 0X06 <> LATIN SMALL LETTER U WITH GRAVE */
			'\xec',       /* 0X07 <> LATIN SMALL LETTER I WITH GRAVE */
			'\xf2',       /* 0X08 <> LATIN SMALL LETTER O WITH GRAVE */
			'\xe7',       /* 0X09 <> LATIN SMALL LETTER C WITH CEDILLA */
			'\x0a',       /* 0X0a <LINE FEED> */
			'\xd8',       /* 0X0b <> LATIN CAPITAL LETTER O WITH STROKE */
			'\xf8',       /* 0X0c <> LATIN SMALL LETTER O WITH STROKE */
			'\x0d',       /* 0X0d <CARRIAGE RETURN> */
			'\xc5',       /* 0X0e <> LATIN CAPITAL LETTER A WITH RING ABOVE */
			'\xe5',       /* 0X0f <> LATIN SMALL LETTER A WITH RING ABOVE */
			'\xc4',       /* 0x10 <> GREEK CAPITAL LETTER DELTA */
			'\x5f',       /* 0X11 <_> LOW LINE*/
			'\xd6',       /* 0x12 <> GREEK CAPITAL LETTER PHI */
			'\xc3',       /* 0x13 <> GREEK CAPITAL LETTER GAMMA */
			'\xcb',       /* 0x14 <> GREEK CAPITAL LETTER LAMBDA */
			'\xd9',       /* 0x15 <> GREEK CAPITAL LETTER OMEGA */
			'\xd0',       /* 0x16 <> GREEK CAPITAL LETTER PI */
			'\xd8',       /* 0x17 <> GREEK CAPITAL LETTER PSI */
			'\xd3',       /* 0x18 <> GREEK CAPITAL LETTER SIGMA */
			'\xc8',       /* 0x19 <> GREEK CAPITAL LETTER THETA */
			'\xce',       /* 0x1a <> GREEK CAPITAL LETTER XI */
			'\x1b',       /* 0X1b <ESCAPE> */
			'\xc6',       /* 0X1c <> LATIN CAPITAL LETTER AE */
			'\xe6',       /* 0X1d <> LATIN SMALL LETTER AE */
			'\xdf',       /* 0X1e <> LATIN SMALL LETTER SHARP S */
			'\xc9',       /* 0X1f <> LATIN CAPITAL LETTER E WITH ACUTE */
			'\x20',       /* 0x20 <> */
			'\x21',       /* 0X21 <!> EXCLAMATION MARK */
			'\x22',       /* 0X22 <""> QUOTATION MARK */
			'\x23',       /* 0X23 <#> NUMBER SIGN */
			'\xa4',       /* 0X24 <¤> CURRENCY SIGN */
			'\x25',       /* 0X25 <%> PERCENT SIGN */
			'\x26',       /* 0X26 <&> AMPERSAND */
			'\x27',       /* 0X27 <'> APOSTROPHE */
			'\x28',       /* 0X28 <(> LEFT PARENTHISIS */
			'\x29',       /* 0X29 <)> RIGHT PARENTHISIS */
			'\x2a',       /* 0X2a <*> ASTERISK */
			'\x2b',       /* 0X2b <+> PLUS SIGN */
			'\x2c',       /* 0X2c <,> COMMA */
			'\x2d',       /* 0X2d <-> HYPHEN-MINUS */
			'\x2e',       /* 0X2e <.> FULL STOP, DOT */
			'\x2f',       /* 0X2f </> SOLIDUS, SLASH */
			'\x30',       /* 0X30 <0> DIGIT ZERO */
			'\x31',       /* 0X31 <1> DIGIT ONE */
			'\x32',       /* 0X32 <2> DIGIT TWO */
			'\x33',       /* 0X33 <3> DIGIT THREE */
			'\x34',       /* 0X34 <4> DIGIT FOUR */
			'\x35',       /* 0X35 <5> DIGIT FIVE */
			'\x36',       /* 0X36 <6> DIGIT SIX */
			'\x37',       /* 0X37 <7> DIGIT SEVEN */
			'\x38',       /* 0X38 <8> DIGIT EIGHT */
			'\x39',       /* 0X39 <9> DIGIT NINE */
			'\x3a',       /* 0X3a <:> COLON */
			'\x3b',       /* 0X3b <;> SEMICOLON */
			'\x3c',       /* 0X3c <<> LESS-THAN SIGN */
			'\x3d',       /* 0X3d <=> EQUALS SIGN */
			'\x3e',       /* 0X3e <>> GREATER-THAN SIGN */
			'\x3f',       /* 0X3f <?> QUESTION MARK */
			'\xa1',       /* 0X40 <> INVERTED EXCLAMATION MARK */
			'\x41',       /* 0X41 <A> LATIN CAPITAL LETTER A */
			'\x42',       /* 0X42 <B> LATIN CAPITAL LETTER B */
			'\x43',       /* 0X43 <C> LATIN CAPITAL LETTER C */
			'\x44',       /* 0X44 <D> LATIN CAPITAL LETTER D */
			'\x45',       /* 0X45 <E> LATIN CAPITAL LETTER E */
			'\x46',       /* 0X46 <F> LATIN CAPITAL LETTER F */
			'\x47',       /* 0X47 <G> LATIN CAPITAL LETTER G */
			'\x48',       /* 0X48 <H> LATIN CAPITAL LETTER H */
			'\x49',       /* 0X49 <I> LATIN CAPITAL LETTER I */
			'\x4A',       /* 0X4a <J> LATIN CAPITAL LETTER J */
			'\x4B',       /* 0X4b <K> LATIN CAPITAL LETTER K */
			'\x4C',       /* 0X4c <L> LATIN CAPITAL LETTER L */
			'\x4D',       /* 0X4d <M> LATIN CAPITAL LETTER M */
			'\x4E',       /* 0X4e <N> LATIN CAPITAL LETTER N */
			'\x4F',       /* 0X4f <O> LATIN CAPITAL LETTER O */
			'\x50',       /* 0X50 <P> LATIN CAPITAL LETTER P */
			'\x51',       /* 0X51 <Q> LATIN CAPITAL LETTER Q */
			'\x52',       /* 0X52 <R> LATIN CAPITAL LETTER R */
			'\x53',       /* 0X53 <S> LATIN CAPITAL LETTER S */
			'\x54',       /* 0X54 <T> LATIN CAPITAL LETTER T */
			'\x55',       /* 0X55 <U> LATIN CAPITAL LETTER U */
			'\x56',       /* 0X56 <V> LATIN CAPITAL LETTER V */
			'\x57',       /* 0X57 <W> LATIN CAPITAL LETTER W */
			'\x58',       /* 0X58 <X> LATIN CAPITAL LETTER X */
			'\x59',       /* 0X59 <Y> LATIN CAPITAL LETTER Y */
			'\x5A',       /* 0X5a <Z> LATIN CAPITAL LETTER Z */
			'\xc4',       /* 0X5b <> LATIN CAPITAL LETTER A WITH DIAERESIS */
			'\xd6',       /* 0X5c <> LATIN CAPITAL LETTER O WITH DIAERESIS */
			'\xd1',       /* 0X5d <> LATIN CAPITAL LETTER N WITH TILDE */
			'\xdc',       /* 0X5e <> LATIN CAPITAL LETTER U WITH DIAERESIS */
			'\xa7',       /* 0X5f <> SECTION SIGN */
			'\xbf',       /* 0X60 <> INVERTED QUESTION MARK */
			'\x61',       /* 0X61 <a> LATIN SMALL LETTER A */
			'\x62',       /* 0X62 <b> LATIN SMALL LETTER B */
			'\x63',       /* 0X63 <c> LATIN SMALL LETTER C */
			'\x64',       /* 0X64 <d> LATIN SMALL LETTER D */
			'\x65',       /* 0X65 <e> LATIN SMALL LETTER E */
			'\x66',       /* 0X66 <f> LATIN SMALL LETTER F */
			'\x67',       /* 0X67 <g> LATIN SMALL LETTER G */
			'\x68',       /* 0X68 <h> LATIN SMALL LETTER H */
			'\x69',       /* 0X69 <i> LATIN SMALL LETTER I */
			'\x6a',       /* 0X6a <j> LATIN SMALL LETTER J */
			'\x6b',       /* 0X6b <k> LATIN SMALL LETTER K */
			'\x6c',       /* 0X6c <l> LATIN SMALL LETTER L */
			'\x6d',       /* 0X6d <m> LATIN SMALL LETTER M */
			'\x6e',       /* 0X6e <n> LATIN SMALL LETTER N */
			'\x6f',       /* 0X6f <o> LATIN SMALL LETTER O */
			'\x70',       /* 0X70 <p> LATIN SMALL LETTER P */
			'\x71',       /* 0X71 <q> LATIN SMALL LETTER Q */
			'\x72',       /* 0X72 <r> LATIN SMALL LETTER R */
			'\x73',       /* 0X73 <s> LATIN SMALL LETTER S */
			'\x74',       /* 0X74 <t> LATIN SMALL LETTER T */
			'\x75',       /* 0X75 <u> LATIN SMALL LETTER U */
			'\x76',       /* 0X76 <v> LATIN SMALL LETTER V */
			'\x77',       /* 0X77 <w> LATIN SMALL LETTER W */
			'\x78',       /* 0X78 <x> LATIN SMALL LETTER X */
			'\x79',       /* 0X79 <y> LATIN SMALL LETTER Y */
			'\x7a',       /* 0X7a <z> LATIN SMALL LETTER Z */
			'\xe4',       /* 0X7b <> LATIN SMALL LETTER A WITH DIAERESIS */
			'\xf6',       /* 0X7c <> LATIN SMALL LETTER O WITH DIAERESIS */
			'\xf1',       /* 0X7d <> LATIN SMALL LETTER N WITH TILDE */
			'\xfc',       /* 0X7e <> LATIN SMALL LETTER U WITH DIAERESIS */
			'\xe0'        /* 0X7f <> LATIN SMALL LETTER A WITH GRAVE */
		};                             /* emitoa_tab */

		static int escape_sequence = 0;
		static char sms2ascii(char c)
		{
			if('\x1b' == c) 
			{
				escape_sequence = 1;
				return('\x00');
			}
			if(0 != escape_sequence) 
			{
				escape_sequence = 0;
				switch (c)
				{
				case 'e':
					return('\xa4');         /* euro symbol */

				case '\x14':
					return('^');
					
				case '\x28':
					return('{');
					
				case '\x29':
					return('}');
					
				case '\x3c':
					return('[');
					
				case '\x3d':
					return('~');
					
				case '\x3e':
					return(']');
					
				case '\x2f':
					return('\\');
					
				default:
					break;
				} /* end switch */
			}
			return(emitoa_tab[c]);
		}

		static char[] atoemi_tab = new char[256]
		{
			'\x20',       /* 0X00 <NULL> */
			'\x20',       /* 0X01 <START OF HEADING> */
			'\x20',       /* 0X02 <START OF TEXT> */
			'\x20',       /* 0X03 <END OF TEXT> */
			'\x20',       /* 0X04 <END OF TRANSMISSION> */
			'\x20',       /* 0X05 <ENQUIRY> */
			'\x20',       /* 0X06 <ACKNOWLEDGE> */
			'\x20',       /* 0X07 <BELL> */
			'\x20',       /* 0X08 <BACKSPACE> */
			'\x20',       /* 0X09 <HORIZONTAL TABULATION> */
			'\x0a',       /* 0X0a <LINE FEED> */
			'\x20',       /* 0X0b <VERTICAL TABULATION> */
			'\x20',       /* 0X0c <FORM FEED> */
			'\x0d',       /* 0X0d <CARRIAGE RETURN> */
			'\x20',       /* 0X0e <SHIFT OUT> */
			'\x20',       /* 0X0f <SHIFT IN> */
			'\x20',       /* 0X10 <DATA LINK ESCAPE> */
			'\x11',       /* 0X11 <DEVICE CONTROL 1> */
			'\x20',       /* 0X12 <DEVICE CONTROL 2> */
			'\x20',       /* 0X13 <DEVICE CONTROL 3> */
			'\x20',       /* 0X14 <DEVICE CONTROL 4> */
			'\x20',       /* 0X15 <NEGATIVE ACKNOWLEDGE> */
			'\x20',       /* 0X16 <SYSCHRONOUS IDLE> */
			'\x20',       /* 0X17 <END OF TRANSMISSION BLOCK> */
			'\x20',       /* 0X18 <CANCEL> */
			'\x20',       /* 0X19 <END OF MEDIUM> */
			'\x20',       /* 0X1a <SUBSTITUTE> */
			'\x1b',       /* 0X1b <ESCAPE> */
			'\x20',       /* 0X1c <FILE SEPARATOR> */
			'\x20',       /* 0X1d <GROUP SEPARATOR> */
			'\x20',       /* 0X1e <RECORD SEPARATOR> */
			'\x20',       /* 0X1f <UNIT SEPARATOR> */
			'\x20',       /* 0X20 <SPACE> */
			'\x21',       /* 0X21 <!> EXCLAMATION MARK */
			'\x22',       /* 0X22 <""> QUOTATION MARK */
			'\x23',       /* 0X23 <#> NUMBER SIGN */
			'\x02',       /* 0X24 <$> DOLLAR SIGN */
			'\x25',       /* 0X25 <%> PERCENT SIGN */
			'\x26',       /* 0X26 <&> AMPERSAND */
			'\x27',       /* 0X27 <'> APOSTROPHE */
			'\x28',       /* 0X28 <(> LEFT PARENTHISIS */
			'\x29',       /* 0X29 <)> RIGHT PARENTHISIS */
			'\x2a',       /* 0X2a <*> ASTERISK */
			'\x2b',       /* 0X2b <+> PLUS SIGN */
			'\x2c',       /* 0X2c <,> COMMA */
			'\x2d',       /* 0X2d <-> HYPHEN-MINUS */
			'\x2e',       /* 0X2e <.> FULL STOP, DOT */
			'\x2f',       /* 0X2f </> SOLIDUS, SLASH */
			'\x30',       /* 0X30 <0> DIGIT ZERO */
			'\x31',       /* 0X31 <1> DIGIT ONE */
			'\x32',       /* 0X32 <2> DIGIT TWO */
			'\x33',       /* 0X33 <3> DIGIT THREE */
			'\x34',       /* 0X34 <4> DIGIT FOUR */
			'\x35',       /* 0X35 <5> DIGIT FIVE */
			'\x36',       /* 0X36 <6> DIGIT SIX */
			'\x37',       /* 0X37 <7> DIGIT SEVEN */
			'\x38',       /* 0X38 <8> DIGIT EIGHT */
			'\x39',       /* 0X39 <9> DIGIT NINE */
			'\x3a',       /* 0X3a <:> COLON */
			'\x3b',       /* 0X3b <;> SEMICOLON */
			'\x3c',       /* 0X3c <<> LESS-THAN SIGN */
			'\x3d',       /* 0X3d <=> EQUALS SIGN */
			'\x3e',       /* 0X3e <>> GREATER-THAN SIGN */
			'\x3f',       /* 0X3f <?> QUESTION MARK */
			'\x00',       /* 0X40 <@> COMMERCIAL AT */
			'\x41',       /* 0X41 <A> LATIN CAPITAL LETTER A */
			'\x42',       /* 0X42 <B> LATIN CAPITAL LETTER B */
			'\x43',       /* 0X43 <C> LATIN CAPITAL LETTER C */
			'\x44',       /* 0X44 <D> LATIN CAPITAL LETTER D */
			'\x45',       /* 0X45 <E> LATIN CAPITAL LETTER E */
			'\x46',       /* 0X46 <F> LATIN CAPITAL LETTER F */
			'\x47',       /* 0X47 <G> LATIN CAPITAL LETTER G */
			'\x48',       /* 0X48 <H> LATIN CAPITAL LETTER H */
			'\x49',       /* 0X49 <I> LATIN CAPITAL LETTER I */
			'\x4A',       /* 0X4a <J> LATIN CAPITAL LETTER J */
			'\x4B',       /* 0X4b <K> LATIN CAPITAL LETTER K */
			'\x4C',       /* 0X4c <L> LATIN CAPITAL LETTER L */
			'\x4D',       /* 0X4d <M> LATIN CAPITAL LETTER M */
			'\x4E',       /* 0X4e <N> LATIN CAPITAL LETTER N */
			'\x4F',       /* 0X4f <O> LATIN CAPITAL LETTER O */
			'\x50',       /* 0X50 <P> LATIN CAPITAL LETTER P */
			'\x51',       /* 0X51 <Q> LATIN CAPITAL LETTER Q */
			'\x52',       /* 0X52 <R> LATIN CAPITAL LETTER R */
			'\x53',       /* 0X53 <S> LATIN CAPITAL LETTER S */
			'\x54',       /* 0X54 <T> LATIN CAPITAL LETTER T */
			'\x55',       /* 0X55 <U> LATIN CAPITAL LETTER U */
			'\x56',       /* 0X56 <V> LATIN CAPITAL LETTER V */
			'\x57',       /* 0X57 <W> LATIN CAPITAL LETTER W */
			'\x58',       /* 0X58 <X> LATIN CAPITAL LETTER X */
			'\x59',       /* 0X59 <Y> LATIN CAPITAL LETTER Y */
			'\x5A',       /* 0X5a <Z> LATIN CAPITAL LETTER Z */
			'\x20',       /* 0X5b <[> LEFT SQUARE BRACKET */
			'\x20',       /* 0X5c <\> REVERSE SOLIDUS, BACKSLASH */
			'\x20',       /* 0X5d <]> RIGHT SQUARE BRACKET */
			'\x20',       /* 0X5e <^> CIRCUMFLEX ACCENT */
			'\x11',       /* 0X5f <_> LOW LINE */
			'\x20',       /* 0X60 <`> GRAVE ACCENT */
			'\x61',       /* 0X61 <a> LATIN SMALL LETTER A */
			'\x62',       /* 0X62 <b> LATIN SMALL LETTER B */
			'\x63',       /* 0X63 <c> LATIN SMALL LETTER C */
			'\x64',       /* 0X64 <d> LATIN SMALL LETTER D */
			'\x65',       /* 0X65 <e> LATIN SMALL LETTER E */
			'\x66',       /* 0X66 <f> LATIN SMALL LETTER F */
			'\x67',       /* 0X67 <g> LATIN SMALL LETTER G */
			'\x68',       /* 0X68 <h> LATIN SMALL LETTER H */
			'\x69',       /* 0X69 <i> LATIN SMALL LETTER I */
			'\x6a',       /* 0X6a <j> LATIN SMALL LETTER J */
			'\x6b',       /* 0X6b <k> LATIN SMALL LETTER K */
			'\x6c',       /* 0X6c <l> LATIN SMALL LETTER L */
			'\x6d',       /* 0X6d <m> LATIN SMALL LETTER M */
			'\x6e',       /* 0X6e <n> LATIN SMALL LETTER N */
			'\x6f',       /* 0X6f <o> LATIN SMALL LETTER O */
			'\x70',       /* 0X70 <p> LATIN SMALL LETTER P */
			'\x71',       /* 0X71 <q> LATIN SMALL LETTER Q */
			'\x72',       /* 0X72 <r> LATIN SMALL LETTER R */
			'\x73',       /* 0X73 <s> LATIN SMALL LETTER S */
			'\x74',       /* 0X74 <t> LATIN SMALL LETTER T */
			'\x75',       /* 0X75 <u> LATIN SMALL LETTER U */
			'\x76',       /* 0X76 <v> LATIN SMALL LETTER V */
			'\x77',       /* 0X77 <w> LATIN SMALL LETTER W */
			'\x78',       /* 0X78 <x> LATIN SMALL LETTER X */
			'\x79',       /* 0X79 <y> LATIN SMALL LETTER Y */
			'\x7a',       /* 0X7a <z> LATIN SMALL LETTER Z */
			'\x20',       /* 0X7b <{> LEFT CURLY BRACKET */
			'\x20',       /* 0X7c <|> VERTICAL LINE */
			'\x20',       /* 0X7d <}> RIGHT CURLY BRACKET */
			'\x20',       /* 0X7e <~> TILDE */
			'\x20',       /* 0X7f <DELETE> */
			'\x20',       /* 0X80 <> */
			'\x20',       /* 0X81 <> */
			'\x20',       /* 0X82 <> */
			'\x20',       /* 0X83 <> */
			'\x20',       /* 0X84 <> */
			'\x20',       /* 0X85 <> */
			'\x20',       /* 0X86 <> */
			'\x20',       /* 0X87 <> */
			'\x20',       /* 0X88 <> */
			'\x20',       /* 0X89 <> */
			'\x20',       /* 0X8a <> */
			'\x20',       /* 0X8b <> */
			'\x20',       /* 0X8c <> */
			'\x20',       /* 0X8d <> */
			'\x20',       /* 0X8e <> */
			'\x20',       /* 0X8f <> */
			'\x20',       /* 0X90 <> */
			'\x20',       /* 0X91 <> */
			'\x20',       /* 0X92 <> */
			'\x20',       /* 0X93 <> */
			'\x20',       /* 0X94 <> */
			'\x20',       /* 0X95 <> */
			'\x20',       /* 0X96 <> */
			'\x20',       /* 0X97 <> */
			'\x20',       /* 0X98 <> */
			'\x20',       /* 0X99 <> */
			'\x20',       /* 0X9a <> */
			'\x20',       /* 0X9b <> */
			'\x20',       /* 0X9c <> */
			'\x20',       /* 0X9d <> */
			'\x20',       /* 0X9e <> */
			'\x20',       /* 0X9f <> */
			'\x20',       /* 0Xa0 <> */
			'\x40',       /* 0Xa1 <> INVERTED EXCLAMATION MARK */
			'\x20',       /* 0Xa2 <> CENT SIGN */
			'\x01',       /* 0Xa3 <> POUND SIGN */
			'\x24',       /* 0Xa4 <¤> CURRENCY SIGN */
			'\x03',       /* 0x25 <> YEN SIGN */
			'\x20',       /* 0Xa6 <> BROKEN BAR */
			'\x5f',       /* 0Xa7 <> SECTION SIGN */
			'\x20',       /* 0Xa8 <> DIAERESIS */
			'\x20',       /* 0Xa9 <> COPYRIGHT SIGN */
			'\x20',       /* 0Xaa <> */
			'\x20',       /* 0Xab <> */
			'\x20',       /* 0Xac <> */
			'\x20',       /* 0Xad <> */
			'\x20',       /* 0Xae <> */
			'\x20',       /* 0Xaf <> */
			'\x20',       /* 0Xb0 <> */
			'\x20',       /* 0Xb1 <> */
			'\x20',       /* 0Xb2 <> */
			'\x20',       /* 0Xb3 <> */
			'\x20',       /* 0Xb4 <> */
			'\x20',       /* 0Xb5 <> */
			'\x20',       /* 0Xb6 <> */
			'\x20',       /* 0Xb7 <> */
			'\x20',       /* 0Xb8 <> */
			'\x20',       /* 0Xb9 <> */
			'\x20',       /* 0Xba <> */
			'\x20',       /* 0Xbb <> */
			'\x20',       /* 0Xbc <> */
			'\x20',       /* 0Xbd <> */
			'\x20',       /* 0Xbe <> */
			'\x60',       /* 0Xbf <> INVERTED QUESTION MARK */
			'\x20',       /* 0Xc0 <> */
			'\x20',       /* 0Xc1 <> */
			'\x20',       /* 0Xc2 <> */
			'\x20',       /* 0Xc3 <> */
			'\x5b',       /* 0Xc4 <> LATIN CAPITAL LETTER A WITH DIAERESIS */
			'\x0e',       /* 0Xc5 <> LATIN CAPITAL LETTER A WITH RING ABOVE */
			'\x1c',       /* 0Xc6 <> LATIN CAPITAL LETTER AE */
			'\x20',       /* 0Xc7 <> */
			'\x20',       /* 0Xc8 <> */
			'\x1f',       /* 0Xc9 <> LATIN CAPITAL LETTER E WITH ACUTE */
			'\x20',       /* 0Xca <> */
			'\x20',       /* 0Xcb <> */
			'\x20',       /* 0Xcc <> */
			'\x20',       /* 0Xcd <> */
			'\x20',       /* 0Xce <> */
			'\x20',       /* 0Xcf <> */
			'\x20',       /* 0Xd0 <> */
			'\x5d',       /* 0Xd1 <> LATIN CAPITAL LETTER N WITH TILDE */
			'\x20',       /* 0Xd2 <> */
			'\x20',       /* 0Xd3 <> */
			'\x20',       /* 0Xd4 <> */
			'\x20',       /* 0Xd5 <> */
			'\x5c',       /* 0Xd6 <> LATIN CAPITAL LETTER O WITH DIAERESIS */
			'\x20',       /* 0Xd7 <> */
			'\x0b',       /* 0Xd8 <> LATIN CAPITAL LETTER O WITH STROKE */
			'\x20',       /* 0Xd9 <> */
			'\x20',       /* 0Xda <> */
			'\x20',       /* 0Xdb <> */
			'\x5e',       /* 0Xdc <> LATIN CAPITAL LETTER U WITH DIAERESIS */
			'\x20',       /* 0Xdd <> */
			'\x20',       /* 0Xde <> */
			'\x1e',       /* 0Xdf <> LATIN SMALL LETTER SHARP S */
			'\x7f',       /* 0Xe0 <> LATIN SMALL LETTER A WITH GRAVE */
			'\x20',       /* 0Xe1 <> */
			'\x20',       /* 0Xe2 <> */
			'\x20',       /* 0Xe3 <> */
			'\x7b',       /* 0Xe4 <> LATIN SMALL LETTER A WITH DIAERESIS */
			'\x0f',       /* 0Xe5 <> LATIN SMALL LETTER A WITH RING ABOVE */
			'\x1d',       /* 0Xe6 <> LATIN SMALL LETTER AE */
			'\x09',       /* 0Xe7 <> LATIN SMALL LETTER C WITH CEDILLA */
			'\x04',       /* 0Xe8 <> LATIN SMALL LETTER E WITH GRAVE */
			'\x05',       /* 0Xe9 <> LATIN SMALL LETTER E WITH ACUTE */
			'\x20',       /* 0Xea <> */
			'\x20',       /* 0Xeb <> */
			'\x07',       /* 0Xec <> LATIN SMALL LETTER I WITH GRAVE */
			'\x20',       /* 0Xed <> */
			'\x20',       /* 0Xee <> */
			'\x20',       /* 0Xef <> */
			'\x20',       /* 0Xf0 <> */
			'\x7d',       /* 0Xf1 <> LATIN SMALL LETTER N WITH TILDE */
			'\x08',       /* 0Xf2 <> LATIN SMALL LETTER O WITH GRAVE */
			'\x20',       /* 0Xf3 <> */
			'\x20',       /* 0Xf4 <> */
			'\x20',       /* 0Xf5 <> */
			'\x7c',       /* 0Xf6 <> LATIN SMALL LETTER O WITH DIAERESIS */
			'\x20',       /* 0Xf7 <> */
			'\x0c',       /* 0Xf8 <> LATIN SMALL LETTER O WITH STROKE */
			'\x06',       /* 0Xf9 <> LATIN SMALL LETTER U WITH GRAVE */
			'\x20',       /* 0Xfa <> */
			'\x20',       /* 0Xfb <> */
			'\x7e',       /* 0Xfc <> LATIN SMALL LETTER U WITH DIAERESIS */
			'\x20',       /* 0Xfd <> */
			'\x20',       /* 0Xfe <> */
			'\x20'        /* 0Xff <> */
		};

		/*
			Convert an ASCII character to the equivalent character in the SMS
			character set specified in ???.
		*/
		static char ascii2sms(char c)
		{
			return(atoemi_tab[c]);
		}  

		static string atoemi(string src)
		{
			int src_len;
			int di;
			int si;

			src_len = src.Length;
			StringBuilder dst = new StringBuilder();

			for (di = 0, si = 0; si < src_len; si++, di += 2) 
			{
				dst.AppendFormat("{0:X2}", (int)ascii2sms(src[si]));
			}

			return dst.ToString();
		} 

		static int pdu_get_smsc_length(string str)
		{
			if(null == str) return(-1);

			int length;
			string addr_type = str.Substring(2);

			//if(0 == strncmp(str, "00", 2))
			if(str.Substring(0, 2) == "00") return(0);

			//strncpy(lenstr, str, 2);
			//length = (size_t)strtol(lenstr, NULL, 16);
			length = int.Parse(str.Substring(0, 2), NumberStyles.AllowHexSpecifier);
			/* Subtract count of address type field */
			length--;
			/* Check for fill digit */
			if(str[length + 2] == 'F')
				length = 2 * length - 1;
			else
				length = 2 * length;

			//if(0 == strncmp(addr_type, PDU_TOA_INTERNATIONAL_TELEPHONE, 2))
			if(addr_type.Substring(0, 2) == PDU_TOA_INTERNATIONAL_TELEPHONE)
				/* International format -- add 1 for leading '+' */
				length++;

			return(length);
		} /* end_pdu_get_smsc_length() */

		static string pdu_find_sender_length(string src)
		{
			string lenstr;
			int length;

			if(null == src) return null;
			/* Find out length of SMSC information. */
			//			strncpy(lenstr, src, 2);
			lenstr = src.Substring(0, 2);
			//length = (size_t)strtol(lenstr, NULL, 16);
			length = int.Parse(lenstr, NumberStyles.AllowHexSpecifier);
			/* Length means semi-octets which need 2 bytes, each */
			length = 2 * length;
			/* Length field of SMSC information also takes two bytes */
			length += 2;
			/* Sender length is behind first octet of SMS deliver string */
			length += 2;
			//return(src + length);
			return src.Substring(length);
		} /* end pdu_find_sender_length() */

		static int pdu_get_sender_length(string str)
		{
			string lenstr;
			int length;
			string addr_type;
			string ptr;

			if(null == str) return(-1);
			ptr = pdu_find_sender_length(str);
			//strncpy(lenstr, ptr, 2);
			lenstr = ptr.Substring(0, 2);
			addr_type = ptr.Substring(2);
			//length = (size_t)strtol(lenstr, NULL, 16);
			length = int.Parse(lenstr, NumberStyles.AllowHexSpecifier);
			/* Check message format */
			//if(0 == strncmp(addr_type, PDU_TOA_INTERNATIONAL_TELEPHONE, 2))
			if(addr_type.Substring(0, 2) == PDU_TOA_INTERNATIONAL_TELEPHONE)
				/* International format -- add 1 for leading '+' */
				length ++;
			return(length);
		} /* end pdu_get_sender_length() */

		static int pdu_get_text_length(string str)
		{
			int length;
			string lenstr;
			string ptr;

			if(null == str) return(-1);
			ptr = pdu_find_sender_length(str);
			/* ptr now points to length of sender address */
			//			strncpy(lenstr, ptr, 2);
			lenstr = ptr.Substring(0, 2);
			//length = (size_t)strtol(lenstr, NULL, 16);
			length = int.Parse(lenstr, NumberStyles.AllowHexSpecifier);
			if(1 == (length & 0x01)) length++;
			ptr = ptr + length + 4  + 2 + 2 + 14;
			//    ptr = ptr + length + 4  + 2 + 2 + 2;
			//strncpy(lenstr, ptr, 2);
			lenstr = ptr.Substring(0, 2);
			//length = (size_t)strtol(lenstr, NULL, 16);
			length = int.Parse(lenstr, NumberStyles.AllowHexSpecifier);
			return(length);
		} /* pdu_get_text_length() */

		static int pdu_unpack_septets(out StringBuilder dst, string src, int n, byte shift = 0)
		{
			string tmp;
			int i;
			//int j;
			byte cur;
			byte pre;
			byte noctets;
			//byte length;
			int k = 0;

			dst = null;
			if((null == src)/* || (null == dst) */)
				return(-1);

			dst = new StringBuilder();
			dst.Length = 2 * n + 1;
			//noctets = (unsigned char)ceil(((double)n * 7) / 8);
			noctets = (byte)Math.Ceiling(((double)n * 7) / 8);

			//strncpy(tmp, src, 2);
			tmp = src.Substring(0, 2);
			//cur = (unsigned char)strtol(tmp, NULL, 16);
			cur = byte.Parse(tmp, NumberStyles.AllowHexSpecifier);
			//*ptr++ = cur & 0x7f;
			dst[k++] = (char)(cur & 0x7f);
			for (i = 2, shift++; i < noctets*2; i += 2, shift++) 
			{
				if(shift == 7) 
				{
					shift = 0;
				}
				/*strncpy(tmp, src + i, 2);
				cur = (byte)strtol(tmp, NULL, 16);*/
				cur = byte.Parse(src.Substring(i, 2), NumberStyles.AllowHexSpecifier);
				/*strncpy(tmp, src + i - 2, 2);
				pre = (unsigned char)strtol(tmp, NULL, 16);*/
				pre = byte.Parse(src.Substring(i - 2, 2), NumberStyles.AllowHexSpecifier);

				if(0 != shift)
				{
					//					*ptr = (cur << shift);
					//					*ptr &= 0x7f;
					//					*ptr++ |= (pre >> (8 - shift));
					dst[k] = (char)(cur << shift);
					dst[k] &= (char)0x7f;
					dst[k++] |= (char)(pre >> (8 - shift));
				}
				else
				{
					//					*ptr++ = (pre >> 1);
					//					*ptr = cur & 0x7f;
					//					*ptr++ &= 0x7f;
					dst[k++] = (char)(pre >> 1);
					dst[k] = (char)(cur & 0x7f);
					dst[k++] &= (char)0x7f;
				}
			}

			if(shift == 7) 
			{
				/*strncpy(tmp, src + i - 2, 2);
				pre = (unsigned char)strtol(tmp, NULL, 16);*/
				pre = byte.Parse(src.Substring(i - 2, 2), NumberStyles.AllowHexSpecifier);
				//*ptr = pre >> 1;
				dst[k] = (char)(pre >> 1);
				/*
				Check, if the last octet was a real one or just a filler.
				*/
				/*if(*ptr != 0x00)
					ptr++;*/
				if(dst[k] != 0x00) k++;
			}

			//*ptr = '\0';
			dst.Length = k;

			#region moved to pdu_decode

			//			/*
			//			Transform message text from SMS alphabet to ASCII.
			//			*/
			//			//length = ptr - dst;
			//			length = (byte)dst.Length;
			//
			//			for (i = 0, j = 0; i < length; i++, j++) {
			//				dst[j] = sms2ascii(dst[i]);
			//				/*
			//				If sms2ascii returns \x00, we should ignore that character,
			//				because sms2ascii is going to resolve an escape sequence.
			//				*/
			//				if('\x00' == dst[j])
			//					j -= 1;
			//			}
			//
			//			if(j != (int)i) {
			//				dst[j] = '\x00';
			//			}

			#endregion moved to pdu_decode

			return(0);
		} /* end pdu_unpack_septets() */

		static string pdu_find_text_length(string src)
		{
			string ptr;
			string lenstr;
			int length;

			if(null == src) return(null);
			/* Point to sender length */
			ptr = pdu_find_sender_length(src);
			/*strncpy(lenstr, ptr, 2);
			length = (size_t)strtol(lenstr, NULL, 16);*/
			lenstr = ptr.Substring(0, 2);
			length = int.Parse(lenstr, NumberStyles.AllowHexSpecifier);
			if(1 == (length & 0x01)) length++;
			/* Point to type of sender address */
			//			ptr += 2;
			/* Point to sender address */
			//			ptr += 2;
			/* Point to protocol identifier (TP-PID) beyond sender address */
			//			ptr += length;
			/* Point to data coding scheme (TP-DCS) */
			//			ptr += 2;
			/* Point to time stamp (TP-SCTS) */
			//			ptr += 2;
			/* Point to text length (TP-UDL) */
			//			ptr += 14;
			//    ptr += 2;
			//			return(ptr);
			return ptr.Substring(length + 22);
		} /* end pdu_find_text_length() */

		static string pdu_decode_semi_octets(string src, int n)
		{
			if(n < 1) return "";
			
			int i;
			StringBuilder dst = new StringBuilder();
			dst.Length = n;
			for (i = 0; i < n; i += 2) 
			{
				dst[i + 1] = src[i];
				dst[i] = src[i + 1];
			}
			/*if(dst[i - 1] == 'F') dst[i - 1] = '\0';
			else dst[i] = '\0';*/
			if(dst[i - 1] == 'F') dst.Length = i - 1;
			else dst.Length = i;
			return dst.ToString();
		} /* end pdu_encode_semi_octets() */

		static int pdu_decode_smsc(out StringBuilder dst, string src)
		{
			int length;

			dst = null;
			if(null == src)
				return(-1);

			dst = new StringBuilder();
			//strncpy(lenstr, src, 2);
			//length = (size_t)strtol(lenstr, NULL, 16);
			length = 2 * int.Parse(src.Substring(0, 2), NumberStyles.AllowHexSpecifier);
			if(0 == length) 
			{
				//strncpy(dst, "0", 2);
				dst.Append("0");
				return(0);
			}

			length -= 2;
			//if(0 == strncmp(src + 2, PDU_TOA_INTERNATIONAL_TELEPHONE, 2)) {
			if(src.Substring(2, 2) == PDU_TOA_INTERNATIONAL_TELEPHONE) 
			{
				/* International address format -- precede result with '+' */
				dst.Append("+" + pdu_decode_semi_octets(src.Substring(4), length));
			} 
			else 
			{
				dst.Append(pdu_decode_semi_octets(src.Substring(4), length));
			}
			return 0;
		} /* end pdu_decode_smsc() */

		static int pdu_decode_sender(out string dst, string src)
		{
			int length;
			string ptr;

			dst = null;
			if(null == src) return(-1);

			StringBuilder sb = new StringBuilder();
			ptr = pdu_find_sender_length(src);
			if(ptr == null) return(-1);

			/*			strncpy(lenstr, ptr, 2);
						length = (size_t)strtol(lenstr, NULL, 16);*/
			//lenstr = ptr.Substring(0, 2);
			length = int.Parse(ptr.Substring(0, 2), NumberStyles.AllowHexSpecifier);

			if(1 == (length & 0x01)) length++;
			if(length < 1) return 0;

			if(ptr.Substring(2, 2) == PDU_TOA_INTERNATIONAL_TELEPHONE) 
			{
				/* International address format -- precede result with '+' */
				sb.Append("+" + pdu_decode_semi_octets(ptr.Substring(4), length));
			} 
			else 
			{
				sb.Append(pdu_decode_semi_octets(ptr.Substring(4), length));
			}
			dst = sb.ToString();
			return 0;
		} /* end pdu_decode_sender() */

		
		static Encoding encASCII = Encoding.ASCII;
		static Encoding encBEUnicode = Encoding.BigEndianUnicode;
		
		/// <summary>
		/// decode PDU text
		/// </summary>
		/// <param name="pdu">pdu</param>
		/// <param name="sender">sender phone</param>
		/// <param name="text">message as text</param>
		/// <param name="data">message as bytes</param>
		/// <returns>0 - success, else - fail</returns>
		public static int pdu_decode(string pdu, out string sender, out string text, 
			out byte[] data)
		{
			int result;

			text = null;
			data = null;
			/*result = pdu_decode_smsc(out smsc, pdu);
			if(0 != result) return(result);
*/
			//StringBuilder recipient = new StringBuilder(pdu_get_sender_length(pdu) + 1);
			result = pdu_decode_sender(out sender, pdu);
			if(0 != result) return(result);
			
			StringBuilder sb;
			bool compound;
			switch (DCS(pdu, out compound))
			{
				case DCSType.GSM:
					var ptr = pdu_find_text_length(pdu);
					var dataLength = int.Parse(ptr.Substring(0, 2), NumberStyles.AllowHexSpecifier);
					ptr = ptr.Substring(2);
					if (compound)
					{
						// 1byte - header length
						var headerLength = int.Parse(ptr.Substring(0, 2), NumberStyles.AllowHexSpecifier) + 1;
						ptr = ptr.Substring(headerLength*2);
						var symbolsInHeader = (byte) Math.Ceiling((double) (headerLength*8)/7);
						dataLength = dataLength - symbolsInHeader;
						var shift = (byte) (headerLength%7);
						pdu_unpack_septets(out sb, ptr, dataLength, shift);
					}
					else
					{
						pdu_unpack_septets(out sb, ptr, dataLength);
					}

					data = encASCII.GetBytes(sb.ToString());

					/*
					Transform message text from SMS alphabet to ASCII.
					*/
					int j = 0, length = sb.Length;
					for (int i = 0; i < length; ++i, ++j)
					{
						sb[j] = sms2ascii(sb[i]);
						/*
						If sms2ascii returns \x00, we should ignore that character,
						because sms2ascii is going to resolve an escape sequence.
						*/
						if ('\x00' == sb[j])
							j -= 1;
					}

					if (j != length)
					{
						//msg[j] = '\x00';
						sb.Length = j;
					}
					text = sb.ToString();

					break;

				case DCSType.UCS2:
					var pduText = pdu_find_text_length(pdu);

					if (!compound)
					{
						int len = int.Parse(pduText.Substring(0, 2), NumberStyles.AllowHexSpecifier);
						var pduMessageText = pduText.Substring(2, 2 * len);

						sb = new StringBuilder(len / 2);
						for (int i = 0, cnt = len / 2; i < cnt; ++i)
						{
							sb.Append((char)(byte.Parse(pduMessageText.Substring(4 * i, 2),
														NumberStyles.AllowHexSpecifier) << 8 |
											 byte.Parse(pduMessageText.Substring(4 * i + 2, 2),
														NumberStyles.AllowHexSpecifier)));
						}
						text = sb.ToString();
					}
					else
					{
						int messageTextBytesCount = int.Parse(pduText.Substring(0, 2), NumberStyles.AllowHexSpecifier);
						var udhl = int.Parse(pduText.Substring(2, 2), NumberStyles.AllowHexSpecifier);
						var messageBytesHexString = pduText.Substring(2, 2 * messageTextBytesCount);
						var textUnicodeBytes = new List<byte>(messageTextBytesCount);

						for (var i = udhl + 1; i < messageTextBytesCount; ++i)
							textUnicodeBytes.Add(GetByteFromHexString(messageBytesHexString, 2*i));

						text = Encoding.BigEndianUnicode.GetString(textUnicodeBytes.ToArray());
					}
					break;

				default:
					return -1;
			}


			return(result);
		} /* end pdu_decode() */

		private static byte GetByteFromHexString(string s, int index)
		{
			var h = s[index];
			var l = s[index + 1];
			return (byte) (HexDigitToByte(h) << 4 | HexDigitToByte(l));
		}

		private static byte HexDigitToByte(char c)
		{
			if ('0' <= c && c <= '9')
				return (byte) (c - '0');
			if ('A' <= c && c <= 'F')
				return (byte) (c - 'A' + 10);
			if ('a' <= c && c <= 'f')
				return (byte)(c - 'a' + 10);
			throw new ArgumentOutOfRangeException("c", c, "Value is not hex digit");
		}

		static int pdu_encode_semi_octets(out StringBuilder dst, string src)
		{
			int i;
			int srclen = src.Length;

			dst = null;
			if(null == src)
				return(-1);
			
			dst = new StringBuilder();
			dst.Length += srclen + (1 & srclen);
			/*
			Encode semi octets.
			Each two digits have to be swapped.
			Fill with 'F' if odd number of digits.
			*/
			if(1 == (srclen & 0x01)) 
			{
				/* End of odd digits */
				dst[srclen - 1] = 'F';
				dst[srclen] = src[srclen - 1];
			} 
			for (i = 0; i < (srclen & ~1); i += 2)
			{
				dst[i] = src[i + 1];
				dst[i + 1] = src[i];
			}
			return(0);
		} /* end pdu_encode_semi_octets() */
		/*
		  Pack septets into octets and store them in dst.

		  Packing goes as follows:

		  Source characters are assumed to be 7-bit ascii characters,
		  therefore one bit of each source octet is free to be used.

		  Each resulting octet is formed of a septet left filled with the low
		  order bits of the following septet.  The pattern restarts after each
		  seven built octets.  The maximum of 160 characters can be put into
		  140 octets.

		  In the following example, the septets are called a,b...j and the
		  bits are numbered 6543210 from left to right, i.e. high to low
		  order.

		  Source                   Result

		  0a6a5a4a3a2a1a0          b0a6a5a4a3a2a1a0
		  0b6b5b4b3b2b1b0          c1c0b6b5b4b3b2b1
		  0c6c5c4c3c2c1c0          d2d1d0c6c5c4c3c2

		  ...

		  0g6g5g4g3g2g1g0          h6h5h4h3h2h1h0g6
		  0h6h5h4h3h2h1h0          j0i6i5i4i3i2i1i0  <- pattern restarts
		  0i6i5i4i3i2i1i0                               (1 byte win)

		  ...  */
		static int pdu_pack_septets(out StringBuilder dst, string src)
		{
			int i;
			int shift;
			byte octet;
			int srclen = src.Length;

			dst = null;
			if(null == src || srclen == 0)
				return(-1);

			dst = new StringBuilder((srclen + 1) * 7 / 8 * 2);

			for (i = 0, shift = 0; i < srclen - 1; i++) 
			{
				if(shift == 7) 
				{
					shift = 0;
					continue;
				}
				octet = (byte)((src[i] >> shift) | (src[i+1] << (7 - shift)));
				//				_snprintf(dst + strlen(dst),
				//						dstlen - strlen(dst),
				//						"%02X",
				//						octet);
				dst.AppendFormat("{0:X2}", octet);
				shift += 1;
			}
			if(shift < 7) 
			{
				octet = (byte)((src[i] & 0x7f) >> (shift));
				//				_snprintf(dst + strlen(dst),
				//						dstlen - strlen(dst),
				//						"%02X",
				//						octet);
				dst.AppendFormat("{0:X2}", octet);
			}
			return(0);
		} /* end pdu_pack_septets() */

		public static int pdu_encode_smsc(out string smsc, string src)
		{
			int result;
			byte srclen;
			byte noctets;

			if(null == src || long.Parse(src) == 0) 
			{
				/* No SMSC number. Set length to 0 to use phone's settings. */
				smsc = "00";
				return(0);
			}

			StringBuilder dst = new StringBuilder();
			srclen = (byte)src.Length;
			if(src[0] == '+')
				srclen -= 1;

			noctets = (byte)((1 == (srclen & 0x01) ? (srclen + 1) >> 1: srclen >> 1) + 1);
			/*
			Length of SMSC information means number of octets (including type!)
			This does NOT mean 'number of digits'!
			*/
			dst.AppendFormat("{0:X2}", noctets);

			StringBuilder sb;
			if(src[0] == '+') 
			{
				/* International number format */
				dst.Append(PDU_TOA_INTERNATIONAL_TELEPHONE);
				result = pdu_encode_semi_octets(out sb, src.Substring(1));
			} 
			else 
			{
				/* Unknown format */
				dst.Append(PDU_TOA_UNKNOWN_TELEPHONE);
				result = pdu_encode_semi_octets(out sb, src);
			}
			if(0 == result) smsc = dst.Append(sb).ToString();
			else smsc = null;
			return(result);
		} /* end pdu_encode_smsc() */
		/*
		  Encode sender or recipient address.
		  Each semi-octet is two bytes long.
		*/
		static int pdu_encode_address(out StringBuilder dst, string src)
		{
			int result;
			byte srclen;
			byte noctets;

			dst = null;
			if((null == src)/* || (null == dst)*/) return(-1);

			dst = new StringBuilder();
			srclen = (byte)src.Length;
			if(src[0] == '+') srclen -= 1;

			noctets = srclen;
			/* Number of address digits. */
			//			_snprintf(dst + strlen(dst), dstlen, "%02X", noctets);
			dst.AppendFormat("{0:X2}", noctets);

			StringBuilder sb;// = new StringBuilder();
			if(src[0] == '+') 
			{
				/* International number format */
				//strncat(dst, PDU_TOA_INTERNATIONAL_TELEPHONE, dstlen);
				dst.Append(PDU_TOA_INTERNATIONAL_TELEPHONE);
				//result = pdu_encode_semi_octets(dst + dst.Length, src + 1, dst.Length - dstlen);
				result = pdu_encode_semi_octets(out sb, src.Substring(1));
			} 
			else 
			{
				/* Unknown format */
				//strncat(dst, PDU_TOA_UNKNOWN_TELEPHONE, dstlen);
				dst.Append(PDU_TOA_UNKNOWN_TELEPHONE);
				//result = pdu_encode_semi_octets(dst + dst.Length, src, dst.Length - dstlen);
				result = pdu_encode_semi_octets(out sb, src);
			}
			if(0 == result) dst.Append(sb);
			else dst = null;
			return(result);
		} /* end pdu_encode_address() */

		/// <summary>
		/// message max len: GSM, long GSM, UCS, long UCS
		/// </summary>
		static int iGSMlen = 160, iLongGSMlen = 153, iUCSlen = 70, iLongUCSlen = 67;
		
		/// <summary>
		/// encode message to pdu format
		/// </summary>
		/// <param name="SMSC">sms center phone</param>
		/// <param name="target">target phone</param>
		/// <param name="data">data</param>
		/// <param name="istext">text or data</param>
		/// <remarks>if data no alphabet (ASCII -> sms) conversion</remarks>
		/// <param name="msgs">output pdu message</param>
		/// <returns>0 - success</returns>
		public static int pdu_encode(string SMSC, string target, string data, bool istext, 
			out string[] msgs)
		{
			// res code
			int result;
			// init out params
			msgs = null;
			// put input data in sb
			StringBuilder sms_coded_text = new StringBuilder();
			// coded octets of parts of long message
			StringBuilder[] coded_parts;

			bool ucs = false;
			// msg max len
			int maxlen;
			int datalen = data.Length;
			// long msg
			bool multi = false;
			if(istext)
			{
				try
				{
					sms_coded_text.EnsureCapacity(data.Length);
					for(int i = 0; i < datalen; ++i) 
						sms_coded_text.Append(ascii2sms(data[i]));
					if(datalen > iGSMlen) maxlen = iLongGSMlen;
					else maxlen = iGSMlen;
				}
				catch(IndexOutOfRangeException) // conversion impossible. send as ucs
				{
					ucs = true;
					sms_coded_text.Length = 0;
					byte[] chs = encBEUnicode.GetBytes(data);
					sms_coded_text.EnsureCapacity(chs.Length);
					for(int i = 0, cnt = chs.Length; i < cnt; ++i)
						sms_coded_text.AppendFormat("{0:X2}", chs[i]);
					if(datalen > iUCSlen) maxlen = iLongUCSlen;
					else maxlen = iUCSlen;
				}
				// fill out param. len == number of parts
				msgs = new string[(datalen + maxlen - 1) / maxlen];
				// coded parts
				coded_parts = new StringBuilder[msgs.Length];
				if(msgs.Length > 1) // long
				{
					multi = true;
					int cnt = msgs.Length;
					// part header: 050003 + random no, same for parts
					string hdr = "050003" + new Random().Next(255).ToString("X2") + 
						cnt.ToString("X2");
					for(int i = 0; i < cnt; ++i)
					{
						// add sequence no of part to header
						string fullhdr = hdr + (i + 1).ToString("X2");
						if(ucs)
						{
							// fill parts
							coded_parts[i] = new StringBuilder(fullhdr + 
								sms_coded_text.ToString(i * maxlen * 4, 4 * ((i + 1) * maxlen >
								datalen? datalen % maxlen: maxlen)));
							// fill out param with strings of appropriate len
							if(i == cnt - 1) 
								msgs[i] = new string('0', 6 + datalen % maxlen * 2);
							else msgs[i] = new string('0', 6 + maxlen * 2);
						}
						else
						{
							// pack septets and fill parts
							// header should not be packed
							// other hand text must be readable if phone not support long sms
							// so add 7 zeros before pack and remove 6 '0' after
							int part_len;
							if(i == cnt - 1) part_len = datalen % maxlen;
							else part_len = maxlen;
							if(0 != pdu_pack_septets(out coded_parts[i], new string('\0', 7) + 
								sms_coded_text.ToString(i * maxlen, part_len))) return -1;
							coded_parts[i].Remove(0, 12).Insert(0, fullhdr);
							msgs[i] = new string('0', 7 + part_len);
						}
					}
				}
				else	// short
				{
					if(ucs)
					{
						coded_parts[0] = sms_coded_text;
						msgs[0] = new string('0', datalen * 2);
					}
					else
					{
						if(0 != pdu_pack_septets(out coded_parts[0], sms_coded_text.ToString())) return -1;
						msgs[0] = data;
					}
				}
			}
			else
			{
				coded_parts = new StringBuilder[1];
				if(0 != pdu_pack_septets(out coded_parts[0], data)) return -1;
				msgs = new string[]{data};
			}
			
			string smsc;
			result = pdu_encode_smsc(out smsc, SMSC);
			if(0 != result) return result;

			StringBuilder pdu = new StringBuilder(smsc, 512);
			//strncat(tmp, "11", 512);
			if(multi) pdu.Append("51");
			else pdu.Append("11");	// SMS-SUBMIT
			/* Message reference */
			//strncat(tmp, "0C", 512);
			pdu.Append("0C");
			/* Recipient address */
			StringBuilder sb;
			result = pdu_encode_address(out sb, target);
			pdu.Append(sb);
			if(0 != result) return(-1);
			
			/* Protocol identifier (TP-PID) */
			//strncat(tmp, "00", 512);
			pdu.Append("00");
			/* Data coding scheme (TP-DCS) */
			//strncat(tmp, "00", 512);
			if(ucs) pdu.Append("08");
			else pdu.Append("00");
			/* Set validity period to four days */
			//strncat(tmp, "A9", 512);
			pdu.Append("A9");
			/* Pack the message text septets */
			/*pdu_pack_septets(tmp + tmp.Length, sms_coded_text, tmp.MaxCapacity - tmp.Length, 
				sms_text_length);*/
			for(int i = 0; i < msgs.Length; ++i)
				/* Message length in octets */
				//_snprintf(tmp + strlen(tmp), sizeof(tmp) - strlen(tmp), "%02X", strlen(_msg));
				msgs[i] = pdu + msgs[i].Length.ToString("X2") + coded_parts[i];
			//    strncpy(buffer, tmp, n);
			return(0);
		} /* end pdu_encode() */
		
		/// <summary>
		/// pdu data coding scheme
		/// </summary>
		public enum DCSType
		{
			GSM   = 0,
			Eight = 4,
			UCS2  = 8,
		}
		
		[Flags]
			public enum PDUType
		{
			Deliver    = 0x0,
			Submit     = 0x1,
			DataHeader = 0x40
		}

		/// <summary>
		/// get DCS for specified pdu
		/// </summary>
		/// <param name="pdu">pdu string</param>
		/// <returns>pdu dcs</returns>
		public static DCSType DCS(string pdu, out bool compound)
		{
			/* Find out length of SMSC information in semi-octets */
			int length = 2*int.Parse(pdu.Substring(0, 2), NumberStyles.AllowHexSpecifier);
			/* Length field of SMSC information also takes two bytes */
			length += 2;
			var pduTypeField = int.Parse(pdu.Substring(length, 2), NumberStyles.AllowHexSpecifier);
			compound = (pduTypeField & 0x40) != 0;
			switch ((PDUType) (pduTypeField & 3))
			{
				case PDUType.Deliver:
					/* Point to sender length */
					string ptr = pdu_find_sender_length(pdu);
					length = int.Parse(ptr.Substring(0, 2), NumberStyles.AllowHexSpecifier);
					if (1 == (length & 0x01)) length++;
					/* Point to type of sender address */
					//			ptr += 2;
					/* Point to sender address */
					//			ptr += 2;
					/* Point to protocol identifier (TP-PID) beyond sender address */
					//			ptr += length;
					/* Point to data coding scheme (TP-DCS) */
					//			ptr += 2;
					int dcs = int.Parse(ptr.Substring(length + 6, 2), NumberStyles.AllowHexSpecifier);

					return (DCSType) (dcs & 0xC);

				case PDUType.Submit:
					throw new NotImplementedException("SMS-Submit check not implemented");

				default:
					throw new ArgumentException("Unknown sms direction");
			}
		}
	}
}