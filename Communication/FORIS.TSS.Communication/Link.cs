﻿using System;

namespace FORIS.TSS.Communication
{
	/// <summary> Представление объекта соединения (поток?) </summary>
	[Serializable]
	public abstract class Link : ILink
	{
		#region ILink Members

		public virtual int Read(byte[] data)
		{
			throw new NotImplementedException("Read operation not implemented");
		}
		public virtual int Read(char[] data)
		{
			throw new NotImplementedException("Read operation not implemented");
		}
		public virtual void Write(byte[] data)
		{
			throw new NotImplementedException("Write operation not implemented");
		}
		public virtual void Write(string data)
		{
			throw new NotImplementedException("Write operation not implemented");
		}
		public virtual bool Valid
		{
			get
			{
				return false;
			}
		}
		public event DataEventHandler Data;

		#endregion

		protected virtual void OnData(DataEventArgs e)
		{
			Data?.Invoke(this, e);
		}
	}
}