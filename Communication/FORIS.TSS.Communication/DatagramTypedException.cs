﻿using System;

namespace FORIS.TSS.Communication
{
	/// <summary>
	/// datagram type exception
	/// </summary>
	public class DatagramTypedException : DatagramException
	{
		public Type DatagramType;

		public DatagramTypedException(string message, Type type) : base(message)
		{
			DatagramType = type;
		}
	}
}