/*
 * EasySMPP - SMPP protocol library for fast and easy
 * SMSC(Short Message Service Centre) client development
 * even for non-telecom guys.
 * 
 * Easy to use classes covers all needed functionality
 * for SMS applications developers and Content Providers.
 * 
 * Written for .NET 2.0 in C#
 * 
 * Copyright (C) 2006 Balan Andrei, http://balan.name
 * 
 * Licensed under the terms of the GNU Lesser General Public License:
 * 		http://www.opensource.org/licenses/lgpl-license.php
 * 
 * For further information visit:
 * 		http://easysmpp.sf.net/
 * 
 * 
 * "Support Open Source software. What about a donation today?"
 *
 * 
 * File Name: KernelParameters.cs
 * 
 * File Authors:
 * 		Balan Name, http://balan.name
 */
using System;

namespace EasySMPP
{
    public class KernelParameters
    {
        public static readonly int MaxBufferSize = 1048576; // 1MB

        public static readonly int MaxPduSize = 131072;

        public static readonly int ReconnectTimeout = 90000; // miliseconds

        public static readonly int WaitPacketResponse = 30; //seconds

        public static readonly int CanBeDisconnected = 180; //seconds - BETTER TO BE MORE THAN TryToReconnectTimeOut

        public static readonly int NationalNumberLength = 8;

        public static readonly int MaxUndeliverableMessages = 10;

        public static int AskDeliveryReceipt = 0; //NoReceipt = 0;

        public static readonly bool SplitLongText = true;

        public static readonly bool UseEnquireLink = false;

        public static readonly int EnquireLinkTimeout = 45000; //miliseconds
    }
}
