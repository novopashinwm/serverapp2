﻿namespace EasySMPP
{
    public enum MessageStates
    {
        /// <summary>
        /// Сообщение находится в пути.
        /// </summary>
        ENROUTE = 1,

        /// <summary>
        /// Сообщение доставлено адресату.
        /// </summary>
        DELIVERED = 2,

        /// <summary>
        /// Истек период доступности сообщения.
        /// </summary>
        EXPIRED = 3,

        /// <summary>
        /// Сообщение было удалено.
        /// </summary>
        DELETED = 4,

        /// <summary>
        /// Сообщение является недоставляемым.
        /// </summary>
        UNDELIVERABLE = 5,

        /// <summary>
        /// Сообщение находится в принятом состоянии (т.е. читалось вручную от имени абонента клиентской службой)
        /// </summary>
        ACCEPTED = 6,

        /// <summary>
        /// Сообщение находится в недопустимом состоянии.
        /// </summary>
        UNKNOWN = 7,

        /// <summary>
        /// Сообщение находится в отклоненном состоянии.
        /// </summary>
        REJECTED = 8
    }
}
