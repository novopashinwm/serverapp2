/*
 * EasySMPP - SMPP protocol library for fast and easy
 * SMSC(Short Message Service Centre) client development
 * even for non-telecom guys.
 * 
 * Easy to use classes covers all needed functionality
 * for SMS applications developers and Content Providers.
 * 
 * Written for .NET 2.0 in C#
 * 
 * Copyright (C) 2006 Balan Andrei, http://balan.name
 * 
 * Licensed under the terms of the GNU Lesser General Public License:
 * 		http://www.opensource.org/licenses/lgpl-license.php
 * 
 * For further information visit:
 * 		http://easysmpp.sf.net/
 * 
 * 
 * "Support Open Source software. What about a donation today?"
 *
 * 
 * File Name: SMPPClient.cs
 * 
 * File Authors:
 * 		Balan Name, http://balan.name
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace EasySMPP
{
    /// <summary>
    /// Client for SMPP 3.4 protocol
    /// </summary>
    public class SMPPClient : IDisposable
    {

        #region Private variables

        private TcpClient _tcpClient;
        private Stream _stream;

        private int _connectionState;

        private DateTime _enquireLinkSendTime;
        private DateTime _enquireLinkResponseTime;
        private DateTime _lastSeenConnected;
        private DateTime _lastPacketSentTime;

        private Timer _enquireLinkTimer;
        private int _undeliveredMessages;

        private readonly SMSCArray _smscArray = new SMSCArray();

        private byte[] _mbResponse = new byte[KernelParameters.MaxBufferSize];
        private int _mPos;
        private int _mLen;

        private bool _mustBeConnected;

        private byte _askDeliveryReceipt = (byte) KernelParameters.AskDeliveryReceipt;
        private bool _splitLongText = KernelParameters.SplitLongText;
        private int _nationalNumberLength = KernelParameters.NationalNumberLength;
        private bool _useEnquireLink = KernelParameters.UseEnquireLink;
        private int _enquireLinkTimeout = KernelParameters.EnquireLinkTimeout;
        private int _reconnectTimeout = KernelParameters.ReconnectTimeout;

        private readonly SortedList _sarMessages = SortedList.Synchronized(new SortedList());
        #endregion Private variables

        private int _currentMask;
        private int _messageId;
        private int _originalMessageId;
        private readonly List<SubmitSmMessageStatus> _messagePool = new List<SubmitSmMessageStatus>();

        public List<SubmitSmMessageStatus> MessagePool
        {
            get { return _messagePool; }
        }

        public int CurrentSentMask
        {
            get
            {
                return -1;
            }
        }

        public void SetCurrentMask(int sentMask)
        {
            _currentMask = sentMask;
        }

        private int GetCurrentMask()
        {
            return _currentMask;
        }

        #region Public Functions

        public SMPPClient()
        {
            LogLevel = 0;
            _connectionState = ConnectionStates.SMPP_SOCKET_DISCONNECTED;

            _enquireLinkSendTime = DateTime.Now;
            _enquireLinkResponseTime = _enquireLinkSendTime.AddSeconds(1);
            _lastSeenConnected = DateTime.Now;
            _lastPacketSentTime = DateTime.MaxValue;

            _mustBeConnected = false;

            TimerCallback timerDelegate = CheckSystemIntegrity;
            _enquireLinkTimer = new Timer(timerDelegate, null, _enquireLinkTimeout, _enquireLinkTimeout);

        }//SMPPClient

        public void Connect()
        {
            _mustBeConnected = true;
            ConnectToSMSC();
        }//connectToSMSC

        public void Disconnect()
        {
            try
            {
                _mustBeConnected = false;
                UnBind();
            }
            catch (Exception ex)
            {
                LogMessage(LogLevels.LogExceptions, "DisconnectFromSMSC | " + ex);
            }
        }//DisconnectFromSMSC

        public void ClearSMSC()
        {
            _smscArray.Clear();
        }//ClearSMSC

        public void AddSMSC(SMSC mSMSC)
        {
            _smscArray.AddSMSC(mSMSC);
        }//AddSMSC

        #region Send Functions

        public void Send(byte[] data, int n)
        {
            try
            {
                _lastPacketSentTime = DateTime.Now;
                LogMessage(LogLevels.LogPdu, "Sending PDU : " + Tools.ConvertArrayToHexString(data, n));
                _stream.BeginWrite(data, 0, n, SendCallback, _stream);
            }
            catch (Exception ex)
            {
                LogMessage(LogLevels.LogExceptions, "Send | " + ex);
            }
        }//Send

        public int SubmitSM(byte sourceAddressTon, byte sourceAddressNpi, string sourceAddress,
                            byte destinationAddressTon, byte destinationAddressNpi, string destinationAddress,
                            byte esmClass, byte protocolId, byte priorityFlag,
                            DateTime sheduleDeliveryTime, DateTime validityPeriod, byte registeredDelivery,
                            byte replaceIfPresentFlag, byte dataCoding, byte smDefaultMsgId,
                            byte[] message, Int16? sarMsgRefNum, byte? sarTotalSegments, byte? sarSegmentSeqnum)
        {
            try
            {
                var submitSMPdu = new byte[KernelParameters.MaxPduSize];

                // Start filling PDU						

                // Mandatory parameters

                Tools.CopyIntToArray(0x00000004, submitSMPdu, 4);
                var sequenceNumber = _smscArray.currentSMSC.SequenceNumber;
                Tools.CopyIntToArray(sequenceNumber, submitSMPdu, 12);
                var pos = 16;
                submitSMPdu[pos] = 0x00; //service_type
                pos += 1;
                submitSMPdu[pos] = sourceAddressTon;
                pos += 1;
                submitSMPdu[pos] = sourceAddressNpi;
                pos += 1;
                var sourceAddr = Tools.ConvertStringToByteArray(Tools.GetString(sourceAddress, 20, ""));
                Array.Copy(sourceAddr, 0, submitSMPdu, pos, sourceAddr.Length);
                pos += sourceAddr.Length;
                submitSMPdu[pos] = 0x00;
                pos += 1;
                submitSMPdu[pos] = destinationAddressTon;
                pos += 1;
                submitSMPdu[pos] = destinationAddressNpi;
                pos += 1;
                var destinationAddr = Tools.ConvertStringToByteArray(Tools.GetString(destinationAddress, 20, ""));
                Array.Copy(destinationAddr, 0, submitSMPdu, pos, destinationAddr.Length);
                pos += destinationAddr.Length;
                submitSMPdu[pos] = 0x00;
                pos += 1;
                submitSMPdu[pos] = esmClass;
                pos += 1;
                submitSMPdu[pos] = protocolId;
                pos += 1;
                submitSMPdu[pos] = priorityFlag;
                pos += 1;
                var sheduleDeliveryTimeBytes = Tools.ConvertStringToByteArray(Tools.GetDateString(sheduleDeliveryTime));
                Array.Copy(sheduleDeliveryTimeBytes, 0, submitSMPdu, pos, sheduleDeliveryTimeBytes.Length);
                pos += sheduleDeliveryTimeBytes.Length;
                submitSMPdu[pos] = 0x00;
                pos += 1;
                var validityPeriodBytes = Tools.ConvertStringToByteArray(Tools.GetDateString(validityPeriod));
                Array.Copy(validityPeriodBytes, 0, submitSMPdu, pos, validityPeriodBytes.Length);
                pos += validityPeriodBytes.Length;
                submitSMPdu[pos] = 0x00;
                pos += 1;
                submitSMPdu[pos] = registeredDelivery;
                pos += 1;
                submitSMPdu[pos] = replaceIfPresentFlag;
                pos += 1;
                submitSMPdu[pos] = dataCoding;
                pos += 1;
                submitSMPdu[pos] = smDefaultMsgId;
                pos += 1;

                // Message text.
                var smLength = message.Length > 254 ? (byte)254 : (byte)message.Length;

                submitSMPdu[pos] = smLength;
                pos += 1;
                Array.Copy(message, 0, submitSMPdu, pos, smLength);
                pos += smLength;

                // Optional parameters

                if (sarMsgRefNum.HasValue && sarTotalSegments.HasValue && sarSegmentSeqnum.HasValue)
                {
                    LogMessage(LogLevels.LogDebug, string.Format("MessageId {0}, Segments Count {1}, Segments part {2}",
                                                                 sarMsgRefNum.Value, sarTotalSegments.Value,
                                                                 sarSegmentSeqnum.Value));

                    // sar_msg_ref_num tag
                    Tools.CopySmallIntToArray(0x020C, submitSMPdu, pos);
                    pos += 2;

                    // sar_msg_ref_num length
                    Tools.CopySmallIntToArray(0x02, submitSMPdu, pos);
                    pos += 2;

                    // sar_msg_ref_num value
                    Tools.CopySmallIntToArray(sarMsgRefNum.Value, submitSMPdu, pos);
                    pos += 2;

                    // sarTotalSegments tag
                    Tools.CopySmallIntToArray(0x020E, submitSMPdu, pos);
                    pos += 2;

                    // sarTotalSegments length
                    Tools.CopySmallIntToArray(0x01, submitSMPdu, pos);
                    pos += 2;

                    // sarTotalSegments value
                    submitSMPdu[pos] = sarTotalSegments.Value;
                    pos += 1;

                    // sarSegmentSeqnum tag
                    Tools.CopySmallIntToArray(0x020F, submitSMPdu, pos);
                    pos += 2;

                    // sarSegmentSeqnum length
                    Tools.CopySmallIntToArray(0x01, submitSMPdu, pos);
                    pos += 2;

                    // sarSegmentSeqnum value
                    submitSMPdu[pos] = sarSegmentSeqnum.Value;
                    pos += 1;

                }

                Tools.CopyIntToArray(pos, submitSMPdu, 0);

                Send(submitSMPdu, pos);
                _undeliveredMessages++;
                return sequenceNumber;
            }
            catch (Exception ex)
            {
                LogMessage(LogLevels.LogExceptions, "SubmitSM | " + ex);
            }
            return -1;

        }//SubmitSM

        public int DataSM(byte sourceAddressTon, byte sourceAddressNpi, string sourceAddress,
                            byte destinationAddressTon, byte destinationAddressNpi, string destinationAddress,
                            byte esmClass,
                            byte registeredDelivery,
                            byte dataCoding,
                            byte[] data)
        {
            try
            {
                var dataSMPdu = new byte[KernelParameters.MaxPduSize];

                // Start filling PDU						

                Tools.CopyIntToArray(0x00000103, dataSMPdu, 4);
                var sequenceNumber = _smscArray.currentSMSC.SequenceNumber;
                Tools.CopyIntToArray(sequenceNumber, dataSMPdu, 12);
                var pos = 16;
                dataSMPdu[pos] = 0x00; //service_type
                pos += 1;
                dataSMPdu[pos] = sourceAddressTon;
                pos += 1;
                dataSMPdu[pos] = sourceAddressNpi;
                pos += 1;
                var sourceAddr = Tools.ConvertStringToByteArray(Tools.GetString(sourceAddress, 20, ""));
                Array.Copy(sourceAddr, 0, dataSMPdu, pos, sourceAddr.Length);
                pos += sourceAddr.Length;
                dataSMPdu[pos] = 0x00;
                pos += 1;
                dataSMPdu[pos] = destinationAddressTon;
                pos += 1;
                dataSMPdu[pos] = destinationAddressNpi;
                pos += 1;
                var destinationAddr = Tools.ConvertStringToByteArray(Tools.GetString(destinationAddress, 20, ""));
                Array.Copy(destinationAddr, 0, dataSMPdu, pos, destinationAddr.Length);
                pos += destinationAddr.Length;
                dataSMPdu[pos] = 0x00;
                pos += 1;
                dataSMPdu[pos] = esmClass;
                pos += 1;
                dataSMPdu[pos] = registeredDelivery;
                pos += 1;
                dataSMPdu[pos] = dataCoding;
                pos += 1;

                dataSMPdu[pos] = 0x04;
                pos += 1;
                dataSMPdu[pos] = 0x24;
                pos += 1;

                var smLength = data.Length > 32767 ? (Int16)32767 : (Int16)data.Length;

                dataSMPdu[pos] = BitConverter.GetBytes(smLength)[1];
                pos += 1;
                dataSMPdu[pos] = BitConverter.GetBytes(smLength)[0];
                pos += 1;
                Array.Copy(data, 0, dataSMPdu, pos, smLength);
                pos += smLength;

                Tools.CopyIntToArray(pos, dataSMPdu, 0);

                Send(dataSMPdu, pos);
                _undeliveredMessages++;
                return sequenceNumber;
            }
            catch (Exception ex)
            {
                //
                LogMessage(LogLevels.LogExceptions, "DataSM | " + ex);
            }
            return -1;

        }//DataSM

        public int SendSms(String from, String to, String text, int id, ref int? sentMask)
        {
            if (sentMask.HasValue)
                SetCurrentMask(sentMask.Value);
            else
                SetCurrentMask(0);

            //TODO: ���������� ������� ����� ��� �������� ������ ���������

            _messageId = id & 0x00FF;
            _originalMessageId = id;

            var result = SendSms(from, to, _splitLongText, text, _askDeliveryReceipt);
            sentMask = GetCurrentMask();
            return result;
        }//SendSms
        public int SendSms(String from, String to, String text)
        {
            return SendSms(from, to, _splitLongText, text, _askDeliveryReceipt);
        }//SendSms
        public int SendSms(String from, String to, String text, byte askDeliveryReceipt)
        {
            return SendSms(from, to, _splitLongText, text, askDeliveryReceipt);
        }//SendSms
        public int SendSms(String from, String to, bool splitLongText, String text)
        {
            return SendSms(from, to, splitLongText, text, _askDeliveryReceipt);
        }//SendSms
        public int SendSms(String from, String to, bool splitLongText, String text, byte askDeliveryReceipt)
        {
            return SendSms(from, to, splitLongText, text, askDeliveryReceipt, 0, Tools.GetDataCoding(text));
        }

        public int SendSms(String from, String to, bool splitLongText, String text, byte askDeliveryReceipt, byte esmClass, byte dataCoding)
        {
            byte sourceAddressNpi;
            string sourceAddress;
            byte destinationAddressTon;
            byte destinationAddressNpi;
            string destinationAddress;
            byte registeredDelivery;
            byte maxLength;

            sourceAddress = Tools.GetString(from, 20, "");
            var sourceAddressTon = GetAddrTon(sourceAddress);
            sourceAddressNpi = GetAddrNpi(sourceAddress);

            destinationAddress = Tools.GetString(to, 20, "");
            destinationAddressTon = GetAddrTon(destinationAddress);
            destinationAddressNpi = GetAddrNpi(destinationAddress);

            registeredDelivery = askDeliveryReceipt;

            maxLength = (byte) (dataCoding == 8 ? 67 : 150);

            LogMessage(LogLevels.LogDebug, string.Format("Text length: {0}", text.Length));

            byte priorityFlag;
            int smsPartsCount;
            DateTime sheduleDeliveryTime;
            DateTime validityPeriod;
            byte replaceIfPresentFlag;
            byte smDefaultMsgId;
            byte[] message;
            string smsText = text;

            byte protocolId = 0;
            priorityFlag = PriorityFlags.Normal;
            sheduleDeliveryTime = DateTime.MinValue;
            validityPeriod = DateTime.MinValue;
            replaceIfPresentFlag = ReplaceIfPresentFlags.DoNotReplace;
            smDefaultMsgId = 0;

            var iterator = 0;

            if ((text.Length > maxLength) && (splitLongText))
            {
                while (smsText.Length > 0)
                {
                    var subPart = smsText.Substring(0, smsText.Length > maxLength
                                                           ? maxLength
                                                           : smsText.Length);
                    message = dataCoding == 8
                                  ? Encoding.BigEndianUnicode.GetBytes(subPart)
                                  : Encoding.ASCII.GetBytes(subPart);
                    
                    LogMessage(LogLevels.LogWarnings,
                               dataCoding == 8
                                   ? Encoding.BigEndianUnicode.GetString(message, 0, message.Length)
                                   : Encoding.ASCII.GetString(message, 0, message.Length));

                    smsText = smsText.Remove(0,
                                             smsText.Length > maxLength
                                                 ? maxLength
                                                 : smsText.Length);

                    var sarMsgRefNum = (short?) (_messageId & 0x00FF);
                    var sarTotalSegments = (byte?) (text.Length/maxLength + 1);
                    var sarSegmentSeqnum = (byte?) (iterator + 1);

                    var currentSmsPart = new SubmitSmMessageStatus
                                             {
                                                 Added = DateTime.UtcNow,
                                                 Status = SubmitSmStatus.Ready,
                                                 MessageId = sarMsgRefNum,
                                                 SegmentId = sarSegmentSeqnum,
                                                 OriginalMessageId = _originalMessageId,
                                                 From = from,
                                                 To = to
                                             };

                    var isPartSent = (GetCurrentMask() & (1 << iterator)) != 0;

                    if (!_messagePool.Exists(x => x.MessageId == sarMsgRefNum && x.SegmentId == sarSegmentSeqnum))
                        _messagePool.Add(currentSmsPart);

                    if (isPartSent)
                    {
                        currentSmsPart.Status = SubmitSmStatus.Sent;
                        iterator++;
                        continue;
                    }

                    if (
                        _messagePool.Exists(
                            x =>
                            x.MessageId == sarMsgRefNum && x.SegmentId == sarSegmentSeqnum &&
                            x.Status != SubmitSmStatus.Sent))
                    {
                        LogMessage(LogLevels.LogDebug,
                                   string.Format("type: submitSm, Ton {0}, Npi {1}", destinationAddressTon,
                                                 destinationAddressNpi));
                        try
                        {
                            currentSmsPart.SequenceId = SubmitSM(sourceAddressTon, sourceAddressNpi, sourceAddress,
                                                                 destinationAddressTon, destinationAddressNpi,
                                                                 destinationAddress,
                                                                 esmClass, protocolId, priorityFlag,
                                                                 sheduleDeliveryTime, validityPeriod, registeredDelivery,
                                                                 replaceIfPresentFlag,
                                                                 dataCoding, smDefaultMsgId, message,
                                                                 sarMsgRefNum, sarTotalSegments, sarSegmentSeqnum
                                );
                            currentSmsPart.Status = SubmitSmStatus.Pushed;
                        }
                        catch (Exception ex)
                        {
                            LogMessage(LogLevels.LogErrors, ex.ToString());
                            currentSmsPart.Status = SubmitSmStatus.Failed;
                        }
                    }

                    iterator++;
                }

                smsPartsCount = iterator;
            }
            else
            {
                smsPartsCount = 1;
                message = dataCoding == 8
                              ? Encoding.BigEndianUnicode.GetBytes(smsText)
                              : Encoding.ASCII.GetBytes(smsText);

                var currentSmsPart = new SubmitSmMessageStatus
                {
                    Added = DateTime.UtcNow,
                    Status = SubmitSmStatus.Ready,
                    MessageId = (short?) (_messageId & 0x00FF),
                    OriginalMessageId = _originalMessageId,
                    From = from,
                    To = to
                };

                if (!_messagePool.Exists(x => x.MessageId == currentSmsPart.MessageId))
                    _messagePool.Add(currentSmsPart);

                try
                {
                    currentSmsPart.SequenceId = SubmitSM(
                        sourceAddressTon, sourceAddressNpi, sourceAddress,
                             destinationAddressTon, destinationAddressNpi,
                             destinationAddress,
                             esmClass, protocolId, priorityFlag,
                             sheduleDeliveryTime, validityPeriod, registeredDelivery,
                             replaceIfPresentFlag,
                             dataCoding, smDefaultMsgId, message,
                             null, null, null);

                    currentSmsPart.Status = SubmitSmStatus.Pushed;
                }
                catch (Exception ex)
                {
                    LogMessage(LogLevels.LogErrors, ex.ToString());
                    currentSmsPart.Status = SubmitSmStatus.Failed;
                }
            }
            return smsPartsCount;
        }

        //SendSms
        public int SendFlashSms(String from, String to, String text)
        {
            return SendFlashSms(from, to, text, _askDeliveryReceipt);
        }//SendFlashSms
        public int SendFlashSms(String from, String to, String text, byte askDeliveryReceipt)
        {
            return SendSms(from, to, false, text.Substring(0, text.Length > 160 ? 160 : text.Length), askDeliveryReceipt, 0, 0x10);
        }//SendFlashSms
        public int SendMedia(String from, String to, byte[] media)
        {
            return SendMedia(from, to, media, _askDeliveryReceipt);
        }//SendMedia
        public int SendMedia(String from, String to, byte[] media, byte askDeliveryReceipt)
        {
            return SendData(from, to, media, 0x40, 0xF5, askDeliveryReceipt);
        }//SendMedia
        public int SendData(String from, String to, byte[] data)
        {
            return SendData(from, to, data, _askDeliveryReceipt);
        }//SendData
        public int SendData(String from, String to, byte[] data, byte askDeliveryReceipt)
        {
            return SendData(from, to, data, 0, 0, 0);
        }//SendData
        public int SendData(String from, String to, byte[] data, byte esmClass, byte dataCoding, byte askDeliveryReceipt)
        {
            byte sourceAddressTon;
            byte sourceAddressNpi;
            string sourceAddress;
            byte destinationAddressTon;
            byte destinationAddressNpi;
            string destinationAddress;
            byte registeredDelivery;

            sourceAddress = Tools.GetString(from, 20, "");
            sourceAddressTon = GetAddrTon(sourceAddress);
            sourceAddressNpi = GetAddrNpi(sourceAddress);

            destinationAddress = Tools.GetString(from, 20, "");
            destinationAddressTon = GetAddrTon(destinationAddress);
            destinationAddressNpi = GetAddrNpi(destinationAddress);

            registeredDelivery = askDeliveryReceipt;

            var messageId = DataSM(sourceAddressTon, sourceAddressNpi, sourceAddress,
                destinationAddressTon, destinationAddressNpi, destinationAddress,
                esmClass, registeredDelivery, dataCoding, data);

            return messageId;
        }//SendData
        #endregion New Send Functions

        #endregion Public Functions

        #region Properties
        public bool CanSend
        {
            get
            {
                try
                {
                    if ((_connectionState == ConnectionStates.SMPP_BINDED) && (_undeliveredMessages <= KernelParameters.MaxUndeliverableMessages))
                        return true;
                }
                catch (Exception ex)
                {
                    LogMessage(LogLevels.LogExceptions, "CanSend | " + ex);
                }
                return false;
            }
        }//CanSend

        public int LogLevel { get; set; } //CanSend


        public byte AskDeliveryReceipt
        {
            get
            {
                return _askDeliveryReceipt;
            }
            set
            {
                if (value < 3)
                    _askDeliveryReceipt = value;
            }
        }//AskDeliveryReceipt

        public bool SplitLongText
        {
            get
            {
                return _splitLongText;
            }
            set
            {
                _splitLongText = value;
            }
        }//SplitLongText
        public int NationalNumberLength
        {
            get
            {
                return _nationalNumberLength;
            }
            set
            {
                if (value <= 12)
                    _nationalNumberLength = value;
            }
        }//NationalNumberLength
        public bool UseEnquireLink
        {
            get
            {
                return _useEnquireLink;
            }
            set
            {
                _useEnquireLink = value;
            }
        }//UseEnquireLink
        public int EnquireLinkTimeout
        {
            get
            {
                return _enquireLinkTimeout;
            }
            set
            {
                if (value > 1000)
                    _enquireLinkTimeout = value;
            }
        }//EnquireLinkTimeout
        public int ReconnectTimeout
        {
            get
            {
                return _reconnectTimeout;
            }
            set
            {
                if (value > 1000)
                    _reconnectTimeout = value;
            }
        }//ReconnectTimeout
        public bool TestModeOn
        {
            get
            {
                if (_smscArray.HasItems)
                    return _smscArray.currentSMSC.TestModeOn;
                throw new Exception("SMSC Array must contain at least one SMSC Item.");
            }
        }

        #endregion Properties

        #region Events

        public event SubmitSmRespEventHandler OnSubmitSmResp;

        public event DeliverSmEventHandler OnDeliverSm;

        public event LogEventHandler OnLog;

        #endregion Events

        #region Private functions
        private void ConnectToSMSC()
        {
            try
            {
                if (!_smscArray.HasItems)
                {
                    LogMessage(LogLevels.LogErrors, "Connect | ERROR #1011 : No SMSC defined. Please ddd SMSC definition first.");
                    return;
                }
                InitClientParameters();

                //  Create a TCP/IP  socket.
                //Try to disconnect if connected
                TryToDisconnect();
                
                LogMessage(LogLevels.LogSteps, "Trying to connect to " + _smscArray.currentSMSC.Description + "[" + _smscArray.currentSMSC.Host + ":" + _smscArray.currentSMSC.Port + "]");

                _tcpClient = new TcpClient(_smscArray.currentSMSC.Host, _smscArray.currentSMSC.Port);

                if (_smscArray.currentSMSC.SslCertificate == null)
                {
                    _stream = _tcpClient.GetStream();
                }
                else
                {
                    var sslStream = new SslStream(_tcpClient.GetStream(), true, 
                        delegate { return true; },
                        delegate { return _smscArray.currentSMSC.SslCertificate; });
                    sslStream.AuthenticateAsClient(_smscArray.currentSMSC.Host);
                    _stream = sslStream;
                }

                _connectionState = ConnectionStates.SMPP_SOCKET_CONNECT_SENT;
                //TODO: _tcpClient.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, 1);
                _lastSeenConnected = DateTime.Now;
                _connectionState = ConnectionStates.SMPP_SOCKET_CONNECTED;
                LogMessage(LogLevels.LogSteps, "Connected");
                Bind();
            }
            catch (Exception ex)
            {
                LogMessage(LogLevels.LogExceptions, "connectToSMSC | " + ex);
                TryToDisconnect();
                throw;
            }

        }

//connectToSMSC

        private void TryToDisconnect()
        {
            //TODO: add lock
            try
            {
                if (_stream != null)
                {
                    try
                    {
                        _stream.Close();
                    }
                    catch (Exception ex)
                    {
                        LogMessage(LogLevels.LogExceptions, "tryToDisconnect | _stream.Close();" + ex);
                    }
                    _stream = null;
                }

                if (_tcpClient != null)
                {
                    try
                    {
                        _tcpClient.Close();
                    }
                    catch (Exception ex)
                    {
                        LogMessage(LogLevels.LogExceptions, "tryToDisconnect | _tcpClient.Close();" + ex);
                    }
                    _tcpClient = null;
                }
            }
            catch (Exception ex)
            {
                LogMessage(LogLevels.LogExceptions, "tryToDisconnect | " + ex);
            }
            LogMessage(LogLevels.LogSteps, "Disconnected");
            _connectionState = ConnectionStates.SMPP_SOCKET_DISCONNECTED;
        }//tryToDisconnect

        private void Receive()
        {
            // Create the state object.
            StateObject state = new StateObject();
            state.WorkStream = _stream;

            // Begin receiving the data from the remote device.
            var bytesReaded = _stream.Read(state.Buffer, 0, state.Buffer.Length);

            Process(state, bytesReaded);
        }

        private void Bind()
        {
            byte[] bindPdu = new byte[1024];
            int i, n;

            var pos = 7;
            bindPdu[pos] = BindType.BIND_TRANSCEIVER;

            pos = 12;
            Tools.CopyIntToArray(_smscArray.currentSMSC.SequenceNumber, bindPdu, pos);
            pos = 15;

            pos++;
            n = _smscArray.currentSMSC.SystemId.Length;
            for (i = 0; i < n; i++, pos++)
                bindPdu[pos] = (byte)_smscArray.currentSMSC.SystemId[i];
            bindPdu[pos] = 0;

            pos++;
            n = _smscArray.currentSMSC.Password.Length;
            for (i = 0; i < n; i++, pos++)
                bindPdu[pos] = (byte)_smscArray.currentSMSC.Password[i];
            bindPdu[pos] = 0;

            pos++;
            n = _smscArray.currentSMSC.SystemType.Length;
            for (i = 0; i < n; i++, pos++)
                bindPdu[pos] = (byte)_smscArray.currentSMSC.SystemType[i];
            bindPdu[pos] = 0;

            bindPdu[++pos] = 0x34; //interface version
            bindPdu[++pos] = _smscArray.currentSMSC.AddrTon; //addr_ton
            bindPdu[++pos] = _smscArray.currentSMSC.AddrNpi; //addr_npi

            //address_range
            pos++;
            n = _smscArray.currentSMSC.AddressRange.Length;
            for (i = 0; i < n; i++, pos++)
                bindPdu[pos] = (byte)_smscArray.currentSMSC.AddressRange[i];
            bindPdu[pos] = 0x00;

            pos++;
            bindPdu[3] = Convert.ToByte(pos & 0x00FF);
            bindPdu[2] = Convert.ToByte((pos >> 8) & 0x00FF);

            // Begin sending the data to the remote device.
            LogMessage(LogLevels.LogSteps, "BindSent");
            Send(bindPdu, pos);
            _connectionState = ConnectionStates.SMPP_BIND_SENT;
            Receive();
        }//bind

        private void UnBind()
        {
            if (_connectionState == ConnectionStates.SMPP_BINDED)
            {
                try
                {
                    byte[] pdu = new byte[16];

                    Tools.CopyIntToArray(16, pdu, 0);

                    Tools.CopyIntToArray(0x00000006, pdu, 4);

                    Tools.CopyIntToArray(_smscArray.currentSMSC.SequenceNumber, pdu, 12);

                    LogMessage(LogLevels.LogSteps, "Unbind sent.");
                    _connectionState = ConnectionStates.SMPP_UNBIND_SENT;

                    Send(pdu, 16);
                }
                catch (Exception ex)
                {
                    LogMessage(LogLevels.LogExceptions, "unBind | " + ex);
                }
            }

        }//unBind


        private void ProcessSubmitSmResp(SubmitSmRespEventArgs e)
        {
            try
            {
                _undeliveredMessages--;

                if (OnSubmitSmResp != null)
                {
                    OnSubmitSmResp(e);
                }
            }
            catch (Exception ex)
            {
                LogMessage(LogLevels.LogExceptions, "processSubmitSmResp | " + ex);
            }

        }//processSubmitSmResp

        private void ProcessDeliverSm(DeliverSmEventArgs e)
        {
            try
            {
                if (OnDeliverSm != null)
                {
                    OnDeliverSm(e);
                }

                SendDeliverSmResp(e.SequenceNumber, StatusCodes.ESME_ROK);
            }
            catch (Exception ex)
            {
                LogMessage(LogLevels.LogExceptions, "processDeliverSm | " + ex);
            }
        }//processDeliverSm

        private void ProcessLog(LogEventArgs e)
        {
            try
            {
                if (OnLog != null)
                {
                    OnLog(e);
                }
            }
            catch
            {
                //Unsuccessfull logging is nowhere to log, but it should not crash the system
            }

        }//processLog

        private void LogMessage(int logLevel, string pMessage)
        {
            try
            {
                if ((LogLevel & logLevel) > 0)
                {
                    LogEventArgs evArg = new LogEventArgs(pMessage);
                    ProcessLog(evArg);
                }
            }
            catch
            {
                // DO NOT USE LOG INSIDE LOG FUNCTION !!! logMessage(LogLevels.LogExceptions, "logMessage | " +ex.ToString());
            }
        }//logMessage

        private void TryToReconnect()
        {
            try
            {
                TryToDisconnect();
                Thread.Sleep(_reconnectTimeout);
                if (_mustBeConnected)
                {
                    _smscArray.NextSMSC();
                    ConnectToSMSC();
                }
            }
            catch (Exception ex)
            {
                LogMessage(LogLevels.LogExceptions, "tryToReconnect | " + ex);
            }

        }//tryToReconnect

        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                StateObject state = (StateObject) ar.AsyncState;
                var client = state.WorkStream;
                // Read data from the remote device.
                int bytesRead = client.EndRead(ar);
                Process(state, bytesRead);
            }
            catch (Exception ex)
            {
                LogMessage(LogLevels.LogExceptions, "receiveCallback | " + ex);
                UnBind();
            }
        }

        private void Process(StateObject state, int bytesRead)
        {
            try
            {
                byte[] pduBody = new byte[0];

                // Retrieve the state object and the client socket 
                // from the async state object.
                var client = state.WorkStream;
                // Read data from the remote device.
                LogMessage(LogLevels.LogSteps, "Received " + Tools.ConvertIntToHexString(bytesRead) + " bytes");
                if (bytesRead > 0)
                {
                    // There might be more data, so store the data received so far.
                    if ((LogLevel & LogLevels.LogPdu) > 0)
                        LogMessage(LogLevels.LogPdu, "Received Binary Data " + Tools.ConvertArrayToHexString(state.Buffer, bytesRead));
                    
                    // Begin processing SMPP messages
                    
                    _mLen = _mPos + bytesRead;
                    if (_mLen > KernelParameters.MaxBufferSize)
                    {
                        _mPos = 0;
                        _mLen = 0;
                        _mbResponse = new byte[KernelParameters.MaxBufferSize];
                    }
                    else
                    {
                        Array.Copy(state.Buffer, 0, _mbResponse, _mPos, bytesRead);
                        _mPos = _mLen;
                        var exitFlag = false;
                        var x = 0;
                        while (((_mLen - x) >= 16) && (exitFlag == false))
                        {
                            int commandLength = _mbResponse[x + 0];
                            int i;
                            for (i = x + 1; i < x + 4; i++)
                            {
                                commandLength <<= 8;
                                commandLength = commandLength | _mbResponse[i];
                            }

                            uint commandID = _mbResponse[x + 4];
                            for (i = x + 5; i < x + 8; i++)
                            {
                                commandID <<= 8;
                                commandID = commandID | _mbResponse[i];
                            }

                            int commandStatus = _mbResponse[x + 8];
                            for (i = x + 9; i < x + 12; i++)
                            {
                                commandStatus <<= 8;
                                commandStatus = commandStatus | _mbResponse[i];
                            }

                            int sequenceNumber = _mbResponse[x + 12];
                            for (i = x + 13; i < x + 16; i++)
                            {
                                sequenceNumber <<= 8;
                                sequenceNumber = sequenceNumber | _mbResponse[i];
                            }
                            if ((commandLength <= (_mLen - x)) && (commandLength >= 16))
                            {
                                int bodyLength;
                                if (commandLength == 16)
                                    bodyLength = 0;
                                else
                                {
                                    bodyLength = commandLength - 16;
                                    pduBody = new byte[bodyLength];
                                    Array.Copy(_mbResponse, x + 16, pduBody, 0, bodyLength);
                                }

                                //SMPP Command parsing

                                switch (commandID)
                                {
                                    case 0x80000002:
                                        LogMessage(LogLevels.LogSteps, "Bind_Transmitter_Resp");

                                        if (_connectionState == ConnectionStates.SMPP_BIND_SENT)
                                        {
                                            if (commandStatus == 0)
                                            {
                                                _connectionState = ConnectionStates.SMPP_BINDED;
                                                LogMessage(LogLevels.LogSteps, "SMPP Binded");
                                            }
                                            else
                                            {
                                                LogMessage(LogLevels.LogSteps | LogLevels.LogErrors, "SMPP BIND ERROR : " + Tools.ConvertIntToHexString(commandStatus));
                                                TryToDisconnect();
                                                TryToReconnect();
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            LogMessage(LogLevels.LogSteps | LogLevels.LogWarnings, "ERROR #3011 : Unexpected Bind_Transmitter_Resp");
                                        }

                                        break;
                                    case 0x80000009:
                                        LogMessage(LogLevels.LogSteps, "Bind_Transiver_Resp");

                                        if (_connectionState == ConnectionStates.SMPP_BIND_SENT)
                                        {
                                            if (commandStatus == 0)
                                            {
                                                _connectionState = ConnectionStates.SMPP_BINDED;
                                                LogMessage(LogLevels.LogSteps, "SMPP Binded");
                                            }
                                            else
                                            {
                                                LogMessage(LogLevels.LogSteps | LogLevels.LogErrors, "SMPP BIND ERROR : " + Tools.ConvertIntToHexString(commandStatus));
                                                TryToReconnect();
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            LogMessage(LogLevels.LogSteps | LogLevels.LogWarnings, "ERROR #3011 : Unexpected Bind_Transiver_Resp");
                                        }
                                        break;
                                    case 0x80000004:
                                        LogMessage(LogLevels.LogSteps, "Submit_Sm_Resp");
                                        SubmitSmRespEventArgs evArg = new SubmitSmRespEventArgs(sequenceNumber, commandStatus, Tools.ConvertArrayToString(pduBody, bodyLength - 1));
                                        ProcessSubmitSmResp(evArg);
                                        break;
                                    case 0x80000103:
                                        LogMessage(LogLevels.LogSteps, "Data_Sm_Resp");
                                        evArg = new SubmitSmRespEventArgs(sequenceNumber, commandStatus, Tools.ConvertArrayToString(pduBody, bodyLength - 1));
                                        ProcessSubmitSmResp(evArg);
                                        break;
                                    case 0x80000015:
                                        LogMessage(LogLevels.LogSteps, "Enquire_Link_Resp");
                                        _enquireLinkResponseTime = DateTime.Now;
                                        break;
                                    case 0x00000015:
                                        LogMessage(LogLevels.LogSteps, "Enquire_Link");
                                        SendEnquireLinkResp(sequenceNumber);
                                        break;
                                    case 0x80000006:
                                        LogMessage(LogLevels.LogSteps, "Unbind_Resp");
                                        _connectionState = ConnectionStates.SMPP_UNBINDED;
                                        TryToDisconnect();
                                        break;
                                    case 0x00000005:
                                        LogMessage(LogLevels.LogSteps, "Deliver_Sm");
                                        DecodeAndProcessDeliverSm(sequenceNumber, pduBody, bodyLength);
                                        break;
                                    case 0x00000103:
                                        LogMessage(LogLevels.LogSteps, "Data_Sm");
                                        DecodeAndProcessDataSm(sequenceNumber, pduBody, bodyLength);
                                        break;
                                    default:
                                        SendGenericNack(sequenceNumber, StatusCodes.ESME_RINVCMDID);
                                        LogMessage(LogLevels.LogSteps | LogLevels.LogWarnings, "Unknown SMPP PDU type" + Tools.ConvertUIntToHexString(commandID));
                                        break;
                                }

                                //END SMPP Command parsing

                                if (commandLength == (_mLen - x))
                                {
                                    _mLen = 0;
                                    _mPos = 0;
                                    x = 0;
                                    exitFlag = true;
                                }
                                else
                                {
                                    x += commandLength;
                                }
                            }
                            else
                            {
                                SendGenericNack(sequenceNumber, StatusCodes.ESME_RINVMSGLEN);
                                _mLen -= x;
                                _mPos = _mLen;
                                Array.Copy(_mbResponse, x, _mbResponse, 0, _mLen);
                                exitFlag = true;
                                LogMessage(LogLevels.LogSteps | LogLevels.LogWarnings, "Invalid PDU Length");
                            }
                            if (x < _mLen)
                                LogMessage(LogLevels.LogPdu, "NEXT PDU STEP IN POS " + Convert.ToString(x) + " FROM " + Convert.ToString(_mLen));
                        }
                    }

                    // End processing SMPP messages
                    //  Get the rest of the data.
                    client.BeginRead(state.Buffer, 0, StateObject.BufferSize, ReceiveCallback, state);
                }
                else
                {
                    LogMessage(LogLevels.LogSteps | LogLevels.LogWarnings, "Incoming network buffer from SMSC is empty.");
                    /*					if (client.Poll(0,SelectMode.SelectError)&&client.Poll(0,SelectMode.SelectRead)&&client.Poll(0,SelectMode.SelectWrite))
                                        {
                                            logMessage(LogLevels.LogSteps|LogLevels.LogExceptions, "Socket Error");
                                            unBind();
                                        }
                    */
                    TryToReconnect();
                }
            }

            catch (Exception ex)
            {
                LogMessage(LogLevels.LogExceptions, "receiveCallback | " + ex);
                UnBind();
            }

        }//receiveCallback

        private void InitClientParameters()
        {
            _mbResponse = new byte[KernelParameters.MaxBufferSize];
            _mPos = 0;
            _mLen = 0;

            _enquireLinkResponseTime = DateTime.Now;

            _undeliveredMessages = 0;
        }//initClientParameters

        private void DecodeAndProcessDeliverSm(int sequenceNumber, byte[] body, int length)
        {
            if (length < 17)
            {
                SendDeliverSmResp(sequenceNumber, StatusCodes.ESME_RINVCMDLEN);
                return;
            }
            try
            {
                bool isDeliveryReceipt = false;
                bool isUdhiSet = false;
                byte[] sourceAddr = new byte[21];
                byte[] destAddr = new byte[21];
                byte[] shortMessage = new byte[0];
                byte[] messagePayload = new byte[0];
                short messagePayloadLength;

                // Message Delivery Params
                byte[] receiptedMessageIdBytes = new byte[254];
                byte receiptedMessageIdLen = 0;
                byte messageStateByte = 0;
                UInt16 sarMsgRefNum = 0;
                byte sarTotalSegments = 0;
                byte sarSegmentSeqnum = 0;

                var pos = 0;
                while ((pos < 5) && (body[pos] != 0x00))
                    pos++;
                if (body[pos] != 0x00)
                {
                    SendDeliverSmResp(sequenceNumber, StatusCodes.ESME_RUNKNOWNERR);
                    LogMessage(LogLevels.LogExceptions, "decodeAndProcessDeliverSm returned UNKNOWNERR on 0x01");
                    return;
                }
                ++pos; //_source_addr_ton
                ++pos; //_source_addr_npi
                pos++;
                byte saLength = 0;
                while ((saLength < 20) && (body[pos] != 0x00))
                {
                    sourceAddr[saLength] = body[pos];
                    pos++;
                    saLength++;
                }
                if (body[pos] != 0x00)
                {
                    SendDeliverSmResp(sequenceNumber, StatusCodes.ESME_RUNKNOWNERR);
                    LogMessage(LogLevels.LogExceptions, "decodeAndProcessDeliverSm returned UNKNOWNERR on 0x02");
                    return;
                }
                ++pos; //_dest_addr_ton
                ++pos; //_dest_addr_npi
                pos++;
                byte daLength = 0;
                while ((daLength < 20) && (body[pos] != 0x00))
                {
                    destAddr[daLength] = body[pos];
                    pos++;
                    daLength++;
                }
                if (body[pos] != 0x00)
                {
                    SendDeliverSmResp(sequenceNumber, StatusCodes.ESME_RUNKNOWNERR);
                    LogMessage(LogLevels.LogExceptions, "decodeAndProcessDeliverSm returned UNKNOWNERR on 0x03");
                    return;
                }
                var esmClass = body[++pos];
                switch (esmClass)
                {
                    case 0x00:
                        break;
                    case 0x04:
                        LogMessage(LogLevels.LogSteps, "Delivery Receipt Received");
                        isDeliveryReceipt = true;
                        break;
                    case 0x40:
                        LogMessage(LogLevels.LogSteps, "UDHI Indicator set");
                        isUdhiSet = true;
                        break;
                    default:
                        LogMessage(LogLevels.LogSteps | LogLevels.LogWarnings, "Unknown esm_class for DELIVER_SM : " + Tools.GetHexFromByte(esmClass));
                        break;
                }
                pos += 1;
                ++pos; //_priority_flag
                pos += 4;
                var dataCoding = body[++pos];
                pos += 1;
                int smLength = body[++pos];
                pos += 1;
                if (smLength > 0)
                {
                    shortMessage = new byte[smLength];
                    Array.Copy(body, pos, shortMessage, 0, smLength);
                }

                if ((isDeliveryReceipt) || (isUdhiSet) || smLength == 0)
                {
                    bool exit = false;
                    if (smLength > 0)
                    {
                        pos += smLength;
                    }
                    while ((pos < length) && (exit == false))
                    {
                        int parTag;
                        if (Tools.Get2ByteIntFromArray(body, pos, length, out parTag) == false)
                        {
                            exit = true;
                            break;
                        }
                        pos += 2;
                        int parTagLength;
                        if (Tools.Get2ByteIntFromArray(body, pos, length, out parTagLength) == false)
                        {
                            exit = true;
                            break;
                        }
                        pos += 2;
                        switch (parTag)
                        {
                            case 0x020C:
                                if (((pos + parTagLength - 1) <= length) && (parTagLength == 2))
                                {
                                    byte[] temp = new byte[parTagLength];
                                    Array.Copy(body, pos, temp, 0, parTagLength);
                                    pos += parTagLength;
                                    sarMsgRefNum = (UInt16)((temp[0] << 8) + temp[1]);
                                    LogMessage(LogLevels.LogSteps, "sar_msg_ref_num : " + sarMsgRefNum);
                                }
                                else
                                    exit = true;

                                break;
                            case 0x020E:
                                if ((pos <= length) && (parTagLength == 1))
                                {
                                    sarTotalSegments = body[pos];
                                    LogMessage(LogLevels.LogSteps, "sar_total_segments : " + Convert.ToString(sarTotalSegments));
                                    pos++;
                                }
                                else
                                    exit = true;

                                break;
                            case 0x020F:
                                if ((pos <= length) && (parTagLength == 1))
                                {
                                    sarSegmentSeqnum = body[pos];
                                    LogMessage(LogLevels.LogSteps, "sar_segment_seqnum : " + Convert.ToString(sarSegmentSeqnum));
                                    pos++;
                                }
                                else
                                    exit = true;

                                break;
                            case 0x0427:
                                if ((pos <= length) && (parTagLength == 1))
                                {
                                    messageStateByte = body[pos];
                                    LogMessage(LogLevels.LogSteps, "Message state : " + Convert.ToString(messageStateByte));
                                    pos++;
                                }
                                else
                                    exit = true;

                                break;
                            case 0x001E:
                                if ((pos + parTagLength - 1) <= length)
                                {
                                    receiptedMessageIdBytes = new byte[parTagLength];
                                    Array.Copy(body, pos, receiptedMessageIdBytes, 0, parTagLength);
                                    receiptedMessageIdLen = Convert.ToByte(parTagLength);
                                    pos += parTagLength;
                                    LogMessage(LogLevels.LogSteps, "Delivered message id : " + Tools.ConvertArrayToString(receiptedMessageIdBytes, receiptedMessageIdLen - 1));
                                }
                                else
                                    exit = true;
                                break;
                            //message_payload.
                            case 0x0424:
                                if (((pos + parTagLength - 1) <= length))
                                {
                                    messagePayloadLength = (short) parTagLength;
                                    messagePayload = new byte[messagePayloadLength];
                                    Array.Copy(body, pos, messagePayload, 0, messagePayloadLength);
                                    pos += messagePayloadLength;
                                }
                                else
                                    exit = true;
                                break;
                            default:
                                if ((pos + parTagLength - 1) <= length)
                                    pos += parTagLength;
                                else
                                    exit = true;
                                LogMessage(LogLevels.LogDebug, "_par_tag : " + Convert.ToString(parTag));
                                LogMessage(LogLevels.LogDebug, "_par_tag_length : " + Convert.ToString(parTagLength));
                                break;

                        }
                    }
                    LogMessage(LogLevels.LogDebug, "Delivery Receipt Processing Exit value - " + Convert.ToString(exit));
                    if (exit)
                        isDeliveryReceipt = false;
                }


                if (sarMsgRefNum > 0 && sarSegmentSeqnum > 0 && (sarTotalSegments > 1 || isUdhiSet))
                {
                    lock (_sarMessages.SyncRoot)
                    {
                        var partBytes = smLength > 0 ? shortMessage : messagePayload;

                        SortedList<byte, byte[]> tArr;
                        if (_sarMessages.ContainsKey(sarMsgRefNum))
                        {
                            tArr = (SortedList<byte, byte[]>)_sarMessages[sarMsgRefNum];
                            tArr[sarSegmentSeqnum] = partBytes;

                            if (tArr.Count != sarTotalSegments)
                            {
                                SendDeliverSmResp(sequenceNumber, StatusCodes.ESME_ROK);
                                return;
                            }//if
                            
                            smLength = 0;
                            for (byte i = 1; i <= sarTotalSegments; i++)
                            {
                                smLength += tArr[i].Length;
                            }
                            shortMessage = new byte[smLength];
                            smLength = 0;
                            for (byte i = 1; i <= sarTotalSegments; i++)
                            {
                                var segmentBytes = tArr[i];
                                Array.Copy(segmentBytes, 0, shortMessage, smLength, segmentBytes.Length);
                                smLength += segmentBytes.Length;
                            }
                            _sarMessages.Remove(sarMsgRefNum);
                        }//if
                        else
                        {
                            //TODO: remove records older than one week - parts will never come, but memory never will be released
                            tArr = new SortedList<byte, byte[]>();
                            tArr.Add(sarSegmentSeqnum, partBytes);
                            _sarMessages.Add(sarMsgRefNum, tArr);
                            SendDeliverSmResp(sequenceNumber, StatusCodes.ESME_ROK);
                            return;
                        }
                    }//lock
                }
                string textString;

                string hexString;

                byte messageState = 0;
                string receiptedMessageID = "";

                var to = Encoding.ASCII.GetString(destAddr, 0, daLength);
                var @from = Encoding.ASCII.GetString(sourceAddr, 0, saLength);
                
                // ���  ���������� ��� ��������� � "��������������" ���� message_payload.
                if (smLength == 0)
                {
                    textString = dataCoding == 8
                        ? Encoding.BigEndianUnicode.GetString(messagePayload)
                        : Encoding.UTF8.GetString(messagePayload);
                    hexString = Tools.ConvertArrayToHexString(messagePayload, messagePayload.Length);
                }
                else
                {
                    textString = dataCoding == 8
                        ? Encoding.BigEndianUnicode.GetString(shortMessage, 0, smLength)
                        : Encoding.UTF8.GetString(shortMessage, 0, smLength);
                    hexString = Tools.ConvertArrayToHexString(shortMessage, smLength);
                }
                
                if (isDeliveryReceipt)
                {
                    isDeliveryReceipt = true;
                    messageState = messageStateByte;
                    receiptedMessageID = Encoding.ASCII.GetString(receiptedMessageIdBytes, 0, receiptedMessageIdLen - 1);
                }

                LogMessage(LogLevels.LogDebug,
                    string.Format("sequence_number:{0}\r\n to:{1}\r\n from:{2}\r\n textString:{3}\r\n hexString:{4}\r\n data_coding:{5}\r\n esm_class:{6}",
                        sequenceNumber,
                        to,
                        from,
                        textString,
                        hexString,
                        dataCoding,
                        esmClass
                    ));

                DeliverSmEventArgs evArg = new DeliverSmEventArgs(sequenceNumber, to, from, textString, hexString, dataCoding, esmClass, isDeliveryReceipt, messageState, receiptedMessageID);
                ProcessDeliverSm(evArg);
            }
            catch (Exception ex)
            {
                LogMessage(LogLevels.LogExceptions, "decodeAndProcessDeliverSm | " + ex);
                SendDeliverSmResp(sequenceNumber, StatusCodes.ESME_RUNKNOWNERR);
            }

        }//decodeAndProcessDeliverSm

        private void DecodeAndProcessDataSm(int sequenceNumber, byte[] body, int length)
        {
            if (length < 17)
            {
                SendDeliverSmResp(sequenceNumber, StatusCodes.ESME_RINVCMDLEN);
                return;
            }
            try
            {
                bool isDeliveryReceipt = false;
                byte[] sourceAddr = new byte[21];
                byte[] destAddr = new byte[21];

                // Message Delivery Params

                byte[] receiptedMessageid = new byte[254];
                byte receiptedMessageidLen = 0;
                byte messageStateByte = 0;


                var pos = 0;
                while ((pos < 5) && (body[pos] != 0x00))
                    pos++;
                if (body[pos] != 0x00)
                {
                    SendDeliverSmResp(sequenceNumber, StatusCodes.ESME_RUNKNOWNERR);
                    LogMessage(LogLevels.LogExceptions, "decodeAndProcessDeliverSm returned UNKNOWNERR on 0x04");
                    return;
                }
                ++pos; //_source_addr_ton
                ++pos; //_source_addr_npi
                pos++;
                byte saLength = 0;
                while ((saLength < 20) && (body[pos] != 0x00))
                {
                    sourceAddr[saLength] = body[pos];
                    pos++;
                    saLength++;
                }
                if (body[pos] != 0x00)
                {
                    SendDeliverSmResp(sequenceNumber, StatusCodes.ESME_RUNKNOWNERR);
                    LogMessage(LogLevels.LogExceptions, "decodeAndProcessDeliverSm returned UNKNOWNERR on 0x05");
                    return;
                }
                ++pos; //_dest_addr_ton
                ++pos; //_dest_addr_npi
                pos++;
                byte daLength = 0;
                while ((daLength < 20) && (body[pos] != 0x00))
                {
                    destAddr[daLength] = body[pos];
                    pos++;
                    daLength++;
                }
                if (body[pos] != 0x00)
                {
                    SendDeliverSmResp(sequenceNumber, StatusCodes.ESME_RUNKNOWNERR);
                    LogMessage(LogLevels.LogExceptions, "decodeAndProcessDeliverSm returned UNKNOWNERR on 0x06");
                    return;
                }
                var esmClass = body[++pos];
                switch (esmClass)
                {
                    case 0x00:
                        break;
                    case 0x04:
                        LogMessage(LogLevels.LogSteps, "Delivery Receipt Received");
                        isDeliveryReceipt = true;
                        break;
                    default:
                        LogMessage(LogLevels.LogSteps | LogLevels.LogWarnings, "Unknown esm_class for DATA_SM : " + Tools.GetHexFromByte(esmClass));
                        break;
                }
                pos += 1;
                ++pos; //_priority_flag
                pos += 4;
                var dataCoding = body[++pos];
                pos += 1;
                if (isDeliveryReceipt)
                {
                    bool exit = false;
                    while ((pos < length) && (exit == false))
                    {
                        int parTag;
                        if (Tools.Get2ByteIntFromArray(body, pos, length, out parTag) == false)
                        {
                            exit = true;
                            break;
                        }
                        pos += 2;
                        int parTagLength;
                        if (Tools.Get2ByteIntFromArray(body, pos, length, out parTagLength) == false)
                        {
                            exit = true;
                            break;
                        }
                        pos += 2;
                        switch (parTag)
                        {
                            case 0x0427:
                                if ((pos <= length) && (parTagLength == 1))
                                {
                                    messageStateByte = body[pos];
                                    LogMessage(LogLevels.LogSteps, "Message state : " + Convert.ToString(messageStateByte));
                                    pos++;
                                }
                                else
                                    exit = true;

                                break;
                            case 0x001E:
                                if ((pos + parTagLength - 1) <= length)
                                {
                                    receiptedMessageid = new byte[parTagLength];
                                    Array.Copy(body, pos, receiptedMessageid, 0, parTagLength);
                                    receiptedMessageidLen = Convert.ToByte(parTagLength);
                                    pos += parTagLength;
                                    LogMessage(LogLevels.LogSteps, "Delivered message id : " + Tools.ConvertArrayToString(receiptedMessageid, receiptedMessageidLen - 1));
                                }
                                else
                                    exit = true;
                                break;
                            default:
                                if ((pos + parTagLength - 1) <= length)
                                    pos += parTagLength;
                                else
                                    exit = true;
                                LogMessage(LogLevels.LogDebug, "_par_tag : " + Convert.ToString(parTag));
                                LogMessage(LogLevels.LogDebug, "_par_tag_length : " + Convert.ToString(parTagLength));
                                break;

                        }
                    }
                    LogMessage(LogLevels.LogDebug, "Delivery Receipt Processing Exit value - " + Convert.ToString(exit));
                    if (exit)
                        isDeliveryReceipt = false;
                }

                string from;
                string textString = "";

                string hexString = "";

                byte messageState = 0;
                string receiptedMessageID = "";

                var to = Encoding.ASCII.GetString(destAddr, 0, daLength);
                from = Encoding.ASCII.GetString(sourceAddr, 0, saLength);

                if (isDeliveryReceipt)
                {
                    messageState = messageStateByte;
                    receiptedMessageID = Encoding.ASCII.GetString(receiptedMessageid, 0, receiptedMessageidLen - 1);
                }

                DeliverSmEventArgs evArg = new DeliverSmEventArgs(sequenceNumber, to, from, textString, hexString, dataCoding, esmClass, isDeliveryReceipt, messageState, receiptedMessageID);
                ProcessDeliverSm(evArg);
            }
            catch (Exception ex)
            {
                LogMessage(LogLevels.LogExceptions, "decodeAndProcessDeliverSm | " + ex);
                SendDeliverSmResp(sequenceNumber, StatusCodes.ESME_RUNKNOWNERR);
            }

        }//decodeAndProcessDataSm

        private void CheckSystemIntegrity(Object state)
        {
            try
            {
                if (_mustBeConnected)
                {
                    if (_connectionState == ConnectionStates.SMPP_BINDED)
                    {
                        if (_enquireLinkSendTime <= _enquireLinkResponseTime)
                        {
                            _enquireLinkSendTime = DateTime.Now;
                            SendEnquireLink(_smscArray.currentSMSC.SequenceNumber);
                            _lastSeenConnected = DateTime.Now;
                        }
                        else
                        {
                            LogMessage(LogLevels.LogSteps | LogLevels.LogErrors, "checkSystemIntegrity | ERROR #9001 - no response to Enquire Link");
                            TryToReconnect();
                        }
                    }
                    else
                    {
                        if ((DateTime.Now - _lastSeenConnected).TotalSeconds > KernelParameters.CanBeDisconnected)
                        {
                            LogMessage(LogLevels.LogSteps | LogLevels.LogErrors, "checkSystemIntegrity | ERROR #9002 - diconnected more than " + Convert.ToString(KernelParameters.CanBeDisconnected) + " seconds");
                            _lastSeenConnected = DateTime.Now.AddSeconds(KernelParameters.CanBeDisconnected);
                            TryToReconnect();
                        }
                    }
                }
                else
                {
                    if (_connectionState == ConnectionStates.SMPP_UNBIND_SENT)
                    {
                        if ((DateTime.Now - _lastPacketSentTime).TotalSeconds > KernelParameters.WaitPacketResponse)
                        {
                            TryToDisconnect();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogMessage(LogLevels.LogExceptions, "checkSystemIntegrity | " + ex);
            }

        }//checkSystemIntegrity

        public void SendDeliverSmResp(int sequenceNumber, int commandStatus)
        {
            try
            {
                byte[] pdu = new byte[17];

                Tools.CopyIntToArray(17, pdu, 0);

                Tools.CopyIntToArray(0x80000005, pdu, 4);

                Tools.CopyIntToArray(commandStatus, pdu, 8);

                Tools.CopyIntToArray(sequenceNumber, pdu, 12);

                pdu[16] = 0;

                Send(pdu, 17);

            }
            catch (Exception ex)
            {
                LogMessage(LogLevels.LogExceptions, "sendDeliverSmResp | " + ex);
            }
        }//sendDeliverSmResp

        private void SendGenericNack(int sequenceNumber, int commandStatus)
        {
            try
            {
                byte[] pdu = new byte[16];

                Tools.CopyIntToArray(16, pdu, 0);

                Tools.CopyIntToArray(0x80000000, pdu, 4);

                Tools.CopyIntToArray(commandStatus, pdu, 8);

                Tools.CopyIntToArray(sequenceNumber, pdu, 12);

                Send(pdu, 16);
            }
            catch (Exception ex)
            {
                LogMessage(LogLevels.LogExceptions, "sendGenericNack | " + ex);
            }

        }//sendGenericNack

        private void SendEnquireLink(int sequenceNumber)
        {
            try
            {
                byte[] pdu = new byte[16];
                Tools.CopyIntToArray(16, pdu, 0);

                Tools.CopyIntToArray(0x00000015, pdu, 4);

                Tools.CopyIntToArray(0x00000000, pdu, 8);

                Tools.CopyIntToArray(sequenceNumber, pdu, 12);

                Send(pdu, 16);
            }
            catch (Exception ex)
            {
                LogMessage(LogLevels.LogExceptions, "sendEnquireLink | " + ex);
            }
        }//sendEnquireLink

        private void SendEnquireLinkResp(int sequenceNumber)
        {
            try
            {
                byte[] pdu = new byte[16];
                Tools.CopyIntToArray(16, pdu, 0);

                Tools.CopyIntToArray(0x80000015, pdu, 4);

                Tools.CopyIntToArray(0x00000000, pdu, 8);

                Tools.CopyIntToArray(sequenceNumber, pdu, 12);

                Send(pdu, 16);
            }
            catch (Exception ex)
            {
                LogMessage(LogLevels.LogExceptions, "sendEnquireLink | " + ex);
            }
        }//sendEnquireLink

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Stream client = (Stream)ar.AsyncState;

                // Complete sending the data to the remote device.
                client.EndWrite(ar);
                LogMessage(LogLevels.LogSteps | LogLevels.LogPdu, "Sent some bytes");
            }
            catch (Exception ex)
            {
                LogMessage(LogLevels.LogExceptions, "Send | " + ex);
            }
        }//Send

        private byte GetAddrTon(string address)
        {
            int i;
            for (i = 0; i < address.Length; i++)
                if (!Char.IsDigit(address, i))
                {
                    return AddressTons.Alphanumeric;
                }
            if (address.Length == _nationalNumberLength)
                return AddressTons.National;
            if (address.Length > _nationalNumberLength)
                return AddressTons.International;
            return AddressTons.Unknown;
        }//getAddrTon
        private byte GetAddrNpi(string address)
        {
            int i;
            for (i = 0; i < address.Length; i++)
                if (!Char.IsDigit(address, i))
                {
                    return AddressNpis.Unknown;
                }
            return AddressNpis.ISDN;
        }//getAddrTon
        #endregion Private Functions

        public void Dispose()
        {
            var timer = _enquireLinkTimer;
            if (timer != null)
                timer.Dispose();
            _enquireLinkTimer = null;

            Disconnect();
        }
    }

    public class SubmitSmMessageStatus
    {
        public string From { get; set; }
        public string To { get; set; }
        public int OriginalMessageId { get; set; }
        public short? MessageId { get; set; }
        public int SequenceId { get; set; }
        public byte? SegmentId { get; set; }
        public DateTime Added { get; set; }
        public SubmitSmStatus Status { get; set; }
        public int SendMask { get; set; }
    }

    public enum SubmitSmStatus
    {
        Pushed,
        Failed,
        Sent,
        Ready,
        Throttled,
        MessageQueueFull
    }

    //SMPPClient


}
