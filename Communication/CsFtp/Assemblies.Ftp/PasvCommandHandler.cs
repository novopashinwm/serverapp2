using System.Diagnostics;
using System.Net;

namespace Assemblies.Ftp.FtpCommands
{
	class PasvCommandHandler : FtpCommandHandler
	{
	    private int? _port;

	    public PasvCommandHandler(FtpConnectionObject connectionObject)
			: base("PASV", connectionObject)
		{
			
		}

		protected override string OnProcess(string sMessage)
		{
			if (ConnectionObject.PasvSocket == null)
			{			
                _port = ConnectionObject.GetPassivePort();

                if (_port == null)
                {
                    Trace.TraceWarning("{0}.OnProcess: no free ports", GetType());
                    return GetMessage(550, "There\'s no free ports, try again later or use active mode");
                }

                System.Net.Sockets.TcpListener listener = Assemblies.General.SocketHelpers.CreateTcpListener(IPAddress.Any, _port.Value);

				if (listener == null)
				{
					return GetMessage(550, string.Format("Couldn't start listener on port {0}", _port));
				}

				SendPasvReply();

				listener.Start();

				ConnectionObject.PasvSocket = listener.AcceptTcpClient();
				
				listener.Stop();
				return "";
			}
			else
			{
				SendPasvReply();
				return "";
			}
		}

		private void SendPasvReply()
		{
            if (_port == null)
                return;

			string sIpAddress = ConnectionObject.PassiveIPAddress.ToString();

            var portHighValue = _port.Value / 256;
            var portLowValue = _port.Value % 256;

			sIpAddress = sIpAddress.Replace('.', ',');
			sIpAddress += ',';
			sIpAddress += portHighValue;
			sIpAddress += ',';
			sIpAddress += portLowValue;
			Assemblies.General.SocketHelpers.Send(ConnectionObject.Socket, string.Format("227 ={0}\r\n", sIpAddress));
		}
	}
}
