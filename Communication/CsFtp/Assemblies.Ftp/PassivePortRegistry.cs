﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assemblies.Ftp
{
    class PassivePortRegistry
    {
        private const int MaxPortNumber = 65534;
        private const int MinPortNumber = 0;
        private readonly Stack<int> _availablePorts;

        public PassivePortRegistry(int from, int to)
        {
            if (from < MinPortNumber)
                throw new ArgumentOutOfRangeException("from", from, "Port number could not be less than " + MinPortNumber);

            if (MaxPortNumber < to)
                throw new ArgumentOutOfRangeException("to", to, "Port number could not be greater than " + MaxPortNumber);

            if (to < from)
                throw new ArgumentOutOfRangeException("from", from, "Port number 'from' could not be greater than port number 'to' (" + to + ")");

            _availablePorts = new Stack<int>();
            for (var portNumber = from; portNumber <= to; ++portNumber)
                _availablePorts.Push(portNumber);
        }

        public PassivePort CreatePassivePort()
        {
            if (_availablePorts.Count == 0)
                return null;

            lock (_availablePorts)
            {
                if (_availablePorts.Count == 0)
                    return null;
                return new PassivePort(this);
            }
        }

        private int SeizePort()
        {
            return _availablePorts.Pop();
        }

        private void ReleasePort(int value)
        {
            lock (_availablePorts)
                _availablePorts.Push(value);
        }

        public class PassivePort : IDisposable
        {
            private readonly int _value;
            private readonly PassivePortRegistry _registry;
            private bool _disposed;

            public PassivePort(PassivePortRegistry registry)
            {
                if (registry == null)
                    throw new ArgumentNullException("registry");

                _registry = registry;
                _value = _registry.SeizePort();
            }

            public int Value
            {
                get { return _value; }
            }

            public void Dispose()
            {
                if (_disposed)
                    return;
                _disposed = true;
                _registry.ReleasePort(_value);
            }
        }
    }
}
