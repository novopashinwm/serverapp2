using System;
using System.Diagnostics;
using System.Net;
using System.Threading;
using Assemblies.Ftp.FileSystem;

namespace Assemblies.Ftp
{
	/// <summary>
	/// Contains the socket read functionality. Works on its own thread since all socket operation is blocking.
	/// </summary>
	class FtpSocketHandler : IDisposable
	{
		#region Member Variables

		private System.Net.Sockets.TcpClient m_theSocket = null;
		private System.Threading.Thread m_theThread = null;
		private int m_nId = 0;
		private const int m_nBufferSize = 65536;
		private FtpConnectionObject m_theCommands = null;
		private readonly FileSystem.IFileSystemClassFactory m_fileSystemClassFactory = null;
	    private readonly FtpServer _ftpServer;

	    #endregion

		#region Events
		
		public delegate void CloseHandler(FtpSocketHandler handler);
		public event CloseHandler Closed;

		#endregion

		#region Construction
		
		public FtpSocketHandler(IFileSystemClassFactory fileSystemClassFactory, int nId, FtpServer ftpServer)
		{
			m_nId = nId;
			m_fileSystemClassFactory = fileSystemClassFactory;
            _ftpServer = ftpServer;
		}

		#endregion

		#region Methods

		public void Start(System.Net.Sockets.TcpClient socket)
		{
			m_theSocket = socket;

			m_theCommands = new Assemblies.Ftp.FtpConnectionObject(
                m_fileSystemClassFactory, m_nId, socket, _ftpServer);
			m_theThread = new System.Threading.Thread(new System.Threading.ThreadStart(this.ThreadRun));
			m_theThread.Start();
		}

		public void Stop()
		{
			Assemblies.General.SocketHelpers.Close(m_theSocket);
			m_theThread.Join();
		}

		private void ThreadRun()
		{
            try
            {
                Byte[] abData = new Byte[m_nBufferSize];

                try
                {
                    int nReceived = m_theSocket.GetStream().Read(abData, 0, m_nBufferSize);

                    while (nReceived > 0)
                    {
                        m_theCommands.Process(abData);

                        nReceived = m_theSocket.GetStream().Read(abData, 0, m_nBufferSize);
                    }
                }
                catch (System.Net.Sockets.SocketException)
                {
                }
                catch (System.IO.IOException)
                {
                }

                FtpServerMessageHandler.SendMessage(m_nId, "Connection closed");
            }
            catch (ThreadAbortException)
            {
                Trace.TraceInformation("FTP Socket Handler: thread has been aborted");
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
            finally
            {
                if (Closed != null)
                {
                    Closed(this);
                }

                m_theSocket.Close();
            }
		}

		#endregion

		#region Properties
	
		public int Id
		{
			get
			{
				return m_nId;
			}
		}

		#endregion

	    public void Dispose()
	    {
            var theCommands = m_theCommands;
            if (theCommands == null)
                return;
	        theCommands.Dispose();
	    }
	}
}
