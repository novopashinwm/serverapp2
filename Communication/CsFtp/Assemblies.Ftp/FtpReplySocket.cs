using System.Diagnostics;
using System.Net;
using System.Net.Sockets;

namespace Assemblies.Ftp
{
	/// <summary>
	/// Encapsulates the functionality necessary to send data along the reply connection
	/// </summary>
	class FtpReplySocket
	{
	    private const int MessageId = 100;
	    private System.Net.Sockets.TcpClient m_theSocket;

        private int _port;
	    private string _host;

		public FtpReplySocket(FtpConnectionObject connectionObject)
		{
		    _host = connectionObject.PortCommandSocketAddress;
		    _port = connectionObject.PortCommandSocketPort;

			m_theSocket = OpenSocket(connectionObject);

            FtpServerMessageHandler.SendMessage(MessageId, string.Format("Socket to {0}:{1} opened", _host, _port));
        }

		public bool Loaded
		{
			get
			{
				return m_theSocket != null;
			}
		}

		public void Close()
		{
			Assemblies.General.SocketHelpers.Close(m_theSocket);
			m_theSocket = null;
            FtpServerMessageHandler.SendMessage(MessageId, string.Format("Socket to {0}:{1} closed", _host, _port));
        }

		public bool Send(byte [] abData, int nSize)
		{
			return Assemblies.General.SocketHelpers.Send(m_theSocket, abData, 0, nSize);
		}

		public bool Send(char [] abChars, int nSize)
		{
			return Assemblies.General.SocketHelpers.Send(m_theSocket, System.Text.Encoding.ASCII.GetBytes(abChars), 0, nSize);
		}

		public bool Send(string sMessage)
		{
			byte [] abData = System.Text.Encoding.ASCII.GetBytes(sMessage);
			return Send(abData, abData.Length);
		}

		public int Receive(byte [] abData)
		{
			return m_theSocket.GetStream().Read(abData, 0, abData.Length);
		}

		static private System.Net.Sockets.TcpClient OpenSocket(FtpConnectionObject connectionObject)
		{
			System.Net.Sockets.TcpClient socketPasv = connectionObject.PasvSocket;

			if (socketPasv != null)
			{
				connectionObject.PasvSocket = null;
				return socketPasv;
			}

		    TcpClient tcpClient = null;
		    var localEndpoint = connectionObject.Socket.Client.LocalEndPoint as IPEndPoint;
		    try
		    {
		        tcpClient = new TcpClient();
                if (localEndpoint != null)
                    tcpClient.Client.Bind(new IPEndPoint(localEndpoint.Address, 20));
                tcpClient.Connect(connectionObject.PortCommandSocketAddress, connectionObject.PortCommandSocketPort);
            }
            catch (SocketException exception)
            {
                switch (exception.SocketErrorCode)
                {
                    case SocketError.AddressAlreadyInUse:
                        Trace.TraceError("{0}.OpenSocket(localEndpoint!=null: {1}, connectionObject: {2}:{3})",
                                         typeof (FtpReplySocket),
                                         localEndpoint != null,
                                         connectionObject.PortCommandSocketAddress,
                                         connectionObject.PortCommandSocketPort);
                        break;
                    default:
                        Trace.TraceError(
                            "Socket exception, error code = {0}, socket error code {1}, exception details: {2}",
                            exception.ErrorCode, exception.SocketErrorCode, exception);
                        break;
                }
            }

		    return tcpClient;
		}
	}
}
