﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

namespace FORIS.TSS.IO.TAPI
{
	/// <summary>
	/// Summary description for CCall.
	/// </summary>
	public class CCall
	{
		/*
		  class CCall
		  Copyright © 2003 by Agile Software Inc.
		  All Rights Reserved.

		  Created: Helen Warn
				Updated: Christoph Brдndle
		  Updated: February 22, 2003

		*/

		#region members

		// parent CTapi object
		private CTapi m_CTapi;

		// Call handle
		private IntPtr m_hCall;

		// line handle
		private IntPtr m_hLine;

		private uint m_LineDeviceID;

		private uint m_AddressID;

		private uint m_CallID;

		private uint m_RelatedCallID;

		private CTapi.LineCallParamFlags m_CallParamFlags;

		private CTapi.LineCallState m_CallStates;

		private CTapi.linedialparams m_DialParams;

		private CTapi.LineCallOrigin m_Origin;

		private CTapi.LineCallReason m_Reason;

		private CTapi.LineCallReason m_CompletionID;

		private uint m_NumOwners;

		private uint m_NumMonitors;

		private uint m_Trunk;

		private CTapi.LineCallPartyID m_CallerIDFlags;

		private string m_CallerID;

		private string m_CallerIDName;

		private CTapi.LineCallPartyID m_CalledIDFlags;

		private string m_CalledID;

		private string m_CalledIDName;

		private CTapi.LineCallPartyID m_ConnectedIDFlags;

		private string m_ConnectedID;

		private string m_ConnectedIDName;

		private CTapi.LineCallPartyID m_RedirectionIDFlags;

		private string m_RedirectionID;

		private string m_RedirectionIDName;

		private CTapi.LineCallPartyID m_RedirectingIDFlags;

		private string m_RedirectingID;

		private string m_RedirectingIDName;

		private string m_AppName;

		private string m_DisplayableAddress;

		private string m_CalledParty;

		private string m_Comment;

		private byte[] m_Display;

		private byte[] m_UserUserInfo;

		private byte[] m_DevSpecific;



		#endregion // Members

		public CCall(CTapi tapi, IntPtr hCall)
		{
			//
			// TODO: Add constructor logic here
			//
			m_CTapi = tapi;
			m_hCall = hCall;
			m_hLine = IntPtr.Zero;
		}

		#region Properties

		public IntPtr hCall
		{ get { return this.m_hCall; } }

		private IntPtr hTapi
		{ get { return this.m_CTapi.hTapi; } }

		public IntPtr hLine
		{ get { return this.m_hLine; } }

		public uint LineDeviceID
		{
			get { return this.m_LineDeviceID; }
			set { this.m_LineDeviceID = value; }
		}

		public uint AddressID
		{
			get { return this.m_AddressID; }
			set { this.m_AddressID = value; }
		}

		public uint CallID
		{
			get { return this.m_CallID; }
			set { this.m_CallID = value; }
		}

		public uint RelatedCallID
		{
			get { return this.m_RelatedCallID; }
			set { this.m_RelatedCallID = value; }
		}

		public CTapi.LineCallParamFlags CallParamFlags
		{
			get { return this.m_CallParamFlags; }
			set { this.m_CallParamFlags = value; }
		}

		public CTapi.LineCallState CallStates
		{
			get { return this.m_CallStates; }
			set { this.m_CallStates = value; }
		}

		public CTapi.linedialparams DialParams
		{
			get { return this.m_DialParams; }
			set { this.m_DialParams = value; }
		}

		public CTapi.LineCallOrigin Origin
		{
			get { return this.m_Origin; }
			set { this.m_Origin = value; }
		}

		public CTapi.LineCallReason Reason
		{
			get { return this.m_Reason; }
			set { this.m_Reason = value; }
		}

		public CTapi.LineCallReason CompletionID
		{
			get { return this.m_CompletionID; }
			set { this.m_CompletionID = value; }
		}

		public uint NumOwners
		{
			get { return this.m_NumOwners; }
			set { this.m_NumOwners = value; }
		}

		public uint NumMonitors
		{
			get { return this.m_NumMonitors; }
			set { this.m_NumMonitors = value; }
		}

		public uint Trunk
		{
			get { return this.m_Trunk; }
			set { this.m_Trunk = value; }
		}

		public CTapi.LineCallPartyID CallerIDFlags
		{
			get { return this.m_CallerIDFlags; }
			set { this.m_CallerIDFlags = value; }
		}

		public string CallerID
		{
			get { return this.m_CallerID; }
			set { this.m_CallerID = value; }
		}

		public string CallerIDName
		{
			get { return this.m_CallerIDName; }
			set { this.m_CallerIDName = value; }
		}

		public CTapi.LineCallPartyID CalledIDFlags
		{
			get { return this.m_CalledIDFlags; }
			set { this.m_CalledIDFlags = value; }
		}

		public string CalledID
		{
			get { return this.m_CalledID; }
			set { this.m_CalledID = value; }
		}

		public string CalledIDName
		{
			get { return this.m_CalledIDName; }
			set { this.m_CalledIDName = value; }
		}

		public CTapi.LineCallPartyID ConnectedIDFlags
		{
			get { return this.m_ConnectedIDFlags; }
			set { this.m_ConnectedIDFlags = value; }
		}

		public string ConnectedID
		{
			get { return this.m_ConnectedID; }
			set { this.m_ConnectedID = value; }
		}

		public string ConnectedIDName
		{
			get { return this.m_ConnectedIDName; }
			set { this.m_ConnectedIDName = value; }
		}

		public CTapi.LineCallPartyID RedirectionIDFlags
		{
			get { return this.m_RedirectionIDFlags; }
			set { this.m_RedirectionIDFlags = value; }
		}

		public string RedirectionID
		{
			get { return this.m_RedirectionID; }
			set { this.m_RedirectionID = value; }
		}

		public string RedirectionIDName
		{
			get { return this.m_RedirectionIDName; }
			set { this.m_RedirectionIDName = value; }
		}

		public CTapi.LineCallPartyID RedirectingIDFlags
		{
			get { return this.m_RedirectingIDFlags; }
			set { this.m_RedirectingIDFlags = value; }
		}

		public string RedirectingID
		{
			get { return this.m_RedirectingID; }
			set { this.m_RedirectingID = value; }
		}

		public string RedirectingIDName
		{
			get { return this.m_RedirectingIDName; }
			set { this.m_RedirectingIDName = value; }
		}

		public string AppName
		{
			get { return this.m_AppName; }
			set { this.m_AppName = value; }
		}

		public string DisplayableAddress
		{
			get { return this.m_DisplayableAddress; }
			set { this.m_DisplayableAddress = value; }
		}

		public string CalledParty
		{
			get { return this.m_CalledParty; }
			set { this.m_CalledParty = value; }
		}

		public string Comment
		{
			get { return this.m_Comment; }
			set { this.m_Comment = value; }
		}

		public byte[] Display
		{
			get { return this.m_Display; }
			set { this.m_Display = value; }
		}

		public byte[] UserUserInfo
		{
			get { return this.m_UserUserInfo; }
			set { this.m_UserUserInfo = value; }
		}

		public byte[] DevSpecific
		{
			get { return this.m_DevSpecific; }
			set { this.m_DevSpecific = value; }
		}

		#endregion // Properties

		#region Public Methods

		/// <summary>
		/// Get the call information from CTapi.linecallinfo into our private members
		/// </summary>
		/// <returns></returns>
		public CTapi.LineErrReturn GetCallInfo()
		{
			CTapi.linecallinfo callinfo = new CTapi.linecallinfo();
			int buffsize = 1;
			int InitSize = Marshal.SizeOf(callinfo);
			int TotalSize = InitSize;
			int NeededSize = InitSize;
			bool needMoreMem = true;
			IntPtr plinecallinfo;
			byte[] totalbuffer = null;
			CTapi.LineErrReturn ret = CTapi.LineErrReturn.LINEERR_STRUCTURETOOSMALL;

			while (needMoreMem == true)
			{
				if ((NeededSize - TotalSize) > 0)
					buffsize = (NeededSize - InitSize + 1);
				else
					buffsize = 1;

				callinfo.dwTotalSize = (uint)(buffsize + InitSize - 1);
				plinecallinfo = Marshal.AllocHGlobal(NeededSize);
				Marshal.StructureToPtr(callinfo, plinecallinfo, true);
				ret = CTapi.lineGetCallInfo(this.hCall, plinecallinfo);
				Marshal.PtrToStructure(plinecallinfo, callinfo);
				TotalSize = (int)callinfo.dwTotalSize;
				NeededSize = (int)callinfo.dwNeededSize;
				needMoreMem = ((ret == CTapi.LineErrReturn.LINEERR_OK) && (NeededSize > TotalSize)) || (ret == CTapi.LineErrReturn.LINEERR_STRUCTURETOOSMALL);
				if (needMoreMem == false)
				{
					totalbuffer = new byte[TotalSize];
					Marshal.Copy(plinecallinfo, totalbuffer, 0, TotalSize);
				}
				Marshal.FreeHGlobal(plinecallinfo);
			}

			if (ret == CTapi.LineErrReturn.LINEERR_OK)
			{

				if (FillCallInfo(callinfo, totalbuffer) == false)
				{
				}
				else
				{
				}
			}
			return ret;
		}

		public CLine GetLine()
		{
			CLine line;
			for (int i = 0; i < this.m_CTapi.Lines.Length; ++i)
			{
				line = this.m_CTapi.Lines[i];
				if (line.hLine == this.hLine)
					return line;
			}
			return null;
		}
		public string GetCallInfoString()
		{
			StringBuilder sb = new StringBuilder(600);

			sb.Append("Device ID: " + this.LineDeviceID + Environment.NewLine);
			sb.Append("Address ID: " + this.AddressID + Environment.NewLine);
			sb.Append("CALL ID: " + this.CallID + Environment.NewLine);
			sb.Append("Trunk: " + this.Trunk + Environment.NewLine);
			sb.Append("Caller ID: " + this.CallerID + Environment.NewLine);
			sb.Append("Caller ID Name: " + this.CallerIDName + Environment.NewLine);
			sb.Append("Called ID: " + this.CalledID + Environment.NewLine);
			sb.Append("Called ID Name: " + this.CalledIDName + Environment.NewLine);
			sb.Append("Called ID: " + this.CalledID + Environment.NewLine);
			sb.Append("Called ID Name: " + this.CalledIDName + Environment.NewLine);
			sb.Append("Connected ID: " + this.ConnectedID + Environment.NewLine);
			sb.Append("Connected ID Name: " + this.ConnectedIDName + Environment.NewLine);
			sb.Append("Redirection ID: " + this.RedirectionID + Environment.NewLine);
			sb.Append("Redirection ID Name: " + this.RedirectionIDName + Environment.NewLine);
			sb.Append("Redirecting ID: " + this.RedirectingID + Environment.NewLine);
			sb.Append("Redirecting ID Name: " + this.RedirectingIDName + Environment.NewLine);
			sb.Append("App Name: " + this.AppName + Environment.NewLine);
			sb.Append("Called Party: " + this.CalledParty + Environment.NewLine);
			sb.Append("Displayable Address: " + this.DisplayableAddress + Environment.NewLine);
			sb.Append("Comment: " + this.Comment + Environment.NewLine);
			if (this.DevSpecific != null)
				sb.Append("Length of Dev Specific: " + this.DevSpecific.Length + Environment.NewLine);

			return sb.ToString();
		}

		#endregion // Public Methods

		#region Private Methods

		private bool FillCallInfo(CTapi.linecallinfo callinfo, byte[] buffer)
		{
			try
			{

				this.m_hLine = (IntPtr)callinfo.hLine;
				CLine line = this.GetLine();
				if (line == null)
				{
					// handle error
				}
				System.Text.Decoder dec = null;
				CTapi.StringFormat sf = line.StringFormat;
				dec = CTapi.GetDecoder(sf);

				// now start filling in the values
				this.LineDeviceID = callinfo.dwLineDeviceID;
				this.AddressID = callinfo.dwAddressID;
				this.CallID = callinfo.dwCallID;
				this.RelatedCallID = callinfo.dwRelatedCallID;
				this.CallParamFlags = (CTapi.LineCallParamFlags)callinfo.dwCallParamFlags;
				this.CallStates = (CTapi.LineCallState)callinfo.dwCallStates;
				this.DialParams = (CTapi.linedialparams)callinfo.DialParams;
				this.Origin = (CTapi.LineCallOrigin)callinfo.dwOrigin;
				this.Reason = (CTapi.LineCallReason)callinfo.dwReason;
				if (this.Reason == CTapi.LineCallReason.LINECALLREASON_CALLCOMPLETION)
				{
					this.CompletionID = (CTapi.LineCallReason)callinfo.dwCompletionID;
				}
				this.NumOwners = callinfo.dwNumOwners;
				this.NumMonitors = callinfo.dwNumMonitors;
				this.Trunk = callinfo.dwTrunk;

				this.CallerIDFlags = (CTapi.LineCallPartyID)callinfo.dwCallerIDFlags;
				this.CallerID = CAPIUtils.StringFromByteArray(buffer, (int)callinfo.dwCallerIDOffset, (int)callinfo.dwCallerIDSize, dec);
				this.CallerIDName = CAPIUtils.StringFromByteArray(buffer, (int)callinfo.dwCallerIDNameOffset, (int)callinfo.dwCallerIDNameSize, dec);

				this.CalledIDFlags = (CTapi.LineCallPartyID)callinfo.dwCalledIDFlags;
				this.CalledID = CAPIUtils.StringFromByteArray(buffer, (int)callinfo.dwCalledIDOffset, (int)callinfo.dwCalledIDSize, dec);
				this.CalledIDName = CAPIUtils.StringFromByteArray(buffer, (int)callinfo.dwCalledIDNameOffset, (int)callinfo.dwCalledIDNameSize, dec);

				this.ConnectedIDFlags = (CTapi.LineCallPartyID)callinfo.dwConnectedIDFlags;
				this.ConnectedID = CAPIUtils.StringFromByteArray(buffer, (int)callinfo.dwConnectedIDOffset, (int)callinfo.dwConnectedIDSize, dec);
				this.ConnectedIDName = CAPIUtils.StringFromByteArray(buffer, (int)callinfo.dwConnectedIDNameOffset, (int)callinfo.dwConnectedIDNameSize, dec);

				this.RedirectionIDFlags = (CTapi.LineCallPartyID)callinfo.dwRedirectionIDFlags;
				this.RedirectionID = CAPIUtils.StringFromByteArray(buffer, (int)callinfo.dwRedirectionIDOffset, (int)callinfo.dwRedirectionIDSize, dec);
				this.RedirectionIDName = CAPIUtils.StringFromByteArray(buffer, (int)callinfo.dwRedirectionIDNameOffset, (int)callinfo.dwRedirectionIDNameSize, dec);

				this.RedirectingIDFlags = (CTapi.LineCallPartyID)callinfo.dwRedirectingIDFlags;
				this.RedirectingID = CAPIUtils.StringFromByteArray(buffer, (int)callinfo.dwRedirectingIDOffset, (int)callinfo.dwRedirectingIDSize, dec);
				this.RedirectingIDName = CAPIUtils.StringFromByteArray(buffer, (int)callinfo.dwRedirectingIDNameOffset, (int)callinfo.dwRedirectingIDNameSize, dec);

				this.AppName = CAPIUtils.StringFromByteArray(buffer, (int)callinfo.dwAppNameOffset, (int)callinfo.dwAppNameSize, dec);
				this.DisplayableAddress = CAPIUtils.StringFromByteArray(buffer, (int)callinfo.dwDisplayableAddressOffset, (int)callinfo.dwDisplayableAddressSize, dec);
				this.CalledParty = CAPIUtils.StringFromByteArray(buffer, (int)callinfo.dwCalledPartyOffset, (int)callinfo.dwCalledPartySize, dec);
				this.Comment = CAPIUtils.StringFromByteArray(buffer, (int)callinfo.dwCommentOffset, (int)callinfo.dwCommentSize, dec);

				this.Display = CAPIUtils.subarray(buffer, (int)callinfo.dwDisplayOffset, (int)callinfo.dwDisplaySize);
				this.UserUserInfo = CAPIUtils.subarray(buffer, (int)callinfo.dwUserUserInfoOffset, (int)callinfo.dwUserUserInfoSize);
				this.DevSpecific = CAPIUtils.subarray(buffer, (int)callinfo.dwDevSpecificOffset, (int)callinfo.dwDevSpecificSize);

				return true;


			}
			catch (Exception ex)
			{
				//MessageBox.Show(ex.Message);
				Debug.WriteLine("!!ERR!!" + ex.Message);
				return false;
			}
		}

		#endregion // Private Methods

	} // CCall
} // FORIS.TSS.IO