﻿#define TAPI22

using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

namespace FORIS.TSS.IO.TAPI
/*
  namespace FORIS.TSS.IO contains all classes for C# wrapper for Tapi 2
  Copyright © 2003 by Agile Software Inc.
  All Rights Reserved.
  
  Created: Helen Warn
	Updated: Christoph Brдndle
  Updated: February 22, 2003
	Modified: Paul Basnett
	Updated	: February 10-21st
	Mods	: Now makes and answer calls via modems.
*/
{

	/// <summary>
	/// CTapi is where we keep all our structs, etc. It is the parent class for all Tapi objects
	/// and contains the initialization of Tapi & a function for choosing a line to open 
	/// </summary>
	public class CTapi : IDisposable
	{
		/*
		  class CTapi
		  Copyright © 2003 by Agile Software Inc.
		  All Rights Reserved.

		  Created: Helen Warn
				Updated: Christoph Brandle & Brian Hormann
		  Updated: February 22, 2003

		*/
		#region Constants

		public const uint TAPILOWVERSION = (uint)0x10003;
		public const uint TAPIHIVERSION = (uint)0x20002;
		public const uint TSPLOWEXTVERSION = (uint)0x10001;
		public const uint TSPHIEXTVERSION = (uint)0x10001;

		#endregion // Constants

		#region EventArgs

		/// <summary>
		/// for event LINE_APPNEWCALL
		/// </summary>
		public class AppNewCallEventArgs : EventArgs
		{
			public readonly CLine line;
			public readonly CCall call;

			public AppNewCallEventArgs(CLine Line, CCall Call)
			{
				line = Line;
				call = Call;
			}

		}

		/// <summary>
		/// for event LINE_CALLSTATE
		/// </summary>
		public class CallStateEventArgs : EventArgs
		{
			// we need the arg hcall to handle the IDLE message. In this call the call is null but
			// the call handle is passed.
			public readonly IntPtr hcall;
			public readonly CCall call;
			public readonly LineCallState CallState;
			public readonly uint CallStateDetail;

			public CallStateEventArgs(IntPtr hCall, CCall Call, LineCallState callState, uint callStateDetail)
			{
				hcall = hCall;
				call = Call;
				CallState = callState;
				CallStateDetail = callStateDetail;
			}
		}

		/// <summary>
		/// for event LINE_REPLY
		/// </summary>
		public class LineReplyEventArgs : EventArgs
		{
			public readonly uint IDRequest;
			public readonly uint status;

			public LineReplyEventArgs(uint request, uint stat)
			{
				IDRequest = request;
				status = stat;
			}

		}

		#endregion // EventArgs

		#region Events

		public event AppNewCallEventHandler AppNewCall = null;
		public event CallStateEventHandler CallStateEvent = null;
		public event LineReplyEventHandler LineReplyEvent = null;

		#endregion // Events

		#region Delegates

		/// <summary>
		/// main callback which is the entry to all Tapi Events
		/// </summary>
		public delegate void LineCallBack(System.UInt32 dwDevice, System.UInt32 dwMessage, System.UInt32 dwInstance,
		  System.UInt32 dwParam1, System.UInt32 dwParam2, System.UInt32 dwParam3);

		/// <summary>
		/// new call
		/// </summary>
		public delegate void AppNewCallEventHandler(Object sender, AppNewCallEventArgs e);

		/// <summary>
		/// LINE_CALLSTATE event handler
		/// </summary>
		public delegate void CallStateEventHandler(Object sender, CallStateEventArgs e);

		/// <summary>
		/// LINE_REPLY event handler
		/// </summary>
		public delegate void LineReplyEventHandler(Object sender, LineReplyEventArgs e);

		#endregion //Delegates

		#region API Calls		
		// make sure case is exact for all dll imports!!!

		[DllImport("Tapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern CTapi.LineErrReturn lineAccept(
			IntPtr hCall,
			byte[] UserUserInfo,
			uint dwSize);

		[DllImport("Tapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern CTapi.LineErrReturn lineAnswer(
			IntPtr hCall,
			byte[] UserUserInfo,
			uint dwSize);

		[DllImport("Tapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern CTapi.LineErrReturn lineClose(
		  IntPtr hLine);


		[DllImport("tapi32.dll", EntryPoint = "lineConfigDialogW", CharSet = CharSet.Auto)]
		internal static extern LineErrReturn lineConfigDialog(uint dwDeviceID,
			System.IntPtr hwndOwner, string lpszDeviceClass);

		[DllImport("Tapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern CTapi.LineErrReturn lineDeallocateCall(
		  IntPtr hLine);

		[DllImport("Tapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern int lineDevSpecific(
	  IntPtr hLine,
	  uint dwAddressID,
	  IntPtr hCall,
	  IntPtr lpParams,
	  uint dwSize);

		[DllImport("Tapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern CTapi.LineErrReturn lineDrop(
			IntPtr hCall,
			string UserInfo,
			uint dwSize);

		[DllImport("Tapi32.dll", EntryPoint = "lineGetAddressCapsW", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern CTapi.LineErrReturn lineGetAddressCaps(
	  IntPtr hLineApp,
	  uint dwDeviceID,
	  uint dwAddressID,
	  uint dwAPIVersion,
	  uint dwExtVersion,
	  IntPtr lpAddressCaps);

		[DllImport("Tapi32.dll", EntryPoint = "lineGetCallInfoW", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern CTapi.LineErrReturn lineGetCallInfo(
	  IntPtr hCall,
	  IntPtr lpCallInfo);

		/// <summary>
		/// The lineGetCountry function fetches the stored dialing rules 
		/// and other information related to a specified country/region, 
		/// the first country/region in the country/region list, 
		/// or all countries/regions.
		/// </summary>
		/// <param name="dwCountryID">Country/region identifier 
		/// (not the country code) of the country/region for which 
		/// information is to be obtained. If the value 1 is specified, 
		/// information on the first country/region in the 
		/// country/region list is obtained. If the value 0 is specified, 
		/// information on all countries/regions is obtained 
		/// (which can require a great deal of memory — 20 KB or more).</param>
		/// <param name="dwAPIVersion">Highest version of TAPI supported by 
		/// the application (not necessarily the value negotiated 
		/// by lineNegotiateAPIVersion on some particular line device). </param>
		/// <param name="lpLineCountryList">Pointer to a location to 
		/// which a LINECOUNTRYLIST structure is loaded. Prior to 
		/// calling lineGetCountry, the application must set the 
		/// dwTotalSize member of this structure to indicate the amount 
		/// of memory available to TAPI for returning information</param>
		/// <returns>Returns LineErrReturn.OK if the request succeeds or a negative error 
		/// number if an error occurs. Possible return values are:		
		/// LineErrReturn.INCOMPATIBLEAPIVERSION, LineErrReturn.NOMEM, 
		/// LineErrReturn.INIFILECORRUPT, LineErrReturn.OPERATIONFAILED, 
		/// LineErrReturn.INVALCOUNTRYCODE, LineErrReturn.STRUCTURETOOSMALL, 
		/// LineErrReturn.INVALPOINTER. <see cref="LineErrReturn"/></returns>
		[DllImport("Tapi32.dll", EntryPoint = "lineGetCountryW", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern CTapi.LineErrReturn lineGetCountry(int dwCountryID, int dwAPIVersion, IntPtr lpLineCountryList);

		[DllImport("Tapi32.dll", EntryPoint = "lineGetDevCapsW", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern CTapi.LineErrReturn lineGetDevCaps(
	  IntPtr hLineApp,
	  uint dwDeviceID,
	  uint dwAPIVersion,
	  uint dwExtVersion,
	  IntPtr lpLineDevCaps);

		[DllImport("Tapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern CTapi.LineErrReturn lineGetDevConfig(
			uint dwDeviceID,
			ref varstring DeviceConfig,
			string DeviceClass);

		[DllImport("Tapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern CTapi.LineErrReturn lineGetID(
			IntPtr hLine,
			uint dwAddressID,
			IntPtr hCall,
			uint dwSelect,
			ref varstring DeviceID,
			string DeviceClass);

		[DllImport("Tapi32.dll", EntryPoint = "lineInitializeW", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern CTapi.LineErrReturn lineInitialize(
		  out IntPtr hLineApp,
		  IntPtr hAppHandle,
		  LineCallBack lCalllBack,
		  string FriendlyAppName,
		  out System.UInt32 NumDevices,
		  ref System.UInt32 APIVersion);

		[DllImport("Tapi32.dll", EntryPoint = "lineInitializeExW", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern CTapi.LineErrReturn lineInitializeEx(
		  out IntPtr hLineApp,
		  IntPtr hAppHandle,
		  LineCallBack lCalllBack,
		  string FriendlyAppName,
		  out System.UInt32 NumDevices,
		  ref System.UInt32 APIVersion,
		  ref CTapi.lineinitializeexparams lineExInitParams);

		[DllImport("Tapi32.dll", EntryPoint = "lineMakeCallW", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern int lineMakeCall(
			IntPtr hLine,
			out IntPtr hCall,
			string DestAddress,
			uint CountryCode,
			IntPtr lpCallParams);

		[DllImport("Tapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern CTapi.LineErrReturn lineNegotiateAPIVersion(
	  IntPtr hLineApp,
	  uint dwDeviceID,
	  uint dwAPILowVersion,
	  uint dwAPIHighVersion,
	  out uint lpdwAPIVersion,
	  out CTapi.lineextensionid lpExtensionID);

		[DllImport("Tapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern CTapi.LineErrReturn lineNegotiateExtVersion(
	  IntPtr hLineApp,
	  uint dwDeviceID,
	  uint dwAPIVersion,
	  uint dwExtLowVersion,
	  uint dwExtHighVersion,
	  out uint dwExtVersion);

		[DllImport("Tapi32.dll", EntryPoint = "lineOpenW", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern CTapi.LineErrReturn lineOpen(
	  IntPtr hLineApp,
	  uint dwDeviceID,
	  out IntPtr hLine,
	  uint dwAPIVersion,
	  uint dwExtVersion,
	  LineCallBack dwCallbackInstance,
	  LineCallPrivilege dwPrivileges,
	  LineMediaMode dwMediaModes,
	  linecallparams lpCallParams);

		[DllImport("Tapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern CTapi.LineErrReturn lineSetStatusMessages(
	  IntPtr hLine,
	  LineDevState dwLineStates,
	  LineAddressState dwAddressStates
	);

		[DllImport("Tapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern CTapi.LineErrReturn lineShutdown(System.IntPtr hTapi);

		[DllImport("Tapi32.dll", EntryPoint = "lineTranslateAddressW", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern CTapi.LineErrReturn lineTranslateAddress(
			IntPtr hLineApp,
			uint dwDeviceID,
			uint dwAPIVersion,
			string AddressIn,
			uint dwCard,
			uint dwTranslateOptions,
			IntPtr lpTranslateOutput);


		[DllImport("tapi32.dll", EntryPoint = "lineTranslateDialogW", CharSet = CharSet.Auto)]
		internal static extern LineErrReturn lineTranslateDialog(System.IntPtr hLineApp,
			uint dwDeviceID, uint dwAPIVersion, IntPtr hwndOwner, string lpszAddressIn);


		#endregion // API Calls

		#region structs & helper classes

		[StructLayout(LayoutKind.Sequential)]
		public class lineaddresscaps
		{
			public uint dwTotalSize;
			public uint dwNeededSize;
			public uint dwUsedSize;

			public uint dwLineDeviceID;

			public uint dwAddressSize;
			public uint dwAddressOffset;

			public uint dwDevSpecificSize;
			public uint dwDevSpecificOffset;

			public LineAddressSharing dwAddressSharing;
			public LineAddressState dwAddressStates;
			public LineCallInfoState dwCallInfoStates;
			public LineCallPartyID dwCallerIDFlags;
			public LineCallPartyID dwCalledIDFlags;
			public LineCallPartyID dwConnectedIDFlags;
			public LineCallPartyID dwRedirectionIDFlags;
			public LineCallPartyID dwRedirectingIDFlags;
			public LineCallState dwCallStates;
			public LineDialToneMode dwDialToneModes;
			public LineBusyMode dwBusyModes;
			public LineSpecialInfo dwSpecialInfo;
			public LineDisconnectedMode dwDisconnectModes;

			public uint dwMaxNumActiveCalls;
			public uint dwMaxNumOnHoldCalls;
			public uint dwMaxNumOnHoldPendingCalls;
			public uint dwMaxNumConference;
			public uint dwMaxNumTransConf;

			public LineAddrCapFlags dwAddrCapFlags;
			public LineCallFeature dwCallFeatures;
			public LineRemoveFromConf dwRemoveFromConfCaps;
			public LineCallState dwRemoveFromConfState;
			public LineTransferMode dwTransferModes;
			public LineParkMode dwParkModes;

			public LineForwardMode dwForwardModes;
			public uint dwMaxForwardEntries;
			public uint dwMaxSpecificEntries;
			public uint dwMinFwdNumRings;
			public uint dwMaxFwdNumRings;

			public uint dwMaxCallCompletions;
			public uint dwCallCompletionConds;
			public uint dwCallCompletionModes;
			public uint dwNumCompletionMessages;
			public uint dwCompletionMsgTextEntrySize;
			public uint dwCompletionMsgTextSize;
			public uint dwCompletionMsgTextOffset;
			public LineAddrFeature dwAddressFeatures;

			public uint dwPredictiveAutoTransferStates;
			public uint dwNumCallTreatments;
			public uint dwCallTreatmentListSize;
			public uint dwCallTreatmentListOffset;
			public uint dwDeviceClassesSize;
			public uint dwDeviceClassesOffset;
			public uint dwMaxCallDataSize;
			public LineCallFeature2 dwCallFeatures2;
			public uint dwMaxNoAnswerTimeout;
			public LineConnectedMode dwConnectedModes;
			public LineOfferingMode dwOfferingModes;
			public LineMediaMode dwAvailableMediaModes;
			/*
			  [MarshalAs(UnmanagedType.ByValArray, SizeConst=1 )] 
			public byte[] buffer; // set to proper size to handle variable-length
				  */
		}


		[StructLayout(LayoutKind.Sequential)]
		public class linecallinfo
		{
			public uint dwTotalSize;
			public uint dwNeededSize;
			public uint dwUsedSize;

			public System.IntPtr hLine;
			public uint dwLineDeviceID;
			public uint dwAddressID;

			public uint dwBearerMode;
			public uint dwRate;
			public uint dwMediaMode;

			public uint dwAppSpecific;
			public uint dwCallID;
			public uint dwRelatedCallID;
			public uint dwCallParamFlags;
			public uint dwCallStates;

			public uint dwMonitorDigitModes;
			public uint dwMonitorMediaModes;

			public linedialparams DialParams;

			public uint dwOrigin;
			public uint dwReason;
			public uint dwCompletionID;

			public uint dwNumOwners;
			public uint dwNumMonitors;

			public uint dwCountryCode;
			public uint dwTrunk;

			public uint dwCallerIDFlags;
			public uint dwCallerIDSize;
			public uint dwCallerIDOffset;
			public uint dwCallerIDNameSize;
			public uint dwCallerIDNameOffset;

			public uint dwCalledIDFlags;
			public uint dwCalledIDSize;
			public uint dwCalledIDOffset;
			public uint dwCalledIDNameSize;
			public uint dwCalledIDNameOffset;

			public uint dwConnectedIDFlags;
			public uint dwConnectedIDSize;
			public uint dwConnectedIDOffset;
			public uint dwConnectedIDNameSize;
			public uint dwConnectedIDNameOffset;

			public uint dwRedirectionIDFlags;
			public uint dwRedirectionIDSize;
			public uint dwRedirectionIDOffset;
			public uint dwRedirectionIDNameSize;
			public uint dwRedirectionIDNameOffset;

			public uint dwRedirectingIDFlags;
			public uint dwRedirectingIDSize;
			public uint dwRedirectingIDOffset;
			public uint dwRedirectingIDNameSize;
			public uint dwRedirectingIDNameOffset;

			public uint dwAppNameSize;
			public uint dwAppNameOffset;
			public uint dwDisplayableAddressSize;
			public uint dwDisplayableAddressOffset;

			public uint dwCalledPartySize;
			public uint dwCalledPartyOffset;

			public uint dwCommentSize;
			public uint dwCommentOffset;

			public uint dwDisplaySize;
			public uint dwDisplayOffset;

			public uint dwUserUserInfoSize;
			public uint dwUserUserInfoOffset;

			public uint dwHighLevelCompSize;
			public uint dwHighLevelCompOffset;

			public uint dwLowLevelCompSize;
			public uint dwLowLevelCompOffset;

			public uint dwChargingInfoSize;
			public uint dwChargingInfoOffset;

			public uint dwTerminalModesSize;
			public uint dwTerminalModesOffset;

			public uint dwDevSpecificSize;
			public uint dwDevSpecificOffset;

			public uint dwCallTreatment;

			public uint dwCallDataSize;
			public uint dwCallDataOffset;

			public uint dwSendingFlowspecSize;
			public uint dwSendingFlowspecOffset;

			public uint dwReceivingFlowspecSize;
			public uint dwReceivingFlowspecOffset;

			/*
			  [MarshalAs(UnmanagedType.ByValArray, SizeConst=1 )] 
			public byte[] buffer; // set to proper size to handle variable-length struct
				  */
		}


		[StructLayout(LayoutKind.Sequential)]
		public class linecallparams
		{  // Defaults:
			public uint dwTotalSize;

			public uint dwBearerMode;              // voice
			public uint dwMinRate;                 // (3.1kHz)
			public uint dwMaxRate;                 // (3.1kHz)
			public uint dwMediaMode;               // interactiveVoice

			public uint dwCallParamFlags;          // 0
			public uint dwAddressMode;             // addressID
			public uint dwAddressID;               // (any available)

			public linedialparams DialParams;        // (0, 0, 0, 0)

			public uint dwOrigAddressSize;         // 0
			public uint dwOrigAddressOffset;

			public uint dwDisplayableAddressSize;  // 0
			public uint dwDisplayableAddressOffset;

			public uint dwCalledPartySize;         // 0
			public uint dwCalledPartyOffset;

			public uint dwCommentSize;             // 0
			public uint dwCommentOffset;

			public uint dwUserUserInfoSize;        // 0
			public uint dwUserUserInfoOffset;

			public uint dwHighLevelCompSize;       // 0
			public uint dwHighLevelCompOffset;

			public uint dwLowLevelCompSize;        // 0
			public uint dwLowLevelCompOffset;

			public uint dwDevSpecificSize;         // 0
			public uint dwDevSpecificOffset;

			public uint dwPredictiveAutoTransferStates;//TAPI Version 2.0

			public uint dwTargetAddressSize;    //TAPI Version 2.0
			public uint dwTargetAddressOffset;  //TAPI Version 2.0

			public uint dwSendingFlowspecSize;   //TAPI Version 2.0
			public uint dwSendingFlowspecOffset; //TAPI Version 2.0

			public uint dwReceivingFlowspecSize;   //TAPI Version 2.0
			public uint dwReceivingFlowspecOffset; //TAPI Version 2.0

			public uint dwDeviceClassSize;      //TAPI Version 2.0
			public uint dwDeviceClassOffset;    //TAPI Version 2.0

			public uint dwDeviceConfigSize;     //TAPI Version 2.0
			public uint dwDeviceConfigOffset;   //TAPI Version 2.0

			public uint dwCallDataSize;         //TAPI Version 2.0
			public uint dwCallDataOffset;       //TAPI Version 2.0

			public uint dwNoAnswerTimeout;      //TAPI Version 2.0

			public uint dwCallingPartyIDSize;   //TAPI Version 2.0
			public uint dwCallingPartyIDOffset; //TAPI Version 2.0
			/*
			  [MarshalAs(UnmanagedType.ByValArray, SizeConst=1 )] 
			public byte[] buffer; // size will be set by lineopen to accommodate VLS
			*/
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct linecalltreatmententry
		{
			public LineCallTreatment dwCallTreatmentID;
			public uint dwCallTreatmentNameSize;
			public uint dwCallTreatmentNameOffset;
		}

		/// <summary>
		/// Device Capabilities Structure
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		public class linedevcaps
		{
			public uint dwTotalSize;
			public uint dwNeededSize;
			public uint dwUsedSize;

			public uint dwProviderInfoSize;
			public uint dwProviderInfoOffset;

			public uint dwSwitchInfoSize;
			public uint dwSwitchInfoOffset;

			public uint dwPermanentLineID;
			public uint dwLineNameSize;
			public uint dwLineNameOffset;
			public uint dwStringFormat;
			public uint dwAddressModes;
			public uint dwNumAddresses;
			public uint dwBearerModes;
			public uint dwMaxRate;
			public uint dwMediaModes;

			public uint dwGenerateToneModes;
			public uint dwGenerateToneMaxNumFreq;
			public uint dwGenerateDigitModes;
			public uint dwMonitorToneMaxNumFreq;
			public uint dwMonitorToneMaxNumEntries;
			public uint dwMonitorDigitModes;
			public uint dwGatherDigitsMinTimeout;
			public uint dwGatherDigitsMaxTimeout;

			public uint dwMedCtlDigitMaxListSize;
			public uint dwMedCtlMediaMaxListSize;
			public uint dwMedCtlToneMaxListSize;
			public uint dwMedCtlCallStateMaxListSize;

			public uint dwDevCapFlags;
			public uint dwMaxNumActiveCalls;
			public uint dwAnswerMode;
			public uint dwRingModes;
			public uint dwLineStates;

			public uint dwUUIAcceptSize;
			public uint dwUUIAnswerSize;
			public uint dwUUIMakeCallSize;
			public uint dwUUIDropSize;
			public uint dwUUISendUserUserInfoSize;
			public uint dwUUICallInfoSize;

			public linedialparams MinDialParams;
			public linedialparams MaxDialParams;
			public linedialparams DefaultDialParams;

			public uint dwNumTerminals;
			public uint dwTerminalCapsSize;
			public uint dwTerminalCapsOffset;
			public uint dwTerminalTextEntrySize;
			public uint dwTerminalTextSize;
			public uint dwTerminalTextOffset;

			public uint dwDevSpecificSize;
			public uint dwDevSpecificOffset;

			public uint dwLineFeatures;

			public uint dwSettableDevStatus;
			public uint dwDeviceClassesSize;
			public uint dwDeviceClassesOffset;
#if TAPI22
			public System.Guid PermanentLineGuid;      //TAPI Version 2.2
#endif
		}


		/// <summary>
		/// dial params as part of device capabilities
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		public struct linedialparams
		{
			public uint dwDialPause;
			public uint dwDialSpeed;
			public uint dwDigitDuration;
			public uint dwWaitForDialtone;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct lineextensionid
		{
			public uint dwExtensionID0;
			public uint dwExtensionID1;
			public uint dwExtensionID2;
			public uint dwExtensionID3;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct linegeneratetone
		{
			public uint dwFrequency;
			public uint dwCadenceOn;
			public uint dwCadenceOff;
			public uint dwVolume;
		}


		/// <summary>
		/// line initialize extended parameters for LineInitializeEx
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		public struct lineinitializeexparams
		{
			public uint dwTotalSize;
			public uint dwNeededSize;
			public uint dwUsedSize;
			public uint dwOptions;
			public System.IntPtr hEvent;
			public uint dwCompletionKey;
		}
		[StructLayout(LayoutKind.Sequential)]
		public struct linemonitortone
		{
			public uint dwAppSpecific;
			public uint dwDuration;
			public uint dwFrequency1;
			public uint dwFrequency2;
			public uint dwFrequency3;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct linetermcaps
		{
			public uint dwTermDev;
			public uint dwTermModes;
			public uint dwTermSharing;
		}

		[StructLayout(LayoutKind.Sequential)]
		public class linetranslateoutput
		{
			public uint dwTotalSize;
			public uint dwNeededSize;
			public uint dwUsedSize;
			public uint dwDialableStringSize;
			public uint dwDialableStringOffset;
			public uint dwDisplayableStringSize;
			public uint dwDisplayableStringOffset;
			public uint dwCurrentCountry;
			public uint dwDestCountry;
			public LineTranslateResult dwTranslateResults;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct varstring
		{
			public uint dwTotalSize;
			public uint dwNeededSize;
			public uint dwUsedSize;
			public uint dwStringFormat;
			public uint dwStringSize;
			public uint dwStringOffset;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1024)]
			public byte[] buffer; // to handle variable-length struct
		}

		#endregion //structs & helper classes

		#region Enumerations
#pragma warning disable CA1712 // Do not prefix enum values with type name
		[Flags]
		public enum LineAddrCapFlags : uint
		{
			LINEADDRCAPFLAGS_FWDNUMRINGS = (uint)0x00000001,
			LINEADDRCAPFLAGS_PICKUPGROUPID = (uint)0x00000002,
			LINEADDRCAPFLAGS_SECURE = (uint)0x00000004,
			LINEADDRCAPFLAGS_BLOCKIDDEFAULT = (uint)0x00000008,
			LINEADDRCAPFLAGS_BLOCKIDOVERRIDE = (uint)0x00000010,
			LINEADDRCAPFLAGS_DIALED = (uint)0x00000020,
			LINEADDRCAPFLAGS_ORIGOFFHOOK = (uint)0x00000040,
			LINEADDRCAPFLAGS_DESTOFFHOOK = (uint)0x00000080,
			LINEADDRCAPFLAGS_FWDCONSULT = (uint)0x00000100,
			LINEADDRCAPFLAGS_SETUPCONFNULL = (uint)0x00000200,
			LINEADDRCAPFLAGS_AUTORECONNECT = (uint)0x00000400,
			LINEADDRCAPFLAGS_COMPLETIONID = (uint)0x00000800,
			LINEADDRCAPFLAGS_TRANSFERHELD = (uint)0x00001000,
			LINEADDRCAPFLAGS_TRANSFERMAKE = (uint)0x00002000,
			LINEADDRCAPFLAGS_CONFERENCEHELD = (uint)0x00004000,
			LINEADDRCAPFLAGS_CONFERENCEMAKE = (uint)0x00008000,
			LINEADDRCAPFLAGS_PARTIALDIAL = (uint)0x00010000,
			LINEADDRCAPFLAGS_FWDSTATUSVALID = (uint)0x00020000,
			LINEADDRCAPFLAGS_FWDINTEXTADDR = (uint)0x00040000,
			LINEADDRCAPFLAGS_FWDBUSYNAADDR = (uint)0x00080000,
			LINEADDRCAPFLAGS_ACCEPTTOALERT = (uint)0x00100000,
			LINEADDRCAPFLAGS_CONFDROP = (uint)0x00200000,
			LINEADDRCAPFLAGS_PICKUPCALLWAIT = (uint)0x00400000,
			LINEADDRCAPFLAGS_PREDICTIVEDIALER = (uint)0x00800000,       // TAPI v2.0
			LINEADDRCAPFLAGS_QUEUE = (uint)0x01000000,                  // TAPI v2.0
			LINEADDRCAPFLAGS_ROUTEPOINT = (uint)0x02000000,             // TAPI v2.0
			LINEADDRCAPFLAGS_HOLDMAKESNEW = (uint)0x04000000,           // TAPI v2.0
			LINEADDRCAPFLAGS_NOINTERNALCALLS = (uint)0x08000000,        // TAPI v2.0
			LINEADDRCAPFLAGS_NOEXTERNALCALLS = (uint)0x10000000,        // TAPI v2.0
			LINEADDRCAPFLAGS_SETCALLINGID = (uint)0x20000000            // TAPI v2.0
		}
		public enum LineAddressMode : uint
		{
			LINEADDRESSMODE_ADDRESSID = (uint)0x00000001,
			LINEADDRESSMODE_DIALABLEADDR = (uint)0x00000002
		}
		[Flags]
		public enum LineAddressSharing : uint
		{
			LINEADDRESSSHARING_PRIVATE = (uint)0x00000001,
			LINEADDRESSSHARING_BRIDGEDEXCL = (uint)0x00000002,
			LINEADDRESSSHARING_BRIDGEDNEW = (uint)0x00000004,
			LINEADDRESSSHARING_BRIDGEDSHARED = (uint)0x00000008,
			LINEADDRESSSHARING_MONITORED = (uint)0x00000010
		}
		[Flags]
		public enum LineAddressState : uint
		{
			LINEADDRESSSTATE_OTHER = (uint)0x00000001,
			LINEADDRESSSTATE_DEVSPECIFIC = (uint)0x00000002,
			LINEADDRESSSTATE_INUSEZERO = (uint)0x00000004,
			LINEADDRESSSTATE_INUSEONE = (uint)0x00000008,
			LINEADDRESSSTATE_INUSEMANY = (uint)0x00000010,
			LINEADDRESSSTATE_NUMCALLS = (uint)0x00000020,
			LINEADDRESSSTATE_FORWARD = (uint)0x00000040,
			LINEADDRESSSTATE_TERMINALS = (uint)0x00000080,
			LINEADDRESSSTATE_CAPSCHANGE = (uint)0x00000100      // TAPI v1.4    
		}
		[Flags]
		public enum LineAddrFeature : uint
		{
			LINEADDRFEATURE_FORWARD = (uint)0x00000001,
			LINEADDRFEATURE_MAKECALL = (uint)0x00000002,
			LINEADDRFEATURE_PICKUP = (uint)0x00000004,
			LINEADDRFEATURE_SETMEDIACONTROL = (uint)0x00000008,
			LINEADDRFEATURE_SETTERMINAL = (uint)0x00000010,
			LINEADDRFEATURE_SETUPCONF = (uint)0x00000020,
			LINEADDRFEATURE_UNCOMPLETECALL = (uint)0x00000040,
			LINEADDRFEATURE_UNPARK = (uint)0x00000080,
			LINEADDRFEATURE_PICKUPHELD = (uint)0x00000100,          // TAPI v2.0
			LINEADDRFEATURE_PICKUPGROUP = (uint)0x00000200,         // TAPI v2.0
			LINEADDRFEATURE_PICKUPDIRECT = (uint)0x00000400,        // TAPI v2.0
			LINEADDRFEATURE_PICKUPWAITING = (uint)0x00000800,       // TAPI v2.0
			LINEADDRFEATURE_FORWARDFWD = (uint)0x00001000,          // TAPI v2.0
			LINEADDRFEATURE_FORWARDDND = (uint)0x00002000           // TAPI v2.0
		}
		[Flags]
		public enum LineAnswerMode : uint
		{
			LINEANSWERMODE_NONE = (uint)0x00000001,
			LINEANSWERMODE_DROP = (uint)0x00000002,
			LINEANSWERMODE_HOLD = (uint)0x00000004
		}
		[Flags]
		public enum LineBearerMode : uint
		{
			LINEBEARERMODE_VOICE = (uint)0x00000001,
			LINEBEARERMODE_SPEECH = (uint)0x00000002,
			LINEBEARERMODE_MULTIUSE = (uint)0x00000004,
			LINEBEARERMODE_DATA = (uint)0x00000008,
			LINEBEARERMODE_ALTSPEECHDATA = (uint)0x00000010,
			LINEBEARERMODE_NONCALLSIGNALING = (uint)0x00000020,
			LINEBEARERMODE_PASSTHROUGH = (uint)0x00000040,       // TAPI v1.4
			LINEBEARERMODE_RESTRICTEDDATA = (uint)0x00000080     // TAPI v2.0    
		}
		[Flags]
		public enum LineBusyMode : uint
		{
			LINEBUSYMODE_STATION = (uint)0x00000001,
			LINEBUSYMODE_TRUNK = (uint)0x00000002,
			LINEBUSYMODE_UNKNOWN = (uint)0x00000004,
			LINEBUSYMODE_UNAVAIL = (uint)0x00000008
		}
		[Flags]
		public enum LineCallComplMode : uint
		{
			LINECALLCOMPLCOND_BUSY = (uint)0x00000001,
			LINECALLCOMPLCOND_NOANSWER = (uint)0x00000002
		}
		[Flags]
		public enum LineCallFeature : uint
		{
			LINECALLFEATURE_ACCEPT = 0x00000001u,
			LINECALLFEATURE_ADDTOCONF = 0x00000002u,
			LINECALLFEATURE_ANSWER = 0x00000004u,
			LINECALLFEATURE_BLINDTRANSFER = 0x00000008u,
			LINECALLFEATURE_COMPLETECALL = 0x00000010u,
			LINECALLFEATURE_COMPLETETRANSF = 0x00000020u,
			LINECALLFEATURE_DIAL = 0x00000040u,
			LINECALLFEATURE_DROP = 0x00000080u,
			LINECALLFEATURE_GATHERDIGITS = 0x00000100u,
			LINECALLFEATURE_GENERATEDIGITS = 0x00000200u,
			LINECALLFEATURE_GENERATETONE = 0x00000400u,
			LINECALLFEATURE_HOLD = 0x00000800u,
			LINECALLFEATURE_MONITORDIGITS = 0x00001000u,
			LINECALLFEATURE_MONITORMEDIA = 0x00002000u,
			LINECALLFEATURE_MONITORTONES = 0x00004000u,
			LINECALLFEATURE_PARK = 0x00008000u,
			LINECALLFEATURE_PREPAREADDCONF = 0x00010000u,
			LINECALLFEATURE_REDIRECT = 0x00020000u,
			LINECALLFEATURE_REMOVEFROMCONF = 0x00040000u,
			LINECALLFEATURE_SECURECALL = 0x00080000u,
			LINECALLFEATURE_SENDUSERUSER = 0x00100000u,
			LINECALLFEATURE_SETCALLPARAMS = 0x00200000u,
			LINECALLFEATURE_SETMEDIACONTROL = 0x00400000u,
			LINECALLFEATURE_SETTERMINAL = 0x00800000u,
			LINECALLFEATURE_SETUPCONF = 0x01000000u,
			LINECALLFEATURE_SETUPTRANSFER = 0x02000000u,
			LINECALLFEATURE_SWAPHOLD = 0x04000000u,
			LINECALLFEATURE_UNHOLD = 0x08000000u,
			LINECALLFEATURE_RELEASEUSERUSERINFO = 0x10000000u,      // TAPI v1.4
			LINECALLFEATURE_SETTREATMENT = 0x20000000u,      // TAPI v2.0
			LINECALLFEATURE_SETQOS = 0x40000000u,      // TAPI v2.0
			LINECALLFEATURE_SETCALLDATA = 0x80000000u,      // TAPI v2.0
		}

		[Flags]
		public enum LineCallFeature2 : uint
		{
			LINECALLFEATURE2_NOHOLDCONFERENCE = (uint)0x00000001,       // TAPI v2.0
			LINECALLFEATURE2_ONESTEPTRANSFER = (uint)0x00000002,        // TAPI v2.0
			LINECALLFEATURE2_COMPLCAMPON = (uint)0x00000004,            // TAPI v2.0
			LINECALLFEATURE2_COMPLCALLBACK = (uint)0x00000008,          // TAPI v2.0
			LINECALLFEATURE2_COMPLINTRUDE = (uint)0x00000010,           // TAPI v2.0
			LINECALLFEATURE2_COMPLMESSAGE = (uint)0x00000020,           // TAPI v2.0
			LINECALLFEATURE2_TRANSFERNORM = (uint)0x00000040,           // TAPI v2.0
			LINECALLFEATURE2_TRANSFERCONF = (uint)0x00000080,           // TAPI v2.0
			LINECALLFEATURE2_PARKDIRECT = (uint)0x00000100,             // TAPI v2.0
			LINECALLFEATURE2_PARKNONDIRECT = (uint)0x00000200           // TAPI v2.0
		}

		[Flags]
		public enum LineCallInfoState : uint
		{
			LINECALLINFOSTATE_OTHER = (uint)0x00000001,
			LINECALLINFOSTATE_DEVSPECIFIC = (uint)0x00000002,
			LINECALLINFOSTATE_BEARERMODE = (uint)0x00000004,
			LINECALLINFOSTATE_RATE = (uint)0x00000008,
			LINECALLINFOSTATE_MEDIAMODE = (uint)0x00000010,
			LINECALLINFOSTATE_APPSPECIFIC = (uint)0x00000020,
			LINECALLINFOSTATE_CALLID = (uint)0x00000040,
			LINECALLINFOSTATE_RELATEDCALLID = (uint)0x00000080,
			LINECALLINFOSTATE_ORIGIN = (uint)0x00000100,
			LINECALLINFOSTATE_REASON = (uint)0x00000200,
			LINECALLINFOSTATE_COMPLETIONID = (uint)0x00000400,
			LINECALLINFOSTATE_NUMOWNERINCR = (uint)0x00000800,
			LINECALLINFOSTATE_NUMOWNERDECR = (uint)0x00001000,
			LINECALLINFOSTATE_NUMMONITORS = (uint)0x00002000,
			LINECALLINFOSTATE_TRUNK = (uint)0x00004000,
			LINECALLINFOSTATE_CALLERID = (uint)0x00008000,
			LINECALLINFOSTATE_CALLEDID = (uint)0x00010000,
			LINECALLINFOSTATE_CONNECTEDID = (uint)0x00020000,
			LINECALLINFOSTATE_REDIRECTIONID = (uint)0x00040000,
			LINECALLINFOSTATE_REDIRECTINGID = (uint)0x00080000,
			LINECALLINFOSTATE_DISPLAY = (uint)0x00100000,
			LINECALLINFOSTATE_USERUSERINFO = (uint)0x00200000,
			LINECALLINFOSTATE_HIGHLEVELCOMP = (uint)0x00400000,
			LINECALLINFOSTATE_LOWLEVELCOMP = (uint)0x00800000,
			LINECALLINFOSTATE_CHARGINGINFO = (uint)0x01000000,
			LINECALLINFOSTATE_TERMINAL = (uint)0x02000000,
			LINECALLINFOSTATE_DIALPARAMS = (uint)0x04000000,
			LINECALLINFOSTATE_MONITORMODES = (uint)0x08000000,
			LINECALLINFOSTATE_TREATMENT = (uint)0x10000000,      // TAPI v2.0
			LINECALLINFOSTATE_QOS = (uint)0x20000000,            // TAPI v2.0
			LINECALLINFOSTATE_CALLDATA = (uint)0x40000000        // TAPI v2.0
		}

		public enum LineCallOrigin : uint
		{
			LINECALLORIGIN_OUTBOUND = (uint)0x00000001,
			LINECALLORIGIN_INTERNAL = (uint)0x00000002,
			LINECALLORIGIN_EXTERNAL = (uint)0x00000004,
			LINECALLORIGIN_UNKNOWN = (uint)0x00000010,
			LINECALLORIGIN_UNAVAIL = (uint)0x00000020,
			LINECALLORIGIN_CONFERENCE = (uint)0x00000040,
			LINECALLORIGIN_INBOUND = (uint)0x00000080      // TAPI v1.4    
		}

		[Flags]
		public enum LineCallParamFlags : uint
		{
			LINECALLPARAMFLAGS_SECURE = (uint)0x00000001,
			LINECALLPARAMFLAGS_IDLE = (uint)0x00000002,
			LINECALLPARAMFLAGS_BLOCKID = (uint)0x00000004,
			LINECALLPARAMFLAGS_ORIGOFFHOOK = (uint)0x00000008,
			LINECALLPARAMFLAGS_DESTOFFHOOK = (uint)0x00000010,
			LINECALLPARAMFLAGS_NOHOLDCONFERENCE = (uint)0x00000020,       // TAPI v2.0
			LINECALLPARAMFLAGS_PREDICTIVEDIAL = (uint)0x00000040,         // TAPI v2.0
			LINECALLPARAMFLAGS_ONESTEPTRANSFER = (uint)0x00000080         // TAPI v2.0    
		}

		[Flags]
		public enum LineCallPartyID : uint
		{
			LINECALLPARTYID_BLOCKED = (uint)0x00000001,
			LINECALLPARTYID_OUTOFAREA = (uint)0x00000002,
			LINECALLPARTYID_NAME = (uint)0x00000004,
			LINECALLPARTYID_ADDRESS = (uint)0x00000008,
			LINECALLPARTYID_PARTIAL = (uint)0x00000010,
			LINECALLPARTYID_UNKNOWN = (uint)0x00000020,
			LINECALLPARTYID_UNAVAIL = (uint)0x00000040
		}

		[Flags]
		public enum LineCallPrivilege : uint
		{
			LINECALLPRIVILEGE_NONE = (uint)0x00000001,
			LINECALLPRIVILEGE_MONITOR = (uint)0x00000002,
			LINECALLPRIVILEGE_OWNER = (uint)0x00000004
		}

		public enum LineCallReason : uint
		{
			LINECALLREASON_DIRECT = (uint)0x00000001,
			LINECALLREASON_FWDBUSY = (uint)0x00000002,
			LINECALLREASON_FWDNOANSWER = (uint)0x00000004,
			LINECALLREASON_FWDUNCOND = (uint)0x00000008,
			LINECALLREASON_PICKUP = (uint)0x00000010,
			LINECALLREASON_UNPARK = (uint)0x00000020,
			LINECALLREASON_REDIRECT = (uint)0x00000040,
			LINECALLREASON_CALLCOMPLETION = (uint)0x00000080,
			LINECALLREASON_TRANSFER = (uint)0x00000100,
			LINECALLREASON_REMINDER = (uint)0x00000200,
			LINECALLREASON_UNKNOWN = (uint)0x00000400,
			LINECALLREASON_UNAVAIL = (uint)0x00000800,
			LINECALLREASON_INTRUDE = (uint)0x00001000,          // TAPI v1.4
			LINECALLREASON_PARKED = (uint)0x00002000,           // TAPI v1.4
			LINECALLREASON_CAMPEDON = (uint)0x00004000,         // TAPI v2.0
			LINECALLREASON_ROUTEREQUEST = (uint)0x00008000      // TAPI v2.0    
		}

		[Flags]
		public enum LineCallState : uint
		{
			LINECALLSTATE_IDLE = (uint)0x00000001,
			LINECALLSTATE_OFFERING = (uint)0x00000002,
			LINECALLSTATE_ACCEPTED = (uint)0x00000004,
			LINECALLSTATE_DIALTONE = (uint)0x00000008,
			LINECALLSTATE_DIALING = (uint)0x00000010,
			LINECALLSTATE_RINGBACK = (uint)0x00000020,
			LINECALLSTATE_BUSY = (uint)0x00000040,
			LINECALLSTATE_SPECIALINFO = (uint)0x00000080,
			LINECALLSTATE_CONNECTED = (uint)0x00000100,
			LINECALLSTATE_PROCEEDING = (uint)0x00000200,
			LINECALLSTATE_ONHOLD = (uint)0x00000400,
			LINECALLSTATE_CONFERENCED = (uint)0x00000800,
			LINECALLSTATE_ONHOLDPENDCONF = (uint)0x00001000,
			LINECALLSTATE_ONHOLDPENDTRANSFER = (uint)0x00002000,
			LINECALLSTATE_DISCONNECTED = (uint)0x00004000,
			LINECALLSTATE_UNKNOWN = (uint)0x00008000
		}

		public enum LineCallTreatment : uint
		{
			LINECALLTREATMENT_SILENCE = (uint)0x00000001,       // TAPI v2.0
			LINECALLTREATMENT_RINGBACK = (uint)0x00000002,      // TAPI v2.0
			LINECALLTREATMENT_BUSY = (uint)0x00000003,          // TAPI v2.0
			LINECALLTREATMENT_MUSIC = (uint)0x00000004          // TAPI v2.0    
		}

		/// <summary>
		/// define values used in the Options member of the 
		/// LINECARDENTRY structure returned as part of the 
		/// LINETRANSLATECAPS structure returned by lineGetTranslateCaps. 
		/// </summary>
		public enum LineCardOption : uint
		{
			/// <summary>
			/// This calling card is one of the predefined calling card 
			/// definitions included by Microsoft with Win32 Telephony. 
			/// It cannot be removed entirely using Dial Helper; if the 
			/// user attempts to remove it, it will become HIDDEN. 
			/// It thus continues to be accessible for copying of dialing rules. 
			/// </summary>
			PREDEFINED = (uint)0x00000001,      // TAPI v1.4
			/// <summary>
			/// This calling card has been hidden by the user. It is not 
			/// shown by Dial Helper in the main listing of available 
			/// calling cards, but will be shown in the list of cards 
			/// from which dialing rules can be copied.
			/// </summary>
			HIDDEN = (uint)0x00000002,      // TAPI v1.4
		}

		[Flags]
		public enum LineConnectedMode : uint
		{
			LINECONNECTEDMODE_ACTIVE = (uint)0x00000001,         // TAPI v1.4
			LINECONNECTEDMODE_INACTIVE = (uint)0x00000002,       // TAPI v1.4
			LINECONNECTEDMODE_ACTIVEHELD = (uint)0x00000004,     // TAPI v2.0
			LINECONNECTEDMODE_INACTIVEHELD = (uint)0x00000008,   // TAPI v2.0
			LINECONNECTEDMODE_CONFIRMED = (uint)0x00000010       // TAPI v2.0
		}

		[Flags]
		public enum LineDevCapFlags : uint
		{
			LINEDEVCAPFLAGS_CROSSADDRCONF = (uint)0x00000001,
			LINEDEVCAPFLAGS_HIGHLEVCOMP = (uint)0x00000002,
			LINEDEVCAPFLAGS_LOWLEVCOMP = (uint)0x00000004,
			LINEDEVCAPFLAGS_MEDIACONTROL = (uint)0x00000008,
			LINEDEVCAPFLAGS_MULTIPLEADDR = (uint)0x00000010,
			LINEDEVCAPFLAGS_CLOSEDROP = (uint)0x00000020,
			LINEDEVCAPFLAGS_DIALBILLING = (uint)0x00000040,
			LINEDEVCAPFLAGS_DIALQUIET = (uint)0x00000080,
			LINEDEVCAPFLAGS_DIALDIALTONE = (uint)0x00000100
		}

		[Flags]
		public enum LineDevState : uint
		{
			LINEDEVSTATE_OTHER = (uint)0x00000001,
			LINEDEVSTATE_RINGING = (uint)0x00000002,
			LINEDEVSTATE_CONNECTED = (uint)0x00000004,
			LINEDEVSTATE_DISCONNECTED = (uint)0x00000008,
			LINEDEVSTATE_MSGWAITON = (uint)0x00000010,
			LINEDEVSTATE_MSGWAITOFF = (uint)0x00000020,
			LINEDEVSTATE_INSERVICE = (uint)0x00000040,
			LINEDEVSTATE_OUTOFSERVICE = (uint)0x00000080,
			LINEDEVSTATE_MAINTENANCE = (uint)0x00000100,
			LINEDEVSTATE_OPEN = (uint)0x00000200,
			LINEDEVSTATE_CLOSE = (uint)0x00000400,
			LINEDEVSTATE_NUMCALLS = (uint)0x00000800,
			LINEDEVSTATE_NUMCOMPLETIONS = (uint)0x00001000,
			LINEDEVSTATE_TERMINALS = (uint)0x00002000,
			LINEDEVSTATE_ROAMMODE = (uint)0x00004000,
			LINEDEVSTATE_BATTERY = (uint)0x00008000,
			LINEDEVSTATE_SIGNAL = (uint)0x00010000,
			LINEDEVSTATE_DEVSPECIFIC = (uint)0x00020000,
			LINEDEVSTATE_REINIT = (uint)0x00040000,
			LINEDEVSTATE_LOCK = (uint)0x00080000,
			LINEDEVSTATE_CAPSCHANGE = (uint)0x00100000,           // TAPI v1.4
			LINEDEVSTATE_CONFIGCHANGE = (uint)0x00200000,         // TAPI v1.4
			LINEDEVSTATE_TRANSLATECHANGE = (uint)0x00400000,      // TAPI v1.4
			LINEDEVSTATE_COMPLCANCEL = (uint)0x00800000,          // TAPI v1.4
			LINEDEVSTATE_REMOVED = (uint)0x01000000               // TAPI v1.4    
		}


		[Flags]
		public enum LineDevStatus : uint
		{
			LINEDEVSTATUSFLAGS_CONNECTED = (uint)0x00000001,
			LINEDEVSTATUSFLAGS_MSGWAIT = (uint)0x00000002,
			LINEDEVSTATUSFLAGS_INSERVICE = (uint)0x00000004,
			LINEDEVSTATUSFLAGS_LOCKED = (uint)0x00000008
		}

		[Flags]
		public enum LineDialToneMode : uint
		{
			LINEDIALTONEMODE_NORMAL = (uint)0x00000001,
			LINEDIALTONEMODE_SPECIAL = (uint)0x00000002,
			LINEDIALTONEMODE_INTERNAL = (uint)0x00000004,
			LINEDIALTONEMODE_EXTERNAL = (uint)0x00000008,
			LINEDIALTONEMODE_UNKNOWN = (uint)0x00000010,
			LINEDIALTONEMODE_UNAVAIL = (uint)0x00000020
		}

		public enum LineDigitMode : uint
		{
			LINEDIGITMODE_PULSE = (uint)0x00000001,
			LINEDIGITMODE_DTMF = (uint)0x00000002,
			LINEDIGITMODE_DTMFEND = (uint)0x00000004
		}

		[Flags]
		public enum LineDisconnectedMode : uint
		{
			LINEDISCONNECTMODE_NORMAL = (uint)0x00000001,
			LINEDISCONNECTMODE_UNKNOWN = (uint)0x00000002,
			LINEDISCONNECTMODE_REJECT = (uint)0x00000004,
			LINEDISCONNECTMODE_PICKUP = (uint)0x00000008,
			LINEDISCONNECTMODE_FORWARDED = (uint)0x00000010,
			LINEDISCONNECTMODE_BUSY = (uint)0x00000020,
			LINEDISCONNECTMODE_NOANSWER = (uint)0x00000040,
			LINEDISCONNECTMODE_BADADDRESS = (uint)0x00000080,
			LINEDISCONNECTMODE_UNREACHABLE = (uint)0x00000100,
			LINEDISCONNECTMODE_CONGESTION = (uint)0x00000200,
			LINEDISCONNECTMODE_INCOMPATIBLE = (uint)0x00000400,
			LINEDISCONNECTMODE_UNAVAIL = (uint)0x00000800,
			LINEDISCONNECTMODE_NODIALTONE = (uint)0x00001000,         // TAPI v1.4
			LINEDISCONNECTMODE_NUMBERCHANGED = (uint)0x00002000,      // TAPI v2.0
			LINEDISCONNECTMODE_OUTOFORDER = (uint)0x00004000,         // TAPI v2.0
			LINEDISCONNECTMODE_TEMPFAILURE = (uint)0x00008000,        // TAPI v2.0
			LINEDISCONNECTMODE_QOSUNAVAIL = (uint)0x00010000,         // TAPI v2.0
			LINEDISCONNECTMODE_BLOCKED = (uint)0x00020000,            // TAPI v2.0
			LINEDISCONNECTMODE_DONOTDISTURB = (uint)0x00040000,       // TAPI v2.0
			LINEDISCONNECTMODE_CANCELLED = (uint)0x00080000           // TAPI v2.0
		}

		/// <summary>
		/// returns from lineInitializeEx, lineInitialize, lineShutDown, lineNegotiateAPIVersion
		/// </summary>
		public enum LineErrReturn : uint
		{
			LINEERR_OK = 0x00000000u,
			LINEERR_ALLOCATED = 0x80000001u,
			LINEERR_BADDEVICEID = 0x80000002u,
			LINEERR_BEARERMODEUNAVAIL = 0x80000003u,
			LINEERR_CALLUNAVAIL = 0x80000005u,
			LINEERR_COMPLETIONOVERRUN = 0x80000006u,
			LINEERR_CONFERENCEFULL = 0x80000007u,
			LINEERR_DIALBILLING = 0x80000008u,
			LINEERR_DIALDIALTONE = 0x80000009u,
			LINEERR_DIALPROMPT = 0x8000000Au,
			LINEERR_DIALQUIET = 0x8000000Bu,
			LINEERR_INCOMPATIBLEAPIVERSION = 0x8000000Cu,
			LINEERR_INCOMPATIBLEEXTVERSION = 0x8000000Du,
			LINEERR_INIFILECORRUPT = 0x8000000Eu,
			LINEERR_INUSE = 0x8000000Fu,
			LINEERR_INVALADDRESS = 0x80000010u,
			LINEERR_INVALADDRESSID = 0x80000011u,
			LINEERR_INVALADDRESSMODE = 0x80000012u,
			LINEERR_INVALADDRESSSTATE = 0x80000013u,
			LINEERR_INVALAPPHANDLE = 0x80000014u,
			LINEERR_INVALAPPNAME = 0x80000015u,
			LINEERR_INVALBEARERMODE = 0x80000016u,
			LINEERR_INVALCALLCOMPLMODE = 0x80000017u,
			LINEERR_INVALCALLHANDLE = 0x80000018u,
			LINEERR_INVALCALLPARAMS = 0x80000019u,
			LINEERR_INVALCALLPRIVILEGE = 0x8000001Au,
			LINEERR_INVALCALLSELECT = 0x8000001Bu,
			LINEERR_INVALCALLSTATE = 0x8000001Cu,
			LINEERR_INVALCALLSTATELIST = 0x8000001Du,
			LINEERR_INVALCARD = 0x8000001Eu,
			LINEERR_INVALCOMPLETIONID = 0x8000001Fu,
			LINEERR_INVALCONFCALLHANDLE = 0x80000020u,
			LINEERR_INVALCONSULTCALLHANDLE = 0x80000021u,
			LINEERR_INVALCOUNTRYCODE = 0x80000022u,
			LINEERR_INVALDEVICECLASS = 0x80000023u,
			LINEERR_INVALDEVICEHANDLE = 0x80000024u,
			LINEERR_INVALDIALPARAMS = 0x80000025u,
			LINEERR_INVALDIGITLIST = 0x80000026u,
			LINEERR_INVALDIGITMODE = 0x80000027u,
			LINEERR_INVALDIGITS = 0x80000028u,
			LINEERR_INVALEXTVERSION = 0x80000029u,
			LINEERR_INVALGROUPID = 0x8000002Au,
			LINEERR_INVALLINEHANDLE = 0x8000002Bu,
			LINEERR_INVALLINESTATE = 0x8000002Cu,
			LINEERR_INVALLOCATION = 0x8000002Du,
			LINEERR_INVALMEDIALIST = 0x8000002Eu,
			LINEERR_INVALMEDIAMODE = 0x8000002Fu,
			LINEERR_INVALMESSAGEID = 0x80000030u,
			LINEERR_INVALPARAM = 0x80000032u,
			LINEERR_INVALPARKID = 0x80000033u,
			LINEERR_INVALPARKMODE = 0x80000034u,
			LINEERR_INVALPOINTER = 0x80000035u,
			LINEERR_INVALPRIVSELECT = 0x80000036u,
			LINEERR_INVALRATE = 0x80000037u,
			LINEERR_INVALREQUESTMODE = 0x80000038u,
			LINEERR_INVALTERMINALID = 0x80000039u,
			LINEERR_INVALTERMINALMODE = 0x8000003Au,
			LINEERR_INVALTIMEOUT = 0x8000003Bu,
			LINEERR_INVALTONE = 0x8000003Cu,
			LINEERR_INVALTONELIST = 0x8000003Du,
			LINEERR_INVALTONEMODE = 0x8000003Eu,
			LINEERR_INVALTRANSFERMODE = 0x8000003Fu,
			LINEERR_LINEMAPPERFAILED = 0x80000040u,
			LINEERR_NOCONFERENCE = 0x80000041u,
			LINEERR_NODEVICE = 0x80000042u,
			LINEERR_NODRIVER = 0x80000043u,
			LINEERR_NOMEM = 0x80000044u,
			LINEERR_NOREQUEST = 0x80000045u,
			LINEERR_NOTOWNER = 0x80000046u,
			LINEERR_NOTREGISTERED = 0x80000047u,
			LINEERR_OPERATIONFAILED = 0x80000048u,
			LINEERR_OPERATIONUNAVAIL = 0x80000049u,
			LINEERR_RATEUNAVAIL = 0x8000004Au,
			LINEERR_RESOURCEUNAVAIL = 0x8000004Bu,
			LINEERR_REQUESTOVERRUN = 0x8000004Cu,
			LINEERR_STRUCTURETOOSMALL = 0x8000004Du,
			LINEERR_TARGETNOTFOUND = 0x8000004Eu,
			LINEERR_TARGETSELF = 0x8000004Fu,
			LINEERR_UNINITIALIZED = 0x80000050u,
			LINEERR_USERUSERINFOTOOBIG = 0x80000051u,
			LINEERR_REINIT = 0x80000052u,
			LINEERR_ADDRESSBLOCKED = 0x80000053u,
			LINEERR_BILLINGREJECTED = 0x80000054u,
			LINEERR_INVALFEATURE = 0x80000055u,
			LINEERR_NOMULTIPLEINSTANCE = 0x80000056u,
			LINEERR_INVALAGENTID = 0x80000057u,
			LINEERR_INVALAGENTGROUP = 0x80000058u,
			LINEERR_INVALPASSWORD = 0x80000059u,
			LINEERR_INVALAGENTSTATE = 0x8000005Au,
			LINEERR_INVALAGENTACTIVITY = 0x8000005Bu,
			LINEERR_DIALVOICEDETECT = 0x8000005Cu
		};

		[Flags]
		public enum LineFeature : uint
		{
			LINEFEATURE_DEVSPECIFIC = (uint)0x00000001,
			LINEFEATURE_DEVSPECIFICFEAT = (uint)0x00000002,
			LINEFEATURE_FORWARD = (uint)0x00000004,
			LINEFEATURE_MAKECALL = (uint)0x00000008,
			LINEFEATURE_SETMEDIACONTROL = (uint)0x00000010,
			LINEFEATURE_SETTERMINAL = (uint)0x00000020,
			LINEFEATURE_SETDEVSTATUS = (uint)0x00000040,      // TAPI v2.0
			LINEFEATURE_FORWARDFWD = (uint)0x00000080,        // TAPI v2.0
			LINEFEATURE_FORWARDDND = (uint)0x00000100         // TAPI v2.0    
		}

		public enum LineForwardMode : uint
		{
			LINEFORWARDMODE_UNCOND = (uint)0x00000001,
			LINEFORWARDMODE_UNCONDINTERNAL = (uint)0x00000002,
			LINEFORWARDMODE_UNCONDEXTERNAL = (uint)0x00000004,
			LINEFORWARDMODE_UNCONDSPECIFIC = (uint)0x00000008,
			LINEFORWARDMODE_BUSY = (uint)0x00000010,
			LINEFORWARDMODE_BUSYINTERNAL = (uint)0x00000020,
			LINEFORWARDMODE_BUSYEXTERNAL = (uint)0x00000040,
			LINEFORWARDMODE_BUSYSPECIFIC = (uint)0x00000080,
			LINEFORWARDMODE_NOANSW = (uint)0x00000100,
			LINEFORWARDMODE_NOANSWINTERNAL = (uint)0x00000200,
			LINEFORWARDMODE_NOANSWEXTERNAL = (uint)0x00000400,
			LINEFORWARDMODE_NOANSWSPECIFIC = (uint)0x00000800,
			LINEFORWARDMODE_BUSYNA = (uint)0x00001000,
			LINEFORWARDMODE_BUSYNAINTERNAL = (uint)0x00002000,
			LINEFORWARDMODE_BUSYNAEXTERNAL = (uint)0x00004000,
			LINEFORWARDMODE_BUSYNASPECIFIC = (uint)0x00008000,
			LINEFORWARDMODE_UNKNOWN = (uint)0x00010000,           // TAPI v1.4
			LINEFORWARDMODE_UNAVAIL = (uint)0x00020000            // TAPI v1.4    
		}

		public enum LineInitializeExOptions : uint
		{
			LINEINITIALIZEEXOPTION_USEHIDDENWINDOW = (uint)0x00000001,
			LINEINITIALIZEEXOPTION_USEEVENT = (uint)0x00000002,
			LINEINITIALIZEEXOPTION_USECOMPLETIONPORT = (uint)0x00000003
		}

		[Flags]
		public enum LineMediaMode : uint
		{
			LINEMEDIAMODE_UNKNOWN = (uint)0x00000002,
			LINEMEDIAMODE_INTERACTIVEVOICE = (uint)0x00000004,
			LINEMEDIAMODE_AUTOMATEDVOICE = (uint)0x00000008,
			LINEMEDIAMODE_DATAMODEM = (uint)0x00000010,
			LINEMEDIAMODE_G3FAX = (uint)0x00000020,
			LINEMEDIAMODE_TDD = (uint)0x00000040,
			LINEMEDIAMODE_G4FAX = (uint)0x00000080,
			LINEMEDIAMODE_DIGITALDATA = (uint)0x00000100,
			LINEMEDIAMODE_TELETEX = (uint)0x00000200,
			LINEMEDIAMODE_VIDEOTEX = (uint)0x00000400,
			LINEMEDIAMODE_TELEX = (uint)0x00000800,
			LINEMEDIAMODE_MIXED = (uint)0x00001000,
			LINEMEDIAMODE_ADSI = (uint)0x00002000,
			LINEMEDIAMODE_VOICEVIEW = (uint)0x00004000,
			LINEMEDIAMODE_VIDEO = (uint)0x00008000
		}

		public enum LineOfferingMode : uint
		{
			LINEOFFERINGMODE_ACTIVE = (uint)0x00000001,       // TAPI v1.4
			LINEOFFERINGMODE_INACTIVE = (uint)0x00000002      // TAPI v1.4
		}

		public enum LineOpenOption : uint
		{
			LINEOPENOPTION_SINGLEADDRESS = 0x80000000u,      // TAPI v2.0
			LINEOPENOPTION_PROXY = 0x40000000u               // TAPI v2.0
		}

		public enum LineParkMode : uint
		{
			LINEPARKMODE_DIRECTED = (uint)0x00000001,
			LINEPARKMODE_NONDIRECTED = (uint)0x00000002
		}

		public enum LineRemoveFromConf : uint
		{
			LINEREMOVEFROMCONF_NONE = (uint)0x00000001,
			LINEREMOVEFROMCONF_LAST = (uint)0x00000002,
			LINEREMOVEFROMCONF_ANY = (uint)0x00000003
		}

		[Flags]
		public enum LineSpecialInfo : uint
		{
			LINESPECIALINFO_NOCIRCUIT = (uint)0x00000001,
			LINESPECIALINFO_CUSTIRREG = (uint)0x00000002,
			LINESPECIALINFO_REORDER = (uint)0x00000004,
			LINESPECIALINFO_UNKNOWN = (uint)0x00000008,
			LINESPECIALINFO_UNAVAIL = (uint)0x00000010
		}

		[Flags]
		public enum LineTermDev : uint
		{
			LINETERMDEV_PHONE = (uint)0x00000001,
			LINETERMDEV_HEADSET = (uint)0x00000002,
			LINETERMDEV_SPEAKER = (uint)0x00000004
		}

		[Flags]
		public enum LineTermMode : uint
		{
			LINETERMMODE_BUTTONS = (uint)0x00000001,
			LINETERMMODE_LAMPS = (uint)0x00000002,
			LINETERMMODE_DISPLAY = (uint)0x00000004,
			LINETERMMODE_RINGER = (uint)0x00000008,
			LINETERMMODE_HOOKSWITCH = (uint)0x00000010,
			LINETERMMODE_MEDIATOLINE = (uint)0x00000020,
			LINETERMMODE_MEDIAFROMLINE = (uint)0x00000040,
			LINETERMMODE_MEDIABIDIRECT = (uint)0x00000080
		}

		public enum LineTermSharing : uint
		{
			LINETERMSHARING_PRIVATE = (uint)0x00000001,
			LINETERMSHARING_SHAREDEXCL = (uint)0x00000002,
			LINETERMSHARING_SHAREDCONF = (uint)0x00000004
		}

		//[Flags]
		public enum LineToneMode : uint
		{
			LINETONEMODE_CUSTOM = (uint)0x00000001,
			LINETONEMODE_RINGBACK = (uint)0x00000002,
			LINETONEMODE_BUSY = (uint)0x00000004,
			LINETONEMODE_BEEP = (uint)0x00000008,
			LINETONEMODE_BILLING = (uint)0x00000010
		}

		public enum LineTransferMode : uint
		{
			LINETRANSFERMODE_TRANSFER = (uint)0x00000001,
			LINETRANSFERMODE_CONFERENCE = (uint)0x00000002
		}

		[Flags]
		public enum LineTranslateOption : uint
		{
			LINETRANSLATEOPTION_CARDOVERRIDE = (uint)0x00000001,
			LINETRANSLATEOPTION_CANCELCALLWAITING = (uint)0x00000002,      // TAPI v1.4
			LINETRANSLATEOPTION_FORCELOCAL = (uint)0x00000004,      // TAPI v1.4
			LINETRANSLATEOPTION_FORCELD = (uint)0x00000008       // TAPI v1.4
		}

		[Flags]
		public enum LineTranslateResult : uint
		{
			LINETRANSLATERESULT_CANONICAL = (uint)0x00000001,
			LINETRANSLATERESULT_INTERNATIONAL = (uint)0x00000002,
			LINETRANSLATERESULT_LONGDISTANCE = (uint)0x00000004,
			LINETRANSLATERESULT_LOCAL = (uint)0x00000008,
			LINETRANSLATERESULT_INTOLLLIST = (uint)0x00000010,
			LINETRANSLATERESULT_NOTINTOLLLIST = (uint)0x00000020,
			LINETRANSLATERESULT_DIALBILLING = (uint)0x00000040,
			LINETRANSLATERESULT_DIALQUIET = (uint)0x00000080,
			LINETRANSLATERESULT_DIALDIALTONE = (uint)0x00000100,
			LINETRANSLATERESULT_DIALPROMPT = (uint)0x00000200,
			LINETRANSLATERESULT_VOICEDETECT = (uint)0x00000400,      // TAPI v2.0
			LINETRANSLATERESULT_NOTRANSLATION = (uint)0x00000800       // TAPI v3.0
		}

		public enum StringFormat : uint
		{
			STRINGFORMAT_ASCII = (uint)0x00000001,
			STRINGFORMAT_DBCS = (uint)0x00000002,
			STRINGFORMAT_UNICODE = (uint)0x00000003,
			STRINGFORMAT_BINARY = (uint)0x00000004
		}

		/// <summary>
		/// All Events raised by Tapi 2.2
		/// </summary>
		public enum TapiEvent
		{
			LINE_ADDRESSSTATE = 0,
			LINE_CALLINFO,
			LINE_CALLSTATE,
			LINE_CLOSE,
			LINE_DEVSPECIFIC,
			LINE_DEVSPECIFICFEATURE,
			LINE_GATHERDIGITS,
			LINE_GENERATE,
			LINE_LINEDEVSTATE,
			LINE_MONITORDIGITS,
			LINE_MONITORMEDIA,
			LINE_MONITORTONE,
			LINE_REPLY,
			LINE_REQUEST,
			PHONE_BUTTON,
			PHONE_CLOSE,
			PHONE_DEVSPECIFIC,
			PHONE_REPLY,
			PHONE_STATE,

			LINE_CREATE,        // Tapi v1.4
			PHONE_CREATE,

			LINE_AGENTSPECIFIC, // Tapi v2
			LINE_AGENTSTATUS,
			LINE_APPNEWCALL,
			LINE_PROXYREQUEST,
			LINE_REMOVE,
			PHONE_REMOVE
		};

#pragma warning restore CA1712 // Do not prefix enum values with type name

		#endregion //Enumerations

		#region Private Members

		/// <summary>
		/// application friendly name to pass to LineInitializeEx
		/// </summary>
		private string m_friendlyAppName;

		/// <summary>
		///  handle to TAPI, set by LineInitializeEx
		/// </summary>
		private System.IntPtr m_hTapi = System.IntPtr.Zero;

		/// <summary>
		/// number of devices, returned by LineInitializeEx
		/// </summary>
		private uint m_numberDevices = 0;

		/// <summary>
		/// TAPI Version -- default to 2
		/// </summary>
		private uint m_TapiVersion = 0x00020000;

		/// <summary>
		/// Handle of calling application or window
		/// </summary>
		private System.IntPtr m_AppHandle = System.IntPtr.Zero;

		/// <summary>
		/// array of all lines found by lineInitializeEx
		/// </summary>
		private CLine[] m_CLines;

		private LineCallBack m_CallBack;

		#endregion //  Private Members

		#region Asynchronous Event Handling

		/// <summary>
		/// this structure is for use in sinking Asynchronous events such as generated by calling lineDevSpecific
		/// the calling routine should return requestID and pinfo (which it allocates using Marshal.AllocHGlobal
		/// the process that sinks the events should maintain an ArrayList of these structs
		/// When an event occurs (e.g. LINE_REPLY) the event handler should look in the array for one with
		/// a matching requestID, and if it finds one, retrieve the required state data from pinfo
		/// NOTE: the event sink is responsible for releasing the memory using Marshal.FreeHGlobal(pinfo)
		/// </summary>
		public class PendingReplyEvents
		{
			public int requestID;
			public IntPtr pinfo;
			public CCall call;
		}

		#endregion // Asynchronous Event Handling

		#region Constructors

		public CTapi(string AppName, uint Version, IntPtr AppHandle) : this(AppName, Version, AppHandle,
			CTapi.LineInitializeExOptions.LINEINITIALIZEEXOPTION_USEHIDDENWINDOW)
		{ }

		public CTapi(string AppName, uint Version, IntPtr AppHandle, LineInitializeExOptions opts)
		{
			Debug.AutoFlush = true;
			Debug.Indent();
			Debug.WriteLine("CTapi()");
			Debug.Unindent();


			m_friendlyAppName = AppName;
			m_TapiVersion = Version;
			m_AppHandle = AppHandle;
			// we'll need to pass this to several API calls
			m_CallBack = new CTapi.LineCallBack(MainLineCallBack);

			// prepare the lineinitializeexparams
			CTapi.lineinitializeexparams initparams = new CTapi.lineinitializeexparams();
			initparams.dwTotalSize = (uint)Marshal.SizeOf(initparams);
			initparams.dwNeededSize = initparams.dwTotalSize;
			initparams.dwUsedSize = initparams.dwTotalSize;
			initparams.hEvent = System.IntPtr.Zero;
			initparams.dwOptions = (uint)opts;

			// now start up the events!
			try
			{
				///*
				CTapi.LineErrReturn ret = CTapi.lineInitializeEx(out m_hTapi, m_AppHandle, m_CallBack,
					m_friendlyAppName, out m_numberDevices, ref m_TapiVersion, ref initparams);
				//*/
				/*
				CTapi.LineInitReturn ret = CTapi.lineInitialize(out m_hTapi, m_AppHandle, m_CallBack,
					m_friendlyAppName, out m_numberDevices, ref m_TapiVersion);
				*/
				if (ret != CTapi.LineErrReturn.LINEERR_OK)
				{
					// handle the error
				}
				ret = InitializeLines();
				//MessageBox.Show(CTapi.sLineReturn("After InitializeLines", ret));

			}
			catch (Exception ex)
			{
				//MessageBox.Show(ex.Message);
				Debug.WriteLine("!!ERR!!" + ex.Message);
			}
		}

		~CTapi()
		{
			Dispose(false);
		}

		#endregion // Constructors

		#region Properties

		public LineCallBack CallBack
		{ get { return this.m_CallBack; } }

		public IntPtr hTapi
		{ get { return this.m_hTapi; } }

		public CLine[] Lines
		{ get { return this.m_CLines; } }

		#endregion // Properties

		#region Static Methods

		/// <summary>
		/// marshals a guid to the lineextensionid struct
		/// </summary>
		/// <param name="uuid"></param>
		/// <returns></returns>
		public static lineextensionid LineExtIDFromGuid(System.Guid uuid)
		{
			IntPtr buffer = Marshal.AllocHGlobal(Marshal.SizeOf(uuid));
			Marshal.StructureToPtr(uuid, buffer, false);
			//lineextensionid lineex = new CTapi.lineextensionid();
			lineextensionid lineex = (lineextensionid)Marshal.PtrToStructure(buffer, typeof(lineextensionid));
			Marshal.FreeHGlobal(buffer);
			return lineex;
		}

		#endregion // Static Methods

		#region Public Methods

		public CLine GetLineByFilter(string Provider, bool IsVoice, CTapi.LineCallPrivilege priv)
		{
			CLine line;
			bool lineOK = false;
			for (int i = 0; i < this.Lines.Length; ++i)
			{
				line = this.Lines[i];
				lineOK = false;
				// first make sure it meets the basics!
				if ((line.IsCompatible == true) && (line.LineValid == true))
				{
					if ((Provider != null) && (Provider != string.Empty))
					{
						if (line.ProviderInfo.IndexOf(Provider) >= 0)
							lineOK = true;
					}
					else
						lineOK = true;

					// if we get here and lineOK == true, either LineName was null, or the name met the filter criteria		        
					if (lineOK == true)
					{
						if (IsVoice == true)
							lineOK = line.IsVoiceLine();
					}
				}

				if (lineOK == true)
				{
					// open the line and return it
					CTapi.LineErrReturn ret;
					// first get the address caps for the line
					ret = line.FillAddressCaps();
					ret = line.LineOpen(line.DeviceID, priv);
					Debug.Assert(ret == LineErrReturn.LINEERR_OK, "Unable to open line");

					return line;
				}
			}
			return null;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="Provider"></param>
		/// <returns></returns>
		public LineCollection GetVoiceLinesByFilter(string Provider)//, LineCallPrivilege priv)
		{
			LineCollection lineCollection = new LineCollection();
			CLine line;
			bool lineOK = false;
			for (int i = 0; i < this.Lines.Length; ++i)
			{
				line = this.Lines[i];
				lineOK = false;
				// first make sure it meets the basics!
				if ((line.IsCompatible == true) && (line.LineValid == true))
				{
					if ((Provider != null) && (Provider != string.Empty))
					{
						if (line.ProviderInfo.IndexOf(Provider) >= 0)
							lineOK = true;
					}
					else
						lineOK = true;

					// if we get here and lineOK == true, either LineName was null, or the name met the filter criteria		        
					if (lineOK == true)
					{
						lineOK = line.IsVoiceLine();
					}
				}

				if (lineOK == true)
				{
					// open the line and return it
					//LineErrReturn ret;
					// first get the address caps for the line
					/*ret = */
					line.FillAddressCaps();
					lineCollection.Add(line);
				}
			}
			return lineCollection;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public LineCollection GetVoiceLines()
		{
			LineCollection lineCollection = new LineCollection();
			CLine line;
			for (int i = this.Lines.GetLowerBound(0); i < this.Lines.GetUpperBound(0) + 1; i++)
			{
				line = this.Lines[i];
				if (line.IsVoiceLine())
					lineCollection.Add(line);

			}
			return lineCollection;
		}

		//	the DialNumber has to be adjusted to the telephony settings (dial prefix ..)
		public LineErrReturn GetTranslateAddress(CLine Line, string DialNumber, ref linetranslateoutput TranslateOutput, ref byte[] totalbuffer)
		{
			int buffsize = 1;
			int InitSize = Marshal.SizeOf(TranslateOutput);
			int TotalSize = InitSize;
			int NeededSize = InitSize;
			bool needMoreMem = true;
			LineErrReturn ret = LineErrReturn.LINEERR_STRUCTURETOOSMALL;
			IntPtr pTransOutput;

			while (needMoreMem == true)
			{
				if ((NeededSize - TotalSize) > 0)
					buffsize = (NeededSize - InitSize + 1);
				else
					buffsize = 1;

				TranslateOutput.dwTotalSize = (uint)(buffsize + InitSize - 1);
				pTransOutput = Marshal.AllocHGlobal(NeededSize);
				Marshal.StructureToPtr(TranslateOutput, pTransOutput, true);
				ret = lineTranslateAddress(this.m_hTapi, Line.DeviceID, this.m_TapiVersion, DialNumber, 0, 0, pTransOutput);
				Marshal.PtrToStructure(pTransOutput, TranslateOutput);
				TotalSize = (int)TranslateOutput.dwTotalSize;
				NeededSize = (int)TranslateOutput.dwNeededSize;
				needMoreMem = ((ret == LineErrReturn.LINEERR_OK) && (NeededSize > TotalSize)) || (ret == LineErrReturn.LINEERR_STRUCTURETOOSMALL);
				if (needMoreMem == false)
				{
					totalbuffer = new byte[TotalSize];
					Marshal.Copy(pTransOutput, totalbuffer, 0, TotalSize);
				}
				Marshal.FreeHGlobal(pTransOutput);

			}
			return ret;
		}

		//	dials the number and returns whether this was succesful
		// note: this is an asynchronous function, so must correlate the return with
		// the status return of the LINE_REPLY event
		public int DialNumber(CLine Line, string DialNumber, out IntPtr hCall)
		{
			LineErrReturn result = LineErrReturn.LINEERR_OK;
			linetranslateoutput l_TranslateOutput = new linetranslateoutput();
			linecallparams l_callparams = new linecallparams();
			uint l_SizeOfCallParams;
			//			string l_DisplayablePhoneNum;
			string l_DialablePhoneNum;
			int l_MakeCallRequestID;
			//			int l_TransSize;
			byte[] l_buffer = null;

			hCall = IntPtr.Zero;
			System.Text.Decoder dec = null;
			StringFormat sf = Line.StringFormat;

			result = GetTranslateAddress(Line, DialNumber, ref l_TranslateOutput, ref l_buffer);
			if (result != LineErrReturn.LINEERR_OK)
			{
				return (int)result;
			}

			dec = GetDecoder(sf);
			//			l_TransSize	= Marshal.SizeOf(l_TranslateOutput);
			//
			//			l_DisplayablePhoneNum = CAPIUtils.StringFromByteArray(l_buffer, 
			//				(int)l_TranslateOutput.dwDisplayableStringOffset,
			//				(int)l_TranslateOutput.dwDisplayableStringSize, dec );
			l_DialablePhoneNum = CAPIUtils.StringFromByteArray(l_buffer,
				(int)l_TranslateOutput.dwDialableStringOffset,
				(int)l_TranslateOutput.dwDialableStringSize, dec);

			l_SizeOfCallParams = (uint)Marshal.SizeOf(l_callparams);

			// Set the Call Parameters.
			l_callparams.dwTotalSize = l_SizeOfCallParams + l_TranslateOutput.dwDisplayableStringSize;
			l_callparams.dwBearerMode = (uint)LineBearerMode.LINEBEARERMODE_VOICE;
			l_callparams.dwMediaMode = (uint)LineMediaMode.LINEMEDIAMODE_INTERACTIVEVOICE;
			l_callparams.dwCallParamFlags = (uint)LineCallParamFlags.LINECALLPARAMFLAGS_IDLE;
			l_callparams.dwAddressMode = (uint)LineAddressMode.LINEADDRESSMODE_ADDRESSID;
			l_callparams.dwAddressID = Line.Addresses[0].AddressID;
			l_callparams.dwDisplayableAddressSize = l_TranslateOutput.dwDisplayableStringSize;
			l_callparams.dwDisplayableAddressOffset = l_SizeOfCallParams;

			//	copy the Call Parameters structure and the DisplayablePhoneNum to the IntPtr
			int rawsize = (int)l_SizeOfCallParams + (int)l_TranslateOutput.dwDisplayableStringSize;
			IntPtr l_ParamBuffer = Marshal.AllocHGlobal(rawsize);
			IntPtr l_PhoneBuffer = Marshal.AllocHGlobal(
				(int)l_TranslateOutput.dwDisplayableStringSize);
			Marshal.StructureToPtr(l_callparams, l_ParamBuffer, false);
			Marshal.Copy(l_buffer, (int)l_TranslateOutput.dwDialableStringOffset,
				l_PhoneBuffer, (int)l_TranslateOutput.dwDialableStringSize);
			Marshal.WriteIntPtr(l_ParamBuffer, (int)l_SizeOfCallParams, l_PhoneBuffer);
			Marshal.FreeHGlobal(l_PhoneBuffer);

			l_MakeCallRequestID = lineMakeCall(Line.hLine, out hCall, l_DialablePhoneNum, 0, l_ParamBuffer);

			Marshal.FreeHGlobal(l_ParamBuffer);
			return l_MakeCallRequestID;
		}

		/// <summary>
		/// modified. added method for linecallparams case. no additional processing
		/// !! epte !!
		/// </summary>
		/// <param name="Line"></param>
		/// <param name="DialNumber"></param>
		/// <param name="hCall"></param>
		/// <param name="par"></param>
		/// <returns></returns>
		public int DialNumber(CLine Line, string DialNumber, out IntPtr hCall,
			linecallparams par)
		{
			//	copy the Call Parameters structure to the IntPtr
			int rawsize = Marshal.SizeOf(par);
			IntPtr l_ParamBuffer = Marshal.AllocHGlobal(rawsize);
			Marshal.StructureToPtr(par, l_ParamBuffer, false);

			int l_MakeCallRequestID =
				lineMakeCall(Line.hLine, out hCall, DialNumber, 0, l_ParamBuffer);

			Marshal.FreeHGlobal(l_ParamBuffer);
			return l_MakeCallRequestID;
		}

		//	quit the call
		public LineErrReturn HangUp(IntPtr hCall)
		{
			return lineDrop(hCall, "", 0);
		}

		/// <summary>
		/// helper method to get a decoder to use when parsing byte data into properties
		/// </summary>
		/// <param name="sf"></param>
		/// <returns></returns>
		public static System.Text.Decoder GetDecoder(CTapi.StringFormat sf)
		{
			System.Text.Encoding enc;
			System.Text.Decoder dec = null;

			// get the right decoder
			switch (sf)
			{
				case CTapi.StringFormat.STRINGFORMAT_ASCII:
					enc = new System.Text.ASCIIEncoding();
					dec = enc.GetDecoder();
					break;

				case CTapi.StringFormat.STRINGFORMAT_BINARY:
					// no encoding or decoding; just an array of bytes
					// this is a special case that will have to be handled when needed
					break;

				case CTapi.StringFormat.STRINGFORMAT_DBCS:
					enc = Encoding.GetEncoding(1252);
					dec = enc.GetDecoder();
					break;

				case CTapi.StringFormat.STRINGFORMAT_UNICODE:
					enc = new System.Text.UnicodeEncoding();
					dec = enc.GetDecoder();
					break;
			}
			return dec;
		}

		/// <summary>
		/// returns the string associated with the TapiEvent enum
		/// </summary>
		/// <param name="ev"></param>
		/// <returns></returns>
		public static string sTapiEvent(TapiEvent ev)
		{
			return Enum.Format(typeof(TapiEvent), ev, "G");
		}

		/// <summary>
		/// returns friendly string value for CallState enum
		/// </summary>
		/// <param name="lcs"></param>
		/// <returns></returns>
		public static string sCallState(CTapi.LineCallState lcs)
		{
			return Enum.Format(typeof(CTapi.LineCallState), lcs, "G");
		}

		/// <summary>
		/// returns the friendly name of the lineerr return
		/// </summary>
		/// <param name="Caller"></param>
		/// <param name="ret"></param>
		/// <returns></returns>
		public static string sLineReturn(string Caller, LineErrReturn ret)
		{
			return "Caller = " + Caller + " return = " + Enum.Format(typeof(LineErrReturn), ret, "G");
		}

		/// <summary>
		/// returns a friendly string from the StringFormat enum
		/// </summary>
		/// <param name="f"></param>
		/// <returns></returns>
		public static string sStringFormat(StringFormat f)
		{
			return Enum.Format(typeof(StringFormat), f, "G");
		}

		/// <summary>
		/// shows return values after call from LineInitializeEx -- useful for debugging
		/// </summary>
		/// <param name="ret"></param>
		public void ShowStartupInfo(CTapi.LineErrReturn ret)
		{
			StringBuilder sb = new StringBuilder(100);
			sb.Append("Return from lineInitializeEx: " + ret.ToString("X") + Environment.NewLine);
			sb.Append("Tapi Handle: " + m_hTapi.ToString() + Environment.NewLine);
			sb.Append("Number of lines = " + m_numberDevices + Environment.NewLine);
			//MessageBox.Show (sb.ToString());

		}

		/// <summary>
		/// caller should clean up by calling this in the Dispose method
		/// </summary>
		/// <returns></returns>
		public CTapi.LineErrReturn ShutDown()
		{
			this.CloseLines();
			if (this.m_hTapi != IntPtr.Zero)
				return lineShutdown(this.m_hTapi);
			else
				return CTapi.LineErrReturn.LINEERR_OK;
		}

		/// <summary>
		/// modified. added wrapper for lineGetID
		/// !! epte !!
		/// </summary>
		/// <returns></returns>
		public CTapi.LineErrReturn GetID(IntPtr hCall, out IntPtr h)
		{
			h = new IntPtr();
			varstring vs = new varstring();
			vs.dwTotalSize = (uint)Marshal.SizeOf(vs);
			LineErrReturn ler =
				lineGetID(IntPtr.Zero, 0, hCall, 0x00000004, ref vs, "comm/datamodem");
			if (LineErrReturn.LINEERR_OK != ler) return ler;
			if (vs.dwUsedSize < vs.dwNeededSize) return LineErrReturn.LINEERR_NOMEM;

			unsafe
			{
				fixed (IntPtr* p = &h)
				{
					fixed (byte* b = vs.buffer)
					{
						*p = *(IntPtr*)(b + vs.dwStringOffset - (vs.dwTotalSize - vs.buffer.Length));
					}
				}
			}
			return ler;
		}

		/// <summary>
		/// modified. added wrapper for lineAnswer
		/// !! epte !!
		/// </summary>
		/// <returns></returns>
		public CTapi.LineErrReturn Answer(IntPtr hCall)
		{
			return lineAnswer(hCall, null, 0);
		}

		#endregion // Public Methods

		#region Private Methods

		/// <summary>
		/// fills in information for all lines from lineNegotiateAPIVersion
		/// TODO: Consider moving this to CLine
		/// </summary>
		/// <returns></returns>
		private CTapi.LineErrReturn InitializeLines()
		{
			CTapi.LineErrReturn retDevCaps;
			CTapi.lineextensionid LineExtensionID;
			uint TapiVer;
			CLine line;
			this.m_CLines = new CLine[this.m_numberDevices];

			for (int i = 0; i < this.m_numberDevices; i++)
			{
				line = new CLine(this, (uint)i);
				CTapi.LineErrReturn ret = CTapi.lineNegotiateAPIVersion(this.m_hTapi, (uint)i, CTapi.TAPILOWVERSION,
				  CTapi.TAPIHIVERSION, out TapiVer, out LineExtensionID);
				if (ret == CTapi.LineErrReturn.LINEERR_OK)
				{
					line.m_TapiNegotiatedVer = TapiVer;
					line.m_LineExtensionID = LineExtensionID;
					line.m_IsCompatible = true;

					CTapi.LineErrReturn extret = CTapi.lineNegotiateExtVersion(this.m_hTapi, (uint)i,
					  line.TapiNegotiatedVer, CTapi.TSPLOWEXTVERSION, CTapi.TSPHIEXTVERSION, out line.m_ExtVersion);

					if (extret != CTapi.LineErrReturn.LINEERR_OK)
					{
						line.m_ExtVersion = 0;
					}
					retDevCaps = line.GetDeviceCaps();
					if (retDevCaps != CTapi.LineErrReturn.LINEERR_OK)
					{
						// handle error!
						line.LineValid = false;
					}
					else
					{
					}
				}
				else
				{
					line.m_IsCompatible = false;
				}
				m_CLines[i] = line;
			}
			return CTapi.LineErrReturn.LINEERR_OK;
		}

		private void CloseLines()
		{
			for (int i = 0; i < this.m_CLines.Length; ++i)
			{
				this.m_CLines[i].LineClose();
			}
		}

		private LineErrReturn DeallocateCall(IntPtr hCall)
		{
			if (hCall != IntPtr.Zero)
				return lineDeallocateCall(hCall);
			else
				return LineErrReturn.LINEERR_OK;
		}

		#endregion // Private Methods

		#region Internal Methods



		#endregion // Internal Methods

		#region IDisposable

		public void Dispose()
		{
			GC.SuppressFinalize(this);
			Dispose(true);
		}

		/// <summary>
		/// just in case the client prefers to call Close instead of Dispose
		/// </summary>
		public void Close()
		{
			Dispose();
		}

		private void Dispose(bool disposing)
		{
			lock (this)
			{
				if (disposing)
				{
					this.ShutDown();
				}
			}
		}

		#endregion // IDisposable

		#region Event Handling

		public void MainLineCallBack(System.UInt32 dwDevice, System.UInt32 dwMessage, System.UInt32 dwInstance,
		  System.UInt32 dwParam1, System.UInt32 dwParam2, System.UInt32 dwParam3)
		{
			switch ((CTapi.TapiEvent)dwMessage)
			{
				case CTapi.TapiEvent.LINE_ADDRESSSTATE:
					break;
				case CTapi.TapiEvent.LINE_CALLINFO:
					break;
				case CTapi.TapiEvent.LINE_CALLSTATE:
					{
						LineCallState lcs = (LineCallState)dwParam1;
						//LineErrReturn ret;
						IntPtr hCall = (IntPtr)dwDevice;
						OnCallStateEvent(new CallStateEventArgs(hCall, new CCall(this, hCall), lcs, dwParam2));
						// if it's IDLE, deallocate the call after all event handling
						if (lcs == LineCallState.LINECALLSTATE_IDLE)
						{
							/*ret = */
							this.DeallocateCall(hCall);
							// what do we want to do if ret is not OK?
						}
					}
					break;
				case CTapi.TapiEvent.LINE_CLOSE:
					break;
				case CTapi.TapiEvent.LINE_DEVSPECIFIC:
					break;
				case CTapi.TapiEvent.LINE_DEVSPECIFICFEATURE:
					break;
				case CTapi.TapiEvent.LINE_GATHERDIGITS:
					break;
				case CTapi.TapiEvent.LINE_GENERATE:
					break;
				case CTapi.TapiEvent.LINE_LINEDEVSTATE:
					break;
				case CTapi.TapiEvent.LINE_MONITORDIGITS:
					break;
				case CTapi.TapiEvent.LINE_MONITORMEDIA:
					break;
				case CTapi.TapiEvent.LINE_MONITORTONE:
					break;
				case CTapi.TapiEvent.LINE_REPLY:
					OnLineReply(new LineReplyEventArgs(dwParam1, dwParam2));
					break;
				case CTapi.TapiEvent.LINE_REQUEST:
					break;
				case CTapi.TapiEvent.PHONE_BUTTON:
					break;
				case CTapi.TapiEvent.PHONE_CLOSE:
					break;
				case CTapi.TapiEvent.PHONE_DEVSPECIFIC:
					break;
				case CTapi.TapiEvent.PHONE_REPLY:
					break;
				case CTapi.TapiEvent.PHONE_STATE:
					break;
				case CTapi.TapiEvent.LINE_CREATE:
					break;
				case CTapi.TapiEvent.PHONE_CREATE:
					break;
				case CTapi.TapiEvent.LINE_AGENTSPECIFIC:
					break;
				case CTapi.TapiEvent.LINE_AGENTSTATUS:
					break;
				case CTapi.TapiEvent.LINE_APPNEWCALL:
					{
						IntPtr hCall = (IntPtr)dwParam2;
						//		        IntPtr hLine = (IntPtr)dwDevice;
						uint LineDeviceID = dwParam1;
						OnAppNewCall(new AppNewCallEventArgs(this.m_CLines[(int)LineDeviceID], new CCall(this, hCall)));
					}
					break;
				case CTapi.TapiEvent.LINE_PROXYREQUEST:
					break;
				case CTapi.TapiEvent.LINE_REMOVE:
					break;
				case CTapi.TapiEvent.PHONE_REMOVE:
					break;
			} // end switch

		}

		public virtual void OnAppNewCall(AppNewCallEventArgs e)
		{
			if (AppNewCall != null)
				AppNewCall(this, e);
		}

		public virtual void OnCallStateEvent(CallStateEventArgs e)
		{
			if (CallStateEvent != null)
				CallStateEvent(this, e);
		}

		public virtual void OnLineReply(LineReplyEventArgs e)
		{
			if (LineReplyEvent != null)
				LineReplyEvent(this, e);
		}

		#endregion // Event Handling		

	} // class CTapi
} // namespaoce FORIS.TSS.IO