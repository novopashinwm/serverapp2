﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

namespace FORIS.TSS.IO.TAPI
{
	/// <summary>
	/// Class to encasulate a TAPI Line.
	/// </summary>
	public class CLine
	{
		/*
		  class CLine
		  Copyright © 2003 by Agile Software Inc.
		  All Rights Reserved.

		  Created: Helen Warn
				Updated: Christoph Brдndle
		  Updated: February 22, 2003

		*/

		#region members
		/// <summary>
		/// TODO: Change these to private and add accessors
		/// </summary>

		// owner CTapi object
		private CTapi m_CTapi = null;

		//  = 0 to (CTapi.m_numberDevices - 1), assigned when retrieving data from CTapi.lineInitializeEx
		private uint m_deviceID;

		// negotiated Tapi version returned from CTapi.lineInitializeEx
		protected internal uint m_TapiNegotiatedVer;

		// returned from CTapi.lineInitializeEx
		protected internal CTapi.lineextensionid m_LineExtensionID;

		// true if the line is compatible with the tapi versions supported. If this value is false,
		// line should not be returned by filtered search
		// this needs to be accessed by CTapi, so must be protected internal
		protected internal bool m_IsCompatible;

		private string m_ProviderInfo;

		private string m_SwitchInfo;

		private uint m_PermanentLineID;

		private string m_LineName;

		private CTapi.StringFormat m_StringFormat;

		private CTapi.LineAddressMode m_AddressModes;

		private uint m_NumAddresses;

		private CTapi.LineBearerMode m_BearerModes;

		private uint m_MaxRate;

		private CTapi.LineMediaMode m_MediaModes;

		private CTapi.LineToneMode m_GenerateToneModes;

		private uint m_GenerateToneMaxNumFreq;

		private CTapi.LineDigitMode m_GenerateDigitModes;

		private uint m_MonitorToneMaxNumFreq;

		private uint m_MonitorToneMaxNumEntries;

		private CTapi.LineDigitMode m_MonitorDigitModes;

		private uint m_GatherDigitsMinTimeout;

		private uint m_GatherDigitsMaxTimeout;

		private uint m_MedCtlDigitMaxListSize;

		private uint m_MedCtlMediaMaxListSize;

		private uint m_MedCtlCallStateMaxListSize;

		private CTapi.LineDevCapFlags m_DevCapFlags;

		private uint m_MaxNumActiveCalls;

		private CTapi.LineAnswerMode m_AnswerMode;

		private uint m_RingModes;

		private CTapi.LineDevState m_LineStates;

		private CTapi.LineFeature m_LineFeatures;

		private System.Guid m_PermanentLineGuid;

		private bool m_LineValid;

		// Extension version number dwExtVersion returned by lineNegotiateExtVersion
		// this is set to zero if we fail to negotiate an extension version
		protected internal uint m_ExtVersion;

		// this is set when the line is opened by LineOpen
		private System.IntPtr m_hLine;

		// this will be set by FillDeviceCaps
		private CAddress[] m_CAddresses;

		#endregion Members


		public CLine(CTapi Tapi, uint deviceID)
		{
			//
			// TODO: Add constructor logic here
			//
			m_deviceID = deviceID;
			m_CTapi = Tapi;
			m_hLine = System.IntPtr.Zero;
			m_ExtVersion = 0;
			m_LineValid = true;
		}

		#region Properties

		public System.IntPtr hTapi
		{ get { return this.m_CTapi.hTapi; } }

		public IntPtr hLine
		{ get { return this.m_hLine; } }

		public uint DeviceID
		{
			get { return m_deviceID; }
			//set{this.m_deviceID = value;}
		}

		public uint TapiNegotiatedVer
		{
			get { return m_TapiNegotiatedVer; }
			//set {this.m_TapiNegotiatedVer = value;}
		}

		public CTapi.lineextensionid LineExtensionID
		{
			get { return this.m_LineExtensionID; }
			//set {this.m_LineExtensionID = value;}
		}

		public uint ExtVersion
		{ get { return this.m_ExtVersion; } }

		public bool IsCompatible
		{
			get { return this.m_IsCompatible; }
			//set {this.m_IsCompatible = value;}
		}

		public string ProviderInfo
		{ get { return this.m_ProviderInfo; } }

		public string SwitchInfo
		{ get { return this.m_SwitchInfo; } }

		public uint PermanentLineID
		{ get { return this.m_PermanentLineID; } }

		public string LineName
		{ get { return this.m_LineName; } }

		public CTapi.StringFormat StringFormat
		{ get { return this.m_StringFormat; } }

		public CTapi.LineAddressMode AddressModes
		{ get { return this.m_AddressModes; } }

		public uint NumAddresses
		{ get { return this.m_NumAddresses; } }

		public CTapi.LineBearerMode BearerModes
		{ get { return this.m_BearerModes; } }

		public uint MaxRate
		{ get { return this.m_MaxRate; } }

		public CTapi.LineMediaMode MediaModes
		{ get { return this.m_MediaModes; } }

		public CTapi.LineToneMode GenerateToneModes
		{ get { return this.m_GenerateToneModes; } }

		public uint GenerateToneMaxNumFreq
		{ get { return this.m_GenerateToneMaxNumFreq; } }

		public CTapi.LineDigitMode GenerateDigitModes
		{ get { return this.m_GenerateDigitModes; } }

		public uint MonitorToneMaxNumFreq
		{ get { return this.m_MonitorToneMaxNumFreq; } }

		public uint MonitorToneMaxNumEntries
		{ get { return this.m_MonitorToneMaxNumEntries; } }

		public CTapi.LineDigitMode MonitorDigitModes
		{ get { return this.m_MonitorDigitModes; } }

		public uint GatherDigitsMinTimeout
		{ get { return this.m_GatherDigitsMinTimeout; } }

		public uint GatherDigitsMaxTimeout
		{ get { return this.m_GatherDigitsMaxTimeout; } }

		public uint MedCtlDigitMaxListSize
		{ get { return this.m_MedCtlDigitMaxListSize; } }

		public uint MedCtlMediaMaxListSize
		{ get { return this.m_MedCtlMediaMaxListSize; } }

		public uint MedCtlCallStateMaxListSize
		{ get { return this.m_MedCtlCallStateMaxListSize; } }

		public CTapi.LineDevCapFlags DevCapFlags
		{ get { return this.m_DevCapFlags; } }

		public uint MaxNumActiveCalls
		{ get { return this.m_MaxNumActiveCalls; } }

		public CTapi.LineAnswerMode AnswerMode
		{ get { return this.m_AnswerMode; } }

		public uint RingModes
		{ get { return this.m_RingModes; } }

		public CTapi.LineDevState LineStates
		{ get { return this.m_LineStates; } }

		public CTapi.LineFeature LineFeatures
		{ get { return this.m_LineFeatures; } }

		public System.Guid PermanentLineGuid
		{ get { return this.m_PermanentLineGuid; } }

		public CAddress[] Addresses
		{ get { return this.m_CAddresses; } }

		public bool LineValid
		{
			get { return this.m_LineValid; }
			set { this.m_LineValid = value; }
		}

		#endregion //Properties

		#region Public Methods

		public override string ToString()
		{
			return this.LineName;
		}

		public CTapi.LineErrReturn GetDeviceCaps()
		{
			CTapi.linedevcaps devcaps = new CTapi.linedevcaps();
			int buffsize = 1;
			int InitSize = Marshal.SizeOf(devcaps);
			int TotalSize = InitSize;
			int NeededSize = InitSize;
			bool needMoreMem = true;
			IntPtr plinedevcaps;
			byte[] totalbuffer = null;
			CTapi.LineErrReturn ret = CTapi.LineErrReturn.LINEERR_STRUCTURETOOSMALL;

			while (needMoreMem == true)
			{
				if ((NeededSize - TotalSize) > 0)
					buffsize = (NeededSize - InitSize + 1);
				else
					buffsize = 1;

				devcaps.dwTotalSize = (uint)(buffsize + InitSize - 1);
				plinedevcaps = Marshal.AllocHGlobal(NeededSize);
				Marshal.StructureToPtr(devcaps, plinedevcaps, true);
				ret = CTapi.lineGetDevCaps(this.hTapi, this.DeviceID, this.TapiNegotiatedVer, 0, plinedevcaps);
				Marshal.PtrToStructure(plinedevcaps, devcaps);
				TotalSize = (int)devcaps.dwTotalSize;
				NeededSize = (int)devcaps.dwNeededSize;
				needMoreMem = ((ret == CTapi.LineErrReturn.LINEERR_OK) && (NeededSize > TotalSize)) || (ret == CTapi.LineErrReturn.LINEERR_STRUCTURETOOSMALL);
				if (needMoreMem == false)
				{
					totalbuffer = new byte[TotalSize];
					Marshal.Copy(plinedevcaps, totalbuffer, 0, TotalSize);
				}
				Marshal.FreeHGlobal(plinedevcaps);
			}

			if (ret == CTapi.LineErrReturn.LINEERR_OK)
			{
				if (this.FillDeviceCaps(devcaps, totalbuffer) == false)
				{
					// handle error
					this.LineValid = false;
				}
				else
				{
				}
			}
			return ret;
		}
		/*
					These Methods were added by Brian Hormann 
					Best Software - CRM Division
					Date 2/27/2003		 
		 */

		public CTapi.LineErrReturn Configure(IntPtr owner)
		{
			// Can be DEVICECLASS or string.empty in last parm.
			return CTapi.lineConfigDialog(this.DeviceID, owner, string.Empty);
		}

		/// <summary>
		/// displays an application-modal dialog box that allows the 
		/// user to change the current location of a phone number 
		/// about to be dialed, adjust location and calling card parameters, 
		/// and see the effect.
		/// </summary>
		/// <param name="owner">Handle to a window to which the dialog box 
		/// is to be attached. Can be a NULL value to indicate that any 
		/// window created during the function should have no owner window.</param>
		/// <returns>Returns <see cref="LineErrReturn"/> values. 
		/// OK if the request succeeds or a other value if an error occurs. 
		/// Possible return values are:
		/// BADDEVICEID, INVALPARAM, INCOMPATIBLEAPIVERSION, INVALPOINTER, 
		/// INIFILECORRUPT, NODRIVER, INUSE, NOMEM, INVALADDRESS, 
		/// INVALAPPHANDLE, OPERATIONFAILED.
		/// </returns>
		public CTapi.LineErrReturn ConfigureLocation(IntPtr owner)
		{
			return CTapi.lineTranslateDialog(this.hTapi, this.DeviceID, this.TapiNegotiatedVer, owner, string.Empty);
		}

		/// <summary>
		/// fill in the address caps for each address
		/// </summary>
		/// <returns></returns>
		public CTapi.LineErrReturn FillAddressCaps()
		{
			CTapi.LineErrReturn ret = CTapi.LineErrReturn.LINEERR_OK;
			CAddress addr;
			for (int i = 0; i < this.NumAddresses; ++i)
			{
				addr = new CAddress(this, (uint)i, this.ExtVersion);
				ret = addr.GetAddrCaps();
				if (ret == CTapi.LineErrReturn.LINEERR_OK)
					addr.AddressValid = true;
				else
				{
					// handle error
					addr.AddressValid = false;
				}
				this.Addresses[i] = addr;
			}
			return ret;
		}

		/// <summary>
		/// returns a string of line info -- useful for debugging!
		/// </summary>
		/// <returns></returns>
		public string GetLineInfoString()
		{
			StringBuilder sb = new StringBuilder(600);
			sb.Append("Device ID: " + this.DeviceID + Environment.NewLine);
			sb.Append("Line Name: " + this.LineName + Environment.NewLine);
			sb.Append("Provider Info: " + this.ProviderInfo + Environment.NewLine);
			sb.Append("Switch Info: " + this.SwitchInfo + Environment.NewLine);
			sb.Append("Address Modes: " + this.AddressModes + Environment.NewLine);
			sb.Append("Bearer Modes: " + this.BearerModes + Environment.NewLine);
			sb.Append("Media Modes: " + this.MediaModes + Environment.NewLine);
			sb.Append("Is Voice Line: " + this.IsVoiceLine().ToString() + Environment.NewLine);
			sb.Append("Number of Addresses: " + this.NumAddresses.ToString() + Environment.NewLine);
			sb.Append("Line Features: " + this.LineFeatures + Environment.NewLine);
			sb.Append("Extension Version: " + this.ExtVersion.ToString("X") + Environment.NewLine);

			return sb.ToString();
		}

		/// <summary>
		/// returns true if the line can support a voice call
		/// </summary>
		/// <returns></returns>
		public bool IsVoiceLine()
		{
			if (((this.BearerModes & CTapi.LineBearerMode.LINEBEARERMODE_VOICE) != 0)
			  && ((this.MediaModes & CTapi.LineMediaMode.LINEMEDIAMODE_INTERACTIVEVOICE) != 0)
			  && ((this.LineFeatures & CTapi.LineFeature.LINEFEATURE_MAKECALL) != 0))
				return true;
			else
				return false;
		}

		/// <summary>
		/// close a line if it is open
		/// </summary>
		/// <returns></returns>
		public CTapi.LineErrReturn LineClose()
		{
			if (this.m_hLine != IntPtr.Zero)
				return CTapi.lineClose(this.m_hLine);
			else
				return CTapi.LineErrReturn.LINEERR_OK;
		}
		/// <summary>
		/// modified. explicit LineMediaMode.LINEMEDIAMODE_DATAMODEM instead of m_MediaModes
		/// !! epte !!
		/// </summary>
		/// <param name="deviceID"></param>
		/// <param name="priv"></param>
		/// <returns></returns>
		public CTapi.LineErrReturn LineOpen(uint deviceID, CTapi.LineCallPrivilege priv)
		{
			if (priv == CTapi.LineCallPrivilege.LINECALLPRIVILEGE_NONE)
				return CTapi.LineErrReturn.LINEERR_INVALCALLPRIVILEGE;
			if (this.IsCompatible == false)
				return CTapi.LineErrReturn.LINEERR_INCOMPATIBLEAPIVERSION;
			CTapi.linecallparams callparams = new CTapi.linecallparams();
			return CTapi.lineOpen(this.hTapi, deviceID, out this.m_hLine, this.TapiNegotiatedVer,
				this.m_ExtVersion, this.m_CTapi.CallBack, priv
				, CTapi.LineMediaMode.LINEMEDIAMODE_DATAMODEM   //this.m_MediaModes
				, callparams);
		}
		#endregion // Public Methods

		#region Private Methods
		/// <summary>
		/// fill the line properties from the linedevcaps structure
		/// </summary>
		/// <param name="?"></param>
		/// <returns></returns>
		private bool FillDeviceCaps(CTapi.linedevcaps devcaps, byte[] buffer)
		{
			try
			{
				System.Text.Decoder dec = null;
				this.m_StringFormat = (CTapi.StringFormat)devcaps.dwStringFormat;
				dec = CTapi.GetDecoder(this.m_StringFormat);

				// now start filling in the values
				this.m_ProviderInfo = CAPIUtils.StringFromByteArray(buffer, (int)devcaps.dwProviderInfoOffset, (int)devcaps.dwProviderInfoSize, dec);
				this.m_SwitchInfo = CAPIUtils.StringFromByteArray(buffer, (int)devcaps.dwSwitchInfoOffset, (int)devcaps.dwSwitchInfoSize, dec);
				this.m_PermanentLineID = devcaps.dwPermanentLineID;
				this.m_LineName = CAPIUtils.StringFromByteArray(buffer, (int)devcaps.dwLineNameOffset, (int)devcaps.dwLineNameSize, dec);
				this.m_AddressModes = (CTapi.LineAddressMode)devcaps.dwAddressModes;
				this.m_NumAddresses = devcaps.dwNumAddresses;
				this.m_CAddresses = new CAddress[this.m_NumAddresses];
				this.m_BearerModes = (CTapi.LineBearerMode)devcaps.dwBearerModes;
				this.m_MaxRate = devcaps.dwMaxRate;
				this.m_MediaModes = (CTapi.LineMediaMode)devcaps.dwMediaModes;
				this.m_LineFeatures = (CTapi.LineFeature)devcaps.dwLineFeatures;

				return true;
			}
			catch (Exception ex)
			{
				//MessageBox.Show(ex.Message);
				Debug.WriteLine("!!ERR!!" + ex.Message);
				return false;
			}
		}

		#endregion // Private Methods		

	} // class CLine

	/// <summary>
	/// Collection class for line objects. <see cref="Line"/>
	/// </summary>
	public sealed class LineCollection : IEnumerable
	{
		/*
				class LineCollection
				Copyright © 2003 by Best Software - CRM Divison.
				All Rights Reserved.

				Created: Brian Hormann
				Updated: February 22, 2003

			*/
		private ArrayList lines;

		internal LineCollection()
		{
			lines = new ArrayList();
		}
		internal LineCollection(int numberLines)
		{
			lines = new ArrayList(numberLines);
		}

		public IEnumerator GetEnumerator()
		{
			return lines.GetEnumerator();
		}

		public void Add(CLine line)
		{
			lines.Add(line);
		}

		public int Count
		{
			get { return lines.Count; }
		}

		public void Clear()
		{
			lines.Clear();
		}

		public void Remove(CLine line)
		{
			lines.Remove(line);
		}

		public bool Contains(CLine line)
		{
			return lines.Contains(line);
		}

		public CLine Item(int index)
		{
			if (index < 0 || index > lines.Count - 1)
				throw new ArgumentOutOfRangeException("index", index, "Needs to between 0 and count -1");

			return (CLine)lines[index];

		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public CLine[] ToArray()
		{
			return (CLine[])this.lines.ToArray(typeof(CLine));
		}
	} // LineCollection

} // namespace FORIS.TSS.IO