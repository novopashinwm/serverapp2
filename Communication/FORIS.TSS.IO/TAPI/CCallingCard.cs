﻿using System.Collections;

namespace FORIS.TSS.IO.TAPI
{
	/// <summary>
	/// Tapi Calling card Class
	/// </summary>
	public class CallingCard
	{
		/*
			class CallingCard
			Copyright © 2003 by Best Software - CRM Divison.
			All Rights Reserved.

			Created: Brian Hormann
			Updated: February 22, 2003

		*/

		internal int permanentCardID;
		internal string name;
		internal int numberDigits;
		internal string sameAreaDialingRule;
		internal string longDistanceDialingRule;
		internal string internationalDialingRule;
		internal CTapi.LineCardOption options;

		public CallingCard()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		#region Public
		/// <summary>
		/// Permanent identifier that identifies the card. 
		/// </summary>
		public int PermanentCardID
		{
			get { return permanentCardID; }
		}
		/// <summary>
		/// string that describes the card in a user-friendly manner
		/// </summary>
		public string Name
		{
			get { return name; }
		}
		/// <summary>
		/// Number of digits in the existing card number. 
		/// The card number itself is not returned for security 
		/// reasons (it is stored in scrambled form by TAPI). 
		/// The application can use this to insert filler bytes 
		/// into a text control in "password" mode to show that 
		/// a number exists. 
		/// </summary>
		public int NumberDigits
		{
			get { return numberDigits; }
		}
		/// <summary>
		/// dialing rule defined for calls to numbers in the same area code.
		/// </summary>
		public string SameAreaDialingRule
		{
			get { return sameAreaDialingRule; }
		}
		/// <summary>
		/// dialing rule defined for calls to numbers in the 
		/// other areas in the same country/region. 
		/// The rule is a null-terminated string.
		/// </summary>
		public string LongDistanceDialingRule
		{
			get { return longDistanceDialingRule; }
		}
		/// <summary>
		/// dialing rule defined for calls to numbers in other 
		/// countries/regions
		/// </summary>
		public string InternationalDialingRule
		{
			get { return internationalDialingRule; }
		}
		/// <summary>
		/// Indicates other settings associated 
		/// with this calling card, using the <see cref="LineCardOption"/>
		/// </summary>
		public CTapi.LineCardOption Options
		{
			get { return options; }
		}
		#endregion

	}


	public sealed class CallingCardCollection : IEnumerable
	{
		/*
			class CallingCardCollection
			Copyright © 2003 by Best Software - CRM Divison.
			All Rights Reserved.

			Created: Brian Hormann
			Updated: February 22, 2003

		*/
		private ArrayList callingCards;

		internal CallingCardCollection()
		{
			callingCards = new ArrayList();
		}
		internal CallingCardCollection(int numberCallingCards)
		{
			callingCards = new ArrayList(numberCallingCards);
		}

		public IEnumerator GetEnumerator()
		{
			return callingCards.GetEnumerator();
		}

		public void Add(CallingCard callingCard)
		{
			callingCards.Add(callingCard);
		}

		public int Count
		{
			get { return callingCards.Count; }
		}

		public void Clear()
		{
			callingCards.Clear();
		}

		public void Remove(CallingCard callingCard)
		{
			callingCards.Remove(callingCard);
		}

		public bool Contains(CallingCard callingCard)
		{
			return callingCards.Contains(callingCard);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public CallingCard[] ToArray()
		{
			return (CallingCard[])this.callingCards.ToArray(typeof(CallingCard));
		}
	}
}