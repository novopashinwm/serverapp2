﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Text;

namespace FORIS.TSS.IO.TAPI
{
	/// <summary>
	/// Tapi Locations.  Builds the Location Collection and gets the default 
	/// location information.
	/// </summary>
	public class CLocations
	{
		/*
			class Locations
			Copyright © 2003 by Best Software - CRM Divison.
			All Rights Reserved.

			Created: Brian Hormann
			Updated: February 22, 2003

		*/
		#region Private Members
		int currentLocation;
		CTapi tapi;

		LocationCollection locations;
		//int numberCallingCards;
		CallingCardCollection callingCards;
		//CallingCard[] callingCards;

		int currentPreferredCardID;

		#endregion

		#region Constr's
		public CLocations(CTapi tapi)
		{
			locations = new LocationCollection();
			callingCards = new CallingCardCollection();
			this.tapi = tapi;
			GetData();
		}

		#endregion

		#region Public

		public int CurrentLocation
		{
			get { return this.currentLocation; }
		}
		public int CurrentPreferredCardID
		{
			get { return currentPreferredCardID; }
		}
		public LocationCollection LocationList
		{
			get { return locations; }
		}

		/// <summary>
		/// TODO: Code this 
		/// </summary>
		/// <returns></returns>
		//		public Location Current
		//		{
		//			get
		//			{
		//	
		//			}
		//		}
		#endregion

		#region Helpers
		public CTapi.LineErrReturn GetData()
		{
			CTapi.LineErrReturn ret = CTapi.LineErrReturn.LINEERR_STRUCTURETOOSMALL;
			HelperStruct hStruct = new HelperStruct();
			int buffsize = 1;
			byte[] totalbuffer = null;
			int InitSize = Marshal.SizeOf(hStruct);
			int TotalSize = InitSize;
			int NeededSize = InitSize;
			bool needMoreMem = true;
			IntPtr phStruct;

			while (needMoreMem == true)
			{
				if ((NeededSize - TotalSize) > 0)
					buffsize = (NeededSize - InitSize + 1);
				else
					buffsize = 1;

				hStruct.TotalSize = (uint)(buffsize + InitSize - 1);
				phStruct = Marshal.AllocHGlobal(NeededSize);
				Marshal.StructureToPtr(hStruct, phStruct, true);

				ret = lineGetTranslateCaps(tapi.hTapi, CTapi.TAPIHIVERSION, phStruct);

				Marshal.PtrToStructure(phStruct, hStruct);
				TotalSize = (int)hStruct.TotalSize;
				NeededSize = (int)hStruct.NeededSize;
				needMoreMem =
					((ret == CTapi.LineErrReturn.LINEERR_OK)
					&& (NeededSize > TotalSize)) ||
					(ret == CTapi.LineErrReturn.LINEERR_STRUCTURETOOSMALL);
				if (needMoreMem == false)
				{
					totalbuffer = new byte[TotalSize];
					Marshal.Copy(phStruct, totalbuffer, 0, TotalSize);
				}
				Marshal.FreeHGlobal(phStruct);
			}

			//numberLocations = (int)hStruct.NumLocations;
			if ((int)hStruct.NumLocations > 0)
			{
				locations = new LocationCollection((int)hStruct.NumLocations);
				//locations = new Location[numberLocations];
				PopulateLocations(totalbuffer, hStruct, (int)hStruct.NumLocations);
			}

			currentLocation = (int)hStruct.CurrentLocationID;

			//numberCallingCards = (int)hStruct.NumCards;
			if ((int)hStruct.NumCards > 0)
			{
				callingCards = new CallingCardCollection((int)hStruct.NumCards);
				PopulateCallingCards(totalbuffer, hStruct, (int)hStruct.NumCards);
			}
			currentPreferredCardID = (int)hStruct.CurrentPreferredCardID;
			return ret;
		}

		private void PopulateLocations(byte[] arr, HelperStruct hStruct, int numberLocations)
		{
			//Used to make call and fill structure.
			Location location;
			int currentOffset = (int)hStruct.LocationListOffset;
			System.Text.Decoder dec = new System.Text.UnicodeEncoding().GetDecoder();

			for (int i = 0; i < numberLocations; i++)
			{
				LineLocationEntry lineLocation = new LineLocationEntry();
				lineLocation.PermanentLocationID = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				lineLocation.LocationNameSize = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				lineLocation.LocationNameOffset = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				lineLocation.CountryCode = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				lineLocation.CityCodeSize = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				lineLocation.CityCodeOffset = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				lineLocation.PreferredCardID = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				lineLocation.LocalAccessCodeSize = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				lineLocation.LocalAccessCodeOffset = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				lineLocation.LongDistanceAccessCodeSize = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				lineLocation.LongDistanceAccessCodeOffset = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				lineLocation.TollPrefixListSize = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				lineLocation.TollPrefixListOffset = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				lineLocation.CountryID = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				lineLocation.Options = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				lineLocation.CancelCallWaitingSize = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				lineLocation.CancelCallWaitingOffset = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;

				location = new Location();
				// Now that we have all the value for the helper stuct now populate the
				// location class with values.
				location.permanentID = lineLocation.PermanentLocationID;
				location.name = CAPIUtils.StringFromByteArray
					(arr, (int)lineLocation.LocationNameOffset,
					(int)lineLocation.LocationNameSize, dec);
				location.countryCode = lineLocation.CountryCode;
				location.cityCode = CAPIUtils.StringFromByteArray
					(arr, (int)lineLocation.CityCodeOffset,
					(int)lineLocation.CityCodeSize, dec);
				location.preferredCardID = lineLocation.PreferredCardID;
				location.localAccessCode = CAPIUtils.StringFromByteArray
					(arr, (int)lineLocation.LocalAccessCodeOffset,
					(int)lineLocation.LocalAccessCodeSize, dec);
				location.longDistanceAccessCode = CAPIUtils.StringFromByteArray
					(arr, (int)lineLocation.LongDistanceAccessCodeOffset,
					(int)lineLocation.LongDistanceAccessCodeSize, dec);
				location.tollPrefixList = CAPIUtils.StringFromByteArray
					(arr, (int)lineLocation.TollPrefixListOffset,
					(int)lineLocation.TollPrefixListSize, dec);
				location.countryID = lineLocation.CountryID;
				location.options = lineLocation.Options;
				location.cancelCallWaiting = CAPIUtils.StringFromByteArray
					(arr, (int)lineLocation.CancelCallWaitingOffset,
					(int)lineLocation.CancelCallWaitingSize, dec);
				locations.Add(location);
			}
		}

		private void PopulateCallingCards(byte[] arr, HelperStruct hStruct, int numberCallingCards)
		{
			CallingCard callingCard;
			//Used to make call and fill structure.
			int currentOffset = (int)hStruct.CardListOffset;
			System.Text.Decoder dec = new System.Text.UnicodeEncoding().GetDecoder();

			for (int i = 0; i < numberCallingCards; i++)
			{
				LineCardEntry callingCardEntry = new LineCardEntry();
				callingCardEntry.PermanentCardID = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				callingCardEntry.CardNameSize = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				callingCardEntry.CardNameOffset = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				callingCardEntry.CardNumberDigits = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				callingCardEntry.SameAreaRuleSize = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				callingCardEntry.SameAreaRuleOffset = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				callingCardEntry.LongDistanceRuleSize = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				callingCardEntry.LongDistanceRuleOffset = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				callingCardEntry.InternationalRuleSize = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				callingCardEntry.InternationalRuleOffset = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;
				callingCardEntry.Options = BitConverter.ToInt32(arr, currentOffset);
				currentOffset += 4;

				callingCard = new CallingCard();

				// Now that we have all the value for the helper stuct now populate the
				// callingcard class with values.
				callingCard.permanentCardID = callingCardEntry.PermanentCardID;
				callingCard.name = CAPIUtils.StringFromByteArray
					(arr, (int)callingCardEntry.CardNameOffset,
					(int)callingCardEntry.CardNameSize, dec);
				callingCard.numberDigits = (int)callingCardEntry.CardNumberDigits;
				callingCard.sameAreaDialingRule = CAPIUtils.StringFromByteArray
					(arr, (int)callingCardEntry.SameAreaRuleOffset,
					(int)callingCardEntry.SameAreaRuleSize, dec);
				callingCard.longDistanceDialingRule = CAPIUtils.StringFromByteArray
					(arr, (int)callingCardEntry.LongDistanceRuleOffset,
					(int)callingCardEntry.LongDistanceRuleSize, dec);
				callingCard.internationalDialingRule = CAPIUtils.StringFromByteArray
					(arr, (int)callingCardEntry.InternationalRuleOffset,
					(int)callingCardEntry.InternationalRuleSize, dec);
				callingCard.options = (CTapi.LineCardOption)callingCardEntry.Options;
				// increment the counter.
				this.callingCards.Add(callingCard);
			}
		}
		#endregion

		#region Internal Stucts for class Data Population 
		[StructLayout(LayoutKind.Sequential)]
		internal class HelperStruct
		{
			internal uint TotalSize;
			internal uint NeededSize;
			internal uint UsedSize;
			internal uint NumLocations;
			internal uint LocationListSize;
			internal uint LocationListOffset;
			internal uint CurrentLocationID;
			internal uint NumCards;
			internal uint CardListSize;
			internal uint CardListOffset;
			internal uint CurrentPreferredCardID;
		}

		[StructLayout(LayoutKind.Sequential)]
		internal class LineLocationEntry
		{
			internal int PermanentLocationID;
			internal int LocationNameSize;
			internal int LocationNameOffset;
			internal int CountryCode;
			internal int CityCodeSize;
			internal int CityCodeOffset;
			internal int PreferredCardID;
			internal int LocalAccessCodeSize;                          // TAPI v1.4
			internal int LocalAccessCodeOffset;                        // TAPI v1.4
			internal int LongDistanceAccessCodeSize;                   // TAPI v1.4
			internal int LongDistanceAccessCodeOffset;                 // TAPI v1.4
			internal int TollPrefixListSize;                           // TAPI v1.4
			internal int TollPrefixListOffset;                         // TAPI v1.4
			internal int CountryID;                                    // TAPI v1.4
			internal int Options;                                      // TAPI v1.4
			internal int CancelCallWaitingSize;                        // TAPI v1.4
			internal int CancelCallWaitingOffset;
		}

		[StructLayout(LayoutKind.Sequential)]
		internal class LineCardEntry
		{
			internal int PermanentCardID;
			internal int CardNameSize;
			internal int CardNameOffset;
			internal int CardNumberDigits;                             // TAPI v1.4
			internal int SameAreaRuleSize;                             // TAPI v1.4
			internal int SameAreaRuleOffset;                           // TAPI v1.4
			internal int LongDistanceRuleSize;                         // TAPI v1.4
			internal int LongDistanceRuleOffset;                       // TAPI v1.4
			internal int InternationalRuleSize;                        // TAPI v1.4
			internal int InternationalRuleOffset;                      // TAPI v1.4
			internal int Options;
		}

		#endregion
		#region TAPI External API's
		/// <summary>
		/// 
		/// </summary>
		/// <param name="hLineApp"></param>
		/// <param name="dwAPIVersion"></param>
		/// <param name="lpTranslateCaps"></param>
		/// <returns></returns>
		[DllImport("Tapi32.dll", EntryPoint = "lineGetTranslateCapsW", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern CTapi.LineErrReturn
			lineGetTranslateCaps(IntPtr hLineApp, uint dwAPIVersion, IntPtr lpTranslateCaps);
		#endregion

	}

	public sealed class LocationCollection : IEnumerable
	{
		/*
			class LocationCollection
			Copyright © 2003 by Best Software - CRM Divison.
			All Rights Reserved.
	  
			Created: Brian Hormann
			Updated: February 22, 2003

		*/
		private ArrayList locations;

		internal LocationCollection()
		{
			locations = new ArrayList();
		}
		internal LocationCollection(int numberLocations)
		{
			locations = new ArrayList(numberLocations);
		}

		public IEnumerator GetEnumerator()
		{
			return locations.GetEnumerator();
		}

		public void Add(Location location)
		{
			locations.Add(location);
		}

		public int Count
		{
			get { return locations.Count; }
		}

		public void Clear()
		{
			locations.Clear();
		}

		public void Remove(Location location)
		{
			locations.Remove(location);
		}

		public bool Contains(Location location)
		{
			return locations.Contains(location);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public Location[] ToArray()
		{
			return (Location[])this.locations.ToArray(typeof(Location));
		}
	}
	/// <summary>
	/// Summary description for Location.
	/// </summary>
	public class Location
	{
		/*
			class Location
			Copyright © 2003 by Best Software - CRM Divison.
			All Rights Reserved.

			Created: Brian Hormann
			Updated: February 22, 2003

		*/
		#region Private
		internal int permanentID;
		internal string name;
		internal int countryCode;
		internal string cityCode;
		internal int preferredCardID;
		internal string localAccessCode;
		internal string longDistanceAccessCode;
		internal string tollPrefixList;
		internal int countryID;
		internal int options;
		internal string cancelCallWaiting;

		#endregion

		#region Constr's
		public Location()
		{ }

		#endregion

		#region Public
		/// <summary>
		/// Permanent identifier that identifies the location.
		/// </summary>
		public int PermanentID
		{
			get { return permanentID; }
		}
		/// <summary>
		///	string that describes the location in a user-friendly manner
		/// </summary>
		public string Name
		{
			get { return name; }
		}
		/// <summary>
		/// Country code of the location
		/// </summary>
		public int CountryCode
		{
			get { return countryCode; }
		}
		/// <summary>
		/// string specifying the city/area code associated with the location. 
		/// This information, along with the country code, can be used by 
		/// applications to "default" entry fields for the user when 
		/// entering phone numbers, to encourage the entry of proper 
		/// canonical numbers.
		/// </summary>
		public string CityCode
		{
			get { return cityCode; }
		}
		/// <summary>
		/// Preferred calling card when dialing from this location. 
		/// </summary>
		public int PreferredCardID
		{
			get { return preferredCardID; }
		}
		/// <summary>
		/// string containing the access code to be 
		/// dialed before calls to addresses in the local calling area.
		/// </summary>
		public string LocalAccessCode
		{
			get { return localAccessCode; }
		}
		/// <summary>
		/// string containing the access code to be dialed before 
		/// calls to addresses outside the local calling area. 
		/// </summary>
		public string LongDistanceAccessCode
		{
			get { return longDistanceAccessCode; }
		}

		/// <summary>
		/// string containing the toll prefix list for the location. 
		/// The string contains only prefixes consisting of the 
		/// digits "0" through "9", separated from each other 
		/// by a single "," (comma) character.
		/// </summary>
		public string TollPrefixList
		{
			get { return tollPrefixList; }
		}
		/// <summary>
		/// Country identifier of the country/region selected 
		/// for the location. This can be used with the lineGetCountry 
		/// function to obtain additional information about the 
		/// specific country/region, such as the country/region name 
		/// </summary>
		public int CountryID
		{
			get { return countryID; }
		}
		/// <summary>
		/// Options in effect for this location
		/// </summary>
		public int Options
		{
			get { return options; }
		}
		/// <summary>
		/// string containing the dial digits and modifier characters 
		/// that should be prefixed to the dialable string (after 
		/// the pulse/tone character) when an application sets the 
		/// LINETRANSLATEOPTION_CANCELCALLWAITING bit in the 
		/// dwTranslateOptions parameter of lineTranslateAddress
		/// </summary>
		public string CancelCallWaiting
		{
			get { return cancelCallWaiting; }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return name;
		}

		public string GetInfoString()
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("PermanentID = " + permanentID.ToString() + Environment.NewLine);
			sb.Append("Name = " + name.ToString() + Environment.NewLine);
			sb.Append("Country Code = " + countryCode + Environment.NewLine);
			sb.Append("City Code = " + cityCode + Environment.NewLine);
			sb.Append("Preferred Card ID = " + preferredCardID + Environment.NewLine);
			sb.Append("Local Access Code = " + localAccessCode + Environment.NewLine);
			sb.Append("Long Distance Access Code = " + preferredCardID + Environment.NewLine);
			sb.Append("Toll Prefix List = " + tollPrefixList + Environment.NewLine);
			sb.Append("Country ID = " + countryID + Environment.NewLine);
			sb.Append("Options = " + options + Environment.NewLine);
			sb.Append("Cancel Call Waiting = " + cancelCallWaiting + Environment.NewLine);
			return sb.ToString();
		}

		#endregion
	}
}