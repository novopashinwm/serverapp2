﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Text;

namespace FORIS.TSS.IO.TAPI
{
	/// <summary>
	/// Tapi Country List class.
	/// Populates a CountryListCollection 
	/// with country values for TAPI.
	/// </summary>
	public class CCountries
	{

		/*
			class CCountries
			Copyright © 2003 by Best Software - CRM Divison.
			All Rights Reserved.

			Created: Brian Hormann
			Updated: February 22, 2003

		*/

		#region Private Members
		CountryListCollection m_countries;

		CTapi m_CTapi;
		#endregion

		#region Constr's
		public CCountries(CTapi tapi)
		{
			this.m_CTapi = tapi;
			this.m_countries = new CountryListCollection();
			GetData();
		}
		#endregion

		#region Public
		public CountryListCollection CountryList
		{
			get { return m_countries; }
		}

		#endregion

		#region Helpers
		internal CTapi.LineErrReturn GetData()
		{
			this.m_countries = new CountryListCollection();
			CTapi.LineErrReturn ret = CTapi.LineErrReturn.LINEERR_STRUCTURETOOSMALL;
			HelperStruct hStruct = null;
			int countryId = 1;          // Starting Country.
										// Loop while there is a next country id value and use this value to
										// get the country in the loop.
			do
			{
				byte[] totalbuffer = null;
				FillStruct(ref hStruct, countryId, ref totalbuffer);

				//locations = new Location[numberLocations];
				CCountry country = PopulateCountry(hStruct, totalbuffer);
				if (country != null)
				{
					m_countries.Add(country);
					countryId = country.NextCountryID;
				}
			}
			while (countryId > 0);
			return ret;
		}

		private CCountry PopulateCountry(HelperStruct hStruct, byte[] arr)
		{
			//Used to make call and fill structure.
			CCountry country = null;
			int currentOffset = (int)hStruct.CountryListOffset;
			System.Text.Decoder dec = new System.Text.UnicodeEncoding().GetDecoder();

			LineCountryEntry countryEntry = new LineCountryEntry();
			countryEntry.CountryID = BitConverter.ToInt32(arr, currentOffset);
			currentOffset += 4;
			countryEntry.CountryCode = BitConverter.ToInt32(arr, currentOffset);
			currentOffset += 4;
			countryEntry.NextCountryID = BitConverter.ToInt32(arr, currentOffset);
			currentOffset += 4;
			countryEntry.CountryNameSize = BitConverter.ToInt32(arr, currentOffset);
			currentOffset += 4;
			countryEntry.CountryNameOffset = BitConverter.ToInt32(arr, currentOffset);
			currentOffset += 4;
			countryEntry.SameAreaRuleSize = BitConverter.ToInt32(arr, currentOffset);
			currentOffset += 4;
			countryEntry.SameAreaRuleOffset = BitConverter.ToInt32(arr, currentOffset);
			currentOffset += 4;
			countryEntry.LongDistanceRuleSize = BitConverter.ToInt32(arr, currentOffset);
			currentOffset += 4;
			countryEntry.LongDistanceRuleOffset = BitConverter.ToInt32(arr, currentOffset);
			currentOffset += 4;
			countryEntry.InternationalRuleSize = BitConverter.ToInt32(arr, currentOffset);
			currentOffset += 4;
			countryEntry.InternationalRuleOffset = BitConverter.ToInt32(arr, currentOffset);
			currentOffset += 4;

			country = new CCountry();
			// Now that we have all the value for the helper stuct now populate the
			// location class with values.
			country.m_countryId = countryEntry.CountryID;
			country.m_code = countryEntry.CountryCode;
			country.m_nextCountryID = countryEntry.NextCountryID;
			country.m_name = CAPIUtils.StringFromByteArray
				(arr, (int)countryEntry.CountryNameOffset,
				(int)countryEntry.CountryNameSize, dec);

			country.m_sameAreaRule = CAPIUtils.StringFromByteArray
				(arr, (int)countryEntry.SameAreaRuleOffset,
				(int)countryEntry.SameAreaRuleSize, dec);

			country.m_longDistanceRule = CAPIUtils.StringFromByteArray
				(arr, (int)countryEntry.LongDistanceRuleOffset,
				(int)countryEntry.LongDistanceRuleSize, dec);

			country.m_internationalRule = CAPIUtils.StringFromByteArray
				(arr, (int)countryEntry.InternationalRuleOffset,
				(int)countryEntry.InternationalRuleSize, dec);

			return country;
		}

		private CTapi.LineErrReturn FillStruct(ref HelperStruct hStruct, int countryId, ref byte[] totalbuffer)
		{
			CTapi.LineErrReturn ret = CTapi.LineErrReturn.LINEERR_STRUCTURETOOSMALL;

			hStruct = new HelperStruct();
			int buffsize = 1;
			int InitSize = Marshal.SizeOf(hStruct);
			int TotalSize = InitSize;
			int NeededSize = InitSize;
			bool needMoreMem = true;
			IntPtr phStruct;

			while (needMoreMem == true)
			{
				if ((NeededSize - TotalSize) > 0)
					buffsize = (NeededSize - InitSize + 1);
				else
					buffsize = 1;

				hStruct.TotalSize = (uint)(buffsize + InitSize - 1);
				phStruct = Marshal.AllocHGlobal(NeededSize);
				Marshal.StructureToPtr(hStruct, phStruct, true);

				ret = CTapi.lineGetCountry(countryId, (int)CTapi.TAPIHIVERSION, phStruct);

				Marshal.PtrToStructure(phStruct, hStruct);
				TotalSize = (int)hStruct.TotalSize;
				NeededSize = (int)hStruct.NeededSize;
				needMoreMem =
					((ret == CTapi.LineErrReturn.LINEERR_OK)
					&& (NeededSize > TotalSize)) ||
					(ret == CTapi.LineErrReturn.LINEERR_STRUCTURETOOSMALL);
				if (needMoreMem == false)
				{
					totalbuffer = new byte[TotalSize];
					Marshal.Copy(phStruct, totalbuffer, 0, TotalSize);
				}
				Marshal.FreeHGlobal(phStruct);
			}
			return ret;
		}
		#endregion

		#region Internal Stucts for class Data Population 
		[StructLayout(LayoutKind.Sequential)]
		internal class HelperStruct
		{
			internal uint TotalSize;
			internal uint NeededSize;
			internal uint UsedSize;
			internal uint NumCountries;
			internal uint CountryListSize;
			internal uint CountryListOffset;
		}
		/// <summary>
		/// Internal structure for getting the 
		/// Country Information.  This information
		/// will then be used to fill the country class 
		/// with information that class will then be 
		/// added into the country collection class.
		/// </summary>
		internal class LineCountryEntry
		{
			internal int CountryID;
			internal int CountryCode;
			internal int NextCountryID;
			internal int CountryNameSize;
			internal int CountryNameOffset;
			internal int SameAreaRuleSize;
			internal int SameAreaRuleOffset;
			internal int LongDistanceRuleSize;
			internal int LongDistanceRuleOffset;
			internal int InternationalRuleSize;
			internal int InternationalRuleOffset;
		}

		#endregion

	} // class CCountries

	/// <summary>
	/// Summary description for CallingCard.
	/// </summary>
	public class CCountry
	{
		/*
			class Country
			Copyright © 2003 by Best Software - CRM Divison.
			All Rights Reserved.

			Created: Brian Hormann
			Updated: February 22, 2003

		*/
		internal int m_countryId;
		internal int m_code;
		internal int m_nextCountryID;
		internal string m_name;
		internal string m_sameAreaRule;
		internal string m_longDistanceRule;
		internal string m_internationalRule;

		public CCountry()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		#region Public
		/// <summary>
		/// Country/region identifier of the entry. 
		/// The country/region identifier is an internal 
		/// identifier that allows multiple entries to exist 
		/// in the country/region list with the same country code 
		/// (for example, all countries in North America and the 
		/// Caribbean share country code 1, but require separate 
		/// entries in the list). 
		/// </summary>
		public int ID
		{
			get { return m_countryId; }
		}
		/// <summary>
		/// Country code of the country/region represented by the entry 
		/// (that is, the digits that would be dialed in an international call). 
		/// Only this value should ever be displayed to users 
		/// (country identifiers should never be displayed, as 
		/// they would be confusing).
		/// </summary>
		public int Code
		{
			get { return m_code; }
		}

		/// <summary>
		/// Country identifier of the next entry in the country/region list. 
		/// Because country codes and identifiers are not assigned in any 
		/// regular numeric sequence, the country/region list is a 
		/// single linked list, with each entry pointing to the next. 
		/// The last country/region in the list has a NextCountryID value of 
		/// zero. When the LINECOUNTRYLIST structure is used to obtain 
		/// the entire list, the entries in the list are in sequence as 
		/// linked by their NextCountryID members. 
		/// </summary>
		public int NextCountryID
		{
			get { return m_nextCountryID; }
		}
		/// <summary>
		/// string that specifies the name of the country/region
		/// </summary>
		public string Name
		{
			get { return m_name; }
		}

		/// <summary>
		/// string containing the dialing rule for 
		/// direct-dialed calls to the same area code
		/// </summary>
		public string SameAreaRule
		{
			get { return m_sameAreaRule; }
		}
		/// <summary>
		/// string containing the dialing rule for direct-dialed calls 
		/// to other areas in the same country/region
		/// </summary>
		public string LongDistanceRule
		{
			get { return m_longDistanceRule; }
		}
		/// <summary>
		/// string containing the dialing rule for direct-dialed 
		/// calls to other countries/regions.
		/// </summary>
		public string InternationalRule
		{
			get { return m_internationalRule; }
		}

		public override string ToString()
		{
			return this.Name;
		}

		public string GetCountryStringDesc()
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("Country Id = " + m_countryId.ToString() + Environment.NewLine);
			sb.Append("Code = " + m_code.ToString() + Environment.NewLine);
			sb.Append("Name = " + m_name.ToString() + Environment.NewLine);
			sb.Append("Same Area Rule = " + m_sameAreaRule + Environment.NewLine);
			sb.Append("Long Distance Rule = " + m_longDistanceRule + Environment.NewLine);
			sb.Append("International Rule = " + m_internationalRule);
			return sb.ToString();
		}
		#endregion

	} // class CCountry

	public sealed class CountryListCollection : IEnumerable
	{
		/*
			class CountryListCollection
			Copyright © 2003 by Best Software - CRM Divison.
			All Rights Reserved.

			Created: Brian Hormann
			Updated: February 22, 2003

		*/
		private ArrayList m_countryList;

		internal CountryListCollection()
		{
			m_countryList = new ArrayList();
		}
		internal CountryListCollection(int numberCountries)
		{
			m_countryList = new ArrayList(numberCountries);
		}

		public IEnumerator GetEnumerator()
		{
			return m_countryList.GetEnumerator();
		}

		public void Add(CCountry country)
		{
			m_countryList.Add(country);
		}

		public int Count
		{
			get { return m_countryList.Count; }
		}

		public void Clear()
		{
			m_countryList.Clear();
		}

		public void Remove(CCountry country)
		{
			m_countryList.Remove(country);
		}

		public bool Contains(CCountry country)
		{
			return m_countryList.Contains(country);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public CCountry[] ToArray()
		{
			return (CCountry[])this.m_countryList.ToArray(typeof(CCountry));
		}
	} // class CountryListCollection
} // namespace FORIS.TSS.IO