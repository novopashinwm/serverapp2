﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

namespace FORIS.TSS.IO.TAPI
{
	/// <summary>
	/// Summary description for CAddress.
	/// </summary>
	public class CAddress
	{
		/*
		  class CAddress
		  Copyright © 2003 by Agile Software Inc.
		  All Rights Reserved.

		  Created: Helen Warn
				Updated: Christoph Brдndle
		  Updated: February 22, 2003

		*/

		#region Private Members

		private uint m_AddressID;

		private CLine m_CLine;

		protected internal uint m_ExtVersion;

		private string m_Address;

		private CTapi.LineCallInfoState m_CallInfoStates;

		private CTapi.LineCallState m_CallStates;

		private bool m_AddressValid;

		#endregion // Private Members

		public CAddress(CLine Line, uint AddressID)
		{
			//
			// TODO: Add constructor logic here
			//
			m_CLine = Line;
			m_AddressID = AddressID;
			m_ExtVersion = 0;
			m_AddressValid = true;
		}

		public CAddress(CLine Line, uint AddressID, uint ExtVersion) : this(Line, AddressID)
		{
			m_ExtVersion = ExtVersion;
		}

		#region Properties

		public uint LineDeviceID
		{ get { return this.m_CLine.DeviceID; } }

		public uint AddressID
		{ get { return this.m_AddressID; } }

		public string Address
		{ get { return this.m_Address; } }

		public CTapi.LineCallInfoState CallInfoStates
		{ get { return this.m_CallInfoStates; } }

		public CTapi.LineCallState CallStates
		{ get { return this.m_CallStates; } }

		public bool AddressValid
		{
			get { return this.m_AddressValid; }
			set { this.m_AddressValid = value; }
		}

		public uint ExtVersion
		{
			get { return this.m_ExtVersion; }
		}

		/// <summary>
		/// needed for API call
		/// </summary>
		private IntPtr hTapi
		{ get { return this.m_CLine.hTapi; } }

		#endregion // Properties

		#region Public Methods

		public CTapi.LineErrReturn GetAddrCaps()
		{
			CTapi.lineaddresscaps addrcaps = new CTapi.lineaddresscaps();
			int buffsize = 1;
			addrcaps.dwLineDeviceID = this.LineDeviceID;
			int InitSize = Marshal.SizeOf(addrcaps);
			int TotalSize = InitSize;
			int NeededSize = InitSize;
			bool needMoreMem = true;
			IntPtr plineaddresscaps;
			byte[] totalbuffer = null;
			CTapi.LineErrReturn ret = CTapi.LineErrReturn.LINEERR_STRUCTURETOOSMALL;

			while (needMoreMem == true)
			{
				if ((NeededSize - TotalSize) > 0)
					buffsize = (NeededSize - InitSize + 1);
				else
					buffsize = 1;

				addrcaps.dwTotalSize = (uint)(buffsize + InitSize - 1);
				addrcaps.dwLineDeviceID = this.LineDeviceID;
				plineaddresscaps = Marshal.AllocHGlobal(NeededSize);
				Marshal.StructureToPtr(addrcaps, plineaddresscaps, true);
				ret = CTapi.lineGetAddressCaps(this.hTapi, this.LineDeviceID, this.AddressID, this.m_CLine.TapiNegotiatedVer, this.m_ExtVersion, plineaddresscaps);
				Marshal.PtrToStructure(plineaddresscaps, addrcaps);
				TotalSize = (int)addrcaps.dwTotalSize;
				NeededSize = (int)addrcaps.dwNeededSize;
				needMoreMem = ((ret == CTapi.LineErrReturn.LINEERR_OK) && (NeededSize > TotalSize)) || (ret == CTapi.LineErrReturn.LINEERR_STRUCTURETOOSMALL);
				if (needMoreMem == false)
				{
					totalbuffer = new byte[TotalSize];
					Marshal.Copy(plineaddresscaps, totalbuffer, 0, TotalSize);
				}
				Marshal.FreeHGlobal(plineaddresscaps);
			}

			if (ret == CTapi.LineErrReturn.LINEERR_OK)
			{
				if (FillAddressCaps(addrcaps, totalbuffer) == false)
				{
					// handle error
					this.m_AddressValid = false;
				}
				else
				{
				}
			}
			return ret;
		}


		public string GetAddressInfoString()
		{
			StringBuilder sb = new StringBuilder(600);
			sb.Append("Device ID: " + this.m_CLine.DeviceID + Environment.NewLine);
			sb.Append("Address ID: " + this.AddressID + Environment.NewLine);
			sb.Append("Address: " + this.Address + Environment.NewLine);
			sb.Append("Call Info States: " + this.CallInfoStates + Environment.NewLine);
			sb.Append("Call States: " + this.CallStates + Environment.NewLine);

			return sb.ToString();
		}

		#endregion // Public Methods

		#region Private Methods


		private bool FillAddressCaps(CTapi.lineaddresscaps addrcaps, byte[] buffer)
		{
			try
			{
				// byte array to use for extracting variable-length data
				System.Text.Decoder dec = null;
				CTapi.StringFormat sf = this.m_CLine.StringFormat;

				dec = CTapi.GetDecoder(sf);

				// now start filling in the values
				this.m_Address = CAPIUtils.StringFromByteArray(buffer, (int)addrcaps.dwAddressOffset, (int)addrcaps.dwAddressSize, dec);
				this.m_CallInfoStates = (CTapi.LineCallInfoState)addrcaps.dwCallInfoStates;
				this.m_CallStates = (CTapi.LineCallState)addrcaps.dwCallStates;

				return true;
			}
			catch (Exception ex)
			{
				Debug.WriteLine("!!ERR!!" + ex.Message);
				return false;
			}
		}

		#endregion // Private Methods


	} // class CAddress
} // namespace FORIS.TSS.IO