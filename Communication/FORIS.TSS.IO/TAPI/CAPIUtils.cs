﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Text;

namespace FORIS.TSS.IO.TAPI
{
	/// <summary>
	/// Utility routines for handling Tapi API call info.
	/// </summary>
	public class CAPIUtils
	{
		/*
		  class CAPIUtils
		  Copyright © 2003 by Agile Software Inc.
		  All Rights Reserved.

		  Created: Helen Warn
		  Updated: February 2, 2003

		*/

		public CAPIUtils()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/// <summary>
		/// serializes any object that can be marshalled into a byte array
		/// </summary>
		/// <param name="anything"></param>
		/// <returns></returns>
		public static byte[] RawSerialize(object anything)
		{
			int rawsize = Marshal.SizeOf(anything);
			IntPtr buffer = Marshal.AllocHGlobal(rawsize);
			Marshal.StructureToPtr(anything, buffer, false);
			byte[] arr = new byte[rawsize];
			Marshal.Copy(buffer, arr, 0, rawsize);
			Marshal.FreeHGlobal(buffer);
			return arr;
		}

		/// <summary>
		/// Use this routine if we have already allocated a buffer byte[] of length numBytes
		/// to return a structure as a byte array (used for VLS)
		/// </summary>
		/// <param name="anything"></param>
		/// <param name="numBytes"></param>
		/// <returns></returns>
		public static byte[] RawSerialize(object anything, int numBytes)
		{
			IntPtr buffer = Marshal.AllocHGlobal(numBytes);
			Marshal.StructureToPtr(anything, buffer, false);
			byte[] arr = new byte[numBytes];
			Marshal.Copy(buffer, arr, 0, numBytes);
			Marshal.FreeHGlobal(buffer);
			return arr;
		}
		/// <summary>
		/// this works for any object that can be marshalled
		/// </summary>
		/// <param name="anything"></param>
		/// <returns></returns>
		public static uint GetSize(object anything)
		{
			return (uint)Marshal.SizeOf(anything);
		}

		/// <summary>
		/// returns a subarray of byte[] from index start of length len
		/// I wonder if there is a better way to do this???
		/// </summary>
		/// <param name="arr"></param>
		/// <param name="start"></param>
		/// <param name="len"></param>
		/// <returns></returns>
		public static byte[] subarray(byte[] arr, int start, int len)
		{
			IntPtr buffer = Marshal.AllocHGlobal(len);
			Marshal.Copy(arr, start, buffer, len);
			byte[] ret = new byte[len];
			Marshal.Copy(buffer, ret, 0, len);
			Marshal.FreeHGlobal(buffer);
			return ret;
		}

		/// <summary>
		/// returns a string from a byte array.
		/// NOTE: this routine is used to convert null-terminated C++ strings, so it skips the last character
		/// </summary>
		/// <param name="arr">array containing the bytes to be converted</param>
		/// <param name="offset">starting position in arr</param>
		/// <param name="len">length of bytes to be converted</param>
		/// <param name="dec">decoder to use to convert</param>
		/// <returns></returns>
		public static string StringFromByteArray(byte[] arr, int offset, int len, System.Text.Decoder dec)
		{
			// if there's nothing to display, just exit
			if ((len == 0) || (arr.Length == 0))
				return null;

			// if there's no decoder, just return a null string
			// we may be able to change this later, to have the method return an object
			// and return a binary array here of len bytes in arr after the offset
			if (dec == null)
				return null;

			StringBuilder sb = new StringBuilder(len);
			int count = dec.GetCharCount(arr, offset, len);
			char[] chars = new char[count];
			char ch;
			dec.GetChars(arr, offset, len, chars, 0);

			// must skip null-termination of C++ string
			for (int i = 0; i < (count); ++i)
			{
				ch = chars[i];

				if (Char.GetUnicodeCategory(ch) != UnicodeCategory.Control)
				{
					sb.Append(ch.ToString());
				}
				else
					// return with what we've got if we hit a control character
					return sb.ToString();
			}
			return sb.ToString();
		}

		/// <summary>
		/// this only works if we know that the Byte array contains character data
		/// </summary>
		/// <param name="arr"></param>
		/// <returns></returns>
		public static string GetByteArrayToString(byte[] arr)
		{
			int len = arr.Length;
			System.Text.StringBuilder sb = new System.Text.StringBuilder(len);
			for (int i = 0; i < len; ++i)
			{ sb.Append(Convert.ToChar(arr[i])); }
			return sb.ToString();
		}

	} // class CAPIUtils
} // namespace FORIS.TSS.IO