using System;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;

namespace FORIS.TSS.IO.TCP
{
	public class TCPListener : TcpClient
	{
		/// <summary>
		/// �����������
		/// </summary>
		/// <param name="Server">����� �������</param>
		/// <param name="Port">����� �����</param>
		public TCPListener(string Server, int Port): base(Server, Port)
		{
			
		}

		/// <summary>
		/// �������� ����������
		/// </summary>
		/// <param name="datagram">����������</param>
		/// <param name="size">������ ����������</param>
		public void Send( byte [] datagram, int size )
		{
			try
			{
				NetworkStream stream = this.GetStream();

				if(stream != null && stream.CanWrite)
					stream.Write(datagram, 0, size);
			}
			catch(IOException ie)
			{
				Trace.WriteLine(ie.Message, "sending");
				throw ie;
			}
			catch(SocketException se)
			{
				Trace.WriteLine(se.Message, "sending");
				throw se;
			}
			catch(Exception ex)
			{
				Trace.WriteLine(ex.Message, "sending");
				throw ex;
			}
		}
	}
}