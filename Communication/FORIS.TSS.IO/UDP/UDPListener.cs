using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace FORIS.TSS.IO.UDP
{
	/// <summary>
	/// ������� ������� ��������� � ���������� ������, ���������� �� UDP ���������
	/// </summary>
	public delegate void UDPListenerEventHandler(object sender, UDPListenerEventArgs e);

	/// <summary>
	/// ������� ��������� ��� ���� ����� �������� UDP
	/// </summary>
	public interface IUDPListener
	{
		/// <summary>
		/// ������� ������� �������� ������ �� UDP ���������
		/// </summary>
		event UDPListenerEventHandler ReceiveEvent;

		/// <summary>
		/// ������� ��������� ������ �������������
		/// </summary>
		event EventHandler StopEvent;

		/// <summary>
		/// ������� ������� ������ �������������
		/// </summary>
		event EventHandler StartEvent;
	}

	/// <summary>
	/// ������ ��� ������������� ������ �� UDP ���������
	/// </summary>
    [Serializable]
	public abstract class UDPListener : UdpClient, IUDPListener
	{
		/// <summary>
		/// ������� ��������� ���������� �� UDP ���������
		/// </summary>
		public event UDPListenerEventHandler ReceiveEvent;

		/// <summary>
		/// ������� ��������� ������ �������������
		/// </summary>
		public event EventHandler StopEvent;

		/// <summary>
		/// ������� ������� ������ �������������
		/// </summary>
		public event EventHandler StartEvent;

		/// <summary>
		/// ����� �������
		/// </summary>
		protected Thread listenerThread = null;

		/// <summary>
		/// ����� ��� �������������
		/// </summary>
		protected IPEndPoint ipEndPoint = null;

		/// <summary>
		/// ����� �������
		/// </summary>
		protected IPEndPoint ipClientEndPoint = new IPEndPoint(IPAddress.Any, 0);

		#region .ctor
		/// <summary>
		/// ������ ��� ������������� ������ �� UDP ���������
		/// </summary>
		/// <param name="ipEndPoint">IP ����� � ���� ��� �������������</param>
		public UDPListener(IPEndPoint ipEndPoint) : base(ipEndPoint.Port)
		{
			this.ipEndPoint = ipEndPoint;
		}
		#endregion .ctor

		#region public void Start()
		/// <summary>
		/// ����� ������ �������������
		/// </summary>
		public void Start()
		{
			OnStart();
		}
		#endregion // public void Start()

		#region public void Stop()
		/// <summary>
		/// ��������� ������ ������������� 
		/// </summary>
		public void Stop()
		{
			OnStop();
		}
		#endregion // public void Stop()

		#region void ListenerThread()
		// ������� ������ ������������� UDP �����
		protected virtual void ListenerThread()
		{
			try
			{
				while(listenerThread.IsAlive) 
				{
					byte[] bytes = Receive(ref ipClientEndPoint);

					if(ReceiveEvent != null)
						ReceiveEvent(this, new UDPListenerEventArgs(bytes, ipClientEndPoint));
				}		
			}
			catch(Exception e)
			{
				Trace.WriteLine("!! ERR !! - " + e.Message);
			}
			finally
			{
				OnStop();
			}
		}
		#endregion // void ListenerThread()

		#region void OnStop()
		// ��������� ������ �������������
		protected virtual void OnStop()
		{
			Close();
			//
			if(listenerThread != null)
				if(listenerThread.IsAlive)
					listenerThread.Abort();
			//
			if(StopEvent != null)
				StopEvent(this, EventArgs.Empty);
		}
		#endregion // void OnStop()

		#region void OnStart()
		// ������ ������ �������������
		protected virtual void OnStart()
		{
			try
			{
				//
				listenerThread = new Thread(new ThreadStart(this.ListenerThread));
				listenerThread.Name = "UDP Listener Thread";
				listenerThread.IsBackground = true;
				listenerThread.Start();

				//
				if(StartEvent != null)
					StartEvent(this, EventArgs.Empty);
			}
			catch(Exception e)
			{
				throw e;
			}
		}
		#endregion // void OnStart()
	}
}
