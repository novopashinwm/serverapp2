using System;
using System.Net;

namespace FORIS.TSS.IO.UDP
{
	/// <summary>
	/// �������� ������� ������� ��� ������������� ������ �� UDP ���������
	/// </summary>
	public class UDPListenerEventArgs : EventArgs
	{
		protected byte[] arbData;
		public virtual byte[] Data
		{
			get
			{
				return arbData;
			}
			set
			{
				arbData = value;
			}
		}
		
		protected IPEndPoint endPoint;
		public virtual IPEndPoint EndPoint
		{
			get
			{
				return endPoint;
			}
			set
			{
				endPoint = value;
			}
		}

		protected DateTime time = DateTime.Now;
		public virtual DateTime Time
		{
			get
			{
				return time;
			}
			set
			{
				time = value;
			}
		}
		
		public UDPListenerEventArgs()
		{
		}
		
		public UDPListenerEventArgs(byte[] data, IPEndPoint ep)
		{
			arbData = data;
			endPoint = ep;
		}
	}
}