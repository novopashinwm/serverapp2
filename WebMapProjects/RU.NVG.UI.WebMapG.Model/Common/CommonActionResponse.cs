﻿using System;
using System.Runtime.Serialization;

namespace RU.NVG.UI.WebMapG.Model.Common
{
	/// <summary> Ответ операции Api Action(PageAction, JsonPageAction) </summary>
	/// <typeparam name="T"></typeparam>
	[Serializable]
	[DataContract]
	public class CommonActionResponse<T>
	{
		/// <summary> Код результата операции </summary>
		[DataMember] public CommonActionResult Result     { get; set; } = CommonActionResult.Success;
		/// <summary> Текстовое описание результата операции </summary>
		[DataMember] public string             ResultText { get; set; }
		/// <summary> Объект результата операции </summary>
		[DataMember] public T                  Object     { get; set; }
	}
}