﻿using System;

namespace RU.NVG.UI.WebMapG.Model.Common
{
	public class BanResponse
	{
		public bool       IsError { get; private set; }
		public ResultCode Code    { get; private set; }
		public DateTime   BanDate { get; private set; }
		public BanResponse(DateTime banDate)
		{
			IsError = true;
			Code    = ResultCode.Banned;
			BanDate = banDate;
		}
	}
}