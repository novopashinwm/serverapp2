﻿using System;

namespace RU.NVG.UI.WebMapG.Model.Common
{
	public class CommonResponse<T>
	{
		public T          Item;
		public ResultCode Result       = ResultCode.Success;
		public Enum       ErrorType    = null;
		public object     ErrorValue   = null;
		public string     ErrorMessage = null;
	}
}