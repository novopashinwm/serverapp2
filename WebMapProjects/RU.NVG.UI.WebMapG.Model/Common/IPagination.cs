﻿using FORIS.TSS.BusinessLogic.DTO;

namespace RU.NVG.UI.WebMapG.Model.Common
{
	public interface IPagination
	{
		Pagination Pagination { get; set; }
	}
}