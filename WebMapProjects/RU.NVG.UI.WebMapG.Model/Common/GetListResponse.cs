﻿using FORIS.TSS.BusinessLogic.DTO;

namespace RU.NVG.UI.WebMapG.Model.Common
{
	public class GetListResponse<T> : ICollectionItem<T>, IPagination
	{
		public T[]        Items      { get; set; }
		public Pagination Pagination { get; set; }
	}
}