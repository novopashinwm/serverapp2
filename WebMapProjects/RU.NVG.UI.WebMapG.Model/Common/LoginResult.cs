﻿namespace RU.NVG.UI.WebMapG.Model.Common
{
	public enum LoginResult
	{
		Success,
		WrongPassword,
		SessionAlreadyExists,
		CannotConnectToServer,
		NoAvailableVehicles,
		Error,
		InvalidConfirmationKey,
		UserNotFound,
		/// <summary> Требуется ожидание окончания регистрации пользователя, клиент должен показать прогресс-бар и повторить попытку через 5 секунд </summary>
		Wait,
		/// <summary> Доступ заблокирован </summary>
		AccessLocked
	}
}