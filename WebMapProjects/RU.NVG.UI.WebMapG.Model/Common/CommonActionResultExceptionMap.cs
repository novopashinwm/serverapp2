﻿using System;
using System.Collections.Generic;
using System.Security;
using FORIS.TSS.Common.Helpers;

namespace RU.NVG.UI.WebMapG.Model.Common
{
	/// <summary> Перечисление результатов операции Api Action(PageAction, JsonPageAction) </summary>
	public static class CommonActionResultExceptionMap
	{
		private static Dictionary<Type, CommonActionResult> exceptionResultMap = new Dictionary<Type, CommonActionResult>
		{
			{ typeof(ArgumentException),           CommonActionResult.NotValidParam  },
			{ typeof(ArgumentNullException),       CommonActionResult.NotValidParam  },
			{ typeof(ArgumentOutOfRangeException), CommonActionResult.NotValidParam  },
			{ typeof(SecurityException),           CommonActionResult.Denied         },
			{ typeof(KeyNotFoundException),        CommonActionResult.ObjectNotFound },
		};
		public static CommonActionResult GetCommonActionResultByExceptionType(Type type)
		{
			return exceptionResultMap
				.GetValueOrDefault(type, CommonActionResult.Error);
		}
	}
}