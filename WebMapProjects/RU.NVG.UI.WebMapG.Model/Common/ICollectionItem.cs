﻿namespace RU.NVG.UI.WebMapG.Model.Common
{
	public interface ICollectionItem<T>
	{
		T[] Items { get; set; }
	}
}