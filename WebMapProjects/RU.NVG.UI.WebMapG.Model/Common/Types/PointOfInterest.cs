﻿using System;
using System.Runtime.Serialization;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;

namespace RU.NVG.UI.WebMapG.Model.Common.Types
{
	[DataContract(IsReference = true)]
	[Serializable()]
	public class PointOfInterest
	{
		public const int           EmptyId            = -1;
		public const WebPointType  DefaultType        = WebPointType.UserPoint;
		[DataMember] public int    Id   { get; set; } = EmptyId;
		[DataMember] public string Name { get; set; }
		[DataMember] public string Desc { get; set; }
		[DataMember] public double Lng  { get; set; }
		[DataMember] public double Lat  { get; set; }
		public WebPoint ToWebPoint()
		{
			return new WebPoint
			{
				Id   = Id,
				Name = Name,
				Desc = Desc,
				Type = DefaultType,
				X    = Lng,
				Y    = Lat,
			};
		}
		public static PointOfInterest FromWebPoint(WebPoint webPoint)
		{
			if (null == webPoint || null == webPoint.Type || DefaultType != webPoint.Type)
				return default(PointOfInterest);
			return new PointOfInterest
			{
				Id   = webPoint.Id,
				Name = webPoint.Name,
				Desc = webPoint.Desc,
				Lng  = webPoint.X,
				Lat  = webPoint.Y,
			};
		}
	}
}