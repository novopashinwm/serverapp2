﻿using FORIS.TSS.BusinessLogic;

namespace RU.NVG.UI.WebMapG.Model.Common
{
	public class SignInResult
	{
		public LoginResult   Result;
		public int?          OperatorId;
		public string        AppId;
		public int?          VehicleId;
		public string        SimId;
		public SystemRight[] Rights;
		public SystemRight[] DepartmentRights;
	}
}