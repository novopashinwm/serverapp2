﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace RU.NVG.UI.WebMapG.Model.Common
{
	/// <summary> Перечисление результатов операции Api Action(PageAction, JsonPageAction) </summary>
	[Serializable]
	[DataContract]
	public enum CommonActionResult
	{
		/// <summary> Операция успешно выполнена </summary>
		[EnumMember, Description("Action success")]
		Success,
		/// <summary> Операция завершена с ошибкой </summary>
		[EnumMember, Description("Action error")]
		Error,
		/// <summary> Операция получила неправильный параметр </summary>
		[EnumMember, Description("Action received an invalid parameter")]
		NotValidParam,
		/// <summary> Операция запрещена </summary>
		[EnumMember, Description("Action denied")]
		Denied,
		/// <summary> Объект не найден по идентификатору </summary>
		[EnumMember, Description("Object not found")]
		ObjectNotFound
	}
}