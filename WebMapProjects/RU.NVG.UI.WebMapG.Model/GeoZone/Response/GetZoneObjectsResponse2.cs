﻿using FORIS.TSS.BusinessLogic.DTO;
using RU.NVG.UI.WebMapG.Model.Common;

namespace RU.NVG.UI.WebMapG.Model.GeoZone.Response
{
	public class GetZoneObjectsResponse2 : BaseResponse
	{
		public FORIS.TSS.BusinessLogic.DTO.GeoZone[] GeoZones { get; set; }

		public Group[] GeoZoneGroups { get; set; }

		public GetZoneObjectsResponse2(FORIS.TSS.BusinessLogic.DTO.GeoZone[] geoZones,
			Group[] geoZoneGroups)
		{
			GeoZones = geoZones ?? new FORIS.TSS.BusinessLogic.DTO.GeoZone[0];
			GeoZoneGroups = geoZoneGroups ?? new Group[0];
		}
	}
}