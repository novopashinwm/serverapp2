﻿using System.Drawing;

namespace RU.NVG.UI.WebMapG.Model.GeoZone.Request
{
	public interface ISaveCircleZoneRequest : ISaveZoneRequest
	{
		float  Radius { get; }
		PointF Point { get; }
	}
}