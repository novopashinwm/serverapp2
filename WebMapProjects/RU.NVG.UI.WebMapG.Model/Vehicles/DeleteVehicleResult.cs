﻿namespace RU.NVG.UI.WebMapG.Model.Vehicles
{
	public class DeleteVehicleResult
	{
		public Interfaces.Web.Enums.DeleteVehicleResult Result;
		public string                                   ResultText;
	}
}