﻿using System;

namespace RU.NVG.UI.WebMapG.Model.Vehicles
{
	public class SetVehicleAttributeResult
	{
		public Enum   Result;
		public string ResultText;
	}
}