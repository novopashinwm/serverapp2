﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Map;

namespace Interfaces.Web
{
	[Serializable]
	public class WebMobileHistory : IWebMobileHistory
	{
		private SortedList<int, IWebMobileInfoCP> mobileUnits;
		private List<GroupingMobileUnit> grouping;
		private double run = -1;
		private double runCAN = -1;

		public WebMobileHistory()
		{
		}

		public WebMobileHistory(SortedList<int, IWebMobileInfoCP> mobileUnits, List<GroupingMobileUnit> grouping, double run)
		{
			this.mobileUnits = mobileUnits;
			this.grouping = grouping;
			this.run = run;
		}

		public SortedList<int, IWebMobileInfoCP> MobileUnits
		{
			get { return mobileUnits; }
		}

		public List<GroupingMobileUnit> Grouping
		{
			get { return grouping; }
		}


		public TimeSpan MotionTime
		{
			get
			{
				TimeSpan mt = TimeSpan.Zero;
				foreach (GroupingMobileUnit groupingMobileUnit in grouping)
				{
					if (groupingMobileUnit.GroupingType != GroupingType.Motion)
						continue;
					int seconds = groupingMobileUnit.Nodes[groupingMobileUnit.Nodes.Count - 1].MobileUnit.Time -
					groupingMobileUnit.Nodes[0].MobileUnit.Time;
					mt = mt.Add(TimeSpan.FromSeconds(seconds));
				}

				return mt;
			}
		}

		public TimeSpan StandTime
		{
			get
			{
				TimeSpan mt = TimeSpan.Zero;
				foreach (GroupingMobileUnit groupingMobileUnit in grouping)
				{
					if (groupingMobileUnit.GroupingType != GroupingType.Stand && groupingMobileUnit.GroupingType != GroupingType.Stop)
						continue;
					int seconds = groupingMobileUnit.Nodes[groupingMobileUnit.Nodes.Count - 1].MobileUnit.Time -
					groupingMobileUnit.Nodes[0].MobileUnit.Time;
					mt = mt.Add(TimeSpan.FromSeconds(seconds));
				}

				return mt;
			}
		}

		/// <summary>
		/// Пробег, км
		/// </summary>
		public Double Run
		{
			get
			{
				if (run == -1)
				{
					run = 0;
					foreach (GroupingMobileUnit groupingMobileUnit in grouping)
					{
						if (groupingMobileUnit.GroupingType != GroupingType.Motion)
							continue;

						for (int i = 0; i < groupingMobileUnit.Nodes.Count; i++)
						{
							if (i == 0) continue;
							IWebMobileInfoCP mb1 = groupingMobileUnit.Nodes[i - 1].MobileUnit;
							IWebMobileInfoCP mb2 = groupingMobileUnit.Nodes[i].MobileUnit;

							run += DistTo(new DPoint((float)mb1.Longitude, (float)mb1.Latitude), new DPoint((float)mb2.Longitude, (float)mb2.Latitude));
						}
					}
				}

				return run;
			}
			private set
			{
				run = value;
			}
		}

		public float DistTo(DPoint Pfrom, DPoint Pto)
		{
			if (Pfrom.x == Pto.x && Pfrom.y == Pto.y)
			{
				return 0;
			}
			DPoint from;
			DPoint to;
			FORIS.TSS.BusinessLogic.Map.Geo.geoToPlanar(Pfrom.x, Pfrom.y, out from);
			FORIS.TSS.BusinessLogic.Map.Geo.geoToPlanar(Pto.x, Pto.y, out to);
			return from.distanceTo(to);
		}

		public Double MaxSpeed
		{
			get
			{
				int maxSpeed = 0;

				foreach (GroupingMobileUnit groupingMobileUnit in grouping)
				{
					foreach (GroupingMobileUnitNode node in groupingMobileUnit.Nodes)
					{
						if (node.MobileUnit.Speed == null)
							continue;
						var speed = node.MobileUnit.Speed.Value;
						if (speed > maxSpeed)
							maxSpeed = speed;
					}
				}

				return maxSpeed;
			}
		}
	}
}