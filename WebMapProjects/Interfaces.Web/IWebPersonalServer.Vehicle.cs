﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using Interfaces.Web.Enums;

namespace Interfaces.Web
{
	public partial interface IWebPersonalServer
	{
		/// <summary> Задаёт идентификатор устройства для терминала данного объекта наблюдения </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <param name="deviceId"> Идентификатор терминала </param>
		SetVehicleDeviceIDResult SetVehicleDeviceId(int vehicleId, string deviceId);
		/// <summary> Задаёт телефон SIM-карты, установленной в терминал данного объекта наблюдения </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <param name="phone"> Номер телефона </param>
		SetVehicleDevicePhoneResult SetVehicleDevicePhone(int vehicleId, string phone);
		/// <summary> Задаёт пароль для управления терминалом данного объекта наблюдения </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <param name="password"> Пароль </param>
		SetVehicleDevicePasswordResult SetVehicleDevicePassword(int vehicleId, string password);
		/// <summary> Задаёт новый тип трекера, установленного на объект наблюдения vehicleId </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <param name="trackerType"> Новый тип трекера </param>
		SetVehicleControllerFieldResult SetVehicleDeviceType(int vehicleId, string trackerType);
		/// <summary> Задаёт позицию для объекта наблюдения, у которого позиция не приходит от трекера </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <param name="lat"> Широта </param>
		/// <param name="lng"> Долгота </param>
		void SetVehiclePosition(int vehicleId, double lat, double lng);
		/// <summary> Устанавливает значение IP устройства </summary>
		/// <param name="vehicleId">Идентификатор объекта наблюдения</param>
		/// <param name="ip">Новое значение IP</param>
		SetVehicleControllerFieldResult SetVehicleDeviceIPAddress(int vehicleId, string ip);
		/// <summary> Устанавливает группу геозон, связанную с объектом наблюдения </summary>
		/// <remarks> Введено для реализации автобусных маршрутов в Индии </remarks>
		/// <param name="vehicleId">Идентификатор объекта наблюдения</param>
		/// <param name="zoneGroupId">Идентификатор группы геозон.</param>
		SetVehicleFieldResult SetVehicleZoneGroup(int vehicleId, int? zoneGroupId);
		/// <summary> Возвращает текущую группу зон, связанную с объектом наблюдения </summary>
		/// <param name="vehicleId">Идентификатор объекта наблюдения</param>
		Group GetVehicleZoneGroup(int vehicleId);
		/// <summary> Задаёт новое значение для названия объекта наблюдения </summary>
		/// <param name="vehicleId">Идентификатор объекта наблюдения</param>
		/// <param name="value">Новое название</param>
		SetVehicleFieldResult SetVehicleName(int vehicleId, string value);
		/// <summary> Задаёт новое значение для максимально допустимой скорости </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <param name="value"> Новое значение </param>
		SetVehicleFieldResult SetMaxAllowedSpeed(int vehicleId, int? value);
		/// <summary> Возвращает список типов команд, которые можно выполнять на данном устройстве </summary>
		/// <param name="vehicleId">Идентификатор объекта наблюдения</param>
		/// <param name="includeScenario">Если true, заполняет <see cref="CommandType.Scenario"/></param>
		/// <returns>Возвращает список доступных для вызова команд</returns>
		List<CommandType> GetCommandTypes(int vehicleId, bool includeScenario = false);
		/// <summary> Возвращает пароль для управления настройками трекера </summary>
		/// <param name="vehicleId">Идентификатор объекта наблюдения</param>
		string GetVehiclePassword(int vehicleId);
		/// <summary> Возвращает описание типа трекера, установленного на объекте наблюдения </summary>
		/// <param name="vehicleId">Идентификатор объекта наблюдения</param>
		ControllerType GetVehicleControllerType(int vehicleId);
		/// <summary> Удаляет объект наблюдения с указанным идентификатором </summary>
		/// <param name="vehicleId">Идентификатор объекта наблюдения</param>
		/// <returns> Возвращает true в случае успешного удаления и false в противном случае </returns>
		DeleteVehicleResult DeleteVehicle(int vehicleId);
		/// <summary> Перемещает объект наблюдения с указанным идентификатором в другого клиента </summary>
		/// <param name="vehicleId">Идентификатор объекта наблюдения</param>
		/// <param name="targetDepartmentId">Идентификатор целевого клиента</param>
		void MoveVehicle(int vehicleId, int targetDepartmentId);
		/// <summary> Возвращает историю по полям профиля </summary>
		/// <param name="vehicleId">Идентификатор ТС</param>
		/// <param name="name">Название поля</param>
		/// <returns></returns>
		VehicleProfileHistory[] GetVehicleProfileHistory(int vehicleId, string name);
		/// <summary> Возвращает список доступных атрибутов ТС для редактирования </summary>
		/// <returns></returns>
		List<VehicleAttribute> GetVehicleAttributes(int vehicleId, params VehicleAttributeName[] attributesNames);
		/// <summary> Возвращает значение атрибута по названию </summary>
		/// <param name="vehicleId"></param>
		/// <param name="attributeName"></param>
		/// <param name="vehicleKind"></param>
		/// <returns></returns>
		List<VehicleAttributeValue> GetVehicleAttributeValues(int vehicleId, VehicleAttributeName attributeName, VehicleKind? vehicleKind);
		/// <summary> Установка значения свойства для ТС </summary>
		/// <param name="vehicleId">Идентификатор ТС</param>
		/// <param name="field">Название поля из <see cref="VehicleAttribute"/></param>
		/// <param name="value">Значение поля</param>
		/// <returns></returns>
		Enum SetVehicleAttribute(int vehicleId, VehicleAttributeName field, string value);
		/// <summary> Установка значения свойств для ТС </summary>
		/// <param name="vehicleId">Идентификатор ТС</param>
		/// <param name="values">Значения для требуемых полей</param>
		Dictionary<VehicleAttributeName, Enum> SetVehicleAttributes(
			int vehicleId,
			Dictionary<VehicleAttributeName, string> values);
		/// <summary> Возвращает список расписаний для объекта наблюдения </summary>
		/// <param name="vehicleId">Идентификатор объекта наблюдения</param>
		List<TrackingSchedule> GetVehicleSchedules(int vehicleId);
		/// <summary> Создаёт или изменяет расписание для объекта наблюдения </summary>
		/// <param name="vehicleId">Идентификатор объекта наблюдения</param>
		/// <param name="schedule">Расписание</param>
		/// <returns>Возвращает сохраненное расписание</returns>
		TrackingSchedule SetVehicleSchedule(int vehicleId, TrackingSchedule schedule);
		/// <summary> Удаляет расписание объекта наблюдения </summary>
		/// <param name="scheduleId">Идентификатор расписания</param>
		void DeleteVehicleSchedule(int scheduleId);
		/// <summary> Возвращает название объекта наблюдения </summary>
		/// <param name="vehicleId">Идентификатор объекта наблюдения</param>
		string GetVehicleName(int vehicleId);
		/// <summary> Проверяет есть ли у текущего оператора все указанные права на vehicle </summary>
		bool IsAllowedVehicleAll(int vehicleId, params SystemRight[] rights);
		/// <summary> Проверяет есть ли у текущего оператора любое из указанных прав на vehicle </summary>
		bool IsAllowedVehicleAny(int vehicleId, params SystemRight[] rights);
	}
}