﻿using System.Collections.Generic;
using FORIS.TSS.BusinessLogic;
using Dto = FORIS.TSS.BusinessLogic.DTO;
using Res = FORIS.TSS.BusinessLogic.ResultCodes;
using Rht = FORIS.TSS.BusinessLogic.DTO.OperatorRights;

namespace Interfaces.Web
{
	public partial interface IWebPersonalServer
	{
		/// <summary> Сохраняет изменения на базовые права оператора </summary>
		/// <param name="oper"></param>
		void SaveOperatorRights(Rht::Operator oper);
		/// <summary> Возвращает информацию о том, кто может видеть данный объект наблюдения </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		List<Rht::Operator> WhoCanSeeVehicle(int vehicleId);
		/// <summary> Возвращает информацию о том, кто может данную группу объектов наблюдения </summary>
		/// <param name="vehicleGroupId"> Идентификатор группы объектов наблюдения </param>
		List<Rht::Operator> WhoCanSeeVehicleGroup(int vehicleGroupId);
		/// <summary> Предложить другу доступ к просмотру объекта наблюдения </summary>
		/// <param name="friendOperatorId"> Идентификатор пользователя, которому предлагается дать доступ к просмотру объекта наблюдения </param>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		void ProposeViewVehicle(int friendOperatorId, int vehicleId);
		/// <summary> Отозвать предложение на просмотр объекта наблюдения </summary>
		/// <param name="friendOperatorId"> Идентификатор пользователя, которому ранее предлагалось дать доступ к просмотру объекта наблюдения </param>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		void RecallViewVehicleProposation(int friendOperatorId, int vehicleId);
		/// <summary> Запретить доступ к объекту наблюдения </summary>
		/// <param name="friendOperatorId"> Идентификатор пользователя, которому ранее был предоставлен доступ к просмотру объекта наблюдения </param>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		void DisallowVehicleAccess(int friendOperatorId, int vehicleId);
		/// <summary> Поиск пользователя во всей системе по его контактным данным </summary>
		/// <param name="contact"> Строка контактных данных (email, телефон и т.п.) </param>
		List<Rht::Operator> FindUsers(string contact);
		/// <summary> Отправляет пользователю friendOperatorId предложение дружбы </summary>
		/// <param name="msisdn"> Номер телефона абонента, которому предлагается дружба </param>
		Res::ProposeFriendshipResult ProposeFriendship(string msisdn);
		/// <summary> Возвращает список пользователей, чьими правами может управлять текущий пользователь </summary>
		List<Rht::Operator> GetFriends();
		/// <summary> Возвращает список клиентов, чьими правами может управлять администраторы текущего клиента </summary>
		/// <returns></returns>
		List<Dto::Department> GetPartners();
		/// <summary> Добавляет пользователя с указанным идентификатором в список друзей данного клиента, после чего можно управлять доступами пользователя к своим объектам </summary>
		/// <param name="friendId"> Идентификатор пользователя, полученный, например, через FindUsers </param>
		void AddFriend(int friendId);
		/// <summary> Добавляет клиента с указанным идентификатором в список партнеров данного клиента, после чего можно управлять доступами к объектам клиента </summary>
		/// <param name="departmentId"></param>
		void AddPartner(int departmentId);
		/// <summary> Удаляет пользователя с указанным идентификатором из списка друзей данного клиента и отнимает у этого пользователя все права на объекты и геозоны клиента </summary>
		/// <param name="friendId"> Идентификатор пользователя, полученный, например, через FindUsers </param>
		void DeleteFriend(int friendId);
		/// <summary> Удаляет клиента с указанным идентификатором из списка партнеров данного клиента и отнимает у пользователей этого клиента все права на объекты и геозоны клиента </summary>
		/// <param name="departmentId"> Идентификатор клиента, полученный, например, через FindPartners </param>
		void DeletePartner(int departmentId);
		/// <summary> Устанавливает доступ пользователя operatorId с правом right на объект наблюдения vehicleId </summary>
		/// <param name="operatorId"> Идентификатор пользователя </param>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <param name="right"> Право </param>
		/// <param name="allowed"> true - доступ разрешён, false - доступ запрещён </param>
		Res::SetAccessToVehicleResult SetReplacedAccessToVehicle(int operatorId, int vehicleId, SystemRight right, bool allowed);
		/// <summary> Удаляет доступ пользователя operatorId с правом right на объект наблюдения vehicleId </summary>
		/// <param name="operatorId"> Идентификатор пользователя </param>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <param name="right"> Право (если null, удаляем все удаляемые) </param>
		void DelReplacedAccessToVehicle(int operatorId, int vehicleId, SystemRight? right);
		/// <summary> Устанавливает доступ пользователя operatorId с правом right на объект наблюдения vehicleId </summary>
		/// <param name="msisdn"> Подтвержденный номер телефона пользователя </param>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <param name="right"> Право </param>
		/// <param name="allowed"> true - доступ разрешён, false - доступ запрещён </param>
		Res::SetAccessToVehicleResult SetReplacedAccessToVehicle(string msisdn, int vehicleId, SystemRight right, bool allowed);
		/// <summary> Удаляет доступ пользователя operatorId с правом right на объект наблюдения vehicleId </summary>
		/// <param name="msisdn"> Идентификатор пользователя </param>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <param name="right"> Право (если null, удаляем все удаляемые) </param>
		void DelReplacedAccessToVehicle(string msisdn, int vehicleId, SystemRight? right);
		/// <summary> Устанавливает доступ пользователя operatorId с правом right на группу объектов наблюдения vehicleGroupId </summary>
		/// <param name="operatorId"> Идентификатор пользователя </param>
		/// <param name="vehicleGroupId"> Идентификатор группы объектов наблюдения </param>
		/// <param name="right"> Право </param>
		/// <param name="allowed"> true - доступ разрешён, false - доступ запрещён </param>
		void SetReplacedAccessToVehicleGroup(int operatorId, int vehicleGroupId, SystemRight right, bool allowed);
		/// <summary> Устанавливает доступ пользователя operatorId с правом right на группу объектов наблюдения vehicleGroupId </summary>
		/// <param name="operatorId"> Идентификатор пользователя </param>
		/// <param name="vehicleGroupId"> Идентификатор группы объектов наблюдения </param>
		/// <param name="right">Право</param>
		void DelReplacedAccessToVehicleGroup(int operatorId, int vehicleGroupId, SystemRight right);
		/// <summary>Устанавливает доступ пользователя operatorId с правом right на зону zoneId </summary>
		/// <param name="operatorId">Идентификатор пользователя</param>
		/// <param name="zoneId">Идентификатор зоны</param>
		/// <param name="right">Право</param>
		/// <param name="allowed">true - доступ разрешён, false - доступ запрещён</param>
		void SetAccessToZone(int operatorId, int zoneId, SystemRight right, bool allowed);
		/// <summary>Удаляет доступ пользователя operatorId с правом right на объект наблюдения zoneId </summary>
		/// <param name="operatorId">Идентификатор пользователя</param>
		/// <param name="zoneId">Идентификатор зоны</param>
		/// <param name="right">Право</param>
		void DeleteAccessToZone(int operatorId, int zoneId, SystemRight right);
		/// <summary>Устанавливает доступ пользователя operatorId с правом right на группу зон zoneGroupId </summary>
		/// <param name="operatorId">Идентификатор пользователя</param>
		/// <param name="zoneGroupId">Идентификатор группы зон</param>
		/// <param name="right">Право</param>
		/// <param name="allowed">true - доступ разрешён, false - доступ запрещён</param>
		void SetAccessToZoneGroup(int operatorId, int zoneGroupId, SystemRight right, bool allowed);
		/// <summary>Устанавливает доступ пользователя operatorId с правом right на группу зон zoneGroupId </summary>
		/// <param name="operatorId">Идентификатор пользователя</param>
		/// <param name="zoneGroupId">Идентификатор группы зон</param>
		/// <param name="right">Право</param>
		void DeleteAccessToZoneGroup(int operatorId, int zoneGroupId, SystemRight right);
		/// <summary> Возвращает список доступных прав для администрирования объектов наблюдения и их групп </summary>
		/// <remarks> Только эти права можно передавать в методы
		/// <see cref="SetAccessToVehicle"/>, 
		/// <see cref="DelAccessToVehicle"/>, 
		/// <see cref="SetAccessToVehicleGroup"/> и 
		/// <see cref="DeleteAccessToVehicleGroup"/></remarks>
		SystemRight[] GetAccessibleRightsOnVehicle();
		/// <summary> Возвращает список доступных списков доступа </summary>
		/// <see cref="SetReplacedAccessToVehicle"/>, 
		/// <see cref="DelReplacedAccessToVehicle"/>, 
		/// <see cref="SetReplacedAccessToVehicleGroup"/> и 
		/// <see cref="DelReplacedAccessToVehicleGroup"/>
		SystemRight[] GetReplacedAccessOnVehicle();
		/// <summary> Возвращает список доступных прав для администрирования геозон и их групп </summary>
		/// <remarks>Только эти права можно передавать в методы 
		/// <see cref="SetAccessToZone"/>, 
		/// <see cref="DeleteAccessToZone"/>, 
		/// <see cref="SetAccessToZoneGroup"/> и 
		/// <see cref="DeleteAccessToZoneGroup"/></remarks>
		SystemRight[] GetAccessibleRightsOnZone();
		/// <summary> Возвращает список пользователей, для которых указаны права доступа на указанный объект наблюдения </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		List<Rht::Operator> GetAccessesToVehicle(int vehicleId);
		/// <summary> Возвращает список пользователей, для которых указаны права доступа на указанную группу объектов наблюдения </summary>
		/// <param name="vehicleGroupId"> Идентификатор группы объектов наблюдения </param>
		List<Rht::Operator> GetAccessesToVehicleGroup(int vehicleGroupId);
		/// <summary> Помечает объект наблюдения как игнорируемый данным пользователем </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения (любой видимый) </param>
		/// <param name="ignored"> true, если нужно игнорировать объект наблюдения и false в противном случае </param>
		void IgnoreVehicle(int vehicleId, bool ignored);
		/// <summary> Помечает группу объектов наблюдения как игнорируемую данным пользователем </summary>
		/// <param name="vehicleGroupId"> Идентификатор группы объектов наблюдения </param>
		/// <param name="ignored"> true, если нужно игнорировать группу объектов наблюдения и false в противном случае </param>
		void IgnoreVehicleGroup(int vehicleGroupId, bool ignored);
		/// <summary> Помечает зону как игнорируемую данным пользователем </summary>
		/// <param name="zoneId"> Идентификатор зоны (любой видимый) </param>
		/// <param name="ignored"> true, если нужно игнорировать объект наблюдения и false в противном случае </param>
		void IgnoreZone(int zoneId, bool ignored);
		/// <summary> Помечает группу зон как игнорируемую данным пользователем </summary>
		/// <param name="zoneGroupId"> Идентификатор группы зон (любой видимый) </param>
		/// <param name="ignored"> true, если нужно игнорировать группу зон и false в противном случае </param>
		void IgnoreZoneGroup(int zoneGroupId, bool ignored);
		/// <summary> Добавляет новый трекер по номеру телефона </summary>
		/// <param name="phoneNumber"> Номер телефона </param>
		Dto::AddTrackerResult AddTrackerByPhoneNumber(string phoneNumber);
		/// <summary> Добавляет новый трекер без каких-либо параметров </summary>
		Dto::AddTrackerResult AddTrackerAsEmpty();
		/// <summary> Блокирует клиента </summary>
		/// <param name="departmentId"> Идентификатор клиента </param>
		Dto::Department LockDepartment(int departmentId);
		/// <summary> Снимает блокировку клиента </summary>
		/// <param name="departmentId"> Идентификатор клиента </param>
		Dto::Department UnlockDepartment(int departmentId);
	}
}