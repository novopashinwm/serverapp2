﻿namespace Interfaces.Web.Enums
{
	public enum SavePhoneBookResult
	{
		Success,
		InsufficientRights,
		InvalidFormat
	}
}