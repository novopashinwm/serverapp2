﻿using System;

namespace Interfaces.Web.Enums
{
	[Serializable]
	public enum SetVehicleDevicePasswordResult
	{
		Success,
		InsufficientRights
	}
}