﻿using System;

namespace Interfaces.Web.Enums
{
	[Serializable]
	public enum SetVehicleDeviceIDResult
	{
		Success,
		WrongDeviceID,
		DeviceIDAlreadyExists,
		InsufficientRights
	}
}