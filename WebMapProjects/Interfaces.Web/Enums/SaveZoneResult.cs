﻿namespace Interfaces.Web.Enums
{
	/// <summary> Результат сохранения геозоны </summary>
	public enum SaveZoneResult
	{
		/// <summary> Геозона успешно сохранена </summary>
		Success,
		/// <summary> Геозона не верно заполнена </summary>
		NotValid,
		/// <summary> Запрещено </summary>
		Denied,
		/// <summary> Зона не найдена по идентификатору </summary>
		ZoneNotFound,
		/// <summary> Тип геозоны не найден </summary>
		TypeNotDefined,
		/// <summary> Неизвестная ошибка </summary>
		UnknownError
	}
}