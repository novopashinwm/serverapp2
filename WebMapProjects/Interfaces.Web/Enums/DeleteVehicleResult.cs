﻿namespace Interfaces.Web.Enums
{
	public enum DeleteVehicleResult
	{
		Success,
		VehicleNotInDepartment,
		VehicleIsUser
	}
}