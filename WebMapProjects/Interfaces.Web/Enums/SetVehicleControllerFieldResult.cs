﻿namespace Interfaces.Web.Enums
{
	public enum SetVehicleControllerFieldResult
	{
		/// <summary> Успешно </summary>
		Success,
		/// <summary> Недостаточно прав </summary>
		InsufficientRights,
		/// <summary> Невалидное значение </summary>
		IncorrectValue,
		/// <summary> Некорректное название свойства </summary>
		IncorrectFieldName,
		/// <summary> Не найдено значение ControllerInfo </summary>
		ControllerInfoNotFound
	}
}