﻿namespace Interfaces.Web.Enums
{
	public enum SetVehicleFieldResult
	{
		/// <summary> Успешно </summary>
		Success,
		/// <summary> Недостаточно прав </summary>
		InsufficientRights,
		/// <summary> Невалидное значение </summary>
		IncorrectValue,
		/// <summary> Некорректное название свойства </summary>
		IncorrectFieldName,
		/// <summary> Ошибка </summary>
		Error
	}
}