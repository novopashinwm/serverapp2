﻿namespace Interfaces.Web.Enums
{
	public enum SetVehicleControlDateResult
	{
		/// <summary> Успешно </summary>
		Success,
		/// <summary> Недостаточно прав </summary>
		InsufficientRights,
		/// <summary> Запись не найдена </summary>
		NotExist,
		/// <summary> Некорректный формат даты </summary>
		InvalidDate,
		/// <summary> Некорректное значение типа даты </summary>
		InvalidDateType
	}
}