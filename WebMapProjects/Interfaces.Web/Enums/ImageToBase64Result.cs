﻿namespace Interfaces.Web.Enums
{
	public enum ImageToBase64Result
	{
		Success,
		ImageIsTooBig,
		ServerError
	}
}