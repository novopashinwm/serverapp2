﻿using System;
namespace Interfaces.Web.Enums
{
	[Serializable]
	public enum ActivateNikaTrackerTrialResult
	{
		Success,
		SimIdNotFound,
		ServiceAlreadyPresent
	}
}