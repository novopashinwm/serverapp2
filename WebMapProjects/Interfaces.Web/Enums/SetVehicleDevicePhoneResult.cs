﻿using System;

namespace Interfaces.Web.Enums
{
	[Serializable]
	public enum SetVehicleDevicePhoneResult
	{
		/// <summary> Номер телефона успешно изменён </summary>
		Success,
		/// <summary> Недостаточно прав для изменения номера телефона </summary>
		InsufficientRights,
		/// <summary> Неверный формат номера телефона </summary>
		InvalidFormat,
		/// <summary> Такой номер телефона уже используется в системе </summary>
		AlreadyExists,
		/// <summary> По этому номеру телефону есть приложение обслуживания, но на ПО отсутствует услуга мониторинга </summary>
		NoService,
		/// <summary> По этому номеру телефону есть приложение обслуживания, но услуга уже используется </summary>
		ServiceIsAlreadyUsed
	}
}