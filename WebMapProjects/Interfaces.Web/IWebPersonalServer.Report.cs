﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO;

namespace Interfaces.Web
{
	public partial interface IWebPersonalServer
	{
		/// <summary> Возвращает бизнес объект-коллекцию пунктов меню для отчетов </summary>
		/// <returns></returns>
		List<ReportMenuGroup> GetReportMenu();
		/// <summary> Возвращает бизнес объект-коллекцию пунктов меню для отчетов </summary>
		/// <returns></returns>
		ReportMenuItem GetReportMenuItem(Guid reportGuid);
		/// <summary> Удаляет подписку на отчёт по расписанию </summary>
		/// <param name="subscriptionId">Идентификатор подписки</param>
		void DeleteSubscription(int subscriptionId);
	}
}