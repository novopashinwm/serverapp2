﻿using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Interfaces.Data;

namespace Interfaces.Web
{
	public interface IWebLoginProvider
	{
		/// <summary> Проверяет логин и пароль пользователя </summary>
		/// <param name="login"> Логин </param>
		/// <param name="password"> Пароль </param>
		CheckPasswordResult CheckPasswordForWeb(string login, string password);
		/// <summary> Генерирует ключ для верификации email пользователя, привязывает его к email и отправляет на email </summary>
		/// <param name="subjectFormat"> Тема сообщения </param>
		/// <param name="messageFormatString"><p> Строка форматирования текста сообщения </p>
		/// <p>Аргументы:<ul><li> 0: ключ подтверждения </li></ul></p>
		/// </param>
		/// <param name="emailAddressTo"> Адрес электронной почты </param>
		/// <param name="encodeAsUri"> Если true, ключ подтверждения кодируется для возможности использования как части uri </param>
		void SendConfirmationEmail(
			string emailAddressTo,
			string subjectFormat,
			string messageFormatString,
			bool encodeAsUri);
		/// <summary> Возвращает адрес электронной почты по указанному ключу подтверждения </summary>
		/// <param name="confirmationKey"> Ключ подтверждения, высылаемый на адрес электронной почты </param>
		string GetEmailByConfirmationKey(string confirmationKey);
		/// <summary> Возвращает логин оператора по указанному ключу подтверждения адреса электронной почты </summary>
		/// <param name="confirmationKey"> Ключ подтверждения, высылаемый на адрес электронной почты </param>
		int? GetOperatorIdByConfirmationKey(string confirmationKey);
		/// <summary> Проверяет captcha </summary>
		/// <param name="remoteIP"> IP клиента </param>
		/// <param name="challenge"> Код запроса captcha </param>
		/// <param name="response"> Ввод клиента </param>
		bool CheckCaptcha(string remoteIP, string challenge, string response);
		/// <summary> Возвращает логин оператора по его email, или null, если нет оператора с таким email </summary>
		string GetLoginByEmail(string email);
		/// <summary> Отправляет письмо на указанный почтовый адрес </summary>
		void SendEmail(string email, string subject, string body);
		/// <summary> Отправляет вопрос в техподдержку </summary>
		/// <param name="clientName"> Название клиента </param>
		/// <param name="trackerNumber"> Номер трекера </param>
		/// <param name="clientContacts"> Контактная информация для обратной связи </param>
		/// <param name="question"> Вопрос </param>
		void SendQuestionToSupport(string clientName, string trackerNumber, string clientContacts, string question);
		/// <summary> Возвращает идентификатор пользователя по идентификатору его приложения обслуживания </summary>
		/// <param name="simId"> Идентификатор приложения обслуживания </param>
		/// <param name="name"> Имя устройства, с которого выполняется вход - опционально </param>
		/// <remarks> В процессе выполнения может быть создан пользователь;
		/// имя пользователя и ассоциированного с ним устройства берётся из параметра name </remarks>
		int? GetOperatorIdBySimId(string simId, string name);
		/// <summary> Возвращает идентификатор (создает при необходимости) оператора по идентификатору объекта наблюдения для общих ссылок </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <returns></returns>
		int? GetOperatorId4ShareLink(int vehicleId);
		/// <summary> Отправляет запрос на расчёт стоимости </summary>
		/// <param name="customerForm"> Параметры для расчёта стоимости </param>
		void SendCalculationRequest(CalculationRequest customerForm);
		/// <summary> Проверяет, есть ли такой simId в системе </summary>
		/// <param name="simId"> Идентификатор приложения обслуживания пользователя </param>
		bool SimIdExists(string simId);
	}
}