﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.DTO.Messages;
using FORIS.TSS.BusinessLogic.Enums;
using Message = FORIS.TSS.BusinessLogic.DTO.Messages.Message;
using MessageTemplate = FORIS.TSS.BusinessLogic.Enums.MessageTemplate;

namespace Interfaces.Web
{
	public partial interface IWebPersonalServer
	{
		/// <summary> Возвращает список сообщений, связанных с текущим оператором </summary>
		/// <param name="vehicleIDs"> Фильтр по устройствам (null, если не нужно учитывать) </param>
		/// <param name="from"> Дата "С" (null, если не нужно учитывать) </param>
		/// <param name="to"> Дата "По" (null, если не нужно учитывать) </param>
		List<FORIS.TSS.BusinessLogic.DTO.Message> GetMessages(int[] vehicleIDs, DateTime? @from, DateTime? to);
		/// <summary> Возвращает сообщения, связанные с текущим пользователем </summary>
		/// <param name="messageDirection"> Направление сообщение (входящее/исходящее) </param>
		/// <param name="template"> Шаблон сообщения </param>
		/// <returns></returns>
		List<FORIS.TSS.BusinessLogic.DTO.Message> GetMessages(MessageType messageDirection, MessageTemplate template);
		/// <summary> Возвращает все сообщения текущего пользователя мобильного клиента по идентификатору пользователя и AppId сессии </summary>
		/// <returns></returns>
		Message[] GetMessagesForAppClient(int? lastMessageId, DateTime? dateFrom, DateTime? dateTo, Pagination pagination);
		/// <summary> Возвращает все сообщения текущего пользователя по идентификатору пользователя и набору объектов наблюдения </summary>
		/// <returns></returns>
		Message[] GetMessagesForOperator(int? lastMessageId, DateTime? dateFrom, DateTime? dateTo, Pagination pagination, int[] vehicleIds);
		/// <summary> Возвращает сообщения отправленные или полученные на указанный номер </summary>
		/// <param name="msisdn"> Номер телефона </param>
		/// <param name="firstMessageId"> Идентификатор сообщения, которое является первым </param>
		/// <param name="pagination"> Параметры нумерации и разбиения страниц </param>
		/// <returns></returns>
		HistoryMarked<MessageWeb> GetShortMessagesByMsisdn(string msisdn, int? firstMessageId, Pagination pagination);
		/// <summary> Возвращает сообщения отправленные или полученные на указанный номер </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <param name="firstMessageId"> Идентификатор сообщения, которое является первым </param>
		/// <param name="pagination"> Параметры нумерации и разбиения страниц </param>
		/// <returns></returns>
		HistoryMarked<MessageWeb> GetShortMessagesByVehicle(int vehicleId, int? firstMessageId, Pagination pagination);
		/// <summary> Отправляет информационное SMS на объект наблюдения </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <param name="messageBody"> Текст сообщения </param>
		void SendInformationalSmsToVehicle(int vehicleId, string messageBody);
		/// <summary> Отправляет конфигурационное SMS на объект наблюдения </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <param name="messageBody"> Текст сообщения </param>
		void SendConfigurationSmsToVehicle(int vehicleId, string messageBody);
		/// <summary> Отправляет смс на произвольный номер телефона </summary>
		/// <param name="msisdn"> Номер телефона получателя смс </param>
		/// <param name="source"> Тип телефона, который указывается в качестве отправителя смс </param>
		/// <param name="messageBody"> Тело сообщения </param>
		void SendSms(string msisdn, string source, string messageBody);
		/// <summary> Отправляет сообщение в службу технической поддержки от имени текущего пользователя </summary>
		/// <returns> Возвращает отправленное сообщение </returns>
		FORIS.TSS.BusinessLogic.DTO.Message SendCustomerTypeChangeRequest();
		/// <summary> Отправляет вопрос в службу технической поддержки  </summary>
		/// <param name="contacts"> Предпочтительные контакты, по которым нужно связаться с абонентом </param>
		/// <param name="trackerNumber"> Номер трекера (опционально) </param>
		/// <param name="question"> Текст вопроса </param>
		void SendQuestionToSupport(string contacts, string trackerNumber, string question);
		/// <summary> Отправляет запрос на расчёт стоимости </summary>
		/// <param name="customerForm"> Параметры для расчёта стоимости </param>
		void SendCalculationRequest(CalculationRequest customerForm);
		/// <summary> Обновляет статусы сообщений </summary>
		/// <param name="messageIds"> Идентификаторы сообщений </param>
		/// <param name="status"> Устанавливаемый статус </param>
		/// <returns></returns>
		int[] UpdateMessageStatus(int[] messageIds, MessageProcessingResult status);
		/// <summary> Принимает отчёт от мобильного клиента, что к нему пришел push, адресованный другому appId </summary>
		/// <param name="messageId"> Идентификатор push-уведомления </param>
		/// <param name="wrongAppId"> Идентификатор некорректного приложения </param>
		void WrongMessageDestination(int messageId, string wrongAppId);
	}
}