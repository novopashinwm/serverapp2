﻿
namespace Interfaces.Web
{
	public interface IWebMobileInfo
	{
		int Unique                { get; }
		int ID                    { get; }
		string ObjectName         { get; set; }
		int Type                  { get; }
		int Time                  { get; set; }
		int? Speed                { get; }
		int Course                { get; }
		double Latitude           { get; }
		double Longitude          { get; }
		double? Radius            { get; }
		bool ValidPosition        { get;}
		int Status                { get; }
		int FuelVolume            { get; }
		int Temperature           { get; }
		string LastPositionString { get; }
	}
}