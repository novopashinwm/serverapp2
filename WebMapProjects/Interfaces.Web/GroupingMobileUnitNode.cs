﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Interfaces.Web
{
    /// <summary>
    /// Отдельный представитель группы точек истории.
    /// </summary>
    [Serializable]
    public class GroupingMobileUnitNode
    {
        /*
         * Поле Status отражает состояние объекта слежения и должно быть
         * задано при помощи соответствующего перечисления, но 
         * перенести в эту сборку еще и подобное перечисление довольно сложно.
         * Поэтому перечисление определяется при надобности в использующей сборке.
         */
        public /*readonly*/ int Time;
        public readonly int? Status;
        public readonly IWebMobileInfoCP MobileUnit;

        public GroupingMobileUnitNode(int time, int? status, IWebMobileInfoCP mobileUnit)
        {
            this.Time = time;
            this.Status = status;
            this.MobileUnit = mobileUnit;
        }
    }
}
