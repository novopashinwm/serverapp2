﻿namespace Interfaces.Web
{
	public partial interface IWebPersonalServer
	{
		/// <summary> Регистрирует покупку Android на сервере  </summary>
		/// <param name="purchase">Описание покупки</param>
		/// <param name="signature">ЭЦП для первичной верификации покупки</param>
		void TryAttachAndroidSubscription(string purchase, byte[] signature);
		/// <summary> Регистрирует покупку Apple на сервере  </summary>
		/// <param name="purchaseBase64"></param>
		void TryAttachAppleSubscription(string purchaseBase64);
	}
}