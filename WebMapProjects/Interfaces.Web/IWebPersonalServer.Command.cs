﻿using System;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;

namespace Interfaces.Web
{
	public partial interface IWebPersonalServer
	{
		Command RegisterCommand(Command command);
		/// <summary> Остановить команды для включения/выключения двигателей авто </summary>
		/// <param name="vehicleIds"></param>
		void CancelImmobilization(int[] vehicleIds);
		/// <summary> Остановить указанную команду </summary>
		/// <returns> Возвращает true, если команда была завершена и false в противном случае </returns>
		bool CancelCommand(int id);
		/// <summary> Отправить на выполнение команду минуя очередь команд </summary>
		/// <param name="type"> Тип команды, не все команды поддерживаются </param>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <param name="ready"> Обратный вызов, когда готовы результаты выполнения команды </param>
		void SendInstantCommand(CmdType type, int vehicleId, Action<object> ready);
		/// <summary> Получение сценария для выполнения команды </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <param name="type"> Тип команды </param>
		CommandScenario GetCommandScenario(int vehicleId, CmdType type);
	}
}