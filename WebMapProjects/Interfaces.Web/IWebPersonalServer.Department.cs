﻿using System.Collections.Generic;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.BusinessLogic.Server;
using Rht = FORIS.TSS.BusinessLogic.DTO.OperatorRights;

namespace Interfaces.Web
{
	public partial interface IWebPersonalServer
	{
		/// <summary> Переводит текущего клиента на заданный функционал </summary>
		/// <param name="departmentType"> Тип обслуживания абонента </param>
		void ChangeDepartmentType(DepartmentType departmentType);
		/// <summary> Возвращает права пользователя на текущий департамент </summary>
		List<SystemRight> GetDepartmentRights();
		bool DepartmentExtIDIsAllowed { get; }
		void SetDepartmentName(string value);
		void SetDepartmentExtID(string value);
		void SetDepartmentDescription(string value);
		List<Rht::Operator> GetDepartmentOperators(int departmentId);
		List<Rht::Operator> GetDepartmentAdministrators(int departmentId);
		/// <summary> Создание нового клиента по названию клиента и логину администратора </summary>
		/// <remarks>Создается:
		/// 1. Департамент клиента.
		/// 2. Администратор департамента.
		/// 3. Группа для объектов наблюдения.
		/// 4. Группа для геозон.
		/// 5. Группа для операторов департамента.</remarks>
		/// <returns> Идентификатор нового клиента </returns>
		CreateDepartmentResult CreateDepartment(string name, string adminLogin, string departmentExtID, out int departmentId);
		/// <summary> Создает в системе нового оператора </summary>
		/// <param name="login"></param>
		/// <param name="password"></param>
		/// <param name="nickname"> Имя оператора </param>
		/// <param name="operInfo"> Созданный оператор </param>
		/// <returns></returns>
		CreateOperatorResult CreateDepartmentOperator(string login, string password, string nickname, out Rht::Operator operInfo);
	}
}