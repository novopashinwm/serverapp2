﻿using System;
using System.Collections.Generic;

namespace Interfaces.Web
{
	/// <summary> История объекта слежения </summary>
	public interface IWebMobileHistory
	{
		/// <summary> Каждая точка истории одного объекта слежения представлена отдельным объектом </summary>
		SortedList<int, IWebMobileInfoCP> MobileUnits { get; }
		/// <summary> История разбита на группы точек. Каждая группа представляет собой набор соседних точек с одинаковым состоянием. </summary>
		List<GroupingMobileUnit> Grouping { get; }
		/// <summary> Время в движении </summary>
		TimeSpan MotionTime { get; }
		/// <summary> Время в неподвижном состоянии </summary>
		TimeSpan StandTime { get; }
		/// <summary> Пробег </summary>
		Double Run { get; }
		/// <summary> Максимальная скорость, км/ч </summary>
		Double MaxSpeed { get; }
	}
}