﻿using System;
using System.Collections.Generic;
using System.Threading;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;

namespace Interfaces.Web
{
	[Serializable]
	public class CustomerItem
	{
		public int            id;
		public string         name;
		public string         desc;
		public string         contract;
		public CustomerKind   kind;
		public DepartmentType type;
	}
	[Serializable]
	public class HitItem
	{
		public string HitType;
		public string HitText;
	}
	[Serializable]
	public class SearchHits<T>
	{
		public T             Item;
		public List<HitItem> Hits;
	}
	[Serializable]
	public enum CustomerKind
	{
		Department = 0,
		Operator   = 1
	}

	public partial interface IWebPersonalServer
	{
		/// <summary> Поиск клиентов по тексту </summary>
		/// <param name="searchText"> Искомый текст </param>
		/// <param name="pagination"> Параметр постраничной выдачи </param>
		/// <param name="cancellationToken"> Маркер отмены операции </param>
		/// <returns></returns>
		List<SearchHits<CustomerItem>> SearchCustomers(string searchText, Pagination pagination, CancellationToken cancellationToken);
	}
}