﻿using System;
using System.Collections.Generic;
using System.Threading;
using FORIS.TSS.BusinessLogic.DTO;

namespace Interfaces.Web
{
	public partial interface IWebPersonalServer
	{
		/// <summary> Получить список общих ссылок на объект созданных текущим пользователем </summary>
		/// <param name="pagination"> Параметры нумерации и разбиения страниц </param>
		/// <param name="cancellationToken"> Token отмены операции </param>
		/// <returns></returns>
		IEnumerable<ShareLink> GetShareLinks(Pagination pagination, CancellationToken cancellationToken);
		/// <summary> CRUD:Create Создать общую ссылку на объект (при полном совпадении параметров возвращается существующая) </summary>
		/// <param name="dtoShareLink"> Объект описания общей ссылки на объект, без идентификатора БД </param>
		/// <returns> Объект описания общей ссылки на объект, с установленным идентификатором БД </returns>
		ShareLink ShareLinkCreate(ShareLink dtoShareLink);
		/// <summary> CRUD:Read Общую ссылку на объект по идентификатору объекта описания общей ссылки на объект </summary>
		/// <param name="shareLinkGuid"> Идентификатор общей ссылки на объект </param>
		/// <returns> Объект описания общей ссылки на объект </returns>
		ShareLink ShareLinkRead(Guid shareLinkGuid);
		/// <summary> CRUD:Update Общую ссылку на объект </summary>
		/// <param name="dtoShareLink"> Измененный объект общей ссылки на объект </param>
		/// <returns> Объект описания общей ссылки на объект из БД, после обновления </returns>
		ShareLink ShareLinkUpdate(ShareLink dtoShareLink);
		/// <summary> CRUD:Delete Удалить общую ссылку на объект, по идентификатору объекта описания общей ссылки на объект </summary>
		/// <param name="shareLinkGuid"> Идентификатор объекта описания общей ссылки объекта </param>
		void ShareLinkDelete(Guid shareLinkGuid);
	}
}