﻿using System;
using System.Collections;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO.Historical;

namespace Interfaces.Web
{
    /// <summary>
    /// Группа точек истории.
    /// </summary>
    [Serializable]
    public class GroupingMobileUnit
    {
        public GroupingType GroupingType = GroupingType.None;
        public List<GroupingMobileUnitNode> Nodes;

        public GroupingMobileUnit(GroupingType groupingType)
        {
            GroupingType = groupingType;
            Nodes = new List<GroupingMobileUnitNode>(1);
        }

        public void Add(GroupingMobileUnitNode node)
        {
            if (node != null)
                Nodes.Add(node);
        }
    }
}
