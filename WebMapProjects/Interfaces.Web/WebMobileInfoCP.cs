﻿
using System;

namespace Interfaces.Web
{
    [Serializable]
    public class WebMobileInfoCP : WebMobileInfo, IWebMobileInfoCP
    {
        private /*readonly*/ int timeCP;
        private readonly double latitudeCP;
        private readonly double longitudeCP;

        public WebMobileInfoCP(
            int unique,
            int id,
            int type,
            int time,
            int? speed,
            int course,
            double latitude,
            double longitude,
            bool validPosition,
            int status,
            int timeCP,
            double latitudeCP,
            double longitudeCP,
            int fuelVolume,
            int temperature
            )
            : base(unique, id, type, time, speed, course, latitude, longitude, validPosition, status, fuelVolume, temperature)
        {
            this.timeCP = timeCP;
            this.latitudeCP = latitudeCP;
            this.longitudeCP = longitudeCP;
        }

        public WebMobileInfoCP(
            int unique,
            int id,
            string objectName,
            int type,
            int time,
            int? speed,
            int course,
            double latitude,
            double longitude,
            bool validPosition,
            int status,
            int timeCP,
            double latitudeCP,
            double longitudeCP,
            int fuelVolume,
            int temperature
            )
            : this(unique, id, type, time, speed, course, latitude, longitude, validPosition, status, timeCP, latitudeCP, longitudeCP, fuelVolume, temperature)
        {
            ObjectName = objectName;
        }

        public int TimeCP
        {
            get { return timeCP; }
            set { timeCP = value; }
        }

        public double LatitudeCP
        {
            get { return latitudeCP; }
        }

        public double LongitudeCP
        {
            get { return longitudeCP; }
        }
    }
}
