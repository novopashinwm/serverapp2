﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.DTO.OperatorRights;
using FORIS.TSS.BusinessLogic.DTO.Params;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Mail;
using FORIS.TSS.BusinessLogic.ResultCodes;
using FORIS.TSS.BusinessLogic.Server;
using FORIS.TSS.Common;
using Interfaces.Geo;
using Interfaces.Web.Enums;
using Command = FORIS.TSS.BusinessLogic.DTO.Command;
using Contact = FORIS.TSS.BusinessLogic.DTO.Contact;
using IPersonalServer = FORIS.TSS.BusinessLogic.Server.IPersonalServer;
using Vehicle = FORIS.TSS.BusinessLogic.DTO.Vehicle;
using VehicleControlDate = FORIS.TSS.BusinessLogic.DTO.VehicleControlDate;

namespace Interfaces.Web
{
	public partial interface IWebPersonalServer : IPersonalServer, IRuleServer, ISalesManagerServer, IGeoAddressProvider, IGeoArrivalProvider
	{
		int    OperatorId           { get; }
		string AppId                { get; }
		bool   IsGuest              { get; }
		bool   AllowUseDialer       { get; }
		bool   AllowAddTracker      { get; }
		bool   AllowSmsSend         { get; }
		bool   AllowChangeOwnLogin  { get; }
		bool   IsSuperAdministrator { get; }

		IRemoteTerminalServerManager GetRemoteTerminalServerManager(int vehicleId);
		/// <summary> Получение расстояния в метрах для указанной машины за указанный диапазон времени </summary>
		/// <param name="from">время начала (UTC)</param>
		/// <param name="to">время окончания (UTC)</param>
		/// <param name="vehicle_id">id машины</param>
		/// <returns>расстояние в метрах</returns>
		Single GetDistance(int from, int to, int vehicle_id);
		/// <summary> Возвращает отсортированный по времени список последних позиций ТС из БД за указанный период времени </summary>
		/// <param name="from">начало периода</param>
		/// <param name="to">конец периода</param>
		/// <param name="id">идентификатор машины</param>
		/// <param name="interval">интервал между позициями в секундах</param>
		/// <param name="count">количество запрашиваемых позиций</param>
		/// <returns>отсортированный по времени полный список сгруппированных последних позиций ТС</returns>
		IWebMobileHistory GetMobileHistory(int from, int to, int id, int interval, int count);
		/// <summary> Возвращает отсортированный по времени список последних позиций ТС из БД за указанный период времени </summary>
		/// <param name="from">начало периода</param>
		/// <param name="to">конец периода</param>
		/// <param name="id">идентификатор машины</param>
		/// <param name="interval">интервал между позициями в секундах</param>
		/// <param name="count">количество запрашиваемых позиций</param>
		/// <param name="groupingOnly">флаг получения либо только групп, либо только позиций без группировки</param>
		/// <returns>отсортированный по времени список групп последних позиций ТС без GPS-данных</returns>
		IWebMobileHistory GetMobileHistory(int from, int to, int id, int interval, int count, bool groupingOnly);
		/// <summary> Универсальная Функция вызова процедур для отчетов </summary>
		/// <returns>Полученные из БД данные, возвращаемые хранимой процедурой</returns>
		DataSet GetDataFromDB(ArrayList arParams, string strStoredProcedureName, string[] strTablesNames);
		/// <summary> Получает информацию об объектах наблюдения для веб-клиента </summary>
		/// <remarks>Не следует увеличивать использование метода</remarks>
		/// <param name="vehicleIds">Необязательный параметр: список объектов наблюдения, которыми следует ограничить выборку</param>
		DataSet GetDataForWeb(params int[] vehicleIds);
		/// <summary> Создание/редактирование норматива межзонного пробега </summary>
		/// <param name="zone_id_from">id 1-й геозоны</param>
		/// <param name="zone_id_to">id 2-й геозоны</param>
		/// <param name="distance">норматив (в метрах)</param>
		void UpdateZoneDistanceStandart(int zone_id_from, int zone_id_to, int distance);
		/// <summary> Получение списка всех нормативов межзонного пробега, доступных данному оператору </summary>
		DataSet GetAllZoneDistanceStandarts();
		IDictionary<int, IWebMobileInfoCP> GetMobilesInfo(int[] iArrayGroup);
		/// <summary> Формирует отчёт на диск </summary>
		/// <param name="reportId"> Идентификатор отчёта </param>
		/// <param name="reportParams"> Список параметров отчёта </param>
		/// <param name="formatType"> Формат файла отчёта </param>
		/// <returns> Полное имя файл отчёта </returns>
		/// <remarks> Отчёты создаются в папке на диске </remarks>
		string CreateReportAsFile(Guid reportId, PARAMS reportParams, ReportTypeEnum formatType);
		/// <summary> Формирует отчёт и возвращает объект для дальнейшей сериализации </summary>
		/// <param name="reportId"> Идентификатор отчёта </param>
		/// <param name="reportParams"> Параметры отчёта </param>
		/// <returns></returns>
		object CreateReportAsObj(Guid reportId, PARAMS reportParams);
		/// <summary> Возвращает список подписок для текущего пользователя в виде коллекции DTO </summary>
		/// <returns></returns>
		List<ReportSubscription> GetSubscriptions();
		DataSet GetAddressesListFromDB(Guid mapGuid, string strFind);
		/// <summary> Получение списка последних позиций для всех объектов доступных пользователю </summary>
		/// <returns></returns>
		IDictionary<int, IWebMobileInfoCP> GetLastPositions();
		/// <summary> Получение списка последних позиций для перечисленных объектов </summary>
		/// <returns></returns>
		IDictionary<int, IWebMobileInfoCP> GetLastPositions(int[] uniques);
		#region Functions for Web Points, Lines and Zones

		DataSet GetPointsWebForOperator();
		void AddPointWebForOperator(PointF point, string pointName, string description, int pointType);
		void EdtPointWebForOperator(int pointID, PointF point, string pointName, string description, int pointType);
		void DelPointWebForOperator(int pointID);
		#region WebPointsNew
		List<WebPoint> GetPointsWeb();
		WebPoint WebPointRead(int webPointId);
		WebPoint WebPointCreate(WebPoint dtoWebPoint);
		WebPoint WebPointUpdate(WebPoint dtoWebPoint);
		void WebPointDelete(int webPointId);
		#endregion WebPointsNew
		DataSet GetLinesWebForOperator(Guid mapGuid, bool vertexGet, int lineID);
		void AddLineWebForOperator(Guid mapGuid, PointF[] points, string lineName, string description);
		void EdtLineWebForOperator(/*Guid mapGuid,*/ int lineID, PointF[] points, string lineName, string description);
		void DelLineWebForOperator(/*Guid mapGuid,*/ int lineID);
		DataSet GetZonesWebForOperator(bool vertexGet, int zoneID, bool includeIgnored = false, bool loadSingle = false);
		/// <summary> Возвращает список DTO для геозон </summary>
		/// <param name="includeVertices">Включить ли в выборку вершины геозон</param>
		/// <param name="zoneId">Идентификатор геозоны, если нужно загрузить конкретную геозону</param>
		/// <param name="includeIgnored">Загружать ли геозоны, помеченные для игнорирования</param>
		/// <param name="loadSingle">Загрузить одну запись с идентификатором или массив записей.</param>
		List<GeoZone> GetGeoZones(bool includeVertices = false, int? zoneId = null, bool includeIgnored = false, bool loadSingle = false);
		SaveZoneResult AddZoneWebForOperator(GeoZone zone, out int zoneId);
		SaveZoneResult EdtZoneWebForOperator(GeoZone zone);
		void DelZoneWebForOperator(int zoneId);

		#endregion Functions for Web Points, Lines and Zones
		/// <summary> Создает новую группу </summary>
		int AddVehicleGroup(string name);
		/// <summary>
		/// Возвращает список групп и входящих в группу машин,
		/// доступных оператору через право <see cref="SystemRight.VehicleAccess"/>
		/// </summary>
		List<Group> GetVehicleGroups(GetGroupsArguments arguments = null);
		/// <summary> Добавляет машину в группу </summary>
		/// <param name="vehicleId">ID машины</param>
		/// <param name="groupId">ID группы</param>
		void AddVehicleToGroup(int vehicleId, int groupId);
		/// <summary> Обновляет на сервере данные группы объектов наблюдения </summary>
		/// <param name="group"> Группа объектов наблюдения </param>
		/// <remarks> Не изменяет список объектов в группе </remarks>
		void UpdateVehicleGroup(Group @group);
		/// <summary> Удаляет машину из группы </summary>
		/// <param name="vehicleId">ID машины</param>
		/// <param name="groupId">ID группы</param>
		void RemoveVehicleFromGroup(int vehicleId, int groupId);
		/// <summary> Удаляет группу объектов наблюдения </summary>
		/// <param name="id"> Идентификатор группы </param>
		void RemoveVehicleGroup(int id);
		/// <summary> Возвращает список запланированных задач для указанного объекта </summary>
		/// <param name="idType">Тип идентификатора объекта</param>
		/// <param name="id">Идентификатор объекта</param>
		IRepetitionClass GetMlpSchedule(IdType idType, int id);
		/// <summary> Возвращает объект по ID на основании типа ID </summary>
		/// <param name="idType">Тип ID</param>
		/// <param name="id">ID</param>
		//TODO: Использование EF в веб-приложении замедляет работу приложения
		object GetEntity(IdType idType, int id);
		/// <summary> Устанавливает расписание по запросу координат через MLP для заданного объекта </summary>
		/// <param name="idType">Тип идентификатора объекта, для которого нужно установить расписание</param>
		/// <param name="id">Идентификатор объекта, для которого нужно установить расписание</param>
		/// <param name="schedule">Объект расписания</param>
		void SetMlpSchedule(IdType idType, int id, IRepetitionClass schedule);
		/// <summary> Включение/выключение подписки на отчет/работу по расписанию </summary>
		/// <param name="id">Идентификатор подписки</param>
		/// <param name="value">Включена/выключена подписка</param>
		void SetScheduleEnabled(int id, bool value);
		/// <summary> Возвращает список актуальных команд с загруженными результатами по переданным устройствам </summary>
		/// <param name="vehicleIDs"> Список идентификаторов устройств </param>
		/// <remarks> Актуальные команды - это те команды,
		/// информация о процессе выполнения или результате может быть интересна пользователю в данный момент </remarks>
		List<Command> GetActualCommands(int[] vehicleIDs);
		/// <summary> Получение списка персональных объектов операторов, которые могут видеть текущего оператора
		/// (вместо операторов возвращаются vehicle устройств представляющих этих операторов)
		/// </summary>
		/// <returns>Список операторов</returns>
		List<ShortVehicleInfo> WhoCanSeeMe();
		/// <summary> Возвращает список объектов мониторинга по праву пользователя и по типу контроллера </summary>
		/// <param name="right"></param>
		/// <param name="controllerTypeName"></param>
		/// <returns></returns>
		/// <remarks>Заполняются только unique = vehicleID и ObjectName = vehicle.garageNumber</remarks>
		List<IWebMobileInfo> GetVehiclesByRightsAndControllerType(SystemRight right, string controllerTypeName);
		/// <summary> Возвращает список системных прав пользователя </summary>
		/// <returns></returns>
		List<SystemRight> GetOperatorSystemRights(int operatorId);
		/// <summary> Возвращает набор прав на транспортные средства и группы для выбранного оператора </summary>
		/// <param name="operatorId">Id оператора</param>
		/// <returns></returns>
		Operator GetOperatorVehicleRights(int? operatorId = null);
		/// <summary> Сохранение изменения информации по оператору </summary>
		/// <param name="operInfo">информации по оператору</param>
		/// <returns></returns>
		SetNewLoginResult SaveOperatorInfo(Operator operInfo);
		/// <summary> Сохранение прав на группы машин и машины по оператору </summary>
		/// <param name="operInfo">информации по оператору</param>
		/// <returns></returns>
		void ChangeOperatorVehicleRights(Operator operInfo);
		/// <summary> Удаляет все права оператора на департамент и машины департамента (пропадает для админа департамента, но остается в системе) </summary>
		/// <param name="operatorIdToDelete"></param>
		void DeleteOperatorFromDepartment(int operatorIdToDelete);
		/// <summary> Возвращает исчерпывающую информацию об объектах наблюдения, доступных оператору </summary>
		List<Vehicle> GetVehicles(GetVehiclesArgs arguments = null);
		/// <summary> Получить список контрольных дат для автомобиля </summary>
		/// <param name="vehicleId"></param>
		/// <returns></returns>
		List<VehicleControlDate> GetVehicleControlDates(int vehicleId);
		/// <summary> Возвращает список групп геозон, доступных оператору </summary>
		/// <param name="getGroupsArguments"></param>
		Group[] GetGeoZoneGroups(GetGroupsArguments getGroupsArguments = null);
		/// <summary> Возвращает список групп геозон, доступных оператору </summary>
		Group ZoneGroupCreate(string name);
		Group ZoneGroupUpdate(string name, int id);
		void ZoneGroupDelete(int id);
		/// <summary> Добавляет зону в группу зон </summary>
		void ZoneGroupIncludeZone(int zoneGroupId, int zoneId);
		/// <summary> Удаляет зону из группы зон </summary>
		void ZoneGroupExcludeZone(int zoneGroupId, int zoneId);
		/// <summary> Хранимый часовой пояс оператора </summary>
		TimeZoneInfo TimeZoneInfo { get; }
		/// <summary> Языковые настройки оператора </summary>
		CultureInfo CultureInfo { get; }
		/// <summary> Обновляет на сервере текущую временную зону оператора</summary>
		/// <param name="timeZoneInfo">Данные о временной зоне</param>
		/// <param name="isTimeZoneInfoManual">true, если временная зона задана оператором вручную
		///     и false, если получена автоматически на основании данных браузера</param>
		/// <returns>true, если зона была изменена и false в противном случае</returns>
		bool UpdateTimeZoneInfo(TimeZoneInfo timeZoneInfo, bool isTimeZoneInfoManual);
		/// <summary> Обновляет на сервере культуру оператора </summary>
		/// <param name="cultureCode">Код культуры оператора</param>
		/// <returns>Возвращает true, если код культуры был изменён и false в противном случае</returns>
		bool UpdateCulture(string cultureCode);
		/// <summary>Возвращает данные по истории наблюдения за объектом</summary>
		/// <param name="vehicleID">Идентификатор объекта наблюдения</param>
		/// <param name="logTimeFrom">Время начала периода наблюдения</param>
		/// <param name="logTimeTo">Время конца периода наблюдения</param>
		History GetHistory(int vehicleID, int logTimeFrom, int logTimeTo, FullLogOptions options);
		/// <summary> Возвращает список доступных типов топлива </summary>
		List<FuelType> GetFuelTypes();
		/// <summary> Возвращает код валюты для данного оператора </summary>
		string GetCurrency();
		/// <summary> Возвращает перечень типов контроллеров, доступных для добавления в систему </summary>
		ControllerType[] GetControllerTypeAllowedToAdd();
		/// <summary> Возвращает перечень типов контроллеров, доступных для назначения трекеру </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения, которому назначается новый тип трекера </param>
		ControllerType[] GetControllerTypeAllowedToAssign(int vehicleId);
		/// <summary> Возвращает байты изображения </summary>
		/// <param name="vehicleID"> ID объекта наблюдения </param>
		/// <param name="logTime"> Время </param>
		Picture GetPicture(int vehicleID, int logTime);
		byte[] GetThumbnailBytes(int vehicleID, int logTime, int? width, int? height);
		/// <summary> Возвращает идентификаторы соседних изображений </summary>
		/// <param name="vehicleId">ID объекта наблюдения</param>
		/// <param name="logTime">Время</param>
		/// <returns>Возвращает список идентификаторов фотографии соседних с основным.</returns>
		List<int> GetNearestPictures(int vehicleId, int logTime);
		/// <summary> Удаление оператора из текущего департамента </summary>
		/// <param name="operatorId"></param>
		void DeleteOperator(int operatorId);
		#region Управление клиентами (департаментами/автоколоннами)

		/// <summary>Текущий департамент, с которым работает оператор</summary>
		Department Department { get; }
		/// <summary>Выбор текущего департамента, с которым работает оператор по id департамента</summary>
		void SelectDepartment(int? departmentId);
		/// <summary> Установка текущего приложения </summary>
		/// <param name="appId"></param>
		void SelectAppId(string appId);
		/// <summary> Возвращает список департаментов, которые позволено администрировать оператору </summary>
		/// <returns></returns>
		List<Department> GetAvailableDepartments();
		/// <summary> Возвращает количество департаментов, которые позволено администрировать оператору </summary>
		/// <returns></returns>
		int GetAvailableDepartmentsCount();
		/// <summary> Поиск департаментов ко тексту </summary>
		/// <param name="searchText"></param>
		/// <returns></returns>
		List<Department> SearchAdminDepartments(string searchText);
		/// <summary> Поиск пользователей по тексту </summary>
		/// <param name="searchText"></param>
		/// <returns></returns>
		List<Operator> SearchOperators(string searchText);
		bool AllowAddOperator { get; }

		# endregion Управление клиентами (департаментами/автоколоннами)

		/// <summary> Добавляет оператору новый контакт </summary>
		/// <param name="type">Тип контакта, например <see cref="ContactType.Email"/></param>
		/// <param name="value">Значение контакта, например, 1@2.ru</param>
		/// <param name="operatorContactId">Идентификатор нового контакта</param>
		/// <returns>Результат добавления контакта, возможные варианты:
		/// <see cref="SetNewContactResult.Success"/>,
		/// <see cref="SetNewContactResult.InvalidFormat"/>,
		/// <see cref="SetNewContactResult.AlreadyExists"/>.
		/// </returns>
		[Obsolete]
		SetNewContactResult AddContact(ContactType type, string value, out int operatorContactId);
		/// <summary> Добавляет оператору новый контакт </summary>
		/// <param name="type"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		AddResult<Contact> AddContact(ContactType type, string value);
		/// <summary> Удаление выбранного контакта. </summary>
		/// <param name="contactId"></param>
		SetNewContactResult DeleteContact(int contactId);
		/// <summary> Возвращает бизнес-коллекцию всех email'ов пользователя </summary>
		/// <param name="operatorId"></param>
		/// <returns></returns>
		Contact[] GetOperatorEmails(int operatorId);
		/// <summary> Возвращает коллекцию  всех контактов заданного типа для текущего пользователя </summary>
		Contact[] GetContacts(ContactType contactType);
		/// <summary> Верификация контакта, подтверждение и назначение верифицируемому оператору </summary>
		/// <param name="contactId">Контакт для верификации</param>
		/// <param name="confirmationCode">Код подтверждения</param>
		/// <returns>Возвращает статус состояния</returns>
		VerifyContactResult VerifyContact(int contactId, string confirmationCode);
		/// <summary> Верификация номера телефона, подтверждение и назначение верифицируемому оператору </summary>
		/// <param name="msisdn">Номер телефона для верификации</param>
		/// <param name="confirmationCode">Код подтверждения</param>
		/// <returns>Возвращает true, если удалось подтвердить и false в противном случае</returns>
		VerifyContactResult VerifyMsisdn(string msisdn, string confirmationCode);
		/// <summary> Возвращает всех бизнес операторов, на которых имеются права для всех департаментов, исключая равных по праву администраторов </summary>
		/// <returns></returns>
		List<Operator> GetAllBelongOperators();
		/// <summary> Возвращает детализированную карточку оператора </summary>
		/// <param name="operatorId">ID оператора</param>
		/// <returns></returns>
		Operator GetOperatorDetails(int operatorId);
		/// <summary> Возвращает набор прав на геозоны и группы для выбранного оператора </summary>
		/// <param name="operatorId">Id оператора</param>
		/// <returns></returns>
		Operator GetOperatorZoneRights(int operatorId);
		/// <summary> Возвращает набор базовых прав оператора в системе </summary>
		/// <param name="operatorId">Id оператора</param>
		/// <returns></returns>
		Operator GetOperatorRights(int operatorId);
		/// <summary> Изменяет права оператора на геозоны и группы гоеозон </summary>
		/// <param name="oper">JSON объект оператора(??)</param>
		void ChangeOperatorZoneRights(Operator oper);
		/// <summary> обновить информацию автомобиля </summary>
		/// <param name="vehicleId"></param>
		/// <param name="fuelSpentStandart"></param>
		/// <param name="fuelSpentStandardMH"></param>
		/// <param name="regNumber"></param>
		/// <param name="garageNumber"></param>
		/// <param name="brand"></param>
		/// <param name="fuelTypeId"></param>
		/// <param name="fuelTankVolume"></param>
		/// <param name="vehicleKind"></param>
		/// <param name="ownerFirstName"></param>
		/// <param name="ownerSecondName"></param>
		/// <param name="ownerLastName"></param>
		/// <param name="ownerPhone1"></param>
		/// <param name="ownerPhone2"></param>
		/// <param name="ownerPhone3"></param>
		/// <param name="vehicleImage"></param>
		void UpdateVehicleInfo(int vehicleId, decimal? fuelSpentStandart, decimal? fuelSpentStandardMH, string regNumber, string garageNumber, string brand, int? fuelTypeId, int? fuelTankVolume, int? vehicleKind, string ownerFirstName, string ownerSecondName, string ownerLastName, string ownerPhone1, string ownerPhone2, string ownerPhone3, byte[] vehicleImage);
		void UpdateVehicleControlDates(List<VehicleControlDate> controlDates, int vehicleId);
		void ReportToSchedulerQueue(Guid reportId, PARAMS reportParams, IRepetitionClass repetition, DateTime reportCreateTime, List<int> listEmailIds);
		/// <summary>Возвращает время на сервере</summary>
		/// <remarks>Для проверки отзывчивости ремотинга</remarks>
		DateTime GetServerDateTime();
		/// <summary> получить список водителей не прикрепленных ни к одной машине </summary>
		/// <returns></returns>
		List<Driver> GetFreeDrivers();
		/// <summary> Получить список водителей для автомобиля </summary>
		/// <param name="vehicleId"></param>
		/// <returns></returns>
		List<Driver> GetVehicleDrivers(int vehicleId);
		/// <summary> Назначить водителя грузовику </summary>
		/// <param name="vehicleId"></param>
		/// <param name="driverId"></param>
		void SetVehicleDriver(int vehicleId, int driverId);
		/// <summary> Удалить привязку водителя и автомобиля </summary>
		/// <param name="vehicleId"></param>
		/// <param name="driverId"></param>
		void DeleteVehicleDriver(int vehicleId, int driverId);
		/// <summary> Обновить данные водителя </summary>
		/// <param name="driverId"></param>
		/// <param name="firstName"></param>
		/// <param name="shortName"></param>
		/// <param name="phones"></param>
		/// <returns></returns>
		Driver UpdateDriver(int driverId, string firstName, string shortName, string[] phones);
		/// <summary> Добавить нового драйвера </summary>
		/// <param name="firstName"></param>
		/// <param name="shortName"></param>
		/// <param name="phones"></param>
		/// <param name="vehicleId"></param>
		/// <returns></returns>
		Driver AddDriver(string firstName, string shortName, string[] phones, int? vehicleId = null);
		/// <summary> Удаление водителя из БД </summary>
		/// <param name="driverId"></param>
		void DeleteBaseVehicleDriver(int driverId);
		LocationInfo GetLocationByIP(string ip);
		/// <summary>
		/// Генерирует код подтверждения контакта пользователя, привязывает его к контакту пользователя, отправляет сообщение, содержащее этот код.
		/// Далее пользователь должен подтвердить, что он владеет указанным контактом, передав полученный контрольный код в методе <see cref="ConfirmContact"/>
		/// </summary>
		/// <param name="contactId">Идентификатор контакта пользователя</param>
		/// <param name="subjectFormatString">Шаблон темы сообщения, подаётся единственный аргумент {0} - код подтверждения</param>
		/// <param name="messageFormatString">Шаблон текста сообщения, подаётся единственный аргумент {0} - код подтверждения</param>
		SendConfirmationCodeResult SendConfirmationCode(int contactId, string subjectFormatString, string messageFormatString);
		/// <summary> Устанавливает значение поля Имя для контакта </summary>
		/// <param name="id">Идентификатор контакта</param>
		/// <param name="value">Новое значение поля Имя</param>
		void SetContactName(int id, string value);
		/// <summary> Устанавливает значение поля Комментарий для контакта </summary>
		/// <param name="id">Идентификатор контакта</param>
		/// <param name="value">Новое значение поля Комментарий</param>
		void SetContactComment(int id, string value);
		/// <summary> Возвращает список доступных типов объектов наблюдения, которые можно присвоить данному объекту наблюдения </summary>
		/// <param name="vehicleId">Идентификатор объекта наблюдения</param>
		VehicleKind[] GetAccessibleVehicleKinds(int vehicleId);
		/// <summary> Регистрирует в системе мобильный клиент или обновляет данные регистрации мобильного клиента </summary>
		/// <param name="appId">Уникальный идентификатор приложения, генерируется приложением</param>
		/// <param name="gcmRegistrationId">Идентификатор приложения в облаке Google</param>
		/// <param name="apnDeviceToken"></param>
		/// <param name="debug">Подключается приложение с отладкой</param>
		void UpdateClientRegistration(string appId, string gcmRegistrationId, string apnDeviceToken, bool debug = false);
		/// <summary> Добавляет в БД информацию о текущем UserAgent'е пользователя </summary>
		/// <param name="userAgent">Строка UserAgent'а из заголовка</param>
		void LogUserAgent(string userAgent);
		/// <summary> Подчищает данные, связанные с сессией </summary>
		/// <remarks> В частности, когда пользователь явно выходит, полезно удалить его клиент, чтобы не слать ему оповещения </remarks>
		void CleanupBeforeLogout();
		/// <summary> Устанавливает значения параметров текущей сессии </summary>
		/// <param name="sessionParameters"></param>
		void SetSessionParameters(SessionParameters sessionParameters);
		/// <summary> Проверяет заблокирован клиент или нет </summary>
		/// <returns></returns>
		bool IsLocked();
	}
}