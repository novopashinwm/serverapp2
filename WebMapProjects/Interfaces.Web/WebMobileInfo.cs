﻿using System;

namespace Interfaces.Web
{
    [Serializable]
    public class WebMobileInfo :
        IWebMobileInfo
    {
        private readonly int unique;
        private readonly int id;
        private readonly int type;
        private /*readonly*/ int time;
        private readonly int? speed;
        private readonly int course;
        private readonly double latitude;
        private readonly double longitude;
        private readonly bool validPosition;
        private readonly int status;
        private readonly int fuelVolume;
        private readonly int temperature;        
        
        public WebMobileInfo(
            int unique,
            int id,
            int type,
            int time,
            int? speed,
            int course,
            double latitude,
            double longitude,
            bool validPosition,
            int status,
            int fuelVolume,
            int temperature
            )
        {
            this.unique = unique;
            this.id = id;
            this.type = type;
            this.time = time;
            this.speed = speed;
            this.course = course;
            this.latitude = latitude;
            this.longitude = longitude;
            this.validPosition = validPosition;
            this.status = status;
            this.fuelVolume = fuelVolume;
            this.temperature = temperature;
        }

        public WebMobileInfo(
            int unique,
            int id,
            string objectName,
            int type,
            int digitalSensors,
            int time,
            int speed,
            int course,
            double latitude,
            double longitude,
            bool validPosition,
            int status,
            int fuelVolume,
            int temperature
            )
            : this(unique, id, type, time, speed, course, latitude, longitude, validPosition, status, fuelVolume, temperature)
        {
            ObjectName = objectName;
        }

        #region IWebMobileInfo Members

        /// <summary>
        /// VehicleID
        /// </summary>
        public int Unique
        {
            get { return this.unique; }
        }

        /// <summary>
        /// Controller.Number
        /// </summary>
        public int ID
        {
            get { return this.id; }
        }

        public string ObjectName
        {
            get;
            set;
        }

        public int Type
        {
            get { return this.type; }
        }

        public int Time
        {
            get { return this.time; }
            set { time = value; }
        }

        public int? Speed
        {
            get { return this.speed; }
        }

        public int Course
        {
            get { return this.course; }
        }

        public double Latitude
        {
            get { return this.latitude; }
        }

        public double Longitude
        {
            get { return this.longitude; }
        }

        public double? Radius
        {
            get; set;
        }

        public bool ValidPosition
        {
            get { return this.validPosition; }
        }

        public int Status
        {
            get { return this.status; }
        }

        public int FuelVolume
        {
            get { return this.fuelVolume; }
        }

        public int Temperature
        {
            get { return this.temperature; }
        }

        public string LastPositionString
        {
            get; set;
        }

        #endregion
    }
}
