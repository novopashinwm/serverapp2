﻿using FORIS.TSS.BusinessLogic.DTO;
using Interfaces.Web.Enums;

namespace Interfaces.Web.Result
{
	public class SetControlDateResult
	{
		public SetVehicleControlDateResult Result;
		public VehicleControlDate          Record;
	}
}