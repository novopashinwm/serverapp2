﻿using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;

namespace Interfaces.Web
{
	public partial interface IWebPersonalServer
	{
		/// <summary> Устанавливает новое имя пользователя в системе </summary>
		/// <param name="name">Имя пользователя, до 250 символов</param>
		void SetUserName(string name);
		/// <summary> Идентификатор объекта наблюдения, который является собственным для данного пользователя </summary>
		/// <remarks> Собственный объект наблюдения соответствует приложению обслуживания, к которому привязана учётная запись пользователя </remarks>
		int? OwnVehicleId { get; }
		/// <summary> Возвращает или создаёт идентификатор, по которому можно аутентифицировать и к которому можно привязывать данные из мобильного приложения </summary>
		string GetOrCreateSimId();
		/// <summary> Возвращает личные номер телефона пользователя </summary>
		string GetMsisdn();
		/// <summary>Меняет пароль пользователя</summary>
		/// <param name="oldPassword">Старый пароль</param>
		/// <param name="newPassword">Новый пароль</param>
		SetNewPasswordResult ChangePassword(string oldPassword, string newPassword);
		/// <summary> Возвращает список услуг, доступных пользователю </summary>
		List<BillingService> GetBillingServices();
	}
}