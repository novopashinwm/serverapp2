﻿using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO;

namespace Interfaces.Web
{
	public partial interface IWebPersonalServer
	{
		bool IsSalesManager { get; }
		/// <summary> Возвращает список услуг, доступных для добавления </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		List<BillingServiceType> GetServiceTypesAllowedToAdd(int vehicleId);
		/// <summary> Добавляет услугу указанного типа на объект наблюдения и его приложение обслуживания </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		/// <param name="billingServiceTypeId"> Тип услуги </param>
		void AddService(int vehicleId, int billingServiceTypeId);
		/// <summary> Удаляет указанную услугу </summary>
		/// <param name="billingServiceId"> Идентификатор услуги </param>
		void DelService(int billingServiceId);
		/// <summary> Добавляет блокировку на объект наблюдения </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		void AddBlocking(int vehicleId);
		/// <summary> Удаляет блокировку с объекта наблюдения </summary>
		/// <param name="vehicleId"> Идентификатор объекта наблюдения </param>
		void DelBlocking(int vehicleId);
	}
}