﻿using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Contacts;
using FORIS.TSS.BusinessLogic.Enums;
using Interfaces.Web.Enums;

namespace Interfaces.Web
{
	public partial interface IWebPersonalServer
	{
		/// <summary> Возвращает список контактов оператора </summary>
		/// <param name="operatorId"></param>
		/// <returns></returns>
		Contact[] GetOperatorPhones(int operatorId);
		/// <summary> Отправляет код подтверждения </summary>
		/// <param name="contactType"></param>
		/// <param name="value"></param>
		/// <param name="subjectFormat"></param>
		/// <param name="bodyFormat"></param>
		/// <returns></returns>
		SendConfirmationCodeResult SendConfirmationCode(ContactType contactType, string value, string subjectFormat, string bodyFormat);
		/// <summary> Сохраняет книгу контактов </summary>
		/// <param name="phoneBook"></param>
		/// <returns></returns>
		SavePhoneBookResult SavePhoneBook(PhoneBook phoneBook);
		/// <summary> Возвращает идентификатор контакта. </summary>
		/// <param name="contactType"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		int GetContactId(ContactType contactType, string value);
		/// <summary> Устанавливает значение поля Имя для контакта. </summary>
		/// <param name="msisdn"> Номер телефона контакта </param>
		/// <param name="value"> Новое значение поля Имя </param>
		void UpdatePhoneBookContactName(string msisdn, string value);
	}
}