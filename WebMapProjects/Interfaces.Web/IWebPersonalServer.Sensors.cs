﻿using System;
using System.Collections.Generic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;

namespace Interfaces.Web
{
	public partial interface IWebPersonalServer
	{
		bool IsAllowedSensorsView(int vehicleId);
		bool IsAllowedSensorsEdit(int vehicleId);
		/// <summary> Добавляет маппинг между отображаемым датчиком и входом блока </summary>
		/// <param name="vehicleID">ID объекта наблюдения</param>
		/// <param name="sensorMap">Маппинг датчиков</param>
		[Obsolete]
		int? AddSensorMapping(int vehicleID, SensorMap sensorMap);
		/// <summary>Изменяет маппинг между отображаемым датчиком и входом блока</summary>
		/// <param name="sensorMap">Маппинг датчиков</param>
		[Obsolete]
		void UpdateSensorMapping(SensorMap sensorMap);
		/// <summary>Удаляет маппинг</summary>
		/// <param name="mappingId">Идентификатор маппинга</param>
		[Obsolete]
		void DeleteSensorMapping(int mappingId);
		/// <summary>Возвращает информацию о маппинге датчиков для объекта наблюдения</summary>
		/// <param name="vehicleID">ID объекта наблюдения</param>
		[Obsolete]
		SensorMap[] GetSensorMappings(int vehicleID);
		/// <summary> Сохраняет маппинги датчиков для выбранной легенды </summary>
		/// <param name="vehicleId">Идентификатор объекта</param>
		/// <param name="sensorMap">Массив маппингов датчиков</param>
		/// <returns></returns>
		void SetSensorsMapping(int vehicleId, SensorLegendMapping[] sensorMap);
		/// <summary> Сохраняет маппинги датчиков для выбранной легенды </summary>
		/// <param name="vehicleId">Идентификатор объекта</param>
		/// <param name="sensorLegend">Легенда</param>
		/// <param name="sensorMap">Массив маппингов датчиков</param>
		/// <returns></returns>
		void SetSensorLegendMapping(int vehicleId, SensorLegend sensorLegend, SensorMappingValue[] sensorMap);
		/// <summary> Получает маппинги по легенде </summary>
		/// <param name="vehicleId">Идентификатор объекта</param>
		/// <param name="sensorLegend">Легенда</param>
		/// <returns></returns>
		SensorMappingValue[] GetSensorLegendMappings(int vehicleId, SensorLegend sensorLegend);
		/// <summary> Возвращает словарь замапленных датчиков </summary>
		/// <param name="vehicleId"></param>
		/// <returns></returns>
		SensorLegendMapping[] GetSensorsMappings(int vehicleId);
		/// <summary>Возвращает информацию о доступных входах контроллера для объекта наблюдения</summary>
		/// <param name="vehicleID">ID объекта наблюдения</param>
		ControllerInput[] GetControllerInputs(int vehicleID);
		/// <summary>Возвращает информацию о типах датчиков, доступных данному объекту наблюдения </summary>
		List<Sensor> GetSensorLegends(int vehicleId);
		/// <summary> Возвращает дискретные датчики, которые присутствуют хотя бы на одном объекте наблюдения, доступном пользователю </summary>
		Sensor[] GetConfiguredSensorLegends(SensorType? sensorType = null, int? vehicleId = null);
		/// <summary>
		/// Добавляет, изменяет или удаляет информацию о фактическом изменении значения измеряемого параметра.
		/// Например, позволяет задать объем заправленного топлива
		/// </summary>
		void SensorValueChange(int vehicleId, SensorLegend sensorLegend, int logTime, decimal? value);
	}
}