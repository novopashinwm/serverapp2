﻿using FORIS.TSS.BusinessLogic.Terminal.Model;

namespace Interfaces.Web
{
	/// <summary> Управление удаленными серверами, на которые транслируются данные от объекта наблюдения </summary>
	public interface IRemoteTerminalServerManager
	{
		/// <summary> Возвращает список удаленных серверов </summary>
		RemoteTerminalServer[] GetRemoteTerminalServers();

		/// <summary> Задает параметры отправки данных на удаленный сервер </summary>
		/// <param name="remoteTerminalServer">Удаленный сервер, учитывается только <see cref="RemoteTerminalServer.Id"/> для идентификации, 
		/// <see cref="RemoteTerminalServer.DeviceID"/> для модификации</param>
		void SetRemoteTerminalServers(RemoteTerminalServer remoteTerminalServer);
	}
}