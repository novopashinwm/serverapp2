﻿
namespace Interfaces.Web
{
	/// <summary> Интерфейс, который дополняет информацию о ТС последним корректным временем и позицией </summary>
	public interface IWebMobileInfoCP : IWebMobileInfo
	{
		int TimeCP         { get; set; }
		double LatitudeCP  { get; }
		double LongitudeCP { get; }
	}
}