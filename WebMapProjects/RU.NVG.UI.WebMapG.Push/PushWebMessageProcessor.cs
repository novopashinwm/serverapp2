﻿using System;
using System.Diagnostics;
using System.Linq;
using FORIS.TSS.BusinessLogic.DTO.Push;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.EntityModel;
using FORIS.TSS.ServerApplication.BackgroundProcessors;
using Microsoft.AspNet.SignalR;

namespace RU.NVG.UI.WebMapG.Push
{
	/// <summary> Процессор, который обрабатывает новые сообщения для Web (MessageDestinationType.Web) и посылает клиентам </summary>
	public class PushWebMessageProcessor : BackgroundProcessorSingleton<PushWebMessageProcessor>
	{
		private readonly int      _undoSeconds;
		private          DateTime _lastProcessedTime;


		protected static Func<Entities, DateTime, IQueryable<MESSAGE>> GetMessageBasicQuery =
			(ctx, time) => ctx.MESSAGE
				.Include(MESSAGE.MessageTemplateIncludePath)
				.Include(MESSAGE.MessageFieldIncludePath)
				.Include(MESSAGE.MessageFieldIncludePath + "." + MESSAGE_FIELD.MessageTemplateFieldIncludePath)
				.Where(m => m.TIME > time && m.DestinationType_ID == (int)MessageDestinationType.Web);

		public PushWebMessageProcessor()
			: base(TimeSpan.FromSeconds(1))
		{
			_lastProcessedTime = DateTime.UtcNow;
			_undoSeconds       = ConfigHelper.GetAppSettingAsInt32(this.GetType(), "Timeout", 10);
		}
		protected MessageProcessingResult ProcessMessage(MESSAGE message)
		{
			if (!message.Owner_Operator_ID.HasValue)
				return MessageProcessingResult.Undeliverable;
			// Определяем оператора
			var operatorId   = message.Owner_Operator_ID.Value;
			// Определяем шаблон сообщения
			var templateName = message.MESSAGE_TEMPLATE != null ? message.MESSAGE_TEMPLATE.NAME : string.Empty;
			// Формируем сообщение
			var pushMessage = new PushMessage
			{
				Id         = message.MESSAGE_ID,
				State      = (MessageState)(message.STATUS ?? 1),
				Subject    = message.SUBJECT,
				Body       = message.BODY,
				Template   = templateName,
				Parameters = message.MESSAGE_FIELD.ToDictionary(
					key => key.MESSAGE_TEMPLATE_FIELD.NAME, value => value.CONTENT)
			};
			// Получаем Hub SignalR
			var hub = GlobalHost.ConnectionManager.GetHubContext<MessagesHub>();
			// Получаем все соединения оператора, владельца сообщения
			var connections = MessagesHubSecurity.Instance.Impersonate(operatorId);
			if(connections == null)
				return MessageProcessingResult.InvalidDestinationAddress;
			// Для каждого соединения вызываем метод SignalR.ReceivePushMessage
			foreach (var connection in connections)
				hub.Clients.Client(connection.ConnectionId).ReceivePushMessage(pushMessage);

			return MessageProcessingResult.Success;
		}
		protected override bool Do()
		{
			using (var entities = new Entities())
			{
				if (_undoSeconds > 0)
					entities.UndoLateMessages((int)MessageDestinationType.Web, _undoSeconds);
				var messages = GetMessageBasicQuery(entities, _lastProcessedTime);
				foreach (var message in messages)
				{
					try
					{
						ProcessMessage(message);
					}
					catch (Exception ex)
					{
						Trace.TraceError(GetType().Name + ": error ProcessMessage {0} {1}", message, ex);
					}
				}

				entities.SaveChanges();

				var result = messages.Any();
				if (result)
					_lastProcessedTime = messages.Max(m => m.TIME);
				return result;
			}
		}
	}
}