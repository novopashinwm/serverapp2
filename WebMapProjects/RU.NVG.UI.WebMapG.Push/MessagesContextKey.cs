﻿using System;

namespace RU.NVG.UI.WebMapG.Push
{
	public class MessagesContextKey : TokenRegistrationInfo
	{
		/// <summary> Идентификатор подключения текущего Hub SignalR </summary>
		public string   ConnectionId;
		/// <summary> Дата подключения к хабу </summary>
		public DateTime ConnectTime;
		/// <summary> Дата подключения к хабу в секундах от 1970.01.01 </summary>
		public int      ConnectLogTime;
	}
}