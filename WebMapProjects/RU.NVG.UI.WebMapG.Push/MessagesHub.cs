﻿using System.Linq;
using System.Security;
using System.Threading.Tasks;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Helpers;
using Microsoft.AspNet.SignalR;

namespace RU.NVG.UI.WebMapG.Push
{
	public class MessagesHub : Hub
	{
		public const string SendPushMessagesMethod = "SendPushMessages";
		public override Task OnDisconnected(bool stopCalled)
		{
			MessagesHubSecurity.Instance.Unbind(Context.ConnectionId);
			return base.OnDisconnected(stopCalled);
		}
		public override Task OnConnected()
		{
			var userToken = Context.QueryString["UserToken"];
			MessagesHubSecurity.Instance.Bind(userToken, Context.ConnectionId);
			return base.OnConnected();
		}
		/// <summary> Подписаться на уведомления о входящих смс с номера телефона </summary>
		/// <param name="msisdn"> Номер телефона </param>
		/// <param name="historyId"> LogTime сообщения которое уже есть на клиенте (странное название параметра, но оно такое есть) </param>
		public void SubscribeMsisdn(string msisdn, int historyId)
		{
			msisdn = ContactHelper.GetNormalizedPhone(msisdn);

			var connectionId = Context.ConnectionId;
			var contextKey = MessagesHubSecurity.Instance.GetMessagesContextKeyByConnectionId(connectionId);
			if (contextKey == null)
				throw new SecurityException("not allowed");

			if (MessagesHubSecurity.Instance.ExistSubscribe(contextKey, msisdn))
				return;

			var messageCheckDate        = TimeHelper.GetDateTimeUTC(historyId);
			var stateProcessorCheckDate = PushSmsMessageProcessor.Instance.LastUpdateTime;
			PushSmsMessageProcessor.Instance.Lock();
			try
			{
				var contextInfo = MessagesHubSecurity.Instance.Subscribe(contextKey, msisdn);
				var contactIds = new[] {contextInfo.AsidId, contextInfo.MsisdnId}.Where(id => id != null).Cast<int>().ToArray();
				var connectionsChanges = PushSmsMessageProcessor.Instance.GetMessageUpdates(contactIds, messageCheckDate, stateProcessorCheckDate);
				Groups.Add(connectionId, msisdn);
				if(connectionsChanges.ContainsKey(connectionId))
					Clients.Caller.MessageUpdates(connectionsChanges[connectionId]);
			}
			finally
			{
				PushSmsMessageProcessor.Instance.Unlock();
			}
		}
		public Task UnsubscribeMsisdn(string msisdn)
		{
			msisdn = ContactHelper.GetNormalizedPhone(msisdn);

			var connectionId = Context.ConnectionId;
			var contextKey = MessagesHubSecurity.Instance.GetMessagesContextKeyByConnectionId(connectionId);
			if (contextKey == null)
				throw new SecurityException("not allowed");

			MessagesHubSecurity.Instance.UnSubscribe(contextKey, msisdn);
			return Groups.Remove(connectionId, msisdn);
		}
	}
}