﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Messages;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using FORIS.TSS.Helpers;
using FORIS.TSS.ServerApplication;
using FORIS.TSS.ServerApplication.BackgroundProcessors;
using Microsoft.AspNet.SignalR;
using RU.NVG.UI.WebMapG.Model.Messages;

namespace RU.NVG.UI.WebMapG.Push
{
	/// <summary> Процессор, который определяет последние изменения в сообщениях и отправляет их в Wpp </summary>
	public class PushSmsMessageProcessor : BackgroundProcessorSingleton<PushSmsMessageProcessor>
	{
		/// <summary> Событие для синхронизации получения обновлений при подписке </summary>
		private readonly object _historyMessagesLock = new object();
		public PushSmsMessageProcessor()
			: base(TimeSpan.FromSeconds(1))
		{
			BeforeStart += delegate
			{
				LastUpdateTime = DateTime.UtcNow;
				using (var entities = new Entities())
				{
					_maxMessageId = entities.MESSAGE
						.Select(m => m.MESSAGE_ID)
						.OrderByDescending(x => x)
						.FirstOrDefault();
				}
			};
		}
		public DateTime LastUpdateTime { get; set; }
		public void Lock()
		{
			Monitor.Enter(_historyMessagesLock);
		}
		public void Unlock()
		{
			Monitor.Exit(_historyMessagesLock);
		}
		private int _maxMessageId;
		public Dictionary<string, Dictionary<string, Dictionary<ChangeType, Dictionary<int, MessagesChangeLocale>>>> GetMessageUpdates(
			int[] contactIds, DateTime from, DateTime to)
		{
			var connectionsChanges = new Dictionary<string, Dictionary<string, Dictionary<ChangeType, Dictionary<int, MessagesChangeLocale>>>>();
			var updatedMessages = Server.Instance().GetUpdatedMessages(from, to);
			if (!updatedMessages.Any())
				return connectionsChanges;

			var contactIdsHashSet = new HashSet<int>(contactIds);
			var phoneChanges = new Dictionary<string, Dictionary<int, MessageChange>>();
			foreach (var message in updatedMessages)
			{
				if ((message.From == null || !contactIdsHashSet.Contains(message.From.Id)) &&
					(message.To   == null || !contactIdsHashSet.Contains(message.To.Id)))
					continue;

				var messageChange = new MessageChange();
				messageChange.Message = message;
				messageChange.ChangeType = _maxMessageId < message.Id ? ChangeType.Insert : ChangeType.Update;

				if (message.From != null)
				{
					if (!phoneChanges.ContainsKey(message.From.Value))
						phoneChanges.Add(message.From.Value, new Dictionary<int, MessageChange>());

					var fromChanges = phoneChanges[message.From.Value];
					if (!fromChanges.ContainsKey(message.Id))
						fromChanges.Add(message.Id, messageChange);
				}

				if (message.To != null)
				{
					if (!phoneChanges.ContainsKey(message.To.Value))
						phoneChanges.Add(message.To.Value, new Dictionary<int, MessageChange>());

					var toChanges = phoneChanges[message.To.Value];
					if (!toChanges.ContainsKey(message.Id))
						toChanges.Add(message.Id, messageChange);
				}
			}

			_maxMessageId = updatedMessages.Max(m => m.Id);

			if (!phoneChanges.Any())
				return connectionsChanges;

			foreach (var msisdn in phoneChanges.Keys)
			{
				var connectionInfos = MessagesHubSecurity.Instance.GetConnectionsByMsisdn(msisdn);
				var messageChanges = phoneChanges[msisdn];
				foreach (var connectionInfo in connectionInfos)
				{
					if (!connectionsChanges.ContainsKey(connectionInfo.ConnectionId))
						connectionsChanges.Add(connectionInfo.ConnectionId, new Dictionary<string, Dictionary<ChangeType, Dictionary<int, MessagesChangeLocale>>>());

					var connectionChanges = connectionsChanges[connectionInfo.ConnectionId];
					if (!connectionChanges.ContainsKey(msisdn))
						connectionChanges.Add(msisdn, new Dictionary<ChangeType, Dictionary<int, MessagesChangeLocale>>());

					var msisdnChanges = connectionChanges[msisdn];
					if (!msisdnChanges.ContainsKey(ChangeType.Insert))
						msisdnChanges.Add(ChangeType.Insert, new Dictionary<int, MessagesChangeLocale>());
					if (!msisdnChanges.ContainsKey(ChangeType.Update))
						msisdnChanges.Add(ChangeType.Update, new Dictionary<int, MessagesChangeLocale>());
					foreach (var messageId in messageChanges.Keys)
					{
						var messageChange = messageChanges[messageId];
						var message = messageChange.Message;
						var messageLocaleChange = new MessagesChangeLocale(messageChange, connectionInfo.CultureInfo)
						{
							Type = message.To != null && message.To.Value == msisdn
								? MessageType.Outgoing
								: MessageType.Incoming
						};
						msisdnChanges[messageChange.ChangeType].Add(messageId, messageLocaleChange);
					}
				}
			}

#if DEBUG
			Trace.TraceInformation(
				"Push to web GetMessageUpdates: {0}", connectionsChanges.ToJson());
#endif

			return connectionsChanges;
		}
		protected override bool Do()
		{
			Lock();
			try
			{
				var contactIds = MessagesHubSecurity.Instance.GetContactIds();
				if (!contactIds.Any())
					return false;

				var time = DateTime.UtcNow;
				var connectionsChanges = GetMessageUpdates(contactIds, LastUpdateTime, time);
				LastUpdateTime = time;

				var hub = GlobalHost.ConnectionManager.GetHubContext<MessagesHub>();
				foreach (var connectionId in connectionsChanges.Keys)
				{
					var changes = connectionsChanges[connectionId];
					hub.Clients.Client(connectionId).MessageUpdates(changes);
				}
				return connectionsChanges.Count != 0;
			}
			finally
			{
				Unlock();
			}
		}
	}
}