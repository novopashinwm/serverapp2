﻿using System.Globalization;

namespace RU.NVG.UI.WebMapG.Push
{
	public class TokenRegistrationInfo
	{
		/// <summary> Идентификатор сессии Asp.Net </summary>
		public string      SessionId;
		/// <summary> Идентификатор оператора </summary>
		public int         OperatorId;
		/// <summary> Локализация оператора </summary>
		public CultureInfo CultureInfo;
	}
}