﻿namespace RU.NVG.UI.WebMapG.Push
{
	public class MessagesContextInfo
	{
		/// <summary> Идентификатор контакта для которого происходит мониторинг </summary>
		public int?   AsidId;
		/// <summary> Идентификатор контакта для которого происходит мониторинг </summary>
		public int    MsisdnId;
		/// <summary> Номер телефона </summary>
		public string Msisdn;
		/// <summary> Идентификатор последнего сообщения из отправленного </summary>
		public int?   FirstMessageId;
	}
}