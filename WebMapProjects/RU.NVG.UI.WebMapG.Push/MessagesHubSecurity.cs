﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using FORIS.TSS.ServerApplication;
using FORIS.TSS.TerminalService.Interfaces;
using Microsoft.AspNet.SignalR;

namespace RU.NVG.UI.WebMapG.Push
{
	public sealed class MessagesHubSecurity
	{
		/// <summary> Словарь предварительно выданных токенов (одноразовых), удаляются после подключения Hub </summary>
		private readonly ConcurrentDictionary<string, TokenRegistrationInfo>                         _tokenRegistration = new ConcurrentDictionary<string, TokenRegistrationInfo>();
		/// <summary> Словарь соединений операторов, где ключ идентификатор оператора </summary>
		private readonly ConcurrentDictionary<int, ConcurrentDictionary<string, MessagesContextKey>> _operatorContextKeys = new ConcurrentDictionary<int, ConcurrentDictionary<string, MessagesContextKey>>();
		/// <summary> Словарь соединений, где ключ идентификатор соединения Hub </summary>
		private readonly ConcurrentDictionary<string, MessagesContextKey>                            _connectionContextKeys = new ConcurrentDictionary<string, MessagesContextKey>();
		/// <summary> Словарь контекстов Hub, где ключ составной </summary>
		private readonly ConcurrentDictionary<MessagesContextKey, List<MessagesContextInfo>>         _contextInfos = new ConcurrentDictionary<MessagesContextKey, List<MessagesContextInfo>>();

		private static readonly object              _securityInstanceLock = new object();
		private static volatile MessagesHubSecurity _securityInstance;

		public static MessagesHubSecurity Instance
		{
			get
			{
				// Первая проверка
				if (null == _securityInstance)
					// Ожидание блокировки и блокировка
					lock (_securityInstanceLock)
						// Вторая проверка после блокировки (во время ожидания блокировки, могли уже присвоить)
						if (null == _securityInstance)
							_securityInstance = new MessagesHubSecurity();
				return _securityInstance;
			}
		}
		/// <summary> Регистрируем token, сохраняем сам token и информацию по нему до установления соединения </summary>
		/// <param name="token"></param>
		/// <param name="tokenRegistrationInfo"></param>
		/// <returns></returns>
		public bool Register(string token, TokenRegistrationInfo tokenRegistrationInfo)
		{
			return !_tokenRegistration.TryGetValue(token, out var existingTokenRegistrationInfo) &&
				_tokenRegistration.TryAdd(token, tokenRegistrationInfo);
		}
		/// <summary> Связать token с установленным соединением (токен удаляем после регистрации) </summary>
		/// <param name="userToken"> Токен пользователя </param>
		/// <param name="connectionId"> Идентификатор соединения Hub </param>
		/// <returns></returns>
		public bool Bind(string userToken, string connectionId)
		{
			// Проверяем наличие token
			if (string.IsNullOrWhiteSpace(userToken))
				return false;
			// Удаляем из ранее зарегистрированных/сохраненных/полученных клиентами tokens
			if (!_tokenRegistration.TryRemove(userToken, out var tokenRegistration))
				return false;
			// Производи очистку коллекций по идентификатору соединения (скорее всего можно будет избавиться)
			ClearConnectionContext(connectionId);
			// Создаем контекст соединения на базе зарегистрированного token
			var connectTime = DateTime.UtcNow;
			var contextKey = new MessagesContextKey
			{
				ConnectionId   = connectionId,
				SessionId      = tokenRegistration.SessionId,
				OperatorId     = tokenRegistration.OperatorId,
				CultureInfo    = tokenRegistration.CultureInfo,
				ConnectTime    = connectTime,
				ConnectLogTime = connectTime.ToLogTime(),
			};
			_connectionContextKeys.TryAdd(contextKey.ConnectionId, contextKey);
			// Создаем коллекцию/словарь/группу контекстов соединений для оператора
			if (!_operatorContextKeys.ContainsKey(contextKey.OperatorId))
				_operatorContextKeys.TryAdd(contextKey.OperatorId, new ConcurrentDictionary<string, MessagesContextKey>());
			_operatorContextKeys[contextKey.OperatorId].AddOrUpdate(connectionId, s => contextKey, (s, key) => contextKey);
			// Добавляем в словарь с ключом контекста соединения, список размещения информации о сообщениях для данного контекста
			return _contextInfos.TryAdd(contextKey, new List<MessagesContextInfo>());
		}
		public bool Unbind(string connectionId)
		{
			return _connectionContextKeys.TryGetValue(connectionId, out var contextKey)
				? ClearConnectionContext(connectionId)
				: false;
		}
		public MessagesContextKey GetMessagesContextKeyByConnectionId(string connectionId)
		{
			return _connectionContextKeys.TryGetValue(connectionId, out var contextKey)
				? contextKey
				: default(MessagesContextKey);
		}
		public MessagesContextKey[] Impersonate(int operatorId)
		{
			return _operatorContextKeys.TryGetValue(operatorId, out var connections)
				? connections.Values.ToArray()
				: default(MessagesContextKey[]);
		}
		/// <summary> Связать контекст соединения и номер телефона </summary>
		/// <remarks> Этому контексту соединения будут отправляться уведомления об обмене СМС с этим номером </remarks>
		/// <param name="contextKey"></param>
		/// <param name="msisdn"></param>
		/// <returns></returns>
		public MessagesContextInfo Subscribe(MessagesContextKey contextKey, string msisdn)
		{
			if (!Server.Instance().IsSuperAdministrator(contextKey.OperatorId))
				throw new SecurityException("not allowed");

			if (!_contextInfos.TryGetValue(contextKey, out var contextInfos))
				throw new ObjectNotFoundException("not context found for contextKey");

			var contextInfo = CreateContextInfo(msisdn);

			contextInfos.Add(contextInfo);

			return contextInfo;
		}
		/// <summary> Проверка наличия связи контекста соединения с номером телефона </summary>
		/// <param name="contextKey"></param>
		/// <param name="msisdn"></param>
		/// <returns></returns>
		public bool ExistSubscribe(MessagesContextKey contextKey, string msisdn)
		{
			if (!_contextInfos.TryGetValue(contextKey, out var contextInfos))
				throw new ObjectNotFoundException("not context found for contextKey");
			return contextInfos.Any(ci => ci.Msisdn == msisdn);
		}
		/// <summary> Отвязать от контекста соединения номер телефона </summary>
		/// <param name="contextKey"></param>
		/// <param name="msisdn"></param>
		public void UnSubscribe(MessagesContextKey contextKey, string msisdn)
		{
			if (!_contextInfos.TryGetValue(contextKey, out var contextInfos))
				throw new ObjectNotFoundException("not context found for contextKey");
			contextInfos.RemoveAll(ci => ci.Msisdn == msisdn);
		}
		/// <summary> Получить список идентификаторов контактов (причем любых типов и AsidId и MsisdnId) </summary>
		/// <remarks> Используется только для определения, хоть какие-то контакты есть? Сомнительное использование </remarks>
		/// <returns></returns>
		public int[] GetContactIds()
		{
			return _contextInfos
				.Select(c => c.Value)
				.SelectMany(infos => infos
					.SelectMany(ci => new[] { ci.AsidId, ci.MsisdnId }
						.Where(id => id.HasValue)
						.Cast<int>()))
				.ToArray();
		}
		/// <summary> Метод отправки сообщений пользователям, которые установили соединения и содержат контекст </summary>
		/// <param name="operatorIds"></param>
		/// <param name="action"></param>
		private void NotifyUsers(int[] operatorIds, Action<dynamic, MessagesContextKey> action)
		{
			// Получить все контексты указанных операторов
			var contextKeys = operatorIds
				.Where(operatorId => _operatorContextKeys.ContainsKey(operatorId))
				.SelectMany(operatorId => _operatorContextKeys[operatorId].Values)
				.ToArray();
			// Выполняем отправку всем контекстам соединений
			foreach (var contextKey in contextKeys)
			{
				// Получить MessagesHub (типизированный Hub)
				var hub = GlobalHost.ConnectionManager.GetHubContext<MessagesHub>();
				try
				{
					// Выполнить действие, в которое передается объект соответствующий соединению и сам контекст соединения
					action(hub.Clients.Client(contextKey.ConnectionId), contextKey);
				}
				catch (Exception ex)
				{
					$"Call Action(ConnectionId={contextKey.ConnectionId}, OperatorId={contextKey.OperatorId})"
						.WithException(ex)
						.CallTraceError();
				}
			}
		}
		/// <summary> Получить контексты соединения по номеру телефона </summary>
		/// <param name="msisnd"></param>
		/// <returns></returns>
		public MessagesContextKey[] GetConnectionsByMsisdn(string msisnd)
		{
			return _contextInfos
				.Where(ci => ci.Value.Any(c => c.Msisdn == msisnd))
				.Select(ci => ci.Key)
				.ToArray();
		}
		/// <summary> Очистка коллекций соединений, контекстов и прочего </summary>
		/// <param name="connectionId"></param>
		/// <returns></returns>
		private bool ClearConnectionContext(string connectionId)
		{
			if (!_connectionContextKeys.TryRemove(connectionId, out var contextKey))
				return false;

			if (_operatorContextKeys.TryGetValue(contextKey.OperatorId, out var operatorContextKeys))
			{
				operatorContextKeys.TryRemove(connectionId, out var operatorContextKey);
				if (operatorContextKeys.IsEmpty)
					_operatorContextKeys.TryRemove(contextKey.OperatorId, out operatorContextKeys);
			}

			return _contextInfos.TryRemove(contextKey, out var contextInfos);
		}
		/// <summary> Создать класс информации по номеру телефона, необходимой для отправки уведомлений </summary>
		/// <param name="msisdn"></param>
		/// <returns></returns>
		private MessagesContextInfo CreateContextInfo(string msisdn)
		{
			using (var entities = new Entities())
			{
				var msisdnContact = entities.GetContact(ContactType.Phone, msisdn);
				var contextInfo   = new MessagesContextInfo
				{
					Msisdn   = msisdn,
					MsisdnId = msisdnContact.ID,
				};
				return contextInfo;
			}
		}
		/// <summary> Отправка изменений клиентам по данным терминала, по событию прихода данных </summary>
		/// <param name="mobilUnit"></param>
		public void UpdateVehicle(IMobilUnit mobilUnit)
		{
			try
			{
				// 0. Нет параметра нет действий.
				if (null == mobilUnit)
					return;
				// 1. Получить всех пользователей по объекту наблюдения, получить их клиенты и отправить
				var operatorIds = Server.Instance().GetVehicleOperators(mobilUnit.Unique);
				if (0 == (operatorIds?.Length ?? 0))
					return;
				// 2. Превратить IMobilUnit в DTO.Vehicle (данные полученные из терминала)
				var tmv = mobilUnit.ToDTO();

				var glc = Server.Instance().GlobalCache;
				// 3. Отправить DTO.Vehicle на клиента
				NotifyUsers(operatorIds, (client, context) =>
				{
					var dbv = glc.GetObjectFromCache<List<Vehicle>>($"Vehicles_{context.SessionId}")
						?.FirstOrDefault(v => v.id == tmv.id);
					// Если нет данных об объекте в этом контексте выходим (удаляем все попытки отправить админу лишнее)
					if (null == dbv)
						return;
					// 4. Обновляем свойства отправляемого объекта, объединяя с информацией полученной из GetVehicles
					// Тут можем заполнить любые свойства полученные в GetVehicles
					// 4.1 Обновляем адрес, т.к. координаты пришедшие из терминала могут быть другими
					if (null != context.CultureInfo &&
						tmv.lat.HasValue &&
						tmv.lng.HasValue &&
						tmv.posLogTime.HasValue &&
						tmv.posLogTime.Value > (dbv.posLogTime ?? 0) &&
						tmv.lat.Value != (dbv.lat ?? FORIS.TSS.Common.GeoHelper.InvalidLatitude) &&
						tmv.lng.Value != (dbv.lng ?? FORIS.TSS.Common.GeoHelper.InvalidLongitude))
					{
						var onlineGeocoding4All = Server.Instance().GetConstantAsBool(Constant.OnlineGeocoding4All);
						dbv.lat        = tmv.lat;
						dbv.lng        = tmv.lng;
						dbv.posLogTime = tmv.posLogTime;
						dbv.address    = tmv.address = Server.Instance().GetAddressByPoint(
							tmv.lat.Value,
							tmv.lng.Value,
							context.CultureInfo.TwoLetterISOLanguageName,
							null,
							!((dbv.IsOnlineGeocoding ?? false) || onlineGeocoding4All));
					}
					// 4.2 Не посылаем значения датчиков, если их не было в dbv
					if (0 == (dbv.Sensors?.Count ?? 0))
						tmv.Sensors = dbv.Sensors;
					// Отправка уведомлений клиентам
					client.UpdateVehicle(tmv);
				});
			}
			catch (Exception ex)
			{
				$"MobilUnit.Unique={mobilUnit.Unique}"
					.WithException(ex)
					.CallTraceError();
			}
		}
		/// <summary> Отправка клиентам сообщения тревоги поступившего от терминала </summary>
		public void AlarmVehicle(IMobilUnit mobilUnit, int[] operatorIds)
		{
			var glc = Server.Instance().GlobalCache;
			NotifyUsers(operatorIds, (client, context) =>
			{
				// Не отправляем тревоги суперадминам вообще
				if (Server.Instance().IsSuperAdministrator(context.OperatorId))
					return;
				var dbv = glc.GetObjectFromCache<List<Vehicle>>($"Vehicles_{context.SessionId}")
					?.FirstOrDefault(v => v.id == mobilUnit.Unique);
				// Если нет данных об объекте в этом контексте выходим (удаляем все попытки отправить админу лишнее)
				if (null == dbv)
					return;
				client.AlarmVehicle(mobilUnit.Unique, true);
			});
		}
	}
}