﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web.Services;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.EntityModel;

namespace FORIS.TSS.WebServices.OuterIntegration
{
	/// <summary>
	/// Web service for operating with vehicle groups (moving group from one to other)
	/// </summary>
	[WebService(
		Namespace   = XmlNS,
		Name        = "VehicleGroup",
		Description = "Web service for operating with vehicle groups (moving group from one to other)")]
	public class VehicleGroup : BaseWebService
	{
		private const string VehicleGroupType = "BrigadeType";

		[WebMethod(EnableSession = true)]
		public void MoveVehicleToGroup(string groupName, int vehicleId)
		{
			Trace.TraceInformation("VehicleGroup.MoveVehicleToGroup groupName = {0}, vehicleId = {1}",
								   groupName, vehicleId);
			CheckAuthentication();

			try
			{
				using (var entities = new Entities())
				{
					var vehicle = entities.GetVehicle(vehicleId);
					if (vehicle == null)
						throw new ArgumentOutOfRangeException("vehicleId", vehicleId, "No such vehicle");
					var @operator = entities.GetOperator(OperatorLogin);
					var vehicleGroup = entities.VEHICLEGROUP.FirstOrDefault(
						vg => vg.NAME == groupName &&
							  vg.Type == VehicleGroupType &&
							  vg.Accesses.Any(
								  access => access.operator_id == @operator.OPERATOR_ID &&
											access.right_id == (int) SystemRight.EditGroup));
					if (vehicleGroup == null)
						throw new ArgumentOutOfRangeException("groupName", groupName, "No such vehicle group that is accessible to edit by operator " + @operator.LOGIN);
					if (!vehicle.VEHICLEGROUP.IsLoaded)
						vehicle.VEHICLEGROUP.Load();
					var deletingGroups = vehicle.VEHICLEGROUP.Where(
						vg => vg.Type == VehicleGroupType
						   && vg != vehicleGroup).ToArray();
					foreach (var deletingGroup in deletingGroups)
						vehicle.VEHICLEGROUP.Remove(deletingGroup);
					if (!vehicle.VEHICLEGROUP.Contains(vehicleGroup))
						vehicle.VEHICLEGROUP.Add(vehicleGroup);
					entities.SaveChanges(@operator);
				}
			}
			catch (Exception ex)
			{
				Trace.TraceError(ex.ToString());
				throw;
			}
		}
	}
}