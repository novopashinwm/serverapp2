﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web.Services;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common;
using FORIS.TSS.EntityModel;
using FORIS.TSS.TransportDispatcher.Reports;
using FORIS.TSS.TransportDispatcher.Reports.ReportCollection;

namespace FORIS.TSS.WebServices.OuterIntegration
{
	[WebService(
		Namespace   = XmlNS,
		Name        = "Reports",
		Description = "Web service for operating with vehicles")]
	public class Reports : BaseWebService
	{
		/// <summary> Отчет по интервалам движения </summary>
		/// <param name="vehicleId"> Идентификатор объекта </param>
		/// <param name="parkingInterval"> Время парковки </param>
		/// <param name="dateFrom"> Дата начала в (UTC) </param>
		/// <param name="dateTo"> Дата конца в (UTC) </param>
		/// <param name="departmentId"> Идентификатор клиента </param>
		/// <param name="cultureName"> Название культуры (ru, en) </param>
		/// <param name="timeZoneId"> Идентификатор часового пояса </param>
		/// TimeZoneInfo.GetSystemTimeZones().Id
		/// Dateline Standard Time, UTC-11, Hawaiian Standard Time, Alaskan Standard Time, Pacific Standard Time (Mexico), Pacific Standard Time, US Mountain Standard Time, Mountain Standard Time, Mountain Standard Time (Mexico), Central Standard Time (Mexico), Canada Central Standard Time, Central America Standard Time, Central Standard Time, SA Pacific Standard Time, Eastern Standard Time, US Eastern Standard Time, Venezuela Standard Time, Paraguay Standard Time, Atlantic Standard Time, SA Western Standard Time, Central Brazilian Standard Time, Pacific SA Standard Time, Newfoundland Standard Time, E. South America Standard Time, Argentina Standard Time, Greenland Standard Time, SA Eastern Standard Time, Montevideo Standard Time, Bahia Standard Time, UTC-02, Mid-Atlantic Standard Time, Azores Standard Time, Cape Verde Standard Time, UTC, GMT Standard Time, Morocco Standard Time, Greenwich Standard Time, W. Europe Standard Time, Central Europe Standard Time, Romance Standard Time, Central European Standard Time, Namibia Standard Time, W. Central Africa Standard Time, Jordan Standard Time, GTB Standard Time, Middle East Standard Time, FLE Standard Time, E. Europe Standard Time, Syria Standard Time, Israel Standard Time, Egypt Standard Time, Kaliningrad Standard Time, Turkey Standard Time, Libya Standard Time, South Africa Standard Time, Arabic Standard Time, Russian Standard Time, Arab Standard Time, Belarus Standard Time, E. Africa Standard Time, Iran Standard Time, Arabian Standard Time, Azerbaijan Standard Time, Caucasus Standard Time, Russia Time Zone 3, Mauritius Standard Time, Georgian Standard Time, Afghanistan Standard Time, West Asia Standard Time, Ekaterinburg Standard Time, Pakistan Standard Time, India Standard Time, Sri Lanka Standard Time, Nepal Standard Time, Central Asia Standard Time, Bangladesh Standard Time, N. Central Asia Standard Time, Myanmar Standard Time, SE Asia Standard Time, North Asia Standard Time, China Standard Time, North Asia East Standard Time, Singapore Standard Time, W. Australia Standard Time, Taipei Standard Time, Ulaanbaatar Standard Time, Tokyo Standard Time, Korea Standard Time, Yakutsk Standard Time, Cen. Australia Standard Time, AUS Central Standard Time, E. Australia Standard Time, Vladivostok Standard Time, West Pacific Standard Time, AUS Eastern Standard Time, Magadan Standard Time, Tasmania Standard Time, Central Pacific Standard Time, Russia Time Zone 10, Russia Time Zone 11, New Zealand Standard Time, UTC+12, Kamchatka Standard Time, Fiji Standard Time, Tonga Standard Time, Samoa Standard Time, Line Islands Standard Time
		/// <returns></returns>
		[WebMethod(EnableSession = true, Description = "Get data for WebGisMoveHistory report", MessageName = "WebGisMoveHistory")]
		public WebGisMoveHistoryTwoColumnsDTO WebGisMoveHistory(
			int vehicleId, int parkingInterval, DateTime dateFrom, DateTime dateTo, int departmentId, string cultureName, string timeZoneId)
		{
			using (new MethodCallLogger(Session.SessionID, "Reports.WebGisMoveHistory login = {0}", OperatorLogin))
			{
				try
				{
					CheckAuthentication();

					var cultureInfo = string.IsNullOrEmpty(cultureName) ? null : CultureInfo.GetCultureInfo(cultureName);
					var timeZoneInfo = string.IsNullOrEmpty(timeZoneId)
						? null
						: TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
					var reportParams = CreateParams(departmentId, cultureInfo, timeZoneInfo, dateFrom, dateTo);
					reportParams.Add("VehicleId", vehicleId);
					reportParams.Add("DateFrom", dateFrom);
					reportParams.Add("DateTo", dateTo);

					return (WebGisMoveHistoryTwoColumnsDTO)ReportMake<WebGisMoveHistoryTwoColumns>(reportParams);
				}
				catch (Exception exception)
				{
					Trace.TraceError(exception.ToString());
					throw;
				}
			}
		}
		[WebMethod(EnableSession = true, Description = "Get data for WebGisMoveHistory report", MessageName = "WebGisMoveHistoryDepartment")]
		public WebGisMoveHistoryTwoColumnsDTO WebGisMoveHistory(
			int vehicleId, int parkingInterval, DateTime dateFrom, DateTime dateTo, int departmentId)
		{
			return WebGisMoveHistory(vehicleId, parkingInterval, dateFrom, dateTo, departmentId, string.Empty,
									 string.Empty);
		}
		[WebMethod(EnableSession = true, Description = "Get data for WebGisMoveHistory report", MessageName = "WebGisMoveHistoryDefault")]
		public WebGisMoveHistoryTwoColumnsDTO WebGisMoveHistory(
			int vehicleId, int parkingInterval, DateTime dateFrom, DateTime dateTo)
		{
			return WebGisMoveHistory(vehicleId, parkingInterval, dateFrom, dateTo, default(int));
		}
		/// <summary> Отчет по топливу </summary>
		/// <param name="vehicleId"> Идентификатор объекта </param>
		/// <param name="sensorTypeValue"> Датчик: 1 - датчик уровня топлива, 0 - CAN, 0;1 - датчик уровня топлива и CAN </param>
		/// <param name="fuelSpendStandardType"> Тип норматива расхода топлива EngineHours - моточасы, Run - пробег </param>
		/// <param name="dateFrom"> Дата начала в (UTC) </param>
		/// <param name="dateTo"> Дата конца в (UTC) </param>
		/// <param name="departmentId"> Идентификатор клиента </param>
		/// <param name="cultureName"> Название культуры (ru, en) </param>
		/// <param name="timeZoneId"> Идентификатор часового пояса </param>
		/// TimeZoneInfo.GetSystemTimeZones().Id
		/// Dateline Standard Time, UTC-11, Hawaiian Standard Time, Alaskan Standard Time, Pacific Standard Time (Mexico), Pacific Standard Time, US Mountain Standard Time, Mountain Standard Time, Mountain Standard Time (Mexico), Central Standard Time (Mexico), Canada Central Standard Time, Central America Standard Time, Central Standard Time, SA Pacific Standard Time, Eastern Standard Time, US Eastern Standard Time, Venezuela Standard Time, Paraguay Standard Time, Atlantic Standard Time, SA Western Standard Time, Central Brazilian Standard Time, Pacific SA Standard Time, Newfoundland Standard Time, E. South America Standard Time, Argentina Standard Time, Greenland Standard Time, SA Eastern Standard Time, Montevideo Standard Time, Bahia Standard Time, UTC-02, Mid-Atlantic Standard Time, Azores Standard Time, Cape Verde Standard Time, UTC, GMT Standard Time, Morocco Standard Time, Greenwich Standard Time, W. Europe Standard Time, Central Europe Standard Time, Romance Standard Time, Central European Standard Time, Namibia Standard Time, W. Central Africa Standard Time, Jordan Standard Time, GTB Standard Time, Middle East Standard Time, FLE Standard Time, E. Europe Standard Time, Syria Standard Time, Israel Standard Time, Egypt Standard Time, Kaliningrad Standard Time, Turkey Standard Time, Libya Standard Time, South Africa Standard Time, Arabic Standard Time, Russian Standard Time, Arab Standard Time, Belarus Standard Time, E. Africa Standard Time, Iran Standard Time, Arabian Standard Time, Azerbaijan Standard Time, Caucasus Standard Time, Russia Time Zone 3, Mauritius Standard Time, Georgian Standard Time, Afghanistan Standard Time, West Asia Standard Time, Ekaterinburg Standard Time, Pakistan Standard Time, India Standard Time, Sri Lanka Standard Time, Nepal Standard Time, Central Asia Standard Time, Bangladesh Standard Time, N. Central Asia Standard Time, Myanmar Standard Time, SE Asia Standard Time, North Asia Standard Time, China Standard Time, North Asia East Standard Time, Singapore Standard Time, W. Australia Standard Time, Taipei Standard Time, Ulaanbaatar Standard Time, Tokyo Standard Time, Korea Standard Time, Yakutsk Standard Time, Cen. Australia Standard Time, AUS Central Standard Time, E. Australia Standard Time, Vladivostok Standard Time, West Pacific Standard Time, AUS Eastern Standard Time, Magadan Standard Time, Tasmania Standard Time, Central Pacific Standard Time, Russia Time Zone 10, Russia Time Zone 11, New Zealand Standard Time, UTC+12, Kamchatka Standard Time, Fiji Standard Time, Tonga Standard Time, Samoa Standard Time, Line Islands Standard Time
		/// <returns></returns>
		[WebMethod(EnableSession = true, Description = "Get data for FuelCost report", MessageName = "FuelCost")]
		public FuelCostTssReportDTO FuelCost(
			int vehicleId,
			string sensorTypeValue,
			string fuelSpendStandardType,
			DateTime dateFrom,
			DateTime dateTo,
			int departmentId,
			string cultureName,
			string timeZoneId)
		{
			using (new MethodCallLogger(Session.SessionID, "Reports.FuelCost login = {0}", OperatorLogin))
			{
				try
				{
					CheckAuthentication();

					var cultureInfo  = string.IsNullOrEmpty(cultureName) ? null : CultureInfo.GetCultureInfo(cultureName);
					var timeZoneInfo = string.IsNullOrEmpty(timeZoneId)  ? null : TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
					var reportParams = CreateParams(departmentId, cultureInfo, timeZoneInfo, dateFrom, dateTo);
					reportParams.Add("VehicleID", vehicleId);
					reportParams.Add("SensorTypeValue", sensorTypeValue);
					var fuelSpendStandartType = !string.IsNullOrEmpty(fuelSpendStandardType)
						? Enum.Parse(typeof (FuelSpendStandardType), fuelSpendStandardType)
						: FuelSpendStandardType.Run;
					reportParams.Add("FuelSpendStandardType", fuelSpendStandartType);
					return (FuelCostTssReportDTO)ReportMake<FuelCostTSSReport>(reportParams);
				}
				catch (Exception exception)
				{
					Trace.TraceError(exception.ToString());
					throw;
				}
			}
		}
		[WebMethod(EnableSession = true, Description = "Get data for FuelCost report", MessageName = "FuelCostDepartment")]
		public FuelCostTssReportDTO FuelCost(
			int vehicleId,
			string sensorTypeValue,
			string fuelSpendStandardType,
			DateTime dateFrom,
			DateTime dateTo,
			int departmentId)
		{
			return FuelCost(
				vehicleId,
				sensorTypeValue,
				fuelSpendStandardType,
				dateFrom,
				dateTo,
				departmentId,
				string.Empty,
				string.Empty);
		}
		[WebMethod(EnableSession = true, Description = "Get data for FuelCost report", MessageName = "FuelCostDefault")]
		public FuelCostTssReportDTO FuelCost(
			int vehicleId,
			string sensorTypeValue,
			string fuelSpendStandardType,
			DateTime dateFrom,
			DateTime dateTo)
		{
			return FuelCost(vehicleId, sensorTypeValue, fuelSpendStandardType, dateFrom, dateTo, default(int));
		}
		protected object ReportMake<T>(PARAMS reportParams) where T : TssReportBase
		{
			var reportType    = typeof (T);
			var guidAttribute = (GuidAttribute)Attribute.GetCustomAttribute(reportType, typeof(GuidAttribute));
			var reportGuid    = new Guid(guidAttribute.Value);
			var reportFactory = new ReportsFactoryWeb(ServerApplication.Server.Instance(), Repository.Instance, OperatorId);
			var reportDto     = reportFactory.CreateReportAsDto(reportGuid, reportParams);
			return reportDto;
		}
		protected PARAMS CreateParams(int departmentId, CultureInfo cultureInfo, TimeZoneInfo timeZoneInfo, DateTime dateFrom, DateTime dateTo)
		{
			var reportParams = new PARAMS(true)
			{
				{ "OperatorId",       OperatorId                             },
				{ "DateTimeInterval", new DateTimeInterval(dateFrom, dateTo) },
			};

			using (var entities = new Entities())
			{
				var currency = entities.GetAvailableDepartmentsByOperatorIdQuery(OperatorId)
					.Select(d => d.Country.Currency)
					.FirstOrDefault() ?? "RUR";
				var locale = entities.OPERATOR
					.Where(o => o.OPERATOR_ID == OperatorId)
					.Select(o => new
					{
						CultureCode = o.Culture.Code,
						TimeZoneId  = o.TimeZone.Code
					})
					.FirstOrDefault() ?? new
					{
						CultureCode = CultureInfo.CurrentCulture.Name,
						TimeZoneId  = TimeZoneInfo.Local.Id
					};

				reportParams.Add("Currency", currency);
				reportParams.Add("Culture", cultureInfo ?? CultureInfo.GetCultureInfo(locale.CultureCode));
				reportParams.Add("TimeZoneInfo", timeZoneInfo ?? TimeZoneInfo.FindSystemTimeZoneById(locale.TimeZoneId));

				if (departmentId > 0)
				{
					var department = entities.DEPARTMENT.FirstOrDefault(d => d.DEPARTMENT_ID == departmentId);
					if (department != null)
					{
						reportParams.Add("DepartmentId", department.DEPARTMENT_ID);
						reportParams.Add("DepartmentType", (DepartmentType)(department.Type ?? 1));
					}
				}
			}

			return reportParams;
		}
	}
}