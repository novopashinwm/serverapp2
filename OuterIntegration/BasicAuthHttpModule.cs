﻿using System;
using System.Diagnostics;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using FORIS.TSS.EntityModel;

namespace FORIS.TSS.WebServices.OuterIntegration
{
	public class BasicAuthHttpModule : IHttpModule
	{
		private const string Realm = "My Realm";

		public void Init(HttpApplication context)
		{
			// Register event handlers
			context.AuthenticateRequest += OnApplicationAuthenticateRequest;
			context.EndRequest += OnApplicationEndRequest;
		}

		private static void SetPrincipal(IPrincipal principal)
		{
			Thread.CurrentPrincipal = principal;
			if (HttpContext.Current != null)
			{
				HttpContext.Current.User = principal;
			}
		}

		private static bool CheckPassword(string username, string password)
		{
			using (var entities = new Entities())
			{
				var o = entities.GetOperatorOrDefault(username);
				return o != null && o.PASSWORD == password;
			}
		}

		private static bool AuthenticateUser(string credentials)
		{
			bool validated;
			try
			{
				var encoding = Encoding.GetEncoding("iso-8859-1");
				credentials = encoding.GetString(Convert.FromBase64String(credentials));

				int separator = credentials.IndexOf(':');
				string name = credentials.Substring(0, separator);
				string password = credentials.Substring(separator + 1);

				validated = CheckPassword(name, password);
				if (validated)
				{
					var identity = new GenericIdentity(name);
					SetPrincipal(new GenericPrincipal(identity, null));
				}
			}
			catch (FormatException)
			{
				// Credentials were not formatted correctly.
				validated = false;

			}
			return validated;
		}

		private static void OnApplicationAuthenticateRequest(object sender, EventArgs e)
		{
			var request = HttpContext.Current.Request;
			var authHeader = request.Headers["Authorization"];
			if (authHeader != null)
			{
				Trace.TraceInformation(authHeader);
				
				var authHeaderVal = authHeader.Substring("Basic".Length).Trim();

				//// RFC 2617 sec 1.2, "scheme" name is case-insensitive

				AuthenticateUser(authHeaderVal);
			}
		}

		// If the request was unauthorized, add the WWW-Authenticate header 
		// to the response.
		private static void OnApplicationEndRequest(object sender, EventArgs e)
		{
			var response = HttpContext.Current.Response;
			if (response.StatusCode == 401)
			{
				response.Headers.Add("WWW-Authenticate",
					string.Format("Basic realm=\"{0}\"", Realm));
			}
		}

		public void Dispose()
		{
		}
	}
}