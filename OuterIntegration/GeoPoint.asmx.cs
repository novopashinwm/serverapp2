﻿using System;
using System.Linq;
using System.Web.Services;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using FORIS.TSS.WorkplaceShadow.Geo;
using Interfaces.Geo;

namespace FORIS.TSS.WebServices.OuterIntegration
{
	[WebService(
		Namespace   = XmlNS,
		Name        = "GeoPoint",
		Description = "Web service for managing user geo points")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
	// [System.Web.Script.Services.ScriptService]
	public class GeoPoint : BaseWebService
	{
		[WebMethod(EnableSession = true, Description = "Adds or updates user web point using the name as primary key")]
		public AddOrUpdateResult AddOrUpdate(string name, string description, string type, string address)
		{
			CheckAuthentication();

			var lat = default(double?);
			var lng = default(double?);
			var getLatLngByAddressResult = GetLatLngByAddressResult.UnknownError;
			try
			{
				var geoClient = GeoClient.Default;
				if (geoClient != null)
					getLatLngByAddressResult = geoClient.GetLatLngByAddress(address: address, lat: out lat, lng: out lng);
				else
					getLatLngByAddressResult = GetLatLngByAddressResult.FailedAddressNotFound;
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();
				return AddOrUpdateResult.FailedGeocoderTooBusy;
			}

			if (getLatLngByAddressResult == GetLatLngByAddressResult.FailedGeocoderTooBusy)
				return AddOrUpdateResult.FailedGeocoderTooBusy;

			if (getLatLngByAddressResult == GetLatLngByAddressResult.FailedAddressNotFound || lat == null || lng == null)
				return AddOrUpdateResult.FailedNoAddress;

			using (var entities = new Entities())
			{
				var webPointQuery = entities.WEB_POINT
					.Where(wp =>
						wp.OPERATOR.LOGIN == OperatorLogin &&
						wp.NAME           == name);

				var ormWebPoint = webPointQuery.FirstOrDefault();
				var ormOperator = entities.GetOperator(OperatorLogin);

				var pointIsNew = ormWebPoint == null;
				if (pointIsNew)
				{
					var vertex = new MAP_VERTEX
					{
						ENABLE  = true,
						VISIBLE = true,
					};

					ormWebPoint = new WEB_POINT
					{
						NAME       = name,
						OPERATOR   = ormOperator,
						MAP_VERTEX = vertex,
					};
				}

				ormWebPoint.DESCRIPTION = description;
				ormWebPoint.WEB_POINT_TYPE = string.IsNullOrWhiteSpace(type)
					? null
					: entities.GetWebPointType(type);
				ormWebPoint.MAP_VERTEX.X = (double) lng.Value;
				ormWebPoint.MAP_VERTEX.Y = (double) lat.Value;

				entities.SaveChangesByOperator(ormOperator.OPERATOR_ID);

				return pointIsNew
					? AddOrUpdateResult.SuccessfullyAdded
					: AddOrUpdateResult.SuccessfullyUpdated;
			}
		}

		[Serializable]
		public enum AddOrUpdateResult
		{
			SuccessfullyAdded,
			SuccessfullyUpdated,
			FailedNoAddress,
			FailedGeocoderTooBusy
		}
	}
}