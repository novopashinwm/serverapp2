﻿using System.Web.Services;
using FORIS.TSS.EntityModel;

namespace FORIS.TSS.WebServices.OuterIntegration
{
	[WebService(
		Namespace   = XmlNS,
		Name        = "Soap",
		Description = "")]
	public class Soap : BaseWebService
	{
		/// <summary> Login into the web service </summary>
		/// <param name="login">The User Name to login in as</param>
		/// <param name="password">User's password</param>
		/// <returns>True on successful login.</returns>
		[WebMethod(EnableSession = true,
			Description   = @"Checks whether login and password matches and when succeeded creates also session (returns session id as a cookie). For subsequent calls of other web methods the session cookie should be used.")]
		public bool Login(string login, string password)
		{
			using (new MethodCallLogger(Session.SessionID, "Login with login = {0}", login))
			{
				using (var entities = new Entities())
				{
					var o = entities.GetOperatorOrDefault(login);
					if (o == null)
						return false;
					if (!o.CheckPassword(password))
						return false;
				}
				//NOTE: There are better ways of doing authentication. This is just illustrates Session usage.
				SetSessionUser(login);
				return true;
			}
		}
		/// <summary> Logs out of the Session </summary>
		[WebMethod(EnableSession = true,
			Description   = @"Logs out of the Session.")]
		public void Logout()
		{
			Context.Session.Abandon();
		}
	}
}