﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Security;
using System.Threading;
using System.Web.Services;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.EntityModel;
using FORIS.TSS.WorkplaceShadow.Geo;

namespace FORIS.TSS.WebServices.OuterIntegration
{
	[WebService(
		Namespace   = XmlNS,
		Name        = "EmergencyCall",
		Description = "Web service for operating emergency call objects add/update/delete)")]
	public class EmergencyCall : BaseWebService
	{
		/// <summary>Создает или изменяет вызов</summary>
		[WebMethod(EnableSession = true)]
		public void Set(
			string callNumber,
			string description,
			decimal? lat, decimal? lng,
			string address)
		{
			Trace.TraceInformation(
				"EmergencyCall.Set callNumber = {0}, description = {1}, lat = {2}, lng = {3}, address = {4}",
				callNumber, description, lat, lng, address);

			try
			{
				CheckAuthentication();
				using (var entities = new Entities())
				{
					var ormOperator = entities.GetOperator(OperatorLogin);

					if (!entities.IsAllowed(ormOperator, SystemRight.EditPublicPoints))
						throw new SecurityException("EditPublicPoints is not allowed for operator " + ormOperator.LOGIN);

					var pointByNumber = QueryEmergencyCalls(entities, callNumber).ToList();
					if (!pointByNumber.Any())
					{
						var newPoint = new WEB_POINT
						{
							MAP_VERTEX     = new MAP_VERTEX(),
							WEB_POINT_TYPE = entities.GetWebPointType(WebPointType.EmergencyCall)
						};
						entities.MAP_VERTEX.AddObject(newPoint.MAP_VERTEX);
						newPoint.OPERATOR = null; //де персонифицированная точка интереса
						entities.WEB_POINT.AddObject(newPoint);
						pointByNumber.Add(newPoint);
					}

					foreach (WEB_POINT point in pointByNumber)
					{
						point.NAME        = callNumber;
						point.DESCRIPTION = description;

						// Ищем координаты по адресу, если их не передали
						var dblLat = (double?)lat;
						var dblLng = (double?)lng;
						if ((dblLat == null || dblLng == null) && !string.IsNullOrWhiteSpace(address))
							GeoClient.Default?.GetLatLngByAddress(address, lat: out dblLat, lng: out dblLat);
						if (dblLat != null && dblLng != null)
						{
							point.MAP_VERTEX.X = dblLng.Value;
							point.MAP_VERTEX.Y = dblLat.Value;
						}
						else
						{
							point.MAP_VERTEX.X = Common.GeoHelper.InvalidLongitude;
							point.MAP_VERTEX.Y = Common.GeoHelper.InvalidLatitude;
						}
					}

					entities.SaveChanges(ormOperator);
				}
			}
			catch (ThreadAbortException)
			{
				throw;
			}
			catch (Exception ex)
			{
				Trace.TraceError(ex.ToString());
				throw;
			}
		}
		/// <summary> Удаляет вызов </summary>
		[WebMethod(EnableSession = true)]
		public void Remove(string callNumber)
		{
			Trace.TraceInformation("EmergencyCall.Remove callNumber = {0}", callNumber);

			try
			{
				CheckAuthentication();
				using (var entities = new Entities())
				{
					var ormOperator = entities.GetOperator(OperatorLogin);
					if (!entities.IsAllowed(ormOperator, SystemRight.EditPublicPoints))
						throw new SecurityException("EditPublicPoints is not allowed for operator " + ormOperator.LOGIN);
					var points = QueryEmergencyCalls(entities, callNumber).ToArray();
					foreach (var point in points)
						entities.DeleteWebPoint(point);
					if (points.Any())
					{
						entities.SaveChanges(ormOperator);
					}
				}
			}
			catch (ThreadAbortException)
			{
				throw;
			}
			catch (Exception ex)
			{
				Trace.TraceError(ex.ToString());
				throw;
			}
		}
		private IQueryable<WEB_POINT> QueryEmergencyCalls(Entities entities, string callNumber)
		{
			return entities.WEB_POINT
				.Include("MAP_VERTEX")
				.Where(p =>
					p.NAME                   == callNumber                      &&
					p.WEB_POINT_TYPE.TYPE_ID == (int)WebPointType.EmergencyCall &&
					p.OPERATOR               == null);
		}
	}
}