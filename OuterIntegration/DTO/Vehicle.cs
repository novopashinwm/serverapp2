﻿using System;
using System.Xml.Serialization;

namespace FORIS.TSS.WebServices.OuterIntegration.DTO
{
	[Serializable]
	public class Vehicle
	{
		[XmlAttribute] public int    VehicleID;
		[XmlAttribute] public string Name;
		[XmlAttribute] public string Brand;
		[XmlAttribute] public string PublicNumber;
		[XmlAttribute] public string DeviceID;
	}
}