﻿using System;
using System.Linq;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Web;
using FORIS.TSS.Helpers;
using SysDiags = System.Diagnostics;

namespace FORIS.TSS.WebServices.OuterIntegration
{
	public class Global : HttpApplication
	{
		protected void Application_Start(object sender, EventArgs e)
		{
			if (0 == ChannelServices.RegisteredChannels.Count())
			// Чтение конфигурации Remoting, для доступа к клиентам (например: GeoClient и TerminalClient)
				RemotingConfiguration.Configure(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile, false);
		}
		protected void Application_End(object sender, EventArgs e)
		{
		}
		protected void Application_Error(object sender, EventArgs e)
		{
			SysDiags::Trace.TraceError("{0}: Error {1}", Context.Request.RawUrl, Server.GetLastError());
		}
		protected void Session_Start(object sender, EventArgs e)
		{
		}
		protected void Session_End(object sender, EventArgs e)
		{
		}
	}
}