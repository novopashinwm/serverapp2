﻿using System.Diagnostics;
using System.Linq;
using System.Web.Services;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.BusinessLogic.Helpers;
using FORIS.TSS.EntityModel;
using FORIS.TSS.ServerApplication.Billing;

namespace FORIS.TSS.WebServices.OuterIntegration
{
	[WebService(
		Namespace   = XmlNS,
		Name        = "Message",
		Description = "Web service for operating with messages")]
	public class Message : BaseWebService
	{
		public class SendSmsArgs
		{
			public string Msisdn;
			public string Source;
			public string Body;
		}

		public class SendMessageResult
		{
			public MessageProcessingResult Result;
			public int[] MessageIds;
		}

		[WebMethod(EnableSession = true, Description = "Send sms to specified phone")]
		public SendMessageResult SendSms(string msisdn, string source, string body)
		{
			using (
				new MethodCallLogger(
					Session.SessionID,
					"SendSms login = {0}, msisdn = {1}, source = {2}, body = {3}",
					OperatorLogin, msisdn, source, body))
			{
				CheckAuthentication();

				//if (!ContactHelper.IsRussianPhone(msisdn))
				//	return new SendMessageResult { Result = MessageProcessingResult.InvalidDestinationAddress };

				using (var entities = new Entities())
				{
					if (!entities.GetConstantAsIntHashSet(Constant.UsersAllowedToSendAnySms.ToString())
						.Contains(OperatorId))
					{
						Forbidden();
						return null;
					}

					var billingServer = new BillingServer(entities);
					var billingResult = billingServer.AllowRenderService(OperatorId, PaidService.SendNotificationSms);
					if (billingResult != BillingResult.Success)
					{
						Trace.TraceInformation("Billing result: {0}", billingResult);
						return new SendMessageResult {Result = MessageProcessingResult.UnsuccessfulBillingResult};
					}

					var targetContact = entities.GetContact(ContactType.Phone, msisdn);

					Contact sourceContact = null;
					if (!string.IsNullOrWhiteSpace(source))
					{
						if (ContactHelper.IsValidPhone(source))
							sourceContact = entities.GetContact(ContactType.Phone, source);
						// ContactHelper.IsMtsPhone(msisdn) более нет, сделано для возможности сборки
						//else if (ContactHelper.IsMtsPhone(msisdn))
						//	sourceContact = entities.GetContact(ContactType.Text, source);
					}

					var messages = entities.CreateOutgoingSms(targetContact, body);

					foreach (var message in messages)
					{
						message.Owner_Operator_ID = OperatorId;
						if (sourceContact != null)
							message.AddSource(sourceContact);
					}

					entities.SaveChanges();

					return new SendMessageResult
					{
						MessageIds = messages.Select(m => m.MESSAGE_ID).ToArray(),
						Result = MessageProcessingResult.Received
					};
				}

			}
		}
	}
}