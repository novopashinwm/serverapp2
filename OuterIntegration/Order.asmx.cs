﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Security;
using System.Threading;
using System.Web.Script.Services;
using System.Web.Services;
using Compass.Ufin.Orders.Core;
using Compass.Ufin.Orders.Core.Models;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using Microsoft.Extensions.DependencyInjection;
using RU.NVG.UI.WebMapG.Model.Common;

namespace FORIS.TSS.WebServices.OuterIntegration
{
	[WebService(Namespace = XmlNS, Name = "Order", Description = "Веб-сервис для работы с заказами")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[ToolboxItem(false)]
	[ScriptService]
	public class OrderService : BaseWebService
	{
		/// <summary> Блокировка чтения и записи списка заказов, для временной реализации </summary>
		private static ReaderWriterLockSlim _thisReaderWriterLock    = new ReaderWriterLockSlim();
		private IOrdersManagement           GetOrdersManagementService()
		{
			var ordersManagementService = ServerApplication.Server.Instance()
				?.Services                                 // Получаем доступ к сервисам
				?.GetRequiredService<IOrdersManagement>(); // Получаем экземпляр сервиса управления заказами или ошибку
			// Заполняем контекст сервиса идентификатором оператора
			ordersManagementService.OperatorId   = OperatorId;
			// Заполняем контекст сервиса идентификатором департамента, как департамента по умолчанию для оператора
			ordersManagementService.DepartmentId = GetDepartmentByOperatorId(OperatorId)?.id;

			return ordersManagementService;
		}
		[WebMethod(EnableSession = true, Description = "Получить список заказов")]
		public CommonActionResponse<Order[]> GetOrders(DateTime intervalBegUtc, DateTime intervalEndUtc)
		{
			// Трассировка метода
			using var logger = Stopwatcher.GetElapsedLogger($"{Session.SessionID}|{$"login = '{OperatorLogin}', from(Utc):{intervalBegUtc}, to(Utc):{intervalEndUtc}".CallTraceMessage()}");
			// Блокировка метода
			//using var rwLock = _thisReaderWriterLock.ReadLock();
			CheckAuthentication();
			var response = new CommonActionResponse<Order[]>();
			try
			{
				var ordersManagmentService = GetOrdersManagementService();
				response.Object = ordersManagmentService.GetOrders(intervalBegUtc, intervalEndUtc);
				if (null == response.Object)
					response.Result = CommonActionResult.ObjectNotFound;
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();

				response.Object = null;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		[WebMethod(EnableSession = true, Description = "Получить заказ")]
		public CommonActionResponse<Order> GetOrder(long orderId)
		{
			// Трассировка метода
			using var logger = Stopwatcher.GetElapsedLogger($"{Session.SessionID}|{$"login = '{OperatorLogin}', orderId:{orderId}".CallTraceMessage()}");
			// Блокировка метода
			//using var rwLock = _thisReaderWriterLock.ReadLock();
			CheckAuthentication();

			var response = new CommonActionResponse<Order>();
			try
			{
				var ordersManagmentService = GetOrdersManagementService();
				response.Object = ordersManagmentService.GetOrder(orderId);
				if (null == response.Object)
					response.Result = CommonActionResult.ObjectNotFound;
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();

				response.Object = null;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		[WebMethod(EnableSession = true, Description = "Создать заказ")]
		public CommonActionResponse<OrderCreationResponse> Create(OrderCreationRequest request)
		{
			// Трассировка метода
			using var logger = Stopwatcher.GetElapsedLogger($"{Session.SessionID}|{$"login = '{OperatorLogin}'".CallTraceMessage()}");
			// Блокировка метода
			//using var rwLock = _thisReaderWriterLock.WriteLock();

			CheckAuthentication();
			var response = new CommonActionResponse<OrderCreationResponse>();
			try
			{
				if (request == null)
					throw new ArgumentNullException($"{nameof(request)} not be empty.", nameof(request));
				var ordersManagmentService = GetOrdersManagementService();
				var order = ordersManagmentService.GetOrderByExternalId(request.ExternalOrderId);
				if (order != null)
					throw new ArgumentException($"Order '{request.ExternalOrderId}' already exists. Created at '{order.CreatedAtUtc}'.", nameof(request.ExternalOrderId));
				response.Object = ordersManagmentService.Create(request);
				if (null == response.Object)
					response.Result = CommonActionResult.Error;
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();

				response.Object = null;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		[WebMethod(EnableSession = true, Description = "Обновить/изменить заказ")]
		public CommonActionResponse<Order> Update(OrderUpdateRequest request)
		{
			// Трассировка метода
			using var logger = Stopwatcher.GetElapsedLogger($"{Session.SessionID}|{$"login = '{OperatorLogin}'".CallTraceMessage()}");
			// Блокировка метода
			//using var rwLock = _thisReaderWriterLock.WriteLock();

			CheckAuthentication();
			var response = new CommonActionResponse<Order>();
			try
			{
				if (request == null)
					throw new ArgumentNullException($"{nameof(request)} not be empty.", nameof(request));
				var ordersManagmentService = GetOrdersManagementService();
				var updOrder = ordersManagmentService.GetOrder(request.OrderId);
				if (updOrder == default)
					throw new KeyNotFoundException($"{nameof(Order)} with Id={request.OrderId} not found for Operator='{OperatorLogin}'");

				response.Object = ordersManagmentService.Update(request);
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();

				response.Object = null;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		[WebMethod(EnableSession = true, Description = "Удалить заказ")]
		public CommonActionResponse<bool> Delete(long orderId)
		{
			// Трассировка метода
			using var logger = Stopwatcher.GetElapsedLogger($"{Session.SessionID}|{$"login = '{OperatorLogin}', orderId:{orderId}".CallTraceMessage()}");
			// Блокировка метода
			//using var rwLock = _thisReaderWriterLock.WriteLock();

			CheckAuthentication();
			var response = new CommonActionResponse<bool> { Object = false };
			try
			{
				var ordersManagmentService = GetOrdersManagementService();
				var delOrder = ordersManagmentService.GetOrder(orderId);
				if (delOrder == default)
					throw new KeyNotFoundException($"{nameof(Order)} with Id={orderId} not found for Operator='{OperatorLogin}'");

				response.Object = ordersManagmentService.Delete(orderId);
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();

				response.Object = false;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
	}
}