﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading;
using System.Web.Services;
using System.Xml.Serialization;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.Historical;
using FORIS.TSS.BusinessLogic.Enums;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;

namespace FORIS.TSS.WebServices.OuterIntegration
{
	[Serializable]
	[XmlType(TypeName = "GPSDeviceId")]
	public class GPSDevice
	{
		[XmlText]      public string Id;
		[XmlAttribute] public int    VehicleID;
		[XmlAttribute] public string Name;
	}
	[Serializable]
	public class GPSData
	{
		[XmlAttribute]
		public int       VehicleID;
		public string    GPSDeviceId;
		public DateTime? GPSTime;
		public DateTime? LastEventTime;
		public decimal?  Latitude;
		public decimal?  Longitude;
		public int?      SatellitesCount;
		public int?      Speed;
		public int?      Course;
		public Sensor[]  Sensors;
		public int[]     ZoneIds;

		[Serializable]
		public class Sensor
		{
			public string  Name;
			public decimal Value;
		}

		public bool?   IsPositionActual;
		public double? Accuracy;
	}
	[Serializable]
	public class RunIntervalRecord
	{
		/// <summary> Идентификатор объекта наблюдения </summary>
		public int      VehicleId;
		/// <summary> Дата и время начала интервала (в запрашиваемом часовом поясе) </summary>
		public DateTime IntervalBeg;
		/// <summary> Дата и время окончания интервала (в запрашиваемом часовом поясе) </summary>
		public DateTime IntervalEnd;
		/// <summary> Начальное значение накопленного пробега, сантиметры </summary>
		public long?    OdometerBeg;
		/// <summary> Конечное  значение накопленного пробега, сантиметры </summary>
		public long?    OdometerEnd;
		/// <summary> Пройденная за интервал дистанция, метры </summary>
		public long?    Distance;
		/// <summary> Максимальное значение скорости на интервале, км/ч </summary>
		public int?     MaxSpeed;
		/// <summary> Среднее значение скорости на интервале, км/ч </summary>
		public int?     AvgSpeed;
	}
	[WebService(
		Namespace   = XmlNS,
		Name        = "Vehicle",
		Description = "Web service for operating with vehicles")]
	public class Vehicle : BaseWebService
	{
		[WebMethod(EnableSession = true, Description = "Get list of accessible vehicles with detailed information (attributes etc)")]
		[return: XmlRoot("Vehicles")]
		public DTO.Vehicle[] GetVehicles()
		{
			using (new MethodCallLogger(Session.SessionID, "GetVehicles(login={0})", OperatorLogin))
				try
				{
					CheckAuthentication();

					var vehicles = ServerApplication.Server.Instance()
						.GetVehiclesWithPositions(OperatorId, null,
							new GetVehiclesArgs
							{
								IncludeLastData = false
							});

					var result = vehicles.Select(v =>
						new DTO.Vehicle
						{
							VehicleID    = v.id,
							Name         = v.garageNum,
							Brand        = v.brand,
							PublicNumber = v.regNum,
							DeviceID     = v.ctrlNum
						})
						.ToArray();

					return result;
				}
				catch (ThreadAbortException)
				{
					throw;
				}
				catch (Exception ex)
				{
					default(string)
						.WithException(ex)
						.CallTraceError();

					throw;
				}
		}
		[WebMethod(EnableSession = true, Description = "Edit list of vehicles. Returns count of successfully changed vehicles")]
		public int EditVehicles(DTO.Vehicle[] vehicles)
		{
			using (new MethodCallLogger(Session.SessionID, "EditVehicles(login={0})", OperatorLogin))
			{
				try
				{
					CheckAuthentication();

					using (var entities = new Entities())
					{
						var o = entities.GetOperator(OperatorLogin);

						if (vehicles.Length == 0)
							return 0;

						var vehicleIds = vehicles.Select(v => v.VehicleID).ToArray();

						var editedVehicles = entities.VEHICLE
							.Where(v =>
								v.Accesses.Any(access =>
									access.operator_id == o.OPERATOR_ID &&
									access.right_id == (int) SystemRight.EditVehicles) &&
								vehicleIds.Contains(v.VEHICLE_ID))
							.ToDictionary(v => v.VEHICLE_ID);

						foreach (var newVehicle in vehicles)
						{
							VEHICLE editedVehicle;
							if (!editedVehicles.TryGetValue(newVehicle.VehicleID, out editedVehicle))
								continue;

							if (newVehicle.Name != null)
								editedVehicle.GARAGE_NUMBER = newVehicle.Name;

							if (newVehicle.Brand != null)
								editedVehicle.VEHICLE_TYPE = newVehicle.Brand;

							if (newVehicle.PublicNumber != null)
								editedVehicle.PUBLIC_NUMBER = newVehicle.PublicNumber;
						}

						entities.SaveChanges(o);

						return editedVehicles.Count;
					}
				}
				catch (ThreadAbortException)
				{
					throw;
				}
				catch (Exception ex)
				{
					default(string)
						.WithException(ex)
						.CallTraceError();

					throw;
				}

			}
		}
		[WebMethod(EnableSession = true, Description = "Get list of accessible vehicles")]
		[return: XmlRoot("GPSDevices")]
		public GPSDevice[] GetGPSDeviceData()
		{
			using (new MethodCallLogger(Session.SessionID, "GetGPSDeviceData(login={0})", OperatorLogin))
			try
			{
				CheckAuthentication();
				using (var entities = new Entities())
				{
					var devices = entities.GetVehiclesByRight(OperatorId, SystemRight.VehicleAccess)
						.Where(ovr => ovr.CONTROLLER.CONTROLLER_INFO.DEVICE_ID != null)
						.Select(ovr => new
						{
							ovr.VEHICLE_ID,
							ovr.GARAGE_NUMBER,
							ovr.CONTROLLER.CONTROLLER_INFO.DEVICE_ID,
							ViewControllerDeviceIDAllowed = entities.v_operator_vehicle_right.Any(
								imei_ovr =>
									imei_ovr.right_id    == (int)SystemRight.Immobilization &&
									imei_ovr.operator_id == OperatorId &&
									imei_ovr.vehicle_id  == ovr.VEHICLE_ID)
						})
						.ToArray();

					var result = Array.ConvertAll(devices, device =>
						new GPSDevice
						{
							VehicleID = device.VEHICLE_ID,
							Name      = device.GARAGE_NUMBER,
							Id        = device.ViewControllerDeviceIDAllowed && Array.IndexOf(device.DEVICE_ID, (byte)0x00) == -1
								? Encoding.ASCII.GetString(device.DEVICE_ID)
								: null
						});
					return result;
				}
			}
			catch (ThreadAbortException)
			{
				throw;
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex)
					.CallTraceError();

				throw;
			}
		}
		[WebMethod(EnableSession = true, Description = "Get last positions and state information of all the vehicles")]
		[return: XmlRoot("GPSDatas")]
		public GPSData[] GetLastData()
		{
			using (new MethodCallLogger(Session.SessionID, "Vehicle.GetLastData login = {0}", OperatorLogin))
			{
				try
				{
					CheckAuthentication();

					var vehicles = ServerApplication.Server.Instance()
						.GetVehiclesWithPositions(OperatorId, null,
							new GetVehiclesArgs
							{
								IncludeLastData   = true,
								IncludeAttributes = false
							});

					if (vehicles == null || vehicles.Count == 0)
						return null;

					return vehicles.Where(v => v.posLogTime != null).Select(VehicleToGPSData).ToArray();
				}
				catch (Exception ex)
				{
					default(string)
						.WithException(ex)
						.CallTraceError();

					throw;
				}
			}
		}
		private GPSData VehicleToGPSData(BusinessLogic.DTO.Vehicle v)
		{
			return new GPSData
			{
				VehicleID         = v.id,
				GPSDeviceId       = v.ctrlNum,
				GPSTime           = v.posLogTime?.ToUtcDateTime(),
				LastEventTime     = v.logTime?.ToUtcDateTime(),
				Latitude          = (decimal?)v.lat,
				Longitude         = (decimal?)v.lng,
				IsPositionActual  = v.isValid,
				Speed             = v.speed,
				Course            = v.course,
				SatellitesCount   = 4,
				Accuracy          = v.radius,
				ZoneIds           = v.ZoneIds?.ToArray(),
				Sensors           = v.Sensors
					.Where(s => s.LogRecords != null && s.LogRecords.Count != 0)
					.Select(s => new GPSData.Sensor
					{
						Name  = s.Number.ToString(),
						Value = s.LogRecords
							.OrderByDescending(r => r.LogTime)
							.FirstOrDefault()
							.Value
					})
					.ToArray(),
			};
		}
		[WebMethod(EnableSession = true, Description = "Get last position and state information of the vehicle")]
		public GPSData GetLastDataByID(int vehicleID)
		{
			using (new MethodCallLogger(Session.SessionID, "Vehicle.GetLastDataByID login = {0}, vehicleID = {1}", OperatorLogin, vehicleID))
			{
				try
				{
					CheckAuthentication();

					var vehicles = ServerApplication.Server.Instance()
						.GetVehiclesWithPositions(OperatorId, null,
							new GetVehiclesArgs
							{
								VehicleIDs      = new[] { vehicleID },
								IncludeLastData = true
							});

					if (vehicles == null || vehicles.Count == 0)
						return null;
					var v = vehicles[0];

					if (v.posLogTime == null)
						return null;

					return VehicleToGPSData(v);
				}
				catch (Exception ex)
				{
					default(string)
						.WithException(ex)
						.CallTraceError();

					throw;
				}
			}
		}
		[WebMethod(EnableSession = true, Description = "Get position and state information of accessible vehicles")]
		[return: XmlRoot("GPSDatas")]
		public GPSData[] GetGPSDataByContract(string contract_id, DateTime date_from, DateTime date_to, int max_signals)
		{
			MakeIncomingDateTimeToUtcIfNeeded(ref date_from);
			MakeIncomingDateTimeToUtcIfNeeded(ref date_to);

			using (new MethodCallLogger(Session.SessionID, "Vehicle.GetGPSDataByContract login = {4}, contract_id = {0}, date_from = {1}/{5}, date_to = {2}, max_signals = {3}",
				contract_id, date_from, date_to, max_signals, OperatorLogin, date_from.Kind))
				try
				{
					CheckAuthentication();

					var vehicleId  = default(int?);
					using (var entities = new Entities())
					{
						vehicleId  = entities.GetVehicleByDeviceId(contract_id)?.VEHICLE_ID;
						if (vehicleId == null)
						{
							Trace.TraceWarning("Vehicle not found by device_id " + contract_id);
							return null;
						}
					}
					if (!ServerApplication.Server.Instance().IsAllowedVehicleAll(OperatorId, vehicleId.Value, SystemRight.VehicleAccess, SystemRight.PathAccess))
						throw new SecurityException($@"Access forbidden for operator '{OperatorLogin}' to the contract id {contract_id}");

					var logTimeFrom = TimeHelper.GetSecondsFromBase(date_from);
					var logTimeTo   = TimeHelper.GetSecondsFromBase(date_to);

					var log = ServerApplication.Server.Instance()
						.GetFullLog(OperatorId, vehicleId.Value, logTimeFrom, logTimeTo,
							new FullLogOptions
							{
								MaxTotalPointCount = max_signals,
								MaxGeoPointsCount  = max_signals,
								MaxPointsCount     = max_signals
							});

					var result =
					(
						from logTime in log.GetAllLogTimes().Where(value => logTimeFrom <= value && value <= logTimeTo)
						let geo = log.GetCurrentOrPreviousGeoRecord(logTime)
						let gps = log.GetCurrentOrPreviousGpsRecord(logTime)
						select new GPSData
						{
							VehicleID       = vehicleId.Value,
							GPSDeviceId     = contract_id,
							GPSTime         = TimeHelper.GetDateTimeUTC(logTime),
							Latitude        = geo?.Lat,
							Longitude       = geo?.Lng,
							SatellitesCount = gps?.Satellites,
							Speed           = gps?.Speed,
							Course          = gps?.Course,
							Sensors         =
							(
								from sensorLegend in log.Sensors.Keys
								let value = log.SensorByLogTime(sensorLegend, logTime)
								where value.HasValue
								select new GPSData.Sensor
								{
									Name  = sensorLegend.ToString(),
									Value = value.Value
								}
							)
								.ToArray(),
						}
					)
						.Take(max_signals)
						.ToArray();

					return result;
				}
				catch (ThreadAbortException)
				{
					throw;
				}
				catch (Exception ex)
				{
					default(string)
						.WithException(ex)
						.CallTraceError();

					throw;
				}
		}
		[WebMethod(EnableSession = true, Description = "Get position and state information of accessible vehicles")]
		[return: XmlRoot("GPSDatas")]
		public GPSData[] GetLog(int vehicle_id, DateTime date_from, DateTime date_to, int max_signals)
		{
			MakeIncomingDateTimeToUtcIfNeeded(ref date_from);
			MakeIncomingDateTimeToUtcIfNeeded(ref date_to);

			using (new MethodCallLogger(Session.SessionID, "Vehicle.GetLog login = {4}, vehicle_id = {0}, date_from = {1}/{5}, date_to = {2}, max_signals = {3}",
				vehicle_id, date_from, date_to, max_signals, OperatorLogin, date_from.Kind))
				try
				{
					CheckAuthentication();
					var deviceId   = default(string);
					using (var entities = new Entities())
					{
						var vehicleWithDeviceId = entities.VEHICLE
							.Include(VEHICLE.CONTROLLERIncludePath)
							.Where(v => v.VEHICLE_ID == vehicle_id)
							.Select(v => new
							{
								vehicle   = v,
								device_id = v.CONTROLLER.CONTROLLER_INFO.DEVICE_ID
							})
							.FirstOrDefault();
						if (vehicleWithDeviceId == null)
						{
							Trace.TraceWarning("Vehicle not found by vehicle_id " + vehicle_id);
							return null;
						}

						if (vehicleWithDeviceId.device_id != null)
							deviceId = Encoding.ASCII.GetString(vehicleWithDeviceId.device_id);
					}
					if (!ServerApplication.Server.Instance().IsAllowedVehicleAll(OperatorId, vehicle_id, SystemRight.VehicleAccess, SystemRight.PathAccess))
						throw new SecurityException($@"Access forbidden for operator '{OperatorLogin}' to the vehicle id {vehicle_id}");

					var vehicles = ServerApplication.Server.Instance()
						.GetVehiclesWithPositions(OperatorId, null,
							new GetVehiclesArgs
							{
								VehicleIDs      = new[] { vehicle_id },
								IncludeLastData = false
							});

					if (0 == (vehicles?.Count ?? 0))
					{

						Trace.TraceWarning($"Vehicle not found by vehicle_id {vehicle_id}");
						return null;
					}

					var logTimeFrom = TimeHelper.GetSecondsFromBase(date_from);
					var logTimeTo   = TimeHelper.GetSecondsFromBase(date_to);

					var log = ServerApplication.Server.Instance()
						.GetFullLog(OperatorId, vehicle_id, logTimeFrom, logTimeTo,
							new FullLogOptions
							{
								MaxTotalPointCount = max_signals,
								MaxGeoPointsCount  = max_signals,
								MaxPointsCount     = max_signals
							});

					var result =
					(
						from logTime in log.GetAllLogTimes().Where(value => logTimeFrom <= value && value <= logTimeTo)
						let geo = log.GetCurrentOrPreviousGeoRecord(logTime)
						let gps = log.GetCurrentOrPreviousGpsRecord(logTime)
						select new GPSData
						{
							VehicleID       = vehicle_id,
							GPSDeviceId     = deviceId,
							GPSTime         = TimeHelper.GetDateTimeUTC(logTime),
							Latitude        = geo?.Lat,
							Longitude       = geo?.Lng,
							SatellitesCount = gps?.Satellites,
							Speed           = gps?.Speed,
							Course          = gps?.Course,
							Sensors         =
							(
								from sensorLegend in log.Sensors.Keys
								let value = log.SensorByLogTime(sensorLegend, logTime)
								where value.HasValue
								select new GPSData.Sensor
								{
									Name  = sensorLegend.ToString(),
									Value = value.Value
								}
							)
								.ToArray(),
						}
					)
						.Take(max_signals).ToArray();

					return result;
				}
				catch (ThreadAbortException)
				{
					throw;
				}
				catch (Exception ex)
				{
					default(string)
						.WithException(ex)
						.CallTraceError();

					throw;
				}
		}
		[WebMethod(EnableSession = true, Description = "Get position and state information of vehicle to date")]
		[return: XmlRoot("GPSDatas")]
		public GPSData[] GetInstantData(DateTime dateTime)
		{
			MakeIncomingDateTimeToUtcIfNeeded(ref dateTime);

			using (new MethodCallLogger(Session.SessionID, "Vehicle.GetInstantData login = {0}, dateTime = {1}/{2}",
				OperatorLogin, dateTime, dateTime.Kind))
				try
				{
					CheckAuthentication();
					var operatorId = default(int);
					using (var entities = new Entities())
					{
						// Если оператора нет, выдается исключение, поэтому нет проверок на null
						operatorId = entities.GetOperator(OperatorLogin).OPERATOR_ID;
					}
					var vehicles = ServerApplication.Server.Instance()
						.GetVehiclesWithPositions(operatorId, null,
							new GetVehiclesArgs
							{
								CurrentDate       = dateTime,
								IncludeLastData   = true,
								IncludeZones      = true,
								IncludeAttributes = false
							});
					var result   = vehicles
						.Select(VehicleToGPSData)
						.ToArray();

					return result;
				}
				catch (ThreadAbortException)
				{
					throw;
				}
				catch (Exception ex)
				{
					default(string)
						.WithException(ex)
						.CallTraceError();

					throw;
				}
		}
		[WebMethod(EnableSession = true, Description = "Get data on runs grouped by days")]
		[return: XmlRoot("RunSummaryByDays")]
		public RunIntervalRecord[] GetRunSummaryByDays(int vehicleId, DateTime dateTimeBeg, DateTime dateTimeEnd, string timeZoneId = null)
		{
			var result = default(RunIntervalRecord[]);
			using (new MethodCallLogger(Session.SessionID, "Vehicle.GetRunSummaryByDays login = {5}, vehicleId = {0}, dateTimeBeg = {1}/{2}, dateTimeEnd = {3}/{4}",
				vehicleId, dateTimeBeg, dateTimeBeg.Kind, dateTimeEnd, dateTimeEnd.Kind, OperatorLogin))
				try
				{
					CheckAuthentication();

					MakeIncomingDateTimeToUtcIfNeeded(ref dateTimeBeg);
					MakeIncomingDateTimeToUtcIfNeeded(ref dateTimeEnd);

					var serverBasicFunctionSet = ServerApplication.Server.Instance() as IBasicFunctionSet;
					if (null == serverBasicFunctionSet)
						throw new ApplicationException($@"Server not found");

					if (!serverBasicFunctionSet.IsAllowedVehicleAll(OperatorId, vehicleId, SystemRight.VehicleAccess, SystemRight.PathAccess))
						throw new SecurityException($@"Access forbidden for operator '{OperatorLogin}' to the vehicle id {vehicleId}");

					var logTimeBeg   = dateTimeBeg.ToLogTime();
					var logTimeEnd   = dateTimeEnd.ToLogTime();
					var timeZoneInfo = TimeZoneInfo.GetSystemTimeZones()
						.FirstOrDefault(z => z.Id == timeZoneId) ?? TimeZoneInfo.Utc;
					var locDateBeg   = TimeHelper.GetLocalTime(logTimeBeg, timeZoneInfo);
					var locDateEnd   = TimeHelper.GetLocalTime(logTimeEnd, timeZoneInfo);
					var fullLog    = serverBasicFunctionSet.GetFullLog(OperatorId, vehicleId, logTimeBeg, logTimeEnd,
						new FullLogOptions
						{
							MaxTotalPointCount = logTimeEnd - logTimeBeg,
							MaxGeoPointsCount  = logTimeEnd - logTimeBeg,
							MaxPointsCount     = logTimeEnd - logTimeBeg,
							IncludeAddresses   = false,
						});
					result = TimeHelper.GetDayList(locDateBeg, locDateEnd)
						?.GroupJoin(fullLog.Runs,
							day => day.Date,
							run => TimeHelper.GetLocalTime(run.LogTime, timeZoneInfo).Date,
							(day, run) =>
							{
								// Границы интервала в локальном времени результата (указанном в параметре timeZoneId)
								var itvDateBeg  = locDateBeg > day              ? locDateBeg : day;
								var itvDateEnd  = locDateEnd < day.ToEndOfDay() ? locDateEnd : day.ToEndOfDay();
								// Границы интервала в секундах от базового времени (logTime)
								var logDateBeg  = TimeHelper.GetUtcTime(itvDateBeg, timeZoneInfo).ToLogTime();
								var logDateEnd  = TimeHelper.GetUtcTime(itvDateEnd, timeZoneInfo).ToLogTime();
								// Значения одометра на границах интервала
								var runOdomBeg  = fullLog.GetInterpolatedOdometerValue(logDateBeg);
								var runOdomEnd  = fullLog.GetInterpolatedOdometerValue(logDateEnd);
								var runDistance = (runOdomEnd - runOdomBeg) / 100L;
								// Средняя и максимальная скорости на интервале
								var avgSpeed    = fullLog.Geo.Where(s => logDateBeg <= s.LogTime && s.LogTime < logDateEnd).Average(s => s.Speed);
								var maxSpeed    = fullLog.Geo.Where(s => logDateBeg <= s.LogTime && s.LogTime < logDateEnd).Max(s => s.Speed);
								// Возвращаемый результат
								return new RunIntervalRecord
								{
									VehicleId   = vehicleId,
									IntervalBeg = itvDateBeg,
									IntervalEnd = itvDateEnd,
									OdometerBeg = (long?)runOdomBeg,
									OdometerEnd = (long?)runOdomEnd,
									Distance    = (long?)runDistance,
									AvgSpeed    = (int?)avgSpeed,
									MaxSpeed    = (int?)maxSpeed,
								};
							})
						?.ToArray();
					return result;
				}
				catch (Exception ex)
				{
					default(string)
						.WithException(ex)
						.CallTraceError();
					throw;
				}
		}
		private static void MakeIncomingDateTimeToUtcIfNeeded(ref DateTime dateTime)
		{
			if (dateTime.Kind == DateTimeKind.Local)
				dateTime = TimeZoneInfo.ConvertTimeToUtc(dateTime);
		}
	}
}