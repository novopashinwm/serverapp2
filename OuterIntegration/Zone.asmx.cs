﻿using System;
using System.Diagnostics;
using System.Web.Services;
using System.Xml.Serialization;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.EntityModel;

namespace FORIS.TSS.WebServices.OuterIntegration
{
	[WebService(
		Namespace   = XmlNS,
		Name        = "Zone",
		Description = "Web service for operating with geo zones")]
	public class Zone : BaseWebService
	{
		[WebMethod(EnableSession = true, Description = "Get geo zones for current user")]
		[return: XmlRoot("GeoZones")]
		public GeoZone[] GetZones()
		{
			using (new MethodCallLogger(Session.SessionID, "Zone.GetZones login = {0}", OperatorLogin))
			{
				try
				{
					CheckAuthentication();

					using var entities = new Entities();

					var operatorId = entities.GetOperator(OperatorLogin).OPERATOR_ID;

					var zones = ServerApplication.Server.Instance()
						.GetGeoZones(operatorId, null, false);

					return zones.ToArray();
				}
				catch (Exception exception)
				{
					Trace.TraceError(exception.ToString());
					throw;
				}
			}
		}
	}
}