﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using FORIS.TSS.BusinessLogic;
using FORIS.TSS.Common;
using FORIS.TSS.Common.Helpers;
using FORIS.TSS.EntityModel;
using RU.NVG.UI.WebMapG.Model.Common;
using Dto = FORIS.TSS.BusinessLogic.DTO;

namespace FORIS.TSS.WebServices.OuterIntegration
{
	[WebService(Namespace = XmlNS, Name = "Event", Description = "Веб-сервис для работы с событиями")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[ToolboxItem(false)]
	[ScriptService]
	public class EventService : BaseWebService
	{
		/// <summary> Отправить событие о нарушении вождения </summary>
		/// <param name="eventData"> Данные события о нарушении режима вождения </param>
		/// <returns></returns>
		[WebMethod(EnableSession = true, Description = "Отправить событие о нарушении вождения")]
		public CommonActionResponse<bool> SendEventViolationOfDriving(Dto::EventViolationOfDrivingData eventData)
		{
			// Трассировка метода
			using var logger = Stopwatcher.GetElapsedLogger($"{Session.SessionID}|{$"login = '{OperatorLogin}'".CallTraceMessage()}");
			// Блокировка метода
			//using var rwLock = _thisReaderWriterLock.WriteLock();

			CheckAuthentication();
			var response = new CommonActionResponse<bool> { Object = false };
			try
			{
				// Проверки исходных данных
				//////////////////////////////////////////////////
				if (eventData == null)
					throw new ArgumentException($"The event violation data not be missing.", nameof(eventData));
				//////////////////////////////////////////////////
				if (string.IsNullOrWhiteSpace(eventData.DeviceId))
					throw new ArgumentException($"Device external identifier not specified.", nameof(eventData.DeviceId));
				//////////////////////////////////////////////////
				if (string.IsNullOrWhiteSpace(eventData.DriverId))
					throw new ArgumentException($"Driver identifier not specified.", nameof(eventData.DriverId));
				//////////////////////////////////////////////////
				if (string.IsNullOrWhiteSpace(eventData.DriverName))
					throw new ArgumentException($"Driver name not specified.", nameof(eventData.DriverName));
				//////////////////////////////////////////////////
				if (string.IsNullOrWhiteSpace(eventData.EventVideoUrl))
					throw new ArgumentException($"Violation video url not specified.", nameof(eventData.EventVideoUrl));
				//////////////////////////////////////////////////
				// Контекст БД
				using var entities = new Entities();
				// Пользователь должен быть пользователем департамента
				var operatorDepartment = GetDepartmentByOperatorId(OperatorId);
				if (operatorDepartment == default)
					throw new SecurityException($@"Access forbidden for operator '{OperatorLogin}'");

				// Поиск объекта наблюдения в текущем департаменте
				var binDeviceId = Encoding.ASCII.GetBytes(eventData.DeviceId);
				var ormVechicle = entities.VEHICLE
					?.Include($"{VEHICLE.CONTROLLERIncludePath}")
					?.Include($"{VEHICLE.CONTROLLERIncludePath}.{CONTROLLER.CONTROLLER_INFOIncludePath}")
					?.FirstOrDefault(v =>
						v.Department_ID                        == operatorDepartment.id &&
						v.CONTROLLER.CONTROLLER_INFO.DEVICE_ID == binDeviceId);
				if (ormVechicle == default)
					throw new KeyNotFoundException($"Object not found by DeviceId='{eventData.DeviceId}' in '{operatorDepartment.name}'.");
				// Проверяем необходимые права на объект
				var neededRights = new[] { SystemRight.VehicleAccess, SystemRight.PathAccess, SystemRight.ViewingSensorValues };
				if (!entities.IsAllowedVehicleAll(OperatorId, ormVechicle.VEHICLE_ID, neededRights))
					throw new SecurityException($"Operator '{OperatorLogin}' does not have the necessary rights to the object '{eventData.DeviceId}'.");

				var outMessages = new List<MESSAGE>();
				// Ищем операторов для оповещения
				var operatorIds = ServerApplication.Server.Instance().GetVehicleOperators(ormVechicle.VEHICLE_ID) ?? Array.Empty<int>();
				foreach (var operatorId in operatorIds)
				{
					// Не посылаем сами себе и суперадминам
					//if (operatorId == OperatorId || entities.IsSuperAdministrator(operatorId))
					// TODO: Временно посылаем и себе
					if (entities.IsSuperAdministrator(operatorId))
						continue;
					// Ищем оператора которому нужно послать уведомление
					var ormOperator = entities.OPERATOR.FirstOrDefault(o => o.OPERATOR_ID == operatorId);
					if (ormOperator == default)
						continue;
					// Ищем шаблон сообщения для языка оператора
					var ormTemplate = entities.GetMessageTemplate(Dto::Messages.MessageTemplate.EventViolationOfDriving, ormOperator.Culture);
					if (ormTemplate == default)
						continue;
					// Дата события в часовом поясе оператора
					var eventDate = TimeZoneInfo.ConvertTimeFromUtc(eventData.EventTimeUtc, ormOperator.TimeZoneInfoInstance)
						.ToString(TimeHelper.DefaultTimeFormatUpToMinutes);
					// Получить имя объекта для оператора
					var vehicleName = ServerApplication.Server.Instance().GetVehicleName(ormOperator.OPERATOR_ID, ormVechicle.VEHICLE_ID);
					// Создаем временное сообщение
					var ormMessage = new MESSAGE();
					AddMessageFields(entities, ormMessage, eventData, ormVechicle, vehicleName);
					ormMessage.SetContent(ormTemplate);
					// Добавляем сообщения для всех действующих AppId оператора
					outMessages.AddRange(entities.CreateAppNotification(ormOperator.OPERATOR_ID, ormMessage.SUBJECT, ormMessage.BODY, eventDate));
					// Добавляем сообщение для Web
					outMessages.Add     (entities.CreateWebNotification(ormOperator.OPERATOR_ID, ormMessage.SUBJECT, ormMessage.BODY));
					// Заполняем шаблон и поля сообщений
					foreach (var outMessage in outMessages)
					{
						outMessage.MESSAGE_TEMPLATE = ormTemplate;
						AddMessageFields(entities, outMessage, eventData, ormVechicle, vehicleName);
					}
				}
				if (outMessages.Count > 0)
					entities.SaveChanges();

				response.Object = true;
				if (!response.Object)
					response.Result = CommonActionResult.Error;
			}
			catch (Exception ex)
			{
				default(string)
					.WithException(ex, true)
					.CallTraceError();

				response.Object = false;
				response.Result = CommonActionResultExceptionMap
					.GetCommonActionResultByExceptionType(ex.GetType());
				response.ResultText = ex.Message;
			}
			return response;
		}
		private void AddMessageFields(Entities entities, MESSAGE ormMessage, Dto::EventViolationOfDrivingData eventData, VEHICLE ormVechicle, string vehicleName)
		{
			entities.AddMessageField(ormMessage, nameof(eventData.DeviceId),                   eventData.DeviceId);
			entities.AddMessageField(ormMessage, nameof(eventData.DriverId),                   eventData.DriverId);
			entities.AddMessageField(ormMessage, nameof(eventData.DriverName),                 eventData.DriverName);
			entities.AddMessageField(ormMessage, nameof(eventData.EventTimeUtc),               eventData.EventTimeUtc);
			entities.AddMessageField(ormMessage, nameof(eventData.EventType),                  eventData.EventType.ToString());
			entities.AddMessageField(ormMessage, nameof(eventData.EventVideoUrl),              eventData.EventVideoUrl);
			entities.AddMessageField(ormMessage, Dto::Messages.MessageTemplateField.VehicleID, ormVechicle.VEHICLE_ID);
			entities.AddMessageField(ormMessage, "VehicleName",                                vehicleName);
		}
	}
}