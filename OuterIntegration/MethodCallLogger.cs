﻿using System;
using System.Diagnostics;
using System.Xml;

namespace FORIS.TSS.WebServices.OuterIntegration
{
	class MethodCallLogger : IDisposable
	{
		private readonly Stopwatch _stopwatch = new Stopwatch();
		private readonly string    _caption;
		private readonly string    _sessionKey;
		public MethodCallLogger(string sessionKey, string format, params object[] arguments)
		{
			_sessionKey = sessionKey;

			for (var i = 0; i != arguments.Length; ++i)
			{
				var a = arguments[i];
				if (a is DateTime)
					arguments[i] = XmlConvert.ToString((DateTime) a, XmlDateTimeSerializationMode.Utc);
			}

			_caption = string.Format(format, arguments);
			Trace.TraceInformation(_sessionKey + ": " + _caption);

			_stopwatch.Start();
		}

		public void Dispose()
		{
			_stopwatch.Stop();
			Trace.TraceInformation(_sessionKey + ": Action has taken " + _stopwatch.ElapsedMilliseconds + "ms: " + _caption);
		}
	}
}