﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Web.Services;
using FORIS.TSS.BusinessLogic.DTO;
using FORIS.TSS.BusinessLogic.DTO.OperatorRights;
using FORIS.TSS.EntityModel;

namespace FORIS.TSS.WebServices.OuterIntegration
{
	public class BaseWebService : WebService
	{
		public const string XmlNS = "https://out.ufin.online/";

		private Operator GetOperatorByLogin(string login)
		{
			using var entities = new Entities();
			return entities.GetOperatorOrDefault(login)
				?.ToDto();
		}
		private string GetLoginFromIdentity()
		{
			var identity = Context?.User?.Identity;
			if (identity?.IsAuthenticated ?? false)
				return default;
			if (string.IsNullOrWhiteSpace(identity?.Name))
				return default;
			return identity?.Name;
		}
		private Operator GetOperatorContext()
		{
			// Уже есть оператор
			if (_operator != default)
				return _operator;

			// Ищем в HttpContext
			var loginFromIdentity = GetLoginFromIdentity();
			if (!string.IsNullOrWhiteSpace(loginFromIdentity))
				return _operator = GetOperatorByLogin(loginFromIdentity);

			// Ищем в сессии
			var loginFromSession = Context.Session["User"] as string;
			if (!string.IsNullOrWhiteSpace(loginFromSession))
				return _operator = GetOperatorByLogin(loginFromSession);

			// Ищем в конфигурационном файле (сомнительно, что используется)
			var loginFromSettings = ConfigurationManager.AppSettings["operator"];
			if (!string.IsNullOrWhiteSpace(loginFromSettings))
				return _operator = GetOperatorByLogin(loginFromSession);

			return default;
		}
		protected Department GetDepartmentByOperatorId(int operatorId)
		{
			if (0 >= operatorId)
				return default;
			using var entities = new Entities();
			return entities.GetDefaultDepartmentByOperatorId(operatorId)
				?.ToDto();
		}
		protected void CheckAuthentication()
		{
			if (string.IsNullOrEmpty(OperatorLogin))
				UnAuthorized();
		}
		private   Operator   _operator;
		protected Operator   Operator      => GetOperatorContext();
		protected string     OperatorLogin => Operator?.login;
		protected int        OperatorId    => Operator?.id ?? 0;
		protected void SetSessionUser(string login)
		{
			Context.Session["User"] = login;
		}
		protected void UnAuthorized()
		{
			Trace.TraceWarning(Session.SessionID + ": Request is unauthorized - result code 401 was sent to client");
			Context.Response.Clear();
			Context.Response.StatusCode = 401;
			Context.Response.End();
		}
		protected void Forbidden()
		{
			Context.Response.Clear();
			Context.Response.StatusCode = 403;
			Context.Response.End();
		}
	}
}