﻿@@ECHO OFF

REM Переходим в папку в которой лежит этот командный файл
cd "%~dp0"
REM Устанавливаем пути к программам
REM https://github.com/microsoft/vswhere/releases
SET VSWHERE=%ProgramFiles(x86)%\Microsoft Visual Studio\Installer\vswhere.exe
IF EXIST "%VSWHERE%" (
	FOR /f "usebackq tokens=*" %%i in (`"%VSWHERE%" -latest -requires Microsoft.Component.MSBuild -find Common7\Tools\VsDevCmd.bat`) DO (
		SET VSCMDLN=%%i
	)
)
SET PATHCMD=%~dp0
SET PATHBLD=%PATHCMD%
SET PATHVER=%PATHCMD%\..\!Versions

SET FILEOUT=%PATHVER%\Compass.Ufin.OuterIntegrationClient
SET FILEDST=%FILEOUT%.DLL
SET FILESRC=%FILEOUT%.CS

CALL "%VSCMDLN%"

cls && WSDL.EXE /nologo /language:CS /fields /namespace:Compass.Ufin.OuterIntegrationClient /out:"%FILESRC%" ^
https://out.ufin.online/Default.asmx ^
https://out.ufin.online/EmergencyCall.asmx ^
https://out.ufin.online/GeoPoint.asmx ^
https://out.ufin.online/Message.asmx ^
https://out.ufin.online/Reports.asmx ^
https://out.ufin.online/Vehicle.asmx ^
https://out.ufin.online/VehicleGroup.asmx ^
https://out.ufin.online/Zone.asmx ^
https://out.ufin.online/Order.asmx

CSC /target:library /out:"%FILEDST%"  "%FILESRC%"
DEL /Q "%FILESRC%"