@@ECHO OFF
CHCP 65001
REM Переходим в папку в которой лежит этот командный файл
cd "%~dp0"
REM Устанавливаем пути к программам
REM https://github.com/microsoft/vswhere/releases
SET VSWHERE=%ProgramFiles(x86)%\Microsoft Visual Studio\Installer\vswhere.exe
IF EXIST %VSWHERE% (
	FOR /f "usebackq tokens=*" %%i in (`"%VSWHERE%" -latest -requires Microsoft.Component.MSBuild -find MSBuild\**\Bin\MSBuild.exe`) DO (
		SET MSBUILD=%%i
	)
)

SET Z7BUILD=C:\Program Files\7-Zip\7z.exe
SET PATHCMD=%~dp0
SET PATHBLD=%PATHCMD%
SET PATHW02=%PATHCMD%\..\WEB\WEB2
SET PATHVER=%PATHCMD%\..\!Versions
SET PATHLST=%PATHVER%\LatestVersion
SET PATHTLS=%PATHCMD%\..\!Tools
SET PATHTMP=%PATHCMD%\..\!Temp
SET BRWRRUS=%PATHTMP%\BRWRRUS
SET BRWDRUS=%PATHTMP%\BRWDRUS
SET BRWRIND=%PATHTMP%\BRWRIND
SET BRWDIND=%PATHTMP%\BRWDIND
REM  SET BRWRAGP=%PATHTMP%\BRWRAGP
REM  SET BRWDAGP=%PATHTMP%\BRWDAGP
REM  SET BRWRTLT=%PATHTMP%\BRWRTLT
REM  SET BRWDTLT=%PATHTMP%\BRWDTLT

SET CPFILES=%PATHTLS%\CopyFiles.exe
SET DTFILES=%PATHTLS%\SetFileNameAsDateTime.exe
SET NGBUILD=%PATHCMD%\.nuget\nuget.exe

IF NOT EXIST "%PATHVER%" MD "%PATHVER%"
IF NOT EXIST "%PATHTLS%" MD "%PATHTLS%"
IF NOT EXIST "%PATHTMP%" MD "%PATHTMP%"
IF NOT EXIST "%PATHLST%" MD "%PATHLST%"

IF "%1"=="COMPRESS"  GOTO COMPRESS
(IF EXIST "%PATHLST%" RD "%PATHLST%" /S /Q) && MD "%PATHLST%"
REM Получение последних версий из Git
CLS
CD /D "%PATHW02%" && git clean -xfd && git pull && git status || pause

IF "%1"==""          GOTO ALL
IF "%1"=="BUILDALL"  GOTO ALL
IF "%1"=="PRDWEB"    GOTO PRDWEB
IF "%1"=="PRDIND"    GOTO PRDIND
IF "%1"=="DEVWEB"    GOTO DEVWEB
IF "%1"=="DEVDEV"    GOTO DEVDEV
REM  IF "%1"=="PRDAGP"    GOTO PRDAGP
REM  IF "%1"=="PRDTLT"    GOTO PRDTLT

ECHO.
ECHO Значение входного параметра незвестно

:SYNTAX
ECHO.
ECHO Возможные варианты запуска:
ECHO Сборка всех проектов и создание версии:
ECHO %~nx0
ECHO Сборка всех проектов и создание версии без сжатия:
ECHO %~nx0 BUILDALL
ECHO Публикация сайта Ufin(v2), браузерная часть:
ECHO %~nx0 PRDWEB
ECHO Публикация сайта nika-gps.in, браузерная часть:
ECHO %~nx0 PRDIND
ECHO Публикация сайта DevSite/Web, браузерная часть:
ECHO %~nx0 DEVWEB
ECHO Публикация сайта DevSite/Dev, браузерная часть:
ECHO %~nx0 DEVDEV
REM  ECHO Публикация сайта web.anygps.ru, браузерная часть:
REM  ECHO %~nx0 PRDAGP
REM  ECHO Публикация сайта web.teltonika-gps.ru, браузерная часть:
REM  ECHO %~nx0 PRDTLT
ECHO Только сжатие подготовленной версии
ECHO %~nx0 COMPRESS

ECHO.
GOTO EXIT

:BLDRRUS
CD /D "%PATHW02%" && git clean -xfd && (IF NOT DEFINED BLDRRUS CALL build-release-UFIN.cmd    ) && (IF EXIST "%BRWRRUS%" RD "%BRWRRUS%" /S /Q) && MD "%BRWRRUS%" && ("%CPFILES%" "%PATHW02%\pub" "%BRWRRUS%") && SET BLDRRUS="Y" && EXIT /B %ERRORLEVEL% || EXIT /B %ERRORLEVEL%
:BLDDRUS
CD /D "%PATHW02%" && git clean -xfd && (IF NOT DEFINED BLDDRUS CALL build-debug-UFIN.cmd      ) && (IF EXIST "%BRWDRUS%" RD "%BRWDRUS%" /S /Q) && MD "%BRWDRUS%" && ("%CPFILES%" "%PATHW02%\pub" "%BRWDRUS%") && SET BLDDRUS="Y" && EXIT /B %ERRORLEVEL% || EXIT /B %ERRORLEVEL%
:BLDRIND
CD /D "%PATHW02%" && git clean -xfd && (IF NOT DEFINED BLDRIND CALL build-release-TEKONIKA.cmd) && (IF EXIST "%BRWRIND%" RD "%BRWRIND%" /S /Q) && MD "%BRWRIND%" && ("%CPFILES%" "%PATHW02%\pub" "%BRWRIND%") && SET BLDRIND="Y" && EXIT /B %ERRORLEVEL% || EXIT /B %ERRORLEVEL%
:BLDDIND
CD /D "%PATHW02%" && git clean -xfd && (IF NOT DEFINED BLDDIND CALL build-debug-TEKONIKA.cmd  ) && (IF EXIST "%BRWDIND%" RD "%BRWDIND%" /S /Q) && MD "%BRWDIND%" && ("%CPFILES%" "%PATHW02%\pub" "%BRWDIND%") && SET BLDDIND="Y" && EXIT /B %ERRORLEVEL% || EXIT /B %ERRORLEVEL%
REM  :BLDRAGP
REM  CD /D "%PATHW02%" && git clean -xfd && (IF NOT DEFINED BLDRAGP CALL build-release-ANYGPS.cmd  ) && (IF EXIST "%BRWRAGP%" RD "%BRWRAGP%" /S /Q) && MD "%BRWRAGP%" && ("%CPFILES%" "%PATHW02%\pub" "%BRWRAGP%") && SET BLDRAGP="Y" && EXIT /B %ERRORLEVEL% || EXIT /B %ERRORLEVEL%
REM  :BLDDAGP
REM  CD /D "%PATHW02%" && git clean -xfd && (IF NOT DEFINED BLDDAGP CALL build-debug-ANYGPS.cmd    ) && (IF EXIST "%BRWDAGP%" RD "%BRWDAGP%" /S /Q) && MD "%BRWDAGP%" && ("%CPFILES%" "%PATHW02%\pub" "%BRWDAGP%") && SET BLDDAGP="Y" && EXIT /B %ERRORLEVEL% || EXIT /B %ERRORLEVEL%
REM  :BLDRTLT
REM  CD /D "%PATHW02%" && git clean -xfd && (IF NOT DEFINED BLDRTLT CALL build-release-TLTGPS.cmd  ) && (IF EXIST "%BRWRTLT%" RD "%BRWRTLT%" /S /Q) && MD "%BRWRTLT%" && ("%CPFILES%" "%PATHW02%\pub" "%BRWRTLT%") && SET BLDRTLT="Y" && EXIT /B %ERRORLEVEL% || EXIT /B %ERRORLEVEL%
REM  :BLDDTLT
REM  CD /D "%PATHW02%" && git clean -xfd && (IF NOT DEFINED BLDDTLT CALL build-debug-TLTGPS.cmd    ) && (IF EXIST "%BRWDTLT%" RD "%BRWDTLT%" /S /Q) && MD "%BRWDTLT%" && ("%CPFILES%" "%PATHW02%\pub" "%BRWDTLT%") && SET BLDDTLT="Y" && EXIT /B %ERRORLEVEL% || EXIT /B %ERRORLEVEL%

:ALL
:PRDWEB
REM Публикация сайта Ufin(v2), браузерная часть:
CLS
CD /D "%PATHCMD%"
SET DIRDST1=%PATHLST%\Web\Ufin
SET DIRSRC1=%BRWRRUS%
(IF NOT DEFINED BLDRRUS CALL :BLDRRUS) && ("%CPFILES%" "%DIRSRC1%" "%DIRDST1%") || PAUSE
IF "%1"=="PRDWEB" GOTO COMPRESS

:PRDIND
REM Публикация сайта nika-gps.in, браузерная часть:
CLS
CD /D "%PATHCMD%"
SET DIRDST1=%PATHLST%\Web\in.nika-gps.https
SET DIRSRC1=%BRWRIND%
(IF NOT DEFINED BLDRIND CALL :BLDRIND) && ("%CPFILES%" "%DIRSRC1%" "%DIRDST1%") || PAUSE
IF "%1"=="PRDIND" GOTO COMPRESS

:DEVWEB
REM Публикация сайта DevSite/Web, браузерная часть:
CLS
CD /D "%PATHCMD%"
SET DIRDST1=%PATHLST%\Web\DevSite\Web
SET DIRSRC1=%BRWDRUS%
(IF NOT DEFINED BLDDRUS CALL :BLDDRUS) && ("%CPFILES%" "%DIRSRC1%" "%DIRDST1%") || PAUSE
IF "%1"=="DEVWEB" GOTO COMPRESS

:DEVDEV
REM Публикация сайта DevSite/Dev, браузерная часть:
CLS
CD /D "%PATHCMD%"
SET DIRDST1=%PATHLST%\Web\DevSite\Dev
SET DIRSRC1=%BRWDIND%
(IF NOT DEFINED BLDDIND CALL :BLDDIND) && ("%CPFILES%" "%DIRSRC1%" "%DIRDST1%") || PAUSE
IF "%1"=="DEVDEV" GOTO COMPRESS

REM  :PRDAGP
REM  REM Публикация сайта web.anygps.ru, браузерная часть:
REM  CLS
REM  CD /D "%PATHCMD%"
REM  SET DIRDST1=%PATHLST%\Web\ru.anygps.web.https
REM  SET DIRSRC1=%BRWRAGP%
REM  (IF NOT DEFINED BLDRAGP CALL :BLDRAGP) && ("%CPFILES%" "%DIRSRC1%" "%DIRDST1%") || PAUSE
REM  IF "%1"=="PRDAGP" GOTO COMPRESS

REM  :PRDTLT
REM  REM Публикация сайта web.teltonika-gps.ru, браузерная часть:
REM  CLS
REM  CD /D "%PATHCMD%"
REM  SET DIRDST1=%PATHLST%\Web\ru.teltonika-gps.web.https
REM  SET DIRSRC1=%BRWRTLT%
REM  (IF NOT DEFINED BLDRTLT CALL :BLDRTLT) && ("%CPFILES%" "%DIRSRC1%" "%DIRDST1%") || PAUSE
REM  IF "%1"=="PRDTLT" GOTO COMPRESS

IF "%1"==""         GOTO EXIT
IF "%1"=="BUILDALL" GOTO EXIT

:COMPRESS
REM Подготовка архива
CLS
CD /D "%PATHCMD%"
"%Z7BUILD%" a -r -tzip -mx3 "%PATHVER%\LatestVersion.zip" "%PATHLST%\*" && "%DTFILES%" "%PATHVER%\LatestVersion.zip" && RMDIR "%PATHLST%" /S /Q && echo  || pause

:EXIT